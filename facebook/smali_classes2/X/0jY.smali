.class public LX/0jY;
.super LX/0jZ;
.source ""


# static fields
.field private static final b:[I


# instance fields
.field public final a:I
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field private final c:LX/1gi;

.field private final d:LX/0pV;

.field private final e:LX/1gf;

.field private final f:LX/0SG;

.field private final g:Lcom/facebook/api/feedtype/FeedType;

.field private final h:LX/0rB;

.field private final i:LX/1gn;

.field private final j:LX/1go;

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0qV;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1jj;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0jU;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/1gp;

.field private o:Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;

.field private p:LX/1jW;

.field public q:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 123804
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/0jY;->b:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x2
        0x3
        0x1
    .end array-data
.end method

.method public constructor <init>(LX/0pV;LX/0SG;LX/1gn;LX/1go;Landroid/os/Looper;LX/1gi;LX/1gf;Lcom/facebook/api/feedtype/FeedType;LX/0rB;)V
    .locals 1
    .param p5    # Landroid/os/Looper;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/1gi;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/1gf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/api/feedtype/FeedType;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/0rB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 123786
    sget-object v0, LX/0jY;->b:[I

    invoke-direct {p0, p5, v0}, LX/0jZ;-><init>(Landroid/os/Looper;[I)V

    .line 123787
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 123788
    iput-object v0, p0, LX/0jY;->k:LX/0Ot;

    .line 123789
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 123790
    iput-object v0, p0, LX/0jY;->l:LX/0Ot;

    .line 123791
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 123792
    iput-object v0, p0, LX/0jY;->m:LX/0Ot;

    .line 123793
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0jY;->q:Z

    .line 123794
    const/4 v0, 0x5

    iput v0, p0, LX/0jY;->a:I

    .line 123795
    iput-object p4, p0, LX/0jY;->j:LX/1go;

    .line 123796
    iput-object p6, p0, LX/0jY;->c:LX/1gi;

    .line 123797
    iput-object p1, p0, LX/0jY;->d:LX/0pV;

    .line 123798
    iput-object p7, p0, LX/0jY;->e:LX/1gf;

    .line 123799
    iput-object p2, p0, LX/0jY;->f:LX/0SG;

    .line 123800
    iput-object p8, p0, LX/0jY;->g:Lcom/facebook/api/feedtype/FeedType;

    .line 123801
    iput-object p9, p0, LX/0jY;->h:LX/0rB;

    .line 123802
    iput-object p3, p0, LX/0jY;->i:LX/1gn;

    .line 123803
    return-void
.end method

.method private a(LX/1ud;)V
    .locals 3

    .prologue
    .line 123781
    const-string v0, "FeedDataBackgroundUIWorkHandler.doClearGapBetweenCursors"

    const v1, -0xa47f29c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 123782
    :try_start_0
    invoke-static {p0}, LX/0jY;->n(LX/0jY;)LX/1jW;

    move-result-object v0

    iget-object v1, p1, LX/1ud;->a:Ljava/lang/String;

    iget-object v2, p1, LX/1ud;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1jW;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123783
    const v0, 0x311a1ecb

    invoke-static {v0}, LX/02m;->a(I)V

    .line 123784
    return-void

    .line 123785
    :catchall_0
    move-exception v0

    const v1, 0x4c31e13f    # 4.663014E7f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private b(II)V
    .locals 13

    .prologue
    const/4 v3, 0x0

    .line 123747
    const-string v0, "FeedDataBackgroundUIWorkHandler.doInitializeStoriesForUI"

    const v1, -0x35cb1137

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 123748
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    move v4, v3

    move v6, v3

    move v1, v3

    .line 123749
    :goto_0
    add-int/lit8 v0, p1, 0x64

    if-ge v4, v0, :cond_4

    .line 123750
    :try_start_0
    invoke-static {p0}, LX/0jY;->n(LX/0jY;)LX/1jW;

    move-result-object v0

    iget-boolean v2, p0, LX/0jY;->q:Z

    invoke-virtual {v0, v2}, LX/1jW;->a(Z)Lcom/facebook/feed/model/ClientFeedUnitEdge;

    move-result-object v2

    .line 123751
    if-nez v2, :cond_1

    .line 123752
    invoke-static {p0}, LX/0jY;->n(LX/0jY;)LX/1jW;

    move-result-object v0

    .line 123753
    iget-object v2, v0, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    move v0, v2

    .line 123754
    if-eqz v0, :cond_0

    .line 123755
    iget-object v0, p0, LX/0jY;->d:LX/0pV;

    const-string v2, "FreshFeedBackgroundUIWorkHandler"

    sget-object v3, LX/1gs;->EMPTY_COLLECTION:LX/1gs;

    invoke-virtual {v0, v2, v3}, LX/0pV;->a(Ljava/lang/String;LX/1gt;)V

    move v4, v1

    .line 123756
    :goto_1
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 123757
    iget-object v0, p0, LX/0jY;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jU;

    invoke-virtual {v0, v1}, LX/0jU;->a(Ljava/util/List;)V

    .line 123758
    iget-object v0, p0, LX/0jY;->d:LX/0pV;

    const-string v1, "FreshFeedBackgroundUIWorkHandler"

    sget-object v2, LX/1gs;->STORIES_FOR_UI:LX/1gs;

    const-string v3, "retrieved"

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "nullStoriesCount"

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/0pV;->a(Ljava/lang/String;LX/1gt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123759
    invoke-static {p0}, LX/0jY;->m(LX/0jY;)Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;

    move-result-object v0

    .line 123760
    invoke-static {v0}, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->c(Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123761
    iget-object v0, p0, LX/0jY;->c:LX/1gi;

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-static {p0}, LX/0jY;->n(LX/0jY;)LX/1jW;

    move-result-object v2

    invoke-virtual {v2}, LX/1jW;->d()I

    move-result v2

    invoke-virtual {v0, v1, p2, v2}, LX/1gi;->a(LX/0Px;II)V

    .line 123762
    const v0, -0x13c920a7

    invoke-static {v0}, LX/02m;->a(I)V

    .line 123763
    return-void

    .line 123764
    :cond_0
    add-int/lit8 v0, v6, 0x1

    .line 123765
    :try_start_1
    const-string v2, "FreshFeedBackgroundUIWorkHandler"

    const-string v5, "Fresh Feed Story Collection return a null edge"

    invoke-static {v2, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123766
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v6, v0

    goto :goto_0

    .line 123767
    :cond_1
    :try_start_2
    iget-object v0, p0, LX/0jY;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qV;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v5

    sget-object v8, Lcom/facebook/api/feed/FeedFetchContext;->a:Lcom/facebook/api/feed/FeedFetchContext;

    invoke-virtual {v0, v5, v8}, LX/0qV;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/api/feed/FeedFetchContext;)V

    .line 123768
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-static {v0}, LX/1jw;->a(Ljava/lang/Object;)V

    .line 123769
    add-int/lit8 v1, v1, 0x1

    .line 123770
    invoke-virtual {v7, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_2
    .catch LX/4Bu; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    .line 123771
    :goto_3
    if-ge v0, p1, :cond_3

    move v1, v0

    move v0, v6

    goto :goto_2

    .line 123772
    :catch_0
    move-exception v0

    .line 123773
    :try_start_3
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v5

    invoke-interface {v5}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v5

    .line 123774
    const-string v8, "FreshFeedBackgroundUIWorkHandler"

    const-string v9, "Initialization of Flatbuffer failed.  Offset: %d, Length: %d, buffer Length: %d"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    .line 123775
    iget v12, v2, Lcom/facebook/feed/model/ClientFeedUnitEdge;->n:I

    move v12, v12

    .line 123776
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    .line 123777
    iget v12, v2, Lcom/facebook/feed/model/ClientFeedUnitEdge;->o:I

    move v2, v12

    .line 123778
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v10, v11

    const/4 v11, 0x2

    if-nez v5, :cond_2

    move v2, v3

    :goto_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v10, v11

    invoke-static {v8, v0, v9, v10}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    goto :goto_3

    :cond_2
    invoke-virtual {v5}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v2

    goto :goto_4

    .line 123779
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0jY;->c:LX/1gi;

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-static {p0}, LX/0jY;->n(LX/0jY;)LX/1jW;

    move-result-object v3

    invoke-virtual {v3}, LX/1jW;->d()I

    move-result v3

    invoke-virtual {v1, v2, p2, v3}, LX/1gi;->a(LX/0Px;II)V

    .line 123780
    const v1, 0x53990c3c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_3
    move v4, v0

    goto/16 :goto_1

    :cond_4
    move v4, v1

    goto/16 :goto_1
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 123742
    const-string v0, "FeedDataBackgroundUIWorkHandler.setGapAtCursor"

    const v1, 0x463c7888

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 123743
    :try_start_0
    invoke-static {p0}, LX/0jY;->n(LX/0jY;)LX/1jW;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1jW;->c(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123744
    const v0, 0x69178ec8

    invoke-static {v0}, LX/02m;->a(I)V

    .line 123745
    return-void

    .line 123746
    :catchall_0
    move-exception v0

    const v1, 0x753a3242

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private e(LX/0Px;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 123732
    const-string v0, "FreshFeedDataLoaderUIHandler.doProcessNewStories"

    const v1, 0x2be381b0

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 123733
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/0jY;->q:Z

    .line 123734
    invoke-static {p0}, LX/0jY;->n(LX/0jY;)LX/1jW;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/1jW;->b(LX/0Px;I)Z

    move-result v0

    .line 123735
    if-eqz v0, :cond_0

    .line 123736
    invoke-static {p0}, LX/0jY;->m(LX/0jY;)Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;

    move-result-object v0

    .line 123737
    invoke-static {v0}, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->c(Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;)V

    .line 123738
    iget-object v0, p0, LX/0jY;->c:LX/1gi;

    invoke-virtual {v0, p2, p1}, LX/1gi;->a(ILjava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123739
    :cond_0
    const v0, 0x3e72d073

    invoke-static {v0}, LX/02m;->a(I)V

    .line 123740
    return-void

    .line 123741
    :catchall_0
    move-exception v0

    const v1, 0x4b651b8f    # 1.5014799E7f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private f(LX/0Px;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 123727
    const-string v0, "FreshFeedDataLoaderUIHandler.doProcessOlderStories"

    const v1, -0x1517941d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 123728
    :try_start_0
    iget-object v0, p0, LX/0jY;->c:LX/1gi;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, LX/1gi;->a(LX/0Px;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123729
    const v0, 0x74dba30d

    invoke-static {v0}, LX/02m;->a(I)V

    .line 123730
    return-void

    .line 123731
    :catchall_0
    move-exception v0

    const v1, 0x2a581b57

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private g(LX/0Px;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 123718
    const-string v0, "FreshFeedDataLoaderUIHandler.doProcessDBStories"

    const v1, -0x5a9dcf27

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 123719
    :try_start_0
    invoke-static {p0}, LX/0jY;->n(LX/0jY;)LX/1jW;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/1jW;->a(LX/0Px;I)Z

    move-result v0

    .line 123720
    if-eqz v0, :cond_0

    .line 123721
    invoke-static {p0}, LX/0jY;->m(LX/0jY;)Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;

    move-result-object v0

    .line 123722
    invoke-static {v0}, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->c(Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;)V

    .line 123723
    iget-object v0, p0, LX/0jY;->c:LX/1gi;

    invoke-virtual {v0, p2, p1}, LX/1gi;->a(ILjava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123724
    :cond_0
    const v0, 0x3774c29e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 123725
    return-void

    .line 123726
    :catchall_0
    move-exception v0

    const v1, -0xd205b43

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 123710
    const-string v0, "FeedDataBackgroundUIWorkHandler.doProcessClearStories"

    const v1, 0x569f2e40

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 123711
    :try_start_0
    invoke-static {p0}, LX/0jY;->n(LX/0jY;)LX/1jW;

    move-result-object v0

    invoke-virtual {v0}, LX/1jW;->f()V

    .line 123712
    invoke-static {p0}, LX/0jY;->m(LX/0jY;)Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->b()V

    .line 123713
    iget-object v0, p0, LX/0jY;->c:LX/1gi;

    .line 123714
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, LX/1gi;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1gi;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123715
    const v0, -0x3a7128e9

    invoke-static {v0}, LX/02m;->a(I)V

    .line 123716
    return-void

    .line 123717
    :catchall_0
    move-exception v0

    const v1, 0x3c143c78    # 0.00904762f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private h(LX/0Px;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 123703
    const-string v0, "FreshFeedDataLoaderUIHandler.doProcessUnStageStories"

    const v1, 0x26cdb4bc

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 123704
    :try_start_0
    invoke-static {p0}, LX/0jY;->n(LX/0jY;)LX/1jW;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1jW;->a(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123705
    invoke-static {p0}, LX/0jY;->n(LX/0jY;)LX/1jW;

    move-result-object v1

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1jW;->c(Ljava/lang/String;)V

    .line 123706
    iget-object v0, p0, LX/0jY;->c:LX/1gi;

    invoke-virtual {v0, p2, p1}, LX/1gi;->a(ILjava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123707
    :cond_0
    const v0, -0x6ecbfe36    # -1.4200048E-28f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 123708
    return-void

    .line 123709
    :catchall_0
    move-exception v0

    const v1, -0x4d2f3698

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private i()V
    .locals 13

    .prologue
    .line 123805
    const-string v0, "FeedDataBackgroundUIWorkHandler.fetchFreshStoriesIfNeeded"

    const v1, 0x5dc813bd

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 123806
    :try_start_0
    invoke-static {p0}, LX/0jY;->n(LX/0jY;)LX/1jW;

    move-result-object v0

    invoke-virtual {v0}, LX/1jW;->a()I

    move-result v0

    .line 123807
    iget-object v5, p0, LX/0jY;->l:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1jj;

    const/4 v6, 0x5

    .line 123808
    iget-object v7, v5, LX/1jj;->a:LX/0uf;

    sget-wide v9, LX/0X5;->dv:J

    int-to-long v11, v6

    invoke-virtual {v7, v9, v10, v11, v12}, LX/0uf;->a(JJ)J

    move-result-wide v7

    long-to-int v7, v7

    move v5, v7

    .line 123809
    move v1, v5

    .line 123810
    if-ge v0, v1, :cond_0

    .line 123811
    iget-object v1, p0, LX/0jY;->d:LX/0pV;

    const-string v2, "FreshFeedBackgroundUIWorkHandler"

    sget-object v3, LX/1gs;->FETCH_FRESH_STORIES:LX/1gs;

    const-string v4, "freshStoryCount"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0pV;->a(Ljava/lang/String;LX/1gt;Ljava/lang/String;Ljava/lang/String;)V

    .line 123812
    invoke-static {p0}, LX/0jY;->k(LX/0jY;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123813
    :cond_0
    const v0, 0x4fc5a5db

    invoke-static {v0}, LX/02m;->a(I)V

    .line 123814
    return-void

    .line 123815
    :catchall_0
    move-exception v0

    const v1, 0x1ab14281

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static k(LX/0jY;)V
    .locals 13

    .prologue
    .line 123653
    const-string v0, "FeedDataBackgroundUIWorkHandler.loadMoreDataFromNetwork"

    const v1, 0x6c38e9e8

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 123654
    :try_start_0
    iget-object v0, p0, LX/0jY;->e:LX/1gf;

    invoke-virtual {v0}, LX/1gf;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 123655
    const v0, -0x782960b7

    invoke-static {v0}, LX/02m;->a(I)V

    .line 123656
    :goto_0
    return-void

    .line 123657
    :cond_0
    :try_start_1
    invoke-static {p0}, LX/0jY;->n(LX/0jY;)LX/1jW;

    move-result-object v0

    .line 123658
    iget-object v1, v0, LX/1jW;->h:LX/1jY;

    .line 123659
    iget-object v2, v1, LX/1jY;->c:LX/0qp;

    invoke-virtual {v2}, LX/0qp;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 123660
    const/4 v2, 0x0

    .line 123661
    :goto_1
    move-object v1, v2

    .line 123662
    move-object v8, v1

    .line 123663
    if-nez v8, :cond_1

    .line 123664
    iget-object v0, p0, LX/0jY;->d:LX/0pV;

    const-string v1, "FreshFeedBackgroundUIWorkHandler"

    sget-object v2, LX/1gs;->LOAD_MORE_DATA_FROM_NETWORK_ABORT:LX/1gs;

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;LX/1gt;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123665
    const v0, 0x6017e032

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 123666
    :cond_1
    :try_start_2
    new-instance v1, LX/1J0;

    sget-object v2, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    iget-object v3, p0, LX/0jY;->g:Lcom/facebook/api/feedtype/FeedType;

    iget-object v4, p0, LX/0jY;->h:LX/0rB;

    const-string v5, "fresh_feed_more_data_fetch"

    iget-object v0, p0, LX/0jY;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    invoke-static {p0}, LX/0jY;->n(LX/0jY;)LX/1jW;

    move-result-object v0

    .line 123667
    iget-object v9, v0, LX/1jW;->h:LX/1jY;

    const/4 v12, 0x0

    .line 123668
    iget-object v10, v9, LX/1jY;->c:LX/0qp;

    invoke-virtual {v10}, LX/0qp;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_5

    move-object v10, v12

    .line 123669
    :goto_2
    move-object v9, v10

    .line 123670
    move-object v9, v9

    .line 123671
    sget-object v10, LX/0gf;->SCROLLING:LX/0gf;

    invoke-direct/range {v1 .. v10}, LX/1J0;-><init>(LX/0rS;Lcom/facebook/api/feedtype/FeedType;LX/0rB;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;LX/0gf;)V

    .line 123672
    iget-object v0, p0, LX/0jY;->e:LX/1gf;

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 123673
    iget-object v2, v0, LX/1gf;->a:LX/1ge;

    invoke-virtual {v2}, LX/1ge;->d()Z

    move-result v2

    if-nez v2, :cond_8

    move v2, v3

    :goto_3
    const-string v5, "new_stories_load_status is %d but expected state was %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, v0, LX/1gf;->a:LX/1ge;

    .line 123674
    iget v8, v7, LX/1ge;->c:I

    move v7, v8

    .line 123675
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v3

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 123676
    iget-object v2, v0, LX/1gf;->b:LX/1ge;

    invoke-virtual {v2}, LX/1ge;->a()V

    .line 123677
    iget-object v0, p0, LX/0jY;->d:LX/0pV;

    const-string v2, "FreshFeedBackgroundUIWorkHandler"

    sget-object v3, LX/1gs;->LOAD_MORE_DATA_FROM_NETWORK:LX/1gs;

    invoke-virtual {v0, v2, v3}, LX/0pV;->a(Ljava/lang/String;LX/1gt;)V

    .line 123678
    iget-object v0, p0, LX/0jY;->n:LX/1gp;

    invoke-virtual {v0, v1}, LX/1gp;->b(LX/1J0;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 123679
    const v0, -0x6de9b345

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x4283c84b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 123680
    :cond_2
    :try_start_3
    const/4 v2, 0x0

    move v3, v2

    :goto_4
    iget-object v2, v1, LX/1jY;->c:LX/0qp;

    invoke-virtual {v2}, LX/0qp;->size()I

    move-result v2

    if-ge v3, v2, :cond_4

    .line 123681
    iget-object v2, v1, LX/1jY;->c:LX/0qp;

    .line 123682
    iget-object v0, v2, LX/0qp;->e:Ljava/util/List;

    move-object v2, v0

    .line 123683
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1jg;

    iget-boolean v2, v2, LX/1jg;->g:Z

    if-eqz v2, :cond_3

    .line 123684
    iget-object v2, v1, LX/1jY;->c:LX/0qp;

    .line 123685
    iget-object v0, v2, LX/0qp;->e:Ljava/util/List;

    move-object v2, v0

    .line 123686
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1jg;

    iget-object v2, v2, LX/1jg;->b:Ljava/lang/String;

    goto/16 :goto_1

    .line 123687
    :cond_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    .line 123688
    :cond_4
    iget-object v2, v1, LX/1jY;->c:LX/0qp;

    .line 123689
    iget-object v3, v2, LX/0qp;->e:Ljava/util/List;

    move-object v2, v3

    .line 123690
    iget-object v3, v1, LX/1jY;->c:LX/0qp;

    invoke-virtual {v3}, LX/0qp;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1jg;

    iget-object v2, v2, LX/1jg;->b:Ljava/lang/String;

    goto/16 :goto_1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 123691
    :cond_5
    :try_start_4
    const/4 v10, 0x0

    move v11, v10

    :goto_5
    iget-object v10, v9, LX/1jY;->c:LX/0qp;

    invoke-virtual {v10}, LX/0qp;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-ge v11, v10, :cond_7

    .line 123692
    iget-object v10, v9, LX/1jY;->c:LX/0qp;

    .line 123693
    iget-object v0, v10, LX/0qp;->e:Ljava/util/List;

    move-object v10, v0

    .line 123694
    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/1jg;

    iget-boolean v10, v10, LX/1jg;->g:Z

    if-eqz v10, :cond_6

    iget-object v10, v9, LX/1jY;->c:LX/0qp;

    .line 123695
    iget-object v0, v10, LX/0qp;->e:Ljava/util/List;

    move-object v10, v0

    .line 123696
    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/1jg;

    iget-object v10, v10, LX/1jg;->b:Ljava/lang/String;

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 123697
    iget-object v10, v9, LX/1jY;->c:LX/0qp;

    .line 123698
    iget-object v12, v10, LX/0qp;->e:Ljava/util/List;

    move-object v10, v12

    .line 123699
    add-int/lit8 v11, v11, 0x1

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/1jg;

    iget-object v10, v10, LX/1jg;->b:Ljava/lang/String;

    goto/16 :goto_2

    .line 123700
    :cond_6
    add-int/lit8 v10, v11, 0x1

    move v11, v10

    goto :goto_5

    :cond_7
    move-object v10, v12

    .line 123701
    goto/16 :goto_2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_8
    move v2, v4

    .line 123702
    goto/16 :goto_3
.end method

.method private l()V
    .locals 2

    .prologue
    .line 123645
    const-string v0, "FreshFeedDataLoaderUIHandler.doResetFreshFeedCollectionStoryPosition"

    const v1, 0x4cf76a31    # 1.29716616E8f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 123646
    :try_start_0
    invoke-static {p0}, LX/0jY;->n(LX/0jY;)LX/1jW;

    move-result-object v0

    .line 123647
    iget-object v1, v0, LX/1jW;->h:LX/1jY;

    .line 123648
    const/4 v0, 0x0

    iput v0, v1, LX/1jY;->a:I

    .line 123649
    invoke-static {p0}, LX/0jY;->m(LX/0jY;)Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123650
    const v0, 0x4f918ede

    invoke-static {v0}, LX/02m;->a(I)V

    .line 123651
    return-void

    .line 123652
    :catchall_0
    move-exception v0

    const v1, -0x4b94a74b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static m(LX/0jY;)Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;
    .locals 11

    .prologue
    .line 123639
    iget-object v0, p0, LX/0jY;->o:Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;

    if-nez v0, :cond_0

    .line 123640
    iget-object v0, p0, LX/0jY;->i:LX/1gn;

    invoke-static {p0}, LX/0jY;->n(LX/0jY;)LX/1jW;

    move-result-object v1

    .line 123641
    new-instance v2, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, LX/0Tf;

    invoke-static {v0}, LX/0kl;->b(LX/0QB;)LX/0Wd;

    move-result-object v4

    check-cast v4, LX/0Wd;

    invoke-static {v0}, LX/1ji;->a(LX/0QB;)LX/1ji;

    move-result-object v5

    check-cast v5, LX/1ji;

    invoke-static {v0}, LX/0pJ;->a(LX/0QB;)LX/0pJ;

    move-result-object v7

    check-cast v7, LX/0pJ;

    invoke-static {v0}, LX/1fA;->a(LX/0QB;)LX/1fA;

    move-result-object v8

    check-cast v8, LX/1fA;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    const/16 v6, 0x695

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    move-object v6, v1

    invoke-direct/range {v2 .. v10}, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;-><init>(LX/0Tf;LX/0Wd;LX/1ji;LX/1jW;LX/0pJ;LX/1fA;LX/0ad;LX/0Ot;)V

    .line 123642
    move-object v0, v2

    .line 123643
    iput-object v0, p0, LX/0jY;->o:Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;

    .line 123644
    :cond_0
    iget-object v0, p0, LX/0jY;->o:Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;

    return-object v0
.end method

.method public static n(LX/0jY;)LX/1jW;
    .locals 13

    .prologue
    .line 123625
    iget-object v0, p0, LX/0jY;->p:LX/1jW;

    if-nez v0, :cond_0

    .line 123626
    iget-object v0, p0, LX/0jY;->j:LX/1go;

    iget-object v1, p0, LX/0jY;->g:Lcom/facebook/api/feedtype/FeedType;

    .line 123627
    new-instance v2, LX/1jW;

    .line 123628
    new-instance v3, LX/1jX;

    invoke-direct {v3}, LX/1jX;-><init>()V

    .line 123629
    const/16 v4, 0x1c92

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x646

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    .line 123630
    iput-object v4, v3, LX/1jX;->a:LX/0Or;

    iput-object v5, v3, LX/1jX;->b:LX/0Or;

    .line 123631
    move-object v4, v3

    .line 123632
    check-cast v4, LX/1jX;

    invoke-static {v0}, LX/0y5;->a(LX/0QB;)LX/0y5;

    move-result-object v5

    check-cast v5, LX/0y5;

    .line 123633
    new-instance v9, LX/1jY;

    invoke-static {v0}, LX/0pV;->a(LX/0QB;)LX/0pV;

    move-result-object v3

    check-cast v3, LX/0pV;

    invoke-static {v0}, LX/0qj;->b(LX/0QB;)LX/0qj;

    move-result-object v6

    check-cast v6, LX/0qj;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {v0}, LX/1jZ;->a(LX/0QB;)LX/1jZ;

    move-result-object v8

    check-cast v8, LX/1jZ;

    invoke-direct {v9, v3, v6, v7, v8}, LX/1jY;-><init>(LX/0pV;LX/0qj;LX/0ad;LX/1jZ;)V

    .line 123634
    move-object v6, v9

    .line 123635
    check-cast v6, LX/1jY;

    const-class v3, LX/1jd;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/1jd;

    const/16 v3, 0x695

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0pV;->a(LX/0QB;)LX/0pV;

    move-result-object v9

    check-cast v9, LX/0pV;

    invoke-static {v0}, LX/0pJ;->a(LX/0QB;)LX/0pJ;

    move-result-object v10

    check-cast v10, LX/0pJ;

    invoke-static {v0}, LX/1jZ;->a(LX/0QB;)LX/1jZ;

    move-result-object v11

    check-cast v11, LX/1jZ;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    move-object v3, v1

    invoke-direct/range {v2 .. v12}, LX/1jW;-><init>(Lcom/facebook/api/feedtype/FeedType;LX/1jX;LX/0y5;LX/1jY;LX/1jd;LX/0Ot;LX/0pV;LX/0pJ;LX/1jZ;LX/0ad;)V

    .line 123636
    move-object v0, v2

    .line 123637
    iput-object v0, p0, LX/0jY;->p:LX/1jW;

    .line 123638
    :cond_0
    iget-object v0, p0, LX/0jY;->p:LX/1jW;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 123622
    invoke-static {p0}, LX/0jY;->m(LX/0jY;)Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->b()V

    .line 123623
    invoke-super {p0}, LX/0jZ;->a()V

    .line 123624
    return-void
.end method

.method public final a(LX/0Px;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 123620
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1, p1}, LX/0jY;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0jY;->sendMessage(Landroid/os/Message;)Z

    .line 123621
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 123618
    const/16 v0, 0xc

    invoke-virtual {p0, v0, p1}, LX/0jY;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0jY;->sendMessage(Landroid/os/Message;)Z

    .line 123619
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 123616
    const/16 v0, 0xb

    new-instance v1, LX/1ud;

    invoke-direct {v1, p1, p2}, LX/1ud;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, LX/0jY;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0jY;->sendMessage(Landroid/os/Message;)Z

    .line 123617
    return-void
.end method

.method public final b(LX/0Px;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 123597
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1, p1}, LX/0jY;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0jY;->sendMessage(Landroid/os/Message;)Z

    .line 123598
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 123614
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, LX/0jY;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0jY;->sendMessage(Landroid/os/Message;)Z

    .line 123615
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 123599
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 123600
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown what="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123601
    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v0, v1}, LX/0jY;->b(II)V

    .line 123602
    :goto_0
    return-void

    .line 123603
    :pswitch_2
    invoke-direct {p0}, LX/0jY;->i()V

    goto :goto_0

    .line 123604
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/0Px;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v0, v1}, LX/0jY;->e(LX/0Px;I)V

    goto :goto_0

    .line 123605
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/0Px;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v0, v1}, LX/0jY;->g(LX/0Px;I)V

    goto :goto_0

    .line 123606
    :pswitch_5
    invoke-direct {p0}, LX/0jY;->l()V

    goto :goto_0

    .line 123607
    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/0Px;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v0, v1}, LX/0jY;->h(LX/0Px;I)V

    goto :goto_0

    .line 123608
    :pswitch_7
    invoke-direct {p0}, LX/0jY;->h()V

    goto :goto_0

    .line 123609
    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/1ud;

    invoke-direct {p0, v0}, LX/0jY;->a(LX/1ud;)V

    goto :goto_0

    .line 123610
    :pswitch_9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, LX/0jY;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 123611
    :pswitch_a
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0jY;->q:Z

    .line 123612
    goto :goto_0

    .line 123613
    :pswitch_b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/0Px;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v0, v1}, LX/0jY;->f(LX/0Px;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method
