.class public LX/0pX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/0pX;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/hardware/BatteryStateManager;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/net/ConnectivityManager;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Zb;

.field private final d:LX/0pZ;

.field public final e:Ljava/util/concurrent/ExecutorService;

.field public f:Z

.field private g:Z

.field private h:I


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/0Zb;LX/0pZ;LX/0ad;)V
    .locals 5
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/hardware/BatteryStateManager;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/net/ConnectivityManager;",
            ">;",
            "LX/0Zb;",
            "LX/0pZ;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const v4, 0x7fffffff

    const/4 v3, 0x0

    .line 144858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144859
    iput-boolean v3, p0, LX/0pX;->f:Z

    .line 144860
    iput-boolean v3, p0, LX/0pX;->g:Z

    .line 144861
    iput v4, p0, LX/0pX;->h:I

    .line 144862
    iput-object p1, p0, LX/0pX;->e:Ljava/util/concurrent/ExecutorService;

    .line 144863
    iput-object p2, p0, LX/0pX;->a:LX/0Ot;

    .line 144864
    iput-object p3, p0, LX/0pX;->b:LX/0Ot;

    .line 144865
    iput-object p4, p0, LX/0pX;->c:LX/0Zb;

    .line 144866
    iput-object p5, p0, LX/0pX;->d:LX/0pZ;

    .line 144867
    sget-object v0, LX/0c0;->Live:LX/0c0;

    sget-object v1, LX/0c1;->Off:LX/0c1;

    sget-short v2, LX/0pc;->c:S

    invoke-interface {p6, v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0pX;->f:Z

    .line 144868
    sget-object v0, LX/0c0;->Live:LX/0c0;

    sget-object v1, LX/0c1;->Off:LX/0c1;

    sget-short v2, LX/0pc;->a:S

    invoke-interface {p6, v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0pX;->g:Z

    .line 144869
    sget-object v0, LX/0c0;->Live:LX/0c0;

    sget-object v1, LX/0c1;->Off:LX/0c1;

    sget v2, LX/0pc;->b:I

    invoke-interface {p6, v0, v1, v2, v4}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    iput v0, p0, LX/0pX;->h:I

    .line 144870
    return-void
.end method

.method public static a(LX/0QB;)LX/0pX;
    .locals 10

    .prologue
    .line 144725
    sget-object v0, LX/0pX;->i:LX/0pX;

    if-nez v0, :cond_1

    .line 144726
    const-class v1, LX/0pX;

    monitor-enter v1

    .line 144727
    :try_start_0
    sget-object v0, LX/0pX;->i:LX/0pX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 144728
    if-eqz v2, :cond_0

    .line 144729
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 144730
    new-instance v3, LX/0pX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    const/16 v5, 0x292

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x23

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v0}, LX/0pY;->a(LX/0QB;)LX/0pY;

    move-result-object v8

    check-cast v8, LX/0pZ;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-direct/range {v3 .. v9}, LX/0pX;-><init>(Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/0Zb;LX/0pZ;LX/0ad;)V

    .line 144731
    move-object v0, v3

    .line 144732
    sput-object v0, LX/0pX;->i:LX/0pX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144733
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 144734
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 144735
    :cond_1
    sget-object v0, LX/0pX;->i:LX/0pX;

    return-object v0

    .line 144736
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 144737
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Z
    .locals 1

    .prologue
    .line 144857
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/0pX;->a(Ljava/lang/String;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;Ljava/util/Map;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;Ljava/util/Map;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/api/feed/FetchFeedParams;",
            "Ljava/lang/Exception;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)Z"
        }
    .end annotation

    .prologue
    .line 144835
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "client_query_id"

    .line 144836
    iget-object v2, p2, Lcom/facebook/api/feed/FetchFeedParams;->g:Ljava/lang/String;

    move-object v2, v2

    .line 144837
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "native_newsfeed"

    .line 144838
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 144839
    move-object v0, v0

    .line 144840
    if-eqz p3, :cond_1

    .line 144841
    const-string v1, "exception_class"

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 144842
    const-string v1, "exception_message"

    invoke-virtual {p3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 144843
    iget-boolean v1, p0, LX/0pX;->g:Z

    if-eqz v1, :cond_1

    .line 144844
    const-string v1, "exception stack"

    iget v2, p0, LX/0pX;->h:I

    const/4 v3, 0x0

    .line 144845
    invoke-virtual {p3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v5

    .line 144846
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 144847
    array-length p1, v5

    move v4, v3

    :goto_0
    if-ge v3, p1, :cond_0

    aget-object p2, v5, v3

    .line 144848
    if-ge v4, v2, :cond_0

    .line 144849
    invoke-virtual {p2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144850
    const-string p2, "\n"

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144851
    add-int/lit8 v4, v4, 0x1

    .line 144852
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 144853
    :cond_0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 144854
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 144855
    :cond_1
    invoke-virtual {v0, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 144856
    invoke-static {p0, v0}, LX/0pX;->a$redex0(LX/0pX;Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v0

    return v0
.end method

.method public static a$redex0(LX/0pX;Lcom/facebook/analytics/logger/HoneyClientEvent;)Z
    .locals 3

    .prologue
    .line 144820
    iget-boolean v0, p0, LX/0pX;->f:Z

    move v0, v0

    .line 144821
    if-nez v0, :cond_0

    .line 144822
    const/4 v0, 0x0

    .line 144823
    :goto_0
    return v0

    .line 144824
    :cond_0
    iget-object v0, p0, LX/0pX;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0u7;

    invoke-virtual {v0}, LX/0u7;->a()F

    move-result v0

    .line 144825
    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_1

    .line 144826
    const-string v1, "battery_level"

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 144827
    :cond_1
    iget-object v0, p0, LX/0pX;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 144828
    if-eqz v0, :cond_2

    .line 144829
    const-string v1, "network"

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 144830
    const-string v1, "network_state"

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo$DetailedState;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 144831
    :goto_1
    move-object v0, p1

    .line 144832
    iget-object v1, p0, LX/0pX;->c:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 144833
    const/4 v0, 0x1

    goto :goto_0

    .line 144834
    :cond_2
    const-string v0, "network"

    const-string v1, "unknown"

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedParams;)Z
    .locals 3

    .prologue
    .line 144814
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "feed_e2e_chunk_skipped"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "client_query_id"

    .line 144815
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->g:Ljava/lang/String;

    move-object v2, v2

    .line 144816
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "native_newsfeed"

    .line 144817
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 144818
    move-object v0, v0

    .line 144819
    invoke-static {p0, v0}, LX/0pX;->a$redex0(LX/0pX;Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedParams;LX/00b;)Z
    .locals 11

    .prologue
    .line 144789
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->g:Ljava/lang/String;

    move-object v2, v0

    .line 144790
    const-wide/16 v0, 0x0

    .line 144791
    const-string v8, "_"

    invoke-virtual {v2, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 144792
    array-length v9, v8

    const/4 v10, 0x3

    if-ne v9, v10, :cond_1

    .line 144793
    const/4 v9, 0x1

    aget-object v8, v8, v9

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 144794
    :goto_0
    move-wide v4, v8

    .line 144795
    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 144796
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v6, 0x3e8

    div-long/2addr v0, v6

    .line 144797
    sub-long/2addr v0, v4

    .line 144798
    :cond_0
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "feed_e2e_udp_prime_request"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "client_query_id"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "exception_message"

    .line 144799
    sget-object v4, LX/03s;->a:[I

    invoke-virtual {p2}, LX/00b;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 144800
    const-string v4, "none"

    :goto_1
    move-object v4, v4

    .line 144801
    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "delta"

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "native_newsfeed"

    .line 144802
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 144803
    move-object v0, v0

    .line 144804
    invoke-static {p0, v0}, LX/0pX;->a$redex0(LX/0pX;Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v0

    return v0

    :cond_1
    const-wide/16 v8, -0x1

    goto :goto_0

    .line 144805
    :pswitch_0
    const-string v4, "frozen"

    goto :goto_1

    .line 144806
    :pswitch_1
    const-string v4, "error_template"

    goto :goto_1

    .line 144807
    :pswitch_2
    const-string v4, "error_encrypt_channel"

    goto :goto_1

    .line 144808
    :pswitch_3
    const-string v4, "error_encrypting"

    goto :goto_1

    .line 144809
    :pswitch_4
    const-string v4, "error_ip_address"

    goto :goto_1

    .line 144810
    :pswitch_5
    const-string v4, "error_network_io"

    goto :goto_1

    .line 144811
    :pswitch_6
    const-string v4, "error_size_limit"

    goto :goto_1

    .line 144812
    :pswitch_7
    const-string v4, "error_mismatch"

    goto :goto_1

    .line 144813
    :pswitch_8
    const-string v4, "sent"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Z
    .locals 1

    .prologue
    .line 144788
    const-string v0, "feed_e2e_connection_error"

    invoke-direct {p0, v0, p1, p2}, LX/0pX;->a(Ljava/lang/String;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 144768
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v0, v0

    .line 144769
    invoke-virtual {v0}, LX/0gf;->isManual()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "manual"

    .line 144770
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 144771
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    move-object v2, v2

    .line 144772
    invoke-virtual {v2}, LX/0rU;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 144773
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "feed_e2e_load_request"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "fetch_type"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "order_by"

    .line 144774
    iget-object v3, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v3, v3

    .line 144775
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "first_n"

    .line 144776
    iget v3, p1, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v3, v3

    .line 144777
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "after"

    .line 144778
    iget-object v3, p1, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v3, v3

    .line 144779
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "before"

    .line 144780
    iget-object v3, p1, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v3, v3

    .line 144781
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "refresh_mode"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "client_query_id"

    .line 144782
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->g:Ljava/lang/String;

    move-object v2, v2

    .line 144783
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "native_newsfeed"

    .line 144784
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 144785
    move-object v0, v0

    .line 144786
    invoke-static {p0, v0}, LX/0pX;->a$redex0(LX/0pX;Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v0

    return v0

    .line 144787
    :cond_0
    const-string v0, "automatic"

    goto :goto_0
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedParams;ZLjava/lang/Exception;)Z
    .locals 3

    .prologue
    .line 144765
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 144766
    const-string v1, "from_primed_result"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144767
    const-string v1, "feed_e2e_parsing_error"

    invoke-direct {p0, v1, p1, p3, v0}, LX/0pX;->a(Ljava/lang/String;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;Ljava/util/Map;)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedResult;)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 144751
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "feed_e2e_parsing_error"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "client_query_id"

    .line 144752
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v2, v2

    .line 144753
    iget-object v4, v2, Lcom/facebook/api/feed/FetchFeedParams;->g:Ljava/lang/String;

    move-object v2, v4

    .line 144754
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "native_newsfeed"

    .line 144755
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 144756
    move-object v0, v0

    .line 144757
    const-string v1, "from_primed_result"

    .line 144758
    iget-boolean v2, p1, Lcom/facebook/api/feed/FetchFeedResult;->c:Z

    move v2, v2

    .line 144759
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 144760
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_0

    .line 144761
    const-string v1, "null_edges"

    invoke-virtual {v0, v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 144762
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->c()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    if-nez v1, :cond_1

    .line 144763
    const-string v1, "null_page_info"

    invoke-virtual {v0, v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 144764
    :cond_1
    invoke-static {p0, v0}, LX/0pX;->a$redex0(LX/0pX;Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Z
    .locals 1

    .prologue
    .line 144750
    const-string v0, "feed_e2e_receive_error"

    invoke-direct {p0, v0, p1, p2}, LX/0pX;->a(Ljava/lang/String;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/api/feed/FetchFeedResult;)Z
    .locals 4

    .prologue
    .line 144739
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "feed_e2e_end_of_feed_error"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "client_query_id"

    .line 144740
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v2, v2

    .line 144741
    iget-object v3, v2, Lcom/facebook/api/feed/FetchFeedParams;->g:Ljava/lang/String;

    move-object v2, v3

    .line 144742
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "native_newsfeed"

    .line 144743
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 144744
    move-object v0, v0

    .line 144745
    const-string v1, "page_info"

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->c()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 144746
    const-string v1, "from_primed_result"

    .line 144747
    iget-boolean v2, p1, Lcom/facebook/api/feed/FetchFeedResult;->c:Z

    move v2, v2

    .line 144748
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 144749
    invoke-static {p0, v0}, LX/0pX;->a$redex0(LX/0pX;Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v0

    return v0
.end method

.method public final c(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Z
    .locals 1

    .prologue
    .line 144738
    const-string v0, "feed_e2e_other_error"

    invoke-direct {p0, v0, p1, p2}, LX/0pX;->a(Ljava/lang/String;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method
