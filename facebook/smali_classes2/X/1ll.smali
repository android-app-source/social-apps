.class public LX/1ll;
.super LX/1lm;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private a:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private volatile b:Landroid/graphics/Bitmap;

.field private final c:LX/1lk;

.field public final d:I


# direct methods
.method public constructor <init>(LX/1FJ;LX/1lk;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/facebook/imagepipeline/image/QualityInfo;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 312376
    invoke-direct {p0}, LX/1lm;-><init>()V

    .line 312377
    invoke-virtual {p1}, LX/1FJ;->c()LX/1FJ;

    move-result-object v0

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    iput-object v0, p0, LX/1ll;->a:LX/1FJ;

    .line 312378
    iget-object v0, p0, LX/1ll;->a:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, LX/1ll;->b:Landroid/graphics/Bitmap;

    .line 312379
    iput-object p2, p0, LX/1ll;->c:LX/1lk;

    .line 312380
    iput p3, p0, LX/1ll;->d:I

    .line 312381
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;LX/1FN;LX/1lk;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "LX/1FN",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/facebook/imagepipeline/image/QualityInfo;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 312370
    invoke-direct {p0}, LX/1lm;-><init>()V

    .line 312371
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, LX/1ll;->b:Landroid/graphics/Bitmap;

    .line 312372
    iget-object v1, p0, LX/1ll;->b:Landroid/graphics/Bitmap;

    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FN;

    invoke-static {v1, v0}, LX/1FJ;->a(Ljava/lang/Object;LX/1FN;)LX/1FJ;

    move-result-object v0

    iput-object v0, p0, LX/1ll;->a:LX/1FJ;

    .line 312373
    iput-object p3, p0, LX/1ll;->c:LX/1lk;

    .line 312374
    iput p4, p0, LX/1ll;->d:I

    .line 312375
    return-void
.end method

.method private static declared-synchronized j(LX/1ll;)LX/1FJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312365
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1ll;->a:LX/1FJ;

    .line 312366
    const/4 v1, 0x0

    iput-object v1, p0, LX/1ll;->a:LX/1FJ;

    .line 312367
    const/4 v1, 0x0

    iput-object v1, p0, LX/1ll;->b:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312368
    monitor-exit p0

    return-object v0

    .line 312369
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 312364
    iget-object v0, p0, LX/1ll;->b:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 312363
    iget-object v0, p0, LX/1ll;->b:Landroid/graphics/Bitmap;

    invoke-static {v0}, LX/1le;->a(Landroid/graphics/Bitmap;)I

    move-result v0

    return v0
.end method

.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 312382
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1ll;->a:LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 312359
    invoke-static {p0}, LX/1ll;->j(LX/1ll;)LX/1FJ;

    move-result-object v0

    .line 312360
    if-eqz v0, :cond_0

    .line 312361
    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 312362
    :cond_0
    return-void
.end method

.method public final d()LX/1lk;
    .locals 1

    .prologue
    .line 312358
    iget-object v0, p0, LX/1ll;->c:LX/1lk;

    return-object v0
.end method

.method public final declared-synchronized f()LX/1FJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312355
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1ll;->a:LX/1FJ;

    const-string v1, "Cannot convert a closed static bitmap"

    invoke-static {v0, v1}, LX/03g;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312356
    invoke-static {p0}, LX/1ll;->j(LX/1ll;)LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 312357
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 312353
    iget-object v0, p0, LX/1ll;->b:Landroid/graphics/Bitmap;

    .line 312354
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 312351
    iget-object v0, p0, LX/1ll;->b:Landroid/graphics/Bitmap;

    .line 312352
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    goto :goto_0
.end method
