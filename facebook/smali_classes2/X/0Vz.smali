.class public LX/0Vz;
.super Landroid/util/LongSparseArray;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LongSparseArray",
        "<",
        "Landroid/graphics/drawable/Drawable$ConstantState;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Landroid/graphics/drawable/Drawable$ConstantState;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/content/res/Resources;

.field private e:LX/0Vy;


# direct methods
.method public constructor <init>(Landroid/util/LongSparseArray;LX/0Vy;Landroid/content/res/Resources;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Landroid/graphics/drawable/Drawable$ConstantState;",
            ">;",
            "LX/0Vy;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 75319
    invoke-direct {p0}, Landroid/util/LongSparseArray;-><init>()V

    .line 75320
    iput-object p2, p0, LX/0Vz;->e:LX/0Vy;

    .line 75321
    iput-object p3, p0, LX/0Vz;->d:Landroid/content/res/Resources;

    .line 75322
    iput-object p1, p0, LX/0Vz;->c:Landroid/util/LongSparseArray;

    .line 75323
    invoke-virtual {p1}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    .line 75324
    invoke-virtual {p1, v0}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    .line 75325
    invoke-virtual {p1, v4, v5}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v4, v5, v3}, LX/0Vz;->put(JLjava/lang/Object;)V

    .line 75326
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 75327
    :cond_0
    iget-object v0, p0, LX/0Vz;->e:LX/0Vy;

    .line 75328
    iget-object v2, v0, LX/0Vy;->f:[I

    move-object v0, v2

    .line 75329
    invoke-direct {p0, v0, v1}, LX/0Vz;->a([II)V

    .line 75330
    iget-object v0, p0, LX/0Vz;->e:LX/0Vy;

    .line 75331
    iget-object v1, v0, LX/0Vy;->g:[I

    move-object v0, v1

    .line 75332
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LX/0Vz;->a([II)V

    .line 75333
    return-void
.end method

.method private a([II)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 75306
    if-eqz p1, :cond_1

    .line 75307
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 75308
    new-instance v2, Landroid/util/LongSparseArray;

    array-length v0, p1

    invoke-direct {v2, v0}, Landroid/util/LongSparseArray;-><init>(I)V

    .line 75309
    array-length v3, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, p1, v0

    .line 75310
    iget-object v5, p0, LX/0Vz;->d:Landroid/content/res/Resources;

    invoke-virtual {v5, v4, v1, v10}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 75311
    iget v5, v1, Landroid/util/TypedValue;->assetCookie:I

    int-to-long v6, v5

    const/16 v5, 0x20

    shl-long/2addr v6, v5

    iget v5, v1, Landroid/util/TypedValue;->data:I

    int-to-long v8, v5

    or-long/2addr v6, v8

    .line 75312
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v6, v7, v4}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 75313
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 75314
    :cond_0
    if-nez p2, :cond_2

    .line 75315
    iput-object v2, p0, LX/0Vz;->b:Landroid/util/LongSparseArray;

    .line 75316
    :cond_1
    :goto_1
    return-void

    .line 75317
    :cond_2
    if-ne p2, v10, :cond_1

    .line 75318
    iput-object v2, p0, LX/0Vz;->a:Landroid/util/LongSparseArray;

    goto :goto_1
.end method


# virtual methods
.method public final get(J)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 75290
    const/4 v1, 0x0

    .line 75291
    iget-object v0, p0, LX/0Vz;->b:Landroid/util/LongSparseArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Vz;->b:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 75292
    :goto_0
    if-nez v0, :cond_3

    .line 75293
    iget-object v0, p0, LX/0Vz;->a:Landroid/util/LongSparseArray;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0Vz;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 75294
    :goto_1
    if-eqz v0, :cond_2

    .line 75295
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, LX/0Vz;->d:Landroid/content/res/Resources;

    invoke-static {v0, v1}, LX/0Vy;->b(ILandroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 75296
    if-eqz v0, :cond_2

    .line 75297
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    .line 75298
    :goto_2
    return-object v0

    :cond_0
    move-object v0, v1

    .line 75299
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 75300
    goto :goto_1

    .line 75301
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable$ConstantState;

    goto :goto_2

    .line 75302
    :cond_3
    iget-object v1, p0, LX/0Vz;->e:LX/0Vy;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, LX/0Vz;->d:Landroid/content/res/Resources;

    invoke-virtual {v1, v2, v3}, LX/0Vy;->a(ILandroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 75303
    if-eqz v1, :cond_4

    .line 75304
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    goto :goto_2

    .line 75305
    :cond_4
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to inflate custom drawable with id: 0x"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
