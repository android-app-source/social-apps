.class public LX/0cF;
.super LX/0cD;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0cG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0cG",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 3

    .prologue
    .line 87900
    sget-object v0, LX/0cB;->g:Landroid/net/Uri;

    invoke-direct {p0, v0, p1}, LX/0cD;-><init>(Landroid/net/Uri;I)V

    .line 87901
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0cF;->b:Ljava/util/Map;

    .line 87902
    new-instance v0, LX/0cG;

    invoke-direct {v0}, LX/0cG;-><init>()V

    iput-object v0, p0, LX/0cF;->c:LX/0cG;

    .line 87903
    iget-object v0, p0, LX/0cF;->c:LX/0cG;

    const-string v1, "peer://msg_notification_unread_count/clear_thread/{thread_id}"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 87904
    iget-object v0, p0, LX/0cF;->c:LX/0cG;

    const-string v1, "peer://msg_notification_unread_count/thread/{thread_id}"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 87905
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 87906
    iget-object v0, p0, LX/0cF;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 87907
    return-void
.end method

.method public final a(Landroid/net/Uri;LX/4gt;)V
    .locals 3

    .prologue
    .line 87908
    :try_start_0
    iget-object v0, p0, LX/0cF;->c:LX/0cG;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0cG;->a(Ljava/lang/String;)LX/47X;

    move-result-object v1

    .line 87909
    if-eqz v1, :cond_1

    .line 87910
    const/4 v2, 0x1

    iget-object v0, v1, LX/47X;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v2, v0, :cond_1

    .line 87911
    iget-object v0, v1, LX/47X;->b:Landroid/os/Bundle;

    const-string v1, "thread_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 87912
    iget-object v1, p0, LX/0cF;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 87913
    if-eqz v0, :cond_1

    .line 87914
    iget-object v1, p2, LX/4gt;->a:Ljava/lang/Object;

    if-nez v1, :cond_0

    .line 87915
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    iput-object v1, p2, LX/4gt;->a:Ljava/lang/Object;

    .line 87916
    :cond_0
    iget-object v1, p2, LX/4gt;->a:Ljava/lang/Object;

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch LX/47T; {:try_start_0 .. :try_end_0} :catch_0

    .line 87917
    :cond_1
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 87918
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 87919
    iget-object v0, p0, LX/0cF;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 87920
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 87921
    :cond_0
    iget-object v0, p0, LX/0cD;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 87922
    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 87923
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/Object;)Z
    .locals 5
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 87924
    sget-object v0, LX/0cB;->h:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 87925
    iget-object v0, p0, LX/0cF;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87926
    :cond_0
    :goto_0
    return v2

    .line 87927
    :cond_1
    iget-object v0, p0, LX/0cF;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    move v2, v1

    .line 87928
    goto :goto_0

    .line 87929
    :cond_2
    :try_start_0
    iget-object v0, p0, LX/0cF;->c:LX/0cG;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0cG;->a(Ljava/lang/String;)LX/47X;

    move-result-object v3

    .line 87930
    if-eqz v3, :cond_0

    .line 87931
    iget-object v0, v3, LX/47X;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 87932
    :pswitch_0
    iget-object v0, v3, LX/47X;->b:Landroid/os/Bundle;

    const-string v3, "thread_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 87933
    iget-object v3, p0, LX/0cF;->b:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    move v2, v0

    .line 87934
    goto :goto_0

    :cond_3
    move v0, v2

    .line 87935
    goto :goto_1

    .line 87936
    :pswitch_1
    iget-object v0, v3, LX/47X;->b:Landroid/os/Bundle;

    const-string v1, "thread_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 87937
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 87938
    iget-object v0, p0, LX/0cF;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 87939
    if-nez v0, :cond_4

    .line 87940
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    .line 87941
    iget-object v4, p0, LX/0cF;->b:Ljava/util/Map;

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87942
    :cond_4
    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch LX/47T; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    :catch_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 87943
    iget-object v0, p0, LX/0cF;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 87944
    iget-object v0, p0, LX/0cD;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 87945
    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 87946
    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 87947
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 87948
    iget-object v4, p0, LX/0cF;->b:Ljava/util/Map;

    invoke-static {v3}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v3

    invoke-interface {v4, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 87949
    :cond_0
    return-void
.end method
