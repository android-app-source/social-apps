.class public final LX/0UM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0Up;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0Up;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 64489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64490
    iput-object p1, p0, LX/0UM;->a:LX/0QB;

    .line 64491
    return-void
.end method

.method public static a(LX/0QB;)LX/0Ot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Up;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 64492
    new-instance v0, LX/0UM;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0UM;-><init>(LX/0QB;)V

    move-object v0, v0

    .line 64493
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 64494
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0UM;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 64495
    packed-switch p2, :pswitch_data_0

    .line 64496
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64497
    :pswitch_0
    new-instance v1, LX/0Ys;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 64498
    new-instance p0, LX/0Yt;

    invoke-interface {p1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p2

    invoke-direct {p0, p2}, LX/0Yt;-><init>(LX/0QB;)V

    move-object p0, p0

    .line 64499
    invoke-interface {p1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p2

    invoke-static {p0, p2}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object p0

    move-object p0, p0

    .line 64500
    invoke-direct {v1, v0, p0}, LX/0Ys;-><init>(LX/03V;LX/0Ot;)V

    .line 64501
    move-object v0, v1

    .line 64502
    :goto_0
    return-object v0

    .line 64503
    :pswitch_1
    new-instance p0, LX/0Zs;

    invoke-static {p1}, LX/0Zt;->a(LX/0QB;)LX/0Zt;

    move-result-object v0

    check-cast v0, LX/0Zt;

    invoke-static {p1}, LX/0Zu;->b(LX/0QB;)LX/0Zu;

    move-result-object v1

    check-cast v1, LX/0Zv;

    invoke-direct {p0, v0, v1}, LX/0Zs;-><init>(LX/0Zt;LX/0Zv;)V

    .line 64504
    move-object v0, p0

    .line 64505
    goto :goto_0

    .line 64506
    :pswitch_2
    invoke-static {p1}, LX/0ay;->a(LX/0QB;)LX/0ay;

    move-result-object v0

    goto :goto_0

    .line 64507
    :pswitch_3
    invoke-static {p1}, LX/0bg;->a(LX/0QB;)LX/0bg;

    move-result-object v0

    goto :goto_0

    .line 64508
    :pswitch_4
    invoke-static {p1}, LX/0bw;->a(LX/0QB;)LX/0bw;

    move-result-object v0

    goto :goto_0

    .line 64509
    :pswitch_5
    invoke-static {p1}, LX/0bx;->a(LX/0QB;)LX/0bx;

    move-result-object v0

    goto :goto_0

    .line 64510
    :pswitch_6
    new-instance v0, LX/0c3;

    invoke-direct {v0}, LX/0c3;-><init>()V

    .line 64511
    const/16 v1, 0xc9b

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 64512
    iput-object v1, v0, LX/0c3;->a:LX/0Ot;

    .line 64513
    move-object v0, v0

    .line 64514
    goto :goto_0

    .line 64515
    :pswitch_7
    invoke-static {p1}, LX/0c4;->a(LX/0QB;)LX/0c4;

    move-result-object v0

    goto :goto_0

    .line 64516
    :pswitch_8
    invoke-static {p1}, LX/0cW;->a(LX/0QB;)LX/0cW;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 64517
    const/16 v0, 0x9

    return v0
.end method
