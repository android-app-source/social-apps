.class public LX/18W;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final C:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile D:LX/18W;

.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final A:LX/0SG;

.field public final B:LX/11S;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0s9;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0s9;

.field public final d:LX/0s9;

.field public final e:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0lp;

.field public final o:LX/0lC;

.field public final p:LX/11M;

.field public final q:LX/11N;

.field public final r:Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

.field public final s:LX/18X;

.field public final t:LX/0rz;

.field private final u:LX/03V;

.field public final v:LX/11Q;

.field private final w:LX/18b;

.field public final x:LX/0Zh;

.field public final y:LX/00I;

.field public final z:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 206591
    const-class v0, LX/18W;

    sput-object v0, LX/18W;->a:Ljava/lang/Class;

    .line 206592
    sget-object v0, LX/11I;->BATCH_COMPONENT_FETCH_CONFIGURATION:LX/11I;

    iget-object v0, v0, LX/11I;->requestNameString:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/18W;->C:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0s9;LX/0s9;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;Lcom/facebook/http/protocol/SingleMethodRunnerImpl;LX/0lp;LX/0lC;LX/11M;LX/11N;LX/18X;LX/0rz;LX/03V;LX/11Q;LX/18b;LX/0Zh;LX/00I;LX/0Or;LX/0SG;LX/11S;)V
    .locals 1
    .param p2    # LX/0s9;
        .annotation runtime Lcom/facebook/http/annotations/ProductionPlatformAppHttpConfig;
        .end annotation
    .end param
    .param p3    # LX/0s9;
        .annotation runtime Lcom/facebook/http/annotations/BootstrapPlatformAppHttpConfig;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/IsPhpProfilingEnabled;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/IsTeakProfilingEnabled;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/IsWirehogProfilingEnabled;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/IsArtilleryTracingEnabled;
        .end annotation
    .end param
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/config/server/IsRedirectToSandboxEnabled;
        .end annotation
    .end param
    .param p12    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/ShouldIncludeDateHeader;
        .end annotation
    .end param
    .param p23    # LX/0Zh;
        .annotation runtime Lcom/facebook/http/annotations/ParamsCollectionPoolForFbHttpModule;
        .end annotation
    .end param
    .param p25    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/ShouldEnableJsonParserSkipChildrenDepthFix;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0s9;",
            ">;",
            "LX/0s9;",
            "LX/0s9;",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/http/protocol/SingleMethodRunnerImpl;",
            "LX/0lp;",
            "LX/0lC;",
            "LX/11M;",
            "LX/11N;",
            "LX/18X;",
            "LX/0rz;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/11Q;",
            "LX/18b;",
            "LX/0Zh;",
            "LX/00I;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0SG;",
            "LX/11S;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 206593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206594
    iput-object p1, p0, LX/18W;->b:LX/0Or;

    .line 206595
    iput-object p2, p0, LX/18W;->c:LX/0s9;

    .line 206596
    iput-object p3, p0, LX/18W;->d:LX/0s9;

    .line 206597
    iput-object p4, p0, LX/18W;->e:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 206598
    iput-object p5, p0, LX/18W;->f:LX/0Or;

    .line 206599
    iput-object p6, p0, LX/18W;->g:LX/0Or;

    .line 206600
    iput-object p7, p0, LX/18W;->h:LX/0Or;

    .line 206601
    iput-object p8, p0, LX/18W;->i:LX/0Or;

    .line 206602
    iput-object p9, p0, LX/18W;->j:LX/0Or;

    .line 206603
    iput-object p10, p0, LX/18W;->k:LX/0Or;

    .line 206604
    iput-object p11, p0, LX/18W;->l:LX/0Or;

    .line 206605
    iput-object p12, p0, LX/18W;->m:LX/0Or;

    .line 206606
    iput-object p14, p0, LX/18W;->n:LX/0lp;

    .line 206607
    move-object/from16 v0, p15

    iput-object v0, p0, LX/18W;->o:LX/0lC;

    .line 206608
    move-object/from16 v0, p16

    iput-object v0, p0, LX/18W;->p:LX/11M;

    .line 206609
    move-object/from16 v0, p17

    iput-object v0, p0, LX/18W;->q:LX/11N;

    .line 206610
    iput-object p13, p0, LX/18W;->r:Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    .line 206611
    move-object/from16 v0, p18

    iput-object v0, p0, LX/18W;->s:LX/18X;

    .line 206612
    move-object/from16 v0, p19

    iput-object v0, p0, LX/18W;->t:LX/0rz;

    .line 206613
    move-object/from16 v0, p20

    iput-object v0, p0, LX/18W;->u:LX/03V;

    .line 206614
    move-object/from16 v0, p21

    iput-object v0, p0, LX/18W;->v:LX/11Q;

    .line 206615
    move-object/from16 v0, p22

    iput-object v0, p0, LX/18W;->w:LX/18b;

    .line 206616
    move-object/from16 v0, p23

    iput-object v0, p0, LX/18W;->x:LX/0Zh;

    .line 206617
    move-object/from16 v0, p24

    iput-object v0, p0, LX/18W;->y:LX/00I;

    .line 206618
    move-object/from16 v0, p25

    iput-object v0, p0, LX/18W;->z:LX/0Or;

    .line 206619
    move-object/from16 v0, p26

    iput-object v0, p0, LX/18W;->A:LX/0SG;

    .line 206620
    move-object/from16 v0, p27

    iput-object v0, p0, LX/18W;->B:LX/11S;

    .line 206621
    return-void
.end method

.method public static a(LX/0QB;)LX/18W;
    .locals 3

    .prologue
    .line 206622
    sget-object v0, LX/18W;->D:LX/18W;

    if-nez v0, :cond_1

    .line 206623
    const-class v1, LX/18W;

    monitor-enter v1

    .line 206624
    :try_start_0
    sget-object v0, LX/18W;->D:LX/18W;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 206625
    if-eqz v2, :cond_0

    .line 206626
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/18W;->b(LX/0QB;)LX/18W;

    move-result-object v0

    sput-object v0, LX/18W;->D:LX/18W;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 206627
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 206628
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 206629
    :cond_1
    sget-object v0, LX/18W;->D:LX/18W;

    return-object v0

    .line 206630
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 206631
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/18W;
    .locals 30

    .prologue
    .line 206632
    new-instance v2, LX/18W;

    const/16 v3, 0xb5a

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static/range {p0 .. p0}, LX/11J;->a(LX/0QB;)LX/0s9;

    move-result-object v4

    check-cast v4, LX/0s9;

    invoke-static/range {p0 .. p0}, LX/11L;->a(LX/0QB;)LX/0s9;

    move-result-object v5

    check-cast v5, LX/0s9;

    invoke-static/range {p0 .. p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v6

    check-cast v6, Lcom/facebook/http/common/FbHttpRequestProcessor;

    const/16 v7, 0x15e7

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x19e

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x14c3

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x14c5

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x14c6

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0x14c2

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v13, 0x1473

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    const/16 v14, 0x14c9

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    invoke-static/range {p0 .. p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v15

    check-cast v15, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    invoke-static/range {p0 .. p0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v16

    check-cast v16, LX/0lp;

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v17

    check-cast v17, LX/0lC;

    invoke-static/range {p0 .. p0}, LX/11M;->a(LX/0QB;)LX/11M;

    move-result-object v18

    check-cast v18, LX/11M;

    invoke-static/range {p0 .. p0}, LX/11N;->a(LX/0QB;)LX/11N;

    move-result-object v19

    check-cast v19, LX/11N;

    invoke-static/range {p0 .. p0}, LX/18X;->a(LX/0QB;)LX/18X;

    move-result-object v20

    check-cast v20, LX/18X;

    invoke-static/range {p0 .. p0}, LX/0rz;->a(LX/0QB;)LX/0rz;

    move-result-object v21

    check-cast v21, LX/0rz;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v22

    check-cast v22, LX/03V;

    invoke-static/range {p0 .. p0}, LX/11Q;->a(LX/0QB;)LX/11Q;

    move-result-object v23

    check-cast v23, LX/11Q;

    invoke-static/range {p0 .. p0}, LX/18b;->a(LX/0QB;)LX/18b;

    move-result-object v24

    check-cast v24, LX/18b;

    invoke-static/range {p0 .. p0}, LX/11O;->a(LX/0QB;)LX/0Zh;

    move-result-object v25

    check-cast v25, LX/0Zh;

    const-class v26, LX/00I;

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, LX/00I;

    const/16 v27, 0x14c7

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v27

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v28

    check-cast v28, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v29

    check-cast v29, LX/11S;

    invoke-direct/range {v2 .. v29}, LX/18W;-><init>(LX/0Or;LX/0s9;LX/0s9;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;Lcom/facebook/http/protocol/SingleMethodRunnerImpl;LX/0lp;LX/0lC;LX/11M;LX/11N;LX/18X;LX/0rz;LX/03V;LX/11Q;LX/18b;LX/0Zh;LX/00I;LX/0Or;LX/0SG;LX/11S;)V

    .line 206633
    return-object v2
.end method


# virtual methods
.method public final a()LX/2VK;
    .locals 2

    .prologue
    .line 206634
    new-instance v0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;

    invoke-direct {v0, p0}, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;-><init>(LX/18W;)V

    return-object v0
.end method
