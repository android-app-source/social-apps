.class public LX/0yx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/0yx;


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field public final b:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/facebook/feed/data/freshfeed/uih/UIHEventListener;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public d:Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;

.field public e:LX/0z3;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 166482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166483
    iput-object p1, p0, LX/0yx;->a:Ljava/util/concurrent/ExecutorService;

    .line 166484
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/0yx;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 166485
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/0yx;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 166486
    return-void
.end method

.method public static a(LX/0QB;)LX/0yx;
    .locals 4

    .prologue
    .line 166487
    sget-object v0, LX/0yx;->f:LX/0yx;

    if-nez v0, :cond_1

    .line 166488
    const-class v1, LX/0yx;

    monitor-enter v1

    .line 166489
    :try_start_0
    sget-object v0, LX/0yx;->f:LX/0yx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 166490
    if-eqz v2, :cond_0

    .line 166491
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 166492
    new-instance p0, LX/0yx;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v3}, LX/0yx;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 166493
    move-object v0, p0

    .line 166494
    sput-object v0, LX/0yx;->f:LX/0yx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 166495
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 166496
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 166497
    :cond_1
    sget-object v0, LX/0yx;->f:LX/0yx;

    return-object v0

    .line 166498
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 166499
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0yx;LX/6VO;)V
    .locals 3

    .prologue
    .line 166500
    iget-object v0, p0, LX/0yx;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/feed/data/freshfeed/uih/UIHEventDispatcher$DispatchTask;

    invoke-direct {v1, p0, p1}, Lcom/facebook/feed/data/freshfeed/uih/UIHEventDispatcher$DispatchTask;-><init>(LX/0yx;LX/6VO;)V

    const v2, 0x6612fc85

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 166501
    return-void
.end method

.method public static b(LX/0yx;)Z
    .locals 1

    .prologue
    .line 166502
    iget-object v0, p0, LX/0yx;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method
