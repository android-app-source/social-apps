.class public final enum LX/1Ga;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1Ga;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1Ga;

.field public static final enum CACHE:LX/1Ga;

.field public static final enum FILES:LX/1Ga;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 225768
    new-instance v0, LX/1Ga;

    const-string v1, "CACHE"

    invoke-direct {v0, v1, v2}, LX/1Ga;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Ga;->CACHE:LX/1Ga;

    new-instance v0, LX/1Ga;

    const-string v1, "FILES"

    invoke-direct {v0, v1, v3}, LX/1Ga;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Ga;->FILES:LX/1Ga;

    .line 225769
    const/4 v0, 0x2

    new-array v0, v0, [LX/1Ga;

    sget-object v1, LX/1Ga;->CACHE:LX/1Ga;

    aput-object v1, v0, v2

    sget-object v1, LX/1Ga;->FILES:LX/1Ga;

    aput-object v1, v0, v3

    sput-object v0, LX/1Ga;->$VALUES:[LX/1Ga;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 225770
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1Ga;
    .locals 1

    .prologue
    .line 225771
    const-class v0, LX/1Ga;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1Ga;

    return-object v0
.end method

.method public static values()[LX/1Ga;
    .locals 1

    .prologue
    .line 225772
    sget-object v0, LX/1Ga;->$VALUES:[LX/1Ga;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1Ga;

    return-object v0
.end method
