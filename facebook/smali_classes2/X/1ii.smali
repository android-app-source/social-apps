.class public final LX/1ii;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/1ih;


# direct methods
.method public constructor <init>(LX/1ih;)V
    .locals 0

    .prologue
    .line 298873
    iput-object p1, p0, LX/1ii;->a:LX/1ih;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0xa3f54b6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 298874
    iget-object v1, p0, LX/1ii;->a:LX/1ih;

    const/4 p3, 0x1

    .line 298875
    iget-object v2, v1, LX/1ih;->f:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v2, v2

    .line 298876
    if-eqz v2, :cond_0

    const-string v2, "is_blocking"

    invoke-static {v1, v2}, LX/1ih;->b(LX/1ih;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 298877
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 298878
    const-string p0, "arg_is_checkpoint"

    invoke-virtual {v2, p0, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 298879
    const-string p0, "arg_is_blocking_checkpoint"

    invoke-virtual {v2, p0, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 298880
    iget-object p0, v1, LX/1ih;->h:LX/0Xl;

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    const-string p2, "com.facebook.checkpoint.USER_IN_CHECKPOINT"

    invoke-virtual {p1, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v2

    invoke-interface {p0, v2}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 298881
    const-string v2, "is_blocking"

    iput-object v2, v1, LX/1ih;->b:Ljava/lang/String;

    .line 298882
    iput-boolean p3, v1, LX/1ih;->d:Z

    .line 298883
    :cond_0
    const/16 v1, 0x27

    const v2, 0x2a718a71

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
