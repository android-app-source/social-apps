.class public LX/1h6;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/1h6;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 295552
    const-string v0, "most_recent_hosts_part"

    const/4 v1, 0x1

    new-instance v2, LX/1h7;

    invoke-direct {v2}, LX/1h7;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 295553
    return-void
.end method

.method public static a(LX/0QB;)LX/1h6;
    .locals 3

    .prologue
    .line 295554
    sget-object v0, LX/1h6;->a:LX/1h6;

    if-nez v0, :cond_1

    .line 295555
    const-class v1, LX/1h6;

    monitor-enter v1

    .line 295556
    :try_start_0
    sget-object v0, LX/1h6;->a:LX/1h6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 295557
    if-eqz v2, :cond_0

    .line 295558
    :try_start_1
    new-instance v0, LX/1h6;

    invoke-direct {v0}, LX/1h6;-><init>()V

    .line 295559
    move-object v0, v0

    .line 295560
    sput-object v0, LX/1h6;->a:LX/1h6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 295561
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 295562
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 295563
    :cond_1
    sget-object v0, LX/1h6;->a:LX/1h6;

    return-object v0

    .line 295564
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 295565
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
