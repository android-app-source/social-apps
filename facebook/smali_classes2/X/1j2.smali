.class public LX/1j2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:LX/15D;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/15D",
            "<TT;>;"
        }
    .end annotation
.end field

.field public b:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation
.end field

.field private c:Lcom/facebook/http/common/FbHttpRequestProcessor;


# direct methods
.method public constructor <init>(LX/15D;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/facebook/http/common/FbHttpRequestProcessor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15D",
            "<TT;>;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 299712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 299713
    iput-object p1, p0, LX/1j2;->a:LX/15D;

    .line 299714
    iput-object p2, p0, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 299715
    iput-object p3, p0, LX/1j2;->c:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 299716
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 2

    .prologue
    .line 299717
    iget-object v0, p0, LX/1j2;->c:Lcom/facebook/http/common/FbHttpRequestProcessor;

    iget-object v1, p0, LX/1j2;->a:LX/15D;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 299718
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 299719
    iget-object v0, p0, LX/1j2;->c:Lcom/facebook/http/common/FbHttpRequestProcessor;

    iget-object v1, p0, LX/1j2;->a:LX/15D;

    invoke-virtual {v0, v1}, Lcom/facebook/http/common/FbHttpRequestProcessor;->c(LX/15D;)Z

    .line 299720
    return-void
.end method
