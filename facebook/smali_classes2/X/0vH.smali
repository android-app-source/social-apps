.class public final LX/0vH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:LX/0vH;

.field public static final b:Ljava/lang/Object;

.field public static final c:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 157788
    new-instance v0, LX/0vH;

    invoke-direct {v0}, LX/0vH;-><init>()V

    sput-object v0, LX/0vH;->a:LX/0vH;

    .line 157789
    new-instance v0, LX/0vI;

    invoke-direct {v0}, LX/0vI;-><init>()V

    sput-object v0, LX/0vH;->b:Ljava/lang/Object;

    .line 157790
    new-instance v0, LX/0vJ;

    invoke-direct {v0}, LX/0vJ;-><init>()V

    sput-object v0, LX/0vH;->c:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 157786
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157787
    return-void
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 157783
    if-nez p0, :cond_0

    .line 157784
    sget-object p0, LX/0vH;->c:Ljava/lang/Object;

    .line 157785
    :cond_0
    return-object p0
.end method

.method public static a(Ljava/lang/Throwable;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 157791
    new-instance v0, LX/1pS;

    invoke-direct {v0, p0}, LX/1pS;-><init>(Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static a(LX/0vA;Ljava/lang/Object;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0vA",
            "<-TT;>;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 157771
    sget-object v2, LX/0vH;->b:Ljava/lang/Object;

    if-ne p1, v2, :cond_0

    .line 157772
    invoke-interface {p0}, LX/0vA;->R_()V

    .line 157773
    :goto_0
    return v0

    .line 157774
    :cond_0
    sget-object v2, LX/0vH;->c:Ljava/lang/Object;

    if-ne p1, v2, :cond_1

    .line 157775
    const/4 v0, 0x0

    invoke-interface {p0, v0}, LX/0vA;->a(Ljava/lang/Object;)V

    move v0, v1

    .line 157776
    goto :goto_0

    .line 157777
    :cond_1
    if-eqz p1, :cond_3

    .line 157778
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, LX/1pS;

    if-ne v2, v3, :cond_2

    .line 157779
    check-cast p1, LX/1pS;

    iget-object v1, p1, LX/1pS;->e:Ljava/lang/Throwable;

    invoke-interface {p0, v1}, LX/0vA;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 157780
    :cond_2
    invoke-interface {p0, p1}, LX/0vA;->a(Ljava/lang/Object;)V

    move v0, v1

    .line 157781
    goto :goto_0

    .line 157782
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The lite notification can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 157770
    sget-object v0, LX/0vH;->b:Ljava/lang/Object;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
