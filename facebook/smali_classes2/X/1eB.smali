.class public final LX/1eB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Sp;


# instance fields
.field public final synthetic a:LX/1dt;


# direct methods
.method public constructor <init>(LX/1dt;)V
    .locals 0

    .prologue
    .line 287400
    iput-object p1, p0, LX/1eB;->a:LX/1dt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/0wD;
    .locals 1

    .prologue
    .line 287401
    sget-object v0, LX/0wD;->NEWSFEED:LX/0wD;

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 287402
    sget-object v0, LX/1dt;->c:LX/0Rf;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, LX/1dt;->j(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 287403
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 287404
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->c()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    .line 287405
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aE()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 287406
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ap()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->SEE_FIRST:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    if-ne v0, v6, :cond_0

    move v0, v1

    .line 287407
    :goto_1
    return v0

    .line 287408
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 287409
    :cond_1
    sget-object v0, LX/1dt;->b:LX/0Rf;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1
.end method
