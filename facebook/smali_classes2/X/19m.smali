.class public LX/19m;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile x:LX/19m;


# instance fields
.field private final a:LX/0Uh;

.field private final b:LX/0ad;

.field private final c:LX/0c1;

.field public final d:Z

.field public final e:Z

.field public final f:Z

.field public final g:Z

.field public final h:Z

.field public final i:Z

.field public final j:Z

.field private final k:Z

.field public final l:F

.field public final m:F

.field private final n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/19o;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/03z;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Z

.field private final s:I

.field private final t:I

.field public final u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final w:Z


# direct methods
.method public constructor <init>(LX/0Uh;LX/0ad;Landroid/content/Context;LX/0W3;)V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const/4 v2, 0x0

    .line 208565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208566
    iput-object p1, p0, LX/19m;->a:LX/0Uh;

    .line 208567
    iput-object p2, p0, LX/19m;->b:LX/0ad;

    .line 208568
    iget-object v0, p0, LX/19m;->a:LX/0Uh;

    sget v1, LX/19n;->j:I

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19m;->d:Z

    .line 208569
    iget-boolean v0, p0, LX/19m;->d:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/0c1;->On:LX/0c1;

    :goto_0
    iput-object v0, p0, LX/19m;->c:LX/0c1;

    .line 208570
    iget-object v0, p0, LX/19m;->a:LX/0Uh;

    sget v1, LX/19n;->i:I

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19m;->i:Z

    .line 208571
    iget-object v0, p0, LX/19m;->a:LX/0Uh;

    sget v1, LX/19n;->m:I

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19m;->r:Z

    .line 208572
    iget-object v0, p0, LX/19m;->a:LX/0Uh;

    sget v1, LX/19n;->k:I

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19m;->e:Z

    .line 208573
    iget-object v0, p0, LX/19m;->a:LX/0Uh;

    sget v1, LX/19n;->u:I

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19m;->k:Z

    .line 208574
    iget-object v0, p0, LX/19m;->a:LX/0Uh;

    sget v1, LX/19n;->w:I

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19m;->f:Z

    .line 208575
    sget-wide v0, LX/0X5;->f:J

    invoke-interface {p4, v0, v1}, LX/0W4;->a(J)Z

    move-result v0

    iput-boolean v0, p0, LX/19m;->w:Z

    .line 208576
    sget-short v0, LX/0ws;->R:S

    invoke-direct {p0, v0, v2}, LX/19m;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19m;->g:Z

    .line 208577
    sget-short v0, LX/0ws;->Q:S

    invoke-direct {p0, v0, v2}, LX/19m;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/19m;->h:Z

    .line 208578
    invoke-direct {p0}, LX/19m;->u()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/19m;->n:LX/0Px;

    .line 208579
    sget-object v0, LX/0c0;->Live:LX/0c0;

    iget-object v1, p0, LX/19m;->c:LX/0c1;

    sget-char v2, LX/0ws;->P:C

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081979

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/19m;->o:Ljava/lang/String;

    .line 208580
    sget-object v0, LX/0c0;->Live:LX/0c0;

    iget-object v1, p0, LX/19m;->c:LX/0c1;

    sget-char v2, LX/0ws;->O:C

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08197a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/19m;->p:Ljava/lang/String;

    .line 208581
    invoke-direct {p0}, LX/19m;->v()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/19m;->q:LX/0Px;

    .line 208582
    sget-object v0, LX/0c0;->Live:LX/0c0;

    iget-object v1, p0, LX/19m;->c:LX/0c1;

    sget v2, LX/0ws;->p:I

    const/16 v3, 0x2000

    invoke-interface {p2, v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    iput v0, p0, LX/19m;->s:I

    .line 208583
    sget-object v0, LX/0c0;->Live:LX/0c0;

    iget-object v1, p0, LX/19m;->c:LX/0c1;

    sget v2, LX/0ws;->o:I

    const/16 v3, 0x400

    invoke-interface {p2, v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    iput v0, p0, LX/19m;->t:I

    .line 208584
    sget-object v0, LX/0c0;->Live:LX/0c0;

    iget-object v1, p0, LX/19m;->c:LX/0c1;

    sget-char v2, LX/0ws;->s:C

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08197b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/19m;->u:Ljava/lang/String;

    .line 208585
    sget-object v0, LX/0c0;->Live:LX/0c0;

    iget-object v1, p0, LX/19m;->c:LX/0c1;

    sget-char v2, LX/0ws;->r:C

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08197c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/19m;->v:Ljava/lang/String;

    .line 208586
    sget-wide v0, LX/0X5;->g:J

    invoke-interface {p4, v0, v1}, LX/0W4;->g(J)D

    move-result-wide v0

    div-double v0, v6, v0

    double-to-float v0, v0

    iput v0, p0, LX/19m;->l:F

    .line 208587
    sget-wide v0, LX/0X5;->h:J

    invoke-interface {p4, v0, v1}, LX/0W4;->g(J)D

    move-result-wide v0

    div-double v0, v6, v0

    double-to-float v0, v0

    iput v0, p0, LX/19m;->m:F

    .line 208588
    sget-wide v0, LX/0X5;->i:J

    invoke-interface {p4, v0, v1}, LX/0W4;->a(J)Z

    move-result v0

    iput-boolean v0, p0, LX/19m;->j:Z

    .line 208589
    return-void

    .line 208590
    :cond_0
    sget-object v0, LX/0c1;->Off:LX/0c1;

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)LX/19m;
    .locals 7

    .prologue
    .line 208591
    sget-object v0, LX/19m;->x:LX/19m;

    if-nez v0, :cond_1

    .line 208592
    const-class v1, LX/19m;

    monitor-enter v1

    .line 208593
    :try_start_0
    sget-object v0, LX/19m;->x:LX/19m;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 208594
    if-eqz v2, :cond_0

    .line 208595
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 208596
    new-instance p0, LX/19m;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v6

    check-cast v6, LX/0W3;

    invoke-direct {p0, v3, v4, v5, v6}, LX/19m;-><init>(LX/0Uh;LX/0ad;Landroid/content/Context;LX/0W3;)V

    .line 208597
    move-object v0, p0

    .line 208598
    sput-object v0, LX/19m;->x:LX/19m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208599
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 208600
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 208601
    :cond_1
    sget-object v0, LX/19m;->x:LX/19m;

    return-object v0

    .line 208602
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 208603
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(SZ)Z
    .locals 3

    .prologue
    .line 208564
    iget-object v0, p0, LX/19m;->b:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    iget-object v2, p0, LX/19m;->c:LX/0c1;

    invoke-interface {v0, v1, v2, p1, p2}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    return v0
.end method

.method private u()LX/0Px;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/19o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208555
    iget-object v0, p0, LX/19m;->b:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    iget-object v2, p0, LX/19m;->c:LX/0c1;

    sget-char v3, LX/0ws;->S:C

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 208556
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 208557
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 208558
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 208559
    invoke-static {v4}, LX/19o;->fromString(Ljava/lang/String;)LX/19o;

    move-result-object v4

    .line 208560
    sget-object v5, LX/19o;->UNKNOWN:LX/19o;

    if-eq v4, v5, :cond_0

    .line 208561
    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 208562
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 208563
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private v()LX/0Px;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/03z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208546
    iget-object v0, p0, LX/19m;->b:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    iget-object v2, p0, LX/19m;->c:LX/0c1;

    sget-char v3, LX/0ws;->q:C

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 208547
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 208548
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 208549
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 208550
    invoke-static {v4}, LX/03z;->fromString(Ljava/lang/String;)LX/03z;

    move-result-object v4

    .line 208551
    sget-object v5, LX/03z;->UNKNOWN:LX/03z;

    if-eq v4, v5, :cond_0

    .line 208552
    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 208553
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 208554
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final c()Z
    .locals 1

    .prologue
    .line 208604
    iget-boolean v0, p0, LX/19m;->k:Z

    return v0
.end method

.method public final g()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/19o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208545
    iget-object v0, p0, LX/19m;->n:LX/0Px;

    return-object v0
.end method

.method public final j()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/03z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208544
    iget-object v0, p0, LX/19m;->q:LX/0Px;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 208543
    iget-boolean v0, p0, LX/19m;->r:Z

    return v0
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 208541
    iget v0, p0, LX/19m;->s:I

    return v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 208542
    iget v0, p0, LX/19m;->t:I

    return v0
.end method
