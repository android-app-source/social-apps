.class public LX/1cw;
.super Landroid/view/TouchDelegate;
.source ""


# static fields
.field private static final a:Landroid/graphics/Rect;

.field public static final b:LX/0Zj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zj",
            "<",
            "LX/0YU",
            "<",
            "LX/1cx;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public final c:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/1cx;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/1cx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 283680
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/1cw;->a:Landroid/graphics/Rect;

    .line 283681
    new-instance v0, LX/0Zj;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/0Zj;-><init>(I)V

    sput-object v0, LX/1cw;->b:LX/0Zj;

    return-void
.end method

.method public constructor <init>(LX/1cn;)V
    .locals 1

    .prologue
    .line 283682
    sget-object v0, LX/1cw;->a:Landroid/graphics/Rect;

    invoke-direct {p0, v0, p1}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    .line 283683
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/1cw;->c:LX/0YU;

    .line 283684
    return-void
.end method


# virtual methods
.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    .line 283685
    iget-object v0, p0, LX/1cw;->c:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_4

    .line 283686
    iget-object v0, p0, LX/1cw;->c:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cx;

    .line 283687
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 283688
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v5, v4

    .line 283689
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v6, v4

    .line 283690
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    move v4, v2

    .line 283691
    :cond_1
    :goto_1
    if-eqz v4, :cond_2

    .line 283692
    if-eqz v3, :cond_5

    .line 283693
    iget-object v2, v0, LX/1cx;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget-object v3, v0, LX/1cx;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 283694
    :goto_2
    iget-object v2, v0, LX/1cx;->b:Landroid/view/View;

    invoke-virtual {v2, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 283695
    :cond_2
    move v0, v2

    .line 283696
    if-eqz v0, :cond_3

    .line 283697
    const/4 v0, 0x1

    .line 283698
    :goto_3
    return v0

    .line 283699
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 283700
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 283701
    :pswitch_0
    iget-object v4, v0, LX/1cx;->e:Landroid/graphics/Rect;

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 283702
    iput-boolean v3, v0, LX/1cx;->c:Z

    move v4, v3

    .line 283703
    goto :goto_1

    .line 283704
    :pswitch_1
    iget-boolean v4, v0, LX/1cx;->c:Z

    .line 283705
    iget-boolean v7, v0, LX/1cx;->c:Z

    if-eqz v7, :cond_1

    .line 283706
    iget-object v7, v0, LX/1cx;->f:Landroid/graphics/Rect;

    invoke-virtual {v7, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    if-nez v5, :cond_1

    move v3, v2

    .line 283707
    goto :goto_1

    .line 283708
    :pswitch_2
    iget-boolean v4, v0, LX/1cx;->c:Z

    .line 283709
    iput-boolean v2, v0, LX/1cx;->c:Z

    goto :goto_1

    .line 283710
    :cond_5
    iget v2, v0, LX/1cx;->d:I

    mul-int/lit8 v2, v2, 0x2

    neg-int v2, v2

    int-to-float v2, v2

    iget v3, v0, LX/1cx;->d:I

    mul-int/lit8 v3, v3, 0x2

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/view/MotionEvent;->setLocation(FF)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
