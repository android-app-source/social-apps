.class public LX/19v;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/19v;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/0kb;

.field private final c:LX/19w;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0kb;LX/19w;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 209103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209104
    iput-object p1, p0, LX/19v;->a:LX/0Zb;

    .line 209105
    iput-object p2, p0, LX/19v;->b:LX/0kb;

    .line 209106
    iput-object p3, p0, LX/19v;->c:LX/19w;

    .line 209107
    return-void
.end method

.method public static a(LX/0QB;)LX/19v;
    .locals 6

    .prologue
    .line 209090
    sget-object v0, LX/19v;->d:LX/19v;

    if-nez v0, :cond_1

    .line 209091
    const-class v1, LX/19v;

    monitor-enter v1

    .line 209092
    :try_start_0
    sget-object v0, LX/19v;->d:LX/19v;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 209093
    if-eqz v2, :cond_0

    .line 209094
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 209095
    new-instance p0, LX/19v;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-static {v0}, LX/19w;->a(LX/0QB;)LX/19w;

    move-result-object v5

    check-cast v5, LX/19w;

    invoke-direct {p0, v3, v4, v5}, LX/19v;-><init>(LX/0Zb;LX/0kb;LX/19w;)V

    .line 209096
    move-object v0, p0

    .line 209097
    sput-object v0, LX/19v;->d:LX/19v;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209098
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 209099
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 209100
    :cond_1
    sget-object v0, LX/19v;->d:LX/19v;

    return-object v0

    .line 209101
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 209102
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/19v;Ljava/lang/String;LX/7Jg;LX/7Jf;LX/7Jd;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 7

    .prologue
    const-wide/16 v2, -0x1

    .line 209070
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v0, p4, LX/7Jd;->value:Ljava/lang/String;

    invoke-direct {v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 209071
    iget-object v0, p0, LX/19v;->b:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->b()Landroid/net/NetworkInfo;

    move-result-object v5

    .line 209072
    if-nez v5, :cond_0

    move-object v0, v4

    .line 209073
    :goto_0
    return-object v0

    .line 209074
    :cond_0
    sget-object v0, LX/7Je;->VIDEO_ID:LX/7Je;

    iget-object v0, v0, LX/7Je;->value:Ljava/lang/String;

    invoke-virtual {v4, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209075
    sget-object v0, LX/7Je;->VIDEO_SIZE:LX/7Je;

    iget-object v6, v0, LX/7Je;->value:Ljava/lang/String;

    if-eqz p2, :cond_3

    iget-wide v0, p2, LX/7Jg;->d:J

    :goto_1
    invoke-virtual {v4, v6, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209076
    sget-object v0, LX/7Je;->DOWNLOADED_SIZE:LX/7Je;

    iget-object v0, v0, LX/7Je;->value:Ljava/lang/String;

    if-eqz p2, :cond_1

    iget-wide v2, p2, LX/7Jg;->f:J

    :cond_1
    invoke-virtual {v4, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209077
    if-eqz p2, :cond_2

    iget-object v0, p2, LX/7Jg;->b:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 209078
    sget-object v0, LX/7Je;->VIDEO_FILE_NAME:LX/7Je;

    iget-object v0, v0, LX/7Je;->value:Ljava/lang/String;

    iget-object v1, p2, LX/7Jg;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209079
    :cond_2
    sget-object v0, LX/7Je;->DOWNLOAD_ORIGIN:LX/7Je;

    iget-object v1, v0, LX/7Je;->value:Ljava/lang/String;

    if-eqz p3, :cond_4

    iget-object v0, p3, LX/7Jf;->f:Ljava/lang/String;

    :goto_2
    invoke-virtual {v4, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209080
    sget-object v0, LX/7Je;->DOWNLOAD_SESSION_ID:LX/7Je;

    iget-object v1, v0, LX/7Je;->value:Ljava/lang/String;

    if-eqz p2, :cond_5

    iget-object v0, p2, LX/7Jg;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    invoke-virtual {v4, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209081
    sget-object v0, LX/7Je;->CONNECTION_TYPE_PARAM:LX/7Je;

    iget-object v0, v0, LX/7Je;->value:Ljava/lang/String;

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209082
    sget-object v0, LX/7Je;->CONNECTION_SUB_TYPE_PARAM:LX/7Je;

    iget-object v0, v0, LX/7Je;->value:Ljava/lang/String;

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209083
    const-string v0, "OfflineVideoModule"

    .line 209084
    iput-object v0, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 209085
    move-object v0, v4

    .line 209086
    goto :goto_0

    :cond_3
    move-wide v0, v2

    .line 209087
    goto :goto_1

    .line 209088
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 209089
    :cond_5
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static a(LX/19v;Ljava/lang/String;LX/7Jb;Z)V
    .locals 3

    .prologue
    .line 209064
    if-eqz p3, :cond_0

    :try_start_0
    sget-object v0, LX/7Jd;->DOWNLOAD_CANCELLED:LX/7Jd;

    :goto_0
    invoke-direct {p0, p1, v0}, LX/19v;->b(Ljava/lang/String;LX/7Jd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 209065
    sget-object v1, LX/7Je;->DELETE_REASON:LX/7Je;

    iget-object v1, v1, LX/7Je;->value:Ljava/lang/String;

    iget-object v2, p2, LX/7Jb;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209066
    iget-object v1, p0, LX/19v;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 209067
    :goto_1
    return-void

    .line 209068
    :cond_0
    sget-object v0, LX/7Jd;->DOWNLOAD_DELETED:LX/7Jd;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 209069
    :catch_0
    goto :goto_1
.end method

.method private a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 4

    .prologue
    .line 209058
    sget-object v0, LX/7Je;->TOTAL_OFFLINE_VIDEO_SIZE:LX/7Je;

    iget-object v0, v0, LX/7Je;->value:Ljava/lang/String;

    iget-object v1, p0, LX/19v;->c:LX/19w;

    invoke-virtual {v1}, LX/19w;->n()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209059
    sget-object v0, LX/7Je;->AVAILABLE_DISK_SIZE:LX/7Je;

    iget-object v0, v0, LX/7Je;->value:Ljava/lang/String;

    iget-object v1, p0, LX/19v;->c:LX/19w;

    invoke-virtual {v1}, LX/19w;->p()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209060
    sget-object v0, LX/7Je;->OFFLINE_VIDEO_COUNT:LX/7Je;

    iget-object v0, v0, LX/7Je;->value:Ljava/lang/String;

    iget-object v1, p0, LX/19v;->c:LX/19w;

    .line 209061
    iget-object v2, v1, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    move v1, v2

    .line 209062
    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209063
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 209108
    if-eqz p4, :cond_1

    :try_start_0
    sget-object v0, LX/7Jd;->DOWNLOAD_ABORTED:LX/7Jd;

    :goto_0
    invoke-direct {p0, p1, v0}, LX/19v;->b(Ljava/lang/String;LX/7Jd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 209109
    if-eqz p2, :cond_0

    .line 209110
    sget-object v1, LX/7Je;->EXCEPTION:LX/7Je;

    iget-object v1, v1, LX/7Je;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209111
    if-eqz p3, :cond_0

    .line 209112
    sget-object v1, LX/7Je;->EXCEPTION_CODE:LX/7Je;

    iget-object v1, v1, LX/7Je;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209113
    :cond_0
    invoke-direct {p0, v0}, LX/19v;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 209114
    iget-object v1, p0, LX/19v;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 209115
    :goto_1
    return-void

    .line 209116
    :cond_1
    sget-object v0, LX/7Jd;->DOWNLOAD_FAILED:LX/7Jd;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 209117
    :catch_0
    goto :goto_1
.end method

.method private b(Ljava/lang/String;LX/7Jd;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 209028
    iget-object v0, p0, LX/19v;->c:LX/19w;

    invoke-virtual {v0, p1}, LX/19w;->i(Ljava/lang/String;)LX/7Jg;

    move-result-object v2

    .line 209029
    iget-object v0, p0, LX/19v;->c:LX/19w;

    invoke-virtual {v0, p1}, LX/19w;->m(Ljava/lang/String;)LX/7Jf;

    move-result-object v3

    .line 209030
    :try_start_0
    invoke-static {p0, p1, v2, v3, p2}, LX/19v;->a(LX/19v;Ljava/lang/String;LX/7Jg;LX/7Jf;LX/7Jd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 209031
    sget-object v4, LX/7Jd;->DOWNLOAD_COMPLETED:LX/7Jd;

    if-eq p2, v4, :cond_0

    sget-object v4, LX/7Jd;->DOWNLOAD_CANCELLED:LX/7Jd;

    if-eq p2, v4, :cond_0

    sget-object v4, LX/7Jd;->DOWNLOAD_ABORTED:LX/7Jd;

    if-ne p2, v4, :cond_1

    .line 209032
    :cond_0
    sget-object v4, LX/7Je;->DOWNLOAD_ATTEMPTS:LX/7Je;

    iget-object v4, v4, LX/7Je;->value:Ljava/lang/String;

    iget v5, v3, LX/7Jf;->b:I

    invoke-virtual {v0, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209033
    sget-object v4, LX/7Je;->DOWNLOAD_DURATION:LX/7Je;

    iget-object v4, v4, LX/7Je;->value:Ljava/lang/String;

    iget-wide v6, v3, LX/7Jf;->d:J

    iget-wide v8, v3, LX/7Jf;->c:J

    sub-long/2addr v6, v8

    invoke-virtual {v0, v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209034
    invoke-direct {p0, v0}, LX/19v;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 209035
    :cond_1
    if-nez v2, :cond_2

    .line 209036
    sget-object v1, LX/7Je;->SAVED_OFFLINE:LX/7Je;

    iget-object v1, v1, LX/7Je;->value:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209037
    :goto_0
    return-object v0

    .line 209038
    :cond_2
    sget-object v3, LX/7Je;->SAVED_OFFLINE:LX/7Je;

    iget-object v3, v3, LX/7Je;->value:Ljava/lang/String;

    iget-object v4, v2, LX/7Jg;->l:LX/1A0;

    sget-object v5, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-ne v4, v5, :cond_3

    const/4 v1, 0x1

    :cond_3
    invoke-virtual {v0, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209039
    sget-object v1, LX/7Je;->SCHEDULING_POLICY:LX/7Je;

    iget-object v1, v1, LX/7Je;->value:Ljava/lang/String;

    iget-object v3, v2, LX/7Jg;->n:LX/2ft;

    invoke-virtual {v0, v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209040
    sget-object v1, LX/7Je;->DOWNLOAD_TYPE:LX/7Je;

    iget-object v1, v1, LX/7Je;->value:Ljava/lang/String;

    .line 209041
    iget-boolean v3, v2, LX/7Jg;->o:Z

    if-eqz v3, :cond_4

    .line 209042
    sget-object v3, LX/7Jc;->EPHEMERAL:LX/7Jc;

    .line 209043
    :goto_1
    move-object v2, v3

    .line 209044
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 209045
    :catch_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    sget-object v3, LX/7Jc;->DEFAULT:LX/7Jc;

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 209054
    :try_start_0
    sget-object v0, LX/7Jd;->PLAYBACK_BLOCKED:LX/7Jd;

    invoke-direct {p0, p1, v0}, LX/19v;->b(Ljava/lang/String;LX/7Jd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 209055
    sget-object v1, LX/7Je;->TIME_SINCE_LAST_CHECK:LX/7Je;

    iget-object v1, v1, LX/7Je;->value:Ljava/lang/String;

    iget-object v2, p0, LX/19v;->c:LX/19w;

    invoke-virtual {v2, p1}, LX/19w;->l(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 209056
    iget-object v1, p0, LX/19v;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 209057
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/7Jd;)V
    .locals 2

    .prologue
    .line 209050
    invoke-direct {p0, p1, p2}, LX/19v;->b(Ljava/lang/String;LX/7Jd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 209051
    if-eqz v0, :cond_0

    .line 209052
    iget-object v1, p0, LX/19v;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 209053
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 209048
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/19v;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;Z)V

    .line 209049
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 209046
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, LX/19v;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;Z)V

    .line 209047
    return-void
.end method
