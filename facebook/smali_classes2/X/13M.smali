.class public LX/13M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/13H;


# instance fields
.field private final a:LX/13N;


# direct methods
.method public constructor <init>(LX/13N;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 176554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176555
    iput-object p1, p0, LX/13M;->a:LX/13N;

    .line 176556
    return-void
.end method

.method private a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)LX/1Y7;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 176557
    iget-object v0, p0, LX/13M;->a:LX/13N;

    .line 176558
    const/4 v2, -0x1

    .line 176559
    sget-object p0, LX/2fy;->IMPRESSION:LX/2fy;

    if-ne p2, p0, :cond_3

    .line 176560
    iget v2, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->maxImpressions:I

    .line 176561
    :cond_0
    :goto_0
    if-lez v2, :cond_6

    invoke-virtual {v0, p1, p2}, LX/13N;->c(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)I

    move-result p0

    if-lt p0, v2, :cond_6

    const/4 v2, 0x1

    :goto_1
    move v0, v2

    .line 176562
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 176563
    :goto_2
    if-eqz v0, :cond_2

    .line 176564
    sget-object v0, LX/1Y7;->a:LX/1Y7;

    .line 176565
    :goto_3
    return-object v0

    :cond_1
    move v0, v1

    .line 176566
    goto :goto_2

    .line 176567
    :cond_2
    new-instance v0, LX/1Y8;

    invoke-direct {v0, v1}, LX/1Y8;-><init>(Z)V

    .line 176568
    iput-object p2, v0, LX/1Y8;->c:LX/2fy;

    .line 176569
    move-object v0, v0

    .line 176570
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Limit reached for counter: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, LX/2fy;->getReadableName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 176571
    iput-object v1, v0, LX/1Y8;->e:Ljava/lang/String;

    .line 176572
    move-object v0, v0

    .line 176573
    invoke-virtual {v0}, LX/1Y8;->a()LX/1Y7;

    move-result-object v0

    goto :goto_3

    .line 176574
    :cond_3
    sget-object p0, LX/2fy;->PRIMARY_ACTION:LX/2fy;

    if-ne p2, p0, :cond_4

    iget-object p0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-eqz p0, :cond_4

    .line 176575
    iget-object v2, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget v2, v2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->limit:I

    goto :goto_0

    .line 176576
    :cond_4
    sget-object p0, LX/2fy;->SECONDARY_ACTION:LX/2fy;

    if-ne p2, p0, :cond_5

    iget-object p0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-eqz p0, :cond_5

    .line 176577
    iget-object v2, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget v2, v2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->limit:I

    goto :goto_0

    .line 176578
    :cond_5
    sget-object p0, LX/2fy;->DISMISS_ACTION:LX/2fy;

    if-ne p2, p0, :cond_0

    iget-object p0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-eqz p0, :cond_0

    .line 176579
    iget-object v2, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget v2, v2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->limit:I

    goto :goto_0

    .line 176580
    :cond_6
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/13M;
    .locals 2

    .prologue
    .line 176581
    new-instance v1, LX/13M;

    invoke-static {p0}, LX/13N;->a(LX/0QB;)LX/13N;

    move-result-object v0

    check-cast v0, LX/13N;

    invoke-direct {v1, v0}, LX/13M;-><init>(LX/13N;)V

    .line 176582
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/1Y7;
    .locals 2
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 176583
    sget-object v0, LX/1Y7;->a:LX/1Y7;

    .line 176584
    iget v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->maxImpressions:I

    if-lez v1, :cond_1

    .line 176585
    sget-object v0, LX/2fy;->IMPRESSION:LX/2fy;

    invoke-direct {p0, p1, v0}, LX/13M;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)LX/1Y7;

    move-result-object v0

    .line 176586
    iget-boolean v1, v0, LX/1Y7;->c:Z

    if-nez v1, :cond_1

    .line 176587
    :cond_0
    :goto_0
    return-object v0

    .line 176588
    :cond_1
    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-eqz v1, :cond_2

    .line 176589
    sget-object v0, LX/2fy;->PRIMARY_ACTION:LX/2fy;

    invoke-direct {p0, p1, v0}, LX/13M;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)LX/1Y7;

    move-result-object v0

    .line 176590
    iget-boolean v1, v0, LX/1Y7;->c:Z

    if-eqz v1, :cond_0

    .line 176591
    :cond_2
    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-eqz v1, :cond_3

    .line 176592
    sget-object v0, LX/2fy;->SECONDARY_ACTION:LX/2fy;

    invoke-direct {p0, p1, v0}, LX/13M;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)LX/1Y7;

    move-result-object v0

    .line 176593
    iget-boolean v1, v0, LX/1Y7;->c:Z

    if-eqz v1, :cond_0

    .line 176594
    :cond_3
    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-eqz v1, :cond_0

    .line 176595
    sget-object v0, LX/2fy;->DISMISS_ACTION:LX/2fy;

    invoke-direct {p0, p1, v0}, LX/13M;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)LX/1Y7;

    move-result-object v0

    .line 176596
    iget-boolean v1, v0, LX/1Y7;->c:Z

    if-nez v1, :cond_0

    goto :goto_0
.end method
