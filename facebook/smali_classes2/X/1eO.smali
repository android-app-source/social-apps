.class public final LX/1eO;
.super LX/1eP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1eP",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1bh;

.field public final synthetic b:LX/1cR;


# direct methods
.method public constructor <init>(LX/1cR;LX/1cd;LX/1bh;)V
    .locals 0

    .prologue
    .line 288158
    iput-object p1, p0, LX/1eO;->b:LX/1cR;

    iput-object p3, p0, LX/1eO;->a:LX/1bh;

    invoke-direct {p0, p2}, LX/1eP;-><init>(LX/1cd;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Z)V
    .locals 4

    .prologue
    .line 288159
    check-cast p1, LX/1FJ;

    .line 288160
    if-nez p1, :cond_1

    .line 288161
    if-eqz p2, :cond_0

    .line 288162
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 288163
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 288164
    :cond_0
    :goto_0
    return-void

    .line 288165
    :cond_1
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    invoke-virtual {v0}, LX/1ln;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 288166
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 288167
    invoke-virtual {v0, p1, p2}, LX/1cd;->b(Ljava/lang/Object;Z)V

    goto :goto_0

    .line 288168
    :cond_2
    if-nez p2, :cond_5

    .line 288169
    iget-object v0, p0, LX/1eO;->b:LX/1cR;

    iget-object v0, v0, LX/1cR;->a:LX/1Fh;

    iget-object v1, p0, LX/1eO;->a:LX/1bh;

    invoke-interface {v0, v1}, LX/1Fh;->a(Ljava/lang/Object;)LX/1FJ;

    move-result-object v1

    .line 288170
    if-eqz v1, :cond_5

    .line 288171
    :try_start_0
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    invoke-virtual {v0}, LX/1ln;->d()LX/1lk;

    move-result-object v2

    .line 288172
    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    invoke-virtual {v0}, LX/1ln;->d()LX/1lk;

    move-result-object v0

    .line 288173
    iget-boolean v3, v0, LX/1lk;->d:Z

    move v3, v3

    .line 288174
    if-nez v3, :cond_3

    .line 288175
    iget v3, v0, LX/1lk;->b:I

    move v0, v3

    .line 288176
    iget v3, v2, LX/1lk;->b:I

    move v2, v3

    .line 288177
    if-lt v0, v2, :cond_4

    .line 288178
    :cond_3
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 288179
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/1cd;->b(Ljava/lang/Object;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288180
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    :cond_4
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    .line 288181
    :cond_5
    iget-object v0, p0, LX/1eO;->b:LX/1cR;

    iget-object v0, v0, LX/1cR;->a:LX/1Fh;

    iget-object v1, p0, LX/1eO;->a:LX/1bh;

    invoke-interface {v0, v1, p1}, LX/1Fh;->a(Ljava/lang/Object;LX/1FJ;)LX/1FJ;

    move-result-object v1

    .line 288182
    if-eqz p2, :cond_6

    .line 288183
    :try_start_1
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 288184
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, LX/1cd;->b(F)V

    .line 288185
    :cond_6
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 288186
    if-eqz v1, :cond_7

    move-object p1, v1

    :cond_7
    invoke-virtual {v0, p1, p2}, LX/1cd;->b(Ljava/lang/Object;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 288187
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 288188
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0

    .line 288189
    :catchall_1
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method
