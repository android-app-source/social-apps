.class public final LX/0pi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0pi;


# instance fields
.field private a:LX/0pj;

.field private b:LX/0Zm;

.field private c:LX/0So;


# direct methods
.method public constructor <init>(LX/0pj;LX/0Zm;LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 145125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145126
    iput-object p1, p0, LX/0pi;->a:LX/0pj;

    .line 145127
    iput-object p2, p0, LX/0pi;->b:LX/0Zm;

    .line 145128
    iput-object p3, p0, LX/0pi;->c:LX/0So;

    .line 145129
    return-void
.end method

.method public static a(LX/0QB;)LX/0pi;
    .locals 6

    .prologue
    .line 145112
    sget-object v0, LX/0pi;->d:LX/0pi;

    if-nez v0, :cond_1

    .line 145113
    const-class v1, LX/0pi;

    monitor-enter v1

    .line 145114
    :try_start_0
    sget-object v0, LX/0pi;->d:LX/0pi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 145115
    if-eqz v2, :cond_0

    .line 145116
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 145117
    new-instance p0, LX/0pi;

    invoke-static {v0}, LX/0pj;->a(LX/0QB;)LX/0pj;

    move-result-object v3

    check-cast v3, LX/0pj;

    invoke-static {v0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v4

    check-cast v4, LX/0Zm;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-direct {p0, v3, v4, v5}, LX/0pi;-><init>(LX/0pj;LX/0Zm;LX/0So;)V

    .line 145118
    move-object v0, p0

    .line 145119
    sput-object v0, LX/0pi;->d:LX/0pi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145120
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 145121
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 145122
    :cond_1
    sget-object v0, LX/0pi;->d:LX/0pi;

    return-object v0

    .line 145123
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 145124
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/1GE;
    .locals 2

    .prologue
    .line 145111
    new-instance v0, LX/1GX;

    invoke-virtual {p0, p1}, LX/0pi;->b(Ljava/lang/String;)LX/0pk;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1GX;-><init>(LX/0pk;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/0pk;
    .locals 6

    .prologue
    .line 145110
    new-instance v0, LX/0pk;

    iget-object v1, p0, LX/0pi;->a:LX/0pj;

    iget-object v3, p0, LX/0pi;->b:LX/0Zm;

    iget-object v4, p0, LX/0pi;->c:LX/0So;

    move-object v2, p1

    invoke-direct/range {v0 .. v4}, LX/0pk;-><init>(LX/0pj;Ljava/lang/String;LX/0Zm;LX/0So;)V

    return-object v0
.end method
