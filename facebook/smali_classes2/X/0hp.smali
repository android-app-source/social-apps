.class public LX/0hp;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0xB;

.field private final c:LX/0xX;

.field private final d:LX/1CC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 118518
    const-class v0, LX/0hp;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0hp;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0xB;LX/0xX;LX/1CC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 118513
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118514
    iput-object p1, p0, LX/0hp;->b:LX/0xB;

    .line 118515
    iput-object p2, p0, LX/0hp;->c:LX/0xX;

    .line 118516
    iput-object p3, p0, LX/0hp;->d:LX/1CC;

    .line 118517
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 118519
    iget-object v0, p0, LX/0hp;->b:LX/0xB;

    sget-object v1, LX/12j;->VIDEO_HOME:LX/12j;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0xB;->a(LX/12j;I)V

    .line 118520
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 118506
    invoke-virtual {p0, p1}, LX/0hp;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118507
    iget-object v0, p0, LX/0hp;->c:LX/0xX;

    sget-object v1, LX/1vy;->VH_LIVE_NOTIFICATIONS:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 118508
    :cond_0
    :goto_0
    return-void

    .line 118509
    :cond_1
    iget-object v0, p0, LX/0hp;->b:LX/0xB;

    sget-object v1, LX/12j;->VIDEO_HOME:LX/12j;

    invoke-virtual {v0, v1, p1}, LX/0xB;->a(LX/12j;I)V

    .line 118510
    iget-object v0, p0, LX/0hp;->d:LX/1CC;

    .line 118511
    iput p1, v0, LX/1CC;->h:I

    .line 118512
    goto :goto_0
.end method

.method public final b(I)Z
    .locals 2

    .prologue
    .line 118504
    iget-object v0, p0, LX/0hp;->b:LX/0xB;

    sget-object v1, LX/12j;->VIDEO_HOME:LX/12j;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/12j;)I

    move-result v0

    .line 118505
    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
