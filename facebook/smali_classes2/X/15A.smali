.class public final LX/15A;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 180166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180167
    return-void
.end method

.method public static a()Lcom/facebook/http/interfaces/RequestPriority;
    .locals 1

    .prologue
    .line 180168
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v0

    .line 180169
    invoke-static {v0}, LX/15A;->a(I)Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v0

    return-object v0
.end method

.method private static a(I)Lcom/facebook/http/interfaces/RequestPriority;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 180170
    invoke-static {p0}, LX/0TP;->getClosestThreadPriorityFromAndroidThreadPriority(I)LX/0TP;

    move-result-object v0

    .line 180171
    invoke-static {v0}, LX/15A;->a(LX/0TP;)Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v0

    .line 180172
    return-object v0
.end method

.method private static a(LX/0TP;)Lcom/facebook/http/interfaces/RequestPriority;
    .locals 2

    .prologue
    .line 180173
    if-eqz p0, :cond_0

    .line 180174
    sget-object v0, LX/15B;->a:[I

    invoke-virtual {p0}, LX/0TP;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 180175
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ThreadPriority %s is not supported in conversion to network priority."

    invoke-static {v1, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180176
    :pswitch_0
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 180177
    :goto_0
    return-object v0

    .line 180178
    :pswitch_1
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0

    .line 180179
    :pswitch_2
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
