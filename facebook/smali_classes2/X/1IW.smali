.class public LX/1IW;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/1Hz;

.field private final c:LX/1I5;

.field private final d:LX/03V;

.field private final e:LX/1BA;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 228815
    const-class v0, LX/1IW;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1IW;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Hz;LX/1I5;LX/03V;LX/1BA;LX/0Or;)V
    .locals 1
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Hz;",
            "LX/1I5;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/localstats/LocalStatsLogger;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 228807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 228808
    iput-object p1, p0, LX/1IW;->b:LX/1Hz;

    .line 228809
    iput-object p2, p0, LX/1IW;->c:LX/1I5;

    .line 228810
    iput-object p3, p0, LX/1IW;->d:LX/03V;

    .line 228811
    iput-object p4, p0, LX/1IW;->e:LX/1BA;

    .line 228812
    iput-object p5, p0, LX/1IW;->g:LX/0Or;

    .line 228813
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1IW;->f:Ljava/util/List;

    .line 228814
    return-void
.end method

.method private static a(LX/1IW;I)V
    .locals 3

    .prologue
    .line 228802
    invoke-virtual {p0}, LX/1IW;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 228803
    iget-object v0, p0, LX/1IW;->e:LX/1BA;

    invoke-virtual {v0, p1}, LX/1BA;->a(I)V

    .line 228804
    iget-object v0, p0, LX/1IW;->d:LX/03V;

    sget-object v1, LX/1IW;->a:Ljava/lang/String;

    const-string v2, "UserCrypto not available"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 228805
    new-instance v0, LX/48k;

    invoke-direct {v0}, LX/48k;-><init>()V

    throw v0

    .line 228806
    :cond_0
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/1IW;Lcom/facebook/auth/component/AuthenticationResult;)V
    .locals 4

    .prologue
    .line 228786
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Lcom/facebook/auth/component/AuthenticationResult;->a()Ljava/lang/String;

    move-result-object v0

    .line 228787
    invoke-interface {p1}, Lcom/facebook/auth/component/AuthenticationResult;->e()Ljava/lang/String;

    move-result-object v1

    .line 228788
    invoke-interface {p1}, Lcom/facebook/auth/component/AuthenticationResult;->f()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 228789
    :try_start_1
    iget-object v3, p0, LX/1IW;->c:LX/1I5;

    invoke-virtual {v3, v0, v1, v2}, LX/1I5;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 228790
    iget-object v0, p0, LX/1IW;->e:LX/1BA;

    const v1, 0x990001

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, LX/1BA;->a(IS)V
    :try_end_1
    .catch LX/48i; {:try_start_1 .. :try_end_1} :catch_0
    .catch LX/48h; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 228791
    :goto_0
    monitor-exit p0

    return-void

    .line 228792
    :catch_0
    move-exception v0

    .line 228793
    :try_start_2
    iget-object v1, p0, LX/1IW;->d:LX/03V;

    sget-object v2, LX/1IW;->a:Ljava/lang/String;

    const-string v3, "Cannot enable encryption for user"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 228794
    iget-object v0, p0, LX/1IW;->e:LX/1BA;

    const v1, 0x990008

    invoke-virtual {v0, v1}, LX/1BA;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 228795
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 228796
    :catch_1
    move-exception v0

    .line 228797
    :try_start_3
    iget-object v1, p0, LX/1IW;->d:LX/03V;

    sget-object v2, LX/1IW;->a:Ljava/lang/String;

    const-string v3, "Cannot enable encryption for user"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 228798
    iget-object v0, p0, LX/1IW;->e:LX/1BA;

    const v1, 0x990007

    invoke-virtual {v0, v1}, LX/1BA;->a(I)V

    goto :goto_0

    .line 228799
    :catch_2
    move-exception v0

    .line 228800
    iget-object v1, p0, LX/1IW;->d:LX/03V;

    sget-object v2, LX/1IW;->a:Ljava/lang/String;

    const-string v3, "Unexpected error configuring encryption"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 228801
    iget-object v0, p0, LX/1IW;->e:LX/1BA;

    const v1, 0x990001

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, LX/1BA;->a(IS)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized a$redex0(LX/1IW;Z)V
    .locals 4

    .prologue
    .line 228779
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1IW;->c:LX/1I5;

    invoke-virtual {v0, p1}, LX/1I5;->a(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228780
    :goto_0
    :try_start_1
    iget-object v0, p0, LX/1IW;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 228781
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 228782
    :catch_0
    move-exception v0

    .line 228783
    :try_start_2
    iget-object v1, p0, LX/1IW;->d:LX/03V;

    sget-object v2, LX/1IW;->a:Ljava/lang/String;

    const-string v3, "Unexpected error disabling encryption"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 228784
    :cond_0
    iget-object v0, p0, LX/1IW;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 228785
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/io/InputStream;LX/1Hb;)Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 228776
    monitor-enter p0

    const v0, 0x990005

    :try_start_0
    invoke-static {p0, v0}, LX/1IW;->a(LX/1IW;I)V

    .line 228777
    iget-object v0, p0, LX/1IW;->b:LX/1Hz;

    invoke-virtual {v0, p1, p2}, LX/1Hz;->a(Ljava/io/InputStream;LX/1Hb;)Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 228778
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/io/OutputStream;LX/1Hb;[B)Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 228773
    monitor-enter p0

    const v0, 0x990004

    :try_start_0
    invoke-static {p0, v0}, LX/1IW;->a(LX/1IW;I)V

    .line 228774
    iget-object v0, p0, LX/1IW;->b:LX/1Hz;

    invoke-virtual {v0, p1, p2, p3}, LX/1Hz;->a(Ljava/io/OutputStream;LX/1Hb;[B)Ljava/io/OutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 228775
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 228766
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1IW;->c:LX/1I5;

    invoke-virtual {v0}, LX/1I5;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a([BLX/1Hb;)[B
    .locals 1

    .prologue
    .line 228770
    monitor-enter p0

    const v0, 0x990004

    :try_start_0
    invoke-static {p0, v0}, LX/1IW;->a(LX/1IW;I)V

    .line 228771
    iget-object v0, p0, LX/1IW;->b:LX/1Hz;

    invoke-virtual {v0, p1, p2}, LX/1Hz;->a([BLX/1Hb;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 228772
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b([BLX/1Hb;)[B
    .locals 1

    .prologue
    .line 228767
    monitor-enter p0

    const v0, 0x990005

    :try_start_0
    invoke-static {p0, v0}, LX/1IW;->a(LX/1IW;I)V

    .line 228768
    iget-object v0, p0, LX/1IW;->b:LX/1Hz;

    invoke-virtual {v0, p1, p2}, LX/1Hz;->b([BLX/1Hb;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 228769
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
