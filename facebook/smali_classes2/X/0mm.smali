.class public LX/0mm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zh;

.field private final b:LX/0mR;

.field private final c:LX/0mb;


# direct methods
.method public constructor <init>(LX/0Zh;LX/0mR;LX/0mb;)V
    .locals 0

    .prologue
    .line 133075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133076
    iput-object p1, p0, LX/0mm;->a:LX/0Zh;

    .line 133077
    iput-object p2, p0, LX/0mm;->b:LX/0mR;

    .line 133078
    iput-object p3, p0, LX/0mm;->c:LX/0mb;

    .line 133079
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/Writer;)V
    .locals 4

    .prologue
    .line 133049
    iget-object v0, p0, LX/0mm;->a:LX/0Zh;

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v1

    .line 133050
    const-string v0, "writeFixedData"

    const v2, 0x22673882

    invoke-static {v0, v2}, LX/03q;->a(Ljava/lang/String;I)V

    .line 133051
    :try_start_0
    const-string v0, "time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 133052
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 133053
    const-string v0, "app_id"

    iget-object v2, p0, LX/0mm;->b:LX/0mR;

    .line 133054
    iget-object v3, v2, LX/0mR;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/00I;

    invoke-interface {v3}, LX/00I;->c()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 133055
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 133056
    const-string v0, "app_ver"

    iget-object v2, p0, LX/0mm;->b:LX/0mR;

    invoke-virtual {v2}, LX/0mR;->b()Ljava/lang/String;

    move-result-object v2

    .line 133057
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 133058
    const-string v0, "build_num"

    iget-object v2, p0, LX/0mm;->b:LX/0mR;

    .line 133059
    iget-object v3, v2, LX/0mR;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0WV;

    invoke-virtual {v3}, LX/0WV;->b()I

    move-result v3

    move v2, v3

    .line 133060
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 133061
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 133062
    const-string v0, "device"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 133063
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 133064
    const-string v0, "os_ver"

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 133065
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 133066
    const-string v0, "device_id"

    iget-object v2, p0, LX/0mm;->c:LX/0mb;

    .line 133067
    iget-object v3, v2, LX/0mb;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 133068
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 133069
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, LX/0nC;->b(Ljava/io/Writer;LX/0nA;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133070
    invoke-virtual {v1}, LX/0nA;->a()V

    .line 133071
    const v0, 0x389bd4a5

    invoke-static {v0}, LX/03q;->a(I)V

    .line 133072
    return-void

    .line 133073
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/0nA;->a()V

    .line 133074
    const v1, 0x64ecda48

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
.end method
