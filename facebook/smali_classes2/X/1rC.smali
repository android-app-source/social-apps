.class public final LX/1rC;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "LX/1rD;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/1rC;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1rD;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 331821
    invoke-direct {p0, p1}, LX/0aT;-><init>(LX/0Ot;)V

    .line 331822
    return-void
.end method

.method public static a(LX/0QB;)LX/1rC;
    .locals 4

    .prologue
    .line 331823
    sget-object v0, LX/1rC;->a:LX/1rC;

    if-nez v0, :cond_1

    .line 331824
    const-class v1, LX/1rC;

    monitor-enter v1

    .line 331825
    :try_start_0
    sget-object v0, LX/1rC;->a:LX/1rC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 331826
    if-eqz v2, :cond_0

    .line 331827
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 331828
    new-instance v3, LX/1rC;

    const/16 p0, 0xb5

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1rC;-><init>(LX/0Ot;)V

    .line 331829
    move-object v0, v3

    .line 331830
    sput-object v0, LX/1rC;->a:LX/1rC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331831
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 331832
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 331833
    :cond_1
    sget-object v0, LX/1rC;->a:LX/1rC;

    return-object v0

    .line 331834
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 331835
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 331836
    check-cast p3, LX/1rD;

    .line 331837
    iget-object v0, p3, LX/1rD;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 331838
    iput-wide v0, p3, LX/1rD;->f:J

    .line 331839
    return-void
.end method
