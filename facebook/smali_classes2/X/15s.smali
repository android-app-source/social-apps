.class public LX/15s;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/15s;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0ad;

.field public final c:LX/01T;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;LX/01T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 181645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181646
    iput-object p1, p0, LX/15s;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 181647
    iput-object p2, p0, LX/15s;->b:LX/0ad;

    .line 181648
    iput-object p3, p0, LX/15s;->c:LX/01T;

    .line 181649
    return-void
.end method

.method public static a(LX/0QB;)LX/15s;
    .locals 6

    .prologue
    .line 181650
    sget-object v0, LX/15s;->d:LX/15s;

    if-nez v0, :cond_1

    .line 181651
    const-class v1, LX/15s;

    monitor-enter v1

    .line 181652
    :try_start_0
    sget-object v0, LX/15s;->d:LX/15s;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 181653
    if-eqz v2, :cond_0

    .line 181654
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 181655
    new-instance p0, LX/15s;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v5

    check-cast v5, LX/01T;

    invoke-direct {p0, v3, v4, v5}, LX/15s;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;LX/01T;)V

    .line 181656
    move-object v0, p0

    .line 181657
    sput-object v0, LX/15s;->d:LX/15s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 181658
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 181659
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 181660
    :cond_1
    sget-object v0, LX/15s;->d:LX/15s;

    return-object v0

    .line 181661
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 181662
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
