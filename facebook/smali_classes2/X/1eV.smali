.class public LX/1eV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1FL;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public b:Z
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public c:LX/1eW;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public d:J
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public e:J
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final f:Ljava/util/concurrent/Executor;

.field private final g:LX/1eU;

.field public final h:Ljava/lang/Runnable;

.field private final i:Ljava/lang/Runnable;

.field private final j:I


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/1eU;I)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 288512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 288513
    iput-object p1, p0, LX/1eV;->f:Ljava/util/concurrent/Executor;

    .line 288514
    iput-object p2, p0, LX/1eV;->g:LX/1eU;

    .line 288515
    iput p3, p0, LX/1eV;->j:I

    .line 288516
    new-instance v0, Lcom/facebook/imagepipeline/producers/JobScheduler$1;

    invoke-direct {v0, p0}, Lcom/facebook/imagepipeline/producers/JobScheduler$1;-><init>(LX/1eV;)V

    iput-object v0, p0, LX/1eV;->h:Ljava/lang/Runnable;

    .line 288517
    new-instance v0, Lcom/facebook/imagepipeline/producers/JobScheduler$2;

    invoke-direct {v0, p0}, Lcom/facebook/imagepipeline/producers/JobScheduler$2;-><init>(LX/1eV;)V

    iput-object v0, p0, LX/1eV;->i:Ljava/lang/Runnable;

    .line 288518
    const/4 v0, 0x0

    iput-object v0, p0, LX/1eV;->a:LX/1FL;

    .line 288519
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1eV;->b:Z

    .line 288520
    sget-object v0, LX/1eW;->IDLE:LX/1eW;

    iput-object v0, p0, LX/1eV;->c:LX/1eW;

    .line 288521
    iput-wide v2, p0, LX/1eV;->d:J

    .line 288522
    iput-wide v2, p0, LX/1eV;->e:J

    .line 288523
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 288505
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 288506
    sget-object v0, LX/4ev;->a:Ljava/util/concurrent/ScheduledExecutorService;

    if-nez v0, :cond_0

    .line 288507
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    sput-object v0, LX/4ev;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 288508
    :cond_0
    sget-object v0, LX/4ev;->a:Ljava/util/concurrent/ScheduledExecutorService;

    move-object v0, v0

    .line 288509
    iget-object v1, p0, LX/1eV;->i:Ljava/lang/Runnable;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, p1, p2, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 288510
    :goto_0
    return-void

    .line 288511
    :cond_1
    iget-object v0, p0, LX/1eV;->i:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method private static b(LX/1FL;Z)Z
    .locals 1

    .prologue
    .line 288504
    if-nez p1, :cond_0

    invoke-static {p0}, LX/1FL;->e(LX/1FL;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/1eV;)V
    .locals 5

    .prologue
    .line 288435
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 288436
    monitor-enter p0

    .line 288437
    :try_start_0
    iget-object v2, p0, LX/1eV;->a:LX/1FL;

    .line 288438
    iget-boolean v3, p0, LX/1eV;->b:Z

    .line 288439
    const/4 v4, 0x0

    iput-object v4, p0, LX/1eV;->a:LX/1FL;

    .line 288440
    const/4 v4, 0x0

    iput-boolean v4, p0, LX/1eV;->b:Z

    .line 288441
    sget-object v4, LX/1eW;->RUNNING:LX/1eW;

    iput-object v4, p0, LX/1eV;->c:LX/1eW;

    .line 288442
    iput-wide v0, p0, LX/1eV;->e:J

    .line 288443
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288444
    :try_start_1
    invoke-static {v2, v3}, LX/1eV;->b(LX/1FL;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288445
    iget-object v0, p0, LX/1eV;->g:LX/1eU;

    invoke-interface {v0, v2, v3}, LX/1eU;->a(LX/1FL;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 288446
    :cond_0
    invoke-static {v2}, LX/1FL;->d(LX/1FL;)V

    .line 288447
    invoke-static {p0}, LX/1eV;->f(LX/1eV;)V

    .line 288448
    return-void

    .line 288449
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 288450
    :catchall_1
    move-exception v0

    invoke-static {v2}, LX/1FL;->d(LX/1FL;)V

    .line 288451
    invoke-static {p0}, LX/1eV;->f(LX/1eV;)V

    throw v0
.end method

.method private static f(LX/1eV;)V
    .locals 7

    .prologue
    .line 288489
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 288490
    const-wide/16 v2, 0x0

    .line 288491
    const/4 v0, 0x0

    .line 288492
    monitor-enter p0

    .line 288493
    :try_start_0
    iget-object v1, p0, LX/1eV;->c:LX/1eW;

    sget-object v6, LX/1eW;->RUNNING_AND_PENDING:LX/1eW;

    if-ne v1, v6, :cond_1

    .line 288494
    iget-wide v0, p0, LX/1eV;->e:J

    iget v2, p0, LX/1eV;->j:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 288495
    const/4 v0, 0x1

    .line 288496
    iput-wide v4, p0, LX/1eV;->d:J

    .line 288497
    sget-object v1, LX/1eW;->QUEUED:LX/1eW;

    iput-object v1, p0, LX/1eV;->c:LX/1eW;

    .line 288498
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288499
    if-eqz v0, :cond_0

    .line 288500
    sub-long v0, v2, v4

    invoke-direct {p0, v0, v1}, LX/1eV;->a(J)V

    .line 288501
    :cond_0
    return-void

    .line 288502
    :cond_1
    :try_start_1
    sget-object v1, LX/1eW;->IDLE:LX/1eW;

    iput-object v1, p0, LX/1eV;->c:LX/1eW;

    goto :goto_0

    .line 288503
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 288481
    monitor-enter p0

    .line 288482
    :try_start_0
    iget-object v0, p0, LX/1eV;->a:LX/1FL;

    .line 288483
    const/4 v1, 0x0

    iput-object v1, p0, LX/1eV;->a:LX/1FL;

    .line 288484
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/1eV;->b:Z

    .line 288485
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288486
    invoke-static {v0}, LX/1FL;->d(LX/1FL;)V

    .line 288487
    return-void

    .line 288488
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/1FL;Z)Z
    .locals 2

    .prologue
    .line 288470
    invoke-static {p1, p2}, LX/1eV;->b(LX/1FL;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 288471
    const/4 v0, 0x0

    .line 288472
    :goto_0
    return v0

    .line 288473
    :cond_0
    monitor-enter p0

    .line 288474
    :try_start_0
    iget-object v0, p0, LX/1eV;->a:LX/1FL;

    .line 288475
    invoke-static {p1}, LX/1FL;->a(LX/1FL;)LX/1FL;

    move-result-object v1

    iput-object v1, p0, LX/1eV;->a:LX/1FL;

    .line 288476
    iput-boolean p2, p0, LX/1eV;->b:Z

    .line 288477
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288478
    invoke-static {v0}, LX/1FL;->d(LX/1FL;)V

    .line 288479
    const/4 v0, 0x1

    goto :goto_0

    .line 288480
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 288453
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 288454
    const-wide/16 v2, 0x0

    .line 288455
    monitor-enter p0

    .line 288456
    :try_start_0
    iget-object v6, p0, LX/1eV;->a:LX/1FL;

    iget-boolean v7, p0, LX/1eV;->b:Z

    invoke-static {v6, v7}, LX/1eV;->b(LX/1FL;Z)Z

    move-result v6

    if-nez v6, :cond_0

    .line 288457
    monitor-exit p0

    .line 288458
    :goto_0
    return v0

    .line 288459
    :cond_0
    sget-object v6, LX/1lj;->a:[I

    iget-object v7, p0, LX/1eV;->c:LX/1eW;

    invoke-virtual {v7}, LX/1eW;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 288460
    :goto_1
    :pswitch_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288461
    if-eqz v0, :cond_1

    .line 288462
    sub-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, LX/1eV;->a(J)V

    :cond_1
    move v0, v1

    .line 288463
    goto :goto_0

    .line 288464
    :pswitch_1
    :try_start_1
    iget-wide v2, p0, LX/1eV;->e:J

    iget v0, p0, LX/1eV;->j:I

    int-to-long v6, v0

    add-long/2addr v2, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 288465
    iput-wide v4, p0, LX/1eV;->d:J

    .line 288466
    sget-object v0, LX/1eW;->QUEUED:LX/1eW;

    iput-object v0, p0, LX/1eV;->c:LX/1eW;

    move v0, v1

    .line 288467
    goto :goto_1

    .line 288468
    :pswitch_2
    sget-object v6, LX/1eW;->RUNNING_AND_PENDING:LX/1eW;

    iput-object v6, p0, LX/1eV;->c:LX/1eW;

    goto :goto_1

    .line 288469
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final declared-synchronized c()J
    .locals 4

    .prologue
    .line 288452
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/1eV;->e:J

    iget-wide v2, p0, LX/1eV;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long/2addr v0, v2

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
