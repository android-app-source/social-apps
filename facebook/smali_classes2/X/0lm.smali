.class public final LX/0lm;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0ln;

.field public static final b:LX/0ln;

.field public static final c:LX/0ln;

.field public static final d:LX/0ln;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const v7, 0x7fffffff

    const/16 v4, 0x3d

    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 130555
    new-instance v0, LX/0ln;

    const-string v1, "MIME"

    const-string v2, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

    const/16 v5, 0x4c

    invoke-direct/range {v0 .. v5}, LX/0ln;-><init>(Ljava/lang/String;Ljava/lang/String;ZCI)V

    sput-object v0, LX/0lm;->a:LX/0ln;

    .line 130556
    new-instance v0, LX/0ln;

    sget-object v1, LX/0lm;->a:LX/0ln;

    const-string v2, "MIME-NO-LINEFEEDS"

    invoke-direct {v0, v1, v2, v7}, LX/0ln;-><init>(LX/0ln;Ljava/lang/String;I)V

    sput-object v0, LX/0lm;->b:LX/0ln;

    .line 130557
    new-instance v0, LX/0ln;

    sget-object v1, LX/0lm;->a:LX/0ln;

    const-string v2, "PEM"

    const/16 v5, 0x40

    invoke-direct/range {v0 .. v5}, LX/0ln;-><init>(LX/0ln;Ljava/lang/String;ZCI)V

    sput-object v0, LX/0lm;->c:LX/0ln;

    .line 130558
    new-instance v2, Ljava/lang/StringBuffer;

    const-string v0, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

    invoke-direct {v2, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 130559
    const-string v0, "+"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x2d

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 130560
    const-string v0, "/"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x5f

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 130561
    new-instance v0, LX/0ln;

    const-string v1, "MODIFIED-FOR-URL"

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    move v3, v6

    move v4, v6

    move v5, v7

    invoke-direct/range {v0 .. v5}, LX/0ln;-><init>(Ljava/lang/String;Ljava/lang/String;ZCI)V

    sput-object v0, LX/0lm;->d:LX/0ln;

    .line 130562
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 130563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0ln;
    .locals 4

    .prologue
    .line 130564
    sget-object v0, LX/0lm;->a:LX/0ln;

    iget-object v0, v0, LX/0ln;->_name:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130565
    sget-object v0, LX/0lm;->a:LX/0ln;

    .line 130566
    :goto_0
    return-object v0

    .line 130567
    :cond_0
    sget-object v0, LX/0lm;->b:LX/0ln;

    iget-object v0, v0, LX/0ln;->_name:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130568
    sget-object v0, LX/0lm;->b:LX/0ln;

    goto :goto_0

    .line 130569
    :cond_1
    sget-object v0, LX/0lm;->c:LX/0ln;

    iget-object v0, v0, LX/0ln;->_name:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 130570
    sget-object v0, LX/0lm;->c:LX/0ln;

    goto :goto_0

    .line 130571
    :cond_2
    sget-object v0, LX/0lm;->d:LX/0ln;

    iget-object v0, v0, LX/0ln;->_name:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 130572
    sget-object v0, LX/0lm;->d:LX/0ln;

    goto :goto_0

    .line 130573
    :cond_3
    if-nez p0, :cond_4

    .line 130574
    const-string v0, "<null>"

    .line 130575
    :goto_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No Base64Variant with name "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 130576
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
