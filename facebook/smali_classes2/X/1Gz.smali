.class public LX/1Gz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Gd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1Gd",
        "<",
        "LX/1HR;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/app/ActivityManager;


# direct methods
.method public constructor <init>(Landroid/app/ActivityManager;)V
    .locals 0

    .prologue
    .line 226198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226199
    iput-object p1, p0, LX/1Gz;->a:Landroid/app/ActivityManager;

    .line 226200
    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 226186
    invoke-virtual {p0}, LX/1Gz;->b()LX/1HR;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/1HR;
    .locals 6

    .prologue
    const v3, 0x7fffffff

    .line 226187
    new-instance v0, LX/1HR;

    .line 226188
    iget-object v1, p0, LX/1Gz;->a:Landroid/app/ActivityManager;

    invoke-virtual {v1}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v1

    const/high16 v2, 0x100000

    mul-int/2addr v1, v2

    const v2, 0x7fffffff

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 226189
    const/high16 v2, 0x2000000

    if-ge v1, v2, :cond_0

    .line 226190
    const/high16 v1, 0x400000

    .line 226191
    :goto_0
    move v1, v1

    .line 226192
    const/16 v2, 0x100

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, LX/1HR;-><init>(IIIII)V

    return-object v0

    .line 226193
    :cond_0
    const/high16 v2, 0x4000000

    if-ge v1, v2, :cond_1

    .line 226194
    const/high16 v1, 0x600000

    goto :goto_0

    .line 226195
    :cond_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-ge v2, v4, :cond_2

    .line 226196
    const/high16 v1, 0x800000

    goto :goto_0

    .line 226197
    :cond_2
    div-int/lit8 v1, v1, 0x4

    goto :goto_0
.end method
