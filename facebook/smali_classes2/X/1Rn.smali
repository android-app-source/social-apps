.class public LX/1Rn;
.super Landroid/graphics/drawable/Drawable;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        ">",
        "Landroid/graphics/drawable/Drawable;"
    }
.end annotation


# static fields
.field public static a:LX/03R;


# instance fields
.field public A:J

.field public B:J

.field public C:Z

.field public final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/0Zb;

.field private final d:LX/0Zm;

.field private final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final f:Ljava/util/Random;

.field private final g:Landroid/text/TextPaint;

.field private final h:Landroid/graphics/Rect;

.field private final i:Landroid/graphics/Paint;

.field private final j:Ljava/lang/StringBuilder;

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field public o:Ljava/lang/String;

.field public p:J

.field public q:J

.field public r:J

.field public s:J

.field public t:J

.field public u:J

.field public v:J

.field public w:J

.field public x:J

.field public y:J

.field public z:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 246795
    sget-object v0, LX/03R;->UNSET:LX/03R;

    sput-object v0, LX/1Rn;->a:LX/03R;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/concurrent/ExecutorService;LX/0Zb;LX/0Zm;Ljava/util/Random;Landroid/content/Context;)V
    .locals 6
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p5    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    const/high16 v2, 0x40000000    # 2.0f

    const-wide/16 v0, -0x1

    .line 246796
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 246797
    iput-wide v0, p0, LX/1Rn;->p:J

    .line 246798
    iput-wide v0, p0, LX/1Rn;->q:J

    .line 246799
    iput-wide v0, p0, LX/1Rn;->r:J

    .line 246800
    iput-wide v0, p0, LX/1Rn;->s:J

    .line 246801
    iput-wide v0, p0, LX/1Rn;->t:J

    .line 246802
    iput-wide v4, p0, LX/1Rn;->u:J

    .line 246803
    iput-wide v0, p0, LX/1Rn;->v:J

    .line 246804
    iput-wide v0, p0, LX/1Rn;->w:J

    .line 246805
    iput-wide v0, p0, LX/1Rn;->x:J

    .line 246806
    iput-wide v0, p0, LX/1Rn;->y:J

    .line 246807
    iput-wide v0, p0, LX/1Rn;->z:J

    .line 246808
    iput-wide v0, p0, LX/1Rn;->A:J

    .line 246809
    iput-wide v4, p0, LX/1Rn;->B:J

    .line 246810
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Rn;->C:Z

    .line 246811
    iput-object p1, p0, LX/1Rn;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 246812
    iput-object p2, p0, LX/1Rn;->b:Ljava/util/concurrent/ExecutorService;

    .line 246813
    iput-object p3, p0, LX/1Rn;->c:LX/0Zb;

    .line 246814
    iput-object p4, p0, LX/1Rn;->d:LX/0Zm;

    .line 246815
    iput-object p5, p0, LX/1Rn;->f:Ljava/util/Random;

    .line 246816
    const/high16 v0, 0x41400000    # 12.0f

    invoke-static {p6, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/1Rn;->k:I

    .line 246817
    invoke-static {p6, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/1Rn;->l:I

    .line 246818
    invoke-static {p6, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/1Rn;->m:I

    .line 246819
    const/high16 v0, 0x41000000    # 8.0f

    invoke-static {p6, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/1Rn;->n:I

    .line 246820
    const/high16 v0, 0x41200000    # 10.0f

    invoke-static {p6, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    .line 246821
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    iput-object v1, p0, LX/1Rn;->g:Landroid/text/TextPaint;

    .line 246822
    iget-object v1, p0, LX/1Rn;->g:Landroid/text/TextPaint;

    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 246823
    iget-object v1, p0, LX/1Rn;->g:Landroid/text/TextPaint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 246824
    iget-object v1, p0, LX/1Rn;->g:Landroid/text/TextPaint;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 246825
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1Rn;->h:Landroid/graphics/Rect;

    .line 246826
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/1Rn;->i:Landroid/graphics/Paint;

    .line 246827
    iget-object v0, p0, LX/1Rn;->i:Landroid/graphics/Paint;

    const/16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 246828
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x41

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, LX/1Rn;->j:Ljava/lang/StringBuilder;

    .line 246829
    return-void
.end method

.method public static a(Lcom/facebook/prefs/shared/FbSharedPreferences;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 246830
    sget-object v2, LX/1Rn;->a:LX/03R;

    invoke-virtual {v2}, LX/03R;->isSet()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 246831
    sget-object v2, LX/1Rn;->a:LX/03R;

    sget-object v3, LX/03R;->YES:LX/03R;

    if-ne v2, v3, :cond_1

    .line 246832
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 246833
    goto :goto_0

    .line 246834
    :cond_2
    if-eqz p0, :cond_3

    invoke-interface {p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    move v0, v1

    .line 246835
    goto :goto_0

    .line 246836
    :cond_4
    sget-object v2, LX/1Ro;->b:LX/0Tn;

    invoke-interface {p0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;)LX/03R;

    move-result-object v2

    .line 246837
    sput-object v2, LX/1Rn;->a:LX/03R;

    sget-object v3, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v2, v3}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 246838
    sget-object v2, LX/03R;->NO:LX/03R;

    sput-object v2, LX/1Rn;->a:LX/03R;

    .line 246839
    :cond_5
    sget-object v2, LX/1Rn;->a:LX/03R;

    sget-object v3, LX/03R;->YES:LX/03R;

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static g(LX/1Rn;)Z
    .locals 4

    .prologue
    .line 246840
    iget-wide v0, p0, LX/1Rn;->r:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()Ljava/lang/String;
    .locals 8

    .prologue
    .line 246841
    invoke-static {p0}, LX/1Rn;->g(LX/1Rn;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 246842
    iget-wide v4, p0, LX/1Rn;->p:J

    iget-wide v6, p0, LX/1Rn;->q:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, p0, LX/1Rn;->x:J

    .line 246843
    iget-wide v4, p0, LX/1Rn;->r:J

    iput-wide v4, p0, LX/1Rn;->y:J

    .line 246844
    iget-wide v4, p0, LX/1Rn;->t:J

    iput-wide v4, p0, LX/1Rn;->z:J

    .line 246845
    iget-wide v4, p0, LX/1Rn;->s:J

    iput-wide v4, p0, LX/1Rn;->A:J

    .line 246846
    iget-wide v4, p0, LX/1Rn;->u:J

    iput-wide v4, p0, LX/1Rn;->B:J

    .line 246847
    :cond_0
    iget-object v0, p0, LX/1Rn;->j:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 246848
    iget-object v0, p0, LX/1Rn;->j:Ljava/lang/StringBuilder;

    iget-object v1, p0, LX/1Rn;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " prepare: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, LX/1Rn;->x:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " bind: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, LX/1Rn;->y:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " measure: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, LX/1Rn;->z:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " layout: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, LX/1Rn;->A:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " draw: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, LX/1Rn;->B:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 246849
    if-nez p1, :cond_1

    .line 246850
    const/4 v0, 0x0

    .line 246851
    iput-object v0, p0, LX/1Rn;->o:Ljava/lang/String;

    .line 246852
    :cond_0
    :goto_0
    return-void

    .line 246853
    :cond_1
    iget-object v0, p0, LX/1Rn;->o:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 246854
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 246855
    iput-object v0, p0, LX/1Rn;->o:Ljava/lang/String;

    .line 246856
    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 246857
    iget-object v0, p0, LX/1Rn;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/1Rn;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;)Z

    move-result v0

    return v0
.end method

.method public final c(J)V
    .locals 1

    .prologue
    .line 246858
    iput-wide p1, p0, LX/1Rn;->r:J

    .line 246859
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 246860
    iget-object v0, p0, LX/1Rn;->d:LX/0Zm;

    const-string v1, "view_scroll_perf"

    invoke-virtual {v0, v1}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246861
    iget-object v0, p0, LX/1Rn;->f:Ljava/util/Random;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/1Rn;->C:Z

    .line 246862
    :cond_0
    iget-boolean v0, p0, LX/1Rn;->C:Z

    return v0

    .line 246863
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x14

    const/4 v8, 0x0

    const/16 v1, -0x100

    const/high16 v2, -0x10000

    .line 246864
    invoke-virtual {p0}, LX/1Rn;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 246865
    :goto_0
    return-void

    .line 246866
    :cond_0
    invoke-direct {p0}, LX/1Rn;->i()Ljava/lang/String;

    move-result-object v3

    .line 246867
    iget-wide v4, p0, LX/1Rn;->x:J

    iget-wide v6, p0, LX/1Rn;->y:J

    add-long/2addr v4, v6

    iget-wide v6, p0, LX/1Rn;->z:J

    add-long/2addr v4, v6

    iget-wide v6, p0, LX/1Rn;->A:J

    add-long/2addr v4, v6

    iget-wide v6, p0, LX/1Rn;->B:J

    add-long/2addr v4, v6

    .line 246868
    iget-object v6, p0, LX/1Rn;->g:Landroid/text/TextPaint;

    cmp-long v0, v4, v10

    if-lez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v6, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 246869
    iget-object v0, p0, LX/1Rn;->i:Landroid/graphics/Paint;

    cmp-long v4, v4, v10

    if-lez v4, :cond_2

    :goto_2
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 246870
    iget-object v0, p0, LX/1Rn;->g:Landroid/text/TextPaint;

    invoke-static {v3, v0}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v0

    .line 246871
    iget-object v1, p0, LX/1Rn;->h:Landroid/graphics/Rect;

    float-to-int v0, v0

    iget v2, p0, LX/1Rn;->l:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    iget v2, p0, LX/1Rn;->k:I

    invoke-virtual {v1, v8, v8, v0, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 246872
    iget-object v0, p0, LX/1Rn;->h:Landroid/graphics/Rect;

    iget-object v1, p0, LX/1Rn;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 246873
    iget v0, p0, LX/1Rn;->m:I

    int-to-float v0, v0

    iget v1, p0, LX/1Rn;->n:I

    int-to-float v1, v1

    iget-object v2, p0, LX/1Rn;->g:Landroid/text/TextPaint;

    invoke-virtual {p1, v3, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 246874
    goto :goto_1

    :cond_2
    move v2, v1

    .line 246875
    goto :goto_2
.end method

.method public final f(J)V
    .locals 7

    .prologue
    .line 246876
    iput-wide p1, p0, LX/1Rn;->w:J

    .line 246877
    const-wide/16 v5, -0x1

    .line 246878
    const-wide/16 v1, 0x0

    iput-wide v1, p0, LX/1Rn;->u:J

    .line 246879
    iget-wide v1, p0, LX/1Rn;->v:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_0

    .line 246880
    iget-wide v1, p0, LX/1Rn;->u:J

    iget-wide v3, p0, LX/1Rn;->v:J

    add-long/2addr v1, v3

    iput-wide v1, p0, LX/1Rn;->u:J

    .line 246881
    :cond_0
    iget-wide v1, p0, LX/1Rn;->w:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_1

    .line 246882
    iget-wide v1, p0, LX/1Rn;->u:J

    iget-wide v3, p0, LX/1Rn;->w:J

    add-long/2addr v1, v3

    iput-wide v1, p0, LX/1Rn;->u:J

    .line 246883
    :cond_1
    return-void
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 246884
    const/4 v0, -0x1

    return v0
.end method

.method public final setAlpha(I)V
    .locals 0

    .prologue
    .line 246885
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 246886
    return-void
.end method
