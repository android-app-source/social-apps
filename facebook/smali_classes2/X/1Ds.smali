.class public LX/1Ds;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/1Ds;


# instance fields
.field private b:Landroid/content/res/Configuration;

.field private final c:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 218959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218960
    new-instance v0, LX/1Dt;

    const/16 v1, 0x1f4

    invoke-direct {v0, p0, v1}, LX/1Dt;-><init>(LX/1Ds;I)V

    iput-object v0, p0, LX/1Ds;->c:LX/0aq;

    .line 218961
    iput-object p1, p0, LX/1Ds;->b:Landroid/content/res/Configuration;

    .line 218962
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/res/Configuration;)LX/1Ds;
    .locals 2

    .prologue
    .line 218963
    const-class v1, LX/1Ds;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1Ds;->a:LX/1Ds;

    if-eqz v0, :cond_0

    sget-object v0, LX/1Ds;->a:LX/1Ds;

    iget-object v0, v0, LX/1Ds;->b:Landroid/content/res/Configuration;

    invoke-virtual {v0, p0}, Landroid/content/res/Configuration;->equals(Landroid/content/res/Configuration;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 218964
    :cond_0
    new-instance v0, LX/1Ds;

    invoke-direct {v0, p0}, LX/1Ds;-><init>(Landroid/content/res/Configuration;)V

    sput-object v0, LX/1Ds;->a:LX/1Ds;

    .line 218965
    :cond_1
    sget-object v0, LX/1Ds;->a:LX/1Ds;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 218966
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 218967
    iget-object v0, p0, LX/1Ds;->c:LX/0aq;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 218968
    iget-object v0, p0, LX/1Ds;->c:LX/0aq;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218969
    return-void
.end method
