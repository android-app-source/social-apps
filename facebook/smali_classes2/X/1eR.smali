.class public final LX/1eR;
.super LX/1eS;
.source ""


# instance fields
.field public final synthetic a:LX/1cQ;

.field private final c:LX/1eQ;

.field private final d:LX/1Gv;

.field private e:I


# direct methods
.method public constructor <init>(LX/1cQ;LX/1cd;LX/1cW;LX/1eQ;LX/1Gv;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            "LX/1eQ;",
            "Lcom/facebook/imagepipeline/decoder/ProgressiveJpegConfig;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 288289
    iput-object p1, p0, LX/1eR;->a:LX/1cQ;

    .line 288290
    invoke-direct {p0, p1, p2, p3, p6}, LX/1eS;-><init>(LX/1cQ;LX/1cd;LX/1cW;Z)V

    .line 288291
    invoke-static {p4}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1eQ;

    iput-object v0, p0, LX/1eR;->c:LX/1eQ;

    .line 288292
    invoke-static {p5}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Gv;

    iput-object v0, p0, LX/1eR;->d:LX/1Gv;

    .line 288293
    const/4 v0, 0x0

    iput v0, p0, LX/1eR;->e:I

    .line 288294
    return-void
.end method


# virtual methods
.method public final a(LX/1FL;)I
    .locals 1

    .prologue
    .line 288286
    iget-object v0, p0, LX/1eR;->c:LX/1eQ;

    .line 288287
    iget p0, v0, LX/1eQ;->f:I

    move v0, p0

    .line 288288
    return v0
.end method

.method public final declared-synchronized a(LX/1FL;Z)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 288266
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1, p2}, LX/1eS;->a(LX/1FL;Z)Z

    move-result v1

    .line 288267
    if-nez p2, :cond_3

    invoke-static {p1}, LX/1FL;->e(LX/1FL;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 288268
    iget-object v2, p0, LX/1eR;->c:LX/1eQ;

    invoke-virtual {v2, p1}, LX/1eQ;->a(LX/1FL;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 288269
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 288270
    :cond_1
    :try_start_1
    iget-object v2, p0, LX/1eR;->c:LX/1eQ;

    .line 288271
    iget v3, v2, LX/1eQ;->e:I

    move v2, v3

    .line 288272
    iget v3, p0, LX/1eR;->e:I

    if-le v2, v3, :cond_0

    iget-object v3, p0, LX/1eR;->d:LX/1Gv;

    iget v4, p0, LX/1eR;->e:I

    .line 288273
    iget-object v5, v3, LX/1Gv;->a:LX/1Gu;

    invoke-interface {v5}, LX/1Gu;->a()Ljava/util/List;

    move-result-object p2

    .line 288274
    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 288275
    :cond_2
    add-int/lit8 v5, v4, 0x1

    .line 288276
    :goto_1
    move v3, v5

    .line 288277
    if-lt v2, v3, :cond_0

    .line 288278
    iput v2, p0, LX/1eR;->e:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    move v0, v1

    .line 288279
    goto :goto_0

    .line 288280
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 288281
    :cond_4
    const/4 v5, 0x0

    move p1, v5

    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    if-ge p1, v5, :cond_6

    .line 288282
    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-le v5, v4, :cond_5

    .line 288283
    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    goto :goto_1

    .line 288284
    :cond_5
    add-int/lit8 v5, p1, 0x1

    move p1, v5

    goto :goto_2

    .line 288285
    :cond_6
    const v5, 0x7fffffff

    goto :goto_1
.end method

.method public final c()LX/1lk;
    .locals 3

    .prologue
    .line 288261
    iget-object v0, p0, LX/1eR;->d:LX/1Gv;

    iget-object v1, p0, LX/1eR;->c:LX/1eQ;

    .line 288262
    iget p0, v1, LX/1eQ;->e:I

    move v1, p0

    .line 288263
    const/4 p0, 0x0

    .line 288264
    iget-object v2, v0, LX/1Gv;->a:LX/1Gu;

    invoke-interface {v2}, LX/1Gu;->b()I

    move-result v2

    if-lt v1, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v1, v2, p0}, LX/1lk;->a(IZZ)LX/1lk;

    move-result-object v2

    move-object v0, v2

    .line 288265
    return-object v0

    :cond_0
    move v2, p0

    goto :goto_0
.end method
