.class public LX/1m0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final c:Ljava/lang/String;

.field private static volatile t:LX/1m0;


# instance fields
.field public final a:LX/0ad;

.field public final b:LX/0WJ;

.field private final d:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field private final f:Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

.field private final g:Ljava/net/InetSocketAddress;

.field private final h:LX/03V;

.field private final i:LX/1m2;

.field private final j:LX/0Uh;

.field private final k:LX/0Sj;

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Po;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/19s;

.field public final n:LX/16V;

.field public final o:LX/1m5;

.field private final p:LX/1Lu;

.field private q:LX/3Io;

.field private r:LX/1m4;

.field private s:LX/7Po;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 312975
    const-class v0, LX/1m0;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1m0;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/1m2;LX/0Uh;LX/0ad;LX/0Sj;LX/0WJ;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/1Lu;LX/19s;)V
    .locals 4
    .param p2    # LX/1m2;
        .annotation runtime Lcom/facebook/video/server/VideoServerListenerForVideoServer;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/VideoServerHttpServiceExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/video/server/VideoServerListener;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0ad;",
            "LX/0Sj;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/7Po;",
            ">;",
            "LX/1Lu;",
            "LX/19s;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 312956
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 312957
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, LX/1m0;->h:LX/03V;

    .line 312958
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1m2;

    iput-object v0, p0, LX/1m0;->i:LX/1m2;

    .line 312959
    iput-object p3, p0, LX/1m0;->j:LX/0Uh;

    .line 312960
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v0, p0, LX/1m0;->a:LX/0ad;

    .line 312961
    iput-object p5, p0, LX/1m0;->k:LX/0Sj;

    .line 312962
    iput-object p6, p0, LX/1m0;->b:LX/0WJ;

    .line 312963
    iput-object p7, p0, LX/1m0;->e:Ljava/util/concurrent/ExecutorService;

    .line 312964
    iput-object p8, p0, LX/1m0;->l:LX/0Ot;

    .line 312965
    iput-object p9, p0, LX/1m0;->p:LX/1Lu;

    .line 312966
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/1m0;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 312967
    new-instance v0, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

    invoke-direct {v0}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;-><init>()V

    iput-object v0, p0, LX/1m0;->f:Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

    .line 312968
    new-instance v0, Ljava/net/InetSocketAddress;

    const-string v1, "127.0.0.1"

    invoke-direct {v0, v1, v3}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, LX/1m0;->g:Ljava/net/InetSocketAddress;

    .line 312969
    new-instance v0, LX/1m4;

    invoke-direct {v0, p0}, LX/1m4;-><init>(LX/1m0;)V

    iput-object v0, p0, LX/1m0;->r:LX/1m4;

    .line 312970
    iget-object v0, p0, LX/1m0;->f:Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

    iget-object v1, p0, LX/1m0;->r:LX/1m4;

    invoke-static {v1}, LX/1m4;->a(LX/1m4;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/1m0;->r:LX/1m4;

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 312971
    new-instance v0, LX/16V;

    invoke-direct {v0}, LX/16V;-><init>()V

    iput-object v0, p0, LX/1m0;->n:LX/16V;

    .line 312972
    new-instance v0, LX/1m5;

    invoke-direct {v0}, LX/1m5;-><init>()V

    iput-object v0, p0, LX/1m0;->o:LX/1m5;

    .line 312973
    iput-object p10, p0, LX/1m0;->m:LX/19s;

    .line 312974
    return-void
.end method

.method public static a(Landroid/net/Uri;)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 312950
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v1, "127.0.0.1"

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 312951
    :cond_0
    :goto_0
    return v0

    .line 312952
    :cond_1
    const-string v1, "sid"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 312953
    if-eqz v1, :cond_0

    .line 312954
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 312955
    :catch_0
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1m0;
    .locals 14

    .prologue
    .line 312937
    sget-object v0, LX/1m0;->t:LX/1m0;

    if-nez v0, :cond_1

    .line 312938
    const-class v1, LX/1m0;

    monitor-enter v1

    .line 312939
    :try_start_0
    sget-object v0, LX/1m0;->t:LX/1m0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 312940
    if-eqz v2, :cond_0

    .line 312941
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 312942
    new-instance v3, LX/1m0;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/1m1;->a(LX/0QB;)LX/1m2;

    move-result-object v5

    check-cast v5, LX/1m2;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {v0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v8

    check-cast v8, LX/0Sj;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v9

    check-cast v9, LX/0WJ;

    invoke-static {v0}, LX/1m3;->a(LX/0QB;)Ljava/util/concurrent/ExecutorService;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ExecutorService;

    const/16 v11, 0x3801

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {v0}, LX/1Lu;->a(LX/0QB;)LX/1Lu;

    move-result-object v12

    check-cast v12, LX/1Lu;

    invoke-static {v0}, LX/19i;->a(LX/0QB;)LX/19s;

    move-result-object v13

    check-cast v13, LX/19s;

    invoke-direct/range {v3 .. v13}, LX/1m0;-><init>(LX/03V;LX/1m2;LX/0Uh;LX/0ad;LX/0Sj;LX/0WJ;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/1Lu;LX/19s;)V

    .line 312943
    move-object v0, v3

    .line 312944
    sput-object v0, LX/1m0;->t:LX/1m0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 312945
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 312946
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 312947
    :cond_1
    sget-object v0, LX/1m0;->t:LX/1m0;

    return-object v0

    .line 312948
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 312949
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static d(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 312933
    const-string v0, "remote-uri"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 312934
    if-nez v0, :cond_0

    .line 312935
    const/4 v0, 0x0

    .line 312936
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 312932
    sget-object v0, LX/1m0;->c:Ljava/lang/String;

    return-object v0
.end method

.method public static declared-synchronized e(LX/1m0;)LX/7Po;
    .locals 3

    .prologue
    .line 312922
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1m0;->s:LX/7Po;

    if-nez v0, :cond_0

    .line 312923
    iget-object v0, p0, LX/1m0;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Po;

    iput-object v0, p0, LX/1m0;->s:LX/7Po;

    .line 312924
    iget-object v0, p0, LX/1m0;->o:LX/1m5;

    iget-object v1, p0, LX/1m0;->s:LX/7Po;

    .line 312925
    iget-object v2, v1, LX/7Po;->m:LX/04m;

    move-object v1, v2

    .line 312926
    invoke-virtual {v0, v1}, LX/1m5;->a(LX/04m;)V

    .line 312927
    iget-object v0, p0, LX/1m0;->s:LX/7Po;

    .line 312928
    iget-object v1, v0, LX/7Po;->q:LX/16V;

    move-object v0, v1

    .line 312929
    iget-object v1, p0, LX/1m0;->n:LX/16V;

    invoke-virtual {v0, v1}, LX/16V;->a(LX/16V;)V

    .line 312930
    :cond_0
    iget-object v0, p0, LX/1m0;->s:LX/7Po;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 312931
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static e(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 312841
    if-nez p0, :cond_1

    .line 312842
    const/4 p0, 0x0

    .line 312843
    :cond_0
    :goto_0
    return-object p0

    .line 312844
    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v0, "127.0.0.1"

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312845
    :cond_2
    invoke-static {p0}, LX/1m0;->d(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p0

    goto :goto_0
.end method

.method private f()Z
    .locals 3

    .prologue
    .line 312921
    iget-object v0, p0, LX/1m0;->j:LX/0Uh;

    const/16 v1, 0x2c4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public static f(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 312920
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "127.0.0.1"

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 312919
    const-string v0, "vid"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized g()V
    .locals 1

    .prologue
    .line 312915
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1m0;->q:LX/3Io;

    if-nez v0, :cond_0

    .line 312916
    invoke-direct {p0}, LX/1m0;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312917
    :cond_0
    monitor-exit p0

    return-void

    .line 312918
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized h()V
    .locals 15

    .prologue
    .line 312879
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/3Io;

    new-instance v1, LX/3Ip;

    invoke-direct {v1}, LX/3Ip;-><init>()V

    iget-object v2, p0, LX/1m0;->f:Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

    iget-object v3, p0, LX/1m0;->k:LX/0Sj;

    iget-object v4, p0, LX/1m0;->h:LX/03V;

    iget-object v5, p0, LX/1m0;->e:Ljava/util/concurrent/ExecutorService;

    invoke-direct/range {v0 .. v5}, LX/3Io;-><init>(LX/3Ip;Lorg/apache/http/protocol/HttpRequestHandlerRegistry;LX/0Sj;LX/03V;Ljava/util/concurrent/ExecutorService;)V

    iput-object v0, p0, LX/1m0;->q:LX/3Io;

    .line 312880
    iget-object v0, p0, LX/1m0;->q:LX/3Io;

    iget-object v1, p0, LX/1m0;->g:Ljava/net/InetSocketAddress;

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 312881
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v12

    .line 312882
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/net/SocketAddress;

    .line 312883
    new-instance v6, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v6}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    const-string v7, "http.socket.timeout"

    const/16 v9, 0x1388

    invoke-virtual {v6, v7, v9}, Lorg/apache/http/params/BasicHttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    move-result-object v6

    const-string v7, "http.socket.buffer-size"

    const/16 v9, 0x2000

    invoke-interface {v6, v7, v9}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    move-result-object v6

    const-string v7, "http.connection.stalecheck"

    const/4 v9, 0x0

    invoke-interface {v6, v7, v9}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    move-result-object v6

    const-string v7, "http.tcp.nodelay"

    const/4 v9, 0x1

    invoke-interface {v6, v7, v9}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    move-result-object v6

    const-string v7, "http.origin-server"

    const-string v9, "GenericHttpServer"

    invoke-interface {v6, v7, v9}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    move-result-object v6

    const-string v7, "http.protocol.version"

    const-string v9, "HTTP/1.1"

    invoke-interface {v6, v7, v9}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    move-result-object v10

    .line 312884
    new-instance v6, Lorg/apache/http/protocol/BasicHttpProcessor;

    invoke-direct {v6}, Lorg/apache/http/protocol/BasicHttpProcessor;-><init>()V

    .line 312885
    new-instance v7, Lorg/apache/http/protocol/ResponseDate;

    invoke-direct {v7}, Lorg/apache/http/protocol/ResponseDate;-><init>()V

    invoke-virtual {v6, v7}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 312886
    new-instance v7, Lorg/apache/http/protocol/ResponseServer;

    invoke-direct {v7}, Lorg/apache/http/protocol/ResponseServer;-><init>()V

    invoke-virtual {v6, v7}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 312887
    new-instance v7, Lorg/apache/http/protocol/ResponseContent;

    invoke-direct {v7}, Lorg/apache/http/protocol/ResponseContent;-><init>()V

    invoke-virtual {v6, v7}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 312888
    new-instance v7, Lorg/apache/http/protocol/ResponseConnControl;

    invoke-direct {v7}, Lorg/apache/http/protocol/ResponseConnControl;-><init>()V

    invoke-virtual {v6, v7}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 312889
    new-instance v9, Lorg/apache/http/protocol/HttpService;

    new-instance v7, Lorg/apache/http/impl/DefaultConnectionReuseStrategy;

    invoke-direct {v7}, Lorg/apache/http/impl/DefaultConnectionReuseStrategy;-><init>()V

    new-instance v11, Lorg/apache/http/impl/DefaultHttpResponseFactory;

    invoke-direct {v11}, Lorg/apache/http/impl/DefaultHttpResponseFactory;-><init>()V

    invoke-direct {v9, v6, v7, v11}, Lorg/apache/http/protocol/HttpService;-><init>(Lorg/apache/http/protocol/HttpProcessor;Lorg/apache/http/ConnectionReuseStrategy;Lorg/apache/http/HttpResponseFactory;)V

    .line 312890
    invoke-virtual {v9, v10}, Lorg/apache/http/protocol/HttpService;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 312891
    iget-object v6, v0, LX/3Io;->b:Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

    invoke-virtual {v9, v6}, Lorg/apache/http/protocol/HttpService;->setHandlerResolver(Lorg/apache/http/protocol/HttpRequestHandlerResolver;)V

    .line 312892
    iget-object v14, v0, LX/3Io;->c:Ljava/util/ArrayList;

    new-instance v6, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;

    iget-object v11, v0, LX/3Io;->f:Ljava/util/concurrent/ExecutorService;

    move-object v7, v0

    invoke-direct/range {v6 .. v11}, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;-><init>(LX/3Io;Ljava/net/SocketAddress;Lorg/apache/http/protocol/HttpService;Lorg/apache/http/params/HttpParams;Ljava/util/concurrent/ExecutorService;)V

    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 312893
    :cond_0
    iget-object v6, v0, LX/3Io;->c:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;

    .line 312894
    invoke-static {v6}, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->a$redex0(Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;)Ljava/net/SocketAddress;

    move-result-object v8

    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 312895
    invoke-virtual {v6}, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->start()V

    goto :goto_1

    .line 312896
    :cond_1
    move-object v0, v12

    .line 312897
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetSocketAddress;

    .line 312898
    if-nez v0, :cond_2

    .line 312899
    sget-object v0, LX/1m0;->c:Ljava/lang/String;

    const-string v1, "Cannot bind to port"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312900
    :goto_2
    monitor-exit p0

    return-void

    .line 312901
    :cond_2
    :try_start_1
    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v0

    .line 312902
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "http"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "127.0.0.1:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    iget-object v2, p0, LX/1m0;->r:LX/1m4;

    invoke-static {v2}, LX/1m4;->a(LX/1m4;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 312903
    iget-object v2, p0, LX/1m0;->r:LX/1m4;

    .line 312904
    iput-object v1, v2, LX/1m4;->b:Landroid/net/Uri;

    .line 312905
    iget-object v1, p0, LX/1m0;->i:LX/1m2;

    .line 312906
    iput v0, v1, LX/1m2;->f:I

    .line 312907
    iget v2, v1, LX/1m2;->f:I

    .line 312908
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v0, LX/0AB;->LAUNCH_SERVER:LX/0AB;

    iget-object v0, v0, LX/0AB;->value:Ljava/lang/String;

    invoke-direct {v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "port"

    invoke-virtual {v3, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 312909
    iget-object v0, v1, LX/1m2;->e:LX/0Zb;

    invoke-interface {v0, v3}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 312910
    iget-object v0, p0, LX/1m0;->m:LX/19s;

    invoke-virtual {p0}, LX/1m0;->b()Landroid/net/Uri;

    move-result-object v1

    .line 312911
    iput-object v1, v0, LX/19s;->h:Landroid/net/Uri;

    .line 312912
    invoke-static {v0}, LX/19s;->m(LX/19s;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 312913
    goto :goto_2

    .line 312914
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/net/Uri;Ljava/lang/String;Z)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 312860
    monitor-enter p0

    :try_start_0
    const-string v0, "VideoServerBase.createLocalUriForRemoteUri"

    const v1, 0x569bac91

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312861
    if-nez p1, :cond_0

    .line 312862
    :try_start_1
    iget-object v0, p0, LX/1m0;->h:LX/03V;

    sget-object v1, LX/1m0;->c:Ljava/lang/String;

    const-string v2, "createLocalUri called with null!!"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 312863
    const v0, 0x4223597e

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 p1, 0x0

    :goto_0
    monitor-exit p0

    return-object p1

    .line 312864
    :cond_0
    :try_start_3
    invoke-direct {p0}, LX/1m0;->f()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v0

    if-nez v0, :cond_1

    .line 312865
    const v0, -0x17849bda

    :try_start_4
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 312866
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 312867
    :cond_1
    :try_start_5
    invoke-direct {p0}, LX/1m0;->g()V

    .line 312868
    iget-object v0, p0, LX/1m0;->r:LX/1m4;

    .line 312869
    iget-object v1, v0, LX/1m4;->b:Landroid/net/Uri;

    move-object v0, v1

    .line 312870
    if-nez v0, :cond_2

    .line 312871
    iget-object v0, p0, LX/1m0;->h:LX/03V;

    sget-object v1, LX/1m0;->c:Ljava/lang/String;

    const-string v2, "BaseUri returned null!"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 312872
    const v0, -0x1ae51cfe

    :try_start_6
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 312873
    :cond_2
    :try_start_7
    iget-object v1, p0, LX/1m0;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 312874
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "remote-uri"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "vid"

    invoke-virtual {v0, v2, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 312875
    if-eqz p3, :cond_3

    new-instance v2, LX/7I0;

    iget-object v3, p0, LX/1m0;->a:LX/0ad;

    invoke-direct {v2, v3}, LX/7I0;-><init>(LX/0ad;)V

    iget-boolean v2, v2, LX/7I0;->p:Z

    if-nez v2, :cond_4

    .line 312876
    :cond_3
    const-string v2, "sid"

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 312877
    :cond_4
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result-object p1

    .line 312878
    const v0, -0x3bee545

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_1
    move-exception v0

    const v1, 0x56c95d1f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Z)Landroid/net/Uri;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 312856
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 312857
    const/4 v0, 0x0

    .line 312858
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, LX/1m0;->a(Landroid/net/Uri;Ljava/lang/String;Z)Landroid/net/Uri;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 312859
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Landroid/net/Uri;JJLjava/lang/String;ILX/3Di;)V
    .locals 10

    .prologue
    .line 312854
    invoke-static {p0}, LX/1m0;->e(LX/1m0;)LX/7Po;

    move-result-object v0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, LX/7Po;->a(Landroid/net/Uri;JJLjava/lang/String;ILX/3Di;)V

    .line 312855
    return-void
.end method

.method public final b()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 312851
    iget-object v0, p0, LX/1m0;->r:LX/1m4;

    .line 312852
    iget-object p0, v0, LX/1m4;->b:Landroid/net/Uri;

    move-object v0, p0

    .line 312853
    return-object v0
.end method

.method public final c()LX/04m;
    .locals 1

    .prologue
    .line 312850
    iget-object v0, p0, LX/1m0;->o:LX/1m5;

    return-object v0
.end method

.method public final declared-synchronized init()V
    .locals 1

    .prologue
    .line 312846
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1m0;->b:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/1m0;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312847
    invoke-direct {p0}, LX/1m0;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312848
    :cond_0
    monitor-exit p0

    return-void

    .line 312849
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
