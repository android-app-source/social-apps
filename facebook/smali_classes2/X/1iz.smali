.class public LX/1iz;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0eX;

.field private static b:I

.field private static c:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 299498
    new-instance v0, LX/0eX;

    invoke-direct {v0}, LX/0eX;-><init>()V

    sput-object v0, LX/1iz;->a:LX/0eX;

    .line 299499
    const/16 v0, 0x40

    sput v0, LX/1iz;->b:I

    .line 299500
    const/16 v0, 0x20

    new-array v0, v0, [I

    sput-object v0, LX/1iz;->c:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 299658
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(II)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 299652
    sget-object v0, LX/1iz;->c:[I

    array-length v0, v0

    if-lt p0, v0, :cond_0

    .line 299653
    sget-object v0, LX/1iz;->c:[I

    .line 299654
    array-length v1, v0

    mul-int/lit8 v1, v1, 0x2

    new-array v1, v1, [I

    sput-object v1, LX/1iz;->c:[I

    .line 299655
    sget-object v1, LX/1iz;->c:[I

    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 299656
    :cond_0
    sget-object v0, LX/1iz;->c:[I

    aput p1, v0, p0

    .line 299657
    add-int/lit8 v0, p0, 0x1

    return v0
.end method

.method private static a(LX/1iS;)I
    .locals 2
    .param p0    # LX/1iS;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299641
    if-nez p0, :cond_0

    .line 299642
    const/4 v0, 0x0

    .line 299643
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/1iz;->a:LX/0eX;

    .line 299644
    iget v1, p0, LX/1iS;->a:I

    move v1, v1

    .line 299645
    const/4 p0, 0x1

    invoke-virtual {v0, p0}, LX/0eX;->b(I)V

    .line 299646
    const/4 p0, 0x0

    .line 299647
    invoke-virtual {v0, p0, v1, p0}, LX/0eX;->b(III)V

    .line 299648
    invoke-virtual {v0}, LX/0eX;->c()I

    move-result p0

    .line 299649
    move p0, p0

    .line 299650
    move v0, p0

    .line 299651
    goto :goto_0
.end method

.method private static a(LX/1iT;)I
    .locals 5
    .param p0    # LX/1iT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299623
    if-nez p0, :cond_0

    .line 299624
    const/4 v0, 0x0

    .line 299625
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/1iz;->a:LX/0eX;

    .line 299626
    iget-boolean v1, p0, LX/1iT;->a:Z

    move v1, v1

    .line 299627
    sget-object v2, LX/1iz;->a:LX/0eX;

    .line 299628
    iget-object v3, p0, LX/1iT;->b:Ljava/lang/String;

    move-object v3, v3

    .line 299629
    invoke-virtual {v2, v3}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v2

    sget-object v3, LX/1iz;->a:LX/0eX;

    .line 299630
    iget-object v4, p0, LX/1iT;->c:Ljava/lang/String;

    move-object v4, v4

    .line 299631
    invoke-virtual {v3, v4}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v3

    .line 299632
    const/4 v4, 0x3

    invoke-virtual {v0, v4}, LX/0eX;->b(I)V

    .line 299633
    const/4 v4, 0x2

    const/4 p0, 0x0

    invoke-virtual {v0, v4, v3, p0}, LX/0eX;->c(III)V

    .line 299634
    const/4 v4, 0x1

    const/4 p0, 0x0

    invoke-virtual {v0, v4, v2, p0}, LX/0eX;->c(III)V

    .line 299635
    const/4 v4, 0x0

    .line 299636
    invoke-virtual {v0, v4, v1, v4}, LX/0eX;->a(IZZ)V

    .line 299637
    invoke-virtual {v0}, LX/0eX;->c()I

    move-result v4

    .line 299638
    move v4, v4

    .line 299639
    move v0, v4

    .line 299640
    goto :goto_0
.end method

.method private static a(LX/1v1;)I
    .locals 3
    .param p0    # LX/1v1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299610
    if-nez p0, :cond_0

    .line 299611
    const/4 v0, 0x0

    .line 299612
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/1iz;->a:LX/0eX;

    .line 299613
    iget v1, p0, LX/1v1;->a:I

    move v1, v1

    .line 299614
    iget-boolean v2, p0, LX/1v1;->b:Z

    move v2, v2

    .line 299615
    const/4 p0, 0x2

    invoke-virtual {v0, p0}, LX/0eX;->b(I)V

    .line 299616
    const/4 p0, 0x0

    .line 299617
    invoke-virtual {v0, p0, v1, p0}, LX/0eX;->b(III)V

    .line 299618
    const/4 p0, 0x1

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v2, v1}, LX/0eX;->a(IZZ)V

    .line 299619
    invoke-virtual {v0}, LX/0eX;->c()I

    move-result p0

    .line 299620
    move p0, p0

    .line 299621
    move v0, p0

    .line 299622
    goto :goto_0
.end method

.method private static a(LX/2uy;)I
    .locals 12
    .param p0    # LX/2uy;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299600
    if-nez p0, :cond_0

    .line 299601
    const/4 v0, 0x0

    .line 299602
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/1iz;->a:LX/0eX;

    .line 299603
    iget-wide v4, p0, LX/2uy;->a:J

    move-wide v2, v4

    .line 299604
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/0eX;->b(I)V

    .line 299605
    const/4 v6, 0x0

    const-wide/16 v9, 0x0

    move-object v5, v0

    move-wide v7, v2

    invoke-virtual/range {v5 .. v10}, LX/0eX;->a(IJJ)V

    .line 299606
    invoke-virtual {v0}, LX/0eX;->c()I

    move-result v4

    .line 299607
    move v4, v4

    .line 299608
    move v0, v4

    .line 299609
    goto :goto_0
.end method

.method private static a(LX/4mB;)I
    .locals 12
    .param p0    # LX/4mB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299590
    if-nez p0, :cond_0

    .line 299591
    const/4 v0, 0x0

    .line 299592
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/1iz;->a:LX/0eX;

    .line 299593
    iget-wide v4, p0, LX/4mB;->a:J

    move-wide v2, v4

    .line 299594
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/0eX;->b(I)V

    .line 299595
    const/4 v6, 0x0

    const-wide/16 v9, 0x0

    move-object v5, v0

    move-wide v7, v2

    invoke-virtual/range {v5 .. v10}, LX/0eX;->a(IJJ)V

    .line 299596
    invoke-virtual {v0}, LX/0eX;->c()I

    move-result v4

    .line 299597
    move v4, v4

    .line 299598
    move v0, v4

    .line 299599
    goto :goto_0
.end method

.method private static a(LX/4mC;)I
    .locals 12
    .param p0    # LX/4mC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299659
    if-nez p0, :cond_0

    .line 299660
    const/4 v0, 0x0

    .line 299661
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/1iz;->a:LX/0eX;

    .line 299662
    iget-wide v4, p0, LX/4mC;->a:J

    move-wide v2, v4

    .line 299663
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/0eX;->b(I)V

    .line 299664
    const/4 v6, 0x0

    const-wide/16 v9, 0x0

    move-object v5, v0

    move-wide v7, v2

    invoke-virtual/range {v5 .. v10}, LX/0eX;->a(IJJ)V

    .line 299665
    invoke-virtual {v0}, LX/0eX;->c()I

    move-result v4

    .line 299666
    move v4, v4

    .line 299667
    move v0, v4

    .line 299668
    goto :goto_0
.end method

.method private static a(LX/4mD;)I
    .locals 4
    .param p0    # LX/4mD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 299570
    if-nez p0, :cond_0

    .line 299571
    :goto_0
    return v0

    .line 299572
    :cond_0
    iget-object v1, p0, LX/4mD;->a:Ljava/util/List;

    move-object v1, v1

    .line 299573
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 299574
    sget-object v3, LX/1iz;->a:LX/0eX;

    invoke-virtual {v3, v0}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v0

    invoke-static {v1, v0}, LX/1iz;->a(II)I

    move-result v0

    move v1, v0

    .line 299575
    goto :goto_1

    .line 299576
    :cond_1
    sget-object v0, LX/1iz;->a:LX/0eX;

    const/4 v2, 0x4

    .line 299577
    invoke-virtual {v0, v2, v1, v2}, LX/0eX;->a(III)V

    .line 299578
    add-int/lit8 v0, v1, -0x1

    :goto_2
    if-ltz v0, :cond_2

    .line 299579
    sget-object v1, LX/1iz;->a:LX/0eX;

    sget-object v2, LX/1iz;->c:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, LX/0eX;->a(I)V

    .line 299580
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 299581
    :cond_2
    sget-object v0, LX/1iz;->a:LX/0eX;

    invoke-virtual {v0}, LX/0eX;->b()I

    move-result v0

    .line 299582
    sget-object v1, LX/1iz;->a:LX/0eX;

    .line 299583
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/0eX;->b(I)V

    .line 299584
    const/4 v2, 0x0

    .line 299585
    invoke-virtual {v1, v2, v0, v2}, LX/0eX;->c(III)V

    .line 299586
    invoke-virtual {v1}, LX/0eX;->c()I

    move-result v2

    .line 299587
    move v2, v2

    .line 299588
    move v0, v2

    .line 299589
    goto :goto_0
.end method

.method private static a(Lcom/facebook/tigon/iface/AndroidRedirectRequestInfo;)I
    .locals 2
    .param p0    # Lcom/facebook/tigon/iface/AndroidRedirectRequestInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299560
    if-nez p0, :cond_0

    .line 299561
    const/4 v0, 0x0

    .line 299562
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/1iz;->a:LX/0eX;

    invoke-interface {p0}, Lcom/facebook/tigon/iface/AndroidRedirectRequestInfo;->a()Z

    move-result v1

    .line 299563
    const/4 p0, 0x1

    invoke-virtual {v0, p0}, LX/0eX;->b(I)V

    .line 299564
    const/4 p0, 0x0

    .line 299565
    invoke-virtual {v0, p0, v1, p0}, LX/0eX;->a(IZZ)V

    .line 299566
    invoke-virtual {v0}, LX/0eX;->c()I

    move-result p0

    .line 299567
    move p0, p0

    .line 299568
    move v0, p0

    .line 299569
    goto :goto_0
.end method

.method private static a(Lcom/facebook/tigon/iface/FacebookLoggingRequestInfo;)I
    .locals 4
    .param p0    # Lcom/facebook/tigon/iface/FacebookLoggingRequestInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299549
    if-nez p0, :cond_0

    .line 299550
    const/4 v0, 0x0

    .line 299551
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/1iz;->a:LX/0eX;

    sget-object v1, LX/1iz;->a:LX/0eX;

    invoke-interface {p0}, Lcom/facebook/tigon/iface/FacebookLoggingRequestInfo;->logName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v1

    sget-object v2, LX/1iz;->a:LX/0eX;

    invoke-interface {p0}, Lcom/facebook/tigon/iface/FacebookLoggingRequestInfo;->logNamespace()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v2

    .line 299552
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, LX/0eX;->b(I)V

    .line 299553
    const/4 v3, 0x1

    const/4 p0, 0x0

    invoke-virtual {v0, v3, v2, p0}, LX/0eX;->c(III)V

    .line 299554
    const/4 v3, 0x0

    .line 299555
    invoke-virtual {v0, v3, v1, v3}, LX/0eX;->c(III)V

    .line 299556
    invoke-virtual {v0}, LX/0eX;->c()I

    move-result v3

    .line 299557
    move v3, v3

    .line 299558
    move v0, v3

    .line 299559
    goto :goto_0
.end method

.method private static a(Lcom/facebook/tigon/iface/TigonRetrierRequestInfo;)I
    .locals 2
    .param p0    # Lcom/facebook/tigon/iface/TigonRetrierRequestInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299539
    if-nez p0, :cond_0

    .line 299540
    const/4 v0, 0x0

    .line 299541
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/1iz;->a:LX/0eX;

    invoke-interface {p0}, Lcom/facebook/tigon/iface/TigonRetrierRequestInfo;->a()Z

    move-result v1

    .line 299542
    const/4 p0, 0x1

    invoke-virtual {v0, p0}, LX/0eX;->b(I)V

    .line 299543
    const/4 p0, 0x0

    .line 299544
    invoke-virtual {v0, p0, v1, p0}, LX/0eX;->a(IZZ)V

    .line 299545
    invoke-virtual {v0}, LX/0eX;->c()I

    move-result p0

    .line 299546
    move p0, p0

    .line 299547
    move v0, p0

    .line 299548
    goto :goto_0
.end method

.method private static a()Ljava/nio/ByteBuffer;
    .locals 3

    .prologue
    .line 299530
    sget-object v0, LX/1iz;->a:LX/0eX;

    .line 299531
    iget-object v1, v0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    move-object v0, v1

    .line 299532
    sget v1, LX/1iz;->b:I

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    sput v1, LX/1iz;->b:I

    .line 299533
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v1

    if-nez v1, :cond_0

    .line 299534
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 299535
    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 299536
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 299537
    move-object v0, v1

    .line 299538
    :cond_0
    return-object v0
.end method

.method public static declared-synchronized a(Lcom/facebook/tigon/iface/TigonRequest;)Ljava/nio/ByteBuffer;
    .locals 19

    .prologue
    .line 299515
    const-class v18, LX/1iz;

    monitor-enter v18

    :try_start_0
    sget-object v2, LX/1iz;->a:LX/0eX;

    sget v3, LX/1iz;->b:I

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0eX;->a(Ljava/nio/ByteBuffer;)LX/0eX;

    .line 299516
    const/4 v2, 0x0

    .line 299517
    invoke-interface/range {p0 .. p0}, Lcom/facebook/tigon/iface/TigonRequest;->c()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v4, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 299518
    sget-object v6, LX/1iz;->a:LX/0eX;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v6, v3}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v3

    invoke-static {v4, v3}, LX/1iz;->a(II)I

    move-result v3

    .line 299519
    sget-object v4, LX/1iz;->a:LX/0eX;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v2

    invoke-static {v3, v2}, LX/1iz;->a(II)I

    move-result v2

    move v4, v2

    .line 299520
    goto :goto_0

    .line 299521
    :cond_0
    sget-object v2, LX/1iz;->a:LX/0eX;

    invoke-static {v2, v4}, LX/1j0;->a(LX/0eX;I)V

    .line 299522
    add-int/lit8 v2, v4, -0x1

    :goto_1
    if-ltz v2, :cond_1

    .line 299523
    sget-object v3, LX/1iz;->a:LX/0eX;

    sget-object v4, LX/1iz;->c:[I

    aget v4, v4, v2

    invoke-virtual {v3, v4}, LX/0eX;->a(I)V

    .line 299524
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 299525
    :cond_1
    sget-object v2, LX/1iz;->a:LX/0eX;

    invoke-virtual {v2}, LX/0eX;->b()I

    move-result v5

    .line 299526
    sget-object v2, LX/1iz;->a:LX/0eX;

    sget-object v3, LX/1iz;->a:LX/0eX;

    invoke-interface/range {p0 .. p0}, Lcom/facebook/tigon/iface/TigonRequest;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v3

    sget-object v4, LX/1iz;->a:LX/0eX;

    invoke-interface/range {p0 .. p0}, Lcom/facebook/tigon/iface/TigonRequest;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v4

    invoke-interface/range {p0 .. p0}, Lcom/facebook/tigon/iface/TigonRequest;->d()LX/1iO;

    move-result-object v6

    iget v6, v6, LX/1iO;->a:I

    invoke-interface/range {p0 .. p0}, Lcom/facebook/tigon/iface/TigonRequest;->d()LX/1iO;

    move-result-object v7

    iget v7, v7, LX/1iO;->b:I

    sget-object v8, LX/1iP;->b:LX/1he;

    move-object/from16 v0, p0

    invoke-interface {v0, v8}, Lcom/facebook/tigon/iface/TigonRequest;->a(LX/1he;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/tigon/iface/FacebookLoggingRequestInfo;

    invoke-static {v8}, LX/1iz;->a(Lcom/facebook/tigon/iface/FacebookLoggingRequestInfo;)I

    move-result v8

    sget-object v9, LX/1iP;->c:LX/1he;

    move-object/from16 v0, p0

    invoke-interface {v0, v9}, Lcom/facebook/tigon/iface/TigonRequest;->a(LX/1he;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/2uy;

    invoke-static {v9}, LX/1iz;->a(LX/2uy;)I

    move-result v9

    sget-object v10, LX/1iP;->a:LX/1he;

    move-object/from16 v0, p0

    invoke-interface {v0, v10}, Lcom/facebook/tigon/iface/TigonRequest;->a(LX/1he;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/4mB;

    invoke-static {v10}, LX/1iz;->a(LX/4mB;)I

    move-result v10

    sget-object v11, LX/1iP;->g:LX/1he;

    move-object/from16 v0, p0

    invoke-interface {v0, v11}, Lcom/facebook/tigon/iface/TigonRequest;->a(LX/1he;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LX/4mC;

    invoke-static {v11}, LX/1iz;->a(LX/4mC;)I

    move-result v11

    sget-object v12, LX/1iP;->h:LX/1he;

    move-object/from16 v0, p0

    invoke-interface {v0, v12}, Lcom/facebook/tigon/iface/TigonRequest;->a(LX/1he;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/facebook/tigon/iface/TigonRetrierRequestInfo;

    invoke-static {v12}, LX/1iz;->a(Lcom/facebook/tigon/iface/TigonRetrierRequestInfo;)I

    move-result v12

    sget-object v13, LX/1iP;->e:LX/1he;

    move-object/from16 v0, p0

    invoke-interface {v0, v13}, Lcom/facebook/tigon/iface/TigonRequest;->a(LX/1he;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, LX/1v1;

    invoke-static {v13}, LX/1iz;->a(LX/1v1;)I

    move-result v13

    sget-object v14, LX/1iP;->i:LX/1he;

    move-object/from16 v0, p0

    invoke-interface {v0, v14}, Lcom/facebook/tigon/iface/TigonRequest;->a(LX/1he;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, LX/1iS;

    invoke-static {v14}, LX/1iz;->a(LX/1iS;)I

    move-result v14

    sget-object v15, LX/1iP;->j:LX/1he;

    move-object/from16 v0, p0

    invoke-interface {v0, v15}, Lcom/facebook/tigon/iface/TigonRequest;->a(LX/1he;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, LX/4mD;

    invoke-static {v15}, LX/1iz;->a(LX/4mD;)I

    move-result v15

    sget-object v16, LX/1iP;->d:LX/1he;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Lcom/facebook/tigon/iface/TigonRequest;->a(LX/1he;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, LX/1iT;

    invoke-static/range {v16 .. v16}, LX/1iz;->a(LX/1iT;)I

    move-result v16

    sget-object v17, LX/1iP;->f:LX/1he;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/facebook/tigon/iface/TigonRequest;->a(LX/1he;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/facebook/tigon/iface/AndroidRedirectRequestInfo;

    invoke-static/range {v17 .. v17}, LX/1iz;->a(Lcom/facebook/tigon/iface/AndroidRedirectRequestInfo;)I

    move-result v17

    invoke-static/range {v2 .. v17}, LX/1j0;->a(LX/0eX;IIIIIIIIIIIIIII)I

    move-result v2

    .line 299527
    sget-object v3, LX/1iz;->a:LX/0eX;

    invoke-static {v3, v2}, LX/1j0;->b(LX/0eX;I)V

    .line 299528
    invoke-static {}, LX/1iz;->a()Ljava/nio/ByteBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    monitor-exit v18

    return-object v2

    .line 299529
    :catchall_0
    move-exception v2

    monitor-exit v18

    throw v2
.end method

.method public static declared-synchronized a(Lcom/facebook/tigon/tigonapi/TigonError;)Ljava/nio/ByteBuffer;
    .locals 7

    .prologue
    .line 299501
    const-class v1, LX/1iz;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1iz;->a:LX/0eX;

    sget v2, LX/1iz;->b:I

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0eX;->a(Ljava/nio/ByteBuffer;)LX/0eX;

    .line 299502
    sget-object v0, LX/1iz;->a:LX/0eX;

    iget v2, p0, Lcom/facebook/tigon/tigonapi/TigonError;->a:I

    sget-object v3, LX/1iz;->a:LX/0eX;

    iget-object v4, p0, Lcom/facebook/tigon/tigonapi/TigonError;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v3

    iget v4, p0, Lcom/facebook/tigon/tigonapi/TigonError;->c:I

    sget-object v5, LX/1iz;->a:LX/0eX;

    iget-object v6, p0, Lcom/facebook/tigon/tigonapi/TigonError;->d:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v5

    .line 299503
    const/4 v6, 0x4

    invoke-virtual {v0, v6}, LX/0eX;->b(I)V

    .line 299504
    const/4 v6, 0x3

    const/4 p0, 0x0

    invoke-virtual {v0, v6, v5, p0}, LX/0eX;->c(III)V

    .line 299505
    const/4 v6, 0x2

    const/4 p0, 0x0

    invoke-virtual {v0, v6, v4, p0}, LX/0eX;->b(III)V

    .line 299506
    const/4 v6, 0x1

    const/4 p0, 0x0

    invoke-virtual {v0, v6, v3, p0}, LX/0eX;->c(III)V

    .line 299507
    const/4 v6, 0x0

    .line 299508
    invoke-virtual {v0, v6, v2, v6}, LX/0eX;->b(III)V

    .line 299509
    invoke-virtual {v0}, LX/0eX;->c()I

    move-result v6

    .line 299510
    move v6, v6

    .line 299511
    move v0, v6

    .line 299512
    sget-object v2, LX/1iz;->a:LX/0eX;

    invoke-virtual {v2, v0}, LX/0eX;->c(I)V

    .line 299513
    invoke-static {}, LX/1iz;->a()Ljava/nio/ByteBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 299514
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
