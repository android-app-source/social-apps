.class public final LX/1UA;
.super LX/1Rm;
.source ""


# instance fields
.field public final synthetic a:LX/1T1;

.field private final b:LX/1T1;


# direct methods
.method public constructor <init>(LX/1T1;LX/1T1;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 255565
    iput-object p1, p0, LX/1UA;->a:LX/1T1;

    .line 255566
    invoke-direct {p0, p3}, LX/1Rm;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 255567
    iput-object p2, p0, LX/1UA;->b:LX/1T1;

    .line 255568
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 7

    .prologue
    const/16 v5, 0x28

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 255569
    iget-object v0, p0, LX/1UA;->a:LX/1T1;

    iget-boolean v0, v0, LX/1T1;->i:Z

    if-eqz v0, :cond_0

    .line 255570
    iget-object v0, p0, LX/1UA;->a:LX/1T1;

    .line 255571
    iget-object v3, v0, LX/1T1;->d:LX/0g8;

    invoke-interface {v3}, LX/0g8;->q()I

    move-result v3

    .line 255572
    iget-object v4, v0, LX/1T1;->d:LX/0g8;

    invoke-interface {v4}, LX/0g8;->r()I

    move-result v4

    .line 255573
    iget-object v6, v0, LX/1T1;->d:LX/0g8;

    invoke-interface {v6}, LX/0g8;->B()Z

    move-result v6

    if-eqz v6, :cond_0

    if-ltz v3, :cond_0

    if-ltz v4, :cond_0

    if-le v3, v4, :cond_b

    .line 255574
    :cond_0
    :goto_0
    iget-object v0, p0, LX/1UA;->a:LX/1T1;

    iget-boolean v0, v0, LX/1T1;->i:Z

    if-eqz v0, :cond_2

    .line 255575
    invoke-virtual {p0}, LX/1Rm;->b()V

    .line 255576
    iget-object v0, p0, LX/1UA;->a:LX/1T1;

    iget-object v0, v0, LX/1T1;->d:LX/0g8;

    invoke-interface {v0}, LX/0g8;->B()Z

    move-result v0

    if-nez v0, :cond_1

    .line 255577
    iget-object v0, p0, LX/1UA;->a:LX/1T1;

    .line 255578
    iget-object v1, v0, LX/1T1;->d:LX/0g8;

    invoke-interface {v1}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, v0, LX/1T1;->j:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 255579
    :cond_1
    :goto_1
    return-void

    .line 255580
    :cond_2
    iget-object v0, p0, LX/1UA;->a:LX/1T1;

    iget v0, v0, LX/1T1;->f:I

    if-ltz v0, :cond_5

    move v0, v1

    .line 255581
    :goto_2
    iget-object v3, p0, LX/1UA;->a:LX/1T1;

    iget v3, v3, LX/1T1;->g:I

    iget-object v4, p0, LX/1UA;->a:LX/1T1;

    iget-object v4, v4, LX/1T1;->b:LX/1R2;

    invoke-interface {v4}, LX/1R2;->b()I

    move-result v4

    if-ge v3, v4, :cond_6

    move v3, v1

    .line 255582
    :goto_3
    if-eqz v0, :cond_8

    iget-object v0, p0, LX/1UA;->a:LX/1T1;

    iget v0, v0, LX/1T1;->a:I

    if-eq v0, v1, :cond_3

    if-nez v3, :cond_8

    .line 255583
    :cond_3
    iget-object v0, p0, LX/1UA;->a:LX/1T1;

    .line 255584
    iput v2, v0, LX/1T1;->a:I

    .line 255585
    iget-object v0, p0, LX/1UA;->a:LX/1T1;

    iget v0, v0, LX/1T1;->f:I

    iget-object v1, p0, LX/1UA;->a:LX/1T1;

    invoke-static {v1}, LX/1T1;->i$redex0(LX/1T1;)I

    move-result v1

    if-ge v0, v1, :cond_4

    iget-object v0, p0, LX/1UA;->a:LX/1T1;

    iget v0, v0, LX/1T1;->f:I

    if-ge v0, v5, :cond_7

    .line 255586
    :cond_4
    iget-object v0, p0, LX/1UA;->a:LX/1T1;

    iget-object v1, p0, LX/1UA;->a:LX/1T1;

    iget v1, v1, LX/1T1;->a:I

    invoke-static {v0, v1}, LX/1T1;->a(LX/1T1;I)V

    goto :goto_1

    :cond_5
    move v0, v2

    .line 255587
    goto :goto_2

    :cond_6
    move v3, v2

    .line 255588
    goto :goto_3

    .line 255589
    :cond_7
    iget-object v0, p0, LX/1UA;->a:LX/1T1;

    iget-object v1, p0, LX/1UA;->a:LX/1T1;

    iget v1, v1, LX/1T1;->a:I

    invoke-static {v0, v1}, LX/1T1;->b$redex0(LX/1T1;I)V

    goto :goto_1

    .line 255590
    :cond_8
    if-eqz v3, :cond_1

    .line 255591
    iget-object v0, p0, LX/1UA;->a:LX/1T1;

    .line 255592
    iput v1, v0, LX/1T1;->a:I

    .line 255593
    iget-object v0, p0, LX/1UA;->a:LX/1T1;

    iget v0, v0, LX/1T1;->g:I

    iget-object v1, p0, LX/1UA;->a:LX/1T1;

    invoke-static {v1}, LX/1T1;->h(LX/1T1;)I

    move-result v1

    if-le v0, v1, :cond_9

    iget-object v0, p0, LX/1UA;->a:LX/1T1;

    iget v0, v0, LX/1T1;->g:I

    if-ge v0, v5, :cond_a

    .line 255594
    :cond_9
    iget-object v0, p0, LX/1UA;->a:LX/1T1;

    iget-object v1, p0, LX/1UA;->a:LX/1T1;

    iget v1, v1, LX/1T1;->a:I

    invoke-static {v0, v1}, LX/1T1;->a(LX/1T1;I)V

    goto :goto_1

    .line 255595
    :cond_a
    iget-object v0, p0, LX/1UA;->a:LX/1T1;

    iget-object v1, p0, LX/1UA;->a:LX/1T1;

    iget v1, v1, LX/1T1;->a:I

    invoke-static {v0, v1}, LX/1T1;->b$redex0(LX/1T1;I)V

    goto :goto_1

    .line 255596
    :cond_b
    const/4 v6, 0x0

    iput-boolean v6, v0, LX/1T1;->i:Z

    .line 255597
    add-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    .line 255598
    iput v3, v0, LX/1T1;->h:I

    .line 255599
    iput v3, v0, LX/1T1;->f:I

    .line 255600
    add-int/lit8 v3, v3, 0x1

    iput v3, v0, LX/1T1;->g:I

    .line 255601
    const/4 v3, 0x1

    iput v3, v0, LX/1T1;->a:I

    goto/16 :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 255602
    iget-object v0, p0, LX/1UA;->b:LX/1T1;

    .line 255603
    iget-boolean v1, v0, LX/1T1;->i:Z

    if-nez v1, :cond_0

    iget v1, v0, LX/1T1;->f:I

    if-gez v1, :cond_0

    iget v1, v0, LX/1T1;->g:I

    iget-object p0, v0, LX/1T1;->b:LX/1R2;

    invoke-interface {p0}, LX/1R2;->b()I

    move-result p0

    if-ge v1, p0, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 255604
    return v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
