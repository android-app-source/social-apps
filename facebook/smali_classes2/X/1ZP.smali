.class public LX/1ZP;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field public static final f:Ljava/lang/String;

.field public static final g:Ljava/lang/String;

.field public static final h:Landroid/content/pm/Signature;

.field public static final i:Landroid/content/pm/Signature;

.field public static final j:Landroid/content/pm/Signature;

.field public static final k:Ljava/lang/String;

.field public static final l:Landroid/content/pm/Signature;

.field public static final m:Landroid/content/pm/Signature;

.field public static final n:Landroid/content/pm/Signature;

.field public static final o:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 274685
    sget-boolean v0, LX/1ZQ;->a:Z

    move v0, v0

    .line 274686
    if-eqz v0, :cond_0

    const-string v0, "com.facebook.appmanager.dev"

    :goto_0
    sput-object v0, LX/1ZP;->a:Ljava/lang/String;

    .line 274687
    sget-boolean v0, LX/1ZQ;->a:Z

    move v0, v0

    .line 274688
    if-eqz v0, :cond_1

    const-string v0, "com.facebook.appmanager.dev.ACCESS"

    :goto_1
    sput-object v0, LX/1ZP;->b:Ljava/lang/String;

    .line 274689
    sget-boolean v0, LX/1ZQ;->a:Z

    move v0, v0

    .line 274690
    if-eqz v0, :cond_2

    const-string v0, "com.facebook.system.dev"

    :goto_2
    sput-object v0, LX/1ZP;->c:Ljava/lang/String;

    .line 274691
    sget-boolean v0, LX/1ZQ;->a:Z

    move v0, v0

    .line 274692
    if-eqz v0, :cond_3

    const-string v0, "com.facebook.system.dev.ACCESS"

    :goto_3
    sput-object v0, LX/1ZP;->d:Ljava/lang/String;

    .line 274693
    sget-boolean v0, LX/1ZQ;->a:Z

    move v0, v0

    .line 274694
    if-eqz v0, :cond_4

    const-string v0, "com.facebook.services.dev"

    :goto_4
    sput-object v0, LX/1ZP;->e:Ljava/lang/String;

    .line 274695
    sget-boolean v0, LX/1ZQ;->a:Z

    move v0, v0

    .line 274696
    if-eqz v0, :cond_5

    const-string v0, "com.facebook.services.dev.ACCESS"

    :goto_5
    sput-object v0, LX/1ZP;->f:Ljava/lang/String;

    .line 274697
    sget-boolean v0, LX/1ZQ;->a:Z

    move v0, v0

    .line 274698
    if-eqz v0, :cond_6

    const-string v0, "com.facebook.permission.debug.FB_APP_COMMUNICATION"

    :goto_6
    sput-object v0, LX/1ZP;->g:Ljava/lang/String;

    .line 274699
    sget-object v0, LX/1ZR;->b:Landroid/content/pm/Signature;

    sput-object v0, LX/1ZP;->h:Landroid/content/pm/Signature;

    .line 274700
    sget-object v0, LX/1ZR;->a:Landroid/content/pm/Signature;

    sput-object v0, LX/1ZP;->i:Landroid/content/pm/Signature;

    .line 274701
    sget-boolean v0, LX/1ZQ;->a:Z

    move v0, v0

    .line 274702
    if-eqz v0, :cond_7

    sget-object v0, LX/1ZP;->h:Landroid/content/pm/Signature;

    :goto_7
    sput-object v0, LX/1ZP;->j:Landroid/content/pm/Signature;

    .line 274703
    sget-boolean v0, LX/1ZQ;->a:Z

    move v0, v0

    .line 274704
    if-eqz v0, :cond_8

    const-string v0, "7XE60X540nq3JXIiFpcVSgM8diY"

    :goto_8
    sput-object v0, LX/1ZP;->k:Ljava/lang/String;

    .line 274705
    sget-object v0, LX/1ZR;->d:Landroid/content/pm/Signature;

    sput-object v0, LX/1ZP;->l:Landroid/content/pm/Signature;

    .line 274706
    sget-object v0, LX/1ZR;->c:Landroid/content/pm/Signature;

    sput-object v0, LX/1ZP;->m:Landroid/content/pm/Signature;

    .line 274707
    sget-boolean v0, LX/1ZQ;->a:Z

    move v0, v0

    .line 274708
    if-eqz v0, :cond_9

    sget-object v0, LX/1ZP;->l:Landroid/content/pm/Signature;

    :goto_9
    sput-object v0, LX/1ZP;->n:Landroid/content/pm/Signature;

    .line 274709
    sget-boolean v0, LX/1ZQ;->a:Z

    move v0, v0

    .line 274710
    if-eqz v0, :cond_a

    const-string v0, "Xo8WBi6jzSxKDVR4drqm84yr9iU"

    :goto_a
    sput-object v0, LX/1ZP;->o:Ljava/lang/String;

    return-void

    .line 274711
    :cond_0
    const-string v0, "com.facebook.appmanager"

    goto :goto_0

    .line 274712
    :cond_1
    const-string v0, "com.facebook.appmanager.ACCESS"

    goto :goto_1

    .line 274713
    :cond_2
    const-string v0, "com.facebook.system"

    goto :goto_2

    .line 274714
    :cond_3
    const-string v0, "com.facebook.system.ACCESS"

    goto :goto_3

    .line 274715
    :cond_4
    const-string v0, "com.facebook.services"

    goto :goto_4

    .line 274716
    :cond_5
    const-string v0, "com.facebook.services.ACCESS"

    goto :goto_5

    .line 274717
    :cond_6
    const-string v0, "com.facebook.permission.prod.FB_APP_COMMUNICATION"

    goto :goto_6

    .line 274718
    :cond_7
    sget-object v0, LX/1ZP;->i:Landroid/content/pm/Signature;

    goto :goto_7

    .line 274719
    :cond_8
    const-string v0, "e6fv6XFRr-tXEDJmsSANhagF19Y"

    goto :goto_8

    .line 274720
    :cond_9
    sget-object v0, LX/1ZP;->m:Landroid/content/pm/Signature;

    goto :goto_9

    .line 274721
    :cond_a
    const-string v0, "ijxLJi1yGs1JpL-X1SExmchvork"

    goto :goto_a
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 274722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 274723
    return-void
.end method
