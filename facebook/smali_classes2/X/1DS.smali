.class public LX/1DS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1DV;

.field public final b:LX/1DW;

.field private final c:LX/1DX;

.field public final d:LX/0Uh;

.field private final e:LX/1DU;

.field public final f:LX/1DT;

.field public final g:LX/0ad;

.field public final h:LX/0pJ;

.field public final i:LX/1Da;

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1ji;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/1DY;

.field public final l:LX/1Du;

.field public final m:LX/1Dv;

.field public final n:LX/1Dw;

.field public final o:LX/1Dx;

.field public final p:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Qx;",
            ">;"
        }
    .end annotation
.end field

.field public final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Jp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/1DT;LX/1DU;LX/0ad;LX/0pJ;LX/1DV;LX/1DW;LX/1DX;LX/0Uh;LX/1DY;LX/1Da;LX/0Ot;LX/1Du;LX/1Dv;LX/1Dw;LX/1Dx;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1DT;",
            "LX/1DU;",
            "LX/0ad;",
            "LX/0pJ;",
            "LX/1DV;",
            "LX/1DW;",
            "LX/1DX;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/1DY;",
            "LX/1Da;",
            "LX/0Ot",
            "<",
            "LX/1ji;",
            ">;",
            "LX/1Du;",
            "LX/1Dv;",
            "LX/1Dw;",
            "LX/1Dx;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/1Qx;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Jp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 217631
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217632
    iput-object p2, p0, LX/1DS;->e:LX/1DU;

    .line 217633
    iput-object p1, p0, LX/1DS;->f:LX/1DT;

    .line 217634
    iput-object p3, p0, LX/1DS;->g:LX/0ad;

    .line 217635
    iput-object p4, p0, LX/1DS;->h:LX/0pJ;

    .line 217636
    iput-object p5, p0, LX/1DS;->a:LX/1DV;

    .line 217637
    iput-object p6, p0, LX/1DS;->b:LX/1DW;

    .line 217638
    iput-object p7, p0, LX/1DS;->c:LX/1DX;

    .line 217639
    iput-object p8, p0, LX/1DS;->d:LX/0Uh;

    .line 217640
    iput-object p9, p0, LX/1DS;->k:LX/1DY;

    .line 217641
    iput-object p10, p0, LX/1DS;->i:LX/1Da;

    .line 217642
    iput-object p11, p0, LX/1DS;->j:LX/0Ot;

    .line 217643
    iput-object p12, p0, LX/1DS;->l:LX/1Du;

    .line 217644
    iput-object p13, p0, LX/1DS;->m:LX/1Dv;

    .line 217645
    iput-object p14, p0, LX/1DS;->n:LX/1Dw;

    .line 217646
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1DS;->o:LX/1Dx;

    .line 217647
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1DS;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 217648
    move-object/from16 v0, p17

    iput-object v0, p0, LX/1DS;->q:LX/0Ot;

    .line 217649
    move-object/from16 v0, p18

    iput-object v0, p0, LX/1DS;->r:LX/0Ot;

    .line 217650
    return-void
.end method

.method public static a(LX/0QB;)LX/1DS;
    .locals 1

    .prologue
    .line 217651
    invoke-static {p0}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1DS;
    .locals 21

    .prologue
    .line 217652
    new-instance v2, LX/1DS;

    const-class v3, LX/1DT;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1DT;

    const-class v4, LX/1DU;

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1DU;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0pJ;->a(LX/0QB;)LX/0pJ;

    move-result-object v6

    check-cast v6, LX/0pJ;

    const-class v7, LX/1DV;

    move-object/from16 v0, p0

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/1DV;

    const-class v8, LX/1DW;

    move-object/from16 v0, p0

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/1DW;

    const-class v9, LX/1DX;

    move-object/from16 v0, p0

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/1DX;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/1DY;->a(LX/0QB;)LX/1DY;

    move-result-object v11

    check-cast v11, LX/1DY;

    invoke-static/range {p0 .. p0}, LX/1Da;->a(LX/0QB;)LX/1Da;

    move-result-object v12

    check-cast v12, LX/1Da;

    const/16 v13, 0x6bf

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const-class v14, LX/1Du;

    move-object/from16 v0, p0

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/1Du;

    const-class v15, LX/1Dv;

    move-object/from16 v0, p0

    invoke-interface {v0, v15}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/1Dv;

    const-class v16, LX/1Dw;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/1Dw;

    const-class v17, LX/1Dx;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/1Dx;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v18

    check-cast v18, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v19, 0x6cb

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x796

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    invoke-direct/range {v2 .. v20}, LX/1DS;-><init>(LX/1DT;LX/1DU;LX/0ad;LX/0pJ;LX/1DV;LX/1DW;LX/1DX;LX/0Uh;LX/1DY;LX/1Da;LX/0Ot;LX/1Du;LX/1Dv;LX/1Dw;LX/1Dx;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;)V

    .line 217653
    return-object v2
.end method


# virtual methods
.method public final a(LX/0Ot;LX/0g1;)LX/1Ql;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1PW;",
            ">(",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<**-TE;>;>;",
            "LX/0g1;",
            ")",
            "LX/1Ql",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 217654
    new-instance v0, LX/1Ql;

    invoke-direct {v0, p0, p1, p2}, LX/1Ql;-><init>(LX/1DS;LX/0Ot;LX/0g1;)V

    return-object v0
.end method
