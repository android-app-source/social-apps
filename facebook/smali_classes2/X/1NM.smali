.class public final LX/1NM;
.super LX/1NN;
.source ""


# instance fields
.field private final a:LX/0qq;

.field private final b:LX/0SG;

.field public final c:LX/1K8;


# direct methods
.method public constructor <init>(LX/0SG;LX/1K8;LX/0qq;)V
    .locals 0
    .param p3    # LX/0qq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 237358
    invoke-direct {p0}, LX/1NN;-><init>()V

    .line 237359
    iput-object p1, p0, LX/1NM;->b:LX/0SG;

    .line 237360
    iput-object p2, p0, LX/1NM;->c:LX/1K8;

    .line 237361
    iput-object p3, p0, LX/1NM;->a:LX/0qq;

    .line 237362
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/HideableUnit;LX/1Nd;)V
    .locals 5

    .prologue
    .line 237363
    iget-object v0, p2, LX/1Nd;->d:Lcom/facebook/graphql/enums/StoryVisibility;

    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->CONTRACTING:Lcom/facebook/graphql/enums/StoryVisibility;

    if-ne v0, v1, :cond_0

    .line 237364
    iget-object v0, p0, LX/1NM;->c:LX/1K8;

    new-instance v1, LX/1VZ;

    invoke-direct {v1, p1}, LX/1VZ;-><init>(Lcom/facebook/graphql/model/HideableUnit;)V

    invoke-virtual {v0, v1, p1}, LX/1K8;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Va;

    .line 237365
    iget v1, v0, LX/1Va;->a:I

    move v0, v1

    .line 237366
    move v0, v0

    .line 237367
    :goto_0
    invoke-static {p1}, LX/6X8;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/6X8;

    move-result-object v1

    iget-object v2, p0, LX/1NM;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v4, p2, LX/1Nd;->d:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v1, v2, v3, v4, v0}, LX/6X8;->a(JLcom/facebook/graphql/enums/StoryVisibility;I)LX/6X8;

    move-result-object v0

    invoke-virtual {v0}, LX/6X8;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/HideableUnit;

    .line 237368
    iget-object v1, p0, LX/1NM;->a:LX/0qq;

    invoke-virtual {v1, v0}, LX/0qq;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 237369
    return-void

    .line 237370
    :cond_0
    iget v0, p2, LX/1Nd;->e:I

    goto :goto_0
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 237371
    check-cast p1, LX/1Nd;

    .line 237372
    iget-object v0, p1, LX/1Nd;->a:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 237373
    iget-object v0, p1, LX/1Nd;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 237374
    iget-object v0, p0, LX/1NM;->a:LX/0qq;

    iget-object v1, p1, LX/1Nd;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0qq;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 237375
    :goto_0
    if-eqz v0, :cond_0

    .line 237376
    invoke-direct {p0, v0, p1}, LX/1NM;->a(Lcom/facebook/graphql/model/HideableUnit;LX/1Nd;)V

    .line 237377
    :cond_0
    return-void

    .line 237378
    :cond_1
    iget-object v0, p0, LX/1NM;->a:LX/0qq;

    iget-object v1, p1, LX/1Nd;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0qq;->c(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    .line 237379
    :cond_2
    iget-object v0, p0, LX/1NM;->a:LX/0qq;

    iget-object v1, p1, LX/1Nd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0qq;->d(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 237380
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 237381
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 237382
    instance-of v2, v0, Lcom/facebook/graphql/model/HideableUnit;

    if-eqz v2, :cond_3

    .line 237383
    check-cast v0, Lcom/facebook/graphql/model/HideableUnit;

    invoke-direct {p0, v0, p1}, LX/1NM;->a(Lcom/facebook/graphql/model/HideableUnit;LX/1Nd;)V

    goto :goto_1
.end method
