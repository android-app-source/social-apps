.class public LX/1Cp;
.super LX/0qg;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final b:Ljava/lang/Object;


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 216947
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/1Cp;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 216944
    invoke-direct {p0}, LX/0qg;-><init>()V

    .line 216945
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Cp;->a:Ljava/lang/String;

    .line 216946
    return-void
.end method

.method public static a(LX/0QB;)LX/1Cp;
    .locals 7

    .prologue
    .line 216912
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 216913
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 216914
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 216915
    if-nez v1, :cond_0

    .line 216916
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 216917
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 216918
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 216919
    sget-object v1, LX/1Cp;->b:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 216920
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 216921
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 216922
    :cond_1
    if-nez v1, :cond_4

    .line 216923
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 216924
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 216925
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    .line 216926
    new-instance v0, LX/1Cp;

    invoke-direct {v0}, LX/1Cp;-><init>()V

    .line 216927
    move-object v1, v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 216928
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 216929
    if-nez v1, :cond_2

    .line 216930
    sget-object v0, LX/1Cp;->b:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cp;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 216931
    :goto_1
    if-eqz v0, :cond_3

    .line 216932
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 216933
    :goto_3
    check-cast v0, LX/1Cp;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 216934
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 216935
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 216936
    :catchall_1
    move-exception v0

    .line 216937
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 216938
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 216939
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 216940
    :cond_2
    :try_start_8
    sget-object v0, LX/1Cp;->b:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cp;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/0rH;)LX/0qi;
    .locals 1

    .prologue
    .line 216943
    iget-object v0, p0, LX/1Cp;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0qi;->a(Ljava/lang/String;)LX/0qi;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216942
    const-string v0, "dd_pinned_unit"

    return-object v0
.end method

.method public final b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0rH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 216941
    sget-object v0, LX/0rH;->NEWS_FEED:LX/0rH;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
