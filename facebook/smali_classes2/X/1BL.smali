.class public abstract LX/1BL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final a:LX/0vX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vX",
            "<",
            "Ljava/lang/String;",
            "LX/1fG;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 213019
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213020
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v0

    iput-object v0, p0, LX/1BL;->a:LX/0vX;

    .line 213021
    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;Landroid/net/Uri;)LX/1fG;
    .locals 3

    .prologue
    .line 213022
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1BL;->a:LX/0vX;

    invoke-interface {v0, p1}, LX/0vX;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 213023
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fG;

    .line 213024
    iget-object v2, v0, LX/1fG;->a:Landroid/net/Uri;

    move-object v2, v2

    .line 213025
    invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 213026
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 213027
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d(Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 212998
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1BL;->a:LX/0vX;

    invoke-interface {v0, p1}, LX/0vX;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 212999
    const/4 v1, 0x0

    .line 213000
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fG;

    .line 213001
    iget v3, v0, LX/1fG;->c:I

    move v0, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213002
    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    .line 213003
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 213004
    goto :goto_0

    .line 213005
    :cond_0
    monitor-exit p0

    return v1

    .line 213006
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 213007
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v1

    :goto_0
    const-string v3, "Invalid feed unit id"

    invoke-static {v2, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 213008
    iget-object v2, p0, LX/1BL;->a:LX/0vX;

    invoke-interface {v2, p1}, LX/0Xu;->f(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 213009
    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    move v2, v0

    .line 213010
    goto :goto_0

    .line 213011
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/1BL;->a:LX/0vX;

    invoke-interface {v0, p1}, LX/0vX;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 213012
    invoke-direct {p0, p1}, LX/1BL;->d(Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    .line 213013
    if-ne v2, v0, :cond_2

    .line 213014
    const/4 v0, 0x3

    goto :goto_1

    .line 213015
    :cond_2
    if-nez v2, :cond_3

    move v0, v1

    .line 213016
    goto :goto_1

    .line 213017
    :cond_3
    const/4 v0, 0x2

    goto :goto_1

    .line 213018
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract a(Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)LX/1fG;
.end method

.method public abstract a(LX/1bf;)LX/2xm;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public final declared-synchronized a(LX/1aZ;Ljava/lang/String;LX/1bf;)V
    .locals 4

    .prologue
    .line 212978
    monitor-enter p0

    :try_start_0
    const-string v1, "Controller cannot be null"

    invoke-static {p1, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212979
    instance-of v1, p1, LX/1bp;

    const-string v2, "Controller must be AbstractDraweeController"

    invoke-static {v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 212980
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Invalid feed unit id"

    invoke-static {v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 212981
    const-string v1, "Image request cannot be null"

    invoke-static {p3, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212982
    iget-object v0, p3, LX/1bf;->b:Landroid/net/Uri;

    move-object v2, v0

    .line 212983
    const-string v1, "Image uri cannot be null"

    invoke-static {v2, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212984
    invoke-static {v2}, LX/1H1;->g(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    .line 212985
    invoke-direct {p0, p2, v3}, LX/1BL;->a(Ljava/lang/String;Landroid/net/Uri;)LX/1fG;

    move-result-object v1

    .line 212986
    if-nez v1, :cond_2

    .line 212987
    invoke-virtual {p0, p2, v2, v3}, LX/1BL;->a(Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)LX/1fG;

    move-result-object v1

    move-object v2, v1

    .line 212988
    :goto_1
    move-object v0, p1

    check-cast v0, LX/1bp;

    move-object v1, v0

    .line 212989
    iget-object v0, v2, LX/1fG;->e:LX/1fH;

    move-object v3, v0

    .line 212990
    invoke-virtual {v1, v3}, LX/1bp;->a(LX/1Ai;)V

    .line 212991
    invoke-virtual {p0, p3}, LX/1BL;->a(LX/1bf;)LX/2xm;

    move-result-object v1

    .line 212992
    if-eqz v1, :cond_0

    .line 212993
    iput-object v1, v2, LX/1fG;->h:LX/2xm;

    .line 212994
    check-cast p1, LX/1bp;

    invoke-virtual {p1, v1}, LX/1bp;->a(LX/1Ai;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212995
    :cond_0
    monitor-exit p0

    return-void

    .line 212996
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 212997
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_2
    move-object v2, v1

    goto :goto_1
.end method

.method public final declared-synchronized b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 212974
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Invalid feed unit id"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 212975
    iget-object v0, p0, LX/1BL;->a:LX/0vX;

    invoke-interface {v0, p1}, LX/0vX;->b(Ljava/lang/Object;)Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212976
    monitor-exit p0

    return-void

    .line 212977
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(Ljava/lang/String;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/1fG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 212969
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Invalid feed unit id"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 212970
    iget-object v0, p0, LX/1BL;->a:LX/0vX;

    invoke-interface {v0, p1}, LX/0Xu;->f(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1BL;->a:LX/0vX;

    invoke-interface {v0, p1}, LX/0vX;->a(Ljava/lang/Object;)Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_1
    monitor-exit p0

    return-object v0

    .line 212971
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 212972
    :cond_1
    :try_start_1
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 212973
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
