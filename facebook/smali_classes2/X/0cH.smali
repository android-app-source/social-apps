.class public final LX/0cH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/44w",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/0cG;

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0cG",
            "<TT;>.QueryParameter;>;"
        }
    .end annotation
.end field

.field private d:Ljava/util/regex/Pattern;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final g:I


# direct methods
.method public constructor <init>(LX/0cG;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 87983
    iput-object p1, p0, LX/0cH;->b:LX/0cG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87984
    iput-object p2, p0, LX/0cH;->e:Ljava/lang/String;

    .line 87985
    iput-object p3, p0, LX/0cH;->f:Ljava/lang/Object;

    .line 87986
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    .line 87987
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    .line 87988
    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result p1

    .line 87989
    const/16 p3, 0x7b

    if-eq p1, p3, :cond_0

    const/16 p3, 0x3f

    if-eq p1, p3, :cond_0

    const/16 p3, 0x25

    if-ne p1, p3, :cond_1

    .line 87990
    :cond_0
    :goto_1
    move v0, v0

    .line 87991
    iput v0, p0, LX/0cH;->g:I

    .line 87992
    return-void

    .line 87993
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87994
    :cond_2
    add-int/lit8 v0, v1, -0x1

    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 p1, 0x2f

    if-ne v0, p1, :cond_3

    add-int/lit8 v0, v1, -0x1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private static declared-synchronized a(LX/0cH;)V
    .locals 10

    .prologue
    .line 87995
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0cH;->c:Ljava/util/Map;

    .line 87996
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0cH;->a:Ljava/util/List;

    .line 87997
    iget-object v0, p0, LX/0cH;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0cG;->f(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 87998
    const/4 v0, 0x0

    aget-object v0, v1, v0

    .line 87999
    const/4 v2, 0x1

    aget-object v2, v1, v2

    .line 88000
    sget-object v1, LX/0cG;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 88001
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88002
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 88003
    :cond_0
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v4

    move-object v1, v0

    .line 88004
    :goto_0
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 88005
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    .line 88006
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    const-string v6, "#"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 88007
    if-eqz v6, :cond_1

    const-class v0, Ljava/lang/Long;

    .line 88008
    :goto_1
    const/4 v7, 0x2

    invoke-virtual {v3, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    .line 88009
    iget-object v8, p0, LX/0cH;->a:Ljava/util/List;

    new-instance v9, LX/44w;

    invoke-direct {v9, v0, v7}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88010
    invoke-interface {v4, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 88011
    new-instance v0, LX/47U;

    const-string v1, "Duplicate template key"

    invoke-direct {v0, v1}, LX/47U;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88012
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 88013
    :cond_1
    :try_start_1
    const-class v0, Ljava/lang/String;

    goto :goto_1

    .line 88014
    :cond_2
    if-eqz v6, :cond_3

    const-string v0, "(-?[0-9]+)"

    .line 88015
    :goto_2
    invoke-virtual {v1, v5, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 88016
    goto :goto_0

    .line 88017
    :cond_3
    const-string v0, "([^/]+)"

    goto :goto_2

    .line 88018
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[/]?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 88019
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LX/0cH;->d:Ljava/util/regex/Pattern;

    .line 88020
    invoke-static {v2}, LX/0cG;->e(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 88021
    sget-object v3, LX/0cG;->c:Ljava/util/regex/Pattern;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v3, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 88022
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-nez v1, :cond_5

    .line 88023
    new-instance v0, LX/47U;

    const-string v1, "Query parameter does not match templating syntax"

    invoke-direct {v0, v1}, LX/47U;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88024
    :cond_5
    sget-object v1, LX/47W;->STRING:LX/47W;

    .line 88025
    const-string v5, "#"

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 88026
    sget-object v1, LX/47W;->LONG:LX/47W;

    .line 88027
    :cond_6
    :goto_4
    const/4 v5, 0x2

    invoke-virtual {v3, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    .line 88028
    const/4 v6, 0x3

    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 88029
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 88030
    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 88031
    new-instance v0, LX/47U;

    const-string v1, "Duplicate template key"

    invoke-direct {v0, v1}, LX/47U;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88032
    :cond_7
    const-string v5, "!"

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 88033
    sget-object v1, LX/47W;->BOOLEAN:LX/47W;

    goto :goto_4

    .line 88034
    :cond_8
    iget-object v6, p0, LX/0cH;->c:Ljava/util/Map;

    new-instance v7, LX/47V;

    iget-object v8, p0, LX/0cH;->b:LX/0cG;

    invoke-direct {v7, v8, v5, v1, v3}, LX/47V;-><init>(LX/0cG;Ljava/lang/String;LX/47W;Ljava/lang/String;)V

    invoke-interface {v6, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 88035
    :cond_9
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)LX/47X;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/47X",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 88036
    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    iget-object v3, p0, LX/0cH;->e:Ljava/lang/String;

    const/4 v4, 0x0

    iget v5, p0, LX/0cH;->g:I

    invoke-virtual {p1, v1, v3, v4, v5}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    move-object v0, v2

    .line 88037
    :goto_0
    monitor-exit p0

    return-object v0

    .line 88038
    :cond_0
    :try_start_1
    invoke-static {p1}, LX/0cG;->f(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 88039
    const/4 v3, 0x0

    aget-object v3, v1, v3

    .line 88040
    const/4 v4, 0x1

    aget-object v4, v1, v4

    .line 88041
    iget-object v1, p0, LX/0cH;->d:Ljava/util/regex/Pattern;

    if-nez v1, :cond_1

    .line 88042
    invoke-static {p0}, LX/0cH;->a(LX/0cH;)V

    .line 88043
    :cond_1
    iget-object v1, p0, LX/0cH;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 88044
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-nez v1, :cond_2

    move-object v0, v2

    .line 88045
    goto :goto_0

    .line 88046
    :cond_2
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    move v1, v0

    .line 88047
    :goto_1
    iget-object v0, p0, LX/0cH;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 88048
    iget-object v0, p0, LX/0cH;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/44w;

    .line 88049
    iget-object v6, v0, LX/44w;->a:Ljava/lang/Object;

    const-class v7, Ljava/lang/Long;

    if-ne v6, v7, :cond_3

    .line 88050
    iget-object v0, v0, LX/44w;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v5, v0, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 88051
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 88052
    :cond_3
    iget-object v0, v0, LX/44w;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 88053
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 88054
    :cond_4
    :try_start_2
    invoke-static {v4}, LX/0cG;->e(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 88055
    iget-object v0, p0, LX/0cH;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 88056
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 88057
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/47V;

    .line 88058
    iget-object v6, v0, LX/47V;->c:Ljava/lang/String;

    .line 88059
    iget-boolean v7, v0, LX/47V;->a:Z

    if-eqz v7, :cond_5

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    move-object v0, v2

    .line 88060
    goto/16 :goto_0

    .line 88061
    :cond_5
    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 88062
    :goto_4
    iget-object v7, v0, LX/47V;->d:LX/47W;

    sget-object v8, LX/47W;->LONG:LX/47W;

    if-ne v7, v8, :cond_7

    .line 88063
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {v5, v6, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_3

    .line 88064
    :cond_6
    iget-object v1, v0, LX/47V;->b:Ljava/lang/String;

    goto :goto_4

    .line 88065
    :cond_7
    iget-object v0, v0, LX/47V;->d:LX/47W;

    sget-object v7, LX/47W;->BOOLEAN:LX/47W;

    if-ne v0, v7, :cond_8

    .line 88066
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v5, v6, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_3

    .line 88067
    :cond_8
    invoke-virtual {v5, v6, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 88068
    :cond_9
    new-instance v0, LX/47X;

    iget-object v1, p0, LX/0cH;->f:Ljava/lang/Object;

    invoke-direct {v0, v1, v5}, LX/47X;-><init>(Ljava/lang/Object;Landroid/os/Bundle;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method
