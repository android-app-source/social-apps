.class public LX/113;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/113;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/0So;

.field public final c:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field public e:Z


# direct methods
.method public constructor <init>(LX/0Zb;LX/0So;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 169734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169735
    iput-object p1, p0, LX/113;->a:LX/0Zb;

    .line 169736
    iput-object p2, p0, LX/113;->b:LX/0So;

    .line 169737
    new-instance v0, Ljava/util/ArrayDeque;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object v0, p0, LX/113;->c:Ljava/util/ArrayDeque;

    .line 169738
    return-void
.end method

.method public static a(LX/0QB;)LX/113;
    .locals 5

    .prologue
    .line 169739
    sget-object v0, LX/113;->f:LX/113;

    if-nez v0, :cond_1

    .line 169740
    const-class v1, LX/113;

    monitor-enter v1

    .line 169741
    :try_start_0
    sget-object v0, LX/113;->f:LX/113;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 169742
    if-eqz v2, :cond_0

    .line 169743
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 169744
    new-instance p0, LX/113;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-direct {p0, v3, v4}, LX/113;-><init>(LX/0Zb;LX/0So;)V

    .line 169745
    move-object v0, p0

    .line 169746
    sput-object v0, LX/113;->f:LX/113;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169747
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 169748
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 169749
    :cond_1
    sget-object v0, LX/113;->f:LX/113;

    return-object v0

    .line 169750
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 169751
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
