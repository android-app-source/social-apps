.class public LX/1rz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field private static final c:LX/0yY;

.field public static final d:LX/0Tn;

.field private static volatile o:LX/1rz;


# instance fields
.field private final e:LX/0y3;

.field private final f:LX/0Xl;

.field public final g:LX/0Xl;

.field private final h:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final i:LX/0Tf;

.field public final j:LX/0Zb;

.field private k:LX/0Yb;

.field private l:LX/1rv;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private final n:LX/0dN;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 333667
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/1rz;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".ACTION_STATUS_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1rz;->a:Ljava/lang/String;

    .line 333668
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/1rz;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".ACTION_STATUS_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1rz;->b:Ljava/lang/String;

    .line 333669
    sget-object v0, LX/0yY;->LOCATION_SERVICES_INTERSTITIAL:LX/0yY;

    .line 333670
    sput-object v0, LX/1rz;->c:LX/0yY;

    invoke-static {v0}, LX/0df;->a(LX/0yY;)LX/0Tn;

    move-result-object v0

    sput-object v0, LX/1rz;->d:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0y3;LX/0Xl;LX/0Xl;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tf;LX/0Zb;)V
    .locals 1
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/GlobalFbBroadcast;
        .end annotation
    .end param
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p5    # LX/0Tf;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 333617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333618
    new-instance v0, LX/2FR;

    invoke-direct {v0, p0}, LX/2FR;-><init>(LX/1rz;)V

    iput-object v0, p0, LX/1rz;->n:LX/0dN;

    .line 333619
    iput-object p1, p0, LX/1rz;->e:LX/0y3;

    .line 333620
    iput-object p2, p0, LX/1rz;->f:LX/0Xl;

    .line 333621
    iput-object p3, p0, LX/1rz;->g:LX/0Xl;

    .line 333622
    iput-object p4, p0, LX/1rz;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 333623
    iput-object p5, p0, LX/1rz;->i:LX/0Tf;

    .line 333624
    iput-object p6, p0, LX/1rz;->j:LX/0Zb;

    .line 333625
    return-void
.end method

.method private static a(LX/0Rf;)LX/162;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 333671
    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-virtual {v0}, LX/0mC;->b()LX/162;

    move-result-object v1

    .line 333672
    invoke-virtual {p0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 333673
    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_0

    .line 333674
    :cond_0
    return-object v1
.end method

.method public static a(LX/0QB;)LX/1rz;
    .locals 10

    .prologue
    .line 333638
    sget-object v0, LX/1rz;->o:LX/1rz;

    if-nez v0, :cond_1

    .line 333639
    const-class v1, LX/1rz;

    monitor-enter v1

    .line 333640
    :try_start_0
    sget-object v0, LX/1rz;->o:LX/1rz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 333641
    if-eqz v2, :cond_0

    .line 333642
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 333643
    new-instance v3, LX/1rz;

    invoke-static {v0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v4

    check-cast v4, LX/0y3;

    invoke-static {v0}, LX/0aW;->a(LX/0QB;)LX/0aW;

    move-result-object v5

    check-cast v5, LX/0Xl;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v6

    check-cast v6, LX/0Xl;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, LX/0Tf;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v9

    check-cast v9, LX/0Zb;

    invoke-direct/range {v3 .. v9}, LX/1rz;-><init>(LX/0y3;LX/0Xl;LX/0Xl;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tf;LX/0Zb;)V

    .line 333644
    move-object v0, v3

    .line 333645
    sput-object v0, LX/1rz;->o:LX/1rz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 333646
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 333647
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 333648
    :cond_1
    sget-object v0, LX/1rz;->o:LX/1rz;

    return-object v0

    .line 333649
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 333650
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/1rz;LX/1rv;)V
    .locals 5
    .param p0    # LX/1rz;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 333651
    iget-object v2, p0, LX/1rz;->e:LX/0y3;

    invoke-virtual {v2}, LX/0y3;->b()LX/1rv;

    move-result-object v2

    iput-object v2, p0, LX/1rz;->l:LX/1rv;

    .line 333652
    if-eqz p1, :cond_0

    iget-object v2, p0, LX/1rz;->l:LX/1rv;

    iget-object v2, v2, LX/1rv;->a:LX/0yG;

    iget-object v3, p1, LX/1rv;->a:LX/0yG;

    if-eq v2, v3, :cond_5

    :cond_0
    move v2, v1

    .line 333653
    :goto_0
    if-eqz v2, :cond_1

    .line 333654
    iget-object v3, p0, LX/1rz;->g:LX/0Xl;

    sget-object v4, LX/1rz;->a:Ljava/lang/String;

    invoke-interface {v3, v4}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 333655
    :cond_1
    if-eqz p1, :cond_2

    iget-object v3, p0, LX/1rz;->l:LX/1rv;

    invoke-virtual {v3, p1}, LX/1rv;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    move v0, v1

    .line 333656
    :cond_3
    if-eqz v0, :cond_4

    .line 333657
    new-instance v0, Landroid/content/Intent;

    sget-object v1, LX/1rz;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 333658
    const-string v1, "state_changed"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 333659
    iget-object v1, p0, LX/1rz;->g:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 333660
    iget-object v0, p0, LX/1rz;->l:LX/1rv;

    .line 333661
    iget-object v1, p0, LX/1rz;->j:LX/0Zb;

    const-string v2, "location_providers_changed"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 333662
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 333663
    const-string v2, "location"

    invoke-virtual {v1, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v3, "old_status"

    invoke-static {p1}, LX/1rz;->b(LX/1rv;)LX/0lF;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    move-result-object v2

    const-string v3, "new_status"

    invoke-static {v0}, LX/1rz;->b(LX/1rv;)LX/0lF;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 333664
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 333665
    :cond_4
    return-void

    :cond_5
    move v2, v0

    .line 333666
    goto :goto_0
.end method

.method public static b(LX/1rv;)LX/0lF;
    .locals 3
    .param p0    # LX/1rv;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 333631
    if-nez p0, :cond_0

    .line 333632
    const/4 v0, 0x0

    .line 333633
    :goto_0
    return-object v0

    .line 333634
    :cond_0
    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-virtual {v0}, LX/0mC;->c()LX/0m9;

    move-result-object v0

    .line 333635
    const-string v1, "state"

    iget-object v2, p0, LX/1rv;->a:LX/0yG;

    invoke-virtual {v2}, LX/0yG;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 333636
    const-string v1, "user_enabled_providers"

    iget-object v2, p0, LX/1rv;->b:LX/0Rf;

    invoke-static {v2}, LX/1rz;->a(LX/0Rf;)LX/162;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 333637
    const-string v1, "user_disabled_providers"

    iget-object v2, p0, LX/1rv;->c:LX/0Rf;

    invoke-static {v2}, LX/1rz;->a(LX/0Rf;)LX/162;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_0
.end method

.method public static d(LX/1rz;)V
    .locals 6

    .prologue
    .line 333626
    iget-object v0, p0, LX/1rz;->l:LX/1rv;

    .line 333627
    iget-object v1, p0, LX/1rz;->e:LX/0y3;

    invoke-virtual {v1}, LX/0y3;->b()LX/1rv;

    move-result-object v1

    iput-object v1, p0, LX/1rz;->l:LX/1rv;

    .line 333628
    iget-object v1, p0, LX/1rz;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 333629
    :goto_0
    return-void

    .line 333630
    :cond_0
    iget-object v1, p0, LX/1rz;->i:LX/0Tf;

    new-instance v2, Lcom/facebook/location/FbLocationStatusMonitor$3;

    invoke-direct {v2, p0, v0}, Lcom/facebook/location/FbLocationStatusMonitor$3;-><init>(LX/1rz;LX/1rv;)V

    const-wide/16 v4, 0x1f4

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v4, v5, v0}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    iput-object v0, p0, LX/1rz;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 333610
    iget-object v0, p0, LX/1rz;->l:LX/1rv;

    if-eqz v0, :cond_0

    .line 333611
    iget-object v0, p0, LX/1rz;->l:LX/1rv;

    invoke-static {p0, v0}, LX/1rz;->a$redex0(LX/1rz;LX/1rv;)V

    .line 333612
    :goto_0
    iget-object v0, p0, LX/1rz;->f:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "android.location.PROVIDERS_CHANGED"

    new-instance v2, LX/2FV;

    invoke-direct {v2, p0}, LX/2FV;-><init>(LX/1rz;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/1rz;->k:LX/0Yb;

    .line 333613
    iget-object v0, p0, LX/1rz;->k:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 333614
    iget-object v0, p0, LX/1rz;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1rz;->d:LX/0Tn;

    iget-object v2, p0, LX/1rz;->n:LX/0dN;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 333615
    return-void

    .line 333616
    :cond_0
    iget-object v0, p0, LX/1rz;->e:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->b()LX/1rv;

    move-result-object v0

    iput-object v0, p0, LX/1rz;->l:LX/1rv;

    goto :goto_0
.end method
