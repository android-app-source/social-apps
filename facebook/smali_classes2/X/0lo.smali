.class public final LX/0lo;
.super LX/0lL;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 130770
    invoke-direct {p0}, LX/0lL;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0m2;LX/0lJ;LX/0m5;)LX/0lR;
    .locals 2

    .prologue
    .line 130771
    invoke-static {p2}, LX/0lL;->a(LX/0lJ;)LX/0lR;

    move-result-object v0

    .line 130772
    if-eqz v0, :cond_0

    .line 130773
    :goto_0
    return-object v0

    .line 130774
    :cond_0
    iget-object v0, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 130775
    const-class v1, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    .line 130776
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;->using()Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 130777
    invoke-static {p1, p2, p3}, LX/0lL;->a(LX/0m4;LX/0lJ;LX/0m5;)LX/0lR;

    move-result-object v0

    goto :goto_0

    .line 130778
    :cond_1
    invoke-super {p0, p1, p2, p3}, LX/0lL;->a(LX/0m2;LX/0lJ;LX/0m5;)LX/0lR;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0mu;LX/0lJ;LX/0m5;)LX/0lR;
    .locals 2

    .prologue
    .line 130762
    invoke-static {p2}, LX/0lL;->a(LX/0lJ;)LX/0lR;

    move-result-object v0

    .line 130763
    if-eqz v0, :cond_0

    .line 130764
    :goto_0
    return-object v0

    .line 130765
    :cond_0
    iget-object v0, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 130766
    const-class v1, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    .line 130767
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;->using()Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 130768
    invoke-static {p1, p2, p3}, LX/0lL;->a(LX/0m4;LX/0lJ;LX/0m5;)LX/0lR;

    move-result-object v0

    goto :goto_0

    .line 130769
    :cond_1
    invoke-super {p0, p1, p2, p3}, LX/0lL;->a(LX/0mu;LX/0lJ;LX/0m5;)LX/0lR;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic b(LX/0m2;LX/0lJ;LX/0m5;)LX/0lS;
    .locals 1

    .prologue
    .line 130761
    invoke-virtual {p0, p1, p2, p3}, LX/0lo;->a(LX/0m2;LX/0lJ;LX/0m5;)LX/0lR;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(LX/0mu;LX/0lJ;LX/0m5;)LX/0lS;
    .locals 1

    .prologue
    .line 130760
    invoke-virtual {p0, p1, p2, p3}, LX/0lo;->a(LX/0mu;LX/0lJ;LX/0m5;)LX/0lR;

    move-result-object v0

    return-object v0
.end method
