.class public LX/160;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/proxygen/NativeHandle;


# instance fields
.field private mNativeHandle:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 183963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getNativeHandle()J
    .locals 2

    .prologue
    .line 183964
    iget-wide v0, p0, LX/160;->mNativeHandle:J

    return-wide v0
.end method

.method public setNativeHandle(J)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 183959
    iget-wide v0, p0, LX/160;->mNativeHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    cmp-long v0, p1, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/1hH;->checkState(Z)V

    .line 183960
    iput-wide p1, p0, LX/160;->mNativeHandle:J

    .line 183961
    return-void

    .line 183962
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
