.class public final LX/0Z0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0Z2;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0Z2;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 82911
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82912
    iput-object p1, p0, LX/0Z0;->a:LX/0QB;

    .line 82913
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 82914
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0Z0;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 82915
    packed-switch p2, :pswitch_data_0

    .line 82916
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82917
    :pswitch_0
    new-instance v0, LX/0Z1;

    invoke-direct {v0}, LX/0Z1;-><init>()V

    .line 82918
    move-object v0, v0

    .line 82919
    move-object v0, v0

    .line 82920
    :goto_0
    return-object v0

    .line 82921
    :pswitch_1
    new-instance v0, LX/0Z3;

    invoke-direct {v0}, LX/0Z3;-><init>()V

    .line 82922
    move-object v0, v0

    .line 82923
    move-object v0, v0

    .line 82924
    goto :goto_0

    .line 82925
    :pswitch_2
    new-instance v0, LX/0Z4;

    invoke-direct {v0}, LX/0Z4;-><init>()V

    .line 82926
    move-object v0, v0

    .line 82927
    move-object v0, v0

    .line 82928
    goto :goto_0

    .line 82929
    :pswitch_3
    new-instance v0, LX/0Z5;

    invoke-direct {v0}, LX/0Z5;-><init>()V

    .line 82930
    move-object v0, v0

    .line 82931
    move-object v0, v0

    .line 82932
    goto :goto_0

    .line 82933
    :pswitch_4
    new-instance v0, LX/0Z6;

    invoke-direct {v0}, LX/0Z6;-><init>()V

    .line 82934
    move-object v0, v0

    .line 82935
    move-object v0, v0

    .line 82936
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 82937
    const/4 v0, 0x5

    return v0
.end method
