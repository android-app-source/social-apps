.class public final LX/0et;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0lA;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/concurrent/atomic/AtomicReference;

.field public final synthetic b:LX/0ec;

.field public final synthetic c:LX/0ed;

.field public final synthetic d:Ljava/util/concurrent/atomic/AtomicReference;

.field public final synthetic e:LX/0Vv;


# direct methods
.method public constructor <init>(LX/0Vv;Ljava/util/concurrent/atomic/AtomicReference;LX/0ec;LX/0ed;Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 0

    .prologue
    .line 104294
    iput-object p1, p0, LX/0et;->e:LX/0Vv;

    iput-object p2, p0, LX/0et;->a:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p3, p0, LX/0et;->b:LX/0ec;

    iput-object p4, p0, LX/0et;->c:LX/0ed;

    iput-object p5, p0, LX/0et;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 104295
    iget-object v0, p0, LX/0et;->a:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 104296
    iget-object v0, p0, LX/0et;->e:LX/0Vv;

    iget-object v1, p0, LX/0et;->b:LX/0ec;

    iget-object v2, p0, LX/0et;->c:LX/0ed;

    .line 104297
    iget-object p0, v0, LX/0Vv;->e:LX/0Wn;

    invoke-virtual {p0, v1, v2, p1}, LX/0Wn;->a(LX/0ec;LX/0ed;Ljava/lang/Throwable;)V

    .line 104298
    iget-object p0, v0, LX/0Vv;->v:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {p0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 104299
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 104300
    check-cast p1, LX/0lA;

    .line 104301
    iget-object v0, p0, LX/0et;->a:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 104302
    iget-object v0, p0, LX/0et;->e:LX/0Vv;

    iget-object v1, p0, LX/0et;->b:LX/0ec;

    iget-object v2, p0, LX/0et;->c:LX/0ed;

    iget-object v3, p0, LX/0et;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v0, v1, v2, p1, v3}, LX/0Vv;->a$redex0(LX/0Vv;LX/0ec;LX/0ed;LX/0lA;Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 104303
    return-void
.end method
