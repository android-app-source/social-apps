.class public LX/1V4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/0ad;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 257235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 257236
    iput-object p1, p0, LX/1V4;->a:LX/0ad;

    .line 257237
    return-void
.end method

.method public static a(LX/0QB;)LX/1V4;
    .locals 4

    .prologue
    .line 257238
    const-class v1, LX/1V4;

    monitor-enter v1

    .line 257239
    :try_start_0
    sget-object v0, LX/1V4;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 257240
    sput-object v2, LX/1V4;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 257241
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257242
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 257243
    new-instance p0, LX/1V4;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/1V4;-><init>(LX/0ad;)V

    .line 257244
    move-object v0, p0

    .line 257245
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 257246
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1V4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 257247
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 257248
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 257249
    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final c()Z
    .locals 3

    .prologue
    .line 257250
    iget-object v0, p0, LX/1V4;->d:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 257251
    iget-object v0, p0, LX/1V4;->a:LX/0ad;

    sget v1, LX/1Dd;->b:I

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 257252
    invoke-static {v0}, LX/1V4;->a(I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1V4;->d:Ljava/lang/Boolean;

    .line 257253
    :cond_0
    iget-object v0, p0, LX/1V4;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
