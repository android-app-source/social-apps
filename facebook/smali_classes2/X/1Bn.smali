.class public LX/1Bn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "EmptyCatchBlock"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static a:J

.field public static final b:Ljava/lang/String;

.field private static volatile x:LX/1Bn;


# instance fields
.field private final c:LX/13Q;

.field private final d:LX/0en;

.field public final e:LX/03V;

.field private final f:LX/0ad;

.field private final g:LX/2n4;

.field private final h:LX/0SG;

.field public i:LX/3n3;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Xl;

.field private final l:Landroid/os/Handler;

.field public final m:LX/0kb;

.field private final n:LX/0W3;

.field private final o:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final p:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final q:LX/2n5;

.field private final r:LX/0Uh;

.field public final s:LX/2n7;

.field public final t:LX/1Be;

.field private final u:LX/1Br;

.field public final v:LX/2nB;

.field public w:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 214202
    const-wide/32 v0, 0x5265c00

    sput-wide v0, LX/1Bn;->a:J

    .line 214203
    const-class v0, LX/1Bn;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1Bn;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0en;LX/13Q;LX/03V;LX/0ad;LX/2n4;LX/0SG;Landroid/os/Handler;LX/0Xl;LX/0kb;LX/0W3;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/2n5;LX/0Uh;LX/2n7;LX/1Be;LX/1Br;LX/2nB;)V
    .locals 2
    .param p7    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
        .end annotation
    .end param
    .param p8    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p12    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0en;",
            "LX/13Q;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0ad;",
            "LX/2n4;",
            "LX/0SG;",
            "Landroid/os/Handler;",
            "LX/0Xl;",
            "LX/0kb;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/2n5;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/2n7;",
            "LX/1Be;",
            "LX/1Br;",
            "LX/2nB;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 214180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214181
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/1Bn;->w:Z

    .line 214182
    iput-object p1, p0, LX/1Bn;->d:LX/0en;

    .line 214183
    iput-object p2, p0, LX/1Bn;->c:LX/13Q;

    .line 214184
    iput-object p3, p0, LX/1Bn;->e:LX/03V;

    .line 214185
    iput-object p4, p0, LX/1Bn;->f:LX/0ad;

    .line 214186
    iput-object p5, p0, LX/1Bn;->g:LX/2n4;

    .line 214187
    iput-object p6, p0, LX/1Bn;->h:LX/0SG;

    .line 214188
    iput-object p7, p0, LX/1Bn;->l:Landroid/os/Handler;

    .line 214189
    iput-object p8, p0, LX/1Bn;->k:LX/0Xl;

    .line 214190
    iput-object p9, p0, LX/1Bn;->m:LX/0kb;

    .line 214191
    iput-object p10, p0, LX/1Bn;->n:LX/0W3;

    .line 214192
    iput-object p11, p0, LX/1Bn;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 214193
    iput-object p12, p0, LX/1Bn;->p:LX/0Or;

    .line 214194
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, LX/1Bn;->j:Ljava/util/Map;

    .line 214195
    iput-object p13, p0, LX/1Bn;->q:LX/2n5;

    .line 214196
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1Bn;->r:LX/0Uh;

    .line 214197
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1Bn;->s:LX/2n7;

    .line 214198
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1Bn;->t:LX/1Be;

    .line 214199
    move-object/from16 v0, p17

    iput-object v0, p0, LX/1Bn;->u:LX/1Br;

    .line 214200
    move-object/from16 v0, p18

    iput-object v0, p0, LX/1Bn;->v:LX/2nB;

    .line 214201
    return-void
.end method

.method public static a(LX/0QB;)LX/1Bn;
    .locals 3

    .prologue
    .line 214170
    sget-object v0, LX/1Bn;->x:LX/1Bn;

    if-nez v0, :cond_1

    .line 214171
    const-class v1, LX/1Bn;

    monitor-enter v1

    .line 214172
    :try_start_0
    sget-object v0, LX/1Bn;->x:LX/1Bn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 214173
    if-eqz v2, :cond_0

    .line 214174
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1Bn;->b(LX/0QB;)LX/1Bn;

    move-result-object v0

    sput-object v0, LX/1Bn;->x:LX/1Bn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 214175
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 214176
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 214177
    :cond_1
    sget-object v0, LX/1Bn;->x:LX/1Bn;

    return-object v0

    .line 214178
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 214179
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1Bn;Ljava/lang/String;Ljava/lang/String;Z)LX/1By;
    .locals 12
    .param p0    # LX/1Bn;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 214142
    iget-object v0, p0, LX/1Bn;->i:LX/3n3;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 214143
    :cond_0
    :goto_0
    return-object v0

    .line 214144
    :cond_1
    iget-object v0, p0, LX/1Bn;->i:LX/3n3;

    invoke-virtual {v0, p2}, LX/3n3;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1By;

    .line 214145
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 214146
    if-nez v0, :cond_5

    .line 214147
    :cond_2
    :goto_1
    move v2, v3

    .line 214148
    if-nez v2, :cond_3

    move-object v0, v1

    .line 214149
    goto :goto_0

    .line 214150
    :cond_3
    if-eqz p3, :cond_0

    .line 214151
    iget v1, v0, LX/1By;->a:I

    if-nez v1, :cond_4

    .line 214152
    const-string v1, "browser_prefetch_cache_used_%s_%d"

    invoke-direct {p0, v1, v0}, LX/1Bn;->a(Ljava/lang/String;LX/1By;)V

    .line 214153
    :cond_4
    iget v1, v0, LX/1By;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/1By;->a:I

    goto :goto_0

    .line 214154
    :cond_5
    if-eqz p3, :cond_6

    iget-object v5, p0, LX/1Bn;->m:LX/0kb;

    invoke-virtual {v5}, LX/0kb;->d()Z

    move-result v5

    if-nez v5, :cond_7

    :cond_6
    move v3, v4

    .line 214155
    goto :goto_1

    .line 214156
    :cond_7
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 214157
    iget-wide v8, v0, LX/1By;->i:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-gez v8, :cond_a

    .line 214158
    :cond_8
    :goto_2
    move v5, v6

    .line 214159
    if-eqz v5, :cond_9

    move v3, v4

    .line 214160
    goto :goto_1

    .line 214161
    :cond_9
    if-eqz p1, :cond_2

    invoke-virtual {v0}, LX/1By;->h()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 214162
    iget-object v4, p0, LX/1Bn;->t:LX/1Be;

    sget-object v5, LX/1Bu;->EXPIRED:LX/1Bu;

    invoke-virtual {v4, p1, v5}, LX/1Be;->a(Ljava/lang/String;LX/1Bu;)V

    goto :goto_1

    .line 214163
    :cond_a
    iget-object v8, v0, LX/1By;->g:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    .line 214164
    iget-wide v10, v0, LX/1By;->h:J

    cmp-long v10, v8, v10

    if-lez v10, :cond_8

    .line 214165
    iget-wide v10, v0, LX/1By;->i:J

    cmp-long v10, v8, v10

    if-gez v10, :cond_b

    move v6, v7

    .line 214166
    goto :goto_2

    .line 214167
    :cond_b
    iget-wide v10, v0, LX/1By;->h:J

    sub-long/2addr v8, v10

    sget-wide v10, LX/1Bn;->a:J

    cmp-long v8, v8, v10

    if-gez v8, :cond_8

    .line 214168
    const-wide/16 v8, -0x1

    iput-wide v8, v0, LX/1By;->i:J

    move v6, v7

    .line 214169
    goto :goto_2
.end method

.method private a(Ljava/lang/String;LX/1By;)V
    .locals 3

    .prologue
    .line 214136
    invoke-virtual {p2}, LX/1By;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "html"

    .line 214137
    :goto_0
    iget-object v1, p0, LX/1Bn;->c:LX/13Q;

    .line 214138
    iget v2, p2, LX/1By;->f:I

    move v2, v2

    .line 214139
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p1, v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/13Q;->a(Ljava/lang/String;)V

    .line 214140
    return-void

    .line 214141
    :cond_0
    const-string v0, "res"

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/1Bn;
    .locals 20

    .prologue
    .line 213977
    new-instance v1, LX/1Bn;

    invoke-static/range {p0 .. p0}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v2

    check-cast v2, LX/0en;

    invoke-static/range {p0 .. p0}, LX/13Q;->a(LX/0QB;)LX/13Q;

    move-result-object v3

    check-cast v3, LX/13Q;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/2n4;->a(LX/0QB;)LX/2n4;

    move-result-object v6

    check-cast v6, LX/2n4;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0Zw;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v8

    check-cast v8, Landroid/os/Handler;

    invoke-static/range {p0 .. p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v9

    check-cast v9, LX/0Xl;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v10

    check-cast v10, LX/0kb;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v11

    check-cast v11, LX/0W3;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v12

    check-cast v12, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v13, 0x2fd

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/2n5;->a(LX/0QB;)LX/2n5;

    move-result-object v14

    check-cast v14, LX/2n5;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v15

    check-cast v15, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/2n7;->a(LX/0QB;)LX/2n7;

    move-result-object v16

    check-cast v16, LX/2n7;

    invoke-static/range {p0 .. p0}, LX/1Be;->a(LX/0QB;)LX/1Be;

    move-result-object v17

    check-cast v17, LX/1Be;

    invoke-static/range {p0 .. p0}, LX/1Br;->a(LX/0QB;)LX/1Br;

    move-result-object v18

    check-cast v18, LX/1Br;

    invoke-static/range {p0 .. p0}, LX/2nB;->a(LX/0QB;)LX/2nB;

    move-result-object v19

    check-cast v19, LX/2nB;

    invoke-direct/range {v1 .. v19}, LX/1Bn;-><init>(LX/0en;LX/13Q;LX/03V;LX/0ad;LX/2n4;LX/0SG;Landroid/os/Handler;LX/0Xl;LX/0kb;LX/0W3;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/2n5;LX/0Uh;LX/2n7;LX/1Be;LX/1Br;LX/2nB;)V

    .line 213978
    return-object v1
.end method

.method private static g(LX/1Bn;)V
    .locals 11

    .prologue
    .line 214098
    iget-object v1, p0, LX/1Bn;->i:LX/3n3;

    if-nez v1, :cond_0

    .line 214099
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Could not init cache properly."

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 214100
    :cond_0
    const-string v1, "Start filling in in_app_browser cache"

    const v2, 0x756c92f3

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 214101
    :try_start_0
    iget-object v1, p0, LX/1Bn;->s:LX/2n7;

    invoke-virtual {v1}, LX/2n7;->a()Ljava/util/List;

    move-result-object v7

    .line 214102
    iget-object v1, p0, LX/1Bn;->j:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 214103
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 214104
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, LX/7i4;

    move-object v6, v0

    .line 214105
    iget-object v0, v6, LX/7i4;->b:Ljava/lang/String;

    move-object v1, v0

    .line 214106
    invoke-static {v1}, LX/047;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 214107
    iget-object v1, p0, LX/1Bn;->v:LX/2nB;

    invoke-virtual {v1, v10}, LX/2nB;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 214108
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 214109
    iget-object v1, p0, LX/1Bn;->s:LX/2n7;

    .line 214110
    iget-object v0, v6, LX/7i4;->a:Ljava/lang/String;

    move-object v2, v0

    .line 214111
    iget-object v0, v6, LX/7i4;->b:Ljava/lang/String;

    move-object v3, v0

    .line 214112
    invoke-virtual {v1, v2, v3}, LX/2n7;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 214113
    :catchall_0
    move-exception v1

    const v2, -0x5eeebeca

    invoke-static {v2}, LX/02m;->a(I)V

    throw v1

    .line 214114
    :cond_1
    :try_start_1
    iget-object v0, v6, LX/7i4;->a:Ljava/lang/String;

    move-object v1, v0

    .line 214115
    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    .line 214116
    :goto_1
    iget-object v2, p0, LX/1Bn;->j:Ljava/util/Map;

    .line 214117
    iget-object v0, v6, LX/7i4;->a:Ljava/lang/String;

    move-object v4, v0

    .line 214118
    invoke-interface {v2, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214119
    new-instance v1, LX/1By;

    iget-object v2, p0, LX/1Bn;->h:LX/0SG;

    .line 214120
    iget-object v0, v6, LX/7i4;->a:Ljava/lang/String;

    move-object v4, v0

    .line 214121
    iget-object v0, v6, LX/7i4;->c:LX/7i0;

    move-object v5, v0

    .line 214122
    iget v0, v6, LX/7i4;->d:I

    move v6, v0

    .line 214123
    invoke-direct/range {v1 .. v6}, LX/1By;-><init>(LX/0SG;Ljava/io/File;Ljava/lang/String;LX/7i0;I)V

    .line 214124
    iget-boolean v2, p0, LX/1Bn;->w:Z

    if-nez v2, :cond_2

    invoke-virtual {v1}, LX/1By;->h()Z

    move-result v2

    if-nez v2, :cond_2

    .line 214125
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/1Bn;->w:Z

    .line 214126
    :cond_2
    iget-object v2, p0, LX/1Bn;->i:LX/3n3;

    invoke-virtual {v2, v10, v1}, LX/3n3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214127
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 214128
    :cond_3
    iget-object v0, v6, LX/7i4;->b:Ljava/lang/String;

    move-object v1, v0

    .line 214129
    goto :goto_1

    .line 214130
    :cond_4
    iget-object v1, p0, LX/1Bn;->d:LX/0en;

    iget-object v2, p0, LX/1Bn;->v:LX/2nB;

    invoke-virtual {v2}, LX/2nB;->b()Ljava/io/File;

    move-result-object v2

    new-instance v3, LX/3n7;

    invoke-direct {v3, p0, v8}, LX/3n7;-><init>(LX/1Bn;Ljava/util/Set;)V

    invoke-virtual {v1, v2, v3}, LX/0en;->a(Ljava/io/File;LX/0Rl;)Z

    move-result v1

    .line 214131
    if-nez v1, :cond_5

    .line 214132
    sget-object v1, LX/1Bn;->b:Ljava/lang/String;

    const-string v2, "Failed delete some stray files in cache"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 214133
    :cond_5
    invoke-interface {v7}, Ljava/util/List;->size()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 214134
    const v1, -0x58c7e962

    invoke-static {v1}, LX/02m;->a(I)V

    .line 214135
    return-void
.end method


# virtual methods
.method public final a(LX/1Bt;Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;LX/7i0;)LX/1By;
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StringReferenceComparison"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 213995
    invoke-static {p2}, LX/047;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 213996
    if-ne p2, p3, :cond_1

    move-object v6, v3

    .line 213997
    :goto_0
    iget-object v0, p0, LX/1Bn;->v:LX/2nB;

    .line 213998
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, LX/2nB;->b()Ljava/io/File;

    move-result-object v2

    .line 213999
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6}, LX/03l;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v0, ".tmp"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 214000
    invoke-direct {v1, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v8, v1

    .line 214001
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214002
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 214003
    iget-object v0, p0, LX/1Bn;->e:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/1Bn;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".putInCache"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed delete existing tmp cache file for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object v0, v7

    .line 214004
    :goto_1
    return-object v0

    .line 214005
    :cond_1
    invoke-static {p3}, LX/047;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    goto :goto_0

    .line 214006
    :cond_2
    :try_start_0
    iget-object v0, p0, LX/1Bn;->u:LX/1Br;

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 214007
    iget-boolean v2, p1, LX/1Bt;->c:Z

    if-eqz v2, :cond_3

    iget-object v2, v0, LX/1Br;->g:LX/0ad;

    sget-short v5, LX/1Bm;->e:S

    invoke-interface {v2, v5, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_3
    move-object v1, v4

    .line 214008
    :goto_2
    move-object v9, v1

    .line 214009
    if-eqz v9, :cond_4

    iget-object v0, v9, LX/7hy;->a:Ljava/io/ByteArrayOutputStream;

    if-nez v0, :cond_5

    .line 214010
    :cond_4
    iget-object v0, p0, LX/1Bn;->d:LX/0en;

    invoke-virtual {v0, p4, v8}, LX/0en;->a(Ljava/io/InputStream;Ljava/io/File;)V

    .line 214011
    :cond_5
    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-nez v0, :cond_6

    .line 214012
    iget-object v0, p0, LX/1Bn;->e:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/1Bn;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".zeroLengthTmpFile"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Tmp file 0 length or not exist for %s"

    invoke-static {v2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214013
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    move-object v0, v7

    goto :goto_1

    .line 214014
    :cond_6
    :try_start_1
    iget-object v0, p0, LX/1Bn;->v:LX/2nB;

    invoke-virtual {v0, v8, v6, p3}, LX/2nB;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 214015
    if-nez v2, :cond_7

    .line 214016
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    move-object v0, v7

    goto :goto_1

    .line 214017
    :cond_7
    :try_start_2
    new-instance v0, LX/1By;

    iget-object v1, p0, LX/1Bn;->h:LX/0SG;

    iget v5, p1, LX/1Bt;->b:I

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, LX/1By;-><init>(LX/0SG;Ljava/io/File;Ljava/lang/String;LX/7i0;I)V

    .line 214018
    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v1, v4

    const/4 v4, 0x1

    aput-object p3, v1, v4

    const/4 v4, 0x2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x3

    .line 214019
    iget v4, v0, LX/1By;->b:I

    move v4, v4

    .line 214020
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    const/4 v2, 0x4

    invoke-virtual {v0}, LX/1By;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v2

    const/4 v2, 0x5

    .line 214021
    iget-object v4, v0, LX/1By;->d:Ljava/lang/String;

    move-object v4, v4

    .line 214022
    aput-object v4, v1, v2

    const/4 v2, 0x6

    .line 214023
    iget-object v4, v0, LX/1By;->e:Ljava/lang/String;

    move-object v4, v4

    .line 214024
    aput-object v4, v1, v2

    .line 214025
    iget-object v1, p0, LX/1Bn;->j:Ljava/util/Map;

    if-ne p2, p3, :cond_12

    :goto_3
    invoke-interface {v1, v3, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214026
    iget-object v1, p0, LX/1Bn;->i:LX/3n3;

    invoke-virtual {v1, v6, v0}, LX/3n3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214027
    invoke-virtual {p0}, LX/1Bn;->d()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 214028
    iget-object v1, p0, LX/1Bn;->s:LX/2n7;

    iget v2, p1, LX/1Bt;->b:I

    invoke-virtual {v1, v3, p3, p5, v2}, LX/2n7;->a(Ljava/lang/String;Ljava/lang/String;LX/7i0;I)V

    .line 214029
    :cond_8
    iget-boolean v1, p1, LX/1Bt;->c:Z

    if-eqz v1, :cond_a

    invoke-virtual {p0}, LX/1Bn;->e()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 214030
    iget-object v1, p0, LX/1Bn;->q:LX/2n5;

    .line 214031
    iget-object v2, v1, LX/2n5;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1fI;

    .line 214032
    const-string v5, "ATTACHMENT_LINK"

    const/4 v6, 0x1

    invoke-static {v2, v3, v5, v6}, LX/1fI;->b(LX/1fI;Ljava/lang/String;Ljava/lang/String;I)V

    .line 214033
    goto :goto_4

    .line 214034
    :cond_9
    iget-object v2, v1, LX/2n5;->b:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    .line 214035
    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 214036
    invoke-static {v2, v5, v3}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->b(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 214037
    goto :goto_5

    .line 214038
    :cond_a
    const-string v1, "browser_prefetch_cache_stored_%s_%d"

    invoke-direct {p0, v1, v0}, LX/1Bn;->a(Ljava/lang/String;LX/1By;)V

    .line 214039
    iget-boolean v1, p1, LX/1Bt;->c:Z

    if-nez v1, :cond_b

    .line 214040
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/1Bn;->w:Z

    .line 214041
    :cond_b
    if-eqz v9, :cond_11

    .line 214042
    iget-object v1, p0, LX/1Bn;->u:LX/1Br;

    .line 214043
    iget-object v2, v0, LX/1By;->e:Ljava/lang/String;

    move-object v2, v2

    .line 214044
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 214045
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "fb4a_iab_preview_log"

    invoke-direct {v4, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 214046
    const/4 v3, 0x0

    .line 214047
    iget-boolean v5, v9, LX/7hy;->d:Z

    if-nez v5, :cond_1c

    .line 214048
    const-string v3, "network_too_high"

    move v5, v6

    .line 214049
    :goto_6
    invoke-virtual {v1, p3}, LX/1Br;->c(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_c

    .line 214050
    const-string v3, "blacklisted"

    move v5, v6

    .line 214051
    :cond_c
    invoke-static {v2}, LX/1Br;->e(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_d

    .line 214052
    const-string v3, "non_utf_8"

    move v5, v6

    .line 214053
    :cond_d
    invoke-static {v1, p1}, LX/1Br;->a(LX/1Br;LX/1Bt;)Z

    move-result p0

    if-nez p0, :cond_1b

    .line 214054
    const-string p4, "Ads"

    move v5, v6

    .line 214055
    :goto_7
    if-eqz v5, :cond_1a

    iget-object v3, v9, LX/7hy;->a:Ljava/io/ByteArrayOutputStream;

    if-eqz v3, :cond_1a
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 214056
    :try_start_3
    iget-object v3, v9, LX/7hy;->a:Ljava/io/ByteArrayOutputStream;

    const-string v5, "UTF-8"

    invoke-virtual {v3, v5}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    move-result-object v3

    .line 214057
    const-string v5, "utf-8"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_f

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_18

    const/4 p0, 0x1

    const/4 v5, 0x0

    .line 214058
    if-nez v3, :cond_1d

    .line 214059
    :cond_e
    :goto_8
    move v5, v5

    .line 214060
    if-eqz v5, :cond_18

    .line 214061
    :cond_f
    iget-object v5, v1, LX/1Br;->h:LX/2n4;

    invoke-virtual {v5, p3, v3}, LX/2n4;->a(Ljava/lang/String;Ljava/lang/String;)LX/7ht;

    move-result-object v3

    .line 214062
    iget-object p4, v3, LX/7ht;->b:Ljava/lang/String;

    .line 214063
    iget-object v5, v3, LX/7ht;->a:LX/7hu;

    if-eqz v5, :cond_19

    .line 214064
    iget-object v5, v1, LX/1Br;->b:Landroid/util/LruCache;

    iget-object v3, v3, LX/7ht;->a:LX/7hu;

    invoke-virtual {v5, p3, v3}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_9
    move p2, v7

    .line 214065
    :goto_a
    iget-object v3, v1, LX/1Br;->c:Landroid/util/LruCache;

    if-eqz v3, :cond_10

    .line 214066
    iget-object v3, v1, LX/1Br;->c:Landroid/util/LruCache;

    invoke-virtual {v3, p3, p4}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214067
    :cond_10
    iget-object v3, v1, LX/1Br;->j:LX/2nA;

    iget v6, v9, LX/7hy;->c:I

    iget v7, v9, LX/7hy;->b:I

    move-object v5, v2

    move-object p0, p3

    .line 214068
    const-string v1, "char_info"

    invoke-virtual {v4, v1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214069
    const-string v1, "network_value"

    invoke-virtual {v4, v1, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214070
    const-string v1, "max_version_network_value"

    invoke-virtual {v4, v1, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214071
    const-string v1, "final_url"

    invoke-virtual {v4, v1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214072
    const-string v1, "succeed"

    invoke-virtual {v4, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214073
    const-string v1, "reason"

    invoke-virtual {v4, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214074
    iget-object v1, v3, LX/2nA;->a:LX/0Zb;

    invoke-interface {v1, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 214075
    :cond_11
    :goto_b
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    goto/16 :goto_1

    :cond_12
    move-object v7, p3

    .line 214076
    goto/16 :goto_3

    .line 214077
    :catchall_0
    move-exception v0

    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    throw v0

    .line 214078
    :cond_13
    :try_start_5
    new-instance v2, LX/7hy;

    invoke-direct {v2}, LX/7hy;-><init>()V

    .line 214079
    iget-object v5, v0, LX/1Br;->g:LX/0ad;

    sget v9, LX/1Bm;->i:I

    invoke-interface {v5, v9, v1}, LX/0ad;->a(II)I

    move-result v5

    iput v5, v2, LX/7hy;->b:I

    .line 214080
    invoke-static {v0}, LX/1Br;->d(LX/1Br;)I

    move-result v5

    iput v5, v2, LX/7hy;->c:I

    .line 214081
    invoke-static {v0}, LX/1Br;->c(LX/1Br;)Z

    move-result v5

    if-nez v5, :cond_14

    iget v5, v2, LX/7hy;->c:I

    iget v9, v2, LX/7hy;->b:I

    if-gt v5, v9, :cond_15

    :cond_14
    const/4 v1, 0x1

    :cond_15
    iput-boolean v1, v2, LX/7hy;->d:Z

    .line 214082
    iget-boolean v1, v2, LX/7hy;->d:Z

    .line 214083
    invoke-static {v4}, LX/1Br;->e(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_17

    if-eqz v1, :cond_17

    invoke-virtual {v0, p3}, LX/1Br;->c(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_17

    invoke-static {v0, p1}, LX/1Br;->a(LX/1Br;LX/1Bt;)Z

    move-result v5

    if-eqz v5, :cond_17

    const/4 v5, 0x1

    :goto_c
    move v1, v5

    .line 214084
    if-eqz v1, :cond_16

    .line 214085
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v1, v2, LX/7hy;->a:Ljava/io/ByteArrayOutputStream;

    .line 214086
    iget-object v1, v2, LX/7hy;->a:Ljava/io/ByteArrayOutputStream;

    invoke-static {p4, v8, v1}, LX/1Br;->a(Ljava/io/InputStream;Ljava/io/File;Ljava/io/ByteArrayOutputStream;)V

    :cond_16
    move-object v1, v2

    .line 214087
    goto/16 :goto_2

    :cond_17
    const/4 v5, 0x0

    goto :goto_c
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 214088
    :cond_18
    const-string p4, "non_utf_8"

    move p2, v6

    goto/16 :goto_a

    .line 214089
    :catch_0
    goto :goto_b

    :cond_19
    move v7, v6

    goto/16 :goto_9

    :cond_1a
    move p2, v6

    goto/16 :goto_a

    :cond_1b
    move-object p4, v3

    goto/16 :goto_7

    :cond_1c
    move v5, v7

    goto/16 :goto_6

    .line 214090
    :cond_1d
    iget-object p2, v1, LX/1Br;->d:Ljava/util/regex/Pattern;

    if-eqz p2, :cond_1e

    .line 214091
    iget-object p2, v1, LX/1Br;->d:Ljava/util/regex/Pattern;

    invoke-virtual {p2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p2

    .line 214092
    invoke-virtual {p2}, Ljava/util/regex/Matcher;->find()Z

    move-result p2

    if-eqz p2, :cond_1e

    move v5, p0

    .line 214093
    goto/16 :goto_8

    .line 214094
    :cond_1e
    iget-object p2, v1, LX/1Br;->e:Ljava/util/regex/Pattern;

    if-eqz p2, :cond_e

    .line 214095
    iget-object p2, v1, LX/1Br;->e:Ljava/util/regex/Pattern;

    invoke-virtual {p2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p2

    .line 214096
    invoke-virtual {p2}, Ljava/util/regex/Matcher;->find()Z

    move-result p2

    if-eqz p2, :cond_e

    move v5, p0

    .line 214097
    goto/16 :goto_8
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/1By;)Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;
    .locals 4

    .prologue
    .line 213991
    new-instance v0, Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    iget-object v1, p0, LX/1Bn;->v:LX/2nB;

    invoke-virtual {v1, p2}, LX/2nB;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 213992
    iget-object v2, p3, LX/1By;->d:Ljava/lang/String;

    move-object v2, v2

    .line 213993
    iget-object v3, p3, LX/1By;->e:Ljava/lang/String;

    move-object v3, v3

    .line 213994
    invoke-direct {v0, p1, v1, v2, v3}, Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Z)Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 213979
    iget-object v0, p0, LX/1Bn;->i:LX/3n3;

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 213980
    :goto_0
    return-object v0

    .line 213981
    :cond_1
    invoke-static {p1}, LX/047;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 213982
    iget-object v0, p0, LX/1Bn;->j:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 213983
    if-nez v0, :cond_2

    move-object v0, v1

    move-object v1, p1

    .line 213984
    :goto_1
    const/4 v3, 0x1

    invoke-static {p0, p1, v0, v3}, LX/1Bn;->a(LX/1Bn;Ljava/lang/String;Ljava/lang/String;Z)LX/1By;

    move-result-object v3

    .line 213985
    if-nez v3, :cond_3

    move-object v0, v2

    .line 213986
    goto :goto_0

    .line 213987
    :cond_2
    invoke-static {v0}, LX/047;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_1

    .line 213988
    :cond_3
    if-eqz p2, :cond_4

    invoke-virtual {v3}, LX/1By;->h()Z

    move-result v4

    if-eqz v4, :cond_4

    move-object v0, v2

    .line 213989
    goto :goto_0

    .line 213990
    :cond_4
    invoke-virtual {p0, v1, v0, v3}, LX/1Bn;->a(Ljava/lang/String;Ljava/lang/String;LX/1By;)Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 213973
    if-nez p1, :cond_1

    .line 213974
    :cond_0
    :goto_0
    return-object p1

    .line 213975
    :cond_1
    iget-object v0, p0, LX/1Bn;->j:Ljava/util/Map;

    invoke-static {p1}, LX/047;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 213976
    if-eqz v0, :cond_0

    move-object p1, v0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 213966
    iget-object v0, p0, LX/1Bn;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 213967
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Bn;->i:LX/3n3;

    .line 213968
    iget-object v0, p0, LX/1Bn;->v:LX/2nB;

    invoke-virtual {v0}, LX/2nB;->b()Ljava/io/File;

    move-result-object v0

    .line 213969
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213970
    invoke-static {v0}, LX/2W9;->a(Ljava/io/File;)Z

    .line 213971
    :cond_0
    iget-object v0, p0, LX/1Bn;->s:LX/2n7;

    invoke-virtual {v0}, LX/2n7;->b()V

    .line 213972
    return-void
.end method

.method public final c()Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 213938
    const-string v2, "Start filling in in_app_browser cache"

    const v3, 0x33493e52

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 213939
    :try_start_0
    iget-object v2, p0, LX/1Bn;->v:LX/2nB;

    invoke-virtual {v2}, LX/2nB;->b()Ljava/io/File;

    move-result-object v3

    .line 213940
    iget-object v2, p0, LX/1Bn;->i:LX/3n3;

    if-eqz v2, :cond_8

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 213941
    const/4 v2, 0x1

    .line 213942
    :goto_0
    move v2, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213943
    if-eqz v2, :cond_0

    .line 213944
    const v1, 0x64f96467

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_1
    return v0

    .line 213945
    :cond_0
    :try_start_1
    invoke-virtual {p0}, LX/1Bn;->d()Z

    move-result v2

    if-nez v2, :cond_1

    .line 213946
    invoke-virtual {p0}, LX/1Bn;->b()V

    .line 213947
    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    .line 213948
    invoke-virtual {p0}, LX/1Bn;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 213949
    iget-object v2, p0, LX/1Bn;->s:LX/2n7;

    invoke-virtual {v2}, LX/2n7;->b()V

    .line 213950
    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result v2

    move v2, v2

    .line 213951
    :goto_2
    if-eqz v2, :cond_7

    .line 213952
    iget-object v2, p0, LX/1Bn;->f:LX/0ad;

    sget v4, LX/1Bi;->g:I

    const/high16 v5, 0xa00000

    invoke-interface {v2, v4, v5}, LX/0ad;->a(II)I

    move-result v2

    .line 213953
    invoke-virtual {v3}, Ljava/io/File;->getUsableSpace()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    .line 213954
    mul-int/lit8 v3, v2, 0x5

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-gez v3, :cond_5

    .line 213955
    const v0, 0x747076f6

    invoke-static {v0}, LX/02m;->a(I)V

    move v0, v1

    goto :goto_1

    .line 213956
    :cond_3
    :try_start_2
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    if-eqz v2, :cond_4

    move v2, v0

    .line 213957
    goto :goto_2

    .line 213958
    :cond_4
    const v0, 0x70abbc86

    invoke-static {v0}, LX/02m;->a(I)V

    move v0, v1

    goto :goto_1

    .line 213959
    :cond_5
    :try_start_3
    new-instance v1, LX/3n3;

    invoke-direct {v1, p0, v2}, LX/3n3;-><init>(LX/1Bn;I)V

    iput-object v1, p0, LX/1Bn;->i:LX/3n3;

    .line 213960
    iget-object v1, p0, LX/1Bn;->f:LX/0ad;

    sget-wide v2, LX/1Bi;->h:J

    sget-wide v4, LX/1Bn;->a:J

    invoke-interface {v1, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v2

    sput-wide v2, LX/1Bn;->a:J

    .line 213961
    iget-object v1, p0, LX/1Bn;->v:LX/2nB;

    .line 213962
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, LX/2nB;->b()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-char v3, Ljava/io/File;->separatorChar:C

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/2nB;->b:Ljava/lang/String;

    .line 213963
    invoke-virtual {p0}, LX/1Bn;->d()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 213964
    invoke-static {p0}, LX/1Bn;->g(LX/1Bn;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 213965
    :cond_6
    const v1, -0x323551c7

    invoke-static {v1}, LX/02m;->a(I)V

    goto/16 :goto_1

    :cond_7
    const v0, -0x7ef5e310

    invoke-static {v0}, LX/02m;->a(I)V

    move v0, v1

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    const v1, -0x42494af1

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 213936
    iget-object v0, p0, LX/1Bn;->r:LX/0Uh;

    const/16 v1, 0x597

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 213937
    iget-object v1, p0, LX/1Bn;->f:LX/0ad;

    sget-short v2, LX/1Bi;->f:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 213935
    iget-object v1, p0, LX/1Bn;->f:LX/0ad;

    sget-short v2, LX/0fe;->T:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/1Bn;->n:LX/0W3;

    sget-wide v2, LX/0X5;->dS:J

    invoke-interface {v1, v2, v3, v0}, LX/0W4;->a(JZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method
