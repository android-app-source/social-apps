.class public LX/0R8;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0QA;

.field private final c:Landroid/content/Context;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "LX/0Q4;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0RB;

.field private final f:LX/0RF;

.field public final g:LX/0R2;

.field private final h:Z

.field public final i:Z

.field private final j:Z

.field public final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;",
            "Lcom/facebook/inject/Binder;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0RI;",
            "LX/0RN;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0RI;",
            "LX/4fR;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/LibraryModule;",
            ">;>;"
        }
    .end annotation
.end field

.field private final p:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;>;"
        }
    .end annotation
.end field

.field private final q:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "LX/0RC;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0RI;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0RI;",
            "LX/4fh;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0RI;",
            ">;"
        }
    .end annotation
.end field

.field private final u:LX/0RG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59580
    const-class v0, LX/0R8;

    sput-object v0, LX/0R8;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0QA;Landroid/content/Context;Ljava/util/List;ZLX/0R2;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QA;",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<+",
            "LX/0Q4;",
            ">;Z",
            "LX/0R2;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59360
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59361
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0R8;->k:Ljava/util/Map;

    .line 59362
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0R8;->l:Ljava/util/Map;

    .line 59363
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0R8;->m:Ljava/util/Map;

    .line 59364
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, LX/0R8;->n:Ljava/util/Map;

    .line 59365
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0R8;->o:Ljava/util/List;

    .line 59366
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/0R8;->p:Ljava/util/Set;

    .line 59367
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, LX/0R8;->q:Ljava/util/Map;

    .line 59368
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/0R8;->r:Ljava/util/Set;

    .line 59369
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0R8;->s:Ljava/util/Map;

    .line 59370
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/0R8;->t:Ljava/util/Set;

    .line 59371
    iput-object p1, p0, LX/0R8;->b:LX/0QA;

    .line 59372
    iput-object p2, p0, LX/0R8;->c:Landroid/content/Context;

    .line 59373
    iput-object p3, p0, LX/0R8;->d:Ljava/util/List;

    .line 59374
    new-instance v0, LX/0RB;

    invoke-direct {v0, p2}, LX/0RB;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/0R8;->e:LX/0RB;

    .line 59375
    new-instance v0, LX/0RF;

    iget-object v1, p0, LX/0R8;->e:LX/0RB;

    invoke-direct {v0, v1}, LX/0RF;-><init>(LX/0RB;)V

    iput-object v0, p0, LX/0R8;->f:LX/0RF;

    .line 59376
    iput-object p5, p0, LX/0R8;->g:LX/0R2;

    .line 59377
    iput-boolean p4, p0, LX/0R8;->h:Z

    .line 59378
    iget-object v0, p0, LX/0R8;->g:LX/0R2;

    .line 59379
    iget-boolean v1, v0, LX/0R2;->a:Z

    move v0, v1

    .line 59380
    iput-boolean v0, p0, LX/0R8;->i:Z

    .line 59381
    iget-object v0, p0, LX/0R8;->g:LX/0R2;

    .line 59382
    iget-boolean v1, v0, LX/0R2;->b:Z

    move v0, v1

    .line 59383
    iput-boolean v0, p0, LX/0R8;->j:Z

    .line 59384
    new-instance v0, LX/0RG;

    iget-object v1, p0, LX/0R8;->f:LX/0RF;

    iget-object v2, p0, LX/0R8;->e:LX/0RB;

    iget-object v3, p0, LX/0R8;->c:Landroid/content/Context;

    invoke-direct {v0, v1, v2, v3}, LX/0RG;-><init>(LX/0RF;LX/0RB;Landroid/content/Context;)V

    iput-object v0, p0, LX/0R8;->u:LX/0RG;

    .line 59385
    return-void
.end method

.method private static a(Ljava/lang/Class;)LX/0Q6;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/LibraryModule;",
            ">;)",
            "Lcom/facebook/inject/LibraryModule;"
        }
    .end annotation

    .prologue
    .line 59569
    const/4 v0, 0x0

    :try_start_0
    new-array v0, v0, [Ljava/lang/Class;

    invoke-virtual {p0, v0}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 59570
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 59571
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Q6;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3

    return-object v0

    .line 59572
    :catch_0
    move-exception v0

    .line 59573
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to find public default constructor for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 59574
    :catch_1
    move-exception v0

    .line 59575
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to invoke constructor for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 59576
    :catch_2
    move-exception v0

    .line 59577
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to instantiate "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 59578
    :catch_3
    move-exception v0

    .line 59579
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to access constructor for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(LX/0QA;LX/0Q4;LX/0RN;Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0QA;",
            "LX/0Q4;",
            "LX/0RN",
            "<TT;>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/LibraryModule;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 59505
    iget-object v0, p3, LX/0RN;->b:LX/0RI;

    move-object v3, v0

    .line 59506
    iget-object v0, p3, LX/0RN;->c:LX/0Or;

    move-object v2, v0

    .line 59507
    iput-object v2, p3, LX/0RN;->e:LX/0Or;

    .line 59508
    iget-object v0, p0, LX/0R8;->l:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0RN;

    .line 59509
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/0RN;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p3}, LX/0RN;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    sget-object v1, LX/4fX;->a:LX/0Xu;

    .line 59510
    iget-object v4, v0, LX/0RN;->a:Ljava/lang/String;

    move-object v4, v4

    .line 59511
    iget-object v5, v0, LX/0RN;->b:LX/0RI;

    move-object v5, v5

    .line 59512
    invoke-virtual {v5}, LX/0RI;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v4, v5}, LX/0Xu;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 59513
    instance-of v1, p2, LX/0S4;

    move v1, v1

    .line 59514
    if-nez v1, :cond_1

    .line 59515
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Module %s illegally overriding binding for %s from module %s. Either require module %s(base module) from %s or provide %s as a default binding so it can be overridden in module %s(top module) ."

    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/Object;

    .line 59516
    iget-object v4, p3, LX/0RN;->a:Ljava/lang/String;

    move-object v4, v4

    .line 59517
    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    .line 59518
    iget-object v4, v0, LX/0RN;->b:LX/0RI;

    move-object v4, v4

    .line 59519
    invoke-virtual {v4}, LX/0RI;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    .line 59520
    iget-object v4, v0, LX/0RN;->a:Ljava/lang/String;

    move-object v4, v4

    .line 59521
    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    .line 59522
    iget-object v4, v0, LX/0RN;->a:Ljava/lang/String;

    move-object v4, v4

    .line 59523
    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v9

    .line 59524
    iget-object v4, p3, LX/0RN;->a:Ljava/lang/String;

    move-object v4, v4

    .line 59525
    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v10

    const/4 v4, 0x5

    .line 59526
    iget-object v5, v0, LX/0RN;->b:LX/0RI;

    move-object v0, v5

    .line 59527
    invoke-virtual {v0}, LX/0RI;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x6

    .line 59528
    iget-object v4, p3, LX/0RN;->a:Ljava/lang/String;

    move-object v4, v4

    .line 59529
    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 59530
    :cond_1
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 59531
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    .line 59532
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 59533
    :cond_2
    if-eqz v0, :cond_3

    .line 59534
    iget-object v1, v0, LX/0RN;->a:Ljava/lang/String;

    move-object v1, v1

    .line 59535
    invoke-interface {v4, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 59536
    instance-of v1, p2, LX/0S4;

    move v1, v1

    .line 59537
    if-nez v1, :cond_3

    .line 59538
    iget-object v1, v0, LX/0RN;->a:Ljava/lang/String;

    move-object v1, v1

    .line 59539
    iget-object v4, p3, LX/0RN;->a:Ljava/lang/String;

    move-object v4, v4

    .line 59540
    if-eq v1, v4, :cond_3

    sget-object v1, LX/4fX;->a:LX/0Xu;

    .line 59541
    iget-object v4, v0, LX/0RN;->a:Ljava/lang/String;

    move-object v4, v4

    .line 59542
    iget-object v5, v0, LX/0RN;->b:LX/0RI;

    move-object v5, v5

    .line 59543
    invoke-virtual {v5}, LX/0RI;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v4, v5}, LX/0Xu;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 59544
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Module %s is overriding binding for %s from module %s, but does not require that module. Add %s(base module) in the dependency list of %s."

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    .line 59545
    iget-object v4, p3, LX/0RN;->a:Ljava/lang/String;

    move-object v4, v4

    .line 59546
    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    .line 59547
    iget-object v4, v0, LX/0RN;->b:LX/0RI;

    move-object v4, v4

    .line 59548
    invoke-virtual {v4}, LX/0RI;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    .line 59549
    iget-object v4, v0, LX/0RN;->a:Ljava/lang/String;

    move-object v4, v4

    .line 59550
    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    .line 59551
    iget-object v4, v0, LX/0RN;->a:Ljava/lang/String;

    move-object v0, v4

    .line 59552
    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v9

    .line 59553
    iget-object v0, p3, LX/0RN;->a:Ljava/lang/String;

    move-object v0, v0

    .line 59554
    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v10

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 59555
    :cond_3
    instance-of v0, v2, LX/0RV;

    if-eqz v0, :cond_4

    move-object v0, v2

    .line 59556
    check-cast v0, LX/0RV;

    .line 59557
    iput-object p1, v0, LX/0RV;->mInjector:LX/0QB;

    .line 59558
    :cond_4
    iget-object v0, p3, LX/0RN;->d:Ljava/lang/Class;

    move-object v0, v0

    .line 59559
    if-eqz v0, :cond_8

    iget-boolean v0, p0, LX/0R8;->i:Z

    if-nez v0, :cond_8

    .line 59560
    iget-object v0, p3, LX/0RN;->d:Ljava/lang/Class;

    move-object v0, v0

    .line 59561
    invoke-direct {p0, v0}, LX/0R8;->b(Ljava/lang/Class;)LX/0RC;

    move-result-object v0

    invoke-interface {v0, v2}, LX/0RC;->a(LX/0Or;)LX/0Or;

    move-result-object v1

    .line 59562
    instance-of v0, v1, LX/0RV;

    if-eqz v0, :cond_5

    move-object v0, v1

    .line 59563
    check-cast v0, LX/0RV;

    .line 59564
    iput-object p1, v0, LX/0RV;->mInjector:LX/0QB;

    .line 59565
    :cond_5
    :goto_1
    iget-boolean v0, p0, LX/0R8;->h:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, LX/0R8;->i:Z

    if-eqz v0, :cond_7

    :cond_6
    new-instance v0, LX/4fq;

    invoke-direct {v0, v3, v1}, LX/4fq;-><init>(LX/0RI;LX/0Or;)V

    move-object v1, v0

    .line 59566
    :cond_7
    iput-object v1, p3, LX/0RN;->c:LX/0Or;

    .line 59567
    iget-object v0, p0, LX/0R8;->l:Ljava/util/Map;

    invoke-interface {v0, v3, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59568
    return-void

    :cond_8
    move-object v1, v2

    goto :goto_1
.end method

.method private static a(LX/0R8;LX/0Q4;)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    .line 59449
    iget-boolean v0, p0, LX/0R8;->i:Z

    if-eqz v0, :cond_2

    .line 59450
    iget-object v0, p0, LX/0R8;->g:LX/0R2;

    .line 59451
    iget-boolean v1, v0, LX/0R2;->a:Z

    const-string v2, "Verification mode is disabled."

    invoke-static {v1, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 59452
    iget-object v1, v0, LX/0R2;->d:LX/4fe;

    move-object v0, v1

    .line 59453
    invoke-interface {v0}, LX/4fe;->a()LX/0RH;

    move-result-object v0

    move-object v2, v0

    .line 59454
    :goto_0
    invoke-interface {p1, v2}, LX/0Q4;->configure(LX/0RH;)V

    .line 59455
    iget-object v0, p0, LX/0R8;->k:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59456
    iget-boolean v0, p0, LX/0R8;->i:Z

    if-eqz v0, :cond_0

    .line 59457
    iget-object v0, p0, LX/0R8;->g:LX/0R2;

    invoke-virtual {v0}, LX/0R2;->d()LX/4ff;

    .line 59458
    :cond_0
    iget-object v0, p0, LX/0R8;->p:Ljava/util/Set;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 59459
    iget-object v0, p0, LX/0R8;->q:Ljava/util/Map;

    invoke-virtual {v2}, LX/0RH;->h()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 59460
    invoke-virtual {v2}, LX/0RH;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 59461
    iget-object v3, p0, LX/0R8;->n:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 59462
    invoke-static {v0}, LX/0R8;->a(Ljava/lang/Class;)LX/0Q6;

    move-result-object v3

    .line 59463
    iget-object v4, p0, LX/0R8;->n:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v4, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59464
    invoke-static {p0, v3}, LX/0R8;->a(LX/0R8;LX/0Q4;)V

    .line 59465
    iget-object v3, p0, LX/0R8;->o:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 59466
    :cond_2
    new-instance v0, LX/0RH;

    invoke-direct {v0, p1}, LX/0RH;-><init>(LX/0Q4;)V

    move-object v2, v0

    goto :goto_0

    .line 59467
    :cond_3
    iget-object v0, p0, LX/0R8;->b:LX/0QA;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0QA;->getModuleInjector(Ljava/lang/Class;)LX/0QA;

    move-result-object v3

    .line 59468
    invoke-virtual {v2}, LX/0RH;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0RN;

    .line 59469
    invoke-virtual {v2}, LX/0RH;->f()Ljava/util/List;

    move-result-object v4

    invoke-direct {p0, v3, p1, v0, v4}, LX/0R8;->a(LX/0QA;LX/0Q4;LX/0RN;Ljava/util/List;)V

    goto :goto_2

    .line 59470
    :cond_4
    invoke-virtual {v2}, LX/0RH;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4fR;

    .line 59471
    iget-object v1, v0, LX/4fR;->c:LX/4B4;

    move-object v1, v1

    .line 59472
    instance-of v5, v1, LX/4B4;

    if-eqz v5, :cond_5

    .line 59473
    check-cast v1, LX/4B4;

    .line 59474
    iput-object v3, v1, LX/4B4;->mInjector:LX/0QB;

    .line 59475
    :cond_5
    iget-object v1, v0, LX/4fR;->b:LX/0RI;

    move-object v1, v1

    .line 59476
    iget-object v5, p0, LX/0R8;->m:Ljava/util/Map;

    invoke-interface {v5, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 59477
    :cond_6
    iget-object v0, p0, LX/0R8;->r:Ljava/util/Set;

    invoke-virtual {v2}, LX/0RH;->c()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 59478
    invoke-virtual {v2}, LX/0RH;->c()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v2}, LX/0RH;->d()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 59479
    const-string v3, "set1"

    invoke-static {v0, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59480
    const-string v3, "set2"

    invoke-static {v1, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59481
    invoke-static {v1, v0}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v3

    .line 59482
    new-instance v4, LX/0Rp;

    invoke-direct {v4, v0, v3, v1}, LX/0Rp;-><init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    move-object v0, v4

    .line 59483
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0RI;

    .line 59484
    iget-object v1, p0, LX/0R8;->s:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4fh;

    .line 59485
    if-nez v1, :cond_7

    .line 59486
    new-instance v1, LX/4fh;

    iget-object v4, p0, LX/0R8;->b:LX/0QA;

    invoke-direct {v1, v4, v0}, LX/4fh;-><init>(LX/0QA;LX/0RI;)V

    .line 59487
    iget-object v4, p0, LX/0R8;->s:Ljava/util/Map;

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 59488
    :cond_8
    invoke-virtual {v2}, LX/0RH;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 59489
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0RI;

    .line 59490
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4fl;

    .line 59491
    iget-object v4, p0, LX/0R8;->s:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4fh;

    .line 59492
    iget-object v4, v0, LX/4fl;->b:LX/4fk;

    move-object v0, v4

    .line 59493
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0RI;

    .line 59494
    const/4 v7, 0x0

    iget-object v8, v1, LX/4fh;->d:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    move v8, v7

    :goto_7
    if-ge v8, v0, :cond_d

    .line 59495
    iget-object v7, v1, LX/4fh;->d:Ljava/util/List;

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0RI;

    invoke-virtual {v7, v4}, LX/0RI;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 59496
    :goto_8
    goto :goto_6

    .line 59497
    :cond_9
    iget-object v4, v1, LX/4fh;->d:Ljava/util/List;

    sget-object v5, LX/4fh;->a:Ljava/util/Comparator;

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 59498
    goto :goto_5

    .line 59499
    :cond_a
    iget-object v0, p0, LX/0R8;->t:Ljava/util/Set;

    invoke-virtual {v2}, LX/0RH;->e()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 59500
    iget-object v0, p0, LX/0R8;->u:LX/0RG;

    if-ne p1, v0, :cond_b

    .line 59501
    iget-object v0, p0, LX/0R8;->n:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59502
    :cond_b
    return-void

    .line 59503
    :cond_c
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_7

    .line 59504
    :cond_d
    iget-object v7, v1, LX/4fh;->d:Ljava/util/List;

    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8
.end method

.method private b(Ljava/lang/Class;)LX/0RC;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0RC;"
        }
    .end annotation

    .prologue
    .line 59445
    iget-object v0, p0, LX/0R8;->q:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0RC;

    .line 59446
    if-nez v0, :cond_0

    .line 59447
    new-instance v0, LX/4fr;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No scope registered for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59448
    :cond_0
    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 59433
    const-string v0, "FbInjectorImpl.init#multiBinding"

    const v1, -0x76cc9307

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 59434
    :try_start_0
    iget-object v0, p0, LX/0R8;->s:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4fh;

    .line 59435
    new-instance v2, LX/0RN;

    invoke-direct {v2}, LX/0RN;-><init>()V

    .line 59436
    invoke-virtual {v0}, LX/4fh;->c()LX/0RI;

    move-result-object v3

    .line 59437
    iput-object v3, v2, LX/0RN;->b:LX/0RI;

    .line 59438
    invoke-virtual {v0}, LX/4fh;->b()LX/0Or;

    move-result-object v3

    .line 59439
    iput-object v3, v2, LX/0RN;->c:LX/0Or;

    .line 59440
    invoke-virtual {v0}, LX/4fh;->b()LX/0Or;

    move-result-object v3

    .line 59441
    iput-object v3, v2, LX/0RN;->e:LX/0Or;

    .line 59442
    iget-object v3, p0, LX/0R8;->l:Ljava/util/Map;

    invoke-virtual {v0}, LX/4fh;->c()LX/0RI;

    move-result-object v0

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 59443
    :catchall_0
    move-exception v0

    const v1, 0x2f35f30

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_0
    const v0, 0x336f1d19

    invoke-static {v0}, LX/02m;->a(I)V

    .line 59444
    return-void
.end method

.method private d()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 59416
    const/4 v4, 0x1

    .line 59417
    iget-object v0, p0, LX/0R8;->s:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v3, v5

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4fh;

    .line 59418
    iget-object v1, v0, LX/4fh;->c:LX/0RI;

    move-object v7, v1

    .line 59419
    iget-object v0, p0, LX/0R8;->l:Ljava/util/Map;

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0RN;

    .line 59420
    if-eqz v0, :cond_5

    .line 59421
    iget-object v1, p0, LX/0R8;->k:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 59422
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0RH;

    invoke-virtual {v2}, LX/0RH;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 59423
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    move-object v2, v0

    .line 59424
    :goto_1
    if-nez v3, :cond_3

    .line 59425
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 59426
    :goto_2
    add-int/lit8 v1, v4, 0x1

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 59427
    const-string v3, ": "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59428
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " by "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v3, v0

    move v0, v1

    :goto_3
    move v4, v0

    .line 59429
    goto :goto_0

    .line 59430
    :cond_1
    if-eqz v3, :cond_2

    .line 59431
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "One or more multibind keys were illegally bound:\n"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59432
    :cond_2
    return-void

    :cond_3
    move-object v0, v3

    goto :goto_2

    :cond_4
    move-object v2, v5

    goto :goto_1

    :cond_5
    move v0, v4

    goto :goto_3
.end method


# virtual methods
.method public final a()LX/0S5;
    .locals 6

    .prologue
    .line 59401
    const-string v0, "FbInjectorImpl.init#modules"

    const v1, -0x1cb3beb0

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 59402
    :try_start_0
    iget-object v0, p0, LX/0R8;->u:LX/0RG;

    invoke-static {p0, v0}, LX/0R8;->a(LX/0R8;LX/0Q4;)V

    .line 59403
    iget-object v0, p0, LX/0R8;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Q4;

    .line 59404
    invoke-static {p0, v0}, LX/0R8;->a(LX/0R8;LX/0Q4;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 59405
    :catchall_0
    move-exception v0

    const v1, -0x5e06b177

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_0
    const v0, 0x71f07be7

    invoke-static {v0}, LX/02m;->a(I)V

    .line 59406
    invoke-direct {p0}, LX/0R8;->c()V

    .line 59407
    invoke-direct {p0}, LX/0R8;->d()V

    .line 59408
    const-string v0, "FbInjectorImpl.init#assertBinding"

    const v1, -0x57fa3482

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 59409
    :try_start_1
    iget-object v0, p0, LX/0R8;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0RI;

    .line 59410
    iget-object v2, p0, LX/0R8;->l:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-boolean v2, p0, LX/0R8;->i:Z

    if-nez v2, :cond_1

    .line 59411
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No binding for required key "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 59412
    :catchall_1
    move-exception v0

    const v1, -0x7d0f43ed

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 59413
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/0R8;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 59414
    const v0, -0x6c0817fd

    invoke-static {v0}, LX/02m;->a(I)V

    .line 59415
    new-instance v0, LX/0S5;

    iget-object v1, p0, LX/0R8;->k:Ljava/util/Map;

    iget-object v2, p0, LX/0R8;->e:LX/0RB;

    iget-object v3, p0, LX/0R8;->l:Ljava/util/Map;

    iget-object v4, p0, LX/0R8;->m:Ljava/util/Map;

    iget-object v5, p0, LX/0R8;->o:Ljava/util/List;

    invoke-direct/range {v0 .. v5}, LX/0S5;-><init>(Ljava/util/Map;LX/0RB;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;)V

    return-object v0
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 59386
    const-string v0, "FbInjectorInitializer.runPostInitLogic"

    const v1, 0x6d5e5952

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 59387
    :try_start_0
    iget-object v0, p0, LX/0R8;->q:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0RC;

    .line 59388
    instance-of v2, v0, LX/0RD;

    if-eqz v2, :cond_0

    .line 59389
    const-string v2, "Initializing scope: %s"

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const v4, -0x57a1cb58

    invoke-static {v2, v3, v4}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59390
    :try_start_1
    check-cast v0, LX/0RD;

    iget-object v2, p0, LX/0R8;->b:LX/0QA;

    invoke-interface {v0, v2}, LX/0RD;->a(LX/0QA;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 59391
    const v0, -0x2a50f224

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 59392
    :catchall_0
    move-exception v0

    const v1, 0x236e4b6d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 59393
    :catchall_1
    move-exception v0

    const v1, 0x273855c2

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 59394
    :cond_1
    iget-boolean v0, p0, LX/0R8;->j:Z

    if-nez v0, :cond_2

    .line 59395
    iget-object v0, p0, LX/0R8;->u:LX/0RG;

    iget-object v1, p0, LX/0R8;->b:LX/0QA;

    iget-object v2, p0, LX/0R8;->u:LX/0RG;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0QA;->getModuleInjector(Ljava/lang/Class;)LX/0QA;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0RG;->a(LX/0QA;)V

    .line 59396
    :cond_2
    iget-object v0, p0, LX/0R8;->l:Ljava/util/Map;

    .line 59397
    sget-object v1, LX/0SN;->a:Ljava/lang/Class;

    const-string v2, "Verify"

    invoke-static {v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 59398
    new-instance v1, LX/0SN;

    invoke-direct {v1, v0}, LX/0SN;-><init>(Ljava/util/Map;)V

    invoke-static {v1}, LX/0SN;->a(LX/0SN;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59399
    const v0, -0x1239b67f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 59400
    return-void
.end method
