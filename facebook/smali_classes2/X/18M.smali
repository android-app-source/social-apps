.class public LX/18M;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 206345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206346
    return-void
.end method

.method public static a(LX/16o;)Lcom/facebook/graphql/model/SponsoredImpression;
    .locals 2

    .prologue
    .line 206339
    invoke-static {p0}, LX/0x1;->a(LX/16o;)Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    .line 206340
    if-nez v0, :cond_0

    .line 206341
    invoke-interface {p0}, LX/16o;->M_()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    .line 206342
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v1

    .line 206343
    iput-object v0, v1, LX/0x2;->b:Lcom/facebook/graphql/model/SponsoredImpression;

    .line 206344
    :cond_0
    return-object v0
.end method

.method public static a(LX/17x;)Lcom/facebook/graphql/model/SponsoredImpression;
    .locals 1

    .prologue
    .line 206338
    invoke-interface {p0}, LX/17x;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/model/SponsoredImpression;->a(Lcom/facebook/graphql/model/GraphQLSponsoredData;)Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1Fa;Z)V
    .locals 3

    .prologue
    .line 206327
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    if-eqz v0, :cond_0

    .line 206328
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    .line 206329
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 206330
    iput-boolean p1, v0, LX/0x2;->n:Z

    .line 206331
    :goto_0
    return-void

    .line 206332
    :cond_0
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayKnowFeedUnitItem;

    if-eqz v0, :cond_1

    .line 206333
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayKnowFeedUnitItem;

    .line 206334
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 206335
    iput-boolean p1, v0, LX/0x2;->n:Z

    .line 206336
    goto :goto_0

    .line 206337
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Type doesn\'t have isImpressionLogged property:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/Sponsorable;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 206322
    instance-of v0, p0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    if-eqz v0, :cond_1

    .line 206323
    invoke-static {p0}, LX/0x1;->b(Lcom/facebook/graphql/model/Sponsorable;)I

    move-result v0

    or-int/2addr v0, p1

    invoke-static {p0, v0}, LX/0x1;->a(Lcom/facebook/graphql/model/Sponsorable;I)V

    .line 206324
    :cond_0
    :goto_0
    return-void

    .line 206325
    :cond_1
    if-ne p1, v1, :cond_0

    .line 206326
    invoke-static {p0, v1}, LX/0x1;->a(Lcom/facebook/graphql/model/Sponsorable;Z)V

    goto :goto_0
.end method

.method public static a(LX/1Fa;)Z
    .locals 3

    .prologue
    .line 206309
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    if-eqz v0, :cond_0

    .line 206310
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    .line 206311
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 206312
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 206313
    move v0, v0

    .line 206314
    :goto_0
    return v0

    .line 206315
    :cond_0
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayKnowFeedUnitItem;

    if-eqz v0, :cond_1

    .line 206316
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayKnowFeedUnitItem;

    .line 206317
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 206318
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 206319
    move v0, v0

    .line 206320
    goto :goto_0

    .line 206321
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Type doesn\'t have isImpressionLogged property:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/Sponsorable;)Z
    .locals 2

    .prologue
    .line 206293
    invoke-interface {p0}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    .line 206294
    if-eqz v0, :cond_0

    sget-object v1, Lcom/facebook/graphql/model/SponsoredImpression;->n:Lcom/facebook/graphql/model/SponsoredImpression;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/Sponsorable;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 206304
    instance-of v0, p0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 206305
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-interface {v0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->I_()I

    move-result v0

    .line 206306
    invoke-static {p0}, LX/0x1;->b(Lcom/facebook/graphql/model/Sponsorable;)I

    move-result v1

    shl-int v0, v2, v0

    or-int/2addr v0, v1

    invoke-static {p0, v0}, LX/0x1;->a(Lcom/facebook/graphql/model/Sponsorable;I)V

    .line 206307
    :goto_0
    return-void

    .line 206308
    :cond_0
    invoke-static {p0, v2}, LX/0x1;->a(Lcom/facebook/graphql/model/Sponsorable;Z)V

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/Sponsorable;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 206298
    instance-of v0, p0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 206299
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-interface {v0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->I_()I

    move-result v0

    .line 206300
    invoke-static {p0}, LX/0x1;->b(Lcom/facebook/graphql/model/Sponsorable;)I

    move-result v2

    shl-int v0, v1, v0

    and-int/2addr v0, v2

    if-eqz v0, :cond_0

    move v0, v1

    .line 206301
    :goto_0
    return v0

    .line 206302
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 206303
    :cond_1
    invoke-static {p0}, LX/0x1;->a(Lcom/facebook/graphql/model/Sponsorable;)Z

    move-result v0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/Sponsorable;)I
    .locals 1

    .prologue
    .line 206295
    instance-of v0, p0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    if-eqz v0, :cond_0

    .line 206296
    invoke-static {p0}, LX/0x1;->b(Lcom/facebook/graphql/model/Sponsorable;)I

    move-result v0

    .line 206297
    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, LX/0x1;->a(Lcom/facebook/graphql/model/Sponsorable;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
