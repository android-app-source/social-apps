.class public LX/0l2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static volatile d:LX/0l2;


# instance fields
.field public b:Z

.field private c:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 128181
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "do_not_crash"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0l2;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 128182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128183
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0l2;->b:Z

    .line 128184
    iput-object p1, p0, LX/0l2;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 128185
    return-void
.end method

.method public static a(LX/0QB;)LX/0l2;
    .locals 4

    .prologue
    .line 128186
    sget-object v0, LX/0l2;->d:LX/0l2;

    if-nez v0, :cond_1

    .line 128187
    const-class v1, LX/0l2;

    monitor-enter v1

    .line 128188
    :try_start_0
    sget-object v0, LX/0l2;->d:LX/0l2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 128189
    if-eqz v2, :cond_0

    .line 128190
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 128191
    new-instance p0, LX/0l2;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/0l2;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 128192
    move-object v0, p0

    .line 128193
    sput-object v0, LX/0l2;->d:LX/0l2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 128194
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 128195
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 128196
    :cond_1
    sget-object v0, LX/0l2;->d:LX/0l2;

    return-object v0

    .line 128197
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 128198
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 128199
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 128200
    if-nez v1, :cond_0

    .line 128201
    :goto_0
    return v0

    .line 128202
    :cond_0
    iget-boolean v1, p0, LX/0l2;->b:Z

    if-eqz v1, :cond_1

    .line 128203
    const/4 v0, 0x1

    goto :goto_0

    .line 128204
    :cond_1
    iget-object v1, p0, LX/0l2;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0l2;->a:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/0l2;->b:Z

    .line 128205
    iget-boolean v0, p0, LX/0l2;->b:Z

    goto :goto_0
.end method
