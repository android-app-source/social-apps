.class public abstract LX/1iY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1iZ;


# instance fields
.field private a:Lorg/apache/http/HttpRequest;

.field private b:Lorg/apache/http/protocol/HttpContext;

.field public c:Lorg/apache/http/HttpResponse;

.field public d:LX/1iW;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 298581
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lorg/apache/http/protocol/HttpContext;
    .locals 2

    .prologue
    .line 298578
    iget-object v0, p0, LX/1iY;->a:Lorg/apache/http/HttpRequest;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Did you forget to call super.beginRequest?"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 298579
    iget-object v0, p0, LX/1iY;->b:Lorg/apache/http/protocol/HttpContext;

    return-object v0

    .line 298580
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V
    .locals 6
    .param p3    # Lorg/apache/http/HttpResponse;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 298566
    if-eqz p5, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 298567
    invoke-virtual {p0}, LX/1iY;->b()Lorg/apache/http/HttpRequest;

    move-result-object v0

    if-ne v0, p2, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 298568
    invoke-virtual {p0}, LX/1iY;->a()Lorg/apache/http/protocol/HttpContext;

    move-result-object v0

    if-ne v0, p4, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 298569
    iget-object v0, p0, LX/1iY;->c:Lorg/apache/http/HttpResponse;

    move-object v0, v0

    .line 298570
    if-ne v0, p3, :cond_3

    move v0, v1

    :goto_3
    const-string v3, "stored %s, passed %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 298571
    iget-object v5, p0, LX/1iY;->c:Lorg/apache/http/HttpResponse;

    move-object v5, v5

    .line 298572
    aput-object v5, v4, v2

    aput-object p3, v4, v1

    invoke-static {v0, v3, v4}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 298573
    return-void

    :cond_0
    move v0, v2

    .line 298574
    goto :goto_0

    :cond_1
    move v0, v2

    .line 298575
    goto :goto_1

    :cond_2
    move v0, v2

    .line 298576
    goto :goto_2

    :cond_3
    move v0, v2

    .line 298577
    goto :goto_3
.end method

.method public a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 298559
    invoke-virtual {p0}, LX/1iY;->b()Lorg/apache/http/HttpRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 298560
    invoke-virtual {p0}, LX/1iY;->a()Lorg/apache/http/protocol/HttpContext;

    move-result-object v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 298561
    iput-object p1, p0, LX/1iY;->a:Lorg/apache/http/HttpRequest;

    .line 298562
    iput-object p2, p0, LX/1iY;->b:Lorg/apache/http/protocol/HttpContext;

    .line 298563
    return-void

    :cond_0
    move v0, v2

    .line 298564
    goto :goto_0

    :cond_1
    move v1, v2

    .line 298565
    goto :goto_1
.end method

.method public a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;LX/1iW;)V
    .locals 1

    .prologue
    .line 298555
    iput-object p1, p0, LX/1iY;->a:Lorg/apache/http/HttpRequest;

    .line 298556
    iput-object p2, p0, LX/1iY;->b:Lorg/apache/http/protocol/HttpContext;

    .line 298557
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1iW;

    iput-object v0, p0, LX/1iY;->d:LX/1iW;

    .line 298558
    return-void
.end method

.method public a(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 1

    .prologue
    .line 298542
    iput-object p1, p0, LX/1iY;->c:Lorg/apache/http/HttpResponse;

    .line 298543
    invoke-virtual {p0}, LX/1iY;->a()Lorg/apache/http/protocol/HttpContext;

    move-result-object v0

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 298544
    return-void

    .line 298545
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lorg/apache/http/HttpRequest;
    .locals 2

    .prologue
    .line 298552
    iget-object v0, p0, LX/1iY;->a:Lorg/apache/http/HttpRequest;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Did you forget to call super.beginRequest?"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 298553
    iget-object v0, p0, LX/1iY;->a:Lorg/apache/http/HttpRequest;

    return-object v0

    .line 298554
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 298546
    iget-object v0, p0, LX/1iY;->c:Lorg/apache/http/HttpResponse;

    move-object v0, v0

    .line 298547
    if-ne v0, p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 298548
    invoke-virtual {p0}, LX/1iY;->a()Lorg/apache/http/protocol/HttpContext;

    move-result-object v0

    if-ne v0, p2, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 298549
    return-void

    :cond_0
    move v0, v2

    .line 298550
    goto :goto_0

    :cond_1
    move v1, v2

    .line 298551
    goto :goto_1
.end method
