.class public LX/1Cg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialoguePinnedStoryQueryModel;",
            ">;",
            "LX/3j3;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/concurrent/Executor;

.field public final c:LX/0tX;

.field public final d:LX/0hB;

.field public final e:LX/0y2;

.field public final f:LX/0ad;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0hB;LX/0y2;LX/0ad;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 216680
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216681
    new-instance v0, LX/1Ch;

    invoke-direct {v0, p0}, LX/1Ch;-><init>(LX/1Cg;)V

    iput-object v0, p0, LX/1Cg;->a:LX/0QK;

    .line 216682
    iput-object p1, p0, LX/1Cg;->b:Ljava/util/concurrent/Executor;

    .line 216683
    iput-object p3, p0, LX/1Cg;->d:LX/0hB;

    .line 216684
    iput-object p2, p0, LX/1Cg;->c:LX/0tX;

    .line 216685
    iput-object p4, p0, LX/1Cg;->e:LX/0y2;

    .line 216686
    iput-object p5, p0, LX/1Cg;->f:LX/0ad;

    .line 216687
    return-void
.end method

.method public static b(LX/0QB;)LX/1Cg;
    .locals 6

    .prologue
    .line 216678
    new-instance v0, LX/1Cg;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v3

    check-cast v3, LX/0hB;

    invoke-static {p0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v4

    check-cast v4, LX/0y2;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct/range {v0 .. v5}, LX/1Cg;-><init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0hB;LX/0y2;LX/0ad;)V

    .line 216679
    return-object v0
.end method
