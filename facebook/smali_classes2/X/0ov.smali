.class public final LX/0ov;
.super LX/0R5;
.source ""

# interfaces
.implements LX/0R6;


# instance fields
.field public final synthetic a:LX/0S2;

.field private final b:LX/0QA;

.field private final c:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/0S2;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 1

    .prologue
    .line 143306
    iput-object p1, p0, LX/0ov;->a:LX/0S2;

    .line 143307
    iget-object v0, p1, LX/0S2;->g:LX/0QA;

    invoke-direct {p0, v0}, LX/0R5;-><init>(LX/0QA;)V

    .line 143308
    iget-object v0, p1, LX/0S2;->g:LX/0QA;

    iput-object v0, p0, LX/0ov;->b:LX/0QA;

    .line 143309
    iput-object p2, p0, LX/0ov;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 143310
    iget-object v0, p1, LX/0S2;->h:LX/0RB;

    .line 143311
    iget-object p1, v0, LX/0RB;->a:Landroid/content/Context;

    move-object v0, p1

    .line 143312
    iput-object v0, p0, LX/0ov;->d:Landroid/content/Context;

    .line 143313
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 143305
    iget-object v0, p0, LX/0ov;->a:LX/0S2;

    invoke-static {v0, p0}, LX/0S2;->a$redex0(LX/0S2;LX/0ov;)LX/0S7;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 143304
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "injectComponent should only be called on ContextScope"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 143302
    check-cast p1, LX/0S7;

    invoke-static {p1}, LX/0S2;->a(LX/0S7;)V

    .line 143303
    return-void
.end method

.method public final b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 143301
    iget-object v0, p0, LX/0ov;->d:Landroid/content/Context;

    return-object v0
.end method

.method public final d()LX/0SI;
    .locals 3

    .prologue
    .line 143300
    new-instance v1, LX/0pH;

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v0

    check-cast v0, LX/0WJ;

    iget-object v2, p0, LX/0ov;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-direct {v1, v0, v2}, LX/0pH;-><init>(LX/0WJ;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    return-object v1
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 143314
    const/4 v0, 0x0

    return v0
.end method

.method public final getInstance(LX/0RI;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 143297
    iget-object v0, p0, LX/0ov;->a:LX/0S2;

    invoke-static {v0, p0}, LX/0S2;->a$redex0(LX/0S2;LX/0ov;)LX/0S7;

    move-result-object v1

    .line 143298
    :try_start_0
    iget-object v0, p0, LX/0ov;->b:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getScopeUnawareInjector()LX/0QD;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getInstance(LX/0RI;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 143299
    invoke-static {v1}, LX/0S2;->a(LX/0S7;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/0S2;->a(LX/0S7;)V

    throw v0
.end method

.method public final getLazy(LX/0RI;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Ot",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 143296
    iget-object v0, p0, LX/0ov;->b:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getScopeUnawareInjector()LX/0QD;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    invoke-static {v0, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public final getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;>;)",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 143293
    iget-object v0, p0, LX/0ov;->a:LX/0S2;

    invoke-static {v0, p0}, LX/0S2;->a$redex0(LX/0S2;LX/0ov;)LX/0S7;

    move-result-object v1

    .line 143294
    :try_start_0
    iget-object v0, p0, LX/0ov;->b:LX/0QA;

    invoke-virtual {v0, p1}, LX/0QA;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 143295
    invoke-static {v1}, LX/0S2;->a(LX/0S7;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/0S2;->a(LX/0S7;)V

    throw v0
.end method

.method public final getProvider(LX/0RI;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 143290
    iget-object v0, p0, LX/0ov;->b:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getScopeUnawareInjector()LX/0QD;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    .line 143291
    new-instance p1, LX/42a;

    invoke-direct {p1, p0, v0}, LX/42a;-><init>(LX/0ov;LX/0Or;)V

    move-object v0, p1

    .line 143292
    return-object v0
.end method

.method public final getScopeAwareInjector()LX/0R6;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 143289
    return-object p0
.end method
