.class public LX/1B6;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/localstats/LocalStatsLogger;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1BA;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 212281
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1BA;
    .locals 4

    .prologue
    .line 212268
    sget-object v0, LX/1B6;->a:LX/1BA;

    if-nez v0, :cond_1

    .line 212269
    const-class v1, LX/1B6;

    monitor-enter v1

    .line 212270
    :try_start_0
    sget-object v0, LX/1B6;->a:LX/1BA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 212271
    if-eqz v2, :cond_0

    .line 212272
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 212273
    invoke-static {v0}, LX/13R;->a(LX/0QB;)LX/13S;

    move-result-object v3

    check-cast v3, LX/13S;

    invoke-static {v0}, LX/1B7;->a(LX/0QB;)Ljava/util/Set;

    move-result-object p0

    invoke-static {v3, p0}, LX/1B8;->a(LX/13S;Ljava/util/Set;)LX/1BA;

    move-result-object v3

    move-object v0, v3

    .line 212274
    sput-object v0, LX/1B6;->a:LX/1BA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212275
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 212276
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 212277
    :cond_1
    sget-object v0, LX/1B6;->a:LX/1BA;

    return-object v0

    .line 212278
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 212279
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 212280
    invoke-static {p0}, LX/13R;->a(LX/0QB;)LX/13S;

    move-result-object v0

    check-cast v0, LX/13S;

    invoke-static {p0}, LX/1B7;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1}, LX/1B8;->a(LX/13S;Ljava/util/Set;)LX/1BA;

    move-result-object v0

    return-object v0
.end method
