.class public LX/1IY;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1bh;",
            "LX/1FL;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 228894
    const-class v0, LX/1IY;

    sput-object v0, LX/1IY;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 228891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 228892
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1IY;->b:Ljava/util/Map;

    .line 228893
    return-void
.end method

.method private static declared-synchronized c(LX/1IY;)V
    .locals 1

    .prologue
    .line 228888
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1IY;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228889
    monitor-exit p0

    return-void

    .line 228890
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/1bh;)LX/1FL;
    .locals 6

    .prologue
    .line 228824
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228825
    iget-object v0, p0, LX/1IY;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FL;

    .line 228826
    if-eqz v0, :cond_0

    .line 228827
    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 228828
    :try_start_1
    invoke-static {v0}, LX/1FL;->e(LX/1FL;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 228829
    iget-object v1, p0, LX/1IY;->b:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228830
    sget-object v1, LX/1IY;->a:Ljava/lang/Class;

    const-string v2, "Found closed reference %d for key %s (%d)"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-interface {p1}, LX/1bh;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/03J;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 228831
    const/4 v1, 0x0

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    .line 228832
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 228833
    :cond_1
    :try_start_2
    invoke-static {v0}, LX/1FL;->a(LX/1FL;)LX/1FL;

    move-result-object v1

    .line 228834
    monitor-exit v0

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 228835
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/1bh;LX/1FL;)V
    .locals 2

    .prologue
    .line 228881
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228882
    invoke-static {p2}, LX/1FL;->e(LX/1FL;)Z

    move-result v0

    invoke-static {v0}, LX/03g;->a(Z)V

    .line 228883
    iget-object v0, p0, LX/1IY;->b:Ljava/util/Map;

    invoke-static {p2}, LX/1FL;->a(LX/1FL;)LX/1FL;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FL;

    .line 228884
    invoke-static {v0}, LX/1FL;->d(LX/1FL;)V

    .line 228885
    invoke-static {p0}, LX/1IY;->c(LX/1IY;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228886
    monitor-exit p0

    return-void

    .line 228887
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 228870
    monitor-enter p0

    .line 228871
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, LX/1IY;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 228872
    iget-object v0, p0, LX/1IY;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 228873
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228874
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 228875
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FL;

    .line 228876
    if-eqz v0, :cond_0

    .line 228877
    invoke-virtual {v0}, LX/1FL;->close()V

    .line 228878
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 228879
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 228880
    :cond_1
    return-void
.end method

.method public final declared-synchronized b(LX/1bh;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 228858
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228859
    iget-object v0, p0, LX/1IY;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 228860
    :goto_0
    monitor-exit p0

    return v0

    .line 228861
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1IY;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FL;

    .line 228862
    monitor-enter v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 228863
    :try_start_2
    invoke-static {v0}, LX/1FL;->e(LX/1FL;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 228864
    iget-object v2, p0, LX/1IY;->b:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228865
    sget-object v2, LX/1IY;->a:Ljava/lang/Class;

    const-string v3, "Found closed reference %d for key %s (%d)"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-interface {p1}, LX/1bh;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LX/03J;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 228866
    monitor-exit v0

    move v0, v1

    goto :goto_0

    .line 228867
    :cond_1
    monitor-exit v0

    move v0, v2

    goto :goto_0

    .line 228868
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 228869
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/1bh;LX/1FL;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 228836
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228837
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228838
    invoke-static {p2}, LX/1FL;->e(LX/1FL;)Z

    move-result v0

    invoke-static {v0}, LX/03g;->a(Z)V

    .line 228839
    iget-object v0, p0, LX/1IY;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FL;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 228840
    if-nez v0, :cond_0

    move v0, v1

    .line 228841
    :goto_0
    monitor-exit p0

    return v0

    .line 228842
    :cond_0
    :try_start_1
    invoke-virtual {v0}, LX/1FL;->a()LX/1FJ;

    move-result-object v2

    .line 228843
    invoke-virtual {p2}, LX/1FL;->a()LX/1FJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 228844
    if-eqz v2, :cond_1

    if-eqz v3, :cond_1

    :try_start_2
    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3}, LX/1FJ;->a()Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v5

    if-eq v4, v5, :cond_2

    .line 228845
    :cond_1
    :try_start_3
    invoke-static {v3}, LX/1FJ;->c(LX/1FJ;)V

    .line 228846
    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V

    .line 228847
    invoke-static {v0}, LX/1FL;->d(LX/1FL;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move v0, v1

    goto :goto_0

    .line 228848
    :cond_2
    :try_start_4
    iget-object v1, p0, LX/1IY;->b:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 228849
    :try_start_5
    invoke-static {v3}, LX/1FJ;->c(LX/1FJ;)V

    .line 228850
    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V

    .line 228851
    invoke-static {v0}, LX/1FL;->d(LX/1FL;)V

    .line 228852
    invoke-static {p0}, LX/1IY;->c(LX/1IY;)V

    .line 228853
    const/4 v0, 0x1

    goto :goto_0

    .line 228854
    :catchall_0
    move-exception v1

    invoke-static {v3}, LX/1FJ;->c(LX/1FJ;)V

    .line 228855
    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V

    .line 228856
    invoke-static {v0}, LX/1FL;->d(LX/1FL;)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 228857
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
