.class public LX/17W;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/17W;


# instance fields
.field private final a:LX/17Y;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/47S;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field private final d:LX/0id;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/common/uri/NativeUriInlineHandler;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/17Y;LX/0Ot;LX/0Ot;Lcom/facebook/content/SecureContextHelper;LX/0id;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/17Y;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/47S;",
            ">;>;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/common/uri/NativeUriInlineHandler;",
            ">;>;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0id;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 198180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198181
    iput-object p1, p0, LX/17W;->a:LX/17Y;

    .line 198182
    iput-object p2, p0, LX/17W;->b:LX/0Ot;

    .line 198183
    iput-object p3, p0, LX/17W;->e:LX/0Ot;

    .line 198184
    iput-object p4, p0, LX/17W;->c:Lcom/facebook/content/SecureContextHelper;

    .line 198185
    iput-object p5, p0, LX/17W;->d:LX/0id;

    .line 198186
    return-void
.end method

.method public static a(LX/0QB;)LX/17W;
    .locals 9

    .prologue
    .line 198162
    sget-object v0, LX/17W;->f:LX/17W;

    if-nez v0, :cond_1

    .line 198163
    const-class v1, LX/17W;

    monitor-enter v1

    .line 198164
    :try_start_0
    sget-object v0, LX/17W;->f:LX/17W;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 198165
    if-eqz v2, :cond_0

    .line 198166
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 198167
    new-instance v3, LX/17W;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v4

    check-cast v4, LX/17Y;

    .line 198168
    new-instance v5, LX/17t;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v6

    invoke-direct {v5, v6}, LX/17t;-><init>(LX/0QB;)V

    move-object v5, v5

    .line 198169
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v6

    invoke-static {v5, v6}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v5

    move-object v5, v5

    .line 198170
    new-instance v6, LX/17u;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v7

    invoke-direct {v6, v7}, LX/17u;-><init>(LX/0QB;)V

    move-object v6, v6

    .line 198171
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v7

    invoke-static {v6, v7}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v6

    move-object v6, v6

    .line 198172
    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v8

    check-cast v8, LX/0id;

    invoke-direct/range {v3 .. v8}, LX/17W;-><init>(LX/17Y;LX/0Ot;LX/0Ot;Lcom/facebook/content/SecureContextHelper;LX/0id;)V

    .line 198173
    move-object v0, v3

    .line 198174
    sput-object v0, LX/17W;->f:LX/17W;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 198175
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 198176
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 198177
    :cond_1
    sget-object v0, LX/17W;->f:LX/17W;

    return-object v0

    .line 198178
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 198179
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .param p4    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 198156
    iget-object v0, p0, LX/17W;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/47S;

    .line 198157
    invoke-interface {v0, p1, p3, p2, p4}, LX/47S;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Ljava/util/Map;)V

    goto :goto_0

    .line 198158
    :cond_0
    invoke-static {p2, p3}, LX/17W;->a(Landroid/content/Intent;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 198159
    iget-object v0, p0, LX/17W;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, p2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 198160
    :goto_1
    return-void

    .line 198161
    :cond_1
    iget-object v0, p0, LX/17W;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, p2, p1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1
.end method

.method public static a(Landroid/content/Intent;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 198153
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 198154
    const-string v2, "force_external_activity"

    invoke-virtual {p0, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 198155
    invoke-static {v1}, LX/1H1;->c(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v1}, LX/1H1;->a(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    invoke-static {v1}, LX/32x;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_2

    if-nez v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/47I;)Z
    .locals 3

    .prologue
    .line 198142
    iget-object v0, p0, LX/17W;->a:LX/17Y;

    invoke-interface {v0, p1, p2}, LX/17Y;->a(Landroid/content/Context;LX/47I;)Landroid/content/Intent;

    move-result-object v0

    .line 198143
    if-eqz v0, :cond_1

    .line 198144
    iget-object v1, p2, LX/47I;->b:Landroid/os/Bundle;

    move-object v1, v1

    .line 198145
    if-eqz v1, :cond_0

    .line 198146
    iget-object v1, p2, LX/47I;->b:Landroid/os/Bundle;

    move-object v1, v1

    .line 198147
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 198148
    :cond_0
    iget-object v1, p2, LX/47I;->a:Ljava/lang/String;

    move-object v1, v1

    .line 198149
    iget-object v2, p2, LX/47I;->c:LX/0P1;

    move-object v2, v2

    .line 198150
    invoke-direct {p0, p1, v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Ljava/util/Map;)V

    .line 198151
    const/4 v0, 0x1

    .line 198152
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 198116
    invoke-virtual {p0, p1, p2, v0, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 198141
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z
    .locals 2
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 198131
    iget-object v0, p0, LX/17W;->d:LX/0id;

    const-string v1, "FbUriIntentHandler"

    invoke-virtual {v0, p1, v1}, LX/0id;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 198132
    iget-object v0, p0, LX/17W;->a:LX/17Y;

    invoke-interface {v0, p1, p2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 198133
    if-eqz v0, :cond_1

    .line 198134
    if-eqz p3, :cond_0

    .line 198135
    invoke-virtual {v0, p3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 198136
    :cond_0
    invoke-direct {p0, p1, v0, p2, p4}, LX/17W;->a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Ljava/util/Map;)V

    .line 198137
    const/4 v0, 0x1

    .line 198138
    :goto_0
    return v0

    .line 198139
    :cond_1
    iget-object v0, p0, LX/17W;->d:LX/0id;

    invoke-virtual {v0}, LX/0id;->a()V

    .line 198140
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;ILandroid/app/Activity;)Z
    .locals 7
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;I",
            "Landroid/app/Activity;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 198117
    iget-object v0, p0, LX/17W;->d:LX/0id;

    const-string v1, "FbUriIntentHandler"

    invoke-virtual {v0, p1, v1}, LX/0id;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 198118
    iget-object v0, p0, LX/17W;->a:LX/17Y;

    invoke-interface {v0, p1, p2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 198119
    if-nez v2, :cond_0

    .line 198120
    iget-object v0, p0, LX/17W;->d:LX/0id;

    invoke-virtual {v0}, LX/0id;->a()V

    .line 198121
    const/4 v0, 0x0

    .line 198122
    :goto_0
    return v0

    .line 198123
    :cond_0
    if-eqz p3, :cond_1

    .line 198124
    invoke-virtual {v2, p3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    :cond_1
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    .line 198125
    iget-object p0, v0, LX/17W;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Set;

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/47S;

    .line 198126
    invoke-interface {p0, v1, v3, v2, v4}, LX/47S;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Ljava/util/Map;)V

    goto :goto_1

    .line 198127
    :cond_2
    invoke-static {v2, v3}, LX/17W;->a(Landroid/content/Intent;Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 198128
    iget-object p0, v0, LX/17W;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p0, v2, v5, v6}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 198129
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 198130
    :cond_3
    iget-object p0, v0, LX/17W;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p0, v2, v5, v6}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_2
.end method
