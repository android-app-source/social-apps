.class public LX/19A;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/19A;


# instance fields
.field private final a:LX/19B;

.field private b:I

.field public c:F

.field public d:F

.field private e:Z


# direct methods
.method public constructor <init>(LX/19B;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 207421
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207422
    iput v1, p0, LX/19A;->b:I

    .line 207423
    iput v0, p0, LX/19A;->c:F

    .line 207424
    iput v0, p0, LX/19A;->d:F

    .line 207425
    iput-boolean v1, p0, LX/19A;->e:Z

    .line 207426
    iput-object p1, p0, LX/19A;->a:LX/19B;

    .line 207427
    return-void
.end method

.method public static a(LX/0QB;)LX/19A;
    .locals 4

    .prologue
    .line 207408
    sget-object v0, LX/19A;->f:LX/19A;

    if-nez v0, :cond_1

    .line 207409
    const-class v1, LX/19A;

    monitor-enter v1

    .line 207410
    :try_start_0
    sget-object v0, LX/19A;->f:LX/19A;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 207411
    if-eqz v2, :cond_0

    .line 207412
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 207413
    new-instance p0, LX/19A;

    invoke-static {v0}, LX/19B;->a(LX/0QB;)LX/19B;

    move-result-object v3

    check-cast v3, LX/19B;

    invoke-direct {p0, v3}, LX/19A;-><init>(LX/19B;)V

    .line 207414
    move-object v0, p0

    .line 207415
    sput-object v0, LX/19A;->f:LX/19A;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207416
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 207417
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 207418
    :cond_1
    sget-object v0, LX/19A;->f:LX/19A;

    return-object v0

    .line 207419
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 207420
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(F)I
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 207407
    const/high16 v0, 0x447a0000    # 1000.0f

    div-float/2addr v0, p0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public static e(LX/19A;)V
    .locals 4

    .prologue
    .line 207391
    iget-boolean v0, p0, LX/19A;->e:Z

    if-nez v0, :cond_1

    .line 207392
    iget-object v0, p0, LX/19A;->a:LX/19B;

    .line 207393
    iget-object v1, v0, LX/19B;->a:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRefreshRate()F

    move-result v1

    move v0, v1

    .line 207394
    iput v0, p0, LX/19A;->c:F

    .line 207395
    iget v0, p0, LX/19A;->c:F

    const/high16 v2, 0x42a00000    # 80.0f

    const/high16 v1, 0x41f00000    # 30.0f

    .line 207396
    const/4 v3, 0x0

    cmpg-float v3, v0, v3

    if-gtz v3, :cond_2

    .line 207397
    const/high16 v0, 0x42700000    # 60.0f

    .line 207398
    :cond_0
    :goto_0
    move v0, v0

    .line 207399
    iput v0, p0, LX/19A;->d:F

    .line 207400
    iget v0, p0, LX/19A;->d:F

    invoke-static {v0}, LX/19A;->b(F)I

    move-result v0

    iput v0, p0, LX/19A;->b:I

    .line 207401
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/19A;->e:Z

    .line 207402
    :cond_1
    return-void

    .line 207403
    :cond_2
    cmpg-float v3, v0, v1

    if-gez v3, :cond_3

    move v0, v1

    .line 207404
    goto :goto_0

    .line 207405
    :cond_3
    cmpl-float v1, v0, v2

    if-lez v1, :cond_0

    move v0, v2

    .line 207406
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 207389
    invoke-static {p0}, LX/19A;->e(LX/19A;)V

    .line 207390
    iget v0, p0, LX/19A;->b:I

    return v0
.end method
