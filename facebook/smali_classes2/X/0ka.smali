.class public LX/0ka;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile m:LX/0ka;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0kb;

.field private final c:Landroid/net/wifi/WifiManager;

.field private final d:LX/0SG;

.field private final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final f:LX/03V;

.field private g:LX/0Yd;

.field private volatile h:LX/0ku;

.field private i:J

.field private j:LX/0dN;

.field private k:Z

.field public l:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "LX/0qD;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0kb;Landroid/net/wifi/WifiManager;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 127366
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127367
    sget-object v0, LX/0ku;->WIFI_UNKNOWN:LX/0ku;

    iput-object v0, p0, LX/0ka;->h:LX/0ku;

    .line 127368
    iput-object p1, p0, LX/0ka;->a:Landroid/content/Context;

    .line 127369
    iput-object p2, p0, LX/0ka;->b:LX/0kb;

    .line 127370
    iput-object p3, p0, LX/0ka;->c:Landroid/net/wifi/WifiManager;

    .line 127371
    iput-object p4, p0, LX/0ka;->d:LX/0SG;

    .line 127372
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0ka;->i:J

    .line 127373
    iput-object p5, p0, LX/0ka;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 127374
    iput-object p6, p0, LX/0ka;->f:LX/03V;

    .line 127375
    new-instance v0, LX/0S8;

    invoke-direct {v0}, LX/0S8;-><init>()V

    invoke-virtual {v0}, LX/0S8;->e()LX/0S8;

    move-result-object v0

    invoke-virtual {v0}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/0ka;->l:Ljava/util/concurrent/ConcurrentMap;

    .line 127376
    return-void
.end method

.method public static a(LX/0QB;)LX/0ka;
    .locals 10

    .prologue
    .line 127377
    sget-object v0, LX/0ka;->m:LX/0ka;

    if-nez v0, :cond_1

    .line 127378
    const-class v1, LX/0ka;

    monitor-enter v1

    .line 127379
    :try_start_0
    sget-object v0, LX/0ka;->m:LX/0ka;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 127380
    if-eqz v2, :cond_0

    .line 127381
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 127382
    new-instance v3, LX/0ka;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v5

    check-cast v5, LX/0kb;

    invoke-static {v0}, LX/0kt;->b(LX/0QB;)Landroid/net/wifi/WifiManager;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/WifiManager;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-direct/range {v3 .. v9}, LX/0ka;-><init>(Landroid/content/Context;LX/0kb;Landroid/net/wifi/WifiManager;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;)V

    .line 127383
    move-object v0, v3

    .line 127384
    sput-object v0, LX/0ka;->m:LX/0ka;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127385
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 127386
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 127387
    :cond_1
    sget-object v0, LX/0ka;->m:LX/0ka;

    return-object v0

    .line 127388
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 127389
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/0ka;LX/0ku;)V
    .locals 1

    .prologue
    .line 127390
    iget-object v0, p0, LX/0ka;->h:LX/0ku;

    if-eq v0, p1, :cond_0

    .line 127391
    iput-object p1, p0, LX/0ka;->h:LX/0ku;

    .line 127392
    invoke-direct {p0}, LX/0ka;->e()V

    .line 127393
    :cond_0
    return-void
.end method

.method private e()V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 127396
    new-instance v1, LX/0cA;

    invoke-direct {v1}, LX/0cA;-><init>()V

    .line 127397
    monitor-enter p0

    .line 127398
    :try_start_0
    iget-object v0, p0, LX/0ka;->l:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qD;

    .line 127399
    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 127400
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127401
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qD;

    .line 127402
    invoke-interface {v0}, LX/0qD;->a()V

    goto :goto_1

    .line 127403
    :cond_1
    return-void
.end method

.method public static f(LX/0ka;)V
    .locals 3

    .prologue
    .line 127394
    iget-object v0, p0, LX/0ka;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2KI;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/0ka;->k:Z

    .line 127395
    return-void
.end method


# virtual methods
.method public final a(LX/0qD;)V
    .locals 2

    .prologue
    .line 127364
    iget-object v0, p0, LX/0ka;->l:Ljava/util/concurrent/ConcurrentMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127365
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 127340
    iget-object v0, p0, LX/0ka;->c:Landroid/net/wifi/WifiManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0ka;->c:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 127355
    iget-boolean v1, p0, LX/0ka;->k:Z

    if-eqz v1, :cond_1

    .line 127356
    :cond_0
    :goto_0
    return v0

    .line 127357
    :cond_1
    if-nez p1, :cond_2

    iget-object v1, p0, LX/0ka;->h:LX/0ku;

    sget-object v2, LX/0ku;->WIFI_UNKNOWN:LX/0ku;

    if-ne v1, v2, :cond_3

    .line 127358
    :cond_2
    invoke-virtual {p0}, LX/0ka;->c()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 127359
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    const/4 p1, 0x1

    if-ne v2, p1, :cond_5

    .line 127360
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, LX/0ku;->WIFI_ON:LX/0ku;

    :goto_1
    invoke-static {p0, v1}, LX/0ka;->a$redex0(LX/0ka;LX/0ku;)V

    .line 127361
    :cond_3
    :goto_2
    iget-object v1, p0, LX/0ka;->h:LX/0ku;

    sget-object v2, LX/0ku;->WIFI_ON:LX/0ku;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 127362
    :cond_4
    sget-object v1, LX/0ku;->WIFI_UNKNOWN:LX/0ku;

    goto :goto_1

    .line 127363
    :cond_5
    sget-object v1, LX/0ku;->WIFI_OFF:LX/0ku;

    invoke-static {p0, v1}, LX/0ka;->a$redex0(LX/0ka;LX/0ku;)V

    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 127354
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/0ka;->a(Z)Z

    move-result v0

    return v0
.end method

.method public final c()Landroid/net/NetworkInfo;
    .locals 1

    .prologue
    .line 127353
    iget-object v0, p0, LX/0ka;->b:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    return-object v0
.end method

.method public final init()V
    .locals 4

    .prologue
    .line 127341
    const/4 v0, 0x3

    invoke-static {v0}, LX/0PM;->a(I)Ljava/util/HashMap;

    move-result-object v0

    .line 127342
    const-string v1, "android.net.wifi.supplicant.CONNECTION_CHANGE"

    new-instance v2, LX/2KB;

    invoke-direct {v2, p0}, LX/2KB;-><init>(LX/0ka;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127343
    const-string v1, "android.net.wifi.STATE_CHANGE"

    new-instance v2, LX/2KC;

    invoke-direct {v2, p0}, LX/2KC;-><init>(LX/0ka;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127344
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    new-instance v2, LX/2KG;

    invoke-direct {v2, p0}, LX/2KG;-><init>(LX/0ka;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127345
    new-instance v1, LX/0Yd;

    invoke-direct {v1, v0}, LX/0Yd;-><init>(Ljava/util/Map;)V

    iput-object v1, p0, LX/0ka;->g:LX/0Yd;

    .line 127346
    iget-object v0, p0, LX/0ka;->a:Landroid/content/Context;

    iget-object v1, p0, LX/0ka;->g:LX/0Yd;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.wifi.supplicant.CONNECTION_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 127347
    iget-object v0, p0, LX/0ka;->a:Landroid/content/Context;

    iget-object v1, p0, LX/0ka;->g:LX/0Yd;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.wifi.STATE_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 127348
    iget-object v0, p0, LX/0ka;->a:Landroid/content/Context;

    iget-object v1, p0, LX/0ka;->g:LX/0Yd;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 127349
    new-instance v0, LX/2KH;

    invoke-direct {v0, p0}, LX/2KH;-><init>(LX/0ka;)V

    iput-object v0, p0, LX/0ka;->j:LX/0dN;

    .line 127350
    iget-object v0, p0, LX/0ka;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2KI;->a:LX/0Tn;

    iget-object v2, p0, LX/0ka;->j:LX/0dN;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 127351
    invoke-static {p0}, LX/0ka;->f(LX/0ka;)V

    .line 127352
    return-void
.end method
