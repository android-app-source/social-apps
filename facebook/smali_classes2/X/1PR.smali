.class public final LX/1PR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:LX/1K7;


# direct methods
.method public constructor <init>(LX/1K7;)V
    .locals 0

    .prologue
    .line 244024
    iput-object p1, p0, LX/1PR;->a:LX/1K7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 244025
    iget-object v0, p0, LX/1PR;->a:LX/1K7;

    iget-object v0, v0, LX/1K7;->c:LX/1KM;

    .line 244026
    iput p2, v0, LX/1KM;->a:I

    .line 244027
    packed-switch p2, :pswitch_data_0

    .line 244028
    :cond_0
    :goto_0
    return-void

    .line 244029
    :pswitch_0
    iget-object v0, p0, LX/1PR;->a:LX/1K7;

    iget-object v0, v0, LX/1K7;->b:LX/1K9;

    iget-object v1, p0, LX/1PR;->a:LX/1K7;

    iget-object v1, v1, LX/1K7;->a:LX/1KI;

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 244030
    iget-object v0, p0, LX/1PR;->a:LX/1K7;

    iget-object v0, v0, LX/1K7;->d:LX/1KA;

    iget-object v1, p0, LX/1PR;->a:LX/1K7;

    iget v1, v1, LX/1K7;->g:I

    invoke-virtual {v0, v1}, LX/1KA;->a(I)V

    .line 244031
    iget-object v0, p0, LX/1PR;->a:LX/1K7;

    iget-object v0, v0, LX/1K7;->e:LX/1KN;

    invoke-virtual {v0}, LX/1KN;->b()V

    goto :goto_0

    .line 244032
    :pswitch_1
    iget-object v0, p0, LX/1PR;->a:LX/1K7;

    .line 244033
    iput v2, v0, LX/1K7;->g:I

    .line 244034
    iget-object v0, p0, LX/1PR;->a:LX/1K7;

    iget-object v0, v0, LX/1K7;->d:LX/1KA;

    .line 244035
    iget-object v1, v0, LX/1KA;->a:LX/195;

    invoke-virtual {v1}, LX/195;->a()V

    .line 244036
    iget-object v0, p0, LX/1PR;->a:LX/1K7;

    iget-object v0, v0, LX/1K7;->f:LX/0ad;

    sget-short v1, LX/0wi;->g:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244037
    iget-object v0, p0, LX/1PR;->a:LX/1K7;

    iget-object v0, v0, LX/1K7;->e:LX/1KN;

    const/4 v5, 0x1

    .line 244038
    iget-boolean v3, v0, LX/1KN;->g:Z

    if-eqz v3, :cond_2

    .line 244039
    :cond_1
    :goto_1
    goto :goto_0

    .line 244040
    :cond_2
    iput-boolean v5, v0, LX/1KN;->g:Z

    .line 244041
    iget-object v3, v0, LX/1KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v4, v0, LX/1KN;->f:I

    invoke-interface {v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 244042
    iget v3, v0, LX/1KN;->f:I

    const/4 v4, 0x0

    .line 244043
    sget-boolean v6, LX/032;->a:Z

    if-eqz v6, :cond_3

    .line 244044
    sget-object v6, LX/02r;->b:LX/02r;

    move-object v6, v6

    .line 244045
    if-eqz v6, :cond_3

    invoke-virtual {v6, v3}, LX/02r;->a(I)Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v4, 0x1

    .line 244046
    :cond_3
    move v3, v4

    .line 244047
    if-eqz v3, :cond_1

    .line 244048
    iget-object v3, v0, LX/1KN;->b:LX/19D;

    invoke-virtual {v3}, LX/19D;->a()V

    .line 244049
    iput-boolean v5, v0, LX/1KN;->h:Z

    .line 244050
    iget-object v3, v0, LX/1KN;->d:LX/1KO;

    .line 244051
    iget-object v4, v3, LX/1KO;->c:Landroid/view/MotionEvent;

    if-eqz v4, :cond_4

    const/4 v4, 0x1

    :goto_2
    move v3, v4

    .line 244052
    if-eqz v3, :cond_1

    .line 244053
    iget-object v3, v0, LX/1KN;->d:LX/1KO;

    .line 244054
    iget-wide v6, v3, LX/1KO;->e:J

    iget-wide v8, v3, LX/1KO;->d:J

    sub-long/2addr v6, v8

    move-wide v3, v6

    .line 244055
    iput-wide v3, v0, LX/1KN;->j:J

    .line 244056
    iput-boolean v5, v0, LX/1KN;->i:Z

    goto :goto_1

    :cond_4
    const/4 v4, 0x0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/0g8;III)V
    .locals 2

    .prologue
    .line 244057
    iget-object v0, p0, LX/1PR;->a:LX/1K7;

    invoke-interface {p1}, LX/0g8;->y()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 244058
    iget p0, v0, LX/1K7;->g:I

    add-int/2addr p0, v1

    iput p0, v0, LX/1K7;->g:I

    .line 244059
    return-void
.end method
