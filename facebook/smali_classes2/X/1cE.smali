.class public LX/1cE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cF",
        "<",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/1Fj;

.field private final b:LX/1FQ;

.field public final c:LX/1Gj;


# direct methods
.method public constructor <init>(LX/1Fj;LX/1FQ;LX/1Gj;)V
    .locals 0

    .prologue
    .line 281672
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 281673
    iput-object p1, p0, LX/1cE;->a:LX/1Fj;

    .line 281674
    iput-object p2, p0, LX/1cE;->b:LX/1FQ;

    .line 281675
    iput-object p3, p0, LX/1cE;->c:LX/1Gj;

    .line 281676
    return-void
.end method

.method public static a(LX/1gO;ZLX/1cd;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1gO;",
            "Z",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 281677
    invoke-virtual {p0}, LX/1gO;->a()LX/1FK;

    move-result-object v0

    invoke-static {v0}, LX/1FJ;->a(Ljava/io/Closeable;)LX/1FJ;

    move-result-object v3

    .line 281678
    const/4 v2, 0x0

    .line 281679
    :try_start_0
    new-instance v1, LX/1FL;

    invoke-direct {v1, v3}, LX/1FL;-><init>(LX/1FJ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281680
    :try_start_1
    invoke-virtual {v1}, LX/1FL;->j()V

    .line 281681
    invoke-virtual {p2, v1, p1}, LX/1cd;->b(Ljava/lang/Object;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 281682
    invoke-static {v1}, LX/1FL;->d(LX/1FL;)V

    .line 281683
    invoke-static {v3}, LX/1FJ;->c(LX/1FJ;)V

    .line 281684
    return-void

    .line 281685
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_0
    invoke-static {v1}, LX/1FL;->d(LX/1FL;)V

    .line 281686
    invoke-static {v3}, LX/1FJ;->c(LX/1FJ;)V

    throw v0

    .line 281687
    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public static a$redex0(LX/1cE;LX/1us;Ljava/io/InputStream;I)V
    .locals 12

    .prologue
    .line 281688
    if-lez p3, :cond_2

    .line 281689
    iget-object v0, p0, LX/1cE;->a:LX/1Fj;

    invoke-virtual {v0, p3}, LX/1Fj;->a(I)LX/1gO;

    move-result-object v0

    move-object v1, v0

    .line 281690
    :goto_0
    iget-object v0, p0, LX/1cE;->b:LX/1FQ;

    const/16 v2, 0x4000

    invoke-interface {v0, v2}, LX/1FS;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 281691
    :cond_0
    :goto_1
    :try_start_0
    invoke-virtual {p2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-ltz v2, :cond_3

    .line 281692
    if-lez v2, :cond_0

    .line 281693
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, LX/1gO;->write([BII)V

    .line 281694
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 281695
    iget-object v6, p1, LX/1us;->b:LX/1cW;

    move-object v6, v6

    .line 281696
    iget-object v7, v6, LX/1cW;->a:LX/1bf;

    move-object v6, v7

    .line 281697
    iget-boolean v7, v6, LX/1bf;->e:Z

    move v6, v7

    .line 281698
    if-nez v6, :cond_4

    .line 281699
    const/4 v6, 0x0

    .line 281700
    :goto_2
    move v6, v6

    .line 281701
    if-eqz v6, :cond_1

    .line 281702
    iget-wide v10, p1, LX/1us;->c:J

    move-wide v6, v10

    .line 281703
    sub-long v6, v4, v6

    const-wide/16 v8, 0x64

    cmp-long v6, v6, v8

    if-ltz v6, :cond_1

    .line 281704
    iput-wide v4, p1, LX/1us;->c:J

    .line 281705
    invoke-virtual {p1}, LX/1us;->d()LX/1BV;

    move-result-object v4

    invoke-virtual {p1}, LX/1us;->c()Ljava/lang/String;

    move-result-object v5

    const-string v6, "NetworkFetchProducer"

    const-string v7, "intermediate_result"

    invoke-interface {v4, v5, v6, v7}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281706
    const/4 v4, 0x0

    .line 281707
    iget-object v5, p1, LX/1us;->a:LX/1cd;

    move-object v5, v5

    .line 281708
    invoke-static {v1, v4, v5}, LX/1cE;->a(LX/1gO;ZLX/1cd;)V

    .line 281709
    :cond_1
    invoke-virtual {v1}, LX/1gO;->b()I

    move-result v2

    .line 281710
    if-lez p3, :cond_5

    .line 281711
    int-to-float v4, v2

    int-to-float v5, p3

    div-float/2addr v4, v5

    .line 281712
    :goto_3
    move v2, v4

    .line 281713
    iget-object v3, p1, LX/1us;->a:LX/1cd;

    move-object v3, v3

    .line 281714
    invoke-virtual {v3, v2}, LX/1cd;->b(F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 281715
    :catchall_0
    move-exception v2

    iget-object v3, p0, LX/1cE;->b:LX/1FQ;

    invoke-interface {v3, v0}, LX/1FS;->a(Ljava/lang/Object;)V

    .line 281716
    invoke-virtual {v1}, LX/1gO;->close()V

    throw v2

    .line 281717
    :cond_2
    iget-object v0, p0, LX/1cE;->a:LX/1Fj;

    invoke-virtual {v0}, LX/1Fj;->a()LX/1gO;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 281718
    :cond_3
    :try_start_1
    iget-object v2, p0, LX/1cE;->c:LX/1Gj;

    invoke-virtual {v1}, LX/1gO;->b()I

    move-result v3

    invoke-virtual {v2, p1, v3}, LX/1Gj;->a(LX/1us;I)V

    .line 281719
    invoke-virtual {v1}, LX/1gO;->b()I

    move-result v2

    .line 281720
    invoke-virtual {p1}, LX/1us;->d()LX/1BV;

    move-result-object v3

    invoke-virtual {p1}, LX/1us;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1BV;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 281721
    const/4 v3, 0x0

    .line 281722
    :goto_4
    move-object v2, v3

    .line 281723
    invoke-virtual {p1}, LX/1us;->d()LX/1BV;

    move-result-object v3

    invoke-virtual {p1}, LX/1us;->c()Ljava/lang/String;

    move-result-object v4

    const-string v5, "NetworkFetchProducer"

    invoke-interface {v3, v4, v5, v2}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 281724
    const/4 v2, 0x1

    .line 281725
    iget-object v3, p1, LX/1us;->a:LX/1cd;

    move-object v3, v3

    .line 281726
    invoke-static {v1, v2, v3}, LX/1cE;->a(LX/1gO;ZLX/1cd;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 281727
    iget-object v2, p0, LX/1cE;->b:LX/1FQ;

    invoke-interface {v2, v0}, LX/1FS;->a(Ljava/lang/Object;)V

    .line 281728
    invoke-virtual {v1}, LX/1gO;->close()V

    .line 281729
    return-void

    :cond_4
    :try_start_2
    iget-object v6, p0, LX/1cE;->c:LX/1Gj;

    invoke-virtual {v6, p1}, LX/1Gj;->a(LX/1us;)Z

    move-result v6

    goto/16 :goto_2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_5
    :try_start_3
    const/high16 v4, 0x3f800000    # 1.0f

    neg-int v5, v2

    int-to-double v6, v5

    const-wide v8, 0x40e86a0000000000L    # 50000.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    double-to-float v5, v6

    sub-float/2addr v4, v5

    goto :goto_3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_6
    iget-object v3, p0, LX/1cE;->c:LX/1Gj;

    invoke-virtual {v3, p1, v2}, LX/1Gj;->b(LX/1us;I)Ljava/util/Map;

    move-result-object v3

    goto :goto_4
.end method


# virtual methods
.method public final a(LX/1cd;LX/1cW;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 281730
    iget-object v0, p2, LX/1cW;->c:LX/1BV;

    move-object v0, v0

    .line 281731
    iget-object v1, p2, LX/1cW;->b:Ljava/lang/String;

    move-object v1, v1

    .line 281732
    const-string v2, "NetworkFetchProducer"

    invoke-interface {v0, v1, v2}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 281733
    iget-object v0, p0, LX/1cE;->c:LX/1Gj;

    invoke-virtual {v0, p1, p2}, LX/1Gj;->a(LX/1cd;LX/1cW;)LX/1us;

    move-result-object v0

    .line 281734
    iget-object v1, p0, LX/1cE;->c:LX/1Gj;

    new-instance v2, LX/1ut;

    invoke-direct {v2, p0, v0}, LX/1ut;-><init>(LX/1cE;LX/1us;)V

    invoke-virtual {v1, v0, v2}, LX/1Gj;->a(LX/1us;LX/1ut;)V

    .line 281735
    return-void
.end method
