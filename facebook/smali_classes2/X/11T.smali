.class public LX/11T;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final l:Z

.field private static volatile m:LX/11T;


# instance fields
.field public final a:Ljava/util/Locale;

.field private volatile b:Ljava/text/DateFormat;

.field private volatile c:Ljava/text/DateFormat;

.field private volatile d:Ljava/text/SimpleDateFormat;

.field private volatile e:Ljava/text/SimpleDateFormat;

.field private volatile f:Ljava/text/SimpleDateFormat;

.field private volatile g:Ljava/text/SimpleDateFormat;

.field public volatile h:Ljava/text/SimpleDateFormat;

.field private volatile i:Ljava/text/SimpleDateFormat;

.field public volatile j:Ljava/text/SimpleDateFormat;

.field public volatile k:Ljava/text/SimpleDateFormat;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 172558
    const/16 v2, 0x12

    const/4 v0, 0x0

    .line 172559
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v2, :cond_1

    .line 172560
    :cond_0
    :goto_0
    move v0, v0

    .line 172561
    sput-boolean v0, LX/11T;->l:Z

    return-void

    .line 172562
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ne v1, v2, :cond_2

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "SAMSUNG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "SM-N900"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 172563
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>(LX/0W9;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 172564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172565
    invoke-virtual {p1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    .line 172566
    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 172567
    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    .line 172568
    :goto_1
    move-object v0, v0

    .line 172569
    iput-object v0, p0, LX/11T;->a:Ljava/util/Locale;

    .line 172570
    return-void

    .line 172571
    :sswitch_0
    const-string v3, "fb"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v3, "qz"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 172572
    :pswitch_0
    new-instance v0, Ljava/util/Locale;

    const-string v2, "en"

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0xcbc -> :sswitch_0
        0xe29 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/11T;
    .locals 4

    .prologue
    .line 172577
    sget-object v0, LX/11T;->m:LX/11T;

    if-nez v0, :cond_1

    .line 172578
    const-class v1, LX/11T;

    monitor-enter v1

    .line 172579
    :try_start_0
    sget-object v0, LX/11T;->m:LX/11T;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 172580
    if-eqz v2, :cond_0

    .line 172581
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 172582
    new-instance p0, LX/11T;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v3

    check-cast v3, LX/0W9;

    invoke-direct {p0, v3}, LX/11T;-><init>(LX/0W9;)V

    .line 172583
    move-object v0, p0

    .line 172584
    sput-object v0, LX/11T;->m:LX/11T;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 172585
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 172586
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 172587
    :cond_1
    sget-object v0, LX/11T;->m:LX/11T;

    return-object v0

    .line 172588
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 172589
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/text/SimpleDateFormat;Ljava/lang/String;Ljava/util/Locale;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 172573
    sget-boolean v0, LX/11T;->l:Z

    if-eqz v0, :cond_0

    .line 172574
    invoke-static {p2, p1}, Landroid/text/format/DateFormat;->getBestDateTimePattern(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 172575
    invoke-virtual {p0, v0}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 172576
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/text/DateFormat;
    .locals 2

    .prologue
    .line 172552
    iget-object v0, p0, LX/11T;->b:Ljava/text/DateFormat;

    if-nez v0, :cond_0

    .line 172553
    const/4 v0, 0x3

    iget-object v1, p0, LX/11T;->a:Ljava/util/Locale;

    invoke-static {v0, v1}, Ljava/text/DateFormat;->getTimeInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, LX/11T;->b:Ljava/text/DateFormat;

    .line 172554
    :cond_0
    iget-object v0, p0, LX/11T;->b:Ljava/text/DateFormat;

    return-object v0
.end method

.method public final b()Ljava/text/DateFormat;
    .locals 2

    .prologue
    .line 172555
    iget-object v0, p0, LX/11T;->c:Ljava/text/DateFormat;

    if-nez v0, :cond_0

    .line 172556
    const/4 v0, 0x2

    iget-object v1, p0, LX/11T;->a:Ljava/util/Locale;

    invoke-static {v0, v1}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, LX/11T;->c:Ljava/text/DateFormat;

    .line 172557
    :cond_0
    iget-object v0, p0, LX/11T;->c:Ljava/text/DateFormat;

    return-object v0
.end method

.method public final c()Ljava/text/SimpleDateFormat;
    .locals 3

    .prologue
    .line 172549
    iget-object v0, p0, LX/11T;->d:Ljava/text/SimpleDateFormat;

    if-nez v0, :cond_0

    .line 172550
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEEE"

    iget-object v2, p0, LX/11T;->a:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, LX/11T;->d:Ljava/text/SimpleDateFormat;

    .line 172551
    :cond_0
    iget-object v0, p0, LX/11T;->d:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method public final d()Ljava/text/SimpleDateFormat;
    .locals 3

    .prologue
    .line 172546
    iget-object v0, p0, LX/11T;->e:Ljava/text/SimpleDateFormat;

    if-nez v0, :cond_0

    .line 172547
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EE"

    iget-object v2, p0, LX/11T;->a:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, LX/11T;->e:Ljava/text/SimpleDateFormat;

    .line 172548
    :cond_0
    iget-object v0, p0, LX/11T;->e:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method public final f()Ljava/text/SimpleDateFormat;
    .locals 3

    .prologue
    .line 172541
    iget-object v0, p0, LX/11T;->i:Ljava/text/SimpleDateFormat;

    if-nez v0, :cond_0

    .line 172542
    invoke-virtual {p0}, LX/11T;->b()Ljava/text/DateFormat;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DateFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    .line 172543
    const-string v1, "EEEE, MMMM d, yyyy"

    iget-object v2, p0, LX/11T;->a:Ljava/util/Locale;

    invoke-static {v0, v1, v2}, LX/11T;->a(Ljava/text/SimpleDateFormat;Ljava/lang/String;Ljava/util/Locale;)V

    .line 172544
    iput-object v0, p0, LX/11T;->i:Ljava/text/SimpleDateFormat;

    .line 172545
    :cond_0
    iget-object v0, p0, LX/11T;->i:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method public final g()Ljava/text/SimpleDateFormat;
    .locals 3

    .prologue
    .line 172536
    iget-object v0, p0, LX/11T;->f:Ljava/text/SimpleDateFormat;

    if-nez v0, :cond_0

    .line 172537
    invoke-virtual {p0}, LX/11T;->b()Ljava/text/DateFormat;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DateFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    .line 172538
    const-string v1, "MMMd"

    iget-object v2, p0, LX/11T;->a:Ljava/util/Locale;

    invoke-static {v0, v1, v2}, LX/11T;->a(Ljava/text/SimpleDateFormat;Ljava/lang/String;Ljava/util/Locale;)V

    .line 172539
    iput-object v0, p0, LX/11T;->f:Ljava/text/SimpleDateFormat;

    .line 172540
    :cond_0
    iget-object v0, p0, LX/11T;->f:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method public final h()Ljava/text/SimpleDateFormat;
    .locals 3

    .prologue
    .line 172531
    iget-object v0, p0, LX/11T;->g:Ljava/text/SimpleDateFormat;

    if-nez v0, :cond_0

    .line 172532
    invoke-virtual {p0}, LX/11T;->b()Ljava/text/DateFormat;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DateFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    .line 172533
    const-string v1, "MMMd, yyyy"

    iget-object v2, p0, LX/11T;->a:Ljava/util/Locale;

    invoke-static {v0, v1, v2}, LX/11T;->a(Ljava/text/SimpleDateFormat;Ljava/lang/String;Ljava/util/Locale;)V

    .line 172534
    iput-object v0, p0, LX/11T;->g:Ljava/text/SimpleDateFormat;

    .line 172535
    :cond_0
    iget-object v0, p0, LX/11T;->g:Ljava/text/SimpleDateFormat;

    return-object v0
.end method
