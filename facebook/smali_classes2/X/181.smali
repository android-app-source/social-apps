.class public LX/181;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 203625
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "LX/16g;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 203615
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 203616
    check-cast v0, LX/16g;

    .line 203617
    invoke-static {v0}, LX/0x1;->a(LX/16g;)LX/162;

    move-result-object v1

    .line 203618
    if-eqz v1, :cond_0

    move-object v0, v1

    .line 203619
    :goto_0
    return-object v0

    .line 203620
    :cond_0
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v1, :cond_1

    .line 203621
    invoke-static {p0}, LX/1WF;->k(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    goto :goto_0

    .line 203622
    :cond_1
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_2

    .line 203623
    invoke-static {p0}, LX/182;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    goto :goto_0

    .line 203624
    :cond_2
    invoke-static {v0}, LX/1fz;->a(LX/16g;)LX/162;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;
    .locals 2

    .prologue
    .line 203611
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 203612
    instance-of v1, v0, LX/16g;

    if-eqz v1, :cond_0

    .line 203613
    invoke-static {p0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 203614
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LX/1fz;->a(Ljava/lang/Object;)LX/162;

    move-result-object v0

    goto :goto_0
.end method
