.class public final LX/1bT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1Pn;

.field public final synthetic b:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;LX/1Pn;)V
    .locals 0

    .prologue
    .line 280564
    iput-object p1, p0, LX/1bT;->b:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;

    iput-object p2, p0, LX/1bT;->a:LX/1Pn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x78f89cbd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 280558
    iget-object v0, p0, LX/1bT;->b:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Np;

    const-string v2, "tap_composer_profile_photo_from_feed"

    invoke-virtual {v0, v2}, LX/1Np;->a(Ljava/lang/String;)V

    .line 280559
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    iget-object v2, p0, LX/1bT;->b:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;

    iget-object v2, v2, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;->a:LX/0WJ;

    invoke-virtual {v2}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v2

    .line 280560
    iget-object v3, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v3

    .line 280561
    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 280562
    iget-object v0, p0, LX/1bT;->b:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v3, p0, LX/1bT;->a:LX/1Pn;

    invoke-interface {v3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v3, v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 280563
    const v0, 0x20b88f2a

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
