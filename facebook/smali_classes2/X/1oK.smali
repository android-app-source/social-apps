.class public LX/1oK;
.super LX/1aX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DH::",
        "LX/1aY;",
        ">",
        "LX/1aX",
        "<TDH;>;"
    }
.end annotation


# static fields
.field public static final a:Landroid/os/Handler;


# instance fields
.field public b:Ljava/lang/Runnable;

.field public c:Z

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 318958
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, LX/1oK;->a:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(LX/1aY;)V
    .locals 0
    .param p1    # LX/1aY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDH;)V"
        }
    .end annotation

    .prologue
    .line 318959
    invoke-direct {p0, p1}, LX/1aX;-><init>(LX/1aY;)V

    .line 318960
    return-void
.end method


# virtual methods
.method public final a(LX/1aZ;)V
    .locals 2
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 318961
    iget-boolean v0, p0, LX/1oK;->d:Z

    if-eqz v0, :cond_0

    .line 318962
    sget-object v0, LX/1oK;->a:Landroid/os/Handler;

    iget-object v1, p0, LX/1oK;->b:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 318963
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1oK;->d:Z

    .line 318964
    :cond_0
    iget-boolean v0, p0, LX/1oK;->c:Z

    if-eqz v0, :cond_1

    .line 318965
    iget-object v0, p0, LX/1aX;->f:LX/1aZ;

    move-object v0, v0

    .line 318966
    if-ne v0, p1, :cond_1

    .line 318967
    iget-object v0, p0, LX/1aX;->g:LX/1ab;

    move-object v0, v0

    .line 318968
    sget-object v1, LX/1at;->ON_SAME_CONTROLLER_SKIPPED:LX/1at;

    invoke-virtual {v0, v1}, LX/1ab;->a(LX/1at;)V

    .line 318969
    :goto_0
    return-void

    .line 318970
    :cond_1
    invoke-super {p0, p1}, LX/1aX;->a(LX/1aZ;)V

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 318971
    invoke-static {p0}, LX/15f;->a(Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "draweeHolder"

    invoke-super {p0}, LX/1aX;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "skipSameController"

    iget-boolean v2, p0, LX/1oK;->c:Z

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Z)LX/15g;

    move-result-object v0

    invoke-virtual {v0}, LX/15g;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
