.class public LX/1u7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1pb;


# instance fields
.field private final a:D

.field private final b:I

.field private c:D

.field private d:I


# direct methods
.method public constructor <init>(D)V
    .locals 3

    .prologue
    .line 337874
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 337875
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, LX/1u7;->c:D

    .line 337876
    iput-wide p1, p0, LX/1u7;->a:D

    .line 337877
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_0

    const v0, 0x7fffffff

    :goto_0
    iput v0, p0, LX/1u7;->b:I

    .line 337878
    return-void

    .line 337879
    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    div-double/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()D
    .locals 2

    .prologue
    .line 337880
    iget-wide v0, p0, LX/1u7;->c:D

    return-wide v0
.end method

.method public final a(D)V
    .locals 7

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 337881
    iget-wide v0, p0, LX/1u7;->a:D

    sub-double v0, v4, v0

    .line 337882
    iget v2, p0, LX/1u7;->d:I

    iget v3, p0, LX/1u7;->b:I

    if-le v2, v3, :cond_0

    .line 337883
    iget-wide v2, p0, LX/1u7;->c:D

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    iget-wide v2, p0, LX/1u7;->a:D

    invoke-static {p1, p2}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    iput-wide v0, p0, LX/1u7;->c:D

    .line 337884
    :goto_0
    iget v0, p0, LX/1u7;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1u7;->d:I

    .line 337885
    return-void

    .line 337886
    :cond_0
    iget v2, p0, LX/1u7;->d:I

    if-lez v2, :cond_1

    .line 337887
    iget v2, p0, LX/1u7;->d:I

    int-to-double v2, v2

    mul-double/2addr v0, v2

    iget v2, p0, LX/1u7;->d:I

    int-to-double v2, v2

    add-double/2addr v2, v4

    div-double/2addr v0, v2

    .line 337888
    sub-double v2, v4, v0

    .line 337889
    iget-wide v4, p0, LX/1u7;->c:D

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    mul-double/2addr v0, v4

    invoke-static {p1, p2}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    iput-wide v0, p0, LX/1u7;->c:D

    goto :goto_0

    .line 337890
    :cond_1
    iput-wide p1, p0, LX/1u7;->c:D

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 337891
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, LX/1u7;->c:D

    .line 337892
    const/4 v0, 0x0

    iput v0, p0, LX/1u7;->d:I

    .line 337893
    return-void
.end method
