.class public LX/19l;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/19l;


# instance fields
.field public final a:LX/16V;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 208526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208527
    new-instance v0, LX/16V;

    invoke-direct {v0}, LX/16V;-><init>()V

    iput-object v0, p0, LX/19l;->a:LX/16V;

    .line 208528
    return-void
.end method

.method public static a(LX/0QB;)LX/19l;
    .locals 3

    .prologue
    .line 208529
    sget-object v0, LX/19l;->b:LX/19l;

    if-nez v0, :cond_1

    .line 208530
    const-class v1, LX/19l;

    monitor-enter v1

    .line 208531
    :try_start_0
    sget-object v0, LX/19l;->b:LX/19l;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 208532
    if-eqz v2, :cond_0

    .line 208533
    :try_start_1
    new-instance v0, LX/19l;

    invoke-direct {v0}, LX/19l;-><init>()V

    .line 208534
    move-object v0, v0

    .line 208535
    sput-object v0, LX/19l;->b:LX/19l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208536
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 208537
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 208538
    :cond_1
    sget-object v0, LX/19l;->b:LX/19l;

    return-object v0

    .line 208539
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 208540
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
