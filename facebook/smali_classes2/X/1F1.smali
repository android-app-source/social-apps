.class public LX/1F1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ez;


# instance fields
.field private final a:LX/1Ex;

.field private final b:LX/1Ez;


# direct methods
.method public constructor <init>(LX/1Ex;LX/1Ez;)V
    .locals 0

    .prologue
    .line 221536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221537
    iput-object p1, p0, LX/1F1;->a:LX/1Ex;

    .line 221538
    iput-object p2, p0, LX/1F1;->b:LX/1Ez;

    .line 221539
    return-void
.end method


# virtual methods
.method public final V_()V
    .locals 2

    .prologue
    .line 221533
    iget-object v0, p0, LX/1F1;->a:LX/1Ex;

    invoke-virtual {v0}, LX/1Ex;->a()LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 221534
    iget-object v0, p0, LX/1F1;->b:LX/1Ez;

    invoke-interface {v0}, LX/1F0;->V_()V

    .line 221535
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 221513
    iget-object v0, p0, LX/1F1;->a:LX/1Ex;

    invoke-virtual {v0}, LX/1Ex;->a()LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 221514
    iget-object v0, p0, LX/1F1;->b:LX/1Ez;

    invoke-interface {v0, p1}, LX/1F0;->d(I)V

    .line 221515
    :cond_0
    return-void
.end method

.method public final a(LX/1FR;)V
    .locals 1

    .prologue
    .line 221531
    iget-object v0, p0, LX/1F1;->b:LX/1Ez;

    invoke-interface {v0, p1}, LX/1F0;->a(LX/1FR;)V

    .line 221532
    return-void
.end method

.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 221528
    iget-object v0, p0, LX/1F1;->a:LX/1Ex;

    invoke-virtual {v0}, LX/1Ex;->a()LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 221529
    iget-object v0, p0, LX/1F1;->b:LX/1Ez;

    invoke-interface {v0, p1}, LX/1Ez;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 221530
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 221525
    iget-object v0, p0, LX/1F1;->a:LX/1Ex;

    invoke-virtual {v0}, LX/1Ex;->a()LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 221526
    iget-object v0, p0, LX/1F1;->b:LX/1Ez;

    invoke-interface {v0}, LX/1F0;->b()V

    .line 221527
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 221522
    iget-object v0, p0, LX/1F1;->a:LX/1Ex;

    invoke-virtual {v0}, LX/1Ex;->a()LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 221523
    iget-object v0, p0, LX/1F1;->b:LX/1Ez;

    invoke-interface {v0, p1}, LX/1F0;->b(I)V

    .line 221524
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 221519
    iget-object v0, p0, LX/1F1;->a:LX/1Ex;

    invoke-virtual {v0}, LX/1Ex;->a()LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 221520
    iget-object v0, p0, LX/1F1;->b:LX/1Ez;

    invoke-interface {v0, p1}, LX/1F0;->c(I)V

    .line 221521
    :cond_0
    return-void
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 221516
    iget-object v0, p0, LX/1F1;->a:LX/1Ex;

    invoke-virtual {v0}, LX/1Ex;->a()LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 221517
    iget-object v0, p0, LX/1F1;->b:LX/1Ez;

    invoke-interface {v0, p1}, LX/1F0;->d(I)V

    .line 221518
    :cond_0
    return-void
.end method
