.class public LX/0pO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 144331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/api/feedtype/FeedType;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 144332
    sget-object v0, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0, p0}, Lcom/facebook/api/feedtype/FeedType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144333
    const/4 v0, 0x1

    .line 144334
    :goto_0
    return v0

    .line 144335
    :cond_0
    if-eqz p0, :cond_1

    .line 144336
    iget-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v0, v0

    .line 144337
    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, LX/0pO;->b(Lcom/facebook/api/feedtype/FeedType;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    .line 144338
    goto :goto_0

    .line 144339
    :cond_2
    iget-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    move-object v0, v0

    .line 144340
    instance-of v2, v0, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;

    if-nez v2, :cond_3

    move v0, v1

    .line 144341
    goto :goto_0

    .line 144342
    :cond_3
    check-cast v0, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;

    .line 144343
    iget-object v1, v0, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;->b:Ljava/lang/String;

    move-object v0, v1

    .line 144344
    sget-object v1, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v1}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/api/feedtype/FeedType;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 144345
    iget-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    move-object v0, v0

    .line 144346
    instance-of v0, v0, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;

    if-nez v0, :cond_0

    move v0, v1

    .line 144347
    :goto_0
    return v0

    .line 144348
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    move-object v0, v0

    .line 144349
    check-cast v0, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;

    .line 144350
    iget-object v2, v0, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;->a:Ljava/lang/String;

    move-object v0, v2

    .line 144351
    const-string v2, "EXCLUDE_FRIENDS_CONTENT"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "EXCLUDE_ORIGINAL_FRIENDS_CONTENT"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "RECOMMENDATIONS_FEED"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
