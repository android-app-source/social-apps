.class public LX/1rb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1rb;


# instance fields
.field private final a:LX/0y3;

.field public final b:LX/1rc;


# direct methods
.method public constructor <init>(LX/0y3;LX/1rc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 332675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 332676
    iput-object p1, p0, LX/1rb;->a:LX/0y3;

    .line 332677
    iput-object p2, p0, LX/1rb;->b:LX/1rc;

    .line 332678
    return-void
.end method

.method public static a(LX/0QB;)LX/1rb;
    .locals 5

    .prologue
    .line 332679
    sget-object v0, LX/1rb;->c:LX/1rb;

    if-nez v0, :cond_1

    .line 332680
    const-class v1, LX/1rb;

    monitor-enter v1

    .line 332681
    :try_start_0
    sget-object v0, LX/1rb;->c:LX/1rb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 332682
    if-eqz v2, :cond_0

    .line 332683
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 332684
    new-instance p0, LX/1rb;

    invoke-static {v0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v3

    check-cast v3, LX/0y3;

    invoke-static {v0}, LX/1rc;->a(LX/0QB;)LX/1rc;

    move-result-object v4

    check-cast v4, LX/1rc;

    invoke-direct {p0, v3, v4}, LX/1rb;-><init>(LX/0y3;LX/1rc;)V

    .line 332685
    move-object v0, p0

    .line 332686
    sput-object v0, LX/1rb;->c:LX/1rb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 332687
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 332688
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 332689
    :cond_1
    sget-object v0, LX/1rb;->c:LX/1rb;

    return-object v0

    .line 332690
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 332691
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/1rb;)LX/0lF;
    .locals 7

    .prologue
    .line 332692
    iget-object v0, p0, LX/1rb;->a:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->b()LX/1rv;

    move-result-object v0

    .line 332693
    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-virtual {v1}, LX/0mC;->c()LX/0m9;

    move-result-object v1

    .line 332694
    const-string v2, "state"

    iget-object v3, v0, LX/1rv;->a:LX/0yG;

    invoke-virtual {v3}, LX/0yG;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 332695
    const-string v2, "available_providers_enabled"

    iget-object v3, v0, LX/1rv;->b:LX/0Rf;

    iget-object v0, v0, LX/1rv;->c:LX/0Rf;

    .line 332696
    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-virtual {v4}, LX/0mC;->c()LX/0m9;

    move-result-object v5

    .line 332697
    invoke-virtual {v3}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 332698
    const/4 p0, 0x1

    invoke-virtual {v5, v4, p0}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    goto :goto_0

    .line 332699
    :cond_0
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 332700
    const/4 p0, 0x0

    invoke-virtual {v5, v4, p0}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    goto :goto_1

    .line 332701
    :cond_1
    move-object v0, v5

    .line 332702
    invoke-virtual {v1, v2, v0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 332703
    return-object v1
.end method
