.class public LX/1Da;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/03V;

.field private final b:LX/1Dc;

.field private final c:LX/0Sh;

.field private final d:LX/1Db;

.field private final e:LX/23K;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/0Yi;


# direct methods
.method public constructor <init>(LX/0ad;Landroid/content/Context;LX/03V;LX/1Db;LX/1Dc;LX/0Sh;LX/0Ot;LX/0Yi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "Landroid/content/Context;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1Db;",
            "LX/1Dc;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/components/feed/StackComponentPartDefinition;",
            ">;",
            "LX/0Yi;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 217702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217703
    iput-object p3, p0, LX/1Da;->a:LX/03V;

    .line 217704
    iput-object p5, p0, LX/1Da;->b:LX/1Dc;

    .line 217705
    iput-object p6, p0, LX/1Da;->c:LX/0Sh;

    .line 217706
    iput-object p4, p0, LX/1Da;->d:LX/1Db;

    .line 217707
    sget-short v0, LX/1Dd;->q:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LX/23K;

    invoke-direct {v0, p2, p7}, LX/23K;-><init>(Landroid/content/Context;LX/0Ot;)V

    :goto_0
    iput-object v0, p0, LX/1Da;->e:LX/23K;

    .line 217708
    iput-object p8, p0, LX/1Da;->f:LX/0Yi;

    .line 217709
    return-void

    .line 217710
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1Da;
    .locals 1

    .prologue
    .line 217698
    invoke-static {p0}, LX/1Da;->b(LX/0QB;)LX/1Da;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1Da;
    .locals 9

    .prologue
    .line 217700
    new-instance v0, LX/1Da;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v4

    check-cast v4, LX/1Db;

    invoke-static {p0}, LX/1Dc;->a(LX/0QB;)LX/1Dc;

    move-result-object v5

    check-cast v5, LX/1Dc;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v6

    check-cast v6, LX/0Sh;

    const/16 v7, 0x3ae

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/0Yi;->a(LX/0QB;)LX/0Yi;

    move-result-object v8

    check-cast v8, LX/0Yi;

    invoke-direct/range {v0 .. v8}, LX/1Da;-><init>(LX/0ad;Landroid/content/Context;LX/03V;LX/1Db;LX/1Dc;LX/0Sh;LX/0Ot;LX/0Yi;)V

    .line 217701
    return-object v0
.end method


# virtual methods
.method public final a(LX/0Ot;LX/1Qx;Z)LX/1R0;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1PW;",
            ">(",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<**-TE;>;>;",
            "LX/1Qx;",
            "Z)",
            "LX/1R0",
            "<*TE;>;"
        }
    .end annotation

    .prologue
    .line 217699
    new-instance v0, LX/1Qz;

    iget-object v1, p0, LX/1Da;->a:LX/03V;

    iget-object v2, p0, LX/1Da;->d:LX/1Db;

    iget-object v3, p0, LX/1Da;->b:LX/1Dc;

    iget-object v4, p0, LX/1Da;->c:LX/0Sh;

    iget-object v8, p0, LX/1Da;->e:LX/23K;

    iget-object v9, p0, LX/1Da;->f:LX/0Yi;

    move-object v5, p2

    move-object v6, p1

    move v7, p3

    invoke-direct/range {v0 .. v9}, LX/1Qz;-><init>(LX/03V;LX/1Db;LX/1Dc;LX/0Sh;LX/1Qx;LX/0Ot;ZLX/23K;LX/0Yi;)V

    return-object v0
.end method
