.class public final enum LX/0rL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0rL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0rL;

.field public static final enum CACHED:LX/0rL;

.field public static final enum NORMAL:LX/0rL;

.field public static final enum PRIORITY:LX/0rL;

.field public static final enum REQUIRED:LX/0rL;

.field public static final enum RESTORED:LX/0rL;


# instance fields
.field private final mTypeName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 149463
    new-instance v0, LX/0rL;

    const-string v1, "NORMAL"

    const-string v2, "normal"

    invoke-direct {v0, v1, v3, v2}, LX/0rL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0rL;->NORMAL:LX/0rL;

    .line 149464
    new-instance v0, LX/0rL;

    const-string v1, "PRIORITY"

    const-string v2, "priority"

    invoke-direct {v0, v1, v4, v2}, LX/0rL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0rL;->PRIORITY:LX/0rL;

    .line 149465
    new-instance v0, LX/0rL;

    const-string v1, "REQUIRED"

    const-string v2, "required"

    invoke-direct {v0, v1, v5, v2}, LX/0rL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0rL;->REQUIRED:LX/0rL;

    .line 149466
    new-instance v0, LX/0rL;

    const-string v1, "CACHED"

    const-string v2, "cached"

    invoke-direct {v0, v1, v6, v2}, LX/0rL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0rL;->CACHED:LX/0rL;

    .line 149467
    new-instance v0, LX/0rL;

    const-string v1, "RESTORED"

    const-string v2, "restored"

    invoke-direct {v0, v1, v7, v2}, LX/0rL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0rL;->RESTORED:LX/0rL;

    .line 149468
    const/4 v0, 0x5

    new-array v0, v0, [LX/0rL;

    sget-object v1, LX/0rL;->NORMAL:LX/0rL;

    aput-object v1, v0, v3

    sget-object v1, LX/0rL;->PRIORITY:LX/0rL;

    aput-object v1, v0, v4

    sget-object v1, LX/0rL;->REQUIRED:LX/0rL;

    aput-object v1, v0, v5

    sget-object v1, LX/0rL;->CACHED:LX/0rL;

    aput-object v1, v0, v6

    sget-object v1, LX/0rL;->RESTORED:LX/0rL;

    aput-object v1, v0, v7

    sput-object v0, LX/0rL;->$VALUES:[LX/0rL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 149460
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 149461
    iput-object p3, p0, LX/0rL;->mTypeName:Ljava/lang/String;

    .line 149462
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0rL;
    .locals 1

    .prologue
    .line 149459
    const-class v0, LX/0rL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0rL;

    return-object v0
.end method

.method public static values()[LX/0rL;
    .locals 1

    .prologue
    .line 149457
    sget-object v0, LX/0rL;->$VALUES:[LX/0rL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0rL;

    return-object v0
.end method


# virtual methods
.method public final getTypeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149458
    iget-object v0, p0, LX/0rL;->mTypeName:Ljava/lang/String;

    return-object v0
.end method
