.class public LX/1iu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile l:LX/1iu;


# instance fields
.field private final d:Landroid/content/Context;

.field public final e:Ljava/util/concurrent/ScheduledExecutorService;

.field public final f:Ljava/lang/Object;

.field private g:J

.field public h:Ljava/util/concurrent/Future;

.field public i:Ljava/util/concurrent/Future;

.field private final j:Ljava/lang/Runnable;

.field private final k:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 299336
    const-class v0, LX/1iu;

    sput-object v0, LX/1iu;->c:Ljava/lang/Class;

    .line 299337
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/1iu;->c:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".NETWORKING_ACTIVE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1iu;->a:Ljava/lang/String;

    .line 299338
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/1iu;->c:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".NETWORKING_INACTIVE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1iu;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 3
    .param p2    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 299329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 299330
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/1iu;->f:Ljava/lang/Object;

    .line 299331
    new-instance v0, Lcom/facebook/common/hardware/NetworkActivityBroadcastManager$1;

    sget-object v1, LX/1iu;->c:Ljava/lang/Class;

    const-string v2, "ActiveRadioRunner"

    invoke-direct {v0, p0, v1, v2}, Lcom/facebook/common/hardware/NetworkActivityBroadcastManager$1;-><init>(LX/1iu;Ljava/lang/Class;Ljava/lang/String;)V

    iput-object v0, p0, LX/1iu;->j:Ljava/lang/Runnable;

    .line 299332
    new-instance v0, Lcom/facebook/common/hardware/NetworkActivityBroadcastManager$2;

    sget-object v1, LX/1iu;->c:Ljava/lang/Class;

    const-string v2, "InactiveRadioRunner"

    invoke-direct {v0, p0, v1, v2}, Lcom/facebook/common/hardware/NetworkActivityBroadcastManager$2;-><init>(LX/1iu;Ljava/lang/Class;Ljava/lang/String;)V

    iput-object v0, p0, LX/1iu;->k:Ljava/lang/Runnable;

    .line 299333
    iput-object p1, p0, LX/1iu;->d:Landroid/content/Context;

    .line 299334
    iput-object p2, p0, LX/1iu;->e:Ljava/util/concurrent/ScheduledExecutorService;

    .line 299335
    return-void
.end method

.method public static a(LX/0QB;)LX/1iu;
    .locals 5

    .prologue
    .line 299339
    sget-object v0, LX/1iu;->l:LX/1iu;

    if-nez v0, :cond_1

    .line 299340
    const-class v1, LX/1iu;

    monitor-enter v1

    .line 299341
    :try_start_0
    sget-object v0, LX/1iu;->l:LX/1iu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 299342
    if-eqz v2, :cond_0

    .line 299343
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 299344
    new-instance p0, LX/1iu;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {p0, v3, v4}, LX/1iu;-><init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 299345
    move-object v0, p0

    .line 299346
    sput-object v0, LX/1iu;->l:LX/1iu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 299347
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 299348
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 299349
    :cond_1
    sget-object v0, LX/1iu;->l:LX/1iu;

    return-object v0

    .line 299350
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 299351
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/1iu;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 299325
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 299326
    const-string v1, "pid"

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 299327
    iget-object v1, p0, LX/1iu;->d:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 299328
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 299314
    iget-object v1, p0, LX/1iu;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 299315
    :try_start_0
    iget-wide v2, p0, LX/1iu;->g:J

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/1iu;->g:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    .line 299316
    iget-object v0, p0, LX/1iu;->i:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_1

    .line 299317
    iget-object v0, p0, LX/1iu;->h:Ljava/util/concurrent/Future;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 299318
    iget-object v0, p0, LX/1iu;->i:Ljava/util/concurrent/Future;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 299319
    const/4 v0, 0x0

    iput-object v0, p0, LX/1iu;->i:Ljava/util/concurrent/Future;

    .line 299320
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 299321
    :cond_1
    iget-object v0, p0, LX/1iu;->h:Ljava/util/concurrent/Future;

    if-nez v0, :cond_0

    .line 299322
    iget-object v0, p0, LX/1iu;->e:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v2, p0, LX/1iu;->j:Ljava/lang/Runnable;

    const v3, 0x50f56c93

    invoke-static {v0, v2, v3}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, LX/1iu;->h:Ljava/util/concurrent/Future;

    goto :goto_0

    .line 299323
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 299324
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/1iu;->h:Ljava/util/concurrent/Future;

    const-string v2, "Internal inconsistency managing intent futures"

    invoke-static {v0, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final b()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 299305
    iget-object v3, p0, LX/1iu;->f:Ljava/lang/Object;

    monitor-enter v3

    .line 299306
    :try_start_0
    iget-wide v4, p0, LX/1iu;->g:J

    cmp-long v2, v4, v6

    if-lez v2, :cond_1

    move v2, v0

    :goto_0
    const-string v4, "Imbalanced activate/inactivate calls"

    invoke-static {v2, v4}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 299307
    iget-object v2, p0, LX/1iu;->i:Ljava/util/concurrent/Future;

    if-nez v2, :cond_2

    :goto_1
    const-string v1, "Internal inconsistency managing intent futures"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 299308
    iget-wide v0, p0, LX/1iu;->g:J

    const-wide/16 v4, 0x1

    sub-long/2addr v0, v4

    iput-wide v0, p0, LX/1iu;->g:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_0

    .line 299309
    iget-object v0, p0, LX/1iu;->e:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/1iu;->k:Ljava/lang/Runnable;

    const-wide/16 v4, 0x7d0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v4, v5, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/1iu;->i:Ljava/util/concurrent/Future;

    .line 299310
    :cond_0
    monitor-exit v3

    return-void

    :cond_1
    move v2, v1

    .line 299311
    goto :goto_0

    :cond_2
    move v0, v1

    .line 299312
    goto :goto_1

    .line 299313
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
