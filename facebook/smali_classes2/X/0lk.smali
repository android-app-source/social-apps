.class public LX/0lk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _factory:LX/0li;


# direct methods
.method public constructor <init>(LX/0li;)V
    .locals 0

    .prologue
    .line 130400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130401
    iput-object p1, p0, LX/0lk;->_factory:LX/0li;

    .line 130402
    return-void
.end method

.method private a(LX/4rb;)LX/0lJ;
    .locals 3

    .prologue
    .line 130403
    invoke-virtual {p1}, LX/4rb;->hasMoreTokens()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130404
    const-string v0, "Unexpected end-of-string"

    invoke-static {p1, v0}, LX/0lk;->a(LX/4rb;Ljava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 130405
    :cond_0
    invoke-virtual {p1}, LX/4rb;->nextToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, LX/0lk;->a(Ljava/lang/String;LX/4rb;)Ljava/lang/Class;

    move-result-object v0

    .line 130406
    invoke-virtual {p1}, LX/4rb;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 130407
    invoke-virtual {p1}, LX/4rb;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 130408
    const-string v2, "<"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 130409
    iget-object v1, p0, LX/0lk;->_factory:LX/0li;

    invoke-direct {p0, p1}, LX/0lk;->b(LX/4rb;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0li;->a(Ljava/lang/Class;Ljava/util/List;)LX/0lJ;

    move-result-object v0

    .line 130410
    :goto_0
    return-object v0

    .line 130411
    :cond_1
    invoke-virtual {p1, v1}, LX/4rb;->a(Ljava/lang/String;)V

    .line 130412
    :cond_2
    iget-object v1, p0, LX/0lk;->_factory:LX/0li;

    invoke-virtual {v1, v0}, LX/0li;->b(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;LX/4rb;)Ljava/lang/Class;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/4rb;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 130413
    :try_start_0
    invoke-static {p0}, LX/1Xw;->a(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 130414
    :catch_0
    move-exception v0

    .line 130415
    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_0

    .line 130416
    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 130417
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not locate class \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\', problem: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/0lk;->a(LX/4rb;Ljava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0
.end method

.method private static a(LX/4rb;Ljava/lang/String;)Ljava/lang/IllegalArgumentException;
    .locals 3

    .prologue
    .line 130418
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to parse type \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 130419
    iget-object v2, p0, LX/4rb;->a:Ljava/lang/String;

    move-object v2, v2

    .line 130420
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' (remaining: \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/4rb;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private b(LX/4rb;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4rb;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/0lJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130421
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 130422
    :cond_0
    invoke-virtual {p1}, LX/4rb;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 130423
    invoke-direct {p0, p1}, LX/0lk;->a(LX/4rb;)LX/0lJ;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130424
    invoke-virtual {p1}, LX/4rb;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 130425
    invoke-virtual {p1}, LX/4rb;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 130426
    const-string v2, ">"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v0

    .line 130427
    :cond_1
    const-string v2, ","

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 130428
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected token \'"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', expected \',\' or \'>\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/0lk;->a(LX/4rb;Ljava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 130429
    :cond_2
    const-string v0, "Unexpected end-of-string"

    invoke-static {p1, v0}, LX/0lk;->a(LX/4rb;Ljava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0lJ;
    .locals 3

    .prologue
    .line 130430
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 130431
    new-instance v1, LX/4rb;

    invoke-direct {v1, v0}, LX/4rb;-><init>(Ljava/lang/String;)V

    .line 130432
    invoke-direct {p0, v1}, LX/0lk;->a(LX/4rb;)LX/0lJ;

    move-result-object v0

    .line 130433
    invoke-virtual {v1}, LX/4rb;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 130434
    const-string v0, "Unexpected tokens after complete type"

    invoke-static {v1, v0}, LX/0lk;->a(LX/4rb;Ljava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 130435
    :cond_0
    return-object v0
.end method
