.class public LX/19Y;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 208080
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 208081
    return-void
.end method

.method public static a(Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/1AA;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 208082
    new-instance v0, LX/1AA;

    invoke-direct {v0, p0}, LX/1AA;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    return-object v0
.end method

.method public static a(LX/03V;LX/0Ot;LX/0Ot;)LX/1Lg;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/1Lv;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Lr;",
            ">;)",
            "LX/1Lg;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 208088
    new-instance v0, LX/1Lg;

    invoke-direct {v0, p0, p1, p2}, LX/1Lg;-><init>(LX/03V;LX/0Ot;LX/0Ot;)V

    .line 208089
    return-object v0
.end method

.method public static a(Landroid/content/Context;LX/0SG;LX/1AA;LX/0en;LX/0V8;LX/0pq;LX/0lC;LX/0ad;LX/0wp;LX/0Sh;)LX/1Ln;
    .locals 15
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation build Lcom/facebook/video/server/VideoCache;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 208083
    const-string v3, "video-cache"

    sget-wide v4, LX/0ws;->W:J

    const-wide/32 v6, 0x6400000

    move-object/from16 v0, p7

    invoke-interface {v0, v4, v5, v6, v7}, LX/0ad;->a(JJ)J

    move-result-wide v4

    move-object v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p6

    move-object/from16 v12, p5

    move-object/from16 v13, p8

    move-object/from16 v14, p9

    invoke-static/range {v3 .. v14}, LX/19Y;->a(Ljava/lang/String;JLandroid/content/Context;LX/0SG;LX/1AA;LX/0en;LX/0V8;LX/0lC;LX/0pq;LX/0wp;LX/0Sh;)LX/1Lo;

    move-result-object v2

    .line 208084
    sget-short v3, LX/0ws;->U:S

    const/4 v4, 0x0

    move-object/from16 v0, p7

    invoke-interface {v0, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v3

    if-nez v3, :cond_0

    .line 208085
    :goto_0
    return-object v2

    .line 208086
    :cond_0
    const-string v3, "video-cache-cb"

    sget-wide v4, LX/0ws;->V:J

    const-wide/16 v6, 0x0

    move-object/from16 v0, p7

    invoke-interface {v0, v4, v5, v6, v7}, LX/0ad;->a(JJ)J

    move-result-wide v4

    move-object v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p6

    move-object/from16 v12, p5

    move-object/from16 v13, p8

    move-object/from16 v14, p9

    invoke-static/range {v3 .. v14}, LX/19Y;->a(Ljava/lang/String;JLandroid/content/Context;LX/0SG;LX/1AA;LX/0en;LX/0V8;LX/0lC;LX/0pq;LX/0wp;LX/0Sh;)LX/1Lo;

    move-result-object v4

    .line 208087
    new-instance v3, LX/7Op;

    invoke-direct {v3, v4, v2}, LX/7Op;-><init>(LX/1Ln;LX/1Ln;)V

    move-object v2, v3

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;JLandroid/content/Context;LX/0SG;LX/1AA;LX/0en;LX/0V8;LX/0lC;LX/0pq;LX/0wp;LX/0Sh;)LX/1Lo;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Landroid/content/Context;",
            "LX/0SG;",
            "LX/1AA;",
            "LX/0en;",
            "LX/0V8;",
            "LX/0lC;",
            "LX/0pq;",
            "LX/0wp;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ")",
            "LX/1Lo",
            "<",
            "LX/37C;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208090
    new-instance v0, LX/1Ll;

    invoke-direct {v0, p11, p3, p0}, LX/1Ll;-><init>(LX/0Sh;Landroid/content/Context;Ljava/lang/String;)V

    .line 208091
    new-instance v1, LX/1Lm;

    invoke-direct {v1, v0, p4, p8, p6}, LX/1Lm;-><init>(LX/0Or;LX/0SG;LX/0lC;LX/0en;)V

    .line 208092
    new-instance v0, LX/1Lo;

    new-instance v2, LX/1Lq;

    invoke-direct {v2, p7, p1, p2}, LX/1Lq;-><init>(LX/0V8;J)V

    invoke-direct {v0, v1, v2}, LX/1Lo;-><init>(LX/1Ln;LX/1Lq;)V

    .line 208093
    invoke-virtual {p9, v0}, LX/0pq;->a(LX/0po;)V

    .line 208094
    iget-boolean v1, p10, LX/0wp;->P:Z

    if-eqz v1, :cond_0

    .line 208095
    iput-object p5, v0, LX/1Lo;->d:LX/1AA;

    .line 208096
    :cond_0
    return-object v0
.end method

.method public static a(LX/0ad;)LX/1Lt;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 208077
    sget v0, LX/0ws;->T:I

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    .line 208078
    new-instance v1, LX/1Lt;

    invoke-direct {v1, v0}, LX/1Lt;-><init>(I)V

    return-object v1
.end method

.method public static a(LX/1Ln;LX/1AA;LX/1Lg;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0ka;LX/1Lr;LX/0Or;LX/0yH;Lcom/facebook/http/common/FbHttpRequestProcessor;Landroid/os/Handler;LX/1Lt;LX/0Sj;LX/0Ot;LX/11i;LX/0So;LX/0TR;LX/0WJ;LX/0lC;LX/19s;LX/1Lu;LX/0Ot;LX/0ad;LX/0tQ;)LX/1Lv;
    .locals 25
    .param p0    # LX/1Ln;
        .annotation build Lcom/facebook/video/server/VideoCache;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/IsDialtonePhotoFeatureEnabled;
        .end annotation
    .end param
    .param p10    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ln;",
            "LX/1AA;",
            "LX/1Lg;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0ka;",
            "LX/1Lr;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0yH;",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            "Landroid/os/Handler;",
            "Lcom/facebook/video/server/VideoKeyCreator;",
            "LX/0Sj;",
            "LX/0Ot",
            "<",
            "LX/0A1;",
            ">;",
            "LX/11i;",
            "LX/0So;",
            "LX/0TR;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/0lC;",
            "LX/19s;",
            "LX/1Lu;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;",
            "LX/0ad;",
            "LX/0tQ;",
            ")",
            "LX/1Lv;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 208079
    new-instance v24, LX/1Lv;

    const-string v1, "VideoPrefetching"

    move-object/from16 v0, p6

    iget v2, v0, LX/1Lr;->b:I

    const/16 v3, 0x100

    move-object/from16 v4, p3

    move-object/from16 v5, p12

    move-object/from16 v6, p16

    invoke-static/range {v1 .. v6}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v5

    new-instance v14, LX/1Lw;

    move-object/from16 v0, p9

    invoke-direct {v14, v0}, LX/1Lw;-><init>(Lcom/facebook/http/common/FbHttpRequestProcessor;)V

    move-object/from16 v1, v24

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p13

    move-object/from16 v15, p15

    move-object/from16 v16, p14

    move-object/from16 v17, p17

    move-object/from16 v18, p18

    move-object/from16 v19, p19

    move-object/from16 v20, p20

    move-object/from16 v21, p21

    move-object/from16 v22, p22

    move-object/from16 v23, p23

    invoke-direct/range {v1 .. v23}, LX/1Lv;-><init>(LX/1Ln;LX/1AA;LX/1Lg;LX/0TD;LX/03V;LX/0ka;LX/1Lr;LX/0Or;LX/0yH;Landroid/os/Handler;LX/1Lt;LX/0Ot;LX/1Lw;LX/0So;LX/11i;LX/0WJ;LX/0lC;LX/19s;LX/1Lu;LX/0Ot;LX/0ad;LX/0tQ;)V

    return-object v24
.end method

.method public static a(LX/0So;LX/0kb;LX/0Ot;LX/0Vw;LX/0Zb;)LX/1m2;
    .locals 6
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/video/server/VideoServerListenerForVideoServer;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0So;",
            "LX/0kb;",
            "LX/0Ot",
            "<",
            "LX/0A1;",
            ">;",
            "LX/0Vw;",
            "LX/0Zb;",
            ")",
            "Lcom/facebook/video/server/VideoServerListener;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 208067
    new-instance v0, LX/1m2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/1m2;-><init>(LX/0So;LX/0kb;LX/0Ot;LX/0Vw;LX/0Zb;)V

    return-object v0
.end method

.method public static a(Landroid/os/Handler;LX/1m0;LX/0wp;LX/0oz;LX/0YR;LX/0ka;LX/1AA;LX/0hB;)LX/1mB;
    .locals 8
    .param p0    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 208057
    new-instance v1, LX/1m6;

    invoke-direct {v1}, LX/1m6;-><init>()V

    .line 208058
    iget-object v0, p1, LX/1m0;->o:LX/1m5;

    move-object v2, v0

    .line 208059
    const/4 v3, 0x0

    move-object v0, p2

    move-object v4, p5

    move-object v5, p3

    move-object v6, p4

    move-object v7, p6

    invoke-static/range {v0 .. v7}, LX/1m7;->a(LX/0wp;LX/1m6;LX/04m;ZLX/0ka;LX/0oz;LX/0YR;LX/1AA;)LX/04o;

    move-result-object v0

    .line 208060
    instance-of v1, v0, LX/1mA;

    if-eqz v1, :cond_0

    check-cast v0, LX/1mA;

    move-object v2, v0

    .line 208061
    :goto_0
    new-instance v0, LX/1mB;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, LX/1mB;-><init>(Landroid/os/Handler;LX/1mA;LX/0wp;LX/0ka;LX/1AA;LX/0hB;)V

    .line 208062
    return-object v0

    .line 208063
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static a(LX/0So;LX/0kb;LX/0Ot;LX/0Vw;LX/1Ln;LX/0Sh;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;LX/0ad;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/7PM;LX/19Z;LX/0oz;LX/1Lt;LX/0Ot;LX/0Or;LX/0wq;LX/0V8;LX/0Zb;LX/0WJ;LX/0lC;LX/1Lu;)LX/7Po;
    .locals 24
    .param p4    # LX/1Ln;
        .annotation build Lcom/facebook/video/server/VideoCache;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0So;",
            "LX/0kb;",
            "LX/0Ot",
            "<",
            "LX/0A1;",
            ">;",
            "LX/0Vw;",
            "LX/1Ln;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0ad;",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            "LX/7PM;",
            "LX/19Z;",
            "LX/0oz;",
            "Lcom/facebook/video/server/VideoKeyCreator;",
            "LX/0Ot",
            "<",
            "LX/1Lv;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;",
            "LX/0wq;",
            "LX/0V8;",
            "LX/0Zb;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/0lC;",
            "LX/1Lu;",
            ")",
            "LX/7Po;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 208064
    new-instance v8, LX/7Pt;

    move-object/from16 v0, p17

    invoke-direct {v8, v0}, LX/7Pt;-><init>(LX/0V8;)V

    .line 208065
    new-instance v2, LX/1m2;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p18

    invoke-direct/range {v2 .. v7}, LX/1m2;-><init>(LX/0So;LX/0kb;LX/0Ot;LX/0Vw;LX/0Zb;)V

    .line 208066
    new-instance v3, LX/7Po;

    new-instance v6, LX/16V;

    invoke-direct {v6}, LX/16V;-><init>()V

    new-instance v18, LX/1Lw;

    move-object/from16 v0, v18

    move-object/from16 v1, p9

    invoke-direct {v0, v1}, LX/1Lw;-><init>(Lcom/facebook/http/common/FbHttpRequestProcessor;)V

    move-object/from16 v0, p16

    iget-object v0, v0, LX/0wq;->b:LX/0wr;

    move-object/from16 v22, v0

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v7, p7

    move-object v9, v2

    move-object/from16 v10, p8

    move-object/from16 v11, p14

    move-object/from16 v12, p0

    move-object/from16 v13, p11

    move-object/from16 v14, p13

    move-object/from16 v15, p6

    move-object/from16 v16, p10

    move-object/from16 v17, p12

    move-object/from16 v19, p19

    move-object/from16 v20, p20

    move-object/from16 v21, p15

    move-object/from16 v23, p21

    invoke-direct/range {v3 .. v23}, LX/7Po;-><init>(LX/1Ln;LX/0Sh;LX/16V;LX/03V;LX/0Or;LX/1m2;LX/0ad;LX/0Ot;LX/0So;LX/19Z;LX/1Lt;Ljava/util/concurrent/ScheduledExecutorService;LX/7PM;LX/0oz;LX/1Lw;LX/0WJ;LX/0lC;LX/0Or;LX/0wr;LX/1Lu;)V

    return-object v3
.end method

.method public static a(Landroid/content/Context;LX/0pq;LX/0ad;LX/0Uh;)LX/7Q0;
    .locals 4
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 208072
    new-instance v0, LX/3FK;

    invoke-direct {v0, p2, p3}, LX/3FK;-><init>(LX/0ad;LX/0Uh;)V

    .line 208073
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "video-player-readable-cache"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 208074
    new-instance v2, LX/7Q0;

    iget v0, v0, LX/3FK;->e:I

    invoke-direct {v2, v1, v0}, LX/7Q0;-><init>(Ljava/io/File;I)V

    .line 208075
    invoke-virtual {p1, v2}, LX/0pq;->a(LX/0po;)V

    .line 208076
    return-object v2
.end method

.method public static b(LX/0ad;)LX/19Z;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 208068
    sget-short v0, LX/0ws;->w:S

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 208069
    new-instance v1, LX/19Z;

    invoke-direct {v1, v0}, LX/19Z;-><init>(Z)V

    return-object v1
.end method

.method public static getInstanceForTest_VideoCache(LX/0QA;)LX/1Ln;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QA;",
            ")",
            "LX/1Ln",
            "<",
            "LX/37C;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208070
    invoke-static {p0}, LX/1Lk;->a(LX/0QB;)LX/1Ln;

    move-result-object v0

    check-cast v0, LX/1Ln;

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 208071
    return-void
.end method
