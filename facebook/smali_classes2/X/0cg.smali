.class public LX/0cg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/0cg;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/abtest/qe/bootstrap/cache/QuickExperimentMemoryCache$QuickExperimentMemoryCacheObserver;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/abtest/qe/bootstrap/registry/QuickExperimentCacheUpdateListener;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private c:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private d:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/abtest/qe/bootstrap/registry/QuickExperimentCacheUpdateListener;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 89194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89195
    iput-object p1, p0, LX/0cg;->b:LX/0Or;

    .line 89196
    new-instance v0, LX/0S8;

    invoke-direct {v0}, LX/0S8;-><init>()V

    invoke-virtual {v0}, LX/0S8;->e()LX/0S8;

    move-result-object v0

    invoke-virtual {v0}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/0cg;->a:Ljava/util/Map;

    .line 89197
    iput-boolean v1, p0, LX/0cg;->c:Z

    .line 89198
    iput-boolean v1, p0, LX/0cg;->d:Z

    .line 89199
    return-void
.end method

.method public static a(LX/0QB;)LX/0cg;
    .locals 5

    .prologue
    .line 89150
    sget-object v0, LX/0cg;->e:LX/0cg;

    if-nez v0, :cond_1

    .line 89151
    const-class v1, LX/0cg;

    monitor-enter v1

    .line 89152
    :try_start_0
    sget-object v0, LX/0cg;->e:LX/0cg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 89153
    if-eqz v2, :cond_0

    .line 89154
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 89155
    new-instance v3, LX/0cg;

    .line 89156
    new-instance v4, LX/0ch;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-direct {v4, p0}, LX/0ch;-><init>(LX/0QB;)V

    move-object v4, v4

    .line 89157
    invoke-direct {v3, v4}, LX/0cg;-><init>(LX/0Or;)V

    .line 89158
    move-object v0, v3

    .line 89159
    sput-object v0, LX/0cg;->e:LX/0cg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89160
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 89161
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 89162
    :cond_1
    sget-object v0, LX/0cg;->e:LX/0cg;

    return-object v0

    .line 89163
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 89164
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized b()V
    .locals 2

    .prologue
    .line 89165
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/0cg;->d:Z

    .line 89166
    iget-boolean v0, p0, LX/0cg;->c:Z

    if-eqz v0, :cond_1

    .line 89167
    iget-object v0, p0, LX/0cg;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yu;

    .line 89168
    invoke-virtual {v0}, LX/0Yu;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 89169
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 89170
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0cg;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 89171
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3li;

    .line 89172
    invoke-static {v0}, LX/3li;->c(LX/3li;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89173
    goto :goto_1

    .line 89174
    :cond_1
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 89175
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0cg;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yu;

    .line 89176
    const-string v2, ""

    iput-object v2, v0, LX/0Yu;->d:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89177
    goto :goto_0

    .line 89178
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 89179
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0cg;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 89180
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3li;

    .line 89181
    const/4 v2, 0x0

    invoke-static {v0, v2}, LX/3li;->a(LX/3li;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89182
    goto :goto_1

    .line 89183
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(LX/0Yu;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 89184
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0cg;->a:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89185
    iget-boolean v0, p0, LX/0cg;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/0cg;->d:Z

    if-eqz v0, :cond_0

    .line 89186
    invoke-virtual {p1}, LX/0Yu;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89187
    :cond_0
    monitor-exit p0

    return-void

    .line 89188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized init()V
    .locals 1

    .prologue
    .line 89189
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/0cg;->c:Z

    .line 89190
    iget-boolean v0, p0, LX/0cg;->d:Z

    if-eqz v0, :cond_0

    .line 89191
    invoke-direct {p0}, LX/0cg;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89192
    :cond_0
    monitor-exit p0

    return-void

    .line 89193
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
