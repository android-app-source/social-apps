.class public LX/1I1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1Hq;

.field public final b:Lcom/facebook/crypto/keychain/KeyChain;

.field public final c:LX/1Hy;


# direct methods
.method public constructor <init>(LX/1Hq;Lcom/facebook/crypto/keychain/KeyChain;LX/1Hy;)V
    .locals 0

    .prologue
    .line 228078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 228079
    iput-object p1, p0, LX/1I1;->a:LX/1Hq;

    .line 228080
    iput-object p2, p0, LX/1I1;->b:Lcom/facebook/crypto/keychain/KeyChain;

    .line 228081
    iput-object p3, p0, LX/1I1;->c:LX/1Hy;

    .line 228082
    return-void
.end method

.method public static a(Lcom/facebook/crypto/cipher/NativeGCMCipher;BB[B)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 228083
    new-array v0, v2, [B

    aput-byte p1, v0, v3

    .line 228084
    new-array v1, v2, [B

    aput-byte p2, v1, v3

    .line 228085
    invoke-virtual {p0, v0, v2}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a([BI)V

    .line 228086
    invoke-virtual {p0, v1, v2}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a([BI)V

    .line 228087
    array-length v0, p3

    invoke-virtual {p0, p3, v0}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a([BI)V

    .line 228088
    return-void
.end method
