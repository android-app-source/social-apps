.class public LX/0hg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/0hg;


# instance fields
.field public final a:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Z

.field public g:Z


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 118077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118078
    iput-object p1, p0, LX/0hg;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 118079
    return-void
.end method

.method public static a(LX/0QB;)LX/0hg;
    .locals 4

    .prologue
    .line 118064
    sget-object v0, LX/0hg;->h:LX/0hg;

    if-nez v0, :cond_1

    .line 118065
    const-class v1, LX/0hg;

    monitor-enter v1

    .line 118066
    :try_start_0
    sget-object v0, LX/0hg;->h:LX/0hg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 118067
    if-eqz v2, :cond_0

    .line 118068
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 118069
    new-instance p0, LX/0hg;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {p0, v3}, LX/0hg;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 118070
    move-object v0, p0

    .line 118071
    sput-object v0, LX/0hg;->h:LX/0hg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118072
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 118073
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 118074
    :cond_1
    sget-object v0, LX/0hg;->h:LX/0hg;

    return-object v0

    .line 118075
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 118076
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0hg;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 118080
    iget-object v0, p0, LX/0hg;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 118081
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string p0, "::"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 118082
    invoke-interface {v0, p1, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 118083
    return-void
.end method

.method public static a(LX/0hg;ILjava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 118049
    iget-object v0, p0, LX/0hg;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 118050
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 118051
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, p1, v1, v0}, LX/0hg;->a(LX/0hg;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 118052
    :cond_0
    return-void
.end method

.method public static h(LX/0hg;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118058
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 118059
    const-string v1, "produce_name"

    sget-object v2, LX/7gc;->SHARESHEET:LX/7gc;

    invoke-virtual {v2}, LX/7gc;->getProductName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118060
    sget-object v1, LX/7gb;->SHARESHEET_SESSION_ID:LX/7gb;

    invoke-virtual {v1}, LX/7gb;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/0hg;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118061
    sget-object v1, LX/7gb;->PROMPT_ID:LX/7gb;

    invoke-virtual {v1}, LX/7gb;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/0hg;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118062
    sget-object v1, LX/7gb;->INSPIRATION_GROUP_ID:LX/7gb;

    invoke-virtual {v1}, LX/7gb;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/0hg;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118063
    return-object v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 4

    .prologue
    const v3, 0xbc0006

    .line 118053
    iget-object v0, p0, LX/0hg;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x1

    invoke-interface {v0, v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 118054
    if-eqz p1, :cond_0

    .line 118055
    iget-object v0, p0, LX/0hg;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "source"

    const-string v2, "sharesheet"

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 118056
    :goto_0
    return-void

    .line 118057
    :cond_0
    iget-object v0, p0, LX/0hg;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "source"

    const-string v2, "divebar"

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
