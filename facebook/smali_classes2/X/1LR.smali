.class public LX/1LR;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0fm;
.implements LX/1Cf;
.implements LX/0ft;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Dispatcher::",
        "LX/0fq;",
        ">",
        "LX/0hD;",
        "LX/0fm;",
        "LX/1Cf;",
        "LX/0ft;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wL;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0pV;

.field private final d:LX/0qa;

.field public final e:Z

.field public final f:LX/0ad;

.field public g:LX/1Nv;

.field public h:LX/0fq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TDispatcher;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 233745
    const-class v0, LX/1LR;

    sput-object v0, LX/1LR;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0pV;LX/0Ot;LX/0qa;LX/0ad;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0pV;",
            "LX/0Ot",
            "<",
            "LX/0wL;",
            ">;",
            "LX/0qa;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .prologue
    .line 233746
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 233747
    iput-object p1, p0, LX/1LR;->c:LX/0pV;

    .line 233748
    iput-object p2, p0, LX/1LR;->b:LX/0Ot;

    .line 233749
    iput-object p3, p0, LX/1LR;->d:LX/0qa;

    .line 233750
    sget-short v0, LX/0fe;->bG:S

    const/4 v1, 0x0

    invoke-interface {p4, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/1LR;->e:Z

    .line 233751
    iput-object p4, p0, LX/1LR;->f:LX/0ad;

    .line 233752
    return-void
.end method

.method public static d(LX/1LR;)V
    .locals 3

    .prologue
    .line 233753
    iget-object v0, p0, LX/1LR;->d:LX/0qa;

    .line 233754
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0qa;->a(LX/0qa;Z)V

    .line 233755
    iget-object v0, p0, LX/1LR;->g:LX/1Nv;

    if-eqz v0, :cond_0

    .line 233756
    iget-object v0, p0, LX/1LR;->g:LX/1Nv;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1Nv;->setRefreshing(Z)V

    .line 233757
    :goto_0
    return-void

    .line 233758
    :cond_0
    iget-object v0, p0, LX/1LR;->c:LX/0pV;

    sget-object v1, LX/1LR;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/0rj;->SWIPE_LAYOUT_NULL:LX/0rj;

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;LX/0rj;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0gf;)V
    .locals 3

    .prologue
    .line 233759
    invoke-virtual {p1}, LX/0gf;->isFetchAtTopOfFeed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233760
    invoke-virtual {p1}, LX/0gf;->isScrollHeadFetch()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/1LR;->e:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 233761
    if-nez v0, :cond_0

    .line 233762
    iget-object v0, p0, LX/1LR;->d:LX/0qa;

    .line 233763
    iget-object v1, v0, LX/0qa;->c:LX/0pV;

    const-string v2, "NewsFeedFragment"

    sget-object p1, LX/0rj;->LOADING_INDICATOR_SHOWN:LX/0rj;

    invoke-virtual {v1, v2, p1}, LX/0pV;->a(Ljava/lang/String;LX/0rj;)V

    .line 233764
    iget-object v0, p0, LX/1LR;->g:LX/1Nv;

    if-eqz v0, :cond_1

    .line 233765
    iget-object v0, p0, LX/1LR;->g:LX/1Nv;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Nv;->setRefreshing(Z)V

    .line 233766
    :cond_0
    :goto_1
    return-void

    .line 233767
    :cond_1
    iget-object v0, p0, LX/1LR;->c:LX/0pV;

    sget-object v1, LX/1LR;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/0rj;->SWIPE_LAYOUT_NULL:LX/0rj;

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;LX/0rj;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 233768
    const v0, 0x7f0d120b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/1Nv;

    iput-object v0, p0, LX/1LR;->g:LX/1Nv;

    .line 233769
    iget-object v0, p0, LX/1LR;->g:LX/1Nv;

    new-instance v1, LX/1PG;

    invoke-direct {v1, p0}, LX/1PG;-><init>(LX/1LR;)V

    invoke-interface {v0, v1}, LX/1Nv;->setOnRefreshListener(LX/1PH;)V

    .line 233770
    iget-object v0, p0, LX/1LR;->f:LX/0ad;

    sget-short v1, LX/0fe;->aG:S

    const/4 p1, 0x0

    invoke-interface {v0, v1, p1}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 233771
    if-eqz v0, :cond_0

    .line 233772
    iget-object v0, p0, LX/1LR;->g:LX/1Nv;

    invoke-interface {v0, v2}, LX/1Nv;->setEnabled(Z)V

    .line 233773
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 233774
    if-nez p1, :cond_0

    .line 233775
    invoke-static {p0}, LX/1LR;->d(LX/1LR;)V

    .line 233776
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 233777
    iget-object v0, p0, LX/1LR;->d:LX/0qa;

    invoke-virtual {v0}, LX/0qa;->c()V

    .line 233778
    iget-object v0, p0, LX/1LR;->g:LX/1Nv;

    invoke-interface {v0, v1}, LX/1Nv;->setOnRefreshListener(LX/1PH;)V

    .line 233779
    iput-object v1, p0, LX/1LR;->g:LX/1Nv;

    .line 233780
    return-void
.end method
