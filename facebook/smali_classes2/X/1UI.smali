.class public LX/1UI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VH:",
        "LX/1a1;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/1UG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1UG",
            "<TVH;>;"
        }
    .end annotation
.end field

.field public final b:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/1df",
            "<TVH;>;>;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/concurrent/ExecutorService;

.field private final e:I

.field private f:LX/1Zy;

.field private g:I

.field public h:I

.field public i:Z


# direct methods
.method public constructor <init>(LX/1UG;Landroid/view/ViewGroup;Ljava/lang/Integer;LX/0Wd;)V
    .locals 1
    .param p1    # LX/1UG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1UG",
            "<TVH;>;",
            "Landroid/view/ViewGroup;",
            "Ljava/lang/Integer;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 255757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255758
    iput-object p1, p0, LX/1UI;->a:LX/1UG;

    .line 255759
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/1UI;->c:Ljava/lang/ref/WeakReference;

    .line 255760
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/1UI;->e:I

    .line 255761
    iput-object p4, p0, LX/1UI;->d:Ljava/util/concurrent/ExecutorService;

    .line 255762
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/1UI;->b:LX/0YU;

    .line 255763
    return-void
.end method

.method public static a(LX/1df;)Z
    .locals 2

    .prologue
    .line 255752
    iget v0, p0, LX/1df;->a:I

    iget v1, p0, LX/1df;->b:I

    if-lt v0, v1, :cond_0

    .line 255753
    const/4 v0, 0x0

    .line 255754
    :goto_0
    return v0

    .line 255755
    :cond_0
    invoke-virtual {p0}, LX/1df;->b()V

    .line 255756
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static d(LX/1UI;I)LX/1df;
    .locals 1

    .prologue
    .line 255750
    iget-object v0, p0, LX/1UI;->a:LX/1UG;

    invoke-interface {v0, p1}, LX/1UG;->getItemViewType(I)I

    move-result v0

    .line 255751
    invoke-virtual {p0, v0}, LX/1UI;->c(I)LX/1df;

    move-result-object v0

    return-object v0
.end method

.method public static i(LX/1UI;)V
    .locals 1

    .prologue
    .line 255746
    iget-object v0, p0, LX/1UI;->f:LX/1Zy;

    if-eqz v0, :cond_0

    .line 255747
    iget-object v0, p0, LX/1UI;->f:LX/1Zy;

    invoke-virtual {v0}, LX/1Rm;->b()V

    .line 255748
    const/4 v0, 0x0

    iput-object v0, p0, LX/1UI;->f:LX/1Zy;

    .line 255749
    :cond_0
    return-void
.end method

.method private k()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 255740
    :cond_0
    iget v2, p0, LX/1UI;->g:I

    iget-object v3, p0, LX/1UI;->a:LX/1UG;

    invoke-interface {v3}, LX/1UG;->ij_()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 255741
    iget v2, p0, LX/1UI;->g:I

    invoke-static {p0, v2}, LX/1UI;->d(LX/1UI;I)LX/1df;

    move-result-object v2

    .line 255742
    iget v3, p0, LX/1UI;->g:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/1UI;->g:I

    .line 255743
    invoke-static {v2}, LX/1UI;->a(LX/1df;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 255744
    iget-boolean v2, p0, LX/1UI;->i:Z

    if-nez v2, :cond_1

    move v0, v1

    :cond_1
    iput-boolean v0, p0, LX/1UI;->i:Z

    .line 255745
    :goto_0
    return v1

    :cond_2
    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/1a1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TVH;"
        }
    .end annotation

    .prologue
    .line 255688
    invoke-virtual {p0, p1}, LX/1UI;->c(I)LX/1df;

    move-result-object v0

    .line 255689
    iget-object p0, v0, LX/1df;->d:Ljava/util/Stack;

    invoke-virtual {p0}, Ljava/util/Stack;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 255690
    invoke-virtual {v0}, LX/1df;->b()V

    .line 255691
    :cond_0
    iget-object p0, v0, LX/1df;->d:Ljava/util/Stack;

    invoke-virtual {p0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1a1;

    move-object v0, p0

    .line 255692
    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 255737
    new-instance v0, LX/1Zy;

    iget-object v1, p0, LX/1UI;->d:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v0, p0, v1}, LX/1Zy;-><init>(LX/1UI;Ljava/util/concurrent/ExecutorService;)V

    iput-object v0, p0, LX/1UI;->f:LX/1Zy;

    .line 255738
    iget-object v0, p0, LX/1UI;->f:LX/1Zy;

    invoke-virtual {v0}, LX/1Rm;->a()V

    .line 255739
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 255732
    iget-object v0, p0, LX/1UI;->a:LX/1UG;

    invoke-interface {v0, p1}, LX/1UG;->getItemViewType(I)I

    move-result v0

    .line 255733
    invoke-virtual {p0, v0}, LX/1UI;->c(I)LX/1df;

    move-result-object v0

    .line 255734
    iget v1, v0, LX/1df;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/1df;->c:I

    .line 255735
    iget v1, v0, LX/1df;->b:I

    iget v2, v0, LX/1df;->c:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, LX/1df;->b:I

    .line 255736
    return-void
.end method

.method public final c(I)LX/1df;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1df",
            "<TVH;>;"
        }
    .end annotation

    .prologue
    .line 255729
    iget-object v0, p0, LX/1UI;->b:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 255730
    iget-object v0, p0, LX/1UI;->b:LX/0YU;

    new-instance v1, LX/1df;

    new-instance v2, LX/1dg;

    invoke-direct {v2, p0, p1}, LX/1dg;-><init>(LX/1UI;I)V

    invoke-direct {v1, v2}, LX/1df;-><init>(Ljava/util/concurrent/Callable;)V

    invoke-virtual {v0, p1, v1}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 255731
    :cond_0
    iget-object v0, p0, LX/1UI;->b:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1df;

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 255725
    invoke-virtual {p0}, LX/1UI;->f()V

    .line 255726
    iget-object v0, p0, LX/1UI;->a:LX/1UG;

    invoke-interface {v0}, LX/1UG;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, LX/1UI;->a:LX/1UG;

    invoke-interface {v1}, LX/1UG;->b()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LX/1UI;->g:I

    .line 255727
    iget v0, p0, LX/1UI;->g:I

    iput v0, p0, LX/1UI;->h:I

    .line 255728
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 255722
    invoke-static {p0}, LX/1UI;->i(LX/1UI;)V

    .line 255723
    iget-object v0, p0, LX/1UI;->b:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->b()V

    .line 255724
    return-void
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 255721
    iget-object v0, p0, LX/1UI;->a:LX/1UG;

    invoke-interface {v0}, LX/1UG;->ij_()I

    move-result v0

    iget v1, p0, LX/1UI;->e:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public f()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 255708
    iget-object v1, p0, LX/1UI;->b:LX/0YU;

    invoke-virtual {v1}, LX/0YU;->a()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 255709
    iget-object v3, p0, LX/1UI;->b:LX/0YU;

    invoke-virtual {v3, v1}, LX/0YU;->e(I)I

    move-result v3

    invoke-virtual {p0, v3}, LX/1UI;->c(I)LX/1df;

    move-result-object v3

    iput v0, v3, LX/1df;->c:I

    .line 255710
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 255711
    :cond_0
    iget-object v1, p0, LX/1UI;->a:LX/1UG;

    invoke-interface {v1}, LX/1UG;->ij_()I

    move-result v2

    .line 255712
    invoke-virtual {p0}, LX/1UI;->e()I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_1

    .line 255713
    invoke-virtual {p0, v0}, LX/1UI;->b(I)V

    .line 255714
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 255715
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0}, LX/1UI;->e()I

    move-result v0

    :goto_2
    if-ge v0, v2, :cond_2

    .line 255716
    iget-object v3, p0, LX/1UI;->a:LX/1UG;

    add-int/lit8 v4, v1, -0x1

    invoke-interface {v3, v4}, LX/1UG;->getItemViewType(I)I

    move-result v3

    .line 255717
    invoke-virtual {p0, v3}, LX/1UI;->c(I)LX/1df;

    move-result-object v3

    iget v4, v3, LX/1df;->c:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v3, LX/1df;->c:I

    .line 255718
    invoke-virtual {p0, v0}, LX/1UI;->b(I)V

    .line 255719
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 255720
    :cond_2
    return-void
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 255695
    iget-boolean v0, p0, LX/1UI;->i:Z

    if-nez v0, :cond_0

    iget v0, p0, LX/1UI;->h:I

    if-gez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 255696
    :goto_0
    if-eqz v0, :cond_3

    invoke-direct {p0}, LX/1UI;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 255697
    :cond_1
    :goto_1
    return-void

    .line 255698
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 255699
    :cond_3
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 255700
    :cond_4
    iget v2, p0, LX/1UI;->h:I

    if-ltz v2, :cond_6

    .line 255701
    iget v2, p0, LX/1UI;->h:I

    invoke-static {p0, v2}, LX/1UI;->d(LX/1UI;I)LX/1df;

    move-result-object v2

    .line 255702
    iget v3, p0, LX/1UI;->h:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, LX/1UI;->h:I

    .line 255703
    invoke-static {v2}, LX/1UI;->a(LX/1df;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 255704
    iget-boolean v2, p0, LX/1UI;->i:Z

    if-nez v2, :cond_5

    move v0, v1

    :cond_5
    iput-boolean v0, p0, LX/1UI;->i:Z

    .line 255705
    :goto_2
    move v0, v1

    .line 255706
    if-nez v0, :cond_1

    .line 255707
    invoke-direct {p0}, LX/1UI;->k()Z

    goto :goto_1

    :cond_6
    move v1, v0

    goto :goto_2
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 255693
    iget-object v0, p0, LX/1UI;->a:LX/1UG;

    invoke-interface {v0}, LX/1UG;->ij_()I

    move-result v0

    .line 255694
    if-lez v0, :cond_1

    iget v1, p0, LX/1UI;->g:I

    if-lt v1, v0, :cond_0

    iget v0, p0, LX/1UI;->h:I

    if-ltz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
