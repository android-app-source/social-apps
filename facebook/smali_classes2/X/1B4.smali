.class public LX/1B4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0pf;

.field public b:LX/1B5;

.field public final c:Ljava/lang/Boolean;

.field public final d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(LX/0pf;LX/1B5;LX/0Or;LX/0Or;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsSponsoredFullViewDebugLoggingEnabled;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsSponsoredFullViewLoggedTrackingEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0pf;",
            "LX/1B5;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 211901
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211902
    iput-object p1, p0, LX/1B4;->a:LX/0pf;

    .line 211903
    iput-object p2, p0, LX/1B4;->b:LX/1B5;

    .line 211904
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, LX/1B4;->c:Ljava/lang/Boolean;

    .line 211905
    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, LX/1B4;->d:Ljava/lang/Boolean;

    .line 211906
    return-void
.end method

.method public static b(LX/1B4;Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 211907
    iget-object v0, p0, LX/1B4;->a:LX/0pf;

    invoke-virtual {v0, p1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v0

    .line 211908
    iget-boolean p0, v0, LX/1g0;->l:Z

    move v0, p0

    .line 211909
    return v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/FeedUnit;I)V
    .locals 3

    .prologue
    .line 211910
    if-gez p2, :cond_1

    .line 211911
    :cond_0
    :goto_0
    return-void

    .line 211912
    :cond_1
    invoke-static {p1}, LX/18J;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 211913
    check-cast v0, Lcom/facebook/graphql/model/Sponsorable;

    invoke-interface {v0}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    .line 211914
    iget-boolean v1, v0, Lcom/facebook/graphql/model/SponsoredImpression;->y:Z

    move v0, v1

    .line 211915
    if-eqz v0, :cond_0

    .line 211916
    iget-object v0, p0, LX/1B4;->a:LX/0pf;

    invoke-virtual {v0, p1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v0

    .line 211917
    iget v1, v0, LX/1g0;->m:I

    move v0, v1

    .line 211918
    iget-object v1, p0, LX/1B4;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 211919
    shl-int v2, v1, p2

    and-int/2addr v2, v0

    if-eqz v2, :cond_3

    :goto_1
    move v1, v1

    .line 211920
    if-nez v1, :cond_0

    .line 211921
    :cond_2
    iget-object v1, p0, LX/1B4;->b:LX/1B5;

    invoke-virtual {v1, p1}, LX/1B5;->b(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 211922
    iget-object v1, p0, LX/1B4;->a:LX/0pf;

    invoke-virtual {v1, p1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v1

    const/4 v2, 0x1

    shl-int/2addr v2, p2

    or-int/2addr v0, v2

    .line 211923
    iput v0, v1, LX/1g0;->m:I

    .line 211924
    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method
