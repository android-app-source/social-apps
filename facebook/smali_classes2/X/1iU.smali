.class public final LX/1iU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/tigon/iface/TigonRequest;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1iO;

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1he",
            "<*>;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/tigon/iface/TigonRequestBuilder;)V
    .locals 1

    .prologue
    .line 298415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298416
    iget-object v0, p1, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a:Ljava/lang/String;

    iput-object v0, p0, LX/1iU;->a:Ljava/lang/String;

    .line 298417
    iget-object v0, p1, Lcom/facebook/tigon/iface/TigonRequestBuilder;->b:Ljava/lang/String;

    iput-object v0, p0, LX/1iU;->b:Ljava/lang/String;

    .line 298418
    iget-object v0, p1, Lcom/facebook/tigon/iface/TigonRequestBuilder;->c:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/1iU;->c:Ljava/util/Map;

    .line 298419
    iget-object v0, p1, Lcom/facebook/tigon/iface/TigonRequestBuilder;->d:LX/1iO;

    iput-object v0, p0, LX/1iU;->d:LX/1iO;

    .line 298420
    iget-object v0, p1, Lcom/facebook/tigon/iface/TigonRequestBuilder;->e:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/tigon/iface/TigonRequestBuilder;->e:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/1iU;->e:Ljava/util/Map;

    .line 298421
    return-void

    .line 298422
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1he;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1he",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 298423
    iget-object v0, p0, LX/1iU;->e:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 298424
    const/4 v0, 0x0

    .line 298425
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1iU;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298426
    iget-object v0, p0, LX/1iU;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298427
    iget-object v0, p0, LX/1iU;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 298428
    iget-object v0, p0, LX/1iU;->c:Ljava/util/Map;

    return-object v0
.end method

.method public final d()LX/1iO;
    .locals 1

    .prologue
    .line 298429
    iget-object v0, p0, LX/1iU;->d:LX/1iO;

    return-object v0
.end method
