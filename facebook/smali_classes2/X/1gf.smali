.class public LX/1gf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1ge;

.field public final b:LX/1ge;

.field public final c:LX/1ge;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/1gc;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 294473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 294474
    iput-object p2, p0, LX/1gf;->d:Ljava/lang/String;

    .line 294475
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/1gf;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_new_stories_load_status"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/1gc;->a(Ljava/lang/String;)LX/1ge;

    move-result-object v0

    iput-object v0, p0, LX/1gf;->a:LX/1ge;

    .line 294476
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/1gf;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_more_stories_load_status"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/1gc;->a(Ljava/lang/String;)LX/1ge;

    move-result-object v0

    iput-object v0, p0, LX/1gf;->b:LX/1ge;

    .line 294477
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/1gf;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_older_stories_load_status"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/1gc;->a(Ljava/lang/String;)LX/1ge;

    move-result-object v0

    iput-object v0, p0, LX/1gf;->c:LX/1ge;

    .line 294478
    return-void
.end method


# virtual methods
.method public final b()Z
    .locals 1

    .prologue
    .line 294479
    invoke-virtual {p0}, LX/1gf;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/1gf;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 294480
    iget-object v0, p0, LX/1gf;->c:LX/1ge;

    invoke-virtual {v0}, LX/1ge;->d()Z

    move-result v0

    move v0, v0

    .line 294481
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 294482
    iget-object v0, p0, LX/1gf;->a:LX/1ge;

    invoke-virtual {v0}, LX/1ge;->d()Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 294483
    iget-object v0, p0, LX/1gf;->b:LX/1ge;

    invoke-virtual {v0}, LX/1ge;->d()Z

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 294484
    const-string v0, "%s, %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/1gf;->a:LX/1ge;

    invoke-virtual {v3}, LX/1ge;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LX/1gf;->b:LX/1ge;

    invoke-virtual {v3}, LX/1ge;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
