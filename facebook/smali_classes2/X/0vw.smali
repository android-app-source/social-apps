.class public LX/0vw;
.super LX/0vx;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 158340
    invoke-direct {p0}, LX/0vx;-><init>()V

    return-void
.end method


# virtual methods
.method public final B(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 158343
    invoke-virtual {p1}, Landroid/view/View;->getFitsSystemWindows()Z

    move-result v0

    move v0, v0

    .line 158344
    return v0
.end method

.method public final a(Landroid/view/View;IIII)V
    .locals 0

    .prologue
    .line 158341
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/view/View;->postInvalidate(IIII)V

    .line 158342
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 158318
    invoke-virtual {p1, p2}, Landroid/view/View;->postOnAnimation(Ljava/lang/Runnable;)V

    .line 158319
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 158338
    invoke-virtual {p1, p2, p3, p4}, Landroid/view/View;->postOnAnimationDelayed(Ljava/lang/Runnable;J)V

    .line 158339
    return-void
.end method

.method public final a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 158336
    invoke-virtual {p1, p2, p3}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v0

    move v0, v0

    .line 158337
    return v0
.end method

.method public final c(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 158334
    invoke-virtual {p1}, Landroid/view/View;->hasTransientState()Z

    move-result v0

    move v0, v0

    .line 158335
    return v0
.end method

.method public final d(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 158345
    invoke-virtual {p1}, Landroid/view/View;->postInvalidateOnAnimation()V

    .line 158346
    return-void
.end method

.method public d(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 158330
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 158331
    const/4 p2, 0x2

    .line 158332
    :cond_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 158333
    return-void
.end method

.method public final e(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158328
    invoke-virtual {p1}, Landroid/view/View;->getImportantForAccessibility()I

    move-result v0

    move v0, v0

    .line 158329
    return v0
.end method

.method public final i(Landroid/view/View;)Landroid/view/ViewParent;
    .locals 1

    .prologue
    .line 158326
    invoke-virtual {p1}, Landroid/view/View;->getParentForAccessibility()Landroid/view/ViewParent;

    move-result-object v0

    move-object v0, v0

    .line 158327
    return-object v0
.end method

.method public final u(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158324
    invoke-virtual {p1}, Landroid/view/View;->getMinimumWidth()I

    move-result v0

    move v0, v0

    .line 158325
    return v0
.end method

.method public final v(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158322
    invoke-virtual {p1}, Landroid/view/View;->getMinimumHeight()I

    move-result v0

    move v0, v0

    .line 158323
    return v0
.end method

.method public y(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 158320
    invoke-virtual {p1}, Landroid/view/View;->requestFitSystemWindows()V

    .line 158321
    return-void
.end method
