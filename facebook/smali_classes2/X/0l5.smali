.class public LX/0l5;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0SI;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 128251
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0SI;
    .locals 4

    .prologue
    .line 128252
    const-class v1, LX/0l5;

    monitor-enter v1

    .line 128253
    :try_start_0
    sget-object v0, LX/0l5;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 128254
    sput-object v2, LX/0l5;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 128255
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128256
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 128257
    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v3

    check-cast v3, LX/0WJ;

    const-class p0, Landroid/content/Context;

    invoke-interface {v0, p0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    invoke-static {v3, p0}, LX/0X2;->b(LX/0WJ;Landroid/content/Context;)LX/0SI;

    move-result-object v3

    move-object v0, v3

    .line 128258
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 128259
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/0SI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 128260
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 128261
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 128262
    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v0

    check-cast v0, LX/0WJ;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {v0, v1}, LX/0X2;->b(LX/0WJ;Landroid/content/Context;)LX/0SI;

    move-result-object v0

    return-object v0
.end method
