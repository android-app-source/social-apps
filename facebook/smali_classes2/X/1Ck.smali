.class public LX/1Ck;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Key:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static final d:LX/1Cl;


# instance fields
.field public final a:LX/0Xq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xq",
            "<TKey;",
            "LX/1Mv",
            "<*>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final b:LX/0Sh;

.field public final c:Ljava/util/concurrent/Executor;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 216737
    new-instance v0, LX/1Cl;

    sget-object v1, LX/1Cm;->DISPOSED:LX/1Cm;

    invoke-direct {v0, v2, v2, v1}, LX/1Cl;-><init>(Ljava/lang/Object;Ljava/lang/Throwable;LX/1Cm;)V

    sput-object v0, LX/1Ck;->d:LX/1Cl;

    return-void
.end method

.method public constructor <init>(LX/0Sh;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 216791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216792
    iput-object p1, p0, LX/1Ck;->b:LX/0Sh;

    .line 216793
    iput-object p2, p0, LX/1Ck;->c:Ljava/util/concurrent/Executor;

    .line 216794
    invoke-static {}, LX/0Xq;->t()LX/0Xq;

    move-result-object v0

    iput-object v0, p0, LX/1Ck;->a:LX/0Xq;

    .line 216795
    return-void
.end method

.method public static a(LX/0QB;)LX/1Ck;
    .locals 1

    .prologue
    .line 216796
    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 216797
    :try_start_0
    invoke-interface {p0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 216798
    :catch_0
    move-exception v0

    .line 216799
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static declared-synchronized a$redex0(LX/1Ck;Ljava/lang/Object;LX/0Ve;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;",
            "LX/0Ve",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 216821
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Ck;->a:LX/0Xq;

    invoke-virtual {v0, p1}, LX/0Xr;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Mv;

    .line 216822
    iget-object v2, v0, LX/1Mv;->b:LX/0Ve;

    move-object v2, v2

    .line 216823
    if-ne v2, p2, :cond_0

    .line 216824
    iget-object v1, p0, LX/1Ck;->a:LX/0Xq;

    invoke-virtual {v1, p1, v0}, LX/0Xq;->c(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216825
    :cond_1
    monitor-exit p0

    return-void

    .line 216826
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static b(LX/0QB;)LX/1Ck;
    .locals 3

    .prologue
    .line 216800
    new-instance v2, LX/1Ck;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {v2, v0, v1}, LX/1Ck;-><init>(LX/0Sh;Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 216801
    return-object v2
.end method

.method private d(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TKey;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;",
            "LX/0Ve",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 216802
    new-instance v0, LX/1Mv;

    invoke-direct {v0, p2, p3}, LX/1Mv;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 216803
    monitor-enter p0

    .line 216804
    :try_start_0
    iget-object v1, p0, LX/1Ck;->a:LX/0Xq;

    invoke-virtual {v1, p1, v0}, LX/0Xs;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 216805
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216806
    iget-object v1, v0, LX/1Mv;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v1, v1

    .line 216807
    iget-object p1, v0, LX/1Mv;->b:LX/0Ve;

    move-object p1, p1

    .line 216808
    iget-object p2, p0, LX/1Ck;->c:Ljava/util/concurrent/Executor;

    invoke-static {v1, p1, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 216809
    return-void

    .line 216810
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private e(Ljava/lang/Object;)LX/0Ve;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TKey;)",
            "LX/0Ve",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 216811
    monitor-enter p0

    .line 216812
    :try_start_0
    iget-object v0, p0, LX/1Ck;->a:LX/0Xq;

    invoke-virtual {v0, p1}, LX/0Xr;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 216813
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216814
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 216815
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Mv;

    .line 216816
    :goto_0
    if-eqz v0, :cond_0

    .line 216817
    iget-object v1, v0, LX/1Mv;->b:LX/0Ve;

    move-object v0, v1

    .line 216818
    :goto_1
    return-object v0

    .line 216819
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object v0, v1

    .line 216820
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TKey;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;",
            "LX/0Ve",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 216786
    iget-object v0, p0, LX/1Ck;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 216787
    invoke-virtual {p0, p1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 216788
    new-instance v0, LX/1Mu;

    invoke-direct {v0, p0, p1, p3}, LX/1Mu;-><init>(LX/1Ck;Ljava/lang/Object;LX/0Ve;)V

    invoke-direct {p0, p1, p2, v0}, LX/1Ck;->d(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 216789
    return-void
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 216790
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Ck;->a:LX/0Xq;

    invoke-virtual {v0}, LX/0Xq;->n()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;)Z"
        }
    .end annotation

    .prologue
    .line 216785
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Ck;->a:LX/0Xq;

    invoke-virtual {v0, p1}, LX/0Xq;->f(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TKey;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;>;",
            "LX/0Ve",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 216779
    iget-object v0, p0, LX/1Ck;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 216780
    invoke-virtual {p0, p1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216781
    const/4 v0, 0x0

    .line 216782
    :goto_0
    return v0

    .line 216783
    :cond_0
    invoke-static {p2}, LX/1Ck;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/1Mu;

    invoke-direct {v1, p0, p1, p3}, LX/1Mu;-><init>(LX/1Ck;Ljava/lang/Object;LX/0Ve;)V

    invoke-direct {p0, p1, v0, v1}, LX/1Ck;->d(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 216784
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final declared-synchronized b()LX/1M1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1M1",
            "<TKey;>;"
        }
    .end annotation

    .prologue
    .line 216778
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Ck;->a:LX/0Xq;

    invoke-virtual {v0}, LX/0Xq;->q()LX/1M1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 216768
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, LX/1Ck;->a:LX/0Xq;

    invoke-virtual {v2, p1}, LX/0Xr;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 216769
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-gt v3, v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 216770
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 216771
    const/4 v0, 0x0

    .line 216772
    :goto_1
    monitor-exit p0

    return-object v0

    :cond_0
    move v0, v1

    .line 216773
    goto :goto_0

    .line 216774
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Mv;

    .line 216775
    iget-object v1, v0, LX/1Mv;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216776
    goto :goto_1

    .line 216777
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TKey;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;",
            "LX/0Ve",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 216766
    new-instance v0, LX/4mw;

    invoke-direct {v0, p0, p1, p3}, LX/4mw;-><init>(LX/1Ck;Ljava/lang/Object;LX/0Ve;)V

    invoke-direct {p0, p1, p2, v0}, LX/1Ck;->d(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 216767
    return-void
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 216761
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, LX/1Ck;->a:LX/0Xq;

    invoke-virtual {v0}, LX/0Xq;->n()Z

    move-result v0

    if-nez v0, :cond_0

    .line 216762
    iget-object v0, p0, LX/1Ck;->a:LX/0Xq;

    invoke-virtual {v0}, LX/0Xq;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 216763
    invoke-virtual {p0, v0}, LX/1Ck;->c(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 216764
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 216765
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 216749
    invoke-virtual {p0, p1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 216750
    :goto_0
    return-void

    .line 216751
    :cond_0
    monitor-enter p0

    .line 216752
    :try_start_0
    iget-object v0, p0, LX/1Ck;->a:LX/0Xq;

    invoke-virtual {v0, p1}, LX/0Xr;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v3

    .line 216753
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216754
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Mv;

    .line 216755
    invoke-virtual {v0, v2}, LX/1Mv;->a(Z)V

    .line 216756
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 216757
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 216758
    :cond_1
    monitor-enter p0

    .line 216759
    :try_start_2
    iget-object v0, p0, LX/1Ck;->a:LX/0Xq;

    invoke-virtual {v0, p1}, LX/0Xr;->b(Ljava/lang/Object;)Ljava/util/List;

    .line 216760
    monitor-exit p0

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public final c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TKey;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;",
            "LX/0Ve",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 216738
    new-instance v1, LX/4mx;

    invoke-direct {v1, p0, p1, p3}, LX/4mx;-><init>(LX/1Ck;Ljava/lang/Object;LX/0Ve;)V

    .line 216739
    invoke-direct {p0, p1}, LX/1Ck;->e(Ljava/lang/Object;)LX/0Ve;

    move-result-object v0

    .line 216740
    if-eqz v0, :cond_0

    .line 216741
    instance-of v2, v0, LX/4mx;

    if-nez v2, :cond_0

    .line 216742
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ordered and unordered task can\'t be added under same key : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 216743
    :cond_0
    check-cast v0, LX/4mx;

    .line 216744
    if-eqz v0, :cond_1

    .line 216745
    iput-object v0, v1, LX/4mx;->e:LX/4mx;

    .line 216746
    iput-object v1, v0, LX/4mx;->f:LX/4mx;

    .line 216747
    :cond_1
    invoke-direct {p0, p1, p2, v1}, LX/1Ck;->d(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 216748
    return-void
.end method
