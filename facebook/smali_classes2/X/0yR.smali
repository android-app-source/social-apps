.class public LX/0yR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0yR;


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/zero/token/response/ZeroTokenHeaderResponseListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 164762
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164763
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0yR;->a:Ljava/util/List;

    .line 164764
    return-void
.end method

.method public static a(LX/0QB;)LX/0yR;
    .locals 3

    .prologue
    .line 164769
    sget-object v0, LX/0yR;->b:LX/0yR;

    if-nez v0, :cond_1

    .line 164770
    const-class v1, LX/0yR;

    monitor-enter v1

    .line 164771
    :try_start_0
    sget-object v0, LX/0yR;->b:LX/0yR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 164772
    if-eqz v2, :cond_0

    .line 164773
    :try_start_1
    new-instance v0, LX/0yR;

    invoke-direct {v0}, LX/0yR;-><init>()V

    .line 164774
    move-object v0, v0

    .line 164775
    sput-object v0, LX/0yR;->b:LX/0yR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164776
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 164777
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 164778
    :cond_1
    sget-object v0, LX/0yR;->b:LX/0yR;

    return-object v0

    .line 164779
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 164780
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 164765
    iget-object v0, p0, LX/0yR;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yT;

    .line 164766
    iget-object p0, v0, LX/0yT;->a:LX/0yI;

    invoke-virtual {p0}, LX/0yI;->p()V

    .line 164767
    goto :goto_0

    .line 164768
    :cond_0
    return-void
.end method
