.class public abstract LX/0WB;
.super LX/0WC;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/0QR;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0QR;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0QR;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 75615
    invoke-direct {p0}, LX/0WC;-><init>()V

    .line 75616
    new-instance v0, LX/0We;

    invoke-direct {v0, p0}, LX/0We;-><init>(LX/0WB;)V

    invoke-static {v0}, LX/0Wf;->memoize(LX/0QR;)LX/0QR;

    move-result-object v0

    iput-object v0, p0, LX/0WB;->a:LX/0QR;

    .line 75617
    new-instance v0, LX/0Wh;

    invoke-direct {v0, p0}, LX/0Wh;-><init>(LX/0WB;)V

    invoke-static {v0}, LX/0Wf;->memoize(LX/0QR;)LX/0QR;

    move-result-object v0

    iput-object v0, p0, LX/0WB;->b:LX/0QR;

    .line 75618
    new-instance v0, LX/0Wi;

    invoke-direct {v0, p0}, LX/0Wi;-><init>(LX/0WB;)V

    invoke-static {v0}, LX/0Wf;->memoize(LX/0QR;)LX/0QR;

    move-result-object v0

    iput-object v0, p0, LX/0WB;->c:LX/0QR;

    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75614
    iget-object v0, p0, LX/0WB;->a:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rf;

    return-object v0
.end method

.method public final a(LX/0ea;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ea;",
            ")",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75610
    sget-object v0, LX/0eb;->a:[I

    invoke-virtual {p1}, LX/0ea;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 75611
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unhandled language set type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75612
    :pswitch_0
    iget-object v0, p0, LX/0WB;->b:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rf;

    .line 75613
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/0WB;->c:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rf;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public abstract b()LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract c()LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract d()LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method
