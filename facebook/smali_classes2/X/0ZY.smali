.class public LX/0ZY;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0Zb;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0Zb;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83430
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0Zb;
    .locals 3

    .prologue
    .line 83431
    sget-object v0, LX/0ZY;->a:LX/0Zb;

    if-nez v0, :cond_1

    .line 83432
    const-class v1, LX/0ZY;

    monitor-enter v1

    .line 83433
    :try_start_0
    sget-object v0, LX/0ZY;->a:LX/0Zb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 83434
    if-eqz v2, :cond_0

    .line 83435
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 83436
    const/16 p0, 0x9c

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {p0}, Lcom/facebook/analytics/AnalyticsClientModule;->a(LX/0Ot;)LX/0Zb;

    move-result-object p0

    move-object v0, p0

    .line 83437
    sput-object v0, LX/0ZY;->a:LX/0Zb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83438
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 83439
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 83440
    :cond_1
    sget-object v0, LX/0ZY;->a:LX/0Zb;

    return-object v0

    .line 83441
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 83442
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 83443
    const/16 v0, 0x9c

    invoke-static {p0, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/analytics/AnalyticsClientModule;->a(LX/0Ot;)LX/0Zb;

    move-result-object v0

    return-object v0
.end method
