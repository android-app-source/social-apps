.class public LX/1q4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1q2;


# static fields
.field public static a:J
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static b:J
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field public c:J
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final d:LX/0ad;

.field private e:Z

.field private f:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 329986
    const-wide/16 v0, 0xbb8

    sput-wide v0, LX/1q4;->a:J

    .line 329987
    const-wide/16 v0, 0x0

    sput-wide v0, LX/1q4;->b:J

    return-void
.end method

.method public constructor <init>(LX/0ad;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 329980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 329981
    iput-object p1, p0, LX/1q4;->d:LX/0ad;

    .line 329982
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1q4;->e:Z

    .line 329983
    iput-wide v2, p0, LX/1q4;->f:J

    .line 329984
    iput-wide v2, p0, LX/1q4;->c:J

    .line 329985
    return-void
.end method


# virtual methods
.method public final a(JZ)V
    .locals 7

    .prologue
    .line 329988
    iput-wide p1, p0, LX/1q4;->f:J

    .line 329989
    iget-object v0, p0, LX/1q4;->d:LX/0ad;

    sget-wide v2, LX/1Nu;->f:J

    sget-wide v4, LX/1q4;->a:J

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    .line 329990
    if-eqz p3, :cond_0

    :goto_0
    iput-wide v0, p0, LX/1q4;->c:J

    .line 329991
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1q4;->e:Z

    .line 329992
    return-void

    .line 329993
    :cond_0
    sget-wide v0, LX/1q4;->b:J

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 329979
    iget-boolean v0, p0, LX/1q4;->e:Z

    return v0
.end method

.method public final a(J)Z
    .locals 5

    .prologue
    .line 329975
    iget-wide v0, p0, LX/1q4;->f:J

    sub-long v0, p1, v0

    .line 329976
    iget-wide v2, p0, LX/1q4;->c:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 329977
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1q4;->e:Z

    .line 329978
    return-void
.end method
