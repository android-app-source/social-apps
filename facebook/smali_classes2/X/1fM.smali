.class public LX/1fM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/String;

.field private static volatile q:LX/1fM;


# instance fields
.field public a:I

.field public final c:LX/0pn;

.field private final d:LX/1fN;

.field private final e:LX/0qW;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6AK;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/00H;

.field private final h:LX/1fP;

.field public final i:LX/0y5;

.field public final j:LX/1fQ;

.field private final k:LX/0Uh;

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1HI;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0ad;

.field private final n:Ljava/lang/Object;

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/16u;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0qR;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 291462
    const-class v0, LX/1fM;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1fM;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0pn;LX/1fN;LX/0qW;LX/0Or;LX/00H;LX/1fP;LX/0y5;LX/1fQ;LX/0Uh;LX/0Ot;LX/0ad;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0pn;",
            "LX/1fN;",
            "LX/0qW;",
            "LX/0Or",
            "<",
            "LX/6AK;",
            ">;",
            "LX/00H;",
            "LX/1fP;",
            "LX/0y5;",
            "LX/1fQ;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/1HI;",
            ">;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/16u;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0qR;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 291463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291464
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/1fM;->n:Ljava/lang/Object;

    .line 291465
    iput-object p1, p0, LX/1fM;->c:LX/0pn;

    .line 291466
    iput-object p2, p0, LX/1fM;->d:LX/1fN;

    .line 291467
    iput-object p3, p0, LX/1fM;->e:LX/0qW;

    .line 291468
    iput-object p4, p0, LX/1fM;->f:LX/0Or;

    .line 291469
    iput-object p5, p0, LX/1fM;->g:LX/00H;

    .line 291470
    iput-object p6, p0, LX/1fM;->h:LX/1fP;

    .line 291471
    iput-object p7, p0, LX/1fM;->i:LX/0y5;

    .line 291472
    iput-object p8, p0, LX/1fM;->j:LX/1fQ;

    .line 291473
    iput-object p9, p0, LX/1fM;->k:LX/0Uh;

    .line 291474
    iput-object p10, p0, LX/1fM;->l:LX/0Ot;

    .line 291475
    iput-object p11, p0, LX/1fM;->m:LX/0ad;

    .line 291476
    iput-object p12, p0, LX/1fM;->o:LX/0Ot;

    .line 291477
    iput-object p13, p0, LX/1fM;->p:LX/0Ot;

    .line 291478
    const/4 v0, 0x0

    iput v0, p0, LX/1fM;->a:I

    .line 291479
    return-void
.end method

.method public static a(LX/0QB;)LX/1fM;
    .locals 3

    .prologue
    .line 291480
    sget-object v0, LX/1fM;->q:LX/1fM;

    if-nez v0, :cond_1

    .line 291481
    const-class v1, LX/1fM;

    monitor-enter v1

    .line 291482
    :try_start_0
    sget-object v0, LX/1fM;->q:LX/1fM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 291483
    if-eqz v2, :cond_0

    .line 291484
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1fM;->b(LX/0QB;)LX/1fM;

    move-result-object v0

    sput-object v0, LX/1fM;->q:LX/1fM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291485
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 291486
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 291487
    :cond_1
    sget-object v0, LX/1fM;->q:LX/1fM;

    return-object v0

    .line 291488
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 291489
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 291490
    iget v0, p0, LX/1fM;->a:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/1fM;->a:I

    rem-int/lit8 v0, v0, 0x64

    if-eqz v0, :cond_1

    .line 291491
    :cond_0
    :goto_0
    return-void

    .line 291492
    :cond_1
    iget-object v0, p0, LX/1fM;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6AK;

    invoke-virtual {v0}, LX/6AK;->a()I

    move-result v0

    .line 291493
    const/16 v1, 0xfa

    if-lt v0, v1, :cond_0

    .line 291494
    iget-object v0, p0, LX/1fM;->c:LX/0pn;

    .line 291495
    sget-object v1, LX/0pp;->a:LX/0U1;

    .line 291496
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 291497
    sget-object v2, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v2}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 291498
    sget-object v2, LX/0pp;->e:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v2

    .line 291499
    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v3, v1, v2}, LX/0pn;->a(LX/0pn;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 291500
    iget-object v0, p0, LX/1fM;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6AK;

    invoke-virtual {v0, v1}, LX/6AK;->b(LX/0Px;)V

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/1fM;
    .locals 14

    .prologue
    .line 291506
    new-instance v0, LX/1fM;

    invoke-static {p0}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v1

    check-cast v1, LX/0pn;

    invoke-static {p0}, LX/1fN;->a(LX/0QB;)LX/1fN;

    move-result-object v2

    check-cast v2, LX/1fN;

    invoke-static {p0}, LX/0qW;->a(LX/0QB;)LX/0qW;

    move-result-object v3

    check-cast v3, LX/0qW;

    const/16 v4, 0x175c

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const-class v5, LX/00H;

    invoke-interface {p0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/00H;

    invoke-static {p0}, LX/1fP;->b(LX/0QB;)LX/1fP;

    move-result-object v6

    check-cast v6, LX/1fP;

    invoke-static {p0}, LX/0y5;->a(LX/0QB;)LX/0y5;

    move-result-object v7

    check-cast v7, LX/0y5;

    invoke-static {p0}, LX/1fQ;->a(LX/0QB;)LX/1fQ;

    move-result-object v8

    check-cast v8, LX/1fQ;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    const/16 v10, 0xb99

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    const/16 v12, 0x695

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x696

    invoke-static {p0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v0 .. v13}, LX/1fM;-><init>(LX/0pn;LX/1fN;LX/0qW;LX/0Or;LX/00H;LX/1fP;LX/0y5;LX/1fQ;LX/0Uh;LX/0Ot;LX/0ad;LX/0Ot;LX/0Ot;)V

    .line 291507
    return-object v0
.end method

.method public static b(LX/1fM;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 291501
    sget-object v1, LX/01T;->FB4A:LX/01T;

    iget-object v2, p0, LX/1fM;->g:LX/00H;

    .line 291502
    iget-object v3, v2, LX/00H;->j:LX/01T;

    move-object v2, v3

    .line 291503
    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/1fM;->k:LX/0Uh;

    const/16 v2, 0x594

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static c(LX/1fM;)Z
    .locals 3

    .prologue
    .line 291504
    iget-object v0, p0, LX/1fM;->k:LX/0Uh;

    const/16 v1, 0x3b0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method private d()Z
    .locals 3

    .prologue
    .line 291505
    iget-object v0, p0, LX/1fM;->m:LX/0ad;

    sget-short v1, LX/0fe;->T:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/1t9;)LX/0Px;
    .locals 9
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1t9;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 291425
    const-string v0, "FeedDbMutationService.processFeedDbInsertionRequest"

    const v1, 0x5c25d48d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 291426
    :try_start_0
    iget-object v2, p0, LX/1fM;->n:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 291427
    :try_start_1
    iget-object v0, p0, LX/1fM;->d:LX/1fN;

    iget-object v1, p1, LX/1t9;->a:Lcom/facebook/api/feed/FetchFeedResult;

    invoke-virtual {v0, v1}, LX/1fN;->a(Lcom/facebook/api/feed/FetchFeedResult;)LX/0Px;

    move-result-object v1

    .line 291428
    invoke-static {p0}, LX/1fM;->b(LX/1fM;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291429
    iget-object v0, p0, LX/1fM;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6AK;

    iget-object v3, p1, LX/1t9;->a:Lcom/facebook/api/feed/FetchFeedResult;

    invoke-virtual {v0, v3}, LX/6AK;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 291430
    invoke-direct {p0}, LX/1fM;->a()V

    .line 291431
    :cond_0
    invoke-static {p0}, LX/1fM;->c(LX/1fM;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 291432
    iget-object v0, p0, LX/1fM;->j:LX/1fQ;

    iget-object v3, p1, LX/1t9;->a:Lcom/facebook/api/feed/FetchFeedResult;

    .line 291433
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 291434
    invoke-virtual {v3}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result p0

    const/4 v4, 0x0

    move v6, v4

    :goto_0
    if-ge v6, p0, :cond_2

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 291435
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v5

    instance-of v5, v5, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v5, :cond_1

    .line 291436
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v5}, LX/14w;->p(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    .line 291437
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 291438
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v7, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291439
    :cond_1
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_0

    .line 291440
    :cond_2
    move-object v4, v7

    .line 291441
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 291442
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 291443
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 291444
    new-instance v7, LX/69g;

    invoke-direct {v7, v0, v4}, LX/69g;-><init>(LX/1fQ;Ljava/lang/String;)V

    move-object v4, v7

    .line 291445
    new-instance v7, LX/4Gk;

    invoke-direct {v7}, LX/4Gk;-><init>()V

    invoke-virtual {v7, v5}, LX/4Gk;->a(Ljava/lang/String;)LX/4Gk;

    move-result-object v7

    .line 291446
    new-instance v8, LX/69h;

    invoke-direct {v8}, LX/69h;-><init>()V

    move-object v8, v8

    .line 291447
    const-string p0, "input"

    invoke-virtual {v8, p0, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 291448
    move-object v8, v8

    .line 291449
    const/4 v7, 0x0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291450
    :try_start_2
    iget-object p0, v0, LX/1fQ;->a:LX/0gX;

    invoke-virtual {p0, v8, v4}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;
    :try_end_2
    .catch LX/31B; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    move-result-object v7

    .line 291451
    :goto_2
    move-object v4, v7

    .line 291452
    iget-object v7, v0, LX/1fQ;->d:Ljava/util/Map;

    invoke-interface {v7, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 291453
    :cond_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 291454
    const v0, -0x70f3f5e3

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v1

    :goto_3
    return-object v0

    .line 291455
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 291456
    :catch_0
    move-exception v0

    .line 291457
    :try_start_6
    sget-object v1, LX/1fM;->b:Ljava/lang/String;

    const-string v2, "Error performing insertion on feed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 291458
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 291459
    const v1, 0x7904d610

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_3

    :catchall_1
    move-exception v0

    const v1, -0x74fa5bdd

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 291460
    :catch_1
    move-exception v8

    .line 291461
    const-string p0, "LiveVideoStatusHandler"

    const-string p1, "Live video broadcast status update subscription failed. %s"

    invoke-static {p0, p1, v8}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public final a(LX/1fc;)V
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 291309
    iget-object v0, p1, LX/1fc;->c:Ljava/lang/String;

    const-string v3, "PHOTO"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 291310
    iget-object v0, p0, LX/1fM;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HI;

    iget-object v3, p1, LX/1fc;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/1HI;->c(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    move v3, v0

    .line 291311
    :goto_1
    iget-object v0, p0, LX/1fM;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qR;

    iget-object v4, p1, LX/1fc;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/0qR;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 291312
    if-nez v0, :cond_2

    .line 291313
    sget-object v0, LX/1fM;->b:Ljava/lang/String;

    const-string v3, "DedupKey not found for cache ID: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p1, LX/1fc;->a:Ljava/lang/String;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 291314
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 291315
    goto :goto_0

    .line 291316
    :cond_2
    iget-object v1, p0, LX/1fM;->i:LX/0y5;

    iget-object v2, p1, LX/1fc;->b:Ljava/lang/String;

    iget-object v4, p1, LX/1fc;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v4, v3}, LX/0y5;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 291317
    iget-object v0, p1, LX/1fc;->c:Ljava/lang/String;

    const-string v1, "PHOTO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 291318
    iget-object v0, p0, LX/1fM;->c:LX/0pn;

    iget-object v1, p1, LX/1fc;->a:Ljava/lang/String;

    iget-object v2, p1, LX/1fc;->b:Ljava/lang/String;

    .line 291319
    :try_start_0
    invoke-static {v0, v1}, LX/0pn;->c(LX/0pn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 291320
    if-nez v4, :cond_5

    .line 291321
    sget-object v4, LX/0pn;->a:Ljava/lang/Class;

    const-string v5, "No dedup key found for cache ID: "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-static {v4, v5, v6}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 291322
    :cond_3
    :goto_3
    iget-object v0, p1, LX/1fc;->c:Ljava/lang/String;

    const-string v1, "VIDEO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291323
    iget-object v0, p0, LX/1fM;->c:LX/0pn;

    iget-object v1, p1, LX/1fc;->a:Ljava/lang/String;

    iget-object v2, p1, LX/1fc;->b:Ljava/lang/String;

    .line 291324
    :try_start_1
    invoke-static {v0, v1}, LX/0pn;->c(LX/0pn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 291325
    if-nez v3, :cond_6

    .line 291326
    sget-object v3, LX/0pn;->a:Ljava/lang/Class;

    const-string v4, "No dedup key found for cache ID: "

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-static {v3, v4, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 291327
    :goto_4
    goto :goto_2

    :cond_4
    move v3, v2

    goto :goto_1

    .line 291328
    :cond_5
    :try_start_2
    const-string v5, "PHOTO"

    invoke-static {v0, v4, v2, v5, v3}, LX/0pn;->a(LX/0pn;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 291329
    :catch_0
    goto :goto_3

    .line 291330
    :catch_1
    move-exception v4

    .line 291331
    sget-object v5, LX/0pn;->a:Ljava/lang/Class;

    const-string v6, "exception"

    invoke-static {v5, v6, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 291332
    :cond_6
    :try_start_3
    const-string v4, "VIDEO"

    const/4 v5, 0x0

    invoke-static {v0, v3, v2, v4, v5}, LX/0pn;->a(LX/0pn;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_4

    .line 291333
    :catch_2
    move-exception v3

    .line 291334
    sget-object v4, LX/0pn;->a:Ljava/lang/Class;

    const-string v5, "exception"

    invoke-static {v4, v5, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4
.end method

.method public final a(LX/30t;)V
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 291411
    iget-object v0, p0, LX/1fM;->c:LX/0pn;

    iget-object v1, p1, LX/30t;->a:Ljava/lang/String;

    .line 291412
    if-nez v1, :cond_1

    .line 291413
    :cond_0
    :goto_0
    return-void

    .line 291414
    :cond_1
    invoke-static {v0, v1}, LX/0pn;->e(LX/0pn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 291415
    if-eqz v2, :cond_0

    .line 291416
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 291417
    sget-object v4, LX/0pp;->k:LX/0U1;

    .line 291418
    iget-object p0, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, p0

    .line 291419
    const/4 p0, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v3, v4, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 291420
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 291421
    sget-object p0, LX/0pp;->k:LX/0U1;

    const-string p1, "1"

    invoke-virtual {p0, p1}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 291422
    sget-object p0, LX/0pp;->v:LX/0U1;

    invoke-virtual {p0, v2}, LX/0U1;->c(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 291423
    iget-object v2, v0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0v1;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 291424
    const-string p0, "home_stories"

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, p0, v3, p1, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(LX/69X;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 291402
    :try_start_0
    iget-object v0, p0, LX/1fM;->e:LX/0qW;

    invoke-virtual {v0}, LX/0qW;->a()I

    .line 291403
    iget-object v0, p0, LX/1fM;->c:LX/0pn;

    iget-object v1, p1, LX/69X;->b:Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {v0, v1}, LX/0pn;->a(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v0

    .line 291404
    iget-object v1, p1, LX/69X;->a:LX/AjM;

    .line 291405
    iget-object v2, v1, LX/AjM;->a:Lcom/facebook/feed/data/FeedDataLoaderReranker$2;

    iget-object v2, v2, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;->c:LX/0pl;

    iget-object p0, v1, LX/AjM;->a:Lcom/facebook/feed/data/FeedDataLoaderReranker$2;

    iget-object p0, p0, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;->a:LX/0rl;

    invoke-static {v2, v0, p0}, LX/0pl;->a$redex0(LX/0pl;Lcom/facebook/api/feed/FetchFeedResult;LX/0rl;)V
    :try_end_0
    .catch LX/69I; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 291406
    :goto_0
    return-void

    .line 291407
    :catch_0
    move-exception v0

    .line 291408
    sget-object v1, LX/1fM;->b:Ljava/lang/String;

    const-string v2, "fetchHomeStoriesFromDb failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 291409
    :catch_1
    move-exception v0

    .line 291410
    sget-object v1, LX/1fM;->b:Ljava/lang/String;

    const-string v2, "Error performing reranking on cache feed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(LX/69Y;)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 291393
    iget-object v0, p0, LX/1fM;->d:LX/1fN;

    iget-object v1, p1, LX/69Y;->a:LX/0Px;

    .line 291394
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 291395
    iget-object v2, v0, LX/1fN;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0pn;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/0pn;->a(Ljava/lang/String;)V

    .line 291396
    :goto_0
    invoke-virtual {v0}, LX/1fN;->a()V

    .line 291397
    invoke-static {p0}, LX/1fM;->b(LX/1fM;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291398
    iget-object v0, p0, LX/1fM;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6AK;

    iget-object v1, p1, LX/69Y;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/6AK;->a(LX/0Px;)V

    .line 291399
    invoke-direct {p0}, LX/1fM;->a()V

    .line 291400
    :cond_0
    return-void

    .line 291401
    :cond_1
    iget-object v2, v0, LX/1fN;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0pn;

    invoke-virtual {v2, v1}, LX/0pn;->a(LX/0Px;)I

    goto :goto_0
.end method

.method public final a(LX/69Z;)V
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 291367
    iget-object v0, p1, LX/69Z;->c:Ljava/lang/String;

    const-string v3, "ATTACHMENT_PHOTO"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 291368
    iget-object v0, p0, LX/1fM;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HI;

    iget-object v3, p1, LX/69Z;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/1HI;->c(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    move v3, v0

    .line 291369
    :goto_1
    iget-object v0, p0, LX/1fM;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16u;

    iget-object v4, p1, LX/69Z;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/16u;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 291370
    if-nez v0, :cond_2

    .line 291371
    sget-object v0, LX/1fM;->b:Ljava/lang/String;

    const-string v3, "DedupKey not found for article ID: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p1, LX/69Z;->a:Ljava/lang/String;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 291372
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 291373
    goto :goto_0

    .line 291374
    :cond_2
    iget-object v1, p0, LX/1fM;->i:LX/0y5;

    iget-object v2, p1, LX/69Z;->b:Ljava/lang/String;

    iget-object v4, p1, LX/69Z;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v4, v3}, LX/0y5;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 291375
    iget-object v1, p1, LX/69Z;->c:Ljava/lang/String;

    const-string v2, "ATTACHMENT_PHOTO"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 291376
    iget-object v1, p0, LX/1fM;->c:LX/0pn;

    iget-object v2, p1, LX/69Z;->b:Ljava/lang/String;

    .line 291377
    :try_start_0
    const-string v4, "ATTACHMENT_PHOTO"

    invoke-static {v1, v0, v2, v4, v3}, LX/0pn;->a(LX/0pn;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 291378
    :goto_3
    goto :goto_2

    .line 291379
    :cond_3
    iget-object v1, p1, LX/69Z;->c:Ljava/lang/String;

    const-string v2, "ATTACHMENT_VIDEO"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 291380
    iget-object v1, p0, LX/1fM;->c:LX/0pn;

    iget-object v2, p1, LX/69Z;->b:Ljava/lang/String;

    .line 291381
    :try_start_1
    const-string v3, "ATTACHMENT_VIDEO"

    const/4 v4, 0x0

    invoke-static {v1, v0, v2, v3, v4}, LX/0pn;->a(LX/0pn;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 291382
    :goto_4
    goto :goto_2

    .line 291383
    :cond_4
    iget-object v1, p1, LX/69Z;->c:Ljava/lang/String;

    const-string v2, "ATTACHMENT_TEXT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 291384
    iget-object v1, p0, LX/1fM;->c:LX/0pn;

    iget-object v2, p1, LX/69Z;->a:Ljava/lang/String;

    .line 291385
    :try_start_2
    const-string v3, "ATTACHMENT_TEXT"

    const/4 v4, 0x0

    invoke-static {v1, v0, v2, v3, v4}, LX/0pn;->a(LX/0pn;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 291386
    :goto_5
    goto :goto_2

    :cond_5
    move v3, v2

    goto :goto_1

    .line 291387
    :catch_0
    move-exception v4

    .line 291388
    sget-object p0, LX/0pn;->a:Ljava/lang/Class;

    const-string p1, "exception"

    invoke-static {p0, p1, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 291389
    :catch_1
    move-exception v3

    .line 291390
    sget-object v4, LX/0pn;->a:Ljava/lang/Class;

    const-string p0, "exception"

    invoke-static {v4, p0, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 291391
    :catch_2
    move-exception v3

    .line 291392
    sget-object v4, LX/0pn;->a:Ljava/lang/Class;

    const-string p0, "exception"

    invoke-static {v4, p0, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5
.end method

.method public final a(LX/69b;)V
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 291356
    iget-object v0, p0, LX/1fM;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qR;

    iget-object v1, p1, LX/69b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0qR;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 291357
    if-nez v0, :cond_0

    .line 291358
    sget-object v0, LX/1fM;->b:Ljava/lang/String;

    const-string v1, "DedupKey not found for cache Id: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, LX/69b;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 291359
    :goto_0
    return-void

    .line 291360
    :cond_0
    invoke-direct {p0}, LX/1fM;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 291361
    iget-object v1, p0, LX/1fM;->i:LX/0y5;

    iget-object v2, p1, LX/69b;->b:Ljava/lang/String;

    iget-object v3, p1, LX/69b;->c:Ljava/lang/String;

    iget v4, p1, LX/69b;->d:I

    invoke-virtual {v1, v0, v2, v3, v4}, LX/0y5;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 291362
    :cond_1
    iget-object v1, p0, LX/1fM;->c:LX/0pn;

    iget-object v2, p1, LX/69b;->b:Ljava/lang/String;

    .line 291363
    :try_start_0
    const-string v3, "ATTACHMENT_LINK"

    const/4 v4, 0x0

    invoke-static {v1, v0, v2, v3, v4}, LX/0pn;->a(LX/0pn;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 291364
    :goto_1
    goto :goto_0

    .line 291365
    :catch_0
    move-exception v3

    .line 291366
    sget-object v4, LX/0pn;->a:Ljava/lang/Class;

    const-string p0, "exception"

    invoke-static {v4, p0, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final a(LX/69c;)V
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 291349
    invoke-direct {p0}, LX/1fM;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291350
    iget-object v0, p0, LX/1fM;->i:LX/0y5;

    iget-object v1, p1, LX/69c;->b:Ljava/lang/String;

    iget-object v2, p1, LX/69c;->a:Ljava/lang/String;

    iget v3, p1, LX/69c;->c:I

    invoke-virtual {v0, v1, v2, v3}, LX/0y5;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 291351
    :cond_0
    iget-object v0, p0, LX/1fM;->c:LX/0pn;

    iget-object v1, p1, LX/69c;->a:Ljava/lang/String;

    iget-object v2, p1, LX/69c;->b:Ljava/lang/String;

    iget v3, p1, LX/69c;->c:I

    .line 291352
    :try_start_0
    iget-object v4, v0, LX/0pn;->t:LX/0qP;

    invoke-virtual {v4, v1, v2, v3}, LX/0qP;->a(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 291353
    :goto_0
    return-void

    .line 291354
    :catch_0
    move-exception v4

    .line 291355
    sget-object p0, LX/0pn;->a:Ljava/lang/Class;

    const-string p1, "exception"

    invoke-static {p0, p1, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(LX/69e;)V
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 291335
    iget-object v1, p1, LX/69e;->a:Lcom/facebook/graphql/model/FeedUnit;

    .line 291336
    invoke-static {v1}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v2

    .line 291337
    iget-object v1, v2, LX/0x2;->i:Ljava/lang/String;

    move-object v2, v1

    .line 291338
    move-object v1, v2

    .line 291339
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 291340
    iget-object v2, p0, LX/1fM;->c:LX/0pn;

    iget v3, p1, LX/69e;->b:I

    iget v4, p1, LX/69e;->c:I

    .line 291341
    sget-object v5, LX/0pp;->d:LX/0U1;

    invoke-static {v2, v5, v1, v3, v4}, LX/0pn;->a(LX/0pn;LX/0U1;Ljava/lang/String;II)I

    move-result v5

    move v1, v5

    .line 291342
    if-lez v1, :cond_0

    const/4 v0, 0x1

    .line 291343
    :cond_0
    if-nez v0, :cond_1

    .line 291344
    iget-object v0, p0, LX/1fM;->c:LX/0pn;

    iget-object v1, p1, LX/69e;->a:Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    iget v2, p1, LX/69e;->b:I

    iget v3, p1, LX/69e;->c:I

    .line 291345
    sget-object v4, LX/0pp;->x:LX/0U1;

    invoke-static {v0, v4, v1, v2, v3}, LX/0pn;->a(LX/0pn;LX/0U1;Ljava/lang/String;II)I

    .line 291346
    :cond_1
    iget-object v0, p1, LX/69e;->a:Lcom/facebook/graphql/model/FeedUnit;

    iget v1, p1, LX/69e;->b:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0x1;->b(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V

    .line 291347
    iget-object v0, p0, LX/1fM;->i:LX/0y5;

    iget-object v1, p1, LX/69e;->a:Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    iget v2, p1, LX/69e;->b:I

    invoke-virtual {v0, v1, v2}, LX/0y5;->a(Ljava/lang/String;I)V

    .line 291348
    return-void
.end method
