.class public abstract LX/15w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0lE;
.implements Ljava/io/Closeable;


# instance fields
.field public a:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 182881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 182874
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182875
    iput p1, p0, LX/15w;->a:I

    .line 182876
    return-void
.end method


# virtual methods
.method public abstract A()F
.end method

.method public abstract B()D
.end method

.method public abstract C()Ljava/math/BigDecimal;
.end method

.method public abstract D()Ljava/lang/Object;
.end method

.method public E()I
    .locals 1

    .prologue
    .line 182877
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/15w;->b(I)I

    move-result v0

    return v0
.end method

.method public F()J
    .locals 2

    .prologue
    .line 182878
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, LX/15w;->a(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public G()D
    .locals 2

    .prologue
    .line 182879
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, LX/15w;->a(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public H()Z
    .locals 1

    .prologue
    .line 182880
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/15w;->a(Z)Z

    move-result v0

    return v0
.end method

.method public I()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182882
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/15w;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public J()LX/0lG;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0lG;",
            ">()TT;"
        }
    .end annotation

    .prologue
    .line 182883
    invoke-virtual {p0}, LX/15w;->a()LX/0lD;

    move-result-object v0

    .line 182884
    if-nez v0, :cond_0

    .line 182885
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No ObjectCodec defined for the parser, can not deserialize JSON into JsonNode tree"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 182886
    :cond_0
    invoke-virtual {v0, p0}, LX/0lD;->a(LX/15w;)LX/0lG;

    move-result-object v0

    return-object v0
.end method

.method public a(D)D
    .locals 1

    .prologue
    .line 182887
    return-wide p1
.end method

.method public a(I)I
    .locals 2

    .prologue
    .line 182889
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, LX/15w;->x()I

    move-result p1

    :cond_0
    return p1
.end method

.method public a(Ljava/io/OutputStream;)I
    .locals 1

    .prologue
    .line 182888
    const/4 v0, -0x1

    return v0
.end method

.method public a(Ljava/io/Writer;)I
    .locals 1

    .prologue
    .line 182902
    const/4 v0, -0x1

    return v0
.end method

.method public a(J)J
    .locals 1

    .prologue
    .line 182901
    return-wide p1
.end method

.method public abstract a()LX/0lD;
.end method

.method public a(LX/266;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/266",
            "<*>;)TT;"
        }
    .end annotation

    .prologue
    .line 182897
    invoke-virtual {p0}, LX/15w;->a()LX/0lD;

    move-result-object v0

    .line 182898
    if-nez v0, :cond_0

    .line 182899
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No ObjectCodec defined for the parser, can not deserialize JSON into Java objects"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 182900
    :cond_0
    invoke-virtual {v0, p0, p1}, LX/0lD;->a(LX/15w;LX/266;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 182893
    invoke-virtual {p0}, LX/15w;->a()LX/0lD;

    move-result-object v0

    .line 182894
    if-nez v0, :cond_0

    .line 182895
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No ObjectCodec defined for the parser, can not deserialize JSON into Java objects"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 182896
    :cond_0
    invoke-virtual {v0, p0, p1}, LX/0lD;->a(LX/15w;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract a(LX/0lD;)V
.end method

.method public a(LX/0lr;)Z
    .locals 2

    .prologue
    .line 182892
    iget v0, p0, LX/15w;->a:I

    invoke-virtual {p1}, LX/0lr;->getMask()I

    move-result v1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Z)Z
    .locals 0

    .prologue
    .line 182891
    return p1
.end method

.method public abstract a(LX/0ln;)[B
.end method

.method public b(I)I
    .locals 0

    .prologue
    .line 182890
    return p1
.end method

.method public final b(Ljava/lang/String;)LX/2aQ;
    .locals 2

    .prologue
    .line 182872
    new-instance v0, LX/2aQ;

    invoke-virtual {p0}, LX/15w;->l()LX/28G;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/2aQ;-><init>(Ljava/lang/String;LX/28G;)V

    return-object v0
.end method

.method public b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 182873
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Ljava/lang/Class;)Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 182860
    invoke-virtual {p0}, LX/15w;->a()LX/0lD;

    move-result-object v0

    .line 182861
    if-nez v0, :cond_0

    .line 182862
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No ObjectCodec defined for the parser, can not deserialize JSON into Java objects"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 182863
    :cond_0
    invoke-virtual {v0, p0, p1}, LX/0lD;->b(LX/15w;Ljava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public abstract c()LX/15z;
.end method

.method public abstract close()V
.end method

.method public abstract d()LX/15z;
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 182859
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract f()LX/15w;
.end method

.method public abstract g()LX/15z;
.end method

.method public abstract h()Z
.end method

.method public abstract i()Ljava/lang/String;
.end method

.method public abstract j()LX/12V;
.end method

.method public abstract k()LX/28G;
.end method

.method public abstract l()LX/28G;
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 182858
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract n()V
.end method

.method public abstract o()Ljava/lang/String;
.end method

.method public abstract p()[C
.end method

.method public abstract q()I
.end method

.method public abstract r()I
.end method

.method public abstract s()Z
.end method

.method public abstract t()Ljava/lang/Number;
.end method

.method public abstract u()LX/16L;
.end method

.method public v()B
    .locals 2

    .prologue
    .line 182864
    invoke-virtual {p0}, LX/15w;->x()I

    move-result v0

    .line 182865
    const/16 v1, -0x80

    if-lt v0, v1, :cond_0

    const/16 v1, 0xff

    if-le v0, v1, :cond_1

    .line 182866
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Numeric value ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") out of range of Java byte"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15w;->b(Ljava/lang/String;)LX/2aQ;

    move-result-object v0

    throw v0

    .line 182867
    :cond_1
    int-to-byte v0, v0

    return v0
.end method

.method public abstract version()LX/0ne;
.end method

.method public w()S
    .locals 2

    .prologue
    .line 182868
    invoke-virtual {p0}, LX/15w;->x()I

    move-result v0

    .line 182869
    const/16 v1, -0x8000

    if-lt v0, v1, :cond_0

    const/16 v1, 0x7fff

    if-le v0, v1, :cond_1

    .line 182870
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Numeric value ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") out of range of Java short"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15w;->b(Ljava/lang/String;)LX/2aQ;

    move-result-object v0

    throw v0

    .line 182871
    :cond_1
    int-to-short v0, v0

    return v0
.end method

.method public abstract x()I
.end method

.method public abstract y()J
.end method

.method public abstract z()Ljava/math/BigInteger;
.end method
