.class public LX/1QN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/view/WindowManager;

.field private c:LX/0gw;

.field private final d:Landroid/graphics/Point;

.field private e:LX/0gy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/WindowManager;LX/0gw;LX/0gy;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 244526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244527
    iput-object p1, p0, LX/1QN;->a:Landroid/content/Context;

    .line 244528
    iput-object p2, p0, LX/1QN;->b:Landroid/view/WindowManager;

    .line 244529
    iput-object p3, p0, LX/1QN;->c:LX/0gw;

    .line 244530
    iput-object p4, p0, LX/1QN;->e:LX/0gy;

    .line 244531
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, LX/1QN;->d:Landroid/graphics/Point;

    .line 244532
    return-void
.end method


# virtual methods
.method public getContentWidth()I
    .locals 2
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "contentWidth"
    .end annotation

    .prologue
    .line 244520
    iget-object v0, p0, LX/1QN;->c:LX/0gw;

    invoke-virtual {v0}, LX/0gw;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 244521
    iget-object v0, p0, LX/1QN;->b:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, LX/1QN;->d:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 244522
    iget-object v0, p0, LX/1QN;->d:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    .line 244523
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1QN;->e:LX/0gy;

    invoke-virtual {v0}, LX/0gy;->c()I

    move-result v0

    goto :goto_0
.end method

.method public getIdiom()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "scale"
    .end annotation

    .prologue
    .line 244525
    iget-object v0, p0, LX/1QN;->c:LX/0gw;

    invoke-virtual {v0}, LX/0gw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "tablet"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "phone"

    goto :goto_0
.end method

.method public getScale()F
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "scale"
    .end annotation

    .prologue
    .line 244524
    iget-object v0, p0, LX/1QN;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    return v0
.end method
