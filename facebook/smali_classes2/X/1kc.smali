.class public final LX/1kc;
.super LX/0Py;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Py",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final map:LX/0P1;
    .annotation build Lcom/google/j2objc/annotations/Weak;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 310106
    invoke-direct {p0}, LX/0Py;-><init>()V

    .line 310107
    iput-object p1, p0, LX/1kc;->map:LX/0P1;

    .line 310108
    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 310109
    if-eqz p1, :cond_0

    invoke-virtual {p0}, LX/1kc;->iterator()LX/0Rc;

    move-result-object v0

    .line 310110
    invoke-static {p1}, LX/0Rj;->equalTo(Ljava/lang/Object;)LX/0Rl;

    move-result-object p0

    invoke-static {v0, p0}, LX/0RZ;->c(Ljava/util/Iterator;LX/0Rl;)Z

    move-result p0

    move v0, p0

    .line 310111
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final createAsList()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 310112
    iget-object v0, p0, LX/1kc;->map:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    .line 310113
    new-instance v1, LX/1kd;

    invoke-direct {v1, p0, v0}, LX/1kd;-><init>(LX/1kc;LX/0Px;)V

    return-object v1
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 310101
    const/4 v0, 0x1

    return v0
.end method

.method public final iterator()LX/0Rc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 310105
    new-instance v0, LX/1r2;

    invoke-direct {v0, p0}, LX/1r2;-><init>(LX/1kc;)V

    return-object v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 310104
    invoke-virtual {p0}, LX/1kc;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 310103
    iget-object v0, p0, LX/1kc;->map:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->size()I

    move-result v0

    return v0
.end method

.method public final writeReplace()Ljava/lang/Object;
    .locals 2
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "serialization"
    .end annotation

    .prologue
    .line 310102
    new-instance v0, LX/4yI;

    iget-object v1, p0, LX/1kc;->map:LX/0P1;

    invoke-direct {v0, v1}, LX/4yI;-><init>(LX/0P1;)V

    return-object v0
.end method
