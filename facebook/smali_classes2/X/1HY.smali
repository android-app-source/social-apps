.class public LX/1HY;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/1Ha;

.field private final c:LX/1Fj;

.field public final d:LX/1Fl;

.field public final e:Ljava/util/concurrent/Executor;

.field private final f:Ljava/util/concurrent/Executor;

.field public final g:LX/1IY;

.field public final h:LX/1GF;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 227194
    const-class v0, LX/1HY;

    sput-object v0, LX/1HY;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1Ha;LX/1Fj;LX/1Fl;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;LX/1GF;)V
    .locals 1

    .prologue
    .line 227184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227185
    iput-object p1, p0, LX/1HY;->b:LX/1Ha;

    .line 227186
    iput-object p2, p0, LX/1HY;->c:LX/1Fj;

    .line 227187
    iput-object p3, p0, LX/1HY;->d:LX/1Fl;

    .line 227188
    iput-object p4, p0, LX/1HY;->e:Ljava/util/concurrent/Executor;

    .line 227189
    iput-object p5, p0, LX/1HY;->f:Ljava/util/concurrent/Executor;

    .line 227190
    iput-object p6, p0, LX/1HY;->h:LX/1GF;

    .line 227191
    new-instance v0, LX/1IY;

    invoke-direct {v0}, LX/1IY;-><init>()V

    move-object v0, v0

    .line 227192
    iput-object v0, p0, LX/1HY;->g:LX/1IY;

    .line 227193
    return-void
.end method

.method public static e(LX/1HY;LX/1bh;)Z
    .locals 1

    .prologue
    .line 227173
    iget-object v0, p0, LX/1HY;->g:LX/1IY;

    invoke-virtual {v0, p1}, LX/1IY;->a(LX/1bh;)LX/1FL;

    move-result-object v0

    .line 227174
    if-eqz v0, :cond_0

    .line 227175
    invoke-virtual {v0}, LX/1FL;->close()V

    .line 227176
    invoke-interface {p1}, LX/1bh;->a()Ljava/lang/String;

    .line 227177
    iget-object v0, p0, LX/1HY;->h:LX/1GF;

    invoke-interface {v0, p1}, LX/1GF;->c(LX/1bh;)V

    .line 227178
    const/4 v0, 0x1

    .line 227179
    :goto_0
    return v0

    .line 227180
    :cond_0
    invoke-interface {p1}, LX/1bh;->a()Ljava/lang/String;

    .line 227181
    iget-object v0, p0, LX/1HY;->h:LX/1GF;

    invoke-interface {v0}, LX/1GF;->f()V

    .line 227182
    :try_start_0
    iget-object v0, p0, LX/1HY;->b:LX/1Ha;

    invoke-interface {v0, p1}, LX/1Ha;->d(LX/1bh;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 227183
    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(LX/1HY;LX/1bh;)LX/1FK;
    .locals 6

    .prologue
    .line 227155
    :try_start_0
    invoke-interface {p1}, LX/1bh;->a()Ljava/lang/String;

    .line 227156
    iget-object v0, p0, LX/1HY;->b:LX/1Ha;

    invoke-interface {v0, p1}, LX/1Ha;->a(LX/1bh;)LX/1gI;

    move-result-object v0

    .line 227157
    if-nez v0, :cond_0

    .line 227158
    invoke-interface {p1}, LX/1bh;->a()Ljava/lang/String;

    .line 227159
    iget-object v0, p0, LX/1HY;->h:LX/1GF;

    invoke-interface {v0}, LX/1GF;->h()V

    .line 227160
    const/4 v0, 0x0

    .line 227161
    :goto_0
    return-object v0

    .line 227162
    :cond_0
    invoke-interface {p1}, LX/1bh;->a()Ljava/lang/String;

    .line 227163
    iget-object v1, p0, LX/1HY;->h:LX/1GF;

    invoke-interface {v1}, LX/1GF;->g()V

    .line 227164
    invoke-interface {v0}, LX/1gI;->a()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 227165
    :try_start_1
    iget-object v2, p0, LX/1HY;->c:LX/1Fj;

    invoke-interface {v0}, LX/1gI;->c()J

    move-result-wide v4

    long-to-int v0, v4

    invoke-virtual {v2, v1, v0}, LX/1Fj;->a(Ljava/io/InputStream;I)LX/1FK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 227166
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 227167
    invoke-interface {p1}, LX/1bh;->a()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 227168
    :catch_0
    move-exception v0

    .line 227169
    sget-object v1, LX/1HY;->a:Ljava/lang/Class;

    const-string v2, "Exception reading from cache for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {p1}, LX/1bh;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/03J;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 227170
    iget-object v1, p0, LX/1HY;->h:LX/1GF;

    invoke-interface {v1}, LX/1GF;->i()V

    .line 227171
    throw v0

    .line 227172
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method


# virtual methods
.method public final a()LX/1eg;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1eg",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227195
    iget-object v0, p0, LX/1HY;->g:LX/1IY;

    invoke-virtual {v0}, LX/1IY;->b()V

    .line 227196
    :try_start_0
    new-instance v0, LX/4dp;

    invoke-direct {v0, p0}, LX/4dp;-><init>(LX/1HY;)V

    iget-object v1, p0, LX/1HY;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1}, LX/1eg;->a(Ljava/util/concurrent/Callable;Ljava/util/concurrent/Executor;)LX/1eg;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 227197
    :goto_0
    return-object v0

    .line 227198
    :catch_0
    move-exception v0

    .line 227199
    sget-object v1, LX/1HY;->a:Ljava/lang/Class;

    const-string v2, "Failed to schedule disk-cache clear"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/03J;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 227200
    invoke-static {v0}, LX/1eg;->a(Ljava/lang/Exception;)LX/1eg;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1bh;Ljava/util/concurrent/atomic/AtomicBoolean;)LX/1eg;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bh;",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            ")",
            "LX/1eg",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227143
    iget-object v0, p0, LX/1HY;->g:LX/1IY;

    invoke-virtual {v0, p1}, LX/1IY;->a(LX/1bh;)LX/1FL;

    move-result-object v0

    .line 227144
    if-eqz v0, :cond_0

    .line 227145
    invoke-interface {p1}, LX/1bh;->a()Ljava/lang/String;

    .line 227146
    iget-object p2, p0, LX/1HY;->h:LX/1GF;

    invoke-interface {p2, p1}, LX/1GF;->c(LX/1bh;)V

    .line 227147
    invoke-static {v0}, LX/1eg;->a(Ljava/lang/Object;)LX/1eg;

    move-result-object p2

    move-object v0, p2

    .line 227148
    :goto_0
    return-object v0

    .line 227149
    :cond_0
    :try_start_0
    new-instance v0, LX/1ef;

    invoke-direct {v0, p0, p2, p1}, LX/1ef;-><init>(LX/1HY;Ljava/util/concurrent/atomic/AtomicBoolean;LX/1bh;)V

    iget-object v1, p0, LX/1HY;->e:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1}, LX/1eg;->a(Ljava/util/concurrent/Callable;Ljava/util/concurrent/Executor;)LX/1eg;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 227150
    :goto_1
    move-object v0, v0

    .line 227151
    goto :goto_0

    .line 227152
    :catch_0
    move-exception v0

    .line 227153
    sget-object v1, LX/1HY;->a:Ljava/lang/Class;

    const-string v2, "Failed to schedule disk-cache read for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {p1}, LX/1bh;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/03J;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 227154
    invoke-static {v0}, LX/1eg;->a(Ljava/lang/Exception;)LX/1eg;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(LX/1bh;LX/1FL;)V
    .locals 7

    .prologue
    .line 227119
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 227120
    invoke-static {p2}, LX/1FL;->e(LX/1FL;)Z

    move-result v0

    invoke-static {v0}, LX/03g;->a(Z)V

    .line 227121
    iget-object v0, p0, LX/1HY;->g:LX/1IY;

    invoke-virtual {v0, p1, p2}, LX/1IY;->a(LX/1bh;LX/1FL;)V

    .line 227122
    iput-object p1, p2, LX/1FL;->i:LX/1bh;

    .line 227123
    invoke-static {p2}, LX/1FL;->a(LX/1FL;)LX/1FL;

    move-result-object v1

    .line 227124
    :try_start_0
    iget-object v0, p0, LX/1HY;->f:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/facebook/imagepipeline/cache/BufferedDiskCache$3;

    invoke-direct {v2, p0, p1, v1}, Lcom/facebook/imagepipeline/cache/BufferedDiskCache$3;-><init>(LX/1HY;LX/1bh;LX/1FL;)V

    const v3, -0x41dfe235

    invoke-static {v0, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 227125
    :goto_0
    return-void

    .line 227126
    :catch_0
    move-exception v0

    .line 227127
    sget-object v2, LX/1HY;->a:Ljava/lang/Class;

    const-string v3, "Failed to schedule disk-cache write for %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {p1}, LX/1bh;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/03J;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 227128
    iget-object v0, p0, LX/1HY;->g:LX/1IY;

    invoke-virtual {v0, p1, p2}, LX/1IY;->b(LX/1bh;LX/1FL;)Z

    .line 227129
    invoke-static {v1}, LX/1FL;->d(LX/1FL;)V

    goto :goto_0
.end method

.method public final a(LX/1bh;)Z
    .locals 1

    .prologue
    .line 227142
    iget-object v0, p0, LX/1HY;->g:LX/1IY;

    invoke-virtual {v0, p1}, LX/1IY;->b(LX/1bh;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1HY;->b:LX/1Ha;

    invoke-interface {v0, p1}, LX/1Ha;->c(LX/1bh;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/1bh;)LX/1eg;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bh;",
            ")",
            "LX/1eg",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227133
    invoke-virtual {p0, p1}, LX/1HY;->a(LX/1bh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227134
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/1eg;->a(Ljava/lang/Object;)LX/1eg;

    move-result-object v0

    .line 227135
    :goto_0
    return-object v0

    .line 227136
    :cond_0
    :try_start_0
    new-instance v0, LX/4do;

    invoke-direct {v0, p0, p1}, LX/4do;-><init>(LX/1HY;LX/1bh;)V

    iget-object v1, p0, LX/1HY;->e:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1}, LX/1eg;->a(Ljava/util/concurrent/Callable;Ljava/util/concurrent/Executor;)LX/1eg;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 227137
    :goto_1
    move-object v0, v0

    .line 227138
    goto :goto_0

    .line 227139
    :catch_0
    move-exception v0

    .line 227140
    sget-object v1, LX/1HY;->a:Ljava/lang/Class;

    const-string v2, "Failed to schedule disk-cache read for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {p1}, LX/1bh;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/03J;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 227141
    invoke-static {v0}, LX/1eg;->a(Ljava/lang/Exception;)LX/1eg;

    move-result-object v0

    goto :goto_1
.end method

.method public final c(LX/1bh;)Z
    .locals 1

    .prologue
    .line 227130
    invoke-virtual {p0, p1}, LX/1HY;->a(LX/1bh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227131
    const/4 v0, 0x1

    .line 227132
    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1}, LX/1HY;->e(LX/1HY;LX/1bh;)Z

    move-result v0

    goto :goto_0
.end method
