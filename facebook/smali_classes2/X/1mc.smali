.class public LX/1mc;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 313919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I
    .locals 1

    .prologue
    .line 313920
    invoke-static {p0}, LX/0x1;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Ljava/lang/Integer;

    move-result-object v0

    .line 313921
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Object;)Lcom/facebook/graphql/model/SponsoredImpression;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 313922
    if-eqz p0, :cond_0

    instance-of v0, p0, LX/16p;

    if-nez v0, :cond_1

    .line 313923
    :cond_0
    const/4 v0, 0x0

    .line 313924
    :goto_0
    return-object v0

    :cond_1
    check-cast p0, LX/16p;

    invoke-interface {p0}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V
    .locals 1

    .prologue
    .line 313925
    invoke-interface {p0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0, p1}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Ljava/util/List;I)V

    .line 313926
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Ljava/util/List;I)V
    .locals 2

    .prologue
    .line 313927
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 313928
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v1

    .line 313929
    iput-object v0, v1, LX/0x2;->z:Ljava/lang/Integer;

    .line 313930
    if-eqz p1, :cond_0

    if-ltz p2, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_2

    .line 313931
    :cond_0
    const/4 v0, 0x0

    .line 313932
    :goto_0
    move-object v0, v0

    .line 313933
    invoke-static {v0}, LX/1mc;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v1

    .line 313934
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object p1

    .line 313935
    iput-object v1, p1, LX/0x2;->A:Lcom/facebook/graphql/model/SponsoredImpression;

    .line 313936
    if-eqz v0, :cond_1

    instance-of v1, v0, LX/16h;

    if-nez v1, :cond_3

    .line 313937
    :cond_1
    const/4 v1, 0x0

    .line 313938
    :goto_1
    move-object v0, v1

    .line 313939
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v1

    .line 313940
    iput-object v0, v1, LX/0x2;->B:LX/162;

    .line 313941
    return-void

    :cond_2
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_3
    check-cast v0, LX/16h;

    invoke-static {v0, p0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v1

    goto :goto_1
.end method

.method public static b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Lcom/facebook/graphql/model/SponsoredImpression;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 313942
    invoke-static {p0}, LX/0x1;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    .line 313943
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 313944
    :cond_0
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 313945
    iget-object p0, v0, LX/0x2;->A:Lcom/facebook/graphql/model/SponsoredImpression;

    move-object v0, p0

    .line 313946
    move-object v0, v0

    .line 313947
    return-object v0
.end method

.method public static c(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 313948
    invoke-static {p0}, LX/0x1;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    .line 313949
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 313950
    :cond_0
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 313951
    iget-object p0, v0, LX/0x2;->B:LX/162;

    move-object v0, p0

    .line 313952
    move-object v0, v0

    .line 313953
    return-object v0
.end method
