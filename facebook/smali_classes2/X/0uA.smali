.class public LX/0uA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0u4;


# instance fields
.field private final a:LX/0u7;


# direct methods
.method public constructor <init>(LX/0u7;)V
    .locals 0

    .prologue
    .line 156070
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156071
    iput-object p1, p0, LX/0uA;->a:LX/0u7;

    .line 156072
    return-void
.end method


# virtual methods
.method public final a(J)LX/0uE;
    .locals 3

    .prologue
    .line 156073
    long-to-int v0, p1

    packed-switch v0, :pswitch_data_0

    .line 156074
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 156075
    :pswitch_0
    new-instance v0, LX/0uE;

    iget-object v1, p0, LX/0uA;->a:LX/0u7;

    invoke-virtual {v1}, LX/0u7;->a()F

    move-result v1

    invoke-direct {v0, v1}, LX/0uE;-><init>(F)V

    goto :goto_0

    .line 156076
    :pswitch_1
    new-instance v0, LX/0uE;

    iget-object v1, p0, LX/0uA;->a:LX/0u7;

    invoke-virtual {v1}, LX/0u7;->b()LX/0y1;

    move-result-object v1

    invoke-virtual {v1}, LX/0y1;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0uE;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 156077
    :pswitch_2
    new-instance v0, LX/0uE;

    iget-object v1, p0, LX/0uA;->a:LX/0u7;

    invoke-virtual {v1}, LX/0u7;->d()LX/454;

    move-result-object v1

    invoke-virtual {v1}, LX/454;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0uE;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/0u5;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156078
    new-instance v0, LX/0u5;

    const-string v1, "battery_level"

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, p0, v2, v3}, LX/0u5;-><init>(Ljava/lang/String;LX/0u4;J)V

    new-instance v1, LX/0u5;

    const-string v2, "battery_charge_state"

    const-wide/16 v4, 0x1

    invoke-direct {v1, v2, p0, v4, v5}, LX/0u5;-><init>(Ljava/lang/String;LX/0u4;J)V

    new-instance v2, LX/0u5;

    const-string v3, "battery_plugged_state"

    const-wide/16 v4, 0x2

    invoke-direct {v2, v3, p0, v4, v5}, LX/0u5;-><init>(Ljava/lang/String;LX/0u4;J)V

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
