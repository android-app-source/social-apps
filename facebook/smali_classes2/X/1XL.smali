.class public final LX/1XL;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1XK;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public b:Z

.field public c:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic e:LX/1XK;


# direct methods
.method public constructor <init>(LX/1XK;)V
    .locals 1

    .prologue
    .line 270975
    iput-object p1, p0, LX/1XL;->e:LX/1XK;

    .line 270976
    move-object v0, p1

    .line 270977
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 270978
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 270979
    const-string v0, "UFIFeedbackFlyoutLauncherComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 270980
    if-ne p0, p1, :cond_1

    .line 270981
    :cond_0
    :goto_0
    return v0

    .line 270982
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 270983
    goto :goto_0

    .line 270984
    :cond_3
    check-cast p1, LX/1XL;

    .line 270985
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 270986
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 270987
    if-eq v2, v3, :cond_0

    .line 270988
    iget-object v2, p0, LX/1XL;->a:LX/1X1;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/1XL;->a:LX/1X1;

    iget-object v3, p1, LX/1XL;->a:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 270989
    goto :goto_0

    .line 270990
    :cond_5
    iget-object v2, p1, LX/1XL;->a:LX/1X1;

    if-nez v2, :cond_4

    .line 270991
    :cond_6
    iget-boolean v2, p0, LX/1XL;->b:Z

    iget-boolean v3, p1, LX/1XL;->b:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 270992
    goto :goto_0

    .line 270993
    :cond_7
    iget-object v2, p0, LX/1XL;->c:LX/1Po;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/1XL;->c:LX/1Po;

    iget-object v3, p1, LX/1XL;->c:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 270994
    goto :goto_0

    .line 270995
    :cond_9
    iget-object v2, p1, LX/1XL;->c:LX/1Po;

    if-nez v2, :cond_8

    .line 270996
    :cond_a
    iget-object v2, p0, LX/1XL;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/1XL;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/1XL;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 270997
    goto :goto_0

    .line 270998
    :cond_b
    iget-object v2, p1, LX/1XL;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 270999
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/1XL;

    .line 271000
    iget-object v1, v0, LX/1XL;->a:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/1XL;->a:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/1XL;->a:LX/1X1;

    .line 271001
    return-object v0

    .line 271002
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
