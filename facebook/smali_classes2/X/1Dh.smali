.class public interface abstract LX/1Dh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Di;


# virtual methods
.method public abstract B(I)LX/1Dh;
.end method

.method public abstract C(I)LX/1Dh;
.end method

.method public abstract D(I)LX/1Dh;
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
.end method

.method public abstract E(I)LX/1Dh;
.end method

.method public abstract F(I)LX/1Dh;
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
.end method

.method public abstract G(I)LX/1Dh;
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
.end method

.method public abstract H(I)LX/1Dh;
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
.end method

.method public abstract I(I)LX/1Dh;
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
.end method

.method public abstract J(I)LX/1Dh;
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
.end method

.method public abstract K(I)LX/1Dh;
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
.end method

.method public abstract L(I)LX/1Dh;
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
.end method

.method public abstract M(I)LX/1Dh;
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
.end method

.method public abstract N(I)LX/1Dh;
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
.end method

.method public abstract O(I)LX/1Dh;
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
.end method

.method public abstract P(I)LX/1Dh;
.end method

.method public abstract Q(I)LX/1Dh;
.end method

.method public abstract R(I)LX/1Dh;
.end method

.method public abstract S(I)LX/1Dh;
.end method

.method public abstract T(I)LX/1Dh;
.end method

.method public abstract U(I)LX/1Dh;
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
.end method

.method public abstract V(I)LX/1Dh;
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
.end method

.method public abstract W(I)LX/1Dh;
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
.end method

.method public abstract X(I)LX/1Dh;
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
.end method

.method public abstract Y(I)LX/1Dh;
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
.end method

.method public abstract a(LX/1Dg;)LX/1Dh;
.end method

.method public abstract a(LX/1Di;)LX/1Dh;
.end method

.method public abstract a(LX/1X1;)LX/1Dh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;)",
            "LX/1Dh;"
        }
    .end annotation
.end method

.method public abstract a(LX/1X5;)LX/1Dh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X5",
            "<*>;)",
            "LX/1Dh;"
        }
    .end annotation
.end method

.method public abstract b(LX/1dc;)LX/1Dh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1Dh;"
        }
    .end annotation
.end method

.method public abstract b(Landroid/util/SparseArray;)LX/1Dh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "LX/1Dh;"
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/CharSequence;)LX/1Dh;
.end method

.method public abstract c(LX/1n6;)LX/1Dh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1n6",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1Dh;"
        }
    .end annotation
.end method

.method public abstract c(Z)LX/1Dh;
.end method

.method public abstract d(F)LX/1Dh;
.end method

.method public abstract d(LX/1dQ;)LX/1Dh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;)",
            "LX/1Dh;"
        }
    .end annotation
.end method

.method public abstract d(Z)LX/1Dh;
.end method

.method public abstract e(F)LX/1Dh;
.end method

.method public abstract e(LX/1dQ;)LX/1Dh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dQ",
            "<",
            "LX/48C;",
            ">;)",
            "LX/1Dh;"
        }
    .end annotation
.end method

.method public abstract f(F)LX/1Dh;
.end method

.method public abstract f(LX/1dQ;)LX/1Dh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dQ",
            "<",
            "LX/48J;",
            ">;)",
            "LX/1Dh;"
        }
    .end annotation
.end method

.method public abstract o(II)LX/1Dh;
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
.end method

.method public abstract p(II)LX/1Dh;
    .param p2    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
.end method

.method public abstract q(II)LX/1Dh;
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
.end method

.method public abstract r(II)LX/1Dh;
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
.end method

.method public abstract s(II)LX/1Dh;
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
.end method

.method public abstract t(II)LX/1Dh;
    .param p2    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
.end method

.method public abstract u(II)LX/1Dh;
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
.end method

.method public abstract v(II)LX/1Dh;
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
.end method

.method public abstract w(II)LX/1Dh;
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
.end method

.method public abstract x(II)LX/1Dh;
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
.end method

.method public abstract y(II)LX/1Dh;
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
.end method

.method public abstract z(II)LX/1Dh;
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
.end method
