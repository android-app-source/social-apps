.class public LX/1rT;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 332226
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 332227
    return-void
.end method

.method public static a(Ljava/util/Set;)LX/2HQ;
    .locals 6
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/mqttlite/persistence/HighestMqttPersistence;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/mqttlite/persistence/MqttPersistenceRequirement;",
            ">;)",
            "LX/2HQ;"
        }
    .end annotation

    .prologue
    .line 332228
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 332229
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2HL;

    .line 332230
    iget-object v3, v0, LX/2HL;->a:LX/09k;

    .line 332231
    iget-object v4, v3, LX/09k;->c:LX/0Uh;

    const/16 v5, 0x23a

    const/4 p0, 0x1

    invoke-virtual {v4, v5, p0}, LX/0Uh;->a(IZ)Z

    move-result v4

    move v3, v4

    .line 332232
    if-eqz v3, :cond_0

    iget-object v3, v0, LX/2HL;->a:LX/09k;

    invoke-virtual {v3}, LX/09k;->h()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v0, LX/2HL;->a:LX/09k;

    invoke-virtual {v3}, LX/09k;->b()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 332233
    :cond_0
    sget-object v3, LX/2HQ;->APP_USE:LX/2HQ;

    .line 332234
    :goto_1
    move-object v0, v3

    .line 332235
    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 332236
    :cond_1
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    .line 332237
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/2HQ;->APP_USE:LX/2HQ;

    :goto_2
    return-object v0

    :cond_2
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2HQ;

    goto :goto_2

    :cond_3
    sget-object v3, LX/2HQ;->ALWAYS:LX/2HQ;

    goto :goto_1
.end method

.method public static a(Landroid/os/Looper;)Landroid/os/Handler;
    .locals 1
    .param p0    # Landroid/os/Looper;
        .annotation runtime Lcom/facebook/push/mqtt/external/MqttThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/push/mqtt/external/MqttThread;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 332238
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-object v0
.end method

.method public static a(LX/0Zr;)Landroid/os/Looper;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/push/mqtt/external/MqttThread;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 332239
    const-string v0, "MqttHandler"

    sget-object v1, LX/0TP;->URGENT:LX/0TP;

    invoke-virtual {p0, v0, v1}, LX/0Zr;->a(Ljava/lang/String;LX/0TP;)Landroid/os/HandlerThread;

    move-result-object v0

    .line 332240
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 332241
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method

.method public static a()Ljava/lang/Boolean;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/push/mqtt/annotations/IsMqttCombineConnectGetDiffsEnabled;
    .end annotation

    .prologue
    .line 332242
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 332243
    return-void
.end method
