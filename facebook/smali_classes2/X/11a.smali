.class public final LX/11a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 172665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/facebook/interstitial/manager/InterstitialTrigger;
    .locals 1

    .prologue
    .line 172666
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-direct {v0, p0}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 172667
    invoke-static {p1}, LX/11a;->a(Landroid/os/Parcel;)Lcom/facebook/interstitial/manager/InterstitialTrigger;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 172668
    new-array v0, p1, [Lcom/facebook/interstitial/manager/InterstitialTrigger;

    move-object v0, v0

    .line 172669
    return-object v0
.end method
