.class public LX/0pG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final h:Ljava/lang/Object;


# instance fields
.field public a:Ljava/lang/ref/WeakReference;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/0gC;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/data/FeedDataLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ym;

.field public final e:LX/0pJ;

.field public final f:LX/0ad;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 144082
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/0pG;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Ym;LX/0Or;LX/0pJ;LX/0ad;LX/0Or;LX/0Ot;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .param p6    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ym;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/data/FeedDataLoader;",
            ">;",
            "LX/0pJ;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 144074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144075
    iput-object p2, p0, LX/0pG;->b:LX/0Or;

    .line 144076
    iput-object p1, p0, LX/0pG;->d:LX/0Ym;

    .line 144077
    iput-object p3, p0, LX/0pG;->e:LX/0pJ;

    .line 144078
    iput-object p4, p0, LX/0pG;->f:LX/0ad;

    .line 144079
    iput-object p5, p0, LX/0pG;->c:LX/0Or;

    .line 144080
    iput-object p6, p0, LX/0pG;->g:LX/0Ot;

    .line 144081
    return-void
.end method

.method public static a(LX/0pG;Lcom/facebook/api/feedtype/FeedType;Z)LX/0gC;
    .locals 3

    .prologue
    .line 144062
    invoke-static {p1}, LX/0pO;->a(Lcom/facebook/api/feedtype/FeedType;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0pG;->e:LX/0pJ;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0pJ;->b(Z)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_0
    sget-object v0, Lcom/facebook/api/feedtype/FeedType;->a:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {p1, v0}, Lcom/facebook/api/feedtype/FeedType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 144063
    iget-object v1, p0, LX/0pG;->e:LX/0pJ;

    invoke-virtual {v1, v0}, LX/0pJ;->b(Z)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/0pG;->f:LX/0ad;

    sget-short v2, LX/0fe;->W:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    move v0, v0

    .line 144064
    if-nez v0, :cond_4

    .line 144065
    :cond_2
    iget-object v0, p1, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v0, v0

    .line 144066
    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->b:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    .line 144067
    iget-object v1, p0, LX/0pG;->e:LX/0pJ;

    invoke-virtual {v1, v0}, LX/0pJ;->b(Z)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/0pG;->f:LX/0ad;

    sget-short v2, LX/0fe;->O:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    :cond_3
    move v0, v0

    .line 144068
    if-eqz v0, :cond_5

    .line 144069
    :cond_4
    iget-object v0, p0, LX/0pG;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gC;

    .line 144070
    :goto_0
    invoke-interface {v0, p1}, LX/0gC;->a(Lcom/facebook/api/feedtype/FeedType;)V

    .line 144071
    invoke-interface {v0, p2}, LX/0gC;->a(Z)V

    .line 144072
    return-object v0

    .line 144073
    :cond_5
    iget-object v0, p0, LX/0pG;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gC;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/0pG;
    .locals 14

    .prologue
    .line 144083
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 144084
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 144085
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 144086
    if-nez v1, :cond_0

    .line 144087
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144088
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 144089
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 144090
    sget-object v1, LX/0pG;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 144091
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 144092
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 144093
    :cond_1
    if-nez v1, :cond_4

    .line 144094
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 144095
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 144096
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 144097
    new-instance v7, LX/0pG;

    invoke-static {v0}, LX/0Ym;->a(LX/0QB;)LX/0Ym;

    move-result-object v8

    check-cast v8, LX/0Ym;

    invoke-interface {v0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v9

    const/16 v10, 0x5eb

    invoke-static {v9, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/0pJ;->a(LX/0QB;)LX/0pJ;

    move-result-object v10

    check-cast v10, LX/0pJ;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-interface {v0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v12

    const/16 v13, 0x5f7

    invoke-static {v12, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v13, 0x1430

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v7 .. v13}, LX/0pG;-><init>(LX/0Ym;LX/0Or;LX/0pJ;LX/0ad;LX/0Or;LX/0Ot;)V

    .line 144098
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 144099
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 144100
    if-nez v1, :cond_2

    .line 144101
    sget-object v0, LX/0pG;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pG;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 144102
    :goto_1
    if-eqz v0, :cond_3

    .line 144103
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 144104
    :goto_3
    check-cast v0, LX/0pG;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 144105
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 144106
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 144107
    :catchall_1
    move-exception v0

    .line 144108
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 144109
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 144110
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 144111
    :cond_2
    :try_start_8
    sget-object v0, LX/0pG;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pG;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a()LX/0gC;
    .locals 1

    .prologue
    .line 144061
    iget-object v0, p0, LX/0pG;->d:LX/0Ym;

    invoke-virtual {v0}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0pG;->a(Lcom/facebook/api/feedtype/FeedType;)LX/0gC;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/api/feedtype/FeedType;)LX/0gC;
    .locals 2

    .prologue
    .line 144056
    iget-object v0, p0, LX/0pG;->d:LX/0Ym;

    invoke-virtual {v0, p1}, LX/0Ym;->a(Lcom/facebook/api/feedtype/FeedType;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 144057
    iget-object v0, p0, LX/0pG;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0pG;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0pG;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gC;

    invoke-interface {v0}, LX/0gC;->s()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v0

    invoke-static {p1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 144058
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x1

    invoke-static {p0, p1, v1}, LX/0pG;->a(LX/0pG;Lcom/facebook/api/feedtype/FeedType;Z)LX/0gC;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/0pG;->a:Ljava/lang/ref/WeakReference;

    .line 144059
    :cond_1
    iget-object v0, p0, LX/0pG;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gC;

    move-object v0, v0

    .line 144060
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/0pG;->a(LX/0pG;Lcom/facebook/api/feedtype/FeedType;Z)LX/0gC;

    move-result-object v0

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 3

    .prologue
    .line 144053
    iget-object v0, p0, LX/0pG;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0pG;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 144054
    iget-object v0, p0, LX/0pG;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/feed/data/FeedDataLoaderFactory$1;

    invoke-direct {v1, p0}, Lcom/facebook/feed/data/FeedDataLoaderFactory$1;-><init>(LX/0pG;)V

    const v2, -0x5504327c

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 144055
    :cond_0
    return-void
.end method
