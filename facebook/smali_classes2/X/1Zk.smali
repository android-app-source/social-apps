.class public LX/1Zk;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hk;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DAx;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Zm;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0ad;

.field public f:LX/0if;

.field private final g:I

.field public final h:LX/1Zl;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/0if;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DAx;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Zm;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 275268
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 275269
    const/4 v0, 0x1

    iput v0, p0, LX/1Zk;->g:I

    .line 275270
    new-instance v0, Lcom/facebook/devicerequests/DeviceRequestsNewsFeedScanningController$1;

    invoke-direct {v0, p0}, Lcom/facebook/devicerequests/DeviceRequestsNewsFeedScanningController$1;-><init>(LX/1Zk;)V

    iput-object v0, p0, LX/1Zk;->h:LX/1Zl;

    .line 275271
    iput-object p1, p0, LX/1Zk;->a:LX/0Ot;

    .line 275272
    iput-object p2, p0, LX/1Zk;->b:LX/0Ot;

    .line 275273
    iput-object p3, p0, LX/1Zk;->c:LX/0Ot;

    .line 275274
    iput-object p4, p0, LX/1Zk;->d:LX/0Ot;

    .line 275275
    iput-object p5, p0, LX/1Zk;->e:LX/0ad;

    .line 275276
    iput-object p6, p0, LX/1Zk;->f:LX/0if;

    .line 275277
    return-void
.end method

.method public static b(LX/1Zk;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 275278
    iget-object v0, p0, LX/1Zk;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x325

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275279
    iget-object v0, p0, LX/1Zk;->f:LX/0if;

    sget-object v1, LX/0ig;->aV:LX/0ih;

    invoke-virtual {v0, v1, v4, v5}, LX/0if;->a(LX/0ih;J)V

    .line 275280
    iget-object v0, p0, LX/1Zk;->f:LX/0if;

    sget-object v1, LX/0ig;->aV:LX/0ih;

    const-string v2, "start_feed_scan"

    invoke-virtual {v0, v1, v4, v5, v2}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 275281
    iget-object v0, p0, LX/1Zk;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Zm;

    iget-object v1, p0, LX/1Zk;->h:LX/1Zl;

    invoke-virtual {v0, v1}, LX/1Zm;->a(LX/1Zl;)Z

    .line 275282
    iget-object v0, p0, LX/1Zk;->e:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget v2, LX/24A;->a:I

    const/16 v3, 0xa

    invoke-interface {v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;II)I

    move-result v0

    .line 275283
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    new-instance v2, Lcom/facebook/devicerequests/DeviceRequestsNewsFeedScanningController$2;

    invoke-direct {v2, p0}, Lcom/facebook/devicerequests/DeviceRequestsNewsFeedScanningController$2;-><init>(LX/1Zk;)V

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 275284
    :cond_0
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 0

    .prologue
    .line 275285
    invoke-static {p0}, LX/1Zk;->b(LX/1Zk;)V

    .line 275286
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 275287
    iget-object v0, p0, LX/1Zk;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Zm;

    iget-object v1, p0, LX/1Zk;->h:LX/1Zl;

    invoke-virtual {v0, v1}, LX/1Zm;->b(LX/1Zl;)Z

    .line 275288
    return-void
.end method
