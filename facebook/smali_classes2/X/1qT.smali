.class public LX/1qT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qU;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1qT;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/database/userchecker/IsDbUserCheckEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 330687
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330688
    iput-object p1, p0, LX/1qT;->b:LX/0Or;

    .line 330689
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/1qT;->c:Ljava/util/Map;

    .line 330690
    iput-object p2, p0, LX/1qT;->a:LX/0Or;

    .line 330691
    return-void
.end method

.method public static a(LX/0QB;)LX/1qT;
    .locals 5

    .prologue
    .line 330692
    sget-object v0, LX/1qT;->d:LX/1qT;

    if-nez v0, :cond_1

    .line 330693
    const-class v1, LX/1qT;

    monitor-enter v1

    .line 330694
    :try_start_0
    sget-object v0, LX/1qT;->d:LX/1qT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 330695
    if-eqz v2, :cond_0

    .line 330696
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 330697
    new-instance v3, LX/1qT;

    const/16 v4, 0x12cb

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 p0, 0x30f

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/1qT;-><init>(LX/0Or;LX/0Or;)V

    .line 330698
    move-object v0, v3

    .line 330699
    sput-object v0, LX/1qT;->d:LX/1qT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 330700
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 330701
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 330702
    :cond_1
    sget-object v0, LX/1qT;->d:LX/1qT;

    return-object v0

    .line 330703
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 330704
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 330705
    invoke-static {p1}, LX/1qT;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 330706
    iget-object v1, p0, LX/1qT;->c:Ljava/util/Map;

    invoke-interface {v1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330707
    invoke-static {p1}, LX/1qT;->d(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 330708
    const/4 v2, 0x0

    .line 330709
    :try_start_0
    new-instance v1, Ljava/io/RandomAccessFile;

    const-string v3, "rw"

    invoke-direct {v1, v0, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 330710
    const-wide/16 v2, 0x0

    :try_start_1
    invoke-virtual {v1, v2, v3}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 330711
    const-string v0, "UTF-8"

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/RandomAccessFile;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 330712
    const/4 v0, 0x0

    :try_start_2
    invoke-static {v1, v0}, LX/1md;->a(Ljava/io/Closeable;Z)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 330713
    return-void

    .line 330714
    :catch_0
    move-exception v0

    .line 330715
    new-instance v1, LX/497;

    const-string v2, "Error when closing file"

    invoke-direct {v1, v2, v0}, LX/497;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 330716
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 330717
    :goto_0
    :try_start_3
    new-instance v2, LX/497;

    const-string v3, "Error when writing to a file"

    invoke-direct {v2, v3, v0}, LX/497;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 330718
    :catchall_0
    move-exception v0

    .line 330719
    :goto_1
    const/4 v2, 0x1

    :try_start_4
    invoke-static {v1, v2}, LX/1md;->a(Ljava/io/Closeable;Z)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 330720
    throw v0

    .line 330721
    :catch_2
    move-exception v0

    .line 330722
    new-instance v1, LX/497;

    const-string v2, "Error when closing file"

    invoke-direct {v1, v2, v0}, LX/497;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 330723
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1

    .line 330724
    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 330725
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static d(Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    .line 330726
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-uid"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private e(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 330727
    invoke-static {p1}, LX/1qT;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 330728
    iget-object v0, p0, LX/1qT;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 330729
    if-nez v0, :cond_0

    .line 330730
    invoke-static {p1}, LX/1qT;->d(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 330731
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 330732
    const/4 v0, 0x0

    .line 330733
    :cond_0
    :goto_0
    return-object v0

    .line 330734
    :cond_1
    :try_start_0
    invoke-static {v0}, LX/1t3;->b(Ljava/io/File;)[B

    move-result-object v2

    .line 330735
    new-instance v0, Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-direct {v0, v2, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 330736
    iget-object v2, p0, LX/1qT;->c:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 330737
    :catch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot Read From UID File"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 330738
    sget-object v0, LX/03R;->YES:LX/03R;

    iget-object v1, p0, LX/1qT;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 330739
    :cond_0
    :goto_0
    return-void

    .line 330740
    :cond_1
    iget-object v0, p0, LX/1qT;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 330741
    invoke-direct {p0, p1}, LX/1qT;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 330742
    if-nez v0, :cond_2

    .line 330743
    if-eqz v1, :cond_0

    .line 330744
    new-instance v0, LX/497;

    const-string v1, "Expected uId to be null"

    invoke-direct {v0, v1}, LX/497;-><init>(Ljava/lang/String;)V

    throw v0

    .line 330745
    :cond_2
    if-eqz v1, :cond_3

    .line 330746
    iget-object p0, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p0

    .line 330747
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 330748
    new-instance v0, LX/497;

    const-string v1, "Expected uId to be equal to loggedInUser"

    invoke-direct {v0, v1}, LX/497;-><init>(Ljava/lang/String;)V

    throw v0

    .line 330749
    :cond_3
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 330750
    invoke-direct {p0, p1, v0}, LX/1qT;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 330751
    iget-object v0, p0, LX/1qT;->c:Ljava/util/Map;

    invoke-static {p1}, LX/1qT;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 330752
    invoke-static {p1}, LX/1qT;->d(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 330753
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 330754
    return-void
.end method
