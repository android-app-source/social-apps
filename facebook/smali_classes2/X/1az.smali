.class public LX/1az;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1b0;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1az;


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 279017
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279018
    iput-object p1, p0, LX/1az;->a:Lcom/facebook/content/SecureContextHelper;

    .line 279019
    return-void
.end method

.method public static a(LX/0QB;)LX/1az;
    .locals 4

    .prologue
    .line 279020
    sget-object v0, LX/1az;->b:LX/1az;

    if-nez v0, :cond_1

    .line 279021
    const-class v1, LX/1az;

    monitor-enter v1

    .line 279022
    :try_start_0
    sget-object v0, LX/1az;->b:LX/1az;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 279023
    if-eqz v2, :cond_0

    .line 279024
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 279025
    new-instance p0, LX/1az;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3}, LX/1az;-><init>(Lcom/facebook/content/SecureContextHelper;)V

    .line 279026
    move-object v0, p0

    .line 279027
    sput-object v0, LX/1az;->b:LX/1az;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 279028
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 279029
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 279030
    :cond_1
    sget-object v0, LX/1az;->b:LX/1az;

    return-object v0

    .line 279031
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 279032
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Landroid/content/Intent;Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V
    .locals 1
    .param p3    # Lcom/facebook/productionprompts/logging/PromptAnalytics;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 279033
    const-string v0, "extra_creativecam_launch_configuration"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 279034
    const-string v0, "extra_creativecam_composer_session_id"

    invoke-virtual {p0, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 279035
    const-string v0, "extra_creativecam_prompt_entrypoint_analytics"

    invoke-virtual {p0, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 279036
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;
    .locals 1
    .param p3    # Lcom/facebook/productionprompts/logging/PromptAnalytics;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 279037
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 279038
    invoke-static {v0, p1, p2, p3}, LX/1az;->a(Landroid/content/Intent;Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 279039
    invoke-static {v0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->a(Landroid/content/Intent;)Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/app/Activity;ILcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V
    .locals 2
    .param p5    # Lcom/facebook/productionprompts/logging/PromptAnalytics;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 279040
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 279041
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 279042
    invoke-static {v0, p3, p4, p5}, LX/1az;->a(Landroid/content/Intent;Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 279043
    iget-object v1, p0, LX/1az;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 279044
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;ILcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V
    .locals 3
    .param p5    # Lcom/facebook/productionprompts/logging/PromptAnalytics;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 279045
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 279046
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 279047
    invoke-static {v0, p3, p4, p5}, LX/1az;->a(Landroid/content/Intent;Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 279048
    iget-object v1, p0, LX/1az;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 279049
    return-void
.end method
