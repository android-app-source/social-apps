.class public final LX/1qc;
.super LX/1eP;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1eP",
        "<",
        "LX/1FL;",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1cJ;

.field public final b:LX/1cW;


# direct methods
.method public constructor <init>(LX/1cJ;LX/1cd;LX/1cW;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 331233
    iput-object p1, p0, LX/1qc;->a:LX/1cJ;

    .line 331234
    invoke-direct {p0, p2}, LX/1eP;-><init>(LX/1cd;)V

    .line 331235
    iput-object p3, p0, LX/1qc;->b:LX/1cW;

    .line 331236
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Z)V
    .locals 5

    .prologue
    .line 331237
    check-cast p1, LX/1FL;

    .line 331238
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 331239
    iget-object v0, p0, LX/1qc;->b:LX/1cW;

    .line 331240
    iget-object v1, v0, LX/1cW;->a:LX/1bf;

    move-object v0, v1

    .line 331241
    iget-object v1, v0, LX/1bf;->c:LX/1ny;

    move-object v1, v1

    .line 331242
    iget-boolean v2, v0, LX/1bf;->l:Z

    move v2, v2

    .line 331243
    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    .line 331244
    :cond_0
    :goto_0
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 331245
    invoke-virtual {v0, p1, p2}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 331246
    return-void

    .line 331247
    :cond_1
    iget-object v2, p0, LX/1qc;->a:LX/1cJ;

    iget-object v2, v2, LX/1cJ;->c:LX/1Ao;

    iget-object v3, p0, LX/1qc;->b:LX/1cW;

    .line 331248
    iget-object v4, v3, LX/1cW;->d:Ljava/lang/Object;

    move-object v3, v4

    .line 331249
    invoke-virtual {v2, v0, v3}, LX/1Ao;->c(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v0

    .line 331250
    iget-object v2, p0, LX/1qc;->a:LX/1cJ;

    iget-object v2, v2, LX/1cJ;->d:LX/1IZ;

    .line 331251
    iget-object v3, v1, LX/1ny;->a:Ljava/lang/String;

    move-object v1, v3

    .line 331252
    invoke-interface {v2, v1, v0, p1}, LX/1IZ;->a(Ljava/lang/String;LX/1bh;LX/1FL;)V

    goto :goto_0
.end method
