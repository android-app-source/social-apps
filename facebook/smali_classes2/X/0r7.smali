.class public LX/0r7;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0r7;


# instance fields
.field private b:J

.field private c:Z

.field private d:J

.field private e:J

.field public f:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 149056
    new-instance v0, LX/0r7;

    invoke-direct {v0}, LX/0r7;-><init>()V

    sput-object v0, LX/0r7;->a:LX/0r7;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 149057
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149058
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 149059
    iget-wide v0, p0, LX/0r7;->b:J

    int-to-long v2, p1

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/0r7;->b:J

    .line 149060
    iget-wide v0, p0, LX/0r7;->d:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0r7;->d:J

    .line 149061
    return-void
.end method

.method public final a(JZJJJ)V
    .locals 0

    .prologue
    .line 149062
    iput-wide p1, p0, LX/0r7;->b:J

    .line 149063
    iput-boolean p3, p0, LX/0r7;->c:Z

    .line 149064
    iput-wide p4, p0, LX/0r7;->d:J

    .line 149065
    iput-wide p6, p0, LX/0r7;->e:J

    .line 149066
    iput-wide p8, p0, LX/0r7;->f:J

    .line 149067
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    .line 149068
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 149069
    const-string v1, "sched:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, LX/0r7;->b:J

    div-long/2addr v2, v6

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " hP:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, LX/0r7;->c:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sF:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, LX/0r7;->d:J

    iget-wide v4, p0, LX/0r7;->f:J

    sub-long/2addr v2, v4

    div-long/2addr v2, v6

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sH:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, LX/0r7;->d:J

    iget-wide v4, p0, LX/0r7;->e:J

    sub-long/2addr v2, v4

    div-long/2addr v2, v6

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149070
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
