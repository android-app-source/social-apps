.class public LX/0ks;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0ks;


# instance fields
.field public volatile a:Z

.field public volatile b:Z


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 127907
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127908
    return-void
.end method

.method public static a(LX/0QB;)LX/0ks;
    .locals 3

    .prologue
    .line 127909
    sget-object v0, LX/0ks;->c:LX/0ks;

    if-nez v0, :cond_1

    .line 127910
    const-class v1, LX/0ks;

    monitor-enter v1

    .line 127911
    :try_start_0
    sget-object v0, LX/0ks;->c:LX/0ks;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 127912
    if-eqz v2, :cond_0

    .line 127913
    :try_start_1
    new-instance v0, LX/0ks;

    invoke-direct {v0}, LX/0ks;-><init>()V

    .line 127914
    move-object v0, v0

    .line 127915
    sput-object v0, LX/0ks;->c:LX/0ks;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127916
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 127917
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 127918
    :cond_1
    sget-object v0, LX/0ks;->c:LX/0ks;

    return-object v0

    .line 127919
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 127920
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final c()V
    .locals 1

    .prologue
    .line 127921
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0ks;->b:Z

    .line 127922
    return-void
.end method
