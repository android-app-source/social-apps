.class public LX/1Mr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static a:LX/1gy;

.field public static final e:Ljava/util/regex/Pattern;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:LX/04P;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 236337
    const-string v0, "^[0-9]+L$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/1Mr;->e:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/04P;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 236338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236339
    iput-object p1, p0, LX/1Mr;->b:Landroid/content/Context;

    .line 236340
    if-nez p2, :cond_0

    .line 236341
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 236342
    :cond_0
    iput-object p2, p0, LX/1Mr;->d:LX/04P;

    .line 236343
    iget-object v0, p0, LX/1Mr;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1Mr;->c:Ljava/lang/String;

    .line 236344
    return-void
.end method

.method public static a(LX/1Mr;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 236345
    iget-object v0, p0, LX/1Mr;->d:LX/04P;

    invoke-virtual {v0, p1, p2}, LX/04P;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 236346
    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()LX/1gy;
    .locals 10

    .prologue
    .line 236347
    iget-object v0, p0, LX/1Mr;->c:Ljava/lang/String;

    .line 236348
    const-string v1, "com.facebook.versioncontrol.revision"

    invoke-static {p0, v1, v0}, LX/1Mr;->a(LX/1Mr;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 236349
    const-string v1, "com.facebook.versioncontrol.branch"

    invoke-static {p0, v1, v0}, LX/1Mr;->a(LX/1Mr;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 236350
    const-string v1, "com.facebook.build_time"

    invoke-static {p0, v1, v0}, LX/1Mr;->a(LX/1Mr;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 236351
    const-wide/16 v5, 0x0

    .line 236352
    const-string v7, ""

    .line 236353
    if-eqz v1, :cond_1

    sget-object v2, LX/1Mr;->e:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 236354
    if-eqz v2, :cond_0

    .line 236355
    const/4 v8, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    move-wide v5, v8

    .line 236356
    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v1, v2, v7}, Ljava/text/DateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v1

    .line 236357
    const-string v2, "PST8PDT"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 236358
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    move-object v7, v1

    .line 236359
    :cond_0
    new-instance v2, LX/1gy;

    invoke-direct/range {v2 .. v7}, LX/1gy;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    move-object v0, v2

    .line 236360
    return-object v0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
