.class public LX/1AP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile f:LX/1AP;


# instance fields
.field private final b:LX/0So;

.field public final c:LX/03V;

.field private final d:LX/1AQ;

.field public e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 210468
    const-class v0, LX/1AP;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1AP;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0So;LX/03V;LX/1AQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 210463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210464
    iput-object p1, p0, LX/1AP;->b:LX/0So;

    .line 210465
    iput-object p2, p0, LX/1AP;->c:LX/03V;

    .line 210466
    iput-object p3, p0, LX/1AP;->d:LX/1AQ;

    .line 210467
    return-void
.end method

.method public static a(LX/0QB;)LX/1AP;
    .locals 6

    .prologue
    .line 210450
    sget-object v0, LX/1AP;->f:LX/1AP;

    if-nez v0, :cond_1

    .line 210451
    const-class v1, LX/1AP;

    monitor-enter v1

    .line 210452
    :try_start_0
    sget-object v0, LX/1AP;->f:LX/1AP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 210453
    if-eqz v2, :cond_0

    .line 210454
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 210455
    new-instance p0, LX/1AP;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/1AQ;->a(LX/0QB;)LX/1AQ;

    move-result-object v5

    check-cast v5, LX/1AQ;

    invoke-direct {p0, v3, v4, v5}, LX/1AP;-><init>(LX/0So;LX/03V;LX/1AQ;)V

    .line 210456
    move-object v0, p0

    .line 210457
    sput-object v0, LX/1AP;->f:LX/1AP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210458
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 210459
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 210460
    :cond_1
    sget-object v0, LX/1AP;->f:LX/1AP;

    return-object v0

    .line 210461
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 210462
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/0fx;LX/1Kq;)LX/1Ks;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 210423
    instance-of v0, p2, LX/1Kr;

    .line 210424
    new-instance v1, LX/1Ks;

    invoke-direct {v1, p0, v0, p1, p2}, LX/1Ks;-><init>(LX/1AP;ZLX/0fx;LX/1Kq;)V

    return-object v1
.end method

.method public static a$redex0(LX/1AP;I)V
    .locals 1

    .prologue
    .line 210438
    packed-switch p1, :pswitch_data_0

    .line 210439
    :goto_0
    return-void

    .line 210440
    :pswitch_0
    iget-object v0, p0, LX/1AP;->d:LX/1AQ;

    .line 210441
    iget-boolean p0, v0, LX/1AQ;->d:Z

    if-eqz p0, :cond_0

    .line 210442
    iget-object p0, v0, LX/1AQ;->a:LX/0wY;

    iget-object p1, v0, LX/1AQ;->b:LX/0wa;

    invoke-interface {p0, p1}, LX/0wY;->b(LX/0wa;)V

    .line 210443
    :cond_0
    const/4 p0, 0x0

    iput-boolean p0, v0, LX/1AQ;->d:Z

    .line 210444
    goto :goto_0

    .line 210445
    :pswitch_1
    iget-object v0, p0, LX/1AP;->d:LX/1AQ;

    .line 210446
    iget-boolean p0, v0, LX/1AQ;->d:Z

    if-nez p0, :cond_1

    .line 210447
    iget-object p0, v0, LX/1AQ;->a:LX/0wY;

    iget-object p1, v0, LX/1AQ;->b:LX/0wa;

    invoke-interface {p0, p1}, LX/0wY;->a(LX/0wa;)V

    .line 210448
    :cond_1
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/1AQ;->d:Z

    .line 210449
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private c(LX/0fx;)LX/1Kq;
    .locals 3

    .prologue
    .line 210427
    instance-of v0, p1, LX/1J7;

    if-nez v0, :cond_0

    .line 210428
    sget-object v0, LX/1Ys;->a:LX/1Ys;

    move-object v0, v0

    .line 210429
    :goto_0
    return-object v0

    .line 210430
    :cond_0
    check-cast p1, LX/1J7;

    .line 210431
    invoke-interface {p1}, LX/1J7;->a()I

    move-result v1

    .line 210432
    if-nez v1, :cond_1

    .line 210433
    sget-object v0, LX/1Ys;->a:LX/1Ys;

    move-object v0, v0

    .line 210434
    goto :goto_0

    .line 210435
    :cond_1
    const/4 v0, -0x1

    if-ne v1, v0, :cond_2

    .line 210436
    new-instance v0, LX/1Kp;

    const/16 v1, 0x64

    iget-object v2, p0, LX/1AP;->b:LX/0So;

    invoke-direct {v0, v1, v2}, LX/1Kp;-><init>(ILX/0So;)V

    goto :goto_0

    .line 210437
    :cond_2
    new-instance v0, LX/1Kr;

    iget-object v2, p0, LX/1AP;->d:LX/1AQ;

    invoke-direct {v0, v1, v2}, LX/1Kr;-><init>(ILX/1AQ;)V

    goto :goto_0
.end method


# virtual methods
.method public final b(LX/0fx;)LX/1Ks;
    .locals 1

    .prologue
    .line 210425
    invoke-direct {p0, p1}, LX/1AP;->c(LX/0fx;)LX/1Kq;

    move-result-object v0

    .line 210426
    invoke-direct {p0, p1, v0}, LX/1AP;->a(LX/0fx;LX/1Kq;)LX/1Ks;

    move-result-object v0

    return-object v0
.end method
