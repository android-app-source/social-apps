.class public LX/0VG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/Throwable;

.field public final d:Z

.field private final e:I

.field private final f:Z


# direct methods
.method public constructor <init>(LX/0VK;)V
    .locals 1

    .prologue
    .line 67535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67536
    iget-object v0, p1, LX/0VK;->a:Ljava/lang/String;

    move-object v0, v0

    .line 67537
    iput-object v0, p0, LX/0VG;->a:Ljava/lang/String;

    .line 67538
    iget-object v0, p1, LX/0VK;->b:Ljava/lang/String;

    move-object v0, v0

    .line 67539
    iput-object v0, p0, LX/0VG;->b:Ljava/lang/String;

    .line 67540
    iget-object v0, p1, LX/0VK;->c:Ljava/lang/Throwable;

    move-object v0, v0

    .line 67541
    iput-object v0, p0, LX/0VG;->c:Ljava/lang/Throwable;

    .line 67542
    iget-boolean v0, p1, LX/0VK;->d:Z

    move v0, v0

    .line 67543
    iput-boolean v0, p0, LX/0VG;->d:Z

    .line 67544
    iget v0, p1, LX/0VK;->e:I

    move v0, v0

    .line 67545
    iput v0, p0, LX/0VG;->e:I

    .line 67546
    iget-boolean v0, p1, LX/0VK;->f:Z

    move v0, v0

    .line 67547
    iput-boolean v0, p0, LX/0VG;->f:Z

    .line 67548
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;I)LX/0VG;
    .locals 1

    .prologue
    .line 67581
    new-instance v0, LX/0VK;

    invoke-direct {v0}, LX/0VK;-><init>()V

    .line 67582
    iput-object p0, v0, LX/0VK;->a:Ljava/lang/String;

    .line 67583
    move-object v0, v0

    .line 67584
    iput-object p1, v0, LX/0VK;->b:Ljava/lang/String;

    .line 67585
    move-object v0, v0

    .line 67586
    iput p2, v0, LX/0VK;->e:I

    .line 67587
    move-object v0, v0

    .line 67588
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;
    .locals 1

    .prologue
    .line 67575
    new-instance v0, LX/0VK;

    invoke-direct {v0}, LX/0VK;-><init>()V

    .line 67576
    iput-object p0, v0, LX/0VK;->a:Ljava/lang/String;

    .line 67577
    move-object v0, v0

    .line 67578
    iput-object p1, v0, LX/0VK;->b:Ljava/lang/String;

    .line 67579
    move-object v0, v0

    .line 67580
    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 67574
    if-eq p0, p1, :cond_0

    if-eqz p0, :cond_1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;
    .locals 1

    .prologue
    .line 67568
    new-instance v0, LX/0VK;

    invoke-direct {v0}, LX/0VK;-><init>()V

    .line 67569
    iput-object p0, v0, LX/0VK;->a:Ljava/lang/String;

    .line 67570
    move-object v0, v0

    .line 67571
    iput-object p1, v0, LX/0VK;->b:Ljava/lang/String;

    .line 67572
    move-object v0, v0

    .line 67573
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67567
    iget-object v0, p0, LX/0VG;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67566
    iget-object v0, p0, LX/0VG;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 67565
    iget-object v0, p0, LX/0VG;->c:Ljava/lang/Throwable;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 67564
    iget-boolean v0, p0, LX/0VG;->d:Z

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 67563
    iget v0, p0, LX/0VG;->e:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 67553
    if-ne p0, p1, :cond_1

    .line 67554
    :cond_0
    :goto_0
    return v0

    .line 67555
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 67556
    :cond_3
    check-cast p1, LX/0VG;

    .line 67557
    iget-boolean v2, p0, LX/0VG;->d:Z

    iget-boolean v3, p1, LX/0VG;->d:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 67558
    :cond_4
    iget-boolean v2, p0, LX/0VG;->f:Z

    iget-boolean v3, p1, LX/0VG;->f:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    .line 67559
    :cond_5
    iget v2, p0, LX/0VG;->e:I

    iget v3, p1, LX/0VG;->e:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    .line 67560
    :cond_6
    iget-object v2, p0, LX/0VG;->a:Ljava/lang/String;

    iget-object v3, p1, LX/0VG;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_0

    .line 67561
    :cond_7
    iget-object v2, p0, LX/0VG;->c:Ljava/lang/Throwable;

    iget-object v3, p1, LX/0VG;->c:Ljava/lang/Throwable;

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    goto :goto_0

    .line 67562
    :cond_8
    iget-object v2, p0, LX/0VG;->b:Ljava/lang/String;

    iget-object v3, p1, LX/0VG;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 67552
    iget-boolean v0, p0, LX/0VG;->f:Z

    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 67549
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/0VG;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/0VG;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, LX/0VG;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, LX/0VG;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 67550
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    move v0, v1

    .line 67551
    return v0
.end method
