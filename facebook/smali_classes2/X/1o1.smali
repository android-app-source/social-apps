.class public final LX/1o1;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/1nz;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 318127
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 318128
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "imageUri"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "callerContext"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/1o1;->b:[Ljava/lang/String;

    .line 318129
    iput v3, p0, LX/1o1;->c:I

    .line 318130
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/1o1;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/1o1;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/1o1;LX/1De;IILcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;)V
    .locals 1

    .prologue
    .line 318163
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 318164
    iput-object p4, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    .line 318165
    iget-object v0, p0, LX/1o1;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 318166
    return-void
.end method


# virtual methods
.method public final a(LX/1Up;)LX/1o1;
    .locals 1

    .prologue
    .line 318167
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->f:LX/1Up;

    .line 318168
    return-object p0
.end method

.method public final a(LX/1dc;)LX/1o1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1o1;"
        }
    .end annotation

    .prologue
    .line 318169
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->l:LX/1dc;

    .line 318170
    return-object p0
.end method

.method public final a(LX/4Ab;)LX/1o1;
    .locals 1

    .prologue
    .line 318171
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->j:LX/4Ab;

    .line 318172
    return-object p0
.end method

.method public final a(Landroid/graphics/ColorFilter;)LX/1o1;
    .locals 1

    .prologue
    .line 318173
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->k:Landroid/graphics/ColorFilter;

    .line 318174
    return-object p0
.end method

.method public final a(Landroid/graphics/PointF;)LX/1o1;
    .locals 1

    .prologue
    .line 318175
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->h:Landroid/graphics/PointF;

    .line 318176
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)LX/1o1;
    .locals 2

    .prologue
    .line 318177
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->b:Landroid/net/Uri;

    .line 318178
    iget-object v0, p0, LX/1o1;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 318179
    return-object p0
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;)LX/1o1;
    .locals 2

    .prologue
    .line 318180
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 318181
    iget-object v0, p0, LX/1o1;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 318182
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 318183
    invoke-super {p0}, LX/1X5;->a()V

    .line 318184
    const/4 v0, 0x0

    iput-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    .line 318185
    sget-object v0, LX/1nz;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 318186
    return-void
.end method

.method public final b(LX/1Up;)LX/1o1;
    .locals 1

    .prologue
    .line 318159
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->i:LX/1Up;

    .line 318160
    return-object p0
.end method

.method public final b(LX/1dc;)LX/1o1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1o1;"
        }
    .end annotation

    .prologue
    .line 318161
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->m:LX/1dc;

    .line 318162
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/1o1;
    .locals 1

    .prologue
    .line 318157
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->d:Ljava/lang/String;

    .line 318158
    return-object p0
.end method

.method public final c(F)LX/1o1;
    .locals 1

    .prologue
    .line 318155
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    iput p1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->a:F

    .line 318156
    return-object p0
.end method

.method public final c(LX/1Up;)LX/1o1;
    .locals 1

    .prologue
    .line 318153
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->n:LX/1Up;

    .line 318154
    return-object p0
.end method

.method public final c(LX/1dc;)LX/1o1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1o1;"
        }
    .end annotation

    .prologue
    .line 318151
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->o:LX/1dc;

    .line 318152
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/1nz;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 318141
    iget-object v1, p0, LX/1o1;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1o1;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/1o1;->c:I

    if-ge v1, v2, :cond_2

    .line 318142
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 318143
    :goto_0
    iget v2, p0, LX/1o1;->c:I

    if-ge v0, v2, :cond_1

    .line 318144
    iget-object v2, p0, LX/1o1;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 318145
    iget-object v2, p0, LX/1o1;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 318146
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 318147
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 318148
    :cond_2
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    .line 318149
    invoke-virtual {p0}, LX/1o1;->a()V

    .line 318150
    return-object v0
.end method

.method public final d(LX/1Up;)LX/1o1;
    .locals 1

    .prologue
    .line 318139
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->p:LX/1Up;

    .line 318140
    return-object p0
.end method

.method public final d(LX/1dc;)LX/1o1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1o1;"
        }
    .end annotation

    .prologue
    .line 318137
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->r:LX/1dc;

    .line 318138
    return-object p0
.end method

.method public final e(LX/1Up;)LX/1o1;
    .locals 1

    .prologue
    .line 318135
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->s:LX/1Up;

    .line 318136
    return-object p0
.end method

.method public final e(LX/1dc;)LX/1o1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1o1;"
        }
    .end annotation

    .prologue
    .line 318133
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    iput-object p1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->u:LX/1dc;

    .line 318134
    return-object p0
.end method

.method public final h(I)LX/1o1;
    .locals 1

    .prologue
    .line 318131
    iget-object v0, p0, LX/1o1;->a:Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    iput p1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->q:I

    .line 318132
    return-object p0
.end method
