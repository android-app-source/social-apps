.class public LX/1ow;
.super LX/1ox;
.source ""


# direct methods
.method public constructor <init>(LX/1p0;LX/0yK;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/sdk/annotations/UseBackupRewriteRulesGatekeeper;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/sdk/annotations/UseSessionlessBackupRewriteRules;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1p0;",
            "LX/0yK;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 327581
    invoke-direct/range {p0 .. p5}, LX/1ox;-><init>(LX/1p0;LX/0yK;LX/0Or;LX/0Or;LX/0Or;)V

    .line 327582
    return-void
.end method

.method public static c(LX/0QB;)LX/1ow;
    .locals 6

    .prologue
    .line 327579
    new-instance v0, LX/1ow;

    invoke-static {p0}, Lcom/facebook/zero/logging/FbZeroLogger;->b(LX/0QB;)Lcom/facebook/zero/logging/FbZeroLogger;

    move-result-object v1

    check-cast v1, LX/1p0;

    invoke-static {p0}, LX/0yI;->b(LX/0QB;)LX/0yI;

    move-result-object v2

    check-cast v2, LX/0yK;

    const/16 v3, 0x14d1

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x15b3

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x15b4

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/1ow;-><init>(LX/1p0;LX/0yK;LX/0Or;LX/0Or;LX/0Or;)V

    .line 327580
    return-object v0
.end method
