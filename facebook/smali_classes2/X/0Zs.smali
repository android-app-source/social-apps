.class public LX/0Zs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Zt;

.field private final c:LX/0Zv;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83977
    const-class v0, LX/0Zs;

    sput-object v0, LX/0Zs;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zt;LX/0Zv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 83966
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83967
    iput-object p1, p0, LX/0Zs;->b:LX/0Zt;

    .line 83968
    iput-object p2, p0, LX/0Zs;->c:LX/0Zv;

    .line 83969
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 83972
    const-string v0, "INeedInitForBroadcastReceiverRegister-RegisterActionReceivers"

    const v1, 0x1ca94e0f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 83973
    :try_start_0
    iget-object v0, p0, LX/0Zs;->c:LX/0Zv;

    iget-object v1, p0, LX/0Zs;->b:LX/0Zt;

    invoke-interface {v0, v1}, LX/0Zv;->a(LX/0Zt;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83974
    const v0, 0x3cbd131

    invoke-static {v0}, LX/02m;->a(I)V

    .line 83975
    return-void

    .line 83976
    :catchall_0
    move-exception v0

    const v1, -0x2429e4ca

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 0

    .prologue
    .line 83970
    invoke-direct {p0}, LX/0Zs;->a()V

    .line 83971
    return-void
.end method
