.class public final LX/0SO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Qf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Qf",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public volatile a:LX/0Qf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qf",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final b:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<TV;>;"
        }
    .end annotation
.end field

.field public final c:LX/0SW;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61104
    sget-object v0, LX/0Qd;->u:LX/0Qf;

    move-object v0, v0

    .line 61105
    invoke-direct {p0, v0}, LX/0SO;-><init>(LX/0Qf;)V

    .line 61106
    return-void
.end method

.method public constructor <init>(LX/0Qf;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Qf",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 61099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61100
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/0SO;->b:Lcom/google/common/util/concurrent/SettableFuture;

    .line 61101
    invoke-static {}, LX/0SW;->createUnstarted()LX/0SW;

    move-result-object v0

    iput-object v0, p0, LX/0SO;->c:LX/0SW;

    .line 61102
    iput-object p1, p0, LX/0SO;->a:LX/0Qf;

    .line 61103
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 61098
    iget-object v0, p0, LX/0SO;->a:LX/0Qf;

    invoke-interface {v0}, LX/0Qf;->a()I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;LX/0R1;)LX/0Qf;
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TV;>;TV;",
            "LX/0R1",
            "<TK;TV;>;)",
            "LX/0Qf",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 61097
    return-object p0
.end method

.method public final a(Ljava/lang/Object;LX/0QM;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "LX/0QM",
            "<-TK;TV;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 61079
    :try_start_0
    iget-object v0, p0, LX/0SO;->c:LX/0SW;

    invoke-virtual {v0}, LX/0SW;->start()LX/0SW;

    .line 61080
    iget-object v0, p0, LX/0SO;->a:LX/0Qf;

    invoke-interface {v0}, LX/0Qf;->get()Ljava/lang/Object;

    move-result-object v0

    .line 61081
    if-nez v0, :cond_2

    .line 61082
    invoke-virtual {p2, p1}, LX/0QM;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 61083
    invoke-virtual {p0, v0}, LX/0SO;->b(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, LX/0SO;->b:Lcom/google/common/util/concurrent/SettableFuture;

    .line 61084
    :cond_0
    :goto_0
    return-object v0

    .line 61085
    :cond_1
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 61086
    :cond_2
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61087
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61088
    invoke-virtual {p2, p1}, LX/0QM;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 61089
    if-nez v0, :cond_3

    .line 61090
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 61091
    :cond_3
    new-instance v1, LX/4wK;

    invoke-direct {v1, p0}, LX/4wK;-><init>(LX/0SO;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 61092
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 61093
    invoke-virtual {p0, v1}, LX/0SO;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/0SO;->b:Lcom/google/common/util/concurrent/SettableFuture;

    .line 61094
    :goto_1
    instance-of v1, v1, Ljava/lang/InterruptedException;

    if-eqz v1, :cond_0

    .line 61095
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 61096
    :cond_4
    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 61067
    if-eqz p1, :cond_0

    .line 61068
    invoke-virtual {p0, p1}, LX/0SO;->b(Ljava/lang/Object;)Z

    .line 61069
    :goto_0
    return-void

    .line 61070
    :cond_0
    sget-object v0, LX/0Qd;->u:LX/0Qf;

    move-object v0, v0

    .line 61071
    iput-object v0, p0, LX/0SO;->a:LX/0Qf;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 61078
    iget-object v0, p0, LX/0SO;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method public final b()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 61107
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)Z"
        }
    .end annotation

    .prologue
    .line 61077
    iget-object v0, p0, LX/0SO;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, 0x5797ff36

    invoke-static {v0, p1, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 61076
    const/4 v0, 0x1

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 61075
    iget-object v0, p0, LX/0SO;->a:LX/0Qf;

    invoke-interface {v0}, LX/0Qf;->d()Z

    move-result v0

    return v0
.end method

.method public final e()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 61074
    iget-object v0, p0, LX/0SO;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-static {v0}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 61073
    iget-object v0, p0, LX/0SO;->c:LX/0SW;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, LX/0SW;->elapsed(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final get()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 61072
    iget-object v0, p0, LX/0SO;->a:LX/0Qf;

    invoke-interface {v0}, LX/0Qf;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
