.class public final enum LX/17O;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/17O;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/17O;

.field public static final enum CORE:LX/17O;

.field public static final enum CORE_AND_SAMPLED:LX/17O;

.field public static final enum NONE:LX/17O;

.field public static final enum UNSET:LX/17O;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 197410
    new-instance v0, LX/17O;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/17O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/17O;->NONE:LX/17O;

    .line 197411
    new-instance v0, LX/17O;

    const-string v1, "CORE"

    invoke-direct {v0, v1, v3}, LX/17O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/17O;->CORE:LX/17O;

    .line 197412
    new-instance v0, LX/17O;

    const-string v1, "CORE_AND_SAMPLED"

    invoke-direct {v0, v1, v4}, LX/17O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/17O;->CORE_AND_SAMPLED:LX/17O;

    .line 197413
    new-instance v0, LX/17O;

    const-string v1, "UNSET"

    invoke-direct {v0, v1, v5}, LX/17O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/17O;->UNSET:LX/17O;

    .line 197414
    const/4 v0, 0x4

    new-array v0, v0, [LX/17O;

    sget-object v1, LX/17O;->NONE:LX/17O;

    aput-object v1, v0, v2

    sget-object v1, LX/17O;->CORE:LX/17O;

    aput-object v1, v0, v3

    sget-object v1, LX/17O;->CORE_AND_SAMPLED:LX/17O;

    aput-object v1, v0, v4

    sget-object v1, LX/17O;->UNSET:LX/17O;

    aput-object v1, v0, v5

    sput-object v0, LX/17O;->$VALUES:[LX/17O;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 197415
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/17O;
    .locals 1

    .prologue
    .line 197416
    const-class v0, LX/17O;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/17O;

    return-object v0
.end method

.method public static values()[LX/17O;
    .locals 1

    .prologue
    .line 197417
    sget-object v0, LX/17O;->$VALUES:[LX/17O;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/17O;

    return-object v0
.end method
