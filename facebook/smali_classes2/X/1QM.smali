.class public LX/1QM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pj;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5KM;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/1QN;

.field private c:LX/5KM;

.field private d:LX/1QO;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Ot;LX/1QN;LX/1QO;Ljava/lang/String;)V
    .locals 0
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/5KM;",
            ">;",
            "LX/1QN;",
            "LX/1QO;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 244508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244509
    iput-object p1, p0, LX/1QM;->a:LX/0Ot;

    .line 244510
    iput-object p2, p0, LX/1QM;->b:LX/1QN;

    .line 244511
    iput-object p3, p0, LX/1QM;->d:LX/1QO;

    .line 244512
    iput-object p4, p0, LX/1QM;->e:Ljava/lang/String;

    .line 244513
    return-void
.end method


# virtual methods
.method public getAnalyticsModule()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "analyticsModule"
    .end annotation

    .prologue
    .line 244514
    iget-object v0, p0, LX/1QM;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getFontFoundry()LX/1QO;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "fontFoundry"
    .end annotation

    .prologue
    .line 244515
    iget-object v0, p0, LX/1QM;->d:LX/1QO;

    return-object v0
.end method

.method public getNavigator()LX/5KM;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "navigator"
    .end annotation

    .prologue
    .line 244516
    iget-object v0, p0, LX/1QM;->c:LX/5KM;

    if-nez v0, :cond_0

    .line 244517
    iget-object v0, p0, LX/1QM;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5KM;

    iput-object v0, p0, LX/1QM;->c:LX/5KM;

    .line 244518
    :cond_0
    iget-object v0, p0, LX/1QM;->c:LX/5KM;

    return-object v0
.end method

.method public getTraitCollection()LX/1QN;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "traitCollection"
    .end annotation

    .prologue
    .line 244519
    iget-object v0, p0, LX/1QM;->b:LX/1QN;

    return-object v0
.end method
