.class public final enum LX/1pL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1pL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1pL;

.field public static final enum UTF16_BE:LX/1pL;

.field public static final enum UTF16_LE:LX/1pL;

.field public static final enum UTF32_BE:LX/1pL;

.field public static final enum UTF32_LE:LX/1pL;

.field public static final enum UTF8:LX/1pL;


# instance fields
.field public final _bigEndian:Z

.field public final _javaName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 328165
    new-instance v0, LX/1pL;

    const-string v1, "UTF8"

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v3, v2, v3}, LX/1pL;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, LX/1pL;->UTF8:LX/1pL;

    .line 328166
    new-instance v0, LX/1pL;

    const-string v1, "UTF16_BE"

    const-string v2, "UTF-16BE"

    invoke-direct {v0, v1, v4, v2, v4}, LX/1pL;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, LX/1pL;->UTF16_BE:LX/1pL;

    .line 328167
    new-instance v0, LX/1pL;

    const-string v1, "UTF16_LE"

    const-string v2, "UTF-16LE"

    invoke-direct {v0, v1, v5, v2, v3}, LX/1pL;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, LX/1pL;->UTF16_LE:LX/1pL;

    .line 328168
    new-instance v0, LX/1pL;

    const-string v1, "UTF32_BE"

    const-string v2, "UTF-32BE"

    invoke-direct {v0, v1, v6, v2, v4}, LX/1pL;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, LX/1pL;->UTF32_BE:LX/1pL;

    .line 328169
    new-instance v0, LX/1pL;

    const-string v1, "UTF32_LE"

    const-string v2, "UTF-32LE"

    invoke-direct {v0, v1, v7, v2, v3}, LX/1pL;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, LX/1pL;->UTF32_LE:LX/1pL;

    .line 328170
    const/4 v0, 0x5

    new-array v0, v0, [LX/1pL;

    sget-object v1, LX/1pL;->UTF8:LX/1pL;

    aput-object v1, v0, v3

    sget-object v1, LX/1pL;->UTF16_BE:LX/1pL;

    aput-object v1, v0, v4

    sget-object v1, LX/1pL;->UTF16_LE:LX/1pL;

    aput-object v1, v0, v5

    sget-object v1, LX/1pL;->UTF32_BE:LX/1pL;

    aput-object v1, v0, v6

    sget-object v1, LX/1pL;->UTF32_LE:LX/1pL;

    aput-object v1, v0, v7

    sput-object v0, LX/1pL;->$VALUES:[LX/1pL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 328161
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 328162
    iput-object p3, p0, LX/1pL;->_javaName:Ljava/lang/String;

    .line 328163
    iput-boolean p4, p0, LX/1pL;->_bigEndian:Z

    .line 328164
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1pL;
    .locals 1

    .prologue
    .line 328157
    const-class v0, LX/1pL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1pL;

    return-object v0
.end method

.method public static values()[LX/1pL;
    .locals 1

    .prologue
    .line 328160
    sget-object v0, LX/1pL;->$VALUES:[LX/1pL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1pL;

    return-object v0
.end method


# virtual methods
.method public final getJavaName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 328159
    iget-object v0, p0, LX/1pL;->_javaName:Ljava/lang/String;

    return-object v0
.end method

.method public final isBigEndian()Z
    .locals 1

    .prologue
    .line 328158
    iget-boolean v0, p0, LX/1pL;->_bigEndian:Z

    return v0
.end method
