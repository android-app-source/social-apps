.class public LX/1K7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:LX/1KI;

.field public final b:LX/1K9;

.field public final c:LX/1KM;

.field public final d:LX/1KA;

.field public final e:LX/1KN;

.field public final f:LX/0ad;

.field public g:I


# direct methods
.method public constructor <init>(LX/1K8;LX/1K9;LX/1KA;LX/1KH;LX/0ad;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 230922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230923
    iput-object p2, p0, LX/1K7;->b:LX/1K9;

    .line 230924
    new-instance v0, LX/1KI;

    invoke-direct {v0}, LX/1KI;-><init>()V

    iput-object v0, p0, LX/1K7;->a:LX/1KI;

    .line 230925
    sget-object v0, LX/1KK;->a:LX/1KK;

    invoke-virtual {p1, v0}, LX/1K8;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1KM;

    iput-object v0, p0, LX/1K7;->c:LX/1KM;

    .line 230926
    iget-object v0, p0, LX/1K7;->c:LX/1KM;

    const/4 v1, 0x0

    .line 230927
    iput v1, v0, LX/1KM;->a:I

    .line 230928
    iput-object p3, p0, LX/1K7;->d:LX/1KA;

    .line 230929
    const v0, 0xa0096

    .line 230930
    new-instance v2, LX/1KN;

    invoke-static {p4}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-class v4, LX/197;

    invoke-interface {p4, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/197;

    invoke-static {p4}, LX/19A;->a(LX/0QB;)LX/19A;

    move-result-object v5

    check-cast v5, LX/19A;

    invoke-static {p4}, LX/1KO;->a(LX/0QB;)LX/1KO;

    move-result-object v6

    check-cast v6, LX/1KO;

    invoke-static {p4}, LX/199;->a(LX/0QB;)LX/199;

    move-result-object v7

    check-cast v7, LX/199;

    move v8, v0

    invoke-direct/range {v2 .. v8}, LX/1KN;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/197;LX/19A;LX/1KO;LX/199;I)V

    .line 230931
    move-object v0, v2

    .line 230932
    iput-object v0, p0, LX/1K7;->e:LX/1KN;

    .line 230933
    iput-object p5, p0, LX/1K7;->f:LX/0ad;

    .line 230934
    return-void
.end method

.method public static a(LX/0QB;)LX/1K7;
    .locals 9

    .prologue
    .line 230911
    const-class v1, LX/1K7;

    monitor-enter v1

    .line 230912
    :try_start_0
    sget-object v0, LX/1K7;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 230913
    sput-object v2, LX/1K7;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 230914
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230915
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 230916
    new-instance v3, LX/1K7;

    invoke-static {v0}, LX/1K8;->a(LX/0QB;)LX/1K8;

    move-result-object v4

    check-cast v4, LX/1K8;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v5

    check-cast v5, LX/1K9;

    invoke-static {v0}, LX/1KA;->a(LX/0QB;)LX/1KA;

    move-result-object v6

    check-cast v6, LX/1KA;

    const-class v7, LX/1KH;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/1KH;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/1K7;-><init>(LX/1K8;LX/1K9;LX/1KA;LX/1KH;LX/0ad;)V

    .line 230917
    move-object v0, v3

    .line 230918
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 230919
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1K7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 230920
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 230921
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
