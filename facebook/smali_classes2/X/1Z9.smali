.class public LX/1Z9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1AN;

.field public final b:Landroid/view/ViewGroup;

.field private final c:LX/0g8;

.field private final d:Landroid/view/View$OnClickListener;

.field public e:LX/1CM;

.field private f:Z

.field public g:Landroid/view/View$OnClickListener;

.field public h:Z

.field public i:LX/0fo;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;LX/0g8;Landroid/view/View$OnClickListener;LX/1AN;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 274385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 274386
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Z9;->e:LX/1CM;

    .line 274387
    iput-boolean v1, p0, LX/1Z9;->f:Z

    .line 274388
    iput-boolean v1, p0, LX/1Z9;->h:Z

    .line 274389
    iput-object p1, p0, LX/1Z9;->b:Landroid/view/ViewGroup;

    .line 274390
    iput-object p2, p0, LX/1Z9;->c:LX/0g8;

    .line 274391
    iput-object p4, p0, LX/1Z9;->a:LX/1AN;

    .line 274392
    iput-object p3, p0, LX/1Z9;->d:Landroid/view/View$OnClickListener;

    .line 274393
    return-void
.end method


# virtual methods
.method public final a(LX/1lq;)V
    .locals 9

    .prologue
    .line 274394
    iget-boolean v0, p0, LX/1Z9;->h:Z

    if-nez v0, :cond_0

    .line 274395
    const/4 v2, -0x2

    .line 274396
    new-instance v0, LX/Am6;

    iget-object v1, p0, LX/1Z9;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Am6;-><init>(Landroid/content/Context;)V

    .line 274397
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 274398
    const/16 v2, 0x51

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 274399
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 274400
    iget-object v1, p0, LX/1Z9;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 274401
    iget-object v1, p0, LX/1Z9;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274402
    move-object v0, v0

    .line 274403
    iget-object v1, p0, LX/1Z9;->e:LX/1CM;

    .line 274404
    check-cast v0, LX/Am6;

    iput-object v0, v1, LX/1CM;->f:LX/Am6;

    .line 274405
    iget-object v2, v1, LX/1CM;->f:LX/Am6;

    new-instance v3, LX/Alz;

    invoke-direct {v3, v1}, LX/Alz;-><init>(LX/1CM;)V

    invoke-virtual {v2, v3}, LX/Am6;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 274406
    invoke-virtual {v1}, LX/1CM;->c()V

    .line 274407
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Z9;->h:Z

    .line 274408
    :cond_0
    sget-object v0, LX/An2;->a:[I

    invoke-virtual {p1}, LX/1lq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 274409
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal loadIntentHint to trigger new storeis pill below "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 274410
    :pswitch_0
    iget-object v0, p0, LX/1Z9;->i:LX/0fo;

    iget-object v1, p0, LX/1Z9;->e:LX/1CM;

    .line 274411
    iget-object v2, v1, LX/1CM;->f:LX/Am6;

    move-object v1, v2

    .line 274412
    invoke-interface {v0, v1}, LX/0fo;->b(Landroid/view/View;)V

    .line 274413
    iget-object v0, p0, LX/1Z9;->e:LX/1CM;

    .line 274414
    invoke-static {v0}, LX/1CM;->i(LX/1CM;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/1CM;->g:LX/Am3;

    if-nez v1, :cond_5

    .line 274415
    :cond_1
    :goto_0
    return-void

    .line 274416
    :pswitch_1
    iget-object v0, p0, LX/1Z9;->e:LX/1CM;

    .line 274417
    invoke-static {v0}, LX/1CM;->k(LX/1CM;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v0}, LX/1CM;->j(LX/1CM;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 274418
    iget-object v3, v0, LX/1CM;->g:LX/Am3;

    sget-object v4, LX/Am3;->ANIMATING_TO_LOADING_TEXT:LX/Am3;

    if-ne v3, v4, :cond_6

    const/4 v3, 0x1

    :goto_1
    move v3, v3

    .line 274419
    if-nez v3, :cond_2

    .line 274420
    iget-object v3, v0, LX/1CM;->g:LX/Am3;

    sget-object v4, LX/Am3;->ANIMATING_TO_SPINNER:LX/Am3;

    if-ne v3, v4, :cond_7

    const/4 v3, 0x1

    :goto_2
    move v3, v3

    .line 274421
    if-eqz v3, :cond_3

    .line 274422
    :cond_2
    sget-object v3, LX/Am3;->ANIMATING_TO_LOADED_TEXT:LX/Am3;

    iput-object v3, v0, LX/1CM;->g:LX/Am3;

    .line 274423
    iget-object v3, v0, LX/1CM;->f:LX/Am6;

    new-instance v4, LX/Am2;

    invoke-direct {v4, v0}, LX/Am2;-><init>(LX/1CM;)V

    const/16 v7, 0x8

    .line 274424
    iget-object v5, v3, LX/Am6;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v5

    if-nez v5, :cond_8

    .line 274425
    iget-object v5, v3, LX/Am6;->l:LX/Am5;

    sget-object v6, LX/Am4;->SPINNER:LX/Am4;

    .line 274426
    iput-object v6, v5, LX/Am5;->b:LX/Am4;

    .line 274427
    :goto_3
    iget-object v5, v3, LX/Am6;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 274428
    iget-object v5, v3, LX/Am6;->e:Landroid/widget/LinearLayout;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 274429
    iget-object v5, v3, LX/Am6;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 274430
    iget-object v5, v3, LX/Am6;->l:LX/Am5;

    .line 274431
    iput-object v4, v5, LX/Am5;->c:LX/Am0;

    .line 274432
    iget-object v5, v3, LX/Am6;->k:LX/0wd;

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v5, v7, v8}, LX/0wd;->b(D)LX/0wd;

    .line 274433
    :cond_3
    goto :goto_0

    .line 274434
    :pswitch_2
    iget-object v0, p0, LX/1Z9;->e:LX/1CM;

    .line 274435
    iget-object v1, v0, LX/1CM;->g:LX/Am3;

    sget-object v2, LX/Am3;->SHOWING_LOADED_TEXT:LX/Am3;

    if-ne v1, v2, :cond_a

    const/4 v1, 0x1

    :goto_4
    move v1, v1

    .line 274436
    if-nez v1, :cond_4

    .line 274437
    iget-object v1, v0, LX/1CM;->g:LX/Am3;

    sget-object v2, LX/Am3;->ANIMATING_TO_LOADED_TEXT:LX/Am3;

    if-ne v1, v2, :cond_b

    const/4 v1, 0x1

    :goto_5
    move v1, v1

    .line 274438
    if-eqz v1, :cond_9

    .line 274439
    :cond_4
    :goto_6
    goto :goto_0

    .line 274440
    :pswitch_3
    iget-object v0, p0, LX/1Z9;->e:LX/1CM;

    invoke-virtual {v0}, LX/1CM;->c()V

    goto :goto_0

    .line 274441
    :cond_5
    sget-object v1, LX/Am3;->SHOWING_SPINNER:LX/Am3;

    iput-object v1, v0, LX/1CM;->g:LX/Am3;

    .line 274442
    iget-object v1, v0, LX/1CM;->f:LX/Am6;

    invoke-virtual {v1}, LX/Am6;->a()V

    .line 274443
    iget-object v1, v0, LX/1CM;->f:LX/Am6;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/Am6;->setVisibility(I)V

    goto/16 :goto_0

    :cond_6
    const/4 v3, 0x0

    goto :goto_1

    :cond_7
    const/4 v3, 0x0

    goto :goto_2

    .line 274444
    :cond_8
    iget-object v5, v3, LX/Am6;->l:LX/Am5;

    sget-object v6, LX/Am4;->LOADING_TEXT:LX/Am4;

    .line 274445
    iput-object v6, v5, LX/Am5;->b:LX/Am4;

    .line 274446
    goto :goto_3

    .line 274447
    :cond_9
    invoke-virtual {v0}, LX/1CM;->c()V

    goto :goto_6

    :cond_a
    const/4 v1, 0x0

    goto :goto_4

    :cond_b
    const/4 v1, 0x0

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Z)Z
    .locals 9

    .prologue
    .line 274448
    iget-boolean v0, p0, LX/1Z9;->f:Z

    if-nez v0, :cond_1

    .line 274449
    iget-object v0, p0, LX/1Z9;->b:Landroid/view/ViewGroup;

    const v1, 0x7f0d0d53

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 274450
    instance-of v1, v0, Landroid/view/ViewStub;

    if-eqz v1, :cond_0

    .line 274451
    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 274452
    :cond_0
    iget-object v1, p0, LX/1Z9;->a:LX/1AN;

    iget-object v2, p0, LX/1Z9;->c:LX/0g8;

    const/4 v4, 0x0

    .line 274453
    iput-object v0, v1, LX/1AN;->k:Landroid/view/View;

    .line 274454
    iput-object v2, v1, LX/1AN;->o:LX/0g8;

    .line 274455
    iget-object v3, v1, LX/1AN;->f:LX/0ad;

    sget-short v5, LX/0fe;->y:S

    invoke-interface {v3, v5, v4}, LX/0ad;->a(SZ)Z

    move-result v3

    iput-boolean v3, v1, LX/1AN;->s:Z

    .line 274456
    iget-boolean v3, v1, LX/1AN;->s:Z

    if-nez v3, :cond_4

    const/4 v3, 0x1

    .line 274457
    :goto_0
    new-instance v5, LX/30u;

    iget-object v6, v1, LX/1AN;->o:LX/0g8;

    invoke-direct {v5, v1, v6, v3}, LX/30u;-><init>(LX/1AN;LX/0g8;Z)V

    iput-object v5, v1, LX/1AN;->p:LX/30v;

    .line 274458
    iget-object v3, v1, LX/1AN;->j:LX/1AP;

    iget-object v5, v1, LX/1AN;->p:LX/30v;

    invoke-virtual {v3, v5}, LX/1AP;->b(LX/0fx;)LX/1Ks;

    move-result-object v3

    iput-object v3, v1, LX/1AN;->q:LX/0fx;

    .line 274459
    iget-object v3, v1, LX/1AN;->q:LX/0fx;

    invoke-interface {v2, v3}, LX/0g8;->b(LX/0fx;)V

    .line 274460
    new-instance v3, LX/30w;

    invoke-direct {v3, v1}, LX/30w;-><init>(LX/1AN;)V

    iput-object v3, v1, LX/1AN;->m:LX/30w;

    .line 274461
    iget-object v3, v1, LX/1AN;->b:LX/0wW;

    invoke-virtual {v3}, LX/0wW;->a()LX/0wd;

    move-result-object v3

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v5, v6}, LX/0wd;->a(D)LX/0wd;

    move-result-object v3

    iget-object v4, v1, LX/1AN;->m:LX/30w;

    invoke-virtual {v3, v4}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v3

    sget-object v4, LX/1AN;->a:LX/0wT;

    invoke-virtual {v3, v4}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v3

    iput-object v3, v1, LX/1AN;->l:LX/0wd;

    .line 274462
    iget-object v3, v1, LX/1AN;->k:Landroid/view/View;

    new-instance v4, LX/30x;

    invoke-direct {v4, v1}, LX/30x;-><init>(LX/1AN;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 274463
    sget-object v3, LX/1AO;->HIDDEN:LX/1AO;

    iput-object v3, v1, LX/1AN;->n:LX/1AO;

    .line 274464
    iget-object v1, p0, LX/1Z9;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274465
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Z9;->f:Z

    .line 274466
    :cond_1
    if-eqz p1, :cond_3

    .line 274467
    iget-object v0, p0, LX/1Z9;->i:LX/0fo;

    iget-object v1, p0, LX/1Z9;->a:LX/1AN;

    .line 274468
    iget-object v2, v1, LX/1AN;->k:Landroid/view/View;

    move-object v1, v2

    .line 274469
    invoke-interface {v0, v1}, LX/0fo;->b(Landroid/view/View;)V

    .line 274470
    iget-object v0, p0, LX/1Z9;->a:LX/1AN;

    .line 274471
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/1AN;->r:Z

    .line 274472
    invoke-static {v0}, LX/1AN;->h$redex0(LX/1AN;)Z

    move-result v3

    .line 274473
    if-eqz v3, :cond_2

    .line 274474
    iget-object v4, v0, LX/1AN;->e:Landroid/os/Handler;

    iget-object v5, v0, LX/1AN;->i:Ljava/lang/Runnable;

    const-wide/16 v7, 0xc8

    const v6, -0x6f81407e

    invoke-static {v4, v5, v7, v8, v6}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 274475
    :cond_2
    move v0, v3

    .line 274476
    :goto_1
    return v0

    :cond_3
    iget-object v0, p0, LX/1Z9;->a:LX/1AN;

    invoke-virtual {v0}, LX/1AN;->d()Z

    move-result v0

    goto :goto_1

    :cond_4
    move v3, v4

    .line 274477
    goto :goto_0
.end method
