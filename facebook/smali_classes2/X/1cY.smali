.class public abstract LX/1cY;
.super LX/1cZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/1cZ",
        "<TT;>;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/1cV;

.field private final b:LX/1BU;


# direct methods
.method public constructor <init>(LX/1cF;LX/1cV;LX/1BU;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cF",
            "<TT;>;",
            "LX/1cV;",
            "LX/1BU;",
            ")V"
        }
    .end annotation

    .prologue
    .line 282203
    invoke-direct {p0}, LX/1cZ;-><init>()V

    .line 282204
    iput-object p2, p0, LX/1cY;->a:LX/1cV;

    .line 282205
    iput-object p3, p0, LX/1cY;->b:LX/1BU;

    .line 282206
    iget-object v0, p0, LX/1cY;->b:LX/1BU;

    .line 282207
    iget-object v1, p2, LX/1cW;->a:LX/1bf;

    move-object v1, v1

    .line 282208
    iget-object v2, p0, LX/1cY;->a:LX/1cV;

    .line 282209
    iget-object v3, v2, LX/1cW;->d:Ljava/lang/Object;

    move-object v2, v3

    .line 282210
    iget-object v3, p0, LX/1cY;->a:LX/1cV;

    .line 282211
    iget-object v4, v3, LX/1cW;->b:Ljava/lang/String;

    move-object v3, v4

    .line 282212
    iget-object v4, p0, LX/1cY;->a:LX/1cV;

    invoke-virtual {v4}, LX/1cW;->f()Z

    move-result v4

    invoke-interface {v0, v1, v2, v3, v4}, LX/1BU;->a(LX/1bf;Ljava/lang/Object;Ljava/lang/String;Z)V

    .line 282213
    new-instance v0, LX/1cc;

    invoke-direct {v0, p0}, LX/1cc;-><init>(LX/1cY;)V

    move-object v0, v0

    .line 282214
    invoke-interface {p1, v0, p2}, LX/1cF;->a(LX/1cd;LX/1cW;)V

    .line 282215
    return-void
.end method

.method public static b(LX/1cY;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 282216
    invoke-super {p0, p1}, LX/1cZ;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282217
    iget-object v0, p0, LX/1cY;->b:LX/1BU;

    iget-object v1, p0, LX/1cY;->a:LX/1cV;

    .line 282218
    iget-object v2, v1, LX/1cW;->a:LX/1bf;

    move-object v1, v2

    .line 282219
    iget-object v2, p0, LX/1cY;->a:LX/1cV;

    .line 282220
    iget-object v3, v2, LX/1cW;->b:Ljava/lang/String;

    move-object v2, v3

    .line 282221
    iget-object v3, p0, LX/1cY;->a:LX/1cV;

    invoke-virtual {v3}, LX/1cW;->f()Z

    move-result v3

    invoke-interface {v0, v1, v2, p1, v3}, LX/1BU;->a(LX/1bf;Ljava/lang/String;Ljava/lang/Throwable;Z)V

    .line 282222
    :cond_0
    return-void
.end method

.method public static declared-synchronized i(LX/1cY;)V
    .locals 1

    .prologue
    .line 282200
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/1cZ;->a()Z

    move-result v0

    invoke-static {v0}, LX/03g;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 282201
    monitor-exit p0

    return-void

    .line 282202
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public b(Ljava/lang/Object;Z)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation

    .prologue
    .line 282183
    invoke-super {p0, p1, p2}, LX/1cZ;->a(Ljava/lang/Object;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282184
    if-eqz p2, :cond_0

    .line 282185
    iget-object v0, p0, LX/1cY;->b:LX/1BU;

    iget-object v1, p0, LX/1cY;->a:LX/1cV;

    .line 282186
    iget-object v2, v1, LX/1cW;->a:LX/1bf;

    move-object v1, v2

    .line 282187
    iget-object v2, p0, LX/1cY;->a:LX/1cV;

    .line 282188
    iget-object v3, v2, LX/1cW;->b:Ljava/lang/String;

    move-object v2, v3

    .line 282189
    iget-object v3, p0, LX/1cY;->a:LX/1cV;

    invoke-virtual {v3}, LX/1cW;->f()Z

    move-result v3

    invoke-interface {v0, v1, v2, v3}, LX/1BU;->a(LX/1bf;Ljava/lang/String;Z)V

    .line 282190
    :cond_0
    return-void
.end method

.method public final g()Z
    .locals 3

    .prologue
    .line 282191
    invoke-super {p0}, LX/1cZ;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 282192
    const/4 v0, 0x0

    .line 282193
    :goto_0
    return v0

    .line 282194
    :cond_0
    invoke-super {p0}, LX/1cZ;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 282195
    iget-object v0, p0, LX/1cY;->b:LX/1BU;

    iget-object v1, p0, LX/1cY;->a:LX/1cV;

    .line 282196
    iget-object v2, v1, LX/1cW;->b:Ljava/lang/String;

    move-object v1, v2

    .line 282197
    invoke-interface {v0, v1}, LX/1BU;->a(Ljava/lang/String;)V

    .line 282198
    iget-object v0, p0, LX/1cY;->a:LX/1cV;

    invoke-virtual {v0}, LX/1cW;->i()V

    .line 282199
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
