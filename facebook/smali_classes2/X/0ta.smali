.class public final enum LX/0ta;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0ta;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0ta;

.field public static final enum FROM_CACHE_HAD_SERVER_ERROR:LX/0ta;

.field public static final enum FROM_CACHE_INCOMPLETE:LX/0ta;

.field public static final enum FROM_CACHE_STALE:LX/0ta;

.field public static final enum FROM_CACHE_UP_TO_DATE:LX/0ta;

.field public static final enum FROM_DB_NEED_INITIAL_FETCH:LX/0ta;

.field public static final enum FROM_SERVER:LX/0ta;

.field public static final enum NO_DATA:LX/0ta;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 155011
    new-instance v0, LX/0ta;

    const-string v1, "FROM_SERVER"

    invoke-direct {v0, v1, v3}, LX/0ta;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ta;->FROM_SERVER:LX/0ta;

    .line 155012
    new-instance v0, LX/0ta;

    const-string v1, "FROM_CACHE_UP_TO_DATE"

    invoke-direct {v0, v1, v4}, LX/0ta;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    .line 155013
    new-instance v0, LX/0ta;

    const-string v1, "FROM_CACHE_STALE"

    invoke-direct {v0, v1, v5}, LX/0ta;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    .line 155014
    new-instance v0, LX/0ta;

    const-string v1, "NO_DATA"

    invoke-direct {v0, v1, v6}, LX/0ta;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ta;->NO_DATA:LX/0ta;

    .line 155015
    new-instance v0, LX/0ta;

    const-string v1, "FROM_CACHE_INCOMPLETE"

    invoke-direct {v0, v1, v7}, LX/0ta;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ta;->FROM_CACHE_INCOMPLETE:LX/0ta;

    .line 155016
    new-instance v0, LX/0ta;

    const-string v1, "FROM_CACHE_HAD_SERVER_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0ta;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ta;->FROM_CACHE_HAD_SERVER_ERROR:LX/0ta;

    .line 155017
    new-instance v0, LX/0ta;

    const-string v1, "FROM_DB_NEED_INITIAL_FETCH"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/0ta;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ta;->FROM_DB_NEED_INITIAL_FETCH:LX/0ta;

    .line 155018
    const/4 v0, 0x7

    new-array v0, v0, [LX/0ta;

    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    aput-object v1, v0, v3

    sget-object v1, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    aput-object v1, v0, v4

    sget-object v1, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    aput-object v1, v0, v5

    sget-object v1, LX/0ta;->NO_DATA:LX/0ta;

    aput-object v1, v0, v6

    sget-object v1, LX/0ta;->FROM_CACHE_INCOMPLETE:LX/0ta;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0ta;->FROM_CACHE_HAD_SERVER_ERROR:LX/0ta;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0ta;->FROM_DB_NEED_INITIAL_FETCH:LX/0ta;

    aput-object v2, v0, v1

    sput-object v0, LX/0ta;->$VALUES:[LX/0ta;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 155019
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0ta;
    .locals 1

    .prologue
    .line 155020
    const-class v0, LX/0ta;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0ta;

    return-object v0
.end method

.method public static values()[LX/0ta;
    .locals 1

    .prologue
    .line 155021
    sget-object v0, LX/0ta;->$VALUES:[LX/0ta;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0ta;

    return-object v0
.end method
