.class public LX/1lV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2vA;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/32f;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 311966
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311967
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 311968
    iput-object v0, p0, LX/1lV;->a:LX/0Ot;

    .line 311969
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 311970
    iput-object v0, p0, LX/1lV;->b:LX/0Ot;

    .line 311971
    return-void
.end method

.method public static b(LX/0QB;)LX/1lV;
    .locals 4

    .prologue
    .line 311972
    new-instance v1, LX/1lV;

    invoke-direct {v1}, LX/1lV;-><init>()V

    .line 311973
    const/16 v0, 0xfd9

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v0, 0xfd2

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    .line 311974
    iput-object v2, v1, LX/1lV;->a:LX/0Ot;

    iput-object v3, v1, LX/1lV;->b:LX/0Ot;

    iput-object v0, v1, LX/1lV;->c:LX/0Uh;

    .line 311975
    return-object v1
.end method


# virtual methods
.method public final a(LX/0Px;LX/1RN;)LX/0Px;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/1RN;",
            ">;",
            "LX/1RN;",
            ")",
            "LX/0Px",
            "<",
            "LX/1RN;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 311976
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 311977
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_4

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RN;

    .line 311978
    if-eq p2, v0, :cond_3

    iget-object v5, v0, LX/1RN;->a:LX/1kK;

    invoke-interface {v5}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, LX/1RN;->a:LX/1kK;

    invoke-interface {v6}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, LX/1RN;->b:LX/1lP;

    iget-object v7, v7, LX/1lP;->e:Ljava/lang/String;

    .line 311979
    iget-object v8, p0, LX/1lV;->b:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/32f;

    .line 311980
    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 311981
    const/4 v9, 0x0

    .line 311982
    :goto_1
    move v8, v9

    .line 311983
    if-nez v8, :cond_2

    iget-object v8, p0, LX/1lV;->a:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/2vA;

    invoke-virtual {v8, v5}, LX/2vA;->d(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2

    const/4 v8, 0x0

    .line 311984
    if-eqz v7, :cond_1

    iget-object v9, p0, LX/1lV;->c:LX/0Uh;

    sget v10, LX/1S0;->c:I

    invoke-virtual {v9, v10, v8}, LX/0Uh;->a(IZ)Z

    move-result v9

    if-eqz v9, :cond_1

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->LOW:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->VERY_LOW:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    :cond_0
    const/4 v8, 0x1

    :cond_1
    move v8, v8

    .line 311985
    if-eqz v8, :cond_5

    :cond_2
    const/4 v8, 0x1

    :goto_2
    move v5, v8

    .line 311986
    if-eqz v5, :cond_3

    .line 311987
    new-instance v5, LX/32d;

    invoke-direct {v5, v0}, LX/32d;-><init>(LX/1RN;)V

    sget-object v0, LX/24P;->MINIMIZED:LX/24P;

    .line 311988
    new-instance v6, LX/32e;

    invoke-direct {v6, v0, v2}, LX/32e;-><init>(LX/24P;Z)V

    move-object v0, v6

    .line 311989
    invoke-virtual {v5, v0}, LX/32d;->a(LX/32e;)LX/32d;

    move-result-object v0

    invoke-virtual {v0}, LX/32d;->a()LX/1RN;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 311990
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 311991
    :cond_3
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 311992
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_5
    const/4 v8, 0x0

    goto :goto_2

    .line 311993
    :cond_6
    iget-object v9, v8, LX/32f;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v10, LX/1kp;->s:LX/0Tn;

    const/4 v11, 0x0

    invoke-interface {v9, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 311994
    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    goto :goto_1
.end method
