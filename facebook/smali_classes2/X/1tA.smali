.class public LX/1tA;
.super LX/056;
.source ""


# static fields
.field private static final r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public A:I

.field private final s:Ljava/lang/Object;

.field public t:LX/1tC;

.field public u:LX/1tU;

.field public v:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public w:LX/1th;

.field public x:LX/1tj;

.field private final y:Ljava/lang/Object;

.field private z:LX/0UE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0UE",
            "<",
            "LX/0AF;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mStickySubscriptionsLock"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 335992
    new-instance v0, LX/1tB;

    invoke-direct {v0}, LX/1tB;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, LX/1tA;->r:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 336010
    invoke-direct {p0}, LX/056;-><init>()V

    .line 336011
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/1tA;->s:Ljava/lang/Object;

    .line 336012
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/1tA;->y:Ljava/lang/Object;

    .line 336013
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, LX/1tA;->z:LX/0UE;

    return-void
.end method

.method private a(LX/072;Ljava/lang/String;[BLX/0AL;LX/0AM;IJLjava/lang/String;)LX/6mp;
    .locals 11
    .param p5    # LX/0AM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 336001
    sget-object v0, LX/1tA;->r:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 336002
    sget-object v0, LX/1tA;->r:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 336003
    const-string v1, "/t_sm"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 336004
    iget v0, p0, LX/1tA;->A:I

    move v9, v0

    .line 336005
    :goto_0
    if-lez v9, :cond_1

    .line 336006
    iget-object v0, p0, LX/1tA;->w:LX/1th;

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object/from16 v4, p5

    move/from16 v5, p6

    move-wide/from16 v6, p7

    move-object/from16 v8, p9

    invoke-virtual/range {v0 .. v8}, LX/1th;->a(Ljava/lang/String;[BLX/0AL;LX/0AM;IJLjava/lang/String;)LX/6mp;

    move-result-object v0

    .line 336007
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/072;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 336008
    invoke-static {p0, p1, v0, v9}, LX/1tA;->a(LX/1tA;LX/072;LX/6mp;I)Z

    .line 336009
    :cond_0
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v9, v0

    goto :goto_0
.end method

.method private a(Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 335993
    const-string v0, "MqttConnectionManager"

    const-string v1, "send/publish/t_fs; appState=%s, keepaliveSec=%s subscribe %s unsubscribe %s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    aput-object p3, v2, v3

    const/4 v3, 0x3

    aput-object p4, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 335994
    iget-object v1, p0, LX/056;->e:Ljava/util/Map;

    monitor-enter v1

    .line 335995
    :try_start_0
    invoke-virtual {p0, p3, p4}, LX/056;->b(Ljava/util/List;Ljava/util/List;)Landroid/util/Pair;

    move-result-object v0

    .line 335996
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    if-nez v0, :cond_0

    .line 335997
    monitor-exit v1

    .line 335998
    :goto_0
    return-void

    .line 335999
    :cond_0
    iget-object v2, p0, LX/056;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/facebook/mqttlite/MqttConnectionManager$2;

    invoke-direct {v3, p0, v0, p1, p2}, Lcom/facebook/mqttlite/MqttConnectionManager$2;-><init>(LX/1tA;Landroid/util/Pair;Ljava/lang/Boolean;Ljava/lang/Integer;)V

    const v0, -0x3277ba3d    # -2.8578416E8f

    invoke-static {v2, v3, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 336000
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static a(LX/1tA;LX/072;LX/6mp;I)Z
    .locals 11

    .prologue
    .line 335974
    const-string v0, "MqttConnectionManager"

    const-string v1, "send/queued_message; id=%d retry=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p2, LX/6mp;->g:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p2}, LX/6mp;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 335975
    new-instance v0, LX/6mq;

    sget-object v2, LX/07S;->PUBACK:LX/07S;

    invoke-virtual {p1}, LX/072;->m()I

    move-result v3

    iget-object v1, p0, LX/056;->g:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/6mq;-><init>(LX/072;LX/07S;IJ)V

    .line 335976
    iget-object v1, p2, LX/6mp;->c:LX/0AL;

    sget-object v2, LX/0AL;->ACKNOWLEDGED_DELIVERY:LX/0AL;

    if-ne v1, v2, :cond_0

    .line 335977
    iget-object v1, p0, LX/056;->k:LX/06J;

    iget v2, p2, LX/6mp;->e:I

    new-instance v3, Lcom/facebook/mqttlite/MqttConnectionManager$3;

    invoke-direct {v3, p0, v0, p2}, Lcom/facebook/mqttlite/MqttConnectionManager$3;-><init>(LX/1tA;LX/6mq;LX/6mp;)V

    invoke-virtual {v1, v0, v2, v3}, LX/06J;->a(LX/0B1;ILjava/lang/Runnable;)Z

    move-result v1

    .line 335978
    if-nez v1, :cond_0

    .line 335979
    const/4 v0, 0x1

    .line 335980
    :goto_0
    return v0

    .line 335981
    :cond_0
    if-lez p3, :cond_1

    .line 335982
    iget-object v1, p0, LX/1tA;->w:LX/1th;

    new-instance v2, Lcom/facebook/mqttlite/MqttConnectionManager$4;

    invoke-direct {v2, p0, p2, v0}, Lcom/facebook/mqttlite/MqttConnectionManager$4;-><init>(LX/1tA;LX/6mp;LX/6mq;)V

    invoke-virtual {v1, v0, p3, v2}, LX/1th;->a(LX/6mq;ILjava/lang/Runnable;)V

    .line 335983
    :cond_1
    :try_start_0
    iget-object v3, p2, LX/6mp;->a:Ljava/lang/String;

    iget-object v4, p2, LX/6mp;->b:[B

    iget-object v5, p2, LX/6mp;->c:LX/0AL;

    iget v6, v0, LX/0B1;->c:I

    iget-object v7, p2, LX/6mp;->d:LX/0AM;

    iget-wide v8, p2, LX/6mp;->f:J

    iget-object v10, p2, LX/6mp;->h:Ljava/lang/String;

    move-object v2, p1

    invoke-virtual/range {v2 .. v10}, LX/072;->a(Ljava/lang/String;[BLX/0AL;ILX/0AM;JLjava/lang/String;)I
    :try_end_0
    .catch LX/0BK; {:try_start_0 .. :try_end_0} :catch_0

    .line 335984
    iget-object v1, p2, LX/6mp;->c:LX/0AL;

    sget-object v2, LX/0AL;->ACKNOWLEDGED_DELIVERY:LX/0AL;

    if-eq v1, v2, :cond_2

    .line 335985
    invoke-virtual {v0}, LX/0B1;->b()V

    .line 335986
    iget-object v0, p0, LX/1tA;->w:LX/1th;

    iget v1, p2, LX/6mp;->g:I

    invoke-virtual {v0, v1}, LX/1th;->a(I)V

    .line 335987
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 335988
    :catch_0
    move-exception v0

    .line 335989
    const-string v1, "MqttConnectionManager"

    const-string v2, "exception/publish"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 335990
    sget-object v0, LX/0AX;->SEND_FAILURE:LX/0AX;

    sget-object v1, LX/0BA;->CONNECTION_LOST:LX/0BA;

    invoke-virtual {p0, p1, v0, v1}, LX/056;->a(LX/072;LX/0AX;LX/0BA;)Ljava/util/concurrent/Future;

    .line 335991
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;[BLX/0AL;LX/0AM;IJLjava/lang/String;)LX/05B;
    .locals 12
    .param p4    # LX/0AM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[B",
            "LX/0AL;",
            "LX/0AM;",
            "IJ",
            "Ljava/lang/String;",
            ")",
            "LX/05B",
            "<",
            "LX/0B2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 335962
    const-string v0, "/send_message2"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/t_sm"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 335963
    :cond_0
    iget-object v0, p0, LX/056;->j:LX/05i;

    const-class v1, LX/0BF;

    invoke-virtual {v0, v1}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/0BF;

    sget-object v1, LX/0BG;->MessageSendAttempt:LX/0BG;

    invoke-virtual {v0, v1}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 335964
    :cond_1
    iget-object v2, p0, LX/056;->b:LX/072;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p5

    move-wide/from16 v8, p6

    move-object/from16 v10, p8

    .line 335965
    invoke-direct/range {v1 .. v10}, LX/1tA;->a(LX/072;Ljava/lang/String;[BLX/0AL;LX/0AM;IJLjava/lang/String;)LX/6mp;

    move-result-object v0

    .line 335966
    if-eqz v0, :cond_2

    .line 335967
    invoke-static {v0}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v0

    .line 335968
    :goto_0
    return-object v0

    .line 335969
    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {v2}, LX/072;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 335970
    :cond_3
    const-string v0, "/t_sm"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/1tA;->v:LX/05N;

    invoke-interface {v0}, LX/05N;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 335971
    iget-object v0, p0, LX/1tA;->w:LX/1th;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-wide/from16 v6, p6

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, LX/1th;->a(Ljava/lang/String;[BLX/0AL;LX/0AM;IJLjava/lang/String;)LX/6mp;

    move-result-object v0

    .line 335972
    invoke-static {v0}, LX/05B;->a(Ljava/lang/Object;)LX/05B;

    move-result-object v0

    goto :goto_0

    .line 335973
    :cond_4
    invoke-super/range {p0 .. p8}, LX/056;->a(Ljava/lang/String;[BLX/0AL;LX/0AM;IJLjava/lang/String;)LX/05B;

    move-result-object v0

    goto :goto_0
.end method

.method public final declared-synchronized a(LX/072;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/072;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/0Hx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 336014
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 336015
    iget-object v0, p0, LX/1tA;->w:LX/1th;

    invoke-virtual {v0}, LX/1th;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6mp;

    .line 336016
    const-string v3, "/t_sm"

    iget-object v4, v0, LX/6mp;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 336017
    invoke-virtual {v0}, LX/6mp;->b()V

    .line 336018
    new-instance v3, LX/0Hx;

    iget-object v4, v0, LX/6mp;->a:Ljava/lang/String;

    invoke-virtual {p1}, LX/072;->m()I

    move-result v5

    iget-object v6, v0, LX/6mp;->b:[B

    iget v0, v0, LX/6mp;->g:I

    invoke-direct {v3, v4, v5, v6, v0}, LX/0Hx;-><init>(Ljava/lang/String;I[BI)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 336019
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 336020
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/1tA;->x:LX/1tj;

    invoke-virtual {v0}, LX/1tj;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6mp;

    .line 336021
    new-instance v3, LX/0Hx;

    iget-object v4, v0, LX/6mp;->a:Ljava/lang/String;

    invoke-virtual {p1}, LX/072;->m()I

    move-result v5

    iget-object v6, v0, LX/6mp;->b:[B

    iget v0, v0, LX/6mp;->g:I

    invoke-direct {v3, v4, v5, v6, v0}, LX/0Hx;-><init>(Ljava/lang/String;I[BI)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 336022
    :cond_2
    monitor-exit p0

    return-object v1
.end method

.method public final a(ZLjava/util/List;Ljava/util/List;)V
    .locals 4
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 335953
    iget-object v2, p0, LX/1tA;->s:Ljava/lang/Object;

    monitor-enter v2

    .line 335954
    :try_start_0
    iget-object v3, p0, LX/056;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    if-nez p1, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v1, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v3

    .line 335955
    if-eqz v3, :cond_0

    .line 335956
    invoke-virtual {p0}, LX/056;->j()V

    .line 335957
    :cond_0
    if-eqz v3, :cond_3

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :goto_1
    if-eqz v3, :cond_1

    invoke-virtual {p0}, LX/056;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :cond_1
    invoke-direct {p0, v1, v0, p2, p3}, LX/1tA;->a(Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/util/List;Ljava/util/List;)V

    .line 335958
    monitor-exit v2

    return-void

    .line 335959
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    move-object v1, v0

    .line 335960
    goto :goto_1

    .line 335961
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c(Ljava/util/List;)V
    .locals 10
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 335943
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 335944
    :cond_0
    :goto_0
    return-void

    .line 335945
    :cond_1
    const/4 v9, 0x0

    .line 335946
    :try_start_0
    invoke-static {p1}, LX/1tU;->b(Ljava/util/List;)[B

    move-result-object v2

    .line 335947
    if-eqz v2, :cond_2

    .line 335948
    const-string v1, "/subscribe"

    sget-object v3, LX/0AL;->ACKNOWLEDGED_DELIVERY:LX/0AL;

    const/4 v4, 0x0

    invoke-virtual {p0}, LX/056;->e()I

    move-result v5

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, LX/1tA;->a(Ljava/lang/String;[BLX/0AL;LX/0AM;IJLjava/lang/String;)LX/05B;

    move-result-object v0

    .line 335949
    invoke-virtual {v0}, LX/05B;->a()Z
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/0BK; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_1
    move v9, v0

    .line 335950
    :goto_2
    if-nez v9, :cond_0

    .line 335951
    invoke-super {p0, p1}, LX/056;->c(Ljava/util/List;)V

    goto :goto_0

    :catch_0
    goto :goto_2

    .line 335952
    :catch_1
    goto :goto_2

    :cond_2
    move v0, v9

    goto :goto_1
.end method

.method public final d(Ljava/util/List;)V
    .locals 10
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 335896
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 335897
    :cond_0
    :goto_0
    return-void

    .line 335898
    :cond_1
    const/4 v9, 0x0

    .line 335899
    :try_start_0
    invoke-static {p1}, LX/1tU;->c(Ljava/util/List;)[B

    move-result-object v2

    .line 335900
    const-string v1, "/unsubscribe"

    sget-object v3, LX/0AL;->ACKNOWLEDGED_DELIVERY:LX/0AL;

    const/4 v4, 0x0

    invoke-virtual {p0}, LX/056;->e()I

    move-result v5

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, LX/1tA;->a(Ljava/lang/String;[BLX/0AL;LX/0AM;IJLjava/lang/String;)LX/05B;

    move-result-object v0

    .line 335901
    invoke-virtual {v0}, LX/05B;->a()Z
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/0BK; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 335902
    :goto_1
    if-nez v0, :cond_0

    .line 335903
    invoke-super {p0, p1}, LX/056;->d(Ljava/util/List;)V

    goto :goto_0

    .line 335904
    :catch_0
    move v0, v9

    goto :goto_1

    :catch_1
    move v0, v9

    goto :goto_1
.end method

.method public final e(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0Hx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 335929
    iget-object v1, p0, LX/056;->b:LX/072;

    .line 335930
    if-nez v1, :cond_1

    .line 335931
    :cond_0
    :goto_0
    return-void

    .line 335932
    :cond_1
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 335933
    if-eqz p1, :cond_2

    .line 335934
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Hx;

    .line 335935
    iget v0, v0, LX/0Hx;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 335936
    :cond_2
    iget-object v0, p0, LX/1tA;->w:LX/1th;

    invoke-virtual {v0}, LX/1th;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6mp;

    .line 335937
    invoke-virtual {v0}, LX/6mp;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 335938
    iget-object v4, p0, LX/056;->d:LX/05h;

    iget v5, v0, LX/6mp;->g:I

    iget-object v6, v0, LX/6mp;->a:Ljava/lang/String;

    iget-object v7, p0, LX/056;->g:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v7}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v8

    iget-wide v10, v0, LX/6mp;->f:J

    sub-long/2addr v8, v10

    .line 335939
    const/4 v7, 0x6

    new-array v7, v7, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "msg_id"

    aput-object v11, v7, v10

    const/4 v10, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v7, v10

    const/4 v10, 0x2

    const-string v11, "operation"

    aput-object v11, v7, v10

    const/4 v10, 0x3

    aput-object v6, v7, v10

    const/4 v10, 0x4

    const-string v11, "timespan_ms"

    aput-object v11, v7, v10

    const/4 v10, 0x5

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v7, v10

    invoke-static {v7}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v7

    .line 335940
    const-string v10, "mqtt_queue_peek"

    invoke-virtual {v4, v10, v7}, LX/05h;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 335941
    invoke-virtual {v0}, LX/6mp;->b()V

    .line 335942
    const/4 v4, 0x0

    invoke-static {p0, v1, v0, v4}, LX/1tA;->a(LX/1tA;LX/072;LX/6mp;I)Z

    move-result v0

    if-nez v0, :cond_3

    goto/16 :goto_0
.end method

.method public final f(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0Hx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 335926
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Hx;

    .line 335927
    iget-object v2, p0, LX/1tA;->w:LX/1th;

    iget v0, v0, LX/0Hx;->d:I

    invoke-virtual {v2, v0}, LX/1th;->a(I)V

    goto :goto_0

    .line 335928
    :cond_0
    return-void
.end method

.method public final g(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 335905
    new-instance v4, LX/0UE;

    invoke-direct {v4, p1}, LX/0UE;-><init>(Ljava/util/Collection;)V

    .line 335906
    iget-object v5, p0, LX/1tA;->y:Ljava/lang/Object;

    monitor-enter v5

    .line 335907
    :try_start_0
    invoke-virtual {v4}, LX/0UE;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0AF;

    .line 335908
    iget-object v6, p0, LX/1tA;->z:LX/0UE;

    invoke-virtual {v6, v0}, LX/0UE;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 335909
    if-nez v1, :cond_0

    .line 335910
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 335911
    :cond_0
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    move-object v0, v1

    move-object v1, v0

    .line 335912
    goto :goto_0

    .line 335913
    :cond_2
    const/4 v0, 0x0

    iget-object v3, p0, LX/1tA;->z:LX/0UE;

    invoke-virtual {v3}, LX/0UE;->size()I

    move-result v6

    move v3, v0

    :goto_1
    if-ge v3, v6, :cond_5

    .line 335914
    iget-object v0, p0, LX/1tA;->z:LX/0UE;

    invoke-virtual {v0, v3}, LX/0UE;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0AF;

    .line 335915
    invoke-virtual {v4, v0}, LX/0UE;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 335916
    if-nez v2, :cond_3

    .line 335917
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 335918
    :cond_3
    iget-object v0, v0, LX/0AF;->a:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    move-object v0, v2

    .line 335919
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v0

    goto :goto_1

    .line 335920
    :cond_5
    iput-object v4, p0, LX/1tA;->z:LX/0UE;

    .line 335921
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335922
    if-nez v1, :cond_6

    if-eqz v2, :cond_7

    .line 335923
    :cond_6
    invoke-virtual {p0, v1, v2}, LX/056;->a(Ljava/util/List;Ljava/util/List;)V

    .line 335924
    :cond_7
    return-void

    .line 335925
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
