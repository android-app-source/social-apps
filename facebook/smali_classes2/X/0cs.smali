.class public abstract enum LX/0cs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0cs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0cs;

.field public static final enum STRONG:LX/0cs;

.field public static final enum STRONG_EVICTABLE:LX/0cs;

.field public static final enum STRONG_EXPIRABLE:LX/0cs;

.field public static final enum STRONG_EXPIRABLE_EVICTABLE:LX/0cs;

.field public static final enum WEAK:LX/0cs;

.field public static final enum WEAK_EVICTABLE:LX/0cs;

.field public static final enum WEAK_EXPIRABLE:LX/0cs;

.field public static final enum WEAK_EXPIRABLE_EVICTABLE:LX/0cs;

.field public static final factories:[[LX/0cs;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 89446
    new-instance v0, LX/0ct;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v3}, LX/0ct;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0cs;->STRONG:LX/0cs;

    .line 89447
    new-instance v0, LX/0cu;

    const-string v1, "STRONG_EXPIRABLE"

    invoke-direct {v0, v1, v4}, LX/0cu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0cs;->STRONG_EXPIRABLE:LX/0cs;

    .line 89448
    new-instance v0, LX/0cv;

    const-string v1, "STRONG_EVICTABLE"

    invoke-direct {v0, v1, v5}, LX/0cv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0cs;->STRONG_EVICTABLE:LX/0cs;

    .line 89449
    new-instance v0, LX/0cw;

    const-string v1, "STRONG_EXPIRABLE_EVICTABLE"

    invoke-direct {v0, v1, v6}, LX/0cw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0cs;->STRONG_EXPIRABLE_EVICTABLE:LX/0cs;

    .line 89450
    new-instance v0, LX/0cx;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v7}, LX/0cx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0cs;->WEAK:LX/0cs;

    .line 89451
    new-instance v0, LX/0cy;

    const-string v1, "WEAK_EXPIRABLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0cy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0cs;->WEAK_EXPIRABLE:LX/0cs;

    .line 89452
    new-instance v0, LX/0cz;

    const-string v1, "WEAK_EVICTABLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/0cz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0cs;->WEAK_EVICTABLE:LX/0cs;

    .line 89453
    new-instance v0, LX/0d0;

    const-string v1, "WEAK_EXPIRABLE_EVICTABLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/0d0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0cs;->WEAK_EXPIRABLE_EVICTABLE:LX/0cs;

    .line 89454
    const/16 v0, 0x8

    new-array v0, v0, [LX/0cs;

    sget-object v1, LX/0cs;->STRONG:LX/0cs;

    aput-object v1, v0, v3

    sget-object v1, LX/0cs;->STRONG_EXPIRABLE:LX/0cs;

    aput-object v1, v0, v4

    sget-object v1, LX/0cs;->STRONG_EVICTABLE:LX/0cs;

    aput-object v1, v0, v5

    sget-object v1, LX/0cs;->STRONG_EXPIRABLE_EVICTABLE:LX/0cs;

    aput-object v1, v0, v6

    sget-object v1, LX/0cs;->WEAK:LX/0cs;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0cs;->WEAK_EXPIRABLE:LX/0cs;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0cs;->WEAK_EVICTABLE:LX/0cs;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0cs;->WEAK_EXPIRABLE_EVICTABLE:LX/0cs;

    aput-object v2, v0, v1

    sput-object v0, LX/0cs;->$VALUES:[LX/0cs;

    .line 89455
    new-array v0, v6, [[LX/0cs;

    new-array v1, v7, [LX/0cs;

    sget-object v2, LX/0cs;->STRONG:LX/0cs;

    aput-object v2, v1, v3

    sget-object v2, LX/0cs;->STRONG_EXPIRABLE:LX/0cs;

    aput-object v2, v1, v4

    sget-object v2, LX/0cs;->STRONG_EVICTABLE:LX/0cs;

    aput-object v2, v1, v5

    sget-object v2, LX/0cs;->STRONG_EXPIRABLE_EVICTABLE:LX/0cs;

    aput-object v2, v1, v6

    aput-object v1, v0, v3

    new-array v1, v3, [LX/0cs;

    aput-object v1, v0, v4

    new-array v1, v7, [LX/0cs;

    sget-object v2, LX/0cs;->WEAK:LX/0cs;

    aput-object v2, v1, v3

    sget-object v2, LX/0cs;->WEAK_EXPIRABLE:LX/0cs;

    aput-object v2, v1, v4

    sget-object v2, LX/0cs;->WEAK_EVICTABLE:LX/0cs;

    aput-object v2, v1, v5

    sget-object v2, LX/0cs;->WEAK_EXPIRABLE_EVICTABLE:LX/0cs;

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    sput-object v0, LX/0cs;->factories:[[LX/0cs;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 89459
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getFactory(LX/0ci;ZZ)LX/0cs;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 89456
    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz p2, :cond_0

    const/4 v0, 0x2

    :cond_0
    or-int/2addr v0, v1

    .line 89457
    sget-object v1, LX/0cs;->factories:[[LX/0cs;

    invoke-virtual {p0}, LX/0ci;->ordinal()I

    move-result v2

    aget-object v1, v1, v2

    aget-object v0, v1, v0

    return-object v0

    :cond_1
    move v1, v0

    .line 89458
    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0cs;
    .locals 1

    .prologue
    .line 89444
    const-class v0, LX/0cs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0cs;

    return-object v0
.end method

.method public static values()[LX/0cs;
    .locals 1

    .prologue
    .line 89445
    sget-object v0, LX/0cs;->$VALUES:[LX/0cs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0cs;

    return-object v0
.end method


# virtual methods
.method public copyEntry(LX/0d3;LX/0qF;LX/0qF;)LX/0qF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0d3",
            "<TK;TV;>;",
            "LX/0qF",
            "<TK;TV;>;",
            "LX/0qF",
            "<TK;TV;>;)",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 89438
    invoke-interface {p2}, LX/0qF;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, LX/0qF;->getHash()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, LX/0cs;->newEntry(LX/0d3;Ljava/lang/Object;ILX/0qF;)LX/0qF;

    move-result-object v0

    return-object v0
.end method

.method public copyEvictableEntry(LX/0qF;LX/0qF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0qF",
            "<TK;TV;>;",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 89434
    invoke-interface {p1}, LX/0qF;->getPreviousEvictable()LX/0qF;

    move-result-object v0

    invoke-static {v0, p2}, LX/0cn;->b(LX/0qF;LX/0qF;)V

    .line 89435
    invoke-interface {p1}, LX/0qF;->getNextEvictable()LX/0qF;

    move-result-object v0

    invoke-static {p2, v0}, LX/0cn;->b(LX/0qF;LX/0qF;)V

    .line 89436
    invoke-static {p1}, LX/0cn;->e(LX/0qF;)V

    .line 89437
    return-void
.end method

.method public copyExpirableEntry(LX/0qF;LX/0qF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0qF",
            "<TK;TV;>;",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 89439
    invoke-interface {p1}, LX/0qF;->getExpirationTime()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, LX/0qF;->setExpirationTime(J)V

    .line 89440
    invoke-interface {p1}, LX/0qF;->getPreviousExpirable()LX/0qF;

    move-result-object v0

    invoke-static {v0, p2}, LX/0cn;->a(LX/0qF;LX/0qF;)V

    .line 89441
    invoke-interface {p1}, LX/0qF;->getNextExpirable()LX/0qF;

    move-result-object v0

    invoke-static {p2, v0}, LX/0cn;->a(LX/0qF;LX/0qF;)V

    .line 89442
    invoke-static {p1}, LX/0cn;->d(LX/0qF;)V

    .line 89443
    return-void
.end method

.method public abstract newEntry(LX/0d3;Ljava/lang/Object;ILX/0qF;)LX/0qF;
    .param p4    # LX/0qF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0d3",
            "<TK;TV;>;TK;I",
            "LX/0qF",
            "<TK;TV;>;)",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end method
