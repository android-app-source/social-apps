.class public abstract LX/0pK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0W3;

.field private final b:[I

.field private final c:[Z

.field private final d:[Z

.field private final e:[Z

.field private final f:[Lorg/json/JSONObject;

.field private final g:[Z

.field private final h:[Ljava/lang/String;

.field private final i:[Z

.field private final j:[[Ljava/lang/String;

.field private final k:[Z


# direct methods
.method public constructor <init>(LX/0W3;IIII)V
    .locals 7

    .prologue
    .line 144214
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, LX/0pK;-><init>(LX/0W3;IIIII)V

    .line 144215
    return-void
.end method

.method public constructor <init>(LX/0W3;IIIII)V
    .locals 1

    .prologue
    .line 144201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144202
    iput-object p1, p0, LX/0pK;->a:LX/0W3;

    .line 144203
    new-array v0, p2, [I

    iput-object v0, p0, LX/0pK;->b:[I

    .line 144204
    new-array v0, p2, [Z

    iput-object v0, p0, LX/0pK;->c:[Z

    .line 144205
    new-array v0, p3, [Z

    iput-object v0, p0, LX/0pK;->d:[Z

    .line 144206
    new-array v0, p3, [Z

    iput-object v0, p0, LX/0pK;->e:[Z

    .line 144207
    new-array v0, p5, [Lorg/json/JSONObject;

    iput-object v0, p0, LX/0pK;->f:[Lorg/json/JSONObject;

    .line 144208
    new-array v0, p5, [Z

    iput-object v0, p0, LX/0pK;->g:[Z

    .line 144209
    new-array v0, p4, [Ljava/lang/String;

    iput-object v0, p0, LX/0pK;->h:[Ljava/lang/String;

    .line 144210
    new-array v0, p4, [Z

    iput-object v0, p0, LX/0pK;->i:[Z

    .line 144211
    new-array v0, p6, [[Ljava/lang/String;

    iput-object v0, p0, LX/0pK;->j:[[Ljava/lang/String;

    .line 144212
    new-array v0, p6, [Z

    iput-object v0, p0, LX/0pK;->k:[Z

    .line 144213
    return-void
.end method


# virtual methods
.method public final a(JII)I
    .locals 3

    .prologue
    .line 144216
    iget-object v0, p0, LX/0pK;->c:[Z

    aget-boolean v0, v0, p3

    if-nez v0, :cond_0

    .line 144217
    iget-object v0, p0, LX/0pK;->b:[I

    iget-object v1, p0, LX/0pK;->a:LX/0W3;

    invoke-interface {v1, p1, p2, p4}, LX/0W4;->a(JI)I

    move-result v1

    aput v1, v0, p3

    .line 144218
    iget-object v0, p0, LX/0pK;->c:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p3

    .line 144219
    :cond_0
    iget-object v0, p0, LX/0pK;->b:[I

    aget v0, v0, p3

    return v0
.end method

.method public final a(JILjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 144197
    iget-object v0, p0, LX/0pK;->i:[Z

    aget-boolean v0, v0, p3

    if-nez v0, :cond_0

    .line 144198
    iget-object v0, p0, LX/0pK;->h:[Ljava/lang/String;

    iget-object v1, p0, LX/0pK;->a:LX/0W3;

    invoke-interface {v1, p1, p2, p4}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, p3

    .line 144199
    iget-object v0, p0, LX/0pK;->i:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p3

    .line 144200
    :cond_0
    iget-object v0, p0, LX/0pK;->h:[Ljava/lang/String;

    aget-object v0, v0, p3

    return-object v0
.end method

.method public final a(JIZ)Z
    .locals 3

    .prologue
    .line 144193
    iget-object v0, p0, LX/0pK;->e:[Z

    aget-boolean v0, v0, p3

    if-nez v0, :cond_0

    .line 144194
    iget-object v0, p0, LX/0pK;->d:[Z

    iget-object v1, p0, LX/0pK;->a:LX/0W3;

    invoke-interface {v1, p1, p2, p4}, LX/0W4;->a(JZ)Z

    move-result v1

    aput-boolean v1, v0, p3

    .line 144195
    iget-object v0, p0, LX/0pK;->e:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p3

    .line 144196
    :cond_0
    iget-object v0, p0, LX/0pK;->d:[Z

    aget-boolean v0, v0, p3

    return v0
.end method
