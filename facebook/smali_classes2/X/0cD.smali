.class public abstract LX/0cD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field public static a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/0cD;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/net/Uri;

.field public final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87892
    new-instance v0, LX/0cE;

    invoke-direct {v0}, LX/0cE;-><init>()V

    sput-object v0, LX/0cD;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;I)V
    .locals 0

    .prologue
    .line 87893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87894
    iput-object p1, p0, LX/0cD;->b:Landroid/net/Uri;

    .line 87895
    iput p2, p0, LX/0cD;->c:I

    .line 87896
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(Landroid/net/Uri;LX/4gt;)V
.end method

.method public abstract a(Landroid/os/Bundle;)V
.end method

.method public abstract a(Landroid/net/Uri;Ljava/lang/Object;)Z
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract b(Landroid/os/Bundle;)V
.end method
