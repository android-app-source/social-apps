.class public LX/1Fn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 224625
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224626
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 224627
    iput-object v0, p0, LX/1Fn;->a:LX/0Ot;

    .line 224628
    return-void
.end method

.method public static a(LX/0QB;)LX/1Fn;
    .locals 1

    .prologue
    .line 224624
    invoke-static {p0}, LX/1Fn;->b(LX/0QB;)LX/1Fn;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1Fn;
    .locals 2

    .prologue
    .line 224620
    new-instance v0, LX/1Fn;

    invoke-direct {v0}, LX/1Fn;-><init>()V

    .line 224621
    const/16 v1, 0xdf4

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 224622
    iput-object v1, v0, LX/1Fn;->a:LX/0Ot;

    .line 224623
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 224615
    iget-object v0, p0, LX/1Fn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->dS:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 4

    .prologue
    .line 224619
    iget-object v0, p0, LX/1Fn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->dT:J

    const/16 v1, 0xc8

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 4

    .prologue
    .line 224618
    iget-object v0, p0, LX/1Fn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->dU:J

    const/16 v1, 0x1e

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    return v0
.end method

.method public final e()D
    .locals 6

    .prologue
    .line 224617
    iget-object v0, p0, LX/1Fn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->dV:J

    const-wide v4, 0x3fc3333333333333L    # 0.15

    invoke-interface {v0, v2, v3, v4, v5}, LX/0W4;->a(JD)D

    move-result-wide v0

    return-wide v0
.end method

.method public final g()Z
    .locals 4

    .prologue
    .line 224616
    iget-object v0, p0, LX/1Fn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->dX:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method
