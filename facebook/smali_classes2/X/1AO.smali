.class public final enum LX/1AO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1AO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1AO;

.field public static final enum HIDDEN:LX/1AO;

.field public static final enum HIDING:LX/1AO;

.field public static final enum REVEALING:LX/1AO;

.field public static final enum SHOWN:LX/1AO;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 210415
    new-instance v0, LX/1AO;

    const-string v1, "REVEALING"

    invoke-direct {v0, v1, v2}, LX/1AO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1AO;->REVEALING:LX/1AO;

    .line 210416
    new-instance v0, LX/1AO;

    const-string v1, "HIDING"

    invoke-direct {v0, v1, v3}, LX/1AO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1AO;->HIDING:LX/1AO;

    .line 210417
    new-instance v0, LX/1AO;

    const-string v1, "SHOWN"

    invoke-direct {v0, v1, v4}, LX/1AO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1AO;->SHOWN:LX/1AO;

    .line 210418
    new-instance v0, LX/1AO;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v5}, LX/1AO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1AO;->HIDDEN:LX/1AO;

    .line 210419
    const/4 v0, 0x4

    new-array v0, v0, [LX/1AO;

    sget-object v1, LX/1AO;->REVEALING:LX/1AO;

    aput-object v1, v0, v2

    sget-object v1, LX/1AO;->HIDING:LX/1AO;

    aput-object v1, v0, v3

    sget-object v1, LX/1AO;->SHOWN:LX/1AO;

    aput-object v1, v0, v4

    sget-object v1, LX/1AO;->HIDDEN:LX/1AO;

    aput-object v1, v0, v5

    sput-object v0, LX/1AO;->$VALUES:[LX/1AO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 210420
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1AO;
    .locals 1

    .prologue
    .line 210421
    const-class v0, LX/1AO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1AO;

    return-object v0
.end method

.method public static values()[LX/1AO;
    .locals 1

    .prologue
    .line 210422
    sget-object v0, LX/1AO;->$VALUES:[LX/1AO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1AO;

    return-object v0
.end method
