.class public LX/1i4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/1i4;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1iZ;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0kb;

.field private final c:LX/0p7;

.field private final d:LX/0So;

.field private final e:LX/03V;


# direct methods
.method public constructor <init>(LX/03V;LX/0Or;LX/0kb;LX/0p7;LX/0So;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1iZ;",
            ">;>;",
            "LX/0kb;",
            "LX/0p7;",
            "LX/0So;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 297462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297463
    iput-object p1, p0, LX/1i4;->e:LX/03V;

    .line 297464
    invoke-static {}, LX/0Rj;->notNull()LX/0Rl;

    move-result-object v0

    .line 297465
    new-instance p1, LX/1iA;

    invoke-direct {p1, p2, v0}, LX/1iA;-><init>(LX/0Or;LX/0Rl;)V

    move-object v0, p1

    .line 297466
    iput-object v0, p0, LX/1i4;->a:LX/0Or;

    .line 297467
    iput-object p3, p0, LX/1i4;->b:LX/0kb;

    .line 297468
    iput-object p4, p0, LX/1i4;->c:LX/0p7;

    .line 297469
    iput-object p5, p0, LX/1i4;->d:LX/0So;

    .line 297470
    return-void
.end method

.method public static a(LX/0QB;)LX/1i4;
    .locals 9

    .prologue
    .line 297471
    sget-object v0, LX/1i4;->f:LX/1i4;

    if-nez v0, :cond_1

    .line 297472
    const-class v1, LX/1i4;

    monitor-enter v1

    .line 297473
    :try_start_0
    sget-object v0, LX/1i4;->f:LX/1i4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 297474
    if-eqz v2, :cond_0

    .line 297475
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 297476
    new-instance v3, LX/1i4;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/1hf;->a(LX/0QB;)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v6

    check-cast v6, LX/0kb;

    invoke-static {v0}, LX/0p7;->a(LX/0QB;)LX/0p7;

    move-result-object v7

    check-cast v7, LX/0p7;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v8

    check-cast v8, LX/0So;

    invoke-direct/range {v3 .. v8}, LX/1i4;-><init>(LX/03V;LX/0Or;LX/0kb;LX/0p7;LX/0So;)V

    .line 297477
    move-object v0, v3

    .line 297478
    sput-object v0, LX/1i4;->f:LX/1i4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297479
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 297480
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 297481
    :cond_1
    sget-object v0, LX/1i4;->f:LX/1i4;

    return-object v0

    .line 297482
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 297483
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/1i4;Lorg/apache/http/protocol/HttpContext;Lorg/apache/http/client/methods/HttpUriRequest;LX/1iW;)LX/4c3;
    .locals 3

    .prologue
    .line 297484
    iget-object v0, p0, LX/1i4;->a:LX/0Or;

    iget-object v1, p0, LX/1i4;->e:LX/03V;

    .line 297485
    new-instance p0, LX/4c3;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    invoke-direct {p0, p1, p2, v2, v1}, LX/4c3;-><init>(Lorg/apache/http/protocol/HttpContext;Lorg/apache/http/HttpRequest;Ljava/util/Set;LX/03V;)V

    move-object v0, p0

    .line 297486
    iput-object p3, v0, LX/4c3;->c:LX/1iW;

    .line 297487
    iget-object v1, v0, LX/4c3;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1iZ;

    .line 297488
    invoke-interface {v1, p2, p1, p3}, LX/1iZ;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;LX/1iW;)V

    goto :goto_0

    .line 297489
    :cond_0
    iget-object v1, v0, LX/4c3;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1iZ;

    .line 297490
    invoke-interface {v1, p2, p1}, LX/1iZ;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)V

    goto :goto_1

    .line 297491
    :cond_1
    return-object v0
.end method

.method private static a(LX/4c3;Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    .prologue
    .line 297492
    invoke-virtual {p0, p1}, LX/4c3;->b(Ljava/io/IOException;)V

    .line 297493
    throw p1
.end method


# virtual methods
.method public final a(Lorg/apache/http/client/methods/HttpUriRequest;LX/15F;Lorg/apache/http/protocol/HttpContext;LX/1MQ;LX/0am;LX/2BD;)Lorg/apache/http/HttpResponse;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Lcom/facebook/http/interfaces/HttpRequestState;",
            "Lorg/apache/http/protocol/HttpContext;",
            "LX/1MQ;",
            "LX/0am",
            "<",
            "Ljava/util/List",
            "<",
            "LX/1iW;",
            ">;>;",
            "LX/2BD;",
            ")",
            "Lorg/apache/http/HttpResponse;"
        }
    .end annotation

    .prologue
    .line 297494
    new-instance v1, LX/1iW;

    invoke-interface {p4}, LX/1MQ;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/1i4;->c:LX/0p7;

    iget-object v3, p0, LX/1i4;->d:LX/0So;

    invoke-direct {v1, v0, v2, v3, p6}, LX/1iW;-><init>(Ljava/lang/String;LX/0p7;LX/0So;LX/2BD;)V

    .line 297495
    invoke-virtual {p5}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297496
    invoke-virtual {p5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 297497
    :cond_0
    invoke-static {p0, p3, p1, v1}, LX/1i4;->a(LX/1i4;Lorg/apache/http/protocol/HttpContext;Lorg/apache/http/client/methods/HttpUriRequest;LX/1iW;)LX/4c3;

    move-result-object v0

    .line 297498
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {p1}, LX/1jS;->a(Lorg/apache/http/HttpRequest;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 297499
    iput-wide v2, v1, LX/1iW;->a:J

    .line 297500
    iget-object v2, p0, LX/1i4;->b:LX/0kb;

    if-eqz v2, :cond_1

    .line 297501
    iget-object v2, p0, LX/1i4;->b:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->k()Ljava/lang/String;

    move-result-object v2

    .line 297502
    iput-object v2, v1, LX/1iW;->g:Ljava/lang/String;

    .line 297503
    iget-object v2, p0, LX/1i4;->b:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->l()Ljava/lang/String;

    move-result-object v2

    .line 297504
    iput-object v2, v1, LX/1iW;->h:Ljava/lang/String;

    .line 297505
    iget-object v2, p0, LX/1i4;->b:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->j()Ljava/lang/String;

    move-result-object v2

    .line 297506
    iput-object v2, v1, LX/1iW;->f:Ljava/lang/String;

    .line 297507
    :cond_1
    :try_start_0
    invoke-interface {p4, p1, p2, p3, v1}, LX/1MQ;->a(Lorg/apache/http/client/methods/HttpUriRequest;LX/15F;Lorg/apache/http/protocol/HttpContext;LX/1iW;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 297508
    invoke-virtual {v0, v1, p3}, LX/4c3;->a(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V

    .line 297509
    return-object v1

    .line 297510
    :catch_0
    move-exception v1

    .line 297511
    invoke-static {v0, v1}, LX/1i4;->a(LX/4c3;Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method
