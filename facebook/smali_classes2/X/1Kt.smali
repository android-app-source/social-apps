.class public abstract LX/1Kt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final a:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1Ce;",
            ">;"
        }
    .end annotation
.end field

.field public d:I

.field public e:I

.field public f:Z

.field public final g:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0ad;

.field public final i:LX/03V;

.field public final j:LX/0Sh;


# direct methods
.method public constructor <init>(LX/0ad;LX/03V;LX/0Sh;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 233263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233264
    iput v0, p0, LX/1Kt;->d:I

    .line 233265
    iput v0, p0, LX/1Kt;->e:I

    .line 233266
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Kt;->f:Z

    .line 233267
    iput-object p1, p0, LX/1Kt;->h:LX/0ad;

    .line 233268
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/1Kt;->c:Ljava/util/List;

    .line 233269
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/1Kt;->g:LX/01J;

    .line 233270
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/1Kt;->a:LX/01J;

    .line 233271
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/1Kt;->b:LX/01J;

    .line 233272
    iput-object p2, p0, LX/1Kt;->i:LX/03V;

    .line 233273
    iput-object p3, p0, LX/1Kt;->j:LX/0Sh;

    .line 233274
    return-void
.end method


# virtual methods
.method public a(LX/0g8;)V
    .locals 1

    .prologue
    .line 233320
    invoke-virtual {p0, p1}, LX/1Kt;->d(LX/0g8;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233321
    :goto_0
    return-void

    .line 233322
    :cond_0
    new-instance v0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;

    invoke-direct {v0, p0, p1}, Lcom/facebook/common/viewport/BaseViewportMonitor$1;-><init>(LX/1Kt;LX/0g8;)V

    invoke-interface {p1, v0}, LX/0g8;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(LX/0g8;I)V
    .locals 0

    .prologue
    .line 233319
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 10

    .prologue
    .line 233280
    iget-boolean v0, p0, LX/1Kt;->f:Z

    if-nez v0, :cond_1

    .line 233281
    :cond_0
    :goto_0
    return-void

    .line 233282
    :cond_1
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 233283
    const-string v0, "BaseViewportMonitor.onScroll"

    const v1, -0xf8fa395

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 233284
    :try_start_0
    iget v2, p0, LX/1Kt;->d:I

    .line 233285
    iget v5, p0, LX/1Kt;->e:I

    .line 233286
    iput p2, p0, LX/1Kt;->d:I

    .line 233287
    add-int v0, p2, p3

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1Kt;->e:I

    .line 233288
    iget-object v0, p0, LX/1Kt;->b:LX/01J;

    invoke-virtual {v0}, LX/01J;->clear()V

    .line 233289
    const/4 v3, 0x0

    .line 233290
    invoke-interface {p1}, LX/0g8;->s()I

    move-result v6

    .line 233291
    const/4 v1, 0x0

    iget v0, p0, LX/1Kt;->d:I

    move v4, v1

    move-object v1, v3

    move v3, v0

    .line 233292
    :goto_1
    if-ge v4, p3, :cond_6

    if-ge v3, v6, :cond_6

    .line 233293
    invoke-virtual {p0, p1, v3}, LX/1Kt;->b(LX/0g8;I)Ljava/lang/Object;

    move-result-object v0

    .line 233294
    if-lt v3, v2, :cond_2

    if-le v3, v5, :cond_3

    .line 233295
    :cond_2
    iget v7, p0, LX/1Kt;->d:I

    .line 233296
    iget-object v8, p0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result p2

    .line 233297
    const/4 v8, 0x0

    move v9, v8

    :goto_2
    if-ge v9, p2, :cond_3

    .line 233298
    iget-object v8, p0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/1Ce;

    invoke-interface {v8, p1, v0, v7, v4}, LX/1Ce;->a(LX/0g8;Ljava/lang/Object;II)V

    .line 233299
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_2

    .line 233300
    :cond_3
    if-eqz v0, :cond_b

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_b

    .line 233301
    iget-object v1, p0, LX/1Kt;->g:LX/01J;

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v1, v0, v7}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_4

    .line 233302
    invoke-virtual {p0, v0}, LX/1Kt;->a(Ljava/lang/Object;)V

    .line 233303
    :cond_4
    iget-object v1, p0, LX/1Kt;->b:LX/01J;

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v1, v0, v7}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_5

    .line 233304
    invoke-virtual {p0, p1, v0, v4}, LX/1Kt;->a(LX/0g8;Ljava/lang/Object;I)V

    .line 233305
    :cond_5
    :goto_3
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-object v1, v0

    goto :goto_1

    :cond_6
    move v0, v2

    .line 233306
    :goto_4
    if-gt v0, v5, :cond_a

    if-ge v0, p4, :cond_a

    .line 233307
    iget v1, p0, LX/1Kt;->d:I

    if-lt v0, v1, :cond_7

    iget v1, p0, LX/1Kt;->e:I

    if-le v0, v1, :cond_9

    .line 233308
    :cond_7
    invoke-virtual {p0, p1, v0}, LX/1Kt;->b(LX/0g8;I)Ljava/lang/Object;

    move-result-object v1

    .line 233309
    iget v2, p0, LX/1Kt;->d:I

    sub-int v2, v0, v2

    .line 233310
    iget-object v3, p0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    .line 233311
    const/4 v3, 0x0

    move v4, v3

    :goto_5
    if-ge v4, v6, :cond_8

    .line 233312
    iget-object v3, p0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Ce;

    invoke-interface {v3, p1, v1, v2}, LX/1Ce;->b(LX/0g8;Ljava/lang/Object;I)V

    .line 233313
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_5

    .line 233314
    :cond_8
    iget-object v2, p0, LX/1Kt;->b:LX/01J;

    invoke-virtual {v2, v1}, LX/01J;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 233315
    iget-object v2, p0, LX/1Kt;->g:LX/01J;

    invoke-virtual {v2, v1}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 233316
    invoke-virtual {p0, v1}, LX/1Kt;->b(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 233317
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 233318
    :cond_a
    const v0, -0x5b6db455

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x1e0528c1

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_b
    move-object v0, v1

    goto :goto_3
.end method

.method public final a(LX/0g8;Ljava/lang/Object;I)V
    .locals 3

    .prologue
    .line 233275
    iget-object v0, p0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 233276
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 233277
    iget-object v0, p0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ce;

    invoke-interface {v0, p1, p2, p3}, LX/1Ce;->a(LX/0g8;Ljava/lang/Object;I)V

    .line 233278
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 233279
    :cond_0
    return-void
.end method

.method public final a(LX/1Ce;)V
    .locals 1

    .prologue
    .line 233216
    iget-object v0, p0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 233217
    iget-object v0, p0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233218
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 233258
    iget-object v0, p0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 233259
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 233260
    iget-object v0, p0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ce;

    invoke-interface {v0, p1}, LX/1Ce;->a(Ljava/lang/Object;)V

    .line 233261
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 233262
    :cond_0
    return-void
.end method

.method public final a(ZLX/0g8;)V
    .locals 2

    .prologue
    .line 233323
    iget-object v0, p0, LX/1Kt;->j:LX/0Sh;

    const-string v1, "BaseViewportMonitor should only be used on the UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 233324
    if-eqz p1, :cond_0

    .line 233325
    invoke-virtual {p0, p2}, LX/1Kt;->a(LX/0g8;)V

    .line 233326
    :goto_0
    iput-boolean p1, p0, LX/1Kt;->f:Z

    .line 233327
    return-void

    .line 233328
    :cond_0
    invoke-virtual {p0, p2}, LX/1Kt;->c(LX/0g8;)V

    goto :goto_0
.end method

.method public abstract b(LX/0g8;I)Ljava/lang/Object;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public final b(LX/0g8;)V
    .locals 3

    .prologue
    .line 233253
    iget-object v0, p0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 233254
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 233255
    iget-object v0, p0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ce;

    invoke-interface {v0, p1}, LX/1Ce;->a(LX/0g8;)V

    .line 233256
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 233257
    :cond_0
    return-void
.end method

.method public final b(LX/1Ce;)V
    .locals 1

    .prologue
    .line 233251
    iget-object v0, p0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 233252
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 233246
    iget-object v0, p0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 233247
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 233248
    iget-object v0, p0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ce;

    invoke-interface {v0, p1}, LX/1Ce;->b(Ljava/lang/Object;)V

    .line 233249
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 233250
    :cond_0
    return-void
.end method

.method public final c(LX/0g8;)V
    .locals 4

    .prologue
    .line 233229
    invoke-interface {p1}, LX/0g8;->s()I

    move-result v0

    if-nez v0, :cond_1

    .line 233230
    :cond_0
    return-void

    .line 233231
    :cond_1
    invoke-interface {p1}, LX/0g8;->q()I

    move-result v0

    iput v0, p0, LX/1Kt;->d:I

    .line 233232
    invoke-interface {p1}, LX/0g8;->r()I

    move-result v0

    iput v0, p0, LX/1Kt;->e:I

    .line 233233
    invoke-interface {p1}, LX/0g8;->s()I

    move-result v1

    .line 233234
    iget v0, p0, LX/1Kt;->d:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_3

    .line 233235
    iget v0, p0, LX/1Kt;->d:I

    .line 233236
    :goto_0
    iget v2, p0, LX/1Kt;->e:I

    if-gt v0, v2, :cond_3

    if-ge v0, v1, :cond_3

    .line 233237
    invoke-virtual {p0, p1, v0}, LX/1Kt;->b(LX/0g8;I)Ljava/lang/Object;

    move-result-object v2

    .line 233238
    iget-object v3, p0, LX/1Kt;->g:LX/01J;

    invoke-virtual {v3, v2}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 233239
    invoke-virtual {p0, v2}, LX/1Kt;->b(Ljava/lang/Object;)V

    .line 233240
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 233241
    :cond_3
    iget-object v0, p0, LX/1Kt;->g:LX/01J;

    invoke-virtual {v0}, LX/01J;->clear()V

    .line 233242
    iget-object v0, p0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 233243
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_0

    .line 233244
    iget-object v0, p0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ce;

    invoke-interface {v0, p1}, LX/1Ce;->b(LX/0g8;)V

    .line 233245
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final d(LX/0g8;)Z
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 233219
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 233220
    :goto_0
    return v0

    .line 233221
    :cond_1
    invoke-interface {p1}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/widget/listview/BetterListView;

    if-eqz v0, :cond_3

    .line 233222
    invoke-interface {p1}, LX/0g8;->o()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 233223
    :cond_3
    invoke-interface {p1}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-eqz v0, :cond_5

    .line 233224
    invoke-interface {p1}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 233225
    iget-object v3, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v3

    .line 233226
    if-nez v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0

    .line 233227
    :cond_5
    iget-object v0, p0, LX/1Kt;->i:LX/03V;

    const-string v2, "BaseViewportMonitor#hasNullAdapter()"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected scrollingViewProxy view type when attempting to check backing adapter state"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 233228
    goto :goto_0
.end method
