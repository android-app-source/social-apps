.class public LX/0Pn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Po;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Po",
        "<",
        "LX/0Pn;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/0aw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aw",
            "<",
            "LX/0Pn;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:I

.field public c:I

.field public d:J

.field public e:J

.field public f:J

.field public g:I

.field public h:I

.field public i:Z

.field public j:Z

.field public k:Ljava/lang/String;

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:LX/00q;

.field public p:LX/03R;

.field public q:I

.field public r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public s:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public t:S

.field public u:J

.field public v:Z

.field public w:Z

.field private x:LX/0Pn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57306
    new-instance v0, LX/0av;

    const/16 v1, 0x1f4

    invoke-direct {v0, v1}, LX/0av;-><init>(I)V

    sput-object v0, LX/0Pn;->a:LX/0aw;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57305
    return-void
.end method

.method public static a(IIIIZZZJZJJZ)LX/0Pn;
    .locals 3

    .prologue
    .line 57287
    sget-object v1, LX/0Pn;->a:LX/0aw;

    invoke-virtual {v1}, LX/0aw;->b()LX/0Po;

    move-result-object v1

    check-cast v1, LX/0Pn;

    .line 57288
    iput p0, v1, LX/0Pn;->g:I

    .line 57289
    iput p3, v1, LX/0Pn;->h:I

    .line 57290
    iput-boolean p4, v1, LX/0Pn;->i:Z

    .line 57291
    iput-boolean p5, v1, LX/0Pn;->j:Z

    .line 57292
    iput-boolean p6, v1, LX/0Pn;->m:Z

    .line 57293
    iput-wide p7, v1, LX/0Pn;->d:J

    .line 57294
    iput-boolean p9, v1, LX/0Pn;->n:Z

    .line 57295
    iput-wide p7, v1, LX/0Pn;->u:J

    .line 57296
    iput-wide p10, v1, LX/0Pn;->f:J

    .line 57297
    iput-wide p12, v1, LX/0Pn;->e:J

    .line 57298
    iput p1, v1, LX/0Pn;->c:I

    .line 57299
    iput p2, v1, LX/0Pn;->b:I

    .line 57300
    const/4 v2, 0x1

    iput-short v2, v1, LX/0Pn;->t:S

    .line 57301
    move/from16 v0, p14

    iput-boolean v0, v1, LX/0Pn;->v:Z

    .line 57302
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/0Pn;->w:Z

    .line 57303
    return-object v1
.end method

.method public static a(IIJZZ)LX/0Pn;
    .locals 2

    .prologue
    .line 57280
    sget-object v0, LX/0Pn;->a:LX/0aw;

    invoke-virtual {v0}, LX/0aw;->b()LX/0Po;

    move-result-object v0

    check-cast v0, LX/0Pn;

    .line 57281
    iput p0, v0, LX/0Pn;->g:I

    .line 57282
    iput p1, v0, LX/0Pn;->c:I

    .line 57283
    iput-wide p2, v0, LX/0Pn;->d:J

    .line 57284
    iput-boolean p4, v0, LX/0Pn;->n:Z

    .line 57285
    iput-boolean p5, v0, LX/0Pn;->m:Z

    .line 57286
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 57279
    iget-object v0, p0, LX/0Pn;->x:LX/0Pn;

    return-object v0
.end method

.method public final a(LX/03R;Z)V
    .locals 2

    .prologue
    .line 57275
    if-nez p2, :cond_0

    iget-object v0, p0, LX/0Pn;->p:LX/03R;

    sget-object v1, LX/03R;->YES:LX/03R;

    if-eq v0, v1, :cond_1

    .line 57276
    :cond_0
    if-eqz p1, :cond_2

    :goto_0
    iput-object p1, p0, LX/0Pn;->p:LX/03R;

    .line 57277
    :cond_1
    return-void

    .line 57278
    :cond_2
    sget-object p1, LX/03R;->UNSET:LX/03R;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 57272
    check-cast p1, LX/0Pn;

    .line 57273
    iput-object p1, p0, LX/0Pn;->x:LX/0Pn;

    .line 57274
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 57307
    iget-object v0, p0, LX/0Pn;->r:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 57308
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0Pn;->r:Ljava/util/ArrayList;

    .line 57309
    :cond_0
    iget-object v0, p0, LX/0Pn;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57310
    iget-object v0, p0, LX/0Pn;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57311
    const-string v0, "tag_name"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57312
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Pn;->l:Z

    .line 57313
    :cond_1
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 57269
    iget-boolean v0, p0, LX/0Pn;->n:Z

    if-eqz v0, :cond_0

    :goto_0
    iput-boolean p1, p0, LX/0Pn;->n:Z

    .line 57270
    return-void

    .line 57271
    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public final a(ZZ)V
    .locals 1

    .prologue
    .line 57267
    invoke-static {p1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/0Pn;->a(LX/03R;Z)V

    .line 57268
    return-void
.end method

.method public final b()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 57238
    iput v1, p0, LX/0Pn;->b:I

    .line 57239
    iput v1, p0, LX/0Pn;->c:I

    .line 57240
    iput-wide v4, p0, LX/0Pn;->d:J

    .line 57241
    iput-wide v4, p0, LX/0Pn;->e:J

    .line 57242
    iput-wide v4, p0, LX/0Pn;->f:J

    .line 57243
    iput v1, p0, LX/0Pn;->g:I

    .line 57244
    iput v1, p0, LX/0Pn;->h:I

    .line 57245
    iput-boolean v1, p0, LX/0Pn;->i:Z

    .line 57246
    iput-boolean v1, p0, LX/0Pn;->j:Z

    .line 57247
    iput-object v2, p0, LX/0Pn;->k:Ljava/lang/String;

    .line 57248
    iput-boolean v1, p0, LX/0Pn;->l:Z

    .line 57249
    iput-boolean v1, p0, LX/0Pn;->m:Z

    .line 57250
    iput-boolean v1, p0, LX/0Pn;->n:Z

    .line 57251
    iput-object v2, p0, LX/0Pn;->o:LX/00q;

    .line 57252
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/0Pn;->p:LX/03R;

    .line 57253
    iput v1, p0, LX/0Pn;->q:I

    .line 57254
    iput-object v2, p0, LX/0Pn;->r:Ljava/util/ArrayList;

    .line 57255
    iput-object v2, p0, LX/0Pn;->s:Ljava/util/ArrayList;

    .line 57256
    iput-short v1, p0, LX/0Pn;->t:S

    .line 57257
    iput-wide v4, p0, LX/0Pn;->u:J

    .line 57258
    iput-boolean v1, p0, LX/0Pn;->v:Z

    .line 57259
    iput-boolean v1, p0, LX/0Pn;->w:Z

    .line 57260
    return-void
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 57266
    iget-wide v0, p0, LX/0Pn;->u:J

    return-wide v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 57263
    iget-object v0, p0, LX/0Pn;->o:LX/00q;

    if-eqz v0, :cond_0

    .line 57264
    iget-object v0, p0, LX/0Pn;->o:LX/00q;

    invoke-virtual {v0}, LX/00q;->o()V

    .line 57265
    :cond_0
    return-void
.end method

.method public final i()J
    .locals 4

    .prologue
    .line 57262
    iget v0, p0, LX/0Pn;->c:I

    int-to-long v0, v0

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    iget v2, p0, LX/0Pn;->g:I

    int-to-long v2, v2

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 57261
    iget v0, p0, LX/0Pn;->g:I

    return v0
.end method
