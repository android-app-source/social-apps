.class public final LX/0Yx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0VI;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0Yx;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0fU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0fU;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 82820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82821
    iput-object p1, p0, LX/0Yx;->a:LX/0Ot;

    .line 82822
    return-void
.end method

.method public static a(LX/0QB;)LX/0Yx;
    .locals 4

    .prologue
    .line 82823
    sget-object v0, LX/0Yx;->b:LX/0Yx;

    if-nez v0, :cond_1

    .line 82824
    const-class v1, LX/0Yx;

    monitor-enter v1

    .line 82825
    :try_start_0
    sget-object v0, LX/0Yx;->b:LX/0Yx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 82826
    if-eqz v2, :cond_0

    .line 82827
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 82828
    new-instance v3, LX/0Yx;

    const/16 p0, 0x230

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/0Yx;-><init>(LX/0Ot;)V

    .line 82829
    move-object v0, v3

    .line 82830
    sput-object v0, LX/0Yx;->b:LX/0Yx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82831
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 82832
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 82833
    :cond_1
    sget-object v0, LX/0Yx;->b:LX/0Yx;

    return-object v0

    .line 82834
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 82835
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82813
    const-string v0, "activity_stack"

    return-object v0
.end method

.method public final a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 82814
    iget-object v0, p0, LX/0Yx;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fU;

    .line 82815
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 82816
    iget-object v1, v0, LX/0fU;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0fc;

    .line 82817
    const-string p1, "%s%n"

    invoke-virtual {v1}, LX/0fc;->b()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 82818
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 82819
    return-object v0
.end method
