.class public LX/0gn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/0gn;


# instance fields
.field private final a:LX/0go;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/content/Context;

.field private final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/0fJ;

.field private final g:LX/0Xl;

.field private h:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/apptab/state/TabTag;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0go;Lcom/facebook/content/SecureContextHelper;LX/0fJ;LX/0Xl;LX/0Or;LX/0Or;)V
    .locals 1
    .param p5    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentBaseActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0go;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0fJ;",
            "LX/0Xl;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 113410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113411
    iput-object p1, p0, LX/0gn;->d:Landroid/content/Context;

    .line 113412
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0go;

    iput-object v0, p0, LX/0gn;->a:LX/0go;

    .line 113413
    iput-object p4, p0, LX/0gn;->f:LX/0fJ;

    .line 113414
    iput-object p6, p0, LX/0gn;->b:LX/0Or;

    .line 113415
    iput-object p7, p0, LX/0gn;->c:LX/0Or;

    .line 113416
    iput-object p3, p0, LX/0gn;->e:Lcom/facebook/content/SecureContextHelper;

    .line 113417
    iput-object p5, p0, LX/0gn;->g:LX/0Xl;

    .line 113418
    return-void
.end method

.method public static a(LX/0QB;)LX/0gn;
    .locals 3

    .prologue
    .line 113400
    sget-object v0, LX/0gn;->i:LX/0gn;

    if-nez v0, :cond_1

    .line 113401
    const-class v1, LX/0gn;

    monitor-enter v1

    .line 113402
    :try_start_0
    sget-object v0, LX/0gn;->i:LX/0gn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 113403
    if-eqz v2, :cond_0

    .line 113404
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/0gn;->b(LX/0QB;)LX/0gn;

    move-result-object v0

    sput-object v0, LX/0gn;->i:LX/0gn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 113405
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 113406
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 113407
    :cond_1
    sget-object v0, LX/0gn;->i:LX/0gn;

    return-object v0

    .line 113408
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 113409
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Landroid/content/Intent;Landroid/content/ComponentName;)Z
    .locals 1
    .param p0    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 113397
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 113398
    :cond_0
    const/4 v0, 0x0

    .line 113399
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/0gn;
    .locals 8

    .prologue
    .line 113395
    new-instance v0, LX/0gn;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0go;->a(LX/0QB;)LX/0go;

    move-result-object v2

    check-cast v2, LX/0go;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0ns;->a(LX/0QB;)LX/0ns;

    move-result-object v4

    check-cast v4, LX/0fJ;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v5

    check-cast v5, LX/0Xl;

    const/16 v6, 0xc

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0xb

    invoke-static {p0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, LX/0gn;-><init>(Landroid/content/Context;LX/0go;Lcom/facebook/content/SecureContextHelper;LX/0fJ;LX/0Xl;LX/0Or;LX/0Or;)V

    .line 113396
    return-object v0
.end method

.method public static c(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 113394
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 113352
    invoke-direct {p0, p1}, LX/0gn;->e(Landroid/content/Intent;)Lcom/facebook/apptab/state/TabTag;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Landroid/content/Intent;)Lcom/facebook/apptab/state/TabTag;
    .locals 6

    .prologue
    .line 113385
    iget-object v0, p0, LX/0gn;->h:LX/0P1;

    if-nez v0, :cond_1

    .line 113386
    iget-object v0, p0, LX/0gn;->a:LX/0go;

    invoke-virtual {v0}, LX/0go;->a()Lcom/facebook/apptab/state/NavigationConfig;

    move-result-object v0

    .line 113387
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 113388
    iget-object v3, v0, Lcom/facebook/apptab/state/NavigationConfig;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/TabTag;

    .line 113389
    iget-object v5, v0, Lcom/facebook/apptab/state/TabTag;->a:Ljava/lang/String;

    invoke-virtual {v2, v5, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 113390
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 113391
    :cond_0
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/0gn;->h:LX/0P1;

    .line 113392
    :cond_1
    const-string v0, "extra_launch_uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 113393
    iget-object v1, p0, LX/0gn;->h:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/TabTag;

    return-object v0
.end method

.method private f(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 4
    .param p1    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 113371
    if-nez p1, :cond_1

    .line 113372
    const/4 p1, 0x0

    .line 113373
    :cond_0
    :goto_0
    return-object p1

    .line 113374
    :cond_1
    const-string v0, "tabbar_target_intent"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 113375
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 113376
    invoke-direct {p0, p1}, LX/0gn;->e(Landroid/content/Intent;)Lcom/facebook/apptab/state/TabTag;

    move-result-object v2

    .line 113377
    if-nez v2, :cond_2

    .line 113378
    new-instance v0, Landroid/content/ComponentName;

    iget-object v2, p0, LX/0gn;->d:Landroid/content/Context;

    const-class v3, Lcom/facebook/katana/activity/ImmersiveActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 113379
    :goto_1
    const-string v0, "tabbar_target_intent"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object p1, v1

    .line 113380
    goto :goto_0

    .line 113381
    :cond_2
    iget-object v0, p0, LX/0gn;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 113382
    const/high16 v0, 0x4000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 113383
    iget-object v0, p0, LX/0gn;->f:LX/0fJ;

    invoke-interface {v0, v1}, LX/0fJ;->a(Landroid/content/Intent;)V

    .line 113384
    const-string v0, "target_tab_name"

    invoke-virtual {v2}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method private static g(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 113370
    const-string v0, "redirect_to_local_broadcast"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static h(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 113369
    const-string v0, "redirect_to_local_broadcast"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2
    .param p1    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 113362
    if-nez p1, :cond_1

    .line 113363
    const/4 p1, 0x0

    .line 113364
    :cond_0
    :goto_0
    return-object p1

    .line 113365
    :cond_1
    const-string v0, "tabbar_target_intent"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 113366
    if-eqz v0, :cond_0

    .line 113367
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    move-object p1, v0

    .line 113368
    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 113354
    invoke-direct {p0, p2}, LX/0gn;->d(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113355
    iget-object v1, p0, LX/0gn;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, p2}, LX/0gn;->f(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 113356
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 113357
    :goto_0
    return v0

    .line 113358
    :cond_0
    invoke-static {p2}, LX/0gn;->g(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 113359
    iget-object v1, p0, LX/0gn;->g:LX/0Xl;

    invoke-static {p2}, LX/0gn;->h(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 113360
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 113361
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/content/Intent;)Z
    .locals 1
    .param p1    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 113353
    iget-object v0, p0, LX/0gn;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-static {p1, v0}, LX/0gn;->a(Landroid/content/Intent;Landroid/content/ComponentName;)Z

    move-result v0

    return v0
.end method
