.class public LX/1hP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/Integer;

.field private final b:Ljava/lang/Integer;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 296053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296054
    iput-object v0, p0, LX/1hP;->c:Ljava/util/List;

    .line 296055
    iput-object v0, p0, LX/1hP;->d:Ljava/util/Map;

    .line 296056
    iput-object p1, p0, LX/1hP;->a:Ljava/lang/Integer;

    .line 296057
    iput-object p2, p0, LX/1hP;->b:Ljava/lang/Integer;

    .line 296058
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 296059
    if-eqz p2, :cond_0

    .line 296060
    iget-object v0, p0, LX/1hP;->d:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296061
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 296046
    iget-object v0, p0, LX/1hP;->d:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 296047
    iget-object v0, p0, LX/1hP;->d:Ljava/util/Map;

    .line 296048
    :goto_0
    return-object v0

    .line 296049
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1hP;->d:Ljava/util/Map;

    .line 296050
    const-string v0, "device_auto_time_setting"

    iget-object v1, p0, LX/1hP;->a:Ljava/lang/Integer;

    invoke-direct {p0, v0, v1}, LX/1hP;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296051
    const-string v0, "device_auto_time_zone_setting"

    iget-object v1, p0, LX/1hP;->b:Ljava/lang/Integer;

    invoke-direct {p0, v0, v1}, LX/1hP;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296052
    iget-object v0, p0, LX/1hP;->d:Ljava/util/Map;

    goto :goto_0
.end method
