.class public LX/1WF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 267910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 267909
    invoke-static {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLStory;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 267898
    const/4 v1, 0x0

    .line 267899
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 267900
    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    .line 267901
    :goto_0
    if-eqz v1, :cond_1

    .line 267902
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 267903
    instance-of v2, v2, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_0

    .line 267904
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 267905
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 267906
    :cond_0
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v2

    .line 267907
    goto :goto_0

    .line 267908
    :cond_1
    return-object v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;I)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;I)",
            "Lcom/facebook/graphql/model/GraphQLStory;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 267883
    const/4 v1, 0x0

    .line 267884
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 267885
    move-object v3, v0

    .line 267886
    :goto_0
    if-eqz v3, :cond_2

    .line 267887
    iget-object v0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 267888
    instance-of v4, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v4, :cond_0

    .line 267889
    add-int/lit8 v1, v1, 0x1

    .line 267890
    if-ne v1, p1, :cond_1

    .line 267891
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 267892
    :goto_1
    return-object v0

    .line 267893
    :cond_0
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v0, :cond_1

    move-object v0, v2

    .line 267894
    goto :goto_1

    :cond_1
    move v0, v1

    .line 267895
    iget-object v1, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 267896
    move-object v3, v1

    move v1, v0

    goto :goto_0

    :cond_2
    move-object v0, v2

    .line 267897
    goto :goto_1
.end method

.method public static c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLStory;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 267882
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;I)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0
.end method

.method public static e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 267874
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 267875
    :goto_0
    if-eqz v0, :cond_1

    .line 267876
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 267877
    instance-of v1, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    .line 267878
    :goto_1
    return-object v0

    .line 267879
    :cond_0
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v1

    .line 267880
    goto :goto_0

    .line 267881
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Lcom/facebook/graphql/model/FeedUnit;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 267870
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 267871
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/facebook/graphql/model/FeedUnit;

    if-nez v1, :cond_1

    .line 267872
    :cond_0
    const/4 v0, 0x0

    .line 267873
    :goto_0
    return-object v0

    :cond_1
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    goto :goto_0
.end method

.method public static g(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "LX/16m;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 267838
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 267839
    :goto_0
    if-eqz v0, :cond_1

    .line 267840
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 267841
    instance-of v1, v1, LX/16m;

    if-eqz v1, :cond_0

    .line 267842
    :goto_1
    return-object v0

    .line 267843
    :cond_0
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v1

    .line 267844
    goto :goto_0

    .line 267845
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static h(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLFeedback;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 267860
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 267861
    :goto_0
    if-eqz v0, :cond_1

    .line 267862
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 267863
    instance-of v1, v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v1, :cond_0

    .line 267864
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 267865
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 267866
    :goto_1
    return-object v0

    .line 267867
    :cond_0
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v1

    .line 267868
    goto :goto_0

    .line 267869
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLComment;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 267858
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 267859
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLComment;

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    goto :goto_0
.end method

.method public static k(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 267846
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 267847
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 267848
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 267849
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->B()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 267850
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->B()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 267851
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    instance-of v2, v2, LX/16g;

    if-eqz v2, :cond_1

    .line 267852
    iget-object v2, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v2

    .line 267853
    invoke-static {v2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    .line 267854
    if-eqz v2, :cond_1

    .line 267855
    invoke-virtual {v1, v2}, LX/162;->a(LX/162;)LX/162;

    .line 267856
    :cond_1
    invoke-static {v0, v1}, LX/0x1;->a(LX/16g;LX/162;)V

    .line 267857
    return-object v1
.end method
