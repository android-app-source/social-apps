.class public final enum LX/1iv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1iv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1iv;

.field public static final enum HTTP_CLIENT_EXECUTE:LX/1iv;

.field public static final enum READ_RESPONSE_BODY:LX/1iv;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 299370
    new-instance v0, LX/1iv;

    const-string v1, "HTTP_CLIENT_EXECUTE"

    const-string v2, "http_client_execute"

    invoke-direct {v0, v1, v3, v2}, LX/1iv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1iv;->HTTP_CLIENT_EXECUTE:LX/1iv;

    .line 299371
    new-instance v0, LX/1iv;

    const-string v1, "READ_RESPONSE_BODY"

    const-string v2, "read_response_body"

    invoke-direct {v0, v1, v4, v2}, LX/1iv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1iv;->READ_RESPONSE_BODY:LX/1iv;

    .line 299372
    const/4 v0, 0x2

    new-array v0, v0, [LX/1iv;

    sget-object v1, LX/1iv;->HTTP_CLIENT_EXECUTE:LX/1iv;

    aput-object v1, v0, v3

    sget-object v1, LX/1iv;->READ_RESPONSE_BODY:LX/1iv;

    aput-object v1, v0, v4

    sput-object v0, LX/1iv;->$VALUES:[LX/1iv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 299373
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 299374
    iput-object p3, p0, LX/1iv;->mName:Ljava/lang/String;

    .line 299375
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1iv;
    .locals 1

    .prologue
    .line 299376
    const-class v0, LX/1iv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1iv;

    return-object v0
.end method

.method public static values()[LX/1iv;
    .locals 1

    .prologue
    .line 299377
    sget-object v0, LX/1iv;->$VALUES:[LX/1iv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1iv;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 299378
    iget-object v0, p0, LX/1iv;->mName:Ljava/lang/String;

    return-object v0
.end method
