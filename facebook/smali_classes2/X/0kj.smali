.class public final LX/0kj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/net/NetworkInfo;


# direct methods
.method public constructor <init>(Landroid/net/NetworkInfo;)V
    .locals 1

    .prologue
    .line 127768
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127769
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    iput-object v0, p0, LX/0kj;->a:Landroid/net/NetworkInfo;

    .line 127770
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 127771
    if-ne p0, p1, :cond_1

    .line 127772
    :cond_0
    :goto_0
    return v0

    .line 127773
    :cond_1
    instance-of v2, p1, LX/0kj;

    if-nez v2, :cond_2

    move v0, v1

    .line 127774
    goto :goto_0

    .line 127775
    :cond_2
    check-cast p1, LX/0kj;

    .line 127776
    iget-object v2, p0, LX/0kj;->a:Landroid/net/NetworkInfo;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    iget-object v3, p1, LX/0kj;->a:Landroid/net/NetworkInfo;

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    if-ne v2, v3, :cond_3

    iget-object v2, p0, LX/0kj;->a:Landroid/net/NetworkInfo;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v2

    iget-object v3, p1, LX/0kj;->a:Landroid/net/NetworkInfo;

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v3

    if-ne v2, v3, :cond_3

    iget-object v2, p0, LX/0kj;->a:Landroid/net/NetworkInfo;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v2

    iget-object v3, p1, LX/0kj;->a:Landroid/net/NetworkInfo;

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/NetworkInfo$State;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/0kj;->a:Landroid/net/NetworkInfo;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, LX/0kj;->a:Landroid/net/NetworkInfo;

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/0kj;->a:Landroid/net/NetworkInfo;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v2

    iget-object v3, p1, LX/0kj;->a:Landroid/net/NetworkInfo;

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v3

    if-ne v2, v3, :cond_3

    iget-object v2, p0, LX/0kj;->a:Landroid/net/NetworkInfo;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isFailover()Z

    move-result v2

    iget-object v3, p1, LX/0kj;->a:Landroid/net/NetworkInfo;

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isFailover()Z

    move-result v3

    if-ne v2, v3, :cond_3

    iget-object v2, p0, LX/0kj;->a:Landroid/net/NetworkInfo;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v2

    iget-object v3, p1, LX/0kj;->a:Landroid/net/NetworkInfo;

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v3

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
