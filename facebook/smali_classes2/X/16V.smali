.class public LX/16V;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/1AD",
            "<*>;>;",
            "Ljava/util/List",
            "<",
            "LX/16Y;",
            ">;>;"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/16V;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/Object;

.field private d:I

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 185352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185353
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/16V;->c:Ljava/lang/Object;

    .line 185354
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/16V;->a:Ljava/util/Map;

    .line 185355
    const/4 v0, 0x1

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/16V;->b:Ljava/util/List;

    .line 185356
    const/4 v0, 0x0

    iput v0, p0, LX/16V;->d:I

    .line 185357
    const/4 v0, 0x2

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/16V;->e:Ljava/util/List;

    .line 185358
    return-void
.end method

.method public static a(LX/16V;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 185340
    iget-object v1, p0, LX/16V;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 185341
    :try_start_0
    iget v0, p0, LX/16V;->d:I

    if-lez v0, :cond_0

    .line 185342
    iget-object v0, p0, LX/16V;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185343
    :goto_0
    monitor-exit v1

    return-void

    .line 185344
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 185345
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(LX/16V;)V
    .locals 1

    .prologue
    .line 185350
    new-instance v0, Lcom/facebook/common/eventbus/TypedEventBus$3;

    invoke-direct {v0, p0, p1}, Lcom/facebook/common/eventbus/TypedEventBus$3;-><init>(LX/16V;LX/16V;)V

    invoke-static {p0, v0}, LX/16V;->a(LX/16V;Ljava/lang/Runnable;)V

    .line 185351
    return-void
.end method

.method public final a(LX/1AD;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<HandlerType::",
            "LX/16Y;",
            ">(",
            "LX/1AD",
            "<THandlerType;>;)V"
        }
    .end annotation

    .prologue
    .line 185359
    iget-object v1, p0, LX/16V;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 185360
    :try_start_0
    iget v0, p0, LX/16V;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/16V;->d:I

    .line 185361
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 185362
    :try_start_1
    iget-object v0, p0, LX/16V;->a:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 185363
    if-eqz v0, :cond_0

    .line 185364
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16Y;

    .line 185365
    invoke-virtual {p1, v0}, LX/1AD;->a(LX/16Y;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 185366
    :catchall_0
    move-exception v0

    move-object v1, v0

    iget-object v2, p0, LX/16V;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 185367
    :try_start_2
    iget v0, p0, LX/16V;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/16V;->d:I

    .line 185368
    iget v0, p0, LX/16V;->d:I

    if-nez v0, :cond_5

    .line 185369
    iget-object v0, p0, LX/16V;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 185370
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    .line 185371
    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 185372
    :catchall_2
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v0

    .line 185373
    :cond_0
    :try_start_4
    iget-object v0, p0, LX/16V;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 185374
    iget-object v0, p0, LX/16V;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16V;

    .line 185375
    invoke-virtual {v0, p1}, LX/16V;->a(LX/1AD;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 185376
    :cond_1
    iget-object v1, p0, LX/16V;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 185377
    :try_start_5
    iget v0, p0, LX/16V;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/16V;->d:I

    .line 185378
    iget v0, p0, LX/16V;->d:I

    if-nez v0, :cond_3

    .line 185379
    iget-object v0, p0, LX/16V;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 185380
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_3

    .line 185381
    :catchall_3
    move-exception v0

    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v0

    .line 185382
    :cond_2
    :try_start_6
    iget-object v0, p0, LX/16V;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 185383
    :cond_3
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 185384
    return-void

    .line 185385
    :cond_4
    :try_start_7
    iget-object v0, p0, LX/16V;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 185386
    :cond_5
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v1
.end method

.method public final a(Ljava/lang/Class;LX/16Y;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<HandlerType::",
            "LX/16Y;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "LX/1AD",
            "<THandlerType;>;>;THandlerType;)V"
        }
    .end annotation

    .prologue
    .line 185348
    new-instance v0, Lcom/facebook/common/eventbus/TypedEventBus$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/common/eventbus/TypedEventBus$1;-><init>(LX/16V;Ljava/lang/Class;LX/16Y;)V

    invoke-static {p0, v0}, LX/16V;->a(LX/16V;Ljava/lang/Runnable;)V

    .line 185349
    return-void
.end method

.method public final b(Ljava/lang/Class;LX/16Y;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<HandlerType::",
            "LX/16Y;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "LX/1AD",
            "<THandlerType;>;>;THandlerType;)V"
        }
    .end annotation

    .prologue
    .line 185346
    new-instance v0, Lcom/facebook/common/eventbus/TypedEventBus$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/common/eventbus/TypedEventBus$2;-><init>(LX/16V;Ljava/lang/Class;LX/16Y;)V

    invoke-static {p0, v0}, LX/16V;->a(LX/16V;Ljava/lang/Runnable;)V

    .line 185347
    return-void
.end method
