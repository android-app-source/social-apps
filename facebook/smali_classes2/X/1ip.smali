.class public LX/1ip;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1iq;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/1ir;

.field private static volatile h:LX/1ip;


# instance fields
.field private final b:LX/1BA;

.field private final c:LX/0So;

.field private d:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private e:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private f:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private g:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 299211
    new-instance v0, LX/1ir;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, LX/1ir;-><init>(IJ)V

    sput-object v0, LX/1ip;->a:LX/1ir;

    return-void
.end method

.method public constructor <init>(LX/0So;LX/1BA;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 299212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 299213
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1ip;->d:Z

    .line 299214
    iput-object p1, p0, LX/1ip;->c:LX/0So;

    .line 299215
    iput-object p2, p0, LX/1ip;->b:LX/1BA;

    .line 299216
    iget-object v0, p0, LX/1ip;->b:LX/1BA;

    invoke-virtual {v0, p0}, LX/1BA;->a(LX/1iq;)V

    .line 299217
    return-void
.end method

.method public static a(LX/0QB;)LX/1ip;
    .locals 5

    .prologue
    .line 299218
    sget-object v0, LX/1ip;->h:LX/1ip;

    if-nez v0, :cond_1

    .line 299219
    const-class v1, LX/1ip;

    monitor-enter v1

    .line 299220
    :try_start_0
    sget-object v0, LX/1ip;->h:LX/1ip;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 299221
    if-eqz v2, :cond_0

    .line 299222
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 299223
    new-instance p0, LX/1ip;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {v0}, LX/1B6;->a(LX/0QB;)LX/1BA;

    move-result-object v4

    check-cast v4, LX/1BA;

    invoke-direct {p0, v3, v4}, LX/1ip;-><init>(LX/0So;LX/1BA;)V

    .line 299224
    move-object v0, p0

    .line 299225
    sput-object v0, LX/1ip;->h:LX/1ip;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 299226
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 299227
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 299228
    :cond_1
    sget-object v0, LX/1ip;->h:LX/1ip;

    return-object v0

    .line 299229
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 299230
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static declared-synchronized d(LX/1ip;)V
    .locals 6

    .prologue
    .line 299231
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/1ip;->e:I

    if-lez v0, :cond_0

    .line 299232
    iget-object v0, p0, LX/1ip;->b:LX/1BA;

    const v1, 0x4a000a

    iget-wide v2, p0, LX/1ip;->g:J

    iget-wide v4, p0, LX/1ip;->f:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, LX/1BA;->a(IJ)V

    .line 299233
    iget-object v0, p0, LX/1ip;->b:LX/1BA;

    const v1, 0x4a0009

    iget v2, p0, LX/1ip;->e:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, LX/1BA;->a(IJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299234
    :cond_0
    monitor-exit p0

    return-void

    .line 299235
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 299236
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1ip;->d:Z

    if-eqz v0, :cond_0

    .line 299237
    invoke-static {p0}, LX/1ip;->d(LX/1ip;)V

    .line 299238
    iget-object v0, p0, LX/1ip;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/1ip;->f:J

    .line 299239
    const/4 v0, 0x0

    iput v0, p0, LX/1ip;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299240
    :cond_0
    monitor-exit p0

    return-void

    .line 299241
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 3

    .prologue
    .line 299242
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1ip;->b:LX/1BA;

    const v1, 0x4a000b

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, LX/1BA;->a(IS)V

    .line 299243
    iget-boolean v0, p0, LX/1ip;->d:Z

    if-eqz v0, :cond_0

    .line 299244
    invoke-static {p0}, LX/1ip;->d(LX/1ip;)V

    .line 299245
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1ip;->d:Z

    .line 299246
    const/4 v0, 0x0

    iput v0, p0, LX/1ip;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299247
    :cond_0
    monitor-exit p0

    return-void

    .line 299248
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 3

    .prologue
    .line 299249
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1ip;->b:LX/1BA;

    const v1, 0x4a000b

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, LX/1BA;->a(IS)V

    .line 299250
    iget v0, p0, LX/1ip;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1ip;->e:I

    .line 299251
    iget-object v0, p0, LX/1ip;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/1ip;->g:J

    .line 299252
    iget-boolean v0, p0, LX/1ip;->d:Z

    if-nez v0, :cond_0

    .line 299253
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1ip;->d:Z

    .line 299254
    iget-wide v0, p0, LX/1ip;->g:J

    iput-wide v0, p0, LX/1ip;->f:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299255
    :cond_0
    monitor-exit p0

    return-void

    .line 299256
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
