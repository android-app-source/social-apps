.class public final LX/1PD;
.super LX/1OX;
.source ""


# instance fields
.field public final synthetic a:LX/0g7;


# direct methods
.method public constructor <init>(LX/0g7;)V
    .locals 0

    .prologue
    .line 243685
    iput-object p1, p0, LX/1PD;->a:LX/0g7;

    invoke-direct {p0}, LX/1OX;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 4

    .prologue
    .line 243686
    iget-object v0, p0, LX/1PD;->a:LX/0g7;

    .line 243687
    iput p2, v0, LX/0g7;->m:I

    .line 243688
    iget-object v0, p0, LX/1PD;->a:LX/0g7;

    iget-object v0, v0, LX/0g7;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 243689
    iget-object v0, p0, LX/1PD;->a:LX/0g7;

    iget-object v0, v0, LX/0g7;->e:LX/0fx;

    if-eqz v0, :cond_0

    .line 243690
    iget-object v0, p0, LX/1PD;->a:LX/0g7;

    iget-object v0, v0, LX/0g7;->e:LX/0fx;

    iget-object v1, p0, LX/1PD;->a:LX/0g7;

    invoke-interface {v0, v1, p2}, LX/0fx;->a(LX/0g8;I)V

    .line 243691
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 243692
    iget-object v0, p0, LX/1PD;->a:LX/0g7;

    iget-object v0, v0, LX/0g7;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fx;

    iget-object v3, p0, LX/1PD;->a:LX/0g7;

    invoke-interface {v0, v3, p2}, LX/0fx;->a(LX/0g8;I)V

    .line 243693
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 243694
    :cond_1
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 243695
    iget-object v0, p0, LX/1PD;->a:LX/0g7;

    iget-object v0, v0, LX/0g7;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 243696
    iget-object v0, p0, LX/1PD;->a:LX/0g7;

    iget-object v0, v0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getBetterLayoutManager()LX/1OS;

    move-result-object v0

    .line 243697
    invoke-interface {v0}, LX/1OS;->J()V

    .line 243698
    iget-object v1, p0, LX/1PD;->a:LX/0g7;

    invoke-interface {v0}, LX/1OS;->I()I

    move-result v4

    .line 243699
    iput v4, v1, LX/0g7;->k:I

    .line 243700
    iget-object v1, p0, LX/1PD;->a:LX/0g7;

    iget v1, v1, LX/0g7;->k:I

    const/4 v4, -0x1

    if-ne v1, v4, :cond_0

    .line 243701
    :goto_0
    return-void

    .line 243702
    :cond_0
    iget-object v1, p0, LX/1PD;->a:LX/0g7;

    invoke-interface {v0}, LX/1OS;->n()I

    move-result v0

    .line 243703
    iput v0, v1, LX/0g7;->l:I

    .line 243704
    iget-object v0, p0, LX/1PD;->a:LX/0g7;

    iget v0, v0, LX/0g7;->l:I

    iget-object v1, p0, LX/1PD;->a:LX/0g7;

    iget v1, v1, LX/0g7;->k:I

    sub-int/2addr v0, v1

    add-int/lit8 v4, v0, 0x1

    .line 243705
    iget-object v0, p0, LX/1PD;->a:LX/0g7;

    invoke-virtual {v0}, LX/0g7;->s()I

    move-result v5

    .line 243706
    iget-object v0, p0, LX/1PD;->a:LX/0g7;

    .line 243707
    iput p3, v0, LX/0g7;->a:I

    .line 243708
    iget-object v0, p0, LX/1PD;->a:LX/0g7;

    iget-object v0, v0, LX/0g7;->e:LX/0fx;

    if-eqz v0, :cond_1

    .line 243709
    iget-object v0, p0, LX/1PD;->a:LX/0g7;

    iget-object v0, v0, LX/0g7;->e:LX/0fx;

    iget-object v1, p0, LX/1PD;->a:LX/0g7;

    iget-object v6, p0, LX/1PD;->a:LX/0g7;

    iget v6, v6, LX/0g7;->k:I

    invoke-interface {v0, v1, v6, v4, v5}, LX/0fx;->a(LX/0g8;III)V

    :cond_1
    move v1, v2

    .line 243710
    :goto_1
    if-ge v1, v3, :cond_2

    .line 243711
    iget-object v0, p0, LX/1PD;->a:LX/0g7;

    iget-object v0, v0, LX/0g7;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fx;

    iget-object v6, p0, LX/1PD;->a:LX/0g7;

    iget-object v7, p0, LX/1PD;->a:LX/0g7;

    iget v7, v7, LX/0g7;->k:I

    invoke-interface {v0, v6, v7, v4, v5}, LX/0fx;->a(LX/0g8;III)V

    .line 243712
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 243713
    :cond_2
    iget-object v0, p0, LX/1PD;->a:LX/0g7;

    .line 243714
    iput v2, v0, LX/0g7;->a:I

    .line 243715
    goto :goto_0
.end method
