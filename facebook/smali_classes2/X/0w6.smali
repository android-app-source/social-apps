.class public LX/0w6;
.super LX/0w5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/flatbuffers/Flattenable;",
        ">",
        "LX/0w5",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 159253
    invoke-direct {p0}, LX/0w5;-><init>()V

    .line 159254
    iput-object p1, p0, LX/0w6;->a:Ljava/lang/Class;

    .line 159255
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 159251
    iget-object v0, p0, LX/0w6;->a:Ljava/lang/Class;

    const-class v1, Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;

    .line 159252
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/15i;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 159250
    iget-object v0, p0, LX/0w6;->a:Ljava/lang/Class;

    invoke-virtual {p1, p2, v0}, LX/15i;->a(ILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15w;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 159249
    iget-object v0, p0, LX/0w6;->a:Ljava/lang/Class;

    invoke-virtual {p1, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    return-object v0
.end method

.method public final a(LX/0lC;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 4

    .prologue
    .line 159246
    iget-object v0, p1, LX/0lC;->_typeFactory:LX/0li;

    move-object v0, v0

    .line 159247
    const-class v1, Ljava/util/HashMap;

    const-class v2, Ljava/lang/String;

    iget-object v3, p0, LX/0w6;->a:Ljava/lang/Class;

    invoke-virtual {v0, v1, v2, v3}, LX/0li;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;)LX/1Xn;

    move-result-object v0

    .line 159248
    invoke-virtual {p1, p2, v0}, LX/0lC;->a(Ljava/lang/String;LX/0lJ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(LX/15i;II)Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15i;",
            "II)",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 159245
    iget-object v0, p0, LX/0w6;->a:Ljava/lang/Class;

    invoke-virtual {p1, p2, p3, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15i;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 159242
    iget-object v0, p0, LX/0w6;->a:Ljava/lang/Class;

    .line 159243
    const/4 p0, 0x0

    invoke-static {p1, v0, p0}, LX/4Bz;->a(LX/15i;Ljava/lang/Class;LX/16a;)Ljava/util/List;

    move-result-object p0

    move-object v0, p0

    .line 159244
    return-object v0
.end method

.method public final a(Ljava/lang/Class;)Z
    .locals 1

    .prologue
    .line 159241
    iget-object v0, p0, LX/0w6;->a:Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/0lC;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 159256
    iget-object v0, p0, LX/0w6;->a:Ljava/lang/Class;

    invoke-virtual {p1, p2, v0}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    return-object v0
.end method

.method public final b(LX/15i;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 159222
    iget-object v0, p0, LX/0w6;->a:Ljava/lang/Class;

    invoke-virtual {p1, v0}, LX/15i;->a(Ljava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159240
    iget-object v0, p0, LX/0w6;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159239
    iget-object v0, p0, LX/0w6;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159238
    iget-object v0, p0, LX/0w6;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()[B
    .locals 4

    .prologue
    .line 159228
    const/4 v3, 0x0

    .line 159229
    new-instance v0, LX/186;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 159230
    iget-object v1, p0, LX/0w6;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 159231
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 159232
    invoke-virtual {v0, v3, v3, v3}, LX/186;->a(III)V

    .line 159233
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 159234
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 159235
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 159236
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    move-object v0, v0

    .line 159237
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 159225
    instance-of v0, p1, LX/0w6;

    if-nez v0, :cond_0

    .line 159226
    const/4 v0, 0x0

    .line 159227
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/0w6;->a:Ljava/lang/Class;

    check-cast p1, LX/0w6;

    iget-object v1, p1, LX/0w6;->a:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 159224
    iget-object v0, p0, LX/0w6;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159223
    iget-object v0, p0, LX/0w6;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
