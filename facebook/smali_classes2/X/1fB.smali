.class public LX/1fB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Gd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1Gd",
        "<",
        "LX/1ca",
        "<TT;>;>;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<TT;>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<TT;>;>;>;)V"
        }
    .end annotation

    .prologue
    .line 291067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291068
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "List of suppliers is empty!"

    invoke-static {v0, v1}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 291069
    iput-object p1, p0, LX/1fB;->a:Ljava/util/List;

    .line 291070
    return-void

    .line 291071
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 291072
    new-instance v0, LX/1fl;

    invoke-direct {v0, p0}, LX/1fl;-><init>(LX/1fB;)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 291073
    if-ne p1, p0, :cond_0

    .line 291074
    const/4 v0, 0x1

    .line 291075
    :goto_0
    return v0

    .line 291076
    :cond_0
    instance-of v0, p1, LX/1fB;

    if-nez v0, :cond_1

    .line 291077
    const/4 v0, 0x0

    goto :goto_0

    .line 291078
    :cond_1
    check-cast p1, LX/1fB;

    .line 291079
    iget-object v0, p0, LX/1fB;->a:Ljava/util/List;

    iget-object v1, p1, LX/1fB;->a:Ljava/util/List;

    invoke-static {v0, v1}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 291080
    iget-object v0, p0, LX/1fB;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 291081
    invoke-static {p0}, LX/15f;->a(Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "list"

    iget-object v2, p0, LX/1fB;->a:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    invoke-virtual {v0}, LX/15g;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
