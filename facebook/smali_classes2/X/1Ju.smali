.class public LX/1Ju;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Item:",
        "Ljava/lang/Object;",
        "Snapshot::",
        "LX/9Ad",
        "<TItem;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/1Jv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/workingrange/core/WorkingRange",
            "<TItem;TSnapshot;>;"
        }
    .end annotation
.end field

.field public final b:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Lcom/facebook/workingrange/core/WorkingRangeListener",
            "<TItem;TSnapshot;>;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<TItem;>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<TItem;>;"
        }
    .end annotation
.end field

.field public e:LX/1Jw;


# direct methods
.method public constructor <init>(LX/1Jv;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/workingrange/core/WorkingRange",
            "<TItem;TSnapshot;>;)V"
        }
    .end annotation

    .prologue
    const/16 v1, 0x10

    .line 230644
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230645
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/1Ju;->b:LX/01J;

    .line 230646
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, LX/1Ju;->c:Ljava/util/HashSet;

    .line 230647
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, LX/1Ju;->d:Ljava/util/HashSet;

    .line 230648
    sget-object v0, LX/1Jw;->a:LX/1Jw;

    iput-object v0, p0, LX/1Ju;->e:LX/1Jw;

    .line 230649
    iput-object p1, p0, LX/1Ju;->a:LX/1Jv;

    .line 230650
    return-void
.end method

.method public static a(LX/1Ju;LX/1Jw;LX/9Ad;)V
    .locals 6
    .param p0    # LX/1Ju;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Jw;",
            "TSnapshot;)V"
        }
    .end annotation

    .prologue
    .line 230651
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/1Jw;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 230652
    :cond_0
    return-void

    .line 230653
    :cond_1
    iget v0, p1, LX/1Jw;->d:I

    move v0, v0

    .line 230654
    iget v1, p1, LX/1Jw;->e:I

    move v3, v1

    .line 230655
    move v2, v0

    .line 230656
    :goto_0
    if-gt v2, v3, :cond_0

    .line 230657
    invoke-interface {p2, v2}, LX/9Ad;->a(I)Ljava/lang/Object;

    move-result-object v4

    .line 230658
    invoke-static {v4}, LX/1Ju;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 230659
    if-eqz v4, :cond_3

    iget-object v0, p0, LX/1Ju;->c:Ljava/util/HashSet;

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 230660
    :goto_1
    const/4 v0, 0x0

    iget-object v1, p0, LX/1Ju;->b:LX/01J;

    invoke-virtual {v1}, LX/01J;->size()I

    move-result v5

    move v1, v0

    :goto_2
    if-ge v1, v5, :cond_2

    .line 230661
    iget-object v0, p0, LX/1Ju;->b:LX/01J;

    invoke-virtual {v0, v1}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1K0;

    invoke-virtual {v0, v4, v2, p2}, LX/1K0;->a(Ljava/lang/Object;ILX/9Ad;)V

    .line 230662
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 230663
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    goto :goto_1
.end method

.method public static a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 230633
    if-nez p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/1Ju;LX/1Jw;LX/9Ad;)V
    .locals 4
    .param p0    # LX/1Ju;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Jw;",
            "TSnapshot;)V"
        }
    .end annotation

    .prologue
    .line 230634
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/1Jw;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 230635
    :cond_0
    return-void

    .line 230636
    :cond_1
    iget v0, p1, LX/1Jw;->d:I

    move v0, v0

    .line 230637
    iget v1, p1, LX/1Jw;->e:I

    move v1, v1

    .line 230638
    :goto_0
    if-gt v0, v1, :cond_0

    .line 230639
    invoke-interface {p2, v0}, LX/9Ad;->a(I)Ljava/lang/Object;

    move-result-object v2

    .line 230640
    invoke-static {v2}, LX/1Ju;->a(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 230641
    invoke-static {p0, v2}, LX/1Ju;->b(LX/1Ju;Ljava/lang/Object;)Z

    .line 230642
    iget-object v2, p0, LX/1Ju;->b:LX/01J;

    invoke-virtual {v2}, LX/01J;->size()I

    .line 230643
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static b(LX/1Ju;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TItem;)Z"
        }
    .end annotation

    .prologue
    .line 230591
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/1Ju;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/9Ad;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TSnapshot;)V"
        }
    .end annotation

    .prologue
    .line 230592
    iget-object v0, p0, LX/1Ju;->a:LX/1Jv;

    invoke-virtual {v0, p1}, LX/1Jv;->a(LX/9Ad;)LX/1Jw;

    move-result-object v0

    .line 230593
    iget-object v1, p0, LX/1Ju;->e:LX/1Jw;

    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, LX/1Jw;->c:LX/1Jw;

    if-ne v0, v1, :cond_1

    .line 230594
    :cond_0
    :goto_0
    return-void

    .line 230595
    :cond_1
    iget-object v1, p0, LX/1Ju;->e:LX/1Jw;

    invoke-virtual {v1, v0}, LX/1Jw;->a(LX/1Jw;)LX/1Jx;

    move-result-object v1

    .line 230596
    iget-object v2, p0, LX/1Ju;->e:LX/1Jw;

    invoke-virtual {v0, v2}, LX/1Jw;->a(LX/1Jw;)LX/1Jx;

    move-result-object v2

    .line 230597
    iget-object v3, v2, LX/1Jx;->b:LX/1Jw;

    move-object v3, v3

    .line 230598
    invoke-static {p0, v3, p1}, LX/1Ju;->b(LX/1Ju;LX/1Jw;LX/9Ad;)V

    .line 230599
    iget-object v3, v2, LX/1Jx;->c:LX/1Jw;

    move-object v3, v3

    .line 230600
    invoke-static {p0, v3, p1}, LX/1Ju;->b(LX/1Ju;LX/1Jw;LX/9Ad;)V

    .line 230601
    iget-object v3, v1, LX/1Jx;->b:LX/1Jw;

    move-object v3, v3

    .line 230602
    invoke-static {p0, v3, p1}, LX/1Ju;->a(LX/1Ju;LX/1Jw;LX/9Ad;)V

    .line 230603
    iget-object v3, v1, LX/1Jx;->c:LX/1Jw;

    move-object v3, v3

    .line 230604
    invoke-static {p0, v3, p1}, LX/1Ju;->a(LX/1Ju;LX/1Jw;LX/9Ad;)V

    .line 230605
    invoke-static {v1}, LX/A8e;->a(LX/1Jx;)V

    .line 230606
    invoke-static {v2}, LX/A8e;->a(LX/1Jx;)V

    .line 230607
    iget-object v1, p0, LX/1Ju;->e:LX/1Jw;

    invoke-static {v1}, LX/A8e;->a(LX/1Jw;)V

    .line 230608
    iput-object v0, p0, LX/1Ju;->e:LX/1Jw;

    .line 230609
    goto :goto_0
.end method

.method public final b(LX/9Ad;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TSnapshot;)V"
        }
    .end annotation

    .prologue
    .line 230610
    iget-object v0, p0, LX/1Ju;->a:LX/1Jv;

    invoke-virtual {v0, p1}, LX/1Jv;->a(LX/9Ad;)LX/1Jw;

    move-result-object v0

    .line 230611
    sget-object v1, LX/1Jw;->c:LX/1Jw;

    if-ne v0, v1, :cond_0

    .line 230612
    :goto_0
    return-void

    .line 230613
    :cond_0
    iget-object v1, p0, LX/1Ju;->d:Ljava/util/HashSet;

    iget-object v2, p0, LX/1Ju;->c:Ljava/util/HashSet;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 230614
    iget-object v1, p0, LX/1Ju;->d:Ljava/util/HashSet;

    .line 230615
    invoke-virtual {v0}, LX/1Jw;->e()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 230616
    :cond_1
    iget-object v1, p0, LX/1Ju;->d:Ljava/util/HashSet;

    .line 230617
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 230618
    const/4 v2, 0x0

    iget-object v3, p0, LX/1Ju;->b:LX/01J;

    invoke-virtual {v3}, LX/01J;->size()I

    move-result v6

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_2

    .line 230619
    iget-object v2, p0, LX/1Ju;->b:LX/01J;

    invoke-virtual {v2, v3}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1K0;

    const/high16 v7, -0x80000000

    invoke-virtual {v2, v5, v7, p1}, LX/1K0;->a(Ljava/lang/Object;ILX/9Ad;)V

    .line 230620
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 230621
    :cond_3
    iget-object v1, p0, LX/1Ju;->d:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 230622
    iget-object v1, p0, LX/1Ju;->e:LX/1Jw;

    invoke-static {v1}, LX/A8e;->a(LX/1Jw;)V

    .line 230623
    iput-object v0, p0, LX/1Ju;->e:LX/1Jw;

    goto :goto_0

    .line 230624
    :cond_4
    iget v2, v0, LX/1Jw;->d:I

    move v2, v2

    .line 230625
    iget v3, v0, LX/1Jw;->e:I

    move v3, v3

    .line 230626
    :goto_2
    if-gt v2, v3, :cond_1

    .line 230627
    invoke-interface {p1, v2}, LX/9Ad;->a(I)Ljava/lang/Object;

    move-result-object v4

    .line 230628
    invoke-static {v4}, LX/1Ju;->a(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 230629
    invoke-virtual {v1, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 230630
    invoke-static {p0, v4}, LX/1Ju;->b(LX/1Ju;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 230631
    iget-object v4, p0, LX/1Ju;->b:LX/01J;

    invoke-virtual {v4}, LX/01J;->size()I

    .line 230632
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method
