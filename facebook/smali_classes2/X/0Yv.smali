.class public LX/0Yv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0VI;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0Yv;


# instance fields
.field public a:LX/0Yw;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 82744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82745
    new-instance v0, LX/0Yw;

    const v1, 0x7fffffff

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, LX/0Yw;-><init>(II)V

    iput-object v0, p0, LX/0Yv;->a:LX/0Yw;

    .line 82746
    return-void
.end method

.method public static a(LX/0QB;)LX/0Yv;
    .locals 3

    .prologue
    .line 82747
    sget-object v0, LX/0Yv;->b:LX/0Yv;

    if-nez v0, :cond_1

    .line 82748
    const-class v1, LX/0Yv;

    monitor-enter v1

    .line 82749
    :try_start_0
    sget-object v0, LX/0Yv;->b:LX/0Yv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 82750
    if-eqz v2, :cond_0

    .line 82751
    :try_start_1
    new-instance v0, LX/0Yv;

    invoke-direct {v0}, LX/0Yv;-><init>()V

    .line 82752
    move-object v0, v0

    .line 82753
    sput-object v0, LX/0Yv;->b:LX/0Yv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82754
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 82755
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 82756
    :cond_1
    sget-object v0, LX/0Yv;->b:LX/0Yv;

    return-object v0

    .line 82757
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 82758
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82759
    const-string v0, "fragment_intent_actions"

    return-object v0
.end method

.method public final a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82760
    iget-object v0, p0, LX/0Yv;->a:LX/0Yw;

    invoke-virtual {v0}, LX/0Yw;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
