.class public LX/1p6;
.super LX/04p;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field private static final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final e:LX/0Tn;

.field private static volatile k:LX/1p6;


# instance fields
.field private final f:LX/0dN;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final i:LX/0Xl;

.field public volatile j:LX/04q;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 327860
    const-class v0, LX/1p6;

    sput-object v0, LX/1p6;->d:Ljava/lang/Class;

    .line 327861
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "sandbox/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 327862
    sput-object v0, LX/1p6;->a:LX/0Tn;

    const-string v1, "mqtt/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 327863
    sput-object v0, LX/1p6;->e:LX/0Tn;

    const-string v1, "server_tier"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1p6;->b:LX/0Tn;

    .line 327864
    sget-object v0, LX/1p6;->e:LX/0Tn;

    const-string v1, "sandbox"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1p6;->c:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Xl;)V
    .locals 3
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/config/server/ShouldUsePreferredConfig;
        .end annotation
    .end param
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/GlobalFbBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Xl;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 327850
    invoke-direct {p0}, LX/04p;-><init>()V

    .line 327851
    iput-object p1, p0, LX/1p6;->g:LX/0Or;

    .line 327852
    iput-object p2, p0, LX/1p6;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 327853
    iput-object p3, p0, LX/1p6;->i:LX/0Xl;

    .line 327854
    new-instance v0, LX/1p7;

    invoke-direct {v0, p0}, LX/1p7;-><init>(LX/1p6;)V

    iput-object v0, p0, LX/1p6;->f:LX/0dN;

    .line 327855
    sget-object v0, LX/1p6;->b:LX/0Tn;

    sget-object v1, LX/1p6;->c:LX/0Tn;

    sget-object v2, LX/1p8;->b:LX/0Tn;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    .line 327856
    iget-object v1, p0, LX/1p6;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, p0, LX/1p6;->f:LX/0dN;

    invoke-interface {v1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;LX/0dN;)V

    .line 327857
    invoke-static {}, LX/04q;->a()LX/04q;

    move-result-object v0

    iput-object v0, p0, LX/1p6;->j:LX/04q;

    .line 327858
    iget-object v0, p0, LX/1p6;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    new-instance v1, Lcom/facebook/mqttlite/MqttConnectionConfigManager$2;

    invoke-direct {v1, p0}, Lcom/facebook/mqttlite/MqttConnectionConfigManager$2;-><init>(LX/1p6;)V

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/lang/Runnable;)V

    .line 327859
    return-void
.end method

.method public static a(LX/0QB;)LX/1p6;
    .locals 6

    .prologue
    .line 327837
    sget-object v0, LX/1p6;->k:LX/1p6;

    if-nez v0, :cond_1

    .line 327838
    const-class v1, LX/1p6;

    monitor-enter v1

    .line 327839
    :try_start_0
    sget-object v0, LX/1p6;->k:LX/1p6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 327840
    if-eqz v2, :cond_0

    .line 327841
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 327842
    new-instance v5, LX/1p6;

    const/16 v3, 0x1474

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0aW;->a(LX/0QB;)LX/0aW;

    move-result-object v4

    check-cast v4, LX/0Xl;

    invoke-direct {v5, p0, v3, v4}, LX/1p6;-><init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Xl;)V

    .line 327843
    move-object v0, v5

    .line 327844
    sput-object v0, LX/1p6;->k:LX/1p6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 327845
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 327846
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 327847
    :cond_1
    sget-object v0, LX/1p6;->k:LX/1p6;

    return-object v0

    .line 327848
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 327849
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/1p6;LX/0Tn;)Lorg/json/JSONObject;
    .locals 5

    .prologue
    .line 327829
    iget-object v0, p0, LX/1p6;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-string v1, ""

    invoke-interface {v0, p1, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 327830
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 327831
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327832
    :goto_0
    return-object v0

    .line 327833
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 327834
    goto :goto_0

    .line 327835
    :catch_0
    move-exception v1

    .line 327836
    sget-object v2, LX/1p6;->d:Ljava/lang/Class;

    const-string v3, ""

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static f(LX/1p6;)V
    .locals 0

    .prologue
    .line 327826
    invoke-virtual {p0}, LX/04p;->a()V

    .line 327827
    invoke-virtual {p0}, LX/04p;->c()V

    .line 327828
    return-void
.end method

.method private g()LX/04q;
    .locals 6

    .prologue
    .line 327804
    sget-object v0, LX/1p8;->b:LX/0Tn;

    invoke-static {p0, v0}, LX/1p6;->a(LX/1p6;LX/0Tn;)Lorg/json/JSONObject;

    move-result-object v0

    .line 327805
    invoke-virtual {p0, v0}, LX/04p;->a(Lorg/json/JSONObject;)V

    .line 327806
    iget-object v1, p0, LX/1p6;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    .line 327807
    :cond_0
    :goto_0
    invoke-static {v0}, LX/04q;->a(Lorg/json/JSONObject;)LX/04q;

    move-result-object v0

    return-object v0

    .line 327808
    :cond_1
    iget-object v1, p0, LX/1p6;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/1p6;->b:LX/0Tn;

    const-string v3, "default"

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 327809
    const-string v2, "sandbox"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327810
    iget-object v1, p0, LX/1p6;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/1p6;->c:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 327811
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 327812
    const/4 v5, 0x1

    const/4 p0, 0x0

    .line 327813
    :try_start_0
    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 327814
    const-string v2, ":"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    .line 327815
    const/4 v3, 0x0

    aget-object v1, v2, v3

    .line 327816
    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 327817
    :goto_1
    const-string v3, "host_name_v6"

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 327818
    const-string v3, "default_port"

    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 327819
    const-string v3, "backup_port"

    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 327820
    const-string v2, "use_ssl"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 327821
    const-string v2, "use_compression"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 327822
    :goto_2
    goto :goto_0

    .line 327823
    :cond_2
    const/16 v2, 0x22b3

    goto :goto_1

    .line 327824
    :catch_0
    move-exception v2

    .line 327825
    const-string v3, "ConnectionConfigManager"

    const-string v4, "Failed to parse mqtt sandbox URL"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, p0

    invoke-static {v3, v4, v5}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 327795
    invoke-direct {p0}, LX/1p6;->g()LX/04q;

    move-result-object v0

    .line 327796
    iget-object v1, p0, LX/1p6;->j:LX/04q;

    invoke-virtual {v0, v1}, LX/04q;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327797
    :goto_0
    return-void

    .line 327798
    :cond_0
    iput-object v0, p0, LX/1p6;->j:LX/04q;

    goto :goto_0
.end method

.method public final b()LX/04q;
    .locals 1

    .prologue
    .line 327803
    iget-object v0, p0, LX/1p6;->j:LX/04q;

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 327801
    iget-object v0, p0, LX/1p6;->i:LX/0Xl;

    const-string v1, "com.facebook.rti.mqtt.ACTION_MQTT_CONFIG_CHANGED"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 327802
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 327799
    sget-object v0, LX/1p8;->b:LX/0Tn;

    invoke-static {p0, v0}, LX/1p6;->a(LX/1p6;LX/0Tn;)Lorg/json/JSONObject;

    move-result-object v0

    .line 327800
    const-string v1, "host_name_v6"

    const-string v2, "mqtt-mini.facebook.com"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
