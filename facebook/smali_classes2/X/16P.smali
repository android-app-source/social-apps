.class public LX/16P;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field private final c:LX/0i1;

.field private d:Lcom/facebook/interstitial/api/FetchInterstitialResult;

.field private e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 185158
    const-class v0, LX/16P;

    sput-object v0, LX/16P;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0i1;)V
    .locals 1

    .prologue
    .line 185154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185155
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0i1;

    iput-object v0, p0, LX/16P;->c:LX/0i1;

    .line 185156
    invoke-interface {p1}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/16P;->a:Ljava/lang/String;

    .line 185157
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 185126
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/16P;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/interstitial/api/FetchInterstitialResult;LX/03V;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 185143
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 185144
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 185145
    iput-object p1, p0, LX/16P;->d:Lcom/facebook/interstitial/api/FetchInterstitialResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185146
    :try_start_1
    iget-object v1, p0, LX/16P;->c:LX/0i1;

    iget-object v2, p1, Lcom/facebook/interstitial/api/FetchInterstitialResult;->data:Landroid/os/Parcelable;

    invoke-interface {v1, v2}, LX/0i1;->a(Landroid/os/Parcelable;)V

    .line 185147
    iget-object v1, p0, LX/16P;->c:LX/0i1;

    iget-wide v2, p1, Lcom/facebook/interstitial/api/FetchInterstitialResult;->fetchTimeMs:J

    invoke-interface {v1, v2, v3}, LX/0i1;->a(J)V

    .line 185148
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/16P;->e:Z
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 185149
    :goto_0
    monitor-exit p0

    return v0

    .line 185150
    :catch_0
    move-exception v0

    .line 185151
    :try_start_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/16P;->b:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_prepareController"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 185152
    const/4 v0, 0x0

    goto :goto_0

    .line 185153
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/0i1;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 185139
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/16P;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 185140
    const/4 v0, 0x0

    .line 185141
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/16P;->c:LX/0i1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 185142
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Lcom/facebook/interstitial/api/FetchInterstitialResult;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 185138
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/16P;->d:Lcom/facebook/interstitial/api/FetchInterstitialResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185133
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/16P;->e()LX/0Px;

    move-result-object v0

    .line 185134
    if-nez v0, :cond_0

    .line 185135
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185136
    :cond_0
    monitor-exit p0

    return-object v0

    .line 185137
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 185128
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/16P;->b()LX/0i1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 185129
    if-nez v0, :cond_0

    .line 185130
    const/4 v0, 0x0

    .line 185131
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-interface {v0}, LX/0i1;->c()LX/0Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 185132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 185127
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "isInited"

    iget-boolean v2, p0, LX/16P;->e:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v1

    const-string v2, "InterstitialId"

    iget-object v0, p0, LX/16P;->c:LX/0i1;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/16P;->c:LX/0i1;

    invoke-interface {v0}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "FetchInterstitialResult"

    iget-object v2, p0, LX/16P;->d:Lcom/facebook/interstitial/api/FetchInterstitialResult;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
