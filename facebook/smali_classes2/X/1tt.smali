.class public LX/1tt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1tr;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0ka;

.field private d:LX/1tu;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0ka;LX/1tu;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/rtcpresence/annotations/IsVoipEnabledForUser;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/rtcpresence/annotations/CanReceiveRtcConferencingPeerToPeer;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0ka;",
            "Lcom/facebook/push/mqtt/capability/MqttVoipCapability;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 336855
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336856
    iput-object p1, p0, LX/1tt;->a:LX/0Or;

    .line 336857
    iput-object p2, p0, LX/1tt;->b:LX/0Or;

    .line 336858
    iput-object p3, p0, LX/1tt;->c:LX/0ka;

    .line 336859
    iput-object p4, p0, LX/1tt;->d:LX/1tu;

    .line 336860
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/EnumSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "LX/1tp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 336861
    iget-object v0, p0, LX/1tt;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 336862
    const-class v0, LX/1tp;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 336863
    :goto_0
    return-object v0

    .line 336864
    :cond_0
    iget-object v0, p0, LX/1tt;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 336865
    sget-object v0, LX/1tp;->VOIP:LX/1tp;

    sget-object v1, LX/1tp;->VOIP_WEB:LX/1tp;

    sget-object v2, LX/1tp;->ONE_ON_ONE_OVER_MULTIWAY:LX/1tp;

    invoke-static {v0, v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    goto :goto_0

    .line 336866
    :cond_1
    sget-object v0, LX/1tp;->VOIP:LX/1tp;

    sget-object v1, LX/1tp;->VOIP_WEB:LX/1tp;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    goto :goto_0
.end method
