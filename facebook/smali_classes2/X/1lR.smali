.class public LX/1lR;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1lR;


# instance fields
.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field public f:I

.field public g:I

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 311879
    new-instance v0, LX/1lR;

    .line 311880
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 311881
    invoke-direct {v0, v1}, LX/1lR;-><init>(Ljava/util/List;)V

    sput-object v0, LX/1lR;->a:LX/1lR;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 311913
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311914
    iput v0, p0, LX/1lR;->e:I

    .line 311915
    iput-boolean v0, p0, LX/1lR;->h:Z

    .line 311916
    iput-boolean v0, p0, LX/1lR;->i:Z

    .line 311917
    iput-boolean v0, p0, LX/1lR;->j:Z

    .line 311918
    iput-boolean v0, p0, LX/1lR;->k:Z

    .line 311919
    iput-boolean v0, p0, LX/1lR;->l:Z

    .line 311920
    if-eqz p1, :cond_2

    .line 311921
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/1lR;->b:LX/0Px;

    .line 311922
    iget-object v0, p0, LX/1lR;->b:LX/0Px;

    new-instance v1, LX/1lS;

    invoke-direct {v1, p0}, LX/1lS;-><init>(LX/1lR;)V

    invoke-static {v0, v1}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/1lR;->c:LX/0Px;

    .line 311923
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 311924
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/media/util/model/MediaModel;

    .line 311925
    iget-object v3, v0, Lcom/facebook/media/util/model/MediaModel;->b:LX/1po;

    move-object v3, v3

    .line 311926
    sget-object v4, LX/1po;->PHOTO:LX/1po;

    if-ne v3, v4, :cond_0

    .line 311927
    iget-object v3, v0, Lcom/facebook/media/util/model/MediaModel;->a:Ljava/lang/String;

    move-object v0, v3

    .line 311928
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 311929
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/1lR;->d:LX/0Px;

    .line 311930
    :goto_1
    return-void

    .line 311931
    :cond_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 311932
    iput-object v0, p0, LX/1lR;->b:LX/0Px;

    .line 311933
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 311934
    iput-object v0, p0, LX/1lR;->d:LX/0Px;

    iput-object v0, p0, LX/1lR;->c:LX/0Px;

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 311911
    iget-object v0, p0, LX/1lR;->b:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 311912
    iget-object v0, p0, LX/1lR;->b:LX/0Px;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 311910
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v0, p0, LX/1lR;->l:Z

    if-eqz v0, :cond_0

    const-string v0, "476866155826452:"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/1lR;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "824928570957117:"

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 311909
    iget-boolean v0, p0, LX/1lR;->l:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->MOVIE_EVERGREEN:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 311908
    iget-object v0, p0, LX/1lR;->b:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1lR;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 311883
    if-ne p0, p1, :cond_1

    .line 311884
    :cond_0
    :goto_0
    return v0

    .line 311885
    :cond_1
    instance-of v2, p1, LX/1lR;

    if-nez v2, :cond_2

    move v0, v1

    .line 311886
    goto :goto_0

    .line 311887
    :cond_2
    check-cast p1, LX/1lR;

    .line 311888
    iget v2, p0, LX/1lR;->e:I

    .line 311889
    iget v3, p1, LX/1lR;->e:I

    move v3, v3

    .line 311890
    if-ne v2, v3, :cond_3

    iget v2, p0, LX/1lR;->f:I

    .line 311891
    iget v3, p1, LX/1lR;->f:I

    move v3, v3

    .line 311892
    if-ne v2, v3, :cond_3

    iget v2, p0, LX/1lR;->g:I

    .line 311893
    iget v3, p1, LX/1lR;->g:I

    move v3, v3

    .line 311894
    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, LX/1lR;->h:Z

    .line 311895
    iget-boolean v3, p1, LX/1lR;->h:Z

    move v3, v3

    .line 311896
    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, LX/1lR;->i:Z

    .line 311897
    iget-boolean v3, p1, LX/1lR;->i:Z

    move v3, v3

    .line 311898
    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, LX/1lR;->j:Z

    .line 311899
    iget-boolean v3, p1, LX/1lR;->j:Z

    move v3, v3

    .line 311900
    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, LX/1lR;->k:Z

    .line 311901
    iget-boolean v3, p1, LX/1lR;->k:Z

    move v3, v3

    .line 311902
    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, LX/1lR;->l:Z

    .line 311903
    iget-boolean v3, p1, LX/1lR;->l:Z

    move v3, v3

    .line 311904
    if-ne v2, v3, :cond_3

    .line 311905
    iget-object v2, p1, LX/1lR;->c:LX/0Px;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 311906
    iget-object v2, p1, LX/1lR;->c:LX/0Px;

    move-object v2, v2

    .line 311907
    iget-object v3, p0, LX/1lR;->c:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 311882
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/1lR;->b:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, LX/1lR;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, LX/1lR;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, LX/1lR;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, LX/1lR;->i:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, LX/1lR;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, LX/1lR;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-boolean v2, p0, LX/1lR;->l:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
