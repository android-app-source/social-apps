.class public LX/1KN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/196;


# instance fields
.field public final a:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final b:LX/19D;

.field private final c:LX/19A;

.field public final d:LX/1KO;

.field private final e:LX/199;

.field public final f:I

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:J

.field private k:I

.field private l:J


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/197;LX/19A;LX/1KO;LX/199;I)V
    .locals 4
    .param p6    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 231426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231427
    iput-boolean v0, p0, LX/1KN;->g:Z

    .line 231428
    iput-boolean v0, p0, LX/1KN;->h:Z

    .line 231429
    iput-boolean v0, p0, LX/1KN;->i:Z

    .line 231430
    iput-wide v2, p0, LX/1KN;->j:J

    .line 231431
    iput v0, p0, LX/1KN;->k:I

    .line 231432
    iput-wide v2, p0, LX/1KN;->l:J

    .line 231433
    iput-object p1, p0, LX/1KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 231434
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/197;->a(Ljava/lang/Boolean;)LX/19D;

    move-result-object v0

    iput-object v0, p0, LX/1KN;->b:LX/19D;

    .line 231435
    iget-object v0, p0, LX/1KN;->b:LX/19D;

    .line 231436
    iput-object p0, v0, LX/19D;->f:LX/196;

    .line 231437
    iput-object p3, p0, LX/1KN;->c:LX/19A;

    .line 231438
    iput-object p4, p0, LX/1KN;->d:LX/1KO;

    .line 231439
    iput-object p5, p0, LX/1KN;->e:LX/199;

    .line 231440
    iput p6, p0, LX/1KN;->f:I

    .line 231441
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    .line 231442
    iget-boolean v0, p0, LX/1KN;->g:Z

    if-nez v0, :cond_0

    .line 231443
    :goto_0
    return-void

    .line 231444
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 231445
    int-to-float v0, v0

    iget-object v1, p0, LX/1KN;->c:LX/19A;

    invoke-virtual {v1}, LX/19A;->a()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 231446
    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    const/16 v2, 0xf

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 231447
    iget v2, p0, LX/1KN;->k:I

    add-int/2addr v2, v1

    iput v2, p0, LX/1KN;->k:I

    .line 231448
    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, LX/1KN;->c:LX/19A;

    invoke-virtual {v2}, LX/19A;->a()I

    move-result v2

    mul-int/2addr v1, v2

    .line 231449
    iget-wide v2, p0, LX/1KN;->l:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/1KN;->l:J

    .line 231450
    if-lez v0, :cond_1

    .line 231451
    const-string v0, "ScrollPerf.FrameDropped"

    const v1, 0x71cf513b

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 231452
    const v0, -0xbe349d9

    invoke-static {v0}, LX/02m;->a(I)V

    .line 231453
    :cond_1
    const-string v0, "ScrollPerf.FrameStarted"

    const v1, -0x2b2dff23

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 231454
    const v0, -0x29a4d8f3

    invoke-static {v0}, LX/02m;->a(I)V

    .line 231455
    goto :goto_0
.end method

.method public final b()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 231456
    iget-boolean v0, p0, LX/1KN;->g:Z

    if-nez v0, :cond_0

    .line 231457
    :goto_0
    return-void

    .line 231458
    :cond_0
    iput-boolean v6, p0, LX/1KN;->g:Z

    .line 231459
    iget-boolean v0, p0, LX/1KN;->h:Z

    if-eqz v0, :cond_1

    .line 231460
    iget-object v0, p0, LX/1KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/1KN;->f:I

    const-string v2, "vsync_time"

    iget-object v3, p0, LX/1KN;->c:LX/19A;

    invoke-virtual {v3}, LX/19A;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 231461
    iget-object v0, p0, LX/1KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/1KN;->f:I

    const-string v2, "total_skipped_frames"

    iget v3, p0, LX/1KN;->k:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 231462
    iget-object v0, p0, LX/1KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/1KN;->f:I

    const-string v2, "total_time_spent"

    iget-wide v4, p0, LX/1KN;->l:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 231463
    iget-object v0, p0, LX/1KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/1KN;->f:I

    const-string v2, "time_since_startup"

    iget-object v3, p0, LX/1KN;->e:LX/199;

    invoke-virtual {v3}, LX/199;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 231464
    iget-object v0, p0, LX/1KN;->b:LX/19D;

    invoke-virtual {v0}, LX/19D;->b()V

    .line 231465
    iput-boolean v6, p0, LX/1KN;->h:Z

    .line 231466
    :cond_1
    iget-boolean v0, p0, LX/1KN;->i:Z

    if-eqz v0, :cond_2

    .line 231467
    iget-object v0, p0, LX/1KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/1KN;->f:I

    const-string v2, "touch_event_latency"

    iget-wide v4, p0, LX/1KN;->j:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 231468
    iput-boolean v6, p0, LX/1KN;->i:Z

    .line 231469
    :cond_2
    iget-object v0, p0, LX/1KN;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/1KN;->f:I

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_0
.end method
