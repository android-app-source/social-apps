.class public final LX/0aP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 84702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84703
    return-void
.end method

.method public static a(Ljava/io/InputStream;[BII)I
    .locals 3

    .prologue
    .line 84704
    invoke-static {p0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84705
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84706
    if-gez p3, :cond_0

    .line 84707
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "len is negative"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84708
    :cond_0
    const/4 v0, 0x0

    .line 84709
    :goto_0
    if-ge v0, p3, :cond_1

    .line 84710
    add-int v1, p2, v0

    sub-int v2, p3, v0

    invoke-virtual {p0, p1, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 84711
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 84712
    add-int/2addr v0, v1

    .line 84713
    goto :goto_0

    .line 84714
    :cond_1
    return v0
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    .locals 6

    .prologue
    .line 84715
    invoke-static {p0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84716
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84717
    const/16 v0, 0x1000

    new-array v2, v0, [B

    .line 84718
    const-wide/16 v0, 0x0

    .line 84719
    :goto_0
    invoke-virtual {p0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 84720
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 84721
    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 84722
    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 84723
    goto :goto_0

    .line 84724
    :cond_0
    return-wide v0
.end method

.method public static a(Ljava/io/InputStream;)[B
    .locals 1

    .prologue
    .line 84725
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 84726
    invoke-static {p0, v0}, LX/0aP;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 84727
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;I)[B
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 84728
    new-array v0, p1, [B

    move v1, p1

    .line 84729
    :goto_0
    if-lez v1, :cond_2

    .line 84730
    sub-int v2, p1, v1

    .line 84731
    invoke-virtual {p0, v0, v2, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    .line 84732
    if-ne v3, v5, :cond_1

    .line 84733
    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    .line 84734
    :cond_0
    :goto_1
    return-object v0

    .line 84735
    :cond_1
    sub-int/2addr v1, v3

    .line 84736
    goto :goto_0

    .line 84737
    :cond_2
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 84738
    if-eq v1, v5, :cond_0

    .line 84739
    new-instance v2, LX/45V;

    invoke-direct {v2}, LX/45V;-><init>()V

    .line 84740
    invoke-virtual {v2, v1}, LX/45V;->write(I)V

    .line 84741
    invoke-static {p0, v2}, LX/0aP;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 84742
    invoke-virtual {v2}, LX/45V;->size()I

    move-result v1

    add-int/2addr v1, p1

    new-array v1, v1, [B

    .line 84743
    invoke-static {v0, v4, v1, v4, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 84744
    invoke-virtual {v2, v1, p1}, LX/45V;->a([BI)V

    move-object v0, v1

    .line 84745
    goto :goto_1
.end method

.method public static b(Ljava/io/InputStream;[BII)V
    .locals 4

    .prologue
    .line 84746
    invoke-static {p0, p1, p2, p3}, LX/0aP;->a(Ljava/io/InputStream;[BII)I

    move-result v0

    .line 84747
    if-eq v0, p3, :cond_0

    .line 84748
    new-instance v1, Ljava/io/EOFException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reached end of stream after reading "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " bytes; "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " bytes expected"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 84749
    :cond_0
    return-void
.end method
