.class public LX/0s5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/0s5;


# instance fields
.field private final a:LX/0s6;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0s6;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0s6;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 151220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151221
    iput-object p1, p0, LX/0s5;->a:LX/0s6;

    .line 151222
    iput-object p2, p0, LX/0s5;->b:LX/0Ot;

    .line 151223
    iput-object p3, p0, LX/0s5;->c:LX/0Ot;

    .line 151224
    iput-object p4, p0, LX/0s5;->d:LX/0Ot;

    .line 151225
    return-void
.end method

.method public static a(LX/0QB;)LX/0s5;
    .locals 7

    .prologue
    .line 151207
    sget-object v0, LX/0s5;->e:LX/0s5;

    if-nez v0, :cond_1

    .line 151208
    const-class v1, LX/0s5;

    monitor-enter v1

    .line 151209
    :try_start_0
    sget-object v0, LX/0s5;->e:LX/0s5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 151210
    if-eqz v2, :cond_0

    .line 151211
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 151212
    new-instance v4, LX/0s5;

    invoke-static {v0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v3

    check-cast v3, LX/0s6;

    const/16 v5, 0xf9a

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xac0

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0xdf4

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, v5, v6, p0}, LX/0s5;-><init>(LX/0s6;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 151213
    move-object v0, v4

    .line 151214
    sput-object v0, LX/0s5;->e:LX/0s5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151215
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 151216
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 151217
    :cond_1
    sget-object v0, LX/0s5;->e:LX/0s5;

    return-object v0

    .line 151218
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 151219
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/03R;)Z
    .locals 2

    .prologue
    .line 151204
    sget-object v0, LX/0sE;->a:[I

    invoke-virtual {p0}, LX/03R;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 151205
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 151206
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 151203
    iget-object v0, p0, LX/0s5;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x3ab

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    invoke-static {v0}, LX/0s5;->a(LX/03R;)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 151202
    iget-object v0, p0, LX/0s5;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x3ac

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    invoke-static {v0}, LX/0s5;->a(LX/03R;)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 151201
    invoke-virtual {p0}, LX/0s5;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0s5;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0sF;->c:LX/0Tn;

    invoke-interface {v0, v2, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final d()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 151188
    invoke-virtual {p0}, LX/0s5;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0s5;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0sF;->b:LX/0Tn;

    invoke-interface {v0, v2, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final g()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 151189
    iget-object v0, p0, LX/0s5;->a:LX/0s6;

    const-string v3, "org.torproject.android"

    const/16 v4, 0x40

    invoke-virtual {v0, v3, v4}, LX/01H;->e(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 151190
    if-nez v0, :cond_0

    move v0, v1

    .line 151191
    :goto_0
    return v0

    .line 151192
    :cond_0
    iget-object v3, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v3, :cond_3

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v3, v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 151193
    iget-object v3, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v3

    invoke-static {v3}, LX/03l;->a([B)Ljava/lang/String;

    move-result-object v3

    .line 151194
    :goto_1
    move-object v3, v3

    .line 151195
    iget-object v0, p0, LX/0s5;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    invoke-static {v0}, LX/4cj;->a(LX/0W3;)Ljava/util/List;

    move-result-object v0

    .line 151196
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 151197
    if-eqz v0, :cond_1

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 151198
    goto :goto_0

    .line 151199
    :cond_2
    const-class v0, LX/0s5;

    const-string v4, "rejecting unknown signature %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v3, v2, v1

    invoke-static {v0, v4, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 151200
    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method
