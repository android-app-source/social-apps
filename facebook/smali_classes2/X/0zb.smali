.class public final LX/0zb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0za;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0za;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 167424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167425
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0zb;->b:Z

    .line 167426
    return-void
.end method

.method private static a(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/0za;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 167427
    if-nez p0, :cond_1

    .line 167428
    :cond_0
    return-void

    .line 167429
    :cond_1
    const/4 v1, 0x0

    .line 167430
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0za;

    .line 167431
    :try_start_0
    invoke-interface {v0}, LX/0za;->b()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 167432
    :catch_0
    move-exception v3

    .line 167433
    if-nez v1, :cond_5

    .line 167434
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 167435
    :goto_1
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    .line 167436
    goto :goto_0

    .line 167437
    :cond_2
    if-eqz v1, :cond_0

    .line 167438
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    .line 167439
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 167440
    instance-of v2, v0, Ljava/lang/RuntimeException;

    if-eqz v2, :cond_3

    .line 167441
    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 167442
    :cond_3
    new-instance v0, LX/52x;

    invoke-direct {v0, v1}, LX/52x;-><init>(Ljava/util/Collection;)V

    throw v0

    .line 167443
    :cond_4
    new-instance v0, LX/52x;

    invoke-direct {v0, v1}, LX/52x;-><init>(Ljava/util/Collection;)V

    throw v0

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0za;)V
    .locals 2

    .prologue
    .line 167444
    const/4 v0, 0x0

    .line 167445
    monitor-enter p0

    .line 167446
    :try_start_0
    iget-boolean v1, p0, LX/0zb;->b:Z

    if-eqz v1, :cond_1

    .line 167447
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167448
    if-eqz p1, :cond_0

    .line 167449
    invoke-interface {p1}, LX/0za;->b()V

    .line 167450
    :cond_0
    return-void

    .line 167451
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/0zb;->a:Ljava/util/List;

    if-nez v1, :cond_2

    .line 167452
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, LX/0zb;->a:Ljava/util/List;

    .line 167453
    :cond_2
    iget-object v1, p0, LX/0zb;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object p1, v0

    goto :goto_0

    .line 167454
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 167455
    monitor-enter p0

    .line 167456
    :try_start_0
    iget-boolean v0, p0, LX/0zb;->b:Z

    if-eqz v0, :cond_0

    .line 167457
    monitor-exit p0

    .line 167458
    :goto_0
    return-void

    .line 167459
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0zb;->b:Z

    .line 167460
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167461
    iget-object v0, p0, LX/0zb;->a:Ljava/util/List;

    invoke-static {v0}, LX/0zb;->a(Ljava/util/Collection;)V

    .line 167462
    const/4 v0, 0x0

    iput-object v0, p0, LX/0zb;->a:Ljava/util/List;

    goto :goto_0

    .line 167463
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 167464
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0zb;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
