.class public LX/0YB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0YB;


# instance fields
.field public final a:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 80697
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80698
    const-string v0, "DebugTraceRetryPreference"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, LX/0YB;->a:Landroid/content/SharedPreferences;

    .line 80699
    return-void
.end method

.method public static a(LX/0QB;)LX/0YB;
    .locals 4

    .prologue
    .line 80700
    sget-object v0, LX/0YB;->b:LX/0YB;

    if-nez v0, :cond_1

    .line 80701
    const-class v1, LX/0YB;

    monitor-enter v1

    .line 80702
    :try_start_0
    sget-object v0, LX/0YB;->b:LX/0YB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 80703
    if-eqz v2, :cond_0

    .line 80704
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 80705
    new-instance p0, LX/0YB;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/0YB;-><init>(Landroid/content/Context;)V

    .line 80706
    move-object v0, p0

    .line 80707
    sput-object v0, LX/0YB;->b:LX/0YB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80708
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 80709
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 80710
    :cond_1
    sget-object v0, LX/0YB;->b:LX/0YB;

    return-object v0

    .line 80711
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 80712
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()I
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SharedPreferencesUse"
        }
    .end annotation

    .prologue
    .line 80713
    iget-object v0, p0, LX/0YB;->a:Landroid/content/SharedPreferences;

    const-string v1, "DebugTraceRetryKey"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method
