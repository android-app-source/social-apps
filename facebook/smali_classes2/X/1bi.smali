.class public LX/1bi;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 280881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(II)I
    .locals 1

    .prologue
    .line 280882
    add-int/lit8 v0, p0, 0x1f

    .line 280883
    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, p1

    .line 280884
    return v0
.end method

.method public static a(Ljava/lang/Object;)I
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 280877
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 280878
    :goto_0
    add-int/lit8 p0, v0, 0x1f

    .line 280879
    move v0, p0

    .line 280880
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 280870
    if-nez p0, :cond_0

    move v1, v0

    :goto_0
    if-nez p1, :cond_1

    :goto_1
    invoke-static {v1, v0}, LX/1bi;->a(II)I

    move-result v0

    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 280871
    if-nez p0, :cond_0

    move v2, v0

    :goto_0
    if-nez p1, :cond_1

    move v1, v0

    :goto_1
    if-nez p2, :cond_2

    .line 280872
    :goto_2
    add-int/lit8 p0, v2, 0x1f

    .line 280873
    mul-int/lit8 p0, p0, 0x1f

    add-int/2addr p0, v1

    .line 280874
    mul-int/lit8 p0, p0, 0x1f

    add-int/2addr p0, v0

    .line 280875
    move v0, p0

    .line 280876
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    move v2, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_2
.end method
