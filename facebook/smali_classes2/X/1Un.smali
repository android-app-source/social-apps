.class public LX/1Un;
.super LX/1Cz;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "LX/1Cz",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final a:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 256768
    invoke-direct {p0}, LX/1Cz;-><init>()V

    .line 256769
    iput p1, p0, LX/1Un;->a:I

    .line 256770
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 256771
    iget v0, p0, LX/1Un;->a:I

    .line 256772
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 256773
    if-nez v1, :cond_0

    .line 256774
    :goto_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, LX/1Un;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 256775
    :cond_0
    invoke-static {v0, p1}, LX/1Cz;->c(ILandroid/content/Context;)Landroid/util/AttributeSet;

    move-result-object v1

    .line 256776
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, p1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 256777
    iget v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v3, -0x2

    if-ne v1, v3, :cond_1

    iget v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_1
    const-string v2, "Feed layouts should use layout_height = WRAP_CONTENT and layout_width = MATCH_PARENT."

    invoke-static {v1, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
