.class public LX/0yy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0yz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0yz",
        "<",
        "LX/6VO;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/14q;",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 166503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166504
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0yy;->a:Ljava/util/Map;

    .line 166505
    return-void
.end method

.method public static declared-synchronized b(LX/0yy;LX/14q;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 2

    .prologue
    .line 166506
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0yy;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 166507
    if-nez v0, :cond_0

    .line 166508
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    .line 166509
    iget-object v1, p0, LX/0yy;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166510
    :cond_0
    monitor-exit p0

    return-object v0

    .line 166511
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(LX/6VO;)V
    .locals 1

    .prologue
    .line 166512
    check-cast p1, LX/6VO;

    .line 166513
    iget-object v0, p1, LX/6VO;->a:LX/14q;

    move-object v0, v0

    .line 166514
    invoke-static {p0, v0}, LX/0yy;->b(LX/0yy;LX/14q;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 166515
    return-void
.end method

.method public final b(LX/6VO;)V
    .locals 1

    .prologue
    .line 166516
    check-cast p1, LX/6VO;

    .line 166517
    iget-object v0, p1, LX/6VO;->a:LX/14q;

    move-object v0, v0

    .line 166518
    invoke-static {p0, v0}, LX/0yy;->b(LX/0yy;LX/14q;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 166519
    return-void
.end method
