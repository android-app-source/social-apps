.class public LX/0g2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fi;
.implements LX/0fp;
.implements LX/0fq;
.implements LX/0fr;
.implements LX/0g3;
.implements LX/0ft;


# static fields
.field public static final a:LX/0Tn;

.field public static final e:Ljava/lang/String;

.field public static final f:LX/0Tn;

.field private static final g:Ljava/lang/String;


# instance fields
.field private final A:LX/17Q;

.field public final B:LX/0pT;

.field public final C:LX/0pl;

.field private final D:LX/1JG;

.field public final E:LX/0qa;

.field public final F:LX/0aG;

.field public final G:Lcom/facebook/performancelogger/PerformanceLogger;

.field public final H:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/feedtype/FeedTypeDataItem;",
            ">;>;"
        }
    .end annotation
.end field

.field public final I:LX/0oy;

.field public final J:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final K:LX/0SG;

.field private final L:LX/0qX;

.field private final M:LX/0Tf;

.field public final N:LX/0pJ;

.field private final O:LX/0Wd;

.field public final P:LX/1JH;

.field private final Q:LX/1JU;

.field public final R:LX/1EU;

.field public S:LX/1NJ;

.field public T:Landroid/content/Context;

.field private U:Lcom/facebook/feed/fragment/NewsFeedFragment;

.field public V:Lcom/facebook/feed/fragment/NewsFeedFragment;

.field public W:LX/1CY;

.field public X:LX/0bH;

.field public Y:Lcom/facebook/api/feedtype/FeedType;

.field private Z:LX/1KS;

.field public aa:LX/1EM;

.field public ab:Landroid/view/View;

.field public ac:LX/0fx;

.field public ad:LX/0fx;

.field public ae:LX/1UQ;

.field public af:LX/0g5;

.field public ag:LX/4nS;

.field public ah:LX/1Z9;

.field public ai:I

.field public aj:LX/1NK;

.field public ak:Z

.field public al:Z

.field private am:Lcom/facebook/feed/loader/FeedDataLoaderInitializationParams;

.field public an:LX/1Iv;

.field private ao:Z

.field public b:LX/0gC;

.field public c:LX/0fz;

.field public d:Z

.field public final h:LX/1J3;

.field public final i:LX/1J6;

.field private final j:LX/1J9;

.field public final k:LX/0pG;

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/data/FeedDataLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0pV;

.field public final o:LX/0ad;

.field public final p:LX/0jU;

.field public final q:LX/0kb;

.field public final r:LX/0Zb;

.field private final s:Lcom/facebook/common/perftest/PerfTestConfig;

.field public final t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2hU;",
            ">;"
        }
    .end annotation
.end field

.field public final u:LX/0Yi;

.field private final v:LX/1JC;

.field private final w:Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;

.field public final x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wL;",
            ">;"
        }
    .end annotation
.end field

.field public final y:LX/0iS;

.field public final z:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Cz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 110476
    const-class v0, LX/0g2;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0g2;->e:Ljava/lang/String;

    .line 110477
    sget-object v0, LX/0Tm;->e:LX/0Tn;

    const-string v1, "WARM_START_RERANK"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 110478
    sput-object v0, LX/0g2;->a:LX/0Tn;

    const-string v1, "RESET_TIME"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0g2;->f:LX/0Tn;

    .line 110479
    const-string v0, "0"

    sput-object v0, LX/0g2;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1J3;LX/1J6;LX/1J9;LX/0pG;LX/0Or;LX/0Or;LX/0pV;LX/0ad;LX/0jU;LX/0kb;LX/0Zb;Lcom/facebook/common/perftest/PerfTestConfig;LX/0Ot;LX/0Yi;LX/1JC;Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;LX/0Ot;LX/0iS;LX/0Ot;LX/17Q;LX/0pT;LX/0pl;LX/1JG;LX/0qa;LX/0aG;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Ot;LX/0oy;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0qX;LX/0Tf;LX/0pJ;LX/0Wd;LX/1JH;LX/1JU;LX/1EU;)V
    .locals 2
    .param p32    # LX/0Tf;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p34    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1J3;",
            "LX/1J6;",
            "LX/1J9;",
            "LX/0pG;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/data/FeedDataLoader;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;",
            ">;",
            "LX/0pV;",
            "LX/0ad;",
            "LX/0jU;",
            "LX/0kb;",
            "LX/0Zb;",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            "LX/0Ot",
            "<",
            "LX/2hU;",
            ">;",
            "LX/0Yi;",
            "LX/1JC;",
            "Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;",
            "LX/0Ot",
            "<",
            "LX/0wL;",
            ">;",
            "LX/0iS;",
            "LX/0Ot",
            "<",
            "LX/2Cz;",
            ">;",
            "LX/17Q;",
            "LX/0pT;",
            "LX/0pl;",
            "LX/1JG;",
            "LX/0qa;",
            "LX/0aG;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/feedtype/FeedTypeDataItem;",
            ">;>;",
            "LX/0oy;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0SG;",
            "LX/0qX;",
            "LX/0Tf;",
            "LX/0pJ;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            "LX/1JH;",
            "LX/1JU;",
            "LX/1EU;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 110175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110176
    const/4 v1, 0x0

    iput v1, p0, LX/0g2;->ai:I

    .line 110177
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/0g2;->ak:Z

    .line 110178
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/0g2;->al:Z

    .line 110179
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/0g2;->d:Z

    .line 110180
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/0g2;->ao:Z

    .line 110181
    iput-object p1, p0, LX/0g2;->h:LX/1J3;

    .line 110182
    iput-object p2, p0, LX/0g2;->i:LX/1J6;

    .line 110183
    iput-object p3, p0, LX/0g2;->j:LX/1J9;

    .line 110184
    iput-object p4, p0, LX/0g2;->k:LX/0pG;

    .line 110185
    iput-object p5, p0, LX/0g2;->l:LX/0Or;

    .line 110186
    iput-object p6, p0, LX/0g2;->m:LX/0Or;

    .line 110187
    iput-object p7, p0, LX/0g2;->n:LX/0pV;

    .line 110188
    iput-object p8, p0, LX/0g2;->o:LX/0ad;

    .line 110189
    iput-object p9, p0, LX/0g2;->p:LX/0jU;

    .line 110190
    iput-object p10, p0, LX/0g2;->q:LX/0kb;

    .line 110191
    iput-object p11, p0, LX/0g2;->r:LX/0Zb;

    .line 110192
    iput-object p12, p0, LX/0g2;->s:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 110193
    iput-object p13, p0, LX/0g2;->t:LX/0Ot;

    .line 110194
    move-object/from16 v0, p14

    iput-object v0, p0, LX/0g2;->u:LX/0Yi;

    .line 110195
    move-object/from16 v0, p15

    iput-object v0, p0, LX/0g2;->v:LX/1JC;

    .line 110196
    move-object/from16 v0, p16

    iput-object v0, p0, LX/0g2;->w:Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;

    .line 110197
    move-object/from16 v0, p17

    iput-object v0, p0, LX/0g2;->x:LX/0Ot;

    .line 110198
    move-object/from16 v0, p18

    iput-object v0, p0, LX/0g2;->y:LX/0iS;

    .line 110199
    move-object/from16 v0, p19

    iput-object v0, p0, LX/0g2;->z:LX/0Ot;

    .line 110200
    move-object/from16 v0, p20

    iput-object v0, p0, LX/0g2;->A:LX/17Q;

    .line 110201
    move-object/from16 v0, p21

    iput-object v0, p0, LX/0g2;->B:LX/0pT;

    .line 110202
    move-object/from16 v0, p22

    iput-object v0, p0, LX/0g2;->C:LX/0pl;

    .line 110203
    move-object/from16 v0, p23

    iput-object v0, p0, LX/0g2;->D:LX/1JG;

    .line 110204
    move-object/from16 v0, p24

    iput-object v0, p0, LX/0g2;->E:LX/0qa;

    .line 110205
    move-object/from16 v0, p25

    iput-object v0, p0, LX/0g2;->F:LX/0aG;

    .line 110206
    move-object/from16 v0, p26

    iput-object v0, p0, LX/0g2;->G:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 110207
    move-object/from16 v0, p27

    iput-object v0, p0, LX/0g2;->H:LX/0Ot;

    .line 110208
    move-object/from16 v0, p28

    iput-object v0, p0, LX/0g2;->I:LX/0oy;

    .line 110209
    move-object/from16 v0, p29

    iput-object v0, p0, LX/0g2;->J:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 110210
    move-object/from16 v0, p30

    iput-object v0, p0, LX/0g2;->K:LX/0SG;

    .line 110211
    move-object/from16 v0, p31

    iput-object v0, p0, LX/0g2;->L:LX/0qX;

    .line 110212
    move-object/from16 v0, p32

    iput-object v0, p0, LX/0g2;->M:LX/0Tf;

    .line 110213
    move-object/from16 v0, p33

    iput-object v0, p0, LX/0g2;->N:LX/0pJ;

    .line 110214
    move-object/from16 v0, p34

    iput-object v0, p0, LX/0g2;->O:LX/0Wd;

    .line 110215
    move-object/from16 v0, p35

    iput-object v0, p0, LX/0g2;->P:LX/1JH;

    .line 110216
    move-object/from16 v0, p36

    iput-object v0, p0, LX/0g2;->Q:LX/1JU;

    .line 110217
    move-object/from16 v0, p37

    iput-object v0, p0, LX/0g2;->R:LX/1EU;

    .line 110218
    return-void
.end method

.method public static H(LX/0g2;)Z
    .locals 3

    .prologue
    .line 110219
    iget-object v0, p0, LX/0g2;->o:LX/0ad;

    sget-short v1, LX/0fe;->aG:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static J(LX/0g2;)V
    .locals 4

    .prologue
    .line 110220
    iget-object v0, p0, LX/0g2;->C:LX/0pl;

    invoke-virtual {v0}, LX/0pl;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0g2;->J:Lcom/facebook/prefs/shared/FbSharedPreferences;

    if-eqz v0, :cond_0

    .line 110221
    iget-object v0, p0, LX/0g2;->J:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0g2;->f:LX/0Tn;

    iget-object v2, p0, LX/0g2;->K:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 110222
    :cond_0
    return-void
.end method

.method public static L(LX/0g2;)Z
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 110223
    iget-boolean v1, p0, LX/0g2;->al:Z

    if-nez v1, :cond_0

    iget-object v1, p0, LX/0g2;->C:LX/0pl;

    invoke-virtual {v1}, LX/0pl;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0g2;->b:LX/0gC;

    invoke-interface {v1}, LX/0gC;->u()Z

    move-result v1

    if-nez v1, :cond_1

    .line 110224
    :cond_0
    :goto_0
    return v0

    .line 110225
    :cond_1
    const-wide/16 v10, 0x0

    .line 110226
    iget-object v12, p0, LX/0g2;->J:Lcom/facebook/prefs/shared/FbSharedPreferences;

    if-eqz v12, :cond_2

    .line 110227
    iget-object v12, p0, LX/0g2;->J:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v13, LX/0g2;->f:LX/0Tn;

    invoke-interface {v12, v13, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v10

    .line 110228
    :cond_2
    move-wide v2, v10

    .line 110229
    iget-object v1, p0, LX/0g2;->K:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    sub-long/2addr v4, v2

    .line 110230
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    iget-object v6, p0, LX/0g2;->L:LX/0qX;

    const/16 v7, 0x9

    .line 110231
    sget-wide v10, LX/0X5;->cj:J

    invoke-static {v6, v10, v11, v7}, LX/0qX;->a(LX/0qX;JI)I

    move-result v10

    move v6, v10

    .line 110232
    int-to-long v6, v6

    invoke-virtual {v1, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    .line 110233
    const-wide/16 v8, 0x0

    cmp-long v1, v2, v8

    if-lez v1, :cond_0

    cmp-long v1, v4, v6

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static M(LX/0g2;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 110163
    iget-object v1, p0, LX/0g2;->L:LX/0qX;

    .line 110164
    sget-wide v2, LX/0X5;->cq:J

    invoke-static {v1, v2, v3, v0}, LX/0qX;->a(LX/0qX;JZ)Z

    move-result v2

    move v1, v2

    .line 110165
    if-eqz v1, :cond_0

    .line 110166
    const/4 v1, 0x1

    move v1, v1

    .line 110167
    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0g2;->V:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v1}, Lcom/facebook/feed/fragment/NewsFeedFragment;->z()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private static O(LX/0g2;)V
    .locals 4

    .prologue
    .line 110234
    const-string v0, "NewsfeedFragment.updateDataLoader"

    const v1, -0x70dc70bc

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 110235
    :try_start_0
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    if-nez v0, :cond_1

    .line 110236
    iget-object v0, p0, LX/0g2;->Y:Lcom/facebook/api/feedtype/FeedType;

    .line 110237
    iget-object v1, v0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v0, v1

    .line 110238
    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 110239
    const/4 v0, 0x0

    .line 110240
    iget-object v1, p0, LX/0g2;->N:LX/0pJ;

    invoke-virtual {v1, v0}, LX/0pJ;->b(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0g2;->o:LX/0ad;

    sget-short v2, LX/0fe;->ac:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v0, v0

    .line 110241
    if-eqz v0, :cond_5

    iget-object v0, p0, LX/0g2;->m:LX/0Or;

    .line 110242
    :goto_0
    iget-object v1, p0, LX/0g2;->Y:Lcom/facebook/api/feedtype/FeedType;

    .line 110243
    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0gC;

    .line 110244
    invoke-interface {v2, v1}, LX/0gC;->a(Lcom/facebook/api/feedtype/FeedType;)V

    .line 110245
    const/4 v3, 0x0

    invoke-interface {v2, v3}, LX/0gC;->a(Z)V

    .line 110246
    move-object v0, v2

    .line 110247
    iput-object v0, p0, LX/0g2;->b:LX/0gC;

    .line 110248
    :goto_1
    iget-object v1, p0, LX/0g2;->n:LX/0pV;

    sget-object v0, LX/0g2;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    invoke-interface {v0}, LX/0gC;->y()Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, LX/0rj;->GOT_PERSISTED_DATA_LOADER:LX/0rj;

    :goto_2
    iget-object v3, p0, LX/0g2;->b:LX/0gC;

    invoke-interface {v3}, LX/0gC;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/0pV;->a(Ljava/lang/String;LX/0rj;Ljava/lang/String;)V

    .line 110249
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    invoke-interface {v0}, LX/0gC;->h()LX/0fz;

    move-result-object v0

    iput-object v0, p0, LX/0g2;->c:LX/0fz;

    .line 110250
    iget-object v0, p0, LX/0g2;->h:LX/1J3;

    iget-object v1, p0, LX/0g2;->b:LX/0gC;

    .line 110251
    iput-object v1, v0, LX/1J3;->c:LX/0gC;

    .line 110252
    iget-object v0, p0, LX/0g2;->i:LX/1J6;

    iget-object v1, p0, LX/0g2;->b:LX/0gC;

    .line 110253
    iput-object v1, v0, LX/1J6;->d:LX/0gC;

    .line 110254
    :cond_1
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    instance-of v0, v0, LX/0pR;

    if-eqz v0, :cond_2

    .line 110255
    iget-object v1, p0, LX/0g2;->aa:LX/1EM;

    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    check-cast v0, LX/0pR;

    .line 110256
    iput-object v0, v1, LX/1EM;->g:LX/0pR;

    .line 110257
    :cond_2
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    instance-of v0, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/0g2;->P:LX/1JH;

    .line 110258
    iget-object v1, v0, LX/1JH;->d:LX/1JR;

    invoke-virtual {v1}, LX/1JR;->a()Z

    move-result v1

    move v0, v1

    .line 110259
    if-eqz v0, :cond_4

    .line 110260
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    check-cast v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    iget-object v1, p0, LX/0g2;->P:LX/1JH;

    .line 110261
    iget-object v2, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ab:Ljava/util/List;

    if-nez v2, :cond_3

    .line 110262
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ab:Ljava/util/List;

    .line 110263
    :cond_3
    iget-object v2, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ab:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110264
    iget-object v0, p0, LX/0g2;->P:LX/1JH;

    .line 110265
    invoke-static {v0}, LX/1JH;->i(LX/1JH;)V

    .line 110266
    :cond_4
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    iget-object v1, p0, LX/0g2;->aa:LX/1EM;

    invoke-interface {v0, v1}, LX/0gC;->a(LX/1EM;)V

    .line 110267
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    invoke-interface {v0, p0}, LX/0gC;->a(LX/0g3;)V

    .line 110268
    iget-object v0, p0, LX/0g2;->W:LX/1CY;

    iget-object v1, p0, LX/0g2;->c:LX/0fz;

    invoke-virtual {v0, v1, p0}, LX/1CY;->a(LX/0fz;LX/0g4;)V

    .line 110269
    iget-object v0, p0, LX/0g2;->W:LX/1CY;

    invoke-virtual {v0}, LX/1CY;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110270
    const v0, -0x685f36c2

    invoke-static {v0}, LX/02m;->a(I)V

    .line 110271
    return-void

    .line 110272
    :cond_5
    :try_start_1
    iget-object v0, p0, LX/0g2;->l:LX/0Or;

    goto/16 :goto_0

    .line 110273
    :cond_6
    iget-object v0, p0, LX/0g2;->k:LX/0pG;

    iget-object v1, p0, LX/0g2;->Y:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0, v1}, LX/0pG;->a(Lcom/facebook/api/feedtype/FeedType;)LX/0gC;

    move-result-object v0

    iput-object v0, p0, LX/0g2;->b:LX/0gC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 110274
    :catchall_0
    move-exception v0

    const v1, 0x19125037

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 110275
    :cond_7
    :try_start_2
    sget-object v0, LX/0rj;->GOT_TRANSIENT_DATA_LOADER:LX/0rj;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2
.end method

.method public static Q(LX/0g2;)V
    .locals 2

    .prologue
    .line 110276
    iget-object v0, p0, LX/0g2;->ag:LX/4nS;

    if-eqz v0, :cond_0

    .line 110277
    iget-object v0, p0, LX/0g2;->ag:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->b()V

    .line 110278
    :cond_0
    iget-object v0, p0, LX/0g2;->c:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->h()V

    .line 110279
    invoke-virtual {p0}, LX/0g2;->C()V

    .line 110280
    sget-object v0, LX/0gf;->PULL_TO_REFRESH:LX/0gf;

    invoke-virtual {p0, v0}, LX/0g2;->c(LX/0gf;)V

    .line 110281
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "manual_refresh"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "native_newsfeed"

    .line 110282
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 110283
    move-object v0, v0

    .line 110284
    move-object v0, v0

    .line 110285
    if-eqz v0, :cond_1

    .line 110286
    iget-object v1, p0, LX/0g2;->r:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 110287
    :cond_1
    return-void
.end method

.method public static R(LX/0g2;)V
    .locals 3

    .prologue
    .line 110288
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    invoke-interface {v0}, LX/0gC;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110289
    :goto_0
    return-void

    .line 110290
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, LX/0g2;->c:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->k()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 110291
    iget-object v0, p0, LX/0g2;->c:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 110292
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 110293
    :cond_1
    iget-object v0, p0, LX/0g2;->c:LX/0fz;

    .line 110294
    iget-object v2, v0, LX/0fz;->i:LX/0qv;

    move-object v0, v2

    .line 110295
    invoke-virtual {v0}, LX/0qv;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 110296
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 110297
    :cond_2
    iget-object v0, p0, LX/0g2;->p:LX/0jU;

    invoke-virtual {v0, v1}, LX/0jU;->a(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public static a(LX/0g2;LX/0rj;)V
    .locals 4

    .prologue
    .line 110298
    iget-object v0, p0, LX/0g2;->n:LX/0pV;

    sget-object v1, LX/0g2;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ", Stories:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, LX/0g2;->ai:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, LX/0pV;->a(Ljava/lang/String;LX/0rj;Ljava/lang/String;)V

    .line 110299
    return-void
.end method

.method public static a(LX/0g2;ZILX/1lo;)V
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 110300
    sget-object v0, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    .line 110301
    sget-object v1, LX/1lo;->CACHED_ONLY:LX/1lo;

    if-ne p3, v1, :cond_0

    .line 110302
    sget-object v0, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    .line 110303
    :cond_0
    sget-object v4, LX/0gf;->SCROLLING:LX/0gf;

    .line 110304
    iget-object v5, p0, LX/0g2;->b:LX/0gC;

    sget-object v1, LX/1lo;->ALWAYS:LX/1lo;

    if-ne p3, v1, :cond_3

    move v1, v2

    :goto_0
    invoke-interface {v5, v0, v4, v1}, LX/0gC;->a(LX/0rS;LX/0gf;Z)LX/0uO;

    move-result-object v1

    .line 110305
    sget-object v5, LX/1lp;->c:[I

    invoke-virtual {v1}, LX/0uO;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 110306
    :goto_1
    :pswitch_0
    sget-object v4, LX/0uO;->SUCCESS:LX/0uO;

    if-eq v1, v4, :cond_1

    sget-object v4, LX/0uO;->ALREADY_SCHEDULED:LX/0uO;

    if-ne v1, v4, :cond_2

    :cond_1
    if-eqz p1, :cond_2

    .line 110307
    iget-object v1, p0, LX/0g2;->b:LX/0gC;

    sget-object v4, LX/0gf;->SCROLLING:LX/0gf;

    sget-object v5, LX/1lo;->ALWAYS:LX/1lo;

    if-ne p3, v5, :cond_7

    :goto_2
    invoke-interface {v1, v0, v4, v2}, LX/0gC;->b(LX/0rS;LX/0gf;Z)V

    .line 110308
    :cond_2
    return-void

    :cond_3
    move v1, v3

    .line 110309
    goto :goto_0

    .line 110310
    :pswitch_1
    iget-object v5, p0, LX/0g2;->Z:LX/1KS;

    sget-object v6, LX/1lr;->TAIL:LX/1lr;

    invoke-virtual {v5, v4, v6}, LX/1KS;->a(LX/0gf;LX/1lr;)V

    .line 110311
    iget-object v4, p0, LX/0g2;->G:Lcom/facebook/performancelogger/PerformanceLogger;

    const v5, 0xa003e

    const-string v6, "NNFTailFetchTime"

    invoke-interface {v4, v5, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 110312
    iget-object v4, p0, LX/0g2;->u:LX/0Yi;

    const v10, 0xa0032

    const v9, 0xa003e

    const/4 v8, 0x0

    .line 110313
    iget-object v5, v4, LX/0Yi;->l:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0kb;

    invoke-virtual {v5}, LX/0kb;->d()Z

    move-result v5

    .line 110314
    iget-object v6, v4, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v7, "NNFTailFetchTime"

    invoke-interface {v6, v9, v7}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 110315
    const-string v6, "NNFTailFetchTime"

    invoke-static {v4, v9, v6, v8}, LX/0Yi;->a(LX/0Yi;ILjava/lang/String;Z)V

    .line 110316
    if-eqz v5, :cond_8

    .line 110317
    const v5, 0xa0042

    const-string v6, "NNFTailFetchNetworkCallTime"

    invoke-static {v4, v5, v6, v8}, LX/0Yi;->a(LX/0Yi;ILjava/lang/String;Z)V

    .line 110318
    :cond_4
    :goto_3
    if-eqz p1, :cond_5

    iget-object v5, v4, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v6, "NNFTailFetchTime"

    invoke-interface {v5, v9, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, v4, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v5, v10}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v5

    if-nez v5, :cond_5

    .line 110319
    invoke-static {v4, v10, v8}, LX/0Yi;->a(LX/0Yi;IZ)V

    .line 110320
    :cond_5
    iget-object v4, p0, LX/0g2;->V:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-interface {v4}, LX/0fw;->k()LX/0g8;

    move-result-object v4

    .line 110321
    new-instance v5, LX/1oN;

    invoke-direct {v5, p0, p2}, LX/1oN;-><init>(LX/0g2;I)V

    move-object v5, v5

    .line 110322
    invoke-interface {v4, v5}, LX/0g8;->b(LX/0fu;)V

    .line 110323
    :cond_6
    goto :goto_1

    .line 110324
    :pswitch_2
    sget-object v4, LX/1lo;->ALWAYS:LX/1lo;

    invoke-static {p0, p1, p2, v4}, LX/0g2;->a(LX/0g2;ZILX/1lo;)V

    goto/16 :goto_1

    .line 110325
    :pswitch_3
    iget-object v5, p0, LX/0g2;->Z:LX/1KS;

    sget-object v6, LX/1lr;->TAIL:LX/1lr;

    invoke-virtual {v5, v4, v6}, LX/1KS;->a(LX/0gf;LX/1lr;)V

    goto/16 :goto_1

    :cond_7
    move v2, v3

    .line 110326
    goto :goto_2

    .line 110327
    :cond_8
    const v5, 0xa0043

    const-string v6, "NNFTailFetchNotConnectedCallTime"

    invoke-static {v4, v5, v6, v8}, LX/0Yi;->a(LX/0Yi;ILjava/lang/String;Z)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(ZLX/18G;Ljava/lang/String;ILcom/facebook/api/feed/FetchFeedResult;)V
    .locals 7
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 110328
    const-string v0, "NewsFeedFragment.handleDataLoaded"

    const v1, 0x446a734f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 110329
    if-eqz p5, :cond_0

    .line 110330
    :try_start_0
    iget-object v0, p0, LX/0g2;->W:LX/1CY;

    invoke-virtual {p5}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1CY;->a(Ljava/lang/Iterable;)V

    .line 110331
    :cond_0
    iget-object v0, p0, LX/0g2;->q:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v1

    .line 110332
    sget-object v6, LX/0gf;->UNKNOWN:LX/0gf;

    .line 110333
    sget-object v0, LX/1lp;->b:[I

    invoke-virtual {p2}, LX/18G;->ordinal()I

    move-result v3

    aget v0, v0, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v0, :pswitch_data_0

    .line 110334
    :cond_1
    :goto_0
    const v0, -0x35e1c988    # -2592158.0f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 110335
    return-void

    .line 110336
    :pswitch_0
    if-eqz p1, :cond_2

    :try_start_1
    iget-object v0, p0, LX/0g2;->c:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    if-nez p1, :cond_4

    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    invoke-interface {v0}, LX/0gC;->w()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    const/4 v2, 0x1

    .line 110337
    :cond_4
    iget-object v0, p0, LX/0g2;->Z:LX/1KS;

    iget-object v1, p0, LX/0g2;->c:LX/0fz;

    invoke-virtual {v1}, LX/0fz;->i()I

    move-result v4

    .line 110338
    iget-object v1, p5, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v5, v1

    .line 110339
    move v1, p1

    move v3, p4

    invoke-virtual/range {v0 .. v5}, LX/1KS;->a(ZZIILX/0ta;)V

    .line 110340
    iget-boolean v0, p0, LX/0g2;->al:Z

    if-eqz v0, :cond_5

    .line 110341
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0g2;->al:Z

    .line 110342
    :cond_5
    if-eqz p5, :cond_9

    .line 110343
    iget-object v0, p5, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v0, v0

    .line 110344
    if-eqz v0, :cond_9

    .line 110345
    iget-object v0, p5, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v0, v0

    .line 110346
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v0, v1

    .line 110347
    :goto_1
    if-lez p4, :cond_1

    .line 110348
    invoke-static {p0, v0}, LX/0g2;->d(LX/0g2;LX/0gf;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 110349
    :catchall_0
    move-exception v0

    const v1, 0x357ac216

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 110350
    :pswitch_1
    :try_start_2
    iget-object v2, p0, LX/0g2;->Z:LX/1KS;

    iget-object v0, p0, LX/0g2;->c:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->x()Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, LX/AjJ;->EMPTY_DATASET:LX/AjJ;

    :goto_2
    invoke-virtual {v2, v0}, LX/1KS;->a(LX/AjJ;)V

    .line 110351
    iget-object v0, p0, LX/0g2;->c:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->size()I

    move-result v0

    if-lez v0, :cond_a

    iget-object v0, p0, LX/0g2;->af:LX/0g5;

    invoke-virtual {v0}, LX/0g7;->q()I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 110352
    if-eqz v0, :cond_6

    .line 110353
    if-eqz v1, :cond_6

    .line 110354
    new-instance v2, LX/62h;

    iget-object v0, p0, LX/0g2;->T:Landroid/content/Context;

    invoke-direct {v2, v0}, LX/62h;-><init>(Landroid/content/Context;)V

    .line 110355
    new-instance v0, LX/DBf;

    invoke-direct {v0, p0, v2}, LX/DBf;-><init>(LX/0g2;LX/62h;)V

    invoke-virtual {v2, v0}, LX/62h;->setRetryClickListener(Landroid/view/View$OnClickListener;)V

    .line 110356
    iget-object v0, p0, LX/0g2;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2hU;

    const/16 v3, 0x2710

    invoke-virtual {v0, v2, v3}, LX/2hU;->a(Landroid/view/View;I)LX/4nS;

    move-result-object v0

    iput-object v0, p0, LX/0g2;->ag:LX/4nS;

    .line 110357
    iget-object v0, p0, LX/0g2;->ag:LX/4nS;

    iget-object v2, p0, LX/0g2;->ab:Landroid/view/View;

    .line 110358
    iput-object v2, v0, LX/4nS;->i:Landroid/view/View;

    .line 110359
    iget-object v0, p0, LX/0g2;->ag:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->a()V

    .line 110360
    :cond_6
    if-eqz p3, :cond_7

    iget-object v0, p0, LX/0g2;->T:Landroid/content/Context;

    if-nez v0, :cond_b

    .line 110361
    :cond_7
    :goto_4
    iput-boolean v1, p0, LX/0g2;->ak:Z

    goto/16 :goto_0

    .line 110362
    :cond_8
    sget-object v0, LX/AjJ;->HAS_STORIES:LX/AjJ;

    goto :goto_2

    .line 110363
    :pswitch_2
    iget-object v0, p0, LX/0g2;->Z:LX/1KS;

    sget-object v1, LX/AjJ;->CANCELED:LX/AjJ;

    invoke-virtual {v0, v1}, LX/1KS;->a(LX/AjJ;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :cond_9
    move-object v0, v6

    goto :goto_1

    :cond_a
    :try_start_3
    const/4 v0, 0x0

    goto :goto_3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 110364
    :cond_b
    iget-object v0, p0, LX/0g2;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Cz;

    iget-object v2, p0, LX/0g2;->T:Landroid/content/Context;

    invoke-virtual {v0, v2, p3}, LX/2Cz;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 110365
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 110366
    if-eqz v0, :cond_7

    .line 110367
    iget-object v0, p0, LX/0g2;->T:Landroid/content/Context;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Beta only: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static ab(LX/0g2;)LX/162;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 110368
    iget-object v0, p0, LX/0g2;->V:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->A()Z

    move-result v0

    if-nez v0, :cond_0

    .line 110369
    const/4 v0, 0x0

    .line 110370
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0g2;->af:LX/0g5;

    iget-object v1, p0, LX/0g2;->c:LX/0fz;

    iget-object v2, p0, LX/0g2;->ae:LX/1UQ;

    invoke-static {v0, v1, v2}, LX/Am9;->a(LX/0g5;LX/0fz;LX/1UQ;)LX/162;

    move-result-object v0

    goto :goto_0
.end method

.method public static ac(LX/0g2;)I
    .locals 3

    .prologue
    .line 110453
    iget-object v0, p0, LX/0g2;->V:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->A()Z

    move-result v0

    if-nez v0, :cond_0

    .line 110454
    const/4 v0, -0x1

    .line 110455
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/0g2;->af:LX/0g5;

    iget-object v1, p0, LX/0g2;->c:LX/0fz;

    iget-object v2, p0, LX/0g2;->ae:LX/1UQ;

    invoke-static {v0, v1, v2}, LX/Am9;->b(LX/0g5;LX/0fz;LX/1UQ;)I

    move-result v0

    goto :goto_0
.end method

.method public static ad(LX/0g2;)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 110371
    iget-object v1, p0, LX/0g2;->af:LX/0g5;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0g2;->af:LX/0g5;

    invoke-virtual {v1}, LX/0g7;->q()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 110372
    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, LX/0g2;->ac(LX/0g2;)I

    move-result v0

    goto :goto_0
.end method

.method public static af(LX/0g2;)V
    .locals 1

    .prologue
    .line 110480
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    instance-of v0, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    if-eqz v0, :cond_1

    .line 110481
    iget-object v0, p0, LX/0g2;->an:LX/1Iv;

    if-eqz v0, :cond_0

    .line 110482
    iget-object v0, p0, LX/0g2;->an:LX/1Iv;

    invoke-virtual {v0}, LX/1Iv;->d()V

    .line 110483
    :cond_0
    :goto_0
    return-void

    .line 110484
    :cond_1
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    invoke-interface {v0}, LX/0gC;->j()Z

    goto :goto_0
.end method

.method public static ah(LX/0g2;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 110460
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    instance-of v0, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    if-eqz v0, :cond_0

    .line 110461
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    check-cast v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    .line 110462
    iget-object v2, p0, LX/0g2;->an:LX/1Iv;

    if-eqz v2, :cond_1

    .line 110463
    iget-object v2, p0, LX/0g2;->an:LX/1Iv;

    .line 110464
    iget-object v3, v2, LX/1Iv;->o:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    move v2, v3

    .line 110465
    if-eqz v2, :cond_1

    .line 110466
    iget-object v2, p0, LX/0g2;->an:LX/1Iv;

    invoke-virtual {v2}, LX/1Iv;->l()V

    .line 110467
    iget-object v2, p0, LX/0g2;->an:LX/1Iv;

    .line 110468
    invoke-virtual {v2}, LX/1Iv;->j()LX/0Px;

    move-result-object v3

    .line 110469
    iget-object v4, v2, LX/1Iv;->o:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 110470
    move-object v2, v3

    .line 110471
    invoke-direct {p0}, LX/0g2;->ai()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->a(LX/0Px;Z)V

    move v0, v1

    .line 110472
    :goto_1
    return v0

    .line 110473
    :cond_0
    iget-object v0, p0, LX/0g2;->c:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->h()V

    move v0, v1

    .line 110474
    goto :goto_1

    .line 110475
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private ai()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 110456
    iget-object v1, p0, LX/0g2;->R:LX/1EU;

    invoke-virtual {v1}, LX/1EU;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/0g2;->S:LX/1NJ;

    invoke-virtual {v1}, LX/1NJ;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 110457
    :cond_0
    :goto_0
    return v0

    .line 110458
    :cond_1
    invoke-static {p0}, LX/0g2;->ak(LX/0g2;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 110459
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static ak(LX/0g2;)Z
    .locals 1

    .prologue
    .line 110485
    iget-object v0, p0, LX/0g2;->R:LX/1EU;

    invoke-virtual {v0}, LX/1EU;->a()Z

    move-result v0

    return v0
.end method

.method public static b(LX/0QB;)LX/0g2;
    .locals 40

    .prologue
    .line 110451
    new-instance v2, LX/0g2;

    invoke-static/range {p0 .. p0}, LX/1J3;->a(LX/0QB;)LX/1J3;

    move-result-object v3

    check-cast v3, LX/1J3;

    invoke-static/range {p0 .. p0}, LX/1J6;->a(LX/0QB;)LX/1J6;

    move-result-object v4

    check-cast v4, LX/1J6;

    invoke-static/range {p0 .. p0}, LX/1J9;->a(LX/0QB;)LX/1J9;

    move-result-object v5

    check-cast v5, LX/1J9;

    invoke-static/range {p0 .. p0}, LX/0pG;->a(LX/0QB;)LX/0pG;

    move-result-object v6

    check-cast v6, LX/0pG;

    const/16 v7, 0x5eb

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x5f7

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/0pV;->a(LX/0QB;)LX/0pV;

    move-result-object v9

    check-cast v9, LX/0pV;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0jU;->a(LX/0QB;)LX/0jU;

    move-result-object v11

    check-cast v11, LX/0jU;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v12

    check-cast v12, LX/0kb;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v13

    check-cast v13, LX/0Zb;

    invoke-static/range {p0 .. p0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v14

    check-cast v14, Lcom/facebook/common/perftest/PerfTestConfig;

    const/16 v15, 0x12c1

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/0Yi;->a(LX/0QB;)LX/0Yi;

    move-result-object v16

    check-cast v16, LX/0Yi;

    invoke-static/range {p0 .. p0}, LX/1JC;->a(LX/0QB;)LX/1JC;

    move-result-object v17

    check-cast v17, LX/1JC;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->a(LX/0QB;)Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;

    move-result-object v18

    check-cast v18, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;

    const/16 v19, 0x64

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    invoke-static/range {p0 .. p0}, LX/0iS;->a(LX/0QB;)LX/0iS;

    move-result-object v20

    check-cast v20, LX/0iS;

    const/16 v21, 0x11fe

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v21

    invoke-static/range {p0 .. p0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v22

    check-cast v22, LX/17Q;

    invoke-static/range {p0 .. p0}, LX/0pT;->a(LX/0QB;)LX/0pT;

    move-result-object v23

    check-cast v23, LX/0pT;

    invoke-static/range {p0 .. p0}, LX/0pl;->a(LX/0QB;)LX/0pl;

    move-result-object v24

    check-cast v24, LX/0pl;

    invoke-static/range {p0 .. p0}, LX/1JG;->a(LX/0QB;)LX/1JG;

    move-result-object v25

    check-cast v25, LX/1JG;

    invoke-static/range {p0 .. p0}, LX/0qa;->a(LX/0QB;)LX/0qa;

    move-result-object v26

    check-cast v26, LX/0qa;

    invoke-static/range {p0 .. p0}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v27

    check-cast v27, LX/0aG;

    invoke-static/range {p0 .. p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v28

    check-cast v28, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static/range {p0 .. p0}, LX/0oa;->a(LX/0QB;)LX/0Ot;

    move-result-object v29

    invoke-static/range {p0 .. p0}, LX/0oy;->a(LX/0QB;)LX/0oy;

    move-result-object v30

    check-cast v30, LX/0oy;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v31

    check-cast v31, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v32

    check-cast v32, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0qX;->a(LX/0QB;)LX/0qX;

    move-result-object v33

    check-cast v33, LX/0qX;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v34

    check-cast v34, LX/0Tf;

    invoke-static/range {p0 .. p0}, LX/0pJ;->a(LX/0QB;)LX/0pJ;

    move-result-object v35

    check-cast v35, LX/0pJ;

    invoke-static/range {p0 .. p0}, LX/0kl;->a(LX/0QB;)LX/0Wd;

    move-result-object v36

    check-cast v36, LX/0Wd;

    invoke-static/range {p0 .. p0}, LX/1JH;->a(LX/0QB;)LX/1JH;

    move-result-object v37

    check-cast v37, LX/1JH;

    const-class v38, LX/1JU;

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v38

    check-cast v38, LX/1JU;

    invoke-static/range {p0 .. p0}, LX/1EU;->a(LX/0QB;)LX/1EU;

    move-result-object v39

    check-cast v39, LX/1EU;

    invoke-direct/range {v2 .. v39}, LX/0g2;-><init>(LX/1J3;LX/1J6;LX/1J9;LX/0pG;LX/0Or;LX/0Or;LX/0pV;LX/0ad;LX/0jU;LX/0kb;LX/0Zb;Lcom/facebook/common/perftest/PerfTestConfig;LX/0Ot;LX/0Yi;LX/1JC;Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;LX/0Ot;LX/0iS;LX/0Ot;LX/17Q;LX/0pT;LX/0pl;LX/1JG;LX/0qa;LX/0aG;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Ot;LX/0oy;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0qX;LX/0Tf;LX/0pJ;LX/0Wd;LX/1JH;LX/1JU;LX/1EU;)V

    .line 110452
    return-object v2
.end method

.method public static b(LX/0g2;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 110438
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "position"

    invoke-static {p0}, LX/0g2;->ad(LX/0g2;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "tracking"

    invoke-static {p0}, LX/0g2;->ab(LX/0g2;)LX/162;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "ranking_time"

    const/4 v3, 0x0

    .line 110439
    iget-object v4, p0, LX/0g2;->V:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v4}, Lcom/facebook/feed/fragment/NewsFeedFragment;->A()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, LX/0g2;->c:LX/0fz;

    if-nez v4, :cond_1

    .line 110440
    :cond_0
    :goto_0
    move-object v2, v3

    .line 110441
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "num_stories_fetched_in_last_result"

    iget v2, p0, LX/0g2;->ai:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "android_pull_to_refresh"

    .line 110442
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 110443
    move-object v0, v0

    .line 110444
    iget-object v1, p0, LX/0g2;->r:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 110445
    return-void

    .line 110446
    :cond_1
    invoke-static {p0}, LX/0g2;->ac(LX/0g2;)I

    move-result v4

    .line 110447
    if-ltz v4, :cond_0

    iget-object v5, p0, LX/0g2;->c:LX/0fz;

    invoke-virtual {v5}, LX/0fz;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 110448
    iget-object v5, p0, LX/0g2;->c:LX/0fz;

    invoke-virtual {v5, v4}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v4

    .line 110449
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v5

    instance-of v5, v5, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v5, :cond_0

    .line 110450
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aM()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0
.end method

.method public static b(LX/0g2;ILX/1lq;)Z
    .locals 3

    .prologue
    .line 110425
    if-lez p1, :cond_2

    iget-object v0, p0, LX/0g2;->V:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, LX/1lq;->showNewStoryPill()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 110426
    :cond_0
    iget-object v0, p0, LX/0g2;->H:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rB;

    .line 110427
    iget-object v2, v0, LX/0rB;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v2, v2

    .line 110428
    iget-object p1, p0, LX/0g2;->Y:Lcom/facebook/api/feedtype/FeedType;

    .line 110429
    iget-object p2, p1, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object p1, p2

    .line 110430
    invoke-virtual {v2, p1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 110431
    :goto_0
    move-object v0, v0

    .line 110432
    if-eqz v0, :cond_3

    .line 110433
    iget-boolean v1, v0, LX/0rB;->c:Z

    move v0, v1

    .line 110434
    :goto_1
    move v0, v0

    .line 110435
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0g2;->U:Lcom/facebook/feed/fragment/NewsFeedFragment;

    .line 110436
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mDetached:Z

    move v0, v1

    .line 110437
    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/0g2;Z)V
    .locals 14

    .prologue
    .line 110385
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    invoke-interface {v0}, LX/0gC;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110386
    iget-object v0, p0, LX/0g2;->n:LX/0pV;

    sget-object v1, LX/0g2;->e:Ljava/lang/String;

    const-string v2, "warm refresh skipped: loading"

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 110387
    const/4 v0, 0x1

    .line 110388
    :goto_0
    iget-object v1, p0, LX/0g2;->u:LX/0Yi;

    .line 110389
    iput-boolean v0, v1, LX/0Yi;->F:Z

    .line 110390
    iget-boolean v2, v1, LX/0Yi;->r:Z

    if-eqz v2, :cond_0

    if-nez v0, :cond_0

    .line 110391
    iget-object v2, v1, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const p0, 0xa0014

    const-string v0, "NNFFreshContentStart"

    invoke-interface {v2, p0, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 110392
    :cond_0
    return-void

    .line 110393
    :cond_1
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 110394
    iget-object v3, p0, LX/0g2;->b:LX/0gC;

    instance-of v3, v3, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    if-nez v3, :cond_3

    move v3, v4

    .line 110395
    :goto_1
    move v0, v3

    .line 110396
    or-int/lit8 v0, v0, 0x0

    .line 110397
    const/4 v1, 0x0

    .line 110398
    iget-object v2, p0, LX/0g2;->b:LX/0gC;

    invoke-interface {v2}, LX/0gC;->i()Z

    move-result v2

    if-nez v2, :cond_a

    .line 110399
    iget-object v2, p0, LX/0g2;->n:LX/0pV;

    sget-object v3, LX/0g2;->e:Ljava/lang/String;

    const-string v4, "warm start auto refresh skipped: disallowed"

    invoke-virtual {v2, v3, v4}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 110400
    :cond_2
    :goto_2
    move v1, v1

    .line 110401
    or-int/2addr v0, v1

    goto :goto_0

    .line 110402
    :cond_3
    iget-object v3, p0, LX/0g2;->b:LX/0gC;

    check-cast v3, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    .line 110403
    if-eqz p1, :cond_4

    invoke-virtual {v3}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->l()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 110404
    iget-object v3, p0, LX/0g2;->n:LX/0pV;

    sget-object v4, LX/0g2;->e:Ljava/lang/String;

    const-string v6, "fresh warm refresh immediately"

    invoke-virtual {v3, v4, v6}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v5

    .line 110405
    goto :goto_1

    .line 110406
    :cond_4
    iget-object v6, p0, LX/0g2;->an:LX/1Iv;

    if-nez v6, :cond_5

    .line 110407
    iget-object v3, p0, LX/0g2;->n:LX/0pV;

    sget-object v5, LX/0g2;->e:Ljava/lang/String;

    const-string v6, "fresh warm auto refresh not available"

    invoke-virtual {v3, v5, v6}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    .line 110408
    goto :goto_1

    .line 110409
    :cond_5
    iget-object v6, p0, LX/0g2;->an:LX/1Iv;

    invoke-virtual {v6}, LX/1Iv;->n()Z

    move-result v6

    if-nez v6, :cond_6

    .line 110410
    iget-object v3, p0, LX/0g2;->n:LX/0pV;

    sget-object v5, LX/0g2;->e:Ljava/lang/String;

    const-string v6, "warm start auto refresh skipped: disallowed"

    invoke-virtual {v3, v5, v6}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    .line 110411
    goto :goto_1

    .line 110412
    :cond_6
    if-eqz p1, :cond_7

    iget-object v6, p0, LX/0g2;->an:LX/1Iv;

    const/4 v11, 0x0

    .line 110413
    iget-object v8, v6, LX/1Iv;->k:LX/0r8;

    invoke-virtual {v8}, LX/0r8;->e()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 110414
    :goto_3
    move v6, v11

    .line 110415
    if-eqz v6, :cond_7

    .line 110416
    iget-object v4, p0, LX/0g2;->n:LX/0pV;

    sget-object v6, LX/0g2;->e:Ljava/lang/String;

    const-string v7, "fresh warm auto refresh immediately"

    invoke-virtual {v4, v6, v7}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 110417
    invoke-virtual {v3}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->O()V

    move v3, v5

    .line 110418
    goto :goto_1

    .line 110419
    :cond_7
    iget-object v3, p0, LX/0g2;->n:LX/0pV;

    sget-object v5, LX/0g2;->e:Ljava/lang/String;

    const-string v6, "fresh warm auto refresh scheduled"

    invoke-virtual {v3, v5, v6}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 110420
    iget-object v3, p0, LX/0g2;->an:LX/1Iv;

    invoke-virtual {v3}, LX/1Iv;->d()V

    move v3, v4

    .line 110421
    goto :goto_1

    :cond_8
    invoke-static {v6}, LX/1Iv;->u(LX/1Iv;)LX/0r5;

    move-result-object v8

    iget-object v9, v6, LX/1Iv;->e:LX/0SG;

    invoke-interface {v9}, LX/0SG;->a()J

    move-result-wide v9

    iget-object v12, v6, LX/1Iv;->b:LX/1EM;

    invoke-virtual {v12}, LX/1EM;->d()Z

    move-result v12

    if-nez v12, :cond_9

    iget-object v12, v6, LX/1Iv;->b:LX/1EM;

    invoke-virtual {v12}, LX/1EM;->c()Z

    move-result v12

    if-eqz v12, :cond_9

    const/4 v11, 0x1

    :cond_9
    iget-object v12, v6, LX/1Iv;->b:LX/1EM;

    invoke-virtual {v12}, LX/1EM;->b()J

    move-result-wide v12

    invoke-virtual/range {v8 .. v13}, LX/0r5;->a(JZJ)Z

    move-result v11

    goto :goto_3

    .line 110422
    :cond_a
    iget-object v2, p0, LX/0g2;->b:LX/0gC;

    instance-of v2, v2, Lcom/facebook/feed/data/FeedDataLoader;

    if-eqz v2, :cond_2

    .line 110423
    iget-object v1, p0, LX/0g2;->b:LX/0gC;

    check-cast v1, Lcom/facebook/feed/data/FeedDataLoader;

    .line 110424
    invoke-virtual {v1}, Lcom/facebook/feed/data/FeedDataLoader;->l()Z

    move-result v1

    goto/16 :goto_2
.end method

.method private static d(LX/0g2;LX/0gf;)V
    .locals 3

    .prologue
    .line 110379
    const-string v0, "NewsFeedFragment.selectRowIfRerank"

    const v1, 0x1f6731e6

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 110380
    :try_start_0
    iget-object v0, p0, LX/0g2;->af:LX/0g5;

    if-eqz v0, :cond_0

    sget-object v0, LX/0gf;->RERANK:LX/0gf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq p1, v0, :cond_1

    .line 110381
    :cond_0
    const v0, -0x70514304

    invoke-static {v0}, LX/02m;->a(I)V

    .line 110382
    :goto_0
    return-void

    .line 110383
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/0g2;->af:LX/0g5;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0g7;->d(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 110384
    const v0, 0x6c882f7f

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x517c4a2a

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static e(LX/0g2;I)V
    .locals 3

    .prologue
    .line 110373
    iget-object v0, p0, LX/0g2;->o:LX/0ad;

    sget v1, LX/0fe;->aA:I

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 110374
    :cond_0
    :goto_0
    return-void

    .line 110375
    :cond_1
    iget-object v0, p0, LX/0g2;->ah:LX/1Z9;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1Z9;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110376
    const-string v0, "feed_new_story_pill_shown"

    invoke-static {p0, v0}, LX/0g2;->b(LX/0g2;Ljava/lang/String;)V

    .line 110377
    sget-object v0, LX/0rj;->NEW_STORY_PILL_SHOWN:LX/0rj;

    invoke-static {p0, v0}, LX/0g2;->a(LX/0g2;LX/0rj;)V

    .line 110378
    goto :goto_0
.end method


# virtual methods
.method public final C()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 110168
    iget-object v0, p0, LX/0g2;->ah:LX/1Z9;

    invoke-virtual {v0, v2}, LX/1Z9;->a(Z)Z

    move-result v0

    .line 110169
    iget v1, p0, LX/0g2;->ai:I

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 110170
    const-string v0, "feed_new_story_pill_hidden"

    invoke-static {p0, v0}, LX/0g2;->b(LX/0g2;Ljava/lang/String;)V

    .line 110171
    sget-object v0, LX/0rj;->NEW_STORY_PILL_HIDDEN:LX/0rj;

    invoke-static {p0, v0}, LX/0g2;->a(LX/0g2;LX/0rj;)V

    .line 110172
    :cond_0
    iput v2, p0, LX/0g2;->ai:I

    .line 110173
    iget-object v0, p0, LX/0g2;->y:LX/0iS;

    iget v1, p0, LX/0g2;->ai:I

    invoke-virtual {v0, v1}, LX/0iS;->a(I)V

    .line 110174
    return-void
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 109943
    iget-object v0, p0, LX/0g2;->W:LX/1CY;

    invoke-virtual {v0}, LX/1CY;->b()V

    .line 109944
    iget-object v0, p0, LX/0g2;->p:LX/0jU;

    invoke-virtual {v0}, LX/0jU;->a()V

    .line 109945
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 109946
    iget-object v0, p0, LX/0g2;->V:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->z()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0g2;->V:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-interface {v0}, LX/0fv;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 109947
    :goto_0
    invoke-static {p0}, LX/0g2;->ak(LX/0g2;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 109948
    if-eqz v0, :cond_3

    sget-object v0, LX/1lq;->AvoidNewStoryPill:LX/1lq;

    :goto_1
    iget-object v1, p0, LX/0g2;->c:LX/0fz;

    invoke-virtual {v1}, LX/0fz;->i()I

    move-result v1

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-virtual {p0, p1, v0, v1, v2}, LX/0g2;->a(ILX/1lq;ILX/0ta;)V

    .line 109949
    :cond_1
    return-void

    .line 109950
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 109951
    :cond_3
    sget-object v0, LX/1lq;->ShowNewStoryPill:LX/1lq;

    goto :goto_1
.end method

.method public final a(ILX/1lq;ILX/0ta;)V
    .locals 6

    .prologue
    .line 109952
    const-string v0, "NewsfeedFragmentDataController.onHeadLoadComplete"

    const v1, -0x5bc5bfb

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 109953
    :try_start_0
    iget-object v0, p0, LX/0g2;->V:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->A()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 109954
    const v0, 0x53722c

    invoke-static {v0}, LX/02m;->a(I)V

    .line 109955
    :goto_0
    return-void

    .line 109956
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0g2;->Z:LX/1KS;

    const/4 v1, 0x1

    const/4 v2, 0x1

    move v3, p1

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, LX/1KS;->a(ZZIILX/0ta;)V

    .line 109957
    sget-object v0, LX/1lp;->a:[I

    invoke-virtual {p2}, LX/1lq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 109958
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 109959
    invoke-static {p0}, LX/0g2;->ak(LX/0g2;)Z

    move-result v2

    if-eqz v2, :cond_2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 109960
    :cond_1
    :goto_1
    :pswitch_0
    const v0, -0x388b185b

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 109961
    :pswitch_1
    :try_start_2
    iget-object v0, p0, LX/0g2;->S:LX/1NJ;

    invoke-virtual {v0, p2}, LX/1NJ;->a(LX/1lq;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 109962
    :catchall_0
    move-exception v0

    const v1, 0x9f6f82e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 109963
    :cond_2
    invoke-static {p0, p1, p2}, LX/0g2;->b(LX/0g2;ILX/1lq;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 109964
    invoke-static {p0}, LX/0g2;->M(LX/0g2;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 109965
    iput-boolean v1, p0, LX/0g2;->d:Z

    .line 109966
    :cond_3
    :goto_2
    iget-object v1, p0, LX/0g2;->V:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v1}, Lcom/facebook/feed/fragment/NewsFeedFragment;->z()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p2}, LX/1lq;->avoidNewStoryPill()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 109967
    invoke-static {p0}, LX/0g2;->ah(LX/0g2;)Z

    .line 109968
    invoke-virtual {p0}, LX/0g2;->C()V

    .line 109969
    :cond_4
    if-lez p1, :cond_1

    iget-object v1, p0, LX/0g2;->Y:Lcom/facebook/api/feedtype/FeedType;

    .line 109970
    iget-object v2, v1, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v1, v2

    .line 109971
    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 109972
    iget-object v0, p0, LX/0g2;->y:LX/0iS;

    invoke-virtual {v0, p1}, LX/0iS;->a(I)V

    goto :goto_1

    .line 109973
    :cond_5
    iput-boolean v0, p0, LX/0g2;->d:Z

    .line 109974
    invoke-static {p0, p1}, LX/0g2;->e(LX/0g2;I)V

    move v0, v1

    .line 109975
    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/0gf;)V
    .locals 2

    .prologue
    .line 109976
    iget-object v0, p0, LX/0g2;->Z:LX/1KS;

    sget-object v1, LX/1lr;->HEAD:LX/1lr;

    invoke-virtual {v0, p1, v1}, LX/1KS;->a(LX/0gf;LX/1lr;)V

    .line 109977
    iget-object v0, p0, LX/0g2;->w:Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;

    .line 109978
    const/4 v1, 0x0

    iput v1, v0, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->a:I

    .line 109979
    return-void
.end method

.method public final a(LX/0gf;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 109980
    invoke-virtual {p1}, LX/0gf;->isNewStoriesFetch()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109981
    iget-object v0, p0, LX/0g2;->n:LX/0pV;

    sget-object v1, LX/0g2;->e:Ljava/lang/String;

    const-string v2, "Network load failed, scheduling auto refresh on network available"

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 109982
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0g2;->ao:Z

    .line 109983
    :cond_0
    return-void
.end method

.method public final a(LX/1UQ;)V
    .locals 2

    .prologue
    .line 109984
    iput-object p1, p0, LX/0g2;->ae:LX/1UQ;

    .line 109985
    iget-object v0, p0, LX/0g2;->h:LX/1J3;

    iget-object v1, p0, LX/0g2;->ae:LX/1UQ;

    .line 109986
    iput-object v1, v0, LX/1J3;->b:LX/1UQ;

    .line 109987
    iget-object v0, p0, LX/0g2;->i:LX/1J6;

    iget-object v1, p0, LX/0g2;->ae:LX/1UQ;

    .line 109988
    iput-object v1, v0, LX/1J6;->b:LX/1UQ;

    .line 109989
    iget-object v0, p0, LX/0g2;->S:LX/1NJ;

    .line 109990
    iput-object p1, v0, LX/1NJ;->h:LX/1UQ;

    .line 109991
    return-void
.end method

.method public final a(LX/69s;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 109992
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 109993
    if-nez p1, :cond_1

    .line 109994
    :cond_0
    :goto_0
    return-void

    .line 109995
    :cond_1
    iget-object v0, p0, LX/0g2;->am:Lcom/facebook/feed/loader/FeedDataLoaderInitializationParams;

    if-eqz v0, :cond_0

    .line 109996
    const-string v0, "data_loader_init_params"

    iget-object v1, p0, LX/0g2;->am:Lcom/facebook/feed/loader/FeedDataLoaderInitializationParams;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 11

    .prologue
    .line 109997
    iput-object p1, p0, LX/0g2;->ab:Landroid/view/View;

    .line 109998
    iget-boolean v0, p0, LX/0g2;->al:Z

    if-eqz v0, :cond_0

    .line 109999
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    invoke-interface {v0}, LX/0gC;->u()Z

    .line 110000
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    iget-object v1, p0, LX/0g2;->am:Lcom/facebook/feed/loader/FeedDataLoaderInitializationParams;

    invoke-interface {v0, v1}, LX/0gC;->a(Lcom/facebook/feed/loader/FeedDataLoaderInitializationParams;)Z

    .line 110001
    :cond_0
    iget-object v1, p0, LX/0g2;->j:LX/1J9;

    iget-object v0, p0, LX/0g2;->ab:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, p0, LX/0g2;->c:LX/0fz;

    .line 110002
    iget-object v3, v1, LX/1J9;->a:LX/1JA;

    .line 110003
    iget-boolean v4, v3, LX/1JA;->a:Z

    move v3, v4

    .line 110004
    if-eqz v3, :cond_1

    iget-object v3, v1, LX/1J9;->b:LX/1JB;

    .line 110005
    new-instance v4, LX/AlI;

    invoke-static {v3}, LX/1JA;->b(LX/0QB;)LX/1JA;

    move-result-object v5

    check-cast v5, LX/1JA;

    const-class v6, LX/323;

    invoke-interface {v3, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/323;

    const-class v7, LX/AlH;

    invoke-interface {v3, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/AlH;

    move-object v8, p0

    move-object v9, v0

    move-object v10, v2

    invoke-direct/range {v4 .. v10}, LX/AlI;-><init>(LX/1JA;LX/323;LX/AlH;LX/0g2;Landroid/view/ViewGroup;LX/0fz;)V

    .line 110006
    move-object v3, v4

    .line 110007
    :goto_0
    move-object v0, v3

    .line 110008
    iget-object v1, p0, LX/0g2;->V:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->a(LX/0fx;)LX/0fx;

    move-result-object v0

    iput-object v0, p0, LX/0g2;->ad:LX/0fx;

    .line 110009
    return-void

    :cond_1
    new-instance v3, LX/1ZB;

    invoke-direct {v3}, LX/1ZB;-><init>()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/fragment/NewsFeedFragment;Lcom/facebook/feed/fragment/NewsFeedFragment;Lcom/facebook/api/feedtype/FeedType;LX/1CY;LX/0bH;LX/1Iv;LX/1EM;Landroid/content/Context;Landroid/os/Bundle;LX/1KS;)V
    .locals 3
    .param p9    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 110010
    iput-object p1, p0, LX/0g2;->U:Lcom/facebook/feed/fragment/NewsFeedFragment;

    .line 110011
    iput-object p2, p0, LX/0g2;->V:Lcom/facebook/feed/fragment/NewsFeedFragment;

    .line 110012
    iput-object p4, p0, LX/0g2;->W:LX/1CY;

    .line 110013
    iput-object p3, p0, LX/0g2;->Y:Lcom/facebook/api/feedtype/FeedType;

    .line 110014
    iput-object p5, p0, LX/0g2;->X:LX/0bH;

    .line 110015
    iput-object p8, p0, LX/0g2;->T:Landroid/content/Context;

    .line 110016
    iput-object p10, p0, LX/0g2;->Z:LX/1KS;

    .line 110017
    iput-object p6, p0, LX/0g2;->an:LX/1Iv;

    .line 110018
    iput-object p7, p0, LX/0g2;->aa:LX/1EM;

    .line 110019
    if-eqz p9, :cond_0

    .line 110020
    const-string v0, "data_loader_init_params"

    invoke-virtual {p9, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/loader/FeedDataLoaderInitializationParams;

    iput-object v0, p0, LX/0g2;->am:Lcom/facebook/feed/loader/FeedDataLoaderInitializationParams;

    .line 110021
    :cond_0
    iget-object v0, p0, LX/0g2;->p:LX/0jU;

    invoke-virtual {v0, p0}, LX/0jU;->a(LX/0fi;)V

    .line 110022
    invoke-static {p0}, LX/0g2;->O(LX/0g2;)V

    .line 110023
    iget-object v0, p0, LX/0g2;->V:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v1, p0, LX/0g2;->i:LX/1J6;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/fragment/NewsFeedFragment;->a(LX/0fx;)LX/0fx;

    move-result-object v0

    iput-object v0, p0, LX/0g2;->ac:LX/0fx;

    .line 110024
    iget-object v0, p0, LX/0g2;->B:LX/0pT;

    invoke-virtual {v0}, LX/0pT;->d()V

    .line 110025
    iget-object v0, p0, LX/0g2;->Q:LX/1JU;

    .line 110026
    iget-object v1, p0, LX/0g2;->c:LX/0fz;

    move-object v1, v1

    .line 110027
    iget-object v2, p0, LX/0g2;->aa:LX/1EM;

    invoke-virtual {v0, v1, v2}, LX/1JU;->a(LX/0fz;LX/1EM;)LX/1NJ;

    move-result-object v0

    iput-object v0, p0, LX/0g2;->S:LX/1NJ;

    .line 110028
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 1

    .prologue
    .line 110159
    iget-object v0, p0, LX/0g2;->c:LX/0fz;

    .line 110160
    iget-object p0, v0, LX/0fz;->a:LX/0qq;

    move-object v0, p0

    .line 110161
    invoke-virtual {v0, p1}, LX/0qq;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 110162
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 110029
    iget-object v0, p0, LX/0g2;->af:LX/0g5;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0g2;->af:LX/0g5;

    .line 110030
    iget-object v1, v0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v1

    .line 110031
    if-eqz v0, :cond_0

    .line 110032
    iget-object v0, p0, LX/0g2;->af:LX/0g5;

    .line 110033
    iget-object v1, v0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v1

    .line 110034
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 110035
    invoke-virtual {v0, p1}, LX/1P1;->a(Z)V

    .line 110036
    :try_start_0
    const-class v1, LX/1P1;

    const-string v2, "b"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 110037
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 110038
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 110039
    :cond_0
    return-void

    .line 110040
    :catch_0
    move-exception v0

    .line 110041
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(ZILX/0ta;)V
    .locals 6

    .prologue
    .line 110042
    iget-object v0, p0, LX/0g2;->Z:LX/1KS;

    const/4 v1, 0x0

    iget-object v2, p0, LX/0g2;->c:LX/0fz;

    invoke-virtual {v2}, LX/0fz;->i()I

    move-result v4

    move v2, p1

    move v3, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, LX/1KS;->a(ZZIILX/0ta;)V

    .line 110043
    return-void
.end method

.method public final a(ZLcom/facebook/api/feed/FetchFeedResult;LX/18G;Ljava/lang/String;ILX/0qw;LX/18O;)V
    .locals 6

    .prologue
    .line 110044
    const-string v0, "NewsfeedFragment.onLoadingComplete"

    const v1, 0x456cc2ee

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 110045
    if-lez p5, :cond_0

    :try_start_0
    iget-object v0, p0, LX/0g2;->Y:Lcom/facebook/api/feedtype/FeedType;

    .line 110046
    iget-object v1, v0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v0, v1

    .line 110047
    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110048
    iget v0, p0, LX/0g2;->ai:I

    add-int/2addr v0, p5

    iput v0, p0, LX/0g2;->ai:I

    .line 110049
    :cond_0
    iget-object v0, p0, LX/0g2;->v:LX/1JC;

    iget-object v5, p0, LX/0g2;->c:LX/0fz;

    move v1, p1

    move-object v2, p6

    move v3, p5

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, LX/1JC;->a(ZLX/0qw;ILcom/facebook/api/feed/FetchFeedResult;LX/0fz;)V

    .line 110050
    iget-object v0, p0, LX/0g2;->w:Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;

    invoke-virtual {v0, p2}, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 110051
    iget-object v0, p0, LX/0g2;->V:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->A()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p0

    move v1, p1

    move-object v2, p3

    move-object v3, p4

    move v4, p5

    move-object v5, p2

    .line 110052
    invoke-direct/range {v0 .. v5}, LX/0g2;->a(ZLX/18G;Ljava/lang/String;ILcom/facebook/api/feed/FetchFeedResult;)V

    .line 110053
    :cond_1
    if-nez p1, :cond_2

    invoke-static {p0}, LX/0g2;->H(LX/0g2;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 110054
    iget-object v0, p0, LX/0g2;->O:LX/0Wd;

    new-instance v1, Lcom/facebook/feed/fragment/NewsFeedFragmentDataController$1;

    invoke-direct {v1, p0}, Lcom/facebook/feed/fragment/NewsFeedFragmentDataController$1;-><init>(LX/0g2;)V

    const v2, 0x556613c3

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 110055
    :cond_2
    if-eqz p1, :cond_3

    sget-object v0, LX/18G;->SUCCESS:LX/18G;

    if-ne p3, v0, :cond_3

    .line 110056
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0g2;->ao:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110057
    :cond_3
    const v0, 0x2f7cd502

    invoke-static {v0}, LX/02m;->a(I)V

    .line 110058
    return-void

    .line 110059
    :catchall_0
    move-exception v0

    const v1, 0x13b54328

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b(LX/0gf;)V
    .locals 0

    .prologue
    .line 110060
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 110061
    iget-object v0, p0, LX/0g2;->V:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110062
    iget-object v0, p0, LX/0g2;->q:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    .line 110063
    iput-boolean v0, p0, LX/0g2;->ak:Z

    .line 110064
    if-eqz v0, :cond_1

    iget-boolean v1, p0, LX/0g2;->ao:Z

    if-eqz v1, :cond_1

    .line 110065
    iget-object v0, p0, LX/0g2;->n:LX/0pV;

    sget-object v1, LX/0g2;->e:Ljava/lang/String;

    const-string v2, "Kicking off auto refresh on connectivity changed"

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 110066
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0g2;->ao:Z

    .line 110067
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    instance-of v0, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    if-eqz v0, :cond_2

    .line 110068
    iget-object v0, p0, LX/0g2;->an:LX/1Iv;

    if-eqz v0, :cond_0

    .line 110069
    iget-object v0, p0, LX/0g2;->an:LX/1Iv;

    .line 110070
    invoke-virtual {v0}, LX/1Iv;->n()Z

    move-result p0

    if-nez p0, :cond_3

    .line 110071
    :cond_0
    :goto_0
    return-void

    .line 110072
    :cond_1
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0g2;->U:Lcom/facebook/feed/fragment/NewsFeedFragment;

    .line 110073
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v1

    .line 110074
    if-eqz v0, :cond_0

    .line 110075
    invoke-static {p0}, LX/0g2;->af(LX/0g2;)V

    .line 110076
    iget-object v0, p0, LX/0g2;->i:LX/1J6;

    invoke-virtual {v0}, LX/1J6;->d()V

    goto :goto_0

    .line 110077
    :cond_2
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    invoke-interface {v0}, LX/0gC;->k()V

    goto :goto_0

    .line 110078
    :cond_3
    iget-object p0, v0, LX/1Iv;->k:LX/0r8;

    invoke-virtual {p0}, LX/0r8;->e()Z

    move-result p0

    if-nez p0, :cond_0

    .line 110079
    invoke-static {v0}, LX/1Iv;->u(LX/1Iv;)LX/0r5;

    move-result-object p0

    invoke-virtual {p0}, LX/0r5;->a()V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 110080
    iget-object v0, p0, LX/0g2;->af:LX/0g5;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0g2;->af:LX/0g5;

    invoke-virtual {v0}, LX/0g7;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(LX/0gf;)V
    .locals 10

    .prologue
    .line 110081
    iget-object v0, p0, LX/0g2;->Z:LX/1KS;

    .line 110082
    iget-object v1, v0, LX/1KS;->m:LX/0Ot;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/1KS;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/1KS;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1PI;

    invoke-virtual {v1}, LX/0hD;->kw_()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110083
    iget-object v1, v0, LX/1KS;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1PI;

    .line 110084
    iget-object v2, v1, LX/1PI;->a:LX/1PJ;

    sget-object v3, LX/1PL;->IS_LOADING:LX/1PL;

    invoke-virtual {v2, v3}, LX/1PJ;->a(LX/1PL;)V

    .line 110085
    :cond_0
    iget-object v1, v0, LX/1KS;->s:LX/1DO;

    if-eqz v1, :cond_1

    .line 110086
    const/4 v1, 0x1

    move v1, v1

    .line 110087
    if-eqz v1, :cond_1

    .line 110088
    iget-object v1, v0, LX/1KS;->s:LX/1DO;

    invoke-virtual {v1, p1}, LX/1DO;->a(LX/0gf;)V

    .line 110089
    :cond_1
    iget-object v1, v0, LX/1KS;->D:LX/0Ot;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/1KS;->D:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/1KS;->D:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 110090
    const/4 v1, 0x1

    move v1, v1

    .line 110091
    if-eqz v1, :cond_2

    .line 110092
    iget-object v1, v0, LX/1KS;->D:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Km;

    invoke-virtual {v1, p1}, LX/1Km;->a(LX/0gf;)V

    .line 110093
    :cond_2
    iget-object v1, v0, LX/1KS;->T:LX/1DC;

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/1KS;->T:LX/1DC;

    invoke-virtual {v1}, LX/0hD;->kw_()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 110094
    iget-object v1, v0, LX/1KS;->T:LX/1DC;

    .line 110095
    iget-object v2, v1, LX/1DC;->d:LX/BaO;

    if-eqz v2, :cond_3

    .line 110096
    iget-object v2, v1, LX/1DC;->d:LX/BaO;

    .line 110097
    iget-object v3, v2, LX/BaO;->b:LX/AK0;

    const/4 v4, 0x1

    sget-object v1, LX/AJz;->USER_PULL_TO_REFRESH:LX/AJz;

    invoke-virtual {v3, v4, v1}, LX/AK0;->a(ZLX/AJz;)V

    .line 110098
    :cond_3
    iget-object v1, v0, LX/1KS;->ab:LX/0Ot;

    if-eqz v1, :cond_4

    iget-object v1, v0, LX/1KS;->ab:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, v0, LX/1KS;->ab:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Zk;

    invoke-virtual {v1}, LX/0hD;->kw_()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 110099
    iget-object v1, v0, LX/1KS;->ab:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Zk;

    .line 110100
    sget-object v2, LX/0gf;->PULL_TO_REFRESH:LX/0gf;

    invoke-virtual {p1, v2}, LX/0gf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 110101
    invoke-static {v1}, LX/1Zk;->b(LX/1Zk;)V

    .line 110102
    :cond_4
    invoke-virtual {p1}, LX/0gf;->isManual()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 110103
    iget-object v0, p0, LX/0g2;->D:LX/1JG;

    .line 110104
    iget-object v2, v0, LX/1JG;->c:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 110105
    iget-object v6, v0, LX/1JG;->a:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    .line 110106
    const-wide/16 v8, 0xe10

    invoke-virtual {v0, v8, v9}, LX/1JG;->a(J)I

    move-result v7

    .line 110107
    iget-object v8, v0, LX/1JG;->a:Ljava/util/List;

    const/4 v9, 0x0

    sub-int/2addr v6, v7

    invoke-interface {v8, v9, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 110108
    iget-object v2, v0, LX/1JG;->a:Ljava/util/List;

    iget-object v3, v0, LX/1JG;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110109
    iget-object v0, p0, LX/0g2;->u:LX/0Yi;

    const/4 v3, 0x0

    .line 110110
    iput-boolean v3, v0, LX/0Yi;->B:Z

    .line 110111
    const v1, 0xa0040

    const-string v2, "NNFPullToRefreshNetworkTime"

    invoke-static {v0, v1, v2, v3}, LX/0Yi;->a(LX/0Yi;ILjava/lang/String;Z)V

    .line 110112
    const v1, 0xa0041

    invoke-static {v0, v1, v3}, LX/0Yi;->a(LX/0Yi;IZ)V

    .line 110113
    iget-object v1, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0055

    const-string v3, "NNFPullToRefreshBeforeExecuteTime"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 110114
    iget-object v0, p0, LX/0g2;->af:LX/0g5;

    if-eqz v0, :cond_5

    .line 110115
    iget-object v0, p0, LX/0g2;->af:LX/0g5;

    .line 110116
    new-instance v1, LX/DBg;

    invoke-direct {v1, p0}, LX/DBg;-><init>(LX/0g2;)V

    move-object v1, v1

    .line 110117
    invoke-virtual {v0, v1}, LX/0g7;->b(LX/0fu;)V

    .line 110118
    :cond_5
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    invoke-interface {v0, p1}, LX/0gC;->a(LX/0gf;)Z

    .line 110119
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 110120
    return-void
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 110121
    iget-object v0, p0, LX/0g2;->R:LX/1EU;

    invoke-virtual {v0}, LX/1EU;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0g2;->S:LX/1NJ;

    invoke-virtual {v0}, LX/1NJ;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110122
    const/4 v0, 0x0

    .line 110123
    :goto_0
    move v0, v0

    .line 110124
    if-eqz v0, :cond_0

    .line 110125
    iget-object v0, p0, LX/0g2;->S:LX/1NJ;

    sget-object v1, LX/1lq;->ShowNSBPLoadingIndicator:LX/1lq;

    invoke-virtual {v0, v1}, LX/1NJ;->a(LX/1lq;)V

    .line 110126
    :cond_0
    return-void

    :cond_1
    invoke-static {p0}, LX/0g2;->ak(LX/0g2;)Z

    move-result v0

    goto :goto_0
.end method

.method public final m()V
    .locals 6

    .prologue
    .line 110127
    iget-object v0, p0, LX/0g2;->an:LX/1Iv;

    invoke-virtual {v0}, LX/1Iv;->j()LX/0Px;

    move-result-object v0

    .line 110128
    invoke-direct {p0}, LX/0g2;->ai()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 110129
    iget-object v1, p0, LX/0g2;->S:LX/1NJ;

    .line 110130
    iget-object v2, v1, LX/1NJ;->j:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 110131
    :cond_0
    invoke-static {p0}, LX/0g2;->ah(LX/0g2;)Z

    .line 110132
    :cond_1
    :goto_0
    return-void

    .line 110133
    :cond_2
    invoke-static {p0}, LX/0g2;->ak(LX/0g2;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110134
    iget-object v0, p0, LX/0g2;->S:LX/1NJ;

    sget-object v1, LX/1lq;->HideNSBPIfNotFullyLoaded:LX/1lq;

    invoke-virtual {v0, v1}, LX/1NJ;->a(LX/1lq;)V

    goto :goto_0

    .line 110135
    :cond_3
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 110136
    iget-object v5, v1, LX/1NJ;->j:Ljava/util/Set;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 110137
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1
.end method

.method public final o()V
    .locals 0

    .prologue
    .line 110138
    invoke-static {p0}, LX/0g2;->Q(LX/0g2;)V

    .line 110139
    return-void
.end method

.method public final s()V
    .locals 3

    .prologue
    .line 110140
    iget-object v0, p0, LX/0g2;->af:LX/0g5;

    invoke-virtual {v0}, LX/0g7;->s()I

    move-result v0

    if-nez v0, :cond_0

    .line 110141
    :goto_0
    return-void

    .line 110142
    :cond_0
    iget-object v0, p0, LX/0g2;->af:LX/0g5;

    invoke-virtual {v0}, LX/0g7;->s()I

    move-result v1

    .line 110143
    iget-object v0, p0, LX/0g2;->af:LX/0g5;

    invoke-virtual {v0}, LX/0g7;->r()I

    move-result v0

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    .line 110144
    :goto_1
    sget-object v2, LX/1lo;->ALWAYS:LX/1lo;

    invoke-static {p0, v0, v1, v2}, LX/0g2;->a(LX/0g2;ZILX/1lo;)V

    goto :goto_0

    .line 110145
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final t()LX/1J6;
    .locals 1

    .prologue
    .line 110146
    iget-object v0, p0, LX/0g2;->i:LX/1J6;

    return-object v0
.end method

.method public final u()V
    .locals 2

    .prologue
    .line 110147
    new-instance v0, LX/1NK;

    invoke-direct {v0, p0}, LX/1NK;-><init>(LX/0g2;)V

    iput-object v0, p0, LX/0g2;->aj:LX/1NK;

    .line 110148
    iget-object v0, p0, LX/0g2;->X:LX/0bH;

    iget-object v1, p0, LX/0g2;->aj:LX/1NK;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 110149
    return-void
.end method

.method public final y()V
    .locals 4

    .prologue
    .line 110150
    const-string v0, "feed_new_story_pill_tapped"

    invoke-static {p0, v0}, LX/0g2;->b(LX/0g2;Ljava/lang/String;)V

    .line 110151
    sget-object v0, LX/0rj;->NEW_STORY_PILL_TAPPED:LX/0rj;

    invoke-static {p0, v0}, LX/0g2;->a(LX/0g2;LX/0rj;)V

    .line 110152
    invoke-static {p0}, LX/0g2;->ah(LX/0g2;)Z

    .line 110153
    invoke-virtual {p0}, LX/0g2;->C()V

    .line 110154
    iget-object v0, p0, LX/0g2;->y:LX/0iS;

    .line 110155
    iget-object v1, v0, LX/0iS;->a:LX/0Xl;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.facebook.feed.util.NEW_STORY_BUTTON_PRESSED"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 110156
    iget-object v0, p0, LX/0g2;->af:LX/0g5;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0g7;->g(I)V

    .line 110157
    iget-object v0, p0, LX/0g2;->V:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-interface {v0}, LX/0fv;->f()V

    .line 110158
    return-void
.end method
