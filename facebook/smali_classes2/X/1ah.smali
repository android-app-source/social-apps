.class public LX/1ah;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;
.implements LX/1ai;
.implements LX/1aj;
.implements LX/1ak;


# static fields
.field private static final d:Landroid/graphics/Matrix;


# instance fields
.field public a:Landroid/graphics/drawable/Drawable;

.field public b_:LX/1ak;

.field public final c:LX/1al;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 278266
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, LX/1ah;->d:Landroid/graphics/Matrix;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 278267
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 278268
    new-instance v0, LX/1al;

    invoke-direct {v0}, LX/1al;-><init>()V

    iput-object v0, p0, LX/1ah;->c:LX/1al;

    .line 278269
    iput-object p1, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    .line 278270
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p0, p0}, LX/1am;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable$Callback;LX/1ak;)V

    .line 278271
    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 278259
    invoke-virtual {p0}, LX/1ah;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 278272
    invoke-virtual {p0, p1}, LX/1ah;->b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1ak;)V
    .locals 0

    .prologue
    .line 278273
    iput-object p1, p0, LX/1ah;->b_:LX/1ak;

    .line 278274
    return-void
.end method

.method public a(Landroid/graphics/Matrix;)V
    .locals 0

    .prologue
    .line 278275
    invoke-virtual {p0, p1}, LX/1ah;->b(Landroid/graphics/Matrix;)V

    .line 278276
    return-void
.end method

.method public final a(Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 278277
    iget-object v0, p0, LX/1ah;->b_:LX/1ak;

    if-eqz v0, :cond_0

    .line 278278
    iget-object v0, p0, LX/1ah;->b_:LX/1ak;

    invoke-interface {v0, p1}, LX/1ak;->a(Landroid/graphics/RectF;)V

    .line 278279
    :goto_0
    return-void

    .line 278280
    :cond_0
    invoke-virtual {p0}, LX/1ah;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 278281
    const/4 v1, 0x0

    .line 278282
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    .line 278283
    invoke-static {v0, v1, v1}, LX/1am;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable$Callback;LX/1ak;)V

    .line 278284
    invoke-static {p1, v1, v1}, LX/1am;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable$Callback;LX/1ak;)V

    .line 278285
    iget-object v1, p0, LX/1ah;->c:LX/1al;

    invoke-static {p1, v1}, LX/1am;->a(Landroid/graphics/drawable/Drawable;LX/1al;)V

    .line 278286
    invoke-static {p1, p0}, LX/1am;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 278287
    invoke-static {p1, p0, p0}, LX/1am;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable$Callback;LX/1ak;)V

    .line 278288
    iput-object p1, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    .line 278289
    move-object v0, v0

    .line 278290
    invoke-virtual {p0}, LX/1ah;->invalidateSelf()V

    .line 278291
    return-object v0
.end method

.method public final b(Landroid/graphics/Matrix;)V
    .locals 1

    .prologue
    .line 278292
    iget-object v0, p0, LX/1ah;->b_:LX/1ak;

    if-eqz v0, :cond_0

    .line 278293
    iget-object v0, p0, LX/1ah;->b_:LX/1ak;

    invoke-interface {v0, p1}, LX/1ak;->a(Landroid/graphics/Matrix;)V

    .line 278294
    :goto_0
    return-void

    .line 278295
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Matrix;->reset()V

    goto :goto_0
.end method

.method public final b(Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 278296
    sget-object v0, LX/1ah;->d:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, LX/1ah;->b(Landroid/graphics/Matrix;)V

    .line 278297
    invoke-virtual {p0}, LX/1ah;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 278298
    sget-object v0, LX/1ah;->d:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 278299
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 278300
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 278301
    return-void
.end method

.method public final getCurrent()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 278302
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 278303
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 278304
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 278264
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    return v0
.end method

.method public final getPadding(Landroid/graphics/Rect;)Z
    .locals 1

    .prologue
    .line 278265
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 278233
    invoke-virtual {p0}, LX/1ah;->invalidateSelf()V

    .line 278234
    return-void
.end method

.method public isStateful()Z
    .locals 1

    .prologue
    .line 278235
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    return v0
.end method

.method public final mutate()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 278236
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 278237
    return-object p0
.end method

.method public onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 278238
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 278239
    return-void
.end method

.method public onLevelChange(I)Z
    .locals 1

    .prologue
    .line 278240
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    move-result v0

    return v0
.end method

.method public final onStateChange([I)Z
    .locals 1

    .prologue
    .line 278241
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result v0

    return v0
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 278242
    invoke-virtual {p0, p2, p3, p4}, LX/1ah;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 278243
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 278244
    iget-object v0, p0, LX/1ah;->c:LX/1al;

    .line 278245
    iput p1, v0, LX/1al;->a:I

    .line 278246
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 278247
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 278248
    iget-object v0, p0, LX/1ah;->c:LX/1al;

    invoke-virtual {v0, p1}, LX/1al;->a(Landroid/graphics/ColorFilter;)V

    .line 278249
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 278250
    return-void
.end method

.method public final setDither(Z)V
    .locals 1

    .prologue
    .line 278251
    iget-object v0, p0, LX/1ah;->c:LX/1al;

    invoke-virtual {v0, p1}, LX/1al;->a(Z)V

    .line 278252
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 278253
    return-void
.end method

.method public final setFilterBitmap(Z)V
    .locals 1

    .prologue
    .line 278254
    iget-object v0, p0, LX/1ah;->c:LX/1al;

    invoke-virtual {v0, p1}, LX/1al;->b(Z)V

    .line 278255
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 278256
    return-void
.end method

.method public final setHotspot(FF)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 278257
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setHotspot(FF)V

    .line 278258
    return-void
.end method

.method public setVisible(ZZ)Z
    .locals 1

    .prologue
    .line 278260
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 278261
    iget-object v0, p0, LX/1ah;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v0

    return v0
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 278262
    invoke-virtual {p0, p2}, LX/1ah;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 278263
    return-void
.end method
