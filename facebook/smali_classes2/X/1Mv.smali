.class public LX/1Mv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final b:LX/0Ve;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ve",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;",
            "LX/0Ve",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 236396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236397
    iput-object p1, p0, LX/1Mv;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 236398
    iput-object p2, p0, LX/1Mv;->b:LX/0Ve;

    .line 236399
    return-void
.end method

.method public static a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;",
            "LX/0Ve",
            "<TT;>;)",
            "LX/1Mv",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 236400
    new-instance v0, LX/1Mv;

    invoke-direct {v0, p0, p1}, LX/1Mv;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    return-object v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    .prologue
    .line 236401
    iget-object v0, p0, LX/1Mv;->b:LX/0Ve;

    invoke-interface {v0}, LX/0Ve;->dispose()V

    .line 236402
    iget-object v0, p0, LX/1Mv;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, p1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 236403
    return-void
.end method
