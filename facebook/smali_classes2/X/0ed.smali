.class public LX/0ed;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/0ee;

.field public final c:LX/0WV;

.field private final d:Ljava/util/Locale;

.field public final e:LX/0eh;

.field public final f:Ljava/lang/String;

.field public g:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0ee;Landroid/content/Context;Ljava/util/Locale;LX/0WV;LX/0eh;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 92212
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, LX/0ed;-><init>(LX/0ee;Landroid/content/Context;Ljava/util/Locale;LX/0WV;LX/0eh;Ljava/lang/String;Ljava/lang/String;)V

    .line 92213
    return-void
.end method

.method private constructor <init>(LX/0ee;Landroid/content/Context;Ljava/util/Locale;LX/0WV;LX/0eh;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p7    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 92214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92215
    iput-object p2, p0, LX/0ed;->a:Landroid/content/Context;

    .line 92216
    iput-object p1, p0, LX/0ed;->b:LX/0ee;

    .line 92217
    iput-object p3, p0, LX/0ed;->d:Ljava/util/Locale;

    .line 92218
    iput-object p4, p0, LX/0ed;->c:LX/0WV;

    .line 92219
    iput-object p5, p0, LX/0ed;->e:LX/0eh;

    .line 92220
    iput-object p6, p0, LX/0ed;->f:Ljava/lang/String;

    .line 92221
    iput-object p7, p0, LX/0ed;->g:Ljava/lang/String;

    .line 92222
    return-void
.end method


# virtual methods
.method public final c()I
    .locals 1

    .prologue
    .line 92223
    iget-object v0, p0, LX/0ed;->c:LX/0WV;

    invoke-virtual {v0}, LX/0WV;->b()I

    move-result v0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92224
    iget-object v0, p0, LX/0ed;->d:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 92225
    if-ne p0, p1, :cond_1

    .line 92226
    :cond_0
    :goto_0
    return v0

    .line 92227
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 92228
    goto :goto_0

    .line 92229
    :cond_3
    check-cast p1, LX/0ed;

    .line 92230
    iget-object v2, p0, LX/0ed;->a:Landroid/content/Context;

    iget-object v3, p1, LX/0ed;->a:Landroid/content/Context;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/0ed;->b:LX/0ee;

    iget-object v3, p1, LX/0ed;->b:LX/0ee;

    if-ne v2, v3, :cond_4

    iget-object v2, p0, LX/0ed;->c:LX/0WV;

    iget-object v3, p1, LX/0ed;->c:LX/0WV;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/0ed;->d:Ljava/util/Locale;

    iget-object v3, p1, LX/0ed;->d:Ljava/util/Locale;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/0ed;->e:LX/0eh;

    iget-object v3, p1, LX/0ed;->e:LX/0eh;

    if-ne v2, v3, :cond_4

    iget-object v2, p0, LX/0ed;->f:Ljava/lang/String;

    iget-object v3, p1, LX/0ed;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/0ed;->g:Ljava/lang/String;

    iget-object v3, p1, LX/0ed;->g:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final g()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92231
    iget-object v0, p0, LX/0ed;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 92232
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/0ed;->a:Landroid/content/Context;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/0ed;->b:LX/0ee;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/0ed;->c:LX/0WV;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/0ed;->d:Ljava/util/Locale;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LX/0ed;->e:LX/0eh;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LX/0ed;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, LX/0ed;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Lcom/facebook/http/interfaces/RequestPriority;
    .locals 2

    .prologue
    .line 92233
    sget-object v0, LX/31q;->a:[I

    iget-object v1, p0, LX/0ed;->b:LX/0ee;

    invoke-virtual {v1}, LX/0ee;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 92234
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    :goto_0
    return-object v0

    .line 92235
    :pswitch_0
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
