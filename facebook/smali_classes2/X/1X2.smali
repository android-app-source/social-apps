.class public LX/1X2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field public static final b:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field public static final c:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field public static final d:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field public static final e:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field private static l:LX/0Xm;


# instance fields
.field private final f:LX/1xT;

.field private final g:LX/1xU;

.field private final h:LX/1xV;

.field private final i:LX/1xW;

.field private final j:LX/1xS;

.field private final k:LX/0dn;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 270338
    const v0, 0x7f0b1ae6

    sput v0, LX/1X2;->a:I

    .line 270339
    sget v0, LX/1X3;->a:I

    sput v0, LX/1X2;->b:I

    .line 270340
    const v0, 0x7f0b00e8

    sput v0, LX/1X2;->c:I

    .line 270341
    const v0, 0x7f0b00dd

    sput v0, LX/1X2;->d:I

    .line 270342
    const v0, 0x7f0b00d9

    sput v0, LX/1X2;->e:I

    return-void
.end method

.method public constructor <init>(LX/0dn;LX/1xS;LX/1xT;LX/1xU;LX/1xV;LX/1xW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 270366
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 270367
    iput-object p1, p0, LX/1X2;->k:LX/0dn;

    .line 270368
    iput-object p2, p0, LX/1X2;->j:LX/1xS;

    .line 270369
    iput-object p3, p0, LX/1X2;->f:LX/1xT;

    .line 270370
    iput-object p4, p0, LX/1X2;->g:LX/1xU;

    .line 270371
    iput-object p5, p0, LX/1X2;->h:LX/1xV;

    .line 270372
    iput-object p6, p0, LX/1X2;->i:LX/1xW;

    .line 270373
    return-void
.end method

.method public static a(LX/0QB;)LX/1X2;
    .locals 10

    .prologue
    .line 270374
    const-class v1, LX/1X2;

    monitor-enter v1

    .line 270375
    :try_start_0
    sget-object v0, LX/1X2;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 270376
    sput-object v2, LX/1X2;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 270377
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270378
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 270379
    new-instance v3, LX/1X2;

    invoke-static {v0}, LX/0dn;->a(LX/0QB;)LX/0dn;

    move-result-object v4

    check-cast v4, LX/0dn;

    invoke-static {v0}, LX/1xS;->a(LX/0QB;)LX/1xS;

    move-result-object v5

    check-cast v5, LX/1xS;

    invoke-static {v0}, LX/1xT;->a(LX/0QB;)LX/1xT;

    move-result-object v6

    check-cast v6, LX/1xT;

    invoke-static {v0}, LX/1xU;->a(LX/0QB;)LX/1xU;

    move-result-object v7

    check-cast v7, LX/1xU;

    invoke-static {v0}, LX/1xV;->a(LX/0QB;)LX/1xV;

    move-result-object v8

    check-cast v8, LX/1xV;

    invoke-static {v0}, LX/1xW;->a(LX/0QB;)LX/1xW;

    move-result-object v9

    check-cast v9, LX/1xW;

    invoke-direct/range {v3 .. v9}, LX/1X2;-><init>(LX/0dn;LX/1xS;LX/1xT;LX/1xU;LX/1xV;LX/1xW;)V

    .line 270380
    move-object v0, v3

    .line 270381
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 270382
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1X2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270383
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 270384
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;IIZZZZZZZIIIII)LX/1Dg;
    .locals 9
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pb;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p11    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p12    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p13    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p14    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p15    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p16    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p17    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;IIZZZZZZZIIIII)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 270343
    if-eqz p17, :cond_0

    .line 270344
    invoke-virtual {p1}, LX/1De;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, LX/03r;->HeaderActorComponent:[I

    move/from16 v0, p17

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 270345
    const/16 v1, 0x0

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 270346
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    move v2, v1

    .line 270347
    :goto_0
    iget-object v1, p0, LX/1X2;->k:LX/0dn;

    invoke-virtual {v1}, LX/0dn;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/1X2;->j:LX/1xS;

    invoke-virtual {v1, p1}, LX/1xS;->c(LX/1De;)LX/BsS;

    move-result-object v3

    move-object v1, p3

    check-cast v1, LX/1Po;

    invoke-virtual {v3, v1}, LX/BsS;->a(LX/1Po;)LX/BsS;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/BsS;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/BsS;

    move-result-object v1

    .line 270348
    :goto_1
    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, p4}, LX/1Di;->i(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1, p4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    .line 270349
    if-nez v2, :cond_2

    .line 270350
    const/4 v2, 0x4

    move/from16 v0, p14

    invoke-interface {v1, v2, v0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x1

    move/from16 v0, p15

    invoke-interface {v1, v2, v0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x5

    const v3, 0x7f0b00df

    invoke-interface {v1, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x3

    const v3, 0x7f0b00d4

    invoke-interface {v1, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    .line 270351
    :goto_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    .line 270352
    if-eqz p7, :cond_3

    .line 270353
    const/4 v1, 0x4

    move/from16 v0, p14

    invoke-interface {v2, v1, v0}, LX/1Dh;->u(II)LX/1Dh;

    .line 270354
    :goto_3
    iget-object v1, p0, LX/1X2;->g:LX/1xU;

    invoke-virtual {v1, p1}, LX/1xU;->c(LX/1De;)LX/1xo;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1xo;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1xo;

    move-result-object v3

    move-object v1, p3

    check-cast v1, LX/1Pn;

    invoke-virtual {v3, v1}, LX/1xo;->a(LX/1Pn;)LX/1xo;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/1xo;->i(I)LX/1xo;

    move-result-object v1

    invoke-virtual {v1, p6}, LX/1xo;->a(Z)LX/1xo;

    move-result-object v1

    move/from16 v0, p13

    invoke-virtual {v1, v0}, LX/1xo;->j(I)LX/1xo;

    move-result-object v1

    move/from16 v0, p17

    invoke-virtual {v1, v0}, LX/1xo;->h(I)LX/1xo;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v3, 0x1

    move/from16 v0, p16

    invoke-interface {v1, v3, v0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    const/4 v3, 0x5

    const v4, 0x7f0b00e5

    invoke-interface {v1, v3, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v1, v3}, LX/1Di;->a(F)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    iget-object v2, p0, LX/1X2;->i:LX/1xW;

    invoke-virtual {v2, p1}, LX/1xW;->c(LX/1De;)LX/1yY;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v3, 0x1

    const v4, 0x7f0b00ed

    invoke-interface {v2, v3, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x5

    const v4, 0x7f0b00ea

    invoke-interface {v2, v3, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x3

    const v4, 0x7f0b00ee

    invoke-interface {v2, v3, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    iget-object v2, p0, LX/1X2;->h:LX/1xV;

    invoke-virtual {v2, p1}, LX/1xV;->c(LX/1De;)LX/1yc;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1yc;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1yc;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/1yc;->a(LX/1Pb;)LX/1yc;

    move-result-object v2

    move/from16 v0, p17

    invoke-virtual {v2, v0}, LX/1yc;->h(I)LX/1yc;

    move-result-object v2

    move/from16 v0, p8

    invoke-virtual {v2, v0}, LX/1yc;->a(Z)LX/1yc;

    move-result-object v2

    move/from16 v0, p9

    invoke-virtual {v2, v0}, LX/1yc;->e(Z)LX/1yc;

    move-result-object v2

    move/from16 v0, p10

    invoke-virtual {v2, v0}, LX/1yc;->c(Z)LX/1yc;

    move-result-object v2

    move/from16 v0, p11

    invoke-virtual {v2, v0}, LX/1yc;->d(Z)LX/1yc;

    move-result-object v2

    move/from16 v0, p12

    invoke-virtual {v2, v0}, LX/1yc;->b(Z)LX/1yc;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    return-object v1

    .line 270355
    :cond_0
    const/4 v1, 0x0

    move v2, v1

    goto/16 :goto_0

    .line 270356
    :cond_1
    iget-object v1, p0, LX/1X2;->f:LX/1xT;

    invoke-virtual {v1, p1}, LX/1xT;->c(LX/1De;)LX/1xY;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1xY;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1xY;

    move-result-object v3

    move-object v1, p3

    check-cast v1, LX/1Po;

    invoke-virtual {v3, v1}, LX/1xY;->a(LX/1Po;)LX/1xY;

    move-result-object v1

    goto/16 :goto_1

    .line 270357
    :cond_2
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 270358
    const/high16 v3, 0x40400000    # 3.0f

    mul-float/2addr v3, v2

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 270359
    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v4, v2

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 270360
    const/high16 v5, 0x40800000    # 4.0f

    mul-float/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 270361
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 270362
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    move/from16 v0, p14

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 270363
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    move/from16 v0, p15

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 270364
    mul-int/lit8 v8, v3, 0x2

    add-int/2addr v8, v5

    invoke-interface {v1, v8}, LX/1Di;->g(I)LX/1Di;

    move-result-object v1

    add-int/2addr v5, v4

    add-int/2addr v5, v2

    invoke-interface {v1, v5}, LX/1Di;->o(I)LX/1Di;

    move-result-object v1

    const v5, 0x7f021514

    invoke-interface {v1, v5}, LX/1Di;->x(I)LX/1Di;

    move-result-object v1

    const/4 v5, 0x4

    sub-int/2addr v6, v3

    invoke-interface {v1, v5, v6}, LX/1Di;->a(II)LX/1Di;

    move-result-object v1

    const/4 v5, 0x1

    sub-int v6, v7, v4

    invoke-interface {v1, v5, v6}, LX/1Di;->a(II)LX/1Di;

    move-result-object v1

    const/4 v5, 0x5

    const v6, 0x7f0b00e0

    invoke-interface {v1, v5, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    const/4 v5, 0x3

    const v6, 0x7f0b00dc

    invoke-interface {v1, v5, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    const/4 v5, 0x6

    invoke-interface {v1, v5, v3}, LX/1Di;->e(II)LX/1Di;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3, v4}, LX/1Di;->e(II)LX/1Di;

    move-result-object v1

    const/4 v3, 0x3

    invoke-interface {v1, v3, v2}, LX/1Di;->e(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto/16 :goto_2

    .line 270365
    :cond_3
    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    goto/16 :goto_3
.end method
