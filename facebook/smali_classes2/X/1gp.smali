.class public LX/1gp;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:Lcom/facebook/api/feedtype/FeedType;

.field private final b:LX/1gr;

.field private final c:LX/1gq;

.field private final d:LX/0Zr;

.field public e:Landroid/os/HandlerThread;

.field public f:Landroid/os/HandlerThread;

.field public g:LX/1gu;

.field public h:LX/1gv;

.field private final i:LX/1gi;

.field private final j:LX/0jY;


# direct methods
.method public constructor <init>(Lcom/facebook/api/feedtype/FeedType;LX/1gi;LX/0jY;LX/1gq;LX/1gr;LX/0Zr;)V
    .locals 0
    .param p1    # Lcom/facebook/api/feedtype/FeedType;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1gi;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0jY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 294973
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 294974
    iput-object p1, p0, LX/1gp;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 294975
    iput-object p2, p0, LX/1gp;->i:LX/1gi;

    .line 294976
    iput-object p5, p0, LX/1gp;->b:LX/1gr;

    .line 294977
    iput-object p4, p0, LX/1gp;->c:LX/1gq;

    .line 294978
    iput-object p6, p0, LX/1gp;->d:LX/0Zr;

    .line 294979
    iput-object p3, p0, LX/1gp;->j:LX/0jY;

    .line 294980
    return-void
.end method

.method public static b(LX/1gp;)V
    .locals 10

    .prologue
    .line 294960
    const-string v0, "FreshFeedFetcher.ensureDBHandler"

    const v1, 0x1bb2831c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 294961
    :try_start_0
    iget-object v0, p0, LX/1gp;->e:Landroid/os/HandlerThread;

    if-nez v0, :cond_0

    .line 294962
    iget-object v0, p0, LX/1gp;->d:LX/0Zr;

    const-string v1, "freshfeed_db_fetcher"

    sget-object v2, LX/0TP;->URGENT:LX/0TP;

    invoke-virtual {v0, v1, v2}, LX/0Zr;->a(Ljava/lang/String;LX/0TP;)Landroid/os/HandlerThread;

    move-result-object v0

    iput-object v0, p0, LX/1gp;->e:Landroid/os/HandlerThread;

    .line 294963
    iget-object v0, p0, LX/1gp;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 294964
    iget-object v0, p0, LX/1gp;->b:LX/1gr;

    iget-object v1, p0, LX/1gp;->e:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v2, p0, LX/1gp;->a:Lcom/facebook/api/feedtype/FeedType;

    iget-object v3, p0, LX/1gp;->i:LX/1gi;

    iget-object v4, p0, LX/1gp;->j:LX/0jY;

    .line 294965
    new-instance v5, LX/1gu;

    invoke-direct {v5, v1, v2, v3, v4}, LX/1gu;-><init>(Landroid/os/Looper;Lcom/facebook/api/feedtype/FeedType;LX/1gi;LX/0jY;)V

    .line 294966
    const/16 v6, 0xe8

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x685

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x643

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1032

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    .line 294967
    iput-object v6, v5, LX/1gu;->e:LX/0Ot;

    iput-object v7, v5, LX/1gu;->f:LX/0Ot;

    iput-object v8, v5, LX/1gu;->g:LX/0Ot;

    iput-object v9, v5, LX/1gu;->h:LX/0Ot;

    .line 294968
    move-object v0, v5

    .line 294969
    iput-object v0, p0, LX/1gp;->g:LX/1gu;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294970
    :cond_0
    const v0, -0x49b0fd04

    invoke-static {v0}, LX/02m;->a(I)V

    .line 294971
    return-void

    .line 294972
    :catchall_0
    move-exception v0

    const v1, -0x486572a9

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static c(LX/1gp;)V
    .locals 4

    .prologue
    .line 294952
    const-string v0, "FreshFeedFetcher.ensureNetworkHandler"

    const v1, 0x22db2a3

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 294953
    :try_start_0
    iget-object v0, p0, LX/1gp;->f:Landroid/os/HandlerThread;

    if-nez v0, :cond_0

    .line 294954
    iget-object v0, p0, LX/1gp;->d:LX/0Zr;

    const-string v1, "freshfeed_network_fetcher"

    sget-object v2, LX/0TP;->URGENT:LX/0TP;

    invoke-virtual {v0, v1, v2}, LX/0Zr;->a(Ljava/lang/String;LX/0TP;)Landroid/os/HandlerThread;

    move-result-object v0

    iput-object v0, p0, LX/1gp;->f:Landroid/os/HandlerThread;

    .line 294955
    iget-object v0, p0, LX/1gp;->f:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 294956
    iget-object v0, p0, LX/1gp;->c:LX/1gq;

    iget-object v1, p0, LX/1gp;->f:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v2, p0, LX/1gp;->i:LX/1gi;

    iget-object v3, p0, LX/1gp;->j:LX/0jY;

    invoke-virtual {v0, v1, v2, v3}, LX/1gq;->a(Landroid/os/Looper;LX/1gi;LX/0jY;)LX/1gv;

    move-result-object v0

    iput-object v0, p0, LX/1gp;->h:LX/1gv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294957
    :cond_0
    const v0, -0x171fcae1

    invoke-static {v0}, LX/02m;->a(I)V

    .line 294958
    return-void

    .line 294959
    :catchall_0
    move-exception v0

    const v1, 0x127f234b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 294945
    const-string v0, "FreshFeedFetcher.loadEdgesFromDB"

    const v1, 0x1058e9bd

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 294946
    :try_start_0
    invoke-static {p0}, LX/1gp;->b(LX/1gp;)V

    .line 294947
    iget-object v0, p0, LX/1gp;->g:LX/1gu;

    .line 294948
    const/4 v1, 0x1

    const/4 p0, 0x0

    invoke-virtual {v0, v1, p1, p0}, LX/1gu;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1gu;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294949
    const v0, 0x26a3135b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 294950
    return-void

    .line 294951
    :catchall_0
    move-exception v0

    const v1, -0x26b8f3fb

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(LX/0Px;Lcom/facebook/api/feedtype/FeedType;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;",
            "Lcom/facebook/api/feedtype/FeedType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 294924
    const-string v0, "FreshFeedFetcher.handlePushedStories"

    const v1, 0x7385fa7f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 294925
    :try_start_0
    invoke-static {p0}, LX/1gp;->c(LX/1gp;)V

    .line 294926
    iget-object v0, p0, LX/1gp;->h:LX/1gv;

    .line 294927
    const/4 v1, 0x4

    new-instance p0, LX/3rL;

    invoke-direct {p0, p1, p2}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1, p0}, LX/1gv;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1gv;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294928
    const v0, -0x2dc9467f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 294929
    return-void

    .line 294930
    :catchall_0
    move-exception v0

    const v1, 0x1ac3af3a

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(LX/1J0;)V
    .locals 2

    .prologue
    .line 294938
    const-string v0, "FreshFeedFetcher.loadNewDataFromNetwork"

    const v1, 0x36ed1a6f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 294939
    :try_start_0
    invoke-static {p0}, LX/1gp;->c(LX/1gp;)V

    .line 294940
    iget-object v0, p0, LX/1gp;->h:LX/1gv;

    .line 294941
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, LX/1gv;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1gv;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294942
    const v0, 0xdfc2689

    invoke-static {v0}, LX/02m;->a(I)V

    .line 294943
    return-void

    .line 294944
    :catchall_0
    move-exception v0

    const v1, 0x33bd0a2b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b(LX/1J0;)V
    .locals 2

    .prologue
    .line 294931
    const-string v0, "FreshFeedFetcher.loadMoreDataFromNetwork"

    const v1, 0x4eba2d8a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 294932
    :try_start_0
    invoke-static {p0}, LX/1gp;->c(LX/1gp;)V

    .line 294933
    iget-object v0, p0, LX/1gp;->h:LX/1gv;

    .line 294934
    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, LX/1gv;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1gv;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294935
    const v0, -0x7c27a209

    invoke-static {v0}, LX/02m;->a(I)V

    .line 294936
    return-void

    .line 294937
    :catchall_0
    move-exception v0

    const v1, 0x7125af11

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
