.class public final LX/1eY;
.super LX/1eP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1eP",
        "<",
        "LX/1FL;",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1cP;

.field public final b:LX/1cW;

.field public c:Z

.field public final d:LX/1eV;


# direct methods
.method public constructor <init>(LX/1cP;LX/1cd;LX/1cW;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 288634
    iput-object p1, p0, LX/1eY;->a:LX/1cP;

    .line 288635
    invoke-direct {p0, p2}, LX/1eP;-><init>(LX/1cd;)V

    .line 288636
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1eY;->c:Z

    .line 288637
    iput-object p3, p0, LX/1eY;->b:LX/1cW;

    .line 288638
    new-instance v0, LX/1eZ;

    invoke-direct {v0, p0, p1}, LX/1eZ;-><init>(LX/1eY;LX/1cP;)V

    .line 288639
    new-instance v1, LX/1eV;

    iget-object v2, p1, LX/1cP;->a:Ljava/util/concurrent/Executor;

    const/16 v3, 0x64

    invoke-direct {v1, v2, v0, v3}, LX/1eV;-><init>(Ljava/util/concurrent/Executor;LX/1eU;I)V

    iput-object v1, p0, LX/1eY;->d:LX/1eV;

    .line 288640
    iget-object v0, p0, LX/1eY;->b:LX/1cW;

    new-instance v1, LX/1ea;

    invoke-direct {v1, p0, p1, p2}, LX/1ea;-><init>(LX/1eY;LX/1cP;LX/1cd;)V

    invoke-virtual {v0, v1}, LX/1cW;->a(LX/1cg;)V

    .line 288641
    return-void
.end method

.method private static a(LX/1eY;LX/1FL;LX/1bf;III)Ljava/util/Map;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FL;",
            "LX/1bf;",
            "III)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 288624
    iget-object v0, p0, LX/1eY;->b:LX/1cW;

    invoke-virtual {v0}, LX/1cW;->c()LX/1BV;

    move-result-object v0

    iget-object v1, p0, LX/1eY;->b:LX/1cW;

    invoke-virtual {v1}, LX/1cW;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1BV;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 288625
    const/4 v0, 0x0

    .line 288626
    :goto_0
    return-object v0

    .line 288627
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, LX/1FL;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, LX/1FL;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 288628
    invoke-virtual {p2}, LX/1bf;->f()LX/1o9;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 288629
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, LX/1bf;->f()LX/1o9;

    move-result-object v2

    iget v2, v2, LX/1o9;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, LX/1bf;->f()LX/1o9;

    move-result-object v2

    iget v2, v2, LX/1o9;->b:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 288630
    :goto_1
    if-lez p3, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/8"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 288631
    :goto_2
    const-string v0, "Original size"

    const-string v2, "Requested size"

    const-string v4, "Fraction"

    const-string v6, "queueTime"

    iget-object v7, p0, LX/1eY;->d:LX/1eV;

    invoke-virtual {v7}, LX/1eV;->c()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    const-string v8, "downsampleEnumerator"

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "softwareEnumerator"

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static/range {v0 .. v11}, LX/2oC;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    .line 288632
    :cond_1
    const-string v3, "Unspecified"

    goto :goto_1

    .line 288633
    :cond_2
    const-string v5, ""

    goto :goto_2
.end method

.method public static b(LX/1eY;LX/1FL;Z)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 288543
    iget-object v0, p0, LX/1eY;->b:LX/1cW;

    .line 288544
    iget-object v1, v0, LX/1cW;->c:LX/1BV;

    move-object v0, v1

    .line 288545
    iget-object v1, p0, LX/1eY;->b:LX/1cW;

    .line 288546
    iget-object v2, v1, LX/1cW;->b:Ljava/lang/String;

    move-object v1, v2

    .line 288547
    const-string v2, "ResizeAndRotateProducer"

    invoke-interface {v0, v1, v2}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 288548
    iget-object v0, p0, LX/1eY;->b:LX/1cW;

    .line 288549
    iget-object v1, v0, LX/1cW;->a:LX/1bf;

    move-object v2, v1

    .line 288550
    iget-object v0, p0, LX/1eY;->a:LX/1cP;

    iget-object v0, v0, LX/1cP;->b:LX/1Fj;

    invoke-virtual {v0}, LX/1Fj;->a()LX/1gO;

    move-result-object v7

    .line 288551
    :try_start_0
    iget-object v0, p0, LX/1eY;->a:LX/1cP;

    iget-boolean v0, v0, LX/1cP;->c:Z

    invoke-static {v2, p1, v0}, LX/1cP;->d(LX/1bf;LX/1FL;Z)I

    move-result v5

    .line 288552
    invoke-static {v2, p1}, LX/4es;->a(LX/1bf;LX/1FL;)I

    move-result v0

    .line 288553
    const/16 v1, 0x8

    div-int/2addr v1, v0

    move v4, v1

    .line 288554
    iget-object v0, p0, LX/1eY;->a:LX/1cP;

    iget-boolean v0, v0, LX/1cP;->e:Z

    if-eqz v0, :cond_0

    move v3, v4

    :goto_0
    move-object v0, p0

    move-object v1, p1

    .line 288555
    invoke-static/range {v0 .. v5}, LX/1eY;->a(LX/1eY;LX/1FL;LX/1bf;III)Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v4

    .line 288556
    :try_start_1
    invoke-virtual {p1}, LX/1FL;->b()Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v1

    .line 288557
    :try_start_2
    iget-object v0, v2, LX/1bf;->i:LX/1bd;

    move-object v0, v0

    .line 288558
    invoke-static {v0, p1}, LX/1cP;->b(LX/1bd;LX/1FL;)I

    move-result v0

    const/16 v2, 0x55

    invoke-static {v1, v7, v0, v3, v2}, Lcom/facebook/imagepipeline/nativecode/JpegTranscoder;->a(Ljava/io/InputStream;Ljava/io/OutputStream;III)V

    .line 288559
    invoke-virtual {v7}, LX/1gO;->a()LX/1FK;

    move-result-object v0

    invoke-static {v0}, LX/1FJ;->a(Ljava/io/Closeable;)LX/1FJ;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-result-object v2

    .line 288560
    :try_start_3
    new-instance v3, LX/1FL;

    invoke-direct {v3, v2}, LX/1FL;-><init>(LX/1FJ;)V

    .line 288561
    sget-object v0, LX/1ld;->a:LX/1lW;

    .line 288562
    iput-object v0, v3, LX/1FL;->c:LX/1lW;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 288563
    :try_start_4
    invoke-virtual {v3}, LX/1FL;->j()V

    .line 288564
    iget-object v0, p0, LX/1eY;->b:LX/1cW;

    .line 288565
    iget-object v5, v0, LX/1cW;->c:LX/1BV;

    move-object v0, v5

    .line 288566
    iget-object v5, p0, LX/1eY;->b:LX/1cW;

    .line 288567
    iget-object v6, v5, LX/1cW;->b:Ljava/lang/String;

    move-object v5, v6

    .line 288568
    const-string v6, "ResizeAndRotateProducer"

    invoke-interface {v0, v5, v6, v4}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 288569
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 288570
    invoke-virtual {v0, v3, p2}, LX/1cd;->b(Ljava/lang/Object;Z)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 288571
    :try_start_5
    invoke-static {v3}, LX/1FL;->d(LX/1FL;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 288572
    :try_start_6
    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 288573
    invoke-static {v1}, LX/1vz;->a(Ljava/io/InputStream;)V

    .line 288574
    invoke-virtual {v7}, LX/1gO;->close()V

    .line 288575
    :goto_1
    return-void

    :cond_0
    move v3, v5

    .line 288576
    goto :goto_0

    .line 288577
    :catchall_0
    move-exception v0

    :try_start_7
    invoke-static {v3}, LX/1FL;->d(LX/1FL;)V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 288578
    :catchall_1
    move-exception v0

    :try_start_8
    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 288579
    :catch_0
    move-exception v0

    move-object v6, v4

    .line 288580
    :goto_2
    :try_start_9
    iget-object v2, p0, LX/1eY;->b:LX/1cW;

    .line 288581
    iget-object v3, v2, LX/1cW;->c:LX/1BV;

    move-object v2, v3

    .line 288582
    iget-object v3, p0, LX/1eY;->b:LX/1cW;

    .line 288583
    iget-object v4, v3, LX/1cW;->b:Ljava/lang/String;

    move-object v3, v4

    .line 288584
    const-string v4, "ResizeAndRotateProducer"

    invoke-interface {v2, v3, v4, v0, v6}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    .line 288585
    iget-object v2, p0, LX/1eP;->a:LX/1cd;

    move-object v2, v2

    .line 288586
    invoke-virtual {v2, v0}, LX/1cd;->b(Ljava/lang/Throwable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 288587
    invoke-static {v1}, LX/1vz;->a(Ljava/io/InputStream;)V

    .line 288588
    invoke-virtual {v7}, LX/1gO;->close()V

    goto :goto_1

    .line 288589
    :catchall_2
    move-exception v0

    move-object v1, v6

    :goto_3
    invoke-static {v1}, LX/1vz;->a(Ljava/io/InputStream;)V

    .line 288590
    invoke-virtual {v7}, LX/1gO;->close()V

    throw v0

    .line 288591
    :catchall_3
    move-exception v0

    goto :goto_3

    .line 288592
    :catch_1
    move-exception v0

    move-object v1, v6

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v1, v6

    move-object v6, v4

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Z)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 288593
    check-cast p1, LX/1FL;

    .line 288594
    iget-boolean v0, p0, LX/1eY;->c:Z

    if-eqz v0, :cond_1

    .line 288595
    :cond_0
    :goto_0
    return-void

    .line 288596
    :cond_1
    if-nez p1, :cond_2

    .line 288597
    if-eqz p2, :cond_0

    .line 288598
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 288599
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/1cd;->b(Ljava/lang/Object;Z)V

    goto :goto_0

    .line 288600
    :cond_2
    iget-object v0, p0, LX/1eY;->b:LX/1cW;

    .line 288601
    iget-object v1, v0, LX/1cW;->a:LX/1bf;

    move-object v0, v1

    .line 288602
    iget-object v1, p0, LX/1eY;->a:LX/1cP;

    iget-boolean v1, v1, LX/1cP;->c:Z

    .line 288603
    if-eqz p1, :cond_3

    .line 288604
    iget-object v2, p1, LX/1FL;->c:LX/1lW;

    move-object v2, v2

    .line 288605
    sget-object v3, LX/1lW;->a:LX/1lW;

    if-ne v2, v3, :cond_7

    .line 288606
    :cond_3
    sget-object v2, LX/03R;->UNSET:LX/03R;

    .line 288607
    :goto_1
    move-object v0, v2

    .line 288608
    if-nez p2, :cond_4

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-eq v0, v1, :cond_0

    .line 288609
    :cond_4
    sget-object v1, LX/03R;->YES:LX/03R;

    if-eq v0, v1, :cond_5

    .line 288610
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 288611
    invoke-virtual {v0, p1, p2}, LX/1cd;->b(Ljava/lang/Object;Z)V

    goto :goto_0

    .line 288612
    :cond_5
    iget-object v0, p0, LX/1eY;->d:LX/1eV;

    invoke-virtual {v0, p1, p2}, LX/1eV;->a(LX/1FL;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288613
    if-nez p2, :cond_6

    iget-object v0, p0, LX/1eY;->b:LX/1cW;

    invoke-virtual {v0}, LX/1cW;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288614
    :cond_6
    iget-object v0, p0, LX/1eY;->d:LX/1eV;

    invoke-virtual {v0}, LX/1eV;->b()Z

    goto :goto_0

    .line 288615
    :cond_7
    iget-object v2, p1, LX/1FL;->c:LX/1lW;

    move-object v2, v2

    .line 288616
    sget-object v3, LX/1ld;->a:LX/1lW;

    if-eq v2, v3, :cond_8

    .line 288617
    sget-object v2, LX/03R;->NO:LX/03R;

    goto :goto_1

    .line 288618
    :cond_8
    iget-object v2, v0, LX/1bf;->i:LX/1bd;

    move-object v2, v2

    .line 288619
    iget-boolean v3, v2, LX/1bd;->b:Z

    move v3, v3

    .line 288620
    if-nez v3, :cond_b

    invoke-static {v2, p1}, LX/1cP;->b(LX/1bd;LX/1FL;)I

    move-result v3

    if-eqz v3, :cond_b

    const/4 v3, 0x1

    :goto_2
    move v2, v3

    .line 288621
    if-nez v2, :cond_9

    invoke-static {v0, p1, v1}, LX/1cP;->d(LX/1bf;LX/1FL;Z)I

    move-result v2

    .line 288622
    const/16 v3, 0x8

    if-ge v2, v3, :cond_c

    const/4 v3, 0x1

    :goto_3
    move v2, v3

    .line 288623
    if-eqz v2, :cond_a

    :cond_9
    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v2

    goto :goto_1

    :cond_a
    const/4 v2, 0x0

    goto :goto_4

    :cond_b
    const/4 v3, 0x0

    goto :goto_2

    :cond_c
    const/4 v3, 0x0

    goto :goto_3
.end method
