.class public final LX/0UF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0U8;

.field private final b:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(LX/0U8;)V
    .locals 2

    .prologue
    .line 64347
    iput-object p1, p0, LX/0UF;->a:LX/0U8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64348
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/0UF;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 2

    .prologue
    .line 64349
    iget-object v0, p0, LX/0UF;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    iget-object v1, p0, LX/0UF;->a:LX/0U8;

    invoke-virtual {v1}, LX/0U8;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 64350
    iget-object v0, p0, LX/0UF;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v1

    .line 64351
    iget-object v0, p0, LX/0UF;->a:LX/0U8;

    invoke-virtual {v0}, LX/0U8;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    .line 64352
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 64353
    :cond_0
    iget-object v0, p0, LX/0UF;->a:LX/0U8;

    iget-object v2, v0, LX/0U8;->c:[Ljava/lang/Object;

    monitor-enter v2

    .line 64354
    :try_start_0
    iget-object v0, p0, LX/0UF;->a:LX/0U8;

    iget v0, v0, LX/0U8;->e:I

    if-ge v1, v0, :cond_2

    .line 64355
    :goto_0
    iget-object v0, p0, LX/0UF;->a:LX/0U8;

    iget-object v0, v0, LX/0U8;->c:[Ljava/lang/Object;

    aget-object v0, v0, v1

    sget-object v3, LX/0U8;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v3, :cond_1

    .line 64356
    :try_start_1
    iget-object v0, p0, LX/0UF;->a:LX/0U8;

    iget-object v0, v0, LX/0U8;->c:[Ljava/lang/Object;

    const v3, 0x19b43ef2

    invoke-static {v0, v3}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 64357
    :catch_0
    move-exception v0

    .line 64358
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 64359
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 64360
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 64361
    :cond_1
    :try_start_3
    monitor-exit v2

    .line 64362
    :goto_1
    return-object v0

    .line 64363
    :cond_2
    iget-object v0, p0, LX/0UF;->a:LX/0U8;

    iget-object v0, v0, LX/0U8;->c:[Ljava/lang/Object;

    sget-object v3, LX/0U8;->a:Ljava/lang/Object;

    aput-object v3, v0, v1

    .line 64364
    iget-object v0, p0, LX/0UF;->a:LX/0U8;

    .line 64365
    iget v3, v0, LX/0U8;->e:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, LX/0U8;->e:I

    .line 64366
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 64367
    :try_start_4
    iget-object v0, p0, LX/0UF;->a:LX/0U8;

    iget-object v0, v0, LX/0U8;->d:LX/0Sq;

    iget-object v2, p0, LX/0UF;->a:LX/0U8;

    iget-object v2, v2, LX/0U8;->b:LX/0QC;

    invoke-interface {v0, v2, v1}, LX/0Sq;->provide(LX/0QC;I)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v0

    .line 64368
    iget-object v2, p0, LX/0UF;->a:LX/0U8;

    iget-object v2, v2, LX/0U8;->c:[Ljava/lang/Object;

    monitor-enter v2

    .line 64369
    :try_start_5
    iget-object v3, p0, LX/0UF;->a:LX/0U8;

    iget-object v3, v3, LX/0U8;->c:[Ljava/lang/Object;

    aput-object v0, v3, v1

    .line 64370
    iget-object v1, p0, LX/0UF;->a:LX/0U8;

    iget-object v1, v1, LX/0U8;->c:[Ljava/lang/Object;

    const v3, -0x20492c20

    invoke-static {v1, v3}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 64371
    monitor-exit v2

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 64372
    :catchall_2
    move-exception v0

    iget-object v2, p0, LX/0UF;->a:LX/0U8;

    iget-object v2, v2, LX/0U8;->c:[Ljava/lang/Object;

    monitor-enter v2

    .line 64373
    :try_start_6
    iget-object v3, p0, LX/0UF;->a:LX/0U8;

    iget-object v3, v3, LX/0U8;->c:[Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v4, v3, v1

    .line 64374
    iget-object v1, p0, LX/0UF;->a:LX/0U8;

    iget-object v1, v1, LX/0U8;->c:[Ljava/lang/Object;

    const v3, 0x605f062b

    invoke-static {v1, v3}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 64375
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    throw v0

    :catchall_3
    move-exception v0

    :try_start_7
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 64376
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
