.class public LX/0pq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pr;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile j:LX/0pq;


# instance fields
.field public a:LX/0qJ;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0po;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private d:J

.field private final e:LX/0SG;

.field private final f:LX/0V8;

.field private final g:LX/0qK;

.field private final h:LX/0Zb;

.field private final i:LX/0Zm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 146560
    const-class v0, LX/0pq;

    sput-object v0, LX/0pq;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0ps;LX/0V8;LX/0Zb;LX/0Zm;LX/0SG;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 146503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146504
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0pq;->d:J

    .line 146505
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/0pq;->c:Ljava/util/Map;

    .line 146506
    new-instance v0, LX/0qJ;

    invoke-direct {v0, p0}, LX/0qJ;-><init>(LX/0pq;)V

    iput-object v0, p0, LX/0pq;->a:LX/0qJ;

    .line 146507
    iget-object v0, p0, LX/0pq;->a:LX/0qJ;

    .line 146508
    iget-object v1, p1, LX/0ps;->h:Ljava/util/concurrent/ConcurrentMap;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146509
    iput-object p5, p0, LX/0pq;->e:LX/0SG;

    .line 146510
    iput-object p2, p0, LX/0pq;->f:LX/0V8;

    .line 146511
    new-instance v0, LX/0qK;

    iget-object v1, p0, LX/0pq;->e:LX/0SG;

    const/4 v2, 0x2

    const-wide/32 v4, 0x36ee80

    invoke-direct {v0, v1, v2, v4, v5}, LX/0qK;-><init>(LX/0SG;IJ)V

    iput-object v0, p0, LX/0pq;->g:LX/0qK;

    .line 146512
    iput-object p3, p0, LX/0pq;->h:LX/0Zb;

    .line 146513
    iput-object p4, p0, LX/0pq;->i:LX/0Zm;

    .line 146514
    return-void
.end method

.method public static a(LX/0QB;)LX/0pq;
    .locals 9

    .prologue
    .line 146515
    sget-object v0, LX/0pq;->j:LX/0pq;

    if-nez v0, :cond_1

    .line 146516
    const-class v1, LX/0pq;

    monitor-enter v1

    .line 146517
    :try_start_0
    sget-object v0, LX/0pq;->j:LX/0pq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 146518
    if-eqz v2, :cond_0

    .line 146519
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 146520
    new-instance v3, LX/0pq;

    invoke-static {v0}, LX/0ps;->a(LX/0QB;)LX/0ps;

    move-result-object v4

    check-cast v4, LX/0ps;

    invoke-static {v0}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v5

    check-cast v5, LX/0V8;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v7

    check-cast v7, LX/0Zm;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-direct/range {v3 .. v8}, LX/0pq;-><init>(LX/0ps;LX/0V8;LX/0Zb;LX/0Zm;LX/0SG;)V

    .line 146521
    move-object v0, v3

    .line 146522
    sput-object v0, LX/0pq;->j:LX/0pq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146523
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 146524
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 146525
    :cond_1
    sget-object v0, LX/0pq;->j:LX/0pq;

    return-object v0

    .line 146526
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 146527
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(JLjava/lang/String;)V
    .locals 5

    .prologue
    .line 146528
    iget-object v0, p0, LX/0pq;->g:LX/0qK;

    invoke-virtual {v0}, LX/0qK;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0pq;->i:LX/0Zm;

    const-string v1, "disk_cache_trim"

    invoke-virtual {v0, v1}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 146529
    :cond_0
    :goto_0
    return-void

    .line 146530
    :cond_1
    iget-object v0, p0, LX/0pq;->f:LX/0V8;

    sget-object v1, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v0, v1}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v0

    .line 146531
    iget-object v2, p0, LX/0pq;->h:LX/0Zb;

    const-string v3, "disk_cache_trim"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 146532
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 146533
    const-string v3, "before"

    invoke-virtual {v2, v3, p1, p2}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 146534
    const-string v3, "after"

    invoke-virtual {v2, v3, v0, v1}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 146535
    const-string v0, "call"

    invoke-virtual {v2, v0, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 146536
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 146537
    iget-object v0, p0, LX/0pq;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0po;

    .line 146538
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_1

    .line 146539
    :cond_2
    const-string v0, "caches"

    invoke-virtual {v2, v0, v1}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 146540
    invoke-virtual {v2}, LX/0oG;->d()V

    goto :goto_0
.end method


# virtual methods
.method public final a(J)V
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 146541
    const-wide/32 v0, 0x32000

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    .line 146542
    iget-object v0, p0, LX/0pq;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 146543
    iget-wide v2, p0, LX/0pq;->d:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0x493e0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 146544
    iput-wide v0, p0, LX/0pq;->d:J

    .line 146545
    iget-object v0, p0, LX/0pq;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0po;

    .line 146546
    :try_start_0
    invoke-interface {v0}, LX/0po;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 146547
    :catch_0
    move-exception v0

    .line 146548
    sget-object v2, LX/0pq;->b:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 146549
    :cond_0
    iget-object v0, p0, LX/0pq;->f:LX/0V8;

    invoke-virtual {v0}, LX/0V8;->b()V

    .line 146550
    const-string v0, "trimToNothing"

    invoke-direct {p0, p1, p2, v0}, LX/0pq;->a(JLjava/lang/String;)V

    .line 146551
    :goto_1
    return-void

    .line 146552
    :cond_1
    iget-object v0, p0, LX/0pq;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0po;

    .line 146553
    :try_start_1
    invoke-interface {v0}, LX/0po;->U_()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 146554
    :catch_1
    move-exception v0

    .line 146555
    sget-object v2, LX/0pq;->b:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_2

    .line 146556
    :cond_2
    iget-object v0, p0, LX/0pq;->f:LX/0V8;

    invoke-virtual {v0}, LX/0V8;->b()V

    .line 146557
    const-string v0, "trimToMinimum"

    invoke-direct {p0, p1, p2, v0}, LX/0pq;->a(JLjava/lang/String;)V

    goto :goto_1
.end method

.method public final a(LX/0po;)V
    .locals 2

    .prologue
    .line 146558
    iget-object v0, p0, LX/0pq;->c:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146559
    return-void
.end method
