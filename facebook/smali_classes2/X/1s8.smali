.class public final LX/1s8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1s7;


# instance fields
.field public final synthetic a:LX/1s6;


# direct methods
.method public constructor <init>(LX/1s6;)V
    .locals 0

    .prologue
    .line 333740
    iput-object p1, p0, LX/1s8;->a:LX/1s6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;
    .locals 1

    .prologue
    .line 333741
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->INVITE_FRIENDS_TO_EVENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 333742
    const-string v0, "ANDROID_EVENT_PERMALINK"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ANDROID_EVENT_PERMALINK_PRIVATE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
