.class public abstract LX/0m4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0m5;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "LX/0m4",
        "<TT;>;>",
        "Ljava/lang/Object;",
        "LX/0m5;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x7b656637b7cdf9a9L


# instance fields
.field public final _base:LX/0lh;

.field public final _mapperFeatures:I


# direct methods
.method public constructor <init>(LX/0lh;I)V
    .locals 0

    .prologue
    .line 131691
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131692
    iput-object p1, p0, LX/0m4;->_base:LX/0lh;

    .line 131693
    iput p2, p0, LX/0m4;->_mapperFeatures:I

    .line 131694
    return-void
.end method

.method public constructor <init>(LX/0m4;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m4",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 131687
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131688
    iget-object v0, p1, LX/0m4;->_base:LX/0lh;

    iput-object v0, p0, LX/0m4;->_base:LX/0lh;

    .line 131689
    iget v0, p1, LX/0m4;->_mapperFeatures:I

    iput v0, p0, LX/0m4;->_mapperFeatures:I

    .line 131690
    return-void
.end method

.method public static a(Ljava/lang/Class;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<F:",
            "Ljava/lang/Enum",
            "<TF;>;:",
            "LX/0m7;",
            ">(",
            "Ljava/lang/Class",
            "<TF;>;)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 131682
    invoke-virtual {p0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    array-length v5, v0

    move v4, v1

    move v3, v1

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    move-object v1, v2

    .line 131683
    check-cast v1, LX/0m7;

    invoke-interface {v1}, LX/0m7;->enabledByDefault()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 131684
    check-cast v2, LX/0m7;

    invoke-interface {v2}, LX/0m7;->getMask()I

    move-result v1

    or-int/2addr v1, v3

    .line 131685
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v3, v1

    goto :goto_0

    .line 131686
    :cond_0
    return v3

    :cond_1
    move v1, v3

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0lJ;Ljava/lang/Class;)LX/0lJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 131681
    invoke-virtual {p0}, LX/0m4;->n()LX/0li;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/0li;->a(LX/0lJ;Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(LX/0lJ;)LX/0lS;
.end method

.method public a()LX/0lU;
    .locals 1

    .prologue
    .line 131678
    iget-object v0, p0, LX/0m4;->_base:LX/0lh;

    .line 131679
    iget-object p0, v0, LX/0lh;->_annotationIntrospector:LX/0lU;

    move-object v0, p0

    .line 131680
    return-object v0
.end method

.method public final a(LX/0lO;Ljava/lang/Class;)LX/4qy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            "Ljava/lang/Class",
            "<+",
            "LX/4qy",
            "<*>;>;)",
            "LX/4qy",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 131673
    invoke-virtual {p0}, LX/0m4;->l()LX/4py;

    move-result-object v0

    .line 131674
    if-eqz v0, :cond_0

    .line 131675
    invoke-virtual {v0}, LX/4py;->d()LX/4qy;

    move-result-object v0

    .line 131676
    if-eqz v0, :cond_0

    .line 131677
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/0m4;->h()Z

    move-result v0

    invoke-static {p2, v0}, LX/1Xw;->b(Ljava/lang/Class;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4qy;

    goto :goto_0
.end method

.method public final a(LX/0m6;)Z
    .locals 2

    .prologue
    .line 131672
    iget v0, p0, LX/0m4;->_mapperFeatures:I

    invoke-virtual {p1}, LX/0m6;->getMask()I

    move-result v1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Class;)LX/0lJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 131671
    invoke-virtual {p0}, LX/0m4;->n()LX/0li;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/0li;->a(Ljava/lang/reflect/Type;LX/1Y3;)LX/0lJ;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/0lO;Ljava/lang/Class;)LX/4qx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            "Ljava/lang/Class",
            "<+",
            "LX/4qx;",
            ">;)",
            "LX/4qx;"
        }
    .end annotation

    .prologue
    .line 131666
    invoke-virtual {p0}, LX/0m4;->l()LX/4py;

    move-result-object v0

    .line 131667
    if-eqz v0, :cond_0

    .line 131668
    invoke-virtual {v0}, LX/4py;->e()LX/4qx;

    move-result-object v0

    .line 131669
    if-eqz v0, :cond_0

    .line 131670
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/0m4;->h()Z

    move-result v0

    invoke-static {p2, v0}, LX/1Xw;->b(Ljava/lang/Class;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4qx;

    goto :goto_0
.end method

.method public final c(Ljava/lang/Class;)LX/0lS;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lS;"
        }
    .end annotation

    .prologue
    .line 131665
    invoke-virtual {p0, p1}, LX/0m4;->b(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0m4;->a(LX/0lJ;)LX/0lS;

    move-result-object v0

    return-object v0
.end method

.method public c()LX/0lW;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0lW",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 131695
    iget-object v0, p0, LX/0m4;->_base:LX/0lh;

    .line 131696
    iget-object p0, v0, LX/0lh;->_visibilityChecker:LX/0lW;

    move-object v0, p0

    .line 131697
    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 131664
    sget-object v0, LX/0m6;->USE_ANNOTATIONS:LX/0m6;

    invoke-virtual {p0, v0}, LX/0m4;->a(LX/0m6;)Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 131663
    sget-object v0, LX/0m6;->CAN_OVERRIDE_ACCESS_MODIFIERS:LX/0m6;

    invoke-virtual {p0, v0}, LX/0m4;->a(LX/0m6;)Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 131662
    sget-object v0, LX/0m6;->SORT_PROPERTIES_ALPHABETICALLY:LX/0m6;

    invoke-virtual {p0, v0}, LX/0m4;->a(LX/0m6;)Z

    move-result v0

    return v0
.end method

.method public final j()LX/0lM;
    .locals 1

    .prologue
    .line 131659
    iget-object v0, p0, LX/0m4;->_base:LX/0lh;

    .line 131660
    iget-object p0, v0, LX/0lh;->_classIntrospector:LX/0lM;

    move-object v0, p0

    .line 131661
    return-object v0
.end method

.method public final k()LX/4pt;
    .locals 1

    .prologue
    .line 131656
    iget-object v0, p0, LX/0m4;->_base:LX/0lh;

    .line 131657
    iget-object p0, v0, LX/0lh;->_propertyNamingStrategy:LX/4pt;

    move-object v0, p0

    .line 131658
    return-object v0
.end method

.method public final l()LX/4py;
    .locals 1

    .prologue
    .line 131653
    iget-object v0, p0, LX/0m4;->_base:LX/0lh;

    .line 131654
    iget-object p0, v0, LX/0lh;->_handlerInstantiator:LX/4py;

    move-object v0, p0

    .line 131655
    return-object v0
.end method

.method public final m()LX/4qy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4qy",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 131650
    iget-object v0, p0, LX/0m4;->_base:LX/0lh;

    .line 131651
    iget-object p0, v0, LX/0lh;->_typeResolverBuilder:LX/4qy;

    move-object v0, p0

    .line 131652
    return-object v0
.end method

.method public final n()LX/0li;
    .locals 1

    .prologue
    .line 131647
    iget-object v0, p0, LX/0m4;->_base:LX/0lh;

    .line 131648
    iget-object p0, v0, LX/0lh;->_typeFactory:LX/0li;

    move-object v0, p0

    .line 131649
    return-object v0
.end method

.method public final o()Ljava/text/DateFormat;
    .locals 1

    .prologue
    .line 131644
    iget-object v0, p0, LX/0m4;->_base:LX/0lh;

    .line 131645
    iget-object p0, v0, LX/0lh;->_dateFormat:Ljava/text/DateFormat;

    move-object v0, p0

    .line 131646
    return-object v0
.end method

.method public final p()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 131635
    iget-object v0, p0, LX/0m4;->_base:LX/0lh;

    .line 131636
    iget-object p0, v0, LX/0lh;->_locale:Ljava/util/Locale;

    move-object v0, p0

    .line 131637
    return-object v0
.end method

.method public final q()Ljava/util/TimeZone;
    .locals 1

    .prologue
    .line 131641
    iget-object v0, p0, LX/0m4;->_base:LX/0lh;

    .line 131642
    iget-object p0, v0, LX/0lh;->_timeZone:Ljava/util/TimeZone;

    move-object v0, p0

    .line 131643
    return-object v0
.end method

.method public final r()LX/0ln;
    .locals 1

    .prologue
    .line 131638
    iget-object v0, p0, LX/0m4;->_base:LX/0lh;

    .line 131639
    iget-object p0, v0, LX/0lh;->_defaultBase64:LX/0ln;

    move-object v0, p0

    .line 131640
    return-object v0
.end method
