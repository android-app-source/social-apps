.class public LX/1dV;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/util/concurrent/atomic/AtomicInteger;

.field public static final d:Landroid/os/Handler;

.field private static volatile e:Landroid/os/Looper;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ComponentTree.class"
    .end annotation
.end field

.field public static final f:[I

.field public static final g:[I

.field public static final h:Landroid/graphics/Rect;

.field public static final i:Landroid/graphics/Rect;


# instance fields
.field private A:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private B:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final a:I

.field private final j:Ljava/lang/Runnable;

.field public final k:LX/1De;

.field public l:Z

.field public m:Z

.field public n:Z

.field private o:Z

.field private p:Z

.field public q:Lcom/facebook/components/ComponentView;

.field private r:Landroid/os/Handler;

.field private s:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private t:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public u:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public v:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private w:LX/1dN;

.field private x:LX/1dN;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private y:LX/1mg;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private z:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 286234
    const-class v0, LX/1dV;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1dV;->b:Ljava/lang/String;

    .line 286235
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, LX/1dV;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 286236
    new-instance v0, LX/1mf;

    invoke-direct {v0}, LX/1mf;-><init>()V

    sput-object v0, LX/1dV;->d:Landroid/os/Handler;

    .line 286237
    new-array v0, v2, [I

    sput-object v0, LX/1dV;->f:[I

    .line 286238
    new-array v0, v2, [I

    sput-object v0, LX/1dV;->g:[I

    .line 286239
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/1dV;->h:Landroid/graphics/Rect;

    .line 286240
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/1dV;->i:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(LX/1me;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 286241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 286242
    new-instance v0, Lcom/facebook/components/ComponentTree$1;

    invoke-direct {v0, p0}, Lcom/facebook/components/ComponentTree$1;-><init>(LX/1dV;)V

    iput-object v0, p0, LX/1dV;->j:Ljava/lang/Runnable;

    .line 286243
    iput v1, p0, LX/1dV;->u:I

    .line 286244
    iput v1, p0, LX/1dV;->v:I

    .line 286245
    sget-object v0, LX/1dV;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, LX/1dV;->a:I

    .line 286246
    iget-object v0, p1, LX/1me;->a:LX/1De;

    .line 286247
    invoke-static {v0}, LX/1De;->i(LX/1De;)LX/1De;

    move-result-object v1

    .line 286248
    iput-object p0, v1, LX/1De;->k:LX/1dV;

    .line 286249
    move-object v0, v1

    .line 286250
    iput-object v0, p0, LX/1dV;->k:LX/1De;

    .line 286251
    iget-object v0, p1, LX/1me;->b:LX/1X1;

    iput-object v0, p0, LX/1dV;->t:LX/1X1;

    .line 286252
    iget-boolean v0, p1, LX/1me;->c:Z

    iput-boolean v0, p0, LX/1dV;->m:Z

    .line 286253
    iget-boolean v0, p1, LX/1me;->d:Z

    iput-boolean v0, p0, LX/1dV;->n:Z

    .line 286254
    iget-object v0, p1, LX/1me;->e:Landroid/os/Handler;

    iput-object v0, p0, LX/1dV;->r:Landroid/os/Handler;

    .line 286255
    iget-object v0, p1, LX/1me;->f:Ljava/lang/Object;

    iput-object v0, p0, LX/1dV;->z:Ljava/lang/Object;

    .line 286256
    iget-boolean v0, p1, LX/1me;->h:Z

    iput-boolean v0, p0, LX/1dV;->p:Z

    .line 286257
    iget-object v0, p0, LX/1dV;->r:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 286258
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, LX/1dV;->t()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/1dV;->r:Landroid/os/Handler;

    .line 286259
    :cond_0
    iget-object v0, p1, LX/1me;->g:LX/1mg;

    .line 286260
    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-static {v0}, LX/1cy;->a(LX/1mg;)LX/1mg;

    move-result-object v0

    :cond_1
    iput-object v0, p0, LX/1dV;->y:LX/1mg;

    .line 286261
    return-void
.end method

.method private static a(LX/1dV;Ljava/lang/Object;LX/1De;LX/1X1;IIZLX/1ml;)LX/1dN;
    .locals 7
    .param p0    # LX/1dV;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "LX/1De;",
            "LX/1X1",
            "<*>;IIZ",
            "LX/1ml;",
            ")",
            "LX/1dN;"
        }
    .end annotation

    .prologue
    .line 286262
    monitor-enter p0

    .line 286263
    :try_start_0
    new-instance v0, LX/1De;

    iget-object v1, p0, LX/1dV;->y:LX/1mg;

    invoke-static {v1}, LX/1cy;->a(LX/1mg;)LX/1mg;

    move-result-object v1

    invoke-direct {v0, p2, v1}, LX/1De;-><init>(Landroid/content/Context;LX/1mg;)V

    .line 286264
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286265
    if-eqz p1, :cond_0

    .line 286266
    monitor-enter p1

    .line 286267
    :try_start_1
    iget v2, p0, LX/1dV;->a:I

    move-object v1, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move-object v6, p7

    invoke-static/range {v0 .. v6}, LX/1dN;->a(LX/1De;LX/1X1;IIIZLX/1ml;)LX/1dN;

    move-result-object v0

    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 286268
    :goto_0
    return-object v0

    .line 286269
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 286270
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 286271
    :cond_0
    iget v2, p0, LX/1dV;->a:I

    move-object v1, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move-object v6, p7

    invoke-static/range {v0 .. v6}, LX/1dN;->a(LX/1De;LX/1X1;IIIZLX/1ml;)LX/1dN;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/1De;LX/1X5;)LX/1me;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X5",
            "<*>;)",
            "LX/1me;"
        }
    .end annotation

    .prologue
    .line 286272
    invoke-virtual {p1}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    invoke-static {p0, v0}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Context;
    .locals 2

    .prologue
    .line 286273
    move-object v0, p0

    .line 286274
    :goto_0
    instance-of v1, v0, Landroid/content/ContextWrapper;

    if-eqz v1, :cond_0

    .line 286275
    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    .line 286276
    :cond_0
    return-object v0
.end method

.method private static a(LX/1dV;LX/1X1;IIZLX/1no;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;IIZ",
            "LX/1no;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 286277
    monitor-enter p0

    .line 286278
    :try_start_0
    iget-object v2, p0, LX/1dV;->y:LX/1mg;

    .line 286279
    iget-object v4, v2, LX/1mg;->b:Ljava/util/Map;

    move-object v2, v4

    .line 286280
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-lez v2, :cond_0

    if-eqz p1, :cond_0

    .line 286281
    invoke-virtual {p1}, LX/1X1;->g()LX/1X1;

    move-result-object v2

    .line 286282
    sget-object v4, LX/1X1;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v4

    iput v4, v2, LX/1X1;->b:I

    .line 286283
    move-object p1, v2

    .line 286284
    :cond_0
    if-eqz p1, :cond_1

    move v6, v1

    .line 286285
    :goto_0
    if-eq p2, v3, :cond_2

    move v5, v1

    .line 286286
    :goto_1
    if-eq p3, v3, :cond_3

    move v4, v1

    .line 286287
    :goto_2
    iget-boolean v2, p0, LX/1dV;->s:Z

    if-eqz v2, :cond_4

    if-nez v6, :cond_4

    .line 286288
    monitor-exit p0

    .line 286289
    :goto_3
    return-void

    :cond_1
    move v6, v0

    .line 286290
    goto :goto_0

    :cond_2
    move v5, v0

    .line 286291
    goto :goto_1

    :cond_3
    move v4, v0

    .line 286292
    goto :goto_2

    .line 286293
    :cond_4
    if-eqz v5, :cond_5

    iget v2, p0, LX/1dV;->u:I

    if-ne p2, v2, :cond_9

    :cond_5
    move v3, v1

    .line 286294
    :goto_4
    if-eqz v4, :cond_6

    iget v2, p0, LX/1dV;->v:I

    if-ne p3, v2, :cond_a

    :cond_6
    move v2, v1

    .line 286295
    :goto_5
    if-eqz v6, :cond_7

    .line 286296
    iget v7, p1, LX/1X1;->b:I

    move v7, v7

    .line 286297
    iget-object v8, p0, LX/1dV;->t:LX/1X1;

    .line 286298
    iget v9, v8, LX/1X1;->b:I

    move v8, v9

    .line 286299
    if-ne v7, v8, :cond_8

    :cond_7
    move v0, v1

    .line 286300
    :cond_8
    if-eqz v3, :cond_b

    if-eqz v2, :cond_b

    if-eqz v0, :cond_b

    .line 286301
    monitor-exit p0

    goto :goto_3

    .line 286302
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_9
    move v3, v0

    .line 286303
    goto :goto_4

    :cond_a
    move v2, v0

    .line 286304
    goto :goto_5

    .line 286305
    :cond_b
    if-eqz v5, :cond_c

    .line 286306
    :try_start_1
    iput p2, p0, LX/1dV;->u:I

    .line 286307
    :cond_c
    if-eqz v4, :cond_d

    .line 286308
    iput p3, p0, LX/1dV;->v:I

    .line 286309
    :cond_d
    if-eqz v6, :cond_e

    .line 286310
    iput-object p1, p0, LX/1dV;->t:LX/1X1;

    .line 286311
    :cond_e
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 286312
    if-eqz p4, :cond_f

    if-eqz p5, :cond_f

    .line 286313
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The layout can\'t be calculated asynchronously if we need the Size back"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286314
    :cond_f
    if-eqz p4, :cond_10

    .line 286315
    iget-object v0, p0, LX/1dV;->r:Landroid/os/Handler;

    iget-object v1, p0, LX/1dV;->j:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 286316
    iget-object v0, p0, LX/1dV;->r:Landroid/os/Handler;

    iget-object v1, p0, LX/1dV;->j:Ljava/lang/Runnable;

    const v2, 0x22f8c824

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_3

    .line 286317
    :cond_10
    invoke-static {p0, p5}, LX/1dV;->a$redex0(LX/1dV;LX/1no;)V

    goto :goto_3
.end method

.method public static a(LX/1dV;Ljava/lang/String;LX/48B;Z)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v2, -0x1

    .line 286318
    monitor-enter p0

    .line 286319
    :try_start_0
    iget-object v1, p0, LX/1dV;->t:LX/1X1;

    if-nez v1, :cond_0

    .line 286320
    monitor-exit p0

    .line 286321
    :goto_0
    return-void

    .line 286322
    :cond_0
    iget-object v1, p0, LX/1dV;->y:LX/1mg;

    .line 286323
    iget-object v3, v1, LX/1mg;->b:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 286324
    if-nez v3, :cond_1

    .line 286325
    const/4 v3, 0x0

    invoke-static {v3}, LX/1mg;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    move-object v3, v3

    .line 286326
    iget-object v4, v1, LX/1mg;->b:Ljava/util/Map;

    invoke-interface {v4, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286327
    :cond_1
    invoke-interface {v3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 286328
    iget-boolean v1, p0, LX/1dV;->A:Z

    if-eqz v1, :cond_4

    .line 286329
    iget v1, p0, LX/1dV;->B:I

    if-ne v1, v0, :cond_2

    .line 286330
    monitor-exit p0

    goto :goto_0

    .line 286331
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 286332
    :cond_2
    if-eqz p3, :cond_3

    const/4 v0, 0x1

    :cond_3
    :try_start_1
    iput v0, p0, LX/1dV;->B:I

    .line 286333
    monitor-exit p0

    goto :goto_0

    .line 286334
    :cond_4
    iget-object v0, p0, LX/1dV;->t:LX/1X1;

    invoke-virtual {v0}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    .line 286335
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 286336
    const/4 v5, 0x0

    move-object v0, p0

    move v3, v2

    move v4, p3

    invoke-static/range {v0 .. v5}, LX/1dV;->a(LX/1dV;LX/1X1;IIZLX/1no;)V

    goto :goto_0
.end method

.method public static a(Landroid/view/View;[ILandroid/graphics/Rect;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 286337
    invoke-static {}, LX/1dS;->b()V

    .line 286338
    invoke-virtual {p0, p1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 286339
    aget v0, p1, v2

    aget v1, p1, v4

    aget v2, p1, v2

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    aget v3, p1, v4

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 286340
    return-void
.end method

.method private static a(LX/1dN;II)Z
    .locals 1

    .prologue
    .line 286341
    if-eqz p0, :cond_0

    invoke-virtual {p0, p1, p2}, LX/1dN;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/1dN;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/1dV;LX/1dN;)Z
    .locals 4

    .prologue
    .line 286342
    invoke-static {p0}, LX/1dS;->a(Ljava/lang/Object;)V

    .line 286343
    iget-object v0, p0, LX/1dV;->t:LX/1X1;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1dV;->t:LX/1X1;

    .line 286344
    iget v1, v0, LX/1X1;->b:I

    move v0, v1

    .line 286345
    iget v1, p0, LX/1dV;->u:I

    iget v2, p0, LX/1dV;->v:I

    .line 286346
    if-eqz p1, :cond_1

    .line 286347
    iget-object v3, p1, LX/1dN;->e:LX/1X1;

    .line 286348
    iget p0, v3, LX/1X1;->b:I

    move v3, p0

    .line 286349
    if-ne v3, v0, :cond_2

    invoke-virtual {p1, v1, v2}, LX/1dN;->a(II)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    move v3, v3

    .line 286350
    if-eqz v3, :cond_1

    invoke-virtual {p1}, LX/1dN;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    :goto_1
    move v0, v3

    .line 286351
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_2

    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/1dV;LX/1no;)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 286352
    iget-object v0, p0, LX/1dV;->r:Landroid/os/Handler;

    invoke-virtual {v0, v7}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 286353
    monitor-enter p0

    .line 286354
    :try_start_0
    const/4 v1, -0x1

    .line 286355
    invoke-static {p0}, LX/1dS;->a(Ljava/lang/Object;)V

    .line 286356
    iget v0, p0, LX/1dV;->u:I

    if-eq v0, v1, :cond_d

    iget v0, p0, LX/1dV;->v:I

    if-eq v0, v1, :cond_d

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 286357
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1dV;->t:LX/1X1;

    if-nez v0, :cond_1

    .line 286358
    :cond_0
    monitor-exit p0

    .line 286359
    :goto_1
    return-void

    .line 286360
    :cond_1
    invoke-direct {p0}, LX/1dV;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 286361
    monitor-exit p0

    goto :goto_1

    .line 286362
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 286363
    :cond_2
    :try_start_1
    iget v4, p0, LX/1dV;->u:I

    .line 286364
    iget v5, p0, LX/1dV;->v:I

    .line 286365
    iget-object v0, p0, LX/1dV;->t:LX/1X1;

    invoke-virtual {v0}, LX/1X1;->g()LX/1X1;

    move-result-object v3

    .line 286366
    iget-object v0, p0, LX/1dV;->w:LX/1dN;

    if-eqz v0, :cond_c

    .line 286367
    iget-object v0, p0, LX/1dV;->w:LX/1dN;

    invoke-virtual {v0}, LX/1dN;->m()LX/1dN;

    move-result-object v0

    move-object v8, v0

    .line 286368
    :goto_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 286369
    const/4 v6, 0x3

    .line 286370
    iget-object v0, p0, LX/1dV;->k:LX/1De;

    .line 286371
    iget-object v1, v0, LX/1De;->d:LX/1cp;

    move-object v1, v1

    .line 286372
    if-nez v1, :cond_e

    .line 286373
    :goto_3
    iget-object v1, p0, LX/1dV;->z:Ljava/lang/Object;

    iget-object v2, p0, LX/1dV;->k:LX/1De;

    iget-boolean v6, p0, LX/1dV;->n:Z

    if-eqz v8, :cond_3

    .line 286374
    iget-object v0, v8, LX/1dN;->p:LX/1ml;

    move-object v7, v0

    .line 286375
    :cond_3
    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1dV;->a(LX/1dV;Ljava/lang/Object;LX/1De;LX/1X1;IIZLX/1ml;)LX/1dN;

    move-result-object v2

    .line 286376
    if-eqz p1, :cond_4

    .line 286377
    iget v0, v2, LX/1dN;->r:I

    move v0, v0

    .line 286378
    iput v0, p1, LX/1no;->a:I

    .line 286379
    iget v0, v2, LX/1dN;->s:I

    move v0, v0

    .line 286380
    iput v0, p1, LX/1no;->b:I

    .line 286381
    :cond_4
    if-eqz v8, :cond_5

    .line 286382
    invoke-virtual {v8}, LX/1dN;->l()V

    .line 286383
    :cond_5
    const/4 v0, 0x0

    .line 286384
    monitor-enter p0

    .line 286385
    :try_start_2
    invoke-direct {p0}, LX/1dV;->r()Z

    move-result v1

    if-nez v1, :cond_b

    iget v1, p0, LX/1dV;->u:I

    iget v4, p0, LX/1dV;->v:I

    invoke-static {v2, v1, v4}, LX/1dV;->a(LX/1dN;II)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 286386
    if-eqz v2, :cond_7

    .line 286387
    invoke-virtual {v2}, LX/1dN;->n()LX/1mg;

    move-result-object v0

    .line 286388
    if-eqz v0, :cond_7

    .line 286389
    iget-object v1, p0, LX/1dV;->y:LX/1mg;

    if-eqz v1, :cond_6

    .line 286390
    iget-object v1, p0, LX/1dV;->y:LX/1mg;

    invoke-virtual {v1, v0}, LX/1mg;->c(LX/1mg;)V

    .line 286391
    :cond_6
    invoke-static {v0}, LX/1cy;->b(LX/1mg;)V

    .line 286392
    :cond_7
    iget-object v1, p0, LX/1dV;->x:LX/1dN;

    .line 286393
    iput-object v2, p0, LX/1dV;->x:LX/1dN;

    .line 286394
    const/4 v0, 0x1

    .line 286395
    :goto_4
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 286396
    if-eqz v1, :cond_8

    .line 286397
    invoke-virtual {v1}, LX/1dN;->l()V

    .line 286398
    :cond_8
    if-eqz v0, :cond_9

    .line 286399
    invoke-static {}, LX/1dS;->a()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 286400
    invoke-static {p0}, LX/1dV;->m(LX/1dV;)V

    .line 286401
    :cond_9
    :goto_5
    iget-object v0, p0, LX/1dV;->k:LX/1De;

    .line 286402
    iget-object v1, v0, LX/1De;->d:LX/1cp;

    move-object v0, v1

    .line 286403
    if-eqz v0, :cond_a

    .line 286404
    const/4 v1, 0x3

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v3, v2}, LX/1cp;->a(ILjava/lang/Object;I)V

    .line 286405
    :cond_a
    goto/16 :goto_1

    .line 286406
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_b
    move-object v1, v2

    goto :goto_4

    :cond_c
    move-object v8, v7

    goto :goto_2

    :cond_d
    :try_start_4
    const/4 v0, 0x0

    goto/16 :goto_0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 286407
    :cond_e
    const-string v0, "log_tag"

    iget-object v2, p0, LX/1dV;->k:LX/1De;

    .line 286408
    iget-object v9, v2, LX/1De;->c:Ljava/lang/String;

    move-object v2, v9

    .line 286409
    invoke-virtual {v1, v6, v3, v0, v2}, LX/1cp;->a(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 286410
    const-string v0, "tree_diff_enabled"

    iget-boolean v2, p0, LX/1dV;->n:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v6, v3, v0, v2}, LX/1cp;->b(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 286411
    const-string v2, "is_async_layout"

    invoke-static {}, LX/1dS;->a()Z

    move-result v0

    if-nez v0, :cond_f

    const/4 v0, 0x1

    :goto_6
    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v6, v3, v2, v0}, LX/1cp;->b(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_f
    const/4 v0, 0x0

    goto :goto_6

    .line 286412
    :cond_10
    sget-object v0, LX/1dV;->d:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_5
.end method

.method private static b(LX/1dN;III)Z
    .locals 1

    .prologue
    .line 286413
    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, LX/1dN;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286414
    iget v0, p0, LX/1dN;->r:I

    if-ne v0, p2, :cond_1

    iget v0, p0, LX/1dN;->s:I

    if-ne v0, p3, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 286415
    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/1dN;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static l(LX/1dV;)LX/1dN;
    .locals 4
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 286416
    invoke-static {p0}, LX/1dS;->a(Ljava/lang/Object;)V

    .line 286417
    iget-object v0, p0, LX/1dV;->w:LX/1dN;

    invoke-static {p0, v0}, LX/1dV;->a(LX/1dV;LX/1dN;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 286418
    iget-object v0, p0, LX/1dV;->x:LX/1dN;

    iget v1, p0, LX/1dV;->u:I

    iget v2, p0, LX/1dV;->v:I

    invoke-static {v0, v1, v2}, LX/1dV;->a(LX/1dN;II)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1dV;->w:LX/1dN;

    iget v1, p0, LX/1dV;->u:I

    iget v2, p0, LX/1dV;->v:I

    invoke-static {v0, v1, v2}, LX/1dV;->a(LX/1dN;II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 286419
    :cond_0
    const/4 v0, 0x0

    .line 286420
    :goto_0
    if-eqz v0, :cond_2

    .line 286421
    iget-object v0, p0, LX/1dV;->x:LX/1dN;

    .line 286422
    iput-object v3, p0, LX/1dV;->x:LX/1dN;

    .line 286423
    :goto_1
    return-object v0

    .line 286424
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 286425
    :cond_2
    iget-object v0, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    if-eqz v0, :cond_3

    .line 286426
    iget-object v0, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->m()V

    .line 286427
    :cond_3
    iget-object v0, p0, LX/1dV;->w:LX/1dN;

    .line 286428
    iget-object v1, p0, LX/1dV;->x:LX/1dN;

    iput-object v1, p0, LX/1dV;->w:LX/1dN;

    .line 286429
    iput-object v3, p0, LX/1dV;->x:LX/1dN;

    goto :goto_1
.end method

.method public static m(LX/1dV;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 286430
    invoke-static {}, LX/1dS;->b()V

    .line 286431
    iget-boolean v2, p0, LX/1dV;->o:Z

    if-nez v2, :cond_1

    .line 286432
    :cond_0
    :goto_0
    return-void

    .line 286433
    :cond_1
    monitor-enter p0

    .line 286434
    :try_start_0
    iget-object v2, p0, LX/1dV;->t:LX/1X1;

    if-nez v2, :cond_2

    .line 286435
    monitor-exit p0

    goto :goto_0

    .line 286436
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 286437
    :cond_2
    :try_start_1
    iget-object v2, p0, LX/1dV;->w:LX/1dN;

    .line 286438
    invoke-static {p0}, LX/1dV;->l(LX/1dV;)LX/1dN;

    move-result-object v3

    .line 286439
    iget-object v4, p0, LX/1dV;->w:LX/1dN;

    if-eq v4, v2, :cond_4

    move v2, v0

    .line 286440
    :goto_1
    iget-object v4, p0, LX/1dV;->t:LX/1X1;

    .line 286441
    iget v5, v4, LX/1X1;->b:I

    move v4, v5

    .line 286442
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 286443
    if-eqz v3, :cond_3

    .line 286444
    invoke-virtual {v3}, LX/1dN;->l()V

    .line 286445
    :cond_3
    if-eqz v2, :cond_0

    .line 286446
    iget-object v2, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    invoke-virtual {v2}, Lcom/facebook/components/ComponentView;->getMeasuredWidth()I

    move-result v2

    .line 286447
    iget-object v3, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    invoke-virtual {v3}, Lcom/facebook/components/ComponentView;->getMeasuredHeight()I

    move-result v3

    .line 286448
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 286449
    iget-object v5, p0, LX/1dV;->w:LX/1dN;

    invoke-static {v5, v4, v2, v3}, LX/1dV;->b(LX/1dN;III)Z

    move-result v2

    if-nez v2, :cond_5

    .line 286450
    :goto_2
    if-eqz v0, :cond_6

    .line 286451
    iget-object v0, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->requestLayout()V

    goto :goto_0

    :cond_4
    move v2, v1

    .line 286452
    goto :goto_1

    :cond_5
    move v0, v1

    .line 286453
    goto :goto_2

    .line 286454
    :cond_6
    invoke-static {p0}, LX/1dV;->n(LX/1dV;)Z

    goto :goto_0
.end method

.method public static n(LX/1dV;)Z
    .locals 1

    .prologue
    .line 286455
    iget-object v0, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 286456
    iget-boolean v0, p0, LX/1dV;->m:Z

    if-eqz v0, :cond_0

    .line 286457
    invoke-virtual {p0}, LX/1dV;->c()V

    .line 286458
    :goto_0
    const/4 v0, 0x1

    .line 286459
    :goto_1
    return v0

    .line 286460
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/1dV;->a(Landroid/graphics/Rect;)V

    goto :goto_0

    .line 286461
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private r()Z
    .locals 1

    .prologue
    .line 286462
    invoke-static {p0}, LX/1dS;->a(Ljava/lang/Object;)V

    .line 286463
    iget-object v0, p0, LX/1dV;->w:LX/1dN;

    invoke-static {p0, v0}, LX/1dV;->a(LX/1dV;LX/1dN;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1dV;->x:LX/1dN;

    invoke-static {p0, v0}, LX/1dV;->a(LX/1dV;LX/1dN;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static declared-synchronized t()Landroid/os/Looper;
    .locals 4

    .prologue
    .line 286228
    const-class v1, LX/1dV;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1dV;->e:Landroid/os/Looper;

    if-nez v0, :cond_0

    .line 286229
    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "ComponentLayoutThread"

    const/16 v3, 0xa

    invoke-direct {v0, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 286230
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 286231
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    sput-object v0, LX/1dV;->e:Landroid/os/Looper;

    .line 286232
    :cond_0
    sget-object v0, LX/1dV;->e:Landroid/os/Looper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 286233
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 286021
    invoke-static {}, LX/1dS;->b()V

    .line 286022
    iget-object v1, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    if-nez v1, :cond_0

    .line 286023
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to attach a ComponentTree without a set View"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286024
    :cond_0
    monitor-enter p0

    .line 286025
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, LX/1dV;->o:Z

    .line 286026
    invoke-static {p0}, LX/1dV;->l(LX/1dV;)LX/1dN;

    move-result-object v1

    .line 286027
    iget-object v2, p0, LX/1dV;->t:LX/1X1;

    .line 286028
    iget v3, v2, LX/1X1;->b:I

    move v2, v3

    .line 286029
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286030
    if-eqz v1, :cond_1

    .line 286031
    invoke-virtual {v1}, LX/1dN;->l()V

    .line 286032
    :cond_1
    iget-object v1, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    invoke-virtual {v1}, Lcom/facebook/components/ComponentView;->getMeasuredWidth()I

    move-result v1

    .line 286033
    iget-object v3, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    invoke-virtual {v3}, Lcom/facebook/components/ComponentView;->getMeasuredHeight()I

    move-result v3

    .line 286034
    if-eqz v1, :cond_2

    if-nez v3, :cond_3

    .line 286035
    :cond_2
    :goto_0
    return-void

    .line 286036
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 286037
    :cond_3
    iget-object v4, p0, LX/1dV;->w:LX/1dN;

    invoke-static {v4, v2, v1, v3}, LX/1dV;->b(LX/1dN;III)Z

    move-result v1

    if-nez v1, :cond_5

    .line 286038
    :goto_1
    if-nez v0, :cond_4

    iget-object v0, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->n()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 286039
    :cond_4
    iget-object v0, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->requestLayout()V

    goto :goto_0

    .line 286040
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 286041
    :cond_6
    iget-object v0, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->g()V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 286464
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/1dV;->a(IILX/1no;)V

    .line 286465
    return-void
.end method

.method public final a(IILX/1no;)V
    .locals 6

    .prologue
    .line 286042
    const/4 v1, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move v2, p1

    move v3, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/1dV;->a(LX/1dV;LX/1X1;IIZLX/1no;)V

    .line 286043
    return-void
.end method

.method public final a(II[I)V
    .locals 11

    .prologue
    const/4 v10, -0x1

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 286044
    invoke-static {}, LX/1dS;->b()V

    .line 286045
    monitor-enter p0

    .line 286046
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/1dV;->A:Z

    .line 286047
    iput p1, p0, LX/1dV;->u:I

    .line 286048
    iput p2, p0, LX/1dV;->v:I

    .line 286049
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1dV;->s:Z

    .line 286050
    invoke-static {p0}, LX/1dV;->l(LX/1dV;)LX/1dN;

    move-result-object v0

    .line 286051
    iget-object v1, p0, LX/1dV;->w:LX/1dN;

    invoke-static {p0, v1}, LX/1dV;->a(LX/1dV;LX/1dN;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 286052
    iget-object v1, p0, LX/1dV;->t:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v3

    .line 286053
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286054
    if-eqz v0, :cond_0

    .line 286055
    invoke-virtual {v0}, LX/1dN;->l()V

    .line 286056
    :cond_0
    if-eqz v3, :cond_3

    .line 286057
    iget-object v0, p0, LX/1dV;->w:LX/1dN;

    if-eqz v0, :cond_1

    .line 286058
    monitor-enter p0

    .line 286059
    :try_start_1
    iget-object v0, p0, LX/1dV;->w:LX/1dN;

    .line 286060
    const/4 v1, 0x0

    iput-object v1, p0, LX/1dV;->w:LX/1dN;

    .line 286061
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 286062
    invoke-virtual {v0}, LX/1dN;->l()V

    .line 286063
    :cond_1
    iget-object v1, p0, LX/1dV;->z:Ljava/lang/Object;

    iget-object v2, p0, LX/1dV;->k:LX/1De;

    iget-boolean v6, p0, LX/1dV;->n:Z

    move-object v0, p0

    move v4, p1

    move v5, p2

    invoke-static/range {v0 .. v7}, LX/1dV;->a(LX/1dV;Ljava/lang/Object;LX/1De;LX/1X1;IIZLX/1ml;)LX/1dN;

    move-result-object v0

    .line 286064
    invoke-virtual {v0}, LX/1dN;->n()LX/1mg;

    move-result-object v1

    .line 286065
    monitor-enter p0

    .line 286066
    if-eqz v1, :cond_2

    .line 286067
    :try_start_2
    iget-object v2, p0, LX/1dV;->y:LX/1mg;

    invoke-virtual {v2, v1}, LX/1mg;->c(LX/1mg;)V

    .line 286068
    invoke-static {v1}, LX/1cy;->b(LX/1mg;)V

    .line 286069
    :cond_2
    iput-object v0, p0, LX/1dV;->w:LX/1dN;

    .line 286070
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 286071
    iget-object v0, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->m()V

    .line 286072
    :cond_3
    iget-object v0, p0, LX/1dV;->w:LX/1dN;

    .line 286073
    iget v1, v0, LX/1dN;->r:I

    move v0, v1

    .line 286074
    aput v0, p3, v9

    .line 286075
    iget-object v0, p0, LX/1dV;->w:LX/1dN;

    .line 286076
    iget v1, v0, LX/1dN;->s:I

    move v0, v1

    .line 286077
    aput v0, p3, v8

    .line 286078
    monitor-enter p0

    .line 286079
    const/4 v0, 0x0

    :try_start_3
    iput-boolean v0, p0, LX/1dV;->A:Z

    .line 286080
    iget v0, p0, LX/1dV;->B:I

    if-eqz v0, :cond_6

    .line 286081
    iget v0, p0, LX/1dV;->B:I

    .line 286082
    const/4 v1, 0x0

    iput v1, p0, LX/1dV;->B:I

    .line 286083
    iget-object v1, p0, LX/1dV;->t:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v3

    .line 286084
    :goto_1
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 286085
    if-eqz v0, :cond_4

    .line 286086
    if-ne v0, v8, :cond_5

    move v6, v8

    :goto_2
    move-object v2, p0

    move v4, v10

    move v5, v10

    invoke-static/range {v2 .. v7}, LX/1dV;->a(LX/1dV;LX/1X1;IIZLX/1no;)V

    .line 286087
    :cond_4
    return-void

    .line 286088
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 286089
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 286090
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    .line 286091
    :catchall_3
    move-exception v0

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0

    :cond_5
    move v6, v9

    .line 286092
    goto :goto_2

    :cond_6
    move-object v3, v7

    move v0, v9

    goto :goto_1

    :cond_7
    move-object v3, v7

    goto :goto_0
.end method

.method public final a(LX/1X1;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, -0x1

    .line 286093
    if-nez p1, :cond_0

    .line 286094
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Root component can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286095
    :cond_0
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    invoke-static/range {v0 .. v5}, LX/1dV;->a(LX/1dV;LX/1X1;IIZLX/1no;)V

    .line 286096
    return-void
.end method

.method public final a(LX/1X1;II)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;II)V"
        }
    .end annotation

    .prologue
    .line 286097
    if-nez p1, :cond_0

    .line 286098
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Root component can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286099
    :cond_0
    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-static/range {v0 .. v5}, LX/1dV;->a(LX/1dV;LX/1X1;IIZLX/1no;)V

    .line 286100
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 286101
    invoke-static {}, LX/1dS;->b()V

    .line 286102
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1dV;->l:Z

    .line 286103
    iget-object v0, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    iget-object v1, p0, LX/1dV;->w:LX/1dN;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/components/ComponentView;->a(LX/1dN;Landroid/graphics/Rect;)V

    .line 286104
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1dV;->l:Z

    .line 286105
    return-void
.end method

.method public final a(Lcom/facebook/components/ComponentView;)V
    .locals 4
    .param p1    # Lcom/facebook/components/ComponentView;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 286106
    invoke-static {}, LX/1dS;->b()V

    .line 286107
    iget-boolean v0, p0, LX/1dV;->o:Z

    if-eqz v0, :cond_2

    .line 286108
    iget-object v0, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    if-eqz v0, :cond_1

    .line 286109
    iget-object v0, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 286110
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/components/ComponentView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/1dV;->k:LX/1De;

    .line 286111
    invoke-static {v0}, LX/1dV;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v1}, LX/1dV;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v3

    if-ne v2, v3, :cond_4

    const/4 v2, 0x1

    :goto_1
    move v0, v2

    .line 286112
    if-nez v0, :cond_3

    .line 286113
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Base view context differs, view context is: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/components/ComponentView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ComponentTree context is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/1dV;->k:LX/1De;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286114
    :cond_1
    invoke-virtual {p0}, LX/1dV;->d()V

    goto :goto_0

    .line 286115
    :cond_2
    iget-object v0, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    if-eqz v0, :cond_0

    .line 286116
    iget-object v0, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->i()V

    goto :goto_0

    .line 286117
    :cond_3
    iput-object p1, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    .line 286118
    return-void

    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final b(II)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 286119
    const/4 v4, 0x1

    move-object v0, p0

    move v2, p1

    move v3, p2

    move-object v5, v1

    invoke-static/range {v0 .. v5}, LX/1dV;->a(LX/1dV;LX/1X1;IIZLX/1no;)V

    .line 286120
    return-void
.end method

.method public final b(LX/1X1;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, -0x1

    .line 286121
    if-nez p1, :cond_0

    .line 286122
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Root component can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286123
    :cond_0
    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    invoke-static/range {v0 .. v5}, LX/1dV;->a(LX/1dV;LX/1X1;IIZLX/1no;)V

    .line 286124
    return-void
.end method

.method public final b(LX/1X1;II)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;II)V"
        }
    .end annotation

    .prologue
    .line 286125
    if-nez p1, :cond_0

    .line 286126
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Root component can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286127
    :cond_0
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-static/range {v0 .. v5}, LX/1dV;->a(LX/1dV;LX/1X1;IIZLX/1no;)V

    .line 286128
    return-void
.end method

.method public final b(Ljava/lang/String;LX/48B;)V
    .locals 2

    .prologue
    .line 286129
    iget-boolean v0, p0, LX/1dV;->p:Z

    if-nez v0, :cond_0

    .line 286130
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Triggering async state updates on this component tree is disabled, use sync state updates."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286131
    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, LX/1dV;->a(LX/1dV;Ljava/lang/String;LX/48B;Z)V

    .line 286132
    return-void
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 286133
    invoke-static {}, LX/1dS;->b()V

    .line 286134
    iget-boolean v0, p0, LX/1dV;->m:Z

    if-nez v0, :cond_0

    .line 286135
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Calling incrementalMountComponent() but incremental mount is not enabled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286136
    :cond_0
    sget-object v0, LX/1dV;->h:Landroid/graphics/Rect;

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 286137
    invoke-static {}, LX/1dS;->b()V

    .line 286138
    iget-object v1, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    sget-object v4, LX/1dV;->f:[I

    invoke-static {v1, v4, v0}, LX/1dV;->a(Landroid/view/View;[ILandroid/graphics/Rect;)V

    .line 286139
    iget-object v1, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    invoke-virtual {v1}, Lcom/facebook/components/ComponentView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 286140
    instance-of v4, v1, Landroid/view/View;

    if-eqz v4, :cond_2

    .line 286141
    check-cast v1, Landroid/view/View;

    .line 286142
    sget-object v4, LX/1dV;->g:[I

    sget-object v5, LX/1dV;->i:Landroid/graphics/Rect;

    invoke-static {v1, v4, v5}, LX/1dV;->a(Landroid/view/View;[ILandroid/graphics/Rect;)V

    .line 286143
    sget-object v1, LX/1dV;->h:Landroid/graphics/Rect;

    sget-object v4, LX/1dV;->h:Landroid/graphics/Rect;

    sget-object v5, LX/1dV;->i:Landroid/graphics/Rect;

    invoke-virtual {v1, v4, v5}, Landroid/graphics/Rect;->setIntersect(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    .line 286144
    :goto_0
    move v0, v1

    .line 286145
    if-eqz v0, :cond_1

    .line 286146
    sget-object v0, LX/1dV;->h:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, LX/1dV;->a(Landroid/graphics/Rect;)V

    .line 286147
    :cond_1
    return-void

    .line 286148
    :cond_2
    sget-object v1, LX/1dV;->f:[I

    aget v1, v1, v2

    neg-int v1, v1

    sget-object v2, LX/1dV;->f:[I

    aget v2, v2, v3

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    move v1, v3

    .line 286149
    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 286150
    invoke-static {}, LX/1dS;->b()V

    .line 286151
    monitor-enter p0

    .line 286152
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/1dV;->o:Z

    .line 286153
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1dV;->s:Z

    .line 286154
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 286155
    invoke-static {}, LX/1dS;->b()V

    .line 286156
    iget-boolean v0, p0, LX/1dV;->o:Z

    if-eqz v0, :cond_0

    .line 286157
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Clearing the ComponentView while the ComponentTree is attached"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286158
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    .line 286159
    return-void
.end method

.method public getComponentView()Lcom/facebook/components/ComponentView;
    .locals 1
    .annotation build Landroid/support/annotation/Keep;
    .end annotation

    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 286160
    invoke-static {}, LX/1dS;->b()V

    .line 286161
    iget-object v0, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    return-object v0
.end method

.method public final h()V
    .locals 12

    .prologue
    .line 286162
    invoke-static {}, LX/1dS;->b()V

    .line 286163
    iget-object v0, p0, LX/1dV;->w:LX/1dN;

    if-eqz v0, :cond_5

    .line 286164
    iget-object v0, p0, LX/1dV;->w:LX/1dN;

    invoke-virtual {v0}, LX/1dN;->m()LX/1dN;

    move-result-object v0

    .line 286165
    :goto_0
    iget-object v1, p0, LX/1dV;->k:LX/1De;

    .line 286166
    iget-object v2, v1, LX/1De;->d:LX/1cp;

    move-object v1, v2

    .line 286167
    if-eqz v1, :cond_0

    .line 286168
    const/16 v2, 0x9

    const-string v3, "log_tag"

    iget-object v4, p0, LX/1dV;->k:LX/1De;

    .line 286169
    iget-object v5, v4, LX/1De;->c:Ljava/lang/String;

    move-object v4, v5

    .line 286170
    invoke-virtual {v1, v2, p0, v3, v4}, LX/1cp;->a(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 286171
    :cond_0
    const/4 v2, 0x0

    .line 286172
    iget-object v1, v0, LX/1dN;->h:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/1dN;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 286173
    iget-object v1, v0, LX/1dN;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_3

    .line 286174
    iget-object v1, v0, LX/1dN;->h:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1dK;

    .line 286175
    iget-object v5, v1, LX/1dK;->b:LX/1X1;

    move-object v1, v5

    .line 286176
    invoke-static {v1}, LX/1X1;->c(LX/1X1;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 286177
    iget-object v5, v1, LX/1X1;->e:LX/1S3;

    move-object v5, v5

    .line 286178
    iget-boolean v1, v5, LX/1S3;->b:Z

    move v1, v1

    .line 286179
    if-nez v1, :cond_2

    .line 286180
    invoke-virtual {v5}, LX/1S3;->n()I

    move-result v6

    move v1, v2

    .line 286181
    :goto_2
    if-ge v1, v6, :cond_1

    iget-object v7, v0, LX/1dN;->c:LX/1De;

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 286182
    invoke-virtual {v5}, LX/1S3;->n()I

    move-result v8

    if-nez v8, :cond_7

    move v8, v9

    .line 286183
    :goto_3
    move v7, v8

    .line 286184
    if-eqz v7, :cond_1

    .line 286185
    iget-object v7, v0, LX/1dN;->c:LX/1De;

    iget-object v8, v0, LX/1dN;->c:LX/1De;

    invoke-virtual {v5, v8}, LX/1S3;->a(LX/1De;)Ljava/lang/Object;

    move-result-object v8

    invoke-static {v7, v5, v8}, LX/1cy;->a(Landroid/content/Context;LX/1S3;Ljava/lang/Object;)V

    .line 286186
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 286187
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, v5, LX/1S3;->b:Z

    .line 286188
    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 286189
    :cond_3
    iget-object v1, p0, LX/1dV;->k:LX/1De;

    .line 286190
    iget-object v2, v1, LX/1De;->d:LX/1cp;

    move-object v1, v2

    .line 286191
    if-eqz v1, :cond_4

    .line 286192
    const/16 v2, 0x9

    const/16 v3, 0x10

    invoke-virtual {v1, v2, p0, v3}, LX/1cp;->a(ILjava/lang/Object;I)V

    .line 286193
    :cond_4
    invoke-virtual {v0}, LX/1dN;->l()V

    .line 286194
    :goto_4
    return-void

    .line 286195
    :cond_5
    monitor-enter p0

    .line 286196
    :try_start_0
    iget-object v0, p0, LX/1dV;->x:LX/1dN;

    .line 286197
    if-nez v0, :cond_6

    .line 286198
    monitor-exit p0

    goto :goto_4

    .line 286199
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 286200
    :cond_6
    :try_start_1
    invoke-virtual {v0}, LX/1dN;->m()LX/1dN;

    .line 286201
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 286202
    :cond_7
    sget-object v8, LX/1cy;->i:Ljava/util/Map;

    invoke-interface {v8, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/util/SparseArray;

    .line 286203
    if-nez v8, :cond_8

    move v8, v10

    .line 286204
    goto :goto_3

    .line 286205
    :cond_8
    iget v11, v5, LX/1S3;->d:I

    move v11, v11

    .line 286206
    invoke-virtual {v8, v11}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/34V;

    .line 286207
    if-eqz v8, :cond_9

    .line 286208
    iget v11, v8, LX/34V;->a:I

    iget v7, v8, LX/34V;->b:I

    if-lt v11, v7, :cond_b

    const/4 v11, 0x1

    :goto_5
    move v8, v11

    .line 286209
    if-nez v8, :cond_a

    :cond_9
    move v8, v10

    goto :goto_3

    :cond_a
    move v8, v9

    goto :goto_3

    :cond_b
    const/4 v11, 0x0

    goto :goto_5
.end method

.method public final declared-synchronized i()LX/1mg;
    .locals 1

    .prologue
    .line 286210
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1dV;->y:LX/1mg;

    invoke-static {v0}, LX/1cy;->a(LX/1mg;)LX/1mg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final j()V
    .locals 3

    .prologue
    .line 286211
    monitor-enter p0

    .line 286212
    :try_start_0
    iget-object v0, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    if-eqz v0, :cond_0

    .line 286213
    iget-object v0, p0, LX/1dV;->q:Lcom/facebook/components/ComponentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 286214
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/1dV;->t:LX/1X1;

    .line 286215
    iget-object v0, p0, LX/1dV;->w:LX/1dN;

    .line 286216
    const/4 v1, 0x0

    iput-object v1, p0, LX/1dV;->w:LX/1dN;

    .line 286217
    iget-object v1, p0, LX/1dV;->x:LX/1dN;

    .line 286218
    const/4 v2, 0x0

    iput-object v2, p0, LX/1dV;->x:LX/1dN;

    .line 286219
    iget-object v2, p0, LX/1dV;->y:LX/1mg;

    invoke-static {v2}, LX/1cy;->b(LX/1mg;)V

    .line 286220
    const/4 v2, 0x0

    iput-object v2, p0, LX/1dV;->y:LX/1mg;

    .line 286221
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286222
    if-eqz v0, :cond_1

    .line 286223
    invoke-virtual {v0}, LX/1dN;->l()V

    .line 286224
    :cond_1
    if-eqz v1, :cond_2

    .line 286225
    invoke-virtual {v1}, LX/1dN;->l()V

    .line 286226
    :cond_2
    return-void

    .line 286227
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
