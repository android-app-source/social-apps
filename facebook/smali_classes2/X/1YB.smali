.class public LX/1YB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 273355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 273356
    invoke-static {p0}, LX/1YB;->b(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 273357
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not identify a cache ID for object "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 273358
    :cond_0
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 273359
    :goto_0
    invoke-interface {v0}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 273360
    invoke-interface {v0}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v0

    .line 273361
    :goto_1
    return-object v0

    :cond_1
    move-object v0, p0

    .line 273362
    check-cast v0, LX/0jW;

    goto :goto_0

    .line 273363
    :cond_2
    invoke-static {v0}, LX/1YB;->c(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 273364
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 273365
    :cond_3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not identify a cache ID for object "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 273366
    instance-of v1, p0, LX/0jW;

    if-nez v1, :cond_1

    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    if-nez v1, :cond_1

    .line 273367
    :cond_0
    :goto_0
    return v0

    .line 273368
    :cond_1
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    if-eqz v1, :cond_2

    check-cast p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object p0

    .line 273369
    :goto_1
    if-eqz p0, :cond_0

    .line 273370
    invoke-interface {p0}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 273371
    const/4 v0, 0x1

    goto :goto_0

    .line 273372
    :cond_2
    check-cast p0, LX/0jW;

    goto :goto_1

    .line 273373
    :cond_3
    invoke-static {p0}, LX/1YB;->c(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private static c(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 273374
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v1, :cond_1

    .line 273375
    :cond_0
    :goto_0
    return v0

    .line 273376
    :cond_1
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 273377
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
