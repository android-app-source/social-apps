.class public final LX/1kn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/0Px",
        "<",
        "LX/1kK;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1kP;


# direct methods
.method public constructor <init>(LX/1kP;)V
    .locals 0

    .prologue
    .line 310207
    iput-object p1, p0, LX/1kn;->a:LX/1kP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 15

    .prologue
    .line 310208
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 310209
    iget-object v1, p0, LX/1kn;->a:LX/1kP;

    iget-object v1, v1, LX/1kP;->c:LX/1kT;

    invoke-virtual {v1}, LX/1kT;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 310210
    iget-object v1, p0, LX/1kn;->a:LX/1kP;

    .line 310211
    iget-object v6, v1, LX/1kP;->c:LX/1kT;

    .line 310212
    invoke-static {v6}, LX/1kT;->f(LX/1kT;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 310213
    invoke-static {v6}, LX/1kT;->e(LX/1kT;)V

    .line 310214
    :cond_0
    iget-object v8, v6, LX/1kT;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v9, LX/1kp;->q:LX/0Tn;

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 310215
    iget-object v8, v6, LX/1kT;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v10, LX/1kp;->r:LX/0Tn;

    const-wide/16 v12, 0x0

    invoke-interface {v8, v10, v12, v13}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 310216
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-wide/32 v12, 0x1b7740

    sub-long/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    .line 310217
    invoke-static {v9}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 310218
    new-instance v11, LX/2ry;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "448167148704580:"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, LX/2ry;-><init>(Ljava/lang/String;)V

    .line 310219
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    .line 310220
    iput-wide v12, v11, LX/2ry;->f:J

    .line 310221
    move-object v10, v11

    .line 310222
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    .line 310223
    iput-wide v12, v10, LX/2ry;->g:J

    .line 310224
    move-object v8, v10

    .line 310225
    sget-object v10, LX/1kT;->a:Ljava/lang/String;

    .line 310226
    iput-object v10, v8, LX/2ry;->p:Ljava/lang/String;

    .line 310227
    move-object v8, v8

    .line 310228
    const-string v10, "Clipboard Prompt"

    .line 310229
    iput-object v10, v8, LX/2ry;->b:Ljava/lang/String;

    .line 310230
    move-object v8, v8

    .line 310231
    iget-object v10, v6, LX/1kT;->d:Ljava/lang/String;

    .line 310232
    iput-object v10, v8, LX/2ry;->c:Ljava/lang/String;

    .line 310233
    move-object v10, v8

    .line 310234
    iget-object v8, v6, LX/1kT;->f:LX/1E1;

    invoke-virtual {v8}, LX/1E1;->c()Z

    move-result v8

    if-eqz v8, :cond_2

    move-object v8, v9

    .line 310235
    :goto_0
    iput-object v8, v10, LX/2ry;->d:Ljava/lang/String;

    .line 310236
    move-object v8, v10

    .line 310237
    iget-object v10, v6, LX/1kT;->c:Ljava/lang/String;

    .line 310238
    iput-object v10, v8, LX/2ry;->e:Ljava/lang/String;

    .line 310239
    move-object v8, v8

    .line 310240
    iput-object v9, v8, LX/2ry;->j:Ljava/lang/String;

    .line 310241
    move-object v8, v8

    .line 310242
    const-wide v12, 0x3fb999999999999aL    # 0.1

    .line 310243
    iput-wide v12, v8, LX/2ry;->x:D

    .line 310244
    move-object v8, v8

    .line 310245
    const-string v9, "ranking_unavailable"

    .line 310246
    iput-object v9, v8, LX/2ry;->y:Ljava/lang/String;

    .line 310247
    invoke-static {v11}, Lcom/facebook/productionprompts/model/ProductionPrompt;->a(LX/2ry;)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v8

    move-object v6, v8

    .line 310248
    if-eqz v6, :cond_1

    .line 310249
    new-instance v7, LX/1kV;

    invoke-direct {v7, v6}, LX/1kV;-><init>(Lcom/facebook/productionprompts/model/ProductionPrompt;)V

    invoke-virtual {v0, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 310250
    :cond_1
    iget-object v1, p0, LX/1kn;->a:LX/1kP;

    iget-object v1, v1, LX/1kQ;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x930001

    const/16 v3, 0xd

    const-string v4, "fetcher"

    const-class v5, LX/1kP;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISLjava/lang/String;Ljava/lang/String;)V

    .line 310251
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 310252
    :cond_2
    iget-object v8, v6, LX/1kT;->e:Ljava/lang/String;

    goto :goto_0
.end method
