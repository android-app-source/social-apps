.class public LX/1s9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/1sB;

.field public final b:LX/1sD;


# direct methods
.method public constructor <init>(LX/1sB;LX/1sD;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 333743
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333744
    iput-object p1, p0, LX/1s9;->a:LX/1sB;

    .line 333745
    iput-object p2, p0, LX/1s9;->b:LX/1sD;

    .line 333746
    return-void
.end method

.method public static a(LX/0QB;)LX/1s9;
    .locals 5

    .prologue
    .line 333747
    const-class v1, LX/1s9;

    monitor-enter v1

    .line 333748
    :try_start_0
    sget-object v0, LX/1s9;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 333749
    sput-object v2, LX/1s9;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 333750
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333751
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 333752
    new-instance p0, LX/1s9;

    invoke-static {v0}, LX/1sA;->a(LX/0QB;)LX/1sA;

    move-result-object v3

    check-cast v3, LX/1sB;

    invoke-static {v0}, LX/1sC;->a(LX/0QB;)LX/1sC;

    move-result-object v4

    check-cast v4, LX/1sD;

    invoke-direct {p0, v3, v4}, LX/1s9;-><init>(LX/1sB;LX/1sD;)V

    .line 333753
    move-object v0, p0

    .line 333754
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 333755
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1s9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 333756
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 333757
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;
    .locals 1

    .prologue
    .line 333758
    iget-object v0, p0, LX/1s9;->a:LX/1sB;

    invoke-interface {v0, p1}, LX/1sB;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v0

    .line 333759
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1s9;->b:LX/1sD;

    invoke-interface {v0, p1}, LX/1sD;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/9oK;)Z
    .locals 2
    .param p1    # LX/9oK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 333760
    if-eqz p1, :cond_1

    invoke-interface {p1}, LX/9oK;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1s9;->b:LX/1sD;

    invoke-interface {v0}, LX/1sD;->a()LX/0Px;

    move-result-object v0

    invoke-interface {p1}, LX/9oK;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1s9;->a:LX/1sB;

    invoke-interface {v0}, LX/1sB;->a()LX/0Px;

    move-result-object v0

    invoke-interface {p1}, LX/9oK;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
