.class public LX/1SC;
.super LX/1Cv;
.source ""

# interfaces
.implements LX/0Vf;


# instance fields
.field public final a:LX/1SD;

.field private final b:I

.field public c:LX/1Cw;

.field public d:Z


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/1SB;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p2    # LX/1SB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 247790
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 247791
    new-instance v0, LX/1SD;

    invoke-direct {v0, p0}, LX/1SD;-><init>(LX/1SC;)V

    iput-object v0, p0, LX/1SC;->a:LX/1SD;

    .line 247792
    new-instance v0, LX/1SE;

    invoke-direct {v0}, LX/1SE;-><init>()V

    iput-object v0, p0, LX/1SC;->c:LX/1Cw;

    .line 247793
    iput-boolean v1, p0, LX/1SC;->d:Z

    .line 247794
    const/4 v0, 0x1

    move v0, v0

    .line 247795
    iput v0, p0, LX/1SC;->b:I

    .line 247796
    iget-object v0, p2, LX/1SB;->a:LX/1D8;

    iget-object v0, v0, LX/1D8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, v0

    .line 247797
    new-instance v1, LX/1SN;

    invoke-direct {v1, p0, p2}, LX/1SN;-><init>(LX/1SC;LX/1SB;)V

    invoke-static {v0, v1, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 247798
    return-void
.end method

.method public static a(LX/1SC;)V
    .locals 2

    .prologue
    .line 247804
    iget-object v0, p0, LX/1SC;->c:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getViewTypeCount()I

    move-result v0

    iget v1, p0, LX/1SC;->b:I

    if-eq v0, v1, :cond_0

    .line 247805
    new-instance v0, LX/62D;

    invoke-direct {v0}, LX/62D;-><init>()V

    throw v0

    .line 247806
    :cond_0
    invoke-super {p0}, LX/1Cv;->notifyDataSetChanged()V

    .line 247807
    return-void
.end method

.method public static b(LX/1SC;)V
    .locals 0

    .prologue
    .line 247802
    invoke-super {p0}, LX/1Cv;->notifyDataSetInvalidated()V

    .line 247803
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 247801
    iget-object v0, p0, LX/1SC;->c:LX/1Cw;

    invoke-interface {v0, p1, p2}, LX/1Cw;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 6

    .prologue
    .line 247799
    iget-object v0, p0, LX/1SC;->c:LX/1Cw;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, LX/1Cw;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    .line 247800
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 247789
    iget-object v0, p0, LX/1SC;->c:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->areAllItemsEnabled()Z

    move-result v0

    return v0
.end method

.method public final dispose()V
    .locals 1

    .prologue
    .line 247784
    iget-boolean v0, p0, LX/1SC;->d:Z

    if-eqz v0, :cond_0

    .line 247785
    :goto_0
    return-void

    .line 247786
    :cond_0
    iget-object v0, p0, LX/1SC;->c:LX/1Cw;

    instance-of v0, v0, LX/0Vf;

    if-eqz v0, :cond_1

    .line 247787
    iget-object v0, p0, LX/1SC;->c:LX/1Cw;

    check-cast v0, LX/0Vf;

    invoke-interface {v0}, LX/0Vf;->dispose()V

    .line 247788
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1SC;->d:Z

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 247783
    iget-object v0, p0, LX/1SC;->c:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getCount()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 247808
    iget-object v0, p0, LX/1SC;->c:LX/1Cw;

    invoke-interface {v0, p1}, LX/1Cw;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 247782
    iget-object v0, p0, LX/1SC;->c:LX/1Cw;

    invoke-interface {v0, p1}, LX/1Cw;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 247781
    iget-object v0, p0, LX/1SC;->c:LX/1Cw;

    invoke-interface {v0, p1}, LX/1Cw;->getItemViewType(I)I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 247780
    iget-object v0, p0, LX/1SC;->c:LX/1Cw;

    invoke-interface {v0, p1, p2, p3}, LX/1Cw;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 247774
    iget v0, p0, LX/1SC;->b:I

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 247779
    iget-object v0, p0, LX/1SC;->c:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->hasStableIds()Z

    move-result v0

    return v0
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 247778
    iget-boolean v0, p0, LX/1SC;->d:Z

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 247777
    iget-object v0, p0, LX/1SC;->c:LX/1Cw;

    invoke-interface {v0, p1}, LX/1Cw;->isEnabled(I)Z

    move-result v0

    return v0
.end method

.method public final notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 247775
    iget-object v0, p0, LX/1SC;->c:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 247776
    return-void
.end method
