.class public LX/1Al;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 211108
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 211109
    return-void
.end method


# virtual methods
.method public final a(LX/1Gd;LX/1Gd;Landroid/net/Uri;Ljava/lang/String;LX/1bh;Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;)Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "LX/1bh;",
            "Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;",
            ")",
            "Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;"
        }
    .end annotation

    .prologue
    .line 211110
    new-instance v0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/1bq;->a(LX/0QB;)LX/1br;

    move-result-object v2

    check-cast v2, LX/1br;

    invoke-static {p0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/1c6;->a(LX/0QB;)LX/1Fh;

    move-result-object v4

    check-cast v4, LX/1Fh;

    invoke-static {p0}, LX/1bs;->a(LX/0QB;)LX/1c3;

    move-result-object v5

    check-cast v5, LX/1c3;

    invoke-static {p0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v6

    check-cast v6, LX/0yc;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {p0}, LX/1c5;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v8

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    move-object/from16 v14, p6

    invoke-direct/range {v0 .. v14}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;-><init>(Landroid/content/res/Resources;LX/1br;Ljava/util/concurrent/Executor;LX/1Fh;LX/1c3;LX/0yc;LX/0Zb;Ljava/util/Set;LX/1Gd;LX/1Gd;Landroid/net/Uri;Ljava/lang/String;LX/1bh;Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;)V

    .line 211111
    return-object v0
.end method
