.class public LX/16I;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/16I;


# instance fields
.field public final a:LX/0Xl;

.field public final b:LX/0kb;


# direct methods
.method public constructor <init>(LX/0Xl;LX/0kb;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 184770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184771
    iput-object p1, p0, LX/16I;->a:LX/0Xl;

    .line 184772
    iput-object p2, p0, LX/16I;->b:LX/0kb;

    .line 184773
    return-void
.end method

.method public static a(LX/0QB;)LX/16I;
    .locals 5

    .prologue
    .line 184774
    sget-object v0, LX/16I;->c:LX/16I;

    if-nez v0, :cond_1

    .line 184775
    const-class v1, LX/16I;

    monitor-enter v1

    .line 184776
    :try_start_0
    sget-object v0, LX/16I;->c:LX/16I;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 184777
    if-eqz v2, :cond_0

    .line 184778
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 184779
    new-instance p0, LX/16I;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-direct {p0, v3, v4}, LX/16I;-><init>(LX/0Xl;LX/0kb;)V

    .line 184780
    move-object v0, p0

    .line 184781
    sput-object v0, LX/16I;->c:LX/16I;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 184782
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 184783
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 184784
    :cond_1
    sget-object v0, LX/16I;->c:LX/16I;

    return-object v0

    .line 184785
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 184786
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;
    .locals 3

    .prologue
    .line 184787
    iget-object v0, p0, LX/16I;->a:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    new-instance v2, LX/1Ee;

    invoke-direct {v2, p0, p1, p2}, LX/1Ee;-><init>(LX/16I;LX/1Ed;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    .line 184788
    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 184789
    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 184790
    iget-object v0, p0, LX/16I;->b:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    return v0
.end method
