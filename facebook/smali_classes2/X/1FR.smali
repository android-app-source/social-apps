.class public abstract LX/1FR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1FS;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1FS",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0rb;

.field public final b:LX/1F7;

.field public final c:Landroid/util/SparseArray;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/1FU",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Set;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation
.end field

.field public final e:LX/1FV;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final f:LX/1FV;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final g:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private h:Z

.field private final i:LX/1F0;


# direct methods
.method public constructor <init>(LX/0rb;LX/1F7;LX/1F0;)V
    .locals 2

    .prologue
    .line 222451
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222452
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, LX/1FR;->g:Ljava/lang/Class;

    .line 222453
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rb;

    iput-object v0, p0, LX/1FR;->a:LX/0rb;

    .line 222454
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1F7;

    iput-object v0, p0, LX/1FR;->b:LX/1F7;

    .line 222455
    invoke-static {p3}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1F0;

    iput-object v0, p0, LX/1FR;->i:LX/1F0;

    .line 222456
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/1FR;->c:Landroid/util/SparseArray;

    .line 222457
    new-instance v0, Landroid/util/SparseIntArray;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    invoke-static {p0, v0}, LX/1FR;->a(LX/1FR;Landroid/util/SparseIntArray;)V

    .line 222458
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    .line 222459
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v1

    move-object v0, v1

    .line 222460
    move-object v0, v0

    .line 222461
    iput-object v0, p0, LX/1FR;->d:Ljava/util/Set;

    .line 222462
    new-instance v0, LX/1FV;

    invoke-direct {v0}, LX/1FV;-><init>()V

    iput-object v0, p0, LX/1FR;->f:LX/1FV;

    .line 222463
    new-instance v0, LX/1FV;

    invoke-direct {v0}, LX/1FV;-><init>()V

    iput-object v0, p0, LX/1FR;->e:LX/1FV;

    .line 222464
    return-void
.end method

.method private static declared-synchronized a(LX/1FR;Landroid/util/SparseIntArray;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 222465
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 222466
    iget-object v1, p0, LX/1FR;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    .line 222467
    iget-object v1, p0, LX/1FR;->b:LX/1F7;

    iget-object v1, v1, LX/1F7;->c:Landroid/util/SparseIntArray;

    .line 222468
    if-eqz v1, :cond_1

    .line 222469
    :goto_0
    invoke-virtual {v1}, Landroid/util/SparseIntArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 222470
    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    .line 222471
    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v3

    .line 222472
    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4}, Landroid/util/SparseIntArray;->get(II)I

    move-result v4

    .line 222473
    iget-object v5, p0, LX/1FR;->c:Landroid/util/SparseArray;

    new-instance v6, LX/1FU;

    invoke-virtual {p0, v2}, LX/1FR;->d(I)I

    move-result v7

    invoke-direct {v6, v7, v3, v4}, LX/1FU;-><init>(III)V

    invoke-virtual {v5, v2, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 222474
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 222475
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1FR;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222476
    :goto_1
    monitor-exit p0

    return-void

    .line 222477
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/1FR;->h:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 222478
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c()V
    .locals 1

    .prologue
    .line 222479
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/1FR;->f(LX/1FR;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1FR;->f:LX/1FV;

    iget v0, v0, LX/1FV;->b:I

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222480
    monitor-exit p0

    return-void

    .line 222481
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private d()V
    .locals 7
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 222374
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, LX/1FR;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 222375
    new-instance v4, Landroid/util/SparseIntArray;

    invoke-direct {v4}, Landroid/util/SparseIntArray;-><init>()V

    .line 222376
    monitor-enter p0

    move v2, v1

    .line 222377
    :goto_0
    :try_start_0
    iget-object v0, p0, LX/1FR;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 222378
    iget-object v0, p0, LX/1FR;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FU;

    .line 222379
    invoke-virtual {v0}, LX/1FU;->b()I

    move-result v5

    if-lez v5, :cond_0

    .line 222380
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222381
    :cond_0
    iget-object v5, p0, LX/1FR;->c:Landroid/util/SparseArray;

    invoke-virtual {v5, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    .line 222382
    iget v6, v0, LX/1FU;->d:I

    move v0, v6

    .line 222383
    invoke-virtual {v4, v5, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 222384
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 222385
    :cond_1
    invoke-static {p0, v4}, LX/1FR;->a(LX/1FR;Landroid/util/SparseIntArray;)V

    .line 222386
    iget-object v0, p0, LX/1FR;->f:LX/1FV;

    const/4 v2, 0x0

    .line 222387
    iput v2, v0, LX/1FV;->a:I

    .line 222388
    iput v2, v0, LX/1FV;->b:I

    .line 222389
    invoke-static {}, LX/1FR;->g()V

    .line 222390
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222391
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 222392
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FU;

    .line 222393
    :goto_2
    invoke-virtual {v0}, LX/1FU;->d()Ljava/lang/Object;

    move-result-object v2

    .line 222394
    if-eqz v2, :cond_2

    .line 222395
    invoke-virtual {p0, v2}, LX/1FR;->b(Ljava/lang/Object;)V

    goto :goto_2

    .line 222396
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 222397
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 222398
    :cond_3
    return-void
.end method

.method private declared-synchronized e()V
    .locals 1
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 222482
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/1FR;->f(LX/1FR;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222483
    iget-object v0, p0, LX/1FR;->b:LX/1F7;

    iget v0, v0, LX/1F7;->b:I

    invoke-static {p0, v0}, LX/1FR;->f(LX/1FR;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222484
    :cond_0
    monitor-exit p0

    return-void

    .line 222485
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized f(LX/1FR;I)V
    .locals 5
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 222511
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1FR;->e:LX/1FV;

    iget v0, v0, LX/1FV;->b:I

    iget-object v1, p0, LX/1FR;->f:LX/1FV;

    iget v1, v1, LX/1FV;->b:I

    add-int/2addr v0, v1

    sub-int/2addr v0, p1

    iget-object v1, p0, LX/1FR;->f:LX/1FV;

    iget v1, v1, LX/1FV;->b:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 222512
    if-gtz v1, :cond_0

    .line 222513
    :goto_0
    monitor-exit p0

    return-void

    .line 222514
    :cond_0
    const/4 v0, 0x2

    :try_start_1
    invoke-static {v0}, LX/03J;->a(I)Z

    .line 222515
    invoke-static {}, LX/1FR;->g()V

    .line 222516
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    iget-object v0, p0, LX/1FR;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 222517
    if-lez v1, :cond_2

    .line 222518
    iget-object v0, p0, LX/1FR;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FU;

    .line 222519
    :goto_2
    if-lez v1, :cond_1

    .line 222520
    invoke-virtual {v0}, LX/1FU;->d()Ljava/lang/Object;

    move-result-object v3

    .line 222521
    if-eqz v3, :cond_1

    .line 222522
    invoke-virtual {p0, v3}, LX/1FR;->b(Ljava/lang/Object;)V

    .line 222523
    iget v3, v0, LX/1FU;->a:I

    sub-int/2addr v1, v3

    .line 222524
    iget-object v3, p0, LX/1FR;->f:LX/1FV;

    iget v4, v0, LX/1FU;->a:I

    invoke-virtual {v3, v4}, LX/1FV;->b(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 222525
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 222526
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 222527
    :cond_2
    :try_start_2
    invoke-static {}, LX/1FR;->g()V

    .line 222528
    const/4 v0, 0x2

    invoke-static {v0}, LX/03J;->a(I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private static declared-synchronized f(LX/1FR;)Z
    .locals 2
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 222486
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1FR;->e:LX/1FV;

    iget v0, v0, LX/1FV;->b:I

    iget-object v1, p0, LX/1FR;->f:LX/1FV;

    iget v1, v1, LX/1FV;->b:I

    add-int/2addr v0, v1

    iget-object v1, p0, LX/1FR;->b:LX/1F7;

    iget v1, v1, LX/1F7;->b:I

    if-le v0, v1, :cond_1

    const/4 v0, 0x1

    .line 222487
    :goto_0
    if-eqz v0, :cond_0

    .line 222488
    iget-object v1, p0, LX/1FR;->i:LX/1F0;

    invoke-interface {v1}, LX/1F0;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222489
    :cond_0
    monitor-exit p0

    return v0

    .line 222490
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized g(I)LX/1FU;
    .locals 2
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1FU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 222491
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1FR;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FU;

    .line 222492
    if-nez v0, :cond_0

    iget-boolean v1, p0, LX/1FR;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 222493
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 222494
    :cond_1
    const/4 v0, 0x2

    :try_start_1
    invoke-static {v0}, LX/03J;->a(I)Z

    .line 222495
    invoke-virtual {p0, p1}, LX/1FR;->e(I)LX/1FU;

    move-result-object v0

    .line 222496
    iget-object v1, p0, LX/1FR;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 222497
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static g()V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InvalidAccessToGuardedField"
        }
    .end annotation

    .prologue
    .line 222498
    const/4 v0, 0x2

    invoke-static {v0}, LX/03J;->a(I)Z

    .line 222499
    return-void
.end method

.method private declared-synchronized h(I)Z
    .locals 5
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 222500
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/1FR;->b:LX/1F7;

    iget v1, v1, LX/1F7;->a:I

    .line 222501
    iget-object v2, p0, LX/1FR;->e:LX/1FV;

    iget v2, v2, LX/1FV;->b:I

    sub-int v2, v1, v2

    if-le p1, v2, :cond_0

    .line 222502
    iget-object v1, p0, LX/1FR;->i:LX/1F0;

    invoke-interface {v1}, LX/1F0;->V_()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222503
    :goto_0
    monitor-exit p0

    return v0

    .line 222504
    :cond_0
    :try_start_1
    iget-object v2, p0, LX/1FR;->b:LX/1F7;

    iget v2, v2, LX/1F7;->b:I

    .line 222505
    iget-object v3, p0, LX/1FR;->e:LX/1FV;

    iget v3, v3, LX/1FV;->b:I

    iget-object v4, p0, LX/1FR;->f:LX/1FV;

    iget v4, v4, LX/1FV;->b:I

    add-int/2addr v3, v4

    sub-int v3, v2, v3

    if-le p1, v3, :cond_1

    .line 222506
    sub-int/2addr v2, p1

    invoke-static {p0, v2}, LX/1FR;->f(LX/1FR;I)V

    .line 222507
    :cond_1
    iget-object v2, p0, LX/1FR;->e:LX/1FV;

    iget v2, v2, LX/1FV;->b:I

    iget-object v3, p0, LX/1FR;->f:LX/1FV;

    iget v3, v3, LX/1FV;->b:I

    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    if-le p1, v1, :cond_2

    .line 222508
    iget-object v1, p0, LX/1FR;->i:LX/1F0;

    invoke-interface {v1}, LX/1F0;->V_()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 222509
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 222510
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TV;"
        }
    .end annotation

    .prologue
    .line 222402
    invoke-direct {p0}, LX/1FR;->c()V

    .line 222403
    invoke-virtual {p0, p1}, LX/1FR;->c(I)I

    move-result v1

    .line 222404
    monitor-enter p0

    .line 222405
    :try_start_0
    invoke-direct {p0, v1}, LX/1FR;->g(I)LX/1FU;

    move-result-object v2

    .line 222406
    if-eqz v2, :cond_1

    .line 222407
    invoke-virtual {v2}, LX/1FU;->d()Ljava/lang/Object;

    move-result-object v0

    .line 222408
    if-eqz v0, :cond_0

    .line 222409
    iget v3, v2, LX/1FU;->d:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, LX/1FU;->d:I

    .line 222410
    :cond_0
    move-object v0, v0

    .line 222411
    if-eqz v0, :cond_1

    .line 222412
    iget-object v1, p0, LX/1FR;->d:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LX/03g;->b(Z)V

    .line 222413
    invoke-virtual {p0, v0}, LX/1FR;->c(Ljava/lang/Object;)I

    move-result v1

    .line 222414
    invoke-virtual {p0, v1}, LX/1FR;->d(I)I

    move-result v1

    .line 222415
    iget-object v2, p0, LX/1FR;->e:LX/1FV;

    invoke-virtual {v2, v1}, LX/1FV;->a(I)V

    .line 222416
    iget-object v2, p0, LX/1FR;->f:LX/1FV;

    invoke-virtual {v2, v1}, LX/1FV;->b(I)V

    .line 222417
    iget-object v2, p0, LX/1FR;->i:LX/1F0;

    invoke-interface {v2, v1}, LX/1F0;->a(I)V

    .line 222418
    invoke-static {}, LX/1FR;->g()V

    .line 222419
    const/4 v1, 0x2

    invoke-static {v1}, LX/03J;->a(I)Z

    .line 222420
    monitor-exit p0

    .line 222421
    :goto_0
    return-object v0

    .line 222422
    :cond_1
    invoke-virtual {p0, v1}, LX/1FR;->d(I)I

    move-result v3

    .line 222423
    invoke-direct {p0, v3}, LX/1FR;->h(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 222424
    new-instance v0, LX/4eL;

    iget-object v1, p0, LX/1FR;->b:LX/1F7;

    iget v1, v1, LX/1F7;->a:I

    iget-object v2, p0, LX/1FR;->e:LX/1FV;

    iget v2, v2, LX/1FV;->b:I

    iget-object v4, p0, LX/1FR;->f:LX/1FV;

    iget v4, v4, LX/1FV;->b:I

    invoke-direct {v0, v1, v2, v4, v3}, LX/4eL;-><init>(IIII)V

    throw v0

    .line 222425
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 222426
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/1FR;->e:LX/1FV;

    invoke-virtual {v0, v3}, LX/1FV;->a(I)V

    .line 222427
    if-eqz v2, :cond_3

    .line 222428
    iget v0, v2, LX/1FU;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, LX/1FU;->d:I

    .line 222429
    :cond_3
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 222430
    const/4 v0, 0x0

    .line 222431
    :try_start_2
    invoke-virtual {p0, v1}, LX/1FR;->b(I)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    .line 222432
    :goto_1
    monitor-enter p0

    .line 222433
    :try_start_3
    iget-object v1, p0, LX/1FR;->d:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LX/03g;->b(Z)V

    .line 222434
    invoke-direct {p0}, LX/1FR;->e()V

    .line 222435
    iget-object v1, p0, LX/1FR;->i:LX/1F0;

    invoke-interface {v1, v3}, LX/1F0;->b(I)V

    .line 222436
    invoke-static {}, LX/1FR;->g()V

    .line 222437
    const/4 v1, 0x2

    invoke-static {v1}, LX/03J;->a(I)Z

    .line 222438
    monitor-exit p0

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 222439
    :catch_0
    move-exception v2

    .line 222440
    monitor-enter p0

    .line 222441
    :try_start_4
    iget-object v4, p0, LX/1FR;->e:LX/1FV;

    invoke-virtual {v4, v3}, LX/1FV;->b(I)V

    .line 222442
    invoke-direct {p0, v1}, LX/1FR;->g(I)LX/1FU;

    move-result-object v1

    .line 222443
    if-eqz v1, :cond_4

    .line 222444
    invoke-virtual {v1}, LX/1FU;->f()V

    .line 222445
    :cond_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 222446
    invoke-static {v2}, LX/0V9;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 222447
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 222448
    iget-object v0, p0, LX/1FR;->a:LX/0rb;

    invoke-interface {v0, p0}, LX/0rb;->a(LX/0rf;)V

    .line 222449
    iget-object v0, p0, LX/1FR;->i:LX/1F0;

    invoke-interface {v0, p0}, LX/1F0;->a(LX/1FR;)V

    .line 222450
    return-void
.end method

.method public final a(LX/32G;)V
    .locals 0

    .prologue
    .line 222324
    invoke-direct {p0}, LX/1FR;->d()V

    .line 222325
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 222326
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 222327
    invoke-virtual {p0, p1}, LX/1FR;->c(Ljava/lang/Object;)I

    move-result v0

    .line 222328
    invoke-virtual {p0, v0}, LX/1FR;->d(I)I

    move-result v1

    .line 222329
    monitor-enter p0

    .line 222330
    :try_start_0
    invoke-direct {p0, v0}, LX/1FR;->g(I)LX/1FU;

    move-result-object v2

    .line 222331
    iget-object v3, p0, LX/1FR;->d:Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 222332
    iget-object v2, p0, LX/1FR;->g:Ljava/lang/Class;

    const-string v3, "release (free, value unrecognized) (object, size) = (%x, %s)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    .line 222333
    sget-object v0, LX/03J;->a:LX/03G;

    const/4 v5, 0x6

    invoke-interface {v0, v5}, LX/03G;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222334
    sget-object v0, LX/03J;->a:LX/03G;

    invoke-static {v2}, LX/03J;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4}, LX/03J;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v5, v6}, LX/03G;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 222335
    :cond_0
    invoke-virtual {p0, p1}, LX/1FR;->b(Ljava/lang/Object;)V

    .line 222336
    iget-object v0, p0, LX/1FR;->i:LX/1F0;

    invoke-interface {v0, v1}, LX/1F0;->c(I)V

    .line 222337
    :goto_0
    invoke-static {}, LX/1FR;->g()V

    .line 222338
    monitor-exit p0

    return-void

    .line 222339
    :cond_1
    if-eqz v2, :cond_2

    .line 222340
    iget v0, v2, LX/1FU;->d:I

    invoke-virtual {v2}, LX/1FU;->b()I

    move-result v3

    add-int/2addr v0, v3

    iget v3, v2, LX/1FU;->b:I

    if-le v0, v3, :cond_5

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 222341
    if-nez v0, :cond_2

    invoke-static {p0}, LX/1FR;->f(LX/1FR;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0, p1}, LX/1FR;->d(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 222342
    :cond_2
    if-eqz v2, :cond_3

    .line 222343
    invoke-virtual {v2}, LX/1FU;->f()V

    .line 222344
    :cond_3
    const/4 v0, 0x2

    invoke-static {v0}, LX/03J;->a(I)Z

    .line 222345
    invoke-virtual {p0, p1}, LX/1FR;->b(Ljava/lang/Object;)V

    .line 222346
    iget-object v0, p0, LX/1FR;->e:LX/1FV;

    invoke-virtual {v0, v1}, LX/1FV;->b(I)V

    .line 222347
    iget-object v0, p0, LX/1FR;->i:LX/1F0;

    invoke-interface {v0, v1}, LX/1F0;->c(I)V

    goto :goto_0

    .line 222348
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 222349
    :cond_4
    :try_start_1
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 222350
    iget v0, v2, LX/1FU;->d:I

    if-lez v0, :cond_6

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 222351
    iget v0, v2, LX/1FU;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v2, LX/1FU;->d:I

    .line 222352
    invoke-virtual {v2, p1}, LX/1FU;->b(Ljava/lang/Object;)V

    .line 222353
    iget-object v0, p0, LX/1FR;->f:LX/1FV;

    invoke-virtual {v0, v1}, LX/1FV;->a(I)V

    .line 222354
    iget-object v0, p0, LX/1FR;->e:LX/1FV;

    invoke-virtual {v0, v1}, LX/1FV;->b(I)V

    .line 222355
    iget-object v0, p0, LX/1FR;->i:LX/1F0;

    invoke-interface {v0, v1}, LX/1F0;->d(I)V

    .line 222356
    const/4 v0, 0x2

    invoke-static {v0}, LX/03J;->a(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_5
    :try_start_2
    const/4 v0, 0x0

    goto :goto_1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 222357
    :cond_6
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public abstract b(I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TV;"
        }
    .end annotation
.end method

.method public final declared-synchronized b()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222358
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 222359
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/1FR;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 222360
    iget-object v0, p0, LX/1FR;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    .line 222361
    iget-object v0, p0, LX/1FR;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FU;

    .line 222362
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "buckets_used_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, LX/1FR;->d(I)I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 222363
    iget v4, v0, LX/1FU;->d:I

    move v0, v4

    .line 222364
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222365
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 222366
    :cond_0
    const-string v0, "soft_cap"

    iget-object v1, p0, LX/1FR;->b:LX/1F7;

    iget v1, v1, LX/1F7;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222367
    const-string v0, "hard_cap"

    iget-object v1, p0, LX/1FR;->b:LX/1F7;

    iget v1, v1, LX/1F7;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222368
    const-string v0, "used_count"

    iget-object v1, p0, LX/1FR;->e:LX/1FV;

    iget v1, v1, LX/1FV;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222369
    const-string v0, "used_bytes"

    iget-object v1, p0, LX/1FR;->e:LX/1FV;

    iget v1, v1, LX/1FV;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222370
    const-string v0, "free_count"

    iget-object v1, p0, LX/1FR;->f:LX/1FV;

    iget v1, v1, LX/1FV;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222371
    const-string v0, "free_bytes"

    iget-object v1, p0, LX/1FR;->f:LX/1FV;

    iget v1, v1, LX/1FV;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222372
    monitor-exit p0

    return-object v2

    .line 222373
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract b(Ljava/lang/Object;)V
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation
.end method

.method public abstract c(I)I
.end method

.method public abstract c(Ljava/lang/Object;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)I"
        }
    .end annotation
.end method

.method public abstract d(I)I
.end method

.method public d(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)Z"
        }
    .end annotation

    .prologue
    .line 222399
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 222400
    const/4 v0, 0x1

    return v0
.end method

.method public e(I)LX/1FU;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1FU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 222401
    new-instance v0, LX/1FU;

    invoke-virtual {p0, p1}, LX/1FR;->d(I)I

    move-result v1

    const v2, 0x7fffffff

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LX/1FU;-><init>(III)V

    return-object v0
.end method
