.class public abstract LX/1jo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1jp;


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/1jm;I)V
    .locals 2

    .prologue
    .line 300890
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 300891
    const/16 v0, 0xa

    iput v0, p0, LX/1jo;->a:I

    .line 300892
    const-string v0, "unknown"

    iput-object v0, p0, LX/1jo;->b:Ljava/lang/String;

    .line 300893
    const-string v0, "unknown"

    iput-object v0, p0, LX/1jo;->c:Ljava/lang/String;

    .line 300894
    if-nez p1, :cond_0

    .line 300895
    new-instance v0, LX/5MH;

    const-string v1, "Bad config"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300896
    :cond_0
    iget-object v0, p1, LX/1jm;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p2, :cond_1

    .line 300897
    new-instance v0, LX/5MH;

    const-string v1, "Unsupported config version"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300898
    :cond_1
    iget-object v0, p1, LX/1jm;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    iget-object v0, p1, LX/1jm;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_2

    .line 300899
    iget-object v0, p1, LX/1jm;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/1jo;->a:I

    .line 300900
    :cond_2
    iget-object v0, p1, LX/1jm;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 300901
    iget-object v0, p1, LX/1jm;->e:Ljava/lang/String;

    iput-object v0, p0, LX/1jo;->b:Ljava/lang/String;

    .line 300902
    :cond_3
    iget-object v0, p1, LX/1jm;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 300903
    iget-object v0, p1, LX/1jm;->a:Ljava/lang/String;

    iput-object v0, p0, LX/1jo;->c:Ljava/lang/String;

    .line 300904
    :cond_4
    return-void
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 300905
    iget v0, p0, LX/1jo;->a:I

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 300906
    iget-object v0, p0, LX/1jo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 300907
    iget-object v0, p0, LX/1jo;->c:Ljava/lang/String;

    return-object v0
.end method
