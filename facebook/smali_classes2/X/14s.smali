.class public LX/14s;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public mAttachmentMediaCacheState:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attachment_media_cache_state"
    .end annotation
.end field

.field public mAttachmentMediaExpected:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attachment_media_expected"
    .end annotation
.end field

.field public mAttachmentMediaLoaded:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attachment_media_loaded"
    .end annotation
.end field

.field public mClientWeight:D
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "client_weight"
    .end annotation
.end field

.field public mFeaturesMeta:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "features_meta"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mFetchedAt:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "fetched_at"
    .end annotation
.end field

.field public mHasAttachmentText:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_attachment_text"
    .end annotation
.end field

.field public mImageCacheState:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "image_cache_state"
    .end annotation
.end field

.field public mImagesExpected:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "images_expected"
    .end annotation
.end field

.field public mImagesLoaded:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "images_loaded"
    .end annotation
.end field

.field public mIsAttachmentTextLoaded:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attachment_text_is_loaded"
    .end annotation
.end field

.field public mLinkCacheState:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attachment_link_cache_state"
    .end annotation
.end field

.field public mLiveCommentAgeMs:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "live_comment_age_ms"
    .end annotation
.end field

.field public mLiveVideoState:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "live_video_state"
    .end annotation
.end field

.field public mRankingWeight:D
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "weight"
    .end annotation
.end field

.field public mResultType:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "result_type"
    .end annotation
.end field

.field public mSeenState:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "client_seen_state"
    .end annotation
.end field

.field public mStoryHasDownloadedVideo:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "story_has_downloaded_video"
    .end annotation
.end field

.field public mStoryHasVideo:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "story_has_video"
    .end annotation
.end field

.field public mVideoCacheState:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "video_cache_state"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 179226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179227
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LX/14s;->mLiveCommentAgeMs:J

    return-void
.end method

.method public static a(II)I
    .locals 1

    .prologue
    .line 179228
    if-ne p0, p1, :cond_0

    .line 179229
    const/4 v0, 0x2

    .line 179230
    :goto_0
    return v0

    .line 179231
    :cond_0
    if-nez p1, :cond_1

    .line 179232
    const/4 v0, 0x0

    goto :goto_0

    .line 179233
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(LX/14t;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 179234
    invoke-virtual {p0, p1}, LX/14t;->b(Ljava/lang/String;)I

    move-result v0

    .line 179235
    invoke-virtual {p0, p1}, LX/14t;->a(Ljava/lang/String;)I

    move-result v1

    .line 179236
    invoke-static {v0, v1}, LX/14s;->a(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/14t;)V
    .locals 3

    .prologue
    .line 179237
    const-string v0, "ATTACHMENT_TEXT"

    invoke-virtual {p1, v0}, LX/14t;->b(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_b

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 179238
    iget-boolean v1, p0, LX/14s;->mHasAttachmentText:Z

    if-eq v1, v0, :cond_0

    .line 179239
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/14s;->a:Z

    .line 179240
    :cond_0
    iput-boolean v0, p0, LX/14s;->mHasAttachmentText:Z

    .line 179241
    const-string v0, "ATTACHMENT_TEXT"

    invoke-virtual {p1, v0}, LX/14t;->b(Ljava/lang/String;)I

    move-result v0

    const-string v1, "ATTACHMENT_TEXT"

    invoke-virtual {p1, v1}, LX/14t;->a(Ljava/lang/String;)I

    move-result v1

    if-ne v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 179242
    iget-boolean v1, p0, LX/14s;->mIsAttachmentTextLoaded:Z

    if-eq v1, v0, :cond_1

    .line 179243
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/14s;->a:Z

    .line 179244
    :cond_1
    iput-boolean v0, p0, LX/14s;->mIsAttachmentTextLoaded:Z

    .line 179245
    const-string v0, "PHOTO"

    invoke-virtual {p1, v0}, LX/14t;->a(Ljava/lang/String;)I

    move-result v0

    .line 179246
    iget v1, p0, LX/14s;->mImagesLoaded:I

    if-eq v0, v1, :cond_2

    .line 179247
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/14s;->a:Z

    .line 179248
    :cond_2
    iput v0, p0, LX/14s;->mImagesLoaded:I

    .line 179249
    const-string v0, "PHOTO"

    invoke-virtual {p1, v0}, LX/14t;->b(Ljava/lang/String;)I

    move-result v0

    .line 179250
    iget v1, p0, LX/14s;->mImagesExpected:I

    if-eq v0, v1, :cond_3

    .line 179251
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/14s;->a:Z

    .line 179252
    :cond_3
    iput v0, p0, LX/14s;->mImagesExpected:I

    .line 179253
    const-string v0, "ATTACHMENT_PHOTO"

    invoke-virtual {p1, v0}, LX/14t;->b(Ljava/lang/String;)I

    move-result v0

    const-string v1, "ATTACHMENT_VIDEO"

    invoke-virtual {p1, v1}, LX/14t;->b(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 179254
    iget v1, p0, LX/14s;->mAttachmentMediaExpected:I

    if-eq v1, v0, :cond_4

    .line 179255
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/14s;->a:Z

    .line 179256
    :cond_4
    iput v0, p0, LX/14s;->mAttachmentMediaExpected:I

    .line 179257
    const-string v0, "ATTACHMENT_PHOTO"

    invoke-virtual {p1, v0}, LX/14t;->a(Ljava/lang/String;)I

    move-result v0

    const-string v1, "ATTACHMENT_VIDEO"

    invoke-virtual {p1, v1}, LX/14t;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 179258
    iget v1, p0, LX/14s;->mAttachmentMediaLoaded:I

    if-eq v1, v0, :cond_5

    .line 179259
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/14s;->a:Z

    .line 179260
    :cond_5
    iput v0, p0, LX/14s;->mAttachmentMediaLoaded:I

    .line 179261
    const-string v0, "PHOTO"

    invoke-static {p1, v0}, LX/14s;->a(LX/14t;Ljava/lang/String;)I

    move-result v0

    .line 179262
    iget v1, p0, LX/14s;->mImageCacheState:I

    if-eq v0, v1, :cond_6

    .line 179263
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/14s;->a:Z

    .line 179264
    :cond_6
    iput v0, p0, LX/14s;->mImageCacheState:I

    .line 179265
    const-string v0, "VIDEO"

    invoke-static {p1, v0}, LX/14s;->a(LX/14t;Ljava/lang/String;)I

    move-result v0

    .line 179266
    iget v1, p0, LX/14s;->mVideoCacheState:I

    if-eq v0, v1, :cond_7

    .line 179267
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/14s;->a:Z

    .line 179268
    :cond_7
    iput v0, p0, LX/14s;->mVideoCacheState:I

    .line 179269
    const-string v0, "VIDEO"

    invoke-virtual {p1, v0}, LX/14t;->b(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_d

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 179270
    iget-boolean v1, p0, LX/14s;->mStoryHasVideo:Z

    if-eq v0, v1, :cond_8

    .line 179271
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/14s;->a:Z

    .line 179272
    :cond_8
    iput-boolean v0, p0, LX/14s;->mStoryHasVideo:Z

    .line 179273
    const-string v0, "ATTACHMENT_PHOTO"

    invoke-virtual {p1, v0}, LX/14t;->b(Ljava/lang/String;)I

    move-result v0

    const-string v1, "ATTACHMENT_VIDEO"

    invoke-virtual {p1, v1}, LX/14t;->b(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 179274
    const-string v1, "ATTACHMENT_PHOTO"

    invoke-virtual {p1, v1}, LX/14t;->a(Ljava/lang/String;)I

    move-result v1

    const-string v2, "ATTACHMENT_VIDEO"

    invoke-virtual {p1, v2}, LX/14t;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 179275
    invoke-static {v0, v1}, LX/14s;->a(II)I

    move-result v0

    move v0, v0

    .line 179276
    iget v1, p0, LX/14s;->mAttachmentMediaCacheState:I

    if-eq v1, v0, :cond_9

    .line 179277
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/14s;->a:Z

    .line 179278
    :cond_9
    iput v0, p0, LX/14s;->mAttachmentMediaCacheState:I

    .line 179279
    const-string v0, "ATTACHMENT_LINK"

    invoke-static {p1, v0}, LX/14s;->a(LX/14t;Ljava/lang/String;)I

    move-result v0

    .line 179280
    iget v1, p0, LX/14s;->mLinkCacheState:I

    if-eq v0, v1, :cond_a

    .line 179281
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/14s;->a:Z

    .line 179282
    :cond_a
    iput v0, p0, LX/14s;->mLinkCacheState:I

    .line 179283
    return-void

    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_d
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 179284
    iget-boolean v0, p0, LX/14s;->mStoryHasDownloadedVideo:Z

    if-eq p1, v0, :cond_0

    .line 179285
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/14s;->a:Z

    .line 179286
    :cond_0
    iput-boolean p1, p0, LX/14s;->mStoryHasDownloadedVideo:Z

    .line 179287
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 179288
    iget v0, p0, LX/14s;->mSeenState:I

    if-eq p1, v0, :cond_0

    .line 179289
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/14s;->a:Z

    .line 179290
    :cond_0
    iput p1, p0, LX/14s;->mSeenState:I

    .line 179291
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 179292
    iget v0, p0, LX/14s;->mLiveVideoState:I

    if-eq p1, v0, :cond_0

    .line 179293
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/14s;->a:Z

    .line 179294
    :cond_0
    iput p1, p0, LX/14s;->mLiveVideoState:I

    .line 179295
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 179296
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/14s;->a:Z

    .line 179297
    return-void
.end method
