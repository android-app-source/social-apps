.class public LX/13a;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hk;


# static fields
.field private static final s:LX/0wT;


# instance fields
.field private final a:LX/0x7;

.field private b:Landroid/view/View;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;

.field public e:Landroid/view/ViewStub;

.field public f:LX/0wd;

.field public g:LX/13c;

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tK;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bhc;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wW;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Yb;

.field public p:LX/13Z;

.field private q:Z

.field public r:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 176930
    const-wide v0, 0x407561999999999aL    # 342.1

    const-wide v2, 0x4042770a3d70a3d7L    # 36.93

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/13a;->s:LX/0wT;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 176931
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 176932
    new-instance v0, LX/13b;

    invoke-direct {v0, p0}, LX/13b;-><init>(LX/13a;)V

    iput-object v0, p0, LX/13a;->a:LX/0x7;

    .line 176933
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 176934
    iput-object v0, p0, LX/13a;->h:LX/0Ot;

    .line 176935
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 176936
    iput-object v0, p0, LX/13a;->i:LX/0Ot;

    .line 176937
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 176938
    iput-object v0, p0, LX/13a;->j:LX/0Ot;

    .line 176939
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 176940
    iput-object v0, p0, LX/13a;->k:LX/0Ot;

    .line 176941
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 176942
    iput-object v0, p0, LX/13a;->l:LX/0Ot;

    .line 176943
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 176944
    iput-object v0, p0, LX/13a;->m:LX/0Ot;

    .line 176945
    sget-object v0, LX/13Z;->DSM_INDICATOR_DISABLED:LX/13Z;

    iput-object v0, p0, LX/13a;->p:LX/13Z;

    .line 176946
    return-void
.end method

.method public static a$redex0(LX/13a;I)V
    .locals 3

    .prologue
    .line 176947
    iget-object v0, p0, LX/13a;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 176948
    iget-object v0, p0, LX/13a;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    const/4 p1, 0x0

    .line 176949
    invoke-virtual {v0}, LX/0tK;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/0tK;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    sget-short v2, LX/0wm;->F:S

    invoke-interface {v1, v2, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    :goto_0
    move v0, v1

    .line 176950
    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/13a;->r:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/13a;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176951
    iget-object v0, p0, LX/13a;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/DBh;

    invoke-direct {v1, p0}, LX/DBh;-><init>(LX/13a;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 176952
    :cond_0
    return-void

    :cond_1
    iget-object v1, v0, LX/0tK;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    sget-short v2, LX/0wm;->l:S

    invoke-interface {v1, v2, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    goto :goto_0
.end method

.method public static a$redex0(LX/13a;LX/16C;)V
    .locals 10

    .prologue
    const/16 v2, 0x8

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 176953
    iget-object v0, p0, LX/13a;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176954
    invoke-static {p0}, LX/13a;->f(LX/13a;)LX/13Z;

    move-result-object v0

    .line 176955
    iget-object v1, p0, LX/13a;->p:LX/13Z;

    if-eq v1, v0, :cond_0

    .line 176956
    iput-object v0, p0, LX/13a;->p:LX/13Z;

    .line 176957
    iget-object v1, p0, LX/13a;->g:LX/13c;

    if-eqz v1, :cond_0

    .line 176958
    iget-object v1, p0, LX/13a;->g:LX/13c;

    invoke-virtual {v1, v0, p1}, LX/13c;->a(LX/13Z;LX/16C;)V

    .line 176959
    :cond_0
    :goto_0
    return-void

    .line 176960
    :cond_1
    iget-object v0, p0, LX/13a;->e:Landroid/view/ViewStub;

    if-eqz v0, :cond_0

    .line 176961
    iget-object v0, p0, LX/13a;->b:Landroid/view/View;

    if-nez v0, :cond_2

    .line 176962
    iget-object v0, p0, LX/13a;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0, v1}, LX/0tK;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176963
    iget-object v0, p0, LX/13a;->e:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/13a;->b:Landroid/view/View;

    .line 176964
    iget-object v0, p0, LX/13a;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v3, LX/13a;->s:LX/0wT;

    invoke-virtual {v0, v3}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v4, v5}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    new-instance v3, LX/DBi;

    invoke-direct {v3, p0}, LX/DBi;-><init>(LX/13a;)V

    invoke-virtual {v0, v3}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/13a;->f:LX/0wd;

    .line 176965
    iget-object v0, p0, LX/13a;->b:Landroid/view/View;

    const v3, 0x7f0d0ce0

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/13a;->c:Landroid/widget/TextView;

    .line 176966
    iget-object v0, p0, LX/13a;->b:Landroid/view/View;

    const v3, 0x7f0d0ce1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/13a;->d:Landroid/widget/TextView;

    .line 176967
    iget-object v0, p0, LX/13a;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v3, 0x7f020408

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 176968
    iget-object v3, p0, LX/13a;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v0, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 176969
    const v0, 0x7f080a0e

    invoke-static {p0, v0}, LX/13a;->a$redex0(LX/13a;I)V

    .line 176970
    iget-object v0, p0, LX/13a;->d:Landroid/widget/TextView;

    const v3, 0x7f080a13

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 176971
    iget-object v0, p0, LX/13a;->d:Landroid/widget/TextView;

    new-instance v3, LX/2tp;

    invoke-direct {v3, p0}, LX/2tp;-><init>(LX/13a;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176972
    :cond_2
    iget-object v0, p0, LX/13a;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    .line 176973
    iget-object v3, p0, LX/13a;->c:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v3

    .line 176974
    iget-object v4, p0, LX/13a;->c:Landroid/widget/TextView;

    const/16 v5, 0xff

    invoke-static {v3}, Landroid/graphics/Color;->red(I)I

    move-result v6

    invoke-static {v3}, Landroid/graphics/Color;->green(I)I

    move-result v7

    invoke-static {v3}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {v5, v6, v7, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 176975
    invoke-static {p0}, LX/13a;->f(LX/13a;)LX/13Z;

    move-result-object v3

    .line 176976
    sget-object v4, LX/13Z;->DSM_INDICATOR_DISABLED:LX/13Z;

    if-eq v4, v3, :cond_4

    .line 176977
    sget-object v4, LX/13Z;->DSM_INDICATOR_ENABLED_ON_STATE:LX/13Z;

    if-ne v4, v3, :cond_3

    .line 176978
    const v0, 0x7f080a0e

    invoke-static {p0, v0}, LX/13a;->a$redex0(LX/13a;I)V

    .line 176979
    iget-object v0, p0, LX/13a;->c:Landroid/widget/TextView;

    const v4, 0x7f0a033c

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 176980
    iget-object v0, p0, LX/13a;->d:Landroid/widget/TextView;

    const v4, 0x7f0a033c

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 176981
    iput-boolean v1, p0, LX/13a;->q:Z

    .line 176982
    :goto_1
    iget-object v0, p0, LX/13a;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    const/4 v6, 0x1

    .line 176983
    invoke-virtual {v0}, LX/0tK;->q()Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, v0, LX/0tK;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0ad;

    sget-short v5, LX/0wm;->x:S

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    :goto_2
    move v0, v4

    .line 176984
    if-nez v0, :cond_5

    .line 176985
    :goto_3
    invoke-direct {p0, v2}, LX/13a;->b(I)V

    .line 176986
    iget-object v0, p0, LX/13a;->p:LX/13Z;

    if-eq v3, v0, :cond_0

    .line 176987
    iput-object v3, p0, LX/13a;->p:LX/13Z;

    .line 176988
    iget-object v0, p0, LX/13a;->g:LX/13c;

    if-eqz v0, :cond_0

    .line 176989
    iget-object v0, p0, LX/13a;->g:LX/13c;

    invoke-virtual {v0, v3, p1}, LX/13c;->a(LX/13Z;LX/16C;)V

    goto/16 :goto_0

    .line 176990
    :cond_3
    iget-boolean v4, p0, LX/13a;->q:Z

    if-nez v4, :cond_6

    .line 176991
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/13a;->q:Z

    .line 176992
    iget-object v0, p0, LX/13a;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bhc;

    invoke-virtual {v0}, LX/Bhc;->b()J

    move-result-wide v4

    .line 176993
    const v0, 0x7f080a10

    invoke-static {p0, v0}, LX/13a;->a$redex0(LX/13a;I)V

    .line 176994
    iget-object v0, p0, LX/13a;->c:Landroid/widget/TextView;

    new-instance v6, Lcom/facebook/feed/fragment/NewsFeedFragmentDsmController$3;

    invoke-direct {v6, p0}, Lcom/facebook/feed/fragment/NewsFeedFragmentDsmController$3;-><init>(LX/13a;)V

    invoke-virtual {v0, v6, v4, v5}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 176995
    iget-object v0, p0, LX/13a;->c:Landroid/widget/TextView;

    new-instance v6, Lcom/facebook/feed/fragment/NewsFeedFragmentDsmController$4;

    invoke-direct {v6, p0}, Lcom/facebook/feed/fragment/NewsFeedFragmentDsmController$4;-><init>(LX/13a;)V

    const-wide/16 v8, 0xc8

    add-long/2addr v4, v8

    invoke-virtual {v0, v6, v4, v5}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 176996
    iget-object v0, p0, LX/13a;->c:Landroid/widget/TextView;

    const v4, 0x7f0a033d

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 176997
    iget-object v0, p0, LX/13a;->d:Landroid/widget/TextView;

    const v4, 0x7f0a033d

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_1

    .line 176998
    :cond_4
    iput-boolean v1, p0, LX/13a;->q:Z

    move v1, v2

    .line 176999
    goto :goto_1

    :cond_5
    move v2, v1

    goto :goto_3

    :cond_6
    move v1, v0

    goto :goto_1

    :cond_7
    iget-object v4, v0, LX/0tK;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0ad;

    sget-short v5, LX/0wm;->d:S

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    goto :goto_2
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 177000
    iget-object v0, p0, LX/13a;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    .line 177001
    iget-object v1, p0, LX/13a;->b:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    .line 177002
    iget-object v1, p0, LX/13a;->g:LX/13c;

    if-eqz v1, :cond_0

    if-eq p1, v0, :cond_0

    .line 177003
    iget-object v0, p0, LX/13a;->g:LX/13c;

    .line 177004
    iget-object v1, v0, LX/13c;->a:LX/0jP;

    iget-object v1, v1, LX/0jP;->k:Lcom/facebook/widget/CustomViewPager;

    new-instance p0, Lcom/facebook/katana/fragment/maintab/controllercallbacks/FbMainTabFragmentDataSensitivityController$2$1;

    invoke-direct {p0, v0}, Lcom/facebook/katana/fragment/maintab/controllercallbacks/FbMainTabFragmentDataSensitivityController$2$1;-><init>(LX/13c;)V

    invoke-virtual {v1, p0}, Lcom/facebook/widget/CustomViewPager;->post(Ljava/lang/Runnable;)Z

    .line 177005
    :cond_0
    return-void
.end method

.method public static f(LX/13a;)LX/13Z;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 177006
    iget-object v0, p0, LX/13a;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 177007
    sget-object v0, LX/13Z;->DSM_OFF:LX/13Z;

    .line 177008
    :goto_0
    return-object v0

    .line 177009
    :cond_0
    iget-object v0, p0, LX/13a;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0, v1}, LX/0tK;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 177010
    iget-object v0, p0, LX/13a;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0, v1}, LX/0tK;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 177011
    sget-object v0, LX/13Z;->DSM_INDICATOR_ENABLED_ON_STATE:LX/13Z;

    goto :goto_0

    .line 177012
    :cond_1
    sget-object v0, LX/13Z;->DSM_INDICATOR_ENABLED_OFF_STATE:LX/13Z;

    goto :goto_0

    .line 177013
    :cond_2
    sget-object v0, LX/13Z;->DSM_INDICATOR_DISABLED:LX/13Z;

    goto :goto_0
.end method


# virtual methods
.method public final c()V
    .locals 3

    .prologue
    .line 177014
    iget-object v0, p0, LX/13a;->o:LX/0Yb;

    if-eqz v0, :cond_0

    .line 177015
    :goto_0
    iget-object v0, p0, LX/13a;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    iget-object v1, p0, LX/13a;->a:LX/0x7;

    invoke-virtual {v0, v1}, LX/0tK;->a(LX/0x7;)V

    .line 177016
    sget-object v0, LX/16C;->RESUME:LX/16C;

    invoke-static {p0, v0}, LX/13a;->a$redex0(LX/13a;LX/16C;)V

    .line 177017
    return-void

    .line 177018
    :cond_0
    new-instance v1, LX/167;

    invoke-direct {v1, p0}, LX/167;-><init>(LX/13a;)V

    .line 177019
    iget-object v0, p0, LX/13a;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v2, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-interface {v0, v2, v1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/13a;->o:LX/0Yb;

    .line 177020
    iget-object v0, p0, LX/13a;->o:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 177021
    iget-object v0, p0, LX/13a;->o:LX/0Yb;

    if-eqz v0, :cond_0

    .line 177022
    iget-object v0, p0, LX/13a;->o:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 177023
    const/4 v0, 0x0

    iput-object v0, p0, LX/13a;->o:LX/0Yb;

    .line 177024
    :cond_0
    iget-object v0, p0, LX/13a;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    iget-object v1, p0, LX/13a;->a:LX/0x7;

    invoke-virtual {v0, v1}, LX/0tK;->b(LX/0x7;)V

    .line 177025
    return-void
.end method
