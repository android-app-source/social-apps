.class public LX/1Jf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Jg;


# instance fields
.field public final a:LX/0Sh;

.field public final b:LX/0So;

.field public final c:Ljava/lang/Object;

.field public final d:LX/1Jj;

.field public final e:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "LX/1Rb;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/prefetch/PrefetchParams;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:LX/0fx;

.field private final g:Ljava/util/concurrent/ScheduledExecutorService;

.field private final h:Lcom/facebook/feed/rows/prefetch/MultiRowImagePrefetcherWrapperImpl$PrefetchRegistrationRunnable;

.field public final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1Rb;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/prefetch/PrefetchParams;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field public j:LX/1R4;

.field public k:Z

.field public l:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field public m:J


# direct methods
.method public constructor <init>(LX/0Sh;LX/0So;LX/1JX;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 3
    .param p3    # LX/1JX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 230266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230267
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, LX/1Jf;->e:Landroid/util/LruCache;

    .line 230268
    new-instance v0, LX/1Jh;

    invoke-direct {v0, p0}, LX/1Jh;-><init>(LX/1Jf;)V

    iput-object v0, p0, LX/1Jf;->f:LX/0fx;

    .line 230269
    new-instance v0, Lcom/facebook/feed/rows/prefetch/MultiRowImagePrefetcherWrapperImpl$PrefetchRegistrationRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/feed/rows/prefetch/MultiRowImagePrefetcherWrapperImpl$PrefetchRegistrationRunnable;-><init>(LX/1Jf;)V

    iput-object v0, p0, LX/1Jf;->h:Lcom/facebook/feed/rows/prefetch/MultiRowImagePrefetcherWrapperImpl$PrefetchRegistrationRunnable;

    .line 230270
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/1Jf;->i:Ljava/util/Map;

    .line 230271
    iput-object p1, p0, LX/1Jf;->a:LX/0Sh;

    .line 230272
    iput-object p2, p0, LX/1Jf;->b:LX/0So;

    .line 230273
    new-instance v0, LX/1Ji;

    invoke-direct {v0, p0}, LX/1Ji;-><init>(LX/1Jf;)V

    .line 230274
    iget-object v1, p3, LX/1JX;->a:LX/1JY;

    iget-object v2, p3, LX/1JX;->c:LX/1JV;

    iget-object p1, p3, LX/1JX;->b:LX/1JZ;

    .line 230275
    new-instance p3, LX/1Jj;

    invoke-static {v1}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object p2

    check-cast p2, LX/0Sh;

    invoke-direct {p3, p2, v2, v0, p1}, LX/1Jj;-><init>(LX/0Sh;LX/1JV;LX/1Ji;LX/1JZ;)V

    .line 230276
    move-object v1, p3

    .line 230277
    move-object v0, v1

    .line 230278
    iput-object v0, p0, LX/1Jf;->d:LX/1Jj;

    .line 230279
    iput-object p4, p0, LX/1Jf;->g:Ljava/util/concurrent/ScheduledExecutorService;

    .line 230280
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/1Jf;->c:Ljava/lang/Object;

    .line 230281
    return-void
.end method

.method public static a$redex0(LX/1Jf;II)V
    .locals 2

    .prologue
    .line 230282
    const-string v0, "MultiRowImagePrefetcherWrapperImpl::updateVisibleItems"

    const v1, 0x601b1fbc

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 230283
    :try_start_0
    iget-object v0, p0, LX/1Jf;->d:LX/1Jj;

    add-int v1, p1, p2

    add-int/lit8 v1, v1, -0x1

    .line 230284
    iget-object p0, v0, LX/1Jj;->a:LX/0Sh;

    invoke-virtual {p0}, LX/0Sh;->a()V

    .line 230285
    iget-boolean p0, v0, LX/1Jj;->j:Z

    invoke-static {p0}, LX/0PB;->checkState(Z)V

    .line 230286
    iget p0, v0, LX/1Jj;->h:I

    if-ne p0, p1, :cond_0

    iget p0, v0, LX/1Jj;->i:I

    if-ne p0, v1, :cond_0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230287
    :goto_0
    const v0, -0x7a36893a

    invoke-static {v0}, LX/02m;->a(I)V

    .line 230288
    return-void

    .line 230289
    :catchall_0
    move-exception v0

    const v1, -0x36abea7f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 230290
    :cond_0
    iput p1, v0, LX/1Jj;->h:I

    .line 230291
    iput v1, v0, LX/1Jj;->i:I

    .line 230292
    invoke-static {v0}, LX/1Jj;->d(LX/1Jj;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0fx;
    .locals 1

    .prologue
    .line 230265
    iget-object v0, p0, LX/1Jf;->f:LX/0fx;

    return-object v0
.end method

.method public final a(LX/0g8;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 230293
    iget-object v0, p0, LX/1Jf;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 230294
    iget-boolean v0, p0, LX/1Jf;->k:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 230295
    iget-object v0, p0, LX/1Jf;->j:LX/1R4;

    if-eqz v0, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 230296
    iput-boolean v1, p0, LX/1Jf;->k:Z

    .line 230297
    iget-object v0, p0, LX/1Jf;->d:LX/1Jj;

    const/4 v2, 0x1

    .line 230298
    iget-object v1, v0, LX/1Jj;->a:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 230299
    iget-boolean v1, v0, LX/1Jj;->j:Z

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 230300
    iput-boolean v2, v0, LX/1Jj;->j:Z

    .line 230301
    invoke-interface {p1}, LX/0g8;->q()I

    move-result v0

    .line 230302
    invoke-interface {p1}, LX/0g8;->r()I

    move-result v1

    sub-int/2addr v1, v0

    add-int/lit8 v1, v1, 0x1

    invoke-static {p0, v0, v1}, LX/1Jf;->a$redex0(LX/1Jf;II)V

    .line 230303
    return-void

    :cond_1
    move v0, v2

    .line 230304
    goto :goto_0

    .line 230305
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(LX/1R4;)V
    .locals 0

    .prologue
    .line 230263
    iput-object p1, p0, LX/1Jf;->j:LX/1R4;

    .line 230264
    return-void
.end method

.method public final a(LX/1Rb;Lcom/facebook/photos/prefetch/PrefetchParams;)V
    .locals 6

    .prologue
    .line 230254
    iget-object v1, p0, LX/1Jf;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 230255
    :try_start_0
    iget-object v0, p0, LX/1Jf;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 230256
    if-nez v0, :cond_0

    .line 230257
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 230258
    iget-object v2, p0, LX/1Jf;->i:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230259
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230260
    iget-object v0, p0, LX/1Jf;->l:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_1

    .line 230261
    iget-object v0, p0, LX/1Jf;->g:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v2, p0, LX/1Jf;->h:Lcom/facebook/feed/rows/prefetch/MultiRowImagePrefetcherWrapperImpl$PrefetchRegistrationRunnable;

    const-wide/16 v4, 0xfa

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v4, v5, v3}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/1Jf;->l:Ljava/util/concurrent/ScheduledFuture;

    .line 230262
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 230244
    iget-object v0, p0, LX/1Jf;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 230245
    iget-boolean v0, p0, LX/1Jf;->k:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 230246
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Jf;->k:Z

    .line 230247
    iget-object v0, p0, LX/1Jf;->d:LX/1Jj;

    .line 230248
    iget-object p0, v0, LX/1Jj;->a:LX/0Sh;

    invoke-virtual {p0}, LX/0Sh;->a()V

    .line 230249
    iget-boolean p0, v0, LX/1Jj;->j:Z

    invoke-static {p0}, LX/0PB;->checkState(Z)V

    .line 230250
    const/4 p0, 0x0

    iput-boolean p0, v0, LX/1Jj;->j:Z

    .line 230251
    invoke-static {v0}, LX/1Jj;->d(LX/1Jj;)V

    .line 230252
    return-void
.end method

.method public final c()LX/1R4;
    .locals 1

    .prologue
    .line 230253
    iget-object v0, p0, LX/1Jf;->j:LX/1R4;

    return-object v0
.end method
