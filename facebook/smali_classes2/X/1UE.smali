.class public LX/1UE;
.super LX/1UF;
.source ""

# interfaces
.implements LX/1Rq;


# instance fields
.field private final a:LX/1Rq;


# direct methods
.method public constructor <init>(LX/1Rq;)V
    .locals 0

    .prologue
    .line 255666
    invoke-direct {p0, p1}, LX/1UF;-><init>(LX/1Qr;)V

    .line 255667
    iput-object p1, p0, LX/1UE;->a:LX/1Rq;

    .line 255668
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 255665
    iget-object v0, p0, LX/1UE;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1OQ;->C_(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 1

    .prologue
    .line 255664
    iget-object v0, p0, LX/1UE;->a:LX/1Rq;

    invoke-interface {v0, p1, p2}, LX/1OQ;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KR;)V
    .locals 1

    .prologue
    .line 255662
    iget-object v0, p0, LX/1UE;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1Rr;->a(LX/1KR;)V

    .line 255663
    return-void
.end method

.method public final a(LX/1OD;)V
    .locals 1

    .prologue
    .line 255660
    iget-object v0, p0, LX/1UE;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1OQ;->a(LX/1OD;)V

    .line 255661
    return-void
.end method

.method public a(LX/1a1;I)V
    .locals 1

    .prologue
    .line 255658
    iget-object v0, p0, LX/1UE;->a:LX/1Rq;

    invoke-interface {v0, p1, p2}, LX/1OQ;->a(LX/1a1;I)V

    .line 255659
    return-void
.end method

.method public final a_(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 255656
    iget-object v0, p0, LX/1UE;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1OQ;->a_(Landroid/support/v7/widget/RecyclerView;)V

    .line 255657
    return-void
.end method

.method public final b(LX/1KR;)V
    .locals 1

    .prologue
    .line 255654
    iget-object v0, p0, LX/1UE;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1Rr;->b(LX/1KR;)V

    .line 255655
    return-void
.end method

.method public final b(LX/1OD;)V
    .locals 1

    .prologue
    .line 255642
    iget-object v0, p0, LX/1UE;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1OQ;->b(LX/1OD;)V

    .line 255643
    return-void
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 255652
    iget-object v0, p0, LX/1UE;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1OQ;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 255653
    return-void
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 255651
    iget-object v0, p0, LX/1UE;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1Rs;->b(I)Z

    move-result v0

    return v0
.end method

.method public final eC_()Z
    .locals 1

    .prologue
    .line 255650
    iget-object v0, p0, LX/1UE;->a:LX/1Rq;

    invoke-interface {v0}, LX/1OQ;->eC_()Z

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 255649
    iget-object v0, p0, LX/1UE;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1OP;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 255648
    iget-object v0, p0, LX/1UE;->a:LX/1Rq;

    invoke-interface {v0, p1}, LX/1OP;->getItemViewType(I)I

    move-result v0

    return v0
.end method

.method public final ii_()I
    .locals 1

    .prologue
    .line 255647
    iget-object v0, p0, LX/1UE;->a:LX/1Rq;

    invoke-interface {v0}, LX/1OO;->ii_()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 255646
    iget-object v0, p0, LX/1UE;->a:LX/1Rq;

    invoke-interface {v0}, LX/1OP;->ij_()I

    move-result v0

    return v0
.end method

.method public final notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 255644
    iget-object v0, p0, LX/1UE;->a:LX/1Rq;

    invoke-interface {v0}, LX/1OP;->notifyDataSetChanged()V

    .line 255645
    return-void
.end method
