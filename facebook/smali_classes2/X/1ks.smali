.class public LX/1ks;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1kt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/graphql/executor/GraphQLQueryExecutor$InternalCacheProcessor",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final f:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public final a:LX/0zO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final c:LX/0sj;

.field private final d:LX/0sg;

.field public final e:LX/0sT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 310452
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, LX/1ks;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(LX/0zO;LX/0sj;LX/0sg;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0sT;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zO",
            "<TT;>;",
            "LX/0sj;",
            "Lcom/facebook/graphql/executor/iface/ConsistencyCacheFactory;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0sT;",
            ")V"
        }
    .end annotation

    .prologue
    .line 310354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310355
    iput-object p1, p0, LX/1ks;->a:LX/0zO;

    .line 310356
    iput-object p2, p0, LX/1ks;->c:LX/0sj;

    .line 310357
    iput-object p3, p0, LX/1ks;->d:LX/0sg;

    .line 310358
    iput-object p4, p0, LX/1ks;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 310359
    iput-object p5, p0, LX/1ks;->e:LX/0sT;

    .line 310360
    iget-object v0, p0, LX/1ks;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x31001e    # 4.499981E-39f

    const/16 v2, 0xa

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(II)V

    .line 310361
    return-void
.end method

.method public static a(LX/1ks;Lcom/facebook/graphql/executor/GraphQLResult;I)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;I)",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const v3, 0x31001e    # 4.499981E-39f

    .line 310425
    iget-object v0, p0, LX/1ks;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "resolve_consistency"

    invoke-interface {v0, v3, p2, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 310426
    const-string v0, "DefaultCacheProcessor.needConsistency"

    const v2, -0x70f3dc02

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 310427
    :try_start_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 310428
    instance-of v0, v0, LX/0jT;

    if-eqz v0, :cond_1

    .line 310429
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 310430
    check-cast v0, LX/0jT;

    .line 310431
    iget-object v1, p0, LX/1ks;->d:LX/0sg;

    invoke-virtual {p1}, Lcom/facebook/graphql/executor/GraphQLResult;->e()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0sg;->a(Ljava/util/Collection;)LX/2lk;

    move-result-object v1

    .line 310432
    invoke-interface {v1}, LX/2lk;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 310433
    invoke-interface {v1, v0}, LX/2lk;->d(LX/0jT;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310434
    iget-object v0, p0, LX/1ks;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x31001e    # 4.499981E-39f

    const-string v2, "fields_change_on_read"

    invoke-interface {v0, v1, p2, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310435
    :cond_0
    :goto_0
    const v0, 0x3a47057d

    invoke-static {v0}, LX/02m;->a(I)V

    .line 310436
    return-object p1

    .line 310437
    :cond_1
    :try_start_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 310438
    instance-of v0, v0, Ljava/util/List;

    if-eqz v0, :cond_0

    .line 310439
    iget-object v0, p0, LX/1ks;->d:LX/0sg;

    invoke-virtual {p1}, Lcom/facebook/graphql/executor/GraphQLResult;->e()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0sg;->a(Ljava/util/Collection;)LX/2lk;

    move-result-object v3

    .line 310440
    invoke-interface {v3}, LX/2lk;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 310441
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 310442
    check-cast v0, Ljava/util/List;

    .line 310443
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 310444
    instance-of v5, v0, LX/0jT;

    if-eqz v5, :cond_5

    .line 310445
    check-cast v0, LX/0jT;

    invoke-interface {v3, v0}, LX/2lk;->d(LX/0jT;)Z

    move-result v0

    .line 310446
    if-nez v2, :cond_2

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_2
    move v2, v0

    .line 310447
    goto :goto_1

    :cond_3
    move v0, v1

    .line 310448
    goto :goto_2

    .line 310449
    :cond_4
    if-eqz v2, :cond_0

    .line 310450
    iget-object v0, p0, LX/1ks;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x31001e    # 4.499981E-39f

    const-string v2, "fields_change_on_read"

    invoke-interface {v0, v1, p2, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 310451
    :catchall_0
    move-exception v0

    const v1, 0x5766d5f2

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method private static d(LX/1ks;)I
    .locals 5

    .prologue
    .line 310419
    sget-object v0, LX/1ks;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    .line 310420
    iget-object v1, p0, LX/1ks;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310023    # 4.499988E-39f

    const-string v3, "query"

    iget-object v4, p0, LX/1ks;->a:LX/0zO;

    .line 310421
    iget-object p0, v4, LX/0zO;->m:LX/0gW;

    move-object v4, p0

    .line 310422
    iget-object p0, v4, LX/0gW;->f:Ljava/lang/String;

    move-object v4, p0

    .line 310423
    invoke-interface {v1, v2, v0, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IILjava/lang/String;Ljava/lang/String;)V

    .line 310424
    return v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 310418
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    const v4, 0x310023    # 4.499988E-39f

    .line 310411
    invoke-static {p0}, LX/1ks;->d(LX/1ks;)I

    move-result v1

    .line 310412
    :try_start_0
    iget-object v0, p0, LX/1ks;->c:LX/0sj;

    iget-object v2, p0, LX/1ks;->a:LX/0zO;

    invoke-interface {v0, v2, p1}, LX/0sj;->a(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 310413
    iget-object v0, p0, LX/1ks;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310023    # 4.499988E-39f

    const/4 v3, 0x2

    invoke-interface {v0, v2, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 310414
    const/4 v0, 0x1

    return v0

    .line 310415
    :catch_0
    move-exception v0

    .line 310416
    iget-object v2, p0, LX/1ks;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v3, 0x3

    invoke-interface {v2, v4, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 310417
    throw v0
.end method

.method public final b()Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const v5, 0x31001e    # 4.499981E-39f

    .line 310379
    sget-object v0, LX/1ks;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v1

    .line 310380
    :try_start_0
    iget-object v0, p0, LX/1ks;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x31001e    # 4.499981E-39f

    const-string v3, "query"

    iget-object v4, p0, LX/1ks;->a:LX/0zO;

    .line 310381
    iget-object v6, v4, LX/0zO;->m:LX/0gW;

    move-object v4, v6

    .line 310382
    iget-object v6, v4, LX/0gW;->f:Ljava/lang/String;

    move-object v4, v6

    .line 310383
    invoke-interface {v0, v2, v1, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IILjava/lang/String;Ljava/lang/String;)V

    .line 310384
    iget-object v0, p0, LX/1ks;->a:LX/0zO;

    invoke-virtual {v0}, LX/0zO;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310385
    iget-object v0, p0, LX/1ks;->c:LX/0sj;

    instance-of v0, v0, LX/0sh;

    if-nez v0, :cond_3

    .line 310386
    const/4 v0, 0x0

    .line 310387
    :goto_0
    move v0, v0

    .line 310388
    if-eqz v0, :cond_0

    .line 310389
    iget-object v0, p0, LX/1ks;->c:LX/0sj;

    instance-of v0, v0, LX/0sh;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 310390
    iget-object v0, p0, LX/1ks;->c:LX/0sj;

    check-cast v0, LX/0sh;

    .line 310391
    iget-object v2, p0, LX/1ks;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x31001e    # 4.499981E-39f

    const-string v4, "resolve_consistency_experimental"

    invoke-interface {v2, v3, v1, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 310392
    iget-object v2, p0, LX/1ks;->a:LX/0zO;

    invoke-virtual {v0, v2}, LX/0sh;->c(LX/0zO;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    move-object v0, v0

    .line 310393
    :goto_1
    iget-object v2, p0, LX/1ks;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x31001e    # 4.499981E-39f

    const/4 v4, 0x2

    invoke-interface {v2, v3, v1, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 310394
    return-object v0

    .line 310395
    :cond_0
    iget-object v0, p0, LX/1ks;->c:LX/0sj;

    iget-object v2, p0, LX/1ks;->a:LX/0zO;

    invoke-interface {v0, v2}, LX/0sj;->b(LX/0zO;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 310396
    if-eqz v0, :cond_4

    .line 310397
    iget-object v2, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 310398
    if-eqz v2, :cond_4

    iget-object v2, p0, LX/1ks;->a:LX/0zO;

    .line 310399
    iget-boolean v3, v2, LX/0zO;->p:Z

    move v2, v3

    .line 310400
    if-eqz v2, :cond_4

    iget-object v2, p0, LX/1ks;->e:LX/0sT;

    const/4 v3, 0x0

    .line 310401
    invoke-virtual {v2}, LX/0sT;->f()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v2, LX/0sT;->a:LX/0ad;

    sget-short v6, LX/1NG;->l:S

    invoke-interface {v4, v6, v3}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v3, 0x1

    :cond_1
    move v2, v3

    .line 310402
    if-nez v2, :cond_4

    const/4 v2, 0x1

    .line 310403
    :goto_2
    if-eqz v2, :cond_2

    .line 310404
    invoke-static {p0, v0, v1}, LX/1ks;->a(LX/1ks;Lcom/facebook/graphql/executor/GraphQLResult;I)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 310405
    :cond_2
    move-object v0, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 310406
    goto :goto_1

    .line 310407
    :catch_0
    move-exception v0

    .line 310408
    iget-object v2, p0, LX/1ks;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v3, 0x3

    invoke-interface {v2, v5, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 310409
    throw v0

    :cond_3
    :try_start_1
    iget-object v0, p0, LX/1ks;->e:LX/0sT;

    invoke-virtual {v0}, LX/0sT;->d()Z

    move-result v0

    goto :goto_0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 310410
    :cond_4
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public final b(Lcom/facebook/graphql/executor/GraphQLResult;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    const v4, 0x310023    # 4.499988E-39f

    .line 310363
    iget-object v0, p0, LX/1ks;->c:LX/0sj;

    instance-of v0, v0, LX/0sh;

    if-nez v0, :cond_0

    .line 310364
    invoke-virtual {p0, p1}, LX/1ks;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Z

    move-result v0

    .line 310365
    :goto_0
    return v0

    .line 310366
    :cond_0
    iget-object v0, p0, LX/1ks;->c:LX/0sj;

    check-cast v0, LX/0sh;

    .line 310367
    invoke-static {p0}, LX/1ks;->d(LX/1ks;)I

    move-result v1

    .line 310368
    iget-object v2, p0, LX/1ks;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v3, "valid_buffer"

    invoke-interface {v2, v4, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 310369
    :try_start_0
    invoke-static {p1}, LX/1lN;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/15i;

    move-result-object v2

    .line 310370
    if-eqz v2, :cond_1

    .line 310371
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 310372
    :goto_1
    move-object v2, v2

    .line 310373
    iget-object v3, p0, LX/1ks;->a:LX/0zO;

    invoke-virtual {v0, v3, p1, v2}, LX/0sh;->a(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;Ljava/nio/ByteBuffer;)V

    .line 310374
    iget-object v0, p0, LX/1ks;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310023    # 4.499988E-39f

    const/4 v3, 0x2

    invoke-interface {v0, v2, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 310375
    const/4 v0, 0x1

    goto :goto_0

    .line 310376
    :catch_0
    move-exception v0

    .line 310377
    iget-object v2, p0, LX/1ks;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v3, 0x3

    invoke-interface {v2, v4, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 310378
    throw v0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final c(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;)",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 310362
    return-object p1
.end method
