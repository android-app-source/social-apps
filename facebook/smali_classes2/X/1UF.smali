.class public LX/1UF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Qr;


# instance fields
.field private final a:LX/1Qr;


# direct methods
.method public constructor <init>(LX/1Qr;)V
    .locals 0

    .prologue
    .line 255682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255683
    iput-object p1, p0, LX/1UF;->a:LX/1Qr;

    .line 255684
    return-void
.end method


# virtual methods
.method public a(LX/5Mj;)V
    .locals 1

    .prologue
    .line 255678
    iget-object v0, p0, LX/1UF;->a:LX/1Qr;

    invoke-interface {v0, p1}, LX/0fn;->a(LX/5Mj;)V

    .line 255679
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 255680
    iget-object v0, p0, LX/1UF;->a:LX/1Qr;

    invoke-interface {v0, p1}, LX/1Qr;->a(Landroid/content/res/Configuration;)V

    .line 255681
    return-void
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 255685
    iget-object v0, p0, LX/1UF;->a:LX/1Qr;

    invoke-interface {v0, p1}, LX/1Qr;->c(I)I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 255674
    iget-object v0, p0, LX/1UF;->a:LX/1Qr;

    invoke-interface {v0}, LX/1Qr;->d()I

    move-result v0

    return v0
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 255675
    iget-object v0, p0, LX/1UF;->a:LX/1Qr;

    invoke-interface {v0}, LX/0Vf;->dispose()V

    .line 255676
    return-void
.end method

.method public final e()LX/1R4;
    .locals 1

    .prologue
    .line 255677
    iget-object v0, p0, LX/1UF;->a:LX/1Qr;

    invoke-interface {v0}, LX/1Qr;->e()LX/1R4;

    move-result-object v0

    return-object v0
.end method

.method public final g(I)I
    .locals 1

    .prologue
    .line 255669
    iget-object v0, p0, LX/1UF;->a:LX/1Qr;

    invoke-interface {v0, p1}, LX/1Qr;->g(I)I

    move-result v0

    return v0
.end method

.method public final h_(I)I
    .locals 1

    .prologue
    .line 255670
    iget-object v0, p0, LX/1UF;->a:LX/1Qr;

    invoke-interface {v0, p1}, LX/1Qr;->h_(I)I

    move-result v0

    return v0
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 255671
    iget-object v0, p0, LX/1UF;->a:LX/1Qr;

    invoke-interface {v0}, LX/0Vf;->isDisposed()Z

    move-result v0

    return v0
.end method

.method public final m_(I)I
    .locals 1

    .prologue
    .line 255672
    iget-object v0, p0, LX/1UF;->a:LX/1Qr;

    invoke-interface {v0, p1}, LX/1Qr;->m_(I)I

    move-result v0

    return v0
.end method

.method public final n_(I)I
    .locals 1

    .prologue
    .line 255673
    iget-object v0, p0, LX/1UF;->a:LX/1Qr;

    invoke-interface {v0, p1}, LX/1Qr;->n_(I)I

    move-result v0

    return v0
.end method
