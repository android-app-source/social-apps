.class public LX/1kS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static e:LX/0Xm;


# instance fields
.field private final c:LX/03V;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1lP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 309668
    const-class v0, LX/1kS;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1kS;->a:Ljava/lang/String;

    .line 309669
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->NEUTRAL:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1kS;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 309664
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309665
    iput-object p1, p0, LX/1kS;->c:LX/03V;

    .line 309666
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1kS;->d:Ljava/util/Map;

    .line 309667
    return-void
.end method

.method public static a(LX/0QB;)LX/1kS;
    .locals 4

    .prologue
    .line 309653
    const-class v1, LX/1kS;

    monitor-enter v1

    .line 309654
    :try_start_0
    sget-object v0, LX/1kS;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 309655
    sput-object v2, LX/1kS;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 309656
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309657
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 309658
    new-instance p0, LX/1kS;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/1kS;-><init>(LX/03V;)V

    .line 309659
    move-object v0, p0

    .line 309660
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 309661
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1kS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 309662
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 309663
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/1kK;)LX/1lP;
    .locals 5

    .prologue
    .line 309647
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1kS;->d:Ljava/util/Map;

    invoke-interface {p1}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309648
    iget-object v0, p0, LX/1kS;->d:Ljava/util/Map;

    invoke-interface {p1}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lP;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 309649
    :goto_0
    monitor-exit p0

    return-object v0

    .line 309650
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1kS;->c:LX/03V;

    sget-object v1, LX/1kS;->a:Ljava/lang/String;

    const-string v2, "prompt type not found: %s. Client prompt types stored: %s"

    invoke-interface {p1}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/1kS;->d:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 309651
    const-string v0, "ranking_unavailable"

    invoke-interface {p1}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v1

    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    sget-object v4, LX/1kS;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, LX/1lP;->a(Ljava/lang/String;Ljava/lang/String;DLjava/lang/String;)LX/1lP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 309652
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;)V
    .locals 10
    .param p1    # Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 309639
    monitor-enter p0

    if-nez p1, :cond_1

    .line 309640
    :cond_0
    monitor-exit p0

    return-void

    .line 309641
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ClientProductionPromptsInfoModel;

    .line 309642
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ClientProductionPromptsInfoModel;->k()Lcom/facebook/graphql/enums/GraphQLPromptType;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 309643
    iget-object v4, p0, LX/1kS;->d:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ClientProductionPromptsInfoModel;->k()Lcom/facebook/graphql/enums/GraphQLPromptType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 309644
    iget-object v4, p0, LX/1kS;->d:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ClientProductionPromptsInfoModel;->k()Lcom/facebook/graphql/enums/GraphQLPromptType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ClientProductionPromptsInfoModel;->m()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ClientProductionPromptsInfoModel;->k()Lcom/facebook/graphql/enums/GraphQLPromptType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ClientProductionPromptsInfoModel;->l()D

    move-result-wide v8

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ClientProductionPromptsInfoModel;->j()Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v7, v8, v9, v0}, LX/1lP;->a(Ljava/lang/String;Ljava/lang/String;DLjava/lang/String;)LX/1lP;

    move-result-object v0

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 309645
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 309646
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
