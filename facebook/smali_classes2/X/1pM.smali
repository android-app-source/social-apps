.class public final LX/1pM;
.super LX/15u;
.source ""


# static fields
.field private static final T:[I

.field private static final U:[I


# instance fields
.field public M:LX/0lD;

.field public final N:LX/0lv;

.field public O:[I

.field public P:Z

.field public Q:Ljava/io/InputStream;

.field public R:[B

.field public S:Z

.field private V:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 329027
    sget-object v0, LX/12H;->b:[I

    move-object v0, v0

    .line 329028
    sput-object v0, LX/1pM;->T:[I

    .line 329029
    sget-object v0, LX/12H;->a:[I

    move-object v0, v0

    .line 329030
    sput-object v0, LX/1pM;->U:[I

    return-void
.end method

.method public constructor <init>(LX/12A;ILjava/io/InputStream;LX/0lD;LX/0lv;[BIIZ)V
    .locals 1

    .prologue
    .line 329016
    invoke-direct {p0, p1, p2}, LX/15u;-><init>(LX/12A;I)V

    .line 329017
    const/16 v0, 0x10

    new-array v0, v0, [I

    iput-object v0, p0, LX/1pM;->O:[I

    .line 329018
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1pM;->P:Z

    .line 329019
    iput-object p3, p0, LX/1pM;->Q:Ljava/io/InputStream;

    .line 329020
    iput-object p4, p0, LX/1pM;->M:LX/0lD;

    .line 329021
    iput-object p5, p0, LX/1pM;->N:LX/0lv;

    .line 329022
    iput-object p6, p0, LX/1pM;->R:[B

    .line 329023
    iput p7, p0, LX/1pM;->d:I

    .line 329024
    iput p8, p0, LX/1pM;->e:I

    .line 329025
    iput-boolean p9, p0, LX/1pM;->S:Z

    .line 329026
    return-void
.end method

.method private V()LX/15z;
    .locals 4

    .prologue
    .line 329008
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1pM;->p:Z

    .line 329009
    iget-object v0, p0, LX/15u;->m:LX/15z;

    .line 329010
    const/4 v1, 0x0

    iput-object v1, p0, LX/1pM;->m:LX/15z;

    .line 329011
    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v1, :cond_1

    .line 329012
    iget-object v1, p0, LX/15u;->l:LX/15y;

    iget v2, p0, LX/15u;->j:I

    iget v3, p0, LX/15u;->k:I

    invoke-virtual {v1, v2, v3}, LX/15y;->b(II)LX/15y;

    move-result-object v1

    iput-object v1, p0, LX/1pM;->l:LX/15y;

    .line 329013
    :cond_0
    :goto_0
    iput-object v0, p0, LX/1pM;->K:LX/15z;

    return-object v0

    .line 329014
    :cond_1
    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_0

    .line 329015
    iget-object v1, p0, LX/15u;->l:LX/15y;

    iget v2, p0, LX/15u;->j:I

    iget v3, p0, LX/15u;->k:I

    invoke-virtual {v1, v2, v3}, LX/15y;->c(II)LX/15y;

    move-result-object v1

    iput-object v1, p0, LX/1pM;->l:LX/15y;

    goto :goto_0
.end method

.method private W()I
    .locals 5

    .prologue
    const/16 v4, 0x39

    const/16 v1, 0x30

    .line 328993
    iget v0, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v0, v2, :cond_1

    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 328994
    :cond_0
    :goto_0
    return v0

    .line 328995
    :cond_1
    iget-object v0, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    .line 328996
    if-lt v0, v1, :cond_2

    if-le v0, v4, :cond_3

    :cond_2
    move v0, v1

    .line 328997
    goto :goto_0

    .line 328998
    :cond_3
    sget-object v2, LX/0lr;->ALLOW_NUMERIC_LEADING_ZEROS:LX/0lr;

    invoke-virtual {p0, v2}, LX/15w;->a(LX/0lr;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 328999
    const-string v2, "Leading zeroes not allowed"

    invoke-virtual {p0, v2}, LX/15u;->c(Ljava/lang/String;)V

    .line 329000
    :cond_4
    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/1pM;->d:I

    .line 329001
    if-ne v0, v1, :cond_0

    .line 329002
    :cond_5
    iget v2, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v2, v3, :cond_6

    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 329003
    :cond_6
    iget-object v0, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    .line 329004
    if-lt v0, v1, :cond_7

    if-le v0, v4, :cond_8

    :cond_7
    move v0, v1

    .line 329005
    goto :goto_0

    .line 329006
    :cond_8
    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/1pM;->d:I

    .line 329007
    if-eq v0, v1, :cond_5

    goto :goto_0
.end method

.method private X()LX/0lx;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 328986
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    .line 328987
    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v0

    if-nez v0, :cond_0

    .line 328988
    const-string v0, ": was expecting closing \'\"\' for name"

    invoke-virtual {p0, v0}, LX/15v;->d(Ljava/lang/String;)V

    .line 328989
    :cond_0
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v0, v0, v1

    and-int/lit16 v4, v0, 0xff

    .line 328990
    const/16 v0, 0x22

    if-ne v4, v0, :cond_1

    .line 328991
    sget-object v0, LX/2CO;->c:LX/2CO;

    move-object v0, v0

    .line 328992
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, LX/1pM;->O:[I

    move-object v0, p0

    move v3, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, LX/1pM;->a([IIIII)LX/0lx;

    move-result-object v0

    goto :goto_0
.end method

.method private Y()LX/0lx;
    .locals 12

    .prologue
    const/16 v10, 0x27

    const/4 v9, 0x4

    const/4 v1, 0x0

    .line 328937
    iget v0, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v0, v2, :cond_0

    .line 328938
    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v0

    if-nez v0, :cond_0

    .line 328939
    const-string v0, ": was expecting closing \'\'\' for name"

    invoke-virtual {p0, v0}, LX/15v;->d(Ljava/lang/String;)V

    .line 328940
    :cond_0
    iget-object v0, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v0, v0, v2

    and-int/lit16 v5, v0, 0xff

    .line 328941
    if-ne v5, v10, :cond_1

    .line 328942
    sget-object v0, LX/2CO;->c:LX/2CO;

    move-object v0, v0

    .line 328943
    :goto_0
    return-object v0

    .line 328944
    :cond_1
    iget-object v0, p0, LX/1pM;->O:[I

    .line 328945
    sget-object v7, LX/1pM;->U:[I

    move v3, v1

    move v4, v1

    move v2, v1

    .line 328946
    :goto_1
    if-eq v5, v10, :cond_9

    .line 328947
    const/16 v6, 0x22

    if-eq v5, v6, :cond_f

    aget v6, v7, v5

    if-eqz v6, :cond_f

    .line 328948
    const/16 v6, 0x5c

    if-eq v5, v6, :cond_4

    .line 328949
    const-string v6, "name"

    invoke-virtual {p0, v5, v6}, LX/15v;->c(ILjava/lang/String;)V

    .line 328950
    :goto_2
    const/16 v6, 0x7f

    if-le v5, v6, :cond_f

    .line 328951
    if-lt v3, v9, :cond_e

    .line 328952
    array-length v3, v0

    if-lt v2, v3, :cond_2

    .line 328953
    array-length v3, v0

    invoke-static {v0, v3}, LX/1pM;->a([II)[I

    move-result-object v0

    iput-object v0, p0, LX/1pM;->O:[I

    .line 328954
    :cond_2
    add-int/lit8 v3, v2, 0x1

    aput v4, v0, v2

    move v2, v1

    move v4, v3

    move v3, v1

    .line 328955
    :goto_3
    const/16 v6, 0x800

    if-ge v5, v6, :cond_5

    .line 328956
    shl-int/lit8 v3, v3, 0x8

    shr-int/lit8 v6, v5, 0x6

    or-int/lit16 v6, v6, 0xc0

    or-int/2addr v3, v6

    .line 328957
    add-int/lit8 v2, v2, 0x1

    move v11, v2

    move v2, v3

    move-object v3, v0

    move v0, v11

    .line 328958
    :goto_4
    and-int/lit8 v5, v5, 0x3f

    or-int/lit16 v5, v5, 0x80

    move v6, v2

    move v2, v0

    move-object v0, v3

    move v3, v5

    .line 328959
    :goto_5
    if-ge v2, v9, :cond_7

    .line 328960
    add-int/lit8 v2, v2, 0x1

    .line 328961
    shl-int/lit8 v5, v6, 0x8

    or-int/2addr v3, v5

    move v11, v2

    move v2, v3

    move v3, v4

    move-object v4, v0

    move v0, v11

    .line 328962
    :goto_6
    iget v5, p0, LX/15u;->d:I

    iget v6, p0, LX/15u;->e:I

    if-lt v5, v6, :cond_3

    .line 328963
    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v5

    if-nez v5, :cond_3

    .line 328964
    const-string v5, " in field name"

    invoke-virtual {p0, v5}, LX/15v;->d(Ljava/lang/String;)V

    .line 328965
    :cond_3
    iget-object v5, p0, LX/1pM;->R:[B

    iget v6, p0, LX/15u;->d:I

    add-int/lit8 v8, v6, 0x1

    iput v8, p0, LX/1pM;->d:I

    aget-byte v5, v5, v6

    and-int/lit16 v5, v5, 0xff

    move v11, v0

    move-object v0, v4

    move v4, v2

    move v2, v3

    move v3, v11

    goto :goto_1

    .line 328966
    :cond_4
    invoke-virtual {p0}, LX/1pM;->R()C

    move-result v5

    goto :goto_2

    .line 328967
    :cond_5
    shl-int/lit8 v3, v3, 0x8

    shr-int/lit8 v6, v5, 0xc

    or-int/lit16 v6, v6, 0xe0

    or-int/2addr v3, v6

    .line 328968
    add-int/lit8 v2, v2, 0x1

    .line 328969
    if-lt v2, v9, :cond_d

    .line 328970
    array-length v2, v0

    if-lt v4, v2, :cond_6

    .line 328971
    array-length v2, v0

    invoke-static {v0, v2}, LX/1pM;->a([II)[I

    move-result-object v0

    iput-object v0, p0, LX/1pM;->O:[I

    .line 328972
    :cond_6
    add-int/lit8 v2, v4, 0x1

    aput v3, v0, v4

    move v3, v2

    move-object v4, v0

    move v0, v1

    move v2, v1

    .line 328973
    :goto_7
    shl-int/lit8 v2, v2, 0x8

    shr-int/lit8 v6, v5, 0x6

    and-int/lit8 v6, v6, 0x3f

    or-int/lit16 v6, v6, 0x80

    or-int/2addr v2, v6

    .line 328974
    add-int/lit8 v0, v0, 0x1

    move v11, v3

    move-object v3, v4

    move v4, v11

    goto :goto_4

    .line 328975
    :cond_7
    array-length v2, v0

    if-lt v4, v2, :cond_8

    .line 328976
    array-length v2, v0

    invoke-static {v0, v2}, LX/1pM;->a([II)[I

    move-result-object v0

    iput-object v0, p0, LX/1pM;->O:[I

    .line 328977
    :cond_8
    add-int/lit8 v5, v4, 0x1

    aput v6, v0, v4

    .line 328978
    const/4 v2, 0x1

    move-object v4, v0

    move v0, v2

    move v2, v3

    move v3, v5

    goto :goto_6

    .line 328979
    :cond_9
    if-lez v3, :cond_c

    .line 328980
    array-length v1, v0

    if-lt v2, v1, :cond_a

    .line 328981
    array-length v1, v0

    invoke-static {v0, v1}, LX/1pM;->a([II)[I

    move-result-object v0

    iput-object v0, p0, LX/1pM;->O:[I

    .line 328982
    :cond_a
    add-int/lit8 v1, v2, 0x1

    aput v4, v0, v2

    move v11, v1

    move-object v1, v0

    move v0, v11

    .line 328983
    :goto_8
    iget-object v2, p0, LX/1pM;->N:LX/0lv;

    invoke-virtual {v2, v1, v0}, LX/0lv;->a([II)LX/0lx;

    move-result-object v2

    .line 328984
    if-nez v2, :cond_b

    .line 328985
    invoke-direct {p0, v1, v0, v3}, LX/1pM;->a([III)LX/0lx;

    move-result-object v0

    goto/16 :goto_0

    :cond_b
    move-object v0, v2

    goto/16 :goto_0

    :cond_c
    move-object v1, v0

    move v0, v2

    goto :goto_8

    :cond_d
    move v11, v2

    move v2, v3

    move v3, v4

    move-object v4, v0

    move v0, v11

    goto :goto_7

    :cond_e
    move v11, v3

    move v3, v4

    move v4, v2

    move v2, v11

    goto/16 :goto_3

    :cond_f
    move v6, v4

    move v4, v2

    move v2, v3

    move v3, v5

    goto/16 :goto_5
.end method

.method private Z()V
    .locals 6

    .prologue
    .line 328913
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1pM;->P:Z

    .line 328914
    sget-object v3, LX/1pM;->T:[I

    .line 328915
    iget-object v4, p0, LX/1pM;->R:[B

    .line 328916
    :goto_0
    iget v1, p0, LX/15u;->d:I

    .line 328917
    iget v0, p0, LX/15u;->e:I

    .line 328918
    if-lt v1, v0, :cond_0

    .line 328919
    invoke-virtual {p0}, LX/15u;->K()V

    .line 328920
    iget v1, p0, LX/15u;->d:I

    .line 328921
    iget v0, p0, LX/15u;->e:I

    .line 328922
    :cond_0
    :goto_1
    if-ge v1, v0, :cond_1

    .line 328923
    add-int/lit8 v2, v1, 0x1

    aget-byte v1, v4, v1

    and-int/lit16 v1, v1, 0xff

    .line 328924
    aget v5, v3, v1

    if-eqz v5, :cond_4

    .line 328925
    iput v2, p0, LX/1pM;->d:I

    .line 328926
    const/16 v0, 0x22

    if-eq v1, v0, :cond_3

    .line 328927
    aget v0, v3, v1

    packed-switch v0, :pswitch_data_0

    .line 328928
    const/16 v0, 0x20

    if-ge v1, v0, :cond_2

    .line 328929
    const-string v0, "string value"

    invoke-virtual {p0, v1, v0}, LX/15v;->c(ILjava/lang/String;)V

    goto :goto_0

    .line 328930
    :cond_1
    iput v1, p0, LX/1pM;->d:I

    goto :goto_0

    .line 328931
    :pswitch_0
    invoke-virtual {p0}, LX/1pM;->R()C

    goto :goto_0

    .line 328932
    :pswitch_1
    invoke-direct {p0}, LX/1pM;->ag()V

    goto :goto_0

    .line 328933
    :pswitch_2
    invoke-direct {p0}, LX/1pM;->ah()V

    goto :goto_0

    .line 328934
    :pswitch_3
    invoke-direct {p0}, LX/1pM;->ai()V

    goto :goto_0

    .line 328935
    :cond_2
    invoke-direct {p0, v1}, LX/1pM;->q(I)V

    goto :goto_0

    .line 328936
    :cond_3
    return-void

    :cond_4
    move v1, v2

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(II)LX/0lx;
    .locals 2

    .prologue
    .line 328908
    iget-object v0, p0, LX/1pM;->N:LX/0lv;

    invoke-virtual {v0, p1}, LX/0lv;->a(I)LX/0lx;

    move-result-object v0

    .line 328909
    if-eqz v0, :cond_0

    .line 328910
    :goto_0
    return-object v0

    .line 328911
    :cond_0
    iget-object v0, p0, LX/1pM;->O:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 328912
    iget-object v0, p0, LX/1pM;->O:[I

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, p2}, LX/1pM;->a([III)LX/0lx;

    move-result-object v0

    goto :goto_0
.end method

.method private a(III)LX/0lx;
    .locals 6

    .prologue
    .line 328670
    iget-object v1, p0, LX/1pM;->O:[I

    const/4 v2, 0x0

    move-object v0, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, LX/1pM;->a([IIIII)LX/0lx;

    move-result-object v0

    return-object v0
.end method

.method private a(IIII)LX/0lx;
    .locals 6

    .prologue
    .line 328903
    iget-object v0, p0, LX/1pM;->O:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 328904
    iget-object v1, p0, LX/1pM;->O:[I

    const/4 v2, 0x1

    move-object v0, p0

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/1pM;->a([IIIII)LX/0lx;

    move-result-object v0

    return-object v0
.end method

.method private a(I[I)LX/0lx;
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/16 v4, 0x22

    .line 328876
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/1pM;->d:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 328877
    aget v1, p2, v0

    if-eqz v1, :cond_1

    .line 328878
    if-ne v0, v4, :cond_0

    .line 328879
    iget v0, p0, LX/1pM;->V:I

    invoke-direct {p0, v0, p1, v5}, LX/1pM;->b(III)LX/0lx;

    move-result-object v0

    .line 328880
    :goto_0
    return-object v0

    .line 328881
    :cond_0
    iget v1, p0, LX/1pM;->V:I

    invoke-direct {p0, v1, p1, v0, v5}, LX/1pM;->a(IIII)LX/0lx;

    move-result-object v0

    goto :goto_0

    .line 328882
    :cond_1
    shl-int/lit8 v1, p1, 0x8

    or-int/2addr v0, v1

    .line 328883
    iget-object v1, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    .line 328884
    aget v2, p2, v1

    if-eqz v2, :cond_3

    .line 328885
    if-ne v1, v4, :cond_2

    .line 328886
    iget v1, p0, LX/1pM;->V:I

    invoke-direct {p0, v1, v0, v6}, LX/1pM;->b(III)LX/0lx;

    move-result-object v0

    goto :goto_0

    .line 328887
    :cond_2
    iget v2, p0, LX/1pM;->V:I

    invoke-direct {p0, v2, v0, v1, v6}, LX/1pM;->a(IIII)LX/0lx;

    move-result-object v0

    goto :goto_0

    .line 328888
    :cond_3
    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v0, v1

    .line 328889
    iget-object v1, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    .line 328890
    aget v2, p2, v1

    if-eqz v2, :cond_5

    .line 328891
    if-ne v1, v4, :cond_4

    .line 328892
    iget v1, p0, LX/1pM;->V:I

    invoke-direct {p0, v1, v0, v7}, LX/1pM;->b(III)LX/0lx;

    move-result-object v0

    goto :goto_0

    .line 328893
    :cond_4
    iget v2, p0, LX/1pM;->V:I

    invoke-direct {p0, v2, v0, v1, v7}, LX/1pM;->a(IIII)LX/0lx;

    move-result-object v0

    goto :goto_0

    .line 328894
    :cond_5
    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v0, v1

    .line 328895
    iget-object v1, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    .line 328896
    aget v2, p2, v1

    if-eqz v2, :cond_7

    .line 328897
    if-ne v1, v4, :cond_6

    .line 328898
    iget v1, p0, LX/1pM;->V:I

    invoke-direct {p0, v1, v0, v8}, LX/1pM;->b(III)LX/0lx;

    move-result-object v0

    goto :goto_0

    .line 328899
    :cond_6
    iget v2, p0, LX/1pM;->V:I

    invoke-direct {p0, v2, v0, v1, v8}, LX/1pM;->a(IIII)LX/0lx;

    move-result-object v0

    goto :goto_0

    .line 328900
    :cond_7
    iget-object v2, p0, LX/1pM;->O:[I

    const/4 v3, 0x0

    iget v4, p0, LX/1pM;->V:I

    aput v4, v2, v3

    .line 328901
    iget-object v2, p0, LX/1pM;->O:[I

    aput v0, v2, v5

    .line 328902
    invoke-direct {p0, v1}, LX/1pM;->i(I)LX/0lx;

    move-result-object v0

    goto :goto_0
.end method

.method private a([III)LX/0lx;
    .locals 11

    .prologue
    .line 328813
    shl-int/lit8 v0, p2, 0x2

    add-int/lit8 v0, v0, -0x4

    add-int v6, v0, p3

    .line 328814
    const/4 v0, 0x4

    if-ge p3, v0, :cond_7

    .line 328815
    add-int/lit8 v0, p2, -0x1

    aget v0, p1, v0

    .line 328816
    add-int/lit8 v1, p2, -0x1

    rsub-int/lit8 v2, p3, 0x4

    shl-int/lit8 v2, v2, 0x3

    shl-int v2, v0, v2

    aput v2, p1, v1

    .line 328817
    :goto_0
    iget-object v1, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v1}, LX/15x;->l()[C

    move-result-object v1

    .line 328818
    const/4 v5, 0x0

    .line 328819
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_b

    .line 328820
    shr-int/lit8 v2, v3, 0x2

    aget v2, p1, v2

    .line 328821
    and-int/lit8 v4, v3, 0x3

    .line 328822
    rsub-int/lit8 v4, v4, 0x3

    shl-int/lit8 v4, v4, 0x3

    shr-int/2addr v2, v4

    and-int/lit16 v2, v2, 0xff

    .line 328823
    add-int/lit8 v3, v3, 0x1

    .line 328824
    const/16 v4, 0x7f

    if-le v2, v4, :cond_d

    .line 328825
    and-int/lit16 v4, v2, 0xe0

    const/16 v7, 0xc0

    if-ne v4, v7, :cond_8

    .line 328826
    and-int/lit8 v4, v2, 0x1f

    .line 328827
    const/4 v2, 0x1

    move v10, v2

    move v2, v4

    move v4, v10

    .line 328828
    :goto_2
    add-int v7, v3, v4

    if-le v7, v6, :cond_0

    .line 328829
    const-string v7, " in field name"

    invoke-virtual {p0, v7}, LX/15v;->d(Ljava/lang/String;)V

    .line 328830
    :cond_0
    shr-int/lit8 v7, v3, 0x2

    aget v7, p1, v7

    .line 328831
    and-int/lit8 v8, v3, 0x3

    .line 328832
    rsub-int/lit8 v8, v8, 0x3

    shl-int/lit8 v8, v8, 0x3

    shr-int/2addr v7, v8

    .line 328833
    add-int/lit8 v3, v3, 0x1

    .line 328834
    and-int/lit16 v8, v7, 0xc0

    const/16 v9, 0x80

    if-eq v8, v9, :cond_1

    .line 328835
    invoke-direct {p0, v7}, LX/1pM;->s(I)V

    .line 328836
    :cond_1
    shl-int/lit8 v2, v2, 0x6

    and-int/lit8 v7, v7, 0x3f

    or-int/2addr v2, v7

    .line 328837
    const/4 v7, 0x1

    if-le v4, v7, :cond_4

    .line 328838
    shr-int/lit8 v7, v3, 0x2

    aget v7, p1, v7

    .line 328839
    and-int/lit8 v8, v3, 0x3

    .line 328840
    rsub-int/lit8 v8, v8, 0x3

    shl-int/lit8 v8, v8, 0x3

    shr-int/2addr v7, v8

    .line 328841
    add-int/lit8 v3, v3, 0x1

    .line 328842
    and-int/lit16 v8, v7, 0xc0

    const/16 v9, 0x80

    if-eq v8, v9, :cond_2

    .line 328843
    invoke-direct {p0, v7}, LX/1pM;->s(I)V

    .line 328844
    :cond_2
    shl-int/lit8 v2, v2, 0x6

    and-int/lit8 v7, v7, 0x3f

    or-int/2addr v2, v7

    .line 328845
    const/4 v7, 0x2

    if-le v4, v7, :cond_4

    .line 328846
    shr-int/lit8 v7, v3, 0x2

    aget v7, p1, v7

    .line 328847
    and-int/lit8 v8, v3, 0x3

    .line 328848
    rsub-int/lit8 v8, v8, 0x3

    shl-int/lit8 v8, v8, 0x3

    shr-int/2addr v7, v8

    .line 328849
    add-int/lit8 v3, v3, 0x1

    .line 328850
    and-int/lit16 v8, v7, 0xc0

    const/16 v9, 0x80

    if-eq v8, v9, :cond_3

    .line 328851
    and-int/lit16 v8, v7, 0xff

    invoke-direct {p0, v8}, LX/1pM;->s(I)V

    .line 328852
    :cond_3
    shl-int/lit8 v2, v2, 0x6

    and-int/lit8 v7, v7, 0x3f

    or-int/2addr v2, v7

    .line 328853
    :cond_4
    const/4 v7, 0x2

    if-le v4, v7, :cond_d

    .line 328854
    const/high16 v4, 0x10000

    sub-int/2addr v2, v4

    .line 328855
    array-length v4, v1

    if-lt v5, v4, :cond_5

    .line 328856
    iget-object v1, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v1}, LX/15x;->o()[C

    move-result-object v1

    .line 328857
    :cond_5
    add-int/lit8 v4, v5, 0x1

    const v7, 0xd800

    shr-int/lit8 v8, v2, 0xa

    add-int/2addr v7, v8

    int-to-char v7, v7

    aput-char v7, v1, v5

    .line 328858
    const v5, 0xdc00

    and-int/lit16 v2, v2, 0x3ff

    or-int/2addr v2, v5

    move v10, v2

    move v2, v3

    move v3, v4

    move-object v4, v1

    move v1, v10

    .line 328859
    :goto_3
    array-length v5, v4

    if-lt v3, v5, :cond_6

    .line 328860
    iget-object v4, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v4}, LX/15x;->o()[C

    move-result-object v4

    .line 328861
    :cond_6
    add-int/lit8 v5, v3, 0x1

    int-to-char v1, v1

    aput-char v1, v4, v3

    move v3, v2

    move-object v1, v4

    .line 328862
    goto/16 :goto_1

    .line 328863
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 328864
    :cond_8
    and-int/lit16 v4, v2, 0xf0

    const/16 v7, 0xe0

    if-ne v4, v7, :cond_9

    .line 328865
    and-int/lit8 v4, v2, 0xf

    .line 328866
    const/4 v2, 0x2

    move v10, v2

    move v2, v4

    move v4, v10

    goto/16 :goto_2

    .line 328867
    :cond_9
    and-int/lit16 v4, v2, 0xf8

    const/16 v7, 0xf0

    if-ne v4, v7, :cond_a

    .line 328868
    and-int/lit8 v4, v2, 0x7

    .line 328869
    const/4 v2, 0x3

    move v10, v2

    move v2, v4

    move v4, v10

    goto/16 :goto_2

    .line 328870
    :cond_a
    invoke-direct {p0, v2}, LX/1pM;->r(I)V

    .line 328871
    const/4 v2, 0x1

    move v4, v2

    goto/16 :goto_2

    .line 328872
    :cond_b
    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3, v5}, Ljava/lang/String;-><init>([CII)V

    .line 328873
    const/4 v1, 0x4

    if-ge p3, v1, :cond_c

    .line 328874
    add-int/lit8 v1, p2, -0x1

    aput v0, p1, v1

    .line 328875
    :cond_c
    iget-object v0, p0, LX/1pM;->N:LX/0lv;

    invoke-virtual {v0, v2, p1, p2}, LX/0lv;->a(Ljava/lang/String;[II)LX/0lx;

    move-result-object v0

    return-object v0

    :cond_d
    move-object v4, v1

    move v1, v2

    move v2, v3

    move v3, v5

    goto :goto_3
.end method

.method private a([IIII)LX/0lx;
    .locals 2

    .prologue
    .line 328806
    array-length v0, p1

    if-lt p2, v0, :cond_0

    .line 328807
    array-length v0, p1

    invoke-static {p1, v0}, LX/1pM;->a([II)[I

    move-result-object p1

    iput-object p1, p0, LX/1pM;->O:[I

    .line 328808
    :cond_0
    add-int/lit8 v1, p2, 0x1

    aput p3, p1, p2

    .line 328809
    iget-object v0, p0, LX/1pM;->N:LX/0lv;

    invoke-virtual {v0, p1, v1}, LX/0lv;->a([II)LX/0lx;

    move-result-object v0

    .line 328810
    if-nez v0, :cond_1

    .line 328811
    invoke-direct {p0, p1, v1, p4}, LX/1pM;->a([III)LX/0lx;

    move-result-object v0

    .line 328812
    :cond_1
    return-object v0
.end method

.method private a([IIIII)LX/0lx;
    .locals 9

    .prologue
    const/4 v7, 0x4

    const/4 v1, 0x0

    .line 328764
    sget-object v5, LX/1pM;->U:[I

    .line 328765
    :goto_0
    aget v0, v5, p4

    if-eqz v0, :cond_d

    .line 328766
    const/16 v0, 0x22

    if-eq p4, v0, :cond_7

    .line 328767
    const/16 v0, 0x5c

    if-eq p4, v0, :cond_2

    .line 328768
    const-string v0, "name"

    invoke-virtual {p0, p4, v0}, LX/15v;->c(ILjava/lang/String;)V

    .line 328769
    :goto_1
    const/16 v0, 0x7f

    if-le p4, v0, :cond_d

    .line 328770
    if-lt p5, v7, :cond_c

    .line 328771
    array-length v0, p1

    if-lt p2, v0, :cond_0

    .line 328772
    array-length v0, p1

    invoke-static {p1, v0}, LX/1pM;->a([II)[I

    move-result-object p1

    iput-object p1, p0, LX/1pM;->O:[I

    .line 328773
    :cond_0
    add-int/lit8 v4, p2, 0x1

    aput p3, p1, p2

    move p5, v1

    move p3, v1

    move-object v0, p1

    .line 328774
    :goto_2
    const/16 v2, 0x800

    if-ge p4, v2, :cond_3

    .line 328775
    shl-int/lit8 v2, p3, 0x8

    shr-int/lit8 v3, p4, 0x6

    or-int/lit16 v3, v3, 0xc0

    or-int/2addr v3, v2

    .line 328776
    add-int/lit8 v2, p5, 0x1

    move v8, v2

    move v2, v3

    move-object v3, v0

    move v0, v8

    .line 328777
    :goto_3
    and-int/lit8 v6, p4, 0x3f

    or-int/lit16 p3, v6, 0x80

    move p5, v0

    move p2, v4

    move-object v0, v3

    move v3, v2

    .line 328778
    :goto_4
    if-ge p5, v7, :cond_5

    .line 328779
    add-int/lit8 p5, p5, 0x1

    .line 328780
    shl-int/lit8 v2, v3, 0x8

    or-int/2addr p3, v2

    move-object p1, v0

    .line 328781
    :goto_5
    iget v0, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v0, v2, :cond_1

    .line 328782
    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v0

    if-nez v0, :cond_1

    .line 328783
    const-string v0, " in field name"

    invoke-virtual {p0, v0}, LX/15v;->d(Ljava/lang/String;)V

    .line 328784
    :cond_1
    iget-object v0, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v0, v0, v2

    and-int/lit16 p4, v0, 0xff

    goto :goto_0

    .line 328785
    :cond_2
    invoke-virtual {p0}, LX/1pM;->R()C

    move-result p4

    goto :goto_1

    .line 328786
    :cond_3
    shl-int/lit8 v2, p3, 0x8

    shr-int/lit8 v3, p4, 0xc

    or-int/lit16 v3, v3, 0xe0

    or-int/2addr v3, v2

    .line 328787
    add-int/lit8 v2, p5, 0x1

    .line 328788
    if-lt v2, v7, :cond_b

    .line 328789
    array-length v2, v0

    if-lt v4, v2, :cond_4

    .line 328790
    array-length v2, v0

    invoke-static {v0, v2}, LX/1pM;->a([II)[I

    move-result-object v0

    iput-object v0, p0, LX/1pM;->O:[I

    .line 328791
    :cond_4
    add-int/lit8 v2, v4, 0x1

    aput v3, v0, v4

    move v3, v2

    move-object v4, v0

    move v0, v1

    move v2, v1

    .line 328792
    :goto_6
    shl-int/lit8 v2, v2, 0x8

    shr-int/lit8 v6, p4, 0x6

    and-int/lit8 v6, v6, 0x3f

    or-int/lit16 v6, v6, 0x80

    or-int/2addr v2, v6

    .line 328793
    add-int/lit8 v0, v0, 0x1

    move v8, v3

    move-object v3, v4

    move v4, v8

    goto :goto_3

    .line 328794
    :cond_5
    array-length v2, v0

    if-lt p2, v2, :cond_6

    .line 328795
    array-length v2, v0

    invoke-static {v0, v2}, LX/1pM;->a([II)[I

    move-result-object v0

    iput-object v0, p0, LX/1pM;->O:[I

    .line 328796
    :cond_6
    add-int/lit8 v2, p2, 0x1

    aput v3, v0, p2

    .line 328797
    const/4 p5, 0x1

    move p2, v2

    move-object p1, v0

    goto :goto_5

    .line 328798
    :cond_7
    if-lez p5, :cond_9

    .line 328799
    array-length v0, p1

    if-lt p2, v0, :cond_8

    .line 328800
    array-length v0, p1

    invoke-static {p1, v0}, LX/1pM;->a([II)[I

    move-result-object p1

    iput-object p1, p0, LX/1pM;->O:[I

    .line 328801
    :cond_8
    add-int/lit8 v0, p2, 0x1

    aput p3, p1, p2

    move p2, v0

    .line 328802
    :cond_9
    iget-object v0, p0, LX/1pM;->N:LX/0lv;

    invoke-virtual {v0, p1, p2}, LX/0lv;->a([II)LX/0lx;

    move-result-object v0

    .line 328803
    if-nez v0, :cond_a

    .line 328804
    invoke-direct {p0, p1, p2, p5}, LX/1pM;->a([III)LX/0lx;

    move-result-object v0

    .line 328805
    :cond_a
    return-object v0

    :cond_b
    move v8, v2

    move v2, v3

    move v3, v4

    move-object v4, v0

    move v0, v8

    goto :goto_6

    :cond_c
    move v4, p2

    move-object v0, p1

    goto/16 :goto_2

    :cond_d
    move v3, p3

    move-object v0, p1

    move p3, p4

    goto/16 :goto_4
.end method

.method private a(IZ)LX/15z;
    .locals 4

    .prologue
    .line 328745
    move v0, p1

    :goto_0
    const/16 v1, 0x49

    if-ne v0, v1, :cond_7

    .line 328746
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    .line 328747
    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v0

    if-nez v0, :cond_0

    .line 328748
    invoke-virtual {p0}, LX/15v;->T()V

    .line 328749
    :cond_0
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/1pM;->d:I

    aget-byte v1, v0, v1

    .line 328750
    const/16 v0, 0x4e

    if-ne v1, v0, :cond_2

    .line 328751
    if-eqz p2, :cond_1

    const-string v0, "-INF"

    .line 328752
    :goto_1
    const/4 v2, 0x3

    invoke-direct {p0, v0, v2}, LX/1pM;->a(Ljava/lang/String;I)V

    .line 328753
    sget-object v2, LX/0lr;->ALLOW_NON_NUMERIC_NUMBERS:LX/0lr;

    invoke-virtual {p0, v2}, LX/15w;->a(LX/0lr;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 328754
    if-eqz p2, :cond_4

    const-wide/high16 v2, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    :goto_2
    invoke-virtual {p0, v0, v2, v3}, LX/15u;->a(Ljava/lang/String;D)LX/15z;

    move-result-object v0

    .line 328755
    :goto_3
    return-object v0

    .line 328756
    :cond_1
    const-string v0, "+INF"

    goto :goto_1

    .line 328757
    :cond_2
    const/16 v0, 0x6e

    if-ne v1, v0, :cond_6

    .line 328758
    if-eqz p2, :cond_3

    const-string v0, "-Infinity"

    goto :goto_1

    :cond_3
    const-string v0, "+Infinity"

    goto :goto_1

    .line 328759
    :cond_4
    const-wide/high16 v2, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    goto :goto_2

    .line 328760
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Non-standard token \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    move v0, v1

    .line 328761
    goto :goto_0

    :cond_6
    move v0, v1

    .line 328762
    :cond_7
    const-string v1, "expected digit (0-9) to follow minus sign, for valid numeric value"

    invoke-virtual {p0, v0, v1}, LX/15u;->a(ILjava/lang/String;)V

    .line 328763
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private a([CIIZI)LX/15z;
    .locals 9

    .prologue
    .line 328697
    const/4 v0, 0x0

    .line 328698
    const/4 v4, 0x0

    .line 328699
    const/16 v1, 0x2e

    if-ne p3, v1, :cond_11

    .line 328700
    add-int/lit8 v1, p2, 0x1

    int-to-char v2, p3

    aput-char v2, p1, p2

    .line 328701
    :goto_0
    iget v2, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v2, v3, :cond_a

    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v2

    if-nez v2, :cond_a

    .line 328702
    const/4 v4, 0x1

    .line 328703
    :cond_0
    if-nez v0, :cond_1

    .line 328704
    const-string v2, "Decimal point not followed by a digit"

    invoke-virtual {p0, p3, v2}, LX/15u;->a(ILjava/lang/String;)V

    :cond_1
    move v6, v0

    move v0, v1

    move-object v1, p1

    .line 328705
    :goto_1
    const/4 v3, 0x0

    .line 328706
    const/16 v2, 0x65

    if-eq p3, v2, :cond_2

    const/16 v2, 0x45

    if-ne p3, v2, :cond_f

    .line 328707
    :cond_2
    array-length v2, v1

    if-lt v0, v2, :cond_3

    .line 328708
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v1

    .line 328709
    const/4 v0, 0x0

    .line 328710
    :cond_3
    add-int/lit8 v2, v0, 0x1

    int-to-char v5, p3

    aput-char v5, v1, v0

    .line 328711
    iget v0, p0, LX/15u;->d:I

    iget v5, p0, LX/15u;->e:I

    if-lt v0, v5, :cond_4

    .line 328712
    invoke-virtual {p0}, LX/15u;->K()V

    .line 328713
    :cond_4
    iget-object v0, p0, LX/1pM;->R:[B

    iget v5, p0, LX/15u;->d:I

    add-int/lit8 v7, v5, 0x1

    iput v7, p0, LX/1pM;->d:I

    aget-byte v0, v0, v5

    and-int/lit16 v5, v0, 0xff

    .line 328714
    const/16 v0, 0x2d

    if-eq v5, v0, :cond_5

    const/16 v0, 0x2b

    if-ne v5, v0, :cond_e

    .line 328715
    :cond_5
    array-length v0, v1

    if-lt v2, v0, :cond_d

    .line 328716
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v1

    .line 328717
    const/4 v0, 0x0

    .line 328718
    :goto_2
    add-int/lit8 v2, v0, 0x1

    int-to-char v5, v5

    aput-char v5, v1, v0

    .line 328719
    iget v0, p0, LX/15u;->d:I

    iget v5, p0, LX/15u;->e:I

    if-lt v0, v5, :cond_6

    .line 328720
    invoke-virtual {p0}, LX/15u;->K()V

    .line 328721
    :cond_6
    iget-object v0, p0, LX/1pM;->R:[B

    iget v5, p0, LX/15u;->d:I

    add-int/lit8 v7, v5, 0x1

    iput v7, p0, LX/1pM;->d:I

    aget-byte v0, v0, v5

    and-int/lit16 v0, v0, 0xff

    move v5, v0

    move v0, v2

    move v2, v3

    .line 328722
    :goto_3
    const/16 v3, 0x39

    if-gt v5, v3, :cond_c

    const/16 v3, 0x30

    if-lt v5, v3, :cond_c

    .line 328723
    add-int/lit8 v2, v2, 0x1

    .line 328724
    array-length v3, v1

    if-lt v0, v3, :cond_7

    .line 328725
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v1

    .line 328726
    const/4 v0, 0x0

    .line 328727
    :cond_7
    add-int/lit8 v3, v0, 0x1

    int-to-char v7, v5

    aput-char v7, v1, v0

    .line 328728
    iget v0, p0, LX/15u;->d:I

    iget v7, p0, LX/15u;->e:I

    if-lt v0, v7, :cond_b

    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v0

    if-nez v0, :cond_b

    .line 328729
    const/4 v4, 0x1

    move v0, v2

    move v1, v4

    move v2, v3

    .line 328730
    :goto_4
    if-nez v0, :cond_8

    .line 328731
    const-string v3, "Exponent indicator not followed by a digit"

    invoke-virtual {p0, v5, v3}, LX/15u;->a(ILjava/lang/String;)V

    .line 328732
    :cond_8
    :goto_5
    if-nez v1, :cond_9

    .line 328733
    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/1pM;->d:I

    .line 328734
    :cond_9
    iget-object v1, p0, LX/15u;->n:LX/15x;

    .line 328735
    iput v2, v1, LX/15x;->j:I

    .line 328736
    invoke-virtual {p0, p4, p5, v6, v0}, LX/15u;->b(ZIII)LX/15z;

    move-result-object v0

    return-object v0

    .line 328737
    :cond_a
    iget-object v2, p0, LX/1pM;->R:[B

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v5, v3, 0x1

    iput v5, p0, LX/1pM;->d:I

    aget-byte v2, v2, v3

    and-int/lit16 p3, v2, 0xff

    .line 328738
    const/16 v2, 0x30

    if-lt p3, v2, :cond_0

    const/16 v2, 0x39

    if-gt p3, v2, :cond_0

    .line 328739
    add-int/lit8 v0, v0, 0x1

    .line 328740
    array-length v2, p1

    if-lt v1, v2, :cond_10

    .line 328741
    iget-object v1, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v1}, LX/15x;->n()[C

    move-result-object p1

    .line 328742
    const/4 v1, 0x0

    move v2, v1

    .line 328743
    :goto_6
    add-int/lit8 v1, v2, 0x1

    int-to-char v3, p3

    aput-char v3, p1, v2

    goto/16 :goto_0

    .line 328744
    :cond_b
    iget-object v0, p0, LX/1pM;->R:[B

    iget v5, p0, LX/15u;->d:I

    add-int/lit8 v7, v5, 0x1

    iput v7, p0, LX/1pM;->d:I

    aget-byte v0, v0, v5

    and-int/lit16 v0, v0, 0xff

    move v5, v0

    move v0, v3

    goto :goto_3

    :cond_c
    move v1, v4

    move v8, v2

    move v2, v0

    move v0, v8

    goto :goto_4

    :cond_d
    move v0, v2

    goto/16 :goto_2

    :cond_e
    move v0, v2

    move v2, v3

    goto/16 :goto_3

    :cond_f
    move v1, v4

    move v2, v0

    move v0, v3

    goto :goto_5

    :cond_10
    move v2, v1

    goto :goto_6

    :cond_11
    move v6, v0

    move-object v1, p1

    move v0, p2

    goto/16 :goto_1
.end method

.method private a([CIZI)LX/15z;
    .locals 6

    .prologue
    .line 328678
    move v5, p4

    move v2, p2

    move-object v1, p1

    :goto_0
    iget v0, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v0, v3, :cond_0

    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v0

    if-nez v0, :cond_0

    .line 328679
    iget-object v0, p0, LX/15u;->n:LX/15x;

    .line 328680
    iput v2, v0, LX/15x;->j:I

    .line 328681
    invoke-virtual {p0, p3, v5}, LX/15u;->a(ZI)LX/15z;

    move-result-object v0

    .line 328682
    :goto_1
    return-object v0

    .line 328683
    :cond_0
    iget-object v0, p0, LX/1pM;->R:[B

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/1pM;->d:I

    aget-byte v0, v0, v3

    and-int/lit16 v3, v0, 0xff

    .line 328684
    const/16 v0, 0x39

    if-gt v3, v0, :cond_1

    const/16 v0, 0x30

    if-ge v3, v0, :cond_3

    .line 328685
    :cond_1
    const/16 v0, 0x2e

    if-eq v3, v0, :cond_2

    const/16 v0, 0x65

    if-eq v3, v0, :cond_2

    const/16 v0, 0x45

    if-ne v3, v0, :cond_4

    :cond_2
    move-object v0, p0

    move v4, p3

    .line 328686
    invoke-direct/range {v0 .. v5}, LX/1pM;->a([CIIZI)LX/15z;

    move-result-object v0

    goto :goto_1

    .line 328687
    :cond_3
    array-length v0, v1

    if-lt v2, v0, :cond_5

    .line 328688
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v1

    .line 328689
    const/4 v2, 0x0

    move v0, v2

    .line 328690
    :goto_2
    add-int/lit8 v2, v0, 0x1

    int-to-char v3, v3

    aput-char v3, v1, v0

    .line 328691
    add-int/lit8 v5, v5, 0x1

    .line 328692
    goto :goto_0

    .line 328693
    :cond_4
    iget v0, p0, LX/15u;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1pM;->d:I

    .line 328694
    iget-object v0, p0, LX/15u;->n:LX/15x;

    .line 328695
    iput v2, v0, LX/15x;->j:I

    .line 328696
    invoke-virtual {p0, p3, v5}, LX/15u;->a(ZI)LX/15z;

    move-result-object v0

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method private a(LX/15z;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 328671
    if-nez p1, :cond_0

    .line 328672
    const/4 v0, 0x0

    .line 328673
    :goto_0
    return-object v0

    .line 328674
    :cond_0
    sget-object v0, LX/4pi;->a:[I

    invoke-virtual {p1}, LX/15z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 328675
    invoke-virtual {p1}, LX/15z;->asString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 328676
    :pswitch_0
    iget-object v0, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 328677
    :pswitch_1
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 329290
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 329291
    :cond_0
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_1

    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    iget-object v1, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    aget-byte v1, v1, v2

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v1, v2, :cond_3

    .line 329292
    :cond_2
    invoke-virtual {p1, v3, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, LX/1pM;->f(Ljava/lang/String;)V

    .line 329293
    :cond_3
    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/1pM;->d:I

    .line 329294
    add-int/lit8 p2, p2, 0x1

    if-lt p2, v0, :cond_0

    .line 329295
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_5

    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v0

    if-nez v0, :cond_5

    .line 329296
    :cond_4
    :goto_0
    return-void

    .line 329297
    :cond_5
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 329298
    const/16 v1, 0x30

    if-lt v0, v1, :cond_4

    const/16 v1, 0x5d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7d

    if-eq v0, v1, :cond_4

    .line 329299
    invoke-direct {p0, v0}, LX/1pM;->l(I)I

    move-result v0

    int-to-char v0, v0

    .line 329300
    invoke-static {v0}, Ljava/lang/Character;->isJavaIdentifierPart(C)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 329301
    invoke-virtual {p1, v3, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1pM;->f(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 329031
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 329032
    :goto_0
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_0

    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 329033
    :cond_0
    iget-object v1, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v1, v1, v2

    .line 329034
    invoke-direct {p0, v1}, LX/1pM;->l(I)I

    move-result v1

    int-to-char v1, v1

    .line 329035
    invoke-static {v1}, Ljava/lang/Character;->isJavaIdentifierPart(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 329036
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 329037
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized token \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\': was expecting "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 329038
    return-void
.end method

.method private a([CI)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 329253
    sget-object v4, LX/1pM;->T:[I

    .line 329254
    iget-object v5, p0, LX/1pM;->R:[B

    .line 329255
    :goto_0
    iget v0, p0, LX/15u;->d:I

    .line 329256
    iget v2, p0, LX/15u;->e:I

    if-lt v0, v2, :cond_0

    .line 329257
    invoke-virtual {p0}, LX/15u;->K()V

    .line 329258
    iget v0, p0, LX/15u;->d:I

    .line 329259
    :cond_0
    array-length v2, p1

    if-lt p2, v2, :cond_1

    .line 329260
    iget-object v2, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v2}, LX/15x;->n()[C

    move-result-object p1

    move p2, v1

    .line 329261
    :cond_1
    iget v2, p0, LX/15u;->e:I

    array-length v3, p1

    sub-int/2addr v3, p2

    add-int/2addr v3, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 329262
    :goto_1
    if-ge v0, v6, :cond_3

    .line 329263
    add-int/lit8 v2, v0, 0x1

    aget-byte v0, v5, v0

    and-int/lit16 v0, v0, 0xff

    .line 329264
    aget v3, v4, v0

    if-eqz v3, :cond_2

    .line 329265
    iput v2, p0, LX/1pM;->d:I

    .line 329266
    const/16 v2, 0x22

    if-eq v0, v2, :cond_7

    .line 329267
    aget v2, v4, v0

    packed-switch v2, :pswitch_data_0

    .line 329268
    const/16 v2, 0x20

    if-ge v0, v2, :cond_6

    .line 329269
    const-string v2, "string value"

    invoke-virtual {p0, v0, v2}, LX/15v;->c(ILjava/lang/String;)V

    .line 329270
    :goto_2
    array-length v2, p1

    if-lt p2, v2, :cond_8

    .line 329271
    iget-object v2, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v2}, LX/15x;->n()[C

    move-result-object p1

    move v2, v1

    .line 329272
    :goto_3
    add-int/lit8 p2, v2, 0x1

    int-to-char v0, v0

    aput-char v0, p1, v2

    goto :goto_0

    .line 329273
    :cond_2
    add-int/lit8 v3, p2, 0x1

    int-to-char v0, v0

    aput-char v0, p1, p2

    move v0, v2

    move p2, v3

    goto :goto_1

    .line 329274
    :cond_3
    iput v0, p0, LX/1pM;->d:I

    goto :goto_0

    .line 329275
    :pswitch_0
    invoke-virtual {p0}, LX/1pM;->R()C

    move-result v0

    goto :goto_2

    .line 329276
    :pswitch_1
    invoke-direct {p0, v0}, LX/1pM;->m(I)I

    move-result v0

    goto :goto_2

    .line 329277
    :pswitch_2
    iget v2, p0, LX/15u;->e:I

    iget v3, p0, LX/15u;->d:I

    sub-int/2addr v2, v3

    const/4 v3, 0x2

    if-lt v2, v3, :cond_4

    .line 329278
    invoke-direct {p0, v0}, LX/1pM;->o(I)I

    move-result v0

    goto :goto_2

    .line 329279
    :cond_4
    invoke-direct {p0, v0}, LX/1pM;->n(I)I

    move-result v0

    goto :goto_2

    .line 329280
    :pswitch_3
    invoke-direct {p0, v0}, LX/1pM;->p(I)I

    move-result v2

    .line 329281
    add-int/lit8 v0, p2, 0x1

    const v3, 0xd800

    shr-int/lit8 v6, v2, 0xa

    or-int/2addr v3, v6

    int-to-char v3, v3

    aput-char v3, p1, p2

    .line 329282
    array-length v3, p1

    if-lt v0, v3, :cond_5

    .line 329283
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object p1

    move v0, v1

    .line 329284
    :cond_5
    const v3, 0xdc00

    and-int/lit16 v2, v2, 0x3ff

    or-int/2addr v2, v3

    move p2, v0

    move v0, v2

    .line 329285
    goto :goto_2

    .line 329286
    :cond_6
    invoke-direct {p0, v0}, LX/1pM;->q(I)V

    goto :goto_2

    .line 329287
    :cond_7
    iget-object v0, p0, LX/15u;->n:LX/15x;

    .line 329288
    iput p2, v0, LX/15x;->j:I

    .line 329289
    return-void

    :cond_8
    move v2, p2

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static a([II)[I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 329247
    if-nez p0, :cond_0

    .line 329248
    new-array v0, p1, [I

    .line 329249
    :goto_0
    return-object v0

    .line 329250
    :cond_0
    array-length v1, p0

    .line 329251
    add-int v0, v1, p1

    new-array v0, v0, [I

    .line 329252
    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method private aa()LX/15z;
    .locals 10

    .prologue
    const/16 v9, 0x27

    const/4 v2, 0x0

    .line 329210
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->l()[C

    move-result-object v0

    .line 329211
    sget-object v6, LX/1pM;->T:[I

    .line 329212
    iget-object v7, p0, LX/1pM;->R:[B

    move v1, v2

    .line 329213
    :cond_0
    :goto_0
    iget v3, p0, LX/15u;->d:I

    iget v4, p0, LX/15u;->e:I

    if-lt v3, v4, :cond_1

    .line 329214
    invoke-virtual {p0}, LX/15u;->K()V

    .line 329215
    :cond_1
    array-length v3, v0

    if-lt v1, v3, :cond_2

    .line 329216
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v0

    move v1, v2

    .line 329217
    :cond_2
    iget v4, p0, LX/15u;->e:I

    .line 329218
    iget v3, p0, LX/15u;->d:I

    array-length v5, v0

    sub-int/2addr v5, v1

    add-int/2addr v3, v5

    .line 329219
    if-ge v3, v4, :cond_a

    .line 329220
    :goto_1
    iget v4, p0, LX/15u;->d:I

    if-ge v4, v3, :cond_0

    .line 329221
    iget v4, p0, LX/15u;->d:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/1pM;->d:I

    aget-byte v4, v7, v4

    and-int/lit16 v5, v4, 0xff

    .line 329222
    if-eq v5, v9, :cond_3

    aget v4, v6, v5

    if-nez v4, :cond_3

    .line 329223
    add-int/lit8 v4, v1, 0x1

    int-to-char v5, v5

    aput-char v5, v0, v1

    move v1, v4

    goto :goto_1

    .line 329224
    :cond_3
    if-eq v5, v9, :cond_7

    .line 329225
    aget v3, v6, v5

    packed-switch v3, :pswitch_data_0

    .line 329226
    const/16 v3, 0x20

    if-ge v5, v3, :cond_4

    .line 329227
    const-string v3, "string value"

    invoke-virtual {p0, v5, v3}, LX/15v;->c(ILjava/lang/String;)V

    .line 329228
    :cond_4
    invoke-direct {p0, v5}, LX/1pM;->q(I)V

    :cond_5
    move v3, v5

    .line 329229
    :goto_2
    array-length v4, v0

    if-lt v1, v4, :cond_8

    .line 329230
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v0

    move v4, v2

    .line 329231
    :goto_3
    add-int/lit8 v1, v4, 0x1

    int-to-char v3, v3

    aput-char v3, v0, v4

    goto :goto_0

    .line 329232
    :pswitch_0
    const/16 v3, 0x22

    if-eq v5, v3, :cond_5

    .line 329233
    invoke-virtual {p0}, LX/1pM;->R()C

    move-result v3

    goto :goto_2

    .line 329234
    :pswitch_1
    invoke-direct {p0, v5}, LX/1pM;->m(I)I

    move-result v3

    goto :goto_2

    .line 329235
    :pswitch_2
    iget v3, p0, LX/15u;->e:I

    iget v4, p0, LX/15u;->d:I

    sub-int/2addr v3, v4

    const/4 v4, 0x2

    if-lt v3, v4, :cond_6

    .line 329236
    invoke-direct {p0, v5}, LX/1pM;->o(I)I

    move-result v3

    goto :goto_2

    .line 329237
    :cond_6
    invoke-direct {p0, v5}, LX/1pM;->n(I)I

    move-result v3

    goto :goto_2

    .line 329238
    :pswitch_3
    invoke-direct {p0, v5}, LX/1pM;->p(I)I

    move-result v4

    .line 329239
    add-int/lit8 v3, v1, 0x1

    const v5, 0xd800

    shr-int/lit8 v8, v4, 0xa

    or-int/2addr v5, v8

    int-to-char v5, v5

    aput-char v5, v0, v1

    .line 329240
    array-length v1, v0

    if-lt v3, v1, :cond_9

    .line 329241
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v0

    move v1, v2

    .line 329242
    :goto_4
    const v3, 0xdc00

    and-int/lit16 v4, v4, 0x3ff

    or-int/2addr v3, v4

    .line 329243
    goto :goto_2

    .line 329244
    :cond_7
    iget-object v0, p0, LX/15u;->n:LX/15x;

    .line 329245
    iput v1, v0, LX/15x;->j:I

    .line 329246
    sget-object v0, LX/15z;->VALUE_STRING:LX/15z;

    return-object v0

    :cond_8
    move v4, v1

    goto :goto_3

    :cond_9
    move v1, v3

    goto :goto_4

    :cond_a
    move v3, v4

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private ab()I
    .locals 4

    .prologue
    const/16 v3, 0x20

    .line 329196
    :cond_0
    :goto_0
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 329197
    :cond_1
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/1pM;->d:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 329198
    if-le v0, v3, :cond_3

    .line 329199
    const/16 v1, 0x2f

    if-eq v0, v1, :cond_2

    .line 329200
    return v0

    .line 329201
    :cond_2
    invoke-direct {p0}, LX/1pM;->ad()V

    goto :goto_0

    .line 329202
    :cond_3
    if-eq v0, v3, :cond_0

    .line 329203
    const/16 v1, 0xa

    if-ne v0, v1, :cond_4

    .line 329204
    invoke-direct {p0}, LX/1pM;->ak()V

    goto :goto_0

    .line 329205
    :cond_4
    const/16 v1, 0xd

    if-ne v0, v1, :cond_5

    .line 329206
    invoke-direct {p0}, LX/1pM;->aj()V

    goto :goto_0

    .line 329207
    :cond_5
    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    .line 329208
    invoke-virtual {p0, v0}, LX/15v;->d(I)V

    goto :goto_0

    .line 329209
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected end-of-input within/between "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v1}, LX/12V;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " entries"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15w;->b(Ljava/lang/String;)LX/2aQ;

    move-result-object v0

    throw v0
.end method

.method private ac()I
    .locals 4

    .prologue
    const/16 v3, 0x20

    .line 329181
    :cond_0
    :goto_0
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 329182
    :cond_1
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/1pM;->d:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 329183
    if-le v0, v3, :cond_3

    .line 329184
    const/16 v1, 0x2f

    if-eq v0, v1, :cond_2

    .line 329185
    :goto_1
    return v0

    .line 329186
    :cond_2
    invoke-direct {p0}, LX/1pM;->ad()V

    goto :goto_0

    .line 329187
    :cond_3
    if-eq v0, v3, :cond_0

    .line 329188
    const/16 v1, 0xa

    if-ne v0, v1, :cond_4

    .line 329189
    invoke-direct {p0}, LX/1pM;->ak()V

    goto :goto_0

    .line 329190
    :cond_4
    const/16 v1, 0xd

    if-ne v0, v1, :cond_5

    .line 329191
    invoke-direct {p0}, LX/1pM;->aj()V

    goto :goto_0

    .line 329192
    :cond_5
    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    .line 329193
    invoke-virtual {p0, v0}, LX/15v;->d(I)V

    goto :goto_0

    .line 329194
    :cond_6
    invoke-virtual {p0}, LX/15u;->P()V

    .line 329195
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private ad()V
    .locals 4

    .prologue
    const/16 v3, 0x2f

    .line 329170
    sget-object v0, LX/0lr;->ALLOW_COMMENTS:LX/0lr;

    invoke-virtual {p0, v0}, LX/15w;->a(LX/0lr;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 329171
    const-string v0, "maybe a (non-standard) comment? (not recognized as one since Feature \'ALLOW_COMMENTS\' not enabled for parser)"

    invoke-virtual {p0, v3, v0}, LX/15v;->b(ILjava/lang/String;)V

    .line 329172
    :cond_0
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v0

    if-nez v0, :cond_1

    .line 329173
    const-string v0, " in a comment"

    invoke-virtual {p0, v0}, LX/15v;->d(Ljava/lang/String;)V

    .line 329174
    :cond_1
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/1pM;->d:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 329175
    if-ne v0, v3, :cond_2

    .line 329176
    invoke-direct {p0}, LX/1pM;->af()V

    .line 329177
    :goto_0
    return-void

    .line 329178
    :cond_2
    const/16 v1, 0x2a

    if-ne v0, v1, :cond_3

    .line 329179
    invoke-direct {p0}, LX/1pM;->ae()V

    goto :goto_0

    .line 329180
    :cond_3
    const-string v1, "was expecting either \'*\' or \'/\' for a comment"

    invoke-virtual {p0, v0, v1}, LX/15v;->b(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private ae()V
    .locals 4

    .prologue
    .line 329153
    sget-object v0, LX/12H;->e:[I

    move-object v0, v0

    .line 329154
    :cond_0
    :goto_0
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_1

    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 329155
    :cond_1
    iget-object v1, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    .line 329156
    aget v2, v0, v1

    .line 329157
    if-eqz v2, :cond_0

    .line 329158
    sparse-switch v2, :sswitch_data_0

    .line 329159
    invoke-direct {p0, v1}, LX/1pM;->q(I)V

    goto :goto_0

    .line 329160
    :sswitch_0
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_2

    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 329161
    :cond_2
    iget-object v1, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    aget-byte v1, v1, v2

    const/16 v2, 0x2f

    if-ne v1, v2, :cond_0

    .line 329162
    iget v0, p0, LX/15u;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1pM;->d:I

    .line 329163
    :goto_1
    return-void

    .line 329164
    :sswitch_1
    invoke-direct {p0}, LX/1pM;->ak()V

    goto :goto_0

    .line 329165
    :sswitch_2
    invoke-direct {p0}, LX/1pM;->aj()V

    goto :goto_0

    .line 329166
    :sswitch_3
    invoke-direct {p0}, LX/1pM;->ag()V

    goto :goto_0

    .line 329167
    :sswitch_4
    invoke-direct {p0}, LX/1pM;->ah()V

    goto :goto_0

    .line 329168
    :sswitch_5
    invoke-direct {p0}, LX/1pM;->ai()V

    goto :goto_0

    .line 329169
    :cond_3
    const-string v0, " in a comment"

    invoke-virtual {p0, v0}, LX/15v;->d(Ljava/lang/String;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x4 -> :sswitch_5
        0xa -> :sswitch_1
        0xd -> :sswitch_2
        0x2a -> :sswitch_0
    .end sparse-switch
.end method

.method private af()V
    .locals 4

    .prologue
    .line 329140
    sget-object v0, LX/12H;->e:[I

    move-object v0, v0

    .line 329141
    :cond_0
    :goto_0
    :sswitch_0
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_1

    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 329142
    :cond_1
    iget-object v1, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    .line 329143
    aget v2, v0, v1

    .line 329144
    if-eqz v2, :cond_0

    .line 329145
    sparse-switch v2, :sswitch_data_0

    .line 329146
    invoke-direct {p0, v1}, LX/1pM;->q(I)V

    goto :goto_0

    .line 329147
    :sswitch_1
    invoke-direct {p0}, LX/1pM;->ak()V

    .line 329148
    :cond_2
    :goto_1
    return-void

    .line 329149
    :sswitch_2
    invoke-direct {p0}, LX/1pM;->aj()V

    goto :goto_1

    .line 329150
    :sswitch_3
    invoke-direct {p0}, LX/1pM;->ag()V

    goto :goto_0

    .line 329151
    :sswitch_4
    invoke-direct {p0}, LX/1pM;->ah()V

    goto :goto_0

    .line 329152
    :sswitch_5
    invoke-direct {p0}, LX/1pM;->ai()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x4 -> :sswitch_5
        0xa -> :sswitch_1
        0xd -> :sswitch_2
        0x2a -> :sswitch_0
    .end sparse-switch
.end method

.method private ag()V
    .locals 3

    .prologue
    .line 329134
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    .line 329135
    invoke-virtual {p0}, LX/15u;->K()V

    .line 329136
    :cond_0
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/1pM;->d:I

    aget-byte v0, v0, v1

    .line 329137
    and-int/lit16 v1, v0, 0xc0

    const/16 v2, 0x80

    if-eq v1, v2, :cond_1

    .line 329138
    and-int/lit16 v0, v0, 0xff

    iget v1, p0, LX/15u;->d:I

    invoke-direct {p0, v0, v1}, LX/1pM;->b(II)V

    .line 329139
    :cond_1
    return-void
.end method

.method private ah()V
    .locals 4

    .prologue
    const/16 v3, 0x80

    .line 328437
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    .line 328438
    invoke-virtual {p0}, LX/15u;->K()V

    .line 328439
    :cond_0
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/1pM;->d:I

    aget-byte v0, v0, v1

    .line 328440
    and-int/lit16 v1, v0, 0xc0

    if-eq v1, v3, :cond_1

    .line 328441
    and-int/lit16 v0, v0, 0xff

    iget v1, p0, LX/15u;->d:I

    invoke-direct {p0, v0, v1}, LX/1pM;->b(II)V

    .line 328442
    :cond_1
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_2

    .line 328443
    invoke-virtual {p0}, LX/15u;->K()V

    .line 328444
    :cond_2
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/1pM;->d:I

    aget-byte v0, v0, v1

    .line 328445
    and-int/lit16 v1, v0, 0xc0

    if-eq v1, v3, :cond_3

    .line 328446
    and-int/lit16 v0, v0, 0xff

    iget v1, p0, LX/15u;->d:I

    invoke-direct {p0, v0, v1}, LX/1pM;->b(II)V

    .line 328447
    :cond_3
    return-void
.end method

.method private ai()V
    .locals 4

    .prologue
    const/16 v3, 0x80

    .line 329118
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    .line 329119
    invoke-virtual {p0}, LX/15u;->K()V

    .line 329120
    :cond_0
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/1pM;->d:I

    aget-byte v0, v0, v1

    .line 329121
    and-int/lit16 v1, v0, 0xc0

    if-eq v1, v3, :cond_1

    .line 329122
    and-int/lit16 v0, v0, 0xff

    iget v1, p0, LX/15u;->d:I

    invoke-direct {p0, v0, v1}, LX/1pM;->b(II)V

    .line 329123
    :cond_1
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_2

    .line 329124
    invoke-virtual {p0}, LX/15u;->K()V

    .line 329125
    :cond_2
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/1pM;->d:I

    aget-byte v0, v0, v1

    .line 329126
    and-int/lit16 v1, v0, 0xc0

    if-eq v1, v3, :cond_3

    .line 329127
    and-int/lit16 v0, v0, 0xff

    iget v1, p0, LX/15u;->d:I

    invoke-direct {p0, v0, v1}, LX/1pM;->b(II)V

    .line 329128
    :cond_3
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_4

    .line 329129
    invoke-virtual {p0}, LX/15u;->K()V

    .line 329130
    :cond_4
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/1pM;->d:I

    aget-byte v0, v0, v1

    .line 329131
    and-int/lit16 v1, v0, 0xc0

    if-eq v1, v3, :cond_5

    .line 329132
    and-int/lit16 v0, v0, 0xff

    iget v1, p0, LX/15u;->d:I

    invoke-direct {p0, v0, v1}, LX/1pM;->b(II)V

    .line 329133
    :cond_5
    return-void
.end method

.method private aj()V
    .locals 2

    .prologue
    .line 329112
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 329113
    :cond_0
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    aget-byte v0, v0, v1

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    .line 329114
    iget v0, p0, LX/15u;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1pM;->d:I

    .line 329115
    :cond_1
    iget v0, p0, LX/15u;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1pM;->g:I

    .line 329116
    iget v0, p0, LX/15u;->d:I

    iput v0, p0, LX/1pM;->h:I

    .line 329117
    return-void
.end method

.method private ak()V
    .locals 1

    .prologue
    .line 329109
    iget v0, p0, LX/15u;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1pM;->g:I

    .line 329110
    iget v0, p0, LX/15u;->d:I

    iput v0, p0, LX/1pM;->h:I

    .line 329111
    return-void
.end method

.method private al()I
    .locals 3

    .prologue
    .line 329106
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    .line 329107
    invoke-virtual {p0}, LX/15u;->K()V

    .line 329108
    :cond_0
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/1pM;->d:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method private b(III)LX/0lx;
    .locals 2

    .prologue
    .line 329100
    iget-object v0, p0, LX/1pM;->N:LX/0lv;

    invoke-virtual {v0, p1, p2}, LX/0lv;->a(II)LX/0lx;

    move-result-object v0

    .line 329101
    if-eqz v0, :cond_0

    .line 329102
    :goto_0
    return-object v0

    .line 329103
    :cond_0
    iget-object v0, p0, LX/1pM;->O:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 329104
    iget-object v0, p0, LX/1pM;->O:[I

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 329105
    iget-object v0, p0, LX/1pM;->O:[I

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1, p3}, LX/1pM;->a([III)LX/0lx;

    move-result-object v0

    goto :goto_0
.end method

.method private b(II)V
    .locals 0

    .prologue
    .line 328905
    iput p2, p0, LX/1pM;->d:I

    .line 328906
    invoke-direct {p0, p1}, LX/1pM;->s(I)V

    .line 328907
    return-void
.end method

.method private b(LX/0ln;)[B
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/16 v6, 0x22

    const/4 v5, -0x2

    .line 329039
    invoke-virtual {p0}, LX/15u;->Q()LX/2SG;

    move-result-object v2

    .line 329040
    :cond_0
    :goto_0
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_1

    .line 329041
    invoke-virtual {p0}, LX/15u;->K()V

    .line 329042
    :cond_1
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v0, v0, v1

    and-int/lit16 v1, v0, 0xff

    .line 329043
    const/16 v0, 0x20

    if-le v1, v0, :cond_0

    .line 329044
    invoke-virtual {p1, v1}, LX/0ln;->b(I)I

    move-result v0

    .line 329045
    if-gez v0, :cond_3

    .line 329046
    if-ne v1, v6, :cond_2

    .line 329047
    invoke-virtual {v2}, LX/2SG;->c()[B

    move-result-object v0

    .line 329048
    :goto_1
    return-object v0

    .line 329049
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v1, v0}, LX/15u;->a(LX/0ln;II)I

    move-result v0

    .line 329050
    if-ltz v0, :cond_0

    .line 329051
    :cond_3
    iget v1, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v1, v3, :cond_4

    .line 329052
    invoke-virtual {p0}, LX/15u;->K()V

    .line 329053
    :cond_4
    iget-object v1, p0, LX/1pM;->R:[B

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/1pM;->d:I

    aget-byte v1, v1, v3

    and-int/lit16 v3, v1, 0xff

    .line 329054
    invoke-virtual {p1, v3}, LX/0ln;->b(I)I

    move-result v1

    .line 329055
    if-gez v1, :cond_5

    .line 329056
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v3, v1}, LX/15u;->a(LX/0ln;II)I

    move-result v1

    .line 329057
    :cond_5
    shl-int/lit8 v0, v0, 0x6

    or-int/2addr v1, v0

    .line 329058
    iget v0, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v0, v3, :cond_6

    .line 329059
    invoke-virtual {p0}, LX/15u;->K()V

    .line 329060
    :cond_6
    iget-object v0, p0, LX/1pM;->R:[B

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/1pM;->d:I

    aget-byte v0, v0, v3

    and-int/lit16 v3, v0, 0xff

    .line 329061
    invoke-virtual {p1, v3}, LX/0ln;->b(I)I

    move-result v0

    .line 329062
    if-gez v0, :cond_b

    .line 329063
    if-eq v0, v5, :cond_8

    .line 329064
    if-ne v3, v6, :cond_7

    .line 329065
    iget-boolean v0, p1, LX/0ln;->a:Z

    move v0, v0

    .line 329066
    if-nez v0, :cond_7

    .line 329067
    shr-int/lit8 v0, v1, 0x4

    .line 329068
    invoke-virtual {v2, v0}, LX/2SG;->a(I)V

    .line 329069
    invoke-virtual {v2}, LX/2SG;->c()[B

    move-result-object v0

    goto :goto_1

    .line 329070
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v3, v0}, LX/15u;->a(LX/0ln;II)I

    move-result v0

    .line 329071
    :cond_8
    if-ne v0, v5, :cond_b

    .line 329072
    iget v0, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v0, v3, :cond_9

    .line 329073
    invoke-virtual {p0}, LX/15u;->K()V

    .line 329074
    :cond_9
    iget-object v0, p0, LX/1pM;->R:[B

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/1pM;->d:I

    aget-byte v0, v0, v3

    and-int/lit16 v0, v0, 0xff

    .line 329075
    invoke-virtual {p1, v0}, LX/0ln;->a(I)Z

    move-result v3

    if-nez v3, :cond_a

    .line 329076
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "expected padding character \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 329077
    iget-char v2, p1, LX/0ln;->b:C

    move v2, v2

    .line 329078
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v7, v1}, LX/15u;->a(LX/0ln;IILjava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 329079
    :cond_a
    shr-int/lit8 v0, v1, 0x4

    .line 329080
    invoke-virtual {v2, v0}, LX/2SG;->a(I)V

    goto/16 :goto_0

    .line 329081
    :cond_b
    shl-int/lit8 v1, v1, 0x6

    or-int/2addr v1, v0

    .line 329082
    iget v0, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v0, v3, :cond_c

    .line 329083
    invoke-virtual {p0}, LX/15u;->K()V

    .line 329084
    :cond_c
    iget-object v0, p0, LX/1pM;->R:[B

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/1pM;->d:I

    aget-byte v0, v0, v3

    and-int/lit16 v3, v0, 0xff

    .line 329085
    invoke-virtual {p1, v3}, LX/0ln;->b(I)I

    move-result v0

    .line 329086
    if-gez v0, :cond_f

    .line 329087
    if-eq v0, v5, :cond_e

    .line 329088
    if-ne v3, v6, :cond_d

    .line 329089
    iget-boolean v0, p1, LX/0ln;->a:Z

    move v0, v0

    .line 329090
    if-nez v0, :cond_d

    .line 329091
    shr-int/lit8 v0, v1, 0x2

    .line 329092
    invoke-virtual {v2, v0}, LX/2SG;->b(I)V

    .line 329093
    invoke-virtual {v2}, LX/2SG;->c()[B

    move-result-object v0

    goto/16 :goto_1

    .line 329094
    :cond_d
    invoke-virtual {p0, p1, v3, v7}, LX/15u;->a(LX/0ln;II)I

    move-result v0

    .line 329095
    :cond_e
    if-ne v0, v5, :cond_f

    .line 329096
    shr-int/lit8 v0, v1, 0x2

    .line 329097
    invoke-virtual {v2, v0}, LX/2SG;->b(I)V

    goto/16 :goto_0

    .line 329098
    :cond_f
    shl-int/lit8 v1, v1, 0x6

    or-int/2addr v0, v1

    .line 329099
    invoke-virtual {v2, v0}, LX/2SG;->c(I)V

    goto/16 :goto_0
.end method

.method private f(I)LX/15z;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 328183
    const/16 v0, 0x22

    if-ne p1, v0, :cond_0

    .line 328184
    iput-boolean v1, p0, LX/1pM;->P:Z

    .line 328185
    sget-object v0, LX/15z;->VALUE_STRING:LX/15z;

    iput-object v0, p0, LX/1pM;->K:LX/15z;

    .line 328186
    :goto_0
    return-object v0

    .line 328187
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 328188
    invoke-direct {p0, p1}, LX/1pM;->k(I)LX/15z;

    move-result-object v0

    iput-object v0, p0, LX/1pM;->K:LX/15z;

    goto :goto_0

    .line 328189
    :sswitch_0
    iget-object v0, p0, LX/15u;->l:LX/15y;

    iget v1, p0, LX/15u;->j:I

    iget v2, p0, LX/15u;->k:I

    invoke-virtual {v0, v1, v2}, LX/15y;->b(II)LX/15y;

    move-result-object v0

    iput-object v0, p0, LX/1pM;->l:LX/15y;

    .line 328190
    sget-object v0, LX/15z;->START_ARRAY:LX/15z;

    iput-object v0, p0, LX/1pM;->K:LX/15z;

    goto :goto_0

    .line 328191
    :sswitch_1
    iget-object v0, p0, LX/15u;->l:LX/15y;

    iget v1, p0, LX/15u;->j:I

    iget v2, p0, LX/15u;->k:I

    invoke-virtual {v0, v1, v2}, LX/15y;->c(II)LX/15y;

    move-result-object v0

    iput-object v0, p0, LX/1pM;->l:LX/15y;

    .line 328192
    sget-object v0, LX/15z;->START_OBJECT:LX/15z;

    iput-object v0, p0, LX/1pM;->K:LX/15z;

    goto :goto_0

    .line 328193
    :sswitch_2
    const-string v0, "expected a value"

    invoke-virtual {p0, p1, v0}, LX/15v;->b(ILjava/lang/String;)V

    .line 328194
    :sswitch_3
    const-string v0, "true"

    invoke-direct {p0, v0, v1}, LX/1pM;->a(Ljava/lang/String;I)V

    .line 328195
    sget-object v0, LX/15z;->VALUE_TRUE:LX/15z;

    iput-object v0, p0, LX/1pM;->K:LX/15z;

    goto :goto_0

    .line 328196
    :sswitch_4
    const-string v0, "false"

    invoke-direct {p0, v0, v1}, LX/1pM;->a(Ljava/lang/String;I)V

    .line 328197
    sget-object v0, LX/15z;->VALUE_FALSE:LX/15z;

    iput-object v0, p0, LX/1pM;->K:LX/15z;

    goto :goto_0

    .line 328198
    :sswitch_5
    const-string v0, "null"

    invoke-direct {p0, v0, v1}, LX/1pM;->a(Ljava/lang/String;I)V

    .line 328199
    sget-object v0, LX/15z;->VALUE_NULL:LX/15z;

    iput-object v0, p0, LX/1pM;->K:LX/15z;

    goto :goto_0

    .line 328200
    :sswitch_6
    invoke-direct {p0, p1}, LX/1pM;->g(I)LX/15z;

    move-result-object v0

    iput-object v0, p0, LX/1pM;->K:LX/15z;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x2d -> :sswitch_6
        0x30 -> :sswitch_6
        0x31 -> :sswitch_6
        0x32 -> :sswitch_6
        0x33 -> :sswitch_6
        0x34 -> :sswitch_6
        0x35 -> :sswitch_6
        0x36 -> :sswitch_6
        0x37 -> :sswitch_6
        0x38 -> :sswitch_6
        0x39 -> :sswitch_6
        0x5b -> :sswitch_0
        0x5d -> :sswitch_2
        0x66 -> :sswitch_4
        0x6e -> :sswitch_5
        0x74 -> :sswitch_3
        0x7b -> :sswitch_1
        0x7d -> :sswitch_2
    .end sparse-switch
.end method

.method private f(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 328435
    const-string v0, "\'null\', \'true\', \'false\' or NaN"

    invoke-direct {p0, p1, v0}, LX/1pM;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 328436
    return-void
.end method

.method private g(I)LX/15z;
    .locals 11

    .prologue
    const/16 v10, 0x39

    const/16 v2, 0x2d

    const/16 v9, 0x30

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 328404
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->l()[C

    move-result-object v1

    .line 328405
    if-ne p1, v2, :cond_2

    move v4, v5

    .line 328406
    :goto_0
    if-eqz v4, :cond_b

    .line 328407
    aput-char v2, v1, v7

    .line 328408
    iget v0, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v0, v2, :cond_0

    .line 328409
    invoke-virtual {p0}, LX/15u;->K()V

    .line 328410
    :cond_0
    iget-object v0, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    .line 328411
    if-lt v0, v9, :cond_1

    if-le v0, v10, :cond_3

    .line 328412
    :cond_1
    invoke-direct {p0, v0, v5}, LX/1pM;->a(IZ)LX/15z;

    move-result-object v0

    .line 328413
    :goto_1
    return-object v0

    :cond_2
    move v4, v7

    .line 328414
    goto :goto_0

    :cond_3
    move v3, v5

    .line 328415
    :goto_2
    if-ne v0, v9, :cond_4

    .line 328416
    invoke-direct {p0}, LX/1pM;->W()I

    move-result v0

    .line 328417
    :cond_4
    add-int/lit8 v2, v3, 0x1

    int-to-char v0, v0

    aput-char v0, v1, v3

    .line 328418
    iget v0, p0, LX/15u;->d:I

    array-length v3, v1

    add-int/2addr v0, v3

    .line 328419
    iget v3, p0, LX/15u;->e:I

    if-le v0, v3, :cond_5

    .line 328420
    iget v0, p0, LX/15u;->e:I

    .line 328421
    :cond_5
    :goto_3
    iget v3, p0, LX/15u;->d:I

    if-lt v3, v0, :cond_6

    .line 328422
    invoke-direct {p0, v1, v2, v4, v5}, LX/1pM;->a([CIZI)LX/15z;

    move-result-object v0

    goto :goto_1

    .line 328423
    :cond_6
    iget-object v3, p0, LX/1pM;->R:[B

    iget v6, p0, LX/15u;->d:I

    add-int/lit8 v8, v6, 0x1

    iput v8, p0, LX/1pM;->d:I

    aget-byte v3, v3, v6

    and-int/lit16 v3, v3, 0xff

    .line 328424
    if-lt v3, v9, :cond_7

    if-gt v3, v10, :cond_7

    .line 328425
    add-int/lit8 v5, v5, 0x1

    .line 328426
    array-length v6, v1

    if-lt v2, v6, :cond_a

    .line 328427
    iget-object v1, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v1}, LX/15x;->n()[C

    move-result-object v1

    move v6, v7

    .line 328428
    :goto_4
    add-int/lit8 v2, v6, 0x1

    int-to-char v3, v3

    aput-char v3, v1, v6

    goto :goto_3

    .line 328429
    :cond_7
    const/16 v0, 0x2e

    if-eq v3, v0, :cond_8

    const/16 v0, 0x65

    if-eq v3, v0, :cond_8

    const/16 v0, 0x45

    if-ne v3, v0, :cond_9

    :cond_8
    move-object v0, p0

    .line 328430
    invoke-direct/range {v0 .. v5}, LX/1pM;->a([CIIZI)LX/15z;

    move-result-object v0

    goto :goto_1

    .line 328431
    :cond_9
    iget v0, p0, LX/15u;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1pM;->d:I

    .line 328432
    iget-object v0, p0, LX/15u;->n:LX/15x;

    .line 328433
    iput v2, v0, LX/15x;->j:I

    .line 328434
    invoke-virtual {p0, v4, v5}, LX/15u;->a(ZI)LX/15z;

    move-result-object v0

    goto :goto_1

    :cond_a
    move v6, v2

    goto :goto_4

    :cond_b
    move v3, v7

    move v0, p1

    goto :goto_2
.end method

.method private h(I)LX/0lx;
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v4, 0x0

    const/16 v5, 0x22

    .line 328366
    if-eq p1, v5, :cond_0

    .line 328367
    invoke-direct {p0, p1}, LX/1pM;->j(I)LX/0lx;

    move-result-object v0

    .line 328368
    :goto_0
    return-object v0

    .line 328369
    :cond_0
    iget v0, p0, LX/15u;->d:I

    add-int/lit8 v0, v0, 0x9

    iget v1, p0, LX/15u;->e:I

    if-le v0, v1, :cond_1

    .line 328370
    invoke-direct {p0}, LX/1pM;->X()LX/0lx;

    move-result-object v0

    goto :goto_0

    .line 328371
    :cond_1
    iget-object v0, p0, LX/1pM;->R:[B

    .line 328372
    sget-object v1, LX/1pM;->U:[I

    .line 328373
    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v2, v0, v2

    and-int/lit16 v2, v2, 0xff

    .line 328374
    aget v3, v1, v2

    if-nez v3, :cond_a

    .line 328375
    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/1pM;->d:I

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0xff

    .line 328376
    aget v4, v1, v3

    if-nez v4, :cond_8

    .line 328377
    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v2, v3

    .line 328378
    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/1pM;->d:I

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0xff

    .line 328379
    aget v4, v1, v3

    if-nez v4, :cond_6

    .line 328380
    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v2, v3

    .line 328381
    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/1pM;->d:I

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0xff

    .line 328382
    aget v4, v1, v3

    if-nez v4, :cond_4

    .line 328383
    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v2, v3

    .line 328384
    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/1pM;->d:I

    aget-byte v0, v0, v3

    and-int/lit16 v0, v0, 0xff

    .line 328385
    aget v3, v1, v0

    if-nez v3, :cond_2

    .line 328386
    iput v2, p0, LX/1pM;->V:I

    .line 328387
    invoke-direct {p0, v0, v1}, LX/1pM;->a(I[I)LX/0lx;

    move-result-object v0

    goto :goto_0

    .line 328388
    :cond_2
    if-ne v0, v5, :cond_3

    .line 328389
    const/4 v0, 0x4

    invoke-direct {p0, v2, v0}, LX/1pM;->a(II)LX/0lx;

    move-result-object v0

    goto :goto_0

    .line 328390
    :cond_3
    const/4 v1, 0x4

    invoke-direct {p0, v2, v0, v1}, LX/1pM;->a(III)LX/0lx;

    move-result-object v0

    goto :goto_0

    .line 328391
    :cond_4
    if-ne v3, v5, :cond_5

    .line 328392
    invoke-direct {p0, v2, v8}, LX/1pM;->a(II)LX/0lx;

    move-result-object v0

    goto :goto_0

    .line 328393
    :cond_5
    invoke-direct {p0, v2, v3, v8}, LX/1pM;->a(III)LX/0lx;

    move-result-object v0

    goto/16 :goto_0

    .line 328394
    :cond_6
    if-ne v3, v5, :cond_7

    .line 328395
    invoke-direct {p0, v2, v7}, LX/1pM;->a(II)LX/0lx;

    move-result-object v0

    goto/16 :goto_0

    .line 328396
    :cond_7
    invoke-direct {p0, v2, v3, v7}, LX/1pM;->a(III)LX/0lx;

    move-result-object v0

    goto/16 :goto_0

    .line 328397
    :cond_8
    if-ne v3, v5, :cond_9

    .line 328398
    invoke-direct {p0, v2, v6}, LX/1pM;->a(II)LX/0lx;

    move-result-object v0

    goto/16 :goto_0

    .line 328399
    :cond_9
    invoke-direct {p0, v2, v3, v6}, LX/1pM;->a(III)LX/0lx;

    move-result-object v0

    goto/16 :goto_0

    .line 328400
    :cond_a
    if-ne v2, v5, :cond_b

    .line 328401
    sget-object v0, LX/2CO;->c:LX/2CO;

    move-object v0, v0

    .line 328402
    goto/16 :goto_0

    .line 328403
    :cond_b
    invoke-direct {p0, v4, v2, v4}, LX/1pM;->a(III)LX/0lx;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private i(I)LX/0lx;
    .locals 12

    .prologue
    const/4 v10, 0x1

    const/4 v3, 0x0

    const/4 v11, 0x4

    const/4 v5, 0x2

    const/16 v8, 0x22

    .line 328335
    sget-object v1, LX/1pM;->U:[I

    move v2, v5

    move v4, p1

    .line 328336
    :goto_0
    iget v0, p0, LX/15u;->e:I

    iget v6, p0, LX/15u;->d:I

    sub-int/2addr v0, v6

    if-ge v0, v11, :cond_0

    .line 328337
    iget-object v1, p0, LX/1pM;->O:[I

    move-object v0, p0

    move v5, v3

    invoke-direct/range {v0 .. v5}, LX/1pM;->a([IIIII)LX/0lx;

    move-result-object v0

    .line 328338
    :goto_1
    return-object v0

    .line 328339
    :cond_0
    iget-object v0, p0, LX/1pM;->R:[B

    iget v6, p0, LX/15u;->d:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, LX/1pM;->d:I

    aget-byte v0, v0, v6

    and-int/lit16 v9, v0, 0xff

    .line 328340
    aget v0, v1, v9

    if-eqz v0, :cond_2

    .line 328341
    if-ne v9, v8, :cond_1

    .line 328342
    iget-object v0, p0, LX/1pM;->O:[I

    invoke-direct {p0, v0, v2, v4, v10}, LX/1pM;->a([IIII)LX/0lx;

    move-result-object v0

    goto :goto_1

    .line 328343
    :cond_1
    iget-object v6, p0, LX/1pM;->O:[I

    move-object v5, p0

    move v7, v2

    move v8, v4

    invoke-direct/range {v5 .. v10}, LX/1pM;->a([IIIII)LX/0lx;

    move-result-object v0

    goto :goto_1

    .line 328344
    :cond_2
    shl-int/lit8 v0, v4, 0x8

    or-int v6, v0, v9

    .line 328345
    iget-object v0, p0, LX/1pM;->R:[B

    iget v4, p0, LX/15u;->d:I

    add-int/lit8 v7, v4, 0x1

    iput v7, p0, LX/1pM;->d:I

    aget-byte v0, v0, v4

    and-int/lit16 v4, v0, 0xff

    .line 328346
    aget v0, v1, v4

    if-eqz v0, :cond_4

    .line 328347
    if-ne v4, v8, :cond_3

    .line 328348
    iget-object v0, p0, LX/1pM;->O:[I

    invoke-direct {p0, v0, v2, v6, v5}, LX/1pM;->a([IIII)LX/0lx;

    move-result-object v0

    goto :goto_1

    .line 328349
    :cond_3
    iget-object v1, p0, LX/1pM;->O:[I

    move-object v0, p0

    move v3, v6

    invoke-direct/range {v0 .. v5}, LX/1pM;->a([IIIII)LX/0lx;

    move-result-object v0

    goto :goto_1

    .line 328350
    :cond_4
    shl-int/lit8 v0, v6, 0x8

    or-int v6, v0, v4

    .line 328351
    iget-object v0, p0, LX/1pM;->R:[B

    iget v4, p0, LX/15u;->d:I

    add-int/lit8 v7, v4, 0x1

    iput v7, p0, LX/1pM;->d:I

    aget-byte v0, v0, v4

    and-int/lit16 v4, v0, 0xff

    .line 328352
    aget v0, v1, v4

    if-eqz v0, :cond_6

    .line 328353
    if-ne v4, v8, :cond_5

    .line 328354
    iget-object v0, p0, LX/1pM;->O:[I

    const/4 v1, 0x3

    invoke-direct {p0, v0, v2, v6, v1}, LX/1pM;->a([IIII)LX/0lx;

    move-result-object v0

    goto :goto_1

    .line 328355
    :cond_5
    iget-object v1, p0, LX/1pM;->O:[I

    const/4 v5, 0x3

    move-object v0, p0

    move v3, v6

    invoke-direct/range {v0 .. v5}, LX/1pM;->a([IIIII)LX/0lx;

    move-result-object v0

    goto :goto_1

    .line 328356
    :cond_6
    shl-int/lit8 v0, v6, 0x8

    or-int v6, v0, v4

    .line 328357
    iget-object v0, p0, LX/1pM;->R:[B

    iget v4, p0, LX/15u;->d:I

    add-int/lit8 v7, v4, 0x1

    iput v7, p0, LX/1pM;->d:I

    aget-byte v0, v0, v4

    and-int/lit16 v4, v0, 0xff

    .line 328358
    aget v0, v1, v4

    if-eqz v0, :cond_8

    .line 328359
    if-ne v4, v8, :cond_7

    .line 328360
    iget-object v0, p0, LX/1pM;->O:[I

    invoke-direct {p0, v0, v2, v6, v11}, LX/1pM;->a([IIII)LX/0lx;

    move-result-object v0

    goto/16 :goto_1

    .line 328361
    :cond_7
    iget-object v1, p0, LX/1pM;->O:[I

    move-object v0, p0

    move v3, v6

    move v5, v11

    invoke-direct/range {v0 .. v5}, LX/1pM;->a([IIIII)LX/0lx;

    move-result-object v0

    goto/16 :goto_1

    .line 328362
    :cond_8
    iget-object v0, p0, LX/1pM;->O:[I

    array-length v0, v0

    if-lt v2, v0, :cond_9

    .line 328363
    iget-object v0, p0, LX/1pM;->O:[I

    invoke-static {v0, v2}, LX/1pM;->a([II)[I

    move-result-object v0

    iput-object v0, p0, LX/1pM;->O:[I

    .line 328364
    :cond_9
    iget-object v7, p0, LX/1pM;->O:[I

    add-int/lit8 v0, v2, 0x1

    aput v6, v7, v2

    move v2, v0

    .line 328365
    goto/16 :goto_0
.end method

.method private j(I)LX/0lx;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 328306
    const/16 v1, 0x27

    if-ne p1, v1, :cond_0

    sget-object v1, LX/0lr;->ALLOW_SINGLE_QUOTES:LX/0lr;

    invoke-virtual {p0, v1}, LX/15w;->a(LX/0lr;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 328307
    invoke-direct {p0}, LX/1pM;->Y()LX/0lx;

    move-result-object v0

    .line 328308
    :goto_0
    return-object v0

    .line 328309
    :cond_0
    sget-object v1, LX/0lr;->ALLOW_UNQUOTED_FIELD_NAMES:LX/0lr;

    invoke-virtual {p0, v1}, LX/15w;->a(LX/0lr;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 328310
    const-string v1, "was expecting double-quote to start field name"

    invoke-virtual {p0, p1, v1}, LX/15v;->b(ILjava/lang/String;)V

    .line 328311
    :cond_1
    sget-object v1, LX/12H;->d:[I

    move-object v6, v1

    .line 328312
    aget v1, v6, p1

    if-eqz v1, :cond_2

    .line 328313
    const-string v1, "was expecting either valid name character (for unquoted name) or double-quote (for quoted) to start field name"

    invoke-virtual {p0, p1, v1}, LX/15v;->b(ILjava/lang/String;)V

    .line 328314
    :cond_2
    iget-object v3, p0, LX/1pM;->O:[I

    move v1, v0

    move v5, v0

    move v2, p1

    move-object v7, v3

    move v3, v0

    move-object v0, v7

    .line 328315
    :goto_1
    const/4 v4, 0x4

    if-ge v1, v4, :cond_4

    .line 328316
    add-int/lit8 v1, v1, 0x1

    .line 328317
    shl-int/lit8 v4, v5, 0x8

    or-int/2addr v2, v4

    move v7, v1

    move v1, v2

    move v2, v3

    move-object v3, v0

    move v0, v7

    .line 328318
    :goto_2
    iget v4, p0, LX/15u;->d:I

    iget v5, p0, LX/15u;->e:I

    if-lt v4, v5, :cond_3

    .line 328319
    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v4

    if-nez v4, :cond_3

    .line 328320
    const-string v4, " in field name"

    invoke-virtual {p0, v4}, LX/15v;->d(Ljava/lang/String;)V

    .line 328321
    :cond_3
    iget-object v4, p0, LX/1pM;->R:[B

    iget v5, p0, LX/15u;->d:I

    aget-byte v4, v4, v5

    and-int/lit16 p1, v4, 0xff

    .line 328322
    aget v4, v6, p1

    if-nez v4, :cond_6

    .line 328323
    iget v4, p0, LX/15u;->d:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/1pM;->d:I

    move v5, v1

    move v1, v0

    move-object v0, v3

    move v3, v2

    move v2, p1

    goto :goto_1

    .line 328324
    :cond_4
    array-length v1, v0

    if-lt v3, v1, :cond_5

    .line 328325
    array-length v1, v0

    invoke-static {v0, v1}, LX/1pM;->a([II)[I

    move-result-object v0

    iput-object v0, p0, LX/1pM;->O:[I

    .line 328326
    :cond_5
    add-int/lit8 v4, v3, 0x1

    aput v5, v0, v3

    .line 328327
    const/4 v1, 0x1

    move-object v3, v0

    move v0, v1

    move v1, v2

    move v2, v4

    goto :goto_2

    .line 328328
    :cond_6
    if-lez v0, :cond_8

    .line 328329
    array-length v4, v3

    if-lt v2, v4, :cond_7

    .line 328330
    array-length v4, v3

    invoke-static {v3, v4}, LX/1pM;->a([II)[I

    move-result-object v3

    iput-object v3, p0, LX/1pM;->O:[I

    .line 328331
    :cond_7
    add-int/lit8 v4, v2, 0x1

    aput v1, v3, v2

    move v2, v4

    .line 328332
    :cond_8
    iget-object v1, p0, LX/1pM;->N:LX/0lv;

    invoke-virtual {v1, v3, v2}, LX/0lv;->a([II)LX/0lx;

    move-result-object v1

    .line 328333
    if-nez v1, :cond_9

    .line 328334
    invoke-direct {p0, v3, v2, v0}, LX/1pM;->a([III)LX/0lx;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private k(I)LX/15z;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 328289
    sparse-switch p1, :sswitch_data_0

    .line 328290
    :cond_0
    :goto_0
    const-string v0, "expected a valid value (number, String, array, object, \'true\', \'false\' or \'null\')"

    invoke-virtual {p0, p1, v0}, LX/15v;->b(ILjava/lang/String;)V

    .line 328291
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 328292
    :sswitch_0
    sget-object v0, LX/0lr;->ALLOW_SINGLE_QUOTES:LX/0lr;

    invoke-virtual {p0, v0}, LX/15w;->a(LX/0lr;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328293
    invoke-direct {p0}, LX/1pM;->aa()LX/15z;

    move-result-object v0

    goto :goto_1

    .line 328294
    :sswitch_1
    const-string v0, "NaN"

    invoke-direct {p0, v0, v1}, LX/1pM;->a(Ljava/lang/String;I)V

    .line 328295
    sget-object v0, LX/0lr;->ALLOW_NON_NUMERIC_NUMBERS:LX/0lr;

    invoke-virtual {p0, v0}, LX/15w;->a(LX/0lr;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 328296
    const-string v0, "NaN"

    const-wide/high16 v2, 0x7ff8000000000000L    # NaN

    invoke-virtual {p0, v0, v2, v3}, LX/15u;->a(Ljava/lang/String;D)LX/15z;

    move-result-object v0

    goto :goto_1

    .line 328297
    :cond_1
    const-string v0, "Non-standard token \'NaN\': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow"

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 328298
    :sswitch_2
    const-string v0, "Infinity"

    invoke-direct {p0, v0, v1}, LX/1pM;->a(Ljava/lang/String;I)V

    .line 328299
    sget-object v0, LX/0lr;->ALLOW_NON_NUMERIC_NUMBERS:LX/0lr;

    invoke-virtual {p0, v0}, LX/15w;->a(LX/0lr;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 328300
    const-string v0, "Infinity"

    const-wide/high16 v2, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    invoke-virtual {p0, v0, v2, v3}, LX/15u;->a(Ljava/lang/String;D)LX/15z;

    move-result-object v0

    goto :goto_1

    .line 328301
    :cond_2
    const-string v0, "Non-standard token \'Infinity\': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow"

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 328302
    :sswitch_3
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_3

    .line 328303
    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v0

    if-nez v0, :cond_3

    .line 328304
    invoke-virtual {p0}, LX/15v;->T()V

    .line 328305
    :cond_3
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/1pM;->d:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/1pM;->a(IZ)LX/15z;

    move-result-object v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x27 -> :sswitch_0
        0x2b -> :sswitch_3
        0x49 -> :sswitch_2
        0x4e -> :sswitch_1
    .end sparse-switch
.end method

.method private l(I)I
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/16 v5, 0x80

    const/4 v1, 0x1

    .line 328263
    if-gez p1, :cond_3

    .line 328264
    and-int/lit16 v0, p1, 0xe0

    const/16 v3, 0xc0

    if-ne v0, v3, :cond_4

    .line 328265
    and-int/lit8 p1, p1, 0x1f

    move v0, v1

    .line 328266
    :goto_0
    invoke-direct {p0}, LX/1pM;->al()I

    move-result v3

    .line 328267
    and-int/lit16 v4, v3, 0xc0

    if-eq v4, v5, :cond_0

    .line 328268
    and-int/lit16 v4, v3, 0xff

    invoke-direct {p0, v4}, LX/1pM;->s(I)V

    .line 328269
    :cond_0
    shl-int/lit8 v4, p1, 0x6

    and-int/lit8 v3, v3, 0x3f

    or-int p1, v4, v3

    .line 328270
    if-le v0, v1, :cond_3

    .line 328271
    invoke-direct {p0}, LX/1pM;->al()I

    move-result v1

    .line 328272
    and-int/lit16 v3, v1, 0xc0

    if-eq v3, v5, :cond_1

    .line 328273
    and-int/lit16 v3, v1, 0xff

    invoke-direct {p0, v3}, LX/1pM;->s(I)V

    .line 328274
    :cond_1
    shl-int/lit8 v3, p1, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int p1, v3, v1

    .line 328275
    if-le v0, v2, :cond_3

    .line 328276
    invoke-direct {p0}, LX/1pM;->al()I

    move-result v0

    .line 328277
    and-int/lit16 v1, v0, 0xc0

    if-eq v1, v5, :cond_2

    .line 328278
    and-int/lit16 v1, v0, 0xff

    invoke-direct {p0, v1}, LX/1pM;->s(I)V

    .line 328279
    :cond_2
    shl-int/lit8 v1, p1, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int p1, v1, v0

    .line 328280
    :cond_3
    return p1

    .line 328281
    :cond_4
    and-int/lit16 v0, p1, 0xf0

    const/16 v3, 0xe0

    if-ne v0, v3, :cond_5

    .line 328282
    and-int/lit8 p1, p1, 0xf

    move v0, v2

    .line 328283
    goto :goto_0

    .line 328284
    :cond_5
    and-int/lit16 v0, p1, 0xf8

    const/16 v3, 0xf0

    if-ne v0, v3, :cond_6

    .line 328285
    and-int/lit8 p1, p1, 0x7

    .line 328286
    const/4 v0, 0x3

    goto :goto_0

    .line 328287
    :cond_6
    and-int/lit16 v0, p1, 0xff

    invoke-direct {p0, v0}, LX/1pM;->r(I)V

    move v0, v1

    .line 328288
    goto :goto_0
.end method

.method private m(I)I
    .locals 3

    .prologue
    .line 328257
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    .line 328258
    invoke-virtual {p0}, LX/15u;->K()V

    .line 328259
    :cond_0
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/1pM;->d:I

    aget-byte v0, v0, v1

    .line 328260
    and-int/lit16 v1, v0, 0xc0

    const/16 v2, 0x80

    if-eq v1, v2, :cond_1

    .line 328261
    and-int/lit16 v1, v0, 0xff

    iget v2, p0, LX/15u;->d:I

    invoke-direct {p0, v1, v2}, LX/1pM;->b(II)V

    .line 328262
    :cond_1
    and-int/lit8 v1, p1, 0x1f

    shl-int/lit8 v1, v1, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/2addr v0, v1

    return v0
.end method

.method private n(I)I
    .locals 5

    .prologue
    const/16 v4, 0x80

    .line 328243
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    .line 328244
    invoke-virtual {p0}, LX/15u;->K()V

    .line 328245
    :cond_0
    and-int/lit8 v0, p1, 0xf

    .line 328246
    iget-object v1, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v1, v1, v2

    .line 328247
    and-int/lit16 v2, v1, 0xc0

    if-eq v2, v4, :cond_1

    .line 328248
    and-int/lit16 v2, v1, 0xff

    iget v3, p0, LX/15u;->d:I

    invoke-direct {p0, v2, v3}, LX/1pM;->b(II)V

    .line 328249
    :cond_1
    shl-int/lit8 v0, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/2addr v0, v1

    .line 328250
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_2

    .line 328251
    invoke-virtual {p0}, LX/15u;->K()V

    .line 328252
    :cond_2
    iget-object v1, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v1, v1, v2

    .line 328253
    and-int/lit16 v2, v1, 0xc0

    if-eq v2, v4, :cond_3

    .line 328254
    and-int/lit16 v2, v1, 0xff

    iget v3, p0, LX/15u;->d:I

    invoke-direct {p0, v2, v3}, LX/1pM;->b(II)V

    .line 328255
    :cond_3
    shl-int/lit8 v0, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/2addr v0, v1

    .line 328256
    return v0
.end method

.method private o(I)I
    .locals 5

    .prologue
    const/16 v4, 0x80

    .line 328233
    and-int/lit8 v0, p1, 0xf

    .line 328234
    iget-object v1, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v1, v1, v2

    .line 328235
    and-int/lit16 v2, v1, 0xc0

    if-eq v2, v4, :cond_0

    .line 328236
    and-int/lit16 v2, v1, 0xff

    iget v3, p0, LX/15u;->d:I

    invoke-direct {p0, v2, v3}, LX/1pM;->b(II)V

    .line 328237
    :cond_0
    shl-int/lit8 v0, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/2addr v0, v1

    .line 328238
    iget-object v1, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v1, v1, v2

    .line 328239
    and-int/lit16 v2, v1, 0xc0

    if-eq v2, v4, :cond_1

    .line 328240
    and-int/lit16 v2, v1, 0xff

    iget v3, p0, LX/15u;->d:I

    invoke-direct {p0, v2, v3}, LX/1pM;->b(II)V

    .line 328241
    :cond_1
    shl-int/lit8 v0, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/2addr v0, v1

    .line 328242
    return v0
.end method

.method private p(I)I
    .locals 5

    .prologue
    const/16 v4, 0x80

    .line 328215
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    .line 328216
    invoke-virtual {p0}, LX/15u;->K()V

    .line 328217
    :cond_0
    iget-object v0, p0, LX/1pM;->R:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/1pM;->d:I

    aget-byte v0, v0, v1

    .line 328218
    and-int/lit16 v1, v0, 0xc0

    if-eq v1, v4, :cond_1

    .line 328219
    and-int/lit16 v1, v0, 0xff

    iget v2, p0, LX/15u;->d:I

    invoke-direct {p0, v1, v2}, LX/1pM;->b(II)V

    .line 328220
    :cond_1
    and-int/lit8 v1, p1, 0x7

    shl-int/lit8 v1, v1, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/2addr v0, v1

    .line 328221
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_2

    .line 328222
    invoke-virtual {p0}, LX/15u;->K()V

    .line 328223
    :cond_2
    iget-object v1, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v1, v1, v2

    .line 328224
    and-int/lit16 v2, v1, 0xc0

    if-eq v2, v4, :cond_3

    .line 328225
    and-int/lit16 v2, v1, 0xff

    iget v3, p0, LX/15u;->d:I

    invoke-direct {p0, v2, v3}, LX/1pM;->b(II)V

    .line 328226
    :cond_3
    shl-int/lit8 v0, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/2addr v0, v1

    .line 328227
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_4

    .line 328228
    invoke-virtual {p0}, LX/15u;->K()V

    .line 328229
    :cond_4
    iget-object v1, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v1, v1, v2

    .line 328230
    and-int/lit16 v2, v1, 0xc0

    if-eq v2, v4, :cond_5

    .line 328231
    and-int/lit16 v2, v1, 0xff

    iget v3, p0, LX/15u;->d:I

    invoke-direct {p0, v2, v3}, LX/1pM;->b(II)V

    .line 328232
    :cond_5
    shl-int/lit8 v0, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/2addr v0, v1

    const/high16 v1, 0x10000

    sub-int/2addr v0, v1

    return v0
.end method

.method private q(I)V
    .locals 1

    .prologue
    .line 328211
    const/16 v0, 0x20

    if-ge p1, v0, :cond_0

    .line 328212
    invoke-virtual {p0, p1}, LX/15v;->d(I)V

    .line 328213
    :cond_0
    invoke-direct {p0, p1}, LX/1pM;->r(I)V

    .line 328214
    return-void
.end method

.method private r(I)V
    .locals 2

    .prologue
    .line 328209
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid UTF-8 start byte 0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 328210
    return-void
.end method

.method private s(I)V
    .locals 2

    .prologue
    .line 328207
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid UTF-8 middle byte 0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 328208
    return-void
.end method


# virtual methods
.method public final I()Ljava/lang/String;
    .locals 2

    .prologue
    .line 328201
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v1, :cond_1

    .line 328202
    iget-boolean v0, p0, LX/1pM;->P:Z

    if-eqz v0, :cond_0

    .line 328203
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1pM;->P:Z

    .line 328204
    invoke-virtual {p0}, LX/1pM;->M()V

    .line 328205
    :cond_0
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    .line 328206
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    invoke-super {p0, v0}, LX/15u;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final L()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 328171
    iget-wide v2, p0, LX/15u;->f:J

    iget v1, p0, LX/15u;->e:I

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/1pM;->f:J

    .line 328172
    iget v1, p0, LX/15u;->h:I

    iget v2, p0, LX/15u;->e:I

    sub-int/2addr v1, v2

    iput v1, p0, LX/1pM;->h:I

    .line 328173
    iget-object v1, p0, LX/1pM;->Q:Ljava/io/InputStream;

    if-eqz v1, :cond_0

    .line 328174
    iget-object v1, p0, LX/1pM;->Q:Ljava/io/InputStream;

    iget-object v2, p0, LX/1pM;->R:[B

    iget-object v3, p0, LX/1pM;->R:[B

    array-length v3, v3

    invoke-virtual {v1, v2, v0, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 328175
    if-lez v1, :cond_1

    .line 328176
    iput v0, p0, LX/1pM;->d:I

    .line 328177
    iput v1, p0, LX/1pM;->e:I

    .line 328178
    const/4 v0, 0x1

    .line 328179
    :cond_0
    return v0

    .line 328180
    :cond_1
    invoke-virtual {p0}, LX/1pM;->N()V

    .line 328181
    if-nez v1, :cond_0

    .line 328182
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InputStream.read() returned 0 characters when trying to read "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/1pM;->R:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final M()V
    .locals 9

    .prologue
    .line 328582
    iget v0, p0, LX/15u;->d:I

    .line 328583
    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    .line 328584
    invoke-virtual {p0}, LX/15u;->K()V

    .line 328585
    iget v0, p0, LX/15u;->d:I

    .line 328586
    :cond_0
    const/4 v1, 0x0

    .line 328587
    iget-object v2, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v2}, LX/15x;->l()[C

    move-result-object v3

    .line 328588
    sget-object v4, LX/1pM;->T:[I

    .line 328589
    iget v2, p0, LX/15u;->e:I

    array-length v5, v3

    add-int/2addr v5, v0

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 328590
    iget-object v6, p0, LX/1pM;->R:[B

    move v8, v1

    move v1, v0

    move v0, v8

    .line 328591
    :goto_0
    if-ge v1, v5, :cond_2

    .line 328592
    aget-byte v2, v6, v1

    and-int/lit16 v7, v2, 0xff

    .line 328593
    aget v2, v4, v7

    if-eqz v2, :cond_1

    .line 328594
    const/16 v2, 0x22

    if-ne v7, v2, :cond_2

    .line 328595
    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/1pM;->d:I

    .line 328596
    iget-object v1, p0, LX/15u;->n:LX/15x;

    .line 328597
    iput v0, v1, LX/15x;->j:I

    .line 328598
    :goto_1
    return-void

    .line 328599
    :cond_1
    add-int/lit8 v2, v1, 0x1

    .line 328600
    add-int/lit8 v1, v0, 0x1

    int-to-char v7, v7

    aput-char v7, v3, v0

    move v0, v1

    move v1, v2

    .line 328601
    goto :goto_0

    .line 328602
    :cond_2
    iput v1, p0, LX/1pM;->d:I

    .line 328603
    invoke-direct {p0, v3, v0}, LX/1pM;->a([CI)V

    goto :goto_1
.end method

.method public final N()V
    .locals 2

    .prologue
    .line 328663
    iget-object v0, p0, LX/1pM;->Q:Ljava/io/InputStream;

    if-eqz v0, :cond_2

    .line 328664
    iget-object v0, p0, LX/15u;->b:LX/12A;

    .line 328665
    iget-boolean v1, v0, LX/12A;->c:Z

    move v0, v1

    .line 328666
    if-nez v0, :cond_0

    sget-object v0, LX/0lr;->AUTO_CLOSE_SOURCE:LX/0lr;

    invoke-virtual {p0, v0}, LX/15w;->a(LX/0lr;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 328667
    :cond_0
    iget-object v0, p0, LX/1pM;->Q:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 328668
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/1pM;->Q:Ljava/io/InputStream;

    .line 328669
    :cond_2
    return-void
.end method

.method public final O()V
    .locals 2

    .prologue
    .line 328656
    invoke-super {p0}, LX/15u;->O()V

    .line 328657
    iget-boolean v0, p0, LX/1pM;->S:Z

    if-eqz v0, :cond_0

    .line 328658
    iget-object v0, p0, LX/1pM;->R:[B

    .line 328659
    if-eqz v0, :cond_0

    .line 328660
    const/4 v1, 0x0

    iput-object v1, p0, LX/1pM;->R:[B

    .line 328661
    iget-object v1, p0, LX/15u;->b:LX/12A;

    invoke-virtual {v1, v0}, LX/12A;->a([B)V

    .line 328662
    :cond_0
    return-void
.end method

.method public final R()C
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 328632
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_0

    .line 328633
    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v1

    if-nez v1, :cond_0

    .line 328634
    const-string v1, " in character escape sequence"

    invoke-virtual {p0, v1}, LX/15v;->d(Ljava/lang/String;)V

    .line 328635
    :cond_0
    iget-object v1, p0, LX/1pM;->R:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/1pM;->d:I

    aget-byte v1, v1, v2

    .line 328636
    sparse-switch v1, :sswitch_data_0

    .line 328637
    invoke-direct {p0, v1}, LX/1pM;->l(I)I

    move-result v0

    int-to-char v0, v0

    invoke-virtual {p0, v0}, LX/15v;->a(C)C

    move-result v0

    .line 328638
    :goto_0
    return v0

    .line 328639
    :sswitch_0
    const/16 v0, 0x8

    goto :goto_0

    .line 328640
    :sswitch_1
    const/16 v0, 0x9

    goto :goto_0

    .line 328641
    :sswitch_2
    const/16 v0, 0xa

    goto :goto_0

    .line 328642
    :sswitch_3
    const/16 v0, 0xc

    goto :goto_0

    .line 328643
    :sswitch_4
    const/16 v0, 0xd

    goto :goto_0

    .line 328644
    :sswitch_5
    int-to-char v0, v1

    goto :goto_0

    :sswitch_6
    move v1, v0

    .line 328645
    :goto_1
    const/4 v2, 0x4

    if-ge v0, v2, :cond_3

    .line 328646
    iget v2, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v2, v3, :cond_1

    .line 328647
    invoke-virtual {p0}, LX/1pM;->L()Z

    move-result v2

    if-nez v2, :cond_1

    .line 328648
    const-string v2, " in character escape sequence"

    invoke-virtual {p0, v2}, LX/15v;->d(Ljava/lang/String;)V

    .line 328649
    :cond_1
    iget-object v2, p0, LX/1pM;->R:[B

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/1pM;->d:I

    aget-byte v2, v2, v3

    .line 328650
    invoke-static {v2}, LX/12H;->a(I)I

    move-result v3

    .line 328651
    if-gez v3, :cond_2

    .line 328652
    const-string v4, "expected a hex-digit for character escape sequence"

    invoke-virtual {p0, v2, v4}, LX/15v;->b(ILjava/lang/String;)V

    .line 328653
    :cond_2
    shl-int/lit8 v1, v1, 0x4

    or-int/2addr v1, v3

    .line 328654
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 328655
    :cond_3
    int-to-char v0, v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_5
        0x2f -> :sswitch_5
        0x5c -> :sswitch_5
        0x62 -> :sswitch_0
        0x66 -> :sswitch_3
        0x6e -> :sswitch_2
        0x72 -> :sswitch_4
        0x74 -> :sswitch_1
        0x75 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(I)I
    .locals 3

    .prologue
    .line 328619
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v1, :cond_3

    .line 328620
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1pM;->p:Z

    .line 328621
    iget-object v0, p0, LX/15u;->m:LX/15z;

    .line 328622
    const/4 v1, 0x0

    iput-object v1, p0, LX/1pM;->m:LX/15z;

    .line 328623
    iput-object v0, p0, LX/1pM;->K:LX/15z;

    .line 328624
    sget-object v1, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    if-ne v0, v1, :cond_1

    .line 328625
    invoke-virtual {p0}, LX/15w;->x()I

    move-result p1

    .line 328626
    :cond_0
    :goto_0
    return p1

    .line 328627
    :cond_1
    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v1, :cond_2

    .line 328628
    iget-object v0, p0, LX/15u;->l:LX/15y;

    iget v1, p0, LX/15u;->j:I

    iget v2, p0, LX/15u;->k:I

    invoke-virtual {v0, v1, v2}, LX/15y;->b(II)LX/15y;

    move-result-object v0

    iput-object v0, p0, LX/1pM;->l:LX/15y;

    goto :goto_0

    .line 328629
    :cond_2
    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_0

    .line 328630
    iget-object v0, p0, LX/15u;->l:LX/15y;

    iget v1, p0, LX/15u;->j:I

    iget v2, p0, LX/15u;->k:I

    invoke-virtual {v0, v1, v2}, LX/15y;->c(II)LX/15y;

    move-result-object v0

    iput-object v0, p0, LX/1pM;->l:LX/15y;

    goto :goto_0

    .line 328631
    :cond_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, LX/15w;->x()I

    move-result p1

    goto :goto_0
.end method

.method public final a(Ljava/io/OutputStream;)I
    .locals 3

    .prologue
    .line 328613
    iget v0, p0, LX/15u;->e:I

    iget v1, p0, LX/15u;->d:I

    sub-int/2addr v0, v1

    .line 328614
    if-gtz v0, :cond_0

    .line 328615
    const/4 v0, 0x0

    .line 328616
    :goto_0
    return v0

    .line 328617
    :cond_0
    iget v1, p0, LX/15u;->d:I

    .line 328618
    iget-object v2, p0, LX/1pM;->R:[B

    invoke-virtual {p1, v2, v1, v0}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0
.end method

.method public final a()LX/0lD;
    .locals 1

    .prologue
    .line 328612
    iget-object v0, p0, LX/1pM;->M:LX/0lD;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 328606
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v1, :cond_1

    .line 328607
    iget-boolean v0, p0, LX/1pM;->P:Z

    if-eqz v0, :cond_0

    .line 328608
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1pM;->P:Z

    .line 328609
    invoke-virtual {p0}, LX/1pM;->M()V

    .line 328610
    :cond_0
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    .line 328611
    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0, p1}, LX/15u;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0lD;)V
    .locals 0

    .prologue
    .line 328604
    iput-object p1, p0, LX/1pM;->M:LX/0lD;

    .line 328605
    return-void
.end method

.method public final a(LX/0ln;)[B
    .locals 3

    .prologue
    .line 328448
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/15u;->r:[B

    if-nez v0, :cond_1

    .line 328449
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current token ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") not VALUE_STRING or VALUE_EMBEDDED_OBJECT, can not access as binary"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 328450
    :cond_1
    iget-boolean v0, p0, LX/1pM;->P:Z

    if-eqz v0, :cond_3

    .line 328451
    :try_start_0
    invoke-direct {p0, p1}, LX/1pM;->b(LX/0ln;)[B

    move-result-object v0

    iput-object v0, p0, LX/1pM;->r:[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 328452
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1pM;->P:Z

    .line 328453
    :cond_2
    :goto_0
    iget-object v0, p0, LX/15u;->r:[B

    return-object v0

    .line 328454
    :catch_0
    move-exception v0

    .line 328455
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to decode VALUE_STRING as base64 ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15w;->b(Ljava/lang/String;)LX/2aQ;

    move-result-object v0

    throw v0

    .line 328456
    :cond_3
    iget-object v0, p0, LX/15u;->r:[B

    if-nez v0, :cond_2

    .line 328457
    invoke-virtual {p0}, LX/15u;->Q()LX/2SG;

    move-result-object v0

    .line 328458
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0, p1}, LX/15v;->a(Ljava/lang/String;LX/2SG;LX/0ln;)V

    .line 328459
    invoke-virtual {v0}, LX/2SG;->c()[B

    move-result-object v0

    iput-object v0, p0, LX/1pM;->r:[B

    goto :goto_0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 328581
    iget-object v0, p0, LX/1pM;->Q:Ljava/io/InputStream;

    return-object v0
.end method

.method public final c()LX/15z;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0x7d

    const/16 v7, 0x5d

    const/4 v6, 0x1

    .line 328520
    const/4 v0, 0x0

    iput v0, p0, LX/1pM;->A:I

    .line 328521
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v2, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v2, :cond_0

    .line 328522
    invoke-direct {p0}, LX/1pM;->V()LX/15z;

    move-result-object v0

    .line 328523
    :goto_0
    return-object v0

    .line 328524
    :cond_0
    iget-boolean v0, p0, LX/1pM;->P:Z

    if-eqz v0, :cond_1

    .line 328525
    invoke-direct {p0}, LX/1pM;->Z()V

    .line 328526
    :cond_1
    invoke-direct {p0}, LX/1pM;->ac()I

    move-result v0

    .line 328527
    if-gez v0, :cond_2

    .line 328528
    invoke-virtual {p0}, LX/15w;->close()V

    .line 328529
    iput-object v1, p0, LX/1pM;->K:LX/15z;

    move-object v0, v1

    goto :goto_0

    .line 328530
    :cond_2
    iget-wide v2, p0, LX/15u;->f:J

    iget v4, p0, LX/15u;->d:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/1pM;->i:J

    .line 328531
    iget v2, p0, LX/15u;->g:I

    iput v2, p0, LX/1pM;->j:I

    .line 328532
    iget v2, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->h:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, LX/1pM;->k:I

    .line 328533
    iput-object v1, p0, LX/1pM;->r:[B

    .line 328534
    if-ne v0, v7, :cond_4

    .line 328535
    iget-object v1, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v1}, LX/12V;->b()Z

    move-result v1

    if-nez v1, :cond_3

    .line 328536
    invoke-virtual {p0, v0, v8}, LX/15u;->a(IC)V

    .line 328537
    :cond_3
    iget-object v0, p0, LX/15u;->l:LX/15y;

    .line 328538
    iget-object v1, v0, LX/15y;->c:LX/15y;

    move-object v0, v1

    .line 328539
    iput-object v0, p0, LX/1pM;->l:LX/15y;

    .line 328540
    sget-object v0, LX/15z;->END_ARRAY:LX/15z;

    iput-object v0, p0, LX/1pM;->K:LX/15z;

    goto :goto_0

    .line 328541
    :cond_4
    if-ne v0, v8, :cond_6

    .line 328542
    iget-object v1, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v1}, LX/12V;->d()Z

    move-result v1

    if-nez v1, :cond_5

    .line 328543
    invoke-virtual {p0, v0, v7}, LX/15u;->a(IC)V

    .line 328544
    :cond_5
    iget-object v0, p0, LX/15u;->l:LX/15y;

    .line 328545
    iget-object v1, v0, LX/15y;->c:LX/15y;

    move-object v0, v1

    .line 328546
    iput-object v0, p0, LX/1pM;->l:LX/15y;

    .line 328547
    sget-object v0, LX/15z;->END_OBJECT:LX/15z;

    iput-object v0, p0, LX/1pM;->K:LX/15z;

    goto :goto_0

    .line 328548
    :cond_6
    iget-object v1, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v1}, LX/15y;->k()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 328549
    const/16 v1, 0x2c

    if-eq v0, v1, :cond_7

    .line 328550
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "was expecting comma to separate "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v2}, LX/12V;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " entries"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/15v;->b(ILjava/lang/String;)V

    .line 328551
    :cond_7
    invoke-direct {p0}, LX/1pM;->ab()I

    move-result v0

    .line 328552
    :cond_8
    iget-object v1, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v1}, LX/12V;->d()Z

    move-result v1

    if-nez v1, :cond_9

    .line 328553
    invoke-direct {p0, v0}, LX/1pM;->f(I)LX/15z;

    move-result-object v0

    goto/16 :goto_0

    .line 328554
    :cond_9
    invoke-direct {p0, v0}, LX/1pM;->h(I)LX/0lx;

    move-result-object v0

    .line 328555
    iget-object v1, p0, LX/15u;->l:LX/15y;

    .line 328556
    iget-object v2, v0, LX/0lx;->a:Ljava/lang/String;

    move-object v0, v2

    .line 328557
    iput-object v0, v1, LX/15y;->f:Ljava/lang/String;

    .line 328558
    sget-object v0, LX/15z;->FIELD_NAME:LX/15z;

    iput-object v0, p0, LX/1pM;->K:LX/15z;

    .line 328559
    invoke-direct {p0}, LX/1pM;->ab()I

    move-result v0

    .line 328560
    const/16 v1, 0x3a

    if-eq v0, v1, :cond_a

    .line 328561
    const-string v1, "was expecting a colon to separate field name and value"

    invoke-virtual {p0, v0, v1}, LX/15v;->b(ILjava/lang/String;)V

    .line 328562
    :cond_a
    invoke-direct {p0}, LX/1pM;->ab()I

    move-result v0

    .line 328563
    const/16 v1, 0x22

    if-ne v0, v1, :cond_b

    .line 328564
    iput-boolean v6, p0, LX/1pM;->P:Z

    .line 328565
    sget-object v0, LX/15z;->VALUE_STRING:LX/15z;

    iput-object v0, p0, LX/1pM;->m:LX/15z;

    .line 328566
    iget-object v0, p0, LX/15v;->K:LX/15z;

    goto/16 :goto_0

    .line 328567
    :cond_b
    sparse-switch v0, :sswitch_data_0

    .line 328568
    invoke-direct {p0, v0}, LX/1pM;->k(I)LX/15z;

    move-result-object v0

    .line 328569
    :goto_1
    iput-object v0, p0, LX/1pM;->m:LX/15z;

    .line 328570
    iget-object v0, p0, LX/15v;->K:LX/15z;

    goto/16 :goto_0

    .line 328571
    :sswitch_0
    sget-object v0, LX/15z;->START_ARRAY:LX/15z;

    goto :goto_1

    .line 328572
    :sswitch_1
    sget-object v0, LX/15z;->START_OBJECT:LX/15z;

    goto :goto_1

    .line 328573
    :sswitch_2
    const-string v1, "expected a value"

    invoke-virtual {p0, v0, v1}, LX/15v;->b(ILjava/lang/String;)V

    .line 328574
    :sswitch_3
    const-string v0, "true"

    invoke-direct {p0, v0, v6}, LX/1pM;->a(Ljava/lang/String;I)V

    .line 328575
    sget-object v0, LX/15z;->VALUE_TRUE:LX/15z;

    goto :goto_1

    .line 328576
    :sswitch_4
    const-string v0, "false"

    invoke-direct {p0, v0, v6}, LX/1pM;->a(Ljava/lang/String;I)V

    .line 328577
    sget-object v0, LX/15z;->VALUE_FALSE:LX/15z;

    goto :goto_1

    .line 328578
    :sswitch_5
    const-string v0, "null"

    invoke-direct {p0, v0, v6}, LX/1pM;->a(Ljava/lang/String;I)V

    .line 328579
    sget-object v0, LX/15z;->VALUE_NULL:LX/15z;

    goto :goto_1

    .line 328580
    :sswitch_6
    invoke-direct {p0, v0}, LX/1pM;->g(I)LX/15z;

    move-result-object v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x2d -> :sswitch_6
        0x30 -> :sswitch_6
        0x31 -> :sswitch_6
        0x32 -> :sswitch_6
        0x33 -> :sswitch_6
        0x34 -> :sswitch_6
        0x35 -> :sswitch_6
        0x36 -> :sswitch_6
        0x37 -> :sswitch_6
        0x38 -> :sswitch_6
        0x39 -> :sswitch_6
        0x5b -> :sswitch_0
        0x5d -> :sswitch_2
        0x66 -> :sswitch_4
        0x6e -> :sswitch_5
        0x74 -> :sswitch_3
        0x7b -> :sswitch_1
        0x7d -> :sswitch_2
    .end sparse-switch
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 328517
    invoke-super {p0}, LX/15u;->close()V

    .line 328518
    iget-object v0, p0, LX/1pM;->N:LX/0lv;

    invoke-virtual {v0}, LX/0lv;->b()V

    .line 328519
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 328501
    iget-object v1, p0, LX/15v;->K:LX/15z;

    sget-object v2, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v1, v2, :cond_4

    .line 328502
    iput-boolean v3, p0, LX/1pM;->p:Z

    .line 328503
    iget-object v1, p0, LX/15u;->m:LX/15z;

    .line 328504
    iput-object v0, p0, LX/1pM;->m:LX/15z;

    .line 328505
    iput-object v1, p0, LX/1pM;->K:LX/15z;

    .line 328506
    sget-object v2, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v1, v2, :cond_2

    .line 328507
    iget-boolean v0, p0, LX/1pM;->P:Z

    if-eqz v0, :cond_0

    .line 328508
    iput-boolean v3, p0, LX/1pM;->P:Z

    .line 328509
    invoke-virtual {p0}, LX/1pM;->M()V

    .line 328510
    :cond_0
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    .line 328511
    :cond_1
    :goto_0
    return-object v0

    .line 328512
    :cond_2
    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_3

    .line 328513
    iget-object v1, p0, LX/15u;->l:LX/15y;

    iget v2, p0, LX/15u;->j:I

    iget v3, p0, LX/15u;->k:I

    invoke-virtual {v1, v2, v3}, LX/15y;->b(II)LX/15y;

    move-result-object v1

    iput-object v1, p0, LX/1pM;->l:LX/15y;

    goto :goto_0

    .line 328514
    :cond_3
    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-ne v1, v2, :cond_1

    .line 328515
    iget-object v1, p0, LX/15u;->l:LX/15y;

    iget v2, p0, LX/15u;->j:I

    iget v3, p0, LX/15u;->k:I

    invoke-virtual {v1, v2, v3}, LX/15y;->c(II)LX/15y;

    move-result-object v1

    iput-object v1, p0, LX/1pM;->l:LX/15y;

    goto :goto_0

    .line 328516
    :cond_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 2

    .prologue
    .line 328495
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v1, :cond_1

    .line 328496
    iget-boolean v0, p0, LX/1pM;->P:Z

    if-eqz v0, :cond_0

    .line 328497
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1pM;->P:Z

    .line 328498
    invoke-virtual {p0}, LX/1pM;->M()V

    .line 328499
    :cond_0
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    .line 328500
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/15v;->K:LX/15z;

    invoke-direct {p0, v0}, LX/1pM;->a(LX/15z;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final p()[C
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 328476
    iget-object v0, p0, LX/15v;->K:LX/15z;

    if-eqz v0, :cond_4

    .line 328477
    sget-object v0, LX/4pi;->a:[I

    iget-object v1, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v1}, LX/15z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 328478
    iget-object v0, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0}, LX/15z;->asCharArray()[C

    move-result-object v0

    .line 328479
    :goto_0
    return-object v0

    .line 328480
    :pswitch_0
    iget-boolean v0, p0, LX/15u;->p:Z

    if-nez v0, :cond_1

    .line 328481
    iget-object v0, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v0

    .line 328482
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 328483
    iget-object v2, p0, LX/15u;->o:[C

    if-nez v2, :cond_2

    .line 328484
    iget-object v2, p0, LX/15u;->b:LX/12A;

    invoke-virtual {v2, v1}, LX/12A;->a(I)[C

    move-result-object v2

    iput-object v2, p0, LX/1pM;->o:[C

    .line 328485
    :cond_0
    :goto_1
    iget-object v2, p0, LX/15u;->o:[C

    invoke-virtual {v0, v3, v1, v2, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 328486
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1pM;->p:Z

    .line 328487
    :cond_1
    iget-object v0, p0, LX/15u;->o:[C

    goto :goto_0

    .line 328488
    :cond_2
    iget-object v2, p0, LX/15u;->o:[C

    array-length v2, v2

    if-ge v2, v1, :cond_0

    .line 328489
    new-array v2, v1, [C

    iput-object v2, p0, LX/1pM;->o:[C

    goto :goto_1

    .line 328490
    :pswitch_1
    iget-boolean v0, p0, LX/1pM;->P:Z

    if-eqz v0, :cond_3

    .line 328491
    iput-boolean v3, p0, LX/1pM;->P:Z

    .line 328492
    invoke-virtual {p0}, LX/1pM;->M()V

    .line 328493
    :cond_3
    :pswitch_2
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->f()[C

    move-result-object v0

    goto :goto_0

    .line 328494
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final q()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 328467
    iget-object v1, p0, LX/15v;->K:LX/15z;

    if-eqz v1, :cond_0

    .line 328468
    sget-object v1, LX/4pi;->a:[I

    iget-object v2, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v2}, LX/15z;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 328469
    iget-object v0, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0}, LX/15z;->asCharArray()[C

    move-result-object v0

    array-length v0, v0

    .line 328470
    :cond_0
    :goto_0
    return v0

    .line 328471
    :pswitch_0
    iget-object v0, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    .line 328472
    :pswitch_1
    iget-boolean v1, p0, LX/1pM;->P:Z

    if-eqz v1, :cond_1

    .line 328473
    iput-boolean v0, p0, LX/1pM;->P:Z

    .line 328474
    invoke-virtual {p0}, LX/1pM;->M()V

    .line 328475
    :cond_1
    :pswitch_2
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->c()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final r()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 328460
    iget-object v1, p0, LX/15v;->K:LX/15z;

    if-eqz v1, :cond_0

    .line 328461
    sget-object v1, LX/4pi;->a:[I

    iget-object v2, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v2}, LX/15z;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 328462
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 328463
    :pswitch_1
    iget-boolean v1, p0, LX/1pM;->P:Z

    if-eqz v1, :cond_1

    .line 328464
    iput-boolean v0, p0, LX/1pM;->P:Z

    .line 328465
    invoke-virtual {p0}, LX/1pM;->M()V

    .line 328466
    :cond_1
    :pswitch_2
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
