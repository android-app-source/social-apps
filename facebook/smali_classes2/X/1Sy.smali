.class public final LX/1Sy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0qs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0qs",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Ru;


# direct methods
.method public constructor <init>(LX/1Ru;)V
    .locals 0

    .prologue
    .line 249349
    iput-object p1, p0, LX/1Sy;->a:LX/1Ru;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(IILjava/lang/Object;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IITT;Z)V"
        }
    .end annotation

    .prologue
    .line 249350
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    invoke-static {v0}, LX/1Ru;->i(LX/1Ru;)V

    .line 249351
    const-string v0, "ObservableAdaptersCollection.onItemMoved"

    const v1, -0x5e33ddfc

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 249352
    :try_start_0
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    invoke-virtual {v0, p1}, LX/1Ru;->c(I)I

    move-result v0

    .line 249353
    iget-object v1, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v1, v1, LX/1Ru;->b:LX/1Sx;

    invoke-virtual {v1, p1}, LX/1Sx;->a(I)LX/1RA;

    move-result-object v1

    .line 249354
    invoke-static {p3}, LX/1Ru;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v2, v3

    .line 249355
    iget-object v3, v1, LX/1RA;->b:Ljava/lang/Object;

    move-object v3, v3

    .line 249356
    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "Moved item should not have changed."

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 249357
    invoke-virtual {v1}, LX/1RA;->a()I

    move-result v2

    .line 249358
    iget-object v3, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v3, v3, LX/1Ru;->n:LX/1OD;

    invoke-virtual {v3, v0, v2}, LX/1OD;->c(II)V

    .line 249359
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v0, v0, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p2, v0, :cond_0

    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    invoke-virtual {v0}, LX/1Ru;->b()I

    move-result v0

    .line 249360
    :goto_0
    iget-object v3, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v3, v3, LX/1Ru;->b:LX/1Sx;

    invoke-virtual {v3, p2, v1}, LX/1Sx;->a(ILX/1RA;)V

    .line 249361
    iget-object v1, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v1, v1, LX/1Ru;->n:LX/1OD;

    invoke-virtual {v1, v0, v2}, LX/1OD;->b(II)V

    .line 249362
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    invoke-static {v0, p1, p4}, LX/1Ru;->a$redex0(LX/1Ru;IZ)V

    .line 249363
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    invoke-static {v0, p2, p4}, LX/1Ru;->a$redex0(LX/1Ru;IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249364
    const v0, 0x13550299

    invoke-static {v0}, LX/02m;->a(I)V

    .line 249365
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    const/4 v1, 0x3

    .line 249366
    iput v1, v0, LX/1Ru;->o:I

    .line 249367
    return-void

    .line 249368
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    invoke-virtual {v0, p2}, LX/1Ru;->c(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 249369
    :catch_0
    move-exception v0

    .line 249370
    :try_start_2
    iget-object v1, p0, LX/1Sy;->a:LX/1Ru;

    .line 249371
    iput-object v0, v1, LX/1Ru;->p:Ljava/lang/Exception;

    .line 249372
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 249373
    :catchall_0
    move-exception v0

    const v1, 0x7ab4900b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(ILjava/lang/Object;Ljava/lang/Object;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;TT;Z)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 249374
    const-string v0, "ObservableAdaptersCollection.onItemChanged"

    const v1, -0x6102a4b1

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 249375
    :try_start_0
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    invoke-virtual {v0, p1}, LX/1Ru;->c(I)I

    move-result v4

    .line 249376
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v0, v0, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RA;

    .line 249377
    iget-object v1, p0, LX/1Sy;->a:LX/1Ru;

    invoke-static {v1, p1}, LX/1Ru;->h(LX/1Ru;I)LX/1RA;

    move-result-object v5

    .line 249378
    invoke-virtual {v0}, LX/1RA;->a()I

    move-result v6

    .line 249379
    const/4 v1, 0x0

    .line 249380
    invoke-virtual {v0}, LX/1RA;->a()I

    move-result v3

    invoke-virtual {v5}, LX/1RA;->a()I

    move-result v7

    if-eq v3, v7, :cond_5

    .line 249381
    :cond_0
    :goto_0
    move v1, v1

    .line 249382
    if-eqz v1, :cond_2

    .line 249383
    iget-object v1, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v1, v1, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v1, p1, v5}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 249384
    iget-object v1, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v1, v1, LX/1Ru;->n:LX/1OD;

    invoke-virtual {v1, v4, v6}, LX/1OD;->a(II)V

    .line 249385
    iget-object v1, p0, LX/1Sy;->a:LX/1Ru;

    const/4 v2, 0x4

    .line 249386
    iput v2, v1, LX/1Ru;->o:I

    .line 249387
    :cond_1
    :goto_1
    invoke-virtual {v0}, LX/1RA;->c()V

    .line 249388
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    invoke-static {v0, p1, p4}, LX/1Ru;->a$redex0(LX/1Ru;IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249389
    const v0, -0x1b49556b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 249390
    return-void

    .line 249391
    :cond_2
    :try_start_1
    iget-object v1, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v1, v1, LX/1Ru;->q:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v1, v1, LX/1Ru;->q:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v1

    check-cast v1, LX/1OS;

    invoke-interface {v1}, LX/1OS;->I()I

    move-result v1

    move v3, v1

    .line 249392
    :goto_2
    iget-object v1, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v1, v1, LX/1Ru;->q:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-eqz v1, :cond_4

    iget-object v1, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v1, v1, LX/1Ru;->q:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v1, v1, LX/1Ru;->q:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    .line 249393
    :goto_3
    iget-object v2, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v2, v2, LX/1Ru;->b:LX/1Sx;

    invoke-virtual {v2, p1}, LX/1Sx;->a(I)LX/1RA;

    .line 249394
    iget-object v2, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v2, v2, LX/1Ru;->n:LX/1OD;

    invoke-virtual {v2, v4, v6}, LX/1OD;->c(II)V

    .line 249395
    iget-object v2, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v2, v2, LX/1Ru;->b:LX/1Sx;

    invoke-virtual {v2, p1, v5}, LX/1Sx;->a(ILX/1RA;)V

    .line 249396
    iget-object v2, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v2, v2, LX/1Ru;->n:LX/1OD;

    invoke-virtual {v5}, LX/1RA;->a()I

    move-result v5

    invoke-virtual {v2, v4, v5}, LX/1OD;->b(II)V

    .line 249397
    iget-object v2, p0, LX/1Sy;->a:LX/1Ru;

    const/4 v4, 0x7

    .line 249398
    iput v4, v2, LX/1Ru;->o:I

    .line 249399
    iget-object v2, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v2, v2, LX/1Ru;->q:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-eqz v2, :cond_1

    const/4 v2, -0x1

    if-eq v3, v2, :cond_1

    .line 249400
    iget-object v2, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v2, v2, LX/1Ru;->q:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v2, v3, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->g(II)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 249401
    :catch_0
    move-exception v0

    .line 249402
    :try_start_2
    iget-object v1, p0, LX/1Sy;->a:LX/1Ru;

    .line 249403
    iput-object v0, v1, LX/1Ru;->p:Ljava/lang/Exception;

    .line 249404
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 249405
    :catchall_0
    move-exception v0

    const v1, 0x294ad11c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_3
    move v3, v2

    .line 249406
    goto :goto_2

    :cond_4
    move v1, v2

    .line 249407
    goto :goto_3

    .line 249408
    :cond_5
    invoke-virtual {v0}, LX/1RA;->a()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    :goto_4
    if-ltz v3, :cond_6

    .line 249409
    invoke-virtual {v0, v3}, LX/1RA;->b(I)LX/1Cz;

    move-result-object v7

    invoke-virtual {v5, v3}, LX/1RA;->b(I)LX/1Cz;

    move-result-object p2

    invoke-virtual {v7, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 249410
    add-int/lit8 v3, v3, -0x1

    goto :goto_4

    .line 249411
    :cond_6
    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method public final a(ILjava/lang/Object;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;Z)V"
        }
    .end annotation

    .prologue
    .line 249412
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    invoke-static {v0}, LX/1Ru;->i(LX/1Ru;)V

    .line 249413
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v0, v0, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le p1, v0, :cond_0

    .line 249414
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    invoke-static {v0, p1, p3}, LX/1Ru;->b$redex0(LX/1Ru;IZ)V

    .line 249415
    :cond_0
    const-string v0, "ObservableAdaptersCollection.onItemInserted"

    const v1, 0x4dad006e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 249416
    :try_start_0
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v0, v0, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p1, v0, :cond_1

    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    invoke-virtual {v0}, LX/1Ru;->b()I

    move-result v0

    .line 249417
    :goto_0
    iget-object v1, p0, LX/1Sy;->a:LX/1Ru;

    invoke-static {v1, p1}, LX/1Ru;->h(LX/1Ru;I)LX/1RA;

    move-result-object v1

    .line 249418
    iget-object v2, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v2, v2, LX/1Ru;->b:LX/1Sx;

    invoke-virtual {v2, p1, v1}, LX/1Sx;->a(ILX/1RA;)V

    .line 249419
    iget-object v2, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v2, v2, LX/1Ru;->n:LX/1OD;

    invoke-virtual {v1}, LX/1RA;->a()I

    move-result v1

    invoke-virtual {v2, v0, v1}, LX/1OD;->b(II)V

    .line 249420
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    invoke-static {v0, p1, p3}, LX/1Ru;->a$redex0(LX/1Ru;IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249421
    const v0, 0xb7df469

    invoke-static {v0}, LX/02m;->a(I)V

    .line 249422
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    const/4 v1, 0x1

    .line 249423
    iput v1, v0, LX/1Ru;->o:I

    .line 249424
    return-void

    .line 249425
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    invoke-virtual {v0, p1}, LX/1Ru;->c(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 249426
    :catch_0
    move-exception v0

    .line 249427
    :try_start_2
    iget-object v1, p0, LX/1Sy;->a:LX/1Ru;

    .line 249428
    iput-object v0, v1, LX/1Ru;->p:Ljava/lang/Exception;

    .line 249429
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 249430
    :catchall_0
    move-exception v0

    const v1, -0x18dc5f00

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b(ILjava/lang/Object;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;Z)V"
        }
    .end annotation

    .prologue
    .line 249431
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    invoke-static {v0}, LX/1Ru;->i(LX/1Ru;)V

    .line 249432
    const-string v0, "ObservableAdaptersCollection.onItemRemoved"

    const v1, 0x2c56e64a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 249433
    :try_start_0
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    invoke-virtual {v0, p1}, LX/1Ru;->c(I)I

    move-result v0

    .line 249434
    iget-object v1, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v1, v1, LX/1Ru;->b:LX/1Sx;

    invoke-virtual {v1, p1}, LX/1Sx;->a(I)LX/1RA;

    move-result-object v1

    .line 249435
    invoke-virtual {v1}, LX/1RA;->c()V

    .line 249436
    iget-object v2, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v2, v2, LX/1Ru;->n:LX/1OD;

    invoke-virtual {v1}, LX/1RA;->a()I

    move-result v1

    invoke-virtual {v2, v0, v1}, LX/1OD;->c(II)V

    .line 249437
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    iget-object v0, v0, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 249438
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    invoke-static {v0, p1, p3}, LX/1Ru;->a$redex0(LX/1Ru;IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249439
    :cond_0
    const v0, 0x7a8d786

    invoke-static {v0}, LX/02m;->a(I)V

    .line 249440
    iget-object v0, p0, LX/1Sy;->a:LX/1Ru;

    const/4 v1, 0x2

    .line 249441
    iput v1, v0, LX/1Ru;->o:I

    .line 249442
    return-void

    .line 249443
    :catch_0
    move-exception v0

    .line 249444
    :try_start_1
    iget-object v1, p0, LX/1Sy;->a:LX/1Ru;

    .line 249445
    iput-object v0, v1, LX/1Ru;->p:Ljava/lang/Exception;

    .line 249446
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 249447
    :catchall_0
    move-exception v0

    const v1, -0x7ef3a18e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
