.class public LX/0kd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "LifetimeActivityListenerUse"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/0kd;


# instance fields
.field private final a:LX/0Sh;

.field private final b:LX/0Uo;

.field public final c:LX/0So;

.field private final d:LX/0ks;

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public f:J

.field public g:J

.field public h:J

.field public i:J


# direct methods
.method public constructor <init>(LX/0Sh;LX/0Uo;LX/0So;LX/0ks;)V
    .locals 4
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 127654
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127655
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 127656
    iput-object v0, p0, LX/0kd;->e:LX/0Ot;

    .line 127657
    iput-wide v2, p0, LX/0kd;->f:J

    .line 127658
    iput-wide v2, p0, LX/0kd;->g:J

    .line 127659
    iput-wide v2, p0, LX/0kd;->h:J

    .line 127660
    iput-wide v2, p0, LX/0kd;->i:J

    .line 127661
    iput-object p1, p0, LX/0kd;->a:LX/0Sh;

    .line 127662
    iput-object p2, p0, LX/0kd;->b:LX/0Uo;

    .line 127663
    iput-object p3, p0, LX/0kd;->c:LX/0So;

    .line 127664
    iput-object p4, p0, LX/0kd;->d:LX/0ks;

    .line 127665
    return-void
.end method

.method public static a(LX/0QB;)LX/0kd;
    .locals 7

    .prologue
    .line 127699
    sget-object v0, LX/0kd;->j:LX/0kd;

    if-nez v0, :cond_1

    .line 127700
    const-class v1, LX/0kd;

    monitor-enter v1

    .line 127701
    :try_start_0
    sget-object v0, LX/0kd;->j:LX/0kd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 127702
    if-eqz v2, :cond_0

    .line 127703
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 127704
    new-instance p0, LX/0kd;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v4

    check-cast v4, LX/0Uo;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {v0}, LX/0ks;->a(LX/0QB;)LX/0ks;

    move-result-object v6

    check-cast v6, LX/0ks;

    invoke-direct {p0, v3, v4, v5, v6}, LX/0kd;-><init>(LX/0Sh;LX/0Uo;LX/0So;LX/0ks;)V

    .line 127705
    const/16 v3, 0x259

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 127706
    iput-object v3, p0, LX/0kd;->e:LX/0Ot;

    .line 127707
    move-object v0, p0

    .line 127708
    sput-object v0, LX/0kd;->j:LX/0kd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127709
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 127710
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 127711
    :cond_1
    sget-object v0, LX/0kd;->j:LX/0kd;

    return-object v0

    .line 127712
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 127713
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(J)Z
    .locals 6

    .prologue
    .line 127696
    invoke-direct {p0, p1, p2}, LX/0kd;->b(J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2}, LX/0kd;->c(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127697
    iget-wide v1, p0, LX/0kd;->g:J

    iget-wide v3, p0, LX/0kd;->h:J

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x3e8

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    iget-wide v1, p0, LX/0kd;->g:J

    sub-long v1, p1, v1

    const-wide/16 v3, 0x1388

    cmp-long v1, v1, v3

    if-gez v1, :cond_2

    iget-wide v1, p0, LX/0kd;->g:J

    iget-wide v3, p0, LX/0kd;->i:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 127698
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private b(J)Z
    .locals 7

    .prologue
    .line 127693
    iget-wide v0, p0, LX/0kd;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0kd;->b:LX/0Uo;

    .line 127694
    iget-wide v5, v0, LX/0Uo;->J:J

    move-wide v0, v5

    .line 127695
    sub-long v0, p1, v0

    const-wide/16 v2, 0x3a98

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(J)Z
    .locals 5

    .prologue
    .line 127714
    iget-wide v0, p0, LX/0kd;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, LX/0kd;->f:J

    iget-wide v2, p0, LX/0kd;->i:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, LX/0kd;->f:J

    iget-wide v2, p0, LX/0kd;->h:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, LX/0kd;->f:J

    sub-long v0, p1, v0

    const-wide/16 v2, 0x2710

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized e(LX/0kd;J)V
    .locals 1

    .prologue
    .line 127689
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, LX/0kd;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127690
    const v0, 0x43087f84

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127691
    :cond_0
    monitor-exit p0

    return-void

    .line 127692
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 127688
    iget-object v0, p0, LX/0kd;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LX/0kd;->a(J)Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized b()V
    .locals 8

    .prologue
    .line 127671
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0kd;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 127672
    iget-object v0, p0, LX/0kd;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 127673
    invoke-direct {p0, v0, v1}, LX/0kd;->a(J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 127674
    :goto_0
    monitor-exit p0

    return-void

    .line 127675
    :cond_0
    :try_start_1
    invoke-direct {p0, v0, v1}, LX/0kd;->b(J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 127676
    const-wide/16 v2, 0x3a98

    iget-object v4, p0, LX/0kd;->b:LX/0Uo;

    .line 127677
    iget-wide v6, v4, LX/0Uo;->J:J

    move-wide v4, v6

    .line 127678
    sub-long/2addr v0, v4

    sub-long v0, v2, v0

    move-wide v2, v0

    .line 127679
    :goto_1
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    const-string v1, "remaining time < 0: %d"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v0, v1, v4}, LX/0Tp;->a(ZLjava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127680
    const v0, -0x33c8530b    # -4.814946E7f

    :try_start_2
    invoke-static {p0, v2, v3, v0}, LX/02L;->a(Ljava/lang/Object;JI)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 127681
    :catch_0
    move-exception v1

    .line 127682
    :try_start_3
    iget-object v0, p0, LX/0kd;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "AppStartupNotifier"

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 127683
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 127684
    :cond_1
    :try_start_4
    invoke-direct {p0, v0, v1}, LX/0kd;->c(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 127685
    const-wide/16 v2, 0x2710

    iget-wide v4, p0, LX/0kd;->f:J

    sub-long/2addr v0, v4

    sub-long v0, v2, v0

    move-wide v2, v0

    goto :goto_1

    .line 127686
    :cond_2
    const-wide/16 v2, 0x1388

    iget-wide v4, p0, LX/0kd;->g:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    sub-long/2addr v0, v4

    sub-long v0, v2, v0

    move-wide v2, v0

    goto :goto_1

    .line 127687
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 127666
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0kd;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/0kd;->i:J

    .line 127667
    iget-object v0, p0, LX/0kd;->d:LX/0ks;

    invoke-virtual {v0}, LX/0ks;->c()V

    .line 127668
    const v0, -0x854241f

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127669
    monitor-exit p0

    return-void

    .line 127670
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
