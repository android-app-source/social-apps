.class public LX/0iJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$ProvidesInspirationLoggingData;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$SetsCameraState",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# static fields
.field public static final b:[Ljava/lang/String;

.field public static final c:[Ljava/lang/String;

.field public static final d:LX/0jK;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;",
            ">;"
        }
    .end annotation
.end field

.field public final e:[I

.field public final f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final g:LX/ArL;

.field public final h:LX/ArT;

.field public final i:LX/0zw;

.field public final j:LX/0gc;

.field public final k:LX/Avn;

.field public final l:LX/0iA;

.field public final m:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public n:LX/0i6;

.field public o:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

.field public p:Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

.field public q:LX/0iB;

.field public r:Z

.field public s:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public t:LX/ArA;

.field public u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;"
        }
    .end annotation
.end field

.field public v:I

.field public final w:Landroid/view/View$OnClickListener;

.field public final x:Landroid/view/View$OnClickListener;

.field public y:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 120666
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "android.permission.CAMERA"

    aput-object v1, v0, v2

    sput-object v0, LX/0iJ;->b:[Ljava/lang/String;

    .line 120667
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v1, v0, v2

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v1, v0, v3

    sput-object v0, LX/0iJ;->c:[Ljava/lang/String;

    .line 120668
    const-class v0, LX/0iJ;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/0iJ;->d:LX/0jK;

    return-void
.end method

.method public constructor <init>(LX/0il;LX/ArL;LX/ArT;LX/0gc;Landroid/app/Activity;LX/0zw;LX/0i4;LX/Avn;LX/0iA;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 4
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/ArL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/ArT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0zw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/ArL;",
            "LX/ArT;",
            "LX/0gc;",
            "Landroid/app/Activity;",
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;",
            "LX/0i4;",
            "LX/Avn;",
            "LX/0iA;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 120740
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120741
    const/4 v0, 0x4

    new-array v0, v0, [I

    const v1, 0x7f0a0782

    aput v1, v0, v3

    const/4 v1, 0x1

    const v2, 0x7f0a0783

    aput v2, v0, v1

    const/4 v1, 0x2

    const v2, 0x7f0a0784

    aput v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x7f0a0785

    aput v2, v0, v1

    iput-object v0, p0, LX/0iJ;->e:[I

    .line 120742
    new-instance v0, LX/Avo;

    invoke-direct {v0, p0}, LX/Avo;-><init>(LX/0iJ;)V

    iput-object v0, p0, LX/0iJ;->w:Landroid/view/View$OnClickListener;

    .line 120743
    new-instance v0, LX/Avp;

    invoke-direct {v0, p0}, LX/Avp;-><init>(LX/0iJ;)V

    iput-object v0, p0, LX/0iJ;->x:Landroid/view/View$OnClickListener;

    .line 120744
    iput-boolean v3, p0, LX/0iJ;->y:Z

    .line 120745
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/0iJ;->f:Ljava/lang/ref/WeakReference;

    .line 120746
    iput-object p4, p0, LX/0iJ;->j:LX/0gc;

    .line 120747
    iput-object p6, p0, LX/0iJ;->i:LX/0zw;

    .line 120748
    iput-object p8, p0, LX/0iJ;->k:LX/Avn;

    .line 120749
    iput-object p9, p0, LX/0iJ;->l:LX/0iA;

    .line 120750
    iput-object p10, p0, LX/0iJ;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 120751
    iput-object p2, p0, LX/0iJ;->g:LX/ArL;

    .line 120752
    iput-object p3, p0, LX/0iJ;->h:LX/ArT;

    .line 120753
    invoke-virtual {p7, p5}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, LX/0iJ;->n:LX/0i6;

    .line 120754
    return-void
.end method

.method public static a(LX/0iJ;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0iJ;",
            "LX/0Or",
            "<",
            "Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 120739
    iput-object p1, p0, LX/0iJ;->a:LX/0Or;

    return-void
.end method

.method public static a(LX/0iJ;LX/ArJ;)V
    .locals 13

    .prologue
    .line 120729
    if-eqz p1, :cond_0

    .line 120730
    invoke-static {p0}, LX/0iJ;->o(LX/0iJ;)V

    .line 120731
    iget-object v1, p0, LX/0iJ;->h:LX/ArT;

    iget v0, p0, LX/0iJ;->v:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    const/4 v0, -0x1

    .line 120732
    :goto_0
    iget-object v4, v1, LX/ArT;->b:LX/ArL;

    iget-object v3, v1, LX/ArT;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v5

    iget-object v3, v1, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0il;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getNuxSessionStartTime()J

    move-result-wide v7

    sub-long/2addr v5, v7

    long-to-float v3, v5

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v3, v5

    .line 120733
    sget-object v9, LX/ArH;->END_NUX_SESSION:LX/ArH;

    invoke-static {v4, v9, p1}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    invoke-static {v4}, LX/ArL;->h(LX/ArL;)Ljava/util/Map;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    sget-object v10, LX/ArI;->DURATION:LX/ArI;

    invoke-virtual {v10}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v10

    float-to-double v11, v3

    invoke-virtual {v9, v10, v11, v12}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    sget-object v10, LX/ArI;->INDEX:LX/ArI;

    invoke-virtual {v10}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    invoke-static {v4, v9}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 120734
    iget-object v3, v1, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0il;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setNuxSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v3

    invoke-static {v1, v3}, LX/ArT;->a(LX/ArT;Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V

    .line 120735
    :cond_0
    return-void

    .line 120736
    :cond_1
    iget-object v0, p0, LX/0iJ;->o:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    .line 120737
    iget v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    move v0, v2

    .line 120738
    goto :goto_0
.end method

.method public static a(LX/0iJ;Lcom/facebook/base/fragment/FbFragment;)V
    .locals 2

    .prologue
    .line 120722
    move-object v0, p1

    check-cast v0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;

    iget-object v1, p0, LX/0iJ;->w:Landroid/view/View$OnClickListener;

    .line 120723
    iput-object v1, v0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->c:Landroid/view/View$OnClickListener;

    .line 120724
    move-object v0, p1

    .line 120725
    check-cast v0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;

    iget-object v1, p0, LX/0iJ;->x:Landroid/view/View$OnClickListener;

    .line 120726
    iput-object v1, v0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->e:Landroid/view/View$OnClickListener;

    .line 120727
    iget-object v0, p0, LX/0iJ;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120728
    return-void
.end method

.method public static a(LX/0iJ;Z)V
    .locals 3

    .prologue
    .line 120718
    iput-boolean p1, p0, LX/0iJ;->r:Z

    .line 120719
    iget-object v0, p0, LX/0iJ;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    move-object v1, v0

    .line 120720
    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    sget-object v2, LX/0iJ;->d:LX/0jK;

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setIsInNuxMode(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 120721
    return-void
.end method

.method public static a$redex0(LX/0iJ;I)V
    .locals 3

    .prologue
    .line 120700
    iget v0, p0, LX/0iJ;->v:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_3

    .line 120701
    iget-object v0, p0, LX/0iJ;->p:Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->setVisibility(I)V

    .line 120702
    iget-object v0, p0, LX/0iJ;->p:Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    iget v1, p0, LX/0iJ;->v:I

    invoke-virtual {v0, v1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setCount(I)V

    .line 120703
    iget-object v0, p0, LX/0iJ;->p:Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setCurrentItem(I)V

    .line 120704
    iget-object v0, p0, LX/0iJ;->o:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setCurrentItem(I)V

    .line 120705
    :goto_0
    if-eqz p1, :cond_0

    iget v0, p0, LX/0iJ;->v:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_4

    .line 120706
    :cond_0
    iget-object v0, p0, LX/0iJ;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    move-object v1, v0

    .line 120707
    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    sget-object v2, LX/0iJ;->d:LX/0jK;

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    .line 120708
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    sget-object v2, LX/86t;->NUX:LX/86t;

    invoke-static {v1, v0, v2}, LX/87R;->a(LX/0jL;Lcom/facebook/composer/system/model/ComposerModelImpl;LX/86t;)LX/0jL;

    move-result-object v0

    .line 120709
    invoke-virtual {v0}, LX/0jL;->a()V

    .line 120710
    :goto_1
    iget-object v0, p0, LX/0iJ;->t:LX/ArA;

    .line 120711
    iget-object v1, v0, LX/ArA;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->W:LX/AtM;

    if-eqz v1, :cond_1

    .line 120712
    iget-object v1, v0, LX/ArA;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->W:LX/AtM;

    invoke-virtual {v1}, LX/AtM;->b()V

    .line 120713
    :cond_1
    iget v0, p0, LX/0iJ;->v:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_2

    .line 120714
    iget-object v0, p0, LX/0iJ;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/AwE;->d:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 120715
    :cond_2
    return-void

    .line 120716
    :cond_3
    iget-object v0, p0, LX/0iJ;->p:Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->setVisibility(I)V

    goto :goto_0

    .line 120717
    :cond_4
    iget-object v0, p0, LX/0iJ;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    sget-object v1, LX/86t;->NUX:LX/86t;

    sget-object v2, LX/0iJ;->d:LX/0jK;

    invoke-static {v0, v1, v2}, LX/87R;->a(LX/0il;LX/86t;LX/0jK;)V

    goto :goto_1
.end method

.method public static m(LX/0iJ;)V
    .locals 2

    .prologue
    .line 120695
    iget-object v0, p0, LX/0iJ;->u:Ljava/util/List;

    iget-object v1, p0, LX/0iJ;->o:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    .line 120696
    iget p0, v1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    move v1, p0

    .line 120697
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    .line 120698
    check-cast v0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->c()V

    .line 120699
    return-void
.end method

.method public static n(LX/0iJ;)V
    .locals 2

    .prologue
    .line 120690
    iget-object v0, p0, LX/0iJ;->u:Ljava/util/List;

    iget-object v1, p0, LX/0iJ;->o:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    .line 120691
    iget p0, v1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    move v1, p0

    .line 120692
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    .line 120693
    check-cast v0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->d()V

    .line 120694
    return-void
.end method

.method public static o(LX/0iJ;)V
    .locals 4

    .prologue
    .line 120684
    iget-object v0, p0, LX/0iJ;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    check-cast v0, LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    const-class v1, LX/0iJ;

    invoke-static {v1}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    iget-object v1, p0, LX/0iJ;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v1

    .line 120685
    iget-object v2, p0, LX/0iJ;->n:LX/0i6;

    sget-object v3, LX/0iJ;->c:[Ljava/lang/String;

    invoke-interface {v2, v3}, LX/0i6;->a([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "granted"

    :goto_0
    move-object v2, v2

    .line 120686
    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->setCameraRollPermissionState(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v1

    .line 120687
    iget-object v2, p0, LX/0iJ;->n:LX/0i6;

    sget-object v3, LX/0iJ;->b:[Ljava/lang/String;

    invoke-interface {v2, v3}, LX/0i6;->a([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "granted"

    :goto_1
    move-object v2, v2

    .line 120688
    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->setCapturePermissionState(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 120689
    return-void

    :cond_0
    const-string v2, "denied"

    goto :goto_0

    :cond_1
    const-string v2, "denied"

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 120683
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 120669
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 120670
    iget-object v0, p0, LX/0iJ;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 120671
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    .line 120672
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v2

    .line 120673
    sget-object v3, LX/86t;->NUX:LX/86t;

    invoke-static {v2, v1, v3}, LX/87R;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/86t;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 120674
    const/4 v1, 0x0

    sget-object v2, LX/0iJ;->d:LX/0jK;

    invoke-static {v0, v1, v2}, LX/87R;->a(LX/0il;ZLX/0jK;)V

    .line 120675
    iget-boolean v0, p0, LX/0iJ;->r:Z

    if-eqz v0, :cond_1

    .line 120676
    invoke-static {p0}, LX/0iJ;->n(LX/0iJ;)V

    .line 120677
    iget-object v0, p0, LX/0iJ;->o:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    .line 120678
    iget v1, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    move v0, v1

    .line 120679
    if-eqz v0, :cond_1

    iget v1, p0, LX/0iJ;->v:I

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_1

    .line 120680
    iget-object v1, p0, LX/0iJ;->o:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setCurrentItem(I)V

    .line 120681
    add-int/lit8 v0, v0, -0x1

    invoke-static {p0, v0}, LX/0iJ;->a$redex0(LX/0iJ;I)V

    .line 120682
    :cond_0
    :goto_0
    return-void

    :cond_1
    goto :goto_0
.end method
