.class public LX/0zN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/0zN;


# instance fields
.field public final a:LX/0W3;

.field public b:Z

.field public c:Z

.field private volatile d:I

.field public volatile e:Z

.field public volatile f:I

.field public volatile g:Z

.field public volatile h:I


# direct methods
.method public constructor <init>(LX/0W3;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 167051
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167052
    iput-boolean v0, p0, LX/0zN;->b:Z

    .line 167053
    iput-boolean v0, p0, LX/0zN;->c:Z

    .line 167054
    iput v1, p0, LX/0zN;->d:I

    .line 167055
    iput-boolean v0, p0, LX/0zN;->e:Z

    .line 167056
    iput v1, p0, LX/0zN;->f:I

    .line 167057
    iput-boolean v0, p0, LX/0zN;->g:Z

    .line 167058
    iput v1, p0, LX/0zN;->h:I

    .line 167059
    iput-object p1, p0, LX/0zN;->a:LX/0W3;

    .line 167060
    return-void
.end method

.method public static a(LX/0QB;)LX/0zN;
    .locals 4

    .prologue
    .line 167061
    sget-object v0, LX/0zN;->i:LX/0zN;

    if-nez v0, :cond_1

    .line 167062
    const-class v1, LX/0zN;

    monitor-enter v1

    .line 167063
    :try_start_0
    sget-object v0, LX/0zN;->i:LX/0zN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 167064
    if-eqz v2, :cond_0

    .line 167065
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 167066
    new-instance p0, LX/0zN;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-direct {p0, v3}, LX/0zN;-><init>(LX/0W3;)V

    .line 167067
    move-object v0, p0

    .line 167068
    sput-object v0, LX/0zN;->i:LX/0zN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 167069
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 167070
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 167071
    :cond_1
    sget-object v0, LX/0zN;->i:LX/0zN;

    return-object v0

    .line 167072
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 167073
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 4

    .prologue
    .line 167074
    iget v0, p0, LX/0zN;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 167075
    iget-object v0, p0, LX/0zN;->a:LX/0W3;

    sget-wide v2, LX/0X5;->cN:J

    const/16 v1, 0x8

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/0zN;->d:I

    .line 167076
    :cond_0
    iget v0, p0, LX/0zN;->d:I

    return v0
.end method
