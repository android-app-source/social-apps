.class public LX/0PR;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/0PS;

.field private static final b:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "LX/0PU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56519
    new-instance v0, LX/0PS;

    invoke-direct {v0}, LX/0PS;-><init>()V

    sput-object v0, LX/0PR;->a:LX/0PS;

    .line 56520
    new-instance v0, LX/0PT;

    invoke-direct {v0}, LX/0PT;-><init>()V

    sput-object v0, LX/0PR;->b:Ljava/lang/ThreadLocal;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 56521
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56522
    return-void
.end method

.method private static a(JZ)J
    .locals 6

    .prologue
    const-wide/16 v2, -0x1

    .line 56564
    const-wide/16 v0, 0x20

    invoke-static {v0, v1}, LX/018;->a(J)V

    .line 56565
    sget-object v0, LX/0PR;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0PU;

    .line 56566
    iget-boolean v1, v0, LX/0PU;->b:Z

    if-eqz v1, :cond_1

    move-wide v0, v2

    .line 56567
    :cond_0
    :goto_0
    return-wide v0

    .line 56568
    :cond_1
    iget-object v1, v0, LX/0PU;->a:LX/0PV;

    invoke-virtual {v1}, LX/0PV;->a()I

    move-result v1

    const/4 v4, 0x2

    if-ge v1, v4, :cond_2

    .line 56569
    const-class v1, LX/0PR;

    const-string v4, "Tracer stack underflow. There\'s an extra stopTracer somewhere."

    invoke-static {v1, v4}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    .line 56570
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0PU;->b:Z

    move-wide v0, v2

    .line 56571
    goto :goto_0

    .line 56572
    :cond_2
    iget-object v1, v0, LX/0PU;->a:LX/0PV;

    invoke-virtual {v1}, LX/0PV;->b()J

    move-result-wide v4

    .line 56573
    iget-object v0, v0, LX/0PU;->a:LX/0PV;

    invoke-virtual {v0}, LX/0PV;->b()J

    move-result-wide v0

    long-to-int v1, v0

    .line 56574
    sget v0, LX/0PW;->b:I

    if-eq v1, v0, :cond_3

    .line 56575
    sget-object v0, LX/0PW;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0PW;

    .line 56576
    invoke-virtual {v0, v1, p0, p1, p2}, LX/0PW;->a(IJZ)J

    move-result-wide v0

    .line 56577
    :goto_1
    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 56578
    invoke-static {}, LX/0PZ;->a()J

    move-result-wide v0

    sub-long/2addr v0, v4

    goto :goto_0

    :cond_3
    move-wide v0, v2

    goto :goto_1
.end method

.method public static a()V
    .locals 3

    .prologue
    .line 56523
    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/0PR;->a(JZ)J

    .line 56524
    return-void
.end method

.method private static a(ILjava/lang/String;)V
    .locals 14

    .prologue
    .line 56525
    sget-object v0, LX/0PW;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0PW;

    .line 56526
    iget v1, v0, LX/0PW;->g:I

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_2

    .line 56527
    iget-object v2, v0, LX/0PW;->f:[LX/0eu;

    aget-object v2, v2, v1

    .line 56528
    if-eqz v2, :cond_1

    .line 56529
    iget v1, v2, LX/0eu;->b:I

    move v1, v1

    .line 56530
    :goto_1
    move v1, v1

    .line 56531
    iget-object v2, v0, LX/0PW;->f:[LX/0eu;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    iget v4, v0, LX/0PW;->g:I

    invoke-interface {v2, v3, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    iget-object v3, v0, LX/0PW;->e:Landroid/util/SparseArray;

    const/16 v0, 0xfa0

    const/4 v9, 0x0

    const/4 v6, 0x0

    .line 56532
    invoke-static {p0}, LX/01m;->b(I)Z

    move-result v4

    if-nez v4, :cond_3

    .line 56533
    :cond_0
    :goto_2
    return-void

    .line 56534
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 56535
    :cond_2
    sget v1, LX/0PW;->b:I

    goto :goto_1

    .line 56536
    :cond_3
    invoke-static {v1, v2, v3}, LX/270;->a(ILjava/util/List;Landroid/util/SparseArray;)Ljava/lang/String;

    move-result-object v4

    .line 56537
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 56538
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v0, :cond_8

    .line 56539
    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 56540
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 56541
    invoke-static {v11, v6, v9, v9}, LX/270;->a(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/String;)V

    .line 56542
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    move v5, v6

    move v7, v6

    .line 56543
    :goto_3
    array-length v8, v10

    if-ge v7, v8, :cond_7

    .line 56544
    if-eqz v7, :cond_5

    add-int/lit8 v8, v7, -0x1

    aget-object v8, v10, v8

    .line 56545
    :goto_4
    aget-object v12, v10, v7

    .line 56546
    if-eqz v4, :cond_4

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v13, v4

    if-ge v13, v0, :cond_6

    .line 56547
    :cond_4
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56548
    const-string v8, "\n"

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56549
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    add-int/2addr v4, v8

    .line 56550
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    :cond_5
    move-object v8, v9

    .line 56551
    goto :goto_4

    .line 56552
    :cond_6
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, p1, v4}, LX/01m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 56553
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 56554
    add-int/lit8 v5, v5, 0x1

    .line 56555
    invoke-static {v11, v5, v8, v12}, LX/270;->a(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/String;)V

    .line 56556
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    goto :goto_3

    .line 56557
    :cond_7
    if-lez v4, :cond_0

    .line 56558
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, p1, v4}, LX/01m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 56559
    :cond_8
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 56560
    const/4 v7, 0x0

    .line 56561
    const/4 v6, 0x0

    invoke-static {v5, v6, v7, v7}, LX/270;->a(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/String;)V

    .line 56562
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56563
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, p1, v4}, LX/01m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public static a(J)V
    .locals 2

    .prologue
    .line 56515
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, LX/0PR;->a(JZ)J

    .line 56516
    return-void
.end method

.method public static a(Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 56517
    const/4 v0, 0x3

    invoke-static {p0}, LX/03P;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PR;->a(ILjava/lang/String;)V

    .line 56518
    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 1
    .annotation build Lcom/facebook/infer/annotation/IgnoreAllocations;
    .end annotation

    .prologue
    .line 56513
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/0PR;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56514
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/infer/annotation/IgnoreAllocations;
    .end annotation

    .prologue
    .line 56511
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {p0, v0}, LX/0PR;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56512
    return-void
.end method

.method public static b()J
    .locals 4

    .prologue
    .line 56510
    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/0PR;->a(JZ)J

    move-result-wide v0

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 56508
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/0PR;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56509
    return-void
.end method

.method public static c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 56506
    const/4 v0, 0x3

    invoke-static {v0, p0}, LX/0PR;->a(ILjava/lang/String;)V

    .line 56507
    return-void
.end method

.method public static c(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 12
    .param p1    # [Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v6, 0x20

    .line 56478
    sget-object v0, LX/0PR;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0PU;

    .line 56479
    iget-boolean v1, v0, LX/0PU;->b:Z

    if-eqz v1, :cond_1

    .line 56480
    :cond_0
    :goto_0
    return-void

    .line 56481
    :cond_1
    iget-object v1, v0, LX/0PU;->a:LX/0PV;

    invoke-virtual {v1}, LX/0PV;->a()I

    move-result v1

    const/16 v2, 0x64

    if-lt v1, v2, :cond_2

    .line 56482
    const-class v1, LX/0PR;

    const-string v2, "Tracer stack overflow. There is probably a missing stopTracer somewhere."

    invoke-static {v1, v2}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    .line 56483
    iget-object v1, v0, LX/0PU;->a:LX/0PV;

    .line 56484
    const/4 v2, -0x1

    iput v2, v1, LX/0PV;->b:I

    .line 56485
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0PU;->b:Z

    goto :goto_0

    .line 56486
    :cond_2
    sget-object v1, LX/0PR;->a:LX/0PS;

    invoke-virtual {v1}, LX/0PS;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 56487
    sget-object v1, LX/0PW;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0PW;

    .line 56488
    const/4 v2, 0x0

    invoke-virtual {v1, p0, p1, v2}, LX/0PW;->a(Ljava/lang/String;[Ljava/lang/Object;Z)I

    move-result v2

    int-to-long v2, v2

    .line 56489
    iget v8, v1, LX/0PW;->g:I

    add-int/lit8 v8, v8, -0x1

    :goto_1
    if-ltz v8, :cond_6

    .line 56490
    iget-object v9, v1, LX/0PW;->f:[LX/0eu;

    aget-object v9, v9, v8

    .line 56491
    if-eqz v9, :cond_5

    .line 56492
    iget-wide v10, v9, LX/0eu;->e:J

    move-wide v8, v10

    .line 56493
    :goto_2
    move-wide v4, v8

    .line 56494
    iget-object v1, v0, LX/0PU;->a:LX/0PV;

    invoke-virtual {v1, v2, v3}, LX/0PV;->a(J)V

    .line 56495
    iget-object v0, v0, LX/0PU;->a:LX/0PV;

    invoke-virtual {v0, v4, v5}, LX/0PV;->a(J)V

    .line 56496
    :goto_3
    invoke-static {v6, v7}, LX/00k;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56497
    if-nez p1, :cond_4

    .line 56498
    :goto_4
    invoke-static {v6, v7, p0}, LX/018;->a(JLjava/lang/String;)V

    goto :goto_0

    .line 56499
    :cond_3
    iget-object v1, v0, LX/0PU;->a:LX/0PV;

    sget v2, LX/0PW;->b:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, LX/0PV;->a(J)V

    .line 56500
    iget-object v0, v0, LX/0PU;->a:LX/0PV;

    invoke-static {}, LX/0PZ;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/0PV;->a(J)V

    goto :goto_3

    .line 56501
    :cond_4
    :try_start_0
    invoke-static {p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/IllegalFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    goto :goto_4

    .line 56502
    :catch_0
    move-exception v0

    .line 56503
    const-string v1, "Tracer"

    const-string v2, "Bad format string"

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 56504
    :cond_5
    add-int/lit8 v8, v8, -0x1

    goto :goto_1

    .line 56505
    :cond_6
    const-wide/16 v8, -0x1

    goto :goto_2
.end method

.method public static d(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 56472
    sget-object v0, LX/0PR;->a:LX/0PS;

    invoke-virtual {v0}, LX/0PS;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56473
    sget-object v0, LX/0PW;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0PW;

    .line 56474
    invoke-static {}, LX/0PW;->h()I

    move-result v1

    const-wide/16 v6, -0x1

    .line 56475
    sget-object v2, LX/49P;->COMMENT:LX/49P;

    move v3, v1

    move-object v4, p0

    move-object v5, p1

    move-wide v8, v6

    invoke-static/range {v2 .. v9}, LX/0eu;->a(LX/49P;ILjava/lang/String;[Ljava/lang/Object;JJ)LX/0eu;

    move-result-object v2

    move-object v1, v2

    .line 56476
    invoke-static {v0, v1}, LX/0PW;->a(LX/0PW;LX/0eu;)V

    .line 56477
    :cond_0
    return-void
.end method
