.class public LX/1QK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pl;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:Landroid/app/Activity;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/01T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 244484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244485
    return-void
.end method

.method public static a(LX/0QB;)LX/1QK;
    .locals 5

    .prologue
    .line 244489
    const-class v1, LX/1QK;

    monitor-enter v1

    .line 244490
    :try_start_0
    sget-object v0, LX/1QK;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 244491
    sput-object v2, LX/1QK;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 244492
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244493
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 244494
    new-instance p0, LX/1QK;

    invoke-direct {p0}, LX/1QK;-><init>()V

    .line 244495
    invoke-static {v0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v4

    check-cast v4, LX/01T;

    .line 244496
    iput-object v3, p0, LX/1QK;->a:Landroid/app/Activity;

    iput-object v4, p0, LX/1QK;->b:LX/01T;

    .line 244497
    move-object v0, p0

    .line 244498
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 244499
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1QK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 244500
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 244501
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final r()Z
    .locals 3

    .prologue
    .line 244486
    const/4 v0, 0x0

    .line 244487
    iget-object v1, p0, LX/1QK;->b:LX/01T;

    sget-object v2, LX/01T;->PAA:LX/01T;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/1QK;->a:Landroid/app/Activity;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1QK;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "comms_hub_story_permalink"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v0, v0

    .line 244488
    return v0
.end method
