.class public LX/0c6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0c6;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 87787
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87788
    return-void
.end method

.method public static a(LX/0QB;)LX/0c6;
    .locals 3

    .prologue
    .line 87789
    sget-object v0, LX/0c6;->a:LX/0c6;

    if-nez v0, :cond_1

    .line 87790
    const-class v1, LX/0c6;

    monitor-enter v1

    .line 87791
    :try_start_0
    sget-object v0, LX/0c6;->a:LX/0c6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 87792
    if-eqz v2, :cond_0

    .line 87793
    :try_start_1
    new-instance v0, LX/0c6;

    invoke-direct {v0}, LX/0c6;-><init>()V

    .line 87794
    move-object v0, v0

    .line 87795
    sput-object v0, LX/0c6;->a:LX/0c6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87796
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 87797
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 87798
    :cond_1
    sget-object v0, LX/0c6;->a:LX/0c6;

    return-object v0

    .line 87799
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 87800
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;I)LX/0cD;
    .locals 1

    .prologue
    .line 87801
    sget-object v0, LX/0cB;->a:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87802
    new-instance v0, LX/1gT;

    invoke-direct {v0, p2}, LX/1gT;-><init>(I)V

    .line 87803
    :goto_0
    return-object v0

    .line 87804
    :cond_0
    sget-object v0, LX/0cB;->e:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87805
    new-instance v0, LX/0cC;

    invoke-direct {v0, p2}, LX/0cC;-><init>(I)V

    goto :goto_0

    .line 87806
    :cond_1
    sget-object v0, LX/0cB;->g:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 87807
    new-instance v0, LX/0cF;

    invoke-direct {v0, p2}, LX/0cF;-><init>(I)V

    goto :goto_0

    .line 87808
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
