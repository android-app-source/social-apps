.class public final LX/1Xc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "LX/1Xc;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private _class:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private _className:Ljava/lang/String;

.field private _hashCode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 271418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271419
    iput-object v0, p0, LX/1Xc;->_class:Ljava/lang/Class;

    .line 271420
    iput-object v0, p0, LX/1Xc;->_className:Ljava/lang/String;

    .line 271421
    const/4 v0, 0x0

    iput v0, p0, LX/1Xc;->_hashCode:I

    .line 271422
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 271413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271414
    iput-object p1, p0, LX/1Xc;->_class:Ljava/lang/Class;

    .line 271415
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1Xc;->_className:Ljava/lang/String;

    .line 271416
    iget-object v0, p0, LX/1Xc;->_className:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iput v0, p0, LX/1Xc;->_hashCode:I

    .line 271417
    return-void
.end method

.method private a(LX/1Xc;)I
    .locals 2

    .prologue
    .line 271412
    iget-object v0, p0, LX/1Xc;->_className:Ljava/lang/String;

    iget-object v1, p1, LX/1Xc;->_className:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 271411
    check-cast p1, LX/1Xc;

    invoke-direct {p0, p1}, LX/1Xc;->a(LX/1Xc;)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 271405
    if-ne p1, p0, :cond_1

    .line 271406
    :cond_0
    :goto_0
    return v0

    .line 271407
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 271408
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    .line 271409
    :cond_3
    check-cast p1, LX/1Xc;

    .line 271410
    iget-object v2, p1, LX/1Xc;->_class:Ljava/lang/Class;

    iget-object v3, p0, LX/1Xc;->_class:Ljava/lang/Class;

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 271403
    iget v0, p0, LX/1Xc;->_hashCode:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271404
    iget-object v0, p0, LX/1Xc;->_className:Ljava/lang/String;

    return-object v0
.end method
