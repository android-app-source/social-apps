.class public LX/0dr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile e:LX/0dr;


# instance fields
.field private final b:LX/03V;

.field private final c:LX/0ad;

.field private d:Ljava/util/HashSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91093
    const-class v0, LX/0dr;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0dr;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 91094
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91095
    iput-object p1, p0, LX/0dr;->b:LX/03V;

    .line 91096
    iput-object p2, p0, LX/0dr;->c:LX/0ad;

    .line 91097
    return-void
.end method

.method public static a(LX/0QB;)LX/0dr;
    .locals 5

    .prologue
    .line 91098
    sget-object v0, LX/0dr;->e:LX/0dr;

    if-nez v0, :cond_1

    .line 91099
    const-class v1, LX/0dr;

    monitor-enter v1

    .line 91100
    :try_start_0
    sget-object v0, LX/0dr;->e:LX/0dr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 91101
    if-eqz v2, :cond_0

    .line 91102
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 91103
    new-instance p0, LX/0dr;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/0dr;-><init>(LX/03V;LX/0ad;)V

    .line 91104
    move-object v0, p0

    .line 91105
    sput-object v0, LX/0dr;->e:LX/0dr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91106
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 91107
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 91108
    :cond_1
    sget-object v0, LX/0dr;->e:LX/0dr;

    return-object v0

    .line 91109
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 91110
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 91111
    iget-object v1, p0, LX/0dr;->c:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget v3, LX/0ds;->b:I

    invoke-interface {v1, v2, v3, v0}, LX/0ad;->a(LX/0c0;II)I

    move-result v1

    .line 91112
    if-nez v1, :cond_1

    .line 91113
    :cond_0
    :goto_0
    return-void

    .line 91114
    :cond_1
    iget-object v2, p0, LX/0dr;->c:LX/0ad;

    sget-object v3, LX/0c0;->Live:LX/0c0;

    sget-char v4, LX/0ds;->c:C

    const-string v5, "none"

    invoke-interface {v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 91115
    :try_start_0
    const-string v3, "java"

    invoke-static {v2, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 91116
    div-int/lit16 v3, v1, 0x2710

    .line 91117
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, p0, LX/0dr;->d:Ljava/util/HashSet;

    .line 91118
    :goto_1
    const/16 v4, 0x2710

    if-ge v0, v4, :cond_0

    .line 91119
    iget-object v4, p0, LX/0dr;->d:Ljava/util/HashSet;

    new-array v5, v3, [B

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 91120
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 91121
    :cond_2
    const-string v0, "cpp"

    invoke-static {v2, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91122
    invoke-static {v1}, Lcom/facebook/fbreact/instance/FbReactMemory;->allocMemory(I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 91123
    :catch_0
    move-exception v0

    .line 91124
    iget-object v3, p0, LX/0dr;->b:LX/03V;

    sget-object v4, LX/0dr;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Failed to allocate "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " in "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
