.class public final LX/0mY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:I

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 132628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132629
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0mY;->b:Ljava/util/ArrayList;

    .line 132630
    iget-object v0, p0, LX/0mY;->b:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/0mY;->c:Ljava/util/List;

    .line 132631
    const/4 v0, -0x1

    iput v0, p0, LX/0mY;->a:I

    .line 132632
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 132652
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0mY;->c:Ljava/util/List;

    .line 132653
    iget v1, p0, LX/0mY;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0mY;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132654
    monitor-exit p0

    return-object v0

    .line 132655
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 132641
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0mY;->d:I

    if-lez v0, :cond_0

    .line 132642
    iget-object v1, p0, LX/0mY;->b:Ljava/util/ArrayList;

    .line 132643
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 132644
    new-instance v0, Ljava/util/ArrayList;

    add-int/lit8 v3, v2, 0x1

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/0mY;->b:Ljava/util/ArrayList;

    .line 132645
    iget-object v0, p0, LX/0mY;->b:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/0mY;->c:Ljava/util/List;

    .line 132646
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 132647
    iget-object v3, p0, LX/0mY;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 132648
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 132649
    :cond_0
    iget-object v0, p0, LX/0mY;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132650
    monitor-exit p0

    return-void

    .line 132651
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 132638
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0mY;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0mY;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132639
    monitor-exit p0

    return-void

    .line 132640
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 132633
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0mY;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 132634
    invoke-virtual {p0, p1}, LX/0mY;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132635
    const/4 v0, 0x1

    .line 132636
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 132637
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
