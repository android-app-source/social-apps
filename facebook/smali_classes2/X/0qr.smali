.class public final LX/0qr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0qs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0qs",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0qs",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 148581
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148582
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/0qr;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(IILjava/lang/Object;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IITT;Z)V"
        }
    .end annotation

    .prologue
    .line 148600
    iget-object v0, p0, LX/0qr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qs;

    .line 148601
    invoke-interface {v0, p1, p2, p3, p4}, LX/0qs;->a(IILjava/lang/Object;Z)V

    goto :goto_0

    .line 148602
    :cond_0
    return-void
.end method

.method public final a(ILjava/lang/Object;Ljava/lang/Object;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;TT;Z)V"
        }
    .end annotation

    .prologue
    .line 148597
    iget-object v0, p0, LX/0qr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qs;

    .line 148598
    invoke-interface {v0, p1, p2, p3, p4}, LX/0qs;->a(ILjava/lang/Object;Ljava/lang/Object;Z)V

    goto :goto_0

    .line 148599
    :cond_0
    return-void
.end method

.method public final a(ILjava/lang/Object;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;Z)V"
        }
    .end annotation

    .prologue
    .line 148594
    iget-object v0, p0, LX/0qr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qs;

    .line 148595
    invoke-interface {v0, p1, p2, p3}, LX/0qs;->a(ILjava/lang/Object;Z)V

    goto :goto_0

    .line 148596
    :cond_0
    return-void
.end method

.method public final a(LX/0qs;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qs",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 148590
    if-nez p1, :cond_0

    .line 148591
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 148592
    :cond_0
    iget-object v0, p0, LX/0qr;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148593
    return-void
.end method

.method public final b(ILjava/lang/Object;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;Z)V"
        }
    .end annotation

    .prologue
    .line 148587
    iget-object v0, p0, LX/0qr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qs;

    .line 148588
    invoke-interface {v0, p1, p2, p3}, LX/0qs;->b(ILjava/lang/Object;Z)V

    goto :goto_0

    .line 148589
    :cond_0
    return-void
.end method

.method public final b(LX/0qs;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qs",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 148583
    if-nez p1, :cond_0

    .line 148584
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 148585
    :cond_0
    iget-object v0, p0, LX/0qr;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 148586
    return-void
.end method
