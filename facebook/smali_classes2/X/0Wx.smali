.class public interface abstract LX/0Wx;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract a()LX/0eT;
.end method

.method public abstract b()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;
.end method

.method public abstract c()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;
.end method

.method public abstract clearCurrentUserData()V
.end method

.method public abstract clearOverrides()V
.end method

.method public abstract deleteOldUserData(I)V
.end method

.method public abstract getFrameworkStatus()Ljava/lang/String;
.end method

.method public abstract getQEInfoFilename()Ljava/lang/String;
.end method

.method public abstract isQEInfoAvailable()Z
.end method

.method public abstract isTigonServiceSet()Z
.end method

.method public abstract isValid()Z
.end method

.method public abstract logExposure(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract refreshConfigInfos(I)Z
.end method

.method public abstract registerConfigChangeListener(Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;)Z
.end method

.method public abstract setTigonService(Lcom/facebook/tigon/iface/TigonServiceHolder;Z)V
.end method

.method public abstract tryUpdateConfigsSynchronously(I)Z
.end method

.method public abstract updateConfigs()Z
.end method

.method public abstract updateConfigsSynchronously(I)Z
.end method
