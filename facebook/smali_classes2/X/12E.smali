.class public final LX/12E;
.super LX/12F;
.source ""


# static fields
.field public static final m:[C


# instance fields
.field public final n:Ljava/io/Writer;

.field public o:[C

.field public p:I

.field public q:I

.field public r:I

.field public s:[C

.field public t:LX/0lc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 174443
    invoke-static {}, LX/12H;->g()[C

    move-result-object v0

    sput-object v0, LX/12E;->m:[C

    return-void
.end method

.method public constructor <init>(LX/12A;ILX/0lD;Ljava/io/Writer;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 174444
    invoke-direct {p0, p1, p2, p3}, LX/12F;-><init>(LX/12A;ILX/0lD;)V

    .line 174445
    iput v0, p0, LX/12E;->p:I

    .line 174446
    iput v0, p0, LX/12E;->q:I

    .line 174447
    iput-object p4, p0, LX/12E;->n:Ljava/io/Writer;

    .line 174448
    invoke-virtual {p1}, LX/12A;->h()[C

    move-result-object v0

    iput-object v0, p0, LX/12E;->o:[C

    .line 174449
    iget-object v0, p0, LX/12E;->o:[C

    array-length v0, v0

    iput v0, p0, LX/12E;->r:I

    .line 174450
    return-void
.end method

.method private a([CIICI)I
    .locals 7

    .prologue
    const/16 v5, 0x30

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x6

    .line 174451
    if-ltz p5, :cond_2

    .line 174452
    if-le p2, v3, :cond_0

    if-ge p2, p3, :cond_0

    .line 174453
    add-int/lit8 p2, p2, -0x2

    .line 174454
    const/16 v0, 0x5c

    aput-char v0, p1, p2

    .line 174455
    add-int/lit8 v0, p2, 0x1

    int-to-char v1, p5

    aput-char v1, p1, v0

    .line 174456
    :goto_0
    return p2

    .line 174457
    :cond_0
    iget-object v0, p0, LX/12E;->s:[C

    .line 174458
    if-nez v0, :cond_1

    .line 174459
    invoke-direct {p0}, LX/12E;->m()[C

    move-result-object v0

    .line 174460
    :cond_1
    int-to-char v1, p5

    aput-char v1, v0, v3

    .line 174461
    iget-object v1, p0, LX/12E;->n:Ljava/io/Writer;

    invoke-virtual {v1, v0, v2, v4}, Ljava/io/Writer;->write([CII)V

    goto :goto_0

    .line 174462
    :cond_2
    const/4 v0, -0x2

    if-eq p5, v0, :cond_7

    .line 174463
    const/4 v0, 0x5

    if-le p2, v0, :cond_4

    if-ge p2, p3, :cond_4

    .line 174464
    add-int/lit8 v0, p2, -0x6

    .line 174465
    add-int/lit8 v1, v0, 0x1

    const/16 v2, 0x5c

    aput-char v2, p1, v0

    .line 174466
    add-int/lit8 v0, v1, 0x1

    const/16 v2, 0x75

    aput-char v2, p1, v1

    .line 174467
    const/16 v1, 0xff

    if-le p4, v1, :cond_3

    .line 174468
    shr-int/lit8 v1, p4, 0x8

    and-int/lit16 v1, v1, 0xff

    .line 174469
    add-int/lit8 v2, v0, 0x1

    sget-object v3, LX/12E;->m:[C

    shr-int/lit8 v4, v1, 0x4

    aget-char v3, v3, v4

    aput-char v3, p1, v0

    .line 174470
    add-int/lit8 v0, v2, 0x1

    sget-object v3, LX/12E;->m:[C

    and-int/lit8 v1, v1, 0xf

    aget-char v1, v3, v1

    aput-char v1, p1, v2

    .line 174471
    and-int/lit16 v1, p4, 0xff

    int-to-char p4, v1

    .line 174472
    :goto_1
    add-int/lit8 v1, v0, 0x1

    sget-object v2, LX/12E;->m:[C

    shr-int/lit8 v3, p4, 0x4

    aget-char v2, v2, v3

    aput-char v2, p1, v0

    .line 174473
    sget-object v0, LX/12E;->m:[C

    and-int/lit8 v2, p4, 0xf

    aget-char v0, v0, v2

    aput-char v0, p1, v1

    .line 174474
    add-int/lit8 p2, v1, -0x5

    goto :goto_0

    .line 174475
    :cond_3
    add-int/lit8 v1, v0, 0x1

    aput-char v5, p1, v0

    .line 174476
    add-int/lit8 v0, v1, 0x1

    aput-char v5, p1, v1

    goto :goto_1

    .line 174477
    :cond_4
    iget-object v0, p0, LX/12E;->s:[C

    .line 174478
    if-nez v0, :cond_5

    .line 174479
    invoke-direct {p0}, LX/12E;->m()[C

    move-result-object v0

    .line 174480
    :cond_5
    iget v1, p0, LX/12E;->q:I

    iput v1, p0, LX/12E;->p:I

    .line 174481
    const/16 v1, 0xff

    if-le p4, v1, :cond_6

    .line 174482
    shr-int/lit8 v1, p4, 0x8

    and-int/lit16 v1, v1, 0xff

    .line 174483
    and-int/lit16 v2, p4, 0xff

    .line 174484
    const/16 v3, 0xa

    sget-object v4, LX/12E;->m:[C

    shr-int/lit8 v5, v1, 0x4

    aget-char v4, v4, v5

    aput-char v4, v0, v3

    .line 174485
    const/16 v3, 0xb

    sget-object v4, LX/12E;->m:[C

    and-int/lit8 v1, v1, 0xf

    aget-char v1, v4, v1

    aput-char v1, v0, v3

    .line 174486
    const/16 v1, 0xc

    sget-object v3, LX/12E;->m:[C

    shr-int/lit8 v4, v2, 0x4

    aget-char v3, v3, v4

    aput-char v3, v0, v1

    .line 174487
    const/16 v1, 0xd

    sget-object v3, LX/12E;->m:[C

    and-int/lit8 v2, v2, 0xf

    aget-char v2, v3, v2

    aput-char v2, v0, v1

    .line 174488
    iget-object v1, p0, LX/12E;->n:Ljava/io/Writer;

    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2, v6}, Ljava/io/Writer;->write([CII)V

    goto/16 :goto_0

    .line 174489
    :cond_6
    sget-object v1, LX/12E;->m:[C

    shr-int/lit8 v2, p4, 0x4

    aget-char v1, v1, v2

    aput-char v1, v0, v6

    .line 174490
    const/4 v1, 0x7

    sget-object v2, LX/12E;->m:[C

    and-int/lit8 v3, p4, 0xf

    aget-char v2, v2, v3

    aput-char v2, v0, v1

    .line 174491
    iget-object v1, p0, LX/12E;->n:Ljava/io/Writer;

    invoke-virtual {v1, v0, v4, v6}, Ljava/io/Writer;->write([CII)V

    goto/16 :goto_0

    .line 174492
    :cond_7
    iget-object v0, p0, LX/12E;->t:LX/0lc;

    if-nez v0, :cond_8

    .line 174493
    iget-object v0, p0, LX/12F;->k:LX/4pa;

    invoke-virtual {v0}, LX/4pa;->b()LX/0lc;

    move-result-object v0

    invoke-interface {v0}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v0

    .line 174494
    :goto_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 174495
    if-lt p2, v1, :cond_9

    if-ge p2, p3, :cond_9

    .line 174496
    sub-int/2addr p2, v1

    .line 174497
    invoke-virtual {v0, v2, v1, p1, p2}, Ljava/lang/String;->getChars(II[CI)V

    goto/16 :goto_0

    .line 174498
    :cond_8
    iget-object v0, p0, LX/12E;->t:LX/0lc;

    invoke-interface {v0}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v0

    .line 174499
    const/4 v1, 0x0

    iput-object v1, p0, LX/12E;->t:LX/0lc;

    goto :goto_2

    .line 174500
    :cond_9
    iget-object v1, p0, LX/12E;->n:Ljava/io/Writer;

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private a(CI)V
    .locals 8

    .prologue
    const/16 v7, 0x5c

    const/16 v3, 0x30

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v6, 0x6

    .line 174501
    if-ltz p2, :cond_2

    .line 174502
    iget v0, p0, LX/12E;->q:I

    if-lt v0, v4, :cond_0

    .line 174503
    iget v0, p0, LX/12E;->q:I

    add-int/lit8 v0, v0, -0x2

    .line 174504
    iput v0, p0, LX/12E;->p:I

    .line 174505
    iget-object v1, p0, LX/12E;->o:[C

    add-int/lit8 v2, v0, 0x1

    aput-char v7, v1, v0

    .line 174506
    iget-object v0, p0, LX/12E;->o:[C

    int-to-char v1, p2

    aput-char v1, v0, v2

    .line 174507
    :goto_0
    return-void

    .line 174508
    :cond_0
    iget-object v0, p0, LX/12E;->s:[C

    .line 174509
    if-nez v0, :cond_1

    .line 174510
    invoke-direct {p0}, LX/12E;->m()[C

    move-result-object v0

    .line 174511
    :cond_1
    iget v1, p0, LX/12E;->q:I

    iput v1, p0, LX/12E;->p:I

    .line 174512
    const/4 v1, 0x1

    int-to-char v2, p2

    aput-char v2, v0, v1

    .line 174513
    iget-object v1, p0, LX/12E;->n:Ljava/io/Writer;

    invoke-virtual {v1, v0, v5, v4}, Ljava/io/Writer;->write([CII)V

    goto :goto_0

    .line 174514
    :cond_2
    const/4 v0, -0x2

    if-eq p2, v0, :cond_7

    .line 174515
    iget v0, p0, LX/12E;->q:I

    if-lt v0, v6, :cond_4

    .line 174516
    iget-object v1, p0, LX/12E;->o:[C

    .line 174517
    iget v0, p0, LX/12E;->q:I

    add-int/lit8 v0, v0, -0x6

    .line 174518
    iput v0, p0, LX/12E;->p:I

    .line 174519
    aput-char v7, v1, v0

    .line 174520
    add-int/lit8 v0, v0, 0x1

    const/16 v2, 0x75

    aput-char v2, v1, v0

    .line 174521
    const/16 v2, 0xff

    if-le p1, v2, :cond_3

    .line 174522
    shr-int/lit8 v2, p1, 0x8

    and-int/lit16 v2, v2, 0xff

    .line 174523
    add-int/lit8 v0, v0, 0x1

    sget-object v3, LX/12E;->m:[C

    shr-int/lit8 v4, v2, 0x4

    aget-char v3, v3, v4

    aput-char v3, v1, v0

    .line 174524
    add-int/lit8 v0, v0, 0x1

    sget-object v3, LX/12E;->m:[C

    and-int/lit8 v2, v2, 0xf

    aget-char v2, v3, v2

    aput-char v2, v1, v0

    .line 174525
    and-int/lit16 v2, p1, 0xff

    int-to-char p1, v2

    .line 174526
    :goto_1
    add-int/lit8 v0, v0, 0x1

    sget-object v2, LX/12E;->m:[C

    shr-int/lit8 v3, p1, 0x4

    aget-char v2, v2, v3

    aput-char v2, v1, v0

    .line 174527
    add-int/lit8 v0, v0, 0x1

    sget-object v2, LX/12E;->m:[C

    and-int/lit8 v3, p1, 0xf

    aget-char v2, v2, v3

    aput-char v2, v1, v0

    goto :goto_0

    .line 174528
    :cond_3
    add-int/lit8 v0, v0, 0x1

    aput-char v3, v1, v0

    .line 174529
    add-int/lit8 v0, v0, 0x1

    aput-char v3, v1, v0

    goto :goto_1

    .line 174530
    :cond_4
    iget-object v0, p0, LX/12E;->s:[C

    .line 174531
    if-nez v0, :cond_5

    .line 174532
    invoke-direct {p0}, LX/12E;->m()[C

    move-result-object v0

    .line 174533
    :cond_5
    iget v1, p0, LX/12E;->q:I

    iput v1, p0, LX/12E;->p:I

    .line 174534
    const/16 v1, 0xff

    if-le p1, v1, :cond_6

    .line 174535
    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    .line 174536
    and-int/lit16 v2, p1, 0xff

    .line 174537
    const/16 v3, 0xa

    sget-object v4, LX/12E;->m:[C

    shr-int/lit8 v5, v1, 0x4

    aget-char v4, v4, v5

    aput-char v4, v0, v3

    .line 174538
    const/16 v3, 0xb

    sget-object v4, LX/12E;->m:[C

    and-int/lit8 v1, v1, 0xf

    aget-char v1, v4, v1

    aput-char v1, v0, v3

    .line 174539
    const/16 v1, 0xc

    sget-object v3, LX/12E;->m:[C

    shr-int/lit8 v4, v2, 0x4

    aget-char v3, v3, v4

    aput-char v3, v0, v1

    .line 174540
    const/16 v1, 0xd

    sget-object v3, LX/12E;->m:[C

    and-int/lit8 v2, v2, 0xf

    aget-char v2, v3, v2

    aput-char v2, v0, v1

    .line 174541
    iget-object v1, p0, LX/12E;->n:Ljava/io/Writer;

    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2, v6}, Ljava/io/Writer;->write([CII)V

    goto/16 :goto_0

    .line 174542
    :cond_6
    sget-object v1, LX/12E;->m:[C

    shr-int/lit8 v2, p1, 0x4

    aget-char v1, v1, v2

    aput-char v1, v0, v6

    .line 174543
    const/4 v1, 0x7

    sget-object v2, LX/12E;->m:[C

    and-int/lit8 v3, p1, 0xf

    aget-char v2, v2, v3

    aput-char v2, v0, v1

    .line 174544
    iget-object v1, p0, LX/12E;->n:Ljava/io/Writer;

    invoke-virtual {v1, v0, v4, v6}, Ljava/io/Writer;->write([CII)V

    goto/16 :goto_0

    .line 174545
    :cond_7
    iget-object v0, p0, LX/12E;->t:LX/0lc;

    if-nez v0, :cond_8

    .line 174546
    iget-object v0, p0, LX/12F;->k:LX/4pa;

    invoke-virtual {v0}, LX/4pa;->b()LX/0lc;

    move-result-object v0

    invoke-interface {v0}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v0

    .line 174547
    :goto_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 174548
    iget v2, p0, LX/12E;->q:I

    if-lt v2, v1, :cond_9

    .line 174549
    iget v2, p0, LX/12E;->q:I

    sub-int/2addr v2, v1

    .line 174550
    iput v2, p0, LX/12E;->p:I

    .line 174551
    iget-object v3, p0, LX/12E;->o:[C

    invoke-virtual {v0, v5, v1, v3, v2}, Ljava/lang/String;->getChars(II[CI)V

    goto/16 :goto_0

    .line 174552
    :cond_8
    iget-object v0, p0, LX/12E;->t:LX/0lc;

    invoke-interface {v0}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v0

    .line 174553
    const/4 v1, 0x0

    iput-object v1, p0, LX/12E;->t:LX/0lc;

    goto :goto_2

    .line 174554
    :cond_9
    iget v1, p0, LX/12E;->q:I

    iput v1, p0, LX/12E;->p:I

    .line 174555
    iget-object v1, p0, LX/12E;->n:Ljava/io/Writer;

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private a(II)V
    .locals 9

    .prologue
    .line 174556
    iget v0, p0, LX/12E;->q:I

    add-int v1, v0, p1

    .line 174557
    iget-object v2, p0, LX/12F;->i:[I

    .line 174558
    array-length v0, v2

    add-int/lit8 v3, p2, 0x1

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 174559
    :goto_0
    iget v0, p0, LX/12E;->q:I

    if-ge v0, v1, :cond_4

    .line 174560
    :cond_0
    iget-object v0, p0, LX/12E;->o:[C

    iget v4, p0, LX/12E;->q:I

    aget-char v4, v0, v4

    .line 174561
    if-ge v4, v3, :cond_2

    .line 174562
    aget v0, v2, v4

    .line 174563
    if-eqz v0, :cond_3

    .line 174564
    :goto_1
    iget v5, p0, LX/12E;->q:I

    iget v6, p0, LX/12E;->p:I

    sub-int/2addr v5, v6

    .line 174565
    if-lez v5, :cond_1

    .line 174566
    iget-object v6, p0, LX/12E;->n:Ljava/io/Writer;

    iget-object v7, p0, LX/12E;->o:[C

    iget v8, p0, LX/12E;->p:I

    invoke-virtual {v6, v7, v8, v5}, Ljava/io/Writer;->write([CII)V

    .line 174567
    :cond_1
    iget v5, p0, LX/12E;->q:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, LX/12E;->q:I

    .line 174568
    invoke-direct {p0, v4, v0}, LX/12E;->a(CI)V

    goto :goto_0

    .line 174569
    :cond_2
    if-le v4, p2, :cond_3

    .line 174570
    const/4 v0, -0x1

    .line 174571
    goto :goto_1

    .line 174572
    :cond_3
    iget v0, p0, LX/12E;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/12E;->q:I

    if-lt v0, v1, :cond_0

    .line 174573
    :cond_4
    return-void
.end method

.method private a(LX/0lc;Z)V
    .locals 6

    .prologue
    const/16 v5, 0x22

    const/4 v4, 0x0

    .line 174574
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    if-eqz v0, :cond_0

    .line 174575
    invoke-direct {p0, p1, p2}, LX/12E;->b(LX/0lc;Z)V

    .line 174576
    :goto_0
    return-void

    .line 174577
    :cond_0
    iget v0, p0, LX/12E;->q:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_1

    .line 174578
    invoke-direct {p0}, LX/12E;->n()V

    .line 174579
    :cond_1
    if-eqz p2, :cond_2

    .line 174580
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    const/16 v2, 0x2c

    aput-char v2, v0, v1

    .line 174581
    :cond_2
    invoke-interface {p1}, LX/0lc;->c()[C

    move-result-object v0

    .line 174582
    sget-object v1, LX/0ls;->QUOTE_FIELD_NAMES:LX/0ls;

    invoke-virtual {p0, v1}, LX/12G;->a(LX/0ls;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 174583
    array-length v1, v0

    invoke-virtual {p0, v0, v4, v1}, LX/0nX;->b([CII)V

    goto :goto_0

    .line 174584
    :cond_3
    iget-object v1, p0, LX/12E;->o:[C

    iget v2, p0, LX/12E;->q:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/12E;->q:I

    aput-char v5, v1, v2

    .line 174585
    array-length v1, v0

    .line 174586
    iget v2, p0, LX/12E;->q:I

    add-int/2addr v2, v1

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, LX/12E;->r:I

    if-lt v2, v3, :cond_5

    .line 174587
    invoke-virtual {p0, v0, v4, v1}, LX/0nX;->b([CII)V

    .line 174588
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_4

    .line 174589
    invoke-direct {p0}, LX/12E;->n()V

    .line 174590
    :cond_4
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v5, v0, v1

    goto :goto_0

    .line 174591
    :cond_5
    iget-object v2, p0, LX/12E;->o:[C

    iget v3, p0, LX/12E;->q:I

    invoke-static {v0, v4, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 174592
    iget v0, p0, LX/12E;->q:I

    add-int/2addr v0, v1

    iput v0, p0, LX/12E;->q:I

    .line 174593
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v5, v0, v1

    goto :goto_0
.end method

.method private a([CIII)V
    .locals 10

    .prologue
    .line 174594
    add-int v3, p3, p2

    .line 174595
    iget-object v4, p0, LX/12F;->i:[I

    .line 174596
    array-length v0, v4

    add-int/lit8 v1, p4, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 174597
    const/4 v0, 0x0

    move v2, p2

    .line 174598
    :goto_0
    if-ge v2, v3, :cond_6

    move v1, v2

    .line 174599
    :cond_0
    aget-char v6, p1, v1

    .line 174600
    if-ge v6, v5, :cond_3

    .line 174601
    aget v0, v4, v6

    .line 174602
    if-eqz v0, :cond_4

    .line 174603
    :goto_1
    sub-int v7, v1, v2

    .line 174604
    const/16 v8, 0x20

    if-ge v7, v8, :cond_5

    .line 174605
    iget v8, p0, LX/12E;->q:I

    add-int/2addr v8, v7

    iget v9, p0, LX/12E;->r:I

    if-le v8, v9, :cond_1

    .line 174606
    invoke-direct {p0}, LX/12E;->n()V

    .line 174607
    :cond_1
    if-lez v7, :cond_2

    .line 174608
    iget-object v8, p0, LX/12E;->o:[C

    iget v9, p0, LX/12E;->q:I

    invoke-static {p1, v2, v8, v9, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 174609
    iget v2, p0, LX/12E;->q:I

    add-int/2addr v2, v7

    iput v2, p0, LX/12E;->q:I

    .line 174610
    :cond_2
    :goto_2
    if-ge v1, v3, :cond_6

    .line 174611
    add-int/lit8 v2, v1, 0x1

    .line 174612
    invoke-direct {p0, v6, v0}, LX/12E;->b(CI)V

    goto :goto_0

    .line 174613
    :cond_3
    if-le v6, p4, :cond_4

    .line 174614
    const/4 v0, -0x1

    .line 174615
    goto :goto_1

    .line 174616
    :cond_4
    add-int/lit8 v1, v1, 0x1

    if-lt v1, v3, :cond_0

    goto :goto_1

    .line 174617
    :cond_5
    invoke-direct {p0}, LX/12E;->n()V

    .line 174618
    iget-object v8, p0, LX/12E;->n:Ljava/io/Writer;

    invoke-virtual {v8, p1, v2, v7}, Ljava/io/Writer;->write([CII)V

    goto :goto_2

    .line 174619
    :cond_6
    return-void
.end method

.method private b(CI)V
    .locals 6

    .prologue
    const/16 v3, 0x5c

    const/16 v4, 0x30

    .line 174620
    if-ltz p2, :cond_1

    .line 174621
    iget v0, p0, LX/12E;->q:I

    add-int/lit8 v0, v0, 0x2

    iget v1, p0, LX/12E;->r:I

    if-le v0, v1, :cond_0

    .line 174622
    invoke-direct {p0}, LX/12E;->n()V

    .line 174623
    :cond_0
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    .line 174624
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    int-to-char v2, p2

    aput-char v2, v0, v1

    .line 174625
    :goto_0
    return-void

    .line 174626
    :cond_1
    const/4 v0, -0x2

    if-eq p2, v0, :cond_4

    .line 174627
    iget v0, p0, LX/12E;->q:I

    add-int/lit8 v0, v0, 0x2

    iget v1, p0, LX/12E;->r:I

    if-le v0, v1, :cond_2

    .line 174628
    invoke-direct {p0}, LX/12E;->n()V

    .line 174629
    :cond_2
    iget v0, p0, LX/12E;->q:I

    .line 174630
    iget-object v1, p0, LX/12E;->o:[C

    .line 174631
    add-int/lit8 v2, v0, 0x1

    aput-char v3, v1, v0

    .line 174632
    add-int/lit8 v0, v2, 0x1

    const/16 v3, 0x75

    aput-char v3, v1, v2

    .line 174633
    const/16 v2, 0xff

    if-le p1, v2, :cond_3

    .line 174634
    shr-int/lit8 v2, p1, 0x8

    and-int/lit16 v2, v2, 0xff

    .line 174635
    add-int/lit8 v3, v0, 0x1

    sget-object v4, LX/12E;->m:[C

    shr-int/lit8 v5, v2, 0x4

    aget-char v4, v4, v5

    aput-char v4, v1, v0

    .line 174636
    add-int/lit8 v0, v3, 0x1

    sget-object v4, LX/12E;->m:[C

    and-int/lit8 v2, v2, 0xf

    aget-char v2, v4, v2

    aput-char v2, v1, v3

    .line 174637
    and-int/lit16 v2, p1, 0xff

    int-to-char p1, v2

    .line 174638
    :goto_1
    add-int/lit8 v2, v0, 0x1

    sget-object v3, LX/12E;->m:[C

    shr-int/lit8 v4, p1, 0x4

    aget-char v3, v3, v4

    aput-char v3, v1, v0

    .line 174639
    sget-object v0, LX/12E;->m:[C

    and-int/lit8 v3, p1, 0xf

    aget-char v0, v0, v3

    aput-char v0, v1, v2

    .line 174640
    iput v2, p0, LX/12E;->q:I

    goto :goto_0

    .line 174641
    :cond_3
    add-int/lit8 v2, v0, 0x1

    aput-char v4, v1, v0

    .line 174642
    add-int/lit8 v0, v2, 0x1

    aput-char v4, v1, v2

    goto :goto_1

    .line 174643
    :cond_4
    iget-object v0, p0, LX/12E;->t:LX/0lc;

    if-nez v0, :cond_5

    .line 174644
    iget-object v0, p0, LX/12F;->k:LX/4pa;

    invoke-virtual {v0}, LX/4pa;->b()LX/0lc;

    move-result-object v0

    invoke-interface {v0}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v0

    .line 174645
    :goto_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 174646
    iget v2, p0, LX/12E;->q:I

    add-int/2addr v2, v1

    iget v3, p0, LX/12E;->r:I

    if-le v2, v3, :cond_6

    .line 174647
    invoke-direct {p0}, LX/12E;->n()V

    .line 174648
    iget v2, p0, LX/12E;->r:I

    if-le v1, v2, :cond_6

    .line 174649
    iget-object v1, p0, LX/12E;->n:Ljava/io/Writer;

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_0

    .line 174650
    :cond_5
    iget-object v0, p0, LX/12E;->t:LX/0lc;

    invoke-interface {v0}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v0

    .line 174651
    const/4 v1, 0x0

    iput-object v1, p0, LX/12E;->t:LX/0lc;

    goto :goto_2

    .line 174652
    :cond_6
    const/4 v2, 0x0

    iget-object v3, p0, LX/12E;->o:[C

    iget v4, p0, LX/12E;->q:I

    invoke-virtual {v0, v2, v1, v3, v4}, Ljava/lang/String;->getChars(II[CI)V

    .line 174653
    iget v0, p0, LX/12E;->q:I

    add-int/2addr v0, v1

    iput v0, p0, LX/12E;->q:I

    goto/16 :goto_0
.end method

.method private b(II)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 174654
    iget-object v6, p0, LX/12F;->i:[I

    .line 174655
    array-length v1, v6

    add-int/lit8 v2, p2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v7

    move v2, v0

    move v1, v0

    .line 174656
    :goto_0
    if-ge v1, p1, :cond_5

    .line 174657
    :cond_0
    iget-object v3, p0, LX/12E;->o:[C

    aget-char v4, v3, v1

    .line 174658
    if-ge v4, v7, :cond_2

    .line 174659
    aget v5, v6, v4

    .line 174660
    if-eqz v5, :cond_3

    .line 174661
    :goto_1
    sub-int v0, v1, v2

    .line 174662
    if-lez v0, :cond_1

    .line 174663
    iget-object v3, p0, LX/12E;->n:Ljava/io/Writer;

    iget-object v8, p0, LX/12E;->o:[C

    invoke-virtual {v3, v8, v2, v0}, Ljava/io/Writer;->write([CII)V

    .line 174664
    if-ge v1, p1, :cond_5

    .line 174665
    :cond_1
    add-int/lit8 v2, v1, 0x1

    .line 174666
    iget-object v1, p0, LX/12E;->o:[C

    move-object v0, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, LX/12E;->a([CIICI)I

    move-result v0

    move v1, v2

    move v2, v0

    move v0, v5

    .line 174667
    goto :goto_0

    .line 174668
    :cond_2
    if-le v4, p2, :cond_4

    .line 174669
    const/4 v5, -0x1

    .line 174670
    goto :goto_1

    :cond_3
    move v0, v5

    .line 174671
    :cond_4
    add-int/lit8 v1, v1, 0x1

    if-lt v1, p1, :cond_0

    move v5, v0

    goto :goto_1

    .line 174672
    :cond_5
    return-void
.end method

.method private b(J)V
    .locals 5

    .prologue
    const/16 v3, 0x22

    .line 174673
    iget v0, p0, LX/12E;->q:I

    add-int/lit8 v0, v0, 0x17

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_0

    .line 174674
    invoke-direct {p0}, LX/12E;->n()V

    .line 174675
    :cond_0
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    .line 174676
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    invoke-static {p1, p2, v0, v1}, LX/13I;->a(J[CI)I

    move-result v0

    iput v0, p0, LX/12E;->q:I

    .line 174677
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    .line 174678
    return-void
.end method

.method private b(LX/0lc;Z)V
    .locals 6

    .prologue
    const/16 v5, 0x22

    const/4 v4, 0x0

    .line 174679
    if-eqz p2, :cond_2

    .line 174680
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->c(LX/0nX;)V

    .line 174681
    :goto_0
    invoke-interface {p1}, LX/0lc;->c()[C

    move-result-object v0

    .line 174682
    sget-object v1, LX/0ls;->QUOTE_FIELD_NAMES:LX/0ls;

    invoke-virtual {p0, v1}, LX/12G;->a(LX/0ls;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 174683
    iget v1, p0, LX/12E;->q:I

    iget v2, p0, LX/12E;->r:I

    if-lt v1, v2, :cond_0

    .line 174684
    invoke-direct {p0}, LX/12E;->n()V

    .line 174685
    :cond_0
    iget-object v1, p0, LX/12E;->o:[C

    iget v2, p0, LX/12E;->q:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/12E;->q:I

    aput-char v5, v1, v2

    .line 174686
    array-length v1, v0

    invoke-virtual {p0, v0, v4, v1}, LX/0nX;->b([CII)V

    .line 174687
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_1

    .line 174688
    invoke-direct {p0}, LX/12E;->n()V

    .line 174689
    :cond_1
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v5, v0, v1

    .line 174690
    :goto_1
    return-void

    .line 174691
    :cond_2
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->h(LX/0nX;)V

    goto :goto_0

    .line 174692
    :cond_3
    array-length v1, v0

    invoke-virtual {p0, v0, v4, v1}, LX/0nX;->b([CII)V

    goto :goto_1
.end method

.method private b(LX/0ln;[BII)V
    .locals 6

    .prologue
    .line 174693
    add-int/lit8 v1, p4, -0x3

    .line 174694
    iget v0, p0, LX/12E;->r:I

    add-int/lit8 v2, v0, -0x6

    .line 174695
    iget v0, p1, LX/0ln;->c:I

    move v0, v0

    .line 174696
    shr-int/lit8 v0, v0, 0x2

    .line 174697
    :cond_0
    :goto_0
    if-gt p3, v1, :cond_2

    .line 174698
    iget v3, p0, LX/12E;->q:I

    if-le v3, v2, :cond_1

    .line 174699
    invoke-direct {p0}, LX/12E;->n()V

    .line 174700
    :cond_1
    add-int/lit8 v3, p3, 0x1

    aget-byte v4, p2, p3

    shl-int/lit8 v4, v4, 0x8

    .line 174701
    add-int/lit8 v5, v3, 0x1

    aget-byte v3, p2, v3

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v3, v4

    .line 174702
    shl-int/lit8 v3, v3, 0x8

    add-int/lit8 p3, v5, 0x1

    aget-byte v4, p2, v5

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v3, v4

    .line 174703
    iget-object v4, p0, LX/12E;->o:[C

    iget v5, p0, LX/12E;->q:I

    invoke-virtual {p1, v3, v4, v5}, LX/0ln;->a(I[CI)I

    move-result v3

    iput v3, p0, LX/12E;->q:I

    .line 174704
    add-int/lit8 v0, v0, -0x1

    if-gtz v0, :cond_0

    .line 174705
    iget-object v0, p0, LX/12E;->o:[C

    iget v3, p0, LX/12E;->q:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/12E;->q:I

    const/16 v4, 0x5c

    aput-char v4, v0, v3

    .line 174706
    iget-object v0, p0, LX/12E;->o:[C

    iget v3, p0, LX/12E;->q:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/12E;->q:I

    const/16 v4, 0x6e

    aput-char v4, v0, v3

    .line 174707
    iget v0, p1, LX/0ln;->c:I

    move v0, v0

    .line 174708
    shr-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 174709
    :cond_2
    sub-int v1, p4, p3

    .line 174710
    if-lez v1, :cond_5

    .line 174711
    iget v0, p0, LX/12E;->q:I

    if-le v0, v2, :cond_3

    .line 174712
    invoke-direct {p0}, LX/12E;->n()V

    .line 174713
    :cond_3
    add-int/lit8 v2, p3, 0x1

    aget-byte v0, p2, p3

    shl-int/lit8 v0, v0, 0x10

    .line 174714
    const/4 v3, 0x2

    if-ne v1, v3, :cond_4

    .line 174715
    aget-byte v2, p2, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v0, v2

    .line 174716
    :cond_4
    iget-object v2, p0, LX/12E;->o:[C

    iget v3, p0, LX/12E;->q:I

    invoke-virtual {p1, v0, v1, v2, v3}, LX/0ln;->a(II[CI)I

    move-result v0

    iput v0, p0, LX/12E;->q:I

    .line 174717
    :cond_5
    return-void
.end method

.method private b(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 174718
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_0

    .line 174719
    invoke-direct {p0}, LX/12E;->n()V

    .line 174720
    :cond_0
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    .line 174721
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->c(Ljava/lang/String;)V

    .line 174722
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_1

    .line 174723
    invoke-direct {p0}, LX/12E;->n()V

    .line 174724
    :cond_1
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    .line 174725
    return-void
.end method

.method private b(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 174726
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    if-eqz v0, :cond_0

    .line 174727
    invoke-direct {p0, p1, p2}, LX/12E;->c(Ljava/lang/String;Z)V

    .line 174728
    :goto_0
    return-void

    .line 174729
    :cond_0
    iget v0, p0, LX/12E;->q:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_1

    .line 174730
    invoke-direct {p0}, LX/12E;->n()V

    .line 174731
    :cond_1
    if-eqz p2, :cond_2

    .line 174732
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    const/16 v2, 0x2c

    aput-char v2, v0, v1

    .line 174733
    :cond_2
    sget-object v0, LX/0ls;->QUOTE_FIELD_NAMES:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 174734
    invoke-direct {p0, p1}, LX/12E;->k(Ljava/lang/String;)V

    goto :goto_0

    .line 174735
    :cond_3
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    .line 174736
    invoke-direct {p0, p1}, LX/12E;->k(Ljava/lang/String;)V

    .line 174737
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_4

    .line 174738
    invoke-direct {p0}, LX/12E;->n()V

    .line 174739
    :cond_4
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    goto :goto_0
.end method

.method private b(S)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 174746
    iget v0, p0, LX/12E;->q:I

    add-int/lit8 v0, v0, 0x8

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_0

    .line 174747
    invoke-direct {p0}, LX/12E;->n()V

    .line 174748
    :cond_0
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    .line 174749
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    invoke-static {p1, v0, v1}, LX/13I;->a(I[CI)I

    move-result v0

    iput v0, p0, LX/12E;->q:I

    .line 174750
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    .line 174751
    return-void
.end method

.method private c(I)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 174740
    iget v0, p0, LX/12E;->q:I

    add-int/lit8 v0, v0, 0xd

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_0

    .line 174741
    invoke-direct {p0}, LX/12E;->n()V

    .line 174742
    :cond_0
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    .line 174743
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    invoke-static {p1, v0, v1}, LX/13I;->a(I[CI)I

    move-result v0

    iput v0, p0, LX/12E;->q:I

    .line 174744
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    .line 174745
    return-void
.end method

.method private c(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 174944
    if-eqz p2, :cond_2

    .line 174945
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->c(LX/0nX;)V

    .line 174946
    :goto_0
    sget-object v0, LX/0ls;->QUOTE_FIELD_NAMES:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 174947
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_0

    .line 174948
    invoke-direct {p0}, LX/12E;->n()V

    .line 174949
    :cond_0
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    .line 174950
    invoke-direct {p0, p1}, LX/12E;->k(Ljava/lang/String;)V

    .line 174951
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_1

    .line 174952
    invoke-direct {p0}, LX/12E;->n()V

    .line 174953
    :cond_1
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    .line 174954
    :goto_1
    return-void

    .line 174955
    :cond_2
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->h(LX/0nX;)V

    goto :goto_0

    .line 174956
    :cond_3
    invoke-direct {p0, p1}, LX/12E;->k(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private c([CII)V
    .locals 8

    .prologue
    .line 174920
    iget-object v0, p0, LX/12F;->k:LX/4pa;

    if-eqz v0, :cond_1

    .line 174921
    invoke-direct {p0, p1, p2, p3}, LX/12E;->d([CII)V

    .line 174922
    :cond_0
    :goto_0
    return-void

    .line 174923
    :cond_1
    iget v0, p0, LX/12F;->j:I

    if-eqz v0, :cond_2

    .line 174924
    iget v0, p0, LX/12F;->j:I

    invoke-direct {p0, p1, p2, p3, v0}, LX/12E;->a([CIII)V

    goto :goto_0

    .line 174925
    :cond_2
    add-int v2, p3, p2

    .line 174926
    iget-object v3, p0, LX/12F;->i:[I

    .line 174927
    array-length v4, v3

    move v1, p2

    .line 174928
    :goto_1
    if-ge v1, v2, :cond_0

    move v0, v1

    .line 174929
    :cond_3
    aget-char v5, p1, v0

    .line 174930
    if-ge v5, v4, :cond_4

    aget v5, v3, v5

    if-nez v5, :cond_5

    .line 174931
    :cond_4
    add-int/lit8 v0, v0, 0x1

    if-lt v0, v2, :cond_3

    .line 174932
    :cond_5
    sub-int v5, v0, v1

    .line 174933
    const/16 v6, 0x20

    if-ge v5, v6, :cond_8

    .line 174934
    iget v6, p0, LX/12E;->q:I

    add-int/2addr v6, v5

    iget v7, p0, LX/12E;->r:I

    if-le v6, v7, :cond_6

    .line 174935
    invoke-direct {p0}, LX/12E;->n()V

    .line 174936
    :cond_6
    if-lez v5, :cond_7

    .line 174937
    iget-object v6, p0, LX/12E;->o:[C

    iget v7, p0, LX/12E;->q:I

    invoke-static {p1, v1, v6, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 174938
    iget v1, p0, LX/12E;->q:I

    add-int/2addr v1, v5

    iput v1, p0, LX/12E;->q:I

    .line 174939
    :cond_7
    :goto_2
    if-ge v0, v2, :cond_0

    .line 174940
    add-int/lit8 v1, v0, 0x1

    aget-char v0, p1, v0

    .line 174941
    aget v5, v3, v0

    invoke-direct {p0, v0, v5}, LX/12E;->b(CI)V

    goto :goto_1

    .line 174942
    :cond_8
    invoke-direct {p0}, LX/12E;->n()V

    .line 174943
    iget-object v6, p0, LX/12E;->n:Ljava/io/Writer;

    invoke-virtual {v6, p1, v1, v5}, Ljava/io/Writer;->write([CII)V

    goto :goto_2
.end method

.method private d(I)V
    .locals 1

    .prologue
    .line 174910
    packed-switch p1, :pswitch_data_0

    .line 174911
    invoke-static {}, LX/4pl;->b()V

    .line 174912
    :cond_0
    :goto_0
    return-void

    .line 174913
    :pswitch_0
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->f(LX/0nX;)V

    goto :goto_0

    .line 174914
    :pswitch_1
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->d(LX/0nX;)V

    goto :goto_0

    .line 174915
    :pswitch_2
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->a(LX/0nX;)V

    goto :goto_0

    .line 174916
    :pswitch_3
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12V;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174917
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->g(LX/0nX;)V

    goto :goto_0

    .line 174918
    :cond_1
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12V;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174919
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->h(LX/0nX;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private d([CII)V
    .locals 12

    .prologue
    .line 174878
    add-int v4, p3, p2

    .line 174879
    iget-object v5, p0, LX/12F;->i:[I

    .line 174880
    iget v0, p0, LX/12F;->j:I

    if-gtz v0, :cond_3

    const v0, 0xffff

    .line 174881
    :goto_0
    array-length v1, v5

    add-int/lit8 v2, v0, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 174882
    iget-object v7, p0, LX/12F;->k:LX/4pa;

    .line 174883
    const/4 v1, 0x0

    move v3, p2

    .line 174884
    :goto_1
    if-ge v3, v4, :cond_8

    move v2, v3

    .line 174885
    :cond_0
    aget-char v8, p1, v2

    .line 174886
    if-ge v8, v6, :cond_4

    .line 174887
    aget v1, v5, v8

    .line 174888
    if-eqz v1, :cond_6

    .line 174889
    :goto_2
    sub-int v9, v2, v3

    .line 174890
    const/16 v10, 0x20

    if-ge v9, v10, :cond_7

    .line 174891
    iget v10, p0, LX/12E;->q:I

    add-int/2addr v10, v9

    iget v11, p0, LX/12E;->r:I

    if-le v10, v11, :cond_1

    .line 174892
    invoke-direct {p0}, LX/12E;->n()V

    .line 174893
    :cond_1
    if-lez v9, :cond_2

    .line 174894
    iget-object v10, p0, LX/12E;->o:[C

    iget v11, p0, LX/12E;->q:I

    invoke-static {p1, v3, v10, v11, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 174895
    iget v3, p0, LX/12E;->q:I

    add-int/2addr v3, v9

    iput v3, p0, LX/12E;->q:I

    .line 174896
    :cond_2
    :goto_3
    if-ge v2, v4, :cond_8

    .line 174897
    add-int/lit8 v3, v2, 0x1

    .line 174898
    invoke-direct {p0, v8, v1}, LX/12E;->b(CI)V

    goto :goto_1

    .line 174899
    :cond_3
    iget v0, p0, LX/12F;->j:I

    goto :goto_0

    .line 174900
    :cond_4
    if-le v8, v0, :cond_5

    .line 174901
    const/4 v1, -0x1

    .line 174902
    goto :goto_2

    .line 174903
    :cond_5
    invoke-virtual {v7}, LX/4pa;->b()LX/0lc;

    move-result-object v9

    iput-object v9, p0, LX/12E;->t:LX/0lc;

    if-eqz v9, :cond_6

    .line 174904
    const/4 v1, -0x2

    .line 174905
    goto :goto_2

    .line 174906
    :cond_6
    add-int/lit8 v2, v2, 0x1

    if-lt v2, v4, :cond_0

    goto :goto_2

    .line 174907
    :cond_7
    invoke-direct {p0}, LX/12E;->n()V

    .line 174908
    iget-object v10, p0, LX/12E;->n:Ljava/io/Writer;

    invoke-virtual {v10, p1, v3, v9}, Ljava/io/Writer;->write([CII)V

    goto :goto_3

    .line 174909
    :cond_8
    return-void
.end method

.method private e(I)V
    .locals 7

    .prologue
    .line 174865
    iget v0, p0, LX/12E;->q:I

    add-int/2addr v0, p1

    .line 174866
    iget-object v1, p0, LX/12F;->i:[I

    .line 174867
    array-length v2, v1

    .line 174868
    :goto_0
    iget v3, p0, LX/12E;->q:I

    if-ge v3, v0, :cond_2

    .line 174869
    :cond_0
    iget-object v3, p0, LX/12E;->o:[C

    iget v4, p0, LX/12E;->q:I

    aget-char v3, v3, v4

    .line 174870
    if-ge v3, v2, :cond_1

    aget v3, v1, v3

    if-nez v3, :cond_3

    .line 174871
    :cond_1
    iget v3, p0, LX/12E;->q:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/12E;->q:I

    if-lt v3, v0, :cond_0

    .line 174872
    :cond_2
    return-void

    .line 174873
    :cond_3
    iget v3, p0, LX/12E;->q:I

    iget v4, p0, LX/12E;->p:I

    sub-int/2addr v3, v4

    .line 174874
    if-lez v3, :cond_4

    .line 174875
    iget-object v4, p0, LX/12E;->n:Ljava/io/Writer;

    iget-object v5, p0, LX/12E;->o:[C

    iget v6, p0, LX/12E;->p:I

    invoke-virtual {v4, v5, v6, v3}, Ljava/io/Writer;->write([CII)V

    .line 174876
    :cond_4
    iget-object v3, p0, LX/12E;->o:[C

    iget v4, p0, LX/12E;->q:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/12E;->q:I

    aget-char v3, v3, v4

    .line 174877
    aget v4, v1, v3

    invoke-direct {p0, v3, v4}, LX/12E;->a(CI)V

    goto :goto_0
.end method

.method private f(I)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 174851
    iget-object v6, p0, LX/12F;->i:[I

    .line 174852
    array-length v7, v6

    move v1, v0

    .line 174853
    :goto_0
    if-ge v0, p1, :cond_4

    .line 174854
    :cond_0
    iget-object v2, p0, LX/12E;->o:[C

    aget-char v4, v2, v0

    .line 174855
    if-ge v4, v7, :cond_1

    aget v2, v6, v4

    if-nez v2, :cond_2

    .line 174856
    :cond_1
    add-int/lit8 v0, v0, 0x1

    if-lt v0, p1, :cond_0

    .line 174857
    :cond_2
    sub-int v2, v0, v1

    .line 174858
    if-lez v2, :cond_3

    .line 174859
    iget-object v3, p0, LX/12E;->n:Ljava/io/Writer;

    iget-object v5, p0, LX/12E;->o:[C

    invoke-virtual {v3, v5, v1, v2}, Ljava/io/Writer;->write([CII)V

    .line 174860
    if-ge v0, p1, :cond_4

    .line 174861
    :cond_3
    add-int/lit8 v2, v0, 0x1

    .line 174862
    iget-object v1, p0, LX/12E;->o:[C

    aget v5, v6, v4

    move-object v0, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, LX/12E;->a([CIICI)I

    move-result v0

    move v1, v0

    move v0, v2

    .line 174863
    goto :goto_0

    .line 174864
    :cond_4
    return-void
.end method

.method private g(I)V
    .locals 11

    .prologue
    .line 174827
    iget v0, p0, LX/12E;->q:I

    add-int v2, v0, p1

    .line 174828
    iget-object v3, p0, LX/12F;->i:[I

    .line 174829
    iget v0, p0, LX/12F;->j:I

    if-gtz v0, :cond_2

    const v0, 0xffff

    .line 174830
    :goto_0
    array-length v1, v3

    add-int/lit8 v4, v0, 0x1

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 174831
    iget-object v5, p0, LX/12F;->k:LX/4pa;

    .line 174832
    :goto_1
    iget v1, p0, LX/12E;->q:I

    if-ge v1, v2, :cond_6

    .line 174833
    :cond_0
    iget-object v1, p0, LX/12E;->o:[C

    iget v6, p0, LX/12E;->q:I

    aget-char v6, v1, v6

    .line 174834
    if-ge v6, v4, :cond_3

    .line 174835
    aget v1, v3, v6

    .line 174836
    if-eqz v1, :cond_5

    .line 174837
    :goto_2
    iget v7, p0, LX/12E;->q:I

    iget v8, p0, LX/12E;->p:I

    sub-int/2addr v7, v8

    .line 174838
    if-lez v7, :cond_1

    .line 174839
    iget-object v8, p0, LX/12E;->n:Ljava/io/Writer;

    iget-object v9, p0, LX/12E;->o:[C

    iget v10, p0, LX/12E;->p:I

    invoke-virtual {v8, v9, v10, v7}, Ljava/io/Writer;->write([CII)V

    .line 174840
    :cond_1
    iget v7, p0, LX/12E;->q:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, LX/12E;->q:I

    .line 174841
    invoke-direct {p0, v6, v1}, LX/12E;->a(CI)V

    goto :goto_1

    .line 174842
    :cond_2
    iget v0, p0, LX/12F;->j:I

    goto :goto_0

    .line 174843
    :cond_3
    if-le v6, v0, :cond_4

    .line 174844
    const/4 v1, -0x1

    .line 174845
    goto :goto_2

    .line 174846
    :cond_4
    invoke-virtual {v5}, LX/4pa;->b()LX/0lc;

    move-result-object v1

    iput-object v1, p0, LX/12E;->t:LX/0lc;

    if-eqz v1, :cond_5

    .line 174847
    const/4 v1, -0x2

    .line 174848
    goto :goto_2

    .line 174849
    :cond_5
    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/12E;->q:I

    if-lt v1, v2, :cond_0

    .line 174850
    :cond_6
    return-void
.end method

.method private h(I)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 174802
    iget-object v7, p0, LX/12F;->i:[I

    .line 174803
    iget v0, p0, LX/12F;->j:I

    if-gtz v0, :cond_2

    const v0, 0xffff

    move v6, v0

    .line 174804
    :goto_0
    array-length v0, v7

    add-int/lit8 v2, v6, 0x1

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 174805
    iget-object v9, p0, LX/12F;->k:LX/4pa;

    move v2, v1

    move v0, v1

    .line 174806
    :goto_1
    if-ge v1, p1, :cond_7

    .line 174807
    :cond_0
    iget-object v3, p0, LX/12E;->o:[C

    aget-char v4, v3, v1

    .line 174808
    if-ge v4, v8, :cond_3

    .line 174809
    aget v5, v7, v4

    .line 174810
    if-eqz v5, :cond_5

    .line 174811
    :goto_2
    sub-int v0, v1, v2

    .line 174812
    if-lez v0, :cond_1

    .line 174813
    iget-object v3, p0, LX/12E;->n:Ljava/io/Writer;

    iget-object v10, p0, LX/12E;->o:[C

    invoke-virtual {v3, v10, v2, v0}, Ljava/io/Writer;->write([CII)V

    .line 174814
    if-ge v1, p1, :cond_7

    .line 174815
    :cond_1
    add-int/lit8 v2, v1, 0x1

    .line 174816
    iget-object v1, p0, LX/12E;->o:[C

    move-object v0, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, LX/12E;->a([CIICI)I

    move-result v0

    move v1, v2

    move v2, v0

    move v0, v5

    .line 174817
    goto :goto_1

    .line 174818
    :cond_2
    iget v0, p0, LX/12F;->j:I

    move v6, v0

    goto :goto_0

    .line 174819
    :cond_3
    if-le v4, v6, :cond_4

    .line 174820
    const/4 v5, -0x1

    .line 174821
    goto :goto_2

    .line 174822
    :cond_4
    invoke-virtual {v9}, LX/4pa;->b()LX/0lc;

    move-result-object v3

    iput-object v3, p0, LX/12E;->t:LX/0lc;

    if-eqz v3, :cond_6

    .line 174823
    const/4 v5, -0x2

    .line 174824
    goto :goto_2

    :cond_5
    move v0, v5

    .line 174825
    :cond_6
    add-int/lit8 v1, v1, 0x1

    if-lt v1, p1, :cond_0

    move v5, v0

    goto :goto_2

    .line 174826
    :cond_7
    return-void
.end method

.method private j(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 174784
    iget v0, p0, LX/12E;->r:I

    iget v1, p0, LX/12E;->q:I

    sub-int v1, v0, v1

    .line 174785
    iget-object v0, p0, LX/12E;->o:[C

    iget v2, p0, LX/12E;->q:I

    invoke-virtual {p1, v5, v1, v0, v2}, Ljava/lang/String;->getChars(II[CI)V

    .line 174786
    iget v0, p0, LX/12E;->q:I

    add-int/2addr v0, v1

    iput v0, p0, LX/12E;->q:I

    .line 174787
    invoke-direct {p0}, LX/12E;->n()V

    .line 174788
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    sub-int/2addr v0, v1

    .line 174789
    :goto_0
    iget v2, p0, LX/12E;->r:I

    if-le v0, v2, :cond_0

    .line 174790
    iget v2, p0, LX/12E;->r:I

    .line 174791
    add-int v3, v1, v2

    iget-object v4, p0, LX/12E;->o:[C

    invoke-virtual {p1, v1, v3, v4, v5}, Ljava/lang/String;->getChars(II[CI)V

    .line 174792
    iput v5, p0, LX/12E;->p:I

    .line 174793
    iput v2, p0, LX/12E;->q:I

    .line 174794
    invoke-direct {p0}, LX/12E;->n()V

    .line 174795
    add-int/2addr v1, v2

    .line 174796
    sub-int/2addr v0, v2

    .line 174797
    goto :goto_0

    .line 174798
    :cond_0
    add-int v2, v1, v0

    iget-object v3, p0, LX/12E;->o:[C

    invoke-virtual {p1, v1, v2, v3, v5}, Ljava/lang/String;->getChars(II[CI)V

    .line 174799
    iput v5, p0, LX/12E;->p:I

    .line 174800
    iput v0, p0, LX/12E;->q:I

    .line 174801
    return-void
.end method

.method private k(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 174772
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 174773
    iget v1, p0, LX/12E;->r:I

    if-le v0, v1, :cond_0

    .line 174774
    invoke-direct {p0, p1}, LX/12E;->l(Ljava/lang/String;)V

    .line 174775
    :goto_0
    return-void

    .line 174776
    :cond_0
    iget v1, p0, LX/12E;->q:I

    add-int/2addr v1, v0

    iget v2, p0, LX/12E;->r:I

    if-le v1, v2, :cond_1

    .line 174777
    invoke-direct {p0}, LX/12E;->n()V

    .line 174778
    :cond_1
    const/4 v1, 0x0

    iget-object v2, p0, LX/12E;->o:[C

    iget v3, p0, LX/12E;->q:I

    invoke-virtual {p1, v1, v0, v2, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 174779
    iget-object v1, p0, LX/12F;->k:LX/4pa;

    if-eqz v1, :cond_2

    .line 174780
    invoke-direct {p0, v0}, LX/12E;->g(I)V

    goto :goto_0

    .line 174781
    :cond_2
    iget v1, p0, LX/12F;->j:I

    if-eqz v1, :cond_3

    .line 174782
    iget v1, p0, LX/12F;->j:I

    invoke-direct {p0, v0, v1}, LX/12E;->a(II)V

    goto :goto_0

    .line 174783
    :cond_3
    invoke-direct {p0, v0}, LX/12E;->e(I)V

    goto :goto_0
.end method

.method private l()V
    .locals 4

    .prologue
    const/16 v3, 0x6c

    .line 174762
    iget v0, p0, LX/12E;->q:I

    add-int/lit8 v0, v0, 0x4

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_0

    .line 174763
    invoke-direct {p0}, LX/12E;->n()V

    .line 174764
    :cond_0
    iget v0, p0, LX/12E;->q:I

    .line 174765
    iget-object v1, p0, LX/12E;->o:[C

    .line 174766
    const/16 v2, 0x6e

    aput-char v2, v1, v0

    .line 174767
    add-int/lit8 v0, v0, 0x1

    const/16 v2, 0x75

    aput-char v2, v1, v0

    .line 174768
    add-int/lit8 v0, v0, 0x1

    aput-char v3, v1, v0

    .line 174769
    add-int/lit8 v0, v0, 0x1

    aput-char v3, v1, v0

    .line 174770
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/12E;->q:I

    .line 174771
    return-void
.end method

.method private l(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 174430
    invoke-direct {p0}, LX/12E;->n()V

    .line 174431
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    move v0, v1

    .line 174432
    :cond_0
    iget v2, p0, LX/12E;->r:I

    .line 174433
    add-int v4, v0, v2

    if-le v4, v3, :cond_1

    sub-int v2, v3, v0

    .line 174434
    :cond_1
    add-int v4, v0, v2

    iget-object v5, p0, LX/12E;->o:[C

    invoke-virtual {p1, v0, v4, v5, v1}, Ljava/lang/String;->getChars(II[CI)V

    .line 174435
    iget-object v4, p0, LX/12F;->k:LX/4pa;

    if-eqz v4, :cond_2

    .line 174436
    invoke-direct {p0, v2}, LX/12E;->h(I)V

    .line 174437
    :goto_0
    add-int/2addr v0, v2

    .line 174438
    if-lt v0, v3, :cond_0

    .line 174439
    return-void

    .line 174440
    :cond_2
    iget v4, p0, LX/12F;->j:I

    if-eqz v4, :cond_3

    .line 174441
    iget v4, p0, LX/12F;->j:I

    invoke-direct {p0, v2, v4}, LX/12E;->b(II)V

    goto :goto_0

    .line 174442
    :cond_3
    invoke-direct {p0, v2}, LX/12E;->f(I)V

    goto :goto_0
.end method

.method private m()[C
    .locals 5

    .prologue
    const/16 v4, 0x75

    const/16 v3, 0x30

    const/16 v2, 0x5c

    .line 174752
    const/16 v0, 0xe

    new-array v0, v0, [C

    .line 174753
    const/4 v1, 0x0

    aput-char v2, v0, v1

    .line 174754
    const/4 v1, 0x2

    aput-char v2, v0, v1

    .line 174755
    const/4 v1, 0x3

    aput-char v4, v0, v1

    .line 174756
    const/4 v1, 0x4

    aput-char v3, v0, v1

    .line 174757
    const/4 v1, 0x5

    aput-char v3, v0, v1

    .line 174758
    const/16 v1, 0x8

    aput-char v2, v0, v1

    .line 174759
    const/16 v1, 0x9

    aput-char v4, v0, v1

    .line 174760
    iput-object v0, p0, LX/12E;->s:[C

    .line 174761
    return-object v0
.end method

.method private n()V
    .locals 4

    .prologue
    .line 174195
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->p:I

    sub-int/2addr v0, v1

    .line 174196
    if-lez v0, :cond_0

    .line 174197
    iget v1, p0, LX/12E;->p:I

    .line 174198
    const/4 v2, 0x0

    iput v2, p0, LX/12E;->p:I

    iput v2, p0, LX/12E;->q:I

    .line 174199
    iget-object v2, p0, LX/12E;->n:Ljava/io/Writer;

    iget-object v3, p0, LX/12E;->o:[C

    invoke-virtual {v2, v3, v1, v0}, Ljava/io/Writer;->write([CII)V

    .line 174200
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(C)V
    .locals 3

    .prologue
    .line 174280
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_0

    .line 174281
    invoke-direct {p0}, LX/12E;->n()V

    .line 174282
    :cond_0
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char p1, v0, v1

    .line 174283
    return-void
.end method

.method public final a(D)V
    .locals 1

    .prologue
    .line 174275
    iget-boolean v0, p0, LX/12G;->d:Z

    if-nez v0, :cond_1

    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1, p2}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    sget-object v0, LX/0ls;->QUOTE_NON_NUMERIC_NUMBERS:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 174276
    :cond_1
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 174277
    :goto_0
    return-void

    .line 174278
    :cond_2
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/12E;->h(Ljava/lang/String;)V

    .line 174279
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 174270
    iget-boolean v0, p0, LX/12G;->d:Z

    if-nez v0, :cond_1

    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    sget-object v0, LX/0ls;->QUOTE_NON_NUMERIC_NUMBERS:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 174271
    :cond_1
    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 174272
    :goto_0
    return-void

    .line 174273
    :cond_2
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/12E;->h(Ljava/lang/String;)V

    .line 174274
    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 174263
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/12E;->h(Ljava/lang/String;)V

    .line 174264
    iget-boolean v0, p0, LX/12G;->d:Z

    if-eqz v0, :cond_0

    .line 174265
    invoke-direct {p0, p1, p2}, LX/12E;->b(J)V

    .line 174266
    :goto_0
    return-void

    .line 174267
    :cond_0
    iget v0, p0, LX/12E;->q:I

    add-int/lit8 v0, v0, 0x15

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_1

    .line 174268
    invoke-direct {p0}, LX/12E;->n()V

    .line 174269
    :cond_1
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    invoke-static {p1, p2, v0, v1}, LX/13I;->a(J[CI)I

    move-result v0

    iput v0, p0, LX/12E;->q:I

    goto :goto_0
.end method

.method public final a(LX/0ln;[BII)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 174254
    const-string v0, "write binary value"

    invoke-virtual {p0, v0}, LX/12E;->h(Ljava/lang/String;)V

    .line 174255
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_0

    .line 174256
    invoke-direct {p0}, LX/12E;->n()V

    .line 174257
    :cond_0
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    .line 174258
    add-int v0, p3, p4

    invoke-direct {p0, p1, p2, p3, v0}, LX/12E;->b(LX/0ln;[BII)V

    .line 174259
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_1

    .line 174260
    invoke-direct {p0}, LX/12E;->n()V

    .line 174261
    :cond_1
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    .line 174262
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 174248
    iget-object v1, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v1, p1}, LX/12U;->a(Ljava/lang/String;)I

    move-result v1

    .line 174249
    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 174250
    const-string v2, "Can not write a field name, expecting a value"

    invoke-static {v2}, LX/12G;->i(Ljava/lang/String;)V

    .line 174251
    :cond_0
    if-ne v1, v0, :cond_1

    :goto_0
    invoke-direct {p0, p1, v0}, LX/12E;->b(Ljava/lang/String;Z)V

    .line 174252
    return-void

    .line 174253
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/math/BigDecimal;)V
    .locals 1

    .prologue
    .line 174241
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/12E;->h(Ljava/lang/String;)V

    .line 174242
    if-nez p1, :cond_0

    .line 174243
    invoke-direct {p0}, LX/12E;->l()V

    .line 174244
    :goto_0
    return-void

    .line 174245
    :cond_0
    iget-boolean v0, p0, LX/12G;->d:Z

    if-eqz v0, :cond_1

    .line 174246
    invoke-direct {p0, p1}, LX/12E;->b(Ljava/lang/Object;)V

    goto :goto_0

    .line 174247
    :cond_1
    invoke-virtual {p1}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/math/BigInteger;)V
    .locals 1

    .prologue
    .line 174234
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/12E;->h(Ljava/lang/String;)V

    .line 174235
    if-nez p1, :cond_0

    .line 174236
    invoke-direct {p0}, LX/12E;->l()V

    .line 174237
    :goto_0
    return-void

    .line 174238
    :cond_0
    iget-boolean v0, p0, LX/12G;->d:Z

    if-eqz v0, :cond_1

    .line 174239
    invoke-direct {p0, p1}, LX/12E;->b(Ljava/lang/Object;)V

    goto :goto_0

    .line 174240
    :cond_1
    invoke-virtual {p1}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(S)V
    .locals 2

    .prologue
    .line 174284
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/12E;->h(Ljava/lang/String;)V

    .line 174285
    iget-boolean v0, p0, LX/12G;->d:Z

    if-eqz v0, :cond_0

    .line 174286
    invoke-direct {p0, p1}, LX/12E;->b(S)V

    .line 174287
    :goto_0
    return-void

    .line 174288
    :cond_0
    iget v0, p0, LX/12E;->q:I

    add-int/lit8 v0, v0, 0x6

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_1

    .line 174289
    invoke-direct {p0}, LX/12E;->n()V

    .line 174290
    :cond_1
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    invoke-static {p1, v0, v1}, LX/13I;->a(I[CI)I

    move-result v0

    iput v0, p0, LX/12E;->q:I

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/16 v3, 0x65

    .line 174217
    const-string v0, "write boolean value"

    invoke-virtual {p0, v0}, LX/12E;->h(Ljava/lang/String;)V

    .line 174218
    iget v0, p0, LX/12E;->q:I

    add-int/lit8 v0, v0, 0x5

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_0

    .line 174219
    invoke-direct {p0}, LX/12E;->n()V

    .line 174220
    :cond_0
    iget v0, p0, LX/12E;->q:I

    .line 174221
    iget-object v1, p0, LX/12E;->o:[C

    .line 174222
    if-eqz p1, :cond_1

    .line 174223
    const/16 v2, 0x74

    aput-char v2, v1, v0

    .line 174224
    add-int/lit8 v0, v0, 0x1

    const/16 v2, 0x72

    aput-char v2, v1, v0

    .line 174225
    add-int/lit8 v0, v0, 0x1

    const/16 v2, 0x75

    aput-char v2, v1, v0

    .line 174226
    add-int/lit8 v0, v0, 0x1

    aput-char v3, v1, v0

    .line 174227
    :goto_0
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/12E;->q:I

    .line 174228
    return-void

    .line 174229
    :cond_1
    const/16 v2, 0x66

    aput-char v2, v1, v0

    .line 174230
    add-int/lit8 v0, v0, 0x1

    const/16 v2, 0x61

    aput-char v2, v1, v0

    .line 174231
    add-int/lit8 v0, v0, 0x1

    const/16 v2, 0x6c

    aput-char v2, v1, v0

    .line 174232
    add-int/lit8 v0, v0, 0x1

    const/16 v2, 0x73

    aput-char v2, v1, v0

    .line 174233
    add-int/lit8 v0, v0, 0x1

    aput-char v3, v1, v0

    goto :goto_0
.end method

.method public final a([CII)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 174208
    const-string v0, "write text value"

    invoke-virtual {p0, v0}, LX/12E;->h(Ljava/lang/String;)V

    .line 174209
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_0

    .line 174210
    invoke-direct {p0}, LX/12E;->n()V

    .line 174211
    :cond_0
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    .line 174212
    invoke-direct {p0, p1, p2, p3}, LX/12E;->c([CII)V

    .line 174213
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_1

    .line 174214
    invoke-direct {p0}, LX/12E;->n()V

    .line 174215
    :cond_1
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    .line 174216
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 174201
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/12E;->h(Ljava/lang/String;)V

    .line 174202
    iget-boolean v0, p0, LX/12G;->d:Z

    if-eqz v0, :cond_0

    .line 174203
    invoke-direct {p0, p1}, LX/12E;->c(I)V

    .line 174204
    :goto_0
    return-void

    .line 174205
    :cond_0
    iget v0, p0, LX/12E;->q:I

    add-int/lit8 v0, v0, 0xb

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_1

    .line 174206
    invoke-direct {p0}, LX/12E;->n()V

    .line 174207
    :cond_1
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    invoke-static {p1, v0, v1}, LX/13I;->a(I[CI)I

    move-result v0

    iput v0, p0, LX/12E;->q:I

    goto :goto_0
.end method

.method public final b(LX/0lc;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 174189
    iget-object v1, p0, LX/12G;->e:LX/12U;

    invoke-interface {p1}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/12U;->a(Ljava/lang/String;)I

    move-result v1

    .line 174190
    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 174191
    const-string v2, "Can not write a field name, expecting a value"

    invoke-static {v2}, LX/12G;->i(Ljava/lang/String;)V

    .line 174192
    :cond_0
    if-ne v1, v0, :cond_1

    :goto_0
    invoke-direct {p0, p1, v0}, LX/12E;->a(LX/0lc;Z)V

    .line 174193
    return-void

    .line 174194
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 174291
    const-string v0, "write text value"

    invoke-virtual {p0, v0}, LX/12E;->h(Ljava/lang/String;)V

    .line 174292
    if-nez p1, :cond_0

    .line 174293
    invoke-direct {p0}, LX/12E;->l()V

    .line 174294
    :goto_0
    return-void

    .line 174295
    :cond_0
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_1

    .line 174296
    invoke-direct {p0}, LX/12E;->n()V

    .line 174297
    :cond_1
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    .line 174298
    invoke-direct {p0, p1}, LX/12E;->k(Ljava/lang/String;)V

    .line 174299
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_2

    .line 174300
    invoke-direct {p0}, LX/12E;->n()V

    .line 174301
    :cond_2
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v3, v0, v1

    goto :goto_0
.end method

.method public final b([CII)V
    .locals 2

    .prologue
    .line 174302
    const/16 v0, 0x20

    if-ge p3, v0, :cond_1

    .line 174303
    iget v0, p0, LX/12E;->r:I

    iget v1, p0, LX/12E;->q:I

    sub-int/2addr v0, v1

    .line 174304
    if-le p3, v0, :cond_0

    .line 174305
    invoke-direct {p0}, LX/12E;->n()V

    .line 174306
    :cond_0
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 174307
    iget v0, p0, LX/12E;->q:I

    add-int/2addr v0, p3

    iput v0, p0, LX/12E;->q:I

    .line 174308
    :goto_0
    return-void

    .line 174309
    :cond_1
    invoke-direct {p0}, LX/12E;->n()V

    .line 174310
    iget-object v0, p0, LX/12E;->n:Ljava/io/Writer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/Writer;->write([CII)V

    goto :goto_0
.end method

.method public final c(LX/0lc;)V
    .locals 6

    .prologue
    const/16 v5, 0x22

    const/4 v4, 0x0

    .line 174311
    const-string v0, "write text value"

    invoke-virtual {p0, v0}, LX/12E;->h(Ljava/lang/String;)V

    .line 174312
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_0

    .line 174313
    invoke-direct {p0}, LX/12E;->n()V

    .line 174314
    :cond_0
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v5, v0, v1

    .line 174315
    invoke-interface {p1}, LX/0lc;->c()[C

    move-result-object v0

    .line 174316
    array-length v1, v0

    .line 174317
    const/16 v2, 0x20

    if-ge v1, v2, :cond_3

    .line 174318
    iget v2, p0, LX/12E;->r:I

    iget v3, p0, LX/12E;->q:I

    sub-int/2addr v2, v3

    .line 174319
    if-le v1, v2, :cond_1

    .line 174320
    invoke-direct {p0}, LX/12E;->n()V

    .line 174321
    :cond_1
    iget-object v2, p0, LX/12E;->o:[C

    iget v3, p0, LX/12E;->q:I

    invoke-static {v0, v4, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 174322
    iget v0, p0, LX/12E;->q:I

    add-int/2addr v0, v1

    iput v0, p0, LX/12E;->q:I

    .line 174323
    :goto_0
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_2

    .line 174324
    invoke-direct {p0}, LX/12E;->n()V

    .line 174325
    :cond_2
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    aput-char v5, v0, v1

    .line 174326
    return-void

    .line 174327
    :cond_3
    invoke-direct {p0}, LX/12E;->n()V

    .line 174328
    iget-object v2, p0, LX/12E;->n:Ljava/io/Writer;

    invoke-virtual {v2, v0, v4, v1}, Ljava/io/Writer;->write([CII)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 174329
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 174330
    iget v0, p0, LX/12E;->r:I

    iget v2, p0, LX/12E;->q:I

    sub-int/2addr v0, v2

    .line 174331
    if-nez v0, :cond_0

    .line 174332
    invoke-direct {p0}, LX/12E;->n()V

    .line 174333
    iget v0, p0, LX/12E;->r:I

    iget v2, p0, LX/12E;->q:I

    sub-int/2addr v0, v2

    .line 174334
    :cond_0
    if-lt v0, v1, :cond_1

    .line 174335
    const/4 v0, 0x0

    iget-object v2, p0, LX/12E;->o:[C

    iget v3, p0, LX/12E;->q:I

    invoke-virtual {p1, v0, v1, v2, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 174336
    iget v0, p0, LX/12E;->q:I

    add-int/2addr v0, v1

    iput v0, p0, LX/12E;->q:I

    .line 174337
    :goto_0
    return-void

    .line 174338
    :cond_1
    invoke-direct {p0, p1}, LX/12E;->j(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 174339
    invoke-super {p0}, LX/12F;->close()V

    .line 174340
    iget-object v0, p0, LX/12E;->o:[C

    if-eqz v0, :cond_1

    sget-object v0, LX/0ls;->AUTO_CLOSE_JSON_CONTENT:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174341
    :goto_0
    iget-object v0, p0, LX/12G;->e:LX/12U;

    move-object v0, v0

    .line 174342
    invoke-virtual {v0}, LX/12V;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 174343
    invoke-virtual {p0}, LX/0nX;->e()V

    goto :goto_0

    .line 174344
    :cond_0
    invoke-virtual {v0}, LX/12V;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174345
    invoke-virtual {p0}, LX/0nX;->g()V

    goto :goto_0

    .line 174346
    :cond_1
    invoke-direct {p0}, LX/12E;->n()V

    .line 174347
    iget-object v0, p0, LX/12E;->n:Ljava/io/Writer;

    if-eqz v0, :cond_3

    .line 174348
    iget-object v0, p0, LX/12F;->h:LX/12A;

    .line 174349
    iget-boolean v1, v0, LX/12A;->c:Z

    move v0, v1

    .line 174350
    if-nez v0, :cond_2

    sget-object v0, LX/0ls;->AUTO_CLOSE_TARGET:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 174351
    :cond_2
    iget-object v0, p0, LX/12E;->n:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->close()V

    .line 174352
    :cond_3
    :goto_1
    invoke-virtual {p0}, LX/12E;->j()V

    .line 174353
    return-void

    .line 174354
    :cond_4
    sget-object v0, LX/0ls;->FLUSH_PASSED_TO_STREAM:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 174355
    iget-object v0, p0, LX/12E;->n:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->flush()V

    goto :goto_1
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 174356
    const-string v0, "start an array"

    invoke-virtual {p0, v0}, LX/12E;->h(Ljava/lang/String;)V

    .line 174357
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12U;->j()LX/12U;

    move-result-object v0

    iput-object v0, p0, LX/12E;->e:LX/12U;

    .line 174358
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    if-eqz v0, :cond_0

    .line 174359
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->e(LX/0nX;)V

    .line 174360
    :goto_0
    return-void

    .line 174361
    :cond_0
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_1

    .line 174362
    invoke-direct {p0}, LX/12E;->n()V

    .line 174363
    :cond_1
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    const/16 v2, 0x5b

    aput-char v2, v0, v1

    goto :goto_0
.end method

.method public final d(LX/0lc;)V
    .locals 1

    .prologue
    .line 174364
    invoke-interface {p1}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->c(Ljava/lang/String;)V

    .line 174365
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 174366
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12V;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 174367
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current context not an ARRAY but "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v1}, LX/12V;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/12G;->i(Ljava/lang/String;)V

    .line 174368
    :cond_0
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    if-eqz v0, :cond_1

    .line 174369
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    iget-object v1, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v1}, LX/12V;->f()I

    move-result v1

    invoke-interface {v0, p0, v1}, LX/0lZ;->b(LX/0nX;I)V

    .line 174370
    :goto_0
    iget-object v0, p0, LX/12G;->e:LX/12U;

    .line 174371
    iget-object v1, v0, LX/12U;->c:LX/12U;

    move-object v0, v1

    .line 174372
    iput-object v0, p0, LX/12E;->e:LX/12U;

    .line 174373
    return-void

    .line 174374
    :cond_1
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_2

    .line 174375
    invoke-direct {p0}, LX/12E;->n()V

    .line 174376
    :cond_2
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    const/16 v2, 0x5d

    aput-char v2, v0, v1

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 174377
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/12E;->h(Ljava/lang/String;)V

    .line 174378
    iget-boolean v0, p0, LX/12G;->d:Z

    if-eqz v0, :cond_0

    .line 174379
    invoke-direct {p0, p1}, LX/12E;->b(Ljava/lang/Object;)V

    .line 174380
    :goto_0
    return-void

    .line 174381
    :cond_0
    invoke-virtual {p0, p1}, LX/0nX;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 174382
    const-string v0, "start an object"

    invoke-virtual {p0, v0}, LX/12E;->h(Ljava/lang/String;)V

    .line 174383
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12U;->k()LX/12U;

    move-result-object v0

    iput-object v0, p0, LX/12E;->e:LX/12U;

    .line 174384
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    if-eqz v0, :cond_0

    .line 174385
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->b(LX/0nX;)V

    .line 174386
    :goto_0
    return-void

    .line 174387
    :cond_0
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_1

    .line 174388
    invoke-direct {p0}, LX/12E;->n()V

    .line 174389
    :cond_1
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    const/16 v2, 0x7b

    aput-char v2, v0, v1

    goto :goto_0
.end method

.method public final flush()V
    .locals 1

    .prologue
    .line 174390
    invoke-direct {p0}, LX/12E;->n()V

    .line 174391
    iget-object v0, p0, LX/12E;->n:Ljava/io/Writer;

    if-eqz v0, :cond_0

    .line 174392
    sget-object v0, LX/0ls;->FLUSH_PASSED_TO_STREAM:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174393
    iget-object v0, p0, LX/12E;->n:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->flush()V

    .line 174394
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 174395
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12V;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 174396
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current context not an object but "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v1}, LX/12V;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/12G;->i(Ljava/lang/String;)V

    .line 174397
    :cond_0
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    if-eqz v0, :cond_1

    .line 174398
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    iget-object v1, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v1}, LX/12V;->f()I

    move-result v1

    invoke-interface {v0, p0, v1}, LX/0lZ;->a(LX/0nX;I)V

    .line 174399
    :goto_0
    iget-object v0, p0, LX/12G;->e:LX/12U;

    .line 174400
    iget-object v1, v0, LX/12U;->c:LX/12U;

    move-object v0, v1

    .line 174401
    iput-object v0, p0, LX/12E;->e:LX/12U;

    .line 174402
    return-void

    .line 174403
    :cond_1
    iget v0, p0, LX/12E;->q:I

    iget v1, p0, LX/12E;->r:I

    if-lt v0, v1, :cond_2

    .line 174404
    invoke-direct {p0}, LX/12E;->n()V

    .line 174405
    :cond_2
    iget-object v0, p0, LX/12E;->o:[C

    iget v1, p0, LX/12E;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/12E;->q:I

    const/16 v2, 0x7d

    aput-char v2, v0, v1

    goto :goto_0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 174406
    const-string v0, "write null value"

    invoke-virtual {p0, v0}, LX/12E;->h(Ljava/lang/String;)V

    .line 174407
    invoke-direct {p0}, LX/12E;->l()V

    .line 174408
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 174409
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12U;->m()I

    move-result v0

    .line 174410
    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 174411
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", expecting field name"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/12G;->i(Ljava/lang/String;)V

    .line 174412
    :cond_0
    iget-object v1, p0, LX/0nX;->a:LX/0lZ;

    if-nez v1, :cond_3

    .line 174413
    packed-switch v0, :pswitch_data_0

    .line 174414
    :cond_1
    :goto_0
    return-void

    .line 174415
    :pswitch_0
    const/16 v0, 0x2c

    .line 174416
    :goto_1
    iget v1, p0, LX/12E;->q:I

    iget v2, p0, LX/12E;->r:I

    if-lt v1, v2, :cond_2

    .line 174417
    invoke-direct {p0}, LX/12E;->n()V

    .line 174418
    :cond_2
    iget-object v1, p0, LX/12E;->o:[C

    iget v2, p0, LX/12E;->q:I

    aput-char v0, v1, v2

    .line 174419
    iget v0, p0, LX/12E;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/12E;->q:I

    goto :goto_0

    .line 174420
    :pswitch_1
    const/16 v0, 0x3a

    .line 174421
    goto :goto_1

    .line 174422
    :pswitch_2
    iget-object v0, p0, LX/12F;->l:LX/0lc;

    if-eqz v0, :cond_1

    .line 174423
    iget-object v0, p0, LX/12F;->l:LX/0lc;

    invoke-interface {v0}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 174424
    :cond_3
    invoke-direct {p0, v0}, LX/12E;->d(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 174425
    iget-object v0, p0, LX/12E;->o:[C

    .line 174426
    if-eqz v0, :cond_0

    .line 174427
    const/4 v1, 0x0

    iput-object v1, p0, LX/12E;->o:[C

    .line 174428
    iget-object v1, p0, LX/12F;->h:LX/12A;

    invoke-virtual {v1, v0}, LX/12A;->b([C)V

    .line 174429
    :cond_0
    return-void
.end method
