.class public LX/0t5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0t5;


# instance fields
.field public final a:Ljava/util/WeakHashMap;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/facebook/graphql/executor/iface/GraphQLObservablePusher;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 153531
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153532
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/0t5;->a:Ljava/util/WeakHashMap;

    .line 153533
    return-void
.end method

.method public static a(LX/0QB;)LX/0t5;
    .locals 3

    .prologue
    .line 153534
    sget-object v0, LX/0t5;->b:LX/0t5;

    if-nez v0, :cond_1

    .line 153535
    const-class v1, LX/0t5;

    monitor-enter v1

    .line 153536
    :try_start_0
    sget-object v0, LX/0t5;->b:LX/0t5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 153537
    if-eqz v2, :cond_0

    .line 153538
    :try_start_1
    new-instance v0, LX/0t5;

    invoke-direct {v0}, LX/0t5;-><init>()V

    .line 153539
    move-object v0, v0

    .line 153540
    sput-object v0, LX/0t5;->b:LX/0t5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 153541
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 153542
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 153543
    :cond_1
    sget-object v0, LX/0t5;->b:LX/0t5;

    return-object v0

    .line 153544
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 153545
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153546
    monitor-enter p0

    .line 153547
    :try_start_0
    iget-object v0, p0, LX/0t5;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    .line 153548
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153549
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 153550
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    .line 153551
    instance-of v3, v0, LX/1My;

    if-eqz v3, :cond_0

    .line 153552
    check-cast v0, LX/1My;

    invoke-virtual {v0, v1}, LX/1My;->a(LX/0Pz;)V

    goto :goto_0

    .line 153553
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 153554
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 153555
    const/4 v0, 0x3

    invoke-virtual {p0, p1, p2, v0}, LX/0t5;->a(ILjava/util/Set;I)V

    .line 153556
    return-void
.end method

.method public final a(ILjava/util/Set;I)V
    .locals 2
    .param p3    # I
        .annotation build Lcom/facebook/graphql/executor/iface/GraphQLObservablePusher$SubscriptionStore;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 153557
    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153558
    :cond_0
    return-void

    .line 153559
    :cond_1
    monitor-enter p0

    .line 153560
    :try_start_0
    iget-object v0, p0, LX/0t5;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    .line 153561
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153562
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    .line 153563
    invoke-virtual {v0, p1, p2, p3}, LX/1My;->a(ILjava/util/Set;I)V

    goto :goto_0

    .line 153564
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized a(LX/1My;)V
    .locals 2

    .prologue
    .line 153565
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0t5;->a:Ljava/util/WeakHashMap;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p1, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153566
    monitor-exit p0

    return-void

    .line 153567
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/1My;)V
    .locals 1

    .prologue
    .line 153568
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0t5;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153569
    monitor-exit p0

    return-void

    .line 153570
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
