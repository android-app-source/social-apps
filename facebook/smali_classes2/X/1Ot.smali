.class public final LX/1Ot;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ou;


# instance fields
.field public final synthetic a:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 242559
    iput-object p1, p0, LX/1Ot;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 242558
    iget-object v0, p0, LX/1Ot;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 242557
    iget-object v0, p0, LX/1Ot;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 242552
    iget-object v0, p0, LX/1Ot;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 242553
    if-eqz v0, :cond_0

    .line 242554
    iget-object v1, p0, LX/1Ot;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1, v0}, Landroid/support/v7/widget/RecyclerView;->i(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V

    .line 242555
    :cond_0
    iget-object v0, p0, LX/1Ot;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->removeViewAt(I)V

    .line 242556
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 242549
    iget-object v0, p0, LX/1Ot;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->addView(Landroid/view/View;I)V

    .line 242550
    iget-object v0, p0, LX/1Ot;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, p1}, Landroid/support/v7/widget/RecyclerView;->j(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V

    .line 242551
    return-void
.end method

.method public final a(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 4

    .prologue
    .line 242517
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 242518
    if-eqz v0, :cond_1

    .line 242519
    invoke-virtual {v0}, LX/1a1;->r()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, LX/1a1;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 242520
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Called attach on a child which is not detached: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 242521
    :cond_0
    invoke-virtual {v0}, LX/1a1;->l()V

    .line 242522
    :cond_1
    iget-object v0, p0, LX/1Ot;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 242523
    return-void
.end method

.method public final b(Landroid/view/View;)LX/1a1;
    .locals 1

    .prologue
    .line 242548
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 242547
    iget-object v0, p0, LX/1Ot;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 242541
    invoke-virtual {p0}, LX/1Ot;->a()I

    move-result v1

    .line 242542
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 242543
    iget-object v2, p0, LX/1Ot;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v0}, LX/1Ot;->b(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/support/v7/widget/RecyclerView;->i(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V

    .line 242544
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 242545
    :cond_0
    iget-object v0, p0, LX/1Ot;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->removeAllViews()V

    .line 242546
    return-void
.end method

.method public final c(I)V
    .locals 4

    .prologue
    .line 242532
    invoke-virtual {p0, p1}, LX/1Ot;->b(I)Landroid/view/View;

    move-result-object v0

    .line 242533
    if-eqz v0, :cond_1

    .line 242534
    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 242535
    if-eqz v0, :cond_1

    .line 242536
    invoke-virtual {v0}, LX/1a1;->r()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/1a1;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 242537
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "called detach on an already detached child "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 242538
    :cond_0
    const/16 v1, 0x100

    invoke-virtual {v0, v1}, LX/1a1;->b(I)V

    .line 242539
    :cond_1
    iget-object v0, p0, LX/1Ot;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView;I)V

    .line 242540
    return-void
.end method

.method public final c(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 242528
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 242529
    if-eqz v0, :cond_0

    .line 242530
    invoke-static {v0}, LX/1a1;->y(LX/1a1;)V

    .line 242531
    :cond_0
    return-void
.end method

.method public final d(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 242524
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 242525
    if-eqz v0, :cond_0

    .line 242526
    invoke-static {v0}, LX/1a1;->z(LX/1a1;)V

    .line 242527
    :cond_0
    return-void
.end method
