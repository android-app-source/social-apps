.class public abstract LX/0m3;
.super LX/0m4;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CFG::",
        "LX/0m7;",
        "T:",
        "LX/0m3",
        "<TCFG;TT;>;>",
        "LX/0m4",
        "<TT;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final a:I

.field private static final serialVersionUID:J = -0x744574246f52876fL


# instance fields
.field public final _mixInAnnotations:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1Xc;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final _rootName:Ljava/lang/String;

.field public final _subtypeResolver:LX/0m0;

.field public final _view:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 131616
    const-class v0, LX/0m6;

    invoke-static {v0}, LX/0m4;->a(Ljava/lang/Class;)I

    move-result v0

    sput v0, LX/0m3;->a:I

    return-void
.end method

.method public constructor <init>(LX/0lh;LX/0m0;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lh;",
            "LX/0m0;",
            "Ljava/util/Map",
            "<",
            "LX/1Xc;",
            "Ljava/lang/Class",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 131623
    sget v0, LX/0m3;->a:I

    invoke-direct {p0, p1, v0}, LX/0m4;-><init>(LX/0lh;I)V

    .line 131624
    iput-object p3, p0, LX/0m3;->_mixInAnnotations:Ljava/util/Map;

    .line 131625
    iput-object p2, p0, LX/0m3;->_subtypeResolver:LX/0m0;

    .line 131626
    iput-object v1, p0, LX/0m3;->_rootName:Ljava/lang/String;

    .line 131627
    iput-object v1, p0, LX/0m3;->_view:Ljava/lang/Class;

    .line 131628
    return-void
.end method

.method public constructor <init>(LX/0m3;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m3",
            "<TCFG;TT;>;)V"
        }
    .end annotation

    .prologue
    .line 131617
    invoke-direct {p0, p1}, LX/0m4;-><init>(LX/0m4;)V

    .line 131618
    iget-object v0, p1, LX/0m3;->_mixInAnnotations:Ljava/util/Map;

    iput-object v0, p0, LX/0m3;->_mixInAnnotations:Ljava/util/Map;

    .line 131619
    iget-object v0, p1, LX/0m3;->_subtypeResolver:LX/0m0;

    iput-object v0, p0, LX/0m3;->_subtypeResolver:LX/0m0;

    .line 131620
    iget-object v0, p1, LX/0m3;->_rootName:Ljava/lang/String;

    iput-object v0, p0, LX/0m3;->_rootName:Ljava/lang/String;

    .line 131621
    iget-object v0, p1, LX/0m3;->_view:Ljava/lang/Class;

    iput-object v0, p0, LX/0m3;->_view:Ljava/lang/Class;

    .line 131622
    return-void
.end method

.method public constructor <init>(LX/0m3;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m3",
            "<TCFG;TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 131629
    iget-object v0, p1, LX/0m4;->_base:LX/0lh;

    invoke-direct {p0, v0, p2}, LX/0m4;-><init>(LX/0lh;I)V

    .line 131630
    iget-object v0, p1, LX/0m3;->_mixInAnnotations:Ljava/util/Map;

    iput-object v0, p0, LX/0m3;->_mixInAnnotations:Ljava/util/Map;

    .line 131631
    iget-object v0, p1, LX/0m3;->_subtypeResolver:LX/0m0;

    iput-object v0, p0, LX/0m3;->_subtypeResolver:LX/0m0;

    .line 131632
    iget-object v0, p1, LX/0m3;->_rootName:Ljava/lang/String;

    iput-object v0, p0, LX/0m3;->_rootName:Ljava/lang/String;

    .line 131633
    iget-object v0, p1, LX/0m3;->_view:Ljava/lang/Class;

    iput-object v0, p0, LX/0m3;->_view:Ljava/lang/Class;

    .line 131634
    return-void
.end method

.method public constructor <init>(LX/0m3;LX/0lh;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m3",
            "<TCFG;TT;>;",
            "LX/0lh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 131609
    iget v0, p1, LX/0m4;->_mapperFeatures:I

    invoke-direct {p0, p2, v0}, LX/0m4;-><init>(LX/0lh;I)V

    .line 131610
    iget-object v0, p1, LX/0m3;->_mixInAnnotations:Ljava/util/Map;

    iput-object v0, p0, LX/0m3;->_mixInAnnotations:Ljava/util/Map;

    .line 131611
    iget-object v0, p1, LX/0m3;->_subtypeResolver:LX/0m0;

    iput-object v0, p0, LX/0m3;->_subtypeResolver:LX/0m0;

    .line 131612
    iget-object v0, p1, LX/0m3;->_rootName:Ljava/lang/String;

    iput-object v0, p0, LX/0m3;->_rootName:Ljava/lang/String;

    .line 131613
    iget-object v0, p1, LX/0m3;->_view:Ljava/lang/Class;

    iput-object v0, p0, LX/0m3;->_view:Ljava/lang/Class;

    .line 131614
    return-void
.end method


# virtual methods
.method public final d(Ljava/lang/Class;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 131615
    iget-object v0, p0, LX/0m3;->_mixInAnnotations:Ljava/util/Map;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0m3;->_mixInAnnotations:Ljava/util/Map;

    new-instance v1, LX/1Xc;

    invoke-direct {v1, p1}, LX/1Xc;-><init>(Ljava/lang/Class;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    goto :goto_0
.end method
