.class public LX/0Zn;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0a8;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0a8;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83880
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0a8;
    .locals 4

    .prologue
    .line 83882
    sget-object v0, LX/0Zn;->a:LX/0a8;

    if-nez v0, :cond_1

    .line 83883
    const-class v1, LX/0Zn;

    monitor-enter v1

    .line 83884
    :try_start_0
    sget-object v0, LX/0Zn;->a:LX/0a8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 83885
    if-eqz v2, :cond_0

    .line 83886
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 83887
    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/Executor;

    invoke-static {v3, p0}, LX/0a7;->a(LX/0Uh;Ljava/util/concurrent/Executor;)LX/0a8;

    move-result-object v3

    move-object v0, v3

    .line 83888
    sput-object v0, LX/0Zn;->a:LX/0a8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83889
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 83890
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 83891
    :cond_1
    sget-object v0, LX/0Zn;->a:LX/0a8;

    return-object v0

    .line 83892
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 83893
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 83881
    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {v0, v1}, LX/0a7;->a(LX/0Uh;Ljava/util/concurrent/Executor;)LX/0a8;

    move-result-object v0

    return-object v0
.end method
