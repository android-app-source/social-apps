.class public final enum LX/0ku;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0ku;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0ku;

.field public static final enum WIFI_OFF:LX/0ku;

.field public static final enum WIFI_ON:LX/0ku;

.field public static final enum WIFI_UNKNOWN:LX/0ku;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 127930
    new-instance v0, LX/0ku;

    const-string v1, "WIFI_ON"

    invoke-direct {v0, v1, v2}, LX/0ku;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ku;->WIFI_ON:LX/0ku;

    .line 127931
    new-instance v0, LX/0ku;

    const-string v1, "WIFI_OFF"

    invoke-direct {v0, v1, v3}, LX/0ku;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ku;->WIFI_OFF:LX/0ku;

    .line 127932
    new-instance v0, LX/0ku;

    const-string v1, "WIFI_UNKNOWN"

    invoke-direct {v0, v1, v4}, LX/0ku;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ku;->WIFI_UNKNOWN:LX/0ku;

    .line 127933
    const/4 v0, 0x3

    new-array v0, v0, [LX/0ku;

    sget-object v1, LX/0ku;->WIFI_ON:LX/0ku;

    aput-object v1, v0, v2

    sget-object v1, LX/0ku;->WIFI_OFF:LX/0ku;

    aput-object v1, v0, v3

    sget-object v1, LX/0ku;->WIFI_UNKNOWN:LX/0ku;

    aput-object v1, v0, v4

    sput-object v0, LX/0ku;->$VALUES:[LX/0ku;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 127929
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0ku;
    .locals 1

    .prologue
    .line 127927
    const-class v0, LX/0ku;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0ku;

    return-object v0
.end method

.method public static values()[LX/0ku;
    .locals 1

    .prologue
    .line 127928
    sget-object v0, LX/0ku;->$VALUES:[LX/0ku;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0ku;

    return-object v0
.end method
