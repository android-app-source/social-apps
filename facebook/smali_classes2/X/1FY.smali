.class public LX/1FY;
.super LX/1FZ;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/1Fi;

.field private final b:LX/1FG;


# direct methods
.method public constructor <init>(LX/1Fi;LX/1FG;)V
    .locals 0

    .prologue
    .line 222583
    invoke-direct {p0}, LX/1FZ;-><init>()V

    .line 222584
    iput-object p1, p0, LX/1FY;->a:LX/1Fi;

    .line 222585
    iput-object p2, p0, LX/1FY;->b:LX/1FG;

    .line 222586
    return-void
.end method


# virtual methods
.method public final b(IILandroid/graphics/Bitmap$Config;)LX/1FJ;
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/graphics/Bitmap$Config;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222587
    iget-object v0, p0, LX/1FY;->a:LX/1Fi;

    int-to-short v1, p1

    int-to-short v2, p2

    invoke-virtual {v0, v1, v2}, LX/1Fi;->a(SS)LX/1FJ;

    move-result-object v1

    .line 222588
    :try_start_0
    new-instance v2, LX/1FL;

    invoke-direct {v2, v1}, LX/1FL;-><init>(LX/1FJ;)V

    .line 222589
    sget-object v0, LX/1ld;->a:LX/1lW;

    .line 222590
    iput-object v0, v2, LX/1FL;->c:LX/1lW;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 222591
    :try_start_1
    iget-object v3, p0, LX/1FY;->b:LX/1FG;

    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    invoke-virtual {v0}, LX/1FK;->a()I

    move-result v0

    invoke-interface {v3, v2, p3, v0}, LX/1FG;->a(LX/1FL;Landroid/graphics/Bitmap$Config;I)LX/1FJ;

    move-result-object v3

    .line 222592
    invoke-virtual {v3}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 222593
    invoke-virtual {v3}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 222594
    :try_start_2
    invoke-static {v2}, LX/1FL;->d(LX/1FL;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 222595
    invoke-virtual {v1}, LX/1FJ;->close()V

    return-object v3

    .line 222596
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-static {v2}, LX/1FL;->d(LX/1FL;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 222597
    :catchall_1
    move-exception v0

    invoke-virtual {v1}, LX/1FJ;->close()V

    throw v0
.end method
