.class public final enum LX/1lq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1lq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1lq;

.field public static final enum AvoidNewStoryPill:LX/1lq;

.field public static final enum HideLoadingIndicator:LX/1lq;

.field public static final enum HideNSBP:LX/1lq;

.field public static final enum HideNSBPIfNotFullyLoaded:LX/1lq;

.field public static final enum ShowNSBPFullyLoadedText:LX/1lq;

.field public static final enum ShowNSBPLoadingIndicator:LX/1lq;

.field public static final enum ShowNewStoryPill:LX/1lq;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 312401
    new-instance v0, LX/1lq;

    const-string v1, "AvoidNewStoryPill"

    invoke-direct {v0, v1, v3}, LX/1lq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lq;->AvoidNewStoryPill:LX/1lq;

    .line 312402
    new-instance v0, LX/1lq;

    const-string v1, "ShowNewStoryPill"

    invoke-direct {v0, v1, v4}, LX/1lq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lq;->ShowNewStoryPill:LX/1lq;

    .line 312403
    new-instance v0, LX/1lq;

    const-string v1, "HideLoadingIndicator"

    invoke-direct {v0, v1, v5}, LX/1lq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lq;->HideLoadingIndicator:LX/1lq;

    .line 312404
    new-instance v0, LX/1lq;

    const-string v1, "ShowNSBPLoadingIndicator"

    invoke-direct {v0, v1, v6}, LX/1lq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lq;->ShowNSBPLoadingIndicator:LX/1lq;

    .line 312405
    new-instance v0, LX/1lq;

    const-string v1, "ShowNSBPFullyLoadedText"

    invoke-direct {v0, v1, v7}, LX/1lq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lq;->ShowNSBPFullyLoadedText:LX/1lq;

    .line 312406
    new-instance v0, LX/1lq;

    const-string v1, "HideNSBPIfNotFullyLoaded"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/1lq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lq;->HideNSBPIfNotFullyLoaded:LX/1lq;

    .line 312407
    new-instance v0, LX/1lq;

    const-string v1, "HideNSBP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/1lq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lq;->HideNSBP:LX/1lq;

    .line 312408
    const/4 v0, 0x7

    new-array v0, v0, [LX/1lq;

    sget-object v1, LX/1lq;->AvoidNewStoryPill:LX/1lq;

    aput-object v1, v0, v3

    sget-object v1, LX/1lq;->ShowNewStoryPill:LX/1lq;

    aput-object v1, v0, v4

    sget-object v1, LX/1lq;->HideLoadingIndicator:LX/1lq;

    aput-object v1, v0, v5

    sget-object v1, LX/1lq;->ShowNSBPLoadingIndicator:LX/1lq;

    aput-object v1, v0, v6

    sget-object v1, LX/1lq;->ShowNSBPFullyLoadedText:LX/1lq;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/1lq;->HideNSBPIfNotFullyLoaded:LX/1lq;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1lq;->HideNSBP:LX/1lq;

    aput-object v2, v0, v1

    sput-object v0, LX/1lq;->$VALUES:[LX/1lq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 312409
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1lq;
    .locals 1

    .prologue
    .line 312410
    const-class v0, LX/1lq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1lq;

    return-object v0
.end method

.method public static values()[LX/1lq;
    .locals 1

    .prologue
    .line 312411
    sget-object v0, LX/1lq;->$VALUES:[LX/1lq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1lq;

    return-object v0
.end method


# virtual methods
.method public final avoidNewStoryPill()Z
    .locals 1

    .prologue
    .line 312412
    sget-object v0, LX/1lq;->AvoidNewStoryPill:LX/1lq;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final showNewStoryPill()Z
    .locals 1

    .prologue
    .line 312413
    sget-object v0, LX/1lq;->ShowNewStoryPill:LX/1lq;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
