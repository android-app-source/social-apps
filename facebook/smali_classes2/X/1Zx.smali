.class public LX/1Zx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1Zx;


# instance fields
.field public a:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 275703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1Zx;
    .locals 4

    .prologue
    .line 275704
    sget-object v0, LX/1Zx;->b:LX/1Zx;

    if-nez v0, :cond_1

    .line 275705
    const-class v1, LX/1Zx;

    monitor-enter v1

    .line 275706
    :try_start_0
    sget-object v0, LX/1Zx;->b:LX/1Zx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 275707
    if-eqz v2, :cond_0

    .line 275708
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 275709
    new-instance p0, LX/1Zx;

    invoke-direct {p0}, LX/1Zx;-><init>()V

    .line 275710
    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    .line 275711
    iput-object v3, p0, LX/1Zx;->a:LX/0Zb;

    .line 275712
    move-object v0, p0

    .line 275713
    sput-object v0, LX/1Zx;->b:LX/1Zx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 275714
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 275715
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 275716
    :cond_1
    sget-object v0, LX/1Zx;->b:LX/1Zx;

    return-object v0

    .line 275717
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 275718
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
