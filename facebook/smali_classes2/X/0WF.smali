.class public LX/0WF;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Landroid/content/pm/PackageManager;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 75649
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Landroid/content/pm/PackageManager;
    .locals 3

    .prologue
    .line 75650
    sget-object v0, LX/0WF;->a:Landroid/content/pm/PackageManager;

    if-nez v0, :cond_1

    .line 75651
    const-class v1, LX/0WF;

    monitor-enter v1

    .line 75652
    :try_start_0
    sget-object v0, LX/0WF;->a:Landroid/content/pm/PackageManager;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 75653
    if-eqz v2, :cond_0

    .line 75654
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 75655
    const-class p0, Landroid/content/Context;

    invoke-interface {v0, p0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    invoke-static {p0}, LX/0VV;->w(Landroid/content/Context;)Landroid/content/pm/PackageManager;

    move-result-object p0

    move-object v0, p0

    .line 75656
    sput-object v0, LX/0WF;->a:Landroid/content/pm/PackageManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75657
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 75658
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 75659
    :cond_1
    sget-object v0, LX/0WF;->a:Landroid/content/pm/PackageManager;

    return-object v0

    .line 75660
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 75661
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 75662
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, LX/0VV;->w(Landroid/content/Context;)Landroid/content/pm/PackageManager;

    move-result-object v0

    return-object v0
.end method
