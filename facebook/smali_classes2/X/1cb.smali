.class public final enum LX/1cb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1cb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1cb;

.field public static final enum FAILURE:LX/1cb;

.field public static final enum IN_PROGRESS:LX/1cb;

.field public static final enum SUCCESS:LX/1cb;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 282325
    new-instance v0, LX/1cb;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v2}, LX/1cb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1cb;->IN_PROGRESS:LX/1cb;

    .line 282326
    new-instance v0, LX/1cb;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3}, LX/1cb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1cb;->SUCCESS:LX/1cb;

    .line 282327
    new-instance v0, LX/1cb;

    const-string v1, "FAILURE"

    invoke-direct {v0, v1, v4}, LX/1cb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1cb;->FAILURE:LX/1cb;

    .line 282328
    const/4 v0, 0x3

    new-array v0, v0, [LX/1cb;

    sget-object v1, LX/1cb;->IN_PROGRESS:LX/1cb;

    aput-object v1, v0, v2

    sget-object v1, LX/1cb;->SUCCESS:LX/1cb;

    aput-object v1, v0, v3

    sget-object v1, LX/1cb;->FAILURE:LX/1cb;

    aput-object v1, v0, v4

    sput-object v0, LX/1cb;->$VALUES:[LX/1cb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 282329
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1cb;
    .locals 1

    .prologue
    .line 282330
    const-class v0, LX/1cb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1cb;

    return-object v0
.end method

.method public static values()[LX/1cb;
    .locals 1

    .prologue
    .line 282331
    sget-object v0, LX/1cb;->$VALUES:[LX/1cb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1cb;

    return-object v0
.end method
