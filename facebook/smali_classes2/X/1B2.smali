.class public LX/1B2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1B3;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile v:LX/1B2;


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1B5;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0pe;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private final p:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation
.end field

.field public final q:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field public final r:LX/1B4;

.field public final s:LX/0ad;

.field private t:Ljava/lang/Boolean;

.field public u:Ljava/lang/Long;


# direct methods
.method public constructor <init>(LX/0SG;LX/0Ot;LX/0pe;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;Landroid/os/Handler;LX/0Or;LX/0ad;)V
    .locals 1
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsSponsoredLoggingInsertionEnabled;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsSponsoredLoggingViewabilityEnabled;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsSponsoredLoggingNonViewabilityEnabled;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsSponsoredLoggingViewabilityDurationEnabled;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsSponsoredLoggingViewabilityPercentEnabled;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsSponsoredFullViewLoggingEnabled;
        .end annotation
    .end param
    .param p10    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "LX/1B5;",
            ">;",
            "LX/0pe;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/os/Handler;",
            "LX/0Or",
            "<",
            "LX/1B4;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 211739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211740
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1B2;->q:Ljava/util/Map;

    .line 211741
    iput-object p1, p0, LX/1B2;->a:LX/0SG;

    .line 211742
    iput-object p2, p0, LX/1B2;->b:LX/0Ot;

    .line 211743
    iput-object p3, p0, LX/1B2;->c:LX/0pe;

    .line 211744
    iput-object p4, p0, LX/1B2;->d:LX/0Or;

    .line 211745
    iput-object p5, p0, LX/1B2;->e:LX/0Or;

    .line 211746
    iput-object p6, p0, LX/1B2;->f:LX/0Or;

    .line 211747
    iput-object p7, p0, LX/1B2;->g:LX/0Or;

    .line 211748
    iput-object p8, p0, LX/1B2;->h:LX/0Or;

    .line 211749
    iput-object p9, p0, LX/1B2;->i:LX/0Or;

    .line 211750
    iput-object p10, p0, LX/1B2;->p:Landroid/os/Handler;

    .line 211751
    invoke-interface {p11}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1B4;

    iput-object v0, p0, LX/1B2;->r:LX/1B4;

    .line 211752
    invoke-virtual {p0}, LX/1B2;->a()V

    .line 211753
    iput-object p12, p0, LX/1B2;->s:LX/0ad;

    .line 211754
    return-void
.end method

.method private static a(LX/0g8;LX/1XQ;ILcom/facebook/graphql/model/FeedUnit;Landroid/view/View;)I
    .locals 3
    .param p1    # LX/1XQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 211755
    invoke-virtual {p4}, Landroid/view/View;->getTop()I

    move-result v0

    .line 211756
    if-eqz p1, :cond_0

    if-eqz p2, :cond_1

    .line 211757
    :cond_0
    :goto_0
    return v0

    .line 211758
    :cond_1
    invoke-interface {p0}, LX/0g8;->q()I

    move-result v1

    .line 211759
    add-int/2addr v1, p2

    const/4 v2, 0x0

    .line 211760
    if-nez p3, :cond_3

    .line 211761
    :cond_2
    :goto_1
    move v1, v2

    .line 211762
    sub-int/2addr v0, v1

    goto :goto_0

    .line 211763
    :cond_3
    invoke-static {p1, v1}, LX/1XQ;->b(LX/1XQ;I)I

    move-result p0

    .line 211764
    iget-object p2, p1, LX/1XQ;->b:LX/1UQ;

    invoke-interface {p2, p0}, LX/1Qr;->g(I)I

    move-result p2

    .line 211765
    iget-object p4, p1, LX/1XQ;->b:LX/1UQ;

    invoke-interface {p4, p0}, LX/1Qr;->c(I)I

    move-result p0

    .line 211766
    iget-object p4, p1, LX/1XQ;->h:LX/1XS;

    .line 211767
    iget-object p1, p4, LX/1XS;->b:Ljava/util/Map;

    invoke-interface {p1, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/1fx;

    .line 211768
    invoke-static {p4, p3, p1, p0}, LX/1XS;->a(LX/1XS;Lcom/facebook/graphql/model/FeedUnit;LX/1fx;I)Z

    move-result v1

    .line 211769
    if-eqz p1, :cond_4

    if-eqz v1, :cond_5

    :cond_4
    const/4 p1, -0x1

    :goto_2
    move p0, p1

    .line 211770
    const/4 p2, -0x1

    if-eq p0, p2, :cond_2

    move v2, p0

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    .line 211771
    invoke-static {p1}, LX/1fx;->c(LX/1fx;)Z

    move-result p4

    if-nez p4, :cond_7

    .line 211772
    const/4 v1, -0x1

    .line 211773
    :cond_6
    move p1, v1

    .line 211774
    goto :goto_2

    :cond_7
    move p4, v1

    .line 211775
    :goto_3
    if-ge p4, p2, :cond_6

    .line 211776
    invoke-static {p1, p4}, LX/1fx;->c(LX/1fx;I)I

    move-result p3

    add-int/2addr p3, v1

    .line 211777
    add-int/lit8 v1, p4, 0x1

    move p4, v1

    move v1, p3

    goto :goto_3
.end method

.method public static a(LX/0QB;)LX/1B2;
    .locals 3

    .prologue
    .line 211778
    sget-object v0, LX/1B2;->v:LX/1B2;

    if-nez v0, :cond_1

    .line 211779
    const-class v1, LX/1B2;

    monitor-enter v1

    .line 211780
    :try_start_0
    sget-object v0, LX/1B2;->v:LX/1B2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 211781
    if-eqz v2, :cond_0

    .line 211782
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1B2;->b(LX/0QB;)LX/1B2;

    move-result-object v0

    sput-object v0, LX/1B2;->v:LX/1B2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211783
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 211784
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 211785
    :cond_1
    sget-object v0, LX/1B2;->v:LX/1B2;

    return-object v0

    .line 211786
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 211787
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/graphql/model/Sponsorable;)V
    .locals 13

    .prologue
    const/4 v6, 0x0

    .line 211788
    if-nez p1, :cond_1

    .line 211789
    :cond_0
    :goto_0
    return-void

    .line 211790
    :cond_1
    invoke-static {p1}, LX/18J;->a(Lcom/facebook/graphql/model/Sponsorable;)Lcom/facebook/graphql/model/BaseImpression;

    move-result-object v1

    .line 211791
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/BaseImpression;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211792
    iget-object v0, p0, LX/1B2;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    const/4 v8, 0x0

    .line 211793
    iget-boolean v7, v1, Lcom/facebook/graphql/model/BaseImpression;->e:Z

    if-nez v7, :cond_2

    .line 211794
    iput-boolean v8, v1, Lcom/facebook/graphql/model/BaseImpression;->d:Z

    .line 211795
    :cond_2
    iput-boolean v8, v1, Lcom/facebook/graphql/model/BaseImpression;->e:Z

    .line 211796
    iput-wide v2, v1, Lcom/facebook/graphql/model/BaseImpression;->h:J

    .line 211797
    const-string v7, ""

    iput-object v7, v1, Lcom/facebook/graphql/model/BaseImpression;->i:Ljava/lang/String;

    .line 211798
    iget-boolean v0, p0, LX/1B2;->j:Z

    if-eqz v0, :cond_4

    .line 211799
    invoke-direct {p0}, LX/1B2;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 211800
    iget-object v0, p0, LX/1B2;->q:Ljava/util/Map;

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 211801
    if-nez v0, :cond_4

    .line 211802
    new-instance v0, Lcom/facebook/feed/logging/ViewBasedLoggingHandler$DelayedImpressionLogger;

    invoke-direct {v0, p0, p1, v1}, Lcom/facebook/feed/logging/ViewBasedLoggingHandler$DelayedImpressionLogger;-><init>(LX/1B2;Lcom/facebook/graphql/model/Sponsorable;Lcom/facebook/graphql/model/BaseImpression;)V

    .line 211803
    iget-object v2, p0, LX/1B2;->p:Landroid/os/Handler;

    .line 211804
    iget-object v7, p0, LX/1B2;->u:Ljava/lang/Long;

    if-nez v7, :cond_3

    .line 211805
    iget-object v7, p0, LX/1B2;->s:LX/0ad;

    sget-wide v9, LX/3E9;->b:J

    const-wide/16 v11, 0x0

    invoke-interface {v7, v9, v10, v11, v12}, LX/0ad;->a(JJ)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, p0, LX/1B2;->u:Ljava/lang/Long;

    .line 211806
    :cond_3
    iget-object v7, p0, LX/1B2;->u:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    move-wide v4, v7

    .line 211807
    const v3, -0x168e7ecd

    invoke-static {v2, v0, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 211808
    iget-object v2, p0, LX/1B2;->q:Ljava/util/Map;

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211809
    :cond_4
    :goto_1
    iget-boolean v0, p0, LX/1B2;->k:Z

    if-eqz v0, :cond_0

    .line 211810
    iget-object v0, p0, LX/1B2;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v1, v6, v2, v3, v6}, Lcom/facebook/graphql/model/BaseImpression;->a(IJZ)Z

    goto :goto_0

    .line 211811
    :cond_5
    invoke-static {p0, p1, v1}, LX/1B2;->a$redex0(LX/1B2;Lcom/facebook/graphql/model/Sponsorable;Lcom/facebook/graphql/model/BaseImpression;)V

    goto :goto_1
.end method

.method private a(Ljava/lang/Object;I)V
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 211826
    invoke-static {p1}, LX/18J;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/Sponsorable;

    move-result-object v1

    .line 211827
    if-nez v1, :cond_1

    .line 211828
    :cond_0
    :goto_0
    return-void

    .line 211829
    :cond_1
    invoke-static {v1}, LX/18J;->a(Lcom/facebook/graphql/model/Sponsorable;)Lcom/facebook/graphql/model/BaseImpression;

    move-result-object v2

    .line 211830
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/BaseImpression;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211831
    iget-boolean v0, p0, LX/1B2;->m:Z

    if-eqz v0, :cond_2

    invoke-virtual {v2, p2}, Lcom/facebook/graphql/model/BaseImpression;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 211832
    iget-object v0, p0, LX/1B2;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/facebook/graphql/model/BaseImpression;->b(J)J

    move-result-wide v4

    .line 211833
    iget-object v0, p0, LX/1B2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1B5;

    invoke-virtual {v0, v1, v4, v5}, LX/1B5;->a(Lcom/facebook/graphql/model/Sponsorable;J)V

    .line 211834
    :cond_2
    iget-boolean v0, p0, LX/1B2;->k:Z

    if-eqz v0, :cond_3

    .line 211835
    iget-object v0, p0, LX/1B2;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    const/4 v0, 0x0

    invoke-virtual {v2, p2, v4, v5, v0}, Lcom/facebook/graphql/model/BaseImpression;->a(IJZ)Z

    move-result v0

    .line 211836
    if-eqz v0, :cond_3

    .line 211837
    iget-object v0, p0, LX/1B2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1B5;

    invoke-virtual {v0, v1}, LX/1B5;->c(Lcom/facebook/graphql/model/Sponsorable;)V

    .line 211838
    :cond_3
    iget-boolean v0, p0, LX/1B2;->n:Z

    if-eqz v0, :cond_0

    .line 211839
    invoke-interface {v1}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    iget-object v1, p0, LX/1B2;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 211840
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v0, Lcom/facebook/graphql/model/BaseImpression;->i:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 211841
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 211842
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/BaseImpression;->i:Ljava/lang/String;

    .line 211843
    goto/16 :goto_0
.end method

.method private a(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 10

    .prologue
    .line 211812
    iget-object v0, p0, LX/1B2;->c:LX/0pe;

    .line 211813
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 211814
    iget-object v6, v0, LX/0pe;->a:LX/0pf;

    invoke-virtual {v6, p1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v6

    .line 211815
    iget-wide v8, v6, LX/1g0;->c:J

    move-wide v6, v8

    .line 211816
    move-wide v0, v6

    .line 211817
    iget-object v2, p0, LX/1B2;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 211818
    sub-long v0, v2, v0

    const-wide/16 v4, 0x64

    cmp-long v0, v0, v4

    if-gez v0, :cond_0

    .line 211819
    const/4 v0, 0x1

    .line 211820
    :goto_0
    return v0

    .line 211821
    :cond_0
    iget-object v0, p0, LX/1B2;->c:LX/0pe;

    .line 211822
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 211823
    iget-object v6, v0, LX/0pe;->a:LX/0pf;

    invoke-virtual {v6, p1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v6

    .line 211824
    iput-wide v2, v6, LX/1g0;->c:J

    .line 211825
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/1B2;Lcom/facebook/graphql/model/Sponsorable;Lcom/facebook/graphql/model/BaseImpression;)V
    .locals 12

    .prologue
    .line 211877
    invoke-static {p1}, LX/18M;->c(Lcom/facebook/graphql/model/Sponsorable;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 211878
    :goto_0
    iget-object v1, p0, LX/1B2;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v10, 0x0

    const/4 v4, 0x0

    .line 211879
    invoke-virtual {p2}, Lcom/facebook/graphql/model/BaseImpression;->k()Z

    move-result v5

    if-nez v5, :cond_4

    .line 211880
    :cond_0
    :goto_1
    move v1, v4

    .line 211881
    if-eqz v1, :cond_1

    .line 211882
    if-eqz v0, :cond_3

    .line 211883
    iget-object v0, p0, LX/1B2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1B5;

    .line 211884
    sget-object v1, LX/3EA;->ORIGINAL:LX/3EA;

    invoke-static {v0, p1, v1}, LX/1B5;->a(LX/1B5;Lcom/facebook/graphql/model/Sponsorable;LX/3EA;)V

    .line 211885
    :cond_1
    :goto_2
    return-void

    .line 211886
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 211887
    :cond_3
    iget-object v0, p0, LX/1B2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1B5;

    .line 211888
    sget-object v1, LX/3EA;->SUBSEQUENT:LX/3EA;

    invoke-static {v0, p1, v1}, LX/1B5;->a(LX/1B5;Lcom/facebook/graphql/model/Sponsorable;LX/3EA;)V

    .line 211889
    goto :goto_2

    .line 211890
    :cond_4
    if-eqz v0, :cond_8

    .line 211891
    invoke-virtual {p2}, Lcom/facebook/graphql/model/BaseImpression;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 211892
    iget-object v5, p2, Lcom/facebook/graphql/model/BaseImpression;->a:LX/183;

    sget-object v6, LX/183;->PENDING:LX/183;

    if-eq v5, v6, :cond_0

    iget-object v5, p2, Lcom/facebook/graphql/model/BaseImpression;->a:LX/183;

    sget-object v6, LX/183;->LOGGING:LX/183;

    if-eq v5, v6, :cond_0

    iget-object v5, p2, Lcom/facebook/graphql/model/BaseImpression;->a:LX/183;

    sget-object v6, LX/183;->LOGGED:LX/183;

    if-eq v5, v6, :cond_0

    .line 211893
    :cond_5
    iget v5, p2, Lcom/facebook/graphql/model/BaseImpression;->l:I

    invoke-virtual {p2}, Lcom/facebook/graphql/model/BaseImpression;->g()I

    move-result v6

    if-ge v5, v6, :cond_0

    iget-wide v6, p2, Lcom/facebook/graphql/model/BaseImpression;->m:J

    cmp-long v5, v6, v10

    if-lez v5, :cond_6

    iget-wide v6, p2, Lcom/facebook/graphql/model/BaseImpression;->m:J

    sub-long v6, v2, v6

    invoke-virtual {p2}, Lcom/facebook/graphql/model/BaseImpression;->f()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-ltz v5, :cond_0

    .line 211894
    :cond_6
    iget-wide v6, p2, Lcom/facebook/graphql/model/BaseImpression;->f:J

    cmp-long v5, v6, v10

    if-lez v5, :cond_7

    iget-wide v6, p2, Lcom/facebook/graphql/model/BaseImpression;->f:J

    sub-long v6, v2, v6

    invoke-virtual {p2}, Lcom/facebook/graphql/model/BaseImpression;->d()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-ltz v5, :cond_0

    .line 211895
    :cond_7
    const/4 v4, 0x1

    goto :goto_1

    .line 211896
    :cond_8
    invoke-virtual {p2}, Lcom/facebook/graphql/model/BaseImpression;->b()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 211897
    iget-object v5, p2, Lcom/facebook/graphql/model/BaseImpression;->a:LX/183;

    sget-object v6, LX/183;->PENDING:LX/183;

    if-eq v5, v6, :cond_0

    iget-object v5, p2, Lcom/facebook/graphql/model/BaseImpression;->a:LX/183;

    sget-object v6, LX/183;->LOGGING:LX/183;

    if-eq v5, v6, :cond_0

    .line 211898
    iget-object v5, p2, Lcom/facebook/graphql/model/BaseImpression;->b:LX/183;

    sget-object v6, LX/183;->PENDING:LX/183;

    if-eq v5, v6, :cond_0

    iget-object v5, p2, Lcom/facebook/graphql/model/BaseImpression;->b:LX/183;

    sget-object v6, LX/183;->LOGGING:LX/183;

    if-ne v5, v6, :cond_5

    goto/16 :goto_1
.end method

.method private static b(LX/0QB;)LX/1B2;
    .locals 13

    .prologue
    .line 211899
    new-instance v0, LX/1B2;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    const/16 v2, 0x67c

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/0pe;->a(LX/0QB;)LX/0pe;

    move-result-object v3

    check-cast v3, LX/0pe;

    const/16 v4, 0x14a9

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x14ad

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x14ab

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x14ac

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x14ae

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x14a8

    invoke-static {p0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {p0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v10

    check-cast v10, Landroid/os/Handler;

    const/16 v11, 0x67a

    invoke-static {p0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-direct/range {v0 .. v12}, LX/1B2;-><init>(LX/0SG;LX/0Ot;LX/0pe;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;Landroid/os/Handler;LX/0Or;LX/0ad;)V

    .line 211900
    return-object v0
.end method

.method private b(Lcom/facebook/graphql/model/Sponsorable;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 211844
    if-nez p1, :cond_1

    .line 211845
    :cond_0
    :goto_0
    return-void

    .line 211846
    :cond_1
    invoke-static {p1}, LX/18J;->a(Lcom/facebook/graphql/model/Sponsorable;)Lcom/facebook/graphql/model/BaseImpression;

    move-result-object v1

    .line 211847
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/BaseImpression;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211848
    invoke-direct {p0}, LX/1B2;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 211849
    iget-object v0, p0, LX/1B2;->q:Ljava/util/Map;

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 211850
    if-eqz v0, :cond_2

    .line 211851
    iget-object v2, p0, LX/1B2;->p:Landroid/os/Handler;

    invoke-static {v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 211852
    :cond_2
    iget-boolean v0, p0, LX/1B2;->n:Z

    if-eqz v0, :cond_3

    .line 211853
    iget-object v0, p0, LX/1B2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1B5;

    .line 211854
    invoke-static {p1}, LX/1fz;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/162;

    move-result-object v2

    .line 211855
    new-instance v3, LX/1g1;

    sget-object v5, LX/1g2;->SPONSORED_PERCENT_UNIT_SEEN:LX/1g2;

    invoke-direct {v3, v5, p1, v2}, LX/1g1;-><init>(LX/1g2;Ljava/lang/Object;LX/162;)V

    invoke-interface {p1}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v2

    .line 211856
    iput-object v2, v3, LX/1g1;->c:Lcom/facebook/graphql/model/BaseImpression;

    .line 211857
    move-object v2, v3

    .line 211858
    invoke-static {v0, v2}, LX/1B5;->a(LX/1B5;LX/1g1;)V

    .line 211859
    :cond_3
    iget-boolean v0, p0, LX/1B2;->m:Z

    if-eqz v0, :cond_4

    invoke-virtual {v1, v4}, Lcom/facebook/graphql/model/BaseImpression;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 211860
    iget-object v0, p0, LX/1B2;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/facebook/graphql/model/BaseImpression;->b(J)J

    move-result-wide v2

    .line 211861
    iget-object v0, p0, LX/1B2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1B5;

    invoke-virtual {v0, p1, v2, v3}, LX/1B5;->a(Lcom/facebook/graphql/model/Sponsorable;J)V

    .line 211862
    :cond_4
    iget-boolean v0, p0, LX/1B2;->k:Z

    if-eqz v0, :cond_5

    .line 211863
    iget-object v0, p0, LX/1B2;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    const/4 v0, 0x1

    invoke-virtual {v1, v4, v2, v3, v0}, Lcom/facebook/graphql/model/BaseImpression;->a(IJZ)Z

    move-result v0

    .line 211864
    if-eqz v0, :cond_5

    .line 211865
    iget-object v0, p0, LX/1B2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1B5;

    invoke-virtual {v0, p1}, LX/1B5;->c(Lcom/facebook/graphql/model/Sponsorable;)V

    .line 211866
    :cond_5
    iget-boolean v0, p0, LX/1B2;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 211867
    invoke-virtual {v1}, Lcom/facebook/graphql/model/BaseImpression;->k()Z

    move-result v2

    if-nez v2, :cond_7

    .line 211868
    :cond_6
    :goto_1
    move v0, v0

    .line 211869
    if-eqz v0, :cond_0

    .line 211870
    iget-object v0, p0, LX/1B2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1B5;

    .line 211871
    invoke-static {p1}, LX/1fz;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/162;

    move-result-object v1

    .line 211872
    new-instance v2, LX/1g1;

    sget-object v3, LX/1g2;->NON_VIEWABILITY_IMPRESSION:LX/1g2;

    invoke-direct {v2, v3, p1, v1}, LX/1g1;-><init>(LX/1g2;Ljava/lang/Object;LX/162;)V

    invoke-static {v0, v2}, LX/1B5;->a(LX/1B5;LX/1g1;)V

    .line 211873
    goto/16 :goto_0

    .line 211874
    :cond_7
    invoke-virtual {v1}, Lcom/facebook/graphql/model/BaseImpression;->c()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 211875
    iget-boolean v2, v1, Lcom/facebook/graphql/model/BaseImpression;->d:Z

    if-nez v2, :cond_6

    .line 211876
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private b()Z
    .locals 3

    .prologue
    .line 211735
    iget-object v0, p0, LX/1B2;->t:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 211736
    iget-object v0, p0, LX/1B2;->s:LX/0ad;

    sget-short v1, LX/3E9;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1B2;->t:Ljava/lang/Boolean;

    .line 211737
    :cond_0
    iget-object v0, p0, LX/1B2;->t:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 211592
    iget-object v0, p0, LX/1B2;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/1B2;->k:Z

    .line 211593
    iget-object v0, p0, LX/1B2;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/1B2;->k:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/1B2;->j:Z

    .line 211594
    iget-object v0, p0, LX/1B2;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/1B2;->l:Z

    .line 211595
    iget-object v0, p0, LX/1B2;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/1B2;->m:Z

    .line 211596
    iget-object v0, p0, LX/1B2;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/1B2;->n:Z

    .line 211597
    iget-object v0, p0, LX/1B2;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/1B2;->o:Z

    .line 211598
    return-void

    .line 211599
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/01J;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/01J",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 211738
    return-void
.end method

.method public final a(LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 211600
    return-void
.end method

.method public final a(LX/0g8;II)V
    .locals 4

    .prologue
    .line 211601
    invoke-static {p1}, LX/1BX;->a(LX/0g8;)LX/1XQ;

    move-result-object v0

    .line 211602
    if-eqz v0, :cond_0

    .line 211603
    invoke-interface {p1, p3}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v1

    .line 211604
    add-int v2, p2, p3

    .line 211605
    if-nez v1, :cond_1

    .line 211606
    :cond_0
    :goto_0
    return-void

    .line 211607
    :cond_1
    invoke-static {v0, v2}, LX/1XQ;->a$redex0(LX/1XQ;I)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    .line 211608
    if-eqz v3, :cond_0

    .line 211609
    iget-object p0, v0, LX/1XQ;->b:LX/1UQ;

    invoke-virtual {p0}, LX/1UQ;->b()I

    move-result p0

    if-lt v2, p0, :cond_0

    iget-object p0, v0, LX/1XQ;->b:LX/1UQ;

    invoke-virtual {p0}, LX/1UQ;->c()I

    move-result p0

    if-gt v2, p0, :cond_0

    .line 211610
    iget-object p0, v0, LX/1XQ;->b:LX/1UQ;

    invoke-interface {p0, v2}, LX/1Qr;->c(I)I

    move-result p0

    .line 211611
    iget-object p1, v0, LX/1XQ;->b:LX/1UQ;

    invoke-interface {p1, v2}, LX/1Qr;->g(I)I

    move-result p1

    .line 211612
    iget-object p2, v0, LX/1XQ;->h:LX/1XS;

    invoke-virtual {p2, v3, p1, p0}, LX/1XS;->b(Lcom/facebook/graphql/model/FeedUnit;II)Z

    move-result p2

    if-nez p2, :cond_0

    .line 211613
    iget-object p2, v0, LX/1XQ;->h:LX/1XS;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p3

    invoke-virtual {p2, v3, p1, p3, p0}, LX/1XS;->a(Lcom/facebook/graphql/model/FeedUnit;III)V

    goto :goto_0
.end method

.method public final a(LX/0g8;Ljava/lang/Object;I)V
    .locals 9

    .prologue
    .line 211614
    invoke-static {p2}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 211615
    invoke-interface {p1, p3}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v3

    .line 211616
    iget-boolean v0, p0, LX/1B2;->o:Z

    if-eqz v0, :cond_3

    .line 211617
    iget-object v0, p0, LX/1B2;->r:LX/1B4;

    if-nez v0, :cond_0

    if-eqz v1, :cond_3

    .line 211618
    :cond_0
    iget-object v0, p0, LX/1B2;->r:LX/1B4;

    const/4 v8, 0x1

    .line 211619
    invoke-static {p1}, LX/1BX;->b(LX/0g8;)LX/1Qr;

    move-result-object v2

    .line 211620
    if-eqz v2, :cond_1

    iget-object v4, v0, LX/1B4;->a:LX/0pf;

    invoke-virtual {v4, v1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v4

    .line 211621
    iget-boolean v5, v4, LX/1g0;->j:Z

    move v4, v5

    .line 211622
    if-eqz v4, :cond_7

    .line 211623
    :cond_1
    :goto_0
    iget-object v0, p0, LX/1B2;->r:LX/1B4;

    const/4 v8, 0x1

    .line 211624
    invoke-static {p1}, LX/1BX;->b(LX/0g8;)LX/1Qr;

    move-result-object v2

    .line 211625
    if-eqz v2, :cond_2

    iget-object v4, v0, LX/1B4;->a:LX/0pf;

    invoke-virtual {v4, v1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v4

    .line 211626
    iget-boolean v5, v4, LX/1g0;->h:Z

    move v4, v5

    .line 211627
    if-eqz v4, :cond_8

    .line 211628
    :cond_2
    :goto_1
    iget-object v0, p0, LX/1B2;->r:LX/1B4;

    const/4 v4, 0x1

    .line 211629
    move-object v2, v1

    check-cast v2, Lcom/facebook/graphql/model/Sponsorable;

    invoke-interface {v2}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v2

    .line 211630
    iget-boolean v5, v2, Lcom/facebook/graphql/model/SponsoredImpression;->y:Z

    move v2, v5

    .line 211631
    if-nez v2, :cond_9

    .line 211632
    :cond_3
    :goto_2
    iget-boolean v0, p0, LX/1B2;->n:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, LX/1B2;->k:Z

    if-eqz v0, :cond_5

    :cond_4
    invoke-direct {p0, v1}, LX/1B2;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 211633
    :cond_5
    :goto_3
    return-void

    .line 211634
    :cond_6
    invoke-static {p1}, LX/1BX;->a(LX/0g8;)LX/1XQ;

    move-result-object v5

    .line 211635
    invoke-static {p1, v5, p3, v1, v3}, LX/1B2;->a(LX/0g8;LX/1XQ;ILcom/facebook/graphql/model/FeedUnit;Landroid/view/View;)I

    move-result v6

    .line 211636
    invoke-interface {p1}, LX/0g8;->d()I

    move-result v2

    move-object v0, p1

    move v4, p3

    .line 211637
    instance-of v7, v1, Lcom/facebook/graphql/model/GraphQLHoldoutAdFeedUnit;

    if-eqz v7, :cond_e

    .line 211638
    :goto_4
    move v0, v2

    .line 211639
    if-lez v0, :cond_5

    .line 211640
    invoke-interface {p1}, LX/0g8;->d()I

    move-result v1

    const/4 v4, 0x0

    .line 211641
    invoke-static {v6, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    mul-int/lit8 v2, v2, -0x1

    .line 211642
    add-int v3, v6, v0

    .line 211643
    sub-int/2addr v3, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 211644
    add-int/2addr v2, v3

    .line 211645
    mul-int/lit8 v2, v2, 0x64

    div-int/2addr v2, v0

    rsub-int/lit8 v2, v2, 0x64

    move v0, v2

    .line 211646
    if-ltz v0, :cond_5

    .line 211647
    invoke-direct {p0, p2, v0}, LX/1B2;->a(Ljava/lang/Object;I)V

    goto :goto_3

    .line 211648
    :cond_7
    invoke-interface {p1}, LX/0g8;->q()I

    move-result v4

    .line 211649
    invoke-interface {p1}, LX/0g8;->r()I

    move-result v5

    .line 211650
    invoke-interface {p1, v3}, LX/0g8;->c(Landroid/view/View;)I

    move-result v6

    .line 211651
    invoke-interface {p1}, LX/0g8;->B()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 211652
    invoke-interface {v2, v6}, LX/1Qr;->h_(I)I

    move-result v6

    .line 211653
    const/high16 v7, -0x80000000

    if-eq v6, v7, :cond_1

    .line 211654
    invoke-interface {v2, v6}, LX/1Qr;->n_(I)I

    move-result v2

    .line 211655
    if-lt v2, v4, :cond_1

    if-gt v2, v5, :cond_1

    .line 211656
    invoke-interface {p1}, LX/0g8;->d()I

    move-result v6

    .line 211657
    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    sub-int/2addr v2, v4

    .line 211658
    invoke-interface {p1, v2}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2, v6}, LX/1BX;->a(Landroid/view/View;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 211659
    iget-object v2, v0, LX/1B4;->a:LX/0pf;

    invoke-virtual {v2, v1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v2

    .line 211660
    iput-boolean v8, v2, LX/1g0;->j:Z

    .line 211661
    iget-object v2, v0, LX/1B4;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 211662
    iget-object v2, v0, LX/1B4;->b:LX/1B5;

    sget-object v4, LX/AkR;->BOTTOM_SEEN:LX/AkR;

    invoke-virtual {v2, v1, v4}, LX/1B5;->a(Lcom/facebook/graphql/model/FeedUnit;LX/AkR;)V

    .line 211663
    iget-object v2, v0, LX/1B4;->a:LX/0pf;

    invoke-virtual {v2, v1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v2

    .line 211664
    iput-boolean v8, v2, LX/1g0;->k:Z

    .line 211665
    goto/16 :goto_0

    .line 211666
    :cond_8
    invoke-interface {p1}, LX/0g8;->q()I

    move-result v4

    .line 211667
    invoke-interface {p1}, LX/0g8;->r()I

    move-result v5

    .line 211668
    invoke-interface {p1, v3}, LX/0g8;->c(Landroid/view/View;)I

    move-result v6

    .line 211669
    invoke-interface {p1}, LX/0g8;->B()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 211670
    invoke-interface {v2, v6}, LX/1Qr;->h_(I)I

    move-result v6

    .line 211671
    const/high16 v7, -0x80000000

    if-eq v6, v7, :cond_2

    .line 211672
    invoke-interface {v2, v6}, LX/1Qr;->m_(I)I

    move-result v2

    .line 211673
    if-lt v2, v4, :cond_2

    if-gt v2, v5, :cond_2

    .line 211674
    invoke-interface {p1}, LX/0g8;->d()I

    move-result v5

    .line 211675
    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    sub-int/2addr v2, v4

    .line 211676
    invoke-interface {p1, v2}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2, v5}, LX/1BX;->a(Landroid/view/View;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 211677
    iget-object v2, v0, LX/1B4;->a:LX/0pf;

    invoke-virtual {v2, v1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v2

    .line 211678
    iput-boolean v8, v2, LX/1g0;->h:Z

    .line 211679
    iget-object v2, v0, LX/1B4;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 211680
    iget-object v2, v0, LX/1B4;->b:LX/1B5;

    sget-object v4, LX/AkR;->TOP_SEEN:LX/AkR;

    invoke-virtual {v2, v1, v4}, LX/1B5;->a(Lcom/facebook/graphql/model/FeedUnit;LX/AkR;)V

    .line 211681
    iget-object v2, v0, LX/1B4;->a:LX/0pf;

    invoke-virtual {v2, v1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v2

    .line 211682
    iput-boolean v8, v2, LX/1g0;->i:Z

    .line 211683
    goto/16 :goto_1

    .line 211684
    :cond_9
    iget-object v2, v0, LX/1B4;->a:LX/0pf;

    invoke-virtual {v2, v1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v2

    .line 211685
    iget-boolean v5, v2, LX/1g0;->j:Z

    move v2, v5

    .line 211686
    if-eqz v2, :cond_a

    iget-object v2, v0, LX/1B4;->a:LX/0pf;

    invoke-virtual {v2, v1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v2

    .line 211687
    iget-boolean v5, v2, LX/1g0;->h:Z

    move v2, v5

    .line 211688
    if-eqz v2, :cond_a

    move v2, v4

    .line 211689
    :goto_5
    if-eqz v2, :cond_3

    .line 211690
    instance-of v2, v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v2, :cond_b

    move-object v2, v1

    .line 211691
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStorySet;->I_()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/1B4;->a(Lcom/facebook/graphql/model/FeedUnit;I)V

    goto/16 :goto_2

    .line 211692
    :cond_a
    const/4 v2, 0x0

    goto :goto_5

    .line 211693
    :cond_b
    instance-of v2, v1, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    if-eqz v2, :cond_c

    move-object v2, v1

    .line 211694
    check-cast v2, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->I_()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/1B4;->a(Lcom/facebook/graphql/model/FeedUnit;I)V

    goto/16 :goto_2

    .line 211695
    :cond_c
    iget-object v2, v0, LX/1B4;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-static {v0, v1}, LX/1B4;->b(LX/1B4;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 211696
    :cond_d
    iget-object v2, v0, LX/1B4;->b:LX/1B5;

    invoke-virtual {v2, v1}, LX/1B5;->b(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 211697
    iget-object v2, v0, LX/1B4;->a:LX/0pf;

    invoke-virtual {v2, v1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v2

    .line 211698
    iput-boolean v4, v2, LX/1g0;->l:Z

    .line 211699
    goto/16 :goto_2

    .line 211700
    :cond_e
    if-eqz v5, :cond_10

    .line 211701
    invoke-interface {v0}, LX/0g8;->q()I

    move-result v7

    add-int/2addr v7, v4

    const/4 v8, 0x0

    const/4 v4, -0x1

    .line 211702
    invoke-static {v5, v7}, LX/1XQ;->b(LX/1XQ;I)I

    move-result v2

    .line 211703
    if-nez v1, :cond_11

    .line 211704
    :cond_f
    :goto_6
    move v2, v8

    .line 211705
    goto/16 :goto_4

    .line 211706
    :cond_10
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v2

    goto/16 :goto_4

    .line 211707
    :cond_11
    iget-object v0, v5, LX/1XQ;->b:LX/1UQ;

    invoke-interface {v0, v2}, LX/1Qr;->c(I)I

    move-result v3

    .line 211708
    iget-object v0, v5, LX/1XQ;->h:LX/1XS;

    invoke-virtual {v0, v1, v3}, LX/1XS;->a(Lcom/facebook/graphql/model/FeedUnit;I)I

    move-result v0

    .line 211709
    if-eq v0, v4, :cond_12

    move v8, v0

    .line 211710
    goto :goto_6

    .line 211711
    :cond_12
    invoke-static {v5, v1, v2}, LX/1XQ;->c(LX/1XQ;Lcom/facebook/graphql/model/FeedUnit;I)V

    .line 211712
    iget-object v0, v5, LX/1XQ;->h:LX/1XS;

    invoke-virtual {v0, v1, v3}, LX/1XS;->a(Lcom/facebook/graphql/model/FeedUnit;I)I

    move-result v0

    .line 211713
    if-eq v0, v4, :cond_f

    move v8, v0

    goto :goto_6
.end method

.method public final a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V
    .locals 3

    .prologue
    .line 211714
    invoke-static {p1}, LX/18J;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/Sponsorable;

    move-result-object v0

    .line 211715
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, LX/1B2;->a(Ljava/lang/Object;I)V

    .line 211716
    iget-boolean v1, p0, LX/1B2;->o:Z

    if-eqz v1, :cond_0

    .line 211717
    iget-object v1, p0, LX/1B2;->r:LX/1B4;

    add-int/lit8 v2, p2, -0x1

    invoke-virtual {v1, p1, v2}, LX/1B4;->a(Lcom/facebook/graphql/model/FeedUnit;I)V

    .line 211718
    :cond_0
    invoke-direct {p0, v0}, LX/1B2;->b(Lcom/facebook/graphql/model/Sponsorable;)V

    .line 211719
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 211720
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 211721
    iget-boolean v0, p0, LX/1B2;->k:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/1B2;->n:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {p1}, LX/1BX;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V
    .locals 1

    .prologue
    .line 211722
    invoke-static {p1}, LX/18J;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/Sponsorable;

    move-result-object v0

    .line 211723
    invoke-direct {p0, v0}, LX/1B2;->a(Lcom/facebook/graphql/model/Sponsorable;)V

    .line 211724
    iget-boolean v0, p0, LX/1B2;->o:Z

    if-eqz v0, :cond_0

    .line 211725
    iget-object v0, p0, LX/1B2;->r:LX/1B4;

    invoke-virtual {v0, p1, p2}, LX/1B4;->a(Lcom/facebook/graphql/model/FeedUnit;I)V

    .line 211726
    :cond_0
    const/16 v0, 0x64

    invoke-direct {p0, p1, v0}, LX/1B2;->a(Ljava/lang/Object;I)V

    .line 211727
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 211728
    invoke-static {p1}, LX/18J;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/Sponsorable;

    move-result-object v0

    .line 211729
    invoke-direct {p0, v0}, LX/1B2;->a(Lcom/facebook/graphql/model/Sponsorable;)V

    .line 211730
    return-void
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 211731
    invoke-static {p1}, LX/18J;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/Sponsorable;

    move-result-object v0

    .line 211732
    invoke-direct {p0, v0}, LX/1B2;->b(Lcom/facebook/graphql/model/Sponsorable;)V

    .line 211733
    return-void
.end method

.method public final d(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 211734
    invoke-static {p1}, LX/1BX;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
