.class public final LX/0vG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/subjects/ReplaySubject$ReplayState",
        "<TT;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field public static final b:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater",
            "<",
            "LX/0vG;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public volatile a:I

.field private final c:LX/0vH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vH",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public volatile e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 157751
    const-class v0, LX/0vG;

    const-string v1, "a"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    move-result-object v0

    sput-object v0, LX/0vG;->b:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 157752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157753
    sget-object v0, LX/0vH;->a:LX/0vH;

    move-object v0, v0

    .line 157754
    iput-object v0, p0, LX/0vG;->c:LX/0vH;

    .line 157755
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/0vG;->d:Ljava/util/ArrayList;

    .line 157756
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Integer;LX/0vO;)Ljava/lang/Integer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "LX/0vO",
            "<-TT;>;)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    .prologue
    .line 157757
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 157758
    :goto_0
    iget v1, p0, LX/0vG;->a:I

    if-ge v0, v1, :cond_0

    .line 157759
    iget-object v1, p0, LX/0vG;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p2, v1}, LX/0vH;->a(LX/0vA;Ljava/lang/Object;)Z

    .line 157760
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157761
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0vO;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0vO",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 157762
    iget-object v0, p1, LX/0vO;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 157763
    check-cast v0, Ljava/lang/Integer;

    .line 157764
    if-eqz v0, :cond_0

    .line 157765
    invoke-virtual {p0, v0, p1}, LX/0vG;->a(Ljava/lang/Integer;LX/0vO;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 157766
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 157767
    iput-object v0, p1, LX/0vO;->d:Ljava/lang/Object;

    .line 157768
    return-void

    .line 157769
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "failed to find lastEmittedLink for: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
