.class public LX/1I5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/crypto/keychain/KeyChain;


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:LX/1Hy;

.field public static final c:LX/1I6;

.field private static final d:LX/1Hb;

.field public static final e:[B


# instance fields
.field public final f:LX/0WS;

.field public final g:Ljava/security/SecureRandom;

.field public final h:LX/03V;

.field private final i:LX/1Ho;

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:[B
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 228212
    const-class v0, LX/1I5;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1I5;->a:Ljava/lang/String;

    .line 228213
    sget-object v0, LX/1Hy;->KEY_256:LX/1Hy;

    sput-object v0, LX/1I5;->b:LX/1Hy;

    .line 228214
    sget-object v0, LX/1I6;->e:LX/1I6;

    move-object v0, v0

    .line 228215
    invoke-virtual {v0}, LX/1I6;->c()LX/1I6;

    move-result-object v0

    sput-object v0, LX/1I5;->c:LX/1I6;

    .line 228216
    const-string v0, "device_key"

    invoke-static {v0}, LX/1Hb;->a(Ljava/lang/String;)LX/1Hb;

    move-result-object v0

    sput-object v0, LX/1I5;->d:LX/1Hb;

    .line 228217
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, LX/1I5;->e:[B

    return-void

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>(LX/0WP;LX/1Ho;Ljava/lang/String;LX/03V;)V
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 228246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 228247
    const-string v0, "user_storage_device_key"

    invoke-virtual {p1, v0}, LX/0WP;->a(Ljava/lang/String;)LX/0WS;

    move-result-object v0

    iput-object v0, p0, LX/1I5;->f:LX/0WS;

    .line 228248
    iput-object p4, p0, LX/1I5;->h:LX/03V;

    .line 228249
    iput-object p2, p0, LX/1I5;->i:LX/1Ho;

    .line 228250
    iput-object p3, p0, LX/1I5;->j:Ljava/lang/String;

    .line 228251
    iget-object v0, p0, LX/1I5;->i:LX/1Ho;

    iget-object v0, v0, LX/1Ho;->b:Ljava/security/SecureRandom;

    iput-object v0, p0, LX/1I5;->g:Ljava/security/SecureRandom;

    .line 228252
    if-eqz p3, :cond_2

    .line 228253
    const/4 v0, 0x0

    .line 228254
    iget-object v1, p0, LX/1I5;->k:[B

    if-eqz v1, :cond_0

    .line 228255
    iget-object v1, p0, LX/1I5;->k:[B

    invoke-static {v1}, LX/1I5;->c([B)V

    .line 228256
    iput-object v0, p0, LX/1I5;->k:[B

    .line 228257
    :cond_0
    const-string v1, "user_storage_device_key"

    invoke-static {p0, v1}, LX/1I5;->b(LX/1I5;Ljava/lang/String;)[B

    move-result-object v1

    .line 228258
    if-eqz v1, :cond_3

    array-length p1, v1

    sget-object p2, LX/1I5;->b:LX/1Hy;

    iget p2, p2, LX/1Hy;->keyLength:I

    if-eq p1, p2, :cond_3

    invoke-static {v1}, LX/1I5;->a([B)Z

    move-result p1

    if-nez p1, :cond_3

    .line 228259
    const-string p1, "user_storage_device_key"

    invoke-static {p0, p1}, LX/1I5;->c(LX/1I5;Ljava/lang/String;)V

    .line 228260
    iget-object p1, p0, LX/1I5;->h:LX/03V;

    sget-object p2, LX/1I5;->a:Ljava/lang/String;

    new-instance p3, Ljava/lang/StringBuilder;

    const-string p4, "Error loading device key. Length: "

    invoke-direct {p3, p4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v1, v1

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, p2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 228261
    :goto_0
    if-nez v0, :cond_1

    iget-object v1, p0, LX/1I5;->j:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 228262
    sget-object v0, LX/1I5;->e:[B

    .line 228263
    iget-object v1, p0, LX/1I5;->f:LX/0WS;

    invoke-virtual {v1}, LX/0WS;->b()LX/1gW;

    move-result-object v1

    .line 228264
    const-string p1, "user_storage_device_key"

    sget-object p2, LX/1I5;->c:LX/1I6;

    invoke-virtual {p2, v0}, LX/1I6;->a([B)Ljava/lang/String;

    move-result-object p2

    invoke-interface {v1, p1, p2}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;

    .line 228265
    invoke-interface {v1}, LX/1gW;->b()Z

    .line 228266
    :cond_1
    iput-object v0, p0, LX/1I5;->k:[B

    .line 228267
    :cond_2
    return-void

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private static a(LX/1I5;Ljava/lang/String;[B[BLX/1gW;)V
    .locals 2

    .prologue
    .line 228236
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228237
    invoke-static {p0, p2}, LX/1I5;->b(LX/1I5;[B)LX/1Hz;

    move-result-object v0

    .line 228238
    :try_start_0
    sget-object v1, LX/1I5;->d:LX/1Hb;

    invoke-virtual {v0, p3, v1}, LX/1Hz;->a([BLX/1Hb;)[B

    move-result-object v0

    .line 228239
    sget-object v1, LX/1I5;->c:LX/1I6;

    invoke-virtual {v1, v0}, LX/1I6;->a([B)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p4, p1, v0}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;
    :try_end_0
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_2
    .catch LX/48g; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 228240
    return-void

    .line 228241
    :catch_0
    move-exception v0

    .line 228242
    :goto_0
    new-instance v1, LX/48h;

    invoke-direct {v1, v0}, LX/48h;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 228243
    :catch_1
    move-exception v0

    .line 228244
    new-instance v1, LX/48h;

    invoke-direct {v1, v0}, LX/48h;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 228245
    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method public static a([B)Z
    .locals 1

    .prologue
    .line 228235
    sget-object v0, LX/1I5;->e:[B

    invoke-static {p0, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    return v0
.end method

.method public static a(LX/1I5;[B[B)[B
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 228226
    invoke-static {p0, p1}, LX/1I5;->b(LX/1I5;[B)LX/1Hz;

    move-result-object v0

    .line 228227
    :try_start_0
    sget-object v1, LX/1I5;->d:LX/1Hb;

    invoke-virtual {v0, p2, v1}, LX/1Hz;->b([BLX/1Hb;)[B
    :try_end_0
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_2
    .catch LX/48g; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 228228
    :goto_0
    return-object v0

    .line 228229
    :catch_0
    move-exception v0

    .line 228230
    :goto_1
    new-instance v1, LX/48h;

    invoke-direct {v1, v0}, LX/48h;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 228231
    :catch_1
    move-exception v0

    .line 228232
    iget-object v1, p0, LX/1I5;->h:LX/03V;

    sget-object v2, LX/1I5;->a:Ljava/lang/String;

    const-string v3, "Wrong user-key"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 228233
    const/4 v0, 0x0

    goto :goto_0

    .line 228234
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;)[B
    .locals 5
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 228218
    if-nez p0, :cond_0

    .line 228219
    new-instance v0, LX/48i;

    const-string v1, "Key cannot be null"

    invoke-direct {v0, v1, v4}, LX/48i;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 228220
    :cond_0
    :try_start_0
    sget-object v0, LX/1I5;->c:LX/1I6;

    invoke-virtual {v0, p0}, LX/1I6;->a(Ljava/lang/CharSequence;)[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 228221
    array-length v1, v0

    sget-object v2, LX/1I5;->b:LX/1Hy;

    iget v2, v2, LX/1Hy;->keyLength:I

    if-eq v1, v2, :cond_1

    .line 228222
    new-instance v1, LX/48i;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Incorrect key length: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v0, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". It should be: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LX/1I5;->b:LX/1Hy;

    iget v2, v2, LX/1Hy;->keyLength:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, v4}, LX/48i;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 228223
    :catch_0
    move-exception v0

    .line 228224
    new-instance v1, LX/48i;

    const-string v2, "Incorrect key, invalid hex"

    invoke-direct {v1, v2, v0}, LX/48i;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 228225
    :cond_1
    return-object v0
.end method

.method private static b(LX/1I5;[B)LX/1Hz;
    .locals 2

    .prologue
    .line 228210
    new-instance v0, LX/2X0;

    invoke-direct {v0, p0, p1}, LX/2X0;-><init>(LX/1I5;[B)V

    .line 228211
    iget-object v1, p0, LX/1I5;->i:LX/1Ho;

    invoke-virtual {v1, v0}, LX/1Ho;->c(Lcom/facebook/crypto/keychain/KeyChain;)LX/1Hz;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/1I5;Ljava/lang/String;)[B
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 228268
    iget-object v1, p0, LX/1I5;->f:LX/0WS;

    const-string v2, ""

    invoke-virtual {v1, p1, v2}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 228269
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 228270
    :goto_0
    return-object v0

    .line 228271
    :cond_0
    :try_start_0
    sget-object v2, LX/1I5;->c:LX/1I6;

    invoke-virtual {v2, v1}, LX/1I6;->a(Ljava/lang/CharSequence;)[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 228272
    :catch_0
    iget-object v2, p0, LX/1I5;->h:LX/03V;

    sget-object v3, LX/1I5;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error loading hex key, "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 228273
    invoke-static {p0, p1}, LX/1I5;->c(LX/1I5;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static c(LX/1I5;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 228208
    iget-object v0, p0, LX/1I5;->f:LX/0WS;

    invoke-virtual {v0}, LX/0WS;->b()LX/1gW;

    move-result-object v0

    invoke-interface {v0, p1}, LX/1gW;->a(Ljava/lang/String;)LX/1gW;

    move-result-object v0

    invoke-interface {v0}, LX/1gW;->b()Z

    .line 228209
    return-void
.end method

.method public static c([B)V
    .locals 1

    .prologue
    .line 228206
    const/4 v0, 0x0

    invoke-static {p0, v0}, Ljava/util/Arrays;->fill([BB)V

    .line 228207
    return-void
.end method

.method private static e(LX/1I5;)[B
    .locals 2

    .prologue
    .line 228131
    sget-object v0, LX/1I5;->b:LX/1Hy;

    iget v0, v0, LX/1Hy;->keyLength:I

    new-array v0, v0, [B

    .line 228132
    iget-object v1, p0, LX/1I5;->g:Ljava/security/SecureRandom;

    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 228133
    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 228161
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    const/4 v0, 0x2

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 228162
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 228163
    invoke-static {p2}, LX/1I5;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228164
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 228165
    invoke-static {p3}, LX/1I5;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228166
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "user_storage_encrypted_key."

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 228167
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "user_storage_not_encrypted_key."

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 228168
    iget-object v0, p0, LX/1I5;->f:LX/0WS;

    invoke-virtual {v0}, LX/0WS;->b()LX/1gW;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 228169
    :try_start_1
    invoke-static {p0, v3}, LX/1I5;->b(LX/1I5;Ljava/lang/String;)[B

    move-result-object v7

    .line 228170
    if-eqz v7, :cond_a

    .line 228171
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_9

    .line 228172
    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {p0, v0, v7}, LX/1I5;->a(LX/1I5;[B[B)[B

    move-result-object v8

    .line 228173
    if-eqz v8, :cond_8

    .line 228174
    new-instance v0, LX/2Wq;

    invoke-direct {v0, v8, v6}, LX/2Wq;-><init>([BI)V

    .line 228175
    :goto_1
    move-object v6, v0

    .line 228176
    iget-object v0, v6, LX/2Wq;->a:[B

    .line 228177
    if-nez v0, :cond_7

    .line 228178
    invoke-static {p0, v4}, LX/1I5;->b(LX/1I5;Ljava/lang/String;)[B

    move-result-object v0

    .line 228179
    if-eqz v0, :cond_1

    array-length v7, v0

    sget-object v8, LX/1I5;->b:LX/1Hy;

    iget v8, v8, LX/1Hy;->keyLength:I

    if-eq v7, v8, :cond_1

    invoke-static {v0}, LX/1I5;->a([B)Z

    move-result v7

    if-nez v7, :cond_1

    .line 228180
    iget-object v7, p0, LX/1I5;->h:LX/03V;

    sget-object v8, LX/1I5;->a:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Device key stored plain was not valid!!! Length: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v0, v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v8, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 228181
    const/4 v0, 0x0

    .line 228182
    invoke-static {p0, v4}, LX/1I5;->c(LX/1I5;Ljava/lang/String;)V

    .line 228183
    :cond_1
    if-nez v0, :cond_7

    .line 228184
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 228185
    :goto_2
    if-eqz v0, :cond_4

    invoke-static {p0}, LX/1I5;->e(LX/1I5;)[B

    move-result-object v0

    :goto_3
    move-object v1, v0

    .line 228186
    :goto_4
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228187
    iget v0, v6, LX/2Wq;->b:I

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 228188
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {p0, v3, v0, v1, v5}, LX/1I5;->a(LX/1I5;Ljava/lang/String;[B[BLX/1gW;)V

    .line 228189
    :cond_2
    invoke-interface {v5, v4}, LX/1gW;->a(Ljava/lang/String;)LX/1gW;

    .line 228190
    const-string v0, "user_storage_device_key"

    sget-object v3, LX/1I5;->c:LX/1I6;

    invoke-virtual {v3, v1}, LX/1I6;->a([B)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5, v0, v3}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;

    .line 228191
    invoke-interface {v5}, LX/1gW;->b()Z

    .line 228192
    iput-object v1, p0, LX/1I5;->k:[B

    .line 228193
    iput-object p1, p0, LX/1I5;->j:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 228194
    :try_start_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 228195
    invoke-static {v0}, LX/1I5;->c([B)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5

    .line 228196
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v0, v1

    .line 228197
    goto :goto_2

    .line 228198
    :cond_4
    :try_start_3
    sget-object v0, LX/1I5;->e:[B
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_3

    .line 228199
    :catchall_1
    move-exception v0

    move-object v1, v0

    :try_start_4
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 228200
    invoke-static {v0}, LX/1I5;->c([B)V

    goto :goto_6

    .line 228201
    :cond_5
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 228202
    :cond_6
    monitor-exit p0

    return-void

    :cond_7
    move-object v1, v0

    goto :goto_4

    .line 228203
    :cond_8
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_0

    .line 228204
    :cond_9
    iget-object v0, p0, LX/1I5;->h:LX/03V;

    sget-object v6, LX/1I5;->a:Ljava/lang/String;

    const-string v7, "Cannot decrypt device-key with either user-key!"

    invoke-virtual {v0, v6, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 228205
    :cond_a
    new-instance v0, LX/2Wq;

    invoke-direct {v0}, LX/2Wq;-><init>()V

    goto/16 :goto_1
.end method

.method public final declared-synchronized a(Z)V
    .locals 4

    .prologue
    .line 228149
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1I5;->k:[B

    if-eqz v0, :cond_2

    .line 228150
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "user_storage_encrypted_key."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/1I5;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 228151
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "user_storage_not_encrypted_key."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/1I5;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 228152
    iget-object v2, p0, LX/1I5;->f:LX/0WS;

    const-string v3, ""

    invoke-virtual {v2, v0, v3}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 228153
    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    .line 228154
    :cond_0
    iget-object v0, p0, LX/1I5;->f:LX/0WS;

    invoke-virtual {v0}, LX/0WS;->b()LX/1gW;

    move-result-object v0

    sget-object v2, LX/1I5;->c:LX/1I6;

    iget-object v3, p0, LX/1I5;->k:[B

    invoke-virtual {v2, v3}, LX/1I6;->a([B)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;

    move-result-object v0

    invoke-interface {v0}, LX/1gW;->b()Z

    .line 228155
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/1I5;->j:Ljava/lang/String;

    .line 228156
    iget-object v0, p0, LX/1I5;->k:[B

    invoke-static {v0}, LX/1I5;->c([B)V

    .line 228157
    const/4 v0, 0x0

    iput-object v0, p0, LX/1I5;->k:[B

    .line 228158
    const-string v0, "user_storage_device_key"

    invoke-static {p0, v0}, LX/1I5;->c(LX/1I5;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228159
    :cond_2
    monitor-exit p0

    return-void

    .line 228160
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()[B
    .locals 5

    .prologue
    .line 228138
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1I5;->k:[B

    if-nez v0, :cond_0

    .line 228139
    iget-object v0, p0, LX/1I5;->h:LX/03V;

    sget-object v1, LX/1I5;->a:Ljava/lang/String;

    const-string v2, "Key is not configured"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 228140
    new-instance v0, LX/48g;

    const-string v1, "Key is not configured"

    invoke-direct {v0, v1}, LX/48g;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228141
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 228142
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1I5;->k:[B

    invoke-static {v0}, LX/1I5;->a([B)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 228143
    invoke-static {p0}, LX/1I5;->e(LX/1I5;)[B

    move-result-object v0

    .line 228144
    iget-object v1, p0, LX/1I5;->f:LX/0WS;

    invoke-virtual {v1}, LX/0WS;->b()LX/1gW;

    move-result-object v1

    .line 228145
    const-string v2, "user_storage_device_key"

    sget-object v3, LX/1I5;->c:LX/1I6;

    iget-object v4, p0, LX/1I5;->k:[B

    invoke-virtual {v3, v4}, LX/1I6;->a([B)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;

    .line 228146
    invoke-interface {v1}, LX/1gW;->b()Z

    .line 228147
    iput-object v0, p0, LX/1I5;->k:[B

    .line 228148
    :cond_1
    iget-object v0, p0, LX/1I5;->k:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public final b()[B
    .locals 2

    .prologue
    .line 228135
    sget-object v0, LX/1Hy;->KEY_256:LX/1Hy;

    iget v0, v0, LX/1Hy;->ivLength:I

    new-array v0, v0, [B

    .line 228136
    iget-object v1, p0, LX/1I5;->g:Ljava/security/SecureRandom;

    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 228137
    return-object v0
.end method

.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 228134
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1I5;->k:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
