.class public LX/1p3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0lC;

.field private final b:LX/0lp;


# direct methods
.method public constructor <init>(LX/0lC;LX/0lp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 327738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 327739
    iput-object p1, p0, LX/1p3;->a:LX/0lC;

    .line 327740
    iput-object p2, p0, LX/1p3;->b:LX/0lp;

    .line 327741
    return-void
.end method

.method public static a(LX/0lF;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 327742
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 327743
    invoke-virtual {p0}, LX/0lF;->G()Ljava/util/Iterator;

    move-result-object v2

    .line 327744
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 327745
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 327746
    const-string v3, "matcher"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 327747
    const-string v4, "replacer"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 327748
    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 327749
    new-instance v4, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    invoke-virtual {v3}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v3, v0}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 327750
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1p3;
    .locals 3

    .prologue
    .line 327751
    new-instance v2, LX/1p3;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-static {p0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v1

    check-cast v1, LX/0lp;

    invoke-direct {v2, v0, v1}, LX/1p3;-><init>(LX/0lC;LX/0lp;)V

    .line 327752
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 327753
    iget-object v0, p0, LX/1p3;->b:LX/0lp;

    invoke-virtual {v0, p1}, LX/0lp;->b(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 327754
    iget-object v1, p0, LX/1p3;->a:LX/0lC;

    invoke-virtual {v1, v0}, LX/0lD;->a(LX/15w;)LX/0lG;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 327755
    invoke-static {v0}, LX/1p3;->a(LX/0lF;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Px;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 327756
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 327757
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    .line 327758
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 327759
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 327760
    :cond_0
    iget-object v0, p0, LX/1p3;->a:LX/0lC;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
