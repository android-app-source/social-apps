.class public LX/0xD;
.super LX/0hD;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/0xD;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private c:LX/0f7;

.field public d:Ljava/lang/String;

.field public e:LX/0dN;

.field public f:Lcom/facebook/apptab/ui/DownloadableImageTabView;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 162236
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 162237
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 162238
    iput-object v0, p0, LX/0xD;->a:LX/0Ot;

    .line 162239
    const-string v0, ""

    iput-object v0, p0, LX/0xD;->d:Ljava/lang/String;

    .line 162240
    return-void
.end method

.method public static a(LX/0QB;)LX/0xD;
    .locals 5

    .prologue
    .line 162241
    sget-object v0, LX/0xD;->g:LX/0xD;

    if-nez v0, :cond_1

    .line 162242
    const-class v1, LX/0xD;

    monitor-enter v1

    .line 162243
    :try_start_0
    sget-object v0, LX/0xD;->g:LX/0xD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 162244
    if-eqz v2, :cond_0

    .line 162245
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 162246
    new-instance v4, LX/0xD;

    invoke-direct {v4}, LX/0xD;-><init>()V

    .line 162247
    const/16 v3, 0xf9a

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    .line 162248
    iput-object p0, v4, LX/0xD;->a:LX/0Ot;

    iput-object v3, v4, LX/0xD;->b:LX/0Uh;

    .line 162249
    move-object v0, v4

    .line 162250
    sput-object v0, LX/0xD;->g:LX/0xD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 162251
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 162252
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 162253
    :cond_1
    sget-object v0, LX/0xD;->g:LX/0xD;

    return-object v0

    .line 162254
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 162255
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 162256
    iget-object v0, p0, LX/0xD;->b:LX/0Uh;

    const/16 v1, 0x3bb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 162257
    :cond_0
    :goto_0
    return-void

    .line 162258
    :cond_1
    instance-of v0, p1, LX/0f7;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 162259
    check-cast p1, LX/0f7;

    iput-object p1, p0, LX/0xD;->c:LX/0f7;

    .line 162260
    iget-object v0, p0, LX/0xD;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->M:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0xD;->d:Ljava/lang/String;

    .line 162261
    new-instance v0, LX/GvE;

    invoke-direct {v0, p0}, LX/GvE;-><init>(LX/0xD;)V

    iput-object v0, p0, LX/0xD;->e:LX/0dN;

    .line 162262
    iget-object v0, p0, LX/0xD;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->M:LX/0Tn;

    iget-object v2, p0, LX/0xD;->e:LX/0dN;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 162263
    sget-object v0, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    .line 162264
    iget-object v1, p0, LX/0xD;->c:LX/0f7;

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0f7;->a(Ljava/lang/String;)LX/0hO;

    move-result-object v0

    .line 162265
    if-nez v0, :cond_2

    .line 162266
    sget-object v0, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    .line 162267
    iget-object v1, p0, LX/0xD;->c:LX/0f7;

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0f7;->a(Ljava/lang/String;)LX/0hO;

    move-result-object v0

    .line 162268
    :cond_2
    instance-of v1, v0, Lcom/facebook/apptab/ui/DownloadableImageTabView;

    if-eqz v1, :cond_0

    .line 162269
    check-cast v0, Lcom/facebook/apptab/ui/DownloadableImageTabView;

    iput-object v0, p0, LX/0xD;->f:Lcom/facebook/apptab/ui/DownloadableImageTabView;

    goto :goto_0
.end method
