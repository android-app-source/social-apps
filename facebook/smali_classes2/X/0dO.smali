.class public LX/0dO;
.super LX/0dP;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final d:LX/0Tn;

.field private static volatile f:LX/0dO;


# instance fields
.field private final e:LX/0Or;
    .annotation runtime Lcom/facebook/dialtone/gk/IsDialtoneEligibleGK;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90282
    sget-object v0, LX/0dQ;->o:LX/0Tn;

    sput-object v0, LX/0dO;->d:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0yU;LX/0Or;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/gk/IsDialtoneEligibleGK;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0yU;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 90283
    sget-object v0, LX/0dO;->d:LX/0Tn;

    invoke-direct {p0, v0, p1}, LX/0dP;-><init>(LX/0Tn;LX/0yU;)V

    .line 90284
    iput-object p2, p0, LX/0dO;->e:LX/0Or;

    .line 90285
    return-void
.end method

.method public static a(LX/0QB;)LX/0dO;
    .locals 5

    .prologue
    .line 90286
    sget-object v0, LX/0dO;->f:LX/0dO;

    if-nez v0, :cond_1

    .line 90287
    const-class v1, LX/0dO;

    monitor-enter v1

    .line 90288
    :try_start_0
    sget-object v0, LX/0dO;->f:LX/0dO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 90289
    if-eqz v2, :cond_0

    .line 90290
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 90291
    new-instance v4, LX/0dO;

    invoke-static {v0}, LX/0yU;->b(LX/0QB;)LX/0yU;

    move-result-object v3

    check-cast v3, LX/0yU;

    const/16 p0, 0x312

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/0dO;-><init>(LX/0yU;LX/0Or;)V

    .line 90292
    move-object v0, v4

    .line 90293
    sput-object v0, LX/0dO;->f:LX/0dO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90294
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 90295
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 90296
    :cond_1
    sget-object v0, LX/0dO;->f:LX/0dO;

    return-object v0

    .line 90297
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 90298
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0yY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90299
    iget-object v0, p0, LX/0dO;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 90300
    const/4 v0, 0x0

    iput-object v0, p0, LX/0dO;->b:LX/0Rf;

    .line 90301
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 90302
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, LX/0dP;->a()LX/0Rf;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 90303
    sget-object v0, LX/0yY;->sDialtoneFeatureKeys:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yY;

    .line 90304
    invoke-virtual {p0}, LX/0dO;->a()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90305
    const/4 v0, 0x1

    .line 90306
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
