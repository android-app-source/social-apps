.class public LX/1lW;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1lW;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 311995
    new-instance v0, LX/1lW;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/1lW;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1lW;->a:LX/1lW;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 311996
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311997
    iput-object p1, p0, LX/1lW;->c:Ljava/lang/String;

    .line 311998
    iput-object p2, p0, LX/1lW;->b:Ljava/lang/String;

    .line 311999
    return-void
.end method

.method public static a(LX/1lW;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 312000
    iget-object v0, p0, LX/1lW;->b:Ljava/lang/String;

    move-object v0, v0

    .line 312001
    if-nez v0, :cond_0

    .line 312002
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown image format "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 312003
    iget-object v2, p0, LX/1lW;->c:Ljava/lang/String;

    move-object v2, v2

    .line 312004
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 312005
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 312006
    iget-object v0, p0, LX/1lW;->c:Ljava/lang/String;

    move-object v0, v0

    .line 312007
    return-object v0
.end method
