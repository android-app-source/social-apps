.class public abstract LX/0Ye;
.super Landroid/content/BroadcastReceiver;
.source ""

# interfaces
.implements LX/0Yf;


# instance fields
.field public final a:LX/0Sk;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 81797
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/0Ye;-><init>(LX/0Sk;)V

    .line 81798
    return-void
.end method

.method public constructor <init>(LX/0Sk;)V
    .locals 0
    .param p1    # LX/0Sk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 81799
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 81800
    iput-object p1, p0, LX/0Ye;->a:LX/0Sk;

    .line 81801
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;Landroid/content/Intent;)LX/0YZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;)Z
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 81802
    const/4 v0, 0x1

    return v0
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x2

    const/16 v0, 0x26

    const v1, -0x608f15bc

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 81803
    iget-object v0, p0, LX/0Ye;->a:LX/0Sk;

    move-object v5, v0

    .line 81804
    const-wide/16 v0, -0x1

    .line 81805
    if-eqz v5, :cond_0

    .line 81806
    iget-object v7, v5, LX/0Sk;->a:LX/0So;

    invoke-interface {v7}, LX/0So;->now()J

    move-result-wide v7

    move-wide v0, v7

    .line 81807
    :cond_0
    :try_start_0
    invoke-virtual {p0}, LX/0Ye;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    .line 81808
    if-eqz v5, :cond_1

    .line 81809
    invoke-virtual {v5, v0, v1, p2, v3}, LX/0Sk;->a(JLandroid/content/Intent;Ljava/lang/Class;)V

    :cond_1
    const/16 v0, 0x27

    const v1, -0x29137512

    invoke-static {p2, v6, v0, v1, v4}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 81810
    :goto_0
    return-void

    .line 81811
    :cond_2
    :try_start_1
    invoke-virtual {p0, p1, p2}, LX/0Ye;->a(Landroid/content/Context;Landroid/content/Intent;)LX/0YZ;

    move-result-object v2

    .line 81812
    if-eqz v2, :cond_5

    .line 81813
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 81814
    invoke-interface {v2, p1, p2, p0}, LX/0YZ;->onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81815
    :cond_3
    :goto_1
    if-eqz v5, :cond_4

    .line 81816
    invoke-virtual {v5, v0, v1, p2, v3}, LX/0Sk;->a(JLandroid/content/Intent;Ljava/lang/Class;)V

    .line 81817
    :cond_4
    const v0, 0x77707ba3

    invoke-static {p2, v0, v4}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 81818
    :cond_5
    :try_start_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/0Ye;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 81819
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    .line 81820
    invoke-static {v2}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    .line 81821
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    move-object v6, v6

    .line 81822
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Rejected the intent for the receiver because it was not registered: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 81823
    goto :goto_1

    .line 81824
    :catchall_0
    move-exception v2

    if-eqz v5, :cond_6

    .line 81825
    invoke-virtual {v5, v0, v1, p2, v3}, LX/0Sk;->a(JLandroid/content/Intent;Ljava/lang/Class;)V

    :cond_6
    const v0, 0x70693f3a

    invoke-static {p2, v0, v4}, LX/02F;->a(Landroid/content/Intent;II)V

    throw v2
.end method
