.class public LX/0lb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0lc;
.implements Ljava/io/Serializable;


# instance fields
.field public _quotedChars:[C

.field public _quotedUTF8Ref:[B

.field public _unquotedUTF8Ref:[B

.field public final _value:Ljava/lang/String;

.field public transient a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 130080
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130081
    if-nez p1, :cond_0

    .line 130082
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Null String illegal for SerializedString"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130083
    :cond_0
    iput-object p1, p0, LX/0lb;->_value:Ljava/lang/String;

    .line 130084
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    .prologue
    .line 130078
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0lb;->a:Ljava/lang/String;

    .line 130079
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    .prologue
    .line 130076
    iget-object v0, p0, LX/0lb;->_value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 130077
    return-void
.end method


# virtual methods
.method public final a([BI)I
    .locals 4

    .prologue
    .line 130066
    iget-object v0, p0, LX/0lb;->_quotedUTF8Ref:[B

    .line 130067
    if-nez v0, :cond_0

    .line 130068
    invoke-static {}, LX/2BC;->a()LX/2BC;

    move-result-object v0

    iget-object v1, p0, LX/0lb;->_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2BC;->b(Ljava/lang/String;)[B

    move-result-object v0

    .line 130069
    iput-object v0, p0, LX/0lb;->_quotedUTF8Ref:[B

    .line 130070
    :cond_0
    array-length v1, v0

    .line 130071
    add-int v2, p2, v1

    array-length v3, p1

    if-le v2, v3, :cond_1

    .line 130072
    const/4 v0, -0x1

    .line 130073
    :goto_0
    return v0

    .line 130074
    :cond_1
    const/4 v2, 0x0

    invoke-static {v0, v2, p1, p2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v0, v1

    .line 130075
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130065
    iget-object v0, p0, LX/0lb;->_value:Ljava/lang/String;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 130041
    iget-object v0, p0, LX/0lb;->_value:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    return v0
.end method

.method public final c()[C
    .locals 2

    .prologue
    .line 130060
    iget-object v0, p0, LX/0lb;->_quotedChars:[C

    .line 130061
    if-nez v0, :cond_0

    .line 130062
    invoke-static {}, LX/2BC;->a()LX/2BC;

    move-result-object v0

    iget-object v1, p0, LX/0lb;->_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2BC;->a(Ljava/lang/String;)[C

    move-result-object v0

    .line 130063
    iput-object v0, p0, LX/0lb;->_quotedChars:[C

    .line 130064
    :cond_0
    return-object v0
.end method

.method public final d()[B
    .locals 2

    .prologue
    .line 130055
    iget-object v0, p0, LX/0lb;->_unquotedUTF8Ref:[B

    .line 130056
    if-nez v0, :cond_0

    .line 130057
    invoke-static {}, LX/2BC;->a()LX/2BC;

    move-result-object v0

    iget-object v1, p0, LX/0lb;->_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2BC;->c(Ljava/lang/String;)[B

    move-result-object v0

    .line 130058
    iput-object v0, p0, LX/0lb;->_unquotedUTF8Ref:[B

    .line 130059
    :cond_0
    return-object v0
.end method

.method public final e()[B
    .locals 2

    .prologue
    .line 130050
    iget-object v0, p0, LX/0lb;->_quotedUTF8Ref:[B

    .line 130051
    if-nez v0, :cond_0

    .line 130052
    invoke-static {}, LX/2BC;->a()LX/2BC;

    move-result-object v0

    iget-object v1, p0, LX/0lb;->_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2BC;->b(Ljava/lang/String;)[B

    move-result-object v0

    .line 130053
    iput-object v0, p0, LX/0lb;->_quotedUTF8Ref:[B

    .line 130054
    :cond_0
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 130045
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    .line 130046
    :goto_0
    return v0

    .line 130047
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 130048
    :cond_2
    check-cast p1, LX/0lb;

    .line 130049
    iget-object v0, p0, LX/0lb;->_value:Ljava/lang/String;

    iget-object v1, p1, LX/0lb;->_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 130044
    iget-object v0, p0, LX/0lb;->_value:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public readResolve()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 130043
    new-instance v0, LX/0lb;

    iget-object v1, p0, LX/0lb;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, LX/0lb;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130042
    iget-object v0, p0, LX/0lb;->_value:Ljava/lang/String;

    return-object v0
.end method
