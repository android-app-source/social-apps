.class public final LX/1rH;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "LX/12J;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1rH;


# instance fields
.field public a:Landroid/os/Handler;


# direct methods
.method public constructor <init>(LX/0Ot;Landroid/os/Handler;)V
    .locals 0
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/12J;",
            ">;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 331927
    invoke-direct {p0, p1}, LX/0aT;-><init>(LX/0Ot;)V

    .line 331928
    iput-object p2, p0, LX/1rH;->a:Landroid/os/Handler;

    .line 331929
    return-void
.end method

.method public static a(LX/0QB;)LX/1rH;
    .locals 5

    .prologue
    .line 331930
    sget-object v0, LX/1rH;->b:LX/1rH;

    if-nez v0, :cond_1

    .line 331931
    const-class v1, LX/1rH;

    monitor-enter v1

    .line 331932
    :try_start_0
    sget-object v0, LX/1rH;->b:LX/1rH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 331933
    if-eqz v2, :cond_0

    .line 331934
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 331935
    new-instance v4, LX/1rH;

    const/16 v3, 0x4f4

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0Zw;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    invoke-direct {v4, p0, v3}, LX/1rH;-><init>(LX/0Ot;Landroid/os/Handler;)V

    .line 331936
    move-object v0, v4

    .line 331937
    sput-object v0, LX/1rH;->b:LX/1rH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331938
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 331939
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 331940
    :cond_1
    sget-object v0, LX/1rH;->b:LX/1rH;

    return-object v0

    .line 331941
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 331942
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 331943
    check-cast p3, LX/12J;

    .line 331944
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 331945
    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331946
    iget-object v0, p0, LX/1rH;->a:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/dialtone/zerobalance/ZeroBalanceController$LocalZeroBalanceControllerReceiverRegistration$1;

    invoke-direct {v1, p0, p3}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceController$LocalZeroBalanceControllerReceiverRegistration$1;-><init>(LX/1rH;LX/12J;)V

    const-wide/16 v2, 0x7d0

    const v4, -0x4f81a2c8

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 331947
    :cond_0
    return-void
.end method
