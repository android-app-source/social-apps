.class public LX/0Zx;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Landroid/os/Looper;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Landroid/os/Looper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 84229
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Landroid/os/Looper;
    .locals 3

    .prologue
    .line 84230
    sget-object v0, LX/0Zx;->a:Landroid/os/Looper;

    if-nez v0, :cond_1

    .line 84231
    const-class v1, LX/0Zx;

    monitor-enter v1

    .line 84232
    :try_start_0
    sget-object v0, LX/0Zx;->a:Landroid/os/Looper;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 84233
    if-eqz v2, :cond_0

    .line 84234
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 84235
    invoke-static {v0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object p0

    check-cast p0, LX/0Zr;

    invoke-static {p0}, LX/0Zy;->a(LX/0Zr;)Landroid/os/Looper;

    move-result-object p0

    move-object v0, p0

    .line 84236
    sput-object v0, LX/0Zx;->a:Landroid/os/Looper;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84237
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 84238
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 84239
    :cond_1
    sget-object v0, LX/0Zx;->a:Landroid/os/Looper;

    return-object v0

    .line 84240
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 84241
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 84242
    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v0

    check-cast v0, LX/0Zr;

    invoke-static {v0}, LX/0Zy;->a(LX/0Zr;)Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method
