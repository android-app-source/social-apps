.class public final LX/1Yr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1J7;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/1Yh;


# direct methods
.method public constructor <init>(LX/1Yh;I)V
    .locals 0

    .prologue
    .line 274065
    iput-object p1, p0, LX/1Yr;->b:LX/1Yh;

    iput p2, p0, LX/1Yr;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 274066
    iget v0, p0, LX/1Yr;->a:I

    return v0
.end method

.method public final a(LX/0g8;I)V
    .locals 0

    .prologue
    .line 274067
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 4

    .prologue
    .line 274068
    iget-object v0, p0, LX/1Yr;->b:LX/1Yh;

    .line 274069
    if-nez p4, :cond_1

    .line 274070
    :cond_0
    :goto_0
    return-void

    .line 274071
    :cond_1
    iget v1, v0, LX/1Yh;->n:I

    if-ne v1, p2, :cond_2

    iget v1, v0, LX/1Yh;->o:I

    add-int v2, p2, p3

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_2

    iget v1, v0, LX/1Yh;->q:I

    if-eq v1, p4, :cond_0

    .line 274072
    :cond_2
    iput p2, v0, LX/1Yh;->n:I

    .line 274073
    add-int v1, p2, p3

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, LX/1Yh;->o:I

    .line 274074
    iput p4, v0, LX/1Yh;->q:I

    .line 274075
    iget-object v1, v0, LX/1Yh;->h:LX/1Yp;

    invoke-interface {v1}, LX/1Yp;->a()LX/1Yq;

    move-result-object v1

    .line 274076
    iget v2, v0, LX/1Yh;->m:I

    .line 274077
    iget v3, v0, LX/1Yh;->p:I

    .line 274078
    iget p0, v0, LX/1Yh;->n:I

    iget p1, v1, LX/1Yq;->a:I

    sub-int/2addr p0, p1

    const/4 p1, 0x0

    invoke-static {p0, p1}, Ljava/lang/Math;->max(II)I

    move-result p0

    iput p0, v0, LX/1Yh;->m:I

    .line 274079
    iget p0, v0, LX/1Yh;->o:I

    iget v1, v1, LX/1Yq;->b:I

    add-int/2addr v1, p0

    add-int/lit8 p0, p4, -0x1

    invoke-static {v1, p0}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v0, LX/1Yh;->p:I

    .line 274080
    iget v1, v0, LX/1Yh;->m:I

    if-ne v1, v2, :cond_3

    iget v1, v0, LX/1Yh;->p:I

    if-eq v1, v3, :cond_0

    .line 274081
    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/1Yh;->l:Z

    move v1, v2

    .line 274082
    :goto_1
    iget p0, v0, LX/1Yh;->m:I

    if-ge v1, p0, :cond_5

    .line 274083
    if-ltz v1, :cond_4

    iget p0, v0, LX/1Yh;->q:I

    if-ge v1, p0, :cond_4

    .line 274084
    invoke-virtual {v0, v1}, LX/1Yh;->c(I)V

    .line 274085
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 274086
    :cond_5
    iget v1, v0, LX/1Yh;->p:I

    add-int/lit8 v1, v1, 0x1

    :goto_2
    if-gt v1, v3, :cond_7

    .line 274087
    if-ltz v1, :cond_6

    iget p0, v0, LX/1Yh;->q:I

    if-ge v1, p0, :cond_6

    .line 274088
    invoke-virtual {v0, v1}, LX/1Yh;->c(I)V

    .line 274089
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 274090
    :cond_7
    iget v1, v0, LX/1Yh;->m:I

    if-lt v1, v2, :cond_8

    iget v1, v0, LX/1Yh;->p:I

    if-le v1, v3, :cond_0

    .line 274091
    :cond_8
    invoke-static {v0}, LX/1Yh;->b(LX/1Yh;)V

    goto :goto_0
.end method
