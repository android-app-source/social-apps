.class public LX/1Z3;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0fm;
.implements LX/0fy;


# instance fields
.field private final a:LX/1Z4;

.field public final b:LX/0ad;

.field public c:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Z4;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 274262
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 274263
    iput-object p1, p0, LX/1Z3;->a:LX/1Z4;

    .line 274264
    iput-object p2, p0, LX/1Z3;->b:LX/0ad;

    .line 274265
    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 0

    .prologue
    .line 274279
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 1

    .prologue
    .line 274272
    iget-object v0, p0, LX/1Z3;->a:LX/1Z4;

    invoke-virtual {v0}, LX/1Z4;->b()V

    .line 274273
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 274274
    iget-object v0, p0, LX/1Z3;->a:LX/1Z4;

    check-cast p1, Landroid/widget/FrameLayout;

    const/4 p0, -0x1

    .line 274275
    invoke-virtual {v0}, LX/1Z4;->a()V

    .line 274276
    iget-object v1, v0, LX/1Z4;->c:LX/1Z5;

    invoke-virtual {p1, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 274277
    iget-object v1, v0, LX/1Z4;->c:LX/1Z5;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, p0, p0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, LX/1Z5;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 274278
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 274270
    iget-object v0, p0, LX/1Z3;->a:LX/1Z4;

    invoke-virtual {v0}, LX/1Z4;->a()V

    .line 274271
    return-void
.end method

.method public final kw_()Z
    .locals 3

    .prologue
    .line 274266
    iget-object v0, p0, LX/1Z3;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 274267
    iget-object v0, p0, LX/1Z3;->b:LX/0ad;

    sget-short v1, LX/1Dd;->z:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1Z3;->c:Ljava/lang/Boolean;

    .line 274268
    :cond_0
    iget-object v0, p0, LX/1Z3;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v0, v0

    .line 274269
    return v0
.end method
