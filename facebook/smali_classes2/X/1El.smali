.class public LX/1El;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile h:LX/1El;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/AFG;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/AF1;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 221114
    const-class v0, LX/1El;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1El;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 221104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221105
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1El;->b:Ljava/util/Map;

    .line 221106
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1El;->c:Ljava/util/Map;

    .line 221107
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1El;->d:Ljava/util/Set;

    .line 221108
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1El;->e:Ljava/util/Set;

    .line 221109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1El;->f:Ljava/util/List;

    .line 221110
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 221111
    iget-object p1, v0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v0, p1

    .line 221112
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1El;->g:Ljava/lang/String;

    .line 221113
    return-void
.end method

.method public static a(LX/0QB;)LX/1El;
    .locals 4

    .prologue
    .line 221091
    sget-object v0, LX/1El;->h:LX/1El;

    if-nez v0, :cond_1

    .line 221092
    const-class v1, LX/1El;

    monitor-enter v1

    .line 221093
    :try_start_0
    sget-object v0, LX/1El;->h:LX/1El;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 221094
    if-eqz v2, :cond_0

    .line 221095
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 221096
    new-instance v3, LX/1El;

    const/16 p0, 0x12cb

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1El;-><init>(LX/0Or;)V

    .line 221097
    move-object v0, v3

    .line 221098
    sput-object v0, LX/1El;->h:LX/1El;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 221099
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 221100
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 221101
    :cond_1
    sget-object v0, LX/1El;->h:LX/1El;

    return-object v0

    .line 221102
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 221103
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1El;LX/7h0;)LX/7h0;
    .locals 8

    .prologue
    .line 221062
    iget-object v0, p1, LX/7h0;->i:Ljava/lang/String;

    move-object v0, v0

    .line 221063
    invoke-static {p0, v0}, LX/1El;->f(LX/1El;Ljava/lang/String;)Lcom/facebook/audience/model/Reply;

    move-result-object v0

    .line 221064
    if-eqz v0, :cond_0

    .line 221065
    iget-wide v6, p1, LX/7h0;->c:J

    move-wide v2, v6

    .line 221066
    iget-wide v6, v0, Lcom/facebook/audience/model/Reply;->i:J

    move-wide v4, v6

    .line 221067
    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 221068
    :cond_0
    :goto_0
    return-object p1

    .line 221069
    :cond_1
    new-instance v1, LX/7gx;

    invoke-direct {v1, p1}, LX/7gx;-><init>(LX/7h0;)V

    .line 221070
    sget-object v2, LX/7gy;->OUTOING_REPLY:LX/7gy;

    .line 221071
    iput-object v2, v1, LX/7gx;->b:LX/7gy;

    .line 221072
    move-object v1, v1

    .line 221073
    iget-wide v6, v0, Lcom/facebook/audience/model/Reply;->i:J

    move-wide v2, v6

    .line 221074
    iput-wide v2, v1, LX/7gx;->c:J

    .line 221075
    move-object v1, v1

    .line 221076
    iget-boolean v2, p1, LX/7h0;->h:Z

    move v2, v2

    .line 221077
    iput-boolean v2, v1, LX/7gx;->h:Z

    .line 221078
    move-object v1, v1

    .line 221079
    iget-object v2, v0, Lcom/facebook/audience/model/Reply;->g:Ljava/lang/String;

    move-object v0, v2

    .line 221080
    invoke-static {p0, v0}, LX/1El;->g(LX/1El;Ljava/lang/String;)LX/7gq;

    move-result-object v2

    .line 221081
    sget-object v3, LX/AFF;->a:[I

    invoke-virtual {v2}, LX/7gq;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 221082
    sget-object v2, LX/7gz;->SENDING:LX/7gz;

    :goto_1
    move-object v0, v2

    .line 221083
    iput-object v0, v1, LX/7gx;->a:LX/7gz;

    .line 221084
    move-object v0, v1

    .line 221085
    iget-object v1, p1, LX/7h0;->b:LX/7gy;

    move-object v1, v1

    .line 221086
    iput-object v1, v0, LX/7gx;->n:LX/7gy;

    .line 221087
    move-object v0, v0

    .line 221088
    invoke-virtual {v0}, LX/7gx;->a()LX/7h0;

    move-result-object p1

    goto :goto_0

    .line 221089
    :pswitch_0
    sget-object v2, LX/7gz;->SENT:LX/7gz;

    goto :goto_1

    .line 221090
    :pswitch_1
    sget-object v2, LX/7gz;->FAILED:LX/7gz;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static declared-synchronized a(LX/1El;Ljava/lang/String;Lcom/facebook/audience/model/UploadShot;)Lcom/facebook/audience/model/Reply;
    .locals 5

    .prologue
    .line 221030
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/facebook/audience/model/Reply;->newBuilder()LX/7go;

    move-result-object v1

    .line 221031
    iput-object p1, v1, LX/7go;->g:Ljava/lang/String;

    .line 221032
    move-object v0, v1

    .line 221033
    invoke-virtual {p2}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/7h8;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/7gp;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 221034
    iput-object v2, v0, LX/7go;->b:Landroid/net/Uri;

    .line 221035
    move-object v0, v0

    .line 221036
    invoke-virtual {p2}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/7h8;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/7gp;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 221037
    iput-object v2, v0, LX/7go;->k:Landroid/net/Uri;

    .line 221038
    move-object v2, v0

    .line 221039
    invoke-virtual {p2}, Lcom/facebook/audience/model/UploadShot;->getCaption()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/audience/model/UploadShot;->getCaption()Ljava/lang/String;

    move-result-object v0

    .line 221040
    :goto_0
    iput-object v0, v2, LX/7go;->d:Ljava/lang/String;

    .line 221041
    move-object v0, v2

    .line 221042
    invoke-virtual {p2}, Lcom/facebook/audience/model/UploadShot;->getCreatedAtTime()J

    move-result-wide v2

    .line 221043
    iput-wide v2, v0, LX/7go;->i:J

    .line 221044
    move-object v0, v0

    .line 221045
    invoke-virtual {p2}, Lcom/facebook/audience/model/UploadShot;->getTimezoneOffset()J

    move-result-wide v2

    .line 221046
    iput-wide v2, v0, LX/7go;->j:J

    .line 221047
    move-object v0, v0

    .line 221048
    const/high16 v2, 0x3f800000    # 1.0f

    .line 221049
    iput v2, v0, LX/7go;->a:F

    .line 221050
    move-object v0, v0

    .line 221051
    const/4 v2, 0x1

    .line 221052
    iput-boolean v2, v0, LX/7go;->c:Z

    .line 221053
    move-object v0, v0

    .line 221054
    invoke-static {p0, p1}, LX/1El;->g(LX/1El;Ljava/lang/String;)LX/7gq;

    move-result-object v2

    .line 221055
    iput-object v2, v0, LX/7go;->h:LX/7gq;

    .line 221056
    move-object v0, v0

    .line 221057
    iget-object v2, p0, LX/1El;->g:Ljava/lang/String;

    .line 221058
    iput-object v2, v0, LX/7go;->e:Ljava/lang/String;

    .line 221059
    invoke-virtual {v1}, LX/7go;->a()Lcom/facebook/audience/model/Reply;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 221060
    :cond_0
    :try_start_1
    const-string v0, ""
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 221061
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 221027
    iget-object v0, p0, LX/1El;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AF1;

    .line 221028
    invoke-interface {v0, p1}, LX/AF1;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 221029
    :cond_0
    return-void
.end method

.method private static declared-synchronized d(LX/1El;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 221026
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1El;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized e(LX/1El;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 221015
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1El;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AFG;

    .line 221016
    iget-object v1, p0, LX/1El;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 221017
    iget-object v1, p0, LX/1El;->b:Ljava/util/Map;

    iget-object v2, v0, LX/AFG;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 221018
    iget-object v1, p0, LX/1El;->b:Ljava/util/Map;

    iget-object v2, v0, LX/AFG;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 221019
    iget-object v1, p0, LX/1El;->b:Ljava/util/Map;

    iget-object v0, v0, LX/AFG;->b:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 221020
    :cond_0
    iget-object v0, p0, LX/1El;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221021
    iget-object v0, p0, LX/1El;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 221022
    :cond_1
    iget-object v0, p0, LX/1El;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 221023
    iget-object v0, p0, LX/1El;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221024
    :cond_2
    monitor-exit p0

    return-void

    .line 221025
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized f(LX/1El;Ljava/lang/String;)Lcom/facebook/audience/model/Reply;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 221004
    monitor-enter p0

    :try_start_0
    new-instance v4, Ljava/util/Date;

    const-wide/16 v2, 0x0

    invoke-direct {v4, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 221005
    iget-object v0, p0, LX/1El;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 221006
    :goto_0
    monitor-exit p0

    return-object v0

    .line 221007
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1El;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v2, v1

    move-object v3, v1

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 221008
    iget-object v1, p0, LX/1El;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AFG;

    .line 221009
    iget-object v6, v1, LX/AFG;->a:Lcom/facebook/audience/model/UploadShot;

    invoke-static {v6}, LX/7h8;->a(Lcom/facebook/audience/model/UploadShot;)LX/7h5;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/7h5;->after(Ljava/util/Date;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 221010
    iget-object v2, v1, LX/AFG;->a:Lcom/facebook/audience/model/UploadShot;

    invoke-static {v2}, LX/7h8;->a(Lcom/facebook/audience/model/UploadShot;)LX/7h5;

    move-result-object v2

    .line 221011
    iget-object v1, v1, LX/AFG;->a:Lcom/facebook/audience/model/UploadShot;

    :goto_2
    move-object v3, v1

    move-object v4, v2

    move-object v2, v0

    .line 221012
    goto :goto_1

    .line 221013
    :cond_1
    invoke-static {p0, v2, v3}, LX/1El;->a(LX/1El;Ljava/lang/String;Lcom/facebook/audience/model/UploadShot;)Lcom/facebook/audience/model/Reply;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 221014
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move-object v0, v2

    move-object v1, v3

    move-object v2, v4

    goto :goto_2
.end method

.method public static declared-synchronized g(LX/1El;Ljava/lang/String;)LX/7gq;
    .locals 1

    .prologue
    .line 220950
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1El;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220951
    sget-object v0, LX/7gq;->SENT:LX/7gq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220952
    :goto_0
    monitor-exit p0

    return-object v0

    .line 220953
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1El;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 220954
    sget-object v0, LX/7gq;->FAILED:LX/7gq;

    goto :goto_0

    .line 220955
    :cond_1
    sget-object v0, LX/7gq;->SENDING:LX/7gq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 220956
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Lcom/facebook/audience/model/UploadShot;
    .locals 1

    .prologue
    .line 221001
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1El;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AFG;

    .line 221002
    iget-object p1, v0, LX/AFG;->a:Lcom/facebook/audience/model/UploadShot;

    move-object v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221003
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/AF1;)V
    .locals 1

    .prologue
    .line 220999
    iget-object v0, p0, LX/1El;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221000
    return-void
.end method

.method public final declared-synchronized a(Lcom/facebook/audience/model/ReplyThread;)V
    .locals 5

    .prologue
    .line 220989
    monitor-enter p0

    .line 220990
    :try_start_0
    iget-object v0, p1, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    move-object v2, v0

    .line 220991
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/Reply;

    .line 220992
    iget-object v4, v0, Lcom/facebook/audience/model/Reply;->g:Ljava/lang/String;

    move-object v4, v4

    .line 220993
    invoke-static {p0, v4}, LX/1El;->d(LX/1El;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 220994
    iget-object v4, v0, Lcom/facebook/audience/model/Reply;->g:Ljava/lang/String;

    move-object v0, v4

    .line 220995
    invoke-static {p0, v0}, LX/1El;->e(LX/1El;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220996
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 220997
    :cond_1
    monitor-exit p0

    return-void

    .line 220998
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/audience/model/UploadShot;)V
    .locals 2

    .prologue
    .line 220985
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1El;->e:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 220986
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getReplyThreadId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1El;->c(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220987
    monitor-exit p0

    return-void

    .line 220988
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 220975
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1El;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AFG;

    .line 220976
    iget-object v2, v0, LX/AFG;->b:Ljava/lang/String;

    .line 220977
    iget-object v1, p0, LX/1El;->b:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 220978
    iget-object v1, p0, LX/1El;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 220979
    iget-object v1, p0, LX/1El;->b:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 220980
    iget-object v1, p0, LX/1El;->c:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220981
    iget-object v0, p0, LX/1El;->d:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 220982
    invoke-direct {p0, v2}, LX/1El;->c(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220983
    monitor-exit p0

    return-void

    .line 220984
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/audience/model/UploadShot;)V
    .locals 2

    .prologue
    .line 220966
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p2}, LX/1El;->d(LX/1El;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220967
    invoke-static {p0, p2}, LX/1El;->e(LX/1El;Ljava/lang/String;)V

    .line 220968
    :cond_0
    iget-object v0, p0, LX/1El;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 220969
    iget-object v0, p0, LX/1El;->b:Ljava/util/Map;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220970
    :cond_1
    iget-object v0, p0, LX/1El;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 220971
    iget-object v0, p0, LX/1El;->c:Ljava/util/Map;

    new-instance v1, LX/AFG;

    invoke-direct {v1, p3, p1}, LX/AFG;-><init>(Lcom/facebook/audience/model/UploadShot;Ljava/lang/String;)V

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220972
    invoke-direct {p0, p1}, LX/1El;->c(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220973
    monitor-exit p0

    return-void

    .line 220974
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 220965
    iget-object v0, p0, LX/1El;->e:Ljava/util/Set;

    iget-object v1, p0, LX/1El;->e:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/Reply;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220957
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1El;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 220958
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220959
    :goto_0
    monitor-exit p0

    return-object v0

    .line 220960
    :cond_0
    :try_start_1
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 220961
    iget-object v0, p0, LX/1El;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 220962
    iget-object v1, p0, LX/1El;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AFG;

    iget-object v1, v1, LX/AFG;->a:Lcom/facebook/audience/model/UploadShot;

    invoke-static {p0, v0, v1}, LX/1El;->a(LX/1El;Ljava/lang/String;Lcom/facebook/audience/model/UploadShot;)Lcom/facebook/audience/model/Reply;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 220963
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 220964
    :cond_1
    :try_start_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto :goto_0
.end method
