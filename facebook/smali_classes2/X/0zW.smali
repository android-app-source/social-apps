.class public LX/0zW;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/lang/Object;

.field private c:Ljava/lang/String;

.field private volatile d:LX/15I;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private volatile e:Lcom/facebook/http/interfaces/RequestPriority;

.field private f:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/facebook/http/interfaces/RequestPriority;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mPriorityLock"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 167339
    const-class v0, LX/0zW;

    sput-object v0, LX/0zW;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 167337
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/0zW;-><init>(Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 167338
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 2
    .param p2    # Lcom/facebook/http/interfaces/RequestPriority;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 167331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167332
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/0zW;->b:Ljava/lang/Object;

    .line 167333
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/0zW;->c:Ljava/lang/String;

    .line 167334
    iput-object p2, p0, LX/0zW;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 167335
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/0zW;->f:Ljava/util/concurrent/atomic/AtomicReference;

    .line 167336
    return-void
.end method

.method private static a(LX/0zW;Lcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 2

    .prologue
    .line 167326
    iget-object v1, p0, LX/0zW;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 167327
    :try_start_0
    iput-object p1, p0, LX/0zW;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 167328
    const/4 v0, 0x0

    iput-object v0, p0, LX/0zW;->g:Lcom/facebook/http/interfaces/RequestPriority;

    .line 167329
    iget-object v0, p0, LX/0zW;->d:LX/15I;

    invoke-interface {v0, p1}, LX/15I;->a(Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 167330
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/http/interfaces/RequestPriority;
    .locals 1

    .prologue
    .line 167320
    iget-object v0, p0, LX/0zW;->e:Lcom/facebook/http/interfaces/RequestPriority;

    if-nez v0, :cond_1

    .line 167321
    iget-object v0, p0, LX/0zW;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/interfaces/RequestPriority;

    .line 167322
    if-eqz v0, :cond_0

    .line 167323
    :goto_0
    return-object v0

    .line 167324
    :cond_0
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0

    .line 167325
    :cond_1
    iget-object v0, p0, LX/0zW;->e:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0
.end method

.method public final a(LX/15I;)V
    .locals 3

    .prologue
    .line 167298
    iget-object v1, p0, LX/0zW;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 167299
    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15I;

    iput-object v0, p0, LX/0zW;->d:LX/15I;

    .line 167300
    iget-object v0, p0, LX/0zW;->g:Lcom/facebook/http/interfaces/RequestPriority;

    if-eqz v0, :cond_0

    .line 167301
    iget-object v0, p0, LX/0zW;->g:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {p0}, LX/0zW;->a()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v2

    invoke-static {p0, v0, v2}, LX/0zW;->a(LX/0zW;Lcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 167302
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 0

    .prologue
    .line 167317
    if-nez p1, :cond_0

    .line 167318
    :goto_0
    return-void

    .line 167319
    :cond_0
    iput-object p1, p0, LX/0zW;->e:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0
.end method

.method public final b(Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 2
    .param p1    # Lcom/facebook/http/interfaces/RequestPriority;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 167314
    if-nez p1, :cond_0

    .line 167315
    :goto_0
    return-void

    .line 167316
    :cond_0
    iget-object v0, p0, LX/0zW;->f:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final c(Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 3

    .prologue
    .line 167303
    const-string v0, "Cannot change priority to null"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167304
    iget-object v1, p0, LX/0zW;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 167305
    :try_start_0
    iget-object v0, p0, LX/0zW;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 167306
    iput-object p1, p0, LX/0zW;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 167307
    iget-object v2, p0, LX/0zW;->d:LX/15I;

    if-nez v2, :cond_0

    .line 167308
    iput-object p1, p0, LX/0zW;->g:Lcom/facebook/http/interfaces/RequestPriority;

    .line 167309
    monitor-exit v1

    .line 167310
    :goto_0
    return-void

    .line 167311
    :cond_0
    invoke-static {p0, p1, v0}, LX/0zW;->a(LX/0zW;Lcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 167312
    monitor-exit v1

    goto :goto_0

    .line 167313
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
