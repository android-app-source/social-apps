.class public LX/1Yl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile h:LX/1Yl;


# instance fields
.field public final b:LX/1Ym;

.field private final c:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Landroid/net/Uri;",
            "LX/7K1;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0So;

.field public e:LX/1m6;

.field public f:Landroid/net/Uri;

.field public g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 274010
    const-class v0, LX/1Yl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1Yl;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Ym;LX/0So;LX/0wq;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 274011
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 274012
    iput-object p1, p0, LX/1Yl;->b:LX/1Ym;

    .line 274013
    iput-object p2, p0, LX/1Yl;->d:LX/0So;

    .line 274014
    new-instance v0, LX/1Yn;

    iget v1, p3, LX/0wq;->t:I

    invoke-direct {v0, p0, v1}, LX/1Yn;-><init>(LX/1Yl;I)V

    iput-object v0, p0, LX/1Yl;->c:LX/0aq;

    .line 274015
    return-void
.end method

.method public static a(LX/0QB;)LX/1Yl;
    .locals 6

    .prologue
    .line 274016
    sget-object v0, LX/1Yl;->h:LX/1Yl;

    if-nez v0, :cond_1

    .line 274017
    const-class v1, LX/1Yl;

    monitor-enter v1

    .line 274018
    :try_start_0
    sget-object v0, LX/1Yl;->h:LX/1Yl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 274019
    if-eqz v2, :cond_0

    .line 274020
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 274021
    new-instance p0, LX/1Yl;

    const-class v3, LX/1Ym;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1Ym;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {v0}, LX/0wq;->b(LX/0QB;)LX/0wq;

    move-result-object v5

    check-cast v5, LX/0wq;

    invoke-direct {p0, v3, v4, v5}, LX/1Yl;-><init>(LX/1Ym;LX/0So;LX/0wq;)V

    .line 274022
    move-object v0, p0

    .line 274023
    sput-object v0, LX/1Yl;->h:LX/1Yl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 274024
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 274025
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 274026
    :cond_1
    sget-object v0, LX/1Yl;->h:LX/1Yl;

    return-object v0

    .line 274027
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 274028
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;LX/1m6;Z)LX/7K1;
    .locals 8

    .prologue
    .line 274029
    iput-object p2, p0, LX/1Yl;->f:Landroid/net/Uri;

    .line 274030
    iput-object p3, p0, LX/1Yl;->g:Ljava/lang/String;

    .line 274031
    iput-object p4, p0, LX/1Yl;->e:LX/1m6;

    .line 274032
    iget-object v0, p0, LX/1Yl;->c:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7K1;

    .line 274033
    if-eqz v0, :cond_0

    .line 274034
    iget-boolean v1, v0, LX/7K1;->k:Z

    move v1, v1

    .line 274035
    if-eqz v1, :cond_1

    .line 274036
    invoke-virtual {v0}, LX/7K1;->g()V

    .line 274037
    :cond_0
    iget-object v2, p0, LX/1Yl;->b:LX/1Ym;

    iget-object v4, p0, LX/1Yl;->f:Landroid/net/Uri;

    iget-object v5, p0, LX/1Yl;->g:Ljava/lang/String;

    iget-object v6, p0, LX/1Yl;->e:LX/1m6;

    move-object v3, p1

    move v7, p5

    invoke-virtual/range {v2 .. v7}, LX/1Ym;->a(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;LX/1m6;Z)LX/7K1;

    move-result-object v2

    move-object v0, v2

    .line 274038
    :cond_1
    return-object v0
.end method
