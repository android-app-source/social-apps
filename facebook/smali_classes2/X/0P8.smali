.class public final LX/0P8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation


# static fields
.field public static final a:[Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54999
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, LX/0P8;->a:[Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 54998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 54995
    if-nez p0, :cond_0

    .line 54996
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "at index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54997
    :cond_0
    return-object p0
.end method

.method public static a([Ljava/lang/Object;II)V
    .locals 2

    .prologue
    .line 54967
    aget-object v0, p0, p1

    .line 54968
    aget-object v1, p0, p2

    aput-object v1, p0, p1

    .line 54969
    aput-object v0, p0, p2

    .line 54970
    return-void
.end method

.method public static a(Ljava/lang/Class;I)[Ljava/lang/Object;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "Array.newInstance(Class, int)"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;I)[TT;"
        }
    .end annotation

    .prologue
    .line 54994
    invoke-static {p0, p1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;[Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<*>;[",
            "Ljava/lang/Object;",
            ")[",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 54989
    const/4 v0, 0x0

    .line 54990
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 54991
    add-int/lit8 v1, v0, 0x1

    aput-object v3, p1, v0

    move v0, v1

    .line 54992
    goto :goto_0

    .line 54993
    :cond_0
    return-object p1
.end method

.method public static a(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<*>;[TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 54982
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    .line 54983
    array-length v1, p1

    if-ge v1, v0, :cond_0

    .line 54984
    invoke-static {p1, v0}, LX/0P9;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    .line 54985
    :cond_0
    invoke-static {p0, p1}, LX/0P8;->a(Ljava/lang/Iterable;[Ljava/lang/Object;)[Ljava/lang/Object;

    .line 54986
    array-length v1, p1

    if-le v1, v0, :cond_1

    .line 54987
    const/4 v1, 0x0

    aput-object v1, p1, v0

    .line 54988
    :cond_1
    return-object p1
.end method

.method public static varargs a([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54981
    array-length v0, p0

    invoke-static {p0, v0}, LX/0P8;->c([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 54978
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, LX/0P8;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    .line 54979
    array-length v1, p0

    aput-object p1, v0, v1

    .line 54980
    return-object v0
.end method

.method public static b([Ljava/lang/Object;I)[Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;I)[TT;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 54975
    invoke-static {p0, p1}, LX/0P9;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    .line 54976
    array-length v1, p0

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 54977
    return-object v0
.end method

.method public static c([Ljava/lang/Object;I)[Ljava/lang/Object;
    .locals 2

    .prologue
    .line 54971
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 54972
    aget-object v1, p0, v0

    invoke-static {v1, v0}, LX/0P8;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    .line 54973
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54974
    :cond_0
    return-object p0
.end method
