.class public LX/1A8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0X9;
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0X9;",
        "Ljava/util/concurrent/Callable",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:LX/0cV;


# direct methods
.method private constructor <init>(Ljava/util/concurrent/Callable;LX/0Sj;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<TE;>;",
            "LX/0Sj;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 210087
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210088
    iput-object p1, p0, LX/1A8;->a:Ljava/util/concurrent/Callable;

    .line 210089
    iget-object v0, p0, LX/1A8;->a:Ljava/util/concurrent/Callable;

    invoke-static {v0}, LX/22f;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1A8;->b:Ljava/lang/String;

    .line 210090
    iget-object v0, p0, LX/1A8;->a:Ljava/util/concurrent/Callable;

    invoke-static {v0}, LX/22f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1A8;->c:Ljava/lang/String;

    .line 210091
    iget-object v0, p0, LX/1A8;->b:Ljava/lang/String;

    invoke-interface {p2, p3, v0}, LX/0Sj;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0cV;

    move-result-object v0

    iput-object v0, p0, LX/1A8;->d:LX/0cV;

    .line 210092
    return-void
.end method

.method public static a(Ljava/util/concurrent/Callable;LX/0Sj;Ljava/lang/String;)Ljava/util/concurrent/Callable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;",
            "LX/0Sj;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/concurrent/Callable",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 210098
    invoke-interface {p1}, LX/0Sj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210099
    new-instance v0, LX/1A8;

    invoke-direct {v0, p0, p1, p2}, LX/1A8;-><init>(Ljava/util/concurrent/Callable;LX/0Sj;Ljava/lang/String;)V

    move-object p0, v0

    .line 210100
    :cond_0
    return-object p0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210097
    iget-object v0, p0, LX/1A8;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210096
    iget-object v0, p0, LX/1A8;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final call()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 210093
    iget-object v0, p0, LX/1A8;->d:LX/0cV;

    invoke-interface {v0}, LX/0cV;->a()V

    .line 210094
    :try_start_0
    iget-object v0, p0, LX/1A8;->a:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 210095
    iget-object v1, p0, LX/1A8;->d:LX/0cV;

    invoke-interface {v1}, LX/0cV;->b()V

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1A8;->d:LX/0cV;

    invoke-interface {v1}, LX/0cV;->b()V

    throw v0
.end method
