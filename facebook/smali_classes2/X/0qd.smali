.class public LX/0qd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final i:Ljava/lang/Object;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0qb;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:LX/0Wd;

.field public final e:LX/0SG;

.field private final f:LX/0qe;

.field public g:Z

.field public h:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 148137
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/0qd;->i:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0Wd;LX/0SG;LX/0qe;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p4    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0qb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            "LX/0SG;",
            "LX/0qe;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 148127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148128
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0qd;->g:Z

    .line 148129
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0qd;->h:J

    .line 148130
    iput-object p1, p0, LX/0qd;->a:LX/0Ot;

    .line 148131
    iput-object p2, p0, LX/0qd;->b:LX/0Ot;

    .line 148132
    iput-object p3, p0, LX/0qd;->c:Ljava/util/concurrent/ExecutorService;

    .line 148133
    iput-object p4, p0, LX/0qd;->d:LX/0Wd;

    .line 148134
    iput-object p5, p0, LX/0qd;->e:LX/0SG;

    .line 148135
    iput-object p6, p0, LX/0qd;->f:LX/0qe;

    .line 148136
    return-void
.end method

.method public static a(LX/0QB;)LX/0qd;
    .locals 14

    .prologue
    .line 148073
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 148074
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 148075
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 148076
    if-nez v1, :cond_0

    .line 148077
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148078
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 148079
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 148080
    sget-object v1, LX/0qd;->i:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 148081
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 148082
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 148083
    :cond_1
    if-nez v1, :cond_4

    .line 148084
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 148085
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 148086
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 148087
    new-instance v7, LX/0qd;

    const/16 v8, 0x22b

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xafd

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v11

    check-cast v11, LX/0Wd;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    invoke-static {v0}, LX/0qe;->a(LX/0QB;)LX/0qe;

    move-result-object v13

    check-cast v13, LX/0qe;

    invoke-direct/range {v7 .. v13}, LX/0qd;-><init>(LX/0Ot;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0Wd;LX/0SG;LX/0qe;)V

    .line 148088
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 148089
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 148090
    if-nez v1, :cond_2

    .line 148091
    sget-object v0, LX/0qd;->i:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qd;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 148092
    :goto_1
    if-eqz v0, :cond_3

    .line 148093
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 148094
    :goto_3
    check-cast v0, LX/0qd;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 148095
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 148096
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 148097
    :catchall_1
    move-exception v0

    .line 148098
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 148099
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 148100
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 148101
    :cond_2
    :try_start_8
    sget-object v0, LX/0qd;->i:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qd;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/0zS;)LX/1Zp;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zS;",
            ")",
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 148117
    new-instance v0, LX/3SF;

    invoke-direct {v0}, LX/3SF;-><init>()V

    move-object v1, v0

    .line 148118
    const-string v2, "names"

    iget-object v0, p0, LX/0qd;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qb;

    .line 148119
    new-instance v4, Ljava/util/ArrayList;

    iget-object v3, v0, LX/0qb;->a:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 148120
    iget-object v3, v0, LX/0qb;->a:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0qg;

    .line 148121
    invoke-virtual {v3}, LX/0qg;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 148122
    :cond_0
    move-object v0, v4

    .line 148123
    invoke-virtual {v1, v2, v0}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 148124
    iget-object v0, p0, LX/0qd;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    iget-object v2, p0, LX/0qd;->f:LX/0qe;

    invoke-virtual {v2}, LX/0qe;->c()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 148125
    new-instance v1, LX/3SG;

    invoke-direct {v1, p0, p1}, LX/3SG;-><init>(LX/0qd;LX/0zS;)V

    iget-object v2, p0, LX/0qd;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 148126
    return-object v0
.end method

.method public final declared-synchronized a()V
    .locals 4
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 148108
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0qd;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 148109
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 148110
    :cond_1
    :try_start_1
    iget-wide v0, p0, LX/0qd;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 148111
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0qd;->g:Z

    .line 148112
    iget-object v0, p0, LX/0qd;->d:LX/0Wd;

    new-instance v1, Lcom/facebook/clashmanagement/api/ClashUnitDataMaintenanceHelper$1;

    invoke-direct {v1, p0}, Lcom/facebook/clashmanagement/api/ClashUnitDataMaintenanceHelper$1;-><init>(LX/0qd;)V

    const v2, -0x5f18bc99

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 148113
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 148114
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/0qd;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/0qd;->h:J

    sub-long/2addr v0, v2

    iget-object v2, p0, LX/0qd;->f:LX/0qe;

    invoke-virtual {v2}, LX/0qe;->c()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 148115
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0qd;->g:Z

    .line 148116
    iget-object v0, p0, LX/0qd;->d:LX/0Wd;

    new-instance v1, Lcom/facebook/clashmanagement/api/ClashUnitDataMaintenanceHelper$2;

    invoke-direct {v1, p0}, Lcom/facebook/clashmanagement/api/ClashUnitDataMaintenanceHelper$2;-><init>(LX/0qd;)V

    const v2, -0x1a8d9c1b

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized b()LX/1Zp;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 148102
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0qd;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 148103
    const/4 v0, 0x0

    .line 148104
    :goto_0
    monitor-exit p0

    return-object v0

    .line 148105
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/0qd;->g:Z

    .line 148106
    sget-object v0, LX/0zS;->d:LX/0zS;

    invoke-virtual {p0, v0}, LX/0qd;->a(LX/0zS;)LX/1Zp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 148107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
