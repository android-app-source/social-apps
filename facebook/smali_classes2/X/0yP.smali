.class public LX/0yP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/33e;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0yN;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0yi;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0yJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 164728
    const-class v0, LX/0yP;

    sput-object v0, LX/0yP;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0yN;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/33e;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0yN;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 164701
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164702
    iput-object p1, p0, LX/0yP;->b:LX/0Ot;

    .line 164703
    iput-object p2, p0, LX/0yP;->c:LX/0Ot;

    .line 164704
    iput-object p3, p0, LX/0yP;->d:LX/0Ot;

    .line 164705
    iput-object p4, p0, LX/0yP;->f:LX/0Or;

    .line 164706
    iput-object p5, p0, LX/0yP;->e:LX/0yN;

    .line 164707
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0yP;->g:Ljava/util/Map;

    .line 164708
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/0yP;->h:Ljava/util/Set;

    .line 164709
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 164723
    iget-object v0, p0, LX/0yP;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 164724
    if-eqz v0, :cond_0

    .line 164725
    const/4 v2, 0x1

    invoke-interface {v0, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    goto :goto_0

    .line 164726
    :cond_1
    iget-object v0, p0, LX/0yP;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 164727
    return-void
.end method

.method public final a(LX/0yJ;)V
    .locals 1

    .prologue
    .line 164721
    iget-object v0, p0, LX/0yP;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 164722
    return-void
.end method

.method public final a(LX/0yi;LX/32P;)V
    .locals 6

    .prologue
    .line 164729
    iget-object v0, p0, LX/0yP;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-eq v0, v1, :cond_1

    .line 164730
    :cond_0
    :goto_0
    return-void

    .line 164731
    :cond_1
    iget-object v0, p0, LX/0yP;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 164732
    if-nez v0, :cond_0

    .line 164733
    iget-object v0, p0, LX/0yP;->e:LX/0yN;

    invoke-interface {v0}, LX/0yN;->a()LX/1p2;

    move-result-object v1

    invoke-virtual {p1}, LX/0yi;->getLastTimeCheckedKey()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, LX/0yP;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, LX/1p2;->a(Ljava/lang/String;J)LX/1p2;

    move-result-object v0

    invoke-interface {v0}, LX/1p2;->a()V

    .line 164734
    new-instance v1, LX/33d;

    invoke-direct {v1, p0, p1}, LX/33d;-><init>(LX/0yP;LX/0yi;)V

    .line 164735
    iget-object v0, p0, LX/0yP;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/33e;

    invoke-virtual {p0, p1, p2}, LX/0yP;->b(LX/0yi;LX/32P;)Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;

    move-result-object v2

    invoke-interface {v0, v2, v1}, LX/33e;->a(Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 164736
    iget-object v1, p0, LX/0yP;->g:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Ljava/lang/Throwable;LX/0yi;)V
    .locals 2

    .prologue
    .line 164717
    iget-object v0, p0, LX/0yP;->g:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 164718
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, LX/0yP;->h:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yJ;

    .line 164719
    invoke-interface {v0, p1, p2}, LX/0yJ;->a(Ljava/lang/Throwable;LX/0yi;)V

    goto :goto_0

    .line 164720
    :cond_0
    return-void
.end method

.method public final b(LX/0yi;LX/32P;)Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;
    .locals 7

    .prologue
    .line 164716
    new-instance v0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;

    iget-object v1, p0, LX/0yP;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1pA;

    invoke-virtual {v1}, LX/1pA;->a()Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-result-object v1

    iget-object v2, p0, LX/0yP;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1pA;

    invoke-virtual {v2}, LX/1pA;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/0yP;->e:LX/0yN;

    invoke-virtual {p1}, LX/0yi;->getBackupRewriteRulesKey()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0yN;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v4, 0x1

    :goto_0
    iget-object v3, p0, LX/0yP;->e:LX/0yN;

    invoke-virtual {p1}, LX/0yi;->getTokenHashKey()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-interface {v3, v5, v6}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v3, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;-><init>(Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;Ljava/lang/String;LX/0yi;ZLjava/lang/String;LX/32P;)V

    return-object v0

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final b(LX/0yJ;)V
    .locals 1

    .prologue
    .line 164714
    iget-object v0, p0, LX/0yP;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 164715
    return-void
.end method

.method public b(Lcom/facebook/zero/sdk/token/ZeroToken;LX/0yi;)V
    .locals 2

    .prologue
    .line 164710
    iget-object v0, p0, LX/0yP;->g:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 164711
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, LX/0yP;->h:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yJ;

    .line 164712
    invoke-interface {v0, p1, p2}, LX/0yJ;->a(Lcom/facebook/zero/sdk/token/ZeroToken;LX/0yi;)V

    goto :goto_0

    .line 164713
    :cond_0
    return-void
.end method
