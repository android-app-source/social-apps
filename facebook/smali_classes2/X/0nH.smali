.class public final enum LX/0nH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0nH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0nH;

.field public static final enum ARRAY:LX/0nH;

.field public static final enum BINARY:LX/0nH;

.field public static final enum BOOLEAN:LX/0nH;

.field public static final enum MISSING:LX/0nH;

.field public static final enum NULL:LX/0nH;

.field public static final enum NUMBER:LX/0nH;

.field public static final enum OBJECT:LX/0nH;

.field public static final enum POJO:LX/0nH;

.field public static final enum STRING:LX/0nH;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 135047
    new-instance v0, LX/0nH;

    const-string v1, "ARRAY"

    invoke-direct {v0, v1, v3}, LX/0nH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0nH;->ARRAY:LX/0nH;

    .line 135048
    new-instance v0, LX/0nH;

    const-string v1, "BINARY"

    invoke-direct {v0, v1, v4}, LX/0nH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0nH;->BINARY:LX/0nH;

    .line 135049
    new-instance v0, LX/0nH;

    const-string v1, "BOOLEAN"

    invoke-direct {v0, v1, v5}, LX/0nH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0nH;->BOOLEAN:LX/0nH;

    .line 135050
    new-instance v0, LX/0nH;

    const-string v1, "MISSING"

    invoke-direct {v0, v1, v6}, LX/0nH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0nH;->MISSING:LX/0nH;

    .line 135051
    new-instance v0, LX/0nH;

    const-string v1, "NULL"

    invoke-direct {v0, v1, v7}, LX/0nH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0nH;->NULL:LX/0nH;

    .line 135052
    new-instance v0, LX/0nH;

    const-string v1, "NUMBER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0nH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0nH;->NUMBER:LX/0nH;

    .line 135053
    new-instance v0, LX/0nH;

    const-string v1, "OBJECT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/0nH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0nH;->OBJECT:LX/0nH;

    .line 135054
    new-instance v0, LX/0nH;

    const-string v1, "POJO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/0nH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0nH;->POJO:LX/0nH;

    .line 135055
    new-instance v0, LX/0nH;

    const-string v1, "STRING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/0nH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0nH;->STRING:LX/0nH;

    .line 135056
    const/16 v0, 0x9

    new-array v0, v0, [LX/0nH;

    sget-object v1, LX/0nH;->ARRAY:LX/0nH;

    aput-object v1, v0, v3

    sget-object v1, LX/0nH;->BINARY:LX/0nH;

    aput-object v1, v0, v4

    sget-object v1, LX/0nH;->BOOLEAN:LX/0nH;

    aput-object v1, v0, v5

    sget-object v1, LX/0nH;->MISSING:LX/0nH;

    aput-object v1, v0, v6

    sget-object v1, LX/0nH;->NULL:LX/0nH;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0nH;->NUMBER:LX/0nH;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0nH;->OBJECT:LX/0nH;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0nH;->POJO:LX/0nH;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0nH;->STRING:LX/0nH;

    aput-object v2, v0, v1

    sput-object v0, LX/0nH;->$VALUES:[LX/0nH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 135046
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0nH;
    .locals 1

    .prologue
    .line 135045
    const-class v0, LX/0nH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0nH;

    return-object v0
.end method

.method public static values()[LX/0nH;
    .locals 1

    .prologue
    .line 135044
    sget-object v0, LX/0nH;->$VALUES:[LX/0nH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0nH;

    return-object v0
.end method
