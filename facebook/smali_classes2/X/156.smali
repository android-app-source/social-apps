.class public final LX/156;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/157;


# instance fields
.field public final synthetic a:LX/14u;


# direct methods
.method public constructor <init>(LX/14u;)V
    .locals 0

    .prologue
    .line 180086
    iput-object p1, p0, LX/156;->a:LX/14u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 180087
    iget-object v0, p0, LX/156;->a:LX/14u;

    iget-object v0, v0, LX/14u;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 180088
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 180089
    :goto_0
    return-void

    .line 180090
    :cond_0
    iget-object v1, p0, LX/156;->a:LX/14u;

    iget-object v1, v1, LX/14u;->f:LX/6VM;

    if-eqz v1, :cond_1

    .line 180091
    iget-object v1, p0, LX/156;->a:LX/14u;

    iget-object v1, v1, LX/14u;->f:LX/6VM;

    invoke-virtual {v1, v0}, LX/6VM;->a(Ljava/lang/String;)V

    .line 180092
    :cond_1
    iget-object v0, p0, LX/156;->a:LX/14u;

    invoke-virtual {v0, p1}, LX/14u;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)V
    .locals 2
    .param p3    # Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 180093
    iget-object v0, p0, LX/156;->a:LX/14u;

    iget-object v0, v0, LX/14u;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 180094
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 180095
    :cond_0
    :goto_0
    return-void

    .line 180096
    :cond_1
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE_STOPPED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq p2, v1, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SEAL_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq p2, v1, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne p2, v1, :cond_4

    .line 180097
    :cond_2
    const/4 v1, 0x1

    .line 180098
    :goto_1
    move v1, v1

    .line 180099
    if-eqz v1, :cond_0

    .line 180100
    iget-object v1, p0, LX/156;->a:LX/14u;

    iget-object v1, v1, LX/14u;->f:LX/6VM;

    if-eqz v1, :cond_3

    .line 180101
    iget-object v1, p0, LX/156;->a:LX/14u;

    iget-object v1, v1, LX/14u;->f:LX/6VM;

    invoke-virtual {v1, v0}, LX/6VM;->a(Ljava/lang/String;)V

    .line 180102
    :cond_3
    iget-object v0, p0, LX/156;->a:LX/14u;

    invoke-virtual {v0, p1}, LX/14u;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method
