.class public LX/11f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<TV;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/11e;


# direct methods
.method public constructor <init>(LX/11e;)V
    .locals 2

    .prologue
    .line 172826
    iput-object p1, p0, LX/11f;->c:LX/11e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172827
    iget-object v0, p0, LX/11f;->c:LX/11e;

    iget-object v0, v0, LX/11e;->c:Ljava/util/Collection;

    iput-object v0, p0, LX/11f;->b:Ljava/util/Collection;

    .line 172828
    iget-object v1, p1, LX/11e;->c:Ljava/util/Collection;

    .line 172829
    instance-of p1, v1, Ljava/util/List;

    if-eqz p1, :cond_0

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object p1

    :goto_0
    move-object p1, p1

    .line 172830
    move-object v0, p1

    .line 172831
    iput-object v0, p0, LX/11f;->a:Ljava/util/Iterator;

    .line 172832
    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    goto :goto_0
.end method

.method public constructor <init>(LX/11e;Ljava/util/Iterator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 172833
    iput-object p1, p0, LX/11f;->c:LX/11e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172834
    iget-object v0, p0, LX/11f;->c:LX/11e;

    iget-object v0, v0, LX/11e;->c:Ljava/util/Collection;

    iput-object v0, p0, LX/11f;->b:Ljava/util/Collection;

    .line 172835
    iput-object p2, p0, LX/11f;->a:Ljava/util/Iterator;

    .line 172836
    return-void
.end method

.method public static b(LX/11f;)V
    .locals 2

    .prologue
    .line 172837
    iget-object v0, p0, LX/11f;->c:LX/11e;

    invoke-virtual {v0}, LX/11e;->a()V

    .line 172838
    iget-object v0, p0, LX/11f;->c:LX/11e;

    iget-object v0, v0, LX/11e;->c:Ljava/util/Collection;

    iget-object v1, p0, LX/11f;->b:Ljava/util/Collection;

    if-eq v0, v1, :cond_0

    .line 172839
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 172840
    :cond_0
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 172841
    invoke-static {p0}, LX/11f;->b(LX/11f;)V

    .line 172842
    iget-object v0, p0, LX/11f;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 172843
    invoke-static {p0}, LX/11f;->b(LX/11f;)V

    .line 172844
    iget-object v0, p0, LX/11f;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 172845
    iget-object v0, p0, LX/11f;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 172846
    iget-object v0, p0, LX/11f;->c:LX/11e;

    iget-object v0, v0, LX/11e;->f:LX/0Xs;

    invoke-static {v0}, LX/0Xs;->b(LX/0Xs;)I

    .line 172847
    iget-object v0, p0, LX/11f;->c:LX/11e;

    invoke-virtual {v0}, LX/11e;->b()V

    .line 172848
    return-void
.end method
