.class public LX/1WX;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1XJ;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1zY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 268906
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1WX;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1zY;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 268903
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 268904
    iput-object p1, p0, LX/1WX;->b:LX/0Ot;

    .line 268905
    return-void
.end method

.method public static a(LX/0QB;)LX/1WX;
    .locals 4

    .prologue
    .line 268892
    const-class v1, LX/1WX;

    monitor-enter v1

    .line 268893
    :try_start_0
    sget-object v0, LX/1WX;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 268894
    sput-object v2, LX/1WX;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 268895
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268896
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 268897
    new-instance v3, LX/1WX;

    const/16 p0, 0x7b4

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1WX;-><init>(LX/0Ot;)V

    .line 268898
    move-object v0, v3

    .line 268899
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 268900
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1WX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268901
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 268902
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;II)LX/1XJ;
    .locals 2

    .prologue
    .line 268885
    new-instance v0, LX/1XI;

    invoke-direct {v0, p0}, LX/1XI;-><init>(LX/1WX;)V

    .line 268886
    sget-object v1, LX/1WX;->a:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1XJ;

    .line 268887
    if-nez v1, :cond_0

    .line 268888
    new-instance v1, LX/1XJ;

    invoke-direct {v1}, LX/1XJ;-><init>()V

    .line 268889
    :cond_0
    invoke-static {v1, p1, p2, p3, v0}, LX/1XJ;->a$redex0(LX/1XJ;LX/1De;IILX/1XI;)V

    .line 268890
    move-object v0, v1

    .line 268891
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 268907
    invoke-static {}, LX/1dS;->b()V

    .line 268908
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;IILX/1X1;)LX/1Dg;
    .locals 15

    .prologue
    .line 268814
    check-cast p4, LX/1XI;

    .line 268815
    iget-object v1, p0, LX/1WX;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1zY;

    move-object/from16 v0, p4

    iget-object v4, v0, LX/1XI;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object/from16 v0, p4

    iget-boolean v5, v0, LX/1XI;->b:Z

    move-object/from16 v0, p4

    iget-boolean v6, v0, LX/1XI;->c:Z

    move-object/from16 v0, p4

    iget-boolean v7, v0, LX/1XI;->d:Z

    move-object/from16 v0, p4

    iget-object v8, v0, LX/1XI;->e:LX/1dQ;

    move-object/from16 v0, p4

    iget v9, v0, LX/1XI;->f:I

    move-object/from16 v0, p4

    iget v10, v0, LX/1XI;->g:I

    move-object/from16 v0, p4

    iget v11, v0, LX/1XI;->h:I

    move-object/from16 v0, p4

    iget v12, v0, LX/1XI;->i:F

    move-object/from16 v0, p4

    iget v13, v0, LX/1XI;->j:F

    move-object/from16 v0, p4

    iget v14, v0, LX/1XI;->k:F

    move-object/from16 v2, p1

    move/from16 v3, p2

    invoke-virtual/range {v1 .. v14}, LX/1zY;->a(LX/1De;ILcom/facebook/graphql/model/GraphQLStory;ZZZLX/1dQ;IIIFFF)LX/1Dg;

    move-result-object v1

    .line 268816
    return-object v1
.end method

.method public final c(LX/1De;)LX/1XJ;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 268813
    invoke-virtual {p0, p1, v0, v0}, LX/1WX;->a(LX/1De;II)LX/1XJ;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/1De;LX/1X1;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 268817
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 268818
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v2

    .line 268819
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v3

    .line 268820
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v4

    .line 268821
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 268822
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 268823
    iget-object v0, p0, LX/1WX;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-object v0, p1

    const/4 p1, 0x0

    const/4 v8, 0x0

    .line 268824
    sget-object v7, LX/03r;->UFIFeedbackSummaryComponentSpec:[I

    invoke-virtual {v0, v7, v8}, LX/1De;->a([II)Landroid/content/res/TypedArray;

    move-result-object v9

    .line 268825
    invoke-virtual {v9}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v10

    move v7, v8

    :goto_0
    if-ge v7, v10, :cond_6

    .line 268826
    invoke-virtual {v9, v7}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v11

    .line 268827
    const/16 p0, 0x6

    if-ne v11, p0, :cond_1

    .line 268828
    invoke-virtual {v9, v11, v8}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 268829
    iput-object v11, v1, LX/1np;->a:Ljava/lang/Object;

    .line 268830
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 268831
    :cond_1
    const/16 p0, 0x0

    if-ne v11, p0, :cond_2

    .line 268832
    invoke-virtual {v9, v11, v8}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 268833
    iput-object v11, v2, LX/1np;->a:Ljava/lang/Object;

    .line 268834
    goto :goto_1

    .line 268835
    :cond_2
    const/16 p0, 0x3

    if-ne v11, p0, :cond_3

    .line 268836
    invoke-virtual {v9, v11, p1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    .line 268837
    iput-object v11, v4, LX/1np;->a:Ljava/lang/Object;

    .line 268838
    goto :goto_1

    .line 268839
    :cond_3
    const/16 p0, 0x4

    if-ne v11, p0, :cond_4

    .line 268840
    invoke-virtual {v9, v11, p1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    .line 268841
    iput-object v11, v5, LX/1np;->a:Ljava/lang/Object;

    .line 268842
    goto :goto_1

    .line 268843
    :cond_4
    const/16 p0, 0x5

    if-ne v11, p0, :cond_5

    .line 268844
    invoke-virtual {v9, v11, p1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    .line 268845
    iput-object v11, v6, LX/1np;->a:Ljava/lang/Object;

    .line 268846
    goto :goto_1

    .line 268847
    :cond_5
    const/16 p0, 0x2

    if-ne v11, p0, :cond_0

    .line 268848
    invoke-virtual {v9, v11, v8}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 268849
    iput-object v11, v3, LX/1np;->a:Ljava/lang/Object;

    .line 268850
    goto :goto_1

    .line 268851
    :cond_6
    invoke-virtual {v9}, Landroid/content/res/TypedArray;->recycle()V

    .line 268852
    check-cast p2, LX/1XI;

    .line 268853
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 268854
    if-eqz v0, :cond_7

    .line 268855
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 268856
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/1XI;->f:I

    .line 268857
    :cond_7
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 268858
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 268859
    if-eqz v0, :cond_8

    .line 268860
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 268861
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/1XI;->g:I

    .line 268862
    :cond_8
    invoke-static {v2}, LX/1cy;->a(LX/1np;)V

    .line 268863
    iget-object v0, v3, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 268864
    if-eqz v0, :cond_9

    .line 268865
    iget-object v0, v3, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 268866
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/1XI;->h:I

    .line 268867
    :cond_9
    invoke-static {v3}, LX/1cy;->a(LX/1np;)V

    .line 268868
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 268869
    if-eqz v0, :cond_a

    .line 268870
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 268871
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p2, LX/1XI;->i:F

    .line 268872
    :cond_a
    invoke-static {v4}, LX/1cy;->a(LX/1np;)V

    .line 268873
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 268874
    if-eqz v0, :cond_b

    .line 268875
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 268876
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p2, LX/1XI;->j:F

    .line 268877
    :cond_b
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 268878
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 268879
    if-eqz v0, :cond_c

    .line 268880
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 268881
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p2, LX/1XI;->k:F

    .line 268882
    :cond_c
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 268883
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 268884
    const/4 v0, 0x1

    return v0
.end method
