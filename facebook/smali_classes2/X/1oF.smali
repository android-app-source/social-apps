.class public LX/1oF;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements LX/1d3;
.implements LX/1cu;


# instance fields
.field public a:Landroid/text/Layout;

.field public c:F

.field public d:Z

.field public e:Ljava/lang/CharSequence;

.field public f:Landroid/content/res/ColorStateList;

.field public g:I

.field public h:I

.field public i:[Landroid/text/style/ClickableSpan;

.field public j:I

.field public k:I

.field public l:Landroid/graphics/Path;

.field private m:Landroid/graphics/Path;

.field public n:Z

.field private o:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 318818
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    return-void
.end method

.method private a(FFF)Landroid/text/style/ClickableSpan;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 318799
    new-instance v5, Landroid/graphics/Region;

    invoke-direct {v5}, Landroid/graphics/Region;-><init>()V

    .line 318800
    new-instance v6, Landroid/graphics/Region;

    invoke-direct {v6}, Landroid/graphics/Region;-><init>()V

    .line 318801
    iget-object v1, p0, LX/1oF;->m:Landroid/graphics/Path;

    if-nez v1, :cond_0

    .line 318802
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, LX/1oF;->m:Landroid/graphics/Path;

    .line 318803
    :cond_0
    iget-object v1, p0, LX/1oF;->a:Landroid/text/Layout;

    invoke-static {v1}, LX/1nt;->a(Landroid/text/Layout;)I

    move-result v1

    iget-object v2, p0, LX/1oF;->a:Landroid/text/Layout;

    invoke-static {v2}, LX/1nt;->b(Landroid/text/Layout;)I

    move-result v2

    invoke-virtual {v6, v0, v0, v1, v2}, Landroid/graphics/Region;->set(IIII)Z

    .line 318804
    iget-object v1, p0, LX/1oF;->m:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 318805
    iget-object v1, p0, LX/1oF;->m:Landroid/graphics/Path;

    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, p1, p2, p3, v2}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 318806
    iget-object v1, p0, LX/1oF;->m:Landroid/graphics/Path;

    invoke-virtual {v5, v1, v6}, Landroid/graphics/Region;->setPath(Landroid/graphics/Path;Landroid/graphics/Region;)Z

    .line 318807
    iget-object v7, p0, LX/1oF;->i:[Landroid/text/style/ClickableSpan;

    array-length v8, v7

    move v4, v0

    move-object v2, v3

    :goto_0
    if-ge v4, v8, :cond_1

    aget-object v1, v7, v4

    .line 318808
    iget-object v0, p0, LX/1oF;->e:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    iget-object v9, p0, LX/1oF;->a:Landroid/text/Layout;

    .line 318809
    new-instance v10, Landroid/graphics/Region;

    invoke-direct {v10}, Landroid/graphics/Region;-><init>()V

    .line 318810
    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    .line 318811
    invoke-interface {v0, v1}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result p2

    invoke-interface {v0, v1}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result p3

    invoke-virtual {v9, p2, p3, p1}, Landroid/text/Layout;->getSelectionPath(IILandroid/graphics/Path;)V

    .line 318812
    invoke-virtual {v10, p1, v6}, Landroid/graphics/Region;->setPath(Landroid/graphics/Path;Landroid/graphics/Region;)Z

    .line 318813
    sget-object p1, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    invoke-virtual {v10, v5, p1}, Landroid/graphics/Region;->op(Landroid/graphics/Region;Landroid/graphics/Region$Op;)Z

    move-result v10

    move v0, v10

    .line 318814
    if-eqz v0, :cond_3

    .line 318815
    if-eqz v2, :cond_2

    move-object v2, v3

    .line 318816
    :cond_1
    return-object v2

    :cond_2
    move-object v0, v1

    .line 318817
    :goto_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move-object v2, v0

    goto :goto_0

    :cond_3
    move-object v0, v2

    goto :goto_1
.end method

.method public static b(LX/1oF;II)V
    .locals 2

    .prologue
    .line 318790
    iget v0, p0, LX/1oF;->h:I

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/1oF;->j:I

    if-ne v0, p1, :cond_1

    iget v0, p0, LX/1oF;->k:I

    if-ne v0, p2, :cond_1

    .line 318791
    :cond_0
    :goto_0
    return-void

    .line 318792
    :cond_1
    iput p1, p0, LX/1oF;->j:I

    .line 318793
    iput p2, p0, LX/1oF;->k:I

    .line 318794
    iget-object v0, p0, LX/1oF;->o:Landroid/graphics/Paint;

    if-nez v0, :cond_2

    .line 318795
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/1oF;->o:Landroid/graphics/Paint;

    .line 318796
    :cond_2
    iget-object v0, p0, LX/1oF;->o:Landroid/graphics/Paint;

    iget v1, p0, LX/1oF;->h:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 318797
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1oF;->n:Z

    .line 318798
    invoke-virtual {p0}, LX/1oF;->invalidateSelf()V

    goto :goto_0
.end method

.method private d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 318788
    invoke-static {p0, v0, v0}, LX/1oF;->b(LX/1oF;II)V

    .line 318789
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .prologue
    .line 318787
    iget-object v0, p0, LX/1oF;->e:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1oF;->e:Ljava/lang/CharSequence;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;Landroid/text/Layout;FLandroid/content/res/ColorStateList;II[Landroid/text/style/ClickableSpan;)V
    .locals 4

    .prologue
    .line 318771
    iput-object p2, p0, LX/1oF;->a:Landroid/text/Layout;

    .line 318772
    iput p3, p0, LX/1oF;->c:F

    .line 318773
    iput-object p1, p0, LX/1oF;->e:Ljava/lang/CharSequence;

    .line 318774
    iput-object p7, p0, LX/1oF;->i:[Landroid/text/style/ClickableSpan;

    .line 318775
    if-eqz p7, :cond_1

    array-length v0, p7

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/1oF;->d:Z

    .line 318776
    iput p6, p0, LX/1oF;->h:I

    .line 318777
    if-eqz p4, :cond_2

    .line 318778
    iput-object p4, p0, LX/1oF;->f:Landroid/content/res/ColorStateList;

    .line 318779
    invoke-virtual {p4}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    iput v0, p0, LX/1oF;->g:I

    .line 318780
    iget-object v0, p0, LX/1oF;->a:Landroid/text/Layout;

    if-eqz v0, :cond_0

    .line 318781
    iget-object v0, p0, LX/1oF;->a:Landroid/text/Layout;

    invoke-virtual {v0}, Landroid/text/Layout;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    iget-object v1, p0, LX/1oF;->f:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, LX/1oF;->getState()[I

    move-result-object v2

    iget v3, p0, LX/1oF;->g:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 318782
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/1oF;->invalidateSelf()V

    .line 318783
    return-void

    .line 318784
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 318785
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, LX/1oF;->f:Landroid/content/res/ColorStateList;

    .line 318786
    iput p5, p0, LX/1oF;->g:I

    goto :goto_1
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 318819
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    .line 318820
    invoke-virtual {p0}, LX/1oF;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    .line 318821
    if-eq v3, v2, :cond_0

    if-nez v3, :cond_3

    :cond_0
    move v0, v2

    .line 318822
    :goto_0
    iget-boolean v5, p0, LX/1oF;->d:Z

    if-eqz v5, :cond_1

    if-eqz v4, :cond_1

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x3

    if-ne v3, v0, :cond_4

    :cond_2
    :goto_1
    return v2

    :cond_3
    move v0, v1

    .line 318823
    goto :goto_0

    :cond_4
    move v2, v1

    .line 318824
    goto :goto_1
.end method

.method public final a(Landroid/view/MotionEvent;Landroid/view/View;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 318744
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    .line 318745
    const/4 v2, 0x3

    if-ne v3, v2, :cond_0

    .line 318746
    invoke-direct {p0}, LX/1oF;->d()V

    .line 318747
    :goto_0
    return v0

    .line 318748
    :cond_0
    invoke-virtual {p0}, LX/1oF;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 318749
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    iget v5, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v4, v5

    .line 318750
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v5, v2

    .line 318751
    const/4 v2, 0x2

    const/high16 v6, 0x41900000    # 18.0f

    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    invoke-static {v2, v6, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v6

    .line 318752
    iget-object v2, p0, LX/1oF;->a:Landroid/text/Layout;

    invoke-virtual {v2, v5}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v2

    .line 318753
    int-to-float v7, v4

    iget-object p1, p0, LX/1oF;->a:Landroid/text/Layout;

    invoke-virtual {p1, v2}, Landroid/text/Layout;->getLineLeft(I)F

    move-result p1

    cmpl-float v7, v7, p1

    if-ltz v7, :cond_5

    int-to-float v7, v4

    iget-object p1, p0, LX/1oF;->a:Landroid/text/Layout;

    invoke-virtual {p1, v2}, Landroid/text/Layout;->getLineRight(I)F

    move-result p1

    cmpg-float v7, v7, p1

    if-gtz v7, :cond_5

    .line 318754
    iget-object v7, p0, LX/1oF;->a:Landroid/text/Layout;

    int-to-float p1, v4

    invoke-virtual {v7, v2, p1}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v7

    .line 318755
    iget-object v2, p0, LX/1oF;->e:Ljava/lang/CharSequence;

    check-cast v2, Landroid/text/Spanned;

    const-class p1, Landroid/text/style/ClickableSpan;

    invoke-interface {v2, v7, v7, p1}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/text/style/ClickableSpan;

    .line 318756
    if-eqz v2, :cond_5

    array-length v7, v2

    if-lez v7, :cond_5

    .line 318757
    const/4 v7, 0x0

    aget-object v2, v2, v7

    .line 318758
    :goto_1
    move-object v2, v2

    .line 318759
    if-nez v2, :cond_1

    .line 318760
    int-to-float v2, v4

    int-to-float v4, v5

    invoke-direct {p0, v2, v4, v6}, LX/1oF;->a(FFF)Landroid/text/style/ClickableSpan;

    move-result-object v2

    .line 318761
    :cond_1
    if-eqz v2, :cond_4

    .line 318762
    if-ne v3, v1, :cond_3

    .line 318763
    invoke-direct {p0}, LX/1oF;->d()V

    .line 318764
    invoke-virtual {v2, p2}, Landroid/text/style/ClickableSpan;->onClick(Landroid/view/View;)V

    :cond_2
    :goto_2
    move v0, v1

    .line 318765
    goto :goto_0

    .line 318766
    :cond_3
    if-nez v3, :cond_2

    .line 318767
    iget-object v0, p0, LX/1oF;->e:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    .line 318768
    invoke-interface {v0, v2}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v3

    invoke-interface {v0, v2}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    invoke-static {p0, v3, v0}, LX/1oF;->b(LX/1oF;II)V

    .line 318769
    goto :goto_2

    .line 318770
    :cond_4
    invoke-direct {p0}, LX/1oF;->d()V

    goto/16 :goto_0

    :cond_5
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 318728
    iget-object v0, p0, LX/1oF;->a:Landroid/text/Layout;

    if-nez v0, :cond_0

    .line 318729
    :goto_0
    return-void

    .line 318730
    :cond_0
    invoke-virtual {p0}, LX/1oF;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 318731
    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, p0, LX/1oF;->c:F

    add-float/2addr v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 318732
    iget-object v1, p0, LX/1oF;->a:Landroid/text/Layout;

    const/4 v2, 0x0

    .line 318733
    iget v3, p0, LX/1oF;->j:I

    iget v4, p0, LX/1oF;->k:I

    if-ne v3, v4, :cond_2

    .line 318734
    :cond_1
    :goto_1
    move-object v2, v2

    .line 318735
    iget-object v3, p0, LX/1oF;->o:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v1, p1, v2, v3, v4}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    .line 318736
    iget v1, v0, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    int-to-float v1, v1

    iget v0, v0, Landroid/graphics/Rect;->top:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v2, p0, LX/1oF;->c:F

    sub-float/2addr v0, v2

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0

    .line 318737
    :cond_2
    iget v3, p0, LX/1oF;->h:I

    invoke-static {v3}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    if-eqz v3, :cond_1

    .line 318738
    iget-boolean v2, p0, LX/1oF;->n:Z

    if-eqz v2, :cond_4

    .line 318739
    iget-object v2, p0, LX/1oF;->l:Landroid/graphics/Path;

    if-nez v2, :cond_3

    .line 318740
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    iput-object v2, p0, LX/1oF;->l:Landroid/graphics/Path;

    .line 318741
    :cond_3
    iget-object v2, p0, LX/1oF;->a:Landroid/text/Layout;

    iget v3, p0, LX/1oF;->j:I

    iget v4, p0, LX/1oF;->k:I

    iget-object v5, p0, LX/1oF;->l:Landroid/graphics/Path;

    invoke-virtual {v2, v3, v4, v5}, Landroid/text/Layout;->getSelectionPath(IILandroid/graphics/Path;)V

    .line 318742
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/1oF;->n:Z

    .line 318743
    :cond_4
    iget-object v2, p0, LX/1oF;->l:Landroid/graphics/Path;

    goto :goto_1
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 318727
    const/4 v0, 0x0

    return v0
.end method

.method public final isStateful()Z
    .locals 1

    .prologue
    .line 318726
    iget-object v0, p0, LX/1oF;->f:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onStateChange([I)Z
    .locals 3

    .prologue
    .line 318719
    iget-object v0, p0, LX/1oF;->f:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1oF;->a:Landroid/text/Layout;

    if-eqz v0, :cond_0

    .line 318720
    iget-object v0, p0, LX/1oF;->a:Landroid/text/Layout;

    invoke-virtual {v0}, Landroid/text/Layout;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/TextPaint;->getColor()I

    move-result v0

    .line 318721
    iget-object v1, p0, LX/1oF;->f:Landroid/content/res/ColorStateList;

    iget v2, p0, LX/1oF;->g:I

    invoke-virtual {v1, p1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    .line 318722
    if-eq v1, v0, :cond_0

    .line 318723
    iget-object v0, p0, LX/1oF;->a:Landroid/text/Layout;

    invoke-virtual {v0}, Landroid/text/Layout;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 318724
    invoke-virtual {p0}, LX/1oF;->invalidateSelf()V

    .line 318725
    :cond_0
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onStateChange([I)Z

    move-result v0

    return v0
.end method

.method public final setAlpha(I)V
    .locals 0

    .prologue
    .line 318718
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 318717
    return-void
.end method
