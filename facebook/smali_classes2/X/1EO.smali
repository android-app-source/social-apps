.class public final enum LX/1EO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1EO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1EO;

.field public static final enum CHANNEL_VIDEO_PLAYER:LX/1EO;

.field public static final enum EVENT:LX/1EO;

.field public static final enum FULLSCREEN_VIDEO_PLAYER:LX/1EO;

.field public static final enum GOODWILL_THROWBACK:LX/1EO;

.field public static final enum GOOD_FRIENDS:LX/1EO;

.field public static final enum GROUP:LX/1EO;

.field public static final enum NEWSFEED:LX/1EO;

.field public static final enum PAGE:LX/1EO;

.field public static final enum PERMALINK:LX/1EO;

.field public static final enum PHOTOS_FEED:LX/1EO;

.field public static final enum REACTION:LX/1EO;

.field public static final enum SEARCH:LX/1EO;

.field public static final enum TIMELINE:LX/1EO;

.field public static final enum UNKNOWN:LX/1EO;

.field public static final enum VIDEO_CHANNEL:LX/1EO;

.field public static final enum VIDEO_HOME:LX/1EO;


# instance fields
.field public final analyticModule:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 219759
    new-instance v0, LX/1EO;

    const-string v1, "NEWSFEED"

    const-string v2, "native_newsfeed"

    invoke-direct {v0, v1, v4, v2}, LX/1EO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1EO;->NEWSFEED:LX/1EO;

    .line 219760
    new-instance v0, LX/1EO;

    const-string v1, "PERMALINK"

    const-string v2, "native_permalink"

    invoke-direct {v0, v1, v5, v2}, LX/1EO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1EO;->PERMALINK:LX/1EO;

    .line 219761
    new-instance v0, LX/1EO;

    const-string v1, "REACTION"

    const-string v2, "reaction_dialog"

    invoke-direct {v0, v1, v6, v2}, LX/1EO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1EO;->REACTION:LX/1EO;

    .line 219762
    new-instance v0, LX/1EO;

    const-string v1, "CHANNEL_VIDEO_PLAYER"

    const-string v2, "video_channel_player"

    invoke-direct {v0, v1, v7, v2}, LX/1EO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1EO;->CHANNEL_VIDEO_PLAYER:LX/1EO;

    .line 219763
    new-instance v0, LX/1EO;

    const-string v1, "FULLSCREEN_VIDEO_PLAYER"

    const-string v2, "video_fullscreen_player"

    invoke-direct {v0, v1, v8, v2}, LX/1EO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1EO;->FULLSCREEN_VIDEO_PLAYER:LX/1EO;

    .line 219764
    new-instance v0, LX/1EO;

    const-string v1, "VIDEO_HOME"

    const/4 v2, 0x5

    const-string v3, "video_home"

    invoke-direct {v0, v1, v2, v3}, LX/1EO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1EO;->VIDEO_HOME:LX/1EO;

    .line 219765
    new-instance v0, LX/1EO;

    const-string v1, "VIDEO_CHANNEL"

    const/4 v2, 0x6

    const-string v3, "video_channel_feed"

    invoke-direct {v0, v1, v2, v3}, LX/1EO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1EO;->VIDEO_CHANNEL:LX/1EO;

    .line 219766
    new-instance v0, LX/1EO;

    const-string v1, "TIMELINE"

    const/4 v2, 0x7

    const-string v3, "native_timeline"

    invoke-direct {v0, v1, v2, v3}, LX/1EO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1EO;->TIMELINE:LX/1EO;

    .line 219767
    new-instance v0, LX/1EO;

    const-string v1, "PAGE"

    const/16 v2, 0x8

    const-string v3, "pages_public_view"

    invoke-direct {v0, v1, v2, v3}, LX/1EO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1EO;->PAGE:LX/1EO;

    .line 219768
    new-instance v0, LX/1EO;

    const-string v1, "EVENT"

    const/16 v2, 0x9

    const-string v3, "event_feed"

    invoke-direct {v0, v1, v2, v3}, LX/1EO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1EO;->EVENT:LX/1EO;

    .line 219769
    new-instance v0, LX/1EO;

    const-string v1, "GROUP"

    const/16 v2, 0xa

    const-string v3, "group_feed"

    invoke-direct {v0, v1, v2, v3}, LX/1EO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1EO;->GROUP:LX/1EO;

    .line 219770
    new-instance v0, LX/1EO;

    const-string v1, "SEARCH"

    const/16 v2, 0xb

    const-string v3, "search"

    invoke-direct {v0, v1, v2, v3}, LX/1EO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1EO;->SEARCH:LX/1EO;

    .line 219771
    new-instance v0, LX/1EO;

    const-string v1, "GOODWILL_THROWBACK"

    const/16 v2, 0xc

    const-string v3, "goodwill_throwback"

    invoke-direct {v0, v1, v2, v3}, LX/1EO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1EO;->GOODWILL_THROWBACK:LX/1EO;

    .line 219772
    new-instance v0, LX/1EO;

    const-string v1, "PHOTOS_FEED"

    const/16 v2, 0xd

    const-string v3, "photos_feed"

    invoke-direct {v0, v1, v2, v3}, LX/1EO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1EO;->PHOTOS_FEED:LX/1EO;

    .line 219773
    new-instance v0, LX/1EO;

    const-string v1, "GOOD_FRIENDS"

    const/16 v2, 0xe

    const-string v3, "good_friends_feed"

    invoke-direct {v0, v1, v2, v3}, LX/1EO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1EO;->GOOD_FRIENDS:LX/1EO;

    .line 219774
    new-instance v0, LX/1EO;

    const-string v1, "UNKNOWN"

    const/16 v2, 0xf

    const-string v3, "unknown"

    invoke-direct {v0, v1, v2, v3}, LX/1EO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1EO;->UNKNOWN:LX/1EO;

    .line 219775
    const/16 v0, 0x10

    new-array v0, v0, [LX/1EO;

    sget-object v1, LX/1EO;->NEWSFEED:LX/1EO;

    aput-object v1, v0, v4

    sget-object v1, LX/1EO;->PERMALINK:LX/1EO;

    aput-object v1, v0, v5

    sget-object v1, LX/1EO;->REACTION:LX/1EO;

    aput-object v1, v0, v6

    sget-object v1, LX/1EO;->CHANNEL_VIDEO_PLAYER:LX/1EO;

    aput-object v1, v0, v7

    sget-object v1, LX/1EO;->FULLSCREEN_VIDEO_PLAYER:LX/1EO;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/1EO;->VIDEO_HOME:LX/1EO;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1EO;->VIDEO_CHANNEL:LX/1EO;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/1EO;->TIMELINE:LX/1EO;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/1EO;->PAGE:LX/1EO;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/1EO;->EVENT:LX/1EO;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/1EO;->GROUP:LX/1EO;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/1EO;->SEARCH:LX/1EO;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/1EO;->GOODWILL_THROWBACK:LX/1EO;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/1EO;->PHOTOS_FEED:LX/1EO;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/1EO;->GOOD_FRIENDS:LX/1EO;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/1EO;->UNKNOWN:LX/1EO;

    aput-object v2, v0, v1

    sput-object v0, LX/1EO;->$VALUES:[LX/1EO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 219776
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 219777
    iput-object p3, p0, LX/1EO;->analyticModule:Ljava/lang/String;

    .line 219778
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1EO;
    .locals 1

    .prologue
    .line 219779
    const-class v0, LX/1EO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1EO;

    return-object v0
.end method

.method public static values()[LX/1EO;
    .locals 1

    .prologue
    .line 219780
    sget-object v0, LX/1EO;->$VALUES:[LX/1EO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1EO;

    return-object v0
.end method
