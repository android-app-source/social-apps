.class public LX/1WM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/1WM;


# instance fields
.field private final a:LX/1WN;

.field public final b:LX/0oz;

.field private final c:LX/0Uh;

.field public final d:LX/0ad;

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/BJ2;


# direct methods
.method public constructor <init>(LX/1WN;LX/0oz;LX/0Uh;LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 268049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 268050
    iput-object p1, p0, LX/1WM;->a:LX/1WN;

    .line 268051
    iput-object p2, p0, LX/1WM;->b:LX/0oz;

    .line 268052
    iput-object p3, p0, LX/1WM;->c:LX/0Uh;

    .line 268053
    iput-object p4, p0, LX/1WM;->d:LX/0ad;

    .line 268054
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1WM;->e:Ljava/util/Set;

    .line 268055
    return-void
.end method

.method public static a(LX/0QB;)LX/1WM;
    .locals 7

    .prologue
    .line 268056
    sget-object v0, LX/1WM;->g:LX/1WM;

    if-nez v0, :cond_1

    .line 268057
    const-class v1, LX/1WM;

    monitor-enter v1

    .line 268058
    :try_start_0
    sget-object v0, LX/1WM;->g:LX/1WM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 268059
    if-eqz v2, :cond_0

    .line 268060
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 268061
    new-instance p0, LX/1WM;

    invoke-static {v0}, LX/1WN;->b(LX/0QB;)LX/1WN;

    move-result-object v3

    check-cast v3, LX/1WN;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v4

    check-cast v4, LX/0oz;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1WM;-><init>(LX/1WN;LX/0oz;LX/0Uh;LX/0ad;)V

    .line 268062
    move-object v0, p0

    .line 268063
    sput-object v0, LX/1WM;->g:LX/1WM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268064
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 268065
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 268066
    :cond_1
    sget-object v0, LX/1WM;->g:LX/1WM;

    return-object v0

    .line 268067
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 268068
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 268048
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public static b(LX/1WM;)Z
    .locals 3

    .prologue
    .line 268047
    iget-object v0, p0, LX/1WM;->c:LX/0Uh;

    const/16 v1, 0x224

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public static b(LX/5kD;)Z
    .locals 1

    .prologue
    .line 268046
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/5kD;->ah()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLMedia;)Z
    .locals 1

    .prologue
    .line 268045
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bg()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLMedia;)Z
    .locals 1

    .prologue
    .line 268069
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->am()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/33B;
    .locals 3

    .prologue
    .line 268042
    iget-object v0, p0, LX/1WM;->f:LX/BJ2;

    if-nez v0, :cond_0

    .line 268043
    new-instance v0, LX/BJ2;

    const/16 v1, 0x1e

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/BJ2;-><init>(II)V

    iput-object v0, p0, LX/1WM;->f:LX/BJ2;

    .line 268044
    :cond_0
    iget-object v0, p0, LX/1WM;->f:LX/BJ2;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 268038
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 268039
    iget-object v0, p0, LX/1WM;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 268040
    iget-object v0, p0, LX/1WM;->a:LX/1WN;

    const-string v1, "warning_screen_dismissed"

    invoke-virtual {v0, v1, p2, p1}, LX/1WN;->a(Ljava/lang/String;ZLjava/lang/String;)V

    .line 268041
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 268031
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 268032
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 268033
    invoke-static {p0}, LX/1WM;->b(LX/1WM;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 268034
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 268035
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v2, 0x39d284c8

    invoke-static {v1, v2}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v1

    .line 268036
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->D()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 268037
    if-eqz v1, :cond_0

    invoke-static {v0}, LX/1WM;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1WM;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLMedia;)Z
    .locals 1

    .prologue
    .line 268030
    invoke-static {p0}, LX/1WM;->b(LX/1WM;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, LX/1WM;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/1WM;->c(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1WM;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 268025
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1WM;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 268026
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 268027
    iget-object v0, p0, LX/1WM;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 268028
    iget-object v0, p0, LX/1WM;->a:LX/1WN;

    const-string v1, "warning_screen_reenabled"

    invoke-virtual {v0, v1, p2, p1}, LX/1WN;->a(Ljava/lang/String;ZLjava/lang/String;)V

    .line 268029
    :cond_0
    return-void
.end method
