.class public LX/11h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/11i;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile v:LX/11h;


# instance fields
.field public final a:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "LX/11p",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final c:LX/0So;

.field private final d:LX/0SG;

.field private final e:LX/0Zb;

.field private final f:LX/0XZ;

.field private final g:LX/0Wd;

.field public final h:LX/0Uo;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0kb;

.field private final k:LX/11l;

.field private final l:LX/0Xh;

.field private final m:LX/0YR;

.field private final n:Lcom/facebook/sequencelogger/SequenceLoggerImpl$UploadRunnable;

.field private volatile o:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Object;",
            "LX/11p",
            "<*>;>;"
        }
    .end annotation
.end field

.field private p:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/sequencelogger/SequenceListener;",
            ">;"
        }
    .end annotation
.end field

.field public final q:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private r:J

.field private s:J

.field private final t:LX/11n;

.field private u:LX/03R;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 173076
    const-class v0, LX/11h;

    sput-object v0, LX/11h;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0So;LX/0SG;LX/0Zb;LX/0Xh;LX/0XZ;LX/0Wd;LX/0Uo;LX/0Ot;LX/11l;LX/0Xl;LX/0YR;LX/0kb;Ljava/util/Set;)V
    .locals 2
    .param p6    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .param p10    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p13    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0So;",
            "LX/0SG;",
            "LX/0Zb;",
            "Lcom/facebook/quicklog/DebugAndTestConfig;",
            "LX/0XZ;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            "LX/0Uo;",
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;",
            "LX/11l;",
            "LX/0Xl;",
            "LX/0YR;",
            "LX/0kb;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/sequencelogger/SequenceListener;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 172950
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172951
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/11h;->r:J

    .line 172952
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/11h;->s:J

    .line 172953
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/11h;->u:LX/03R;

    .line 172954
    iput-object p2, p0, LX/11h;->d:LX/0SG;

    .line 172955
    iput-object p1, p0, LX/11h;->c:LX/0So;

    .line 172956
    iput-object p3, p0, LX/11h;->e:LX/0Zb;

    .line 172957
    iput-object p4, p0, LX/11h;->l:LX/0Xh;

    .line 172958
    iput-object p5, p0, LX/11h;->f:LX/0XZ;

    .line 172959
    iput-object p6, p0, LX/11h;->g:LX/0Wd;

    .line 172960
    iput-object p7, p0, LX/11h;->h:LX/0Uo;

    .line 172961
    iput-object p8, p0, LX/11h;->i:LX/0Ot;

    .line 172962
    iput-object p12, p0, LX/11h;->j:LX/0kb;

    .line 172963
    iput-object p9, p0, LX/11h;->k:LX/11l;

    .line 172964
    iput-object p11, p0, LX/11h;->m:LX/0YR;

    .line 172965
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, LX/11h;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 172966
    new-instance v0, Lcom/facebook/sequencelogger/SequenceLoggerImpl$UploadRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/sequencelogger/SequenceLoggerImpl$UploadRunnable;-><init>(LX/11h;)V

    iput-object v0, p0, LX/11h;->n:Lcom/facebook/sequencelogger/SequenceLoggerImpl$UploadRunnable;

    .line 172967
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/11h;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 172968
    new-instance v0, LX/11n;

    invoke-direct {v0}, LX/11n;-><init>()V

    iput-object v0, p0, LX/11h;->t:LX/11n;

    .line 172969
    iput-object p13, p0, LX/11h;->p:Ljava/util/Set;

    .line 172970
    iget-object v0, p0, LX/11h;->l:LX/0Xh;

    invoke-virtual {v0}, LX/0Xh;->b()Z

    move-result v0

    sput-boolean v0, LX/11p;->b:Z

    .line 172971
    invoke-interface {p10}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance p1, LX/11q;

    invoke-direct {p1, p0}, LX/11q;-><init>(LX/11h;)V

    invoke-interface {v0, v1, p1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 172972
    return-void
.end method

.method public static a(LX/0QB;)LX/11h;
    .locals 3

    .prologue
    .line 172973
    sget-object v0, LX/11h;->v:LX/11h;

    if-nez v0, :cond_1

    .line 172974
    const-class v1, LX/11h;

    monitor-enter v1

    .line 172975
    :try_start_0
    sget-object v0, LX/11h;->v:LX/11h;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 172976
    if-eqz v2, :cond_0

    .line 172977
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/11h;->b(LX/0QB;)LX/11h;

    move-result-object v0

    sput-object v0, LX/11h;->v:LX/11h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 172978
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 172979
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 172980
    :cond_1
    sget-object v0, LX/11h;->v:LX/11h;

    return-object v0

    .line 172981
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 172982
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/0Pq;Ljava/lang/String;LX/0P1;JLjava/lang/Boolean;)LX/11o;
    .locals 12
    .param p6    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/lang/Boolean;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 172983
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 172984
    invoke-static {p1, p2}, LX/11h;->c(LX/0Pq;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    .line 172985
    invoke-static {p0}, LX/11h;->c(LX/11h;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    invoke-interface {v0, v10}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11p;

    .line 172986
    if-eqz v0, :cond_1

    .line 172987
    invoke-virtual {v0}, LX/11p;->i()V

    .line 172988
    invoke-virtual {v0}, LX/11p;->d()Z

    move-result v2

    .line 172989
    invoke-virtual {v0}, LX/11p;->e()Z

    move-result v1

    .line 172990
    invoke-virtual {v0}, LX/11p;->c()I

    move-result v0

    move v8, v1

    move v9, v2

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide/from16 v4, p4

    move-object v6, p3

    move-object/from16 v7, p6

    .line 172991
    invoke-static/range {v1 .. v7}, LX/11h;->a(LX/11h;LX/0Pq;Ljava/lang/String;JLX/0P1;Ljava/lang/Boolean;)LX/11p;

    move-result-object v1

    .line 172992
    invoke-static {p0}, LX/11h;->c(LX/11h;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v2

    invoke-interface {v2, v10, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 172993
    const-string v2, "Sequence was restarted"

    invoke-virtual {v1, v2}, LX/11p;->g(Ljava/lang/String;)V

    .line 172994
    :cond_0
    :goto_1
    invoke-virtual {v1, v0}, LX/11p;->a(I)V

    .line 172995
    invoke-virtual {v1, v9}, LX/11p;->a(Z)V

    .line 172996
    invoke-virtual {v1, v8}, LX/11p;->b(Z)V

    .line 172997
    invoke-static {p0, p1, p2}, LX/11h;->d(LX/11h;LX/0Pq;Ljava/lang/String;)V

    move-object v0, v1

    .line 172998
    :goto_2
    return-object v0

    .line 172999
    :cond_1
    iget-object v0, p0, LX/11h;->f:LX/0XZ;

    invoke-virtual {v0}, LX/0XZ;->c()LX/0ap;

    move-result-object v2

    .line 173000
    if-nez v2, :cond_3

    const/4 v1, 0x1

    .line 173001
    :goto_3
    if-nez v1, :cond_2

    iget-object v0, p0, LX/11h;->l:LX/0Xh;

    invoke-virtual {v0}, LX/0Xh;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/11h;->l:LX/0Xh;

    invoke-virtual {v0}, LX/0Xh;->d()Z

    move-result v0

    if-nez v0, :cond_4

    .line 173002
    :cond_2
    const/4 v2, 0x1

    .line 173003
    const/4 v0, 0x1

    .line 173004
    :goto_4
    const v3, 0x7fffffff

    if-ne v0, v3, :cond_6

    .line 173005
    invoke-static {p0, p1, p2}, LX/11h;->d(LX/11h;LX/0Pq;Ljava/lang/String;)V

    .line 173006
    iget-object v0, p0, LX/11h;->t:LX/11n;

    goto :goto_2

    .line 173007
    :cond_3
    const/4 v1, 0x0

    goto :goto_3

    .line 173008
    :cond_4
    invoke-virtual {p1}, LX/0Pq;->e()I

    move-result v0

    iget-object v3, p0, LX/11h;->f:LX/0XZ;

    invoke-virtual {v3}, LX/0XZ;->d()Ljava/util/Random;

    move-result-object v3

    invoke-static {v2, v0, v3}, LX/0au;->a(LX/0ap;ILjava/util/Random;)I

    move-result v0

    .line 173009
    invoke-virtual {v2}, LX/0ap;->a()Z

    move-result v2

    goto :goto_4

    .line 173010
    :cond_5
    invoke-static {p0}, LX/11h;->d(LX/11h;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 173011
    invoke-virtual {p1}, LX/0Pq;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Starting Sequence"

    invoke-static {v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    move v8, v1

    move v9, v2

    goto :goto_0
.end method

.method private static a(LX/11h;LX/0Pq;Ljava/lang/String;JLX/0P1;Ljava/lang/Boolean;)LX/11p;
    .locals 11
    .param p1    # LX/0Pq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "Ljava/lang/String;",
            "J",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            ")",
            "LX/11p",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173012
    new-instance v1, LX/11p;

    iget-object v4, p0, LX/11h;->c:LX/0So;

    iget-object v5, p0, LX/11h;->d:LX/0SG;

    move-object v2, p1

    move-object v3, p2

    move-wide v6, p3

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v1 .. v9}, LX/11p;-><init>(LX/0Pq;Ljava/lang/String;LX/0So;LX/0SG;JLX/0P1;Ljava/lang/Boolean;)V

    return-object v1
.end method

.method private static a(LX/11h;LX/0Pq;LX/11p;LX/0P1;JLjava/lang/Boolean;)V
    .locals 10
    .param p4    # J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "LX/11p",
            "<*>;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 173013
    const/4 v0, 0x0

    .line 173014
    invoke-direct {p0, p2}, LX/11h;->b(LX/11p;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 173015
    iget-object v1, p0, LX/11h;->m:LX/0YR;

    invoke-interface {v1}, LX/0YR;->a()LX/1hM;

    move-result-object v1

    .line 173016
    if-eqz v1, :cond_0

    .line 173017
    invoke-virtual {v1}, LX/1hM;->d()Ljava/util/List;

    move-result-object v0

    .line 173018
    :cond_0
    new-array v6, v8, [Ljava/util/List;

    aput-object v0, v6, v7

    move-object v1, p2

    move-wide v2, p4

    move-object v4, p3

    move-object/from16 v5, p6

    invoke-virtual/range {v1 .. v6}, LX/11p;->a(JLX/0P1;Ljava/lang/Boolean;[Ljava/util/List;)J

    move-result-wide v0

    .line 173019
    iget-object v2, p0, LX/11h;->l:LX/0Xh;

    invoke-virtual {v2}, LX/0Xh;->e()Z

    move-result v2

    if-nez v2, :cond_3

    .line 173020
    iget-object v2, p0, LX/11h;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v2, p2}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 173021
    invoke-direct {p0}, LX/11h;->b()V

    .line 173022
    :cond_1
    :goto_0
    iget-object v2, p0, LX/11h;->l:LX/0Xh;

    invoke-virtual {v2}, LX/0Xh;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 173023
    invoke-virtual {p1}, LX/0Pq;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p4, p5, v0, v1}, LX/2BZ;->a(Ljava/lang/String;JJ)V

    .line 173024
    :cond_2
    :goto_1
    return-void

    .line 173025
    :cond_3
    invoke-static {p0, p2}, LX/11h;->a$redex0(LX/11h;LX/11p;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 173026
    invoke-virtual {p1}, LX/0Pq;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Could not deliver event at this time!"

    invoke-static {v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 173027
    :cond_4
    invoke-static {p0}, LX/11h;->d(LX/11h;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 173028
    invoke-virtual {p1}, LX/0Pq;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Stopped sequence; Monotonic Timestamp (ms): %d; Total Elapsed: %d ms"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v8

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private static a(LX/11h;LX/0Pq;Ljava/lang/String;S)V
    .locals 11

    .prologue
    .line 173029
    iget-object v0, p0, LX/11h;->p:Ljava/util/Set;

    if-nez v0, :cond_1

    .line 173030
    :cond_0
    return-void

    .line 173031
    :cond_1
    if-nez p2, :cond_3

    const/4 v0, 0x0

    move v1, v0

    .line 173032
    :goto_0
    iget-object v0, p0, LX/11h;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oB;

    .line 173033
    const/16 v6, 0x10

    .line 173034
    const/4 v3, 0x4

    const/16 v4, 0x38

    .line 173035
    iget v5, p1, LX/0Pq;->e:I

    move v5, v5

    .line 173036
    int-to-long v7, v1

    shl-long/2addr v7, v6

    int-to-long v9, p3

    or-long/2addr v7, v9

    invoke-static {v3, v4, v5, v7, v8}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    .line 173037
    sget-object v3, LX/02r;->b:LX/02r;

    move-object v3, v3

    .line 173038
    if-eqz v3, :cond_2

    .line 173039
    invoke-virtual {v3, v6, p1, v1}, LX/02r;->a(ILjava/lang/Object;I)V

    .line 173040
    :cond_2
    goto :goto_1

    .line 173041
    :cond_3
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    move v1, v0

    goto :goto_0
.end method

.method public static a$redex0(LX/11h;LX/11p;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11p",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 173042
    const/4 v1, 0x1

    .line 173043
    sget-object v0, LX/3nZ;->a:[I

    .line 173044
    iget-object v2, p1, LX/11p;->c:LX/0Pq;

    move-object v2, v2

    .line 173045
    iget-boolean v3, v2, LX/0Pq;->b:Z

    move v3, v3

    .line 173046
    if-eqz v3, :cond_1

    .line 173047
    iget-object v3, p0, LX/11h;->h:LX/0Uo;

    invoke-virtual {v3}, LX/0Uo;->k()LX/03R;

    move-result-object v3

    .line 173048
    :goto_0
    move-object v2, v3

    .line 173049
    invoke-virtual {v2}, LX/03R;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 173050
    :goto_1
    return v0

    .line 173051
    :pswitch_0
    iget-object v0, p1, LX/11p;->f:LX/3nV;

    invoke-virtual {v0}, LX/3nV;->c()Ljava/lang/Boolean;

    .line 173052
    move v0, v1

    .line 173053
    goto :goto_1

    .line 173054
    :pswitch_1
    iget-object v0, p1, LX/11p;->f:LX/3nV;

    invoke-virtual {v0}, LX/3nV;->b()Lcom/facebook/sequencelogger/HoneySequenceLoggerEvent;

    move-result-object v0

    move-object v2, v0

    .line 173055
    const-string v3, "connqual"

    iget-object v0, p0, LX/11h;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oz;

    invoke-virtual {v0}, LX/0oz;->c()LX/0p3;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 173056
    const-string v0, "network_type"

    iget-object v3, p0, LX/11h;->j:LX/0kb;

    invoke-virtual {v3}, LX/0kb;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 173057
    const-string v0, "network_subtype"

    iget-object v3, p0, LX/11h;->j:LX/0kb;

    invoke-virtual {v3}, LX/0kb;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 173058
    const-string v0, "sample_rate"

    .line 173059
    iget v3, p1, LX/11p;->g:I

    move v3, v3

    .line 173060
    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 173061
    const-string v0, "method"

    .line 173062
    iget-boolean v3, p1, LX/11p;->i:Z

    move v3, v3

    .line 173063
    iget-boolean v4, p1, LX/11p;->h:Z

    move v4, v4

    .line 173064
    invoke-static {v3, v4}, LX/17y;->a(ZZ)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 173065
    iget-boolean v0, p1, LX/11p;->j:Z

    move v0, v0

    .line 173066
    if-eqz v0, :cond_0

    .line 173067
    const-string v0, "status"

    const-string v3, "failed"

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 173068
    :cond_0
    const-string v0, "marker_id"

    .line 173069
    iget-object v3, p1, LX/11p;->c:LX/0Pq;

    move-object v3, v3

    .line 173070
    iget v4, v3, LX/0Pq;->e:I

    move v3, v4

    .line 173071
    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 173072
    iget-object v0, p0, LX/11h;->e:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 173073
    iget-object v0, p0, LX/11h;->k:LX/11l;

    invoke-virtual {v0, v2}, LX/11l;->a(Lcom/facebook/sequencelogger/HoneySequenceLoggerEvent;)V

    move v0, v1

    .line 173074
    goto :goto_1

    .line 173075
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    sget-object v3, LX/03R;->NO:LX/03R;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static b(LX/0QB;)LX/11h;
    .locals 14

    .prologue
    .line 173077
    new-instance v0, LX/11h;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {p0}, LX/0Xh;->a(LX/0QB;)LX/0Xh;

    move-result-object v4

    check-cast v4, LX/0Xh;

    invoke-static {p0}, LX/0XZ;->a(LX/0QB;)LX/0XZ;

    move-result-object v5

    check-cast v5, LX/0XZ;

    invoke-static {p0}, LX/11j;->a(LX/0QB;)LX/0Wd;

    move-result-object v6

    check-cast v6, LX/0Wd;

    invoke-static {p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v7

    check-cast v7, LX/0Uo;

    const/16 v8, 0x24e

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/11k;->a(LX/0QB;)LX/11k;

    move-result-object v9

    check-cast v9, LX/11l;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v10

    check-cast v10, LX/0Xl;

    invoke-static {p0}, LX/0YQ;->a(LX/0QB;)LX/0YQ;

    move-result-object v11

    check-cast v11, LX/0YR;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v12

    check-cast v12, LX/0kb;

    invoke-static {p0}, LX/11m;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v13

    invoke-direct/range {v0 .. v13}, LX/11h;-><init>(LX/0So;LX/0SG;LX/0Zb;LX/0Xh;LX/0XZ;LX/0Wd;LX/0Uo;LX/0Ot;LX/11l;LX/0Xl;LX/0YR;LX/0kb;Ljava/util/Set;)V

    .line 173078
    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 173079
    iget-object v0, p0, LX/11h;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173080
    iget-object v0, p0, LX/11h;->g:LX/0Wd;

    iget-object v1, p0, LX/11h;->n:Lcom/facebook/sequencelogger/SequenceLoggerImpl$UploadRunnable;

    const v2, 0x4ea633f0

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 173081
    :cond_0
    return-void
.end method

.method private b(LX/0Pq;Ljava/lang/String;LX/0P1;JLjava/lang/Boolean;)V
    .locals 8
    .param p6    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 173082
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 173083
    invoke-static {p1, p2}, LX/11h;->c(LX/0Pq;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 173084
    invoke-static {p0}, LX/11h;->c(LX/11h;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/11p;

    .line 173085
    const/4 v0, 0x2

    invoke-static {p0, p1, p2, v0}, LX/11h;->a(LX/11h;LX/0Pq;Ljava/lang/String;S)V

    .line 173086
    if-nez v2, :cond_0

    .line 173087
    :goto_0
    return-void

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-wide v4, p4

    move-object v6, p6

    .line 173088
    invoke-static/range {v0 .. v6}, LX/11h;->a(LX/11h;LX/0Pq;LX/11p;LX/0P1;JLjava/lang/Boolean;)V

    goto :goto_0
.end method

.method private b(LX/11p;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11p",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const v5, 0x7fffffff

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 173089
    iget-object v2, p1, LX/11p;->c:LX/0Pq;

    move-object v2, v2

    .line 173090
    iget v3, v2, LX/0Pq;->f:I

    const v4, 0x7fffffff

    if-eq v3, v4, :cond_3

    const/4 v3, 0x1

    :goto_0
    move v2, v3

    .line 173091
    if-nez v2, :cond_1

    .line 173092
    :cond_0
    :goto_1
    return v0

    .line 173093
    :cond_1
    iget-object v2, p0, LX/11h;->f:LX/0XZ;

    invoke-virtual {v2}, LX/0XZ;->c()LX/0ap;

    move-result-object v2

    .line 173094
    if-eqz v2, :cond_2

    .line 173095
    const v3, 0x5d0001

    iget-object v4, p0, LX/11h;->f:LX/0XZ;

    .line 173096
    iget-object p0, v4, LX/0XZ;->d:Ljava/util/Random;

    move-object v4, p0

    .line 173097
    invoke-static {v2, v3, v4}, LX/0au;->a(LX/0ap;ILjava/util/Random;)I

    move-result v2

    if-eq v2, v5, :cond_0

    move v0, v1

    goto :goto_1

    .line 173098
    :cond_2
    iget-object v2, p1, LX/11p;->c:LX/0Pq;

    move-object v2, v2

    .line 173099
    iget v3, v2, LX/0Pq;->f:I

    move v2, v3

    .line 173100
    iget-object v3, p0, LX/11h;->f:LX/0XZ;

    .line 173101
    iget-object v4, v3, LX/0XZ;->d:Ljava/util/Random;

    move-object v3, v4

    .line 173102
    invoke-static {v2, v3}, LX/0au;->a(ILjava/util/Random;)I

    move-result v2

    if-eq v2, v5, :cond_0

    move v0, v1

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static c(LX/0Pq;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 173103
    if-nez p1, :cond_0

    .line 173104
    iget-object v0, p0, LX/0Pq;->a:Ljava/lang/String;

    move-object v0, v0

    .line 173105
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/1ce;

    .line 173106
    iget-object v1, p0, LX/0Pq;->a:Ljava/lang/String;

    move-object v1, v1

    .line 173107
    invoke-direct {v0, v1, p1}, LX/1ce;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static c(LX/11h;)Ljava/util/concurrent/ConcurrentMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Object;",
            "LX/11p",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 173108
    iget-object v0, p0, LX/11h;->o:Ljava/util/concurrent/ConcurrentMap;

    .line 173109
    if-nez v0, :cond_1

    .line 173110
    monitor-enter p0

    .line 173111
    :try_start_0
    iget-object v0, p0, LX/11h;->o:Ljava/util/concurrent/ConcurrentMap;

    .line 173112
    if-nez v0, :cond_0

    .line 173113
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/11h;->o:Ljava/util/concurrent/ConcurrentMap;

    .line 173114
    :cond_0
    monitor-exit p0

    .line 173115
    :cond_1
    return-object v0

    .line 173116
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static d(LX/11h;LX/0Pq;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 172929
    iget-object v0, p0, LX/11h;->p:Ljava/util/Set;

    if-nez v0, :cond_1

    .line 172930
    :cond_0
    return-void

    .line 172931
    :cond_1
    if-nez p2, :cond_3

    const/4 v0, 0x0

    move v1, v0

    .line 172932
    :goto_0
    iget-object v0, p0, LX/11h;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oB;

    .line 172933
    const/16 v6, 0x10

    const/4 v3, 0x0

    .line 172934
    sget-object v4, LX/02r;->b:LX/02r;

    move-object v4, v4

    .line 172935
    if-eqz v4, :cond_4

    .line 172936
    invoke-virtual {v4, v6, v3, p1, v1}, LX/02r;->a(IILjava/lang/Object;I)Z

    move-result v3

    move v5, v3

    .line 172937
    :goto_2
    int-to-long v3, v1

    shl-long/2addr v3, v6

    .line 172938
    const-wide v7, 0xffffffff0000L

    and-long/2addr v3, v7

    .line 172939
    if-eqz v5, :cond_2

    .line 172940
    const-wide/high16 v5, 0x2000000000000L

    or-long/2addr v3, v5

    .line 172941
    :cond_2
    const/4 v5, 0x4

    const/16 v6, 0x37

    .line 172942
    iget v7, p1, LX/0Pq;->e:I

    move v7, v7

    .line 172943
    invoke-static {v5, v6, v7, v3, v4}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    .line 172944
    goto :goto_1

    .line 172945
    :cond_3
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    move v1, v0

    goto :goto_0

    :cond_4
    move v5, v3

    goto :goto_2
.end method

.method private static d(LX/11h;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 172946
    iget-object v0, p0, LX/11h;->u:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-ne v0, v1, :cond_0

    .line 172947
    iget-object v0, p0, LX/11h;->l:LX/0Xh;

    invoke-virtual {v0}, LX/0Xh;->a()LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/11h;->u:LX/03R;

    .line 172948
    iget-object v0, p0, LX/11h;->u:LX/03R;

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    sput-boolean v0, LX/11p;->a:Z

    .line 172949
    :cond_0
    iget-object v0, p0, LX/11h;->u:LX/03R;

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method

.method public static e(LX/11h;LX/0Pq;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 172869
    iget-object v0, p0, LX/11h;->p:Ljava/util/Set;

    if-nez v0, :cond_1

    .line 172870
    :cond_0
    return-void

    .line 172871
    :cond_1
    if-nez p2, :cond_3

    const/4 v0, 0x0

    move v1, v0

    .line 172872
    :goto_0
    iget-object v0, p0, LX/11h;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oB;

    .line 172873
    const/16 v6, 0x10

    .line 172874
    const/4 v3, 0x4

    const/16 v4, 0x39

    .line 172875
    iget v5, p1, LX/0Pq;->e:I

    move v5, v5

    .line 172876
    int-to-long v7, v1

    shl-long/2addr v7, v6

    invoke-static {v3, v4, v5, v7, v8}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    .line 172877
    sget-object v3, LX/02r;->b:LX/02r;

    move-object v3, v3

    .line 172878
    if-eqz v3, :cond_2

    .line 172879
    invoke-virtual {v3, v6, p1, v1}, LX/02r;->b(ILjava/lang/Object;I)V

    .line 172880
    :cond_2
    goto :goto_1

    .line 172881
    :cond_3
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Pq;)LX/11o;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 172882
    iget-object v0, p0, LX/11h;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    invoke-virtual/range {v0 .. v5}, LX/11h;->a(LX/0Pq;Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Pq;LX/0P1;)LX/11o;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 172883
    const/4 v2, 0x0

    iget-object v0, p0, LX/11h;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, LX/11h;->a(LX/0Pq;Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Pq;LX/0P1;J)LX/11o;
    .locals 7
    .param p2    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 172884
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, LX/11h;->a(LX/0Pq;Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Pq;Ljava/lang/String;LX/0P1;J)LX/11o;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 172885
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-direct/range {v0 .. v6}, LX/11h;->a(LX/0Pq;Ljava/lang/String;LX/0P1;JLjava/lang/Boolean;)LX/11o;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Pq;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 172886
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 172887
    invoke-static {p1, p2}, LX/11h;->c(LX/0Pq;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 172888
    invoke-static {p0}, LX/11h;->c(LX/11h;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11p;

    .line 172889
    invoke-static {p0, p1, p2}, LX/11h;->e(LX/11h;LX/0Pq;Ljava/lang/String;)V

    .line 172890
    if-eqz v0, :cond_0

    .line 172891
    invoke-virtual {v0}, LX/11p;->i()V

    .line 172892
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 172893
    invoke-static {p0}, LX/11h;->c(LX/11h;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 172894
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172895
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11p;

    .line 172896
    if-eqz v0, :cond_0

    .line 172897
    iget-object v2, v0, LX/11p;->c:LX/0Pq;

    move-object v2, v2

    .line 172898
    iget-object v3, v2, LX/0Pq;->c:LX/0Rf;

    move-object v2, v3

    .line 172899
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/0Rf;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 172900
    invoke-virtual {v2, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 172901
    iget-object v2, v0, LX/11p;->c:LX/0Pq;

    move-object v2, v2

    .line 172902
    invoke-virtual {v0}, LX/11p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v2, v0}, LX/11h;->e(LX/11h;LX/0Pq;Ljava/lang/String;)V

    .line 172903
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 172904
    :cond_1
    return-void
.end method

.method public final b(LX/0Pq;Ljava/lang/String;)LX/11o;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "Ljava/lang/String;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 172926
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 172927
    invoke-static {p1, p2}, LX/11h;->c(LX/0Pq;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 172928
    invoke-static {p0}, LX/11h;->c(LX/11h;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11o;

    return-object v0
.end method

.method public final b(LX/0Pq;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 172905
    iget-object v0, p0, LX/11h;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    invoke-virtual/range {v0 .. v5}, LX/11h;->b(LX/0Pq;Ljava/lang/String;LX/0P1;J)V

    .line 172906
    return-void
.end method

.method public final b(LX/0Pq;LX/0P1;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 172907
    const/4 v2, 0x0

    iget-object v0, p0, LX/11h;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, LX/11h;->b(LX/0Pq;Ljava/lang/String;LX/0P1;J)V

    .line 172908
    return-void
.end method

.method public final b(LX/0Pq;LX/0P1;J)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 172909
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, LX/11h;->b(LX/0Pq;Ljava/lang/String;LX/0P1;J)V

    .line 172910
    return-void
.end method

.method public final b(LX/0Pq;Ljava/lang/String;LX/0P1;J)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 172911
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-direct/range {v0 .. v6}, LX/11h;->b(LX/0Pq;Ljava/lang/String;LX/0P1;JLjava/lang/Boolean;)V

    .line 172912
    return-void
.end method

.method public final c(LX/0Pq;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 172913
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 172914
    invoke-static {p1, v3}, LX/11h;->c(LX/0Pq;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 172915
    invoke-static {p0}, LX/11h;->c(LX/11h;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/11p;

    .line 172916
    const/4 v0, 0x3

    invoke-static {p0, p1, v3, v0}, LX/11h;->a(LX/11h;LX/0Pq;Ljava/lang/String;S)V

    .line 172917
    if-nez v2, :cond_0

    .line 172918
    iget-object v0, p1, LX/0Pq;->a:Ljava/lang/String;

    move-object v0, v0

    .line 172919
    const-string v1, "Tried to fail a sequence that wasn\'t in progress"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 172920
    :goto_0
    return-void

    .line 172921
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, v2, LX/11p;->j:Z

    .line 172922
    iget-object v0, p0, LX/11h;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    move-object v6, v3

    invoke-static/range {v0 .. v6}, LX/11h;->a(LX/11h;LX/0Pq;LX/11p;LX/0P1;JLjava/lang/Boolean;)V

    goto :goto_0
.end method

.method public final d(LX/0Pq;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;)V"
        }
    .end annotation

    .prologue
    .line 172923
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/11h;->a(LX/0Pq;Ljava/lang/String;)V

    .line 172924
    return-void
.end method

.method public final e(LX/0Pq;)LX/11o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 172925
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/11h;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    return-object v0
.end method
