.class public LX/1Aj;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 211096
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 211097
    return-void
.end method


# virtual methods
.method public final a(LX/1Gd;Ljava/lang/String;LX/1bh;Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;)Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;",
            "Ljava/lang/String;",
            "LX/1bh;",
            "Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;",
            ")",
            "Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;"
        }
    .end annotation

    .prologue
    .line 211098
    new-instance v0, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/1bq;->a(LX/0QB;)LX/1br;

    move-result-object v2

    check-cast v2, LX/1br;

    invoke-static {p0}, LX/1bs;->a(LX/0QB;)LX/1c3;

    move-result-object v3

    check-cast v3, LX/1c3;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {p0}, LX/1c5;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v5

    invoke-static {p0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/1c6;->a(LX/0QB;)LX/1Fh;

    move-result-object v7

    check-cast v7, LX/1Fh;

    move-object v8, p1

    move-object v9, p2

    move-object v10, p3

    move-object/from16 v11, p4

    invoke-direct/range {v0 .. v11}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;-><init>(Landroid/content/res/Resources;LX/1br;LX/1c3;LX/0Zb;Ljava/util/Set;Ljava/util/concurrent/Executor;LX/1Fh;LX/1Gd;Ljava/lang/String;LX/1bh;Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;)V

    .line 211099
    return-object v0
.end method
