.class public LX/1rX;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Landroid/os/Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 332462
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Landroid/os/Handler;
    .locals 3

    .prologue
    .line 332463
    sget-object v0, LX/1rX;->a:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 332464
    const-class v1, LX/1rX;

    monitor-enter v1

    .line 332465
    :try_start_0
    sget-object v0, LX/1rX;->a:Landroid/os/Handler;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 332466
    if-eqz v2, :cond_0

    .line 332467
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 332468
    invoke-static {v0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object p0

    check-cast p0, LX/0Zr;

    invoke-static {p0}, LX/1rY;->a(LX/0Zr;)Landroid/os/Handler;

    move-result-object p0

    move-object v0, p0

    .line 332469
    sput-object v0, LX/1rX;->a:Landroid/os/Handler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 332470
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 332471
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 332472
    :cond_1
    sget-object v0, LX/1rX;->a:Landroid/os/Handler;

    return-object v0

    .line 332473
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 332474
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 332475
    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v0

    check-cast v0, LX/0Zr;

    invoke-static {v0}, LX/1rY;->a(LX/0Zr;)Landroid/os/Handler;

    move-result-object v0

    return-object v0
.end method
