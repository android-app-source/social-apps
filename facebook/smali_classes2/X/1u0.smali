.class public final enum LX/1u0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1u0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1u0;

.field public static final enum CONNECTED:LX/1u0;

.field public static final enum CONNECTING:LX/1u0;

.field public static final enum CONNECT_SENT:LX/1u0;

.field public static final enum DISCONNECTED:LX/1u0;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 336948
    new-instance v0, LX/1u0;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v2}, LX/1u0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1u0;->CONNECTING:LX/1u0;

    .line 336949
    new-instance v0, LX/1u0;

    const-string v1, "CONNECT_SENT"

    invoke-direct {v0, v1, v3}, LX/1u0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1u0;->CONNECT_SENT:LX/1u0;

    .line 336950
    new-instance v0, LX/1u0;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v4}, LX/1u0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1u0;->CONNECTED:LX/1u0;

    .line 336951
    new-instance v0, LX/1u0;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v5}, LX/1u0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1u0;->DISCONNECTED:LX/1u0;

    .line 336952
    const/4 v0, 0x4

    new-array v0, v0, [LX/1u0;

    sget-object v1, LX/1u0;->CONNECTING:LX/1u0;

    aput-object v1, v0, v2

    sget-object v1, LX/1u0;->CONNECT_SENT:LX/1u0;

    aput-object v1, v0, v3

    sget-object v1, LX/1u0;->CONNECTED:LX/1u0;

    aput-object v1, v0, v4

    sget-object v1, LX/1u0;->DISCONNECTED:LX/1u0;

    aput-object v1, v0, v5

    sput-object v0, LX/1u0;->$VALUES:[LX/1u0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 336955
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromOrdinal(I)LX/1u0;
    .locals 1

    .prologue
    .line 336956
    invoke-static {}, LX/1u0;->values()[LX/1u0;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/1u0;
    .locals 1

    .prologue
    .line 336954
    const-class v0, LX/1u0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1u0;

    return-object v0
.end method

.method public static values()[LX/1u0;
    .locals 1

    .prologue
    .line 336953
    sget-object v0, LX/1u0;->$VALUES:[LX/1u0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1u0;

    return-object v0
.end method
