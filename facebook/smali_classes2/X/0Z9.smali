.class public LX/0Z9;
.super LX/0ZA;
.source ""


# instance fields
.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:Ljava/lang/Runnable;

.field private final e:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 82971
    invoke-direct {p0, p1}, LX/0ZA;-><init>(Landroid/content/Context;)V

    .line 82972
    new-instance v0, Lcom/facebook/dialtone/DialtoneAsyncSignalFile$1;

    invoke-direct {v0, p0}, Lcom/facebook/dialtone/DialtoneAsyncSignalFile$1;-><init>(LX/0Z9;)V

    iput-object v0, p0, LX/0Z9;->d:Ljava/lang/Runnable;

    .line 82973
    new-instance v0, Lcom/facebook/dialtone/DialtoneAsyncSignalFile$2;

    invoke-direct {v0, p0}, Lcom/facebook/dialtone/DialtoneAsyncSignalFile$2;-><init>(LX/0Z9;)V

    iput-object v0, p0, LX/0Z9;->e:Ljava/lang/Runnable;

    .line 82974
    iput-object p2, p0, LX/0Z9;->c:Ljava/util/concurrent/ExecutorService;

    .line 82975
    return-void
.end method

.method public static a(LX/0QB;)LX/0Z9;
    .locals 1

    .prologue
    .line 82970
    invoke-static {p0}, LX/0Z9;->b(LX/0QB;)LX/0Z9;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(LX/0Z9;)Z
    .locals 1

    .prologue
    .line 82969
    invoke-super {p0}, LX/0ZA;->b()Z

    move-result v0

    return v0
.end method

.method public static b(LX/0QB;)LX/0Z9;
    .locals 3

    .prologue
    .line 82960
    new-instance v2, LX/0Z9;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v2, v0, v1}, LX/0Z9;-><init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;)V

    .line 82961
    return-object v2
.end method

.method public static synthetic b(LX/0Z9;)Z
    .locals 1

    .prologue
    .line 82968
    invoke-super {p0}, LX/0ZA;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 82965
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/0Z9;->b:Ljava/lang/Boolean;

    .line 82966
    iget-object v0, p0, LX/0Z9;->c:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, LX/0Z9;->e:Ljava/lang/Runnable;

    const v2, -0x31886473

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 82967
    return v3
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 82962
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/0Z9;->b:Ljava/lang/Boolean;

    .line 82963
    iget-object v0, p0, LX/0Z9;->c:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, LX/0Z9;->d:Ljava/lang/Runnable;

    const v2, 0x1d0680df

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 82964
    const/4 v0, 0x1

    return v0
.end method
