.class public LX/0bH;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/1CO;",
        "LX/0bJ;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0bH;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 86787
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 86788
    return-void
.end method

.method public static a(LX/0QB;)LX/0bH;
    .locals 3

    .prologue
    .line 86789
    sget-object v0, LX/0bH;->a:LX/0bH;

    if-nez v0, :cond_1

    .line 86790
    const-class v1, LX/0bH;

    monitor-enter v1

    .line 86791
    :try_start_0
    sget-object v0, LX/0bH;->a:LX/0bH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 86792
    if-eqz v2, :cond_0

    .line 86793
    :try_start_1
    new-instance v0, LX/0bH;

    invoke-direct {v0}, LX/0bH;-><init>()V

    .line 86794
    move-object v0, v0

    .line 86795
    sput-object v0, LX/0bH;->a:LX/0bH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 86796
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 86797
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 86798
    :cond_1
    sget-object v0, LX/0bH;->a:LX/0bH;

    return-object v0

    .line 86799
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 86800
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
