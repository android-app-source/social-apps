.class public abstract LX/0Xk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Xl;


# instance fields
.field public final a:LX/0Sk;


# direct methods
.method public constructor <init>(LX/0Sk;)V
    .locals 0
    .param p1    # LX/0Sk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 79381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79382
    iput-object p1, p0, LX/0Xk;->a:LX/0Sk;

    .line 79383
    return-void
.end method


# virtual methods
.method public final a()LX/0YX;
    .locals 1

    .prologue
    .line 79384
    new-instance v0, LX/0YW;

    invoke-direct {v0, p0}, LX/0YW;-><init>(LX/0Xk;)V

    return-object v0
.end method

.method public abstract a(Landroid/content/BroadcastReceiver;)V
.end method

.method public abstract a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Landroid/os/Handler;)V
    .param p3    # Landroid/os/Handler;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 79385
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79386
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/0Xk;->a(Landroid/content/Intent;)V

    .line 79387
    return-void
.end method
