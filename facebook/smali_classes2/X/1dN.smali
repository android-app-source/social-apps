.class public LX/1dN;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/1dK;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/1dK;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public A:I

.field private B:Landroid/view/accessibility/AccessibilityManager;

.field public C:Z

.field public D:LX/1mg;

.field public c:LX/1De;

.field public d:LX/1my;

.field public e:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field private f:I

.field private g:I

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1dK;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1dW;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0tf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0tf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/1mk;

.field public final l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1dK;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1dK;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1dP;",
            ">;"
        }
    .end annotation
.end field

.field private o:LX/1Dg;

.field public p:LX/1ml;

.field public final q:Ljava/util/concurrent/atomic/AtomicInteger;

.field public r:I

.field public s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:J

.field private x:I

.field private y:Z

.field private z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 285443
    new-instance v0, LX/1mi;

    invoke-direct {v0}, LX/1mi;-><init>()V

    sput-object v0, LX/1dN;->a:Ljava/util/Comparator;

    .line 285444
    new-instance v0, LX/1mj;

    invoke-direct {v0}, LX/1mj;-><init>()V

    sput-object v0, LX/1dN;->b:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/16 v2, 0x8

    .line 285445
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 285446
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/1dN;->h:Ljava/util/List;

    .line 285447
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/1dN;->i:Ljava/util/List;

    .line 285448
    new-instance v0, LX/0tf;

    invoke-direct {v0, v2}, LX/0tf;-><init>(I)V

    iput-object v0, p0, LX/1dN;->j:LX/0tf;

    .line 285449
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1dN;->l:Ljava/util/ArrayList;

    .line 285450
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1dN;->m:Ljava/util/ArrayList;

    .line 285451
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/1dN;->q:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 285452
    iput v4, p0, LX/1dN;->v:I

    .line 285453
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/1dN;->w:J

    .line 285454
    iput v3, p0, LX/1dN;->x:I

    .line 285455
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1dN;->y:Z

    .line 285456
    iput-boolean v4, p0, LX/1dN;->z:Z

    .line 285457
    iput v3, p0, LX/1dN;->A:I

    .line 285458
    iput-boolean v4, p0, LX/1dN;->C:Z

    .line 285459
    new-instance v0, LX/1mk;

    invoke-direct {v0}, LX/1mk;-><init>()V

    iput-object v0, p0, LX/1dN;->k:LX/1mk;

    .line 285460
    sget-boolean v0, LX/1V5;->d:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    iput-object v0, p0, LX/1dN;->n:Ljava/util/List;

    .line 285461
    return-void

    .line 285462
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/1De;LX/1X1;LX/1Dg;IILX/1ml;)LX/1Dg;
    .locals 8

    .prologue
    .line 285463
    iget v0, p0, LX/1De;->i:I

    move v1, v0

    .line 285464
    iget v0, p0, LX/1De;->j:I

    move v2, v0

    .line 285465
    iput p3, p0, LX/1De;->i:I

    .line 285466
    iput p4, p0, LX/1De;->j:I

    .line 285467
    const/4 v5, 0x0

    .line 285468
    iget-object v0, p0, LX/1De;->d:LX/1cp;

    move-object v3, v0

    .line 285469
    if-eqz v3, :cond_0

    .line 285470
    const-string v0, "log_tag"

    .line 285471
    iget-object v4, p0, LX/1De;->c:Ljava/lang/String;

    move-object v4, v4

    .line 285472
    invoke-virtual {v3, v5, p0, v0, v4}, LX/1cp;->a(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 285473
    invoke-virtual {p1}, LX/1X1;->a()Ljava/lang/String;

    move-result-object v0

    .line 285474
    iget-object v4, v3, LX/1cp;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v5}, LX/1cp;->a(I)I

    move-result v6

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v7

    invoke-interface {v4, v6, v7, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 285475
    :cond_0
    iget-object v0, p1, LX/1X1;->e:LX/1S3;

    move-object v0, v0

    .line 285476
    const/4 v4, 0x1

    invoke-virtual {v0, p0, p1, v4}, LX/1S3;->a(LX/1De;LX/1X1;Z)LX/1Dg;

    move-result-object v0

    check-cast v0, LX/1Dg;

    .line 285477
    if-eqz v3, :cond_1

    .line 285478
    const/16 v4, 0x10

    invoke-virtual {v3, v5, p0, v4}, LX/1cp;->a(ILjava/lang/Object;I)V

    .line 285479
    :cond_1
    move-object v0, v0

    .line 285480
    iput v1, p0, LX/1De;->i:I

    .line 285481
    iput v2, p0, LX/1De;->j:I

    .line 285482
    sget-object v1, LX/1De;->a:LX/1Dg;

    if-ne v0, v1, :cond_2

    .line 285483
    :goto_0
    return-object v0

    .line 285484
    :cond_2
    if-eqz p2, :cond_9

    invoke-static {p1}, LX/1X1;->e(LX/1X1;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 285485
    invoke-virtual {p2, v0}, LX/1Dg;->b(LX/1Dg;)V

    .line 285486
    iget-object v1, p2, LX/1Dg;->ai:LX/1ml;

    move-object p5, v1

    .line 285487
    :cond_3
    :goto_1
    const/4 v2, 0x1

    .line 285488
    iget-object v1, v0, LX/1Dg;->j:LX/1De;

    move-object v1, v1

    .line 285489
    iget-object v3, v0, LX/1Dg;->l:LX/1X1;

    move-object v3, v3

    .line 285490
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "measureTree:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, LX/1X1;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/1mm;->a(Ljava/lang/String;)V

    .line 285491
    iget-object v4, v0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v4}, LX/1mn;->i()F

    move-result v4

    move v4, v4

    .line 285492
    invoke-static {v4}, LX/1mo;->a(F)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 285493
    invoke-virtual {v0, p3}, LX/1Dg;->aL(I)V

    .line 285494
    :cond_4
    iget-object v4, v0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v4}, LX/1mn;->j()F

    move-result v4

    move v4, v4

    .line 285495
    invoke-static {v4}, LX/1mo;->a(F)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 285496
    invoke-virtual {v0, p4}, LX/1Dg;->aM(I)V

    .line 285497
    :cond_5
    if-eqz p5, :cond_6

    .line 285498
    const-string v4, "applyDiffNode"

    invoke-static {v4}, LX/1mm;->a(Ljava/lang/String;)V

    .line 285499
    invoke-static {v0, p5}, LX/1dN;->b(LX/1Dg;LX/1ml;)Z

    .line 285500
    invoke-static {}, LX/1mm;->a()V

    .line 285501
    :cond_6
    iget-object v4, v1, LX/1De;->d:LX/1cp;

    move-object v4, v4

    .line 285502
    if-eqz v4, :cond_7

    .line 285503
    const-string v5, "log_tag"

    .line 285504
    iget-object p3, v1, LX/1De;->c:Ljava/lang/String;

    move-object v1, p3

    .line 285505
    invoke-virtual {v4, v2, v3, v5, v1}, LX/1cp;->a(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 285506
    const-string v5, "tree_diff_enabled"

    if-eqz p5, :cond_a

    move v1, v2

    :goto_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v2, v3, v5, v1}, LX/1cp;->b(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 285507
    :cond_7
    iget-object v1, v0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v1}, LX/1mn;->c()V

    .line 285508
    if-eqz v4, :cond_8

    .line 285509
    const/16 v1, 0x10

    invoke-virtual {v4, v2, v3, v1}, LX/1cp;->a(ILjava/lang/Object;I)V

    .line 285510
    :cond_8
    invoke-static {}, LX/1mm;->a()V

    .line 285511
    goto/16 :goto_0

    .line 285512
    :cond_9
    iget-object v1, v0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v1}, LX/1mn;->h()Lcom/facebook/csslayout/YogaDirection;

    move-result-object v1

    move-object v1, v1

    .line 285513
    sget-object v2, Lcom/facebook/csslayout/YogaDirection;->INHERIT:Lcom/facebook/csslayout/YogaDirection;

    if-ne v1, v2, :cond_3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 285514
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 285515
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-lt v4, v5, :cond_c

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v4, 0x400000

    and-int/2addr v3, v4

    if-eqz v3, :cond_c

    .line 285516
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v3

    move v3, v3

    .line 285517
    if-ne v3, v1, :cond_b

    .line 285518
    :goto_3
    move v1, v1

    .line 285519
    if-eqz v1, :cond_3

    .line 285520
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/1Dg;->ab(I)LX/1Dg;

    goto/16 :goto_1

    .line 285521
    :cond_a
    const/4 v1, 0x0

    goto :goto_2

    :cond_b
    move v1, v2

    .line 285522
    goto :goto_3

    :cond_c
    move v1, v2

    .line 285523
    goto :goto_3
.end method

.method public static a(LX/1Dg;II)LX/1Dg;
    .locals 10

    .prologue
    .line 285524
    iget-object v0, p0, LX/1Dg;->j:LX/1De;

    move-object v8, v0

    .line 285525
    iget-object v0, p0, LX/1Dg;->l:LX/1X1;

    move-object v9, v0

    .line 285526
    iget-object v0, p0, LX/1Dg;->p:LX/1Dg;

    move-object v6, v0

    .line 285527
    if-eqz v6, :cond_0

    invoke-virtual {v6}, LX/1Dg;->n()I

    move-result v0

    invoke-virtual {v6}, LX/1Dg;->o()I

    move-result v1

    .line 285528
    iget v2, v6, LX/1Dg;->ag:F

    move v4, v2

    .line 285529
    iget v2, v6, LX/1Dg;->ah:F

    move v5, v2

    .line 285530
    move v2, p1

    move v3, p2

    invoke-static/range {v0 .. v5}, LX/1dN;->a(IIIIFF)Z

    move-result v0

    if-nez v0, :cond_4

    .line 285531
    :cond_0
    if-eqz v6, :cond_2

    .line 285532
    sget-object v0, LX/1De;->a:LX/1Dg;

    if-eq v6, v0, :cond_1

    .line 285533
    const/4 v0, 0x1

    invoke-static {v6, v0}, LX/1dN;->a(LX/1Dg;Z)V

    .line 285534
    :cond_1
    const/4 v0, 0x0

    move-object v6, v0

    .line 285535
    :cond_2
    invoke-virtual {v9}, LX/1X1;->i()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 285536
    iget-object v0, v9, LX/1X1;->h:LX/1Dg;

    move-object v7, v0

    .line 285537
    invoke-static {p0, v7}, LX/1Dg;->a(LX/1Dg;LX/1Dg;)Z

    move-result v0

    .line 285538
    if-eqz v0, :cond_5

    invoke-virtual {v7}, LX/1Dg;->n()I

    move-result v0

    invoke-virtual {v7}, LX/1Dg;->o()I

    move-result v1

    .line 285539
    iget v2, v7, LX/1Dg;->ag:F

    move v4, v2

    .line 285540
    iget v2, v7, LX/1Dg;->ah:F

    move v5, v2

    .line 285541
    move v2, p1

    move v3, p2

    invoke-static/range {v0 .. v5}, LX/1dN;->a(IIIIFF)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 285542
    const/4 v0, 0x0

    iput-object v0, v9, LX/1X1;->h:LX/1Dg;

    .line 285543
    move-object v0, v7

    .line 285544
    :goto_0
    if-nez v0, :cond_3

    .line 285545
    iget-object v0, p0, LX/1Dg;->ai:LX/1ml;

    move-object v5, v0

    .line 285546
    move-object v0, v8

    move-object v1, v9

    move-object v2, p0

    move v3, p1

    move v4, p2

    invoke-static/range {v0 .. v5}, LX/1dN;->a(LX/1De;LX/1X1;LX/1Dg;IILX/1ml;)LX/1Dg;

    move-result-object v0

    .line 285547
    invoke-virtual {v0, p1}, LX/1Dg;->Z(I)V

    .line 285548
    invoke-virtual {v0, p2}, LX/1Dg;->aa(I)V

    .line 285549
    invoke-virtual {v0}, LX/1Dg;->d()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, LX/1Dg;->h(F)V

    .line 285550
    invoke-virtual {v0}, LX/1Dg;->c()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, LX/1Dg;->g(F)V

    .line 285551
    :cond_3
    invoke-virtual {p0, v0}, LX/1Dg;->a(LX/1Dg;)V

    move-object v6, v0

    .line 285552
    :cond_4
    invoke-static {v6}, LX/1Dg;->c(LX/1Dg;)V

    .line 285553
    return-object v6

    .line 285554
    :cond_5
    invoke-virtual {v9}, LX/1X1;->k()V

    :cond_6
    move-object v0, v6

    goto :goto_0
.end method

.method private static a(LX/1Dg;LX/1dN;)LX/1dK;
    .locals 18

    .prologue
    .line 285555
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->ai()LX/1X1;

    move-result-object v1

    .line 285556
    if-nez v1, :cond_0

    .line 285557
    const/4 v1, 0x0

    .line 285558
    :goto_0
    return-object v1

    .line 285559
    :cond_0
    invoke-virtual {v1}, LX/1X1;->n()LX/1S3;

    move-result-object v2

    invoke-virtual {v2}, LX/1S3;->f()LX/1mv;

    move-result-object v2

    sget-object v3, LX/1mv;->NONE:LX/1mv;

    if-ne v2, v3, :cond_1

    .line 285560
    const/4 v1, 0x0

    goto :goto_0

    .line 285561
    :cond_1
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->E()LX/1dQ;

    move-result-object v2

    if-eqz v2, :cond_3

    const/4 v14, 0x1

    .line 285562
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->F()LX/1dQ;

    move-result-object v2

    if-eqz v2, :cond_4

    const/4 v15, 0x1

    .line 285563
    :goto_2
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->H()LX/1dQ;

    move-result-object v2

    if-eqz v2, :cond_5

    const/16 v16, 0x1

    .line 285564
    :goto_3
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->i()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->a()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->b()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->c()I

    move-result v6

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->d()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->h()I

    move-result v8

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->e()I

    move-result v9

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->f()I

    move-result v10

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->g()I

    move-result v11

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->al()I

    move-result v12

    move-object/from16 v0, p1

    iget-boolean v13, v0, LX/1dN;->y:Z

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->G()I

    move-result v17

    move-object/from16 v2, p1

    invoke-static/range {v1 .. v17}, LX/1dN;->a(LX/1X1;LX/1dN;IIIIIIIIIIZZZZI)LX/1dK;

    move-result-object v2

    .line 285565
    invoke-static {v1}, LX/1X1;->d(LX/1X1;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 285566
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->W()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1dK;->a(Ljava/lang/Object;)V

    .line 285567
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->X()Landroid/util/SparseArray;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1dK;->a(Landroid/util/SparseArray;)V

    .line 285568
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->E()LX/1dQ;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1dK;->a(LX/1dQ;)V

    .line 285569
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->F()LX/1dQ;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1dK;->b(LX/1dQ;)V

    .line 285570
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->H()LX/1dQ;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1dK;->c(LX/1dQ;)V

    .line 285571
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->N()LX/1dQ;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1dK;->d(LX/1dQ;)V

    .line 285572
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->O()LX/1dQ;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1dK;->e(LX/1dQ;)V

    .line 285573
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->Q()LX/1dQ;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1dK;->f(LX/1dQ;)V

    .line 285574
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->P()LX/1dQ;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1dK;->g(LX/1dQ;)V

    .line 285575
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->R()LX/1dQ;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1dK;->h(LX/1dQ;)V

    .line 285576
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->S()LX/1dQ;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1dK;->i(LX/1dQ;)V

    .line 285577
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->T()LX/1dQ;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1dK;->j(LX/1dQ;)V

    .line 285578
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->U()LX/1dQ;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1dK;->k(LX/1dQ;)V

    .line 285579
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->M()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1dK;->a(Ljava/lang/CharSequence;)V

    .line 285580
    move-object/from16 v0, p0

    invoke-static {v2, v0}, LX/1dN;->a(LX/1dK;LX/1Dg;)V

    :cond_2
    move-object v1, v2

    .line 285581
    goto/16 :goto_0

    .line 285582
    :cond_3
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 285583
    :cond_4
    const/4 v15, 0x0

    goto/16 :goto_2

    .line 285584
    :cond_5
    const/16 v16, 0x0

    goto/16 :goto_3
.end method

.method private static a(LX/1Dg;LX/1dN;LX/1dK;LX/1dc;IZ)LX/1dK;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Dg;",
            "LX/1dN;",
            "LX/1dK;",
            "LX/1dc",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;IZ)",
            "LX/1dK;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 285585
    new-instance v0, LX/1mw;

    invoke-direct {v0, p3}, LX/1mw;-><init>(LX/1dc;)V

    move-object v1, v0

    .line 285586
    iget-object v0, p0, LX/1Dg;->j:LX/1De;

    move-object v0, v0

    .line 285587
    invoke-static {v0, v1}, LX/1De;->a(LX/1De;LX/1X1;)LX/1De;

    move-result-object v0

    .line 285588
    iput-object v0, v1, LX/1X1;->f:LX/1De;

    .line 285589
    if-eqz p2, :cond_1

    .line 285590
    iget-object v0, v1, LX/1X1;->e:LX/1S3;

    move-object v0, v0

    .line 285591
    check-cast v0, LX/1dT;

    .line 285592
    iget-object v3, p2, LX/1dK;->b:LX/1X1;

    move-object v3, v3

    .line 285593
    invoke-virtual {v0, v3, v1}, LX/1S3;->b(LX/1X1;LX/1X1;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v8, v0

    .line 285594
    :goto_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, LX/1dK;->e()J

    move-result-wide v6

    :goto_2
    move-object v2, p1

    move-object v3, p0

    move v4, p5

    move v5, p4

    .line 285595
    invoke-static/range {v1 .. v8}, LX/1dN;->a(LX/1X1;LX/1dN;LX/1Dg;ZIJZ)LX/1dK;

    move-result-object v0

    .line 285596
    return-object v0

    :cond_0
    move v0, v2

    .line 285597
    goto :goto_0

    :cond_1
    move v8, v2

    .line 285598
    goto :goto_1

    .line 285599
    :cond_2
    const-wide/16 v6, -0x1

    goto :goto_2
.end method

.method private static a(LX/1X1;LX/1dN;IIIIIIIIIIZZZZI)LX/1dK;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;",
            "LX/1dN;",
            "IIIIIIIIIIZZZZI)",
            "LX/1dK;"
        }
    .end annotation

    .prologue
    .line 285600
    invoke-static {p0}, LX/1X1;->d(LX/1X1;)Z

    move-result v10

    .line 285601
    if-eqz v10, :cond_5

    invoke-static {}, LX/1cy;->b()LX/1dL;

    move-result-object v5

    .line 285602
    :goto_0
    invoke-virtual {v5, p0}, LX/1dK;->a(LX/1X1;)V

    .line 285603
    move/from16 v0, p11

    invoke-virtual {v5, v0}, LX/1dK;->f(I)V

    .line 285604
    iget-wide v6, p1, LX/1dN;->w:J

    invoke-virtual {v5, v6, v7}, LX/1dK;->a(J)V

    .line 285605
    iget v4, p1, LX/1dN;->x:I

    if-ltz v4, :cond_0

    .line 285606
    iget-object v4, p1, LX/1dN;->h:Ljava/util/List;

    iget v6, p1, LX/1dN;->x:I

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1dK;

    .line 285607
    invoke-virtual {v4}, LX/1dK;->b()Landroid/graphics/Rect;

    move-result-object v4

    .line 285608
    iget v6, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {v5, v6}, LX/1dK;->a(I)V

    .line 285609
    iget v4, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v5, v4}, LX/1dK;->b(I)V

    .line 285610
    :cond_0
    iget v4, p1, LX/1dN;->t:I

    add-int v9, v4, p3

    .line 285611
    iget v4, p1, LX/1dN;->u:I

    add-int v8, v4, p4

    .line 285612
    add-int v7, v9, p5

    .line 285613
    add-int v6, v8, p6

    .line 285614
    if-eqz v10, :cond_6

    move-object v4, v5

    .line 285615
    check-cast v4, LX/1dL;

    .line 285616
    move/from16 v0, p7

    move/from16 v1, p8

    move/from16 v2, p9

    move/from16 v3, p10

    invoke-virtual {v4, v0, v1, v2, v3}, LX/1dL;->c(IIII)V

    .line 285617
    invoke-virtual {v4, p2}, LX/1dL;->g(I)V

    move v4, v6

    move v6, v7

    move v7, v8

    move v8, v9

    .line 285618
    :goto_1
    invoke-virtual {v5, v8, v7, v6, v4}, LX/1dK;->a(IIII)V

    .line 285619
    const/4 v4, 0x0

    .line 285620
    if-eqz p12, :cond_1

    .line 285621
    const/4 v4, 0x2

    .line 285622
    :cond_1
    if-eqz p13, :cond_2

    .line 285623
    or-int/lit8 v4, v4, 0x4

    .line 285624
    :cond_2
    if-eqz p14, :cond_3

    .line 285625
    or-int/lit8 v4, v4, 0x8

    .line 285626
    :cond_3
    if-eqz p15, :cond_4

    .line 285627
    or-int/lit8 v4, v4, 0x10

    .line 285628
    :cond_4
    or-int v4, v4, p16

    .line 285629
    invoke-virtual {v5, v4}, LX/1dK;->c(I)V

    .line 285630
    return-object v5

    .line 285631
    :cond_5
    invoke-static {}, LX/1cy;->a()LX/1dK;

    move-result-object v5

    goto :goto_0

    .line 285632
    :cond_6
    add-int v9, v9, p7

    .line 285633
    add-int v8, v8, p8

    .line 285634
    sub-int v7, v7, p9

    .line 285635
    sub-int v4, v6, p10

    move v6, v7

    move v7, v8

    move v8, v9

    goto :goto_1
.end method

.method private static a(LX/1X1;LX/1dN;IIIIIIZZZZI)LX/1dK;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;",
            "LX/1dN;",
            "IIIIIIZZZZI)",
            "LX/1dK;"
        }
    .end annotation

    .prologue
    .line 285441
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v11, p7

    move/from16 v12, p8

    move/from16 v13, p9

    move/from16 v14, p10

    move/from16 v15, p11

    move/from16 v16, p12

    invoke-static/range {v0 .. v16}, LX/1dN;->a(LX/1X1;LX/1dN;IIIIIIIIIIZZZZI)LX/1dK;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/1X1;LX/1dN;LX/1Dg;ZIJZ)LX/1dK;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/1dT;",
            ">;",
            "LX/1dN;",
            "Lcom/facebook/components/ComponentLayout;",
            "ZIJZ)",
            "LX/1dK;"
        }
    .end annotation

    .prologue
    .line 285636
    invoke-virtual {p0}, LX/1X1;->n()LX/1S3;

    move-result-object v0

    check-cast v0, LX/1dT;

    iget-object v1, p1, LX/1dN;->c:LX/1De;

    invoke-virtual {v0, v1, p2, p0}, LX/1dT;->a(LX/1De;LX/1Dg;LX/1X1;)V

    .line 285637
    invoke-virtual {p2}, LX/1Dg;->i()I

    move-result v2

    invoke-virtual {p2}, LX/1Dg;->a()I

    move-result v3

    invoke-virtual {p2}, LX/1Dg;->b()I

    move-result v4

    invoke-virtual {p2}, LX/1Dg;->c()I

    move-result v5

    invoke-virtual {p2}, LX/1Dg;->d()I

    move-result v6

    const/4 v7, 0x2

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x80

    move-object v0, p0

    move-object v1, p1

    move/from16 v8, p3

    invoke-static/range {v0 .. v12}, LX/1dN;->a(LX/1X1;LX/1dN;IIIIIIZZZZI)LX/1dK;

    move-result-object v1

    .line 285638
    iget-object v0, p1, LX/1dN;->k:LX/1mk;

    iget v2, p1, LX/1dN;->v:I

    move/from16 v3, p4

    move-wide/from16 v4, p5

    move/from16 v6, p7

    invoke-virtual/range {v0 .. v6}, LX/1mk;->a(LX/1dK;IIJZ)V

    .line 285639
    invoke-static {p1, v1}, LX/1dN;->a(LX/1dN;LX/1dK;)V

    .line 285640
    iget-object v0, p1, LX/1dN;->j:LX/0tf;

    iget-object v2, p1, LX/1dN;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v1, v2}, LX/1dN;->a(LX/0tf;LX/1dK;I)V

    .line 285641
    return-object v1
.end method

.method public static a(LX/1De;LX/1X1;IIIZLX/1ml;)LX/1dN;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/1S3;",
            ">(",
            "LX/1De;",
            "LX/1X1",
            "<TT;>;IIIZ",
            "LX/1ml;",
            ")",
            "LX/1dN;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x0

    .line 285642
    invoke-virtual {p1}, LX/1X1;->c()V

    .line 285643
    sget-object v0, LX/1cy;->d:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dN;

    .line 285644
    if-nez v0, :cond_0

    .line 285645
    new-instance v0, LX/1dN;

    invoke-direct {v0}, LX/1dN;-><init>()V

    .line 285646
    :cond_0
    iput-object p0, v0, LX/1dN;->c:LX/1De;

    .line 285647
    iget-object v1, v0, LX/1dN;->c:LX/1De;

    .line 285648
    iget-object v3, v1, LX/1De;->e:LX/1mg;

    move-object v1, v3

    .line 285649
    iput-object v1, v0, LX/1dN;->D:LX/1mg;

    .line 285650
    iget-object v1, v0, LX/1dN;->q:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 285651
    move-object v6, v0

    .line 285652
    iput-boolean p5, v6, LX/1dN;->z:Z

    .line 285653
    iput p2, v6, LX/1dN;->A:I

    .line 285654
    const-string v0, "accessibility"

    invoke-virtual {p0, v0}, LX/1De;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, v6, LX/1dN;->B:Landroid/view/accessibility/AccessibilityManager;

    .line 285655
    iget-object v0, v6, LX/1dN;->B:Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v0}, LX/1dN;->a(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v0

    iput-boolean v0, v6, LX/1dN;->C:Z

    .line 285656
    iput-object p1, v6, LX/1dN;->e:LX/1X1;

    .line 285657
    iput p3, v6, LX/1dN;->f:I

    .line 285658
    iput p4, v6, LX/1dN;->g:I

    .line 285659
    invoke-virtual {p1, p0}, LX/1X1;->b(LX/1De;)V

    .line 285660
    iget-object v0, p1, LX/1X1;->f:LX/1De;

    move-object v0, v0

    .line 285661
    move-object v1, p1

    move v3, p3

    move v4, p4

    move-object v5, p6

    invoke-static/range {v0 .. v5}, LX/1dN;->a(LX/1De;LX/1X1;LX/1Dg;IILX/1ml;)LX/1Dg;

    move-result-object v0

    .line 285662
    invoke-virtual {v0}, LX/1Dg;->c()I

    move-result v1

    iput v1, v6, LX/1dN;->r:I

    .line 285663
    invoke-virtual {v0}, LX/1Dg;->d()I

    move-result v1

    iput v1, v6, LX/1dN;->s:I

    .line 285664
    iget-object v1, v6, LX/1dN;->k:LX/1mk;

    invoke-virtual {v1}, LX/1mk;->a()V

    .line 285665
    const-wide/16 v4, -0x1

    iput-wide v4, v6, LX/1dN;->w:J

    .line 285666
    iget-object v1, p0, LX/1De;->d:LX/1cp;

    move-object v1, v1

    .line 285667
    sget-object v3, LX/1De;->a:LX/1Dg;

    if-ne v0, v3, :cond_1

    move-object v0, v6

    .line 285668
    :goto_0
    return-object v0

    .line 285669
    :cond_1
    iput-object v0, v6, LX/1dN;->o:LX/1Dg;

    .line 285670
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "collectResults:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/1X1;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/1mm;->a(Ljava/lang/String;)V

    .line 285671
    if-eqz v1, :cond_2

    .line 285672
    const-string v3, "log_tag"

    .line 285673
    iget-object v4, p0, LX/1De;->c:Ljava/lang/String;

    move-object v4, v4

    .line 285674
    invoke-virtual {v1, v7, p1, v3, v4}, LX/1cp;->a(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 285675
    :cond_2
    invoke-static {v0, v6, v2}, LX/1dN;->a(LX/1Dg;LX/1dN;LX/1ml;)V

    .line 285676
    if-eqz v1, :cond_3

    .line 285677
    const/16 v3, 0x10

    invoke-virtual {v1, v7, p1, v3}, LX/1cp;->a(ILjava/lang/Object;I)V

    .line 285678
    :cond_3
    invoke-static {}, LX/1mm;->a()V

    .line 285679
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/1dN;->a(LX/1Dg;Z)V

    .line 285680
    iput-object v2, v6, LX/1dN;->o:LX/1Dg;

    .line 285681
    invoke-static {}, LX/1dS;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-boolean v0, LX/1V5;->e:Z

    if-eqz v0, :cond_4

    .line 285682
    const/4 v2, 0x0

    .line 285683
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 285684
    iget-object v4, v6, LX/1dN;->c:LX/1De;

    .line 285685
    invoke-static {v4}, LX/1dN;->c(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object v0

    .line 285686
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_4

    .line 285687
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-lt v1, v5, :cond_9

    .line 285688
    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v1

    .line 285689
    :goto_1
    move v0, v1

    .line 285690
    if-eqz v0, :cond_5

    .line 285691
    :cond_4
    move-object v0, v6

    .line 285692
    goto :goto_0

    .line 285693
    :cond_5
    invoke-virtual {v6}, LX/1dN;->c()I

    move-result v5

    move v1, v2

    :goto_2
    if-ge v1, v5, :cond_4

    .line 285694
    invoke-virtual {v6, v1}, LX/1dN;->b(I)LX/1dK;

    move-result-object v7

    .line 285695
    iget-object v0, v7, LX/1dK;->b:LX/1X1;

    move-object p0, v0

    .line 285696
    iget-object v0, p0, LX/1X1;->e:LX/1S3;

    move-object p1, v0

    .line 285697
    invoke-virtual {p1}, LX/1S3;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 285698
    invoke-virtual {v7, v3}, LX/1dK;->a(Landroid/graphics/Rect;)V

    .line 285699
    iget-object v0, v7, LX/1dK;->y:LX/1mp;

    move-object v0, v0

    .line 285700
    if-eqz v0, :cond_7

    .line 285701
    iget-object v0, v7, LX/1dK;->y:LX/1mp;

    move-object v0, v0

    .line 285702
    invoke-virtual {v0}, LX/1mp;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 285703
    :try_start_0
    iget-object v0, v7, LX/1dK;->y:LX/1mp;

    move-object v0, v0

    .line 285704
    iget p2, v3, Landroid/graphics/Rect;->left:I

    iget p3, v3, Landroid/graphics/Rect;->top:I

    iget p4, v3, Landroid/graphics/Rect;->right:I

    iget p5, v3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, p2, p3, p4, p5}, LX/1mp;->a(IIII)V
    :try_end_0
    .catch LX/32E; {:try_start_0 .. :try_end_0} :catch_0

    .line 285705
    :cond_6
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 285706
    :catch_0
    :cond_7
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const/4 p2, 0x0

    .line 285707
    sget p3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p4, 0x18

    if-lt p3, p4, :cond_a

    .line 285708
    const/4 p4, 0x0

    .line 285709
    :try_start_1
    invoke-static {}, LX/1mq;->b()V

    .line 285710
    sget-boolean p3, LX/1mq;->a:Z

    if-eqz p3, :cond_10

    .line 285711
    const/4 p3, 0x0

    invoke-static {v0, p3}, Landroid/view/RenderNode;->create(Ljava/lang/String;Landroid/view/View;)Landroid/view/RenderNode;

    move-result-object p5

    .line 285712
    new-instance p3, LX/1mt;

    invoke-direct {p3, p5}, LX/1mt;-><init>(Landroid/view/RenderNode;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    .line 285713
    :goto_4
    move-object p3, p3

    .line 285714
    :goto_5
    if-nez p3, :cond_f

    .line 285715
    :goto_6
    move-object p2, p2

    .line 285716
    if-eqz p2, :cond_6

    .line 285717
    iget v0, p1, LX/1S3;->d:I

    move v0, v0

    .line 285718
    invoke-static {v4, v0}, LX/1cy;->a(Landroid/content/Context;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 285719
    if-nez v0, :cond_8

    .line 285720
    invoke-virtual {p1, v4}, LX/1S3;->a(LX/1De;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 285721
    :cond_8
    invoke-virtual {p1, v4, v0, p0}, LX/1S3;->e(LX/1De;Ljava/lang/Object;LX/1X1;)V

    .line 285722
    invoke-virtual {p1, v4, v0, p0}, LX/1S3;->b(LX/1De;Ljava/lang/Object;LX/1X1;)V

    .line 285723
    invoke-virtual {v7, v3}, LX/1dK;->a(Landroid/graphics/Rect;)V

    .line 285724
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result p3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result p4

    invoke-virtual {v0, v2, v2, p3, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 285725
    :try_start_2
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result p3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result p4

    invoke-virtual {p2, p3, p4}, LX/1mp;->a(II)Landroid/graphics/Canvas;

    move-result-object p3

    .line 285726
    invoke-virtual {v0, p3}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 285727
    invoke-virtual {p2, p3}, LX/1mp;->a(Landroid/graphics/Canvas;)V

    .line 285728
    iget p3, v3, Landroid/graphics/Rect;->left:I

    iget p4, v3, Landroid/graphics/Rect;->top:I

    iget p5, v3, Landroid/graphics/Rect;->right:I

    iget p6, v3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p2, p3, p4, p5, p6}, LX/1mp;->a(IIII)V

    .line 285729
    iput-object p2, v7, LX/1dK;->y:LX/1mp;
    :try_end_2
    .catch LX/32E; {:try_start_2 .. :try_end_2} :catch_1

    .line 285730
    :goto_7
    invoke-virtual {p1, v4, v0, p0}, LX/1S3;->c(LX/1De;Ljava/lang/Object;LX/1X1;)V

    .line 285731
    invoke-virtual {p1, v4, v0, p0}, LX/1S3;->f(LX/1De;Ljava/lang/Object;LX/1X1;)V

    .line 285732
    invoke-static {v4, p1, v0}, LX/1cy;->a(Landroid/content/Context;LX/1S3;Ljava/lang/Object;)V

    goto :goto_3

    .line 285733
    :catch_1
    const/4 p2, 0x0

    .line 285734
    iput-object p2, v7, LX/1dK;->y:LX/1mp;

    .line 285735
    goto :goto_7

    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 285736
    :cond_a
    sget p3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p4, 0x17

    if-lt p3, p4, :cond_b

    .line 285737
    const/4 p4, 0x0

    .line 285738
    :try_start_3
    invoke-static {}, LX/1mq;->b()V

    .line 285739
    sget-boolean p3, LX/1mq;->a:Z

    if-eqz p3, :cond_11

    .line 285740
    const/4 p3, 0x0

    invoke-static {v0, p3}, Landroid/view/RenderNode;->create(Ljava/lang/String;Landroid/view/View;)Landroid/view/RenderNode;

    move-result-object p5

    .line 285741
    new-instance p3, LX/1mq;

    invoke-direct {p3, p5}, LX/1mq;-><init>(Landroid/view/RenderNode;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3

    .line 285742
    :goto_8
    move-object p3, p3

    .line 285743
    goto :goto_5

    .line 285744
    :cond_b
    sget p3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p4, 0x15

    if-lt p3, p4, :cond_c

    .line 285745
    const/4 p3, 0x0

    invoke-static {v0, p3}, Landroid/view/RenderNode;->create(Ljava/lang/String;Landroid/view/View;)Landroid/view/RenderNode;

    move-result-object p3

    .line 285746
    new-instance p4, LX/1n1;

    invoke-direct {p4, p3}, LX/1n1;-><init>(Landroid/view/RenderNode;)V

    move-object p3, p4

    .line 285747
    goto/16 :goto_5

    .line 285748
    :cond_c
    sget p3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p4, 0x12

    if-lt p3, p4, :cond_d

    .line 285749
    invoke-static {v0}, LX/1ms;->b(Ljava/lang/String;)Landroid/view/DisplayList;

    move-result-object p4

    .line 285750
    if-nez p4, :cond_12

    .line 285751
    const/4 p3, 0x0

    .line 285752
    :goto_9
    move-object p3, p3

    .line 285753
    goto/16 :goto_5

    .line 285754
    :cond_d
    sget p3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p4, 0x10

    if-lt p3, p4, :cond_e

    .line 285755
    invoke-static {v0}, LX/1ms;->b(Ljava/lang/String;)Landroid/view/DisplayList;

    move-result-object p4

    .line 285756
    if-nez p4, :cond_13

    .line 285757
    const/4 p3, 0x0

    .line 285758
    :goto_a
    move-object p3, p3

    .line 285759
    goto/16 :goto_5

    :cond_e
    move-object p3, p2

    .line 285760
    goto/16 :goto_5

    .line 285761
    :cond_f
    new-instance p2, LX/1mp;

    invoke-direct {p2, p3}, LX/1mp;-><init>(LX/1mr;)V

    goto/16 :goto_6

    .line 285762
    :catch_2
    const/4 p3, 0x1

    sput-boolean p3, LX/1mt;->b:Z

    :cond_10
    move-object p3, p4

    .line 285763
    goto/16 :goto_4

    .line 285764
    :catch_3
    const/4 p3, 0x1

    sput-boolean p3, LX/1mq;->b:Z

    :cond_11
    move-object p3, p4

    .line 285765
    goto :goto_8

    :cond_12
    new-instance p3, LX/1mu;

    invoke-direct {p3, p4}, LX/1mu;-><init>(Landroid/view/DisplayList;)V

    goto :goto_9

    :cond_13
    new-instance p3, LX/1ms;

    invoke-direct {p3, p4}, LX/1ms;-><init>(Landroid/view/DisplayList;)V

    goto :goto_a
.end method

.method private static a(LX/1Dg;LX/1ml;)LX/1ml;
    .locals 2

    .prologue
    .line 285919
    const-string v0, "diff_node_creation"

    invoke-static {v0}, LX/1mm;->a(Ljava/lang/String;)V

    .line 285920
    sget-object v0, LX/1cy;->q:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ml;

    .line 285921
    if-nez v0, :cond_0

    .line 285922
    new-instance v0, LX/1ml;

    invoke-direct {v0}, LX/1ml;-><init>()V

    .line 285923
    :cond_0
    move-object v0, v0

    .line 285924
    invoke-virtual {p0}, LX/1Dg;->n()I

    move-result v1

    .line 285925
    iput v1, v0, LX/1ml;->i:I

    .line 285926
    invoke-virtual {p0}, LX/1Dg;->o()I

    move-result v1

    .line 285927
    iput v1, v0, LX/1ml;->j:I

    .line 285928
    iget v1, p0, LX/1Dg;->ag:F

    move v1, v1

    .line 285929
    iput v1, v0, LX/1ml;->g:F

    .line 285930
    iget v1, p0, LX/1Dg;->ah:F

    move v1, v1

    .line 285931
    iput v1, v0, LX/1ml;->h:F

    .line 285932
    iget-object v1, p0, LX/1Dg;->l:LX/1X1;

    move-object v1, v1

    .line 285933
    iput-object v1, v0, LX/1ml;->f:LX/1X1;

    .line 285934
    if-eqz p1, :cond_1

    .line 285935
    iget-object v1, p1, LX/1ml;->k:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285936
    :cond_1
    invoke-static {}, LX/1mm;->a()V

    .line 285937
    return-object v0
.end method

.method private static a(LX/0tf;LX/1dK;I)V
    .locals 5

    .prologue
    .line 285766
    if-eqz p0, :cond_0

    .line 285767
    iget-wide v3, p1, LX/1dK;->a:J

    move-wide v0, v3

    .line 285768
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 285769
    :cond_0
    return-void
.end method

.method private static a(LX/1Dg;LX/1dK;LX/1dN;Z)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 285770
    invoke-static {p2, p0}, LX/1dN;->a(LX/1dN;LX/1Dg;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285771
    iput-wide v2, p1, LX/1dK;->a:J

    .line 285772
    iput-wide v2, p1, LX/1dK;->g:J

    .line 285773
    const/4 v0, 0x2

    .line 285774
    iput v0, p1, LX/1dK;->w:I

    .line 285775
    :goto_0
    return-void

    .line 285776
    :cond_0
    iget-object v0, p2, LX/1dN;->k:LX/1mk;

    iget v2, p2, LX/1dN;->v:I

    const/4 v3, 0x3

    const-wide/16 v4, -0x1

    move-object v1, p1

    move v6, p3

    invoke-virtual/range {v0 .. v6}, LX/1mk;->a(LX/1dK;IIJZ)V

    goto :goto_0
.end method

.method private static a(LX/1Dg;LX/1dN;LX/1ml;)V
    .locals 18

    .prologue
    .line 285777
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->Z()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 285778
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->aa()V

    .line 285779
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->ai()LX/1X1;

    move-result-object v12

    .line 285780
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->v()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 285781
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->c()I

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, LX/1mh;->a(II)I

    move-result v2

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->d()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, LX/1mh;->a(II)I

    move-result v3

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, LX/1dN;->a(LX/1Dg;II)LX/1Dg;

    move-result-object v2

    .line 285782
    sget-object v3, LX/1De;->a:LX/1Dg;

    if-ne v2, v3, :cond_1

    .line 285783
    :goto_0
    return-void

    .line 285784
    :cond_1
    move-object/from16 v0, p1

    iget v3, v0, LX/1dN;->t:I

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->a()I

    move-result v4

    add-int/2addr v3, v4

    move-object/from16 v0, p1

    iput v3, v0, LX/1dN;->t:I

    .line 285785
    move-object/from16 v0, p1

    iget v3, v0, LX/1dN;->u:I

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->b()I

    move-result v4

    add-int/2addr v3, v4

    move-object/from16 v0, p1

    iput v3, v0, LX/1dN;->u:I

    .line 285786
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v2, v0, v1}, LX/1dN;->a(LX/1Dg;LX/1dN;LX/1ml;)V

    .line 285787
    move-object/from16 v0, p1

    iget v2, v0, LX/1dN;->t:I

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->a()I

    move-result v3

    sub-int/2addr v2, v3

    move-object/from16 v0, p1

    iput v2, v0, LX/1dN;->t:I

    .line 285788
    move-object/from16 v0, p1

    iget v2, v0, LX/1dN;->u:I

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->b()I

    move-result v3

    sub-int/2addr v2, v3

    move-object/from16 v0, p1

    iput v2, v0, LX/1dN;->u:I

    goto :goto_0

    .line 285789
    :cond_2
    move-object/from16 v0, p1

    iget-boolean v3, v0, LX/1dN;->z:Z

    .line 285790
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->s()LX/1ml;

    move-result-object v13

    .line 285791
    invoke-static {v12}, LX/1X1;->c(LX/1X1;)Z

    move-result v2

    if-eqz v2, :cond_b

    if-eqz v13, :cond_b

    const/4 v2, 0x1

    move v11, v2

    .line 285792
    :goto_1
    if-eqz v11, :cond_c

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->t()Z

    move-result v2

    if-eqz v2, :cond_c

    const/4 v8, 0x1

    .line 285793
    :goto_2
    if-eqz v3, :cond_d

    .line 285794
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, LX/1dN;->a(LX/1Dg;LX/1ml;)LX/1ml;

    move-result-object v2

    .line 285795
    if-nez p2, :cond_1a

    .line 285796
    move-object/from16 v0, p1

    iput-object v2, v0, LX/1dN;->p:LX/1ml;

    move-object v10, v2

    .line 285797
    :goto_3
    invoke-static/range {p0 .. p1}, LX/1dN;->e(LX/1Dg;LX/1dN;)Z

    move-result v9

    .line 285798
    move-object/from16 v0, p1

    iget-wide v14, v0, LX/1dN;->w:J

    .line 285799
    move-object/from16 v0, p1

    iget v0, v0, LX/1dN;->x:I

    move/from16 v16, v0

    .line 285800
    if-eqz v9, :cond_3

    .line 285801
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v10}, LX/1dN;->b(LX/1Dg;LX/1dN;LX/1ml;)I

    move-result v3

    .line 285802
    move-object/from16 v0, p1

    iget v2, v0, LX/1dN;->v:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p1

    iput v2, v0, LX/1dN;->v:I

    .line 285803
    move-object/from16 v0, p1

    iget-object v2, v0, LX/1dN;->h:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1dK;

    invoke-virtual {v2}, LX/1dK;->e()J

    move-result-wide v4

    move-object/from16 v0, p1

    iput-wide v4, v0, LX/1dN;->w:J

    .line 285804
    move-object/from16 v0, p1

    iput v3, v0, LX/1dN;->x:I

    .line 285805
    :cond_3
    move-object/from16 v0, p1

    iget-boolean v0, v0, LX/1dN;->y:Z

    move/from16 v17, v0

    .line 285806
    if-nez v9, :cond_4

    if-eqz v17, :cond_e

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->am()Z

    move-result v2

    if-eqz v2, :cond_e

    :cond_4
    const/4 v2, 0x1

    :goto_4
    move-object/from16 v0, p1

    iput-boolean v2, v0, LX/1dN;->y:Z

    .line 285807
    invoke-static/range {p0 .. p1}, LX/1dN;->a(LX/1Dg;LX/1dN;)LX/1dK;

    move-result-object v3

    .line 285808
    if-eqz v3, :cond_5

    .line 285809
    if-eqz v11, :cond_f

    invoke-virtual {v13}, LX/1ml;->g()LX/1dK;

    move-result-object v2

    invoke-virtual {v2}, LX/1dK;->e()J

    move-result-wide v6

    .line 285810
    :goto_5
    move-object/from16 v0, p1

    iget-object v2, v0, LX/1dN;->k:LX/1mk;

    move-object/from16 v0, p1

    iget v4, v0, LX/1dN;->v:I

    const/4 v5, 0x0

    invoke-virtual/range {v2 .. v8}, LX/1mk;->a(LX/1dK;IIJZ)V

    .line 285811
    :cond_5
    if-eqz v8, :cond_6

    .line 285812
    invoke-virtual {v13}, LX/1ml;->g()LX/1dK;

    move-result-object v2

    invoke-virtual {v2}, LX/1dK;->v()LX/1mp;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/1dK;->a(LX/1mp;)V

    .line 285813
    :cond_6
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->l()LX/1dc;

    move-result-object v7

    .line 285814
    if-eqz v7, :cond_7

    .line 285815
    instance-of v2, v3, LX/1dL;

    if-eqz v2, :cond_10

    move-object v2, v3

    .line 285816
    check-cast v2, LX/1dL;

    .line 285817
    invoke-virtual {v2, v7}, LX/1dL;->a(LX/1dc;)V

    .line 285818
    :cond_7
    :goto_6
    invoke-static {v12}, LX/1X1;->c(LX/1X1;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 285819
    invoke-virtual {v12}, LX/1X1;->n()LX/1S3;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v4, v0, LX/1dN;->c:LX/1De;

    move-object/from16 v0, p0

    invoke-virtual {v2, v4, v0, v12}, LX/1S3;->a(LX/1De;LX/1Dg;LX/1X1;)V

    .line 285820
    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1dN;->a(LX/1dN;LX/1dK;)V

    .line 285821
    move-object/from16 v0, p1

    iget-object v2, v0, LX/1dN;->j:LX/0tf;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/1dN;->h:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v2, v3, v4}, LX/1dN;->a(LX/0tf;LX/1dK;I)V

    .line 285822
    if-eqz v10, :cond_8

    .line 285823
    invoke-virtual {v10, v3}, LX/1ml;->a(LX/1dK;)V

    .line 285824
    :cond_8
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v2, v4, :cond_a

    if-eqz v12, :cond_a

    .line 285825
    invoke-virtual {v12}, LX/1X1;->n()LX/1S3;

    invoke-static {}, LX/1S3;->o()LX/1mx;

    move-result-object v2

    .line 285826
    if-eqz v2, :cond_a

    .line 285827
    move-object/from16 v0, p1

    iget-object v4, v0, LX/1dN;->d:LX/1my;

    if-nez v4, :cond_9

    .line 285828
    invoke-static {}, LX/1cy;->i()LX/1my;

    move-result-object v4

    move-object/from16 v0, p1

    iput-object v4, v0, LX/1dN;->d:LX/1my;

    .line 285829
    :cond_9
    move-object/from16 v0, p1

    iget-object v4, v0, LX/1dN;->d:LX/1my;

    invoke-virtual {v4, v2}, LX/1my;->a(LX/1mx;)V

    .line 285830
    :cond_a
    move-object/from16 v0, p1

    iget v2, v0, LX/1dN;->t:I

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->a()I

    move-result v4

    add-int/2addr v2, v4

    move-object/from16 v0, p1

    iput v2, v0, LX/1dN;->t:I

    .line 285831
    move-object/from16 v0, p1

    iget v2, v0, LX/1dN;->u:I

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->b()I

    move-result v4

    add-int/2addr v2, v4

    move-object/from16 v0, p1

    iput v2, v0, LX/1dN;->u:I

    .line 285832
    const/4 v2, 0x0

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->ae()I

    move-result v4

    :goto_7
    if-ge v2, v4, :cond_12

    .line 285833
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1Dg;->aJ(I)LX/1Dg;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v5, v0, v10}, LX/1dN;->a(LX/1Dg;LX/1dN;LX/1ml;)V

    .line 285834
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 285835
    :cond_b
    const/4 v2, 0x0

    move v11, v2

    goto/16 :goto_1

    .line 285836
    :cond_c
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 285837
    :cond_d
    const/4 v2, 0x0

    move-object v10, v2

    goto/16 :goto_3

    .line 285838
    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 285839
    :cond_f
    const-wide/16 v6, -0x1

    goto/16 :goto_5

    .line 285840
    :cond_10
    if-eqz v13, :cond_11

    invoke-virtual {v13}, LX/1ml;->i()LX/1dK;

    move-result-object v6

    .line 285841
    :goto_8
    const/4 v8, 0x1

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-static/range {v4 .. v9}, LX/1dN;->a(LX/1Dg;LX/1dN;LX/1dK;LX/1dc;IZ)LX/1dK;

    move-result-object v2

    .line 285842
    if-eqz v10, :cond_7

    .line 285843
    invoke-virtual {v10, v2}, LX/1ml;->b(LX/1dK;)V

    goto/16 :goto_6

    .line 285844
    :cond_11
    const/4 v6, 0x0

    goto :goto_8

    .line 285845
    :cond_12
    move-object/from16 v0, p1

    iget v2, v0, LX/1dN;->t:I

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->a()I

    move-result v4

    sub-int/2addr v2, v4

    move-object/from16 v0, p1

    iput v2, v0, LX/1dN;->t:I

    .line 285846
    move-object/from16 v0, p1

    iget v2, v0, LX/1dN;->u:I

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->b()I

    move-result v4

    sub-int/2addr v2, v4

    move-object/from16 v0, p1

    iput v2, v0, LX/1dN;->u:I

    .line 285847
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->m()LX/1dc;

    move-result-object v7

    .line 285848
    if-eqz v7, :cond_13

    .line 285849
    instance-of v2, v3, LX/1dL;

    if-eqz v2, :cond_17

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x17

    if-lt v2, v4, :cond_17

    .line 285850
    check-cast v3, LX/1dL;

    .line 285851
    invoke-virtual {v3, v7}, LX/1dL;->b(LX/1dc;)V

    .line 285852
    :cond_13
    :goto_9
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->p()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 285853
    invoke-static/range {p0 .. p1}, LX/1dN;->b(LX/1Dg;LX/1dN;)LX/1dW;

    move-result-object v4

    .line 285854
    if-eqz v11, :cond_19

    invoke-virtual {v13}, LX/1ml;->h()LX/1dW;

    move-result-object v2

    invoke-virtual {v2}, LX/1dW;->a()J

    move-result-wide v2

    .line 285855
    :goto_a
    move-object/from16 v0, p1

    iget-object v5, v0, LX/1dN;->k:LX/1mk;

    move-object/from16 v0, p1

    iget v6, v0, LX/1dN;->v:I

    invoke-virtual {v5, v4, v6, v2, v3}, LX/1mk;->a(LX/1dW;IJ)V

    .line 285856
    move-object/from16 v0, p1

    iget-object v2, v0, LX/1dN;->i:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285857
    if-eqz v10, :cond_14

    .line 285858
    invoke-virtual {v10, v4}, LX/1ml;->a(LX/1dW;)V

    .line 285859
    :cond_14
    move-object/from16 v0, p1

    iget-object v2, v0, LX/1dN;->n:Ljava/util/List;

    if-eqz v2, :cond_15

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->Y()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_15

    .line 285860
    invoke-static/range {p0 .. p1}, LX/1dN;->c(LX/1Dg;LX/1dN;)LX/1dP;

    move-result-object v2

    .line 285861
    move-object/from16 v0, p1

    iget-object v3, v0, LX/1dN;->n:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285862
    :cond_15
    move-object/from16 v0, p1

    iget-wide v2, v0, LX/1dN;->w:J

    cmp-long v2, v2, v14

    if-eqz v2, :cond_16

    .line 285863
    move-object/from16 v0, p1

    iput-wide v14, v0, LX/1dN;->w:J

    .line 285864
    move/from16 v0, v16

    move-object/from16 v1, p1

    iput v0, v1, LX/1dN;->x:I

    .line 285865
    move-object/from16 v0, p1

    iget v2, v0, LX/1dN;->v:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p1

    iput v2, v0, LX/1dN;->v:I

    .line 285866
    :cond_16
    move/from16 v0, v17

    move-object/from16 v1, p1

    iput-boolean v0, v1, LX/1dN;->y:Z

    .line 285867
    move-object/from16 v0, p1

    iget-object v2, v0, LX/1dN;->l:Ljava/util/ArrayList;

    sget-object v3, LX/1dN;->a:Ljava/util/Comparator;

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 285868
    move-object/from16 v0, p1

    iget-object v2, v0, LX/1dN;->m:Ljava/util/ArrayList;

    sget-object v3, LX/1dN;->b:Ljava/util/Comparator;

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto/16 :goto_0

    .line 285869
    :cond_17
    if-eqz v13, :cond_18

    invoke-virtual {v13}, LX/1ml;->j()LX/1dK;

    move-result-object v6

    .line 285870
    :goto_b
    const/4 v8, 0x2

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-static/range {v4 .. v9}, LX/1dN;->a(LX/1Dg;LX/1dN;LX/1dK;LX/1dc;IZ)LX/1dK;

    move-result-object v2

    .line 285871
    if-eqz v10, :cond_13

    .line 285872
    invoke-virtual {v10, v2}, LX/1ml;->c(LX/1dK;)V

    goto/16 :goto_9

    .line 285873
    :cond_18
    const/4 v6, 0x0

    goto :goto_b

    .line 285874
    :cond_19
    const-wide/16 v2, -0x1

    goto/16 :goto_a

    :cond_1a
    move-object v10, v2

    goto/16 :goto_3
.end method

.method public static a(LX/1Dg;Z)V
    .locals 3

    .prologue
    .line 285875
    sget-object v0, LX/1De;->a:LX/1Dg;

    if-ne p0, v0, :cond_0

    .line 285876
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot release a null node tree"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 285877
    :cond_0
    invoke-virtual {p0}, LX/1Dg;->ae()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_2

    .line 285878
    invoke-virtual {p0, v0}, LX/1Dg;->aJ(I)LX/1Dg;

    move-result-object v1

    .line 285879
    if-eqz p1, :cond_1

    invoke-virtual {p0}, LX/1Dg;->Z()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 285880
    invoke-virtual {p0}, LX/1Dg;->aa()V

    .line 285881
    :cond_1
    iget-object v2, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v2, v0}, LX/1mn;->b(I)LX/1mn;

    move-result-object v2

    invoke-interface {v2}, LX/1mn;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Dg;

    .line 285882
    invoke-static {v1, p1}, LX/1dN;->a(LX/1Dg;Z)V

    .line 285883
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 285884
    :cond_2
    iget-object v0, p0, LX/1Dg;->p:LX/1Dg;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 285885
    if-eqz v0, :cond_3

    .line 285886
    iget-object v0, p0, LX/1Dg;->p:LX/1Dg;

    move-object v0, v0

    .line 285887
    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/1dN;->a(LX/1Dg;Z)V

    .line 285888
    :cond_3
    invoke-virtual {p0}, LX/1Dg;->an()V

    .line 285889
    sget-object v0, LX/1cy;->e:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 285890
    return-void

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(LX/1dK;LX/1Dg;)V
    .locals 8

    .prologue
    .line 285891
    iget-wide v4, p1, LX/1Dg;->q:J

    const-wide v6, 0x80000000L

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    move v0, v4

    .line 285892
    if-nez v0, :cond_0

    .line 285893
    :goto_1
    return-void

    .line 285894
    :cond_0
    const/4 v0, 0x0

    .line 285895
    invoke-static {p1}, LX/1Dg;->ao(LX/1Dg;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 285896
    :goto_2
    move v0, v0

    .line 285897
    invoke-static {p1}, LX/1Dg;->ao(LX/1Dg;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 285898
    const/4 v1, 0x0

    .line 285899
    :goto_3
    move v1, v1

    .line 285900
    invoke-static {p1}, LX/1Dg;->ao(LX/1Dg;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 285901
    const/4 v2, 0x0

    .line 285902
    :goto_4
    move v2, v2

    .line 285903
    invoke-static {p1}, LX/1Dg;->ao(LX/1Dg;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 285904
    const/4 v3, 0x0

    .line 285905
    :goto_5
    move v3, v3

    .line 285906
    invoke-virtual {p0, v0, v1, v2, v3}, LX/1dK;->b(IIII)V

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 285907
    :cond_2
    iget v1, p1, LX/1Dg;->X:F

    invoke-static {v1}, LX/1mo;->a(F)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 285908
    iget-object v1, p1, LX/1Dg;->O:LX/1mz;

    invoke-static {p1, v1, v0}, LX/1Dg;->a(LX/1Dg;LX/1mz;I)F

    move-result v0

    iput v0, p1, LX/1Dg;->X:F

    .line 285909
    :cond_3
    iget v0, p1, LX/1Dg;->X:F

    invoke-static {v0}, LX/1n0;->a(F)I

    move-result v0

    goto :goto_2

    :cond_4
    iget-object v1, p1, LX/1Dg;->O:LX/1mz;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/1mz;->a(I)F

    move-result v1

    invoke-static {v1}, LX/1n0;->a(F)I

    move-result v1

    goto :goto_3

    .line 285910
    :cond_5
    iget v2, p1, LX/1Dg;->Y:F

    invoke-static {v2}, LX/1mo;->a(F)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 285911
    iget-object v2, p1, LX/1Dg;->O:LX/1mz;

    const/4 v3, 0x2

    invoke-static {p1, v2, v3}, LX/1Dg;->a(LX/1Dg;LX/1mz;I)F

    move-result v2

    iput v2, p1, LX/1Dg;->Y:F

    .line 285912
    :cond_6
    iget v2, p1, LX/1Dg;->Y:F

    invoke-static {v2}, LX/1n0;->a(F)I

    move-result v2

    goto :goto_4

    :cond_7
    iget-object v3, p1, LX/1Dg;->O:LX/1mz;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, LX/1mz;->a(I)F

    move-result v3

    invoke-static {v3}, LX/1n0;->a(F)I

    move-result v3

    goto :goto_5
.end method

.method private static a(LX/1dN;LX/1dK;)V
    .locals 1

    .prologue
    .line 285913
    iget-object v0, p0, LX/1dN;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285914
    iget-object v0, p0, LX/1dN;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 285915
    iget-object v0, p0, LX/1dN;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 285916
    return-void
.end method

.method private static a(IIF)Z
    .locals 2

    .prologue
    .line 285917
    const/high16 v0, 0x40000000    # 2.0f

    if-ne p0, v0, :cond_0

    int-to-float v0, p1

    sub-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(IIIF)Z
    .locals 1

    .prologue
    .line 285442
    const/high16 v0, -0x80000000

    if-ne p1, v0, :cond_0

    if-nez p0, :cond_0

    int-to-float v0, p2

    cmpl-float v0, v0, p3

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(IIIIF)Z
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 285918
    if-ne p0, v0, :cond_0

    if-ne p1, v0, :cond_0

    if-le p2, p3, :cond_0

    int-to-float v0, p3

    cmpg-float v0, p4, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(IIIIFF)Z
    .locals 10

    .prologue
    .line 285316
    if-ne p2, p0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 285317
    :goto_0
    if-ne p3, p1, :cond_3

    const/4 v0, 0x1

    .line 285318
    :goto_1
    invoke-static {p2}, LX/1mh;->a(I)I

    move-result v2

    .line 285319
    invoke-static {p3}, LX/1mh;->a(I)I

    move-result v3

    .line 285320
    invoke-static {p0}, LX/1mh;->a(I)I

    move-result v4

    .line 285321
    invoke-static {p1}, LX/1mh;->a(I)I

    move-result v5

    .line 285322
    invoke-static {p2}, LX/1mh;->b(I)I

    move-result v6

    .line 285323
    invoke-static {p3}, LX/1mh;->b(I)I

    move-result v7

    .line 285324
    invoke-static {p0}, LX/1mh;->b(I)I

    move-result v8

    .line 285325
    invoke-static {p1}, LX/1mh;->b(I)I

    move-result v9

    .line 285326
    if-nez v1, :cond_0

    invoke-static {v2, v6, p4}, LX/1dN;->a(IIF)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v4, v2, v6, p4}, LX/1dN;->a(IIIF)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v4, v2, v8, v6, p4}, LX/1dN;->a(IIIIF)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    const/4 v1, 0x1

    .line 285327
    :goto_2
    if-nez v0, :cond_1

    invoke-static {v3, v7, p5}, LX/1dN;->a(IIF)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v5, v3, v7, p5}, LX/1dN;->a(IIIF)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v5, v3, v9, v7, p5}, LX/1dN;->a(IIIIF)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    const/4 v0, 0x1

    .line 285328
    :goto_3
    if-eqz v1, :cond_6

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_4
    return v0

    .line 285329
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 285330
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 285331
    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    .line 285332
    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    .line 285333
    :cond_6
    const/4 v0, 0x0

    goto :goto_4
.end method

.method private static a(LX/1dN;LX/1Dg;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 285177
    iget-object v2, p0, LX/1dN;->o:LX/1Dg;

    .line 285178
    iget-boolean v3, v2, LX/1Dg;->o:Z

    move v2, v3

    .line 285179
    if-eqz v2, :cond_2

    iget-object v2, p0, LX/1dN;->o:LX/1Dg;

    .line 285180
    iget-object v3, v2, LX/1Dg;->p:LX/1Dg;

    move-object v2, v3

    .line 285181
    if-ne p1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, LX/1dN;->o:LX/1Dg;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private static a(Landroid/view/accessibility/AccessibilityManager;)Z
    .locals 1

    .prologue
    .line 285314
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/1d8;->b(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/1Dg;LX/1dN;LX/1ml;)I
    .locals 18

    .prologue
    .line 285282
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->ai()LX/1X1;

    move-result-object v2

    .line 285283
    invoke-static {v2}, LX/1X1;->d(LX/1X1;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-static {v0, v1}, LX/1dN;->a(LX/1dN;LX/1Dg;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 285284
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "We shouldn\'t insert a host as a parent of a View"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 285285
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->E()LX/1dQ;

    move-result-object v15

    .line 285286
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->F()LX/1dQ;

    move-result-object v16

    .line 285287
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->H()LX/1dQ;

    move-result-object v17

    .line 285288
    invoke-static {}, LX/1dI;->q()LX/1X1;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->i()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->a()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->b()I

    move-result v6

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->c()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->d()I

    move-result v8

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->al()I

    move-result v9

    invoke-virtual/range {p0 .. p0}, LX/1Dg;->am()Z

    move-result v10

    if-eqz v15, :cond_2

    const/4 v11, 0x1

    :goto_0
    if-eqz v16, :cond_3

    const/4 v12, 0x1

    :goto_1
    if-eqz v17, :cond_4

    const/4 v13, 0x1

    :goto_2
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->G()I

    move-result v14

    move-object/from16 v3, p1

    invoke-static/range {v2 .. v14}, LX/1dN;->a(LX/1X1;LX/1dN;IIIIIIZZZZI)LX/1dK;

    move-result-object v2

    check-cast v2, LX/1dL;

    .line 285289
    invoke-virtual {v2, v15}, LX/1dK;->a(LX/1dQ;)V

    .line 285290
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, LX/1dK;->b(LX/1dQ;)V

    .line 285291
    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, LX/1dK;->c(LX/1dQ;)V

    .line 285292
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->W()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1dK;->a(Ljava/lang/Object;)V

    .line 285293
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->X()Landroid/util/SparseArray;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1dK;->a(Landroid/util/SparseArray;)V

    .line 285294
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->N()LX/1dQ;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1dK;->d(LX/1dQ;)V

    .line 285295
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->O()LX/1dQ;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1dK;->e(LX/1dQ;)V

    .line 285296
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->Q()LX/1dQ;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1dK;->f(LX/1dQ;)V

    .line 285297
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->P()LX/1dQ;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1dK;->g(LX/1dQ;)V

    .line 285298
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->R()LX/1dQ;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1dK;->h(LX/1dQ;)V

    .line 285299
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->S()LX/1dQ;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1dK;->i(LX/1dQ;)V

    .line 285300
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->T()LX/1dQ;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1dK;->j(LX/1dQ;)V

    .line 285301
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->U()LX/1dQ;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1dK;->k(LX/1dQ;)V

    .line 285302
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->M()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1dK;->a(Ljava/lang/CharSequence;)V

    .line 285303
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->G()I

    move-result v3

    invoke-virtual {v2, v3}, LX/1dK;->d(I)V

    .line 285304
    invoke-virtual/range {p0 .. p0}, LX/1Dg;->V()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1dL;->a(Ljava/lang/String;)V

    .line 285305
    move-object/from16 v0, p0

    invoke-static {v2, v0}, LX/1dN;->a(LX/1dK;LX/1Dg;)V

    .line 285306
    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1dN;->a(LX/1dN;LX/1dK;)V

    .line 285307
    move-object/from16 v0, p1

    iget-object v3, v0, LX/1dN;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 285308
    if-eqz p2, :cond_1

    .line 285309
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, LX/1ml;->d(LX/1dK;)V

    .line 285310
    :cond_1
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v2, v1, v4}, LX/1dN;->a(LX/1Dg;LX/1dK;LX/1dN;Z)V

    .line 285311
    move-object/from16 v0, p1

    iget-object v4, v0, LX/1dN;->j:LX/0tf;

    invoke-static {v4, v2, v3}, LX/1dN;->a(LX/0tf;LX/1dK;I)V

    .line 285312
    return v3

    .line 285313
    :cond_2
    const/4 v11, 0x0

    goto/16 :goto_0

    :cond_3
    const/4 v12, 0x0

    goto/16 :goto_1

    :cond_4
    const/4 v13, 0x0

    goto/16 :goto_2
.end method

.method private static b(LX/1Dg;LX/1dN;)LX/1dW;
    .locals 10

    .prologue
    .line 285256
    iget v0, p1, LX/1dN;->t:I

    invoke-virtual {p0}, LX/1Dg;->a()I

    move-result v1

    add-int/2addr v1, v0

    .line 285257
    iget v0, p1, LX/1dN;->u:I

    invoke-virtual {p0}, LX/1Dg;->b()I

    move-result v2

    add-int/2addr v2, v0

    .line 285258
    invoke-virtual {p0}, LX/1Dg;->c()I

    move-result v0

    add-int v3, v1, v0

    .line 285259
    invoke-virtual {p0}, LX/1Dg;->d()I

    move-result v0

    add-int v4, v2, v0

    .line 285260
    iget-object v0, p0, LX/1Dg;->y:LX/1dQ;

    move-object v5, v0

    .line 285261
    iget-object v0, p0, LX/1Dg;->z:LX/1dQ;

    move-object v6, v0

    .line 285262
    iget-object v0, p0, LX/1Dg;->A:LX/1dQ;

    move-object v7, v0

    .line 285263
    iget-object v0, p0, LX/1Dg;->B:LX/1dQ;

    move-object v8, v0

    .line 285264
    sget-object v0, LX/1cy;->k:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dW;

    .line 285265
    if-nez v0, :cond_0

    .line 285266
    new-instance v0, LX/1dW;

    invoke-direct {v0}, LX/1dW;-><init>()V

    .line 285267
    :cond_0
    move-object v9, v0

    .line 285268
    if-eqz v5, :cond_1

    .line 285269
    iget-object v0, v5, LX/1dQ;->a:LX/1X1;

    .line 285270
    :goto_0
    iput-object v0, v9, LX/1dW;->b:LX/1X1;

    .line 285271
    iget-object v0, v9, LX/1dW;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 285272
    iput-object v5, v9, LX/1dW;->d:LX/1dQ;

    .line 285273
    iput-object v6, v9, LX/1dW;->e:LX/1dQ;

    .line 285274
    iput-object v7, v9, LX/1dW;->f:LX/1dQ;

    .line 285275
    iput-object v8, v9, LX/1dW;->g:LX/1dQ;

    .line 285276
    return-object v9

    .line 285277
    :cond_1
    if-eqz v6, :cond_2

    .line 285278
    iget-object v0, v6, LX/1dQ;->a:LX/1X1;

    goto :goto_0

    .line 285279
    :cond_2
    if-eqz v7, :cond_3

    .line 285280
    iget-object v0, v7, LX/1dQ;->a:LX/1X1;

    goto :goto_0

    .line 285281
    :cond_3
    iget-object v0, v8, LX/1dQ;->a:LX/1X1;

    goto :goto_0
.end method

.method public static b(LX/1Dg;LX/1ml;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 285219
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v0}, LX/1mn;->r()LX/1mn;

    move-result-object v0

    if-nez v0, :cond_8

    .line 285220
    :cond_0
    const/4 v0, 0x0

    .line 285221
    :goto_0
    move-object v0, v0

    .line 285222
    if-nez v0, :cond_2

    move v0, v1

    .line 285223
    :goto_1
    iget-object v3, p0, LX/1Dg;->l:LX/1X1;

    move-object v3, v3

    .line 285224
    invoke-static {v3}, LX/1X1;->e(LX/1X1;)Z

    move-result v3

    if-eqz v3, :cond_3

    if-nez v0, :cond_3

    .line 285225
    invoke-virtual {p0, p1}, LX/1Dg;->a(LX/1ml;)V

    .line 285226
    :cond_1
    :goto_2
    return v1

    :cond_2
    move v0, v2

    .line 285227
    goto :goto_1

    .line 285228
    :cond_3
    invoke-static {p0, p1}, LX/1dN;->d(LX/1Dg;LX/1ml;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 285229
    invoke-virtual {p0, p1}, LX/1Dg;->a(LX/1ml;)V

    .line 285230
    invoke-virtual {p0}, LX/1Dg;->ae()I

    move-result v0

    .line 285231
    iget-object v3, p1, LX/1ml;->k:Ljava/util/List;

    if-nez v3, :cond_9

    const/4 v3, 0x0

    :goto_3
    move v3, v3

    .line 285232
    if-eq v0, v3, :cond_4

    .line 285233
    :goto_4
    if-ge v2, v0, :cond_5

    if-ge v2, v3, :cond_5

    .line 285234
    invoke-virtual {p0, v2}, LX/1Dg;->aJ(I)LX/1Dg;

    move-result-object v4

    .line 285235
    iget-object v5, p1, LX/1ml;->k:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1ml;

    move-object v5, v5

    .line 285236
    invoke-static {v4, v5}, LX/1dN;->b(LX/1Dg;LX/1ml;)Z

    move-result v4

    .line 285237
    or-int/2addr v1, v4

    .line 285238
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_4
    move v1, v2

    .line 285239
    goto :goto_4

    .line 285240
    :cond_5
    const/4 v0, 0x1

    .line 285241
    if-nez p1, :cond_a

    .line 285242
    :cond_6
    :goto_5
    move v0, v0

    .line 285243
    or-int/2addr v1, v0

    .line 285244
    if-nez v1, :cond_1

    .line 285245
    iget-object v0, p0, LX/1Dg;->l:LX/1X1;

    move-object v0, v0

    .line 285246
    if-eqz v0, :cond_7

    .line 285247
    iget-object v2, p1, LX/1ml;->f:LX/1X1;

    move-object v2, v2

    .line 285248
    invoke-virtual {v0, v2}, LX/1X1;->a(LX/1X1;)V

    .line 285249
    :cond_7
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1Dg;->e(Z)V

    .line 285250
    goto :goto_2

    :cond_8
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v0}, LX/1mn;->r()LX/1mn;

    move-result-object v0

    invoke-interface {v0}, LX/1mn;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Dg;

    goto :goto_0

    :cond_9
    iget-object v3, p1, LX/1ml;->k:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    goto :goto_3

    .line 285251
    :cond_a
    iget-object v2, p0, LX/1Dg;->l:LX/1X1;

    move-object v2, v2

    .line 285252
    if-eqz v2, :cond_6

    .line 285253
    iget-object v0, v2, LX/1X1;->e:LX/1S3;

    move-object v0, v0

    .line 285254
    iget-object v3, p1, LX/1ml;->f:LX/1X1;

    move-object v3, v3

    .line 285255
    invoke-virtual {v0, v2, v3}, LX/1S3;->b(LX/1X1;LX/1X1;)Z

    move-result v0

    goto :goto_5
.end method

.method private static c(LX/1Dg;LX/1dN;)LX/1dP;
    .locals 7

    .prologue
    .line 285203
    iget v0, p1, LX/1dN;->t:I

    invoke-virtual {p0}, LX/1Dg;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 285204
    iget v1, p1, LX/1dN;->u:I

    invoke-virtual {p0}, LX/1Dg;->b()I

    move-result v2

    add-int/2addr v1, v2

    .line 285205
    invoke-virtual {p0}, LX/1Dg;->c()I

    move-result v2

    add-int/2addr v2, v0

    .line 285206
    invoke-virtual {p0}, LX/1Dg;->d()I

    move-result v3

    add-int/2addr v3, v1

    .line 285207
    sget-object v4, LX/1cy;->l:LX/0Zi;

    if-nez v4, :cond_0

    .line 285208
    new-instance v4, LX/0Zi;

    const/16 v5, 0x40

    invoke-direct {v4, v5}, LX/0Zi;-><init>(I)V

    sput-object v4, LX/1cy;->l:LX/0Zi;

    .line 285209
    :cond_0
    sget-object v4, LX/1cy;->l:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1dP;

    .line 285210
    if-nez v4, :cond_1

    .line 285211
    new-instance v4, LX/1dP;

    invoke-direct {v4}, LX/1dP;-><init>()V

    .line 285212
    :cond_1
    move-object v4, v4

    .line 285213
    iget-object v5, p0, LX/1Dg;->N:Ljava/lang/String;

    move-object v5, v5

    .line 285214
    iput-object v5, v4, LX/1dP;->a:Ljava/lang/String;

    .line 285215
    iget-object v5, v4, LX/1dP;->c:Landroid/graphics/Rect;

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 285216
    iget-wide v0, p1, LX/1dN;->w:J

    .line 285217
    iput-wide v0, v4, LX/1dP;->b:J

    .line 285218
    return-object v4
.end method

.method public static c(Landroid/content/Context;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 285197
    instance-of v0, p0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 285198
    check-cast p0, Landroid/app/Activity;

    .line 285199
    :goto_0
    return-object p0

    .line 285200
    :cond_0
    instance-of v0, p0, Landroid/content/ContextWrapper;

    if-eqz v0, :cond_1

    .line 285201
    check-cast p0, Landroid/content/ContextWrapper;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/1dN;->c(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object p0

    goto :goto_0

    .line 285202
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method private static d(LX/1Dg;LX/1ml;)Z
    .locals 2

    .prologue
    .line 285182
    if-nez p1, :cond_0

    .line 285183
    const/4 v0, 0x0

    .line 285184
    :goto_0
    return v0

    .line 285185
    :cond_0
    iget-object v0, p0, LX/1Dg;->l:LX/1X1;

    move-object v0, v0

    .line 285186
    iget-object v1, p1, LX/1ml;->f:LX/1X1;

    move-object v1, v1

    .line 285187
    if-ne v0, v1, :cond_1

    .line 285188
    const/4 p0, 0x1

    .line 285189
    :goto_1
    move v0, p0

    .line 285190
    goto :goto_0

    .line 285191
    :cond_1
    if-eqz v0, :cond_2

    if-nez v1, :cond_3

    .line 285192
    :cond_2
    const/4 p0, 0x0

    goto :goto_1

    .line 285193
    :cond_3
    iget-object p0, v0, LX/1X1;->e:LX/1S3;

    move-object p0, p0

    .line 285194
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    .line 285195
    iget-object p1, v1, LX/1X1;->e:LX/1S3;

    move-object p1, p1

    .line 285196
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p0

    goto :goto_1
.end method

.method private static e(LX/1Dg;LX/1dN;)Z
    .locals 9

    .prologue
    .line 285334
    invoke-static {p1, p0}, LX/1dN;->a(LX/1dN;LX/1Dg;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 285335
    iget-object v0, p0, LX/1Dg;->l:LX/1X1;

    move-object v0, v0

    .line 285336
    invoke-static {v0}, LX/1X1;->d(LX/1X1;)Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 285337
    iget-object v0, p0, LX/1Dg;->l:LX/1X1;

    move-object v0, v0

    .line 285338
    iget-object v3, p0, LX/1Dg;->D:LX/1dQ;

    if-nez v3, :cond_0

    iget-object v3, p0, LX/1Dg;->F:LX/1dQ;

    if-nez v3, :cond_0

    iget-object v3, p0, LX/1Dg;->E:LX/1dQ;

    if-nez v3, :cond_0

    iget-object v3, p0, LX/1Dg;->G:LX/1dQ;

    if-nez v3, :cond_0

    iget-object v3, p0, LX/1Dg;->H:LX/1dQ;

    if-nez v3, :cond_0

    iget-object v3, p0, LX/1Dg;->C:LX/1dQ;

    if-nez v3, :cond_0

    iget-object v3, p0, LX/1Dg;->I:LX/1dQ;

    if-nez v3, :cond_0

    iget-object v3, p0, LX/1Dg;->J:LX/1dQ;

    if-eqz v3, :cond_f

    :cond_0
    const/4 v3, 0x1

    :goto_0
    move v3, v3

    .line 285339
    if-nez v3, :cond_1

    if-eqz v0, :cond_8

    .line 285340
    iget-object v3, v0, LX/1X1;->e:LX/1S3;

    move-object v0, v3

    .line 285341
    invoke-virtual {v0}, LX/1S3;->h()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_1
    move v0, v2

    .line 285342
    :goto_1
    iget v3, p0, LX/1Dg;->m:I

    move v3, v3

    .line 285343
    iget-boolean v4, p1, LX/1dN;->C:Z

    if-eqz v4, :cond_9

    const/4 v4, 0x2

    if-eq v3, v4, :cond_9

    if-nez v0, :cond_2

    .line 285344
    iget-object v0, p0, LX/1Dg;->K:Ljava/lang/CharSequence;

    move-object v0, v0

    .line 285345
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v3, :cond_9

    :cond_2
    move v0, v2

    .line 285346
    :goto_2
    iget-object v3, p0, LX/1Dg;->v:LX/1dQ;

    move-object v3, v3

    .line 285347
    if-nez v3, :cond_3

    .line 285348
    iget-object v3, p0, LX/1Dg;->w:LX/1dQ;

    move-object v3, v3

    .line 285349
    if-eqz v3, :cond_a

    :cond_3
    move v3, v2

    .line 285350
    :goto_3
    iget-object v4, p0, LX/1Dg;->x:LX/1dQ;

    move-object v4, v4

    .line 285351
    if-eqz v4, :cond_b

    move v4, v2

    .line 285352
    :goto_4
    iget-object v5, p0, LX/1Dg;->L:Ljava/lang/Object;

    move-object v5, v5

    .line 285353
    if-eqz v5, :cond_c

    move v5, v2

    .line 285354
    :goto_5
    iget-object v6, p0, LX/1Dg;->M:Landroid/util/SparseArray;

    move-object v6, v6

    .line 285355
    if-eqz v6, :cond_d

    move v6, v2

    .line 285356
    :goto_6
    invoke-virtual {p0}, LX/1Dg;->G()I

    move-result v7

    const/16 v8, 0x100

    if-ne v7, v8, :cond_e

    move v7, v2

    .line 285357
    :goto_7
    if-nez v3, :cond_4

    if-nez v4, :cond_4

    if-nez v5, :cond_4

    if-nez v6, :cond_4

    if-nez v0, :cond_4

    if-eqz v7, :cond_5

    :cond_4
    move v1, v2

    :cond_5
    move v0, v1

    .line 285358
    if-nez v0, :cond_6

    .line 285359
    iget-boolean v0, p0, LX/1Dg;->t:Z

    move v0, v0

    .line 285360
    if-eqz v0, :cond_7

    :cond_6
    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_8

    :cond_8
    move v0, v1

    .line 285361
    goto :goto_1

    :cond_9
    move v0, v1

    .line 285362
    goto :goto_2

    :cond_a
    move v3, v1

    .line 285363
    goto :goto_3

    :cond_b
    move v4, v1

    .line 285364
    goto :goto_4

    :cond_c
    move v5, v1

    .line 285365
    goto :goto_5

    :cond_d
    move v6, v1

    .line 285366
    goto :goto_6

    :cond_e
    move v7, v1

    .line 285367
    goto :goto_7

    :cond_f
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(J)I
    .locals 3

    .prologue
    .line 285368
    iget-object v0, p0, LX/1dN;->j:LX/0tf;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, LX/0tf;->a(JLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 285369
    iget-object v0, p0, LX/1dN;->e:LX/1X1;

    .line 285370
    iget p0, v0, LX/1X1;->b:I

    move v0, p0

    .line 285371
    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(II)Z
    .locals 1

    .prologue
    .line 285372
    invoke-static {p1}, LX/1mh;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/1dN;->f:I

    if-ne v0, p1, :cond_2

    :cond_0
    invoke-static {p2}, LX/1mh;->a(I)I

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, LX/1dN;->g:I

    if-ne v0, p2, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)LX/1dK;
    .locals 1

    .prologue
    .line 285315
    iget-object v0, p0, LX/1dN;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dK;

    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 285373
    iget-object v0, p0, LX/1dN;->B:Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v0}, LX/1dN;->a(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v0

    iget-boolean v1, p0, LX/1dN;->C:Z

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 285374
    iget-object v0, p0, LX/1dN;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final l()V
    .locals 8

    .prologue
    const/4 v3, -0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 285375
    iget-object v0, p0, LX/1dN;->q:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 285376
    if-gez v0, :cond_0

    .line 285377
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to releaseRef a recycled LayoutState"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 285378
    :cond_0
    if-nez v0, :cond_7

    .line 285379
    iput-object v4, p0, LX/1dN;->c:LX/1De;

    .line 285380
    iput-object v4, p0, LX/1dN;->e:LX/1X1;

    .line 285381
    iput v2, p0, LX/1dN;->r:I

    .line 285382
    iput v2, p0, LX/1dN;->s:I

    .line 285383
    iput v2, p0, LX/1dN;->t:I

    .line 285384
    iput v2, p0, LX/1dN;->u:I

    .line 285385
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/1dN;->w:J

    .line 285386
    iput v3, p0, LX/1dN;->x:I

    .line 285387
    iput v3, p0, LX/1dN;->A:I

    .line 285388
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1dN;->y:Z

    .line 285389
    iget-object v0, p0, LX/1dN;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    .line 285390
    iget-object v0, p0, LX/1dN;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dK;

    .line 285391
    invoke-virtual {v0}, LX/1dK;->w()V

    .line 285392
    instance-of v5, v0, LX/1dL;

    if-eqz v5, :cond_8

    .line 285393
    sget-object v5, LX/1cy;->o:LX/0Zi;

    check-cast v0, LX/1dL;

    invoke-virtual {v5, v0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 285394
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 285395
    :cond_1
    iget-object v0, p0, LX/1dN;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 285396
    iget-object v0, p0, LX/1dN;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 285397
    iget-object v0, p0, LX/1dN;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 285398
    iget-object v0, p0, LX/1dN;->j:LX/0tf;

    invoke-virtual {v0}, LX/0tf;->b()V

    .line 285399
    iget-object v0, p0, LX/1dN;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    :goto_2
    if-ge v1, v3, :cond_2

    .line 285400
    iget-object v0, p0, LX/1dN;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dW;

    .line 285401
    const/4 v5, 0x0

    .line 285402
    iput-object v5, v0, LX/1dW;->b:LX/1X1;

    .line 285403
    iput-object v5, v0, LX/1dW;->d:LX/1dQ;

    .line 285404
    iput-object v5, v0, LX/1dW;->e:LX/1dQ;

    .line 285405
    iput-object v5, v0, LX/1dW;->f:LX/1dQ;

    .line 285406
    iput-object v5, v0, LX/1dW;->g:LX/1dQ;

    .line 285407
    iget-object v5, v0, LX/1dW;->c:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->setEmpty()V

    .line 285408
    sget-object v5, LX/1cy;->k:LX/0Zi;

    invoke-virtual {v5, v0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 285409
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 285410
    :cond_2
    iget-object v0, p0, LX/1dN;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 285411
    iget-object v0, p0, LX/1dN;->n:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 285412
    iget-object v0, p0, LX/1dN;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    :goto_3
    if-ge v1, v3, :cond_3

    .line 285413
    iget-object v0, p0, LX/1dN;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dP;

    .line 285414
    const/4 v6, 0x0

    iput-object v6, v0, LX/1dP;->a:Ljava/lang/String;

    .line 285415
    const-wide/16 v6, -0x1

    iput-wide v6, v0, LX/1dP;->b:J

    .line 285416
    iget-object v6, v0, LX/1dP;->c:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->setEmpty()V

    .line 285417
    sget-object v5, LX/1cy;->l:LX/0Zi;

    invoke-virtual {v5, v0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 285418
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 285419
    :cond_3
    iget-object v0, p0, LX/1dN;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 285420
    :cond_4
    iput-boolean v2, p0, LX/1dN;->z:Z

    .line 285421
    iput-object v4, p0, LX/1dN;->B:Landroid/view/accessibility/AccessibilityManager;

    .line 285422
    iput-boolean v2, p0, LX/1dN;->C:Z

    .line 285423
    iget-object v0, p0, LX/1dN;->p:LX/1ml;

    if-eqz v0, :cond_5

    .line 285424
    iget-object v0, p0, LX/1dN;->p:LX/1ml;

    invoke-static {v0}, LX/1cy;->a(LX/1ml;)V

    .line 285425
    iput-object v4, p0, LX/1dN;->p:LX/1ml;

    .line 285426
    :cond_5
    iget-object v0, p0, LX/1dN;->k:LX/1mk;

    invoke-virtual {v0}, LX/1mk;->a()V

    .line 285427
    iget-object v0, p0, LX/1dN;->d:LX/1my;

    if-eqz v0, :cond_6

    .line 285428
    iget-object v0, p0, LX/1dN;->d:LX/1my;

    .line 285429
    iget-object v1, v0, LX/1my;->a:LX/01J;

    invoke-virtual {v1}, LX/01J;->clear()V

    .line 285430
    sget-object v1, LX/1cy;->z:LX/0Zj;

    invoke-virtual {v1, v0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 285431
    iput-object v4, p0, LX/1dN;->d:LX/1my;

    .line 285432
    :cond_6
    sget-object v0, LX/1cy;->d:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 285433
    :cond_7
    return-void

    .line 285434
    :cond_8
    sget-object v5, LX/1cy;->j:LX/0Zi;

    invoke-virtual {v5, v0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method public final m()LX/1dN;
    .locals 2

    .prologue
    .line 285435
    iget-object v0, p0, LX/1dN;->q:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    if-nez v0, :cond_0

    .line 285436
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to use a released LayoutState"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 285437
    :cond_0
    return-object p0
.end method

.method public final n()LX/1mg;
    .locals 2

    .prologue
    .line 285438
    iget-object v0, p0, LX/1dN;->D:LX/1mg;

    .line 285439
    const/4 v1, 0x0

    iput-object v1, p0, LX/1dN;->D:LX/1mg;

    .line 285440
    return-object v0
.end method
