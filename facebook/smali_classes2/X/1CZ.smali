.class public LX/1CZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1Ca;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/1Cb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/1Cc;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/1Ct;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/1D8;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/1DA;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/1DC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/1DF;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/1DJ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/1DL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/1DO;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/1EJ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:LX/1EK;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/1EL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 216469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216470
    return-void
.end method

.method public static a(LX/0QB;)LX/1CZ;
    .locals 15

    .prologue
    .line 216471
    new-instance v0, LX/1CZ;

    invoke-direct {v0}, LX/1CZ;-><init>()V

    .line 216472
    const-class v1, LX/1Ca;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/1Ca;

    const-class v2, LX/1Cb;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/1Cb;

    invoke-static {p0}, LX/1Cc;->b(LX/0QB;)LX/1Cc;

    move-result-object v3

    check-cast v3, LX/1Cc;

    invoke-static {p0}, LX/1Ct;->b(LX/0QB;)LX/1Ct;

    move-result-object v4

    check-cast v4, LX/1Ct;

    .line 216473
    new-instance v6, LX/1D8;

    const/16 v5, 0x9ab

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v5, 0xf63

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const-class v5, LX/1D9;

    invoke-interface {p0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/1D9;

    invoke-direct {v6, v7, v8, v5}, LX/1D8;-><init>(LX/0Or;LX/0Ot;LX/1D9;)V

    .line 216474
    move-object v5, v6

    .line 216475
    check-cast v5, LX/1D8;

    invoke-static {p0}, LX/1DA;->b(LX/0QB;)LX/1DA;

    move-result-object v6

    check-cast v6, LX/1DA;

    .line 216476
    new-instance v9, LX/1DC;

    const-class v7, LX/1DE;

    invoke-interface {p0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/1DE;

    invoke-static {p0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v8

    check-cast v8, LX/0fO;

    invoke-direct {v9, v7, v8}, LX/1DC;-><init>(LX/1DE;LX/0fO;)V

    .line 216477
    move-object v7, v9

    .line 216478
    check-cast v7, LX/1DC;

    .line 216479
    new-instance v11, LX/1DF;

    invoke-static {p0}, LX/0pJ;->a(LX/0QB;)LX/0pJ;

    move-result-object v8

    check-cast v8, LX/0pJ;

    const/16 v9, 0x2011

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {p0}, LX/0r3;->a(LX/0QB;)LX/0r3;

    move-result-object v9

    check-cast v9, LX/0r3;

    const-class v10, LX/1DG;

    invoke-interface {p0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/1DG;

    invoke-direct {v11, v8, v12, v9, v10}, LX/1DF;-><init>(LX/0pJ;LX/0Ot;LX/0r3;LX/1DG;)V

    .line 216480
    move-object v8, v11

    .line 216481
    check-cast v8, LX/1DF;

    .line 216482
    new-instance v11, LX/1DJ;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-direct {v11, v9}, LX/1DJ;-><init>(LX/0Uh;)V

    .line 216483
    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    .line 216484
    iput-object v9, v11, LX/1DJ;->a:LX/03V;

    iput-object v10, v11, LX/1DJ;->b:LX/0ad;

    .line 216485
    move-object v9, v11

    .line 216486
    check-cast v9, LX/1DJ;

    invoke-static {p0}, LX/1DL;->a(LX/0QB;)LX/1DL;

    move-result-object v10

    check-cast v10, LX/1DL;

    invoke-static {p0}, LX/1DO;->b(LX/0QB;)LX/1DO;

    move-result-object v11

    check-cast v11, LX/1DO;

    .line 216487
    new-instance v13, LX/1EJ;

    invoke-static {p0}, LX/18A;->b(LX/0QB;)LX/18A;

    move-result-object v12

    check-cast v12, LX/18A;

    invoke-direct {v13, v12}, LX/1EJ;-><init>(LX/18A;)V

    .line 216488
    move-object v12, v13

    .line 216489
    check-cast v12, LX/1EJ;

    const-class v13, LX/1EK;

    invoke-interface {p0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/1EK;

    const-class v14, LX/1EL;

    invoke-interface {p0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/1EL;

    .line 216490
    iput-object v1, v0, LX/1CZ;->a:LX/1Ca;

    iput-object v2, v0, LX/1CZ;->b:LX/1Cb;

    iput-object v3, v0, LX/1CZ;->c:LX/1Cc;

    iput-object v4, v0, LX/1CZ;->d:LX/1Ct;

    iput-object v5, v0, LX/1CZ;->e:LX/1D8;

    iput-object v6, v0, LX/1CZ;->f:LX/1DA;

    iput-object v7, v0, LX/1CZ;->g:LX/1DC;

    iput-object v8, v0, LX/1CZ;->h:LX/1DF;

    iput-object v9, v0, LX/1CZ;->i:LX/1DJ;

    iput-object v10, v0, LX/1CZ;->j:LX/1DL;

    iput-object v11, v0, LX/1CZ;->k:LX/1DO;

    iput-object v12, v0, LX/1CZ;->l:LX/1EJ;

    iput-object v13, v0, LX/1CZ;->m:LX/1EK;

    iput-object v14, v0, LX/1CZ;->n:LX/1EL;

    .line 216491
    move-object v0, v0

    .line 216492
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/fragment/NewsFeedFragment;LX/1Iv;LX/1EM;LX/1Iu;LX/1Iu;Lcom/facebook/api/feedtype/FeedType;LX/1J6;)LX/1KS;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/fragment/NewsFeedFragment;",
            "LX/1Iv;",
            "LX/1EM;",
            "LX/1Iu",
            "<",
            "Lcom/facebook/api/feed/data/LegacyFeedUnitUpdater;",
            ">;",
            "LX/1Iu",
            "<",
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;>;",
            "Lcom/facebook/api/feedtype/FeedType;",
            "LX/1J6;",
            ")",
            "LX/1KS;"
        }
    .end annotation

    .prologue
    .line 216493
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1CZ;->j:LX/1DL;

    invoke-virtual {v1}, LX/1DL;->a()LX/1Jg;

    move-result-object v10

    .line 216494
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1CZ;->m:LX/1EK;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1CZ;->k:LX/1DO;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/1CZ;->c:LX/1Cc;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/1CZ;->d:LX/1Ct;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1CZ;->e:LX/1D8;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/1CZ;->f:LX/1DA;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/1CZ;->g:LX/1DC;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/1CZ;->h:LX/1DF;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/1CZ;->i:LX/1DJ;

    invoke-virtual/range {v1 .. v10}, LX/1EK;->a(LX/1DO;LX/1Cc;LX/1Ct;LX/1D8;LX/1DA;LX/1DC;LX/1DF;LX/1DJ;LX/1Jg;)LX/1Jl;

    move-result-object v14

    .line 216495
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1CZ;->n:LX/1EL;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1CZ;->i:LX/1DJ;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/1CZ;->d:LX/1Ct;

    new-instance v4, LX/1K1;

    invoke-direct {v4, v10}, LX/1K1;-><init>(LX/1Jg;)V

    move-object/from16 v0, p0

    iget-object v6, v0, LX/1CZ;->c:LX/1Cc;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/1CZ;->k:LX/1DO;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1CZ;->b:LX/1Cb;

    sget-object v8, LX/1K2;->NEWSFEED:LX/1K2;

    invoke-virtual {v5, v8}, LX/1Cb;->a(LX/1K2;)LX/1K3;

    move-result-object v8

    new-instance v9, LX/1K4;

    invoke-direct {v9}, LX/1K4;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1CZ;->a:LX/1Ca;

    invoke-virtual {v5, v14}, LX/1Ca;->a(LX/1Jl;)LX/1K6;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, LX/1CZ;->h:LX/1DF;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/1CZ;->l:LX/1EJ;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/1CZ;->f:LX/1DA;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1CZ;->g:LX/1DC;

    move-object/from16 v16, v0

    move-object/from16 v5, p3

    move-object/from16 v10, p1

    move-object/from16 v17, p2

    move-object/from16 v18, p7

    invoke-virtual/range {v1 .. v18}, LX/1EL;->a(LX/1DJ;LX/1Ct;LX/1K1;LX/1EM;LX/1Cc;LX/1DO;LX/1K3;LX/1K4;Lcom/facebook/feed/fragment/NewsFeedFragment;LX/1K6;LX/1DF;LX/1EJ;LX/1Jl;LX/1DA;LX/1DC;LX/1Iv;LX/1J6;)LX/1KS;

    move-result-object v1

    .line 216496
    new-instance v2, LX/1Iu;

    invoke-direct {v2}, LX/1Iu;-><init>()V

    invoke-virtual {v1, v2}, LX/1KS;->a(LX/1Iu;)V

    .line 216497
    move-object/from16 v0, p6

    invoke-virtual {v1, v0}, LX/1KS;->a(Lcom/facebook/api/feedtype/FeedType;)V

    .line 216498
    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, LX/1KS;->a(Lcom/facebook/feed/fragment/NewsFeedFragment;)V

    .line 216499
    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, LX/1KS;->c(LX/1Iu;)V

    .line 216500
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1CZ;->i:LX/1DJ;

    invoke-virtual {v1, v2}, LX/1KS;->a(LX/1DK;)V

    .line 216501
    invoke-virtual {v1, v1}, LX/1KS;->a(LX/1KS;)V

    .line 216502
    invoke-virtual/range {p1 .. p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1KS;->a(Landroid/content/Context;)V

    .line 216503
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1CZ;->i:LX/1DJ;

    invoke-virtual {v1, v2}, LX/1KS;->a(LX/1DJ;)V

    .line 216504
    const-string v2, "native_newsfeed"

    invoke-virtual {v1, v2}, LX/1KS;->a(Ljava/lang/String;)V

    .line 216505
    move-object/from16 v0, p5

    invoke-virtual {v1, v0}, LX/1KS;->b(LX/1Iu;)V

    .line 216506
    invoke-virtual {v1}, LX/1KS;->j()V

    .line 216507
    return-object v1
.end method
