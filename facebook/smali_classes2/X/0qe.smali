.class public LX/0qe;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final b:Ljava/lang/Object;


# instance fields
.field public a:LX/0W3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 148138
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/0qe;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 148139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148140
    iput-object p1, p0, LX/0qe;->a:LX/0W3;

    .line 148141
    return-void
.end method

.method public static a(LX/0QB;)LX/0qe;
    .locals 7

    .prologue
    .line 148142
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 148143
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 148144
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 148145
    if-nez v1, :cond_0

    .line 148146
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148147
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 148148
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 148149
    sget-object v1, LX/0qe;->b:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 148150
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 148151
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 148152
    :cond_1
    if-nez v1, :cond_4

    .line 148153
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 148154
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 148155
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 148156
    new-instance p0, LX/0qe;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v1

    check-cast v1, LX/0W3;

    invoke-direct {p0, v1}, LX/0qe;-><init>(LX/0W3;)V

    .line 148157
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 148158
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 148159
    if-nez v1, :cond_2

    .line 148160
    sget-object v0, LX/0qe;->b:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qe;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 148161
    :goto_1
    if-eqz v0, :cond_3

    .line 148162
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 148163
    :goto_3
    check-cast v0, LX/0qe;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 148164
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 148165
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 148166
    :catchall_1
    move-exception v0

    .line 148167
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 148168
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 148169
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 148170
    :cond_2
    :try_start_8
    sget-object v0, LX/0qe;->b:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qe;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a()J
    .locals 6

    .prologue
    .line 148171
    iget-object v0, p0, LX/0qe;->a:LX/0W3;

    sget-wide v2, LX/0X5;->iA:J

    const-wide/32 v4, 0xdbba0

    invoke-interface {v0, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b()J
    .locals 6

    .prologue
    .line 148172
    iget-object v0, p0, LX/0qe;->a:LX/0W3;

    sget-wide v2, LX/0X5;->iB:J

    const-wide/32 v4, 0xea60

    invoke-interface {v0, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()J
    .locals 6

    .prologue
    .line 148173
    iget-object v0, p0, LX/0qe;->a:LX/0W3;

    sget-wide v2, LX/0X5;->iC:J

    const-wide/32 v4, 0x6ddd00

    invoke-interface {v0, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method
