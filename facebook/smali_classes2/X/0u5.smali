.class public LX/0u5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:J

.field public c:LX/0u4;

.field public final d:LX/0uE;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0u4;J)V
    .locals 1
    .param p2    # LX/0u4;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 155955
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155956
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0u5;->a:Ljava/lang/String;

    .line 155957
    iput-object p2, p0, LX/0u5;->c:LX/0u4;

    .line 155958
    iput-wide p3, p0, LX/0u5;->b:J

    .line 155959
    const/4 v0, 0x0

    iput-object v0, p0, LX/0u5;->d:LX/0uE;

    .line 155960
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0uE;)V
    .locals 2

    .prologue
    .line 155949
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155950
    iput-object p1, p0, LX/0u5;->a:Ljava/lang/String;

    .line 155951
    const/4 v0, 0x0

    iput-object v0, p0, LX/0u5;->c:LX/0u4;

    .line 155952
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0u5;->b:J

    .line 155953
    iput-object p2, p0, LX/0u5;->d:LX/0uE;

    .line 155954
    return-void
.end method

.method public static c(LX/0u5;)Z
    .locals 1

    .prologue
    .line 155907
    iget-object v0, p0, LX/0u5;->c:LX/0u4;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(JLX/8K8;)LX/0uE;
    .locals 11
    .param p3    # LX/8K8;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 155909
    iget-object v0, p0, LX/0u5;->d:LX/0uE;

    if-eqz v0, :cond_0

    .line 155910
    iget-object v0, p0, LX/0u5;->d:LX/0uE;

    .line 155911
    :goto_0
    return-object v0

    .line 155912
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 155913
    iget-wide p1, p0, LX/0u5;->b:J

    .line 155914
    :cond_1
    invoke-static {p0}, LX/0u5;->c(LX/0u5;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 155915
    if-eqz p3, :cond_3

    .line 155916
    iget-object v0, p0, LX/0u5;->a:Ljava/lang/String;

    const/4 v4, 0x0

    .line 155917
    sget-object v3, LX/8K8;->a:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 155918
    if-eqz v3, :cond_2

    iget-object v5, p3, LX/8K8;->b:LX/8Ob;

    if-nez v5, :cond_5

    :cond_2
    move-object v3, v4

    .line 155919
    :goto_1
    move-object v0, v3

    .line 155920
    goto :goto_0

    .line 155921
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 155922
    :cond_4
    iget-object v0, p0, LX/0u5;->c:LX/0u4;

    invoke-interface {v0, p1, p2}, LX/0u4;->a(J)LX/0uE;

    move-result-object v0

    goto :goto_0

    .line 155923
    :cond_5
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    move-object v3, v4

    .line 155924
    goto :goto_1

    .line 155925
    :pswitch_0
    iget-object v3, p3, LX/8K8;->b:LX/8Ob;

    .line 155926
    iget-object v4, v3, LX/8Ob;->E:LX/60x;

    move-object v4, v4

    .line 155927
    new-instance v3, LX/0uE;

    iget v5, v4, LX/60x;->c:I

    iget v4, v4, LX/60x;->b:I

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-direct {v3, v4}, LX/0uE;-><init>(I)V

    goto :goto_1

    .line 155928
    :pswitch_1
    new-instance v3, LX/0uE;

    iget-object v4, p3, LX/8K8;->b:LX/8Ob;

    .line 155929
    iget-object v5, v4, LX/8Ob;->E:LX/60x;

    move-object v4, v5

    .line 155930
    iget v4, v4, LX/60x;->c:I

    invoke-direct {v3, v4}, LX/0uE;-><init>(I)V

    goto :goto_1

    .line 155931
    :pswitch_2
    new-instance v3, LX/0uE;

    iget-object v4, p3, LX/8K8;->b:LX/8Ob;

    .line 155932
    iget-object v5, v4, LX/8Ob;->E:LX/60x;

    move-object v4, v5

    .line 155933
    iget v4, v4, LX/60x;->b:I

    invoke-direct {v3, v4}, LX/0uE;-><init>(I)V

    goto :goto_1

    .line 155934
    :pswitch_3
    new-instance v3, LX/0uE;

    iget-object v4, p3, LX/8K8;->b:LX/8Ob;

    .line 155935
    iget-object v5, v4, LX/8Ob;->E:LX/60x;

    move-object v4, v5

    .line 155936
    iget v4, v4, LX/60x;->e:I

    invoke-direct {v3, v4}, LX/0uE;-><init>(I)V

    goto :goto_1

    .line 155937
    :pswitch_4
    new-instance v3, LX/0uE;

    iget-object v4, p3, LX/8K8;->b:LX/8Ob;

    .line 155938
    iget-object v5, v4, LX/8Ob;->E:LX/60x;

    move-object v4, v5

    .line 155939
    iget v4, v4, LX/60x;->g:I

    invoke-direct {v3, v4}, LX/0uE;-><init>(I)V

    goto :goto_1

    .line 155940
    :pswitch_5
    new-instance v3, LX/0uE;

    iget-object v4, p3, LX/8K8;->b:LX/8Ob;

    .line 155941
    iget-wide v9, v4, LX/8Ob;->m:J

    move-wide v5, v9

    .line 155942
    invoke-direct {v3, v5, v6}, LX/0uE;-><init>(J)V

    goto :goto_1

    .line 155943
    :pswitch_6
    new-instance v3, LX/0uE;

    iget-object v4, p3, LX/8K8;->b:LX/8Ob;

    .line 155944
    iget-object v5, v4, LX/8Ob;->E:LX/60x;

    move-object v4, v5

    .line 155945
    iget-wide v5, v4, LX/60x;->a:J

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    const-wide/16 v7, 0x1

    add-long/2addr v5, v7

    invoke-direct {v3, v5, v6}, LX/0uE;-><init>(J)V

    goto :goto_1

    .line 155946
    :pswitch_7
    new-instance v3, LX/0uE;

    iget-object v4, p3, LX/8K8;->b:LX/8Ob;

    .line 155947
    iget-object v5, v4, LX/8Ob;->n:Ljava/lang/String;

    move-object v4, v5

    .line 155948
    invoke-direct {v3, v4}, LX/0uE;-><init>(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155908
    iget-object v0, p0, LX/0u5;->a:Ljava/lang/String;

    return-object v0
.end method
