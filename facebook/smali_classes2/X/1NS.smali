.class public final LX/1NS;
.super LX/1NT;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/feed/fragment/NewsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/fragment/NewsFeedFragment;)V
    .locals 0

    .prologue
    .line 237403
    iput-object p1, p0, LX/1NS;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-direct {p0}, LX/1NT;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 11

    .prologue
    .line 237404
    check-cast p1, LX/1Ng;

    .line 237405
    iget-object v0, p0, LX/1NS;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v0

    iget-object v1, p1, LX/1Ng;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0fz;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 237406
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 237407
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 237408
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    if-eqz v2, :cond_0

    .line 237409
    iget-object v2, p0, LX/1NS;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v2, v2, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->f:LX/189;

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    iget-object v3, p1, LX/1Ng;->b:Ljava/lang/String;

    .line 237410
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 237411
    invoke-static {v0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    .line 237412
    const/4 v4, 0x0

    move v5, v4

    :goto_1
    if-ge v5, v8, :cond_2

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    .line 237413
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v9

    if-eqz v9, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 237414
    invoke-virtual {v6, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 237415
    :cond_1
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    .line 237416
    :cond_2
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 237417
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    .line 237418
    invoke-static {v0}, LX/4Xp;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)LX/4Xp;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    move-result-object v7

    invoke-static {v7}, LX/4Xr;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;)LX/4Xr;

    move-result-object v7

    .line 237419
    iput-object v4, v7, LX/4Xr;->b:LX/0Px;

    .line 237420
    move-object v4, v7

    .line 237421
    invoke-virtual {v4}, LX/4Xr;->a()Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    move-result-object v4

    .line 237422
    iput-object v4, v6, LX/4Xp;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    .line 237423
    move-object v4, v6

    .line 237424
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->A()LX/0Px;

    move-result-object v6

    invoke-static {v6, v3}, LX/189;->a(LX/0Px;Ljava/lang/String;)LX/0Px;

    move-result-object v6

    .line 237425
    iput-object v6, v4, LX/4Xp;->i:LX/0Px;

    .line 237426
    move-object v4, v4

    .line 237427
    iget-object v6, v2, LX/189;->i:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    .line 237428
    iput-wide v6, v4, LX/4Xp;->e:J

    .line 237429
    move-object v4, v4

    .line 237430
    invoke-virtual {v4}, LX/4Xp;->a()Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    move-result-object v4

    .line 237431
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->I_()I

    move-result v6

    invoke-static {v5, v6}, LX/189;->a(II)I

    move-result v5

    invoke-static {v4, v5}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 237432
    move-object v0, v4

    .line 237433
    iget-object v2, p0, LX/1NS;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v2, v2, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->e:LX/0bH;

    new-instance v3, LX/1Ne;

    invoke-direct {v3, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 237434
    iget-object v2, p0, LX/1NS;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v2, v2, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->e:LX/0bH;

    new-instance v3, LX/1Nf;

    invoke-direct {v3, v0}, LX/1Nf;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    goto/16 :goto_0

    .line 237435
    :cond_3
    return-void
.end method
