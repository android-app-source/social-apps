.class public LX/10n;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/10k;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field public a:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tS;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0fO;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/192;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ArS;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field public h:Z

.field public i:Z

.field public j:J


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 169376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169377
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 169378
    iput-object v0, p0, LX/10n;->b:LX/0Ot;

    .line 169379
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 169380
    iput-object v0, p0, LX/10n;->c:LX/0Ot;

    .line 169381
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 169382
    iput-object v0, p0, LX/10n;->d:LX/0Ot;

    .line 169383
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 169384
    iput-object v0, p0, LX/10n;->e:LX/0Ot;

    .line 169385
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 169386
    iput-object v0, p0, LX/10n;->f:LX/0Ot;

    .line 169387
    return-void
.end method

.method public static a(LX/0QB;)LX/10n;
    .locals 9

    .prologue
    .line 169388
    const-class v1, LX/10n;

    monitor-enter v1

    .line 169389
    :try_start_0
    sget-object v0, LX/10n;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 169390
    sput-object v2, LX/10n;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 169391
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169392
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 169393
    new-instance v3, LX/10n;

    invoke-direct {v3}, LX/10n;-><init>()V

    .line 169394
    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    const/16 v5, 0xa91

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xa86

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xa8a

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2281

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 p0, 0x97

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 169395
    iput-object v4, v3, LX/10n;->a:LX/0SG;

    iput-object v5, v3, LX/10n;->b:LX/0Ot;

    iput-object v6, v3, LX/10n;->c:LX/0Ot;

    iput-object v7, v3, LX/10n;->d:LX/0Ot;

    iput-object v8, v3, LX/10n;->e:LX/0Ot;

    iput-object p0, v3, LX/10n;->f:LX/0Ot;

    .line 169396
    move-object v0, v3

    .line 169397
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 169398
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/10n;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169399
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 169400
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/10n;Z)V
    .locals 4

    .prologue
    const/16 v3, 0x400

    .line 169411
    iget-object v0, p0, LX/10n;->g:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/10n;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 169412
    :cond_0
    :goto_0
    return-void

    .line 169413
    :cond_1
    iget-object v0, p0, LX/10n;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 169414
    if-eqz p1, :cond_2

    .line 169415
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x404

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 169416
    invoke-virtual {v0, v3}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0

    .line 169417
    :cond_2
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 169418
    invoke-virtual {v0, v3}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/ui/drawers/DrawerContentFragment;
    .locals 3

    .prologue
    .line 169401
    const-string v0, "InspirationCameraDivebarFragmentInitializer.getDivebarFragment"

    const v1, 0xa583ec0

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 169402
    iget-object v0, p0, LX/10n;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tS;

    invoke-virtual {v0}, LX/0tS;->a()F

    move-result v0

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 169403
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 169404
    const-string p0, "inspiration_aspect_ratio"

    invoke-virtual {v2, p0, v0}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 169405
    const-string p0, "inspiration_group_session_id"

    invoke-virtual {v2, p0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169406
    new-instance p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;-><init>()V

    .line 169407
    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 169408
    move-object v0, p0

    .line 169409
    const v1, 0x48e7dc26

    invoke-static {v1}, LX/02m;->a(I)V

    .line 169410
    return-object v0
.end method

.method public final a(LX/0ex;LX/0fK;)V
    .locals 1

    .prologue
    .line 169371
    new-instance v0, LX/GgQ;

    invoke-direct {v0, p0}, LX/GgQ;-><init>(LX/10n;)V

    .line 169372
    iput-object v0, p2, LX/0fK;->m:LX/10s;

    .line 169373
    new-instance v0, LX/GgR;

    invoke-direct {v0, p0}, LX/GgR;-><init>(LX/10n;)V

    invoke-virtual {p2, v0}, LX/0fK;->a(LX/GgR;)V

    .line 169374
    new-instance v0, LX/GgS;

    invoke-direct {v0, p0, p2}, LX/GgS;-><init>(LX/10n;LX/0fK;)V

    invoke-interface {p1, v0}, LX/0ex;->a(LX/0T2;)V

    .line 169375
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 169363
    if-eqz p1, :cond_0

    .line 169364
    iget-object v0, p0, LX/10n;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "tap_dive_bar"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    move-result-object v0

    const-string v1, "inspiration_camera_divebar"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0gh;->a(Ljava/lang/String;Z)V

    .line 169365
    :goto_0
    return-void

    .line 169366
    :cond_0
    iget-object v0, p0, LX/10n;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    .line 169367
    iget-object v1, v0, LX/0gh;->w:Ljava/lang/String;

    move-object v0, v1

    .line 169368
    if-nez v0, :cond_1

    .line 169369
    iget-object v0, p0, LX/10n;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "tap_outside"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 169370
    :cond_1
    iget-object v0, p0, LX/10n;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "inspiration_camera_divebar"

    invoke-virtual {v0, v1}, LX/0gh;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(LX/10b;)Z
    .locals 1

    .prologue
    .line 169357
    sget-object v0, LX/10b;->LEFT:LX/10b;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/10n;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fO;

    invoke-virtual {v0}, LX/0fO;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 169362
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 169360
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/10n;->h:Z

    .line 169361
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 169358
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/10n;->i:Z

    .line 169359
    const/4 v0, 0x0

    return v0
.end method
