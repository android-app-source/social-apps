.class public final LX/0zA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final className:Ljava/lang/String;

.field private holderHead:LX/4w1;

.field private holderTail:LX/4w1;

.field private omitNullValues:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 166737
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166738
    new-instance v0, LX/4w1;

    invoke-direct {v0}, LX/4w1;-><init>()V

    iput-object v0, p0, LX/0zA;->holderHead:LX/4w1;

    .line 166739
    iget-object v0, p0, LX/0zA;->holderHead:LX/4w1;

    iput-object v0, p0, LX/0zA;->holderTail:LX/4w1;

    .line 166740
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0zA;->omitNullValues:Z

    .line 166741
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/0zA;->className:Ljava/lang/String;

    .line 166742
    return-void
.end method

.method private addHolder(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 166746
    invoke-static {p0}, LX/0zA;->addHolder(LX/0zA;)LX/4w1;

    move-result-object v1

    .line 166747
    iput-object p2, v1, LX/4w1;->value:Ljava/lang/Object;

    .line 166748
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, LX/4w1;->name:Ljava/lang/String;

    .line 166749
    return-object p0
.end method

.method public static addHolder(LX/0zA;)LX/4w1;
    .locals 2

    .prologue
    .line 166743
    new-instance v0, LX/4w1;

    invoke-direct {v0}, LX/4w1;-><init>()V

    .line 166744
    iget-object v1, p0, LX/0zA;->holderTail:LX/4w1;

    iput-object v0, v1, LX/4w1;->next:LX/4w1;

    iput-object v0, p0, LX/0zA;->holderTail:LX/4w1;

    .line 166745
    return-object v0
.end method


# virtual methods
.method public final add(Ljava/lang/String;F)LX/0zA;
    .locals 1

    .prologue
    .line 166733
    invoke-static {p2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0zA;->addHolder(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/String;I)LX/0zA;
    .locals 1

    .prologue
    .line 166735
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0zA;->addHolder(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/String;J)LX/0zA;
    .locals 2

    .prologue
    .line 166736
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0zA;->addHolder(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 166734
    invoke-direct {p0, p1, p2}, LX/0zA;->addHolder(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/String;Z)LX/0zA;
    .locals 1

    .prologue
    .line 166732
    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0zA;->addHolder(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    return-object v0
.end method

.method public final addValue(Ljava/lang/Object;)LX/0zA;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 166728
    invoke-static {p0}, LX/0zA;->addHolder(LX/0zA;)LX/4w1;

    move-result-object v0

    .line 166729
    iput-object p1, v0, LX/4w1;->value:Ljava/lang/Object;

    .line 166730
    move-object v0, p0

    .line 166731
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 9
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 166710
    iget-boolean v2, p0, LX/0zA;->omitNullValues:Z

    .line 166711
    const-string v1, ""

    .line 166712
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    iget-object v3, p0, LX/0zA;->className:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0x7b

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 166713
    iget-object v0, p0, LX/0zA;->holderHead:LX/4w1;

    iget-object v0, v0, LX/4w1;->next:LX/4w1;

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    .line 166714
    :goto_0
    if-eqz v1, :cond_4

    .line 166715
    iget-object v4, v1, LX/4w1;->value:Ljava/lang/Object;

    .line 166716
    if-eqz v2, :cond_0

    if-eqz v4, :cond_2

    .line 166717
    :cond_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166718
    const-string v0, ", "

    .line 166719
    iget-object v5, v1, LX/4w1;->name:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 166720
    iget-object v5, v1, LX/4w1;->name:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x3d

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 166721
    :cond_1
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->isArray()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 166722
    new-array v5, v7, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    .line 166723
    invoke-static {v5}, Ljava/util/Arrays;->deepToString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 166724
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166725
    :cond_2
    :goto_1
    iget-object v1, v1, LX/4w1;->next:LX/4w1;

    goto :goto_0

    .line 166726
    :cond_3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 166727
    :cond_4
    const/16 v0, 0x7d

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
