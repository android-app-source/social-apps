.class public LX/0zS;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0zS;

.field public static final b:LX/0zS;

.field public static final c:LX/0zS;

.field public static final d:LX/0zS;

.field public static final e:LX/0zS;


# instance fields
.field public final f:Ljava/lang/String;

.field public final g:Z

.field public final h:Z

.field public final i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 167276
    new-instance v0, LX/0zS;

    const-string v1, "FULLY_CACHED"

    invoke-direct {v0, v1, v2, v2, v2}, LX/0zS;-><init>(Ljava/lang/String;ZZZ)V

    sput-object v0, LX/0zS;->a:LX/0zS;

    .line 167277
    new-instance v0, LX/0zS;

    const-string v1, "DISK_CACHE_ONLY"

    invoke-direct {v0, v1, v2, v3, v3}, LX/0zS;-><init>(Ljava/lang/String;ZZZ)V

    sput-object v0, LX/0zS;->b:LX/0zS;

    .line 167278
    new-instance v0, LX/0zS;

    const-string v1, "NETWORK_ONLY"

    invoke-direct {v0, v1, v3, v2, v3}, LX/0zS;-><init>(Ljava/lang/String;ZZZ)V

    sput-object v0, LX/0zS;->c:LX/0zS;

    .line 167279
    new-instance v0, LX/0zS;

    const-string v1, "FETCH_AND_FILL"

    invoke-direct {v0, v1, v3, v2, v2}, LX/0zS;-><init>(Ljava/lang/String;ZZZ)V

    sput-object v0, LX/0zS;->d:LX/0zS;

    .line 167280
    new-instance v0, LX/0zS;

    const-string v1, "PREFETCH_TO_DB"

    invoke-direct {v0, v1, v3, v2, v2}, LX/0zS;-><init>(Ljava/lang/String;ZZZ)V

    sput-object v0, LX/0zS;->e:LX/0zS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ZZZ)V
    .locals 0

    .prologue
    .line 167281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167282
    iput-object p1, p0, LX/0zS;->f:Ljava/lang/String;

    .line 167283
    iput-boolean p2, p0, LX/0zS;->g:Z

    .line 167284
    iput-boolean p3, p0, LX/0zS;->i:Z

    .line 167285
    iput-boolean p4, p0, LX/0zS;->h:Z

    .line 167286
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 167287
    const-class v0, LX/0zS;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "policyName"

    iget-object v2, p0, LX/0zS;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
