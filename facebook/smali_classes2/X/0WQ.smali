.class public LX/0WQ;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static volatile a:Z

.field private static volatile b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 76169
    sget-boolean v0, LX/0WQ;->a:Z

    if-nez v0, :cond_0

    .line 76170
    :try_start_0
    invoke-static {}, LX/0WQ;->b()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 76171
    :goto_0
    sput-object v0, LX/0WQ;->b:Ljava/lang/String;

    .line 76172
    const/4 v0, 0x1

    sput-boolean v0, LX/0WQ;->a:Z

    .line 76173
    :cond_0
    sget-object v0, LX/0WQ;->b:Ljava/lang/String;

    return-object v0

    .line 76174
    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b()Ljava/lang/String;
    .locals 5

    .prologue
    .line 76175
    new-instance v0, Ljava/io/File;

    const-string v1, "/proc/self/cmdline"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 76176
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 76177
    const/16 v0, 0x200

    new-array v0, v0, [B

    .line 76178
    :try_start_0
    invoke-virtual {v1, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v2

    .line 76179
    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 76180
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76181
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    throw v0

    .line 76182
    :cond_0
    :try_start_1
    new-instance v3, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4, v2}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 76183
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    return-object v0
.end method
