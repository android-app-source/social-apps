.class public LX/15V;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final d:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static volatile q:LX/15V;


# instance fields
.field public a:LX/6WJ;

.field private final b:Ljava/lang/String;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/content/SecureContextHelper;

.field public final g:Landroid/content/DialogInterface$OnClickListener;

.field private final h:LX/0Sh;

.field public i:LX/BTo;

.field public j:Landroid/app/Activity;

.field public final k:LX/15W;

.field private final l:LX/0tQ;

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0xX;

.field private final o:LX/15X;

.field private final p:LX/0kb;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 180809
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_DOWNLOAD_STARTED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/15V;->c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 180810
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_DOWNLOAD_NOTIFICATION_FIRED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/15V;->d:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0kb;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0Sh;LX/15W;LX/0tQ;LX/0xX;LX/15X;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0kb;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/15W;",
            "LX/0tQ;",
            "LX/0xX;",
            "LX/15X;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 180796
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180797
    const-string v0, "VIDEOS"

    iput-object v0, p0, LX/15V;->b:Ljava/lang/String;

    .line 180798
    iput-object p1, p0, LX/15V;->m:LX/0Ot;

    .line 180799
    iput-object p2, p0, LX/15V;->p:LX/0kb;

    .line 180800
    iput-object p3, p0, LX/15V;->e:LX/0Or;

    .line 180801
    iput-object p4, p0, LX/15V;->f:Lcom/facebook/content/SecureContextHelper;

    .line 180802
    iput-object p6, p0, LX/15V;->k:LX/15W;

    .line 180803
    new-instance v0, LX/15Y;

    invoke-direct {v0, p0}, LX/15Y;-><init>(LX/15V;)V

    iput-object v0, p0, LX/15V;->g:Landroid/content/DialogInterface$OnClickListener;

    .line 180804
    iput-object p5, p0, LX/15V;->h:LX/0Sh;

    .line 180805
    iput-object p7, p0, LX/15V;->l:LX/0tQ;

    .line 180806
    iput-object p8, p0, LX/15V;->n:LX/0xX;

    .line 180807
    iput-object p9, p0, LX/15V;->o:LX/15X;

    .line 180808
    return-void
.end method

.method public static a(LX/2fs;)J
    .locals 4

    .prologue
    .line 180793
    iget-wide v0, p0, LX/2fs;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 180794
    iget-wide v0, p0, LX/2fs;->b:J

    const-wide/16 v2, 0x64

    mul-long/2addr v0, v2

    iget-wide v2, p0, LX/2fs;->a:J

    div-long/2addr v0, v2

    return-wide v0

    .line 180795
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/15V;
    .locals 13

    .prologue
    .line 180668
    sget-object v0, LX/15V;->q:LX/15V;

    if-nez v0, :cond_1

    .line 180669
    const-class v1, LX/15V;

    monitor-enter v1

    .line 180670
    :try_start_0
    sget-object v0, LX/15V;->q:LX/15V;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 180671
    if-eqz v2, :cond_0

    .line 180672
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 180673
    new-instance v3, LX/15V;

    const/16 v4, 0xc49

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v5

    check-cast v5, LX/0kb;

    const/16 v6, 0xc

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v8

    check-cast v8, LX/0Sh;

    invoke-static {v0}, LX/15W;->b(LX/0QB;)LX/15W;

    move-result-object v9

    check-cast v9, LX/15W;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v10

    check-cast v10, LX/0tQ;

    invoke-static {v0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v11

    check-cast v11, LX/0xX;

    invoke-static {v0}, LX/15X;->a(LX/0QB;)LX/15X;

    move-result-object v12

    check-cast v12, LX/15X;

    invoke-direct/range {v3 .. v12}, LX/15V;-><init>(LX/0Ot;LX/0kb;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0Sh;LX/15W;LX/0tQ;LX/0xX;LX/15X;)V

    .line 180674
    move-object v0, v3

    .line 180675
    sput-object v0, LX/15V;->q:LX/15V;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 180676
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 180677
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 180678
    :cond_1
    sget-object v0, LX/15V;->q:LX/15V;

    return-object v0

    .line 180679
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 180680
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/7Jh;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 180785
    new-instance v0, LX/15i;

    iget-object v1, p0, LX/7Jh;->c:[B

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v4, 0x0

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 180786
    const-string v1, "DownloadVideoUtils.getSavedGrahQLStory"

    invoke-virtual {v0, v1}, LX/15i;->a(Ljava/lang/String;)V

    .line 180787
    sget-object v1, LX/16Z;->a:LX/16Z;

    invoke-virtual {v0, v1}, LX/15i;->a(LX/16a;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 180788
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    iget-object v1, p0, LX/7Jh;->e:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 180789
    iput-object v1, v0, LX/23u;->as:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 180790
    move-object v0, v0

    .line 180791
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 180792
    return-object v0
.end method

.method public static a(J)Ljava/lang/String;
    .locals 4

    .prologue
    .line 180783
    const-wide/16 v0, 0xa

    mul-long/2addr v0, p0

    long-to-double v0, v0

    const-wide v2, 0x412e848000000000L    # 1000000.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    div-double/2addr v0, v2

    .line 180784
    const-string v2, "%.1f MB"

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 180774
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 180775
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 180776
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 180777
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 180778
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v0

    .line 180779
    :goto_0
    return-object v0

    .line 180780
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 180781
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 180782
    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/7Jh;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 180769
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 180770
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Jh;

    .line 180771
    invoke-static {v0}, LX/15V;->a(LX/7Jh;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 180772
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 180773
    :cond_0
    return-object v1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLMedia;)Ljava/util/Map;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 180748
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 180749
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aT()Ljava/lang/String;

    move-result-object v1

    .line 180750
    new-instance v2, LX/0AZ;

    invoke-direct {v2}, LX/0AZ;-><init>()V

    .line 180751
    const/4 v0, 0x0

    .line 180752
    if-eqz v1, :cond_7

    .line 180753
    const/4 v0, 0x0

    invoke-static {v2, v0, v1}, LX/0Gj;->a(LX/0AZ;Ljava/lang/String;Ljava/lang/String;)LX/0AY;

    move-result-object v0

    move-object v5, v0

    .line 180754
    :goto_0
    if-eqz v5, :cond_0

    invoke-virtual {v5}, LX/0AY;->b()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v5, v0}, LX/0AY;->a(I)LX/0Am;

    move-result-object v0

    iget-object v0, v0, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v4

    .line 180755
    :goto_1
    return-object v0

    .line 180756
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, LX/0AY;->a(I)LX/0Am;

    move-result-object v0

    iget-object v2, v0, LX/0Am;->c:Ljava/util/List;

    .line 180757
    const-wide/16 v0, 0x0

    .line 180758
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-wide v2, v0

    :cond_2
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ak;

    .line 180759
    iget-object v1, v0, LX/0Ak;->c:Ljava/util/List;

    .line 180760
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    iget-object v0, v0, LX/0Ah;->c:LX/0AR;

    iget-object v0, v0, LX/0AR;->b:Ljava/lang/String;

    const-string v7, "video/"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 180761
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    .line 180762
    iget-object v7, v0, LX/0Ah;->c:LX/0AR;

    iget-object v7, v7, LX/0AR;->d:Ljava/lang/String;

    iget-object v0, v0, LX/0Ah;->c:LX/0AR;

    iget v0, v0, LX/0AR;->c:I

    int-to-long v8, v0

    iget-wide v10, v5, LX/0AY;->c:J

    mul-long/2addr v8, v10

    const-wide/16 v10, 0x1f40

    div-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v4, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 180763
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    iget-object v0, v0, LX/0Ah;->c:LX/0AR;

    iget-object v0, v0, LX/0AR;->b:Ljava/lang/String;

    const-string v7, "audio/"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 180764
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    iget-object v0, v0, LX/0Ah;->c:LX/0AR;

    iget v0, v0, LX/0AR;->c:I

    int-to-long v0, v0

    iget-wide v2, v5, LX/0AY;->c:J

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x1f40

    div-long v2, v0, v2

    move-wide v0, v2

    :goto_4
    move-wide v2, v0

    .line 180765
    goto :goto_2

    .line 180766
    :cond_4
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 180767
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v6, v2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_5
    move-object v0, v4

    .line 180768
    goto/16 :goto_1

    :cond_6
    move-wide v0, v2

    goto :goto_4

    :cond_7
    move-object v5, v0

    goto/16 :goto_0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/BUL;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/BUL;",
            ")V"
        }
    .end annotation

    .prologue
    .line 180730
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 180731
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 180732
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 180733
    invoke-static {p0}, LX/182;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 180734
    invoke-static {p0}, LX/15V;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v1

    .line 180735
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 180736
    const-string v1, "%s\'s video"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 180737
    :cond_0
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    .line 180738
    :goto_0
    iput-object v2, p1, LX/BUL;->g:Ljava/lang/String;

    .line 180739
    iput-object v1, p1, LX/BUL;->h:Ljava/lang/String;

    .line 180740
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, LX/BUL;->i:Ljava/lang/String;

    .line 180741
    invoke-static {p0}, LX/182;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, LX/BUL;->j:Ljava/lang/String;

    .line 180742
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, LX/BUL;->k:Ljava/lang/String;

    .line 180743
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aq()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, LX/BUL;->l:Ljava/lang/String;

    .line 180744
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 180745
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, LX/BUL;->m:Ljava/lang/String;

    .line 180746
    :cond_1
    return-void

    .line 180747
    :cond_2
    const-string v2, ""

    goto :goto_0
.end method

.method public static c(LX/2fs;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 180725
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 180726
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 180727
    invoke-static {p0}, LX/15V;->a(LX/2fs;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 180728
    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180729
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/15V;)I
    .locals 2

    .prologue
    .line 180721
    sget-object v0, LX/BUC;->b:[I

    iget-object v1, p0, LX/15V;->l:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->m()LX/2qY;

    move-result-object v1

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 180722
    const v0, 0x7f080dba

    :goto_0
    return v0

    .line 180723
    :pswitch_0
    const v0, 0x7f080db9

    goto :goto_0

    .line 180724
    :pswitch_1
    const v0, 0x7f080dba

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static f(LX/15V;)I
    .locals 2

    .prologue
    .line 180717
    sget-object v0, LX/BUC;->b:[I

    iget-object v1, p0, LX/15V;->l:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->m()LX/2qY;

    move-result-object v1

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 180718
    const v0, 0x7f080dbd

    :goto_0
    return v0

    .line 180719
    :pswitch_0
    const v0, 0x7f080dbc

    goto :goto_0

    .line 180720
    :pswitch_1
    const v0, 0x7f080dbd

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static g(LX/15V;)I
    .locals 2

    .prologue
    .line 180713
    sget-object v0, LX/BUC;->b:[I

    iget-object v1, p0, LX/15V;->l:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->m()LX/2qY;

    move-result-object v1

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 180714
    const v0, 0x7f080dc0

    :goto_0
    return v0

    .line 180715
    :pswitch_0
    const v0, 0x7f080dbf

    goto :goto_0

    .line 180716
    :pswitch_1
    const v0, 0x7f080dc1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static h(LX/15V;)I
    .locals 2

    .prologue
    .line 180709
    sget-object v0, LX/BUC;->b:[I

    iget-object v1, p0, LX/15V;->l:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->m()LX/2qY;

    move-result-object v1

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 180710
    const v0, 0x7f080dc3

    :goto_0
    return v0

    .line 180711
    :pswitch_0
    const v0, 0x7f080dc2

    goto :goto_0

    .line 180712
    :pswitch_1
    const v0, 0x7f080dc4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 180707
    iget-object v0, p0, LX/15V;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    iget-object v1, p0, LX/15V;->j:Landroid/app/Activity;

    sget-object v2, LX/0ax;->hJ:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 180708
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;)V
    .locals 3

    .prologue
    .line 180702
    iget-object v0, p0, LX/15V;->j:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 180703
    :goto_0
    return-void

    .line 180704
    :cond_0
    iget-object v0, p0, LX/15V;->n:LX/0xX;

    sget-object v1, LX/1vy;->DOWNLOAD_SECTION:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/15V;->a()Landroid/content/Intent;

    move-result-object v0

    .line 180705
    :goto_1
    iget-object v1, p0, LX/15V;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/15V;->j:Landroid/app/Activity;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 180706
    :cond_1
    invoke-virtual {p0, p1}, LX/15V;->b(Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 180700
    iget-object v0, p0, LX/15V;->k:LX/15W;

    iget-object v1, p0, LX/15V;->j:Landroid/app/Activity;

    sget-object v2, LX/15V;->c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v3, LX/0i1;

    invoke-virtual {v0, v1, v2, v3, p1}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 180701
    return-void
.end method

.method public final b(Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 180695
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/15V;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 180696
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->SAVED_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 180697
    const-string v1, "extra_referer"

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 180698
    const-string v1, "extra_section_name"

    const-string v2, "VIDEOS"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 180699
    return-object v0
.end method

.method public final b(LX/2fs;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 180689
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 180690
    iget-object v1, p0, LX/15V;->o:LX/15X;

    iget-object v2, p0, LX/15V;->j:Landroid/app/Activity;

    invoke-virtual {v1, v2}, LX/15X;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180691
    iget-wide v2, p1, LX/2fs;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 180692
    const-string v1, " \u2022 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180693
    invoke-static {p1}, LX/15V;->c(LX/2fs;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180694
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 180686
    const-class v1, LX/15V;

    monitor-enter v1

    .line 180687
    :try_start_0
    iget-object v0, p0, LX/15V;->h:LX/0Sh;

    new-instance v2, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;

    invoke-direct {v2, p0, p1}, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;-><init>(LX/15V;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v2}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 180688
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 180681
    iget-object v0, p0, LX/15V;->l:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180682
    iget-object v0, p0, LX/15V;->p:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->v()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 180683
    :goto_0
    return v0

    .line 180684
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 180685
    :cond_1
    iget-object v0, p0, LX/15V;->l:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->s()Z

    move-result v0

    goto :goto_0
.end method
