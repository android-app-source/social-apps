.class public LX/1Dy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1DZ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1DZ",
        "<",
        "LX/1EE;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 218982
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1Dy;
    .locals 1

    .prologue
    .line 218983
    new-instance v0, LX/1Dy;

    invoke-direct {v0}, LX/1Dy;-><init>()V

    .line 218984
    move-object v0, v0

    .line 218985
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 218986
    check-cast p1, LX/1EE;

    check-cast p2, LX/1EE;

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 218987
    if-nez p1, :cond_2

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    if-eq p1, p2, :cond_0

    const/4 v2, 0x0

    .line 218988
    iget-object v3, p1, LX/1EE;->a:LX/0Px;

    .line 218989
    iget-object v4, p2, LX/1EE;->a:LX/0Px;

    move-object v4, v4

    .line 218990
    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p1, LX/1EE;->j:I

    .line 218991
    iget v4, p2, LX/1EE;->j:I

    move v4, v4

    .line 218992
    if-ne v3, v4, :cond_3

    iget v3, p1, LX/1EE;->l:I

    .line 218993
    iget v4, p2, LX/1EE;->l:I

    move v4, v4

    .line 218994
    if-ne v3, v4, :cond_3

    iget-boolean v3, p1, LX/1EE;->u:Z

    .line 218995
    iget-boolean v4, p2, LX/1EE;->u:Z

    move v4, v4

    .line 218996
    if-ne v3, v4, :cond_3

    iget v3, p1, LX/1EE;->m:I

    .line 218997
    iget v4, p2, LX/1EE;->m:I

    move v4, v4

    .line 218998
    if-ne v3, v4, :cond_3

    iget v3, p1, LX/1EE;->n:I

    .line 218999
    iget v4, p2, LX/1EE;->n:I

    move v4, v4

    .line 219000
    if-ne v3, v4, :cond_3

    iget v3, p1, LX/1EE;->o:I

    .line 219001
    iget v4, p2, LX/1EE;->o:I

    move v4, v4

    .line 219002
    if-ne v3, v4, :cond_3

    iget v3, p1, LX/1EE;->p:I

    .line 219003
    iget v4, p2, LX/1EE;->p:I

    move v4, v4

    .line 219004
    if-ne v3, v4, :cond_3

    iget-object v3, p1, LX/1EE;->r:Ljava/lang/Integer;

    .line 219005
    iget-object v4, p2, LX/1EE;->r:Ljava/lang/Integer;

    move-object v4, v4

    .line 219006
    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p1, LX/1EE;->f:Ljava/lang/String;

    .line 219007
    iget-object v4, p2, LX/1EE;->f:Ljava/lang/String;

    move-object v4, v4

    .line 219008
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p1, LX/1EE;->g:Ljava/lang/String;

    .line 219009
    iget-object v4, p2, LX/1EE;->g:Ljava/lang/String;

    move-object v4, v4

    .line 219010
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p1, LX/1EE;->h:Ljava/lang/String;

    .line 219011
    iget-object v4, p2, LX/1EE;->h:Ljava/lang/String;

    move-object v4, v4

    .line 219012
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p1, LX/1EE;->i:Ljava/lang/String;

    .line 219013
    iget-object v4, p2, LX/1EE;->i:Ljava/lang/String;

    move-object v4, v4

    .line 219014
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-boolean v3, p1, LX/1EE;->b:Z

    .line 219015
    iget-boolean v4, p2, LX/1EE;->b:Z

    move v4, v4

    .line 219016
    if-ne v3, v4, :cond_3

    iget-boolean v3, p1, LX/1EE;->d:Z

    .line 219017
    iget-boolean v4, p2, LX/1EE;->d:Z

    move v4, v4

    .line 219018
    if-ne v3, v4, :cond_3

    iget-boolean v3, p1, LX/1EE;->c:Z

    .line 219019
    iget-boolean v4, p2, LX/1EE;->c:Z

    move v4, v4

    .line 219020
    if-ne v3, v4, :cond_3

    iget-boolean v3, p1, LX/1EE;->t:Z

    .line 219021
    iget-boolean v4, p2, LX/1EE;->t:Z

    move v4, v4

    .line 219022
    if-eq v3, v4, :cond_4

    .line 219023
    :cond_3
    :goto_1
    move v2, v2

    .line 219024
    if-nez v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 219025
    :cond_4
    iget-object v3, p2, LX/1EE;->s:Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

    .line 219026
    iget-object v4, p1, LX/1EE;->s:Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

    if-nez v4, :cond_5

    if-nez v3, :cond_3

    .line 219027
    :cond_5
    iget-object v4, p1, LX/1EE;->s:Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

    if-eqz v4, :cond_6

    if-eqz v3, :cond_3

    .line 219028
    :cond_6
    iget-object v4, p1, LX/1EE;->s:Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

    if-eqz v4, :cond_7

    if-eqz v3, :cond_7

    .line 219029
    iget-object v4, p1, LX/1EE;->s:Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

    .line 219030
    iget-boolean p0, v4, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->a:Z

    move v4, p0

    .line 219031
    iget-boolean p0, v3, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->a:Z

    move p0, p0

    .line 219032
    if-ne v4, p0, :cond_3

    iget-object v4, p1, LX/1EE;->s:Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

    .line 219033
    iget-boolean p0, v4, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->b:Z

    move v4, p0

    .line 219034
    iget-boolean p0, v3, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->b:Z

    move v3, p0

    .line 219035
    if-ne v4, v3, :cond_3

    .line 219036
    :cond_7
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;J)Z
    .locals 1

    .prologue
    .line 219037
    const/4 v0, 0x1

    return v0
.end method
