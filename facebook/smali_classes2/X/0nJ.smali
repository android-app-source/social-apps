.class public LX/0nJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:[LX/0n7;

.field public static final b:[LX/0nK;

.field public static final c:[LX/0nL;

.field public static final d:[LX/0nM;

.field public static final e:[LX/0nN;

.field private static final serialVersionUID:J = 0x331e912922fbc6b8L


# instance fields
.field public final _abstractTypeResolvers:[LX/0nL;

.field public final _additionalDeserializers:[LX/0n7;

.field public final _additionalKeyDeserializers:[LX/0nN;

.field public final _modifiers:[LX/0nK;

.field public final _valueInstantiators:[LX/0nM;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 135082
    new-array v0, v2, [LX/0n7;

    sput-object v0, LX/0nJ;->a:[LX/0n7;

    .line 135083
    new-array v0, v2, [LX/0nK;

    sput-object v0, LX/0nJ;->b:[LX/0nK;

    .line 135084
    new-array v0, v2, [LX/0nL;

    sput-object v0, LX/0nJ;->c:[LX/0nL;

    .line 135085
    new-array v0, v2, [LX/0nM;

    sput-object v0, LX/0nJ;->d:[LX/0nM;

    .line 135086
    const/4 v0, 0x1

    new-array v0, v0, [LX/0nN;

    new-instance v1, LX/0nO;

    invoke-direct {v1}, LX/0nO;-><init>()V

    aput-object v1, v0, v2

    sput-object v0, LX/0nJ;->e:[LX/0nN;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 135080
    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, LX/0nJ;-><init>([LX/0n7;[LX/0nN;[LX/0nK;[LX/0nL;[LX/0nM;)V

    .line 135081
    return-void
.end method

.method private constructor <init>([LX/0n7;[LX/0nN;[LX/0nK;[LX/0nL;[LX/0nM;)V
    .locals 0

    .prologue
    .line 135073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135074
    if-nez p1, :cond_0

    sget-object p1, LX/0nJ;->a:[LX/0n7;

    :cond_0
    iput-object p1, p0, LX/0nJ;->_additionalDeserializers:[LX/0n7;

    .line 135075
    if-nez p2, :cond_1

    sget-object p2, LX/0nJ;->e:[LX/0nN;

    :cond_1
    iput-object p2, p0, LX/0nJ;->_additionalKeyDeserializers:[LX/0nN;

    .line 135076
    if-nez p3, :cond_2

    sget-object p3, LX/0nJ;->b:[LX/0nK;

    :cond_2
    iput-object p3, p0, LX/0nJ;->_modifiers:[LX/0nK;

    .line 135077
    if-nez p4, :cond_3

    sget-object p4, LX/0nJ;->c:[LX/0nL;

    :cond_3
    iput-object p4, p0, LX/0nJ;->_abstractTypeResolvers:[LX/0nL;

    .line 135078
    if-nez p5, :cond_4

    sget-object p5, LX/0nJ;->d:[LX/0nM;

    :cond_4
    iput-object p5, p0, LX/0nJ;->_valueInstantiators:[LX/0nM;

    .line 135079
    return-void
.end method


# virtual methods
.method public final a(LX/0n7;)LX/0nJ;
    .locals 6

    .prologue
    .line 135069
    if-nez p1, :cond_0

    .line 135070
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can not pass null Deserializers"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135071
    :cond_0
    iget-object v0, p0, LX/0nJ;->_additionalDeserializers:[LX/0n7;

    invoke-static {v0, p1}, LX/0nj;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LX/0n7;

    .line 135072
    new-instance v0, LX/0nJ;

    iget-object v2, p0, LX/0nJ;->_additionalKeyDeserializers:[LX/0nN;

    iget-object v3, p0, LX/0nJ;->_modifiers:[LX/0nK;

    iget-object v4, p0, LX/0nJ;->_abstractTypeResolvers:[LX/0nL;

    iget-object v5, p0, LX/0nJ;->_valueInstantiators:[LX/0nM;

    invoke-direct/range {v0 .. v5}, LX/0nJ;-><init>([LX/0n7;[LX/0nN;[LX/0nK;[LX/0nL;[LX/0nM;)V

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 135068
    iget-object v0, p0, LX/0nJ;->_additionalKeyDeserializers:[LX/0nN;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 135066
    iget-object v0, p0, LX/0nJ;->_modifiers:[LX/0nK;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 135087
    iget-object v0, p0, LX/0nJ;->_abstractTypeResolvers:[LX/0nL;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 135067
    iget-object v0, p0, LX/0nJ;->_valueInstantiators:[LX/0nM;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/0n7;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135065
    iget-object v0, p0, LX/0nJ;->_additionalDeserializers:[LX/0n7;

    invoke-static {v0}, LX/0nj;->b([Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/0nN;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135064
    iget-object v0, p0, LX/0nJ;->_additionalKeyDeserializers:[LX/0nN;

    invoke-static {v0}, LX/0nj;->b([Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/0nK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135063
    iget-object v0, p0, LX/0nJ;->_modifiers:[LX/0nK;

    invoke-static {v0}, LX/0nj;->b([Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/0nL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135062
    iget-object v0, p0, LX/0nJ;->_abstractTypeResolvers:[LX/0nL;

    invoke-static {v0}, LX/0nj;->b([Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/0nM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135061
    iget-object v0, p0, LX/0nJ;->_valueInstantiators:[LX/0nM;

    invoke-static {v0}, LX/0nj;->b([Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method
