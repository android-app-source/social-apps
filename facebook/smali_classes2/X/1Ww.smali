.class public LX/1Ww;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1Ww;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/1Wv;


# direct methods
.method public constructor <init>(LX/0Zb;LX/1Wv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 270190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 270191
    iput-object p1, p0, LX/1Ww;->a:LX/0Zb;

    .line 270192
    iput-object p2, p0, LX/1Ww;->b:LX/1Wv;

    .line 270193
    return-void
.end method

.method public static a(LX/0QB;)LX/1Ww;
    .locals 5

    .prologue
    .line 270194
    sget-object v0, LX/1Ww;->c:LX/1Ww;

    if-nez v0, :cond_1

    .line 270195
    const-class v1, LX/1Ww;

    monitor-enter v1

    .line 270196
    :try_start_0
    sget-object v0, LX/1Ww;->c:LX/1Ww;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 270197
    if-eqz v2, :cond_0

    .line 270198
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 270199
    new-instance p0, LX/1Ww;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/1Wv;->b(LX/0QB;)LX/1Wv;

    move-result-object v4

    check-cast v4, LX/1Wv;

    invoke-direct {p0, v3, v4}, LX/1Ww;-><init>(LX/0Zb;LX/1Wv;)V

    .line 270200
    move-object v0, p0

    .line 270201
    sput-object v0, LX/1Ww;->c:LX/1Ww;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270202
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 270203
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 270204
    :cond_1
    sget-object v0, LX/1Ww;->c:LX/1Ww;

    return-object v0

    .line 270205
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 270206
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
