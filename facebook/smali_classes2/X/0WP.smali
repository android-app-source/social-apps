.class public LX/0WP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field private final b:Ljava/io/File;

.field private final c:I

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/crudolib/prefs/LightSharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Ljava/io/File;I)V
    .locals 1

    .prologue
    .line 76156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76157
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0WP;->d:Ljava/util/Map;

    .line 76158
    iput-object p1, p0, LX/0WP;->a:Ljava/util/concurrent/Executor;

    .line 76159
    iput-object p2, p0, LX/0WP;->b:Ljava/io/File;

    .line 76160
    iput p3, p0, LX/0WP;->c:I

    .line 76161
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)LX/0WS;
    .locals 4

    .prologue
    .line 76162
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0WP;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WS;

    .line 76163
    if-nez v0, :cond_0

    .line 76164
    new-instance v0, LX/0WS;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, LX/0WP;->b:Ljava/io/File;

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iget-object v2, p0, LX/0WP;->a:Ljava/util/concurrent/Executor;

    iget v3, p0, LX/0WP;->c:I

    invoke-direct {v0, v1, v2, v3}, LX/0WS;-><init>(Ljava/io/File;Ljava/util/concurrent/Executor;I)V

    .line 76165
    iget-object v1, p0, LX/0WP;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76166
    :cond_0
    monitor-exit p0

    return-object v0

    .line 76167
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
