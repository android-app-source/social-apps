.class public abstract LX/1Bp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1Bq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 214230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214231
    return-void
.end method


# virtual methods
.method public a(LX/3nA;)LX/1Bx;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 214211
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "no implementation"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(LX/1Bt;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Map;)LX/3nA;
    .locals 2
    .param p1    # LX/1Bt;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Bt;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/3nA;"
        }
    .end annotation

    .prologue
    .line 214229
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "no implementation"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(ZLX/1Bt;Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .param p4    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/1Bt;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 214213
    if-nez p1, :cond_0

    iget-object v0, p0, LX/1Bp;->a:LX/1Bq;

    if-eqz v0, :cond_0

    invoke-virtual {p2, p3}, LX/1Bt;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 214214
    :cond_0
    :goto_0
    return-void

    .line 214215
    :cond_1
    if-eqz p4, :cond_0

    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 214216
    iget-object v0, p0, LX/1Bp;->a:LX/1Bq;

    .line 214217
    iget-object v1, v0, LX/1Bq;->a:LX/1Be;

    .line 214218
    invoke-static {v1}, LX/1Be;->n(LX/1Be;)Z

    move-result p2

    .line 214219
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    .line 214220
    if-eqz p2, :cond_3

    .line 214221
    iget-object p1, v1, LX/1Be;->G:Ljava/util/Map;

    invoke-interface {p1, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/ArrayList;

    .line 214222
    if-nez p1, :cond_2

    .line 214223
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 214224
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214225
    iget-object p0, v1, LX/1Be;->G:Ljava/util/Map;

    invoke-interface {p0, p3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 214226
    :cond_2
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 214227
    :cond_3
    iget-object p1, v1, LX/1Be;->A:Landroid/webkit/CookieManager;

    invoke-virtual {p1, p3, p0}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 214228
    :cond_4
    goto :goto_0
.end method

.method public b(LX/3nA;)V
    .locals 0

    .prologue
    .line 214212
    return-void
.end method
