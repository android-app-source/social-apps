.class public LX/16j;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/16j;


# instance fields
.field public final a:Lcom/facebook/tigon/tigonliger/TigonLigerService;

.field private final b:LX/1hd;

.field private final c:Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;

.field public final d:LX/1hh;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field public final f:LX/0TU;

.field private final g:LX/0Uh;

.field private h:Z

.field public final i:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0TU;Lcom/facebook/tigon/tigonliger/TigonLigerService;LX/1hd;Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;LX/03V;LX/0Uh;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/tigon/httpclientadapter/CallbacksHandlerExecutor;
        .end annotation
    .end param
    .param p2    # LX/0TU;
        .annotation runtime Lcom/facebook/tigon/httpclientadapter/ResponseHandlerThreadPool;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 186382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186383
    new-instance v0, LX/1hh;

    invoke-direct {v0}, LX/1hh;-><init>()V

    iput-object v0, p0, LX/16j;->d:LX/1hh;

    .line 186384
    iput-boolean v1, p0, LX/16j;->h:Z

    .line 186385
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, LX/16j;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 186386
    iput-object p3, p0, LX/16j;->a:Lcom/facebook/tigon/tigonliger/TigonLigerService;

    .line 186387
    iput-object p4, p0, LX/16j;->b:LX/1hd;

    .line 186388
    iput-object p5, p0, LX/16j;->c:Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;

    .line 186389
    iput-object p1, p0, LX/16j;->e:Ljava/util/concurrent/ExecutorService;

    .line 186390
    iput-object p2, p0, LX/16j;->f:LX/0TU;

    .line 186391
    iput-object p7, p0, LX/16j;->g:LX/0Uh;

    .line 186392
    const/16 v0, 0x298

    invoke-virtual {p7, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186393
    sput-object p6, LX/1hi;->e:LX/03V;

    .line 186394
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/16j;
    .locals 11

    .prologue
    .line 186395
    sget-object v0, LX/16j;->j:LX/16j;

    if-nez v0, :cond_1

    .line 186396
    const-class v1, LX/16j;

    monitor-enter v1

    .line 186397
    :try_start_0
    sget-object v0, LX/16j;->j:LX/16j;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 186398
    if-eqz v2, :cond_0

    .line 186399
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 186400
    new-instance v3, LX/16j;

    invoke-static {v0}, LX/16k;->a(LX/0QB;)Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/1AF;->a(LX/0QB;)LX/0TU;

    move-result-object v5

    check-cast v5, LX/0TU;

    invoke-static {v0}, Lcom/facebook/tigon/tigonliger/TigonLigerService;->a(LX/0QB;)Lcom/facebook/tigon/tigonliger/TigonLigerService;

    move-result-object v6

    check-cast v6, Lcom/facebook/tigon/tigonliger/TigonLigerService;

    invoke-static {v0}, LX/1hd;->a(LX/0QB;)LX/1hd;

    move-result-object v7

    check-cast v7, LX/1hd;

    invoke-static {v0}, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->b(LX/0QB;)Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;

    move-result-object v8

    check-cast v8, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-direct/range {v3 .. v10}, LX/16j;-><init>(Ljava/util/concurrent/ExecutorService;LX/0TU;Lcom/facebook/tigon/tigonliger/TigonLigerService;LX/1hd;Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;LX/03V;LX/0Uh;)V

    .line 186401
    move-object v0, v3

    .line 186402
    sput-object v0, LX/16j;->j:LX/16j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186403
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 186404
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 186405
    :cond_1
    sget-object v0, LX/16j;->j:LX/16j;

    return-object v0

    .line 186406
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 186407
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILcom/facebook/http/interfaces/RequestPriority;)V
    .locals 3

    .prologue
    .line 186408
    iget-object v0, p0, LX/16j;->d:LX/1hh;

    invoke-virtual {v0, p1}, LX/1hh;->d(I)LX/1hi;

    move-result-object v0

    .line 186409
    if-nez v0, :cond_1

    .line 186410
    :cond_0
    :goto_0
    return-void

    .line 186411
    :cond_1
    invoke-virtual {v0}, LX/1hi;->g()Lcom/facebook/tigon/tigonapi/TigonRequestToken;

    move-result-object v1

    .line 186412
    if-eqz v1, :cond_0

    .line 186413
    iget-object v2, p0, LX/16j;->c:Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;

    .line 186414
    iget-object p0, v0, LX/1hi;->a:Ljava/lang/String;

    move-object v0, p0

    .line 186415
    invoke-virtual {v2, p2, v0}, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->a(Lcom/facebook/http/interfaces/RequestPriority;Ljava/lang/String;)LX/1iO;

    move-result-object v0

    .line 186416
    invoke-interface {v1, v0}, Lcom/facebook/tigon/tigonapi/TigonRequestToken;->a(LX/1iO;)V

    goto :goto_0
.end method

.method public final a(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;ILorg/apache/http/client/ResponseHandler;Lcom/facebook/http/interfaces/RequestPriority;ZLX/2BD;LX/14P;LX/15F;Ljava/lang/String;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 22
    .param p3    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/2BD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 186417
    sget-boolean v4, LX/19V;->a:Z

    if-eqz v4, :cond_0

    .line 186418
    invoke-interface/range {p1 .. p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    .line 186419
    :cond_0
    const/4 v4, 0x0

    .line 186420
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, LX/16j;->c:Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->a(Lorg/apache/http/HttpRequest;)LX/HcQ;

    move-result-object v19

    .line 186421
    const-string v12, "TigonHttpEntity"

    .line 186422
    if-nez v19, :cond_4

    .line 186423
    invoke-static/range {p1 .. p1}, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->b(Lorg/apache/http/HttpRequest;)LX/1iI;

    move-result-object v5

    .line 186424
    if-nez v5, :cond_2

    const-string v12, "noBuffer"
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186425
    :goto_0
    if-nez v5, :cond_3

    const/4 v4, 0x0

    :goto_1
    move v13, v4

    move-object/from16 v18, v5

    .line 186426
    :goto_2
    new-instance v8, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/16j;->b:LX/1hd;

    move-object/from16 v0, p1

    move-object/from16 v1, p8

    invoke-direct {v8, v4, v0, v1}, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;-><init>(LX/1hd;Lorg/apache/http/client/methods/HttpUriRequest;LX/2BD;)V

    .line 186427
    new-instance v20, LX/1iL;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move/from16 v2, p4

    move-object/from16 v3, p8

    invoke-direct {v0, v1, v2, v3}, LX/1iL;-><init>(LX/16j;ILX/2BD;)V

    .line 186428
    move-object/from16 v0, p0

    iget-object v4, v0, LX/16j;->d:LX/1hh;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/16j;->a:Lcom/facebook/tigon/tigonliger/TigonLigerService;

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/16j;->h:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v5, v0, LX/16j;->g:LX/0Uh;

    const/16 v6, 0x295

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, LX/0Uh;->a(IZ)Z

    move-result v17

    move-object/from16 v5, p2

    move/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v9, p10

    move-object/from16 v10, p13

    move-object/from16 v14, p11

    move-object/from16 v15, p6

    invoke-virtual/range {v4 .. v17}, LX/1hh;->a(Ljava/lang/String;ILorg/apache/http/client/ResponseHandler;Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;LX/15F;Lcom/google/common/util/concurrent/SettableFuture;LX/1AI;Ljava/lang/String;ILjava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;ZZ)LX/1hi;

    move-result-object v21

    .line 186429
    :try_start_1
    move-object/from16 v0, p0

    iget-object v9, v0, LX/16j;->c:Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;

    move/from16 v10, p4

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    move-object/from16 v13, p3

    move-object/from16 v14, p6

    move/from16 v15, p7

    move-object/from16 v16, p9

    move-object/from16 v17, p12

    invoke-virtual/range {v9 .. v17}, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->a(ILorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;ZLX/14P;Ljava/lang/String;)Lcom/facebook/tigon/iface/TigonRequest;

    move-result-object v5

    .line 186430
    move/from16 v0, p4

    invoke-virtual {v8, v5, v0}, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a(Lcom/facebook/tigon/iface/TigonRequest;I)V

    .line 186431
    if-eqz v19, :cond_5

    .line 186432
    move-object/from16 v0, p0

    iget-object v4, v0, LX/16j;->a:Lcom/facebook/tigon/tigonliger/TigonLigerService;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/16j;->e:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v4, v5, v0, v1, v6}, LX/1AH;->a(Lcom/facebook/tigon/iface/TigonRequest;Lcom/facebook/tigon/tigonapi/TigonBodyProvider;Lcom/facebook/tigon/tigonapi/TigonDirectBufferCallbacks;Ljava/util/concurrent/Executor;)Lcom/facebook/tigon/tigonapi/TigonRequestToken;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, LX/1hi;->a(Lcom/facebook/tigon/tigonapi/TigonRequestToken;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 186433
    :cond_1
    :goto_3
    return-void

    .line 186434
    :cond_2
    :try_start_2
    const-string v12, "DirectByteBuffer"

    goto/16 :goto_0

    .line 186435
    :cond_3
    invoke-virtual {v5}, LX/1iI;->b()I

    move-result v4

    goto/16 :goto_1

    .line 186436
    :cond_4
    invoke-virtual/range {v19 .. v19}, LX/HcQ;->a()I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v13

    move-object/from16 v18, v4

    .line 186437
    goto/16 :goto_2

    .line 186438
    :catch_0
    move-exception v4

    .line 186439
    const-string v5, "TigonHttpClientAdapter"

    const-string v6, "Invalid request body from \'%s\' request"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p2, v7, v8

    invoke-static {v5, v4, v6, v7}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 186440
    move-object/from16 v0, p13

    invoke-virtual {v0, v4}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_3

    .line 186441
    :cond_5
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, LX/16j;->a:Lcom/facebook/tigon/tigonliger/TigonLigerService;

    if-nez v18, :cond_6

    const/4 v6, 0x0

    :goto_4
    if-nez v18, :cond_7

    const/4 v7, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v9, v0, LX/16j;->e:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v8, v20

    invoke-interface/range {v4 .. v9}, LX/1AH;->a(Lcom/facebook/tigon/iface/TigonRequest;[Ljava/nio/ByteBuffer;ILcom/facebook/tigon/tigonapi/TigonDirectBufferCallbacks;Ljava/util/concurrent/Executor;)Lcom/facebook/tigon/tigonapi/TigonRequestToken;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, LX/1hi;->a(Lcom/facebook/tigon/tigonapi/TigonRequestToken;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_3

    .line 186442
    :catch_1
    move-exception v4

    .line 186443
    move-object/from16 v0, p13

    invoke-virtual {v0, v4}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 186444
    invoke-virtual/range {v21 .. v21}, LX/1hi;->f()Lcom/facebook/tigon/tigonapi/TigonRequestToken;

    move-result-object v5

    if-nez v5, :cond_1

    .line 186445
    const-string v5, "Failed to send the request"

    const/4 v6, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v4, v6}, LX/1hi;->a(Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 186446
    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-virtual {v0, v1}, LX/16j;->b(I)V

    goto :goto_3

    .line 186447
    :cond_6
    :try_start_4
    invoke-virtual/range {v18 .. v18}, LX/1iI;->a()[Ljava/nio/ByteBuffer;

    move-result-object v6

    goto :goto_4

    :cond_7
    invoke-virtual/range {v18 .. v18}, LX/1iI;->b()I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-result v7

    goto :goto_5
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 186448
    iget-object v0, p0, LX/16j;->d:LX/1hh;

    invoke-virtual {v0, p1}, LX/1hh;->b(I)V

    .line 186449
    return-void
.end method
