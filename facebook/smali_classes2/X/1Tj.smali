.class public LX/1Tj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;
.implements LX/1TG;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 254072
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254073
    iput-object p1, p0, LX/1Tj;->a:LX/0Ot;

    .line 254074
    return-void
.end method

.method public static a(LX/0QB;)LX/1Tj;
    .locals 4

    .prologue
    .line 254079
    const-class v1, LX/1Tj;

    monitor-enter v1

    .line 254080
    :try_start_0
    sget-object v0, LX/1Tj;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 254081
    sput-object v2, LX/1Tj;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 254082
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254083
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 254084
    new-instance v3, LX/1Tj;

    const/16 p0, 0xa0f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1Tj;-><init>(LX/0Ot;)V

    .line 254085
    move-object v0, v3

    .line 254086
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 254087
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Tj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 254088
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 254089
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 3

    .prologue
    .line 254090
    sget-object v0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    sget-object v1, LX/0ja;->a:LX/3AL;

    sget-object v2, LX/0ja;->e:LX/3AM;

    invoke-virtual {p1, v0, v1, v2}, LX/0ja;->a(Ljava/lang/Class;LX/3AL;LX/3AM;)V

    .line 254091
    return-void
.end method

.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 254077
    sget-object v0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 254078
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 254075
    const-class v0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    iget-object v1, p0, LX/1Tj;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 254076
    return-void
.end method
