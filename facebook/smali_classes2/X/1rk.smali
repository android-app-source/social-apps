.class public LX/1rk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;>;"
        }
    .end annotation
.end field

.field private static volatile r:LX/1rk;


# instance fields
.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CSD;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Zb;

.field public final e:LX/0aG;

.field public final f:LX/0SG;

.field public final g:LX/0TD;

.field public final h:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/1rn;

.field public final k:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/03V;

.field private final n:LX/1rU;

.field private final o:LX/1rx;

.field private final p:Landroid/app/NotificationManager;

.field public q:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 332990
    new-instance v0, LX/0S8;

    invoke-direct {v0}, LX/0S8;-><init>()V

    invoke-virtual {v0}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    sput-object v0, LX/1rk;->a:Ljava/util/concurrent/ConcurrentMap;

    .line 332991
    new-instance v0, LX/0S8;

    invoke-direct {v0}, LX/0S8;-><init>()V

    invoke-virtual {v0}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    sput-object v0, LX/1rk;->b:Ljava/util/concurrent/ConcurrentMap;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Zb;LX/0aG;LX/0SG;LX/0TD;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/1rn;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Ot;LX/03V;LX/1rU;LX/1rx;Landroid/app/NotificationManager;)V
    .locals 0
    .param p5    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CSD;",
            ">;",
            "LX/0Zb;",
            "LX/0aG;",
            "LX/0SG;",
            "LX/0TD;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;",
            ">;",
            "LX/1rn;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1rU;",
            "LX/1rx;",
            "Landroid/app/NotificationManager;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 332974
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 332975
    iput-object p1, p0, LX/1rk;->c:LX/0Ot;

    .line 332976
    iput-object p2, p0, LX/1rk;->d:LX/0Zb;

    .line 332977
    iput-object p3, p0, LX/1rk;->e:LX/0aG;

    .line 332978
    iput-object p4, p0, LX/1rk;->f:LX/0SG;

    .line 332979
    iput-object p5, p0, LX/1rk;->g:LX/0TD;

    .line 332980
    iput-object p6, p0, LX/1rk;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 332981
    iput-object p7, p0, LX/1rk;->i:LX/0Ot;

    .line 332982
    iput-object p8, p0, LX/1rk;->j:LX/1rn;

    .line 332983
    iput-object p9, p0, LX/1rk;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 332984
    iput-object p10, p0, LX/1rk;->l:LX/0Ot;

    .line 332985
    iput-object p11, p0, LX/1rk;->m:LX/03V;

    .line 332986
    iput-object p12, p0, LX/1rk;->n:LX/1rU;

    .line 332987
    iput-object p13, p0, LX/1rk;->o:LX/1rx;

    .line 332988
    iput-object p14, p0, LX/1rk;->p:Landroid/app/NotificationManager;

    .line 332989
    return-void
.end method

.method public static a(LX/0QB;)LX/1rk;
    .locals 3

    .prologue
    .line 332964
    sget-object v0, LX/1rk;->r:LX/1rk;

    if-nez v0, :cond_1

    .line 332965
    const-class v1, LX/1rk;

    monitor-enter v1

    .line 332966
    :try_start_0
    sget-object v0, LX/1rk;->r:LX/1rk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 332967
    if-eqz v2, :cond_0

    .line 332968
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1rk;->b(LX/0QB;)LX/1rk;

    move-result-object v0

    sput-object v0, LX/1rk;->r:LX/1rk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 332969
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 332970
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 332971
    :cond_1
    sget-object v0, LX/1rk;->r:LX/1rk;

    return-object v0

    .line 332972
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 332973
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/1rk;Ljava/util/List;)V
    .locals 2
    .param p0    # LX/1rk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 332907
    invoke-static {p1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332908
    iget-object v0, p0, LX/1rk;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CSD;

    const-string v1, "NotificationsSyncHelper"

    invoke-virtual {v0, p1, v1}, LX/CSD;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 332909
    :cond_0
    return-void
.end method

.method private static a(LX/1rk;LX/2nq;)Z
    .locals 2
    .param p0    # LX/1rk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 332963
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1rk;->o:LX/1rx;

    invoke-virtual {v0}, LX/1rx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/1rk;LX/2ub;LX/3DO;Z)V
    .locals 3

    .prologue
    .line 332957
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "notification_sync"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "notifications"

    .line 332958
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 332959
    move-object v0, v0

    .line 332960
    const-string v1, "syncSource"

    iget-object v2, p1, LX/2ub;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "syncType"

    invoke-virtual {p2}, LX/3DO;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "syncSuccess"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 332961
    iget-object v1, p0, LX/1rk;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 332962
    return-void
.end method

.method public static a$redex0(LX/1rk;Lcom/facebook/fbservice/service/OperationResult;Lcom/facebook/auth/viewercontext/ViewerContext;LX/3DO;LX/2ub;Lcom/facebook/common/callercontext/CallerContext;ILjava/lang/String;Ljava/util/List;)V
    .locals 9
    .param p4    # LX/2ub;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/fbservice/service/OperationResult;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/3DO;",
            "LX/2ub;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 332939
    iget-object v0, p0, LX/1rk;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 332940
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;

    .line 332941
    if-nez v0, :cond_1

    .line 332942
    :cond_0
    :goto_0
    return-void

    .line 332943
    :cond_1
    iget-object v1, v0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    if-nez v1, :cond_2

    .line 332944
    iget-object v0, p0, LX/1rk;->m:LX/03V;

    const-string v1, "NotificationsSyncHelper_syncNotifications"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "notificationStories was null, syncType: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", syncSource: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", syncTime: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 332945
    :cond_2
    iget-object v1, v0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    invoke-virtual {v1}, LX/3T7;->a()LX/0Px;

    move-result-object v1

    .line 332946
    invoke-static {p0, v1}, LX/1rk;->a(LX/1rk;Ljava/util/List;)V

    .line 332947
    invoke-static {v1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 332948
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2nq;

    .line 332949
    invoke-static {p0, v1}, LX/1rk;->a(LX/1rk;LX/2nq;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 332950
    iget-object v5, p0, LX/1rk;->p:Landroid/app/NotificationManager;

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x0

    invoke-virtual {v5, v1, v6}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    goto :goto_1

    .line 332951
    :cond_4
    iget-object v1, v0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    invoke-virtual {v1}, LX/3T7;->d()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    if-nez v1, :cond_5

    .line 332952
    iget-object v0, p0, LX/1rk;->m:LX/03V;

    const-string v1, "NotificationsSyncHelper_syncNotifications"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "pageInfo was null, syncType: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", syncSource: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", syncTime: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 332953
    :cond_5
    iget-object v1, v0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    invoke-virtual {v1}, LX/3T7;->d()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPageInfo;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 332954
    iget-object v1, v0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    invoke-virtual {v1}, LX/3T7;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v0, v0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    invoke-virtual {v0}, LX/3T7;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 332955
    :cond_6
    iget-object v0, p0, LX/1rk;->m:LX/03V;

    const-string v1, "NotificationsSyncHelper_syncNotifications"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "hasPreviousPage was true but newStories was empty, syncType "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", syncSource: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", syncTime: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 332956
    :cond_7
    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    invoke-virtual/range {v0 .. v8}, LX/1rk;->a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/3DO;LX/2ub;Lcom/facebook/common/callercontext/CallerContext;ILjava/lang/String;Ljava/util/List;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    goto/16 :goto_0
.end method

.method private static b(LX/0QB;)LX/1rk;
    .locals 15

    .prologue
    .line 332937
    new-instance v0, LX/1rk;

    const/16 v1, 0x2b04

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, LX/0TD;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v7, 0xe5e

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/1rn;->a(LX/0QB;)LX/1rn;

    move-result-object v8

    check-cast v8, LX/1rn;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v9

    check-cast v9, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v10, 0x4cd

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-static {p0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v12

    check-cast v12, LX/1rU;

    invoke-static {p0}, LX/1rx;->b(LX/0QB;)LX/1rx;

    move-result-object v13

    check-cast v13, LX/1rx;

    invoke-static {p0}, LX/1s4;->b(LX/0QB;)Landroid/app/NotificationManager;

    move-result-object v14

    check-cast v14, Landroid/app/NotificationManager;

    invoke-direct/range {v0 .. v14}, LX/1rk;-><init>(LX/0Ot;LX/0Zb;LX/0aG;LX/0SG;LX/0TD;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/1rn;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Ot;LX/03V;LX/1rU;LX/1rx;Landroid/app/NotificationManager;)V

    .line 332938
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/3DO;LX/2ub;Lcom/facebook/common/callercontext/CallerContext;ILjava/lang/String;Ljava/util/List;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 13
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/3DO;",
            "LX/2ub;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 332918
    invoke-virtual {p1}, Lcom/facebook/auth/viewercontext/ViewerContext;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 332919
    sget-object v2, LX/1rk;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 332920
    if-eqz v2, :cond_0

    .line 332921
    :goto_0
    return-object v2

    .line 332922
    :cond_0
    iget-object v12, p0, LX/1rk;->g:LX/0TD;

    new-instance v2, Lcom/facebook/notifications/sync/NotificationsSyncHelper$1;

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object/from16 v6, p3

    move/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v2 .. v9}, Lcom/facebook/notifications/sync/NotificationsSyncHelper$1;-><init>(LX/1rk;Lcom/facebook/auth/viewercontext/ViewerContext;LX/3DO;LX/2ub;ILjava/lang/String;Ljava/util/List;)V

    invoke-interface {v12, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 332923
    new-instance v3, LX/3Ef;

    move/from16 v0, p8

    move-object/from16 v1, p4

    invoke-direct {v3, p0, v0, v1}, LX/3Ef;-><init>(LX/1rk;ZLcom/facebook/common/callercontext/CallerContext;)V

    .line 332924
    iget-object v4, p0, LX/1rk;->g:LX/0TD;

    invoke-static {v2, v3, v4}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v12

    .line 332925
    sget-object v2, LX/1rk;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v12}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 332926
    new-instance v2, LX/3Eg;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-direct {v2, p0, v3, p2, v0}, LX/3Eg;-><init>(LX/1rk;Ljava/lang/Long;LX/3DO;LX/2ub;)V

    iget-object v3, p0, LX/1rk;->g:LX/0TD;

    invoke-static {v12, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 332927
    new-instance v2, LX/3Ei;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v3, p0

    move-object v5, p1

    move-object v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    invoke-direct/range {v2 .. v11}, LX/3Ei;-><init>(LX/1rk;Ljava/lang/Long;Lcom/facebook/auth/viewercontext/ViewerContext;LX/3DO;LX/2ub;Lcom/facebook/common/callercontext/CallerContext;ILjava/lang/String;Ljava/util/List;)V

    iget-object v3, p0, LX/1rk;->g:LX/0TD;

    invoke-static {v12, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 332928
    :cond_1
    sget-object v2, LX/3DO;->FULL:LX/3DO;

    if-ne v2, p2, :cond_2

    const v2, 0x350002

    .line 332929
    :goto_1
    iget-object v3, p0, LX/1rk;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v3, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 332930
    iget-object v3, p0, LX/1rk;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-object/from16 v0, p3

    iget-object v4, v0, LX/2ub;->name:Ljava/lang/String;

    invoke-interface {v3, v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 332931
    iget-object v4, p0, LX/1rk;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz p8, :cond_3

    const-string v3, "recursive_fetch"

    :goto_2
    invoke-interface {v4, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 332932
    iget-object v4, p0, LX/1rk;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v3, p0, LX/1rk;->n:LX/1rU;

    invoke-virtual {v3}, LX/1rU;->c()Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "connection_controller"

    :goto_3
    invoke-interface {v4, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    move-object v2, v12

    .line 332933
    goto/16 :goto_0

    .line 332934
    :cond_2
    const v2, 0x350003

    goto :goto_1

    .line 332935
    :cond_3
    const-string v3, "not_recursive_fetch"

    goto :goto_2

    .line 332936
    :cond_4
    const-string v3, "not_connection_controller"

    goto :goto_3
.end method

.method public final a(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 332910
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 332911
    const-string v1, "fetchGraphQLNotificationsParams"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 332912
    iget-object v1, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v1, v1

    .line 332913
    if-eqz v1, :cond_0

    .line 332914
    const-string v1, "overridden_viewer_context"

    .line 332915
    iget-object v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v2, v2

    .line 332916
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 332917
    :cond_0
    iget-object v1, p0, LX/1rk;->e:LX/0aG;

    const-string v2, "fetch_gql_notifications"

    const v3, -0x54f9ae10

    invoke-static {v1, v2, v0, p2, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v0

    return-object v0
.end method
