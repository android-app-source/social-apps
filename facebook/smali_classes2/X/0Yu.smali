.class public LX/0Yu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0VI;
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/0Yu;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0cb;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3yJ;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ae;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0ae;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0cb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3yJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 82738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82739
    iput-object p1, p0, LX/0Yu;->c:LX/0Ot;

    .line 82740
    iput-object p2, p0, LX/0Yu;->a:LX/0Ot;

    .line 82741
    iput-object p3, p0, LX/0Yu;->b:LX/0Ot;

    .line 82742
    return-void
.end method

.method public static a(LX/0QB;)LX/0Yu;
    .locals 6

    .prologue
    .line 82712
    sget-object v0, LX/0Yu;->e:LX/0Yu;

    if-nez v0, :cond_1

    .line 82713
    const-class v1, LX/0Yu;

    monitor-enter v1

    .line 82714
    :try_start_0
    sget-object v0, LX/0Yu;->e:LX/0Yu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 82715
    if-eqz v2, :cond_0

    .line 82716
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 82717
    new-instance v3, LX/0Yu;

    const/16 v4, 0x1032

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x48

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x1664

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, LX/0Yu;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 82718
    move-object v0, v3

    .line 82719
    sput-object v0, LX/0Yu;->e:LX/0Yu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82720
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 82721
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 82722
    :cond_1
    sget-object v0, LX/0Yu;->e:LX/0Yu;

    return-object v0

    .line 82723
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 82724
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82737
    const-string v0, "active_quick_experiments"

    return-object v0
.end method

.method public final a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82743
    iget-object v0, p0, LX/0Yu;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 82725
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 82726
    iget-object v0, p0, LX/0Yu;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3yJ;

    invoke-virtual {v0}, LX/3yJ;->a()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 82727
    iget-object v1, p0, LX/0Yu;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0cb;

    invoke-interface {v1, v0}, LX/0cb;->b(Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v1

    .line 82728
    iget-boolean v4, v1, LX/2Wx;->c:Z

    move v4, v4

    .line 82729
    if-eqz v4, :cond_0

    .line 82730
    iget-object v4, v1, LX/2Wx;->e:Ljava/lang/String;

    move-object v1, v4

    .line 82731
    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 82732
    :cond_1
    iget-object v0, p0, LX/0Yu;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ae;

    invoke-interface {v0}, LX/0ae;->d()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 82733
    iget-object v1, p0, LX/0Yu;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ae;

    sget-object v4, LX/0oc;->EFFECTIVE:LX/0oc;

    invoke-interface {v1, v4, v0}, LX/0ae;->a(LX/0oc;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 82734
    iget-object v1, p0, LX/0Yu;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ae;

    sget-object v4, LX/0oc;->EFFECTIVE:LX/0oc;

    invoke-interface {v1, v4, v0}, LX/0ae;->b(LX/0oc;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 82735
    :cond_3
    const-string v0, "\n"

    invoke-static {v0}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, LX/0PO;->withKeyValueSeparator(Ljava/lang/String;)LX/0PQ;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0PQ;->join(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0Yu;->d:Ljava/lang/String;

    .line 82736
    return-void
.end method

.method public final init()V
    .locals 1

    .prologue
    .line 82710
    iget-object v0, p0, LX/0Yu;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0cb;

    invoke-interface {v0, p0}, LX/0cb;->a(LX/0Yu;)V

    .line 82711
    return-void
.end method
