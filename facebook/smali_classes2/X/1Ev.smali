.class public LX/1Ev;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1Ez;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1Ez;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 221431
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1Ez;
    .locals 5

    .prologue
    .line 221432
    sget-object v0, LX/1Ev;->a:LX/1Ez;

    if-nez v0, :cond_1

    .line 221433
    const-class v1, LX/1Ev;

    monitor-enter v1

    .line 221434
    :try_start_0
    sget-object v0, LX/1Ev;->a:LX/1Ez;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 221435
    if-eqz v2, :cond_0

    .line 221436
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 221437
    invoke-static {v0}, LX/0Ws;->a(LX/0QB;)LX/2WA;

    move-result-object v3

    check-cast v3, LX/2WA;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/1Ew;->a(LX/0QB;)LX/1Ex;

    move-result-object p0

    check-cast p0, LX/1Ex;

    invoke-static {v3, v4, p0}, LX/1Aq;->a(LX/2WA;LX/0SG;LX/1Ex;)LX/1Ez;

    move-result-object v3

    move-object v0, v3

    .line 221438
    sput-object v0, LX/1Ev;->a:LX/1Ez;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 221439
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 221440
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 221441
    :cond_1
    sget-object v0, LX/1Ev;->a:LX/1Ez;

    return-object v0

    .line 221442
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 221443
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 221444
    invoke-static {p0}, LX/0Ws;->a(LX/0QB;)LX/2WA;

    move-result-object v0

    check-cast v0, LX/2WA;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/1Ew;->a(LX/0QB;)LX/1Ex;

    move-result-object v2

    check-cast v2, LX/1Ex;

    invoke-static {v0, v1, v2}, LX/1Aq;->a(LX/2WA;LX/0SG;LX/1Ex;)LX/1Ez;

    move-result-object v0

    return-object v0
.end method
