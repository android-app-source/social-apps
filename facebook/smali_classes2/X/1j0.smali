.class public final LX/1j0;
.super LX/0eW;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadArgumentPlacement",
        "BadClosingBracePlacement",
        "YodaConditions"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 299681
    invoke-direct {p0}, LX/0eW;-><init>()V

    return-void
.end method

.method private static a(LX/0eX;)I
    .locals 1

    .prologue
    .line 299682
    invoke-virtual {p0}, LX/0eX;->c()I

    move-result v0

    .line 299683
    return v0
.end method

.method public static a(LX/0eX;IIIIIIIIIIIIIII)I
    .locals 2

    .prologue
    .line 299684
    const/16 v1, 0xf

    invoke-virtual {p0, v1}, LX/0eX;->b(I)V

    .line 299685
    move/from16 v0, p15

    invoke-static {p0, v0}, LX/1j0;->q(LX/0eX;I)V

    .line 299686
    move/from16 v0, p14

    invoke-static {p0, v0}, LX/1j0;->p(LX/0eX;I)V

    .line 299687
    invoke-static {p0, p13}, LX/1j0;->o(LX/0eX;I)V

    .line 299688
    invoke-static {p0, p12}, LX/1j0;->n(LX/0eX;I)V

    .line 299689
    invoke-static {p0, p11}, LX/1j0;->m(LX/0eX;I)V

    .line 299690
    invoke-static {p0, p10}, LX/1j0;->l(LX/0eX;I)V

    .line 299691
    invoke-static {p0, p9}, LX/1j0;->k(LX/0eX;I)V

    .line 299692
    invoke-static {p0, p8}, LX/1j0;->j(LX/0eX;I)V

    .line 299693
    invoke-static {p0, p7}, LX/1j0;->i(LX/0eX;I)V

    .line 299694
    invoke-static {p0, p6}, LX/1j0;->h(LX/0eX;I)V

    .line 299695
    invoke-static {p0, p3}, LX/1j0;->e(LX/0eX;I)V

    .line 299696
    invoke-static {p0, p2}, LX/1j0;->d(LX/0eX;I)V

    .line 299697
    invoke-static {p0, p1}, LX/1j0;->c(LX/0eX;I)V

    .line 299698
    invoke-static {p0, p5}, LX/1j0;->g(LX/0eX;I)V

    .line 299699
    invoke-static {p0, p4}, LX/1j0;->f(LX/0eX;I)V

    .line 299700
    invoke-static {p0}, LX/1j0;->a(LX/0eX;)I

    move-result v1

    return v1
.end method

.method public static a(LX/0eX;I)V
    .locals 1

    .prologue
    const/4 v0, 0x4

    .line 299701
    invoke-virtual {p0, v0, p1, v0}, LX/0eX;->a(III)V

    return-void
.end method

.method public static b(LX/0eX;I)V
    .locals 0

    .prologue
    .line 299702
    invoke-virtual {p0, p1}, LX/0eX;->c(I)V

    return-void
.end method

.method private static c(LX/0eX;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 299703
    invoke-virtual {p0, v0, p1, v0}, LX/0eX;->c(III)V

    return-void
.end method

.method private static d(LX/0eX;I)V
    .locals 2

    .prologue
    .line 299673
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static e(LX/0eX;I)V
    .locals 2

    .prologue
    .line 299704
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static f(LX/0eX;I)V
    .locals 3

    .prologue
    .line 299705
    const/4 v0, 0x3

    const v1, 0xffff

    and-int/2addr v1, p1

    int-to-short v1, v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, LX/0eX;->a(ISI)V

    return-void
.end method

.method private static g(LX/0eX;I)V
    .locals 3

    .prologue
    .line 299680
    const/4 v0, 0x4

    const v1, 0xffff

    and-int/2addr v1, p1

    int-to-short v1, v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, LX/0eX;->a(ISI)V

    return-void
.end method

.method private static h(LX/0eX;I)V
    .locals 2

    .prologue
    .line 299706
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static i(LX/0eX;I)V
    .locals 2

    .prologue
    .line 299669
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static j(LX/0eX;I)V
    .locals 2

    .prologue
    .line 299670
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static k(LX/0eX;I)V
    .locals 2

    .prologue
    .line 299671
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static l(LX/0eX;I)V
    .locals 2

    .prologue
    .line 299672
    const/16 v0, 0x9

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static m(LX/0eX;I)V
    .locals 2

    .prologue
    .line 299674
    const/16 v0, 0xa

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static n(LX/0eX;I)V
    .locals 2

    .prologue
    .line 299675
    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static o(LX/0eX;I)V
    .locals 2

    .prologue
    .line 299676
    const/16 v0, 0xc

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static p(LX/0eX;I)V
    .locals 2

    .prologue
    .line 299677
    const/16 v0, 0xd

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static q(LX/0eX;I)V
    .locals 2

    .prologue
    .line 299678
    const/16 v0, 0xe

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method


# virtual methods
.method public final f(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 299679
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v0}, LX/0eW;->e(I)I

    move-result v0

    mul-int/lit8 v2, p1, 0x4

    add-int/2addr v0, v2

    invoke-static {v1, v0}, LX/1jQ;->a(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
