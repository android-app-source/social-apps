.class public LX/1VS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 260301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 260302
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 260291
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->B()LX/0Px;

    move-result-object v0

    .line 260292
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    .line 260293
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 260294
    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260300
    invoke-static {p0}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1VX;->a(Ljava/util/List;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;"
        }
    .end annotation

    .prologue
    .line 260299
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 260303
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->I()LX/0Px;

    move-result-object v0

    .line 260304
    if-eqz v0, :cond_0

    .line 260305
    :goto_0
    return-object v0

    .line 260306
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 260307
    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 1

    .prologue
    .line 260298
    invoke-static {p0}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/1VS;->a(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    return-object v0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 1

    .prologue
    .line 260297
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->I()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/1VS;->a(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    return-object v0
.end method

.method public static e(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 1

    .prologue
    .line 260295
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 260296
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
