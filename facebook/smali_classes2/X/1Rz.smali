.class public final LX/1Rz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1Ry;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1Ry;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 247288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247289
    iput-object p1, p0, LX/1Rz;->a:LX/0QB;

    .line 247290
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 247291
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/1Rz;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 247292
    packed-switch p2, :pswitch_data_0

    .line 247293
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 247294
    :pswitch_0
    invoke-static {p1}, LX/1kE;->b(LX/0QB;)LX/1kE;

    move-result-object v0

    .line 247295
    :goto_0
    return-object v0

    .line 247296
    :pswitch_1
    new-instance v2, LX/1kL;

    const/16 v3, 0x22c0

    invoke-static {p1, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x22c1

    invoke-static {p1, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x22cb

    invoke-static {p1, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {p1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {p1}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    invoke-static {p1}, LX/0T9;->a(LX/0QB;)Ljava/util/concurrent/Executor;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/Executor;

    invoke-static {p1}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v10

    check-cast v10, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct/range {v2 .. v10}, LX/1kL;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/0SG;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 247297
    move-object v0, v2

    .line 247298
    goto :goto_0

    .line 247299
    :pswitch_2
    invoke-static {p1}, LX/1kP;->c(LX/0QB;)LX/1kP;

    move-result-object v0

    goto :goto_0

    .line 247300
    :pswitch_3
    invoke-static {p1}, LX/1kQ;->b(LX/0QB;)LX/1kQ;

    move-result-object v0

    goto :goto_0

    .line 247301
    :pswitch_4
    new-instance v3, LX/1kX;

    invoke-direct {v3}, LX/1kX;-><init>()V

    .line 247302
    invoke-static {p1}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v0

    check-cast v0, LX/0TD;

    .line 247303
    new-instance v1, LX/1kY;

    invoke-direct {v1}, LX/1kY;-><init>()V

    .line 247304
    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/1kG;->a(LX/0QB;)LX/1kG;

    move-result-object v4

    check-cast v4, LX/1kG;

    invoke-static {p1}, LX/1kH;->a(LX/0QB;)LX/1kH;

    move-result-object v5

    check-cast v5, LX/1kH;

    invoke-static {p1}, LX/1kD;->a(LX/0QB;)LX/1kD;

    move-result-object v6

    check-cast v6, LX/1kD;

    invoke-static {p1}, LX/1kZ;->a(LX/0QB;)LX/1kZ;

    move-result-object v7

    check-cast v7, LX/1kZ;

    invoke-static {p1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {p1}, LX/1kb;->a(LX/0QB;)LX/1kb;

    move-result-object v9

    check-cast v9, LX/1kb;

    .line 247305
    iput-object v2, v1, LX/1kY;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v4, v1, LX/1kY;->b:LX/1kG;

    iput-object v5, v1, LX/1kY;->c:LX/1kH;

    iput-object v6, v1, LX/1kY;->d:LX/1kD;

    iput-object v7, v1, LX/1kY;->e:LX/1kZ;

    iput-object v8, v1, LX/1kY;->f:LX/0SG;

    iput-object v9, v1, LX/1kY;->g:LX/1kb;

    .line 247306
    move-object v1, v1

    .line 247307
    check-cast v1, LX/1kY;

    invoke-static {p1}, LX/1kZ;->a(LX/0QB;)LX/1kZ;

    move-result-object v2

    check-cast v2, LX/1kZ;

    .line 247308
    iput-object v0, v3, LX/1kX;->a:LX/0TD;

    iput-object v1, v3, LX/1kX;->b:LX/1kY;

    iput-object v2, v3, LX/1kX;->c:LX/1kZ;

    .line 247309
    move-object v0, v3

    .line 247310
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 247311
    const/4 v0, 0x5

    return v0
.end method
