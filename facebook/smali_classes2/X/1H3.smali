.class public LX/1H3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Or",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 226355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226356
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Or;

    iput-object v0, p0, LX/1H3;->a:LX/0Or;

    .line 226357
    return-void
.end method


# virtual methods
.method public final declared-synchronized get()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 226350
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1H3;->a:LX/0Or;

    if-eqz v0, :cond_0

    .line 226351
    iget-object v0, p0, LX/1H3;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/1H3;->b:Ljava/lang/Object;

    .line 226352
    const/4 v0, 0x0

    iput-object v0, p0, LX/1H3;->a:LX/0Or;

    .line 226353
    :cond_0
    iget-object v0, p0, LX/1H3;->b:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 226354
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
