.class public LX/0kM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0kM;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/03V;


# direct methods
.method public constructor <init>(LX/0Zb;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 127062
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127063
    iput-object p1, p0, LX/0kM;->a:LX/0Zb;

    .line 127064
    iput-object p2, p0, LX/0kM;->b:LX/03V;

    .line 127065
    return-void
.end method

.method public static a(LX/0QB;)LX/0kM;
    .locals 5

    .prologue
    .line 127066
    sget-object v0, LX/0kM;->c:LX/0kM;

    if-nez v0, :cond_1

    .line 127067
    const-class v1, LX/0kM;

    monitor-enter v1

    .line 127068
    :try_start_0
    sget-object v0, LX/0kM;->c:LX/0kM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 127069
    if-eqz v2, :cond_0

    .line 127070
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 127071
    new-instance p0, LX/0kM;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, LX/0kM;-><init>(LX/0Zb;LX/03V;)V

    .line 127072
    move-object v0, p0

    .line 127073
    sput-object v0, LX/0kM;->c:LX/0kM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127074
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 127075
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 127076
    :cond_1
    sget-object v0, LX/0kM;->c:LX/0kM;

    return-object v0

    .line 127077
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 127078
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 127079
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "error"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 127080
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 127081
    const-string v2, "message"

    invoke-virtual {v0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 127082
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 127083
    iput-object v2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 127084
    if-eqz p3, :cond_0

    .line 127085
    iput-object p3, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 127086
    :cond_0
    if-eqz p2, :cond_1

    .line 127087
    iput-object p2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    .line 127088
    :cond_1
    if-eqz p4, :cond_2

    .line 127089
    iput-object p4, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 127090
    :cond_2
    iget-object v2, p0, LX/0kM;->a:LX/0Zb;

    invoke-interface {v2, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 127091
    if-eqz p5, :cond_3

    .line 127092
    iget-object v0, p0, LX/0kM;->b:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":toast"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 127093
    :cond_3
    return-void
.end method
