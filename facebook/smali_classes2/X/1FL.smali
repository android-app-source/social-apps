.class public LX/1FL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "Ljava/io/FileInputStream;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/1lW;

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field private h:I

.field public i:LX/1bh;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1FJ;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 222236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222237
    sget-object v0, LX/1lW;->a:LX/1lW;

    iput-object v0, p0, LX/1FL;->c:LX/1lW;

    .line 222238
    iput v1, p0, LX/1FL;->d:I

    .line 222239
    iput v1, p0, LX/1FL;->e:I

    .line 222240
    iput v1, p0, LX/1FL;->f:I

    .line 222241
    const/4 v0, 0x1

    iput v0, p0, LX/1FL;->g:I

    .line 222242
    iput v1, p0, LX/1FL;->h:I

    .line 222243
    invoke-static {p1}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v0

    invoke-static {v0}, LX/03g;->a(Z)V

    .line 222244
    invoke-virtual {p1}, LX/1FJ;->b()LX/1FJ;

    move-result-object v0

    iput-object v0, p0, LX/1FL;->a:LX/1FJ;

    .line 222245
    const/4 v0, 0x0

    iput-object v0, p0, LX/1FL;->b:LX/1Gd;

    .line 222246
    return-void
.end method

.method private constructor <init>(LX/1Gd;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Gd",
            "<",
            "Ljava/io/FileInputStream;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 222247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222248
    sget-object v0, LX/1lW;->a:LX/1lW;

    iput-object v0, p0, LX/1FL;->c:LX/1lW;

    .line 222249
    iput v1, p0, LX/1FL;->d:I

    .line 222250
    iput v1, p0, LX/1FL;->e:I

    .line 222251
    iput v1, p0, LX/1FL;->f:I

    .line 222252
    const/4 v0, 0x1

    iput v0, p0, LX/1FL;->g:I

    .line 222253
    iput v1, p0, LX/1FL;->h:I

    .line 222254
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 222255
    const/4 v0, 0x0

    iput-object v0, p0, LX/1FL;->a:LX/1FJ;

    .line 222256
    iput-object p1, p0, LX/1FL;->b:LX/1Gd;

    .line 222257
    return-void
.end method

.method public constructor <init>(LX/1Gd;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Gd",
            "<",
            "Ljava/io/FileInputStream;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 222258
    invoke-direct {p0, p1}, LX/1FL;-><init>(LX/1Gd;)V

    .line 222259
    iput p2, p0, LX/1FL;->h:I

    .line 222260
    return-void
.end method

.method public static a(LX/1FL;)LX/1FL;
    .locals 1

    .prologue
    .line 222266
    if-eqz p0, :cond_0

    invoke-direct {p0}, LX/1FL;->k()LX/1FL;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/1FL;)Z
    .locals 1

    .prologue
    .line 222261
    iget v0, p0, LX/1FL;->d:I

    if-ltz v0, :cond_0

    iget v0, p0, LX/1FL;->e:I

    if-ltz v0, :cond_0

    iget v0, p0, LX/1FL;->f:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/1FL;)V
    .locals 0
    .param p0    # LX/1FL;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 222262
    if-eqz p0, :cond_0

    .line 222263
    invoke-virtual {p0}, LX/1FL;->close()V

    .line 222264
    :cond_0
    return-void
.end method

.method public static e(LX/1FL;)Z
    .locals 1
    .param p0    # LX/1FL;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 222265
    if-eqz p0, :cond_0

    invoke-direct {p0}, LX/1FL;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()LX/1FL;
    .locals 3

    .prologue
    .line 222225
    iget-object v0, p0, LX/1FL;->b:LX/1Gd;

    if-eqz v0, :cond_1

    .line 222226
    new-instance v0, LX/1FL;

    iget-object v1, p0, LX/1FL;->b:LX/1Gd;

    iget v2, p0, LX/1FL;->h:I

    invoke-direct {v0, v1, v2}, LX/1FL;-><init>(LX/1Gd;I)V

    .line 222227
    :goto_0
    if-eqz v0, :cond_0

    .line 222228
    invoke-virtual {v0, p0}, LX/1FL;->b(LX/1FL;)V

    .line 222229
    :cond_0
    return-object v0

    .line 222230
    :cond_1
    iget-object v0, p0, LX/1FL;->a:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v1

    .line 222231
    if-nez v1, :cond_2

    const/4 v0, 0x0

    .line 222232
    :goto_1
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 222233
    :cond_2
    :try_start_0
    new-instance v0, LX/1FL;

    invoke-direct {v0, v1}, LX/1FL;-><init>(LX/1FJ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 222234
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method private declared-synchronized l()Z
    .locals 1

    .prologue
    .line 222235
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1FL;->a:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1FL;->b:LX/1Gd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private n()Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222213
    const/4 v1, 0x0

    .line 222214
    :try_start_0
    invoke-virtual {p0}, LX/1FL;->b()Ljava/io/InputStream;

    move-result-object v1

    .line 222215
    invoke-static {v1}, LX/1le;->a(Ljava/io/InputStream;)Landroid/util/Pair;

    move-result-object v2

    .line 222216
    if-eqz v2, :cond_0

    .line 222217
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/1FL;->e:I

    .line 222218
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/1FL;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222219
    :cond_0
    if-eqz v1, :cond_1

    .line 222220
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 222221
    :cond_1
    :goto_0
    return-object v2

    .line 222222
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 222223
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 222224
    :cond_2
    :goto_1
    throw v0

    :catch_0
    goto :goto_0

    :catch_1
    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222212
    iget-object v0, p0, LX/1FL;->a:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 222204
    iget-object v0, p0, LX/1FL;->b:LX/1Gd;

    if-eqz v0, :cond_0

    .line 222205
    iget-object v0, p0, LX/1FL;->b:LX/1Gd;

    invoke-interface {v0}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 222206
    :goto_0
    return-object v0

    .line 222207
    :cond_0
    iget-object v0, p0, LX/1FL;->a:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v2

    .line 222208
    if-eqz v2, :cond_1

    .line 222209
    :try_start_0
    new-instance v1, LX/1lZ;

    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    invoke-direct {v1, v0}, LX/1lZ;-><init>(LX/1FK;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222210
    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V

    throw v0

    .line 222211
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/1FL;)V
    .locals 1

    .prologue
    .line 222190
    iget-object v0, p1, LX/1FL;->c:LX/1lW;

    move-object v0, v0

    .line 222191
    iput-object v0, p0, LX/1FL;->c:LX/1lW;

    .line 222192
    iget v0, p1, LX/1FL;->e:I

    move v0, v0

    .line 222193
    iput v0, p0, LX/1FL;->e:I

    .line 222194
    iget v0, p1, LX/1FL;->f:I

    move v0, v0

    .line 222195
    iput v0, p0, LX/1FL;->f:I

    .line 222196
    iget v0, p1, LX/1FL;->d:I

    move v0, v0

    .line 222197
    iput v0, p0, LX/1FL;->d:I

    .line 222198
    iget v0, p1, LX/1FL;->g:I

    move v0, v0

    .line 222199
    iput v0, p0, LX/1FL;->g:I

    .line 222200
    invoke-virtual {p1}, LX/1FL;->i()I

    move-result v0

    iput v0, p0, LX/1FL;->h:I

    .line 222201
    iget-object v0, p1, LX/1FL;->i:LX/1bh;

    move-object v0, v0

    .line 222202
    iput-object v0, p0, LX/1FL;->i:LX/1bh;

    .line 222203
    return-void
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 222188
    iget-object v0, p0, LX/1FL;->a:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 222189
    return-void
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 222187
    iget v0, p0, LX/1FL;->e:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 222186
    iget v0, p0, LX/1FL;->f:I

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 222185
    iget v0, p0, LX/1FL;->g:I

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 222182
    iget-object v0, p0, LX/1FL;->a:LX/1FJ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1FL;->a:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 222183
    iget-object v0, p0, LX/1FL;->a:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    invoke-virtual {v0}, LX/1FK;->a()I

    move-result v0

    .line 222184
    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/1FL;->h:I

    goto :goto_0
.end method

.method public final j()V
    .locals 3

    .prologue
    .line 222168
    invoke-virtual {p0}, LX/1FL;->b()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, LX/1la;->b(Ljava/io/InputStream;)LX/1lW;

    move-result-object v1

    .line 222169
    iput-object v1, p0, LX/1FL;->c:LX/1lW;

    .line 222170
    invoke-static {v1}, LX/1ld;->a(LX/1lW;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 222171
    invoke-virtual {p0}, LX/1FL;->b()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, LX/35v;->a(Ljava/io/InputStream;)Landroid/util/Pair;

    move-result-object v2

    .line 222172
    if-eqz v2, :cond_0

    .line 222173
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/1FL;->e:I

    .line 222174
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/1FL;->f:I

    .line 222175
    :cond_0
    move-object v0, v2

    .line 222176
    :goto_0
    sget-object v2, LX/1ld;->a:LX/1lW;

    if-ne v1, v2, :cond_3

    iget v1, p0, LX/1FL;->d:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    .line 222177
    if-eqz v0, :cond_1

    .line 222178
    invoke-virtual {p0}, LX/1FL;->b()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, LX/1lg;->a(Ljava/io/InputStream;)I

    move-result v0

    invoke-static {v0}, LX/1li;->a(I)I

    move-result v0

    iput v0, p0, LX/1FL;->d:I

    .line 222179
    :cond_1
    :goto_1
    return-void

    .line 222180
    :cond_2
    invoke-direct {p0}, LX/1FL;->n()Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 222181
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, LX/1FL;->d:I

    goto :goto_1
.end method
