.class public LX/1iX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UsingDefaultJsonDeserializer"
    }
.end annotation


# instance fields
.field public a:J

.field public final b:LX/1hg;


# direct methods
.method public constructor <init>(LX/0am;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "LX/1hg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 298490
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298491
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/1iX;->a:J

    .line 298492
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1hg;

    :goto_0
    iput-object v0, p0, LX/1iX;->b:LX/1hg;

    .line 298493
    return-void

    .line 298494
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(J)V
    .locals 1

    .prologue
    .line 298495
    iput-wide p1, p0, LX/1iX;->a:J

    .line 298496
    return-void
.end method

.method public final b(J)V
    .locals 3

    .prologue
    .line 298486
    iget-wide v0, p0, LX/1iX;->a:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/1iX;->a:J

    .line 298487
    iget-object v0, p0, LX/1iX;->b:LX/1hg;

    if-eqz v0, :cond_0

    .line 298488
    iget-object v0, p0, LX/1iX;->b:LX/1hg;

    invoke-virtual {v0, p1, p2}, LX/1hg;->a(J)V

    .line 298489
    :cond_0
    return-void
.end method
