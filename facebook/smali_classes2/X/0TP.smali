.class public final enum LX/0TP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0TP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0TP;

.field public static final enum BACKGROUND:LX/0TP;

.field public static final enum FOREGROUND:LX/0TP;

.field public static final enum NORMAL:LX/0TP;

.field public static final enum NORMAL_NEW:LX/0TP;

.field public static final enum REALTIME_DO_NOT_USE:LX/0TP;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum URGENT:LX/0TP;


# instance fields
.field private final mAndroidThreadPriority:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x5

    .line 62996
    new-instance v0, LX/0TP;

    const-string v1, "REALTIME_DO_NOT_USE"

    const/4 v2, -0x8

    invoke-direct {v0, v1, v5, v2}, LX/0TP;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0TP;->REALTIME_DO_NOT_USE:LX/0TP;

    .line 62997
    new-instance v0, LX/0TP;

    const-string v1, "URGENT"

    invoke-direct {v0, v1, v6, v4}, LX/0TP;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0TP;->URGENT:LX/0TP;

    .line 62998
    new-instance v0, LX/0TP;

    const-string v1, "NORMAL_NEW"

    invoke-direct {v0, v1, v7, v4}, LX/0TP;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0TP;->NORMAL_NEW:LX/0TP;

    .line 62999
    new-instance v0, LX/0TP;

    const-string v1, "FOREGROUND"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v8, v2}, LX/0TP;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0TP;->FOREGROUND:LX/0TP;

    .line 63000
    new-instance v0, LX/0TP;

    const-string v1, "NORMAL"

    const/4 v2, 0x4

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, LX/0TP;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0TP;->NORMAL:LX/0TP;

    .line 63001
    new-instance v0, LX/0TP;

    const-string v1, "BACKGROUND"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v4, v2}, LX/0TP;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0TP;->BACKGROUND:LX/0TP;

    .line 63002
    const/4 v0, 0x6

    new-array v0, v0, [LX/0TP;

    sget-object v1, LX/0TP;->REALTIME_DO_NOT_USE:LX/0TP;

    aput-object v1, v0, v5

    sget-object v1, LX/0TP;->URGENT:LX/0TP;

    aput-object v1, v0, v6

    sget-object v1, LX/0TP;->NORMAL_NEW:LX/0TP;

    aput-object v1, v0, v7

    sget-object v1, LX/0TP;->FOREGROUND:LX/0TP;

    aput-object v1, v0, v8

    const/4 v1, 0x4

    sget-object v2, LX/0TP;->NORMAL:LX/0TP;

    aput-object v2, v0, v1

    sget-object v1, LX/0TP;->BACKGROUND:LX/0TP;

    aput-object v1, v0, v4

    sput-object v0, LX/0TP;->$VALUES:[LX/0TP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 62993
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 62994
    iput p3, p0, LX/0TP;->mAndroidThreadPriority:I

    .line 62995
    return-void
.end method

.method public static fromStringOrNull(Ljava/lang/String;)LX/0TP;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 62987
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 62988
    :cond_0
    :goto_0
    return-object v0

    .line 62989
    :cond_1
    invoke-static {}, LX/0TP;->values()[LX/0TP;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 62990
    invoke-virtual {v1}, LX/0TP;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v1

    .line 62991
    goto :goto_0

    .line 62992
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public static getClosestThreadPriorityFromAndroidThreadPriority(I)LX/0TP;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 62981
    invoke-static {}, LX/0TP;->values()[LX/0TP;

    move-result-object v4

    array-length v5, v4

    const/4 v0, 0x0

    move v3, v0

    move-object v1, v2

    :goto_0
    if-ge v3, v5, :cond_1

    aget-object v0, v4, v3

    .line 62982
    invoke-virtual {v0}, LX/0TP;->getAndroidThreadPriority()I

    move-result v6

    if-lt v6, p0, :cond_0

    invoke-direct {v0, v2}, LX/0TP;->isLessThanOrNull(LX/0TP;)Z

    move-result v6

    if-eqz v6, :cond_0

    move-object v2, v0

    .line 62983
    :cond_0
    invoke-direct {v0, v1}, LX/0TP;->isGreaterThanOrNull(LX/0TP;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 62984
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-object v1, v0

    goto :goto_0

    .line 62985
    :cond_1
    if-nez v2, :cond_2

    .line 62986
    :goto_2
    return-object v1

    :cond_2
    move-object v1, v2

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method private isGreaterThanOrNull(LX/0TP;)Z
    .locals 3
    .param p1    # LX/0TP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 62971
    if-nez p1, :cond_1

    .line 62972
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, LX/0TP;->getAndroidThreadPriority()I

    move-result v1

    invoke-virtual {p1}, LX/0TP;->getAndroidThreadPriority()I

    move-result v2

    if-gt v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isLessThanOrNull(LX/0TP;)Z
    .locals 3
    .param p1    # LX/0TP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 62979
    if-nez p1, :cond_1

    .line 62980
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, LX/0TP;->getAndroidThreadPriority()I

    move-result v1

    invoke-virtual {p0}, LX/0TP;->getAndroidThreadPriority()I

    move-result v2

    if-gt v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static ofCurrentThread()LX/0TP;
    .locals 1

    .prologue
    .line 62978
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v0

    invoke-static {v0}, LX/0TP;->getClosestThreadPriorityFromAndroidThreadPriority(I)LX/0TP;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0TP;
    .locals 1

    .prologue
    .line 62977
    const-class v0, LX/0TP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0TP;

    return-object v0
.end method

.method public static values()[LX/0TP;
    .locals 1

    .prologue
    .line 62976
    sget-object v0, LX/0TP;->$VALUES:[LX/0TP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0TP;

    return-object v0
.end method


# virtual methods
.method public final getAndroidThreadPriority()I
    .locals 1

    .prologue
    .line 62975
    iget v0, p0, LX/0TP;->mAndroidThreadPriority:I

    return v0
.end method

.method public final isHigherPriorityThan(LX/0TP;)Z
    .locals 2

    .prologue
    .line 62974
    iget v0, p0, LX/0TP;->mAndroidThreadPriority:I

    iget v1, p1, LX/0TP;->mAndroidThreadPriority:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isLowerPriorityThan(LX/0TP;)Z
    .locals 2

    .prologue
    .line 62973
    iget v0, p0, LX/0TP;->mAndroidThreadPriority:I

    iget v1, p1, LX/0TP;->mAndroidThreadPriority:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
