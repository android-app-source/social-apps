.class public LX/0sL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 151636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(II)V
    .locals 1

    .prologue
    .line 151642
    invoke-static {}, LX/0sL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151643
    invoke-static {p0, p1}, Ljunit/framework/Assert;->assertEquals(II)V

    .line 151644
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 151645
    invoke-static {}, LX/0sL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151646
    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 151647
    :cond_0
    return-void
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 151640
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 151641
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 151637
    invoke-static {}, LX/0sL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151638
    invoke-static {p0}, Ljunit/framework/Assert;->assertNull(Ljava/lang/Object;)V

    .line 151639
    :cond_0
    return-void
.end method
