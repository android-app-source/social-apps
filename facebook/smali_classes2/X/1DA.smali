.class public LX/1DA;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hj;


# annotations
.annotation build Lcom/facebook/controllercallbacks/api/ControllerConfig;
.end annotation


# instance fields
.field private final a:LX/1DB;

.field public final b:LX/0pn;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:Ljava/util/concurrent/Executor;

.field public e:LX/DBx;

.field public f:Landroid/content/Context;

.field public g:Lcom/facebook/feed/fragment/NewsFeedFragment;

.field public h:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "Lcom/facebook/api/feed/data/LegacyFeedUnitUpdater;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/api/feedtype/FeedType;

.field private j:LX/0oJ;


# direct methods
.method public constructor <init>(LX/1DB;LX/0pn;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;LX/0oJ;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 217307
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 217308
    iput-object p1, p0, LX/1DA;->a:LX/1DB;

    .line 217309
    iput-object p2, p0, LX/1DA;->b:LX/0pn;

    .line 217310
    iput-object p3, p0, LX/1DA;->c:Ljava/util/concurrent/Executor;

    .line 217311
    iput-object p4, p0, LX/1DA;->d:Ljava/util/concurrent/Executor;

    .line 217312
    iput-object p5, p0, LX/1DA;->j:LX/0oJ;

    .line 217313
    return-void
.end method

.method public static b(LX/0QB;)LX/1DA;
    .locals 6

    .prologue
    .line 217314
    new-instance v0, LX/1DA;

    const-class v1, LX/1DB;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/1DB;

    invoke-static {p0}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v2

    check-cast v2, LX/0pn;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0oJ;->b(LX/0QB;)LX/0oJ;

    move-result-object v5

    check-cast v5, LX/0oJ;

    invoke-direct/range {v0 .. v5}, LX/1DA;-><init>(LX/1DB;LX/0pn;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;LX/0oJ;)V

    .line 217315
    return-object v0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 217316
    const/16 v0, 0x65

    if-ne p1, v0, :cond_1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    iget-object v0, p0, LX/1DA;->i:Lcom/facebook/api/feedtype/FeedType;

    sget-object v1, Lcom/facebook/api/feedtype/FeedType;->d:Lcom/facebook/api/feedtype/FeedType;

    if-eq v0, v1, :cond_1

    .line 217317
    if-eqz p3, :cond_1

    const-string v0, "audience_changed"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 217318
    iget-object v0, p0, LX/1DA;->e:LX/DBx;

    if-eqz v0, :cond_0

    .line 217319
    iget-object v0, p0, LX/1DA;->e:LX/DBx;

    invoke-virtual {v0}, LX/DBx;->a()V

    .line 217320
    :cond_0
    iget-object v0, p0, LX/1DA;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/feed/fragment/controllercallbacks/GoodFriendsCustomizationHeaderController$1;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    const-string p2, "clean-goodfriends-feed"

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/feed/fragment/controllercallbacks/GoodFriendsCustomizationHeaderController$1;-><init>(LX/1DA;Ljava/lang/Class;Ljava/lang/String;)V

    const p1, -0x53bd6129

    invoke-static {v0, v1, p1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 217321
    :cond_1
    return-void
.end method

.method public final b()LX/DBx;
    .locals 9

    .prologue
    .line 217322
    iget-object v0, p0, LX/1DA;->a:LX/1DB;

    iget-object v1, p0, LX/1DA;->i:Lcom/facebook/api/feedtype/FeedType;

    .line 217323
    new-instance v2, LX/DBx;

    invoke-static {v0}, LX/1b1;->b(LX/0QB;)LX/1b1;

    move-result-object v3

    check-cast v3, LX/1b1;

    invoke-static {v0}, LX/87o;->b(LX/0QB;)LX/87o;

    move-result-object v4

    check-cast v4, LX/87o;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    const/16 v6, 0x12cb

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    move-object v8, v1

    invoke-direct/range {v2 .. v8}, LX/DBx;-><init>(LX/1b1;LX/87o;LX/03V;LX/0Or;Landroid/content/Context;Lcom/facebook/api/feedtype/FeedType;)V

    .line 217324
    move-object v0, v2

    .line 217325
    iput-object v0, p0, LX/1DA;->e:LX/DBx;

    .line 217326
    iget-object v0, p0, LX/1DA;->e:LX/DBx;

    return-object v0
.end method

.method public final kw_()Z
    .locals 1

    .prologue
    .line 217327
    iget-object v0, p0, LX/1DA;->j:LX/0oJ;

    invoke-virtual {v0}, LX/0oJ;->a()Z

    move-result v0

    return v0
.end method
