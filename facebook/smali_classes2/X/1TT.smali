.class public LX/1TT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;
.implements LX/1TG;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17S;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17S;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 251572
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 251573
    iput-object p1, p0, LX/1TT;->a:LX/0Ot;

    .line 251574
    iput-object p2, p0, LX/1TT;->b:LX/0Ot;

    .line 251575
    iput-object p3, p0, LX/1TT;->c:LX/0Ot;

    .line 251576
    return-void
.end method

.method public static a(LX/0QB;)LX/1TT;
    .locals 6

    .prologue
    .line 251577
    const-class v1, LX/1TT;

    monitor-enter v1

    .line 251578
    :try_start_0
    sget-object v0, LX/1TT;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 251579
    sput-object v2, LX/1TT;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 251580
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251581
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 251582
    new-instance v3, LX/1TT;

    const/16 v4, 0x1ff9

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1ffd

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x76f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, LX/1TT;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 251583
    move-object v0, v3

    .line 251584
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 251585
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1TT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 251586
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 251587
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 1

    .prologue
    .line 251588
    sget-object v0, LX/JQO;->a:LX/1Cz;

    invoke-static {p1, v0}, LX/99p;->a(LX/0ja;LX/1Cz;)V

    .line 251589
    return-void
.end method

.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 251590
    sget-object v0, LX/3YF;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 251591
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 251592
    iget-object v0, p0, LX/1TT;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17S;

    invoke-virtual {v0}, LX/17S;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251593
    const-class v0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;

    iget-object v1, p0, LX/1TT;->b:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 251594
    :goto_0
    return-void

    .line 251595
    :cond_0
    const-class v0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;

    iget-object v1, p0, LX/1TT;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    goto :goto_0
.end method
