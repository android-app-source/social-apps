.class public LX/0qi;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 148298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148299
    iput-boolean p1, p0, LX/0qi;->a:Z

    .line 148300
    iput-object p2, p0, LX/0qi;->b:Ljava/lang/String;

    .line 148301
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0qi;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 148297
    new-instance v1, LX/0qi;

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v0, p0}, LX/0qi;-><init>(ZLjava/lang/String;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Z)LX/0qi;
    .locals 2

    .prologue
    .line 148284
    new-instance v0, LX/0qi;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LX/0qi;-><init>(ZLjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 148289
    if-ne p0, p1, :cond_1

    .line 148290
    :cond_0
    :goto_0
    return v0

    .line 148291
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 148292
    goto :goto_0

    .line 148293
    :cond_3
    check-cast p1, LX/0qi;

    .line 148294
    iget-boolean v2, p0, LX/0qi;->a:Z

    iget-boolean v3, p1, LX/0qi;->a:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 148295
    goto :goto_0

    .line 148296
    :cond_4
    iget-object v2, p0, LX/0qi;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v0, p0, LX/0qi;->b:Ljava/lang/String;

    iget-object v1, p1, LX/0qi;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_5
    iget-object v2, p1, LX/0qi;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 148285
    iget-boolean v0, p0, LX/0qi;->a:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 148286
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/0qi;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, LX/0qi;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 148287
    return v0

    :cond_1
    move v0, v1

    .line 148288
    goto :goto_0
.end method
