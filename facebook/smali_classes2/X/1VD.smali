.class public LX/1VD;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1X2;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1VD",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1X2;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 257515
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 257516
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1VD;->b:LX/0Zi;

    .line 257517
    iput-object p1, p0, LX/1VD;->a:LX/0Ot;

    .line 257518
    return-void
.end method

.method public static a(LX/0QB;)LX/1VD;
    .locals 4

    .prologue
    .line 257519
    const-class v1, LX/1VD;

    monitor-enter v1

    .line 257520
    :try_start_0
    sget-object v0, LX/1VD;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 257521
    sput-object v2, LX/1VD;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 257522
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257523
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 257524
    new-instance v3, LX/1VD;

    const/16 p0, 0x721

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1VD;-><init>(LX/0Ot;)V

    .line 257525
    move-object v0, v3

    .line 257526
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 257527
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1VD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 257528
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 257529
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 19

    .prologue
    .line 257530
    check-cast p2, LX/1X0;

    .line 257531
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1VD;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1X2;

    move-object/from16 v0, p2

    iget-object v3, v0, LX/1X0;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object/from16 v0, p2

    iget-object v4, v0, LX/1X0;->b:LX/1Pb;

    move-object/from16 v0, p2

    iget v5, v0, LX/1X0;->c:I

    move-object/from16 v0, p2

    iget v6, v0, LX/1X0;->d:I

    move-object/from16 v0, p2

    iget-boolean v7, v0, LX/1X0;->e:Z

    move-object/from16 v0, p2

    iget-boolean v8, v0, LX/1X0;->f:Z

    move-object/from16 v0, p2

    iget-boolean v9, v0, LX/1X0;->g:Z

    move-object/from16 v0, p2

    iget-boolean v10, v0, LX/1X0;->h:Z

    move-object/from16 v0, p2

    iget-boolean v11, v0, LX/1X0;->i:Z

    move-object/from16 v0, p2

    iget-boolean v12, v0, LX/1X0;->j:Z

    move-object/from16 v0, p2

    iget-boolean v13, v0, LX/1X0;->k:Z

    move-object/from16 v0, p2

    iget v14, v0, LX/1X0;->l:I

    move-object/from16 v0, p2

    iget v15, v0, LX/1X0;->m:I

    move-object/from16 v0, p2

    iget v0, v0, LX/1X0;->n:I

    move/from16 v16, v0

    move-object/from16 v0, p2

    iget v0, v0, LX/1X0;->o:I

    move/from16 v17, v0

    move-object/from16 v0, p2

    iget v0, v0, LX/1X0;->p:I

    move/from16 v18, v0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v18}, LX/1X2;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;IIZZZZZZZIIIII)LX/1Dg;

    move-result-object v1

    .line 257532
    return-object v1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 257533
    invoke-static {}, LX/1dS;->b()V

    .line 257534
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/1X4;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1VD",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 257535
    new-instance v1, LX/1X0;

    invoke-direct {v1, p0}, LX/1X0;-><init>(LX/1VD;)V

    .line 257536
    iget-object v2, p0, LX/1VD;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1X4;

    .line 257537
    if-nez v2, :cond_0

    .line 257538
    new-instance v2, LX/1X4;

    invoke-direct {v2, p0}, LX/1X4;-><init>(LX/1VD;)V

    .line 257539
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/1X4;->a$redex0(LX/1X4;LX/1De;IILX/1X0;)V

    .line 257540
    move-object v1, v2

    .line 257541
    move-object v0, v1

    .line 257542
    return-object v0
.end method
