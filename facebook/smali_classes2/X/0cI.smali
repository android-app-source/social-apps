.class public LX/0cI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0cJ;


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:LX/0Xl;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0VT;

.field public final e:LX/03V;

.field public final f:LX/0cK;

.field private final g:Landroid/os/HandlerThread;

.field public h:Landroid/os/Handler;

.field private i:Landroid/os/Messenger;

.field private j:LX/0Yb;

.field public final k:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Integer;",
            "LX/0cK;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "LX/0cN;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Integer;",
            "LX/0cM;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0bD;

.field public final o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final p:Z

.field public volatile q:LX/26n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/26n",
            "<",
            "LX/0bE;",
            ">;"
        }
    .end annotation
.end field

.field public r:Landroid/content/Intent;

.field public final s:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0Xl;LX/0Or;LX/0VT;LX/03V;LX/0bD;Landroid/os/HandlerThread;LX/0Or;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Xl;",
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/facebook/common/process/ProcessUtil;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0bD;",
            "Landroid/os/HandlerThread;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 88112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88113
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/0cI;->k:Ljava/util/concurrent/ConcurrentMap;

    .line 88114
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/0cI;->l:Ljava/util/concurrent/ConcurrentMap;

    .line 88115
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/0cI;->m:Ljava/util/concurrent/ConcurrentMap;

    .line 88116
    new-instance v0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$1;

    invoke-direct {v0, p0}, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$1;-><init>(LX/0cI;)V

    iput-object v0, p0, LX/0cI;->s:Ljava/lang/Runnable;

    .line 88117
    iput-object p1, p0, LX/0cI;->a:Ljava/lang/String;

    .line 88118
    iput-object p2, p0, LX/0cI;->b:LX/0Xl;

    .line 88119
    iput-object p3, p0, LX/0cI;->c:LX/0Or;

    .line 88120
    iput-object p4, p0, LX/0cI;->d:LX/0VT;

    .line 88121
    iput-object p5, p0, LX/0cI;->e:LX/03V;

    .line 88122
    iput-object p6, p0, LX/0cI;->n:LX/0bD;

    .line 88123
    iput-object p7, p0, LX/0cI;->g:Landroid/os/HandlerThread;

    .line 88124
    iput-object p8, p0, LX/0cI;->o:LX/0Or;

    .line 88125
    iput-boolean p9, p0, LX/0cI;->p:Z

    .line 88126
    new-instance v1, LX/0cK;

    const/4 v2, 0x0

    iget-object v0, p0, LX/0cI;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p0, LX/0cI;->d:LX/0VT;

    invoke-virtual {v3}, LX/0VT;->a()LX/00G;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, LX/0cK;-><init>(Landroid/os/Messenger;ILX/00G;)V

    iput-object v1, p0, LX/0cI;->f:LX/0cK;

    .line 88127
    return-void
.end method

.method public static a$redex0(LX/0cI;LX/0cK;)V
    .locals 2

    .prologue
    .line 88128
    iget-object v0, p0, LX/0cI;->k:Ljava/util/concurrent/ConcurrentMap;

    iget v1, p1, LX/0cK;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 88129
    iget-object v0, p0, LX/0cI;->l:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0cN;

    .line 88130
    invoke-interface {v0, p1}, LX/0cN;->a(LX/0cK;)V

    goto :goto_0

    .line 88131
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/0cI;LX/0cK;LX/1gR;)V
    .locals 3

    .prologue
    .line 88132
    iget-object v0, p0, LX/0cI;->k:Ljava/util/concurrent/ConcurrentMap;

    iget v1, p1, LX/0cK;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88133
    iget-object v0, p0, LX/0cI;->l:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0cN;

    .line 88134
    invoke-interface {v0, p1, p2}, LX/0cN;->a(LX/0cK;LX/1gR;)V

    goto :goto_0

    .line 88135
    :cond_0
    const/4 v0, 0x0

    .line 88136
    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88137
    iget-object v1, p1, LX/0cK;->a:Landroid/os/Messenger;

    invoke-virtual {v1}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v1

    new-instance v2, LX/1gS;

    invoke-direct {v2, p0, p1}, LX/1gS;-><init>(LX/0cI;LX/0cK;)V

    const/4 p2, 0x0

    invoke-interface {v1, v2, p2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88138
    const/4 v0, 0x1

    .line 88139
    :goto_1
    move v0, v0

    .line 88140
    if-nez v0, :cond_1

    .line 88141
    invoke-static {p0, p1}, LX/0cI;->a$redex0(LX/0cI;LX/0cK;)V

    .line 88142
    :cond_1
    return-void

    :catch_0
    goto :goto_1
.end method

.method public static b(LX/0cI;Landroid/os/Message;)LX/0cK;
    .locals 6

    .prologue
    .line 88101
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 88102
    iget-object v0, p0, LX/0cI;->k:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0cK;

    .line 88103
    if-nez v0, :cond_0

    .line 88104
    iget-object v2, p0, LX/0cI;->e:LX/03V;

    const-class v3, LX/0cJ;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Message from unknown process: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", probably the message\'s arg1 is not set to the pid of source process. Message details: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", peer infos: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, LX/0cI;->k:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 88105
    :cond_0
    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 88152
    iget-object v0, p0, LX/0cI;->h:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$3;

    invoke-direct {v1, p0}, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$3;-><init>(LX/0cI;)V

    const v2, 0x3e3b325

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 88153
    return-void
.end method

.method public static c(LX/0cI;Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 88143
    iget-object v1, p0, LX/0cI;->m:Ljava/util/concurrent/ConcurrentMap;

    monitor-enter v1

    .line 88144
    :try_start_0
    iget-object v0, p0, LX/0cI;->m:Ljava/util/concurrent/ConcurrentMap;

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0cM;

    .line 88145
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88146
    if-eqz v0, :cond_0

    .line 88147
    invoke-static {p0, p1}, LX/0cI;->b(LX/0cI;Landroid/os/Message;)LX/0cK;

    move-result-object v1

    .line 88148
    if-nez v1, :cond_1

    .line 88149
    :cond_0
    :goto_0
    return-void

    .line 88150
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 88151
    :cond_1
    invoke-interface {v0, v1, p1}, LX/0cM;->a(LX/0cK;Landroid/os/Message;)V

    goto :goto_0
.end method

.method public static d(LX/0cI;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 88157
    iget-object v0, p0, LX/0cI;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 88158
    if-eqz v0, :cond_0

    .line 88159
    iget-object v1, p0, LX/0cI;->r:Landroid/content/Intent;

    const-string v2, "__KEY_LOGGED_USER_ID__"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 88160
    invoke-static {p0}, LX/0cI;->f(LX/0cI;)V

    .line 88161
    :cond_0
    return-object v0
.end method

.method public static f(LX/0cI;)V
    .locals 5

    .prologue
    .line 88154
    invoke-static {p0}, LX/0cI;->g$redex0(LX/0cI;)V

    .line 88155
    iget-object v0, p0, LX/0cI;->h:Landroid/os/Handler;

    iget-object v1, p0, LX/0cI;->s:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    const v4, -0x2bf01001

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 88156
    return-void
.end method

.method public static g$redex0(LX/0cI;)V
    .locals 5

    .prologue
    .line 88106
    :try_start_0
    invoke-static {p0}, LX/0cI;->h(LX/0cI;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 88107
    :goto_0
    return-void

    .line 88108
    :catch_0
    move-exception v0

    .line 88109
    iget-object v1, p0, LX/0cI;->e:LX/03V;

    const-class v2, LX/0cJ;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception occurred when sending peer init intent; peer info: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/0cI;->f:LX/0cK;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; intent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, LX/0cI;->r:Landroid/content/Intent;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static h(LX/0cI;)V
    .locals 2

    .prologue
    .line 88110
    iget-object v0, p0, LX/0cI;->b:LX/0Xl;

    iget-object v1, p0, LX/0cI;->r:Landroid/content/Intent;

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 88111
    return-void
.end method


# virtual methods
.method public final a()LX/0cK;
    .locals 1

    .prologue
    .line 88080
    iget-object v0, p0, LX/0cI;->f:LX/0cK;

    return-object v0
.end method

.method public final a(ILX/0cM;)V
    .locals 3

    .prologue
    .line 88069
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88070
    iget-object v0, p0, LX/0cI;->m:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88071
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The listener for message type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has already registered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88072
    :cond_0
    iget-object v0, p0, LX/0cI;->m:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88073
    return-void
.end method

.method public final a(LX/0cK;Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 88074
    invoke-virtual {p0}, LX/0cI;->a()LX/0cK;

    move-result-object v0

    iget v0, v0, LX/0cK;->b:I

    iput v0, p2, Landroid/os/Message;->arg1:I

    .line 88075
    iget-object v0, p0, LX/0cI;->h:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$6;-><init>(LX/0cI;LX/0cK;Landroid/os/Message;)V

    const v2, 0x6393be18

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 88076
    return-void
.end method

.method public final a(LX/0cN;)V
    .locals 2

    .prologue
    .line 88077
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88078
    iget-object v0, p0, LX/0cI;->l:Ljava/util/concurrent/ConcurrentMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88079
    return-void
.end method

.method public final a(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 88081
    iget-object v0, p0, LX/0cI;->k:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88082
    :goto_0
    return-void

    .line 88083
    :cond_0
    invoke-virtual {p0}, LX/0cI;->a()LX/0cK;

    move-result-object v0

    iget v0, v0, LX/0cK;->b:I

    iput v0, p1, Landroid/os/Message;->arg1:I

    .line 88084
    iget-object v0, p0, LX/0cI;->h:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$5;

    invoke-direct {v1, p0, p1}, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$5;-><init>(LX/0cI;Landroid/os/Message;)V

    const v2, -0x17447098

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 88085
    iget-object v0, p0, LX/0cI;->k:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 2

    .prologue
    .line 88086
    iget-boolean v0, p0, LX/0cI;->p:Z

    if-eqz v0, :cond_0

    .line 88087
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 88088
    invoke-virtual {p0, v0}, LX/0cI;->a(Landroid/os/Message;)V

    .line 88089
    iget-object v0, p0, LX/0cI;->k:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    .line 88090
    invoke-direct {p0}, LX/0cI;->c()V

    .line 88091
    :cond_0
    return-void
.end method

.method public final init()V
    .locals 4

    .prologue
    .line 88092
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, LX/0cO;

    iget-object v2, p0, LX/0cI;->g:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, LX/0cO;-><init>(LX/0cI;Landroid/os/Looper;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, LX/0cI;->i:Landroid/os/Messenger;

    .line 88093
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, LX/0cI;->g:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/0cI;->h:Landroid/os/Handler;

    .line 88094
    iget-object v0, p0, LX/0cI;->f:LX/0cK;

    iget-object v1, p0, LX/0cI;->i:Landroid/os/Messenger;

    iput-object v1, v0, LX/0cK;->a:Landroid/os/Messenger;

    .line 88095
    iget-object v0, p0, LX/0cI;->b:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    iget-object v1, p0, LX/0cI;->a:Ljava/lang/String;

    new-instance v2, LX/0cP;

    invoke-direct {v2, p0}, LX/0cP;-><init>(LX/0cI;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    iget-object v1, p0, LX/0cI;->h:Landroid/os/Handler;

    invoke-interface {v0, v1}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/0cI;->j:LX/0Yb;

    .line 88096
    iget-object v0, p0, LX/0cI;->j:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 88097
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/0cI;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0cI;->r:Landroid/content/Intent;

    .line 88098
    iget-object v0, p0, LX/0cI;->r:Landroid/content/Intent;

    const-string v1, "peer_info"

    iget-object v2, p0, LX/0cI;->f:LX/0cK;

    invoke-virtual {v2}, LX/0cK;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 88099
    invoke-direct {p0}, LX/0cI;->c()V

    .line 88100
    return-void
.end method
