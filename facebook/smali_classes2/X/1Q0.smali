.class public LX/1Q0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pc;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2iT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2iT;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 244228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244229
    iput-object p1, p0, LX/1Q0;->a:LX/0Ot;

    .line 244230
    return-void
.end method

.method public static a(LX/0QB;)LX/1Q0;
    .locals 1

    .prologue
    .line 244231
    invoke-static {p0}, LX/1Q0;->b(LX/0QB;)LX/1Q0;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1Q0;
    .locals 2

    .prologue
    .line 244232
    new-instance v0, LX/1Q0;

    const/16 v1, 0xa79

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Q0;-><init>(LX/0Ot;)V

    .line 244233
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;
    .locals 8
    .param p5    # LX/5P5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    .line 244234
    iget-object v0, p0, LX/1Q0;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2iT;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, LX/2iT;->a(JLX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)V

    .line 244235
    sget-object v0, LX/99N;->a:[I

    invoke-virtual {p4}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 244236
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unsupported Friending Status"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 244237
    :pswitch_0
    new-instance v0, LX/5Oh;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {v0, v1, v7}, LX/5Oh;-><init>(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 244238
    :goto_0
    return-object v0

    .line 244239
    :pswitch_1
    new-instance v0, LX/5Oh;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {v0, v1, v7}, LX/5Oh;-><init>(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    goto :goto_0

    .line 244240
    :pswitch_2
    new-instance v0, LX/5Oh;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {v0, v1, v7}, LX/5Oh;-><init>(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    goto :goto_0

    .line 244241
    :pswitch_3
    new-instance v0, LX/5Oh;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/5Oh;-><init>(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
