.class public final LX/0nu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0nv;


# instance fields
.field private a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;",
            "Lcom/facebook/navigation/HasScrollAwayNavigation",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 136676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136677
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0nu;->a:Ljava/util/HashMap;

    .line 136678
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Creating REAL NavigationComponentMap!!!"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 136679
    iget-object v0, p0, LX/0nu;->a:Ljava/util/HashMap;

    const-class v1, Lcom/facebook/feed/fragment/NewsFeedFragment;

    new-instance v2, LX/0nw;

    invoke-direct {v2}, LX/0nw;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136680
    iget-object v0, p0, LX/0nu;->a:Ljava/util/HashMap;

    const-class v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;

    new-instance v2, LX/0ny;

    invoke-direct {v2}, LX/0ny;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136681
    iget-object v0, p0, LX/0nu;->a:Ljava/util/HashMap;

    const-class v1, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;

    new-instance v2, LX/0o0;

    invoke-direct {v2}, LX/0o0;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136682
    iget-object v0, p0, LX/0nu;->a:Ljava/util/HashMap;

    const-class v1, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    new-instance v2, LX/0o3;

    invoke-direct {v2}, LX/0o3;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136683
    iget-object v0, p0, LX/0nu;->a:Ljava/util/HashMap;

    const-class v1, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    new-instance v2, LX/0o4;

    invoke-direct {v2}, LX/0o4;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136684
    iget-object v0, p0, LX/0nu;->a:Ljava/util/HashMap;

    const-class v1, Lcom/facebook/notifications/widget/NotificationsFragment;

    new-instance v2, LX/0o7;

    invoke-direct {v2}, LX/0o7;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136685
    iget-object v0, p0, LX/0nu;->a:Ljava/util/HashMap;

    const-class v1, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    new-instance v2, LX/0o9;

    invoke-direct {v2}, LX/0o9;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136686
    iget-object v0, p0, LX/0nu;->a:Ljava/util/HashMap;

    const-class v1, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    new-instance v2, LX/0oA;

    invoke-direct {v2}, LX/0oA;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136687
    iget-object v0, p0, LX/0nu;->a:Ljava/util/HashMap;

    const-class v1, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    new-instance v2, LX/0oH;

    invoke-direct {v2}, LX/0oH;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136688
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;)LX/0nx;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/Fragment;",
            ")",
            "Lcom/facebook/navigation/HasScrollAwayNavigation",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136689
    iget-object v0, p0, LX/0nu;->a:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0nx;

    return-object v0
.end method
