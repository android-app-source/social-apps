.class public final LX/17F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/flatbuffers/Flattenable;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:LX/15i;

.field private final b:I

.field private final c:I

.field private final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:LX/16a;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I


# direct methods
.method public constructor <init>(LX/15i;IILX/16a;)V
    .locals 1

    .prologue
    .line 189562
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189563
    iput-object p1, p0, LX/17F;->a:LX/15i;

    .line 189564
    iput p2, p0, LX/17F;->b:I

    .line 189565
    iput p3, p0, LX/17F;->c:I

    .line 189566
    const/4 v0, 0x0

    iput-object v0, p0, LX/17F;->d:Ljava/lang/Class;

    .line 189567
    iput-object p4, p0, LX/17F;->e:LX/16a;

    .line 189568
    const/4 v0, 0x0

    iput v0, p0, LX/17F;->f:I

    .line 189569
    return-void
.end method

.method public constructor <init>(LX/15i;IILjava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15i;",
            "II",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 189554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189555
    iput-object p1, p0, LX/17F;->a:LX/15i;

    .line 189556
    iput p2, p0, LX/17F;->b:I

    .line 189557
    iput p3, p0, LX/17F;->c:I

    .line 189558
    iput-object p4, p0, LX/17F;->d:Ljava/lang/Class;

    .line 189559
    const/4 v0, 0x0

    iput-object v0, p0, LX/17F;->e:LX/16a;

    .line 189560
    const/4 v0, 0x0

    iput v0, p0, LX/17F;->f:I

    .line 189561
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 2

    .prologue
    .line 189570
    iget v0, p0, LX/17F;->f:I

    iget v1, p0, LX/17F;->c:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189540
    iget v0, p0, LX/17F;->f:I

    if-ltz v0, :cond_0

    iget v0, p0, LX/17F;->f:I

    iget v1, p0, LX/17F;->c:I

    if-lt v0, v1, :cond_1

    .line 189541
    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Out of bound for iteration"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 189542
    :cond_1
    iget v0, p0, LX/17F;->b:I

    iget v1, p0, LX/17F;->f:I

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 189543
    iget v1, p0, LX/17F;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/17F;->f:I

    .line 189544
    iget-object v1, p0, LX/17F;->a:LX/15i;

    invoke-static {v1, v0}, LX/15i;->e(LX/15i;I)I

    move-result v1

    .line 189545
    if-nez v1, :cond_2

    .line 189546
    const/4 v0, 0x0

    .line 189547
    :goto_0
    return-object v0

    .line 189548
    :cond_2
    iget-object v2, p0, LX/17F;->d:Ljava/lang/Class;

    if-eqz v2, :cond_3

    .line 189549
    iget-object v2, p0, LX/17F;->a:LX/15i;

    add-int/2addr v0, v1

    iget-object v1, p0, LX/17F;->d:Ljava/lang/Class;

    invoke-virtual {v2, v0, v1}, LX/15i;->a(ILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    goto :goto_0

    .line 189550
    :cond_3
    iget-object v2, p0, LX/17F;->e:LX/16a;

    if-eqz v2, :cond_4

    .line 189551
    iget-object v2, p0, LX/17F;->a:LX/15i;

    add-int/2addr v0, v1

    iget-object v1, p0, LX/17F;->e:LX/16a;

    invoke-virtual {v2, v0, v1}, LX/15i;->a(ILX/16a;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    goto :goto_0

    .line 189552
    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Either clazz or resolver should be provided"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 189553
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
