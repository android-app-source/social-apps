.class public LX/1ia;
.super LX/0Vx;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1ia;


# direct methods
.method public constructor <init>(LX/2WA;)V
    .locals 0
    .param p1    # LX/2WA;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 298642
    invoke-direct {p0, p1}, LX/0Vx;-><init>(LX/2WA;)V

    .line 298643
    return-void
.end method

.method public static a(LX/0QB;)LX/1ia;
    .locals 4

    .prologue
    .line 298644
    sget-object v0, LX/1ia;->b:LX/1ia;

    if-nez v0, :cond_1

    .line 298645
    const-class v1, LX/1ia;

    monitor-enter v1

    .line 298646
    :try_start_0
    sget-object v0, LX/1ia;->b:LX/1ia;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 298647
    if-eqz v2, :cond_0

    .line 298648
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 298649
    new-instance p0, LX/1ia;

    invoke-static {v0}, LX/0Ws;->a(LX/0QB;)LX/2WA;

    move-result-object v3

    check-cast v3, LX/2WA;

    invoke-direct {p0, v3}, LX/1ia;-><init>(LX/2WA;)V

    .line 298650
    move-object v0, p0

    .line 298651
    sput-object v0, LX/1ia;->b:LX/1ia;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 298652
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 298653
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 298654
    :cond_1
    sget-object v0, LX/1ia;->b:LX/1ia;

    return-object v0

    .line 298655
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 298656
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298657
    const-string v0, "network_usage_counters"

    return-object v0
.end method
