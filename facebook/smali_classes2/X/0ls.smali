.class public final enum LX/0ls;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0ls;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0ls;

.field public static final enum AUTO_CLOSE_JSON_CONTENT:LX/0ls;

.field public static final enum AUTO_CLOSE_TARGET:LX/0ls;

.field public static final enum ESCAPE_NON_ASCII:LX/0ls;

.field public static final enum FLUSH_PASSED_TO_STREAM:LX/0ls;

.field public static final enum QUOTE_FIELD_NAMES:LX/0ls;

.field public static final enum QUOTE_NON_NUMERIC_NUMBERS:LX/0ls;

.field public static final enum WRITE_NUMBERS_AS_STRINGS:LX/0ls;


# instance fields
.field private final _defaultState:Z

.field private final _mask:I


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 130941
    new-instance v0, LX/0ls;

    const-string v1, "AUTO_CLOSE_TARGET"

    invoke-direct {v0, v1, v4, v3}, LX/0ls;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0ls;->AUTO_CLOSE_TARGET:LX/0ls;

    .line 130942
    new-instance v0, LX/0ls;

    const-string v1, "AUTO_CLOSE_JSON_CONTENT"

    invoke-direct {v0, v1, v3, v3}, LX/0ls;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0ls;->AUTO_CLOSE_JSON_CONTENT:LX/0ls;

    .line 130943
    new-instance v0, LX/0ls;

    const-string v1, "QUOTE_FIELD_NAMES"

    invoke-direct {v0, v1, v5, v3}, LX/0ls;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0ls;->QUOTE_FIELD_NAMES:LX/0ls;

    .line 130944
    new-instance v0, LX/0ls;

    const-string v1, "QUOTE_NON_NUMERIC_NUMBERS"

    invoke-direct {v0, v1, v6, v3}, LX/0ls;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0ls;->QUOTE_NON_NUMERIC_NUMBERS:LX/0ls;

    .line 130945
    new-instance v0, LX/0ls;

    const-string v1, "WRITE_NUMBERS_AS_STRINGS"

    invoke-direct {v0, v1, v7, v4}, LX/0ls;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0ls;->WRITE_NUMBERS_AS_STRINGS:LX/0ls;

    .line 130946
    new-instance v0, LX/0ls;

    const-string v1, "FLUSH_PASSED_TO_STREAM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/0ls;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0ls;->FLUSH_PASSED_TO_STREAM:LX/0ls;

    .line 130947
    new-instance v0, LX/0ls;

    const-string v1, "ESCAPE_NON_ASCII"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v4}, LX/0ls;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0ls;->ESCAPE_NON_ASCII:LX/0ls;

    .line 130948
    const/4 v0, 0x7

    new-array v0, v0, [LX/0ls;

    sget-object v1, LX/0ls;->AUTO_CLOSE_TARGET:LX/0ls;

    aput-object v1, v0, v4

    sget-object v1, LX/0ls;->AUTO_CLOSE_JSON_CONTENT:LX/0ls;

    aput-object v1, v0, v3

    sget-object v1, LX/0ls;->QUOTE_FIELD_NAMES:LX/0ls;

    aput-object v1, v0, v5

    sget-object v1, LX/0ls;->QUOTE_NON_NUMERIC_NUMBERS:LX/0ls;

    aput-object v1, v0, v6

    sget-object v1, LX/0ls;->WRITE_NUMBERS_AS_STRINGS:LX/0ls;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0ls;->FLUSH_PASSED_TO_STREAM:LX/0ls;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0ls;->ESCAPE_NON_ASCII:LX/0ls;

    aput-object v2, v0, v1

    sput-object v0, LX/0ls;->$VALUES:[LX/0ls;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 130937
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 130938
    const/4 v0, 0x1

    invoke-virtual {p0}, LX/0ls;->ordinal()I

    move-result v1

    shl-int/2addr v0, v1

    iput v0, p0, LX/0ls;->_mask:I

    .line 130939
    iput-boolean p3, p0, LX/0ls;->_defaultState:Z

    .line 130940
    return-void
.end method

.method public static collectDefaults()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 130932
    invoke-static {}, LX/0ls;->values()[LX/0ls;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 130933
    invoke-virtual {v4}, LX/0ls;->enabledByDefault()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 130934
    invoke-virtual {v4}, LX/0ls;->getMask()I

    move-result v4

    or-int/2addr v0, v4

    .line 130935
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 130936
    :cond_1
    return v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0ls;
    .locals 1

    .prologue
    .line 130931
    const-class v0, LX/0ls;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0ls;

    return-object v0
.end method

.method public static values()[LX/0ls;
    .locals 1

    .prologue
    .line 130930
    sget-object v0, LX/0ls;->$VALUES:[LX/0ls;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0ls;

    return-object v0
.end method


# virtual methods
.method public final enabledByDefault()Z
    .locals 1

    .prologue
    .line 130929
    iget-boolean v0, p0, LX/0ls;->_defaultState:Z

    return v0
.end method

.method public final getMask()I
    .locals 1

    .prologue
    .line 130928
    iget v0, p0, LX/0ls;->_mask:I

    return v0
.end method
