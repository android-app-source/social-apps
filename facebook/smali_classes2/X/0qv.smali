.class public LX/0qv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/facebook/graphql/model/FeedEdge;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0qw;

.field public c:Ljava/lang/String;

.field public d:Z

.field public e:Z

.field public f:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 148669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148670
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0qv;->a:Ljava/util/List;

    .line 148671
    sget-object v0, LX/0qw;->CHUNKED_REMAINDER:LX/0qw;

    iput-object v0, p0, LX/0qv;->b:LX/0qw;

    .line 148672
    iput-boolean v1, p0, LX/0qv;->d:Z

    .line 148673
    iput-boolean v1, p0, LX/0qv;->e:Z

    .line 148674
    iput-boolean v1, p0, LX/0qv;->f:Z

    .line 148675
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 148676
    iget-object v0, p0, LX/0qv;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 148677
    sget-object v0, LX/0qw;->CHUNKED_REMAINDER:LX/0qw;

    iput-object v0, p0, LX/0qv;->b:LX/0qw;

    .line 148678
    const/4 v0, 0x0

    iput-object v0, p0, LX/0qv;->c:Ljava/lang/String;

    .line 148679
    iput-boolean v1, p0, LX/0qv;->d:Z

    .line 148680
    iput-boolean v1, p0, LX/0qv;->e:Z

    .line 148681
    iput-boolean v1, p0, LX/0qv;->f:Z

    .line 148682
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 148683
    iget-object v0, p0, LX/0qv;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 148684
    iget-object v0, p0, LX/0qv;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148685
    const/4 v0, 0x0

    .line 148686
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0qv;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148687
    iget-object v0, p0, LX/0qv;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
