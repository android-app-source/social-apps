.class public final LX/1qF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public callback:LX/1qH;

.field public cancelled:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "BlueServiceQueue.this"
    .end annotation
.end field

.field public endTime:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "BlueServiceQueue.this"
    .end annotation
.end field

.field public future:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "BlueServiceQueue.this"
    .end annotation
.end field

.field public handlers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1qB;",
            ">;"
        }
    .end annotation
.end field

.field public final operation:LX/1qE;

.field private final queuedTime:J

.field public final requestState:LX/0zW;

.field public result:Lcom/facebook/fbservice/service/OperationResult;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "BlueServiceQueue.this"
    .end annotation
.end field

.field private startTime:J

.field private final statsCollector:LX/0cV;


# direct methods
.method public constructor <init>(LX/1qE;LX/0zW;LX/0cV;J)V
    .locals 2

    .prologue
    .line 330427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330428
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/1qF;->handlers:Ljava/util/List;

    .line 330429
    iput-object p1, p0, LX/1qF;->operation:LX/1qE;

    .line 330430
    iput-object p2, p0, LX/1qF;->requestState:LX/0zW;

    .line 330431
    iput-object p3, p0, LX/1qF;->statsCollector:LX/0cV;

    .line 330432
    iput-wide p4, p0, LX/1qF;->queuedTime:J

    .line 330433
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1qF;->cancelled:Z

    .line 330434
    return-void
.end method

.method public static synthetic access$702(LX/1qF;J)J
    .locals 1

    .prologue
    .line 330435
    iput-wide p1, p0, LX/1qF;->startTime:J

    return-wide p1
.end method

.method public static startTask(LX/1qF;)V
    .locals 1

    .prologue
    .line 330436
    iget-object v0, p0, LX/1qF;->statsCollector:LX/0cV;

    if-eqz v0, :cond_0

    .line 330437
    iget-object v0, p0, LX/1qF;->statsCollector:LX/0cV;

    invoke-interface {v0}, LX/0cV;->a()V

    .line 330438
    :cond_0
    return-void
.end method

.method public static stopTask(LX/1qF;)V
    .locals 2

    .prologue
    .line 330439
    iget-object v0, p0, LX/1qF;->statsCollector:LX/0cV;

    if-eqz v0, :cond_1

    .line 330440
    iget-object v0, p0, LX/1qF;->result:Lcom/facebook/fbservice/service/OperationResult;

    if-eqz v0, :cond_0

    .line 330441
    :cond_0
    iget-object v0, p0, LX/1qF;->statsCollector:LX/0cV;

    invoke-interface {v0}, LX/0cV;->b()V

    .line 330442
    :cond_1
    return-void
.end method


# virtual methods
.method public getElapsedQueuedTime()J
    .locals 4

    .prologue
    .line 330443
    iget-wide v0, p0, LX/1qF;->startTime:J

    iget-wide v2, p0, LX/1qF;->queuedTime:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must set startTime before invoking getElapsedQueuedTime"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 330444
    iget-wide v0, p0, LX/1qF;->startTime:J

    iget-wide v2, p0, LX/1qF;->queuedTime:J

    sub-long/2addr v0, v2

    return-wide v0

    .line 330445
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
