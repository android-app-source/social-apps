.class public LX/15a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile f:LX/15a;


# instance fields
.field private final b:LX/03V;

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/app/Activity;",
            "LX/2Co;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0Jg;",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/app/Activity;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 180904
    const-class v0, LX/15a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/15a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 180898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180899
    iput-object p1, p0, LX/15a;->b:LX/03V;

    .line 180900
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/15a;->c:Ljava/util/Map;

    .line 180901
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/15a;->d:Ljava/util/Map;

    .line 180902
    const/4 v0, 0x0

    iput-object v0, p0, LX/15a;->e:Landroid/app/Activity;

    .line 180903
    return-void
.end method

.method public static a(LX/0QB;)LX/15a;
    .locals 4

    .prologue
    .line 180905
    sget-object v0, LX/15a;->f:LX/15a;

    if-nez v0, :cond_1

    .line 180906
    const-class v1, LX/15a;

    monitor-enter v1

    .line 180907
    :try_start_0
    sget-object v0, LX/15a;->f:LX/15a;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 180908
    if-eqz v2, :cond_0

    .line 180909
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 180910
    new-instance p0, LX/15a;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/15a;-><init>(LX/03V;)V

    .line 180911
    move-object v0, p0

    .line 180912
    sput-object v0, LX/15a;->f:LX/15a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 180913
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 180914
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 180915
    :cond_1
    sget-object v0, LX/15a;->f:LX/15a;

    return-object v0

    .line 180916
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 180917
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 180893
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    .line 180894
    :cond_0
    iput-object p1, p0, LX/15a;->e:Landroid/app/Activity;

    .line 180895
    iget-object v0, p0, LX/15a;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 180896
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 180897
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 180887
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    .line 180888
    :cond_0
    iget-object v0, p0, LX/15a;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 180889
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 180890
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/15a;->e:Landroid/app/Activity;

    if-ne v0, p1, :cond_2

    .line 180891
    const/4 v0, 0x0

    iput-object v0, p0, LX/15a;->e:Landroid/app/Activity;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 180892
    :cond_2
    monitor-exit p0

    return-void
.end method
