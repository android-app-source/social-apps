.class public final LX/10h;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/0fK;


# direct methods
.method public constructor <init>(LX/0fK;)V
    .locals 0

    .prologue
    .line 169240
    iput-object p1, p0, LX/10h;->a:LX/0fK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 7

    .prologue
    .line 169241
    iget-object v0, p0, LX/10h;->a:LX/0fK;

    iget-object v0, v0, LX/0fK;->n:LX/GgR;

    if-eqz v0, :cond_0

    .line 169242
    iget-object v0, p0, LX/10h;->a:LX/0fK;

    iget-object v0, v0, LX/0fK;->n:LX/GgR;

    .line 169243
    iget-object v1, v0, LX/GgR;->a:LX/10n;

    iget-object v1, v1, LX/10n;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ArS;

    .line 169244
    iget-boolean v2, v1, LX/ArS;->f:Z

    move v1, v2

    .line 169245
    if-nez v1, :cond_1

    .line 169246
    :cond_0
    :goto_0
    return-void

    .line 169247
    :cond_1
    iget-object v1, v0, LX/GgR;->a:LX/10n;

    iget-object v1, v1, LX/10n;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ArS;

    sget-object v2, LX/ArR;->DRAG_DURATION:LX/ArR;

    iget-object v3, v0, LX/GgR;->a:LX/10n;

    iget-object v3, v3, LX/10n;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    iget-object v5, v0, LX/GgR;->a:LX/10n;

    iget-wide v5, v5, LX/10n;->j:J

    sub-long/2addr v3, v5

    long-to-double v3, v3

    const-wide v5, 0x408f400000000000L    # 1000.0

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    .line 169248
    invoke-static {v1}, LX/ArS;->f(LX/ArS;)V

    .line 169249
    iget-object v4, v1, LX/ArS;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0xb6000a

    invoke-virtual {v2}, LX/ArR;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v3}, LX/ArS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 169250
    goto :goto_0
.end method
