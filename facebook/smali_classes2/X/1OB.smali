.class public LX/1OB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0fu;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mOnDrawListeners"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 240442
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 240443
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/1OB;->a:Ljava/util/Set;

    .line 240444
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 240427
    iget-object v2, p0, LX/1OB;->a:Ljava/util/Set;

    monitor-enter v2

    .line 240428
    :try_start_0
    iget-object v0, p0, LX/1OB;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240429
    monitor-exit v2

    .line 240430
    :goto_0
    return-void

    .line 240431
    :cond_0
    const/4 v0, 0x0

    .line 240432
    iget-object v1, p0, LX/1OB;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fu;

    .line 240433
    invoke-interface {v0}, LX/0fu;->ko_()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 240434
    if-nez v1, :cond_2

    .line 240435
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v1

    .line 240436
    :cond_2
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 240437
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 240438
    :cond_3
    if-eqz v1, :cond_4

    .line 240439
    :try_start_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fu;

    .line 240440
    iget-object v3, p0, LX/1OB;->a:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    .line 240441
    :cond_4
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(LX/0fu;)V
    .locals 2

    .prologue
    .line 240424
    iget-object v1, p0, LX/1OB;->a:Ljava/util/Set;

    monitor-enter v1

    .line 240425
    :try_start_0
    iget-object v0, p0, LX/1OB;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 240426
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 240417
    iget-object v1, p0, LX/1OB;->a:Ljava/util/Set;

    monitor-enter v1

    .line 240418
    :try_start_0
    iget-object v0, p0, LX/1OB;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 240419
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(LX/0fu;)V
    .locals 2

    .prologue
    .line 240420
    iget-object v1, p0, LX/1OB;->a:Ljava/util/Set;

    monitor-enter v1

    .line 240421
    :try_start_0
    iget-object v0, p0, LX/1OB;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 240422
    iget-object v0, p0, LX/1OB;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 240423
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
