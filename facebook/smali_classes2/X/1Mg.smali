.class public LX/1Mg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final e:Lorg/apache/http/HttpHost;

.field private static final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile l:LX/1Mg;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/facebook/http/config/NetworkConfig$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0dN;

.field private final d:LX/0dx;

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Lorg/apache/http/HttpHost;

.field private final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 235929
    const/4 v0, 0x0

    sput-object v0, LX/1Mg;->e:Lorg/apache/http/HttpHost;

    .line 235930
    sget-object v0, LX/0dU;->j:LX/0Tn;

    sget-object v1, LX/0dU;->l:LX/0Tn;

    sget-object v2, LX/0dU;->r:LX/0Tn;

    sget-object v3, LX/0dU;->t:LX/0Tn;

    invoke-static {v0, v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/1Mg;->f:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0dx;)V
    .locals 3
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/IsSslPersistentCacheEnabled;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0dx;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 235931
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235932
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Mg;->g:Z

    .line 235933
    iput-boolean v1, p0, LX/1Mg;->h:Z

    .line 235934
    iput-boolean v1, p0, LX/1Mg;->i:Z

    .line 235935
    sget-object v0, LX/1Mg;->e:Lorg/apache/http/HttpHost;

    iput-object v0, p0, LX/1Mg;->j:Lorg/apache/http/HttpHost;

    .line 235936
    iput-object p1, p0, LX/1Mg;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 235937
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/1Mg;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 235938
    iput-object p2, p0, LX/1Mg;->k:LX/0Or;

    .line 235939
    iput-object p3, p0, LX/1Mg;->d:LX/0dx;

    .line 235940
    new-instance v0, LX/1Mh;

    invoke-direct {v0, p0}, LX/1Mh;-><init>(LX/1Mg;)V

    iput-object v0, p0, LX/1Mg;->c:LX/0dN;

    .line 235941
    iget-object v0, p0, LX/1Mg;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1Mg;->f:Ljava/util/Set;

    iget-object v2, p0, LX/1Mg;->c:LX/0dN;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;LX/0dN;)V

    .line 235942
    iget-object v0, p0, LX/1Mg;->d:LX/0dx;

    new-instance v1, LX/1Mi;

    invoke-direct {v1, p0}, LX/1Mi;-><init>(LX/1Mg;)V

    invoke-interface {v0, v1}, LX/0dx;->a(LX/0kr;)V

    .line 235943
    iget-object v0, p0, LX/1Mg;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235944
    invoke-static {p0}, LX/1Mg;->d(LX/1Mg;)V

    .line 235945
    :goto_0
    return-void

    .line 235946
    :cond_0
    iget-object v0, p0, LX/1Mg;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    new-instance v1, Lcom/facebook/http/config/DefaultNetworkConfig$3;

    invoke-direct {v1, p0}, Lcom/facebook/http/config/DefaultNetworkConfig$3;-><init>(LX/1Mg;)V

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1Mg;
    .locals 6

    .prologue
    .line 235947
    sget-object v0, LX/1Mg;->l:LX/1Mg;

    if-nez v0, :cond_1

    .line 235948
    const-class v1, LX/1Mg;

    monitor-enter v1

    .line 235949
    :try_start_0
    sget-object v0, LX/1Mg;->l:LX/1Mg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 235950
    if-eqz v2, :cond_0

    .line 235951
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 235952
    new-instance v5, LX/1Mg;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v4, 0x14c4

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0dt;->b(LX/0QB;)LX/0dx;

    move-result-object v4

    check-cast v4, LX/0dx;

    invoke-direct {v5, v3, p0, v4}, LX/1Mg;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0dx;)V

    .line 235953
    move-object v0, v5

    .line 235954
    sput-object v0, LX/1Mg;->l:LX/1Mg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235955
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 235956
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 235957
    :cond_1
    sget-object v0, LX/1Mg;->l:LX/1Mg;

    return-object v0

    .line 235958
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 235959
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static d(LX/1Mg;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 235960
    const/4 v0, 0x0

    .line 235961
    const/4 v2, 0x1

    .line 235962
    iget-object v3, p0, LX/1Mg;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 235963
    iget-object v3, p0, LX/1Mg;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0dU;->j:LX/0Tn;

    invoke-interface {v3, v4, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    .line 235964
    :cond_0
    move v2, v2

    .line 235965
    iget-boolean v3, p0, LX/1Mg;->g:Z

    if-eq v2, v3, :cond_1

    .line 235966
    iput-boolean v2, p0, LX/1Mg;->g:Z

    move v0, v1

    .line 235967
    :cond_1
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 235968
    iget-object v4, p0, LX/1Mg;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 235969
    const-string v4, "facebook.com"

    iget-object v5, p0, LX/1Mg;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/0dU;->r:LX/0Tn;

    const-string v7, "facebook.com"

    invoke-interface {v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    move v4, v2

    .line 235970
    :goto_0
    if-nez v4, :cond_a

    .line 235971
    iget-object v4, p0, LX/1Mg;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/0dU;->t:LX/0Tn;

    const-string v6, ""

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 235972
    :goto_1
    move v2, v2

    .line 235973
    iget-boolean v3, p0, LX/1Mg;->h:Z

    if-eq v2, v3, :cond_2

    .line 235974
    iput-boolean v2, p0, LX/1Mg;->h:Z

    move v0, v1

    .line 235975
    :cond_2
    const/4 v2, 0x0

    .line 235976
    iget-object v3, p0, LX/1Mg;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0dU;->l:LX/0Tn;

    invoke-interface {v3, v4, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 235977
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 235978
    :cond_3
    :goto_2
    move-object v2, v2

    .line 235979
    if-nez v2, :cond_4

    .line 235980
    iget-object v2, p0, LX/1Mg;->d:LX/0dx;

    invoke-interface {v2}, LX/0dy;->c()Lorg/apache/http/HttpHost;

    move-result-object v2

    .line 235981
    :cond_4
    iget-object v3, p0, LX/1Mg;->j:Lorg/apache/http/HttpHost;

    invoke-static {v3, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 235982
    iput-object v2, p0, LX/1Mg;->j:Lorg/apache/http/HttpHost;

    move v2, v1

    .line 235983
    :goto_3
    iget-object v0, p0, LX/1Mg;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 235984
    iget-boolean v3, p0, LX/1Mg;->i:Z

    if-eq v0, v3, :cond_6

    .line 235985
    iput-boolean v0, p0, LX/1Mg;->i:Z

    .line 235986
    :goto_4
    if-eqz v1, :cond_5

    .line 235987
    iget-object v0, p0, LX/1Mg;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2FQ;

    .line 235988
    invoke-virtual {v0}, LX/2FQ;->a()V

    goto :goto_5

    .line 235989
    :cond_5
    return-void

    :cond_6
    move v1, v2

    goto :goto_4

    :cond_7
    move v2, v0

    goto :goto_3

    :cond_8
    move v4, v3

    .line 235990
    goto :goto_0

    :cond_9
    move v2, v3

    .line 235991
    goto :goto_1

    :cond_a
    move v2, v4

    goto :goto_1

    :cond_b
    move v2, v3

    goto :goto_1

    .line 235992
    :cond_c
    const/16 v4, 0x3a

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 235993
    const/4 v5, -0x1

    if-eq v4, v5, :cond_3

    .line 235994
    add-int/lit8 v2, v4, 0x1

    invoke-virtual {v3, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 235995
    new-instance v2, Lorg/apache/http/HttpHost;

    const/4 v6, 0x0

    invoke-virtual {v3, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v5}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    goto :goto_2
.end method


# virtual methods
.method public final c()Z
    .locals 1

    .prologue
    .line 235996
    iget-boolean v0, p0, LX/1Mg;->h:Z

    return v0
.end method
