.class public abstract LX/0nA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public a:LX/2ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2ah",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/0nD;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field private c:Z

.field public d:Ljava/util/concurrent/atomic/AtomicInteger;

.field private e:LX/0nB;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Zh;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/0nD;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/0nA;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 134814
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134815
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/0nA;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 134816
    iput-boolean v1, p0, LX/0nA;->c:Z

    .line 134817
    return-void
.end method

.method private k()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 134829
    iget-object v0, p0, LX/0nA;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-eqz v0, :cond_0

    .line 134830
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Releasing object with non-zero refCount."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134831
    :cond_0
    invoke-virtual {p0}, LX/0nA;->d()V

    .line 134832
    iget-object v0, p0, LX/0nA;->f:LX/0Zh;

    if-eqz v0, :cond_1

    .line 134833
    iget-object v0, p0, LX/0nA;->f:LX/0Zh;

    .line 134834
    iget v2, v0, LX/0Zh;->d:I

    move v0, v2

    .line 134835
    invoke-virtual {p0, v0}, LX/0nA;->a(I)V

    .line 134836
    :cond_1
    invoke-virtual {p0}, LX/0nA;->h()V

    .line 134837
    invoke-virtual {p0}, LX/0nA;->f()V

    .line 134838
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0nA;->c:Z

    .line 134839
    iput-object v1, p0, LX/0nA;->g:LX/0nD;

    .line 134840
    iput-object v1, p0, LX/0nA;->h:LX/0nA;

    .line 134841
    iput-object v1, p0, LX/0nA;->a:LX/2ah;

    .line 134842
    iget-object v0, p0, LX/0nA;->f:LX/0Zh;

    if-eqz v0, :cond_2

    .line 134843
    iget-object v0, p0, LX/0nA;->e:LX/0nB;

    invoke-static {v0}, LX/0nB;->a(LX/0nB;)V

    .line 134844
    invoke-virtual {p0}, LX/0nA;->e()V

    .line 134845
    :cond_2
    return-void
.end method

.method public static m(LX/0nA;)V
    .locals 2

    .prologue
    .line 134826
    iget-boolean v0, p0, LX/0nA;->b:Z

    if-eqz v0, :cond_0

    .line 134827
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Internal bug, expected object to be immutable"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134828
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 134818
    iget-object v0, p0, LX/0nA;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 134819
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 134820
    :goto_0
    return-void

    .line 134821
    :cond_0
    if-gez v0, :cond_1

    .line 134822
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "release() has been called with refCount == 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134823
    :cond_1
    iget-object v0, p0, LX/0nA;->h:LX/0nA;

    if-eqz v0, :cond_2

    .line 134824
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to release, when added to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/0nA;->h:LX/0nA;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134825
    :cond_2
    invoke-direct {p0}, LX/0nA;->k()V

    goto :goto_0
.end method

.method public abstract a(I)V
.end method

.method public final a(LX/0Zh;)V
    .locals 4

    .prologue
    .line 134806
    iget-object v0, p0, LX/0nA;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 134807
    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 134808
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Acquired object with non-zero initial refCount current = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 134809
    :cond_0
    iput-object p1, p0, LX/0nA;->f:LX/0Zh;

    .line 134810
    iget-object v0, p0, LX/0nA;->e:LX/0nB;

    const-string v1, "release"

    invoke-static {v0, v1}, LX/0nB;->a(LX/0nB;Ljava/lang/String;)LX/0nB;

    move-result-object v0

    iput-object v0, p0, LX/0nA;->e:LX/0nB;

    .line 134811
    invoke-static {p0}, LX/0nA;->m(LX/0nA;)V

    .line 134812
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0nA;->b:Z

    .line 134813
    return-void
.end method

.method public final a(LX/0nA;)V
    .locals 0

    .prologue
    .line 134803
    invoke-virtual {p0}, LX/0nA;->c()V

    .line 134804
    iput-object p1, p0, LX/0nA;->h:LX/0nA;

    .line 134805
    return-void
.end method

.method public final a(LX/0nD;)V
    .locals 1

    .prologue
    .line 134800
    const-string v0, "encoder cannot be null!"

    invoke-static {p1, v0}, LX/0nE;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 134801
    iput-object p1, p0, LX/0nA;->g:LX/0nD;

    .line 134802
    return-void
.end method

.method public final a(Ljava/io/Writer;)V
    .locals 2

    .prologue
    .line 134796
    const-string v0, "Writer is null!"

    invoke-static {p1, v0}, LX/0nE;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 134797
    iget-object v0, p0, LX/0nA;->g:LX/0nD;

    const-string v1, "No encoder set, please call setEncoder() first!"

    invoke-static {v0, v1}, LX/0nE;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 134798
    iget-object v0, p0, LX/0nA;->g:LX/0nD;

    invoke-interface {v0, p1, p0}, LX/0nD;->a(Ljava/io/Writer;LX/0nA;)V

    .line 134799
    return-void
.end method

.method public final a(Ljava/io/Writer;LX/0nD;)V
    .locals 1

    .prologue
    .line 134846
    const-string v0, "Writer is null!"

    invoke-static {p1, v0}, LX/0nE;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 134847
    iget-object v0, p0, LX/0nA;->g:LX/0nD;

    if-eqz v0, :cond_0

    iget-object p2, p0, LX/0nA;->g:LX/0nD;

    .line 134848
    :cond_0
    const-string v0, "No encoder available"

    invoke-static {p2, v0}, LX/0nE;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 134849
    invoke-interface {p2, p1, p0}, LX/0nD;->a(Ljava/io/Writer;LX/0nA;)V

    .line 134850
    return-void
.end method

.method public final b(Ljava/lang/Class;I)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/0nD;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 134766
    iget-object v0, p0, LX/0nA;->a:LX/2ah;

    if-eqz v0, :cond_1

    .line 134767
    iget-object v0, p0, LX/0nA;->a:LX/2ah;

    .line 134768
    invoke-static {v0, p1}, LX/2ah;->a(LX/2ah;Ljava/lang/Object;)I

    move-result v1

    .line 134769
    if-ltz v1, :cond_0

    .line 134770
    iget-object p0, v0, LX/2ah;->b:[I

    aget p2, p0, v1

    .line 134771
    :cond_0
    move p2, p2

    .line 134772
    :cond_1
    return p2
.end method

.method public final b(LX/0nD;)LX/0nD;
    .locals 1

    .prologue
    .line 134773
    iget-object v0, p0, LX/0nA;->g:LX/0nD;

    if-eqz v0, :cond_0

    iget-object p1, p0, LX/0nA;->g:LX/0nD;

    :cond_0
    return-object p1
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 134774
    iget-object v0, p0, LX/0nA;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 134775
    if-ne v0, v1, :cond_0

    .line 134776
    iput-boolean v1, p0, LX/0nA;->c:Z

    .line 134777
    iput-object v2, p0, LX/0nA;->h:LX/0nA;

    .line 134778
    iput-object v2, p0, LX/0nA;->a:LX/2ah;

    .line 134779
    :goto_0
    return-void

    .line 134780
    :cond_0
    if-gez v0, :cond_1

    .line 134781
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "releaseFromParent() has been called with refCount == 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134782
    :cond_1
    invoke-direct {p0}, LX/0nA;->k()V

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 134783
    iget-boolean v0, p0, LX/0nA;->c:Z

    if-eqz v0, :cond_0

    .line 134784
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempting to re-attach a detached ParamsCollection"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134785
    :cond_0
    iget-object v0, p0, LX/0nA;->h:LX/0nA;

    if-eqz v0, :cond_1

    .line 134786
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Already added to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/0nA;->h:LX/0nA;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134787
    :cond_1
    return-void
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 134788
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0nA;->b:Z

    .line 134789
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 134790
    iget-boolean v0, p0, LX/0nA;->b:Z

    if-nez v0, :cond_0

    .line 134791
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Expected object to be mutable"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134792
    :cond_0
    return-void
.end method

.method public abstract h()V
.end method

.method public final i()LX/0Zh;
    .locals 1
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 134793
    iget-object v0, p0, LX/0nA;->f:LX/0Zh;

    .line 134794
    move-object v0, v0

    .line 134795
    check-cast v0, LX/0Zh;

    return-object v0
.end method
