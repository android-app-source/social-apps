.class public LX/1U9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/work/feedplugins/findgroups/FindGroupsGroupPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 255515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255516
    iput-object p1, p0, LX/1U9;->a:LX/0Ot;

    .line 255517
    return-void
.end method

.method public static a(LX/0QB;)LX/1U9;
    .locals 4

    .prologue
    .line 255500
    const-class v1, LX/1U9;

    monitor-enter v1

    .line 255501
    :try_start_0
    sget-object v0, LX/1U9;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 255502
    sput-object v2, LX/1U9;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 255503
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255504
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 255505
    new-instance v3, LX/1U9;

    const/16 p0, 0x38bd

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1U9;-><init>(LX/0Ot;)V

    .line 255506
    move-object v0, v3

    .line 255507
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 255508
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1U9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 255509
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 255510
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 255513
    sget-object v0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255514
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 255511
    const-class v0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;

    iget-object v1, p0, LX/1U9;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 255512
    return-void
.end method
