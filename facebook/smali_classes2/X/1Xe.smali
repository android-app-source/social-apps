.class public final LX/1Xe;
.super LX/1Xf;
.source ""


# instance fields
.field public e:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "LX/0lF;",
            ">;>;"
        }
    .end annotation
.end field

.field public f:Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "LX/0lF;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z


# direct methods
.method public constructor <init>(LX/0lF;LX/1Xf;)V
    .locals 1

    .prologue
    .line 271537
    const/4 v0, 0x2

    invoke-direct {p0, v0, p2}, LX/1Xf;-><init>(ILX/1Xf;)V

    .line 271538
    check-cast p1, LX/0m9;

    invoke-virtual {p1}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, LX/1Xe;->e:Ljava/util/Iterator;

    .line 271539
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Xe;->g:Z

    .line 271540
    return-void
.end method


# virtual methods
.method public final synthetic a()LX/12V;
    .locals 1

    .prologue
    .line 271541
    iget-object v0, p0, LX/1Xf;->c:LX/1Xf;

    move-object v0, v0

    .line 271542
    return-object v0
.end method

.method public final j()LX/15z;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 271543
    iget-boolean v0, p0, LX/1Xe;->g:Z

    if-eqz v0, :cond_2

    .line 271544
    iget-object v0, p0, LX/1Xe;->e:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 271545
    iput-object v1, p0, LX/1Xe;->d:Ljava/lang/String;

    .line 271546
    iput-object v1, p0, LX/1Xe;->f:Ljava/util/Map$Entry;

    .line 271547
    :goto_0
    return-object v1

    .line 271548
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Xe;->g:Z

    .line 271549
    iget-object v0, p0, LX/1Xe;->e:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, LX/1Xe;->f:Ljava/util/Map$Entry;

    .line 271550
    iget-object v0, p0, LX/1Xe;->f:Ljava/util/Map$Entry;

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    iput-object v0, p0, LX/1Xe;->d:Ljava/lang/String;

    .line 271551
    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    goto :goto_0

    .line 271552
    :cond_1
    iget-object v0, p0, LX/1Xe;->f:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_1

    .line 271553
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Xe;->g:Z

    .line 271554
    iget-object v0, p0, LX/1Xe;->f:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-virtual {v0}, LX/0lF;->a()LX/15z;

    move-result-object v1

    goto :goto_0
.end method

.method public final k()LX/15z;
    .locals 1

    .prologue
    .line 271555
    sget-object v0, LX/15z;->END_OBJECT:LX/15z;

    return-object v0
.end method

.method public final l()LX/0lF;
    .locals 1

    .prologue
    .line 271556
    iget-object v0, p0, LX/1Xe;->f:Ljava/util/Map$Entry;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1Xe;->f:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    goto :goto_0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 271557
    invoke-virtual {p0}, LX/1Xe;->l()LX/0lF;

    move-result-object v0

    check-cast v0, LX/0mA;

    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
