.class public final LX/1jB;
.super LX/0eW;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadArgumentPlacement",
        "BadClosingBracePlacement",
        "YodaConditions"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 300041
    invoke-direct {p0}, LX/0eW;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 300042
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-static {v1, v0}, LX/1jQ;->a(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 300043
    const/4 v1, 0x6

    invoke-virtual {p0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v2, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v3, p0, LX/0eW;->a:I

    add-int/2addr v1, v3

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 300044
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 3

    .prologue
    .line 300048
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()I
    .locals 3

    .prologue
    .line 300045
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()I
    .locals 3

    .prologue
    .line 300046
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 3

    .prologue
    .line 300047
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()I
    .locals 3

    .prologue
    .line 300040
    const/16 v0, 0x12

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()I
    .locals 3

    .prologue
    .line 300031
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()J
    .locals 3

    .prologue
    .line 300039
    const/16 v0, 0x16

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 300032
    const/16 v1, 0x18

    invoke-virtual {p0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v2, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v3, p0, LX/0eW;->a:I

    add-int/2addr v1, v3

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final l()J
    .locals 3

    .prologue
    .line 300033
    const/16 v0, 0x1a

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final m()J
    .locals 3

    .prologue
    .line 300034
    const/16 v0, 0x1c

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final n()J
    .locals 3

    .prologue
    .line 300035
    const/16 v0, 0x1e

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final o()J
    .locals 3

    .prologue
    .line 300036
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final p()J
    .locals 3

    .prologue
    .line 300037
    const/16 v0, 0x22

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final q()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 300038
    const/16 v1, 0x24

    invoke-virtual {p0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v2, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v3, p0, LX/0eW;->a:I

    add-int/2addr v1, v3

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
