.class public LX/0Wm;
.super LX/0RV;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ExplicitComplexProvider",
        "FbInjectorGet"
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0RV",
        "<",
        "Lcom/facebook/inject/AssistedProvider",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private volatile a:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 76667
    invoke-direct {p0}, LX/0RV;-><init>()V

    .line 76668
    iput-object p1, p0, LX/0Wm;->b:Ljava/lang/Class;

    .line 76669
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 76670
    if-eqz v0, :cond_0

    .line 76671
    invoke-static {p0}, LX/0Wm;->c(LX/0Wm;)Ljava/lang/reflect/Constructor;

    .line 76672
    :cond_0
    return-void
.end method

.method private b()LX/0Wl;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 76673
    :try_start_0
    invoke-static {p0}, LX/0Wm;->c(LX/0Wm;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Wl;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    return-object v0

    .line 76674
    :catch_0
    move-exception v0

    .line 76675
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 76676
    :catch_1
    move-exception v0

    .line 76677
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 76678
    :catch_2
    move-exception v0

    .line 76679
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method private static c(LX/0Wm;)Ljava/lang/reflect/Constructor;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/reflect/Constructor",
            "<",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 76680
    iget-object v0, p0, LX/0Wm;->a:Ljava/lang/reflect/Constructor;

    .line 76681
    if-nez v0, :cond_0

    .line 76682
    :try_start_0
    iget-object v0, p0, LX/0Wm;->b:Ljava/lang/Class;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 76683
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 76684
    iput-object v0, p0, LX/0Wm;->a:Ljava/lang/reflect/Constructor;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76685
    :cond_0
    return-object v0

    .line 76686
    :catch_0
    move-exception v0

    .line 76687
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Assisted provider "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/0Wm;->b:Ljava/lang/Class;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " doesn\'t have default constructor."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a()LX/0Wl;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 76688
    invoke-direct {p0}, LX/0Wm;->b()LX/0Wl;

    move-result-object v1

    .line 76689
    instance-of v0, v1, LX/0Wl;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 76690
    check-cast v0, LX/0Wl;

    .line 76691
    invoke-virtual {p0}, LX/0RV;->getScopeAwareInjector()LX/0R6;

    move-result-object v2

    check-cast v2, LX/0QA;

    .line 76692
    iput-object v2, v0, LX/0Wl;->mInjector:LX/0QB;

    .line 76693
    :cond_0
    return-object v1
.end method

.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 76694
    invoke-virtual {p0}, LX/0Wm;->a()LX/0Wl;

    move-result-object v0

    return-object v0
.end method
