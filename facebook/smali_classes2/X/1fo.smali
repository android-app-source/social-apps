.class public LX/1fo;
.super Ljava/util/concurrent/AbstractExecutorService;
.source ""


# static fields
.field public static final a:LX/1fo;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 292352
    new-instance v0, LX/1fo;

    invoke-direct {v0}, LX/1fo;-><init>()V

    sput-object v0, LX/1fo;->a:LX/1fo;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 292350
    invoke-direct {p0}, Ljava/util/concurrent/AbstractExecutorService;-><init>()V

    .line 292351
    return-void
.end method


# virtual methods
.method public final awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .locals 1

    .prologue
    .line 292349
    const/4 v0, 0x1

    return v0
.end method

.method public final execute(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 292347
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 292348
    return-void
.end method

.method public final isShutdown()Z
    .locals 1

    .prologue
    .line 292353
    const/4 v0, 0x0

    return v0
.end method

.method public final isTerminated()Z
    .locals 1

    .prologue
    .line 292346
    const/4 v0, 0x0

    return v0
.end method

.method public final shutdown()V
    .locals 0

    .prologue
    .line 292345
    return-void
.end method

.method public final shutdownNow()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 292343
    invoke-virtual {p0}, LX/1fo;->shutdown()V

    .line 292344
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
