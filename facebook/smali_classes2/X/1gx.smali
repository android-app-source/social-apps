.class public final LX/1gx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0tW;
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0tW;",
        "LX/0rl",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1gv;

.field private b:LX/1J0;

.field public c:Lcom/facebook/api/feed/FetchFeedParams;

.field public d:Z

.field public e:Z

.field public f:I

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Lcom/facebook/graphql/model/GraphQLPageInfo;


# direct methods
.method public constructor <init>(LX/1gv;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 295275
    iput-object p1, p0, LX/1gx;->a:LX/1gv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 295276
    const/4 v0, 0x0

    iput v0, p0, LX/1gx;->f:I

    .line 295277
    iput-object v1, p0, LX/1gx;->g:Ljava/lang/String;

    .line 295278
    iput-object v1, p0, LX/1gx;->h:Ljava/lang/String;

    .line 295279
    iput-object v1, p0, LX/1gx;->i:Ljava/lang/String;

    .line 295280
    iput-object v1, p0, LX/1gx;->j:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 295281
    return-void
.end method

.method private static a(LX/1J0;Z)I
    .locals 2

    .prologue
    .line 295446
    iget-object v0, p0, LX/1J0;->h:LX/0gf;

    invoke-virtual {v0}, LX/0gf;->isNewStoriesFetch()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 295447
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 295448
    :goto_0
    return v0

    .line 295449
    :cond_0
    const/4 v0, 0x6

    goto :goto_0

    .line 295450
    :cond_1
    iget-object v0, p0, LX/1J0;->h:LX/0gf;

    sget-object v1, LX/0gf;->ONE_WAY_FEED_BACK:LX/0gf;

    if-ne v0, v1, :cond_2

    .line 295451
    const/16 v0, 0xc

    goto :goto_0

    .line 295452
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;)LX/1pU;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 295428
    new-instance v0, Lcom/facebook/api/feed/FetchFeedResult;

    iget-object v1, p0, LX/1gx;->c:Lcom/facebook/api/feed/FetchFeedParams;

    sget-object v3, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v2, p0, LX/1gx;->b:LX/1J0;

    iget-wide v4, v2, LX/1J0;->e:J

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/facebook/api/feed/FetchFeedResult;-><init>(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;JZ)V

    .line 295429
    iget-object v1, p0, LX/1gx;->a:LX/1gv;

    iget-object v1, v1, LX/1gv;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0pm;

    invoke-virtual {v1, v0}, LX/0pm;->b(Lcom/facebook/api/feed/FetchFeedResult;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v1

    .line 295430
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pm;

    invoke-static {v1}, LX/0pn;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 295431
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/187;

    invoke-virtual {v0, v1}, LX/187;->a(Lcom/facebook/api/feed/FetchFeedResult;)LX/0Px;

    move-result-object v2

    .line 295432
    invoke-virtual {v1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v6

    .line 295433
    :goto_0
    const/4 v3, 0x0

    .line 295434
    iget-object v4, p0, LX/1gx;->a:LX/1gv;

    iget-object v4, v4, LX/1gv;->e:LX/0ad;

    sget-short v5, LX/0fe;->S:S

    invoke-interface {v4, v5, v3}, LX/0ad;->a(SZ)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, LX/1gx;->a:LX/1gv;

    iget-object v4, v4, LX/1gv;->e:LX/0ad;

    sget-short v5, LX/0fe;->f:S

    invoke-interface {v4, v5, v3}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v3, 0x1

    :cond_1
    move v3, v3

    .line 295435
    if-eqz v3, :cond_3

    if-eqz v2, :cond_3

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    if-ne v3, v0, :cond_3

    const/4 v0, 0x1

    .line 295436
    :goto_1
    if-eqz v0, :cond_4

    .line 295437
    new-instance v0, LX/1pU;

    invoke-direct {v0, p0, v1, v2}, LX/1pU;-><init>(LX/1gx;Lcom/facebook/api/feed/FetchFeedResult;LX/0Px;)V

    .line 295438
    :goto_2
    return-object v0

    .line 295439
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0

    :cond_3
    move v0, v6

    .line 295440
    goto :goto_1

    .line 295441
    :cond_4
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 295442
    invoke-virtual {v1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    :goto_3
    if-ge v6, v4, :cond_5

    invoke-virtual {v3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 295443
    invoke-static {v0}, LX/0x0;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Lcom/facebook/feed/model/ClientFeedUnitEdge;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 295444
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 295445
    :cond_5
    new-instance v0, LX/1pU;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, LX/1pU;-><init>(LX/1gx;Lcom/facebook/api/feed/FetchFeedResult;LX/0Px;)V

    goto :goto_2
.end method

.method private static b(Lcom/facebook/api/feed/FetchFeedParams;)Z
    .locals 1

    .prologue
    .line 295426
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 295427
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()I
    .locals 2

    .prologue
    .line 295420
    iget-object v0, p0, LX/1gx;->b:LX/1J0;

    iget-object v0, v0, LX/1J0;->h:LX/0gf;

    invoke-virtual {v0}, LX/0gf;->isNewStoriesFetch()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295421
    const/4 v0, 0x7

    .line 295422
    :goto_0
    return v0

    .line 295423
    :cond_0
    iget-object v0, p0, LX/1gx;->b:LX/1J0;

    iget-object v0, v0, LX/1J0;->h:LX/0gf;

    sget-object v1, LX/0gf;->ONE_WAY_FEED_BACK:LX/0gf;

    if-ne v0, v1, :cond_1

    .line 295424
    const/16 v0, 0xd

    goto :goto_0

    .line 295425
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 295417
    sget-object v0, LX/0gf;->ONE_WAY_FEED_BACK:LX/0gf;

    iget-object v1, p0, LX/1gx;->c:Lcom/facebook/api/feed/FetchFeedParams;

    .line 295418
    iget-object p0, v1, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v1, p0

    .line 295419
    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 295416
    iget-object v0, p0, LX/1gx;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1gx;->j:Lcom/facebook/graphql/model/GraphQLPageInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1gx;->j:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0rl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0rl",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 295412
    const-string v0, "feedback_subscriber"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295413
    const/4 p0, 0x0

    .line 295414
    :goto_0
    return-object p0

    .line 295415
    :cond_0
    const-string v0, "feed_subscriber"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    goto :goto_0
.end method

.method public final a()V
    .locals 5

    .prologue
    .line 295370
    const-string v0, "NetworkRequestSubscriber.onCompleted"

    const v1, 0x6b48c3c7

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 295371
    :try_start_0
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pV;

    sget-object v1, LX/1gv;->b:Ljava/lang/String;

    sget-object v2, LX/1gs;->NETWORK_SUCCESS:LX/1gs;

    const-string v3, "inUse"

    iget-boolean v4, p0, LX/1gx;->e:Z

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0pV;->a(Ljava/lang/String;LX/1gt;Ljava/lang/String;Ljava/lang/String;)V

    .line 295372
    iget-boolean v0, p0, LX/1gx;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 295373
    const v0, 0x10c888bf

    invoke-static {v0}, LX/02m;->a(I)V

    .line 295374
    :goto_0
    return-void

    .line 295375
    :cond_0
    :try_start_1
    invoke-direct {p0}, LX/1gx;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 295376
    iget-object v0, p0, LX/1gx;->c:Lcom/facebook/api/feed/FetchFeedParams;

    invoke-static {v0}, LX/1gx;->b(Lcom/facebook/api/feed/FetchFeedParams;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 295377
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v1, v0, LX/1gv;->g:LX/0jY;

    iget-object v0, p0, LX/1gx;->c:Lcom/facebook/api/feed/FetchFeedParams;

    .line 295378
    iget-object v2, v0, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v2, v2

    .line 295379
    iget-object v0, p0, LX/1gx;->g:Ljava/lang/String;

    if-nez v0, :cond_6

    iget-object v0, p0, LX/1gx;->c:Lcom/facebook/api/feed/FetchFeedParams;

    .line 295380
    iget-object v3, v0, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v0, v3

    .line 295381
    :goto_1
    invoke-virtual {v1, v2, v0}, LX/0jY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 295382
    :cond_1
    :goto_2
    invoke-direct {p0}, LX/1gx;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 295383
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->g:LX/0jY;

    iget-object v1, p0, LX/1gx;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0jY;->a(Ljava/lang/String;)V

    .line 295384
    :cond_2
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->f:LX/1gi;

    iget v1, p0, LX/1gx;->f:I

    invoke-direct {p0}, LX/1gx;->c()I

    move-result v2

    .line 295385
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1, v2}, LX/1gi;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/1gi;->sendMessage(Landroid/os/Message;)Z

    .line 295386
    iget-boolean v0, p0, LX/1gx;->d:Z

    if-nez v0, :cond_3

    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->e:LX/0ad;

    sget-short v1, LX/0fe;->ai:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 295387
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rW;

    iget-object v1, p0, LX/1gx;->c:Lcom/facebook/api/feed/FetchFeedParams;

    .line 295388
    iget-object v2, v1, Lcom/facebook/api/feed/FetchFeedParams;->i:LX/0Px;

    move-object v1, v2

    .line 295389
    iget-object v2, p0, LX/1gx;->c:Lcom/facebook/api/feed/FetchFeedParams;

    .line 295390
    iget-object v3, v2, Lcom/facebook/api/feed/FetchFeedParams;->j:LX/0Px;

    move-object v2, v3

    .line 295391
    invoke-virtual {v0, v1, v2}, LX/0rW;->a(LX/0Px;LX/0Px;)V

    .line 295392
    :cond_3
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    invoke-virtual {v0, p0}, LX/1gv;->a(LX/1gx;)V

    .line 295393
    iget-object v0, p0, LX/1gx;->b:LX/1J0;

    iget-object v0, v0, LX/1J0;->h:LX/0gf;

    invoke-virtual {v0}, LX/0gf;->isNewStoriesFetch()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 295394
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ys;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0ys;->a(Z)V

    .line 295395
    :cond_4
    iget v0, p0, LX/1gx;->f:I

    if-nez v0, :cond_5

    .line 295396
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0r3;

    invoke-virtual {v0}, LX/0r3;->b()V

    .line 295397
    sget-object v0, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-virtual {v0}, LX/0ta;->name()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/1gx;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/1gx;->c:Lcom/facebook/api/feed/FetchFeedParams;

    invoke-static {v0}, LX/1gx;->b(Lcom/facebook/api/feed/FetchFeedParams;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 295398
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0r3;

    invoke-virtual {v0}, LX/0r3;->d()V

    .line 295399
    :cond_5
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0r3;

    iget-object v1, p0, LX/1gx;->b:LX/1J0;

    iget-object v1, v1, LX/1J0;->h:LX/0gf;

    invoke-virtual {v1}, LX/0gf;->name()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, LX/1gx;->b:LX/1J0;

    iget-object v1, v1, LX/1J0;->a:LX/0rS;

    if-eqz v1, :cond_8

    iget-object v1, p0, LX/1gx;->b:LX/1J0;

    iget-object v1, v1, LX/1J0;->a:LX/0rS;

    invoke-virtual {v1}, LX/0rS;->name()Ljava/lang/String;

    move-result-object v1

    :goto_3
    iget-object v3, p0, LX/1gx;->i:Ljava/lang/String;

    iget v4, p0, LX/1gx;->f:I

    invoke-virtual {v0, v2, v1, v3, v4}, LX/0r3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 295400
    const v0, -0x2db7f40b

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 295401
    :cond_6
    :try_start_2
    iget-object v0, p0, LX/1gx;->g:Ljava/lang/String;

    goto/16 :goto_1

    .line 295402
    :cond_7
    iget-object v0, p0, LX/1gx;->c:Lcom/facebook/api/feed/FetchFeedParams;

    .line 295403
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 295404
    if-eqz v1, :cond_9

    .line 295405
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v1, v1

    .line 295406
    if-nez v1, :cond_9

    const/4 v1, 0x1

    :goto_4
    move v0, v1

    .line 295407
    if-eqz v0, :cond_1

    .line 295408
    iget-object v0, p0, LX/1gx;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1gx;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 295409
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->g:LX/0jY;

    iget-object v1, p0, LX/1gx;->h:Ljava/lang/String;

    iget-object v2, p0, LX/1gx;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0jY;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 295410
    :catchall_0
    move-exception v0

    const v1, 0x462d4c47

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 295411
    :cond_8
    const/4 v1, 0x0

    goto :goto_3

    :cond_9
    const/4 v1, 0x0

    goto :goto_4
.end method

.method public final a(LX/1J0;Lcom/facebook/api/feed/FetchFeedParams;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 295360
    iput-object p1, p0, LX/1gx;->b:LX/1J0;

    .line 295361
    iput-object p2, p0, LX/1gx;->c:Lcom/facebook/api/feed/FetchFeedParams;

    .line 295362
    iput-boolean p3, p0, LX/1gx;->d:Z

    .line 295363
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1gx;->e:Z

    .line 295364
    const/4 v0, 0x0

    iput v0, p0, LX/1gx;->f:I

    .line 295365
    iput-object v1, p0, LX/1gx;->g:Ljava/lang/String;

    .line 295366
    iput-object v1, p0, LX/1gx;->h:Ljava/lang/String;

    .line 295367
    iput-object v1, p0, LX/1gx;->j:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 295368
    iput-object v1, p0, LX/1gx;->i:Ljava/lang/String;

    .line 295369
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 295308
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 295309
    const-string v0, "NetworkRequestSubscriber.onNext"

    const v2, 0x6f1dd90f

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 295310
    :try_start_0
    iget-boolean v0, p0, LX/1gx;->e:Z

    if-nez v0, :cond_0

    .line 295311
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pV;

    sget-object v1, LX/1gv;->b:Ljava/lang/String;

    sget-object v2, LX/1gs;->NETWORK_NEXT_NOT_IN_USE:LX/1gs;

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;LX/1gt;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295312
    const v0, -0x18f3c35c

    invoke-static {v0}, LX/02m;->a(I)V

    .line 295313
    :goto_0
    return-void

    .line 295314
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1gx;->b:LX/1J0;

    iget-object v0, v0, LX/1J0;->c:LX/0rB;

    invoke-virtual {v0}, LX/0rB;->b()LX/0rn;

    move-result-object v0

    iget-object v2, p0, LX/1gx;->c:Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {v0, v2, p1}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v3

    .line 295315
    if-nez v3, :cond_1

    .line 295316
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pV;

    sget-object v1, LX/1gv;->b:Ljava/lang/String;

    sget-object v2, LX/1gs;->NETWORK_NEXT_IS_NULL:LX/1gs;

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;LX/1gt;)V

    .line 295317
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0r3;

    const-string v1, "fetch_null_result"

    invoke-virtual {v0, v1}, LX/0r3;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 295318
    const v0, 0x1d4268ea

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 295319
    :cond_1
    :try_start_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x64

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 295320
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_2

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 295321
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ", "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295322
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 295323
    :cond_2
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pV;

    sget-object v2, LX/1gv;->b:Ljava/lang/String;

    sget-object v5, LX/1gs;->NETWORK_NEXT:LX/1gs;

    const-string v6, "cursorReturned"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v5, v6, v4}, LX/0pV;->a(Ljava/lang/String;LX/1gt;Ljava/lang/String;Ljava/lang/String;)V

    .line 295324
    invoke-direct {p0, v3}, LX/1gx;->a(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;)LX/1pU;

    move-result-object v2

    .line 295325
    iget-object v0, v2, LX/1pU;->b:Lcom/facebook/api/feed/FetchFeedResult;

    move-object v4, v0

    .line 295326
    iget-object v0, v4, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 295327
    if-eqz v0, :cond_6

    .line 295328
    iget-object v0, v4, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 295329
    invoke-virtual {v0}, LX/0ta;->name()Ljava/lang/String;

    move-result-object v0

    :goto_2
    iput-object v0, p0, LX/1gx;->i:Ljava/lang/String;

    .line 295330
    iget-object v0, v2, LX/1pU;->c:LX/0Px;

    move-object v2, v0

    .line 295331
    iget-object v5, p0, LX/1gx;->b:LX/1J0;

    iget v0, p0, LX/1gx;->f:I

    if-nez v0, :cond_7

    const/4 v0, 0x1

    :goto_3
    invoke-static {v5, v0}, LX/1gx;->a(LX/1J0;Z)I

    move-result v5

    .line 295332
    iget v0, p0, LX/1gx;->f:I

    if-nez v0, :cond_3

    .line 295333
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v6, v0, LX/1gv;->f:LX/1gi;

    if-nez v2, :cond_8

    move v0, v1

    .line 295334
    :goto_4
    const/16 v7, 0x9

    invoke-virtual {v6, v7, v5, v0}, LX/1gi;->obtainMessage(III)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/1gi;->sendMessage(Landroid/os/Message;)Z

    .line 295335
    :cond_3
    if-nez v5, :cond_4

    .line 295336
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qf;

    invoke-virtual {v0, v4}, LX/0qf;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 295337
    :cond_4
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    :goto_5
    if-ge v1, v4, :cond_9

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 295338
    iget v6, p0, LX/1gx;->f:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, LX/1gx;->f:I

    .line 295339
    iget-object v6, p0, LX/1gx;->h:Ljava/lang/String;

    if-nez v6, :cond_5

    .line 295340
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, LX/1gx;->h:Ljava/lang/String;

    .line 295341
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1gx;->g:Ljava/lang/String;

    .line 295342
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 295343
    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    :cond_7
    move v0, v1

    .line 295344
    goto :goto_3

    .line 295345
    :cond_8
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    goto :goto_4

    .line 295346
    :cond_9
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 295347
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    iput-object v0, p0, LX/1gx;->j:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 295348
    :cond_a
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    iget v0, p0, LX/1gx;->f:I

    if-nez v0, :cond_c

    if-nez v5, :cond_c

    .line 295349
    :cond_b
    const/16 v0, 0xc

    if-ne v5, v0, :cond_d

    .line 295350
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->g:LX/0jY;

    .line 295351
    const/16 v1, 0xe

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v5, v3, v2}, LX/0jY;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jY;->sendMessage(Landroid/os/Message;)Z

    .line 295352
    :goto_6
    invoke-direct {p0}, LX/1gx;->d()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 295353
    iget-object v0, p0, LX/1gx;->h:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, LX/1gx;->g:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 295354
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->g:LX/0jY;

    iget-object v1, p0, LX/1gx;->h:Ljava/lang/String;

    iget-object v2, p0, LX/1gx;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0jY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 295355
    invoke-direct {p0}, LX/1gx;->e()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 295356
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->g:LX/0jY;

    iget-object v1, p0, LX/1gx;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0jY;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 295357
    :cond_c
    const v0, -0x4970a102

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 295358
    :cond_d
    :try_start_3
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->g:LX/0jY;

    invoke-virtual {v0, v2, v5}, LX/0jY;->a(LX/0Px;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_6

    .line 295359
    :catchall_0
    move-exception v0

    const v1, -0x3e664244

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 295282
    const-string v0, "NetworkRequestSubscriber.onError"

    const v2, -0x43cdd24b

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 295283
    :try_start_0
    sget-object v0, LX/1gv;->b:Ljava/lang/String;

    const-string v2, "On Network Error"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, p1, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 295284
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pV;

    sget-object v2, LX/1gv;->b:Ljava/lang/String;

    sget-object v3, LX/1gs;->NETWORK_ERROR:LX/1gs;

    const-string v4, "msg"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v2, v3, v4, v5}, LX/0pV;->a(Ljava/lang/String;LX/1gt;Ljava/lang/String;Ljava/lang/String;)V

    .line 295285
    iget-boolean v0, p0, LX/1gx;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 295286
    const v0, -0x4d178fe7

    invoke-static {v0}, LX/02m;->a(I)V

    .line 295287
    :goto_0
    return-void

    .line 295288
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pW;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, LX/1gx;->c:Lcom/facebook/api/feed/FetchFeedParams;

    .line 295289
    iget-object v5, v4, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v4, v5

    .line 295290
    invoke-virtual {v0, v2, v3, v4}, LX/0pW;->a(Ljava/lang/String;ZLcom/facebook/api/feedtype/FeedType;)V

    .line 295291
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->f:LX/1gi;

    invoke-direct {p0}, LX/1gx;->c()I

    move-result v2

    iget-object v3, p0, LX/1gx;->b:LX/1J0;

    iget-object v3, v3, LX/1J0;->h:LX/0gf;

    .line 295292
    const/4 v4, 0x2

    new-instance v5, LX/AjU;

    invoke-direct {v5, v2, v3, p1}, LX/AjU;-><init>(ILX/0gf;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v4, v5}, LX/1gi;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/1gi;->sendMessage(Landroid/os/Message;)Z

    .line 295293
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    invoke-virtual {v0, p0}, LX/1gv;->a(LX/1gx;)V

    .line 295294
    iget-object v0, p0, LX/1gx;->b:LX/1J0;

    iget-object v0, v0, LX/1J0;->h:LX/0gf;

    invoke-virtual {v0}, LX/0gf;->isNewStoriesFetch()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 295295
    iget v0, p0, LX/1gx;->f:I

    if-nez v0, :cond_1

    .line 295296
    iget-object v0, p0, LX/1gx;->b:LX/1J0;

    const/4 v2, 0x1

    invoke-static {v0, v2}, LX/1gx;->a(LX/1J0;Z)I

    move-result v0

    .line 295297
    iget-object v2, p0, LX/1gx;->a:LX/1gv;

    iget-object v2, v2, LX/1gv;->g:LX/0jY;

    .line 295298
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 295299
    invoke-virtual {v2, v3, v0}, LX/0jY;->a(LX/0Px;I)V

    .line 295300
    :cond_1
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ys;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0ys;->a(Z)V

    .line 295301
    :cond_2
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0r3;

    iget-object v2, p0, LX/1gx;->b:LX/1J0;

    iget-object v2, v2, LX/1J0;->h:LX/0gf;

    invoke-virtual {v2}, LX/0gf;->name()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/1gx;->b:LX/1J0;

    iget-object v3, v3, LX/1J0;->a:LX/0rS;

    if-eqz v3, :cond_3

    iget-object v1, p0, LX/1gx;->b:LX/1J0;

    iget-object v1, v1, LX/1J0;->a:LX/0rS;

    invoke-virtual {v1}, LX/0rS;->name()Ljava/lang/String;

    move-result-object v1

    :cond_3
    iget-object v3, p0, LX/1gx;->i:Ljava/lang/String;

    iget v4, p0, LX/1gx;->f:I

    invoke-virtual {v0, v2, v1, v3, v4}, LX/0r3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 295302
    invoke-static {p1}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v1

    .line 295303
    sget-object v0, LX/1nY;->CANCELLED:LX/1nY;

    if-ne v1, v0, :cond_4

    .line 295304
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0r3;

    invoke-virtual {v0}, LX/0r3;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 295305
    :goto_1
    const v0, -0xa4dc389

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 295306
    :cond_4
    :try_start_2
    iget-object v0, p0, LX/1gx;->a:LX/1gv;

    iget-object v0, v0, LX/1gv;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0r3;

    invoke-virtual {v1}, LX/1nY;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0r3;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 295307
    :catchall_0
    move-exception v0

    const v1, 0x56a1212f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
