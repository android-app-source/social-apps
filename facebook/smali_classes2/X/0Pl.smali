.class public LX/0Pl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Pk;


# instance fields
.field public final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/facebook/loom/config/QPLTraceControlConfiguration;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/util/SparseArray;)V
    .locals 0
    .param p1    # Landroid/util/SparseArray;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/facebook/loom/config/QPLTraceControlConfiguration;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57217
    iput-object p1, p0, LX/0Pl;->a:Landroid/util/SparseArray;

    .line 57218
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/facebook/loom/config/QPLTraceControlConfiguration;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 57219
    iget-object v0, p0, LX/0Pl;->a:Landroid/util/SparseArray;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0Pl;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 57220
    if-nez v0, :cond_0

    .line 57221
    const/4 v0, 0x0

    .line 57222
    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0Pl;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/loom/config/QPLTraceControlConfiguration;

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
