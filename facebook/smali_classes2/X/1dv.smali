.class public LX/1dv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0aG;

.field public final b:LX/1dw;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SI;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1dy;


# direct methods
.method public constructor <init>(LX/0aG;LX/1dw;LX/0Ot;LX/1dy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/1dw;",
            "LX/0Ot",
            "<",
            "LX/0SI;",
            ">;",
            "LX/1dy;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 287078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 287079
    iput-object p1, p0, LX/1dv;->a:LX/0aG;

    .line 287080
    iput-object p2, p0, LX/1dv;->b:LX/1dw;

    .line 287081
    iput-object p3, p0, LX/1dv;->c:LX/0Ot;

    .line 287082
    iput-object p4, p0, LX/1dv;->d:LX/1dy;

    .line 287083
    return-void
.end method

.method public static a(LX/1dv;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;Ljava/lang/String;ZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 287024
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 287025
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 287026
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 287027
    check-cast v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 287028
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 287029
    invoke-static {p1}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    .line 287030
    new-instance v3, LX/55S;

    invoke-direct {v3}, LX/55S;-><init>()V

    invoke-interface {v0}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    .line 287031
    iput-object v4, v3, LX/55S;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 287032
    move-object v3, v3

    .line 287033
    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->m()Ljava/lang/String;

    move-result-object v4

    .line 287034
    iput-object v4, v3, LX/55S;->b:Ljava/lang/String;

    .line 287035
    move-object v3, v3

    .line 287036
    invoke-interface {v0}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->o()Ljava/lang/String;

    move-result-object v4

    .line 287037
    iput-object v4, v3, LX/55S;->c:Ljava/lang/String;

    .line 287038
    move-object v3, v3

    .line 287039
    invoke-virtual {v2}, LX/162;->toString()Ljava/lang/String;

    move-result-object v2

    .line 287040
    iput-object v2, v3, LX/55S;->d:Ljava/lang/String;

    .line 287041
    move-object v2, v3

    .line 287042
    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v3

    .line 287043
    iput-object v3, v2, LX/55S;->e:Ljava/lang/String;

    .line 287044
    move-object v2, v2

    .line 287045
    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->H_()I

    move-result v3

    .line 287046
    iput v3, v2, LX/55S;->f:I

    .line 287047
    move-object v2, v2

    .line 287048
    iput-object p2, v2, LX/55S;->g:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    .line 287049
    move-object v2, v2

    .line 287050
    iput-object p3, v2, LX/55S;->h:Ljava/lang/String;

    .line 287051
    move-object v2, v2

    .line 287052
    iput-boolean p4, v2, LX/55S;->i:Z

    .line 287053
    move-object v2, v2

    .line 287054
    iput-object p5, v2, LX/55S;->j:Ljava/lang/String;

    .line 287055
    move-object v2, v2

    .line 287056
    new-instance v3, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;

    invoke-direct {v3, v2}, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;-><init>(LX/55S;)V

    move-object v2, v3

    .line 287057
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DONT_LIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    if-ne v3, v4, :cond_0

    .line 287058
    invoke-direct {p0, v1}, LX/1dv;->a(Landroid/os/Bundle;)V

    .line 287059
    :cond_0
    const-string v3, "negativeFeedbackActionOnFeedParams"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 287060
    iget-object v2, p0, LX/1dv;->a:LX/0aG;

    const-string v3, "feed_negative_feedback_story"

    const v4, 0x12193b5e

    invoke-static {v2, v3, v1, v4}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    .line 287061
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_1

    .line 287062
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v2

    .line 287063
    sget-object v3, LX/55o;->a:[I

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 287064
    const/4 v3, 0x0

    :goto_0
    move-object v2, v3

    .line 287065
    if-eqz v2, :cond_1

    .line 287066
    new-instance v3, LX/5AE;

    invoke-direct {v3}, LX/5AE;-><init>()V

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    .line 287067
    iput-object v0, v3, LX/5AE;->a:Ljava/lang/String;

    .line 287068
    move-object v0, v3

    .line 287069
    iput-object v2, v0, LX/5AE;->c:Ljava/lang/String;

    .line 287070
    move-object v0, v0

    .line 287071
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->name()Ljava/lang/String;

    move-result-object v2

    .line 287072
    iput-object v2, v0, LX/5AE;->b:Ljava/lang/String;

    .line 287073
    move-object v0, v0

    .line 287074
    invoke-virtual {v0}, LX/5AE;->a()Lcom/facebook/api/graphql/feed/StoryMutationModels$HideableStoryMutationFieldsModel;

    move-result-object v0

    .line 287075
    iget-object v2, p0, LX/1dv;->d:LX/1dy;

    invoke-virtual {v2, v1, v0}, LX/1dy;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0jT;)V

    .line 287076
    :cond_1
    return-object v1

    .line 287077
    :pswitch_0
    sget-object v3, Lcom/facebook/graphql/enums/StoryVisibility;->HIDDEN:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/StoryVisibility;->name()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 287022
    const-string v1, "overridden_viewer_context"

    iget-object v0, p0, LX/1dv;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SI;

    invoke-interface {v0}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 287023
    return-void
.end method

.method public static b(LX/0QB;)LX/1dv;
    .locals 5

    .prologue
    .line 287020
    new-instance v3, LX/1dv;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    invoke-static {p0}, LX/1dw;->a(LX/0QB;)LX/1dw;

    move-result-object v1

    check-cast v1, LX/1dw;

    const/16 v2, 0x1a1

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/1dy;->b(LX/0QB;)LX/1dy;

    move-result-object v2

    check-cast v2, LX/1dy;

    invoke-direct {v3, v0, v1, v4, v2}, LX/1dv;-><init>(LX/0aG;LX/1dw;LX/0Ot;LX/1dy;)V

    .line 287021
    return-object v3
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 287019
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, LX/1dv;->a(LX/1dv;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;Ljava/lang/String;ZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 287018
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/1dv;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 287007
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 287008
    new-instance v1, Lcom/facebook/api/feed/DeleteStoryMethod$Params;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/55G;->LOCAL_AND_SERVER:LX/55G;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/facebook/api/feed/DeleteStoryMethod$Params;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;LX/55G;)V

    .line 287009
    if-eqz p2, :cond_0

    .line 287010
    invoke-direct {p0, v0}, LX/1dv;->a(Landroid/os/Bundle;)V

    .line 287011
    :cond_0
    const-string v2, "deleteStoryParams"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 287012
    iget-object v1, p0, LX/1dv;->a:LX/0aG;

    const-string v2, "feed_delete_story"

    const v3, -0x43853785

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/HideableUnit;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/HideableUnit;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/StoryVisibility;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 287013
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 287014
    new-instance v0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;

    invoke-interface {p1}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-interface {p1}, Lcom/facebook/graphql/model/HideableUnit;->m()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1}, Lcom/facebook/graphql/model/HideableUnit;->H_()I

    move-result v7

    instance-of v3, p1, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    if-eqz v3, :cond_0

    check-cast p1, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    invoke-interface {p1}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->o()Ljava/lang/String;

    move-result-object v8

    :goto_0
    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v8}, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;-><init>(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;ZLjava/lang/String;ILjava/lang/String;)V

    .line 287015
    const-string v1, "hideFeedStoryParams"

    invoke-virtual {v9, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 287016
    iget-object v0, p0, LX/1dv;->a:LX/0aG;

    const-string v1, "feed_hide_story"

    const v2, -0x50de3816

    invoke-static {v0, v1, v9, v2}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    return-object v0

    .line 287017
    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method
