.class public LX/14t;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/16v;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 179327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179328
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/14t;->a:Ljava/util/List;

    .line 179329
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/16v;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 179298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179299
    iput-object p1, p0, LX/14t;->a:Ljava/util/List;

    .line 179300
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 179319
    const/4 v0, 0x0

    .line 179320
    iget-object v1, p0, LX/14t;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16v;

    .line 179321
    iget-object v3, v0, LX/16v;->c:Ljava/lang/String;

    move-object v3, v3

    .line 179322
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 179323
    iget v3, v0, LX/16v;->a:I

    move v0, v3

    .line 179324
    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 179325
    goto :goto_0

    .line 179326
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 179308
    const/4 v1, 0x0

    .line 179309
    iget-object v0, p0, LX/14t;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16v;

    .line 179310
    iget-object v3, v0, LX/16v;->b:Ljava/lang/String;

    move-object v3, v3

    .line 179311
    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 179312
    iget-object v3, v0, LX/16v;->c:Ljava/lang/String;

    move-object v3, v3

    .line 179313
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 179314
    iput p3, v0, LX/16v;->a:I

    .line 179315
    const/4 v0, 0x1

    .line 179316
    :goto_0
    if-nez v0, :cond_1

    .line 179317
    iget-object v0, p0, LX/14t;->a:Ljava/util/List;

    new-instance v1, LX/16v;

    invoke-direct {v1, p1, p2, p3}, LX/16v;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179318
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 179301
    const/4 v0, 0x0

    .line 179302
    iget-object v1, p0, LX/14t;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16v;

    .line 179303
    iget-object p0, v0, LX/16v;->c:Ljava/lang/String;

    move-object v0, p0

    .line 179304
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179305
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 179306
    goto :goto_0

    .line 179307
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method
