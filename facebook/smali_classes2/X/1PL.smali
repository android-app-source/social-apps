.class public final enum LX/1PL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1PL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1PL;

.field public static final enum IS_LOADING:LX/1PL;

.field public static final enum IS_POSTING:LX/1PL;

.field public static final enum IS_SCROLLING:LX/1PL;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 243928
    new-instance v0, LX/1PL;

    const-string v1, "IS_SCROLLING"

    invoke-direct {v0, v1, v2}, LX/1PL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1PL;->IS_SCROLLING:LX/1PL;

    .line 243929
    new-instance v0, LX/1PL;

    const-string v1, "IS_LOADING"

    invoke-direct {v0, v1, v3}, LX/1PL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1PL;->IS_LOADING:LX/1PL;

    .line 243930
    new-instance v0, LX/1PL;

    const-string v1, "IS_POSTING"

    invoke-direct {v0, v1, v4}, LX/1PL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1PL;->IS_POSTING:LX/1PL;

    .line 243931
    const/4 v0, 0x3

    new-array v0, v0, [LX/1PL;

    sget-object v1, LX/1PL;->IS_SCROLLING:LX/1PL;

    aput-object v1, v0, v2

    sget-object v1, LX/1PL;->IS_LOADING:LX/1PL;

    aput-object v1, v0, v3

    sget-object v1, LX/1PL;->IS_POSTING:LX/1PL;

    aput-object v1, v0, v4

    sput-object v0, LX/1PL;->$VALUES:[LX/1PL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 243925
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1PL;
    .locals 1

    .prologue
    .line 243927
    const-class v0, LX/1PL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1PL;

    return-object v0
.end method

.method public static values()[LX/1PL;
    .locals 1

    .prologue
    .line 243926
    sget-object v0, LX/1PL;->$VALUES:[LX/1PL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1PL;

    return-object v0
.end method
