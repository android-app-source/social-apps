.class public LX/0pJ;
.super LX/0pK;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0pJ;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:Lcom/facebook/common/perftest/PerfTestConfig;


# direct methods
.method public constructor <init>(LX/0W3;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/common/perftest/PerfTestConfig;)V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 144189
    const/16 v2, 0xb

    const/16 v3, 0x9

    const/4 v4, 0x3

    move-object v0, p0

    move-object v1, p1

    move v6, v5

    invoke-direct/range {v0 .. v6}, LX/0pK;-><init>(LX/0W3;IIIII)V

    .line 144190
    iput-object p2, p0, LX/0pJ;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 144191
    iput-object p3, p0, LX/0pJ;->b:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 144192
    return-void
.end method

.method public static a(LX/0QB;)LX/0pJ;
    .locals 6

    .prologue
    .line 144163
    sget-object v0, LX/0pJ;->c:LX/0pJ;

    if-nez v0, :cond_1

    .line 144164
    const-class v1, LX/0pJ;

    monitor-enter v1

    .line 144165
    :try_start_0
    sget-object v0, LX/0pJ;->c:LX/0pJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 144166
    if-eqz v2, :cond_0

    .line 144167
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 144168
    new-instance p0, LX/0pJ;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v5

    check-cast v5, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-direct {p0, v3, v4, v5}, LX/0pJ;-><init>(LX/0W3;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/common/perftest/PerfTestConfig;)V

    .line 144169
    move-object v0, p0

    .line 144170
    sput-object v0, LX/0pJ;->c:LX/0pJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144171
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 144172
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 144173
    :cond_1
    sget-object v0, LX/0pJ;->c:LX/0pJ;

    return-object v0

    .line 144174
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 144175
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 144188
    sget-wide v0, LX/0X5;->du:J

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2, p1}, LX/0pK;->a(JILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)Z
    .locals 3

    .prologue
    .line 144187
    sget-wide v0, LX/0X5;->do:J

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2, p1}, LX/0pK;->a(JIZ)Z

    move-result v0

    return v0
.end method

.method public final b(Z)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 144179
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 144180
    sget-boolean v0, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->m:Z

    move v0, v0

    .line 144181
    :goto_0
    :pswitch_0
    return v0

    .line 144182
    :cond_0
    iget-object v2, p0, LX/0pJ;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0pP;->A:LX/0Tn;

    const-string v4, "default"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 144183
    const/4 v2, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 144184
    sget-wide v0, LX/0X5;->dp:J

    const/4 v2, 0x2

    invoke-virtual {p0, v0, v1, v2, p1}, LX/0pK;->a(JIZ)Z

    move-result v0

    goto :goto_0

    .line 144185
    :sswitch_0
    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v2, v1

    goto :goto_1

    :sswitch_1
    const-string v4, "off"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v2, v0

    goto :goto_1

    :pswitch_1
    move v0, v1

    .line 144186
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xddf -> :sswitch_0
        0x1ad6f -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final c(Z)Z
    .locals 3

    .prologue
    .line 144178
    sget-wide v0, LX/0X5;->dq:J

    const/4 v2, 0x3

    invoke-virtual {p0, v0, v1, v2, p1}, LX/0pK;->a(JIZ)Z

    move-result v0

    return v0
.end method

.method public final f(Z)Z
    .locals 3

    .prologue
    .line 144177
    sget-wide v0, LX/0X5;->ds:J

    const/4 v2, 0x5

    invoke-virtual {p0, v0, v1, v2, p1}, LX/0pK;->a(JIZ)Z

    move-result v0

    return v0
.end method

.method public final j(I)I
    .locals 3

    .prologue
    .line 144176
    sget-wide v0, LX/0X5;->dn:J

    const/16 v2, 0xa

    invoke-virtual {p0, v0, v1, v2, p1}, LX/0pK;->a(JII)I

    move-result v0

    return v0
.end method
