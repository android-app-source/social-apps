.class public LX/0ym;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0yn;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/0ym;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0qj;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0YZ;

.field private e:LX/0p3;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private f:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private g:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0qj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 166142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166143
    sget-object v0, LX/0p3;->UNKNOWN:LX/0p3;

    iput-object v0, p0, LX/0ym;->e:LX/0p3;

    .line 166144
    iput-object p1, p0, LX/0ym;->a:LX/0Ot;

    .line 166145
    iput-object p2, p0, LX/0ym;->b:LX/0Ot;

    .line 166146
    iput-object p3, p0, LX/0ym;->c:LX/0Ot;

    .line 166147
    return-void
.end method

.method public static a(LX/0QB;)LX/0ym;
    .locals 6

    .prologue
    .line 166172
    sget-object v0, LX/0ym;->h:LX/0ym;

    if-nez v0, :cond_1

    .line 166173
    const-class v1, LX/0ym;

    monitor-enter v1

    .line 166174
    :try_start_0
    sget-object v0, LX/0ym;->h:LX/0ym;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 166175
    if-eqz v2, :cond_0

    .line 166176
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 166177
    new-instance v3, LX/0ym;

    const/16 v4, 0x24e

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x24d

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x1ce

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, LX/0ym;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 166178
    move-object v0, v3

    .line 166179
    sput-object v0, LX/0ym;->h:LX/0ym;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 166180
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 166181
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 166182
    :cond_1
    sget-object v0, LX/0ym;->h:LX/0ym;

    return-object v0

    .line 166183
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 166184
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/0ym;LX/0p3;)V
    .locals 1

    .prologue
    .line 166167
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0ym;->e:LX/0p3;

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 166168
    iput-object p1, p0, LX/0ym;->e:LX/0p3;

    .line 166169
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0ym;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166170
    :cond_0
    monitor-exit p0

    return-void

    .line 166171
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/0ym;Z)V
    .locals 1

    .prologue
    .line 166162
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0ym;->f:Z

    if-eq v0, p1, :cond_0

    .line 166163
    iput-boolean p1, p0, LX/0ym;->f:Z

    .line 166164
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0ym;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166165
    :cond_0
    monitor-exit p0

    return-void

    .line 166166
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 166161
    return-void
.end method

.method public final declared-synchronized a(LX/0y8;)V
    .locals 1

    .prologue
    .line 166156
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0ym;->e:LX/0p3;

    invoke-virtual {p1, v0}, LX/0y8;->a(LX/0p3;)V

    .line 166157
    iget-boolean v0, p0, LX/0ym;->f:Z

    invoke-virtual {p1, v0}, LX/0y8;->a(Z)V

    .line 166158
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0ym;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166159
    monitor-exit p0

    return-void

    .line 166160
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/0yl;)V
    .locals 3

    .prologue
    .line 166149
    const/4 v0, 0x2

    new-array v0, v0, [LX/0yo;

    const/4 v1, 0x0

    sget-object v2, LX/0yo;->CONNECTION_QUALITY:LX/0yo;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/0yo;->IS_OFFLINE:LX/0yo;

    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, LX/0yl;->a([LX/0yo;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 166150
    :cond_0
    :goto_0
    return-void

    .line 166151
    :cond_1
    iget-object v0, p0, LX/0ym;->d:LX/0YZ;

    if-nez v0, :cond_0

    .line 166152
    iget-object v0, p0, LX/0ym;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oz;

    invoke-virtual {v0}, LX/0oz;->c()LX/0p3;

    move-result-object v0

    invoke-static {p0, v0}, LX/0ym;->a$redex0(LX/0ym;LX/0p3;)V

    .line 166153
    iget-object v0, p0, LX/0ym;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qj;

    invoke-virtual {v0}, LX/0qj;->a()Z

    move-result v0

    invoke-static {p0, v0}, LX/0ym;->a$redex0(LX/0ym;Z)V

    .line 166154
    new-instance v0, LX/0yr;

    invoke-direct {v0, p0}, LX/0yr;-><init>(LX/0ym;)V

    iput-object v0, p0, LX/0ym;->d:LX/0YZ;

    .line 166155
    iget-object v0, p0, LX/0ym;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    sget-object v1, LX/0oz;->a:Ljava/lang/String;

    iget-object v2, p0, LX/0ym;->d:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    goto :goto_0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 166148
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0ym;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
