.class public LX/0jh;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0jh;


# instance fields
.field private final b:LX/0kD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0kD",
            "<",
            "Ljava/lang/reflect/AnnotatedElement;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/reflect/AnnotatedElement;",
            "[[",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 124598
    new-instance v0, LX/0jh;

    invoke-direct {v0}, LX/0jh;-><init>()V

    sput-object v0, LX/0jh;->a:LX/0jh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 124599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124600
    invoke-static {}, LX/0kA;->f()LX/0kA;

    move-result-object v0

    iput-object v0, p0, LX/0jh;->b:LX/0kD;

    .line 124601
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0jh;->c:Ljava/util/Map;

    .line 124602
    return-void
.end method

.method private declared-synchronized b(Ljava/lang/reflect/AnnotatedElement;Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Ljava/lang/annotation/Annotation;",
            ">(",
            "Ljava/lang/reflect/AnnotatedElement;",
            "Ljava/lang/Class",
            "<TA;>;)TA;"
        }
    .end annotation

    .prologue
    .line 124603
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0jh;->b:LX/0kD;

    invoke-interface {v0, p1, p2}, LX/0kD;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    .line 124604
    if-eqz v0, :cond_0

    .line 124605
    iget v1, p0, LX/0jh;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0jh;->d:I

    .line 124606
    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/annotation/Annotation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124607
    :goto_0
    monitor-exit p0

    return-object v0

    .line 124608
    :cond_0
    :try_start_1
    invoke-interface {p1, p2}, Ljava/lang/reflect/AnnotatedElement;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    .line 124609
    iget-object v1, p0, LX/0jh;->b:LX/0kD;

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, LX/0kD;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124610
    iget v1, p0, LX/0jh;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0jh;->e:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 124611
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/reflect/AnnotatedElement;Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/AnnotatedElement;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 124612
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, LX/0jh;->b(Ljava/lang/reflect/AnnotatedElement;Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
