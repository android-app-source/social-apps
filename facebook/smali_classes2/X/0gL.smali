.class public LX/0gL;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/util/concurrent/Executor;

.field public final c:LX/0tX;

.field public final d:LX/0gX;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/AEw;

.field public g:LX/0gM;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 111915
    const-class v0, LX/0gL;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0gL;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;LX/0tX;LX/0Or;LX/0gX;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "LX/0tX;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0gX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 111916
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111917
    iput-object p3, p0, LX/0gL;->e:LX/0Or;

    .line 111918
    iput-object p1, p0, LX/0gL;->b:Ljava/util/concurrent/Executor;

    .line 111919
    iput-object p2, p0, LX/0gL;->c:LX/0tX;

    .line 111920
    iput-object p4, p0, LX/0gL;->d:LX/0gX;

    .line 111921
    return-void
.end method
