.class public LX/1gj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2dg;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/23G;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1jZ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation runtime Lcom/facebook/dialtone/common/IsDialtoneEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final e:LX/0ad;

.field private final f:LX/0Zb;

.field private final g:LX/0pV;


# direct methods
.method public constructor <init>(LX/0ad;LX/0Zb;LX/0pV;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 294872
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 294873
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 294874
    iput-object v0, p0, LX/1gj;->b:LX/0Ot;

    .line 294875
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 294876
    iput-object v0, p0, LX/1gj;->c:LX/0Ot;

    .line 294877
    iput-object p1, p0, LX/1gj;->e:LX/0ad;

    .line 294878
    iput-object p2, p0, LX/1gj;->f:LX/0Zb;

    .line 294879
    iput-object p3, p0, LX/1gj;->g:LX/0pV;

    .line 294880
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1gj;->a:Ljava/util/List;

    .line 294881
    return-void
.end method

.method public static a(LX/0QB;)LX/1gj;
    .locals 4

    .prologue
    .line 294861
    new-instance v3, LX/1gj;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/0pV;->a(LX/0QB;)LX/0pV;

    move-result-object v2

    check-cast v2, LX/0pV;

    invoke-direct {v3, v0, v1, v2}, LX/1gj;-><init>(LX/0ad;LX/0Zb;LX/0pV;)V

    .line 294862
    const/16 v0, 0x76a

    invoke-static {p0, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    const/16 v1, 0x605

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x1483

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    .line 294863
    iput-object v0, v3, LX/1gj;->b:LX/0Ot;

    iput-object v1, v3, LX/1gj;->c:LX/0Ot;

    iput-object v2, v3, LX/1gj;->d:LX/0Or;

    .line 294864
    move-object v0, v3

    .line 294865
    return-object v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)LX/6VL;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 294866
    if-nez p0, :cond_0

    .line 294867
    const/4 v0, 0x0

    .line 294868
    :goto_0
    return-object v0

    .line 294869
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-static {v0}, LX/18J;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 294870
    sget-object v0, LX/6VL;->AD:LX/6VL;

    goto :goto_0

    .line 294871
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {v0}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x78e4008f

    if-ne v0, v1, :cond_2

    sget-object v0, LX/6VL;->PYMK:LX/6VL;

    goto :goto_0

    :cond_2
    sget-object v0, LX/6VL;->OTHER_EGO:LX/6VL;

    goto :goto_0
.end method

.method private a(LX/0fz;ILcom/facebook/feed/model/ClientFeedUnitEdge;LX/2dg;)V
    .locals 11
    .param p4    # LX/2dg;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    const/4 v8, -0x1

    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 294782
    iget-object v0, p0, LX/1gj;->f:LX/0Zb;

    .line 294783
    const-string v1, "ad_distance"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 294784
    move-object v0, v1

    .line 294785
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 294786
    :goto_0
    return-void

    .line 294787
    :cond_0
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    invoke-static {v1}, LX/18J;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    .line 294788
    invoke-virtual {p1}, LX/0fz;->v()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v7, v1

    move v1, v6

    :goto_1
    if-ltz v7, :cond_b

    .line 294789
    invoke-virtual {p1, v7}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v4

    .line 294790
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    invoke-static {v2}, LX/18J;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/1gj;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/23G;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v9

    invoke-virtual {v2, v9}, LX/23G;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    if-nez v2, :cond_4

    move-object v2, v4

    .line 294791
    :goto_2
    if-nez v2, :cond_7

    move v4, v5

    :goto_3
    if-eqz p4, :cond_8

    :goto_4
    invoke-virtual {p1}, LX/0fz;->v()I

    move-result v6

    if-eqz p4, :cond_9

    .line 294792
    iget v7, p4, LX/2dg;->c:I

    move v7, v7

    .line 294793
    :goto_5
    if-eqz p4, :cond_1

    .line 294794
    iget v8, p4, LX/2dg;->b:I

    move v8, v8

    .line 294795
    :cond_1
    if-nez v3, :cond_a

    invoke-static {p3}, LX/1gj;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)LX/6VL;

    move-result-object v9

    :goto_6
    if-nez v3, :cond_2

    invoke-static {v2}, LX/1gj;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)LX/6VL;

    move-result-object v10

    :cond_2
    move v2, p2

    .line 294796
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result p0

    if-nez p0, :cond_c

    .line 294797
    :cond_3
    :goto_7
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0

    .line 294798
    :cond_4
    if-nez v3, :cond_5

    invoke-static {v4}, LX/1gj;->b(LX/0jQ;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/HasGapRule;

    invoke-static {p0, v2}, LX/1gj;->a(LX/1gj;Lcom/facebook/graphql/model/HasGapRule;)Z

    move-result v2

    if-eqz v2, :cond_5

    move-object v2, v4

    .line 294799
    goto :goto_2

    .line 294800
    :cond_5
    iget-object v2, p0, LX/1gj;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    invoke-static {v2}, LX/23G;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 294801
    add-int/lit8 v1, v1, 0x1

    .line 294802
    :cond_6
    add-int/lit8 v2, v7, -0x1

    move v7, v2

    goto :goto_1

    :cond_7
    move v4, v6

    .line 294803
    goto :goto_3

    :cond_8
    move v5, v6

    goto :goto_4

    :cond_9
    move v7, v8

    goto :goto_5

    :cond_a
    move-object v9, v10

    goto :goto_6

    :cond_b
    move-object v2, v10

    goto :goto_2

    .line 294804
    :cond_c
    const-string p0, "is_fresh_feed"

    const/4 p1, 0x1

    invoke-virtual {v0, p0, p1}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p0

    const-string p1, "distance"

    invoke-virtual {p0, p1, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object p0

    const-string p1, "min_gap"

    invoke-virtual {p0, p1, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object p0

    const-string p1, "is_ad"

    invoke-virtual {p0, p1, v3}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p0

    const-string p1, "first_ad"

    invoke-virtual {p0, p1, v4}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p0

    const-string p1, "reinserted_unit"

    invoke-virtual {p0, p1, v5}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p0

    const-string p1, "position"

    invoke-virtual {p0, p1, v6}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object p0

    const-string p1, "native_newsfeed"

    invoke-virtual {p0, p1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 294805
    if-ltz v7, :cond_d

    .line 294806
    const-string p0, "orig_distance"

    invoke-virtual {v0, p0, v7}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 294807
    :cond_d
    if-ltz v8, :cond_e

    .line 294808
    const-string p0, "orig_position"

    invoke-virtual {v0, p0, v8}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 294809
    :cond_e
    if-eqz v9, :cond_f

    .line 294810
    const-string p0, "unit_type"

    iget-object p1, v9, LX/6VL;->value:Ljava/lang/String;

    invoke-virtual {v0, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 294811
    :cond_f
    if-eqz v10, :cond_3

    .line 294812
    const-string p0, "previous_unit_type"

    iget-object p1, v10, LX/6VL;->value:Ljava/lang/String;

    invoke-virtual {v0, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    goto/16 :goto_7
.end method

.method public static a(LX/1gj;LX/0fz;Lcom/facebook/feed/model/ClientFeedUnitEdge;LX/2dg;LX/2df;)Z
    .locals 9
    .param p2    # Lcom/facebook/feed/model/ClientFeedUnitEdge;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, -0x1

    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 294813
    iget-object v0, p0, LX/1gj;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2}, LX/1gj;->a(Lcom/facebook/feed/model/ClientFeedUnitEdge;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v8, v1

    .line 294814
    :cond_0
    :goto_0
    return v8

    .line 294815
    :cond_1
    iget-object v0, p0, LX/1gj;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23G;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/23G;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 294816
    invoke-virtual {p1}, LX/0fz;->x()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 294817
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->m()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/1gj;->e:LX/0ad;

    sget-short v2, LX/0fe;->z:S

    invoke-interface {v0, v2, v8}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 294818
    :cond_2
    if-nez p3, :cond_3

    .line 294819
    new-instance p3, LX/2dg;

    invoke-direct {p3, p2, v8, v8}, LX/2dg;-><init>(Lcom/facebook/feed/model/ClientFeedUnitEdge;II)V

    .line 294820
    :cond_3
    sget-object v0, LX/2df;->HEAD:LX/2df;

    if-ne p4, v0, :cond_4

    .line 294821
    iget-object v0, p0, LX/1gj;->a:Ljava/util/List;

    invoke-interface {v0, v8, p3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 294822
    :goto_1
    iget-object v0, p0, LX/1gj;->g:LX/0pV;

    const-string v2, "FreshFeedGapRuleValidator"

    const-string v3, "Moving sponsored unit from position 0"

    invoke-virtual {v0, v2, v3}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v8, v1

    .line 294823
    goto :goto_0

    .line 294824
    :cond_4
    iget-object v0, p0, LX/1gj;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 294825
    :cond_5
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/HasGapRule;

    .line 294826
    invoke-interface {v0}, Lcom/facebook/graphql/model/HasGapRule;->n()I

    move-result v3

    .line 294827
    invoke-static {p0, v0}, LX/1gj;->a(LX/1gj;Lcom/facebook/graphql/model/HasGapRule;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294828
    invoke-virtual {p1}, LX/0fz;->v()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    move v5, v8

    :goto_2
    if-ltz v2, :cond_10

    .line 294829
    invoke-virtual {p1, v2}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v4

    .line 294830
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-static {v0}, LX/18J;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p0, v4}, LX/1gj;->a(LX/0jQ;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_6
    iget-object v0, p0, LX/1gj;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23G;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/23G;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/HasGapRule;

    invoke-static {p0, v0}, LX/1gj;->a(LX/1gj;Lcom/facebook/graphql/model/HasGapRule;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 294831
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/HasGapRule;

    .line 294832
    invoke-interface {v0}, Lcom/facebook/graphql/model/HasGapRule;->n()I

    move-result v0

    .line 294833
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 294834
    add-int/lit8 v2, v0, -0x1

    if-ge v5, v2, :cond_b

    .line 294835
    if-nez p3, :cond_7

    .line 294836
    new-instance p3, LX/2dg;

    invoke-virtual {p1}, LX/0fz;->v()I

    move-result v2

    invoke-direct {p3, p2, v2, v5}, LX/2dg;-><init>(Lcom/facebook/feed/model/ClientFeedUnitEdge;II)V

    .line 294837
    :cond_7
    sget-object v2, LX/2df;->HEAD:LX/2df;

    if-ne p4, v2, :cond_8

    .line 294838
    iget-object v2, p0, LX/1gj;->a:Ljava/util/List;

    invoke-interface {v2, v8, p3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 294839
    :goto_3
    iget-object v2, p0, LX/1gj;->g:LX/0pV;

    const-string v3, "FreshFeedGapRuleValidator"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Moving sponsored unit too close. min space: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " current gap: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v8, v1

    .line 294840
    goto/16 :goto_0

    .line 294841
    :cond_8
    iget-object v2, p0, LX/1gj;->a:Ljava/util/List;

    invoke-interface {v2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 294842
    :cond_9
    iget-object v0, p0, LX/1gj;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-static {v0}, LX/23G;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 294843
    add-int/lit8 v5, v5, 0x1

    .line 294844
    :cond_a
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto/16 :goto_2

    :cond_b
    move v2, v8

    .line 294845
    :goto_4
    invoke-direct {p0, p1, v0, p2, p3}, LX/1gj;->a(LX/0fz;ILcom/facebook/feed/model/ClientFeedUnitEdge;LX/2dg;)V

    .line 294846
    iget-object v0, p0, LX/1gj;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1jZ;

    iget-object v3, p0, LX/1gj;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz p3, :cond_e

    move v4, v1

    :goto_5
    if-eqz p3, :cond_f

    .line 294847
    iget v1, p3, LX/2dg;->b:I

    move v6, v1

    .line 294848
    :goto_6
    if-eqz p3, :cond_c

    .line 294849
    iget v1, p3, LX/2dg;->c:I

    move v7, v1

    .line 294850
    :cond_c
    move-object v1, p2

    .line 294851
    invoke-virtual {v0}, LX/1jZ;->a()Z

    move-result p0

    if-nez p0, :cond_11

    .line 294852
    :cond_d
    :goto_7
    goto/16 :goto_0

    :cond_e
    move v4, v8

    goto :goto_5

    :cond_f
    move v6, v7

    goto :goto_6

    :cond_10
    move v2, v1

    move v0, v7

    goto :goto_4

    .line 294853
    :cond_11
    invoke-static {v0, v1}, LX/1jZ;->a(LX/1jZ;Lcom/facebook/feed/model/ClientFeedUnitEdge;)LX/2xl;

    move-result-object p0

    .line 294854
    if-eqz p0, :cond_d

    .line 294855
    invoke-virtual {p0, v2}, LX/2xl;->o(Z)V

    .line 294856
    invoke-virtual {p0, v3}, LX/2xl;->p(Z)V

    .line 294857
    invoke-virtual {p0, v4}, LX/2xl;->q(Z)V

    .line 294858
    invoke-virtual {p0, v5}, LX/2xl;->f(I)V

    .line 294859
    invoke-virtual {p0, v6}, LX/2xl;->g(I)V

    .line 294860
    invoke-virtual {p0, v7}, LX/2xl;->h(I)V

    goto :goto_7
.end method

.method private static a(LX/1gj;Lcom/facebook/graphql/model/HasGapRule;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 294780
    iget-object v1, p0, LX/1gj;->e:LX/0ad;

    sget-short v2, LX/0fe;->an:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_1

    .line 294781
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1}, Lcom/facebook/graphql/model/HasGapRule;->n()I

    move-result v1

    if-gtz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/feed/model/ClientFeedUnitEdge;)Z
    .locals 2

    .prologue
    .line 294777
    const-string v0, "Ad"

    .line 294778
    iget-object v1, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A:Ljava/lang/String;

    move-object v1, v1

    .line 294779
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-static {v0}, LX/18J;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0jQ;)Z
    .locals 1

    .prologue
    .line 294776
    invoke-interface {p0}, LX/0jQ;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 294771
    iget-object v0, p0, LX/1gj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 294772
    return-void
.end method

.method public final a(LX/0jQ;)Z
    .locals 2

    .prologue
    .line 294773
    invoke-static {p1}, LX/1gj;->b(LX/0jQ;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 294774
    iget-object v1, p0, LX/1gj;->e:LX/0ad;

    sget-short p1, LX/0fe;->am:S

    invoke-interface {v1, p1, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v0, v0

    .line 294775
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
