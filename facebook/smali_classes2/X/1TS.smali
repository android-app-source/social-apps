.class public LX/1TS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;
.implements LX/1TG;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 251552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 251553
    iput-object p1, p0, LX/1TS;->a:LX/0Ot;

    .line 251554
    return-void
.end method

.method public static a(LX/0QB;)LX/1TS;
    .locals 4

    .prologue
    .line 251561
    const-class v1, LX/1TS;

    monitor-enter v1

    .line 251562
    :try_start_0
    sget-object v0, LX/1TS;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 251563
    sput-object v2, LX/1TS;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 251564
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251565
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 251566
    new-instance v3, LX/1TS;

    const/16 p0, 0x1fdb

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1TS;-><init>(LX/0Ot;)V

    .line 251567
    move-object v0, v3

    .line 251568
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 251569
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1TS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 251570
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 251571
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 1

    .prologue
    .line 251559
    sget-object v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->a:LX/1Cz;

    invoke-static {p1, v0}, LX/99p;->a(LX/0ja;LX/1Cz;)V

    .line 251560
    return-void
.end method

.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 251557
    sget-object v0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 251558
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 251555
    const-class v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    iget-object v1, p0, LX/1TS;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 251556
    return-void
.end method
