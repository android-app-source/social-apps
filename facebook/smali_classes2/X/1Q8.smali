.class public LX/1Q8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Q9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pv;",
        ">",
        "Ljava/lang/Object;",
        "LX/1Q9",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/2eU;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 244381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244382
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/1Q8;->a:Ljava/util/LinkedList;

    .line 244383
    return-void
.end method

.method public static a(LX/0QB;)LX/1Q8;
    .locals 1

    .prologue
    .line 244378
    new-instance v0, LX/1Q8;

    invoke-direct {v0}, LX/1Q8;-><init>()V

    .line 244379
    move-object v0, v0

    .line 244380
    return-object v0
.end method

.method private a(LX/1Ps;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;LX/1Rb;Z)V
    .locals 8
    .param p2    # Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/1Rb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Lcom/facebook/multirow/api/PartWithViewType;",
            "Lcom/facebook/multirow/api/PartWithViewType;",
            "Lcom/facebook/multirow/api/PartWithViewType;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "LX/1Rb;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 244384
    iget-boolean v0, p0, LX/1Q8;->b:Z

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 244385
    check-cast v0, LX/1Pu;

    invoke-interface {v0}, LX/1Pu;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, LX/1Pu;

    invoke-interface {v0}, LX/1Pu;->o()LX/1Rb;

    move-result-object v6

    .line 244386
    :goto_0
    new-instance v0, LX/2eU;

    invoke-interface {p1}, LX/1Ps;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v1

    invoke-interface {p1}, LX/1Ps;->f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v2

    invoke-interface {p1}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v3

    invoke-interface {p1}, LX/1Ps;->i()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p1}, LX/1Ps;->j()Ljava/lang/Object;

    move-result-object v5

    move/from16 v7, p8

    invoke-direct/range {v0 .. v7}, LX/2eU;-><init>(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;LX/1Rb;Z)V

    .line 244387
    iget-object v1, p0, LX/1Q8;->a:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->push(Ljava/lang/Object;)V

    .line 244388
    :cond_0
    invoke-static/range {p1 .. p8}, LX/1Q8;->b(LX/1Ps;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;LX/1Rb;Z)V

    .line 244389
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Q8;->b:Z

    .line 244390
    return-void

    .line 244391
    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private static b(LX/1Ps;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;LX/1Rb;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Lcom/facebook/multirow/api/PartWithViewType;",
            "Lcom/facebook/multirow/api/PartWithViewType;",
            "Lcom/facebook/multirow/api/PartWithViewType;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "LX/1Rb;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 244374
    move-object v0, p0

    check-cast v0, LX/1Pu;

    invoke-interface {v0, p6}, LX/1Pu;->a(LX/1Rb;)V

    .line 244375
    invoke-interface/range {p0 .. p5}, LX/1Ps;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 244376
    check-cast p0, LX/1Pv;

    invoke-interface {p0, p7}, LX/1Pv;->b(Z)V

    .line 244377
    return-void
.end method


# virtual methods
.method public final a(LX/1PW;)V
    .locals 8

    .prologue
    .line 244365
    check-cast p1, LX/1Ps;

    .line 244366
    invoke-interface {p1}, LX/1Ps;->k()V

    move-object v0, p1

    .line 244367
    check-cast v0, LX/1Pu;

    invoke-interface {v0}, LX/1Pu;->p()V

    .line 244368
    iget-object v0, p0, LX/1Q8;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/1Q8;->b:Z

    .line 244369
    iget-boolean v0, p0, LX/1Q8;->b:Z

    if-eqz v0, :cond_0

    .line 244370
    iget-object v0, p0, LX/1Q8;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2eU;

    .line 244371
    iget-object v1, v0, LX/2eU;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    iget-object v2, v0, LX/2eU;->b:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    iget-object v3, v0, LX/2eU;->c:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    iget-object v4, v0, LX/2eU;->d:Ljava/lang/Object;

    iget-object v5, v0, LX/2eU;->e:Ljava/lang/Object;

    iget-object v6, v0, LX/2eU;->f:LX/1Rb;

    iget-boolean v7, v0, LX/2eU;->g:Z

    move-object v0, p1

    invoke-static/range {v0 .. v7}, LX/1Q8;->b(LX/1Ps;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;LX/1Rb;Z)V

    .line 244372
    :cond_0
    return-void

    .line 244373
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(LX/1PW;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;LX/1Rb;Z)V
    .locals 9
    .param p2    # Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/1Rb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 244364
    move-object v1, p1

    check-cast v1, LX/1Ps;

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v0 .. v8}, LX/1Q8;->a(LX/1Ps;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;LX/1Rb;Z)V

    return-void
.end method
