.class public final LX/1oC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 318535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)I
    .locals 2

    .prologue
    .line 318603
    invoke-static {p0}, LX/1mh;->a(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 318604
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected size spec mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 318605
    :sswitch_0
    invoke-static {p0}, LX/1mh;->b(I)I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 318606
    :goto_0
    return v0

    .line 318607
    :sswitch_1
    invoke-static {p0}, LX/1mh;->b(I)I

    move-result v0

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0

    .line 318608
    :sswitch_2
    invoke-static {p0}, LX/1mh;->b(I)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_2
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(IIFLX/1no;)V
    .locals 8

    .prologue
    .line 318567
    invoke-static {p0}, LX/1mh;->a(I)I

    move-result v0

    .line 318568
    invoke-static {p0}, LX/1mh;->b(I)I

    move-result v1

    .line 318569
    invoke-static {p1}, LX/1mh;->a(I)I

    move-result v2

    .line 318570
    invoke-static {p1}, LX/1mh;->b(I)I

    move-result v3

    .line 318571
    int-to-float v4, v1

    div-float/2addr v4, p2

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 318572
    int-to-float v5, v3

    mul-float/2addr v5, p2

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    .line 318573
    if-nez v0, :cond_1

    if-nez v2, :cond_1

    .line 318574
    const/4 v0, 0x0

    iput v0, p3, LX/1no;->a:I

    .line 318575
    const/4 v0, 0x0

    iput v0, p3, LX/1no;->b:I

    .line 318576
    :cond_0
    :goto_0
    return-void

    .line 318577
    :cond_1
    const/high16 v6, -0x80000000

    if-ne v0, v6, :cond_3

    const/high16 v6, -0x80000000

    if-ne v2, v6, :cond_3

    .line 318578
    if-le v4, v3, :cond_2

    .line 318579
    iput v5, p3, LX/1no;->a:I

    .line 318580
    iput v3, p3, LX/1no;->b:I

    goto :goto_0

    .line 318581
    :cond_2
    iput v1, p3, LX/1no;->a:I

    .line 318582
    iput v4, p3, LX/1no;->b:I

    goto :goto_0

    .line 318583
    :cond_3
    const/high16 v6, 0x40000000    # 2.0f

    if-ne v0, v6, :cond_6

    .line 318584
    iput v1, p3, LX/1no;->a:I

    .line 318585
    if-eqz v2, :cond_4

    if-gt v4, v3, :cond_5

    .line 318586
    :cond_4
    iput v4, p3, LX/1no;->b:I

    goto :goto_0

    .line 318587
    :cond_5
    iput v3, p3, LX/1no;->b:I

    .line 318588
    sget-boolean v0, LX/1V5;->a:Z

    if-eqz v0, :cond_0

    .line 318589
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p0}, LX/1mh;->c(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, LX/1mh;->c(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    goto :goto_0

    .line 318590
    :cond_6
    const/high16 v6, 0x40000000    # 2.0f

    if-ne v2, v6, :cond_9

    .line 318591
    iput v3, p3, LX/1no;->b:I

    .line 318592
    if-eqz v0, :cond_7

    if-gt v5, v1, :cond_8

    .line 318593
    :cond_7
    iput v5, p3, LX/1no;->a:I

    goto :goto_0

    .line 318594
    :cond_8
    iput v1, p3, LX/1no;->a:I

    .line 318595
    sget-boolean v0, LX/1V5;->a:Z

    if-eqz v0, :cond_0

    .line 318596
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p0}, LX/1mh;->c(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, LX/1mh;->c(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    goto :goto_0

    .line 318597
    :cond_9
    const/high16 v6, -0x80000000

    if-ne v0, v6, :cond_a

    .line 318598
    iput v1, p3, LX/1no;->a:I

    .line 318599
    iput v4, p3, LX/1no;->b:I

    goto :goto_0

    .line 318600
    :cond_a
    const/high16 v0, -0x80000000

    if-ne v2, v0, :cond_0

    .line 318601
    iput v5, p3, LX/1no;->a:I

    .line 318602
    iput v3, p3, LX/1no;->b:I

    goto :goto_0
.end method

.method public static a(IIIIFLX/1no;)V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 318561
    invoke-static {p0}, LX/1mh;->a(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-static {p0}, LX/1mh;->b(I)I

    move-result v0

    if-le v0, p2, :cond_0

    .line 318562
    invoke-static {p2, v1}, LX/1mh;->a(II)I

    move-result p0

    .line 318563
    :cond_0
    invoke-static {p1}, LX/1mh;->a(I)I

    move-result v0

    if-ne v0, v1, :cond_1

    invoke-static {p1}, LX/1mh;->b(I)I

    move-result v0

    if-le v0, p3, :cond_1

    .line 318564
    invoke-static {p3, v1}, LX/1mh;->a(II)I

    move-result p1

    .line 318565
    :cond_1
    invoke-static {p0, p1, p4, p5}, LX/1oC;->a(IIFLX/1no;)V

    .line 318566
    return-void
.end method

.method public static a(IILX/1no;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 318536
    invoke-static {p0}, LX/1mh;->a(I)I

    move-result v0

    .line 318537
    invoke-static {p0}, LX/1mh;->b(I)I

    move-result v1

    .line 318538
    invoke-static {p1}, LX/1mh;->a(I)I

    move-result v2

    .line 318539
    invoke-static {p1}, LX/1mh;->b(I)I

    move-result v3

    .line 318540
    if-nez v0, :cond_0

    if-nez v2, :cond_0

    .line 318541
    iput v4, p2, LX/1no;->a:I

    .line 318542
    iput v4, p2, LX/1no;->b:I

    .line 318543
    :goto_0
    return-void

    .line 318544
    :cond_0
    const/high16 v4, 0x40000000    # 2.0f

    if-ne v0, v4, :cond_2

    .line 318545
    iput v1, p2, LX/1no;->a:I

    .line 318546
    sparse-switch v2, :sswitch_data_0

    .line 318547
    :cond_1
    :goto_1
    iput v3, p2, LX/1no;->b:I

    .line 318548
    iput v3, p2, LX/1no;->a:I

    goto :goto_0

    .line 318549
    :sswitch_0
    iput v3, p2, LX/1no;->b:I

    goto :goto_0

    .line 318550
    :sswitch_1
    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p2, LX/1no;->b:I

    goto :goto_0

    .line 318551
    :sswitch_2
    iput v1, p2, LX/1no;->b:I

    goto :goto_0

    .line 318552
    :cond_2
    const/high16 v4, -0x80000000

    if-ne v0, v4, :cond_1

    .line 318553
    sparse-switch v2, :sswitch_data_1

    goto :goto_1

    .line 318554
    :sswitch_3
    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 318555
    iput v0, p2, LX/1no;->a:I

    .line 318556
    iput v0, p2, LX/1no;->b:I

    goto :goto_0

    .line 318557
    :sswitch_4
    iput v3, p2, LX/1no;->b:I

    .line 318558
    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p2, LX/1no;->a:I

    goto :goto_0

    .line 318559
    :sswitch_5
    iput v1, p2, LX/1no;->a:I

    .line 318560
    iput v1, p2, LX/1no;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_2
        0x40000000 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_3
        0x0 -> :sswitch_5
        0x40000000 -> :sswitch_4
    .end sparse-switch
.end method
