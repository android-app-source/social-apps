.class public LX/1I0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/crypto/keychain/KeyChain;


# instance fields
.field private final a:Lcom/facebook/crypto/keychain/KeyChain;

.field private final b:LX/1Hy;


# direct methods
.method public constructor <init>(Lcom/facebook/crypto/keychain/KeyChain;LX/1Hy;)V
    .locals 0

    .prologue
    .line 228065
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 228066
    iput-object p1, p0, LX/1I0;->a:Lcom/facebook/crypto/keychain/KeyChain;

    .line 228067
    iput-object p2, p0, LX/1I0;->b:LX/1Hy;

    .line 228068
    return-void
.end method

.method private static a([BILjava/lang/String;)V
    .locals 3

    .prologue
    .line 228069
    array-length v0, p0

    if-eq v0, p1, :cond_0

    .line 228070
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " should be "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes long but is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 228071
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()[B
    .locals 3

    .prologue
    .line 228072
    iget-object v0, p0, LX/1I0;->a:Lcom/facebook/crypto/keychain/KeyChain;

    invoke-interface {v0}, Lcom/facebook/crypto/keychain/KeyChain;->a()[B

    move-result-object v0

    .line 228073
    iget-object v1, p0, LX/1I0;->b:LX/1Hy;

    iget v1, v1, LX/1Hy;->keyLength:I

    const-string v2, "Key"

    invoke-static {v0, v1, v2}, LX/1I0;->a([BILjava/lang/String;)V

    .line 228074
    return-object v0
.end method

.method public final b()[B
    .locals 3

    .prologue
    .line 228075
    iget-object v0, p0, LX/1I0;->a:Lcom/facebook/crypto/keychain/KeyChain;

    invoke-interface {v0}, Lcom/facebook/crypto/keychain/KeyChain;->b()[B

    move-result-object v0

    .line 228076
    iget-object v1, p0, LX/1I0;->b:LX/1Hy;

    iget v1, v1, LX/1Hy;->ivLength:I

    const-string v2, "IV"

    invoke-static {v0, v1, v2}, LX/1I0;->a([BILjava/lang/String;)V

    .line 228077
    return-object v0
.end method
