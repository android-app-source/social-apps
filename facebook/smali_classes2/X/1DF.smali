.class public LX/1DF;
.super LX/0hD;
.source ""

# interfaces
.implements LX/1Cf;


# annotations
.annotation build Lcom/facebook/controllercallbacks/api/ControllerConfig;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Dispatcher::",
        "LX/0fr;",
        ">",
        "LX/0hD;",
        "LX/1Cf;"
    }
.end annotation


# instance fields
.field public a:LX/1UO;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final b:LX/0pJ;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C8t;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0r3;

.field public final e:LX/1DG;

.field public final f:LX/1DI;

.field public g:LX/0fr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TDispatcher;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/api/feedtype/FeedType;

.field public i:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/0pJ;LX/0Ot;LX/0r3;LX/1DG;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0pJ;",
            "LX/0Ot",
            "<",
            "LX/C8t;",
            ">;",
            "LX/0r3;",
            "LX/1DG;",
            ")V"
        }
    .end annotation

    .prologue
    .line 217339
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 217340
    new-instance v0, LX/1DH;

    invoke-direct {v0, p0}, LX/1DH;-><init>(LX/1DF;)V

    iput-object v0, p0, LX/1DF;->f:LX/1DI;

    .line 217341
    iput-object p1, p0, LX/1DF;->b:LX/0pJ;

    .line 217342
    iput-object p2, p0, LX/1DF;->c:LX/0Ot;

    .line 217343
    iput-object p3, p0, LX/1DF;->d:LX/0r3;

    .line 217344
    iput-object p4, p0, LX/1DF;->e:LX/1DG;

    .line 217345
    return-void
.end method
