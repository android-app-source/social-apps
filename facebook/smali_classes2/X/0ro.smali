.class public abstract LX/0ro;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PARAMS:",
        "Ljava/lang/Object;",
        "RESU",
        "LT:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/http/protocol/GraphQlPersistedApiMethod",
        "<TPARAMS;TRESU",
        "LT;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0sO;


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 1

    .prologue
    .line 150958
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150959
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sO;

    iput-object v0, p0, LX/0ro;->a:LX/0sO;

    .line 150960
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/lang/Object;)LX/0zW;
    .locals 1
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            "TPARAMS;)",
            "Lcom/facebook/http/interfaces/RequestState;"
        }
    .end annotation

    .prologue
    .line 150957
    invoke-static {p1, p2}, LX/14T;->a(Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;)LX/0zW;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAMS;)",
            "LX/14N;"
        }
    .end annotation

    .prologue
    .line 150956
    invoke-virtual {p0, p1}, LX/0ro;->d(Ljava/lang/Object;)LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAMS;",
            "LX/1pN;",
            ")TRESU",
            "LT;"
        }
    .end annotation

    .prologue
    .line 150942
    iget-object v0, p0, LX/0ro;->a:LX/0sO;

    invoke-virtual {p2}, LX/1pN;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, p2}, LX/0ro;->b(Ljava/lang/Object;LX/1pN;)I

    move-result v2

    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v3

    .line 150943
    iget-object v4, v0, LX/0sO;->k:LX/0sT;

    invoke-virtual {v4}, LX/0sT;->j()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 150944
    add-int/lit8 v2, v2, 0x1

    .line 150945
    :cond_0
    invoke-virtual {v0, v1, v2, v3}, LX/0sO;->a(Ljava/lang/String;ILX/15w;)LX/15w;

    move-result-object v4

    move-object v0, v4

    .line 150946
    iget-object v1, p0, LX/0ro;->a:LX/0sO;

    .line 150947
    iget-object v2, v1, LX/0sO;->i:LX/0sQ;

    move-object v1, v2

    .line 150948
    if-eqz v1, :cond_1

    .line 150949
    invoke-virtual {p0, p1}, LX/0ro;->f(Ljava/lang/Object;)LX/0gW;

    move-result-object v1

    .line 150950
    iget-object v2, p0, LX/0ro;->a:LX/0sO;

    .line 150951
    iget-object v3, v2, LX/0sO;->i:LX/0sQ;

    move-object v2, v3

    .line 150952
    iget-object v3, v1, LX/0gW;->f:Ljava/lang/String;

    move-object v3, v3

    .line 150953
    iget-object v4, v1, LX/0gW;->h:Ljava/lang/String;

    move-object v1, v4

    .line 150954
    invoke-virtual {v2, v3, v1, v0}, LX/0sQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 150955
    :cond_1
    invoke-virtual {p0, p1, p2, v0}, LX/0ro;->a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAMS;",
            "LX/1pN;",
            "LX/15w;",
            ")TRESU",
            "LT;"
        }
    .end annotation
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 150941
    const/4 v0, 0x0

    return v0
.end method

.method public abstract b(Ljava/lang/Object;LX/1pN;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAMS;",
            "LX/1pN;",
            ")I"
        }
    .end annotation
.end method

.method public b()LX/14S;
    .locals 1

    .prologue
    .line 150940
    sget-object v0, LX/14S;->JSONPARSER:LX/14S;

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAMS;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 150939
    const-string v0, "get"

    return-object v0
.end method

.method public final d(Ljava/lang/Object;)LX/14N;
    .locals 17
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAMS;)",
            "LX/14N;"
        }
    .end annotation

    .prologue
    .line 150932
    invoke-virtual/range {p0 .. p1}, LX/0ro;->f(Ljava/lang/Object;)LX/0gW;

    move-result-object v12

    .line 150933
    invoke-virtual/range {p0 .. p1}, LX/0ro;->e(Ljava/lang/Object;)LX/0w7;

    move-result-object v7

    .line 150934
    if-nez v7, :cond_0

    .line 150935
    invoke-virtual {v12}, LX/0gW;->m()LX/0w7;

    move-result-object v7

    .line 150936
    :cond_0
    invoke-virtual {v12}, LX/0gW;->h()Ljava/lang/String;

    move-result-object v3

    .line 150937
    invoke-virtual/range {p0 .. p1}, LX/0ro;->i(Ljava/lang/Object;)Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v10

    .line 150938
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0ro;->a:LX/0sO;

    invoke-virtual {v12}, LX/0gW;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12}, LX/0gW;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, LX/0ro;->b()LX/14S;

    move-result-object v6

    invoke-virtual/range {p0 .. p1}, LX/0ro;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p0 .. p1}, LX/0ro;->g(Ljava/lang/Object;)LX/14P;

    move-result-object v9

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v3, v10, v1}, LX/0ro;->a(Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/lang/Object;)LX/0zW;

    move-result-object v10

    invoke-virtual {v12}, LX/0gW;->i()Z

    move-result v11

    invoke-virtual {v12}, LX/0gW;->j()Z

    move-result v12

    invoke-virtual/range {p0 .. p1}, LX/0ro;->j(Ljava/lang/Object;)LX/0Px;

    move-result-object v13

    invoke-virtual/range {p0 .. p1}, LX/0ro;->k(Ljava/lang/Object;)LX/0Px;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, LX/0ro;->a()Z

    move-result v15

    invoke-virtual/range {p0 .. p1}, LX/0ro;->h(Ljava/lang/Object;)Z

    move-result v16

    invoke-virtual/range {v2 .. v16}, LX/0sO;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/14S;LX/0w7;Ljava/lang/String;LX/14P;LX/0zW;ZZLX/0Px;LX/0Px;ZZ)LX/14N;

    move-result-object v2

    return-object v2
.end method

.method public e(Ljava/lang/Object;)LX/0w7;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAMS;)",
            "LX/0w7;"
        }
    .end annotation

    .prologue
    .line 150931
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract f(Ljava/lang/Object;)LX/0gW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAMS;)",
            "LX/0gW;"
        }
    .end annotation
.end method

.method public g(Ljava/lang/Object;)LX/14P;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAMS;)",
            "LX/14P;"
        }
    .end annotation

    .prologue
    .line 150930
    sget-object v0, LX/14P;->RETRY_SAFE:LX/14P;

    return-object v0
.end method

.method public h(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAMS;)Z"
        }
    .end annotation

    .prologue
    .line 150929
    const/4 v0, 0x0

    return v0
.end method

.method public i(Ljava/lang/Object;)Lcom/facebook/http/interfaces/RequestPriority;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAMS;)",
            "Lcom/facebook/http/interfaces/RequestPriority;"
        }
    .end annotation

    .prologue
    .line 150928
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    return-object v0
.end method

.method public j(Ljava/lang/Object;)LX/0Px;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAMS;)",
            "LX/0Px",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 150927
    const/4 v0, 0x0

    return-object v0
.end method

.method public k(Ljava/lang/Object;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPARAMS;)",
            "LX/0Px",
            "<",
            "LX/4cQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150925
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 150926
    return-object v0
.end method
