.class public LX/0y2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0y3;

.field public final c:Landroid/location/LocationManager;

.field private final d:LX/0yD;

.field public e:Lcom/facebook/location/ImmutableLocation;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 163895
    const-string v0, "gps"

    const-string v1, "network"

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/0y2;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/0y3;Landroid/location/LocationManager;LX/0yD;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 163889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163890
    const/4 v0, 0x0

    iput-object v0, p0, LX/0y2;->e:Lcom/facebook/location/ImmutableLocation;

    .line 163891
    iput-object p1, p0, LX/0y2;->b:LX/0y3;

    .line 163892
    iput-object p2, p0, LX/0y2;->c:Landroid/location/LocationManager;

    .line 163893
    iput-object p3, p0, LX/0y2;->d:LX/0yD;

    .line 163894
    return-void
.end method

.method public static a(LX/0QB;)LX/0y2;
    .locals 1

    .prologue
    .line 163888
    invoke-static {p0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/0y2;
    .locals 4

    .prologue
    .line 163866
    new-instance v3, LX/0y2;

    invoke-static {p0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v0

    check-cast v0, LX/0y3;

    invoke-static {p0}, LX/0y4;->b(LX/0QB;)Landroid/location/LocationManager;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    invoke-static {p0}, LX/0yD;->a(LX/0QB;)LX/0yD;

    move-result-object v2

    check-cast v2, LX/0yD;

    invoke-direct {v3, v0, v1, v2}, LX/0y2;-><init>(LX/0y3;Landroid/location/LocationManager;LX/0yD;)V

    .line 163867
    return-object v3
.end method


# virtual methods
.method public final a()Lcom/facebook/location/ImmutableLocation;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 163887
    const-wide v0, 0x7fffffffffffffffL

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    invoke-virtual {p0, v0, v1, v2}, LX/0y2;->a(JF)Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)Lcom/facebook/location/ImmutableLocation;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 163886
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    invoke-virtual {p0, p1, p2, v0}, LX/0y2;->a(JF)Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    return-object v0
.end method

.method public final a(JF)Lcom/facebook/location/ImmutableLocation;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 163868
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 163869
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-lez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 163870
    iget-object v0, p0, LX/0y2;->b:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-eq v0, v1, :cond_2

    .line 163871
    :goto_2
    return-object v3

    :cond_0
    move v0, v2

    .line 163872
    goto :goto_0

    :cond_1
    move v1, v2

    .line 163873
    goto :goto_1

    .line 163874
    :cond_2
    iget-object v1, p0, LX/0y2;->e:Lcom/facebook/location/ImmutableLocation;

    .line 163875
    if-eqz v1, :cond_7

    iget-object v0, p0, LX/0y2;->d:LX/0yD;

    invoke-virtual {v0, v1}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v4

    cmp-long v0, v4, p1

    if-gtz v0, :cond_7

    invoke-virtual {v1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v0, p3

    if-gtz v0, :cond_7

    .line 163876
    invoke-virtual {v1}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v0

    .line 163877
    :goto_3
    sget-object v1, LX/0y2;->a:LX/0Rf;

    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :cond_3
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 163878
    :try_start_0
    iget-object v3, p0, LX/0y2;->c:Landroid/location/LocationManager;

    invoke-virtual {v3, v0}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 163879
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/0z8;->a(Landroid/location/Location;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_3

    .line 163880
    iget-object v3, p0, LX/0y2;->d:LX/0yD;

    invoke-virtual {v3, v0}, LX/0yD;->a(Landroid/location/Location;)J

    move-result-wide v4

    cmp-long v3, v4, p1

    if-gtz v3, :cond_6

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    cmpg-float v3, v3, p3

    if-gtz v3, :cond_6

    .line 163881
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-gez v3, :cond_6

    :cond_4
    :goto_5
    move-object v1, v0

    .line 163882
    goto :goto_4

    .line 163883
    :cond_5
    invoke-static {v1}, Lcom/facebook/location/ImmutableLocation;->b(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v3

    goto :goto_2

    .line 163884
    :catch_0
    goto :goto_4

    .line 163885
    :catch_1
    goto :goto_4

    :cond_6
    move-object v0, v1

    goto :goto_5

    :cond_7
    move-object v0, v3

    goto :goto_3
.end method
