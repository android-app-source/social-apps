.class public LX/1ab;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final b:LX/1ab;

.field private static c:Z


# instance fields
.field private final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/1at;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 277842
    new-instance v0, LX/1ab;

    invoke-direct {v0}, LX/1ab;-><init>()V

    sput-object v0, LX/1ab;->b:LX/1ab;

    .line 277843
    const/4 v0, 0x1

    sput-boolean v0, LX/1ab;->c:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 277844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 277845
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, LX/1ab;->a:Ljava/util/Queue;

    .line 277846
    return-void
.end method

.method public static a()LX/1ab;
    .locals 1

    .prologue
    .line 277847
    sget-boolean v0, LX/1ab;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, LX/1ab;

    invoke-direct {v0}, LX/1ab;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/1ab;->b:LX/1ab;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1at;)V
    .locals 2

    .prologue
    .line 277848
    sget-boolean v0, LX/1ab;->c:Z

    if-nez v0, :cond_0

    .line 277849
    :goto_0
    return-void

    .line 277850
    :cond_0
    iget-object v0, p0, LX/1ab;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    const/16 v1, 0x14

    if-le v0, v1, :cond_1

    .line 277851
    iget-object v0, p0, LX/1ab;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    .line 277852
    :cond_1
    iget-object v0, p0, LX/1ab;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277853
    iget-object v0, p0, LX/1ab;->a:Ljava/util/Queue;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
