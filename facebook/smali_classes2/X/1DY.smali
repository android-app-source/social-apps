.class public LX/1DY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1DZ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1DZ",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0qn;


# direct methods
.method public constructor <init>(LX/0qn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 217665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217666
    iput-object p1, p0, LX/1DY;->a:LX/0qn;

    .line 217667
    return-void
.end method

.method public static a(LX/0QB;)LX/1DY;
    .locals 2

    .prologue
    .line 217668
    new-instance v1, LX/1DY;

    invoke-static {p0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v0

    check-cast v0, LX/0qn;

    invoke-direct {v1, v0}, LX/1DY;-><init>(LX/0qn;)V

    .line 217669
    move-object v0, v1

    .line 217670
    return-object v0
.end method

.method public static a(LX/1DY;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 217671
    instance-of v1, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    instance-of v1, p2, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v1, :cond_1

    .line 217672
    :cond_0
    :goto_0
    return v0

    .line 217673
    :cond_1
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 217674
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 217675
    iget-object v1, p0, LX/1DY;->a:LX/0qn;

    invoke-virtual {v1, p1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/1DY;->a:LX/0qn;

    invoke-virtual {v1, p2}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v1, v2, :cond_0

    .line 217676
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)Z"
        }
    .end annotation

    .prologue
    .line 217677
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    if-eqz v0, :cond_2

    instance-of v0, p2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    if-eqz v0, :cond_2

    .line 217678
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    check-cast p2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 217679
    instance-of v2, p1, Lcom/facebook/feed/model/GapFeedEdge;

    move v2, v2

    .line 217680
    if-nez v2, :cond_0

    .line 217681
    instance-of v2, p2, Lcom/facebook/feed/model/GapFeedEdge;

    move v2, v2

    .line 217682
    if-eqz v2, :cond_3

    .line 217683
    :cond_0
    if-ne p2, p1, :cond_1

    move v0, v1

    .line 217684
    :cond_1
    :goto_0
    move v0, v0

    .line 217685
    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 217686
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    invoke-interface {v2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    .line 217687
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/1DY;->a(LX/1DY;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    goto :goto_0

    .line 217688
    :cond_4
    if-eq p1, p2, :cond_5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    if-eq v2, v3, :cond_5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    invoke-interface {v2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    invoke-interface {v3}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;J)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;J)Z"
        }
    .end annotation

    .prologue
    .line 217689
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    if-eqz v0, :cond_2

    instance-of v0, p2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    check-cast p2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 217690
    instance-of v3, p1, Lcom/facebook/feed/model/GapFeedEdge;

    move v3, v3

    .line 217691
    if-nez v3, :cond_0

    .line 217692
    instance-of v3, p2, Lcom/facebook/feed/model/GapFeedEdge;

    move v3, v3

    .line 217693
    if-eqz v3, :cond_3

    :cond_0
    move v1, v2

    .line 217694
    :cond_1
    :goto_0
    move v0, v1

    .line 217695
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 217696
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    invoke-interface {v3}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 217697
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    invoke-interface {v3}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v4

    invoke-interface {v4}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    invoke-interface {v3}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v3

    cmp-long v3, v3, p3

    if-nez v3, :cond_1

    move v1, v2

    goto :goto_0
.end method
