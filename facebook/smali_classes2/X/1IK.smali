.class public final LX/1IK;
.super LX/1IA;
.source ""


# static fields
.field public static final INSTANCE:LX/1IK;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 228622
    new-instance v0, LX/1IK;

    invoke-direct {v0}, LX/1IK;-><init>()V

    sput-object v0, LX/1IK;->INSTANCE:LX/1IK;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 228623
    invoke-direct {p0}, LX/1IA;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 228624
    check-cast p1, Ljava/lang/Character;

    invoke-super {p0, p1}, LX/1IA;->apply(Ljava/lang/Character;)Z

    move-result v0

    return v0
.end method

.method public final matches(C)Z
    .locals 1

    .prologue
    .line 228625
    invoke-static {p1}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 228626
    const-string v0, "CharMatcher.javaLetterOrDigit()"

    return-object v0
.end method
