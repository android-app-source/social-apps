.class public LX/1Sb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1Sb;


# instance fields
.field private final a:LX/03V;


# direct methods
.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 248609
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248610
    iput-object p1, p0, LX/1Sb;->a:LX/03V;

    .line 248611
    return-void
.end method

.method public static a(LX/0QB;)LX/1Sb;
    .locals 4

    .prologue
    .line 248612
    sget-object v0, LX/1Sb;->b:LX/1Sb;

    if-nez v0, :cond_1

    .line 248613
    const-class v1, LX/1Sb;

    monitor-enter v1

    .line 248614
    :try_start_0
    sget-object v0, LX/1Sb;->b:LX/1Sb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 248615
    if-eqz v2, :cond_0

    .line 248616
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 248617
    new-instance p0, LX/1Sb;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/1Sb;-><init>(LX/03V;)V

    .line 248618
    move-object v0, p0

    .line 248619
    sput-object v0, LX/1Sb;->b:LX/1Sb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 248620
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 248621
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 248622
    :cond_1
    sget-object v0, LX/1Sb;->b:LX/1Sb;

    return-object v0

    .line 248623
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 248624
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLStorySaveType;)LX/AEr;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 248625
    sget-object v0, LX/AEq;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 248626
    iget-object v0, p0, LX/1Sb;->a:LX/03V;

    const-string v1, "StorySaveTypeResources"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported save type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 248627
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->GENERIC:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    invoke-virtual {p0, v0}, LX/1Sb;->a(Lcom/facebook/graphql/enums/GraphQLStorySaveType;)LX/AEr;

    move-result-object v0

    :goto_0
    return-object v0

    .line 248628
    :pswitch_0
    new-instance v0, LX/AEr;

    const v1, 0x7f081afb

    const v2, 0x7f081b08

    const v3, 0x7f081b13

    const v4, 0x7f081b20

    invoke-direct {v0, v1, v2, v3, v4}, LX/AEr;-><init>(IIII)V

    goto :goto_0

    .line 248629
    :pswitch_1
    new-instance v0, LX/AEr;

    const v1, 0x7f081afc

    const v2, 0x7f081b09

    const v3, 0x7f081b14

    const v4, 0x7f081b21

    invoke-direct {v0, v1, v2, v3, v4}, LX/AEr;-><init>(IIII)V

    goto :goto_0

    .line 248630
    :pswitch_2
    new-instance v0, LX/AEr;

    const v1, 0x7f081afd

    const v2, 0x7f081b0a

    const v3, 0x7f081b15

    const v4, 0x7f081b22

    invoke-direct {v0, v1, v2, v3, v4}, LX/AEr;-><init>(IIII)V

    goto :goto_0

    .line 248631
    :pswitch_3
    new-instance v0, LX/AEr;

    const v1, 0x7f081afe

    const v2, 0x7f081b0b

    const v3, 0x7f081b16

    const v4, 0x7f081b23

    invoke-direct {v0, v1, v2, v3, v4}, LX/AEr;-><init>(IIII)V

    goto :goto_0

    .line 248632
    :pswitch_4
    new-instance v0, LX/AEr;

    const v1, 0x7f081aff

    const v2, 0x7f081b0c

    const v3, 0x7f081b17

    const v4, 0x7f081b24

    invoke-direct {v0, v1, v2, v3, v4}, LX/AEr;-><init>(IIII)V

    goto :goto_0

    .line 248633
    :pswitch_5
    new-instance v0, LX/AEr;

    const v1, 0x7f081b00

    const v2, 0x7f081b0d

    const v3, 0x7f081b18

    const v4, 0x7f081b25

    invoke-direct {v0, v1, v2, v3, v4}, LX/AEr;-><init>(IIII)V

    goto :goto_0

    .line 248634
    :pswitch_6
    new-instance v0, LX/AEr;

    const v1, 0x7f081b01

    const v2, 0x7f081b0e

    const v3, 0x7f081b19

    const v4, 0x7f081b26

    invoke-direct {v0, v1, v2, v3, v4}, LX/AEr;-><init>(IIII)V

    goto :goto_0

    .line 248635
    :pswitch_7
    new-instance v0, LX/AEr;

    const v1, 0x7f081b02    # 1.8091524E38f

    const v2, 0x7f081b0f

    const v3, 0x7f081b1a

    const v4, 0x7f081b27

    invoke-direct {v0, v1, v2, v3, v4}, LX/AEr;-><init>(IIII)V

    goto/16 :goto_0

    .line 248636
    :pswitch_8
    new-instance v0, LX/AEr;

    const v1, 0x7f081b03

    const v2, 0x7f081b10

    const v3, 0x7f081b1b

    const v4, 0x7f081b28

    invoke-direct {v0, v1, v2, v3, v4}, LX/AEr;-><init>(IIII)V

    goto/16 :goto_0

    .line 248637
    :pswitch_9
    new-instance v0, LX/AEr;

    const v1, 0x7f081b04

    const v2, 0x7f081b12

    const v3, 0x7f081b1c

    const v4, 0x7f081b2a

    invoke-direct {v0, v1, v2, v3, v4}, LX/AEr;-><init>(IIII)V

    goto/16 :goto_0

    .line 248638
    :pswitch_a
    new-instance v0, LX/AEr;

    const v1, 0x7f081b05

    const v2, 0x7f081b11

    const v3, 0x7f081b1d

    const v4, 0x7f081b29

    invoke-direct {v0, v1, v2, v3, v4}, LX/AEr;-><init>(IIII)V

    goto/16 :goto_0

    .line 248639
    :pswitch_b
    new-instance v0, LX/AEr;

    const v1, 0x7f081b06

    const v2, 0x7f081b11

    const v3, 0x7f081b1e

    const v4, 0x7f081b29

    invoke-direct {v0, v1, v2, v3, v4}, LX/AEr;-><init>(IIII)V

    goto/16 :goto_0

    .line 248640
    :pswitch_c
    new-instance v0, LX/AEr;

    const v1, 0x7f081b07

    const v2, 0x7f081b12

    const v3, 0x7f081b1f

    const v4, 0x7f081b2a

    invoke-direct {v0, v1, v2, v3, v4}, LX/AEr;-><init>(IIII)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method
