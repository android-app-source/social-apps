.class public LX/0wT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:D

.field public b:D


# direct methods
.method public constructor <init>(DD)V
    .locals 1

    .prologue
    .line 160068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160069
    iput-wide p1, p0, LX/0wT;->b:D

    .line 160070
    iput-wide p3, p0, LX/0wT;->a:D

    .line 160071
    return-void
.end method

.method public static a(DD)LX/0wT;
    .locals 10

    .prologue
    .line 160072
    new-instance v0, LX/0wT;

    .line 160073
    const-wide/high16 v6, 0x403e000000000000L    # 30.0

    sub-double v6, p0, v6

    const-wide v8, 0x400cf5c28f5c28f6L    # 3.62

    mul-double/2addr v6, v8

    const-wide v8, 0x4068400000000000L    # 194.0

    add-double/2addr v6, v8

    move-wide v2, v6

    .line 160074
    invoke-static {p2, p3}, LX/0wV;->b(D)D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, LX/0wT;-><init>(DD)V

    return-object v0
.end method

.method public static b(DD)LX/0wT;
    .locals 6

    .prologue
    .line 160075
    new-instance v0, LX/0wU;

    invoke-direct {v0, p0, p1, p2, p3}, LX/0wU;-><init>(DD)V

    .line 160076
    iget-wide v4, v0, LX/0wU;->b:D

    move-wide v2, v4

    .line 160077
    iget-wide v4, v0, LX/0wU;->c:D

    move-wide v0, v4

    .line 160078
    invoke-static {v2, v3, v0, v1}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    return-object v0
.end method
