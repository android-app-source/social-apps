.class public LX/1m6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Z

.field private b:Z

.field private c:LX/04G;

.field private d:LX/0AR;

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 313078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313079
    iput-boolean v1, p0, LX/1m6;->a:Z

    .line 313080
    iput-boolean v1, p0, LX/1m6;->b:Z

    .line 313081
    sget-object v0, LX/04G;->INLINE_PLAYER:LX/04G;

    iput-object v0, p0, LX/1m6;->c:LX/04G;

    .line 313082
    iput-object v2, p0, LX/1m6;->d:LX/0AR;

    .line 313083
    iput-boolean v1, p0, LX/1m6;->e:Z

    .line 313084
    iput-object v2, p0, LX/1m6;->f:Ljava/lang/String;

    .line 313085
    iput-boolean v1, p0, LX/1m6;->g:Z

    .line 313086
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/04G;)V
    .locals 1

    .prologue
    .line 313056
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/1m6;->c:LX/04G;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313057
    monitor-exit p0

    return-void

    .line 313058
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0AR;)V
    .locals 1

    .prologue
    .line 313075
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/1m6;->d:LX/0AR;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313076
    monitor-exit p0

    return-void

    .line 313077
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 313072
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/1m6;->f:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313073
    monitor-exit p0

    return-void

    .line 313074
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 313069
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, LX/1m6;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313070
    monitor-exit p0

    return-void

    .line 313071
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 313068
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1m6;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/04G;
    .locals 1

    .prologue
    .line 313087
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1m6;->c:LX/04G;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Z)V
    .locals 1

    .prologue
    .line 313065
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, LX/1m6;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313066
    monitor-exit p0

    return-void

    .line 313067
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()LX/0AR;
    .locals 1

    .prologue
    .line 313064
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1m6;->d:LX/0AR;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Z)V
    .locals 1

    .prologue
    .line 313061
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, LX/1m6;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313062
    monitor-exit p0

    return-void

    .line 313063
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 1

    .prologue
    .line 313060
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1m6;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 313059
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1m6;->f:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Z
    .locals 1

    .prologue
    .line 313055
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1m6;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
