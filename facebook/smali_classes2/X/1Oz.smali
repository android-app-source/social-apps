.class public LX/1Oz;
.super LX/1P0;
.source ""


# instance fields
.field private final a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private final b:LX/1P7;

.field public final c:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "Ljava/util/List",
            "<",
            "LX/1a1;",
            ">;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/1OD;

.field private final f:Z

.field public g:Z


# direct methods
.method public constructor <init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V
    .locals 1

    .prologue
    .line 242740
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;B)V

    .line 242741
    return-void
.end method

.method public constructor <init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;B)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 242818
    invoke-direct {p0, p1, v0, v0}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;LX/03V;LX/0ad;)V

    .line 242819
    return-void
.end method

.method public constructor <init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;LX/03V;LX/0ad;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 242806
    invoke-virtual {p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, LX/1P0;-><init>(Landroid/content/Context;)V

    .line 242807
    new-instance v1, LX/1P7;

    invoke-direct {v1}, LX/1P7;-><init>()V

    iput-object v1, p0, LX/1Oz;->b:LX/1P7;

    .line 242808
    new-instance v1, LX/0YU;

    invoke-direct {v1}, LX/0YU;-><init>()V

    iput-object v1, p0, LX/1Oz;->c:LX/0YU;

    .line 242809
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, LX/1Oz;->d:Ljava/util/Set;

    .line 242810
    new-instance v1, LX/1P9;

    invoke-direct {v1, p0}, LX/1P9;-><init>(LX/1Oz;)V

    iput-object v1, p0, LX/1Oz;->e:LX/1OD;

    .line 242811
    invoke-static {p0}, LX/1P0;->c(LX/1P0;)LX/1P4;

    move-result-object v1

    .line 242812
    iput-object p2, v1, LX/1P4;->a:LX/03V;

    .line 242813
    iput-object p1, p0, LX/1Oz;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 242814
    iget-object v1, p0, LX/1Oz;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v2, LX/1PA;

    invoke-direct {v2, p0}, LX/1PA;-><init>(LX/1Oz;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setViewCacheExtension(LX/1PB;)V

    .line 242815
    if-nez p3, :cond_0

    :goto_0
    iput-boolean v0, p0, LX/1Oz;->f:Z

    .line 242816
    return-void

    .line 242817
    :cond_0
    sget-short v1, LX/1PC;->a:S

    invoke-interface {p3, v1, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0
.end method

.method private a(Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 242789
    iget-object v0, p0, LX/1Oz;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v1

    .line 242790
    if-eqz p2, :cond_2

    .line 242791
    iget-object v0, p0, LX/1Oz;->d:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 242792
    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, LX/1OR;->e(Landroid/view/View;)V

    .line 242793
    iget v0, v1, LX/1a1;->e:I

    move v2, v0

    .line 242794
    iget-object v0, p0, LX/1Oz;->c:LX/0YU;

    invoke-virtual {v0, v2}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 242795
    if-nez v0, :cond_1

    .line 242796
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 242797
    iget-object v3, p0, LX/1Oz;->c:LX/0YU;

    invoke-virtual {v3, v2, v0}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 242798
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242799
    return-void

    .line 242800
    :cond_2
    iget-object v0, p0, LX/1Oz;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 242801
    iget-object v2, v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->D:LX/1OU;

    move-object v0, v2

    .line 242802
    if-eqz v0, :cond_0

    .line 242803
    iget-object v0, p0, LX/1Oz;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 242804
    iget-object v2, v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->D:LX/1OU;

    move-object v0, v2

    .line 242805
    invoke-interface {v0, v1}, LX/1OU;->a(LX/1a1;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/1Oz;LX/1a1;IZ)V
    .locals 2

    .prologue
    .line 242776
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/1aL;->b(Landroid/view/View;)LX/1Ra;

    move-result-object v0

    .line 242777
    iget-object v1, p0, LX/1Oz;->d:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 242778
    :goto_0
    if-eqz p3, :cond_1

    if-eqz v0, :cond_1

    .line 242779
    iget-object v0, p0, LX/1Oz;->b:LX/1P7;

    invoke-virtual {v0, p1, p2}, LX/1OM;->b(LX/1a1;I)V

    .line 242780
    :goto_1
    return-void

    .line 242781
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 242782
    :cond_1
    if-eqz v0, :cond_2

    .line 242783
    iget-object v0, p0, LX/1Oz;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 242784
    iget-object v1, v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->D:LX/1OU;

    move-object v0, v1

    .line 242785
    invoke-interface {v0, p1}, LX/1OU;->a(LX/1a1;)V

    .line 242786
    :cond_2
    iget-object v0, p0, LX/1Oz;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 242787
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v1

    .line 242788
    invoke-virtual {v0, p1, p2}, LX/1OM;->b(LX/1a1;I)V

    goto :goto_1
.end method

.method private m(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 242773
    iget-boolean v0, p0, LX/1Oz;->f:Z

    if-nez v0, :cond_0

    instance-of v0, p1, LX/1a7;

    if-eqz v0, :cond_0

    .line 242774
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    invoke-virtual {v0}, LX/1a3;->c()Z

    move-result v0

    move v0, v0

    .line 242775
    if-nez v0, :cond_0

    check-cast p1, LX/1a7;

    invoke-interface {p1}, LX/1a7;->cr_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILX/1Od;)V
    .locals 1

    .prologue
    .line 242771
    invoke-virtual {p0, p1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/1OR;->a(Landroid/view/View;LX/1Od;)V

    .line 242772
    return-void
.end method

.method public final a(LX/1OM;LX/1OM;)V
    .locals 1

    .prologue
    .line 242766
    if-eqz p1, :cond_0

    .line 242767
    iget-object v0, p0, LX/1Oz;->e:LX/1OD;

    invoke-virtual {p1, v0}, LX/1OM;->b(LX/1OD;)V

    .line 242768
    :cond_0
    if-eqz p2, :cond_1

    .line 242769
    iget-object v0, p0, LX/1Oz;->e:LX/1OD;

    invoke-virtual {p2, v0}, LX/1OM;->a(LX/1OD;)V

    .line 242770
    :cond_1
    return-void
.end method

.method public final a(LX/1Od;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 242756
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    .line 242757
    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_2

    .line 242758
    invoke-virtual {p0, v2}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v3

    .line 242759
    invoke-direct {p0, v3}, LX/1Oz;->m(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242760
    iget-boolean v0, p0, LX/1Oz;->g:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v3, v0}, LX/1Oz;->a(Landroid/view/View;Z)V

    .line 242761
    :cond_0
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 242762
    goto :goto_1

    .line 242763
    :cond_2
    iput-boolean v1, p0, LX/1Oz;->g:Z

    .line 242764
    invoke-super {p0, p1}, LX/1P0;->a(LX/1Od;)V

    .line 242765
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;LX/1Od;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 242746
    move v2, v3

    :goto_0
    iget-object v0, p0, LX/1Oz;->c:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 242747
    iget-object v0, p0, LX/1Oz;->c:LX/0YU;

    iget-object v1, p0, LX/1Oz;->c:LX/0YU;

    invoke-virtual {v1, v2}, LX/0YU;->e(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    move v4, v3

    .line 242748
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v4, v1, :cond_0

    .line 242749
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1a1;

    iget-object v1, v1, LX/1a1;->a:Landroid/view/View;

    invoke-super {p0, v1, p2}, LX/1P0;->a(Landroid/view/View;LX/1Od;)V

    .line 242750
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 242751
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 242752
    :cond_1
    iget-object v0, p0, LX/1Oz;->c:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->b()V

    .line 242753
    iget-object v0, p0, LX/1Oz;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 242754
    invoke-super {p0, p1, p2}, LX/1P0;->a(Landroid/support/v7/widget/RecyclerView;LX/1Od;)V

    .line 242755
    return-void
.end method

.method public a(Landroid/view/View;LX/1Od;)V
    .locals 1

    .prologue
    .line 242742
    invoke-direct {p0, p1}, LX/1Oz;->m(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242743
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/1Oz;->a(Landroid/view/View;Z)V

    .line 242744
    :goto_0
    return-void

    .line 242745
    :cond_0
    invoke-super {p0, p1, p2}, LX/1P0;->a(Landroid/view/View;LX/1Od;)V

    goto :goto_0
.end method
