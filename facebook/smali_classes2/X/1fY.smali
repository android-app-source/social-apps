.class public final LX/1fY;
.super LX/1fZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1fZ",
        "<",
        "LX/1se;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 291801
    invoke-direct {p0}, LX/1fZ;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1tH;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1tH;",
            "Ljava/util/List",
            "<",
            "LX/1se;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 291802
    invoke-static {p2}, LX/1fU;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/1tH;->a(Ljava/util/List;LX/768;)V

    .line 291803
    return-void
.end method

.method public final a(ZLX/1tH;Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/1tH;",
            "Ljava/util/List",
            "<",
            "LX/1se;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/1se;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 291804
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 291805
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1se;

    .line 291806
    iget-object p0, v0, LX/1se;->a:Ljava/lang/String;

    move-object v0, p0

    .line 291807
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 291808
    :cond_0
    invoke-static {p3}, LX/1fU;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {p2, p1, v0, v1}, LX/1tH;->a(ZLjava/util/List;Ljava/util/List;)V

    .line 291809
    return-void
.end method

.method public final b(LX/1tH;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1tH;",
            "Ljava/util/List",
            "<",
            "LX/1se;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 291810
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 291811
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1se;

    .line 291812
    iget-object p0, v0, LX/1se;->a:Ljava/lang/String;

    move-object v0, p0

    .line 291813
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 291814
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1, v1, v0}, LX/1tH;->b(Ljava/util/List;LX/768;)V

    .line 291815
    return-void
.end method
