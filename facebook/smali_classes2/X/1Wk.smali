.class public final enum LX/1Wk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1Wk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1Wk;

.field public static final enum NEWSFEED_FULLBLEED_SHADOW:LX/1Wk;

.field public static final enum NEWSFEED_SHADOW:LX/1Wk;

.field public static final enum NO_SHADOW:LX/1Wk;

.field public static final enum SUBSTORY_SHADOW:LX/1Wk;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 269405
    new-instance v0, LX/1Wk;

    const-string v1, "NEWSFEED_SHADOW"

    invoke-direct {v0, v1, v2}, LX/1Wk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wk;->NEWSFEED_SHADOW:LX/1Wk;

    .line 269406
    new-instance v0, LX/1Wk;

    const-string v1, "NEWSFEED_FULLBLEED_SHADOW"

    invoke-direct {v0, v1, v3}, LX/1Wk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wk;->NEWSFEED_FULLBLEED_SHADOW:LX/1Wk;

    .line 269407
    new-instance v0, LX/1Wk;

    const-string v1, "SUBSTORY_SHADOW"

    invoke-direct {v0, v1, v4}, LX/1Wk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wk;->SUBSTORY_SHADOW:LX/1Wk;

    .line 269408
    new-instance v0, LX/1Wk;

    const-string v1, "NO_SHADOW"

    invoke-direct {v0, v1, v5}, LX/1Wk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wk;->NO_SHADOW:LX/1Wk;

    .line 269409
    const/4 v0, 0x4

    new-array v0, v0, [LX/1Wk;

    sget-object v1, LX/1Wk;->NEWSFEED_SHADOW:LX/1Wk;

    aput-object v1, v0, v2

    sget-object v1, LX/1Wk;->NEWSFEED_FULLBLEED_SHADOW:LX/1Wk;

    aput-object v1, v0, v3

    sget-object v1, LX/1Wk;->SUBSTORY_SHADOW:LX/1Wk;

    aput-object v1, v0, v4

    sget-object v1, LX/1Wk;->NO_SHADOW:LX/1Wk;

    aput-object v1, v0, v5

    sput-object v0, LX/1Wk;->$VALUES:[LX/1Wk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 269410
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1Wk;
    .locals 1

    .prologue
    .line 269411
    const-class v0, LX/1Wk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1Wk;

    return-object v0
.end method

.method public static values()[LX/1Wk;
    .locals 1

    .prologue
    .line 269412
    sget-object v0, LX/1Wk;->$VALUES:[LX/1Wk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1Wk;

    return-object v0
.end method
