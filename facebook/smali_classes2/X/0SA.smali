.class public LX/0SA;
.super LX/0R5;
.source ""

# interfaces
.implements LX/0R6;


# instance fields
.field public final a:LX/0RF;

.field private final b:LX/0QA;


# direct methods
.method public constructor <init>(LX/0QA;LX/0RF;)V
    .locals 0

    .prologue
    .line 60871
    invoke-direct {p0, p1}, LX/0R5;-><init>(LX/0QA;)V

    .line 60872
    iput-object p1, p0, LX/0SA;->b:LX/0QA;

    .line 60873
    iput-object p2, p0, LX/0SA;->a:LX/0RF;

    .line 60874
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 60870
    iget-object v0, p0, LX/0SA;->a:LX/0RF;

    invoke-virtual {v0}, LX/0RF;->enterScope()LX/0S7;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 60849
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "injectComponent should only be called on ContextScope"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 60868
    check-cast p1, LX/0S7;

    invoke-static {p1}, LX/0RF;->a(LX/0S7;)V

    .line 60869
    return-void
.end method

.method public final b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 60863
    iget-object v0, p0, LX/0SA;->a:LX/0RF;

    .line 60864
    iget-object p0, v0, LX/0RF;->b:LX/0RB;

    .line 60865
    iget-object v0, p0, LX/0RB;->a:Landroid/content/Context;

    move-object p0, v0

    .line 60866
    move-object v0, p0

    .line 60867
    return-object v0
.end method

.method public final d()LX/0SI;
    .locals 1

    .prologue
    .line 60862
    invoke-static {p0}, LX/0WI;->a(LX/0QB;)LX/0SI;

    move-result-object v0

    check-cast v0, LX/0SI;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 60861
    const/4 v0, 0x0

    return v0
.end method

.method public final getInstance(LX/0RI;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 60858
    iget-object v0, p0, LX/0SA;->a:LX/0RF;

    invoke-virtual {v0}, LX/0RF;->enterScope()LX/0S7;

    move-result-object v1

    .line 60859
    :try_start_0
    iget-object v0, p0, LX/0SA;->b:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getScopeUnawareInjector()LX/0QD;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getInstance(LX/0RI;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 60860
    invoke-static {v1}, LX/0RF;->a(LX/0S7;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/0RF;->a(LX/0S7;)V

    throw v0
.end method

.method public final getLazy(LX/0RI;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Ot",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 60857
    iget-object v0, p0, LX/0SA;->b:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getScopeUnawareInjector()LX/0QD;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    invoke-static {v0, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public final getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;>;)",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60854
    iget-object v0, p0, LX/0SA;->a:LX/0RF;

    invoke-virtual {v0}, LX/0RF;->enterScope()LX/0S7;

    move-result-object v1

    .line 60855
    :try_start_0
    iget-object v0, p0, LX/0SA;->b:LX/0QA;

    invoke-virtual {v0, p1}, LX/0QA;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 60856
    invoke-static {v1}, LX/0RF;->a(LX/0S7;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/0RF;->a(LX/0S7;)V

    throw v0
.end method

.method public final getProvider(LX/0RI;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 60851
    iget-object v0, p0, LX/0SA;->b:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getScopeUnawareInjector()LX/0QD;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    .line 60852
    new-instance p1, LX/4fs;

    invoke-direct {p1, p0, v0}, LX/4fs;-><init>(LX/0SA;LX/0Or;)V

    move-object v0, p1

    .line 60853
    return-object v0
.end method

.method public final getScopeAwareInjector()LX/0R6;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60850
    return-object p0
.end method
