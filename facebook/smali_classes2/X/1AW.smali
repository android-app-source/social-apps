.class public LX/1AW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1AX;


# instance fields
.field private a:LX/1AY;

.field private final b:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(LX/1AY;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 210528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210529
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, LX/1AW;->b:Landroid/graphics/Rect;

    .line 210530
    iput-object p1, p0, LX/1AW;->a:LX/1AY;

    .line 210531
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Set;Z)Landroid/view/View;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">(",
            "Ljava/util/Set",
            "<TV;>;Z)TV;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 210532
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 210533
    :cond_0
    return-object v2

    .line 210534
    :cond_1
    const/4 v1, 0x0

    .line 210535
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 210536
    iget-object v4, p0, LX/1AW;->a:LX/1AY;

    invoke-virtual {v4, v0, p2}, LX/1AY;->a(Landroid/view/View;Z)LX/7zf;

    move-result-object v4

    .line 210537
    sget-object v5, LX/7zf;->VISIBLE_FOR_AUTOPLAY:LX/7zf;

    if-ne v4, v5, :cond_3

    .line 210538
    iget-object v4, p0, LX/1AW;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v4}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 210539
    if-eqz v2, :cond_2

    iget-object v4, p0, LX/1AW;->b:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    if-ge v4, v1, :cond_3

    .line 210540
    :cond_2
    iget-object v1, p0, LX/1AW;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    move v6, v1

    move-object v1, v0

    move v0, v6

    :goto_1
    move-object v2, v1

    move v1, v0

    .line 210541
    goto :goto_0

    :cond_3
    move v0, v1

    move-object v1, v2

    goto :goto_1
.end method
