.class public final LX/1RN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/1kK;

.field public final b:LX/1lP;

.field public final c:LX/32e;


# direct methods
.method public constructor <init>(LX/1kK;LX/1lP;LX/32e;)V
    .locals 0

    .prologue
    .line 246097
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246098
    iput-object p1, p0, LX/1RN;->a:LX/1kK;

    .line 246099
    iput-object p2, p0, LX/1RN;->b:LX/1lP;

    .line 246100
    iput-object p3, p0, LX/1RN;->c:LX/32e;

    .line 246101
    return-void
.end method

.method public static a(LX/1RN;)LX/1kK;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 246102
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1RN;->a:LX/1kK;

    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 246103
    if-ne p0, p1, :cond_1

    .line 246104
    :cond_0
    :goto_0
    return v0

    .line 246105
    :cond_1
    if-eqz p1, :cond_2

    instance-of v2, p1, LX/1RN;

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 246106
    goto :goto_0

    .line 246107
    :cond_3
    check-cast p1, LX/1RN;

    .line 246108
    iget-object v2, p0, LX/1RN;->a:LX/1kK;

    iget-object v3, p1, LX/1RN;->a:LX/1kK;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/1RN;->b:LX/1lP;

    iget-object v3, p1, LX/1RN;->b:LX/1lP;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/1RN;->c:LX/32e;

    iget-object v3, p1, LX/1RN;->c:LX/32e;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 246109
    iget-object v0, p0, LX/1RN;->a:LX/1kK;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1RN;->a:LX/1kK;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 246110
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, LX/1RN;->b:LX/1lP;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1RN;->b:LX/1lP;

    invoke-virtual {v0}, LX/1lP;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 246111
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/1RN;->c:LX/32e;

    if-eqz v2, :cond_0

    iget-object v1, p0, LX/1RN;->c:LX/32e;

    invoke-virtual {v1}, LX/32e;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 246112
    return v0

    :cond_1
    move v0, v1

    .line 246113
    goto :goto_0

    :cond_2
    move v0, v1

    .line 246114
    goto :goto_1
.end method
