.class public LX/1N8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/0jV;
.implements LX/0sk;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1N8;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/4KY;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mObservers"
    .end annotation
.end field

.field private final b:LX/0t9;


# direct methods
.method public constructor <init>(LX/0t9;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 236725
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236726
    invoke-static {}, LX/1Ab;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/1N8;->a:Ljava/util/Set;

    .line 236727
    iput-object p1, p0, LX/1N8;->b:LX/0t9;

    .line 236728
    return-void
.end method

.method private a()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/4KY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 236729
    iget-object v1, p0, LX/1N8;->a:Ljava/util/Set;

    monitor-enter v1

    .line 236730
    :try_start_0
    iget-object v0, p0, LX/1N8;->a:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 236731
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(LX/0QB;)LX/1N8;
    .locals 4

    .prologue
    .line 236732
    sget-object v0, LX/1N8;->c:LX/1N8;

    if-nez v0, :cond_1

    .line 236733
    const-class v1, LX/1N8;

    monitor-enter v1

    .line 236734
    :try_start_0
    sget-object v0, LX/1N8;->c:LX/1N8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 236735
    if-eqz v2, :cond_0

    .line 236736
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 236737
    new-instance p0, LX/1N8;

    invoke-static {v0}, LX/0t9;->b(LX/0QB;)LX/0t9;

    move-result-object v3

    check-cast v3, LX/0t9;

    invoke-direct {p0, v3}, LX/1N8;-><init>(LX/0t9;)V

    .line 236738
    move-object v0, p0

    .line 236739
    sput-object v0, LX/1N8;->c:LX/1N8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236740
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 236741
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 236742
    :cond_1
    sget-object v0, LX/1N8;->c:LX/1N8;

    return-object v0

    .line 236743
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 236744
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/3DS;)V
    .locals 5

    .prologue
    .line 236745
    iget-object v0, p0, LX/3DS;->b:Ljava/util/ArrayList;

    move-object v0, v0

    .line 236746
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4KY;

    .line 236747
    :try_start_0
    iget v2, v0, LX/4KY;->f:I

    add-int/lit8 v3, v2, 0x1

    iput v3, v0, LX/4KY;->f:I

    .line 236748
    invoke-virtual {v0}, LX/4KY;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 236749
    :catch_0
    move-exception v0

    .line 236750
    const-string v2, "GraphQLObserverMemoryCache"

    const-string v3, "Callback failed"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 236751
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/3Bq;)V
    .locals 3

    .prologue
    .line 236752
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object v0

    .line 236753
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 236754
    :goto_0
    monitor-exit p0

    return-void

    .line 236755
    :cond_0
    :try_start_1
    new-instance v1, LX/3DS;

    invoke-direct {p0}, LX/1N8;->a()LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, LX/3DS;-><init>(LX/0Px;)V

    .line 236756
    iget-object v2, p0, LX/1N8;->b:LX/0t9;

    invoke-virtual {v2, v1, v0, p1}, LX/0t9;->a(LX/2vL;Ljava/util/Collection;LX/3Bq;)V

    .line 236757
    invoke-static {v1}, LX/1N8;->a(LX/3DS;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 236758
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/Collection;LX/3Bq;Ljava/util/Collection;Ljava/lang/String;)V
    .locals 2
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/3Bq;",
            "Ljava/util/Collection",
            "<",
            "LX/3Bq;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 236759
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/3DS;

    invoke-direct {p0}, LX/1N8;->a()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3DS;-><init>(LX/0Px;)V

    .line 236760
    iget-object v1, p0, LX/1N8;->b:LX/0t9;

    invoke-virtual {v1, v0, p1, p2, p3}, LX/0t9;->a(LX/2vL;Ljava/util/Collection;LX/3Bq;Ljava/util/Collection;)V

    .line 236761
    invoke-static {v0}, LX/1N8;->a(LX/3DS;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236762
    monitor-exit p0

    return-void

    .line 236763
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "LX/3Bq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 236764
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/3DS;

    invoke-direct {p0}, LX/1N8;->a()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3DS;-><init>(LX/0Px;)V

    .line 236765
    iget-object v1, p0, LX/1N8;->b:LX/0t9;

    invoke-virtual {v1, v0, p1, p2}, LX/0t9;->a(LX/2vL;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 236766
    invoke-static {v0}, LX/1N8;->a(LX/3DS;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236767
    monitor-exit p0

    return-void

    .line 236768
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final clearUserData()V
    .locals 2

    .prologue
    .line 236769
    iget-object v1, p0, LX/1N8;->a:Ljava/util/Set;

    monitor-enter v1

    .line 236770
    :try_start_0
    iget-object v0, p0, LX/1N8;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 236771
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
