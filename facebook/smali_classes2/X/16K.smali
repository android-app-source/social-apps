.class public final LX/16K;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 184934
    const-string v0, "-9223372036854775808"

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/16K;->a:Ljava/lang/String;

    .line 184935
    const-string v0, "9223372036854775807"

    sput-object v0, LX/16K;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 184801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;D)D
    .locals 3

    .prologue
    .line 184927
    if-nez p0, :cond_1

    .line 184928
    :cond_0
    :goto_0
    return-wide p1

    .line 184929
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 184930
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 184931
    if-eqz v1, :cond_0

    .line 184932
    :try_start_0
    invoke-static {v0}, LX/16K;->c(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p1

    goto :goto_0

    .line 184933
    :catch_0
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)I
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/16 v6, 0x39

    const/16 v5, 0x30

    const/4 v1, 0x1

    .line 184896
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 184897
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .line 184898
    const/16 v3, 0x2d

    if-ne v0, v3, :cond_2

    move v3, v1

    .line 184899
    :goto_0
    if-eqz v3, :cond_6

    .line 184900
    if-eq v4, v1, :cond_0

    const/16 v0, 0xa

    if-le v4, v0, :cond_3

    .line 184901
    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 184902
    :cond_1
    :goto_1
    return v0

    :cond_2
    move v3, v2

    .line 184903
    goto :goto_0

    .line 184904
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v0

    move v0, v1

    move v1, v7

    .line 184905
    :cond_4
    if-gt v0, v6, :cond_5

    if-ge v0, v5, :cond_7

    .line 184906
    :cond_5
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 184907
    :cond_6
    const/16 v2, 0x9

    if-le v4, v2, :cond_4

    .line 184908
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 184909
    :cond_7
    add-int/lit8 v0, v0, -0x30

    .line 184910
    if-ge v1, v4, :cond_e

    .line 184911
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 184912
    if-gt v1, v6, :cond_8

    if-ge v1, v5, :cond_9

    .line 184913
    :cond_8
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 184914
    :cond_9
    mul-int/lit8 v0, v0, 0xa

    add-int/lit8 v1, v1, -0x30

    add-int/2addr v0, v1

    .line 184915
    if-ge v2, v4, :cond_e

    .line 184916
    add-int/lit8 v1, v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 184917
    if-gt v2, v6, :cond_a

    if-ge v2, v5, :cond_b

    .line 184918
    :cond_a
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 184919
    :cond_b
    mul-int/lit8 v0, v0, 0xa

    add-int/lit8 v2, v2, -0x30

    add-int/2addr v0, v2

    .line 184920
    if-ge v1, v4, :cond_e

    .line 184921
    :goto_2
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 184922
    if-gt v1, v6, :cond_c

    if-ge v1, v5, :cond_d

    .line 184923
    :cond_c
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 184924
    :cond_d
    mul-int/lit8 v0, v0, 0xa

    add-int/lit8 v1, v1, -0x30

    add-int/2addr v0, v1

    .line 184925
    if-lt v2, v4, :cond_f

    .line 184926
    :cond_e
    if-eqz v3, :cond_1

    neg-int v0, v0

    goto :goto_1

    :cond_f
    move v1, v2

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;I)I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 184876
    if-nez p0, :cond_1

    .line 184877
    :cond_0
    :goto_0
    return p1

    .line 184878
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 184879
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    .line 184880
    if-eqz v2, :cond_0

    .line 184881
    if-lez v2, :cond_6

    .line 184882
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 184883
    const/16 v5, 0x2b

    if-ne v4, v5, :cond_3

    .line 184884
    invoke-virtual {v3, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 184885
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 184886
    :goto_1
    if-ge v0, v1, :cond_5

    .line 184887
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 184888
    const/16 v4, 0x39

    if-gt v3, v4, :cond_2

    const/16 v4, 0x30

    if-ge v3, v4, :cond_4

    .line 184889
    :cond_2
    :try_start_0
    invoke-static {v2}, LX/16K;->c(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    double-to-int p1, v0

    goto :goto_0

    .line 184890
    :cond_3
    const/16 v5, 0x2d

    if-ne v4, v5, :cond_6

    move v0, v1

    move v1, v2

    move-object v2, v3

    .line 184891
    goto :goto_1

    .line 184892
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 184893
    :cond_5
    :try_start_1
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result p1

    goto :goto_0

    .line 184894
    :catch_0
    goto :goto_0

    .line 184895
    :catch_1
    goto :goto_0

    :cond_6
    move v1, v2

    move-object v2, v3

    goto :goto_1
.end method

.method public static a([CII)I
    .locals 4

    .prologue
    .line 184857
    aget-char v0, p0, p1

    add-int/lit8 v0, v0, -0x30

    .line 184858
    add-int v1, p2, p1

    .line 184859
    add-int/lit8 v2, p1, 0x1

    if-ge v2, v1, :cond_0

    .line 184860
    mul-int/lit8 v0, v0, 0xa

    aget-char v3, p0, v2

    add-int/lit8 v3, v3, -0x30

    add-int/2addr v0, v3

    .line 184861
    add-int/lit8 v2, v2, 0x1

    if-ge v2, v1, :cond_0

    .line 184862
    mul-int/lit8 v0, v0, 0xa

    aget-char v3, p0, v2

    add-int/lit8 v3, v3, -0x30

    add-int/2addr v0, v3

    .line 184863
    add-int/lit8 v2, v2, 0x1

    if-ge v2, v1, :cond_0

    .line 184864
    mul-int/lit8 v0, v0, 0xa

    aget-char v3, p0, v2

    add-int/lit8 v3, v3, -0x30

    add-int/2addr v0, v3

    .line 184865
    add-int/lit8 v2, v2, 0x1

    if-ge v2, v1, :cond_0

    .line 184866
    mul-int/lit8 v0, v0, 0xa

    aget-char v3, p0, v2

    add-int/lit8 v3, v3, -0x30

    add-int/2addr v0, v3

    .line 184867
    add-int/lit8 v2, v2, 0x1

    if-ge v2, v1, :cond_0

    .line 184868
    mul-int/lit8 v0, v0, 0xa

    aget-char v3, p0, v2

    add-int/lit8 v3, v3, -0x30

    add-int/2addr v0, v3

    .line 184869
    add-int/lit8 v2, v2, 0x1

    if-ge v2, v1, :cond_0

    .line 184870
    mul-int/lit8 v0, v0, 0xa

    aget-char v3, p0, v2

    add-int/lit8 v3, v3, -0x30

    add-int/2addr v0, v3

    .line 184871
    add-int/lit8 v2, v2, 0x1

    if-ge v2, v1, :cond_0

    .line 184872
    mul-int/lit8 v0, v0, 0xa

    aget-char v3, p0, v2

    add-int/lit8 v3, v3, -0x30

    add-int/2addr v0, v3

    .line 184873
    add-int/lit8 v2, v2, 0x1

    if-ge v2, v1, :cond_0

    .line 184874
    mul-int/lit8 v0, v0, 0xa

    aget-char v1, p0, v2

    add-int/lit8 v1, v1, -0x30

    add-int/2addr v0, v1

    .line 184875
    :cond_0
    return v0
.end method

.method public static a(Ljava/lang/String;J)J
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 184837
    if-nez p0, :cond_1

    .line 184838
    :cond_0
    :goto_0
    return-wide p1

    .line 184839
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 184840
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    .line 184841
    if-eqz v2, :cond_0

    .line 184842
    if-lez v2, :cond_6

    .line 184843
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 184844
    const/16 v5, 0x2b

    if-ne v4, v5, :cond_3

    .line 184845
    invoke-virtual {v3, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 184846
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 184847
    :goto_1
    if-ge v0, v1, :cond_5

    .line 184848
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 184849
    const/16 v4, 0x39

    if-gt v3, v4, :cond_2

    const/16 v4, 0x30

    if-ge v3, v4, :cond_4

    .line 184850
    :cond_2
    :try_start_0
    invoke-static {v2}, LX/16K;->c(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    double-to-long p1, v0

    goto :goto_0

    .line 184851
    :cond_3
    const/16 v5, 0x2d

    if-ne v4, v5, :cond_6

    move v0, v1

    move v1, v2

    move-object v2, v3

    .line 184852
    goto :goto_1

    .line 184853
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 184854
    :cond_5
    :try_start_1
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide p1

    goto :goto_0

    .line 184855
    :catch_0
    goto :goto_0

    .line 184856
    :catch_1
    goto :goto_0

    :cond_6
    move v1, v2

    move-object v2, v3

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Z)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 184824
    if-eqz p1, :cond_0

    sget-object v0, LX/16K;->a:Ljava/lang/String;

    .line 184825
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    .line 184826
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 184827
    if-ge v3, v4, :cond_1

    move v0, v1

    .line 184828
    :goto_1
    return v0

    .line 184829
    :cond_0
    sget-object v0, LX/16K;->b:Ljava/lang/String;

    goto :goto_0

    .line 184830
    :cond_1
    if-le v3, v4, :cond_2

    move v0, v2

    goto :goto_1

    :cond_2
    move v3, v2

    .line 184831
    :goto_2
    if-ge v3, v4, :cond_5

    .line 184832
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v6

    sub-int/2addr v5, v6

    .line 184833
    if-eqz v5, :cond_4

    .line 184834
    if-gez v5, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1

    .line 184835
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    move v0, v1

    .line 184836
    goto :goto_1
.end method

.method public static a([CIIZ)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 184812
    if-eqz p3, :cond_0

    sget-object v0, LX/16K;->a:Ljava/lang/String;

    .line 184813
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    .line 184814
    if-ge p2, v4, :cond_1

    move v0, v1

    .line 184815
    :goto_1
    return v0

    .line 184816
    :cond_0
    sget-object v0, LX/16K;->b:Ljava/lang/String;

    goto :goto_0

    .line 184817
    :cond_1
    if-le p2, v4, :cond_2

    move v0, v2

    goto :goto_1

    :cond_2
    move v3, v2

    .line 184818
    :goto_2
    if-ge v3, v4, :cond_5

    .line 184819
    add-int v5, p1, v3

    aget-char v5, p0, v5

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v6

    sub-int/2addr v5, v6

    .line 184820
    if-eqz v5, :cond_4

    .line 184821
    if-gez v5, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1

    .line 184822
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    move v0, v1

    .line 184823
    goto :goto_1
.end method

.method public static b(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 184808
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 184809
    const/16 v1, 0x9

    if-gt v0, v1, :cond_0

    .line 184810
    invoke-static {p0}, LX/16K;->a(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    .line 184811
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static b([CII)J
    .locals 6

    .prologue
    .line 184805
    add-int/lit8 v0, p2, -0x9

    .line 184806
    invoke-static {p0, p1, v0}, LX/16K;->a([CII)I

    move-result v1

    int-to-long v2, v1

    const-wide/32 v4, 0x3b9aca00

    mul-long/2addr v2, v4

    .line 184807
    add-int/2addr v0, p1

    const/16 v1, 0x9

    invoke-static {p0, v0, v1}, LX/16K;->a([CII)I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public static c(Ljava/lang/String;)D
    .locals 2

    .prologue
    .line 184802
    const-string v0, "2.2250738585072012e-308"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184803
    const-wide/16 v0, 0x1

    .line 184804
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    goto :goto_0
.end method
