.class public LX/1Zw;
.super LX/0hD;
.source ""

# interfaces
.implements LX/1KU;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1Rb;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Zx;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0Sh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 275657
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 275658
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1Zw;->a:Ljava/util/Set;

    .line 275659
    return-void
.end method

.method public static a(LX/0QB;)LX/1Zw;
    .locals 7

    .prologue
    .line 275660
    const-class v1, LX/1Zw;

    monitor-enter v1

    .line 275661
    :try_start_0
    sget-object v0, LX/1Zw;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 275662
    sput-object v2, LX/1Zw;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 275663
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275664
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 275665
    new-instance p0, LX/1Zw;

    invoke-direct {p0}, LX/1Zw;-><init>()V

    .line 275666
    invoke-static {v0}, LX/1Zx;->a(LX/0QB;)LX/1Zx;

    move-result-object v3

    check-cast v3, LX/1Zx;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v6

    check-cast v6, LX/0Sh;

    .line 275667
    iput-object v3, p0, LX/1Zw;->b:LX/1Zx;

    iput-object v4, p0, LX/1Zw;->c:LX/0Uh;

    iput-object v5, p0, LX/1Zw;->d:LX/03V;

    iput-object v6, p0, LX/1Zw;->e:LX/0Sh;

    .line 275668
    move-object v0, p0

    .line 275669
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 275670
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Zw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 275671
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 275672
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1OP;)V
    .locals 7

    .prologue
    .line 275673
    const-string v0, "NativeInjectionLoggerController.onAdapterDataChanged"

    const v1, -0x74d0d7f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 275674
    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p1}, LX/1OP;->ij_()I

    move-result v1

    .line 275675
    :goto_0
    if-ge v0, v1, :cond_1

    .line 275676
    invoke-interface {p1, v0}, LX/1OP;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    .line 275677
    instance-of v3, v2, LX/1Rk;

    if-eqz v3, :cond_0

    .line 275678
    check-cast v2, LX/1Rk;

    .line 275679
    invoke-virtual {v2}, LX/1Rk;->e()LX/1Rb;

    move-result-object v3

    .line 275680
    iget-object v4, p0, LX/1Zw;->a:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 275681
    iget-object v4, p0, LX/1Zw;->a:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 275682
    invoke-virtual {v2}, LX/1Rk;->f()Ljava/lang/Object;

    move-result-object v3

    .line 275683
    instance-of v4, v3, LX/16i;

    if-eqz v4, :cond_0

    .line 275684
    check-cast v3, LX/16i;

    invoke-interface {v3}, LX/16i;->a()Ljava/lang/String;

    move-result-object v3

    .line 275685
    iget-object v4, p0, LX/1Zw;->b:LX/1Zx;

    .line 275686
    iget-object v5, v2, LX/1Rk;->a:LX/1RA;

    iget v6, v2, LX/1Rk;->b:I

    .line 275687
    invoke-static {v5}, LX/1RA;->e(LX/1RA;)V

    .line 275688
    iget-object v2, v5, LX/1RA;->k:LX/0Px;

    invoke-virtual {v2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1RZ;

    .line 275689
    iget-object v5, v2, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    move-object v2, v5

    .line 275690
    move-object v5, v2

    .line 275691
    move-object v2, v5

    .line 275692
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "triggered_code"

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 275693
    const-string v6, "native_injection"

    .line 275694
    iput-object v6, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 275695
    const-string v6, "code_path"

    invoke-virtual {v5, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 275696
    const-string v6, "story_id"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 275697
    iget-object v6, v4, LX/1Zx;->a:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 275698
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275699
    :cond_1
    const v0, 0x3687eab8

    invoke-static {v0}, LX/02m;->a(I)V

    .line 275700
    return-void

    .line 275701
    :catchall_0
    move-exception v0

    const v1, 0x345fd85c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final kw_()Z
    .locals 3

    .prologue
    .line 275702
    iget-object v0, p0, LX/1Zw;->c:LX/0Uh;

    const/16 v1, 0x3b5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
