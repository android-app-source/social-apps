.class public LX/0ZO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0VI;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/push/mqtt/service/MqttPushServiceClientManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/push/mqtt/service/MqttPushServiceClientManager;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 83266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83267
    iput-object p1, p0, LX/0ZO;->a:LX/0Ot;

    .line 83268
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83269
    const-string v0, "mqtt_health_stats"

    return-object v0
.end method

.method public final a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 83270
    if-eqz p1, :cond_0

    .line 83271
    const/4 v0, 0x0

    .line 83272
    :goto_0
    return-object v0

    .line 83273
    :cond_0
    iget-object v0, p0, LX/0ZO;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Hu;

    invoke-virtual {v0}, LX/2Hu;->a()LX/2gV;

    move-result-object v1

    .line 83274
    :try_start_0
    invoke-virtual {v1}, LX/2gV;->d()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 83275
    invoke-virtual {v1}, LX/2gV;->f()V

    goto :goto_0

    .line 83276
    :catch_0
    move-exception v0

    .line 83277
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 83278
    invoke-virtual {v1}, LX/2gV;->f()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2gV;->f()V

    throw v0
.end method
