.class public final LX/0hW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/Beta;
.end annotation


# static fields
.field public static final a:[B

.field public static final b:Ljava/io/OutputStream;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 117783
    const/16 v0, 0x2000

    new-array v0, v0, [B

    sput-object v0, LX/0hW;->a:[B

    .line 117784
    new-instance v0, LX/0hX;

    invoke-direct {v0}, LX/0hX;-><init>()V

    sput-object v0, LX/0hW;->b:Ljava/io/OutputStream;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 117785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/io/InputStream;[BII)I
    .locals 3

    .prologue
    .line 117786
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 117787
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 117788
    if-gez p3, :cond_0

    .line 117789
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "len is negative"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117790
    :cond_0
    const/4 v0, 0x0

    .line 117791
    :goto_0
    if-ge v0, p3, :cond_1

    .line 117792
    add-int v1, p2, v0

    sub-int v2, p3, v0

    invoke-virtual {p0, p1, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 117793
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 117794
    add-int/2addr v0, v1

    .line 117795
    goto :goto_0

    .line 117796
    :cond_1
    return v0
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    .locals 6

    .prologue
    .line 117797
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 117798
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 117799
    const/16 v0, 0x2000

    new-array v2, v0, [B

    .line 117800
    const-wide/16 v0, 0x0

    .line 117801
    :goto_0
    invoke-virtual {p0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 117802
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 117803
    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 117804
    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 117805
    goto :goto_0

    .line 117806
    :cond_0
    return-wide v0
.end method

.method public static a(Ljava/io/InputStream;[B)V
    .locals 2

    .prologue
    .line 117807
    const/4 v0, 0x0

    array-length v1, p1

    invoke-static {p0, p1, v0, v1}, LX/0hW;->b(Ljava/io/InputStream;[BII)V

    .line 117808
    return-void
.end method

.method public static a(Ljava/io/InputStream;)[B
    .locals 1

    .prologue
    .line 117809
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 117810
    invoke-static {p0, v0}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 117811
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/io/InputStream;J)V
    .locals 5

    .prologue
    .line 117812
    invoke-static {p0, p1, p2}, LX/0hW;->c(Ljava/io/InputStream;J)J

    move-result-wide v0

    .line 117813
    cmp-long v2, v0, p1

    if-gez v2, :cond_0

    .line 117814
    new-instance v2, Ljava/io/EOFException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reached end of stream after skipping "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " bytes; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " bytes expected"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 117815
    :cond_0
    return-void
.end method

.method private static b(Ljava/io/InputStream;[BII)V
    .locals 4

    .prologue
    .line 117816
    invoke-static {p0, p1, p2, p3}, LX/0hW;->a(Ljava/io/InputStream;[BII)I

    move-result v0

    .line 117817
    if-eq v0, p3, :cond_0

    .line 117818
    new-instance v1, Ljava/io/EOFException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reached end of stream after reading "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " bytes; "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " bytes expected"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 117819
    :cond_0
    return-void
.end method

.method public static c(Ljava/io/InputStream;J)J
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    .line 117820
    move-wide v2, v4

    .line 117821
    :goto_0
    cmp-long v0, v2, p1

    if-gez v0, :cond_1

    .line 117822
    sub-long v6, p1, v2

    .line 117823
    invoke-virtual {p0}, Ljava/io/InputStream;->available()I

    move-result v9

    .line 117824
    if-nez v9, :cond_2

    const-wide/16 v9, 0x0

    :goto_1
    move-wide v0, v9

    .line 117825
    cmp-long v8, v0, v4

    if-nez v8, :cond_0

    .line 117826
    sget-object v0, LX/0hW;->a:[B

    array-length v0, v0

    int-to-long v0, v0

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    .line 117827
    sget-object v1, LX/0hW;->a:[B

    const/4 v6, 0x0

    invoke-virtual {p0, v1, v6, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v6, -0x1

    cmp-long v6, v0, v6

    if-eqz v6, :cond_1

    .line 117828
    :cond_0
    add-long/2addr v0, v2

    move-wide v2, v0

    .line 117829
    goto :goto_0

    .line 117830
    :cond_1
    return-wide v2

    :cond_2
    int-to-long v9, v9

    invoke-static {v9, v10, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v9

    invoke-virtual {p0, v9, v10}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v9

    goto :goto_1
.end method
