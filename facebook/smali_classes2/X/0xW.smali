.class public LX/0xW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 163169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163170
    iput-object p1, p0, LX/0xW;->a:LX/0ad;

    .line 163171
    return-void
.end method

.method public static a(LX/0QB;)LX/0xW;
    .locals 4

    .prologue
    .line 163172
    const-class v1, LX/0xW;

    monitor-enter v1

    .line 163173
    :try_start_0
    sget-object v0, LX/0xW;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 163174
    sput-object v2, LX/0xW;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 163175
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163176
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 163177
    new-instance p0, LX/0xW;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/0xW;-><init>(LX/0ad;)V

    .line 163178
    move-object v0, p0

    .line 163179
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 163180
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/0xW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163181
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 163182
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final B()J
    .locals 6

    .prologue
    .line 163183
    iget-object v0, p0, LX/0xW;->a:LX/0ad;

    sget-wide v2, LX/0xc;->u:J

    const-wide/16 v4, 0x1e

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final G()J
    .locals 6

    .prologue
    .line 163184
    iget-object v0, p0, LX/0xW;->a:LX/0ad;

    sget-wide v2, LX/0xc;->F:J

    const-wide/16 v4, 0x0

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 163185
    iget-object v0, p0, LX/0xW;->a:LX/0ad;

    sget-short v1, LX/0xc;->p:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 163186
    invoke-virtual {p0}, LX/0xW;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163187
    iget-object v0, p0, LX/0xW;->a:LX/0ad;

    sget-short v1, LX/0xc;->o:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 163188
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 163189
    iget-object v0, p0, LX/0xW;->a:LX/0ad;

    sget-short v1, LX/0xc;->E:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final j()I
    .locals 3

    .prologue
    .line 163190
    iget-object v0, p0, LX/0xW;->a:LX/0ad;

    sget v1, LX/0xc;->B:I

    const/4 v2, 0x6

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    return v0
.end method

.method public final k()I
    .locals 3

    .prologue
    .line 163167
    iget-object v0, p0, LX/0xW;->a:LX/0ad;

    sget v1, LX/0xc;->C:I

    const/16 v2, 0x1e

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    return v0
.end method

.method public final l()J
    .locals 6

    .prologue
    .line 163168
    iget-object v0, p0, LX/0xW;->a:LX/0ad;

    sget-wide v2, LX/0xc;->A:J

    const-wide/16 v4, 0x3c

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final o()Z
    .locals 3

    .prologue
    .line 163156
    iget-object v0, p0, LX/0xW;->a:LX/0ad;

    sget-short v1, LX/0xc;->s:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final p()Z
    .locals 3

    .prologue
    .line 163157
    iget-object v0, p0, LX/0xW;->a:LX/0ad;

    sget-short v1, LX/0xc;->r:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final q()Z
    .locals 3

    .prologue
    .line 163158
    iget-object v0, p0, LX/0xW;->a:LX/0ad;

    sget-short v1, LX/0xc;->n:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 3

    .prologue
    .line 163159
    iget-object v0, p0, LX/0xW;->a:LX/0ad;

    sget-short v1, LX/0xc;->m:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final s()Z
    .locals 3

    .prologue
    .line 163160
    iget-object v0, p0, LX/0xW;->a:LX/0ad;

    sget-short v1, LX/0xc;->q:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 163161
    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0xW;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Z
    .locals 3

    .prologue
    .line 163162
    iget-object v0, p0, LX/0xW;->a:LX/0ad;

    sget-short v1, LX/0xc;->c:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final u()Z
    .locals 3

    .prologue
    .line 163163
    iget-object v0, p0, LX/0xW;->a:LX/0ad;

    sget-short v1, LX/0xc;->e:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final v()Z
    .locals 3

    .prologue
    .line 163164
    iget-object v0, p0, LX/0xW;->a:LX/0ad;

    sget-short v1, LX/0xc;->d:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final w()Z
    .locals 3

    .prologue
    .line 163165
    iget-object v0, p0, LX/0xW;->a:LX/0ad;

    sget-short v1, LX/0xc;->b:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final y()Z
    .locals 3

    .prologue
    .line 163166
    iget-object v0, p0, LX/0xW;->a:LX/0ad;

    sget-short v1, LX/0xc;->v:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
