.class public LX/0S1;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0RV",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0RI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0RI",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private volatile b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0RI;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0RI",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 60604
    invoke-direct {p0}, LX/0RV;-><init>()V

    .line 60605
    iput-object p1, p0, LX/0S1;->a:LX/0RI;

    .line 60606
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 60607
    iget-object v0, p0, LX/0S1;->b:LX/0Or;

    if-nez v0, :cond_0

    .line 60608
    invoke-virtual {p0}, LX/0RV;->getScopeUnawareInjector()LX/0QD;

    move-result-object v0

    iget-object v1, p0, LX/0S1;->a:LX/0RI;

    invoke-interface {v0, v1}, LX/0QC;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    iput-object v0, p0, LX/0S1;->b:LX/0Or;

    .line 60609
    :cond_0
    iget-object v0, p0, LX/0S1;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
