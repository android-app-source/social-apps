.class public LX/1gH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1gI;


# instance fields
.field public final a:Ljava/io/File;


# direct methods
.method private constructor <init>(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 293932
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 293933
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, LX/1gH;->a:Ljava/io/File;

    .line 293934
    return-void
.end method

.method public static a(Ljava/io/File;)LX/1gH;
    .locals 1

    .prologue
    .line 293935
    if-eqz p0, :cond_0

    new-instance v0, LX/1gH;

    invoke-direct {v0, p0}, LX/1gH;-><init>(Ljava/io/File;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 293936
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, p0, LX/1gH;->a:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method public final b()[B
    .locals 1

    .prologue
    .line 293937
    iget-object v0, p0, LX/1gH;->a:Ljava/io/File;

    invoke-static {v0}, LX/45W;->a(Ljava/io/File;)[B

    move-result-object v0

    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 293938
    iget-object v0, p0, LX/1gH;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 293939
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/1gH;

    if-nez v0, :cond_1

    .line 293940
    :cond_0
    const/4 v0, 0x0

    .line 293941
    :goto_0
    return v0

    .line 293942
    :cond_1
    check-cast p1, LX/1gH;

    .line 293943
    iget-object v0, p0, LX/1gH;->a:Ljava/io/File;

    iget-object v1, p1, LX/1gH;->a:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 293944
    iget-object v0, p0, LX/1gH;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->hashCode()I

    move-result v0

    return v0
.end method
