.class public LX/1kP;
.super LX/1kQ;
.source ""


# instance fields
.field private final b:LX/0TD;

.field public final c:LX/1kT;


# direct methods
.method public constructor <init>(LX/0tX;LX/1kR;LX/1Ck;LX/0Or;LX/0Or;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1kS;LX/1kT;LX/0TD;LX/03V;LX/0ad;LX/0fO;)V
    .locals 12
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/inlinecomposer/abtest/IsInlineComposerPromptEnabled;
        .end annotation
    .end param
    .param p9    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/1kR;",
            "LX/1Ck;",
            "LX/0Or",
            "<",
            "LX/0si;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/1kS;",
            "LX/1kT;",
            "LX/0TD;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0ad;",
            "LX/0fO;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 309539
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    move-object/from16 v11, p12

    invoke-direct/range {v1 .. v11}, LX/1kQ;-><init>(LX/0tX;LX/1kR;LX/1Ck;LX/0Or;LX/0Or;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1kS;LX/03V;LX/0ad;LX/0fO;)V

    .line 309540
    move-object/from16 v0, p8

    iput-object v0, p0, LX/1kP;->c:LX/1kT;

    .line 309541
    move-object/from16 v0, p9

    iput-object v0, p0, LX/1kP;->b:LX/0TD;

    .line 309542
    return-void
.end method

.method public static c(LX/0QB;)LX/1kP;
    .locals 13

    .prologue
    .line 309536
    new-instance v0, LX/1kP;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/1kR;->b(LX/0QB;)LX/1kR;

    move-result-object v2

    check-cast v2, LX/1kR;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    const/16 v4, 0xb0f

    invoke-static {p0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x319

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v6

    check-cast v6, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0}, LX/1kS;->a(LX/0QB;)LX/1kS;

    move-result-object v7

    check-cast v7, LX/1kS;

    invoke-static {p0}, LX/1kT;->a(LX/0QB;)LX/1kT;

    move-result-object v8

    check-cast v8, LX/1kT;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, LX/0TD;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-static {p0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v12

    check-cast v12, LX/0fO;

    invoke-direct/range {v0 .. v12}, LX/1kP;-><init>(LX/0tX;LX/1kR;LX/1Ck;LX/0Or;LX/0Or;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1kS;LX/1kT;LX/0TD;LX/03V;LX/0ad;LX/0fO;)V

    .line 309537
    return-object v0
.end method


# virtual methods
.method public final a(Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "LX/1kK;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 309538
    iget-object v0, p0, LX/1kP;->b:LX/0TD;

    new-instance v1, LX/1kn;

    invoke-direct {v1, p0}, LX/1kn;-><init>(LX/1kP;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 309528
    const-class v0, LX/1kV;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 309529
    invoke-virtual {p0}, LX/1kQ;->c()V

    .line 309530
    iget-object v0, p0, LX/1kP;->c:LX/1kT;

    invoke-virtual {v0}, LX/1kT;->c()V

    .line 309531
    return-void
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 309533
    const-class v0, LX/1kV;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 309534
    iget-object v0, p0, LX/1kP;->c:LX/1kT;

    invoke-virtual {v0}, LX/1kT;->c()V

    .line 309535
    return-void
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309532
    const-class v0, LX/1kV;

    return-object v0
.end method
