.class public final LX/0mf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0mg;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uq;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0mi;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:LX/0mi;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0Uq;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 132712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132713
    iput-object p1, p0, LX/0mf;->a:LX/0Ot;

    .line 132714
    iput-object p2, p0, LX/0mf;->b:LX/0Ot;

    .line 132715
    return-void
.end method

.method private c()V
    .locals 12

    .prologue
    .line 132716
    iget-object v0, p0, LX/0mf;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uq;

    invoke-static {v0}, LX/2DE;->b(LX/0Uq;)V

    .line 132717
    const-string v0, "readForegroundParamsQEData"

    const v1, 0x7f745bcf

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 132718
    :try_start_0
    new-instance v1, LX/0mi;

    iget-object v0, p0, LX/0mf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-wide v2, LX/2DF;->h:J

    sget-wide v4, LX/2DE;->a:J

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v2

    iget-object v0, p0, LX/0mf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-wide v4, LX/2DF;->f:J

    sget-wide v6, LX/2DE;->b:J

    invoke-interface {v0, v4, v5, v6, v7}, LX/0ad;->a(JJ)J

    move-result-wide v4

    iget-object v0, p0, LX/0mf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-wide v6, LX/2DF;->g:J

    const-wide/16 v8, 0x0

    invoke-interface {v0, v6, v7, v8, v9}, LX/0ad;->a(JJ)J

    move-result-wide v6

    iget-object v0, p0, LX/0mf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-wide v8, LX/2DF;->e:J

    sget-wide v10, LX/2DE;->c:J

    invoke-interface {v0, v8, v9, v10, v11}, LX/0ad;->a(JJ)J

    move-result-wide v8

    invoke-direct/range {v1 .. v9}, LX/0mi;-><init>(JJJJ)V

    iput-object v1, p0, LX/0mf;->c:LX/0mi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132719
    const v0, -0x3f539481

    invoke-static {v0}, LX/03q;->a(I)V

    .line 132720
    return-void

    .line 132721
    :catchall_0
    move-exception v0

    const v1, 0x48adf862

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
.end method

.method private d()V
    .locals 12

    .prologue
    .line 132722
    iget-object v0, p0, LX/0mf;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uq;

    invoke-static {v0}, LX/2DE;->b(LX/0Uq;)V

    .line 132723
    const-string v0, "readBackgroundParamsQEData"

    const v1, 0x6ea934e8

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 132724
    :try_start_0
    new-instance v1, LX/0mi;

    iget-object v0, p0, LX/0mf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-wide v2, LX/2DF;->d:J

    sget-wide v4, LX/2DE;->a:J

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v2

    iget-object v0, p0, LX/0mf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-wide v4, LX/2DF;->b:J

    sget-wide v6, LX/2DE;->b:J

    invoke-interface {v0, v4, v5, v6, v7}, LX/0ad;->a(JJ)J

    move-result-wide v4

    iget-object v0, p0, LX/0mf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-wide v6, LX/2DF;->c:J

    const-wide/16 v8, 0x0

    invoke-interface {v0, v6, v7, v8, v9}, LX/0ad;->a(JJ)J

    move-result-wide v6

    iget-object v0, p0, LX/0mf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-wide v8, LX/2DF;->a:J

    sget-wide v10, LX/2DE;->c:J

    invoke-interface {v0, v8, v9, v10, v11}, LX/0ad;->a(JJ)J

    move-result-wide v8

    invoke-direct/range {v1 .. v9}, LX/0mi;-><init>(JJJJ)V

    iput-object v1, p0, LX/0mf;->d:LX/0mi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132725
    const v0, 0x6389fd94

    invoke-static {v0}, LX/03q;->a(I)V

    .line 132726
    return-void

    .line 132727
    :catchall_0
    move-exception v0

    const v1, 0x38ed2c98

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
.end method


# virtual methods
.method public final a()LX/0mi;
    .locals 1

    .prologue
    .line 132728
    iget-object v0, p0, LX/0mf;->c:LX/0mi;

    if-nez v0, :cond_0

    .line 132729
    invoke-direct {p0}, LX/0mf;->c()V

    .line 132730
    :cond_0
    iget-object v0, p0, LX/0mf;->c:LX/0mi;

    return-object v0
.end method

.method public final b()LX/0mi;
    .locals 1

    .prologue
    .line 132731
    iget-object v0, p0, LX/0mf;->d:LX/0mi;

    if-nez v0, :cond_0

    .line 132732
    invoke-direct {p0}, LX/0mf;->d()V

    .line 132733
    :cond_0
    iget-object v0, p0, LX/0mf;->d:LX/0mi;

    return-object v0
.end method
