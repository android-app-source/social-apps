.class public LX/1E0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Uh;

.field private final b:Z


# direct methods
.method public constructor <init>(LX/0Uh;Ljava/lang/Boolean;)V
    .locals 1
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 219040
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219041
    iput-object p1, p0, LX/1E0;->a:LX/0Uh;

    .line 219042
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/1E0;->b:Z

    .line 219043
    return-void
.end method

.method public static a(LX/0QB;)LX/1E0;
    .locals 1

    .prologue
    .line 219044
    invoke-static {p0}, LX/1E0;->b(LX/0QB;)LX/1E0;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1E0;
    .locals 3

    .prologue
    .line 219045
    new-instance v2, LX/1E0;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-direct {v2, v0, v1}, LX/1E0;-><init>(LX/0Uh;Ljava/lang/Boolean;)V

    .line 219046
    return-object v2
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 219047
    iget-boolean v1, p0, LX/1E0;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1E0;->a:LX/0Uh;

    const/16 v2, 0x67a

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 219048
    iget-boolean v1, p0, LX/1E0;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1E0;->a:LX/0Uh;

    const/16 v2, 0x623

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
