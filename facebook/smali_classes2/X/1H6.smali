.class public LX/1H6;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static x:LX/1H7;


# instance fields
.field private final a:LX/1GA;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Landroid/graphics/Bitmap$Config;

.field public final c:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "LX/1HR;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/1Ao;

.field public final e:Landroid/content/Context;

.field private final f:Z

.field public final g:LX/1HC;

.field public final h:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "LX/1HR;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/1Ft;

.field public final j:LX/1GF;

.field public final k:LX/1GL;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/1Gf;

.field public final n:LX/0rb;

.field public final o:LX/1Gj;

.field private final p:LX/1FZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:LX/1FB;

.field private final r:LX/1Gv;

.field public final s:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1BU;",
            ">;"
        }
    .end annotation
.end field

.field public final t:Z

.field public final u:LX/1Gf;

.field public final v:LX/4e5;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final w:LX/1HF;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 226443
    new-instance v0, LX/1H7;

    invoke-direct {v0}, LX/1H7;-><init>()V

    sput-object v0, LX/1H6;->x:LX/1H7;

    return-void
.end method

.method public constructor <init>(LX/1H8;)V
    .locals 4

    .prologue
    .line 226377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226378
    iget-object v0, p1, LX/1H8;->w:LX/1H9;

    .line 226379
    new-instance v1, LX/1HF;

    iget-object v2, v0, LX/1H9;->a:LX/1H8;

    invoke-direct {v1, v0, v2}, LX/1HF;-><init>(LX/1H9;LX/1H8;)V

    move-object v0, v1

    .line 226380
    iput-object v0, p0, LX/1H6;->w:LX/1HF;

    .line 226381
    iget-object v0, p1, LX/1H8;->a:LX/1GA;

    iput-object v0, p0, LX/1H6;->a:LX/1GA;

    .line 226382
    iget-object v0, p1, LX/1H8;->c:LX/1Gd;

    if-nez v0, :cond_1

    new-instance v1, LX/1Gz;

    iget-object v0, p1, LX/1H8;->e:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-direct {v1, v0}, LX/1Gz;-><init>(Landroid/app/ActivityManager;)V

    move-object v0, v1

    :goto_0
    iput-object v0, p0, LX/1H6;->c:LX/1Gd;

    .line 226383
    iget-object v0, p1, LX/1H8;->b:Landroid/graphics/Bitmap$Config;

    if-nez v0, :cond_2

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    :goto_1
    iput-object v0, p0, LX/1H6;->b:Landroid/graphics/Bitmap$Config;

    .line 226384
    iget-object v0, p1, LX/1H8;->d:LX/1Ao;

    if-nez v0, :cond_3

    invoke-static {}, LX/1Ao;->a()LX/1Ao;

    move-result-object v0

    :goto_2
    iput-object v0, p0, LX/1H6;->d:LX/1Ao;

    .line 226385
    iget-object v0, p1, LX/1H8;->e:Landroid/content/Context;

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LX/1H6;->e:Landroid/content/Context;

    .line 226386
    iget-object v0, p1, LX/1H8;->u:LX/1HC;

    if-nez v0, :cond_4

    new-instance v0, LX/1He;

    new-instance v1, LX/1Hf;

    invoke-direct {v1}, LX/1Hf;-><init>()V

    invoke-direct {v0, v1}, LX/1He;-><init>(LX/1Hf;)V

    :goto_3
    iput-object v0, p0, LX/1H6;->g:LX/1HC;

    .line 226387
    iget-boolean v0, p1, LX/1H8;->f:Z

    iput-boolean v0, p0, LX/1H6;->f:Z

    .line 226388
    iget-object v0, p1, LX/1H8;->g:LX/1Gd;

    if-nez v0, :cond_5

    new-instance v0, LX/1HG;

    invoke-direct {v0}, LX/1HG;-><init>()V

    :goto_4
    iput-object v0, p0, LX/1H6;->h:LX/1Gd;

    .line 226389
    iget-object v0, p1, LX/1H8;->i:LX/1GF;

    if-nez v0, :cond_6

    invoke-static {}, LX/4ds;->a()LX/4ds;

    move-result-object v0

    :goto_5
    iput-object v0, p0, LX/1H6;->j:LX/1GF;

    .line 226390
    iget-object v0, p1, LX/1H8;->j:LX/1GL;

    iput-object v0, p0, LX/1H6;->k:LX/1GL;

    .line 226391
    iget-object v0, p1, LX/1H8;->k:LX/1Gd;

    if-nez v0, :cond_7

    new-instance v0, LX/4dy;

    invoke-direct {v0, p0}, LX/4dy;-><init>(LX/1H6;)V

    :goto_6
    iput-object v0, p0, LX/1H6;->l:LX/1Gd;

    .line 226392
    iget-object v0, p1, LX/1H8;->l:LX/1Gf;

    if-nez v0, :cond_8

    iget-object v0, p1, LX/1H8;->e:Landroid/content/Context;

    .line 226393
    invoke-static {v0}, LX/1Gf;->a(Landroid/content/Context;)LX/1Gg;

    move-result-object v1

    invoke-virtual {v1}, LX/1Gg;->a()LX/1Gf;

    move-result-object v1

    move-object v0, v1

    .line 226394
    :goto_7
    iput-object v0, p0, LX/1H6;->m:LX/1Gf;

    .line 226395
    iget-object v0, p1, LX/1H8;->m:LX/0rb;

    if-nez v0, :cond_9

    invoke-static {}, LX/1IX;->a()LX/1IX;

    move-result-object v0

    :goto_8
    iput-object v0, p0, LX/1H6;->n:LX/0rb;

    .line 226396
    iget-object v0, p1, LX/1H8;->n:LX/1Gj;

    if-nez v0, :cond_a

    new-instance v0, LX/4eu;

    invoke-direct {v0}, LX/4eu;-><init>()V

    :goto_9
    iput-object v0, p0, LX/1H6;->o:LX/1Gj;

    .line 226397
    iget-object v0, p1, LX/1H8;->o:LX/1FZ;

    iput-object v0, p0, LX/1H6;->p:LX/1FZ;

    .line 226398
    iget-object v0, p1, LX/1H8;->p:LX/1FB;

    if-nez v0, :cond_b

    new-instance v0, LX/1FB;

    invoke-static {}, LX/1F8;->newBuilder()LX/1F9;

    move-result-object v1

    invoke-virtual {v1}, LX/1F9;->a()LX/1F8;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1FB;-><init>(LX/1F8;)V

    :goto_a
    iput-object v0, p0, LX/1H6;->q:LX/1FB;

    .line 226399
    iget-object v0, p1, LX/1H8;->q:LX/1Gv;

    if-nez v0, :cond_c

    new-instance v0, LX/1Gv;

    invoke-direct {v0}, LX/1Gv;-><init>()V

    :goto_b
    iput-object v0, p0, LX/1H6;->r:LX/1Gv;

    .line 226400
    iget-object v0, p1, LX/1H8;->r:Ljava/util/Set;

    if-nez v0, :cond_d

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    :goto_c
    iput-object v0, p0, LX/1H6;->s:Ljava/util/Set;

    .line 226401
    iget-boolean v0, p1, LX/1H8;->s:Z

    iput-boolean v0, p0, LX/1H6;->t:Z

    .line 226402
    iget-object v0, p1, LX/1H8;->t:LX/1Gf;

    if-nez v0, :cond_e

    iget-object v0, p0, LX/1H6;->m:LX/1Gf;

    :goto_d
    iput-object v0, p0, LX/1H6;->u:LX/1Gf;

    .line 226403
    iget-object v0, p1, LX/1H8;->v:LX/4e5;

    iput-object v0, p0, LX/1H6;->v:LX/4e5;

    .line 226404
    iget-object v0, p0, LX/1H6;->q:LX/1FB;

    invoke-virtual {v0}, LX/1FB;->c()I

    move-result v1

    .line 226405
    iget-object v0, p1, LX/1H8;->h:LX/1Ft;

    if-nez v0, :cond_f

    new-instance v0, LX/4du;

    invoke-direct {v0, v1}, LX/4du;-><init>(I)V

    :goto_e
    iput-object v0, p0, LX/1H6;->i:LX/1Ft;

    .line 226406
    iget-object v0, p0, LX/1H6;->w:LX/1HF;

    .line 226407
    iget-object v1, v0, LX/1HF;->h:Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;

    move-object v0, v1

    .line 226408
    if-eqz v0, :cond_10

    .line 226409
    new-instance v1, LX/4dm;

    .line 226410
    iget-object v2, p0, LX/1H6;->q:LX/1FB;

    move-object v2, v2

    .line 226411
    invoke-direct {v1, v2}, LX/4dm;-><init>(LX/1FB;)V

    .line 226412
    iget-object v2, p0, LX/1H6;->w:LX/1HF;

    invoke-static {v0, v2, v1}, LX/1H6;->a(Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;LX/1HF;LX/4dm;)V

    .line 226413
    :cond_0
    :goto_f
    return-void

    .line 226414
    :cond_1
    iget-object v0, p1, LX/1H8;->c:LX/1Gd;

    goto/16 :goto_0

    .line 226415
    :cond_2
    iget-object v0, p1, LX/1H8;->b:Landroid/graphics/Bitmap$Config;

    goto/16 :goto_1

    .line 226416
    :cond_3
    iget-object v0, p1, LX/1H8;->d:LX/1Ao;

    goto/16 :goto_2

    .line 226417
    :cond_4
    iget-object v0, p1, LX/1H8;->u:LX/1HC;

    goto/16 :goto_3

    .line 226418
    :cond_5
    iget-object v0, p1, LX/1H8;->g:LX/1Gd;

    goto/16 :goto_4

    .line 226419
    :cond_6
    iget-object v0, p1, LX/1H8;->i:LX/1GF;

    goto/16 :goto_5

    .line 226420
    :cond_7
    iget-object v0, p1, LX/1H8;->k:LX/1Gd;

    goto/16 :goto_6

    .line 226421
    :cond_8
    iget-object v0, p1, LX/1H8;->l:LX/1Gf;

    goto/16 :goto_7

    .line 226422
    :cond_9
    iget-object v0, p1, LX/1H8;->m:LX/0rb;

    goto/16 :goto_8

    .line 226423
    :cond_a
    iget-object v0, p1, LX/1H8;->n:LX/1Gj;

    goto/16 :goto_9

    .line 226424
    :cond_b
    iget-object v0, p1, LX/1H8;->p:LX/1FB;

    goto :goto_a

    .line 226425
    :cond_c
    iget-object v0, p1, LX/1H8;->q:LX/1Gv;

    goto :goto_b

    .line 226426
    :cond_d
    iget-object v0, p1, LX/1H8;->r:Ljava/util/Set;

    goto :goto_c

    .line 226427
    :cond_e
    iget-object v0, p1, LX/1H8;->t:LX/1Gf;

    goto :goto_d

    .line 226428
    :cond_f
    iget-object v0, p1, LX/1H8;->h:LX/1Ft;

    goto :goto_e

    .line 226429
    :cond_10
    iget-object v0, p0, LX/1H6;->w:LX/1HF;

    .line 226430
    iget-boolean v1, v0, LX/1HF;->b:Z

    move v0, v1

    .line 226431
    if-eqz v0, :cond_0

    sget-boolean v0, LX/1cG;->a:Z

    if-eqz v0, :cond_0

    .line 226432
    sget-boolean v0, LX/1cG;->f:Z

    if-eqz v0, :cond_11

    .line 226433
    sget-object v0, LX/1cG;->d:Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;

    .line 226434
    :goto_10
    move-object v0, v0

    .line 226435
    if-eqz v0, :cond_0

    .line 226436
    new-instance v1, LX/4dm;

    .line 226437
    iget-object v2, p0, LX/1H6;->q:LX/1FB;

    move-object v2, v2

    .line 226438
    invoke-direct {v1, v2}, LX/4dm;-><init>(LX/1FB;)V

    .line 226439
    iget-object v2, p0, LX/1H6;->w:LX/1HF;

    invoke-static {v0, v2, v1}, LX/1H6;->a(Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;LX/1HF;LX/4dm;)V

    goto :goto_f

    .line 226440
    :cond_11
    const/4 v1, 0x0

    .line 226441
    :try_start_0
    const-string v0, "com.facebook.webpsupport.WebpBitmapFactoryImpl"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 226442
    :goto_11
    const/4 v1, 0x1

    sput-boolean v1, LX/1cG;->f:Z

    goto :goto_10

    :catch_0
    move-object v0, v1

    goto :goto_11
.end method

.method public static a(Landroid/content/Context;)LX/1H8;
    .locals 2

    .prologue
    .line 226376
    new-instance v0, LX/1H8;

    invoke-direct {v0, p0}, LX/1H8;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private static a(Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;LX/1HF;LX/4dm;)V
    .locals 1

    .prologue
    .line 226369
    sput-object p0, LX/1cG;->d:Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;

    .line 226370
    iget-object v0, p1, LX/1HF;->f:LX/4ec;

    move-object v0, v0

    .line 226371
    if-eqz v0, :cond_0

    .line 226372
    sput-object v0, Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;->b:LX/4ec;

    .line 226373
    :cond_0
    if-eqz p2, :cond_1

    .line 226374
    sput-object p2, Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;->c:LX/4dm;

    .line 226375
    :cond_1
    return-void
.end method


# virtual methods
.method public final c()LX/1Ao;
    .locals 1

    .prologue
    .line 226362
    iget-object v0, p0, LX/1H6;->d:LX/1Ao;

    return-object v0
.end method

.method public final d()Landroid/content/Context;
    .locals 1

    .prologue
    .line 226368
    iget-object v0, p0, LX/1H6;->e:Landroid/content/Context;

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 226444
    iget-boolean v0, p0, LX/1H6;->f:Z

    return v0
.end method

.method public final i()LX/1Ft;
    .locals 1

    .prologue
    .line 226367
    iget-object v0, p0, LX/1H6;->i:LX/1Ft;

    return-object v0
.end method

.method public final p()LX/1FB;
    .locals 1

    .prologue
    .line 226366
    iget-object v0, p0, LX/1H6;->q:LX/1FB;

    return-object v0
.end method

.method public final q()LX/1Gv;
    .locals 1

    .prologue
    .line 226365
    iget-object v0, p0, LX/1H6;->r:LX/1Gv;

    return-object v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 226364
    iget-boolean v0, p0, LX/1H6;->t:Z

    return v0
.end method

.method public final v()LX/1HF;
    .locals 1

    .prologue
    .line 226363
    iget-object v0, p0, LX/1H6;->w:LX/1HF;

    return-object v0
.end method
