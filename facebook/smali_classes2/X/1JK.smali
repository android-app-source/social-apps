.class public final LX/1JK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1JI;


# instance fields
.field public final synthetic a:LX/1JH;


# direct methods
.method public constructor <init>(LX/1JH;)V
    .locals 0

    .prologue
    .line 230042
    iput-object p1, p0, LX/1JK;->a:LX/1JH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/model/ClientFeedUnitEdge;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 230043
    invoke-virtual {p1}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A()I

    move-result v1

    if-ne v1, v0, :cond_1

    iget-object v1, p0, LX/1JK;->a:LX/1JH;

    iget-object v1, v1, LX/1JH;->d:LX/1JR;

    .line 230044
    iget-object v2, v1, LX/1JR;->d:Ljava/lang/Boolean;

    if-nez v2, :cond_0

    .line 230045
    iget-object v2, v1, LX/1JR;->i:LX/0W3;

    sget-wide v4, LX/0X5;->eD:J

    invoke-interface {v2, v4, v5}, LX/0W4;->a(J)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, LX/1JR;->d:Ljava/lang/Boolean;

    .line 230046
    :cond_0
    iget-object v2, v1, LX/1JR;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v1, v2

    .line 230047
    if-nez v1, :cond_3

    :cond_1
    invoke-virtual {p1}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A()I

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, LX/1JK;->a:LX/1JH;

    iget-object v1, v1, LX/1JH;->d:LX/1JR;

    .line 230048
    iget-object v2, v1, LX/1JR;->e:Ljava/lang/Boolean;

    if-nez v2, :cond_2

    .line 230049
    iget-object v2, v1, LX/1JR;->i:LX/0W3;

    sget-wide v4, LX/0X5;->eE:J

    invoke-interface {v2, v4, v5}, LX/0W4;->a(J)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, LX/1JR;->e:Ljava/lang/Boolean;

    .line 230050
    :cond_2
    iget-object v2, v1, LX/1JR;->e:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v1, v2

    .line 230051
    if-eqz v1, :cond_4

    :cond_3
    :goto_0
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method
