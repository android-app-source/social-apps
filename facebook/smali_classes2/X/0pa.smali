.class public LX/0pa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0pa;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 144946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0pa;
    .locals 3

    .prologue
    .line 144947
    sget-object v0, LX/0pa;->a:LX/0pa;

    if-nez v0, :cond_1

    .line 144948
    const-class v1, LX/0pa;

    monitor-enter v1

    .line 144949
    :try_start_0
    sget-object v0, LX/0pa;->a:LX/0pa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 144950
    if-eqz v2, :cond_0

    .line 144951
    :try_start_1
    new-instance v0, LX/0pa;

    invoke-direct {v0}, LX/0pa;-><init>()V

    .line 144952
    move-object v0, v0

    .line 144953
    sput-object v0, LX/0pa;->a:LX/0pa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144954
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 144955
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 144956
    :cond_1
    sget-object v0, LX/0pa;->a:LX/0pa;

    return-object v0

    .line 144957
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 144958
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
