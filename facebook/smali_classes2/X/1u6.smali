.class public LX/1u6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final A:LX/1sw;

.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;

.field private static final j:LX/1sw;

.field private static final k:LX/1sw;

.field private static final l:LX/1sw;

.field private static final m:LX/1sw;

.field private static final n:LX/1sw;

.field private static final o:LX/1sw;

.field private static final p:LX/1sw;

.field private static final q:LX/1sw;

.field private static final r:LX/1sw;

.field private static final s:LX/1sw;

.field private static final t:LX/1sw;

.field private static final u:LX/1sw;

.field private static final v:LX/1sw;

.field private static final w:LX/1sw;

.field private static final x:LX/1sw;

.field private static final y:LX/1sw;

.field private static final z:LX/1sw;


# instance fields
.field public final appId:Ljava/lang/Long;

.field public final clientCapabilities:Ljava/lang/Long;

.field public final clientIpAddress:Ljava/lang/String;

.field public final clientMqttSessionId:Ljava/lang/Long;

.field public final clientStack:Ljava/lang/Byte;

.field public final clientType:Ljava/lang/String;

.field public final connectTokenHash:[B

.field public final deviceId:Ljava/lang/String;

.field public final deviceSecret:Ljava/lang/String;

.field public final endpointCapabilities:Ljava/lang/Long;

.field public final fbnsConnectionKey:Ljava/lang/Long;

.field public final fbnsConnectionSecret:Ljava/lang/String;

.field public final fbnsDeviceId:Ljava/lang/String;

.field public final fbnsDeviceSecret:Ljava/lang/String;

.field public final isInitiallyForeground:Ljava/lang/Boolean;

.field public final makeUserAvailableInForeground:Ljava/lang/Boolean;

.field public final networkSubtype:Ljava/lang/Integer;

.field public final networkType:Ljava/lang/Integer;

.field public final noAutomaticForeground:Ljava/lang/Boolean;

.field public final overrideNectarLogging:Ljava/lang/Boolean;

.field public final publishFormat:Ljava/lang/Integer;

.field public final regionPreference:Ljava/lang/String;

.field public final subscribeTopics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final userAgent:Ljava/lang/String;

.field public final userId:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/16 v3, 0x8

    const/4 v6, 0x2

    const/16 v5, 0xa

    const/16 v4, 0xb

    .line 337741
    new-instance v0, LX/1sv;

    const-string v1, "ClientInfo"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/1u6;->b:LX/1sv;

    .line 337742
    new-instance v0, LX/1sw;

    const-string v1, "userId"

    invoke-direct {v0, v1, v5, v7}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->c:LX/1sw;

    .line 337743
    new-instance v0, LX/1sw;

    const-string v1, "userAgent"

    invoke-direct {v0, v1, v4, v6}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->d:LX/1sw;

    .line 337744
    new-instance v0, LX/1sw;

    const-string v1, "clientCapabilities"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->e:LX/1sw;

    .line 337745
    new-instance v0, LX/1sw;

    const-string v1, "endpointCapabilities"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->f:LX/1sw;

    .line 337746
    new-instance v0, LX/1sw;

    const-string v1, "publishFormat"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->g:LX/1sw;

    .line 337747
    new-instance v0, LX/1sw;

    const-string v1, "noAutomaticForeground"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->h:LX/1sw;

    .line 337748
    new-instance v0, LX/1sw;

    const-string v1, "makeUserAvailableInForeground"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->i:LX/1sw;

    .line 337749
    new-instance v0, LX/1sw;

    const-string v1, "deviceId"

    invoke-direct {v0, v1, v4, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->j:LX/1sw;

    .line 337750
    new-instance v0, LX/1sw;

    const-string v1, "isInitiallyForeground"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->k:LX/1sw;

    .line 337751
    new-instance v0, LX/1sw;

    const-string v1, "networkType"

    invoke-direct {v0, v1, v3, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->l:LX/1sw;

    .line 337752
    new-instance v0, LX/1sw;

    const-string v1, "networkSubtype"

    invoke-direct {v0, v1, v3, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->m:LX/1sw;

    .line 337753
    new-instance v0, LX/1sw;

    const-string v1, "clientMqttSessionId"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->n:LX/1sw;

    .line 337754
    new-instance v0, LX/1sw;

    const-string v1, "clientIpAddress"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->o:LX/1sw;

    .line 337755
    new-instance v0, LX/1sw;

    const-string v1, "subscribeTopics"

    const/16 v2, 0xf

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->p:LX/1sw;

    .line 337756
    new-instance v0, LX/1sw;

    const-string v1, "clientType"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->q:LX/1sw;

    .line 337757
    new-instance v0, LX/1sw;

    const-string v1, "appId"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->r:LX/1sw;

    .line 337758
    new-instance v0, LX/1sw;

    const-string v1, "overrideNectarLogging"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->s:LX/1sw;

    .line 337759
    new-instance v0, LX/1sw;

    const-string v1, "connectTokenHash"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->t:LX/1sw;

    .line 337760
    new-instance v0, LX/1sw;

    const-string v1, "regionPreference"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->u:LX/1sw;

    .line 337761
    new-instance v0, LX/1sw;

    const-string v1, "deviceSecret"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->v:LX/1sw;

    .line 337762
    new-instance v0, LX/1sw;

    const-string v1, "clientStack"

    const/4 v2, 0x3

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->w:LX/1sw;

    .line 337763
    new-instance v0, LX/1sw;

    const-string v1, "fbnsConnectionKey"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->x:LX/1sw;

    .line 337764
    new-instance v0, LX/1sw;

    const-string v1, "fbnsConnectionSecret"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->y:LX/1sw;

    .line 337765
    new-instance v0, LX/1sw;

    const-string v1, "fbnsDeviceId"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->z:LX/1sw;

    .line 337766
    new-instance v0, LX/1sw;

    const-string v1, "fbnsDeviceSecret"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u6;->A:LX/1sw;

    .line 337767
    sput-boolean v7, LX/1u6;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Boolean;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/Byte;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            "[B",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Byte;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 337714
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 337715
    iput-object p1, p0, LX/1u6;->userId:Ljava/lang/Long;

    .line 337716
    iput-object p2, p0, LX/1u6;->userAgent:Ljava/lang/String;

    .line 337717
    iput-object p3, p0, LX/1u6;->clientCapabilities:Ljava/lang/Long;

    .line 337718
    iput-object p4, p0, LX/1u6;->endpointCapabilities:Ljava/lang/Long;

    .line 337719
    iput-object p5, p0, LX/1u6;->publishFormat:Ljava/lang/Integer;

    .line 337720
    iput-object p6, p0, LX/1u6;->noAutomaticForeground:Ljava/lang/Boolean;

    .line 337721
    iput-object p7, p0, LX/1u6;->makeUserAvailableInForeground:Ljava/lang/Boolean;

    .line 337722
    iput-object p8, p0, LX/1u6;->deviceId:Ljava/lang/String;

    .line 337723
    iput-object p9, p0, LX/1u6;->isInitiallyForeground:Ljava/lang/Boolean;

    .line 337724
    iput-object p10, p0, LX/1u6;->networkType:Ljava/lang/Integer;

    .line 337725
    iput-object p11, p0, LX/1u6;->networkSubtype:Ljava/lang/Integer;

    .line 337726
    iput-object p12, p0, LX/1u6;->clientMqttSessionId:Ljava/lang/Long;

    .line 337727
    iput-object p13, p0, LX/1u6;->clientIpAddress:Ljava/lang/String;

    .line 337728
    iput-object p14, p0, LX/1u6;->subscribeTopics:Ljava/util/List;

    .line 337729
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1u6;->clientType:Ljava/lang/String;

    .line 337730
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1u6;->appId:Ljava/lang/Long;

    .line 337731
    move-object/from16 v0, p17

    iput-object v0, p0, LX/1u6;->overrideNectarLogging:Ljava/lang/Boolean;

    .line 337732
    move-object/from16 v0, p18

    iput-object v0, p0, LX/1u6;->connectTokenHash:[B

    .line 337733
    move-object/from16 v0, p19

    iput-object v0, p0, LX/1u6;->regionPreference:Ljava/lang/String;

    .line 337734
    move-object/from16 v0, p20

    iput-object v0, p0, LX/1u6;->deviceSecret:Ljava/lang/String;

    .line 337735
    move-object/from16 v0, p21

    iput-object v0, p0, LX/1u6;->clientStack:Ljava/lang/Byte;

    .line 337736
    move-object/from16 v0, p22

    iput-object v0, p0, LX/1u6;->fbnsConnectionKey:Ljava/lang/Long;

    .line 337737
    move-object/from16 v0, p23

    iput-object v0, p0, LX/1u6;->fbnsConnectionSecret:Ljava/lang/String;

    .line 337738
    move-object/from16 v0, p24

    iput-object v0, p0, LX/1u6;->fbnsDeviceId:Ljava/lang/String;

    .line 337739
    move-object/from16 v0, p25

    iput-object v0, p0, LX/1u6;->fbnsDeviceSecret:Ljava/lang/String;

    .line 337740
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 337711
    iget-object v0, p0, LX/1u6;->publishFormat:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    sget-object v0, LX/1tR;->a:LX/1sn;

    iget-object v1, p0, LX/1u6;->publishFormat:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 337712
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'publishFormat\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/1u6;->publishFormat:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 337713
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v8, 0x80

    .line 337277
    if-eqz p2, :cond_18

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 337278
    :goto_0
    if-eqz p2, :cond_19

    const-string v0, "\n"

    move-object v3, v0

    .line 337279
    :goto_1
    if-eqz p2, :cond_1a

    const-string v0, " "

    move-object v1, v0

    .line 337280
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, "ClientInfo"

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 337281
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337282
    const-string v0, "("

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337283
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337284
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337285
    const-string v0, "userId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337286
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337287
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337288
    iget-object v0, p0, LX/1u6;->userId:Ljava/lang/Long;

    if-nez v0, :cond_1b

    .line 337289
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337290
    :goto_3
    iget-object v0, p0, LX/1u6;->userAgent:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 337291
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337292
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337293
    const-string v0, "userAgent"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337294
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337295
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337296
    iget-object v0, p0, LX/1u6;->userAgent:Ljava/lang/String;

    if-nez v0, :cond_1c

    .line 337297
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337298
    :cond_0
    :goto_4
    iget-object v0, p0, LX/1u6;->clientCapabilities:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 337299
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337300
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337301
    const-string v0, "clientCapabilities"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337302
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337303
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337304
    iget-object v0, p0, LX/1u6;->clientCapabilities:Ljava/lang/Long;

    if-nez v0, :cond_1d

    .line 337305
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337306
    :cond_1
    :goto_5
    iget-object v0, p0, LX/1u6;->endpointCapabilities:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 337307
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337308
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337309
    const-string v0, "endpointCapabilities"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337310
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337311
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337312
    iget-object v0, p0, LX/1u6;->endpointCapabilities:Ljava/lang/Long;

    if-nez v0, :cond_1e

    .line 337313
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337314
    :cond_2
    :goto_6
    iget-object v0, p0, LX/1u6;->publishFormat:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 337315
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337316
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337317
    const-string v0, "publishFormat"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337318
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337319
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337320
    iget-object v0, p0, LX/1u6;->publishFormat:Ljava/lang/Integer;

    if-nez v0, :cond_1f

    .line 337321
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337322
    :cond_3
    :goto_7
    iget-object v0, p0, LX/1u6;->noAutomaticForeground:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 337323
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337324
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337325
    const-string v0, "noAutomaticForeground"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337326
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337327
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337328
    iget-object v0, p0, LX/1u6;->noAutomaticForeground:Ljava/lang/Boolean;

    if-nez v0, :cond_21

    .line 337329
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337330
    :cond_4
    :goto_8
    iget-object v0, p0, LX/1u6;->makeUserAvailableInForeground:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 337331
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337332
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337333
    const-string v0, "makeUserAvailableInForeground"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337334
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337335
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337336
    iget-object v0, p0, LX/1u6;->makeUserAvailableInForeground:Ljava/lang/Boolean;

    if-nez v0, :cond_22

    .line 337337
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337338
    :cond_5
    :goto_9
    iget-object v0, p0, LX/1u6;->deviceId:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 337339
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337340
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337341
    const-string v0, "deviceId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337342
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337343
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337344
    iget-object v0, p0, LX/1u6;->deviceId:Ljava/lang/String;

    if-nez v0, :cond_23

    .line 337345
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337346
    :cond_6
    :goto_a
    iget-object v0, p0, LX/1u6;->isInitiallyForeground:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 337347
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337348
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337349
    const-string v0, "isInitiallyForeground"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337350
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337351
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337352
    iget-object v0, p0, LX/1u6;->isInitiallyForeground:Ljava/lang/Boolean;

    if-nez v0, :cond_24

    .line 337353
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337354
    :cond_7
    :goto_b
    iget-object v0, p0, LX/1u6;->networkType:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 337355
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337356
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337357
    const-string v0, "networkType"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337358
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337359
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337360
    iget-object v0, p0, LX/1u6;->networkType:Ljava/lang/Integer;

    if-nez v0, :cond_25

    .line 337361
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337362
    :cond_8
    :goto_c
    iget-object v0, p0, LX/1u6;->networkSubtype:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 337363
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337364
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337365
    const-string v0, "networkSubtype"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337366
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337367
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337368
    iget-object v0, p0, LX/1u6;->networkSubtype:Ljava/lang/Integer;

    if-nez v0, :cond_26

    .line 337369
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337370
    :cond_9
    :goto_d
    iget-object v0, p0, LX/1u6;->clientMqttSessionId:Ljava/lang/Long;

    if-eqz v0, :cond_a

    .line 337371
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337372
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337373
    const-string v0, "clientMqttSessionId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337374
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337375
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337376
    iget-object v0, p0, LX/1u6;->clientMqttSessionId:Ljava/lang/Long;

    if-nez v0, :cond_27

    .line 337377
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337378
    :cond_a
    :goto_e
    iget-object v0, p0, LX/1u6;->clientIpAddress:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 337379
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337380
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337381
    const-string v0, "clientIpAddress"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337382
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337383
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337384
    iget-object v0, p0, LX/1u6;->clientIpAddress:Ljava/lang/String;

    if-nez v0, :cond_28

    .line 337385
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337386
    :cond_b
    :goto_f
    iget-object v0, p0, LX/1u6;->subscribeTopics:Ljava/util/List;

    if-eqz v0, :cond_c

    .line 337387
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337388
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337389
    const-string v0, "subscribeTopics"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337390
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337391
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337392
    iget-object v0, p0, LX/1u6;->subscribeTopics:Ljava/util/List;

    if-nez v0, :cond_29

    .line 337393
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337394
    :cond_c
    :goto_10
    iget-object v0, p0, LX/1u6;->clientType:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 337395
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337396
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337397
    const-string v0, "clientType"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337398
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337399
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337400
    iget-object v0, p0, LX/1u6;->clientType:Ljava/lang/String;

    if-nez v0, :cond_2a

    .line 337401
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337402
    :cond_d
    :goto_11
    iget-object v0, p0, LX/1u6;->appId:Ljava/lang/Long;

    if-eqz v0, :cond_e

    .line 337403
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337404
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337405
    const-string v0, "appId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337406
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337407
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337408
    iget-object v0, p0, LX/1u6;->appId:Ljava/lang/Long;

    if-nez v0, :cond_2b

    .line 337409
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337410
    :cond_e
    :goto_12
    iget-object v0, p0, LX/1u6;->overrideNectarLogging:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    .line 337411
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337412
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337413
    const-string v0, "overrideNectarLogging"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337414
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337415
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337416
    iget-object v0, p0, LX/1u6;->overrideNectarLogging:Ljava/lang/Boolean;

    if-nez v0, :cond_2c

    .line 337417
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337418
    :cond_f
    :goto_13
    iget-object v0, p0, LX/1u6;->connectTokenHash:[B

    if-eqz v0, :cond_10

    .line 337419
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337420
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337421
    const-string v0, "connectTokenHash"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337422
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337423
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337424
    iget-object v0, p0, LX/1u6;->connectTokenHash:[B

    if-nez v0, :cond_2d

    .line 337425
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337426
    :cond_10
    :goto_14
    iget-object v0, p0, LX/1u6;->regionPreference:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 337427
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337428
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337429
    const-string v0, "regionPreference"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337430
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337431
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337432
    iget-object v0, p0, LX/1u6;->regionPreference:Ljava/lang/String;

    if-nez v0, :cond_31

    .line 337433
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337434
    :cond_11
    :goto_15
    iget-object v0, p0, LX/1u6;->deviceSecret:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 337435
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337436
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337437
    const-string v0, "deviceSecret"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337438
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337439
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337440
    iget-object v0, p0, LX/1u6;->deviceSecret:Ljava/lang/String;

    if-nez v0, :cond_32

    .line 337441
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337442
    :cond_12
    :goto_16
    iget-object v0, p0, LX/1u6;->clientStack:Ljava/lang/Byte;

    if-eqz v0, :cond_13

    .line 337443
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337444
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337445
    const-string v0, "clientStack"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337446
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337447
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337448
    iget-object v0, p0, LX/1u6;->clientStack:Ljava/lang/Byte;

    if-nez v0, :cond_33

    .line 337449
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337450
    :cond_13
    :goto_17
    iget-object v0, p0, LX/1u6;->fbnsConnectionKey:Ljava/lang/Long;

    if-eqz v0, :cond_14

    .line 337451
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337452
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337453
    const-string v0, "fbnsConnectionKey"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337454
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337455
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337456
    iget-object v0, p0, LX/1u6;->fbnsConnectionKey:Ljava/lang/Long;

    if-nez v0, :cond_34

    .line 337457
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337458
    :cond_14
    :goto_18
    iget-object v0, p0, LX/1u6;->fbnsConnectionSecret:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 337459
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337460
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337461
    const-string v0, "fbnsConnectionSecret"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337462
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337463
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337464
    iget-object v0, p0, LX/1u6;->fbnsConnectionSecret:Ljava/lang/String;

    if-nez v0, :cond_35

    .line 337465
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337466
    :cond_15
    :goto_19
    iget-object v0, p0, LX/1u6;->fbnsDeviceId:Ljava/lang/String;

    if-eqz v0, :cond_16

    .line 337467
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337468
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337469
    const-string v0, "fbnsDeviceId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337470
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337471
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337472
    iget-object v0, p0, LX/1u6;->fbnsDeviceId:Ljava/lang/String;

    if-nez v0, :cond_36

    .line 337473
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337474
    :cond_16
    :goto_1a
    iget-object v0, p0, LX/1u6;->fbnsDeviceSecret:Ljava/lang/String;

    if-eqz v0, :cond_17

    .line 337475
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337476
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337477
    const-string v0, "fbnsDeviceSecret"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337478
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337479
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337480
    iget-object v0, p0, LX/1u6;->fbnsDeviceSecret:Ljava/lang/String;

    if-nez v0, :cond_37

    .line 337481
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337482
    :cond_17
    :goto_1b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337483
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337484
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 337485
    :cond_18
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 337486
    :cond_19
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 337487
    :cond_1a
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 337488
    :cond_1b
    iget-object v0, p0, LX/1u6;->userId:Ljava/lang/Long;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 337489
    :cond_1c
    iget-object v0, p0, LX/1u6;->userAgent:Ljava/lang/String;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 337490
    :cond_1d
    iget-object v0, p0, LX/1u6;->clientCapabilities:Ljava/lang/Long;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 337491
    :cond_1e
    iget-object v0, p0, LX/1u6;->endpointCapabilities:Ljava/lang/Long;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 337492
    :cond_1f
    sget-object v0, LX/1tR;->b:Ljava/util/Map;

    iget-object v2, p0, LX/1u6;->publishFormat:Ljava/lang/Integer;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 337493
    if-eqz v0, :cond_20

    .line 337494
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337495
    const-string v2, " ("

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337496
    :cond_20
    iget-object v2, p0, LX/1u6;->publishFormat:Ljava/lang/Integer;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 337497
    if-eqz v0, :cond_3

    .line 337498
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 337499
    :cond_21
    iget-object v0, p0, LX/1u6;->noAutomaticForeground:Ljava/lang/Boolean;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 337500
    :cond_22
    iget-object v0, p0, LX/1u6;->makeUserAvailableInForeground:Ljava/lang/Boolean;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 337501
    :cond_23
    iget-object v0, p0, LX/1u6;->deviceId:Ljava/lang/String;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 337502
    :cond_24
    iget-object v0, p0, LX/1u6;->isInitiallyForeground:Ljava/lang/Boolean;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b

    .line 337503
    :cond_25
    iget-object v0, p0, LX/1u6;->networkType:Ljava/lang/Integer;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c

    .line 337504
    :cond_26
    iget-object v0, p0, LX/1u6;->networkSubtype:Ljava/lang/Integer;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_d

    .line 337505
    :cond_27
    iget-object v0, p0, LX/1u6;->clientMqttSessionId:Ljava/lang/Long;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_e

    .line 337506
    :cond_28
    iget-object v0, p0, LX/1u6;->clientIpAddress:Ljava/lang/String;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_f

    .line 337507
    :cond_29
    iget-object v0, p0, LX/1u6;->subscribeTopics:Ljava/util/List;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_10

    .line 337508
    :cond_2a
    iget-object v0, p0, LX/1u6;->clientType:Ljava/lang/String;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_11

    .line 337509
    :cond_2b
    iget-object v0, p0, LX/1u6;->appId:Ljava/lang/Long;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_12

    .line 337510
    :cond_2c
    iget-object v0, p0, LX/1u6;->overrideNectarLogging:Ljava/lang/Boolean;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_13

    .line 337511
    :cond_2d
    iget-object v0, p0, LX/1u6;->connectTokenHash:[B

    array-length v0, v0

    invoke-static {v0, v8}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 337512
    const/4 v0, 0x0

    move v2, v0

    :goto_1c
    if-ge v2, v6, :cond_30

    .line 337513
    if-eqz v2, :cond_2e

    const-string v0, " "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337514
    :cond_2e
    iget-object v0, p0, LX/1u6;->connectTokenHash:[B

    aget-byte v0, v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v7, 0x1

    if-le v0, v7, :cond_2f

    iget-object v0, p0, LX/1u6;->connectTokenHash:[B

    aget-byte v0, v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v7, p0, LX/1u6;->connectTokenHash:[B

    aget-byte v7, v7, v2

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    :goto_1d
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337515
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1c

    .line 337516
    :cond_2f
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "0"

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, LX/1u6;->connectTokenHash:[B

    aget-byte v7, v7, v2

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1d

    .line 337517
    :cond_30
    iget-object v0, p0, LX/1u6;->connectTokenHash:[B

    array-length v0, v0

    if-le v0, v8, :cond_10

    const-string v0, " ..."

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_14

    .line 337518
    :cond_31
    iget-object v0, p0, LX/1u6;->regionPreference:Ljava/lang/String;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_15

    .line 337519
    :cond_32
    iget-object v0, p0, LX/1u6;->deviceSecret:Ljava/lang/String;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_16

    .line 337520
    :cond_33
    iget-object v0, p0, LX/1u6;->clientStack:Ljava/lang/Byte;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_17

    .line 337521
    :cond_34
    iget-object v0, p0, LX/1u6;->fbnsConnectionKey:Ljava/lang/Long;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_18

    .line 337522
    :cond_35
    iget-object v0, p0, LX/1u6;->fbnsConnectionSecret:Ljava/lang/String;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_19

    .line 337523
    :cond_36
    iget-object v0, p0, LX/1u6;->fbnsDeviceId:Ljava/lang/String;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v0, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1a

    .line 337524
    :cond_37
    iget-object v0, p0, LX/1u6;->fbnsDeviceSecret:Ljava/lang/String;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1b
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 337768
    invoke-direct {p0}, LX/1u6;->a()V

    .line 337769
    invoke-virtual {p1}, LX/1su;->a()V

    .line 337770
    iget-object v0, p0, LX/1u6;->userId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 337771
    sget-object v0, LX/1u6;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337772
    iget-object v0, p0, LX/1u6;->userId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 337773
    :cond_0
    iget-object v0, p0, LX/1u6;->userAgent:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 337774
    iget-object v0, p0, LX/1u6;->userAgent:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 337775
    sget-object v0, LX/1u6;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337776
    iget-object v0, p0, LX/1u6;->userAgent:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 337777
    :cond_1
    iget-object v0, p0, LX/1u6;->clientCapabilities:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 337778
    iget-object v0, p0, LX/1u6;->clientCapabilities:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 337779
    sget-object v0, LX/1u6;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337780
    iget-object v0, p0, LX/1u6;->clientCapabilities:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 337781
    :cond_2
    iget-object v0, p0, LX/1u6;->endpointCapabilities:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 337782
    iget-object v0, p0, LX/1u6;->endpointCapabilities:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 337783
    sget-object v0, LX/1u6;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337784
    iget-object v0, p0, LX/1u6;->endpointCapabilities:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 337785
    :cond_3
    iget-object v0, p0, LX/1u6;->publishFormat:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 337786
    iget-object v0, p0, LX/1u6;->publishFormat:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 337787
    sget-object v0, LX/1u6;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337788
    iget-object v0, p0, LX/1u6;->publishFormat:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 337789
    :cond_4
    iget-object v0, p0, LX/1u6;->noAutomaticForeground:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 337790
    iget-object v0, p0, LX/1u6;->noAutomaticForeground:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 337791
    sget-object v0, LX/1u6;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337792
    iget-object v0, p0, LX/1u6;->noAutomaticForeground:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 337793
    :cond_5
    iget-object v0, p0, LX/1u6;->makeUserAvailableInForeground:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 337794
    iget-object v0, p0, LX/1u6;->makeUserAvailableInForeground:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 337795
    sget-object v0, LX/1u6;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337796
    iget-object v0, p0, LX/1u6;->makeUserAvailableInForeground:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 337797
    :cond_6
    iget-object v0, p0, LX/1u6;->deviceId:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 337798
    iget-object v0, p0, LX/1u6;->deviceId:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 337799
    sget-object v0, LX/1u6;->j:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337800
    iget-object v0, p0, LX/1u6;->deviceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 337801
    :cond_7
    iget-object v0, p0, LX/1u6;->isInitiallyForeground:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 337802
    iget-object v0, p0, LX/1u6;->isInitiallyForeground:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 337803
    sget-object v0, LX/1u6;->k:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337804
    iget-object v0, p0, LX/1u6;->isInitiallyForeground:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 337805
    :cond_8
    iget-object v0, p0, LX/1u6;->networkType:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 337806
    iget-object v0, p0, LX/1u6;->networkType:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 337807
    sget-object v0, LX/1u6;->l:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337808
    iget-object v0, p0, LX/1u6;->networkType:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 337809
    :cond_9
    iget-object v0, p0, LX/1u6;->networkSubtype:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 337810
    iget-object v0, p0, LX/1u6;->networkSubtype:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 337811
    sget-object v0, LX/1u6;->m:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337812
    iget-object v0, p0, LX/1u6;->networkSubtype:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 337813
    :cond_a
    iget-object v0, p0, LX/1u6;->clientMqttSessionId:Ljava/lang/Long;

    if-eqz v0, :cond_b

    .line 337814
    iget-object v0, p0, LX/1u6;->clientMqttSessionId:Ljava/lang/Long;

    if-eqz v0, :cond_b

    .line 337815
    sget-object v0, LX/1u6;->n:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337816
    iget-object v0, p0, LX/1u6;->clientMqttSessionId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 337817
    :cond_b
    iget-object v0, p0, LX/1u6;->clientIpAddress:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 337818
    iget-object v0, p0, LX/1u6;->clientIpAddress:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 337819
    sget-object v0, LX/1u6;->o:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337820
    iget-object v0, p0, LX/1u6;->clientIpAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 337821
    :cond_c
    iget-object v0, p0, LX/1u6;->subscribeTopics:Ljava/util/List;

    if-eqz v0, :cond_d

    .line 337822
    iget-object v0, p0, LX/1u6;->subscribeTopics:Ljava/util/List;

    if-eqz v0, :cond_d

    .line 337823
    sget-object v0, LX/1u6;->p:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337824
    new-instance v0, LX/1u3;

    const/16 v1, 0x8

    iget-object v2, p0, LX/1u6;->subscribeTopics:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 337825
    iget-object v0, p0, LX/1u6;->subscribeTopics:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 337826
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    goto :goto_0

    .line 337827
    :cond_d
    iget-object v0, p0, LX/1u6;->clientType:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 337828
    iget-object v0, p0, LX/1u6;->clientType:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 337829
    sget-object v0, LX/1u6;->q:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337830
    iget-object v0, p0, LX/1u6;->clientType:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 337831
    :cond_e
    iget-object v0, p0, LX/1u6;->appId:Ljava/lang/Long;

    if-eqz v0, :cond_f

    .line 337832
    iget-object v0, p0, LX/1u6;->appId:Ljava/lang/Long;

    if-eqz v0, :cond_f

    .line 337833
    sget-object v0, LX/1u6;->r:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337834
    iget-object v0, p0, LX/1u6;->appId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 337835
    :cond_f
    iget-object v0, p0, LX/1u6;->overrideNectarLogging:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    .line 337836
    iget-object v0, p0, LX/1u6;->overrideNectarLogging:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    .line 337837
    sget-object v0, LX/1u6;->s:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337838
    iget-object v0, p0, LX/1u6;->overrideNectarLogging:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 337839
    :cond_10
    iget-object v0, p0, LX/1u6;->connectTokenHash:[B

    if-eqz v0, :cond_11

    .line 337840
    iget-object v0, p0, LX/1u6;->connectTokenHash:[B

    if-eqz v0, :cond_11

    .line 337841
    sget-object v0, LX/1u6;->t:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337842
    iget-object v0, p0, LX/1u6;->connectTokenHash:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 337843
    :cond_11
    iget-object v0, p0, LX/1u6;->regionPreference:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 337844
    iget-object v0, p0, LX/1u6;->regionPreference:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 337845
    sget-object v0, LX/1u6;->u:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337846
    iget-object v0, p0, LX/1u6;->regionPreference:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 337847
    :cond_12
    iget-object v0, p0, LX/1u6;->deviceSecret:Ljava/lang/String;

    if-eqz v0, :cond_13

    .line 337848
    iget-object v0, p0, LX/1u6;->deviceSecret:Ljava/lang/String;

    if-eqz v0, :cond_13

    .line 337849
    sget-object v0, LX/1u6;->v:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337850
    iget-object v0, p0, LX/1u6;->deviceSecret:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 337851
    :cond_13
    iget-object v0, p0, LX/1u6;->clientStack:Ljava/lang/Byte;

    if-eqz v0, :cond_14

    .line 337852
    iget-object v0, p0, LX/1u6;->clientStack:Ljava/lang/Byte;

    if-eqz v0, :cond_14

    .line 337853
    sget-object v0, LX/1u6;->w:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337854
    iget-object v0, p0, LX/1u6;->clientStack:Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(B)V

    .line 337855
    :cond_14
    iget-object v0, p0, LX/1u6;->fbnsConnectionKey:Ljava/lang/Long;

    if-eqz v0, :cond_15

    .line 337856
    iget-object v0, p0, LX/1u6;->fbnsConnectionKey:Ljava/lang/Long;

    if-eqz v0, :cond_15

    .line 337857
    sget-object v0, LX/1u6;->x:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337858
    iget-object v0, p0, LX/1u6;->fbnsConnectionKey:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 337859
    :cond_15
    iget-object v0, p0, LX/1u6;->fbnsConnectionSecret:Ljava/lang/String;

    if-eqz v0, :cond_16

    .line 337860
    iget-object v0, p0, LX/1u6;->fbnsConnectionSecret:Ljava/lang/String;

    if-eqz v0, :cond_16

    .line 337861
    sget-object v0, LX/1u6;->y:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337862
    iget-object v0, p0, LX/1u6;->fbnsConnectionSecret:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 337863
    :cond_16
    iget-object v0, p0, LX/1u6;->fbnsDeviceId:Ljava/lang/String;

    if-eqz v0, :cond_17

    .line 337864
    iget-object v0, p0, LX/1u6;->fbnsDeviceId:Ljava/lang/String;

    if-eqz v0, :cond_17

    .line 337865
    sget-object v0, LX/1u6;->z:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337866
    iget-object v0, p0, LX/1u6;->fbnsDeviceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 337867
    :cond_17
    iget-object v0, p0, LX/1u6;->fbnsDeviceSecret:Ljava/lang/String;

    if-eqz v0, :cond_18

    .line 337868
    iget-object v0, p0, LX/1u6;->fbnsDeviceSecret:Ljava/lang/String;

    if-eqz v0, :cond_18

    .line 337869
    sget-object v0, LX/1u6;->A:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337870
    iget-object v0, p0, LX/1u6;->fbnsDeviceSecret:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 337871
    :cond_18
    invoke-virtual {p1}, LX/1su;->c()V

    .line 337872
    invoke-virtual {p1}, LX/1su;->b()V

    .line 337873
    return-void
.end method

.method public final a(LX/1u6;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 337533
    if-nez p1, :cond_1

    .line 337534
    :cond_0
    :goto_0
    return v2

    .line 337535
    :cond_1
    iget-object v0, p0, LX/1u6;->userId:Ljava/lang/Long;

    if-eqz v0, :cond_34

    move v0, v1

    .line 337536
    :goto_1
    iget-object v3, p1, LX/1u6;->userId:Ljava/lang/Long;

    if-eqz v3, :cond_35

    move v3, v1

    .line 337537
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 337538
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337539
    iget-object v0, p0, LX/1u6;->userId:Ljava/lang/Long;

    iget-object v3, p1, LX/1u6;->userId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337540
    :cond_3
    iget-object v0, p0, LX/1u6;->userAgent:Ljava/lang/String;

    if-eqz v0, :cond_36

    move v0, v1

    .line 337541
    :goto_3
    iget-object v3, p1, LX/1u6;->userAgent:Ljava/lang/String;

    if-eqz v3, :cond_37

    move v3, v1

    .line 337542
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 337543
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337544
    iget-object v0, p0, LX/1u6;->userAgent:Ljava/lang/String;

    iget-object v3, p1, LX/1u6;->userAgent:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337545
    :cond_5
    iget-object v0, p0, LX/1u6;->clientCapabilities:Ljava/lang/Long;

    if-eqz v0, :cond_38

    move v0, v1

    .line 337546
    :goto_5
    iget-object v3, p1, LX/1u6;->clientCapabilities:Ljava/lang/Long;

    if-eqz v3, :cond_39

    move v3, v1

    .line 337547
    :goto_6
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 337548
    :cond_6
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337549
    iget-object v0, p0, LX/1u6;->clientCapabilities:Ljava/lang/Long;

    iget-object v3, p1, LX/1u6;->clientCapabilities:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337550
    :cond_7
    iget-object v0, p0, LX/1u6;->endpointCapabilities:Ljava/lang/Long;

    if-eqz v0, :cond_3a

    move v0, v1

    .line 337551
    :goto_7
    iget-object v3, p1, LX/1u6;->endpointCapabilities:Ljava/lang/Long;

    if-eqz v3, :cond_3b

    move v3, v1

    .line 337552
    :goto_8
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 337553
    :cond_8
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337554
    iget-object v0, p0, LX/1u6;->endpointCapabilities:Ljava/lang/Long;

    iget-object v3, p1, LX/1u6;->endpointCapabilities:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337555
    :cond_9
    iget-object v0, p0, LX/1u6;->publishFormat:Ljava/lang/Integer;

    if-eqz v0, :cond_3c

    move v0, v1

    .line 337556
    :goto_9
    iget-object v3, p1, LX/1u6;->publishFormat:Ljava/lang/Integer;

    if-eqz v3, :cond_3d

    move v3, v1

    .line 337557
    :goto_a
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 337558
    :cond_a
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337559
    iget-object v0, p0, LX/1u6;->publishFormat:Ljava/lang/Integer;

    iget-object v3, p1, LX/1u6;->publishFormat:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337560
    :cond_b
    iget-object v0, p0, LX/1u6;->noAutomaticForeground:Ljava/lang/Boolean;

    if-eqz v0, :cond_3e

    move v0, v1

    .line 337561
    :goto_b
    iget-object v3, p1, LX/1u6;->noAutomaticForeground:Ljava/lang/Boolean;

    if-eqz v3, :cond_3f

    move v3, v1

    .line 337562
    :goto_c
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 337563
    :cond_c
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337564
    iget-object v0, p0, LX/1u6;->noAutomaticForeground:Ljava/lang/Boolean;

    iget-object v3, p1, LX/1u6;->noAutomaticForeground:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337565
    :cond_d
    iget-object v0, p0, LX/1u6;->makeUserAvailableInForeground:Ljava/lang/Boolean;

    if-eqz v0, :cond_40

    move v0, v1

    .line 337566
    :goto_d
    iget-object v3, p1, LX/1u6;->makeUserAvailableInForeground:Ljava/lang/Boolean;

    if-eqz v3, :cond_41

    move v3, v1

    .line 337567
    :goto_e
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 337568
    :cond_e
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337569
    iget-object v0, p0, LX/1u6;->makeUserAvailableInForeground:Ljava/lang/Boolean;

    iget-object v3, p1, LX/1u6;->makeUserAvailableInForeground:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337570
    :cond_f
    iget-object v0, p0, LX/1u6;->deviceId:Ljava/lang/String;

    if-eqz v0, :cond_42

    move v0, v1

    .line 337571
    :goto_f
    iget-object v3, p1, LX/1u6;->deviceId:Ljava/lang/String;

    if-eqz v3, :cond_43

    move v3, v1

    .line 337572
    :goto_10
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 337573
    :cond_10
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337574
    iget-object v0, p0, LX/1u6;->deviceId:Ljava/lang/String;

    iget-object v3, p1, LX/1u6;->deviceId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337575
    :cond_11
    iget-object v0, p0, LX/1u6;->isInitiallyForeground:Ljava/lang/Boolean;

    if-eqz v0, :cond_44

    move v0, v1

    .line 337576
    :goto_11
    iget-object v3, p1, LX/1u6;->isInitiallyForeground:Ljava/lang/Boolean;

    if-eqz v3, :cond_45

    move v3, v1

    .line 337577
    :goto_12
    if-nez v0, :cond_12

    if-eqz v3, :cond_13

    .line 337578
    :cond_12
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337579
    iget-object v0, p0, LX/1u6;->isInitiallyForeground:Ljava/lang/Boolean;

    iget-object v3, p1, LX/1u6;->isInitiallyForeground:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337580
    :cond_13
    iget-object v0, p0, LX/1u6;->networkType:Ljava/lang/Integer;

    if-eqz v0, :cond_46

    move v0, v1

    .line 337581
    :goto_13
    iget-object v3, p1, LX/1u6;->networkType:Ljava/lang/Integer;

    if-eqz v3, :cond_47

    move v3, v1

    .line 337582
    :goto_14
    if-nez v0, :cond_14

    if-eqz v3, :cond_15

    .line 337583
    :cond_14
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337584
    iget-object v0, p0, LX/1u6;->networkType:Ljava/lang/Integer;

    iget-object v3, p1, LX/1u6;->networkType:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337585
    :cond_15
    iget-object v0, p0, LX/1u6;->networkSubtype:Ljava/lang/Integer;

    if-eqz v0, :cond_48

    move v0, v1

    .line 337586
    :goto_15
    iget-object v3, p1, LX/1u6;->networkSubtype:Ljava/lang/Integer;

    if-eqz v3, :cond_49

    move v3, v1

    .line 337587
    :goto_16
    if-nez v0, :cond_16

    if-eqz v3, :cond_17

    .line 337588
    :cond_16
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337589
    iget-object v0, p0, LX/1u6;->networkSubtype:Ljava/lang/Integer;

    iget-object v3, p1, LX/1u6;->networkSubtype:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337590
    :cond_17
    iget-object v0, p0, LX/1u6;->clientMqttSessionId:Ljava/lang/Long;

    if-eqz v0, :cond_4a

    move v0, v1

    .line 337591
    :goto_17
    iget-object v3, p1, LX/1u6;->clientMqttSessionId:Ljava/lang/Long;

    if-eqz v3, :cond_4b

    move v3, v1

    .line 337592
    :goto_18
    if-nez v0, :cond_18

    if-eqz v3, :cond_19

    .line 337593
    :cond_18
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337594
    iget-object v0, p0, LX/1u6;->clientMqttSessionId:Ljava/lang/Long;

    iget-object v3, p1, LX/1u6;->clientMqttSessionId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337595
    :cond_19
    iget-object v0, p0, LX/1u6;->clientIpAddress:Ljava/lang/String;

    if-eqz v0, :cond_4c

    move v0, v1

    .line 337596
    :goto_19
    iget-object v3, p1, LX/1u6;->clientIpAddress:Ljava/lang/String;

    if-eqz v3, :cond_4d

    move v3, v1

    .line 337597
    :goto_1a
    if-nez v0, :cond_1a

    if-eqz v3, :cond_1b

    .line 337598
    :cond_1a
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337599
    iget-object v0, p0, LX/1u6;->clientIpAddress:Ljava/lang/String;

    iget-object v3, p1, LX/1u6;->clientIpAddress:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337600
    :cond_1b
    iget-object v0, p0, LX/1u6;->subscribeTopics:Ljava/util/List;

    if-eqz v0, :cond_4e

    move v0, v1

    .line 337601
    :goto_1b
    iget-object v3, p1, LX/1u6;->subscribeTopics:Ljava/util/List;

    if-eqz v3, :cond_4f

    move v3, v1

    .line 337602
    :goto_1c
    if-nez v0, :cond_1c

    if-eqz v3, :cond_1d

    .line 337603
    :cond_1c
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337604
    iget-object v0, p0, LX/1u6;->subscribeTopics:Ljava/util/List;

    iget-object v3, p1, LX/1u6;->subscribeTopics:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337605
    :cond_1d
    iget-object v0, p0, LX/1u6;->clientType:Ljava/lang/String;

    if-eqz v0, :cond_50

    move v0, v1

    .line 337606
    :goto_1d
    iget-object v3, p1, LX/1u6;->clientType:Ljava/lang/String;

    if-eqz v3, :cond_51

    move v3, v1

    .line 337607
    :goto_1e
    if-nez v0, :cond_1e

    if-eqz v3, :cond_1f

    .line 337608
    :cond_1e
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337609
    iget-object v0, p0, LX/1u6;->clientType:Ljava/lang/String;

    iget-object v3, p1, LX/1u6;->clientType:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337610
    :cond_1f
    iget-object v0, p0, LX/1u6;->appId:Ljava/lang/Long;

    if-eqz v0, :cond_52

    move v0, v1

    .line 337611
    :goto_1f
    iget-object v3, p1, LX/1u6;->appId:Ljava/lang/Long;

    if-eqz v3, :cond_53

    move v3, v1

    .line 337612
    :goto_20
    if-nez v0, :cond_20

    if-eqz v3, :cond_21

    .line 337613
    :cond_20
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337614
    iget-object v0, p0, LX/1u6;->appId:Ljava/lang/Long;

    iget-object v3, p1, LX/1u6;->appId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337615
    :cond_21
    iget-object v0, p0, LX/1u6;->overrideNectarLogging:Ljava/lang/Boolean;

    if-eqz v0, :cond_54

    move v0, v1

    .line 337616
    :goto_21
    iget-object v3, p1, LX/1u6;->overrideNectarLogging:Ljava/lang/Boolean;

    if-eqz v3, :cond_55

    move v3, v1

    .line 337617
    :goto_22
    if-nez v0, :cond_22

    if-eqz v3, :cond_23

    .line 337618
    :cond_22
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337619
    iget-object v0, p0, LX/1u6;->overrideNectarLogging:Ljava/lang/Boolean;

    iget-object v3, p1, LX/1u6;->overrideNectarLogging:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337620
    :cond_23
    iget-object v0, p0, LX/1u6;->connectTokenHash:[B

    if-eqz v0, :cond_56

    move v0, v1

    .line 337621
    :goto_23
    iget-object v3, p1, LX/1u6;->connectTokenHash:[B

    if-eqz v3, :cond_57

    move v3, v1

    .line 337622
    :goto_24
    if-nez v0, :cond_24

    if-eqz v3, :cond_25

    .line 337623
    :cond_24
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337624
    iget-object v0, p0, LX/1u6;->connectTokenHash:[B

    iget-object v3, p1, LX/1u6;->connectTokenHash:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337625
    :cond_25
    iget-object v0, p0, LX/1u6;->regionPreference:Ljava/lang/String;

    if-eqz v0, :cond_58

    move v0, v1

    .line 337626
    :goto_25
    iget-object v3, p1, LX/1u6;->regionPreference:Ljava/lang/String;

    if-eqz v3, :cond_59

    move v3, v1

    .line 337627
    :goto_26
    if-nez v0, :cond_26

    if-eqz v3, :cond_27

    .line 337628
    :cond_26
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337629
    iget-object v0, p0, LX/1u6;->regionPreference:Ljava/lang/String;

    iget-object v3, p1, LX/1u6;->regionPreference:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337630
    :cond_27
    iget-object v0, p0, LX/1u6;->deviceSecret:Ljava/lang/String;

    if-eqz v0, :cond_5a

    move v0, v1

    .line 337631
    :goto_27
    iget-object v3, p1, LX/1u6;->deviceSecret:Ljava/lang/String;

    if-eqz v3, :cond_5b

    move v3, v1

    .line 337632
    :goto_28
    if-nez v0, :cond_28

    if-eqz v3, :cond_29

    .line 337633
    :cond_28
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337634
    iget-object v0, p0, LX/1u6;->deviceSecret:Ljava/lang/String;

    iget-object v3, p1, LX/1u6;->deviceSecret:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337635
    :cond_29
    iget-object v0, p0, LX/1u6;->clientStack:Ljava/lang/Byte;

    if-eqz v0, :cond_5c

    move v0, v1

    .line 337636
    :goto_29
    iget-object v3, p1, LX/1u6;->clientStack:Ljava/lang/Byte;

    if-eqz v3, :cond_5d

    move v3, v1

    .line 337637
    :goto_2a
    if-nez v0, :cond_2a

    if-eqz v3, :cond_2b

    .line 337638
    :cond_2a
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337639
    iget-object v0, p0, LX/1u6;->clientStack:Ljava/lang/Byte;

    iget-object v3, p1, LX/1u6;->clientStack:Ljava/lang/Byte;

    invoke-virtual {v0, v3}, Ljava/lang/Byte;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337640
    :cond_2b
    iget-object v0, p0, LX/1u6;->fbnsConnectionKey:Ljava/lang/Long;

    if-eqz v0, :cond_5e

    move v0, v1

    .line 337641
    :goto_2b
    iget-object v3, p1, LX/1u6;->fbnsConnectionKey:Ljava/lang/Long;

    if-eqz v3, :cond_5f

    move v3, v1

    .line 337642
    :goto_2c
    if-nez v0, :cond_2c

    if-eqz v3, :cond_2d

    .line 337643
    :cond_2c
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337644
    iget-object v0, p0, LX/1u6;->fbnsConnectionKey:Ljava/lang/Long;

    iget-object v3, p1, LX/1u6;->fbnsConnectionKey:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337645
    :cond_2d
    iget-object v0, p0, LX/1u6;->fbnsConnectionSecret:Ljava/lang/String;

    if-eqz v0, :cond_60

    move v0, v1

    .line 337646
    :goto_2d
    iget-object v3, p1, LX/1u6;->fbnsConnectionSecret:Ljava/lang/String;

    if-eqz v3, :cond_61

    move v3, v1

    .line 337647
    :goto_2e
    if-nez v0, :cond_2e

    if-eqz v3, :cond_2f

    .line 337648
    :cond_2e
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337649
    iget-object v0, p0, LX/1u6;->fbnsConnectionSecret:Ljava/lang/String;

    iget-object v3, p1, LX/1u6;->fbnsConnectionSecret:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337650
    :cond_2f
    iget-object v0, p0, LX/1u6;->fbnsDeviceId:Ljava/lang/String;

    if-eqz v0, :cond_62

    move v0, v1

    .line 337651
    :goto_2f
    iget-object v3, p1, LX/1u6;->fbnsDeviceId:Ljava/lang/String;

    if-eqz v3, :cond_63

    move v3, v1

    .line 337652
    :goto_30
    if-nez v0, :cond_30

    if-eqz v3, :cond_31

    .line 337653
    :cond_30
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337654
    iget-object v0, p0, LX/1u6;->fbnsDeviceId:Ljava/lang/String;

    iget-object v3, p1, LX/1u6;->fbnsDeviceId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337655
    :cond_31
    iget-object v0, p0, LX/1u6;->fbnsDeviceSecret:Ljava/lang/String;

    if-eqz v0, :cond_64

    move v0, v1

    .line 337656
    :goto_31
    iget-object v3, p1, LX/1u6;->fbnsDeviceSecret:Ljava/lang/String;

    if-eqz v3, :cond_65

    move v3, v1

    .line 337657
    :goto_32
    if-nez v0, :cond_32

    if-eqz v3, :cond_33

    .line 337658
    :cond_32
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 337659
    iget-object v0, p0, LX/1u6;->fbnsDeviceSecret:Ljava/lang/String;

    iget-object v3, p1, LX/1u6;->fbnsDeviceSecret:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_33
    move v2, v1

    .line 337660
    goto/16 :goto_0

    :cond_34
    move v0, v2

    .line 337661
    goto/16 :goto_1

    :cond_35
    move v3, v2

    .line 337662
    goto/16 :goto_2

    :cond_36
    move v0, v2

    .line 337663
    goto/16 :goto_3

    :cond_37
    move v3, v2

    .line 337664
    goto/16 :goto_4

    :cond_38
    move v0, v2

    .line 337665
    goto/16 :goto_5

    :cond_39
    move v3, v2

    .line 337666
    goto/16 :goto_6

    :cond_3a
    move v0, v2

    .line 337667
    goto/16 :goto_7

    :cond_3b
    move v3, v2

    .line 337668
    goto/16 :goto_8

    :cond_3c
    move v0, v2

    .line 337669
    goto/16 :goto_9

    :cond_3d
    move v3, v2

    .line 337670
    goto/16 :goto_a

    :cond_3e
    move v0, v2

    .line 337671
    goto/16 :goto_b

    :cond_3f
    move v3, v2

    .line 337672
    goto/16 :goto_c

    :cond_40
    move v0, v2

    .line 337673
    goto/16 :goto_d

    :cond_41
    move v3, v2

    .line 337674
    goto/16 :goto_e

    :cond_42
    move v0, v2

    .line 337675
    goto/16 :goto_f

    :cond_43
    move v3, v2

    .line 337676
    goto/16 :goto_10

    :cond_44
    move v0, v2

    .line 337677
    goto/16 :goto_11

    :cond_45
    move v3, v2

    .line 337678
    goto/16 :goto_12

    :cond_46
    move v0, v2

    .line 337679
    goto/16 :goto_13

    :cond_47
    move v3, v2

    .line 337680
    goto/16 :goto_14

    :cond_48
    move v0, v2

    .line 337681
    goto/16 :goto_15

    :cond_49
    move v3, v2

    .line 337682
    goto/16 :goto_16

    :cond_4a
    move v0, v2

    .line 337683
    goto/16 :goto_17

    :cond_4b
    move v3, v2

    .line 337684
    goto/16 :goto_18

    :cond_4c
    move v0, v2

    .line 337685
    goto/16 :goto_19

    :cond_4d
    move v3, v2

    .line 337686
    goto/16 :goto_1a

    :cond_4e
    move v0, v2

    .line 337687
    goto/16 :goto_1b

    :cond_4f
    move v3, v2

    .line 337688
    goto/16 :goto_1c

    :cond_50
    move v0, v2

    .line 337689
    goto/16 :goto_1d

    :cond_51
    move v3, v2

    .line 337690
    goto/16 :goto_1e

    :cond_52
    move v0, v2

    .line 337691
    goto/16 :goto_1f

    :cond_53
    move v3, v2

    .line 337692
    goto/16 :goto_20

    :cond_54
    move v0, v2

    .line 337693
    goto/16 :goto_21

    :cond_55
    move v3, v2

    .line 337694
    goto/16 :goto_22

    :cond_56
    move v0, v2

    .line 337695
    goto/16 :goto_23

    :cond_57
    move v3, v2

    .line 337696
    goto/16 :goto_24

    :cond_58
    move v0, v2

    .line 337697
    goto/16 :goto_25

    :cond_59
    move v3, v2

    .line 337698
    goto/16 :goto_26

    :cond_5a
    move v0, v2

    .line 337699
    goto/16 :goto_27

    :cond_5b
    move v3, v2

    .line 337700
    goto/16 :goto_28

    :cond_5c
    move v0, v2

    .line 337701
    goto/16 :goto_29

    :cond_5d
    move v3, v2

    .line 337702
    goto/16 :goto_2a

    :cond_5e
    move v0, v2

    .line 337703
    goto/16 :goto_2b

    :cond_5f
    move v3, v2

    .line 337704
    goto/16 :goto_2c

    :cond_60
    move v0, v2

    .line 337705
    goto/16 :goto_2d

    :cond_61
    move v3, v2

    .line 337706
    goto/16 :goto_2e

    :cond_62
    move v0, v2

    .line 337707
    goto/16 :goto_2f

    :cond_63
    move v3, v2

    .line 337708
    goto/16 :goto_30

    :cond_64
    move v0, v2

    .line 337709
    goto/16 :goto_31

    :cond_65
    move v3, v2

    .line 337710
    goto/16 :goto_32
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 337529
    if-nez p1, :cond_1

    .line 337530
    :cond_0
    :goto_0
    return v0

    .line 337531
    :cond_1
    instance-of v1, p1, LX/1u6;

    if-eqz v1, :cond_0

    .line 337532
    check-cast p1, LX/1u6;

    invoke-virtual {p0, p1}, LX/1u6;->a(LX/1u6;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 337528
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 337525
    sget-boolean v0, LX/1u6;->a:Z

    .line 337526
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/1u6;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 337527
    return-object v0
.end method
