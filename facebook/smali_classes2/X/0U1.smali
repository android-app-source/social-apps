.class public LX/0U1;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "LX/0U1;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "LX/0U1;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 64133
    new-instance v0, LX/0U1;

    const-string v1, "rowid"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0U1;->a:LX/0U1;

    .line 64134
    new-instance v0, LX/0U2;

    invoke-direct {v0}, LX/0U2;-><init>()V

    sput-object v0, LX/0U1;->b:LX/0QK;

    .line 64135
    new-instance v0, LX/0U3;

    invoke-direct {v0}, LX/0U3;-><init>()V

    sput-object v0, LX/0U1;->c:LX/0QK;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 64136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64137
    iput-object p1, p0, LX/0U1;->d:Ljava/lang/String;

    .line 64138
    iput-object p2, p0, LX/0U1;->e:Ljava/lang/String;

    .line 64139
    return-void
.end method

.method public static final f(Ljava/lang/String;)LX/0QK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0QK",
            "<",
            "LX/0U1;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64140
    new-instance v0, LX/48v;

    invoke-direct {v0, p0}, LX/48v;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)I
    .locals 1

    .prologue
    .line 64141
    iget-object v0, p0, LX/0U1;->d:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)LX/0ux;
    .locals 1

    .prologue
    .line 64145
    iget-object v0, p0, LX/0U1;->d:Ljava/lang/String;

    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64142
    iget-object v0, p0, LX/0U1;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/0ux;
    .locals 1

    .prologue
    .line 64130
    iget-object v0, p0, LX/0U1;->d:Ljava/lang/String;

    invoke-static {v0, p1}, LX/0uu;->b(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64143
    invoke-virtual {p0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/database/Cursor;)J
    .locals 2

    .prologue
    .line 64144
    invoke-virtual {p0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c(Ljava/lang/String;)LX/0ux;
    .locals 1

    .prologue
    .line 64131
    iget-object v0, p0, LX/0U1;->d:Ljava/lang/String;

    invoke-static {v0, p1}, LX/0uu;->c(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    return-object v0
.end method

.method public final d(Landroid/database/Cursor;)I
    .locals 1

    .prologue
    .line 64132
    invoke-virtual {p0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final d(Ljava/lang/String;)LX/0ux;
    .locals 1

    .prologue
    .line 64115
    iget-object v0, p0, LX/0U1;->d:Ljava/lang/String;

    invoke-static {v0, p1}, LX/0uu;->e(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/0U1;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e(Landroid/database/Cursor;)D
    .locals 2

    .prologue
    .line 64114
    invoke-virtual {p0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public final e(Ljava/lang/String;)LX/0ux;
    .locals 1

    .prologue
    .line 64116
    iget-object v0, p0, LX/0U1;->d:Ljava/lang/String;

    invoke-static {v0, p1}, LX/0uu;->f(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/0U1;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64118
    if-ne p0, p1, :cond_1

    .line 64119
    :cond_0
    :goto_0
    return v0

    .line 64120
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 64121
    :cond_3
    check-cast p1, LX/0U1;

    .line 64122
    iget-object v2, p0, LX/0U1;->d:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/0U1;->d:Ljava/lang/String;

    iget-object v3, p1, LX/0U1;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p1, LX/0U1;->d:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 64123
    :cond_6
    iget-object v2, p0, LX/0U1;->e:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/0U1;->e:Ljava/lang/String;

    iget-object v3, p1, LX/0U1;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_7
    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p1, LX/0U1;->e:Ljava/lang/String;

    if-nez v2, :cond_7

    goto :goto_0
.end method

.method public final f(Landroid/database/Cursor;)[B
    .locals 1

    .prologue
    .line 64124
    invoke-virtual {p0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 64125
    iget-object v0, p0, LX/0U1;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0U1;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 64126
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/0U1;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, LX/0U1;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 64127
    return v0

    :cond_1
    move v0, v1

    .line 64128
    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64129
    iget-object v0, p0, LX/0U1;->d:Ljava/lang/String;

    return-object v0
.end method
