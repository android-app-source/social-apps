.class public LX/1Fm;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/concurrent/Executor;

.field public final c:LX/0tX;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 224591
    const-class v0, LX/1Fm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1Fm;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;LX/0tX;LX/0Or;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "LX/0tX;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 224592
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224593
    iput-object p1, p0, LX/1Fm;->b:Ljava/util/concurrent/Executor;

    .line 224594
    iput-object p2, p0, LX/1Fm;->c:LX/0tX;

    .line 224595
    iput-object p3, p0, LX/1Fm;->d:LX/0Or;

    .line 224596
    return-void
.end method

.method public static b(LX/0QB;)LX/1Fm;
    .locals 4

    .prologue
    .line 224597
    new-instance v2, LX/1Fm;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    const/16 v3, 0x15e7

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, LX/1Fm;-><init>(Ljava/util/concurrent/Executor;LX/0tX;LX/0Or;)V

    .line 224598
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/AI9;)V
    .locals 4

    .prologue
    .line 224599
    new-instance v0, LX/AHv;

    invoke-direct {v0}, LX/AHv;-><init>()V

    move-object v1, v0

    .line 224600
    new-instance v2, LX/4E8;

    invoke-direct {v2}, LX/4E8;-><init>()V

    iget-object v0, p0, LX/1Fm;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 224601
    const-string v3, "actor_id"

    invoke-virtual {v2, v3, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 224602
    move-object v0, v2

    .line 224603
    iget-object v2, v1, LX/0gW;->h:Ljava/lang/String;

    move-object v2, v2

    .line 224604
    const-string v3, "client_mutation_id"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 224605
    move-object v0, v0

    .line 224606
    const-string v2, "thread_id"

    invoke-virtual {v0, v2, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 224607
    move-object v0, v0

    .line 224608
    const-string v2, "message"

    invoke-virtual {v0, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 224609
    move-object v0, v0

    .line 224610
    const-string v2, "0"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 224611
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 224612
    iget-object v1, p0, LX/1Fm;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 224613
    new-instance v1, LX/AI8;

    invoke-direct {v1, p0, p3}, LX/AI8;-><init>(LX/1Fm;LX/AI9;)V

    iget-object v2, p0, LX/1Fm;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 224614
    return-void
.end method
