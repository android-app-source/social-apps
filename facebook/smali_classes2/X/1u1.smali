.class public final LX/1u1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 336957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 336958
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/1u0;->fromOrdinal(I)LX/1u0;

    move-result-object v1

    .line 336959
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 336960
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    .line 336961
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    .line 336962
    const/4 v0, 0x1

    new-array v0, v0, [Z

    .line 336963
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 336964
    const/4 v8, 0x0

    aget-boolean v8, v0, v8

    .line 336965
    new-instance v0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;

    invoke-direct/range {v0 .. v8}, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;-><init>(LX/1u0;JJJZ)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 336966
    new-array v0, p1, [Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;

    return-object v0
.end method
