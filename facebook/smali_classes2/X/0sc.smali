.class public LX/0sc;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 152371
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 152372
    return-void
.end method

.method public static a(Landroid/content/res/Resources;Landroid/content/Context;LX/0gw;LX/0gy;Ljava/lang/Boolean;)LX/0sd;
    .locals 4
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/tablet/IsTablet;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 152357
    const v0, 0x7f0b00b9

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 152358
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 152359
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    .line 152360
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 152361
    invoke-virtual {p2}, LX/0gw;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 152362
    iget-object v1, p3, LX/0gy;->b:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    invoke-static {p3, v1}, LX/0gy;->a(LX/0gy;I)LX/0gz;

    move-result-object v1

    .line 152363
    iget-object v3, p3, LX/0gy;->b:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->d()I

    move-result v3

    invoke-static {p3, v3}, LX/0gy;->b(LX/0gy;I)LX/0gz;

    move-result-object v3

    .line 152364
    iget p3, v1, LX/0gz;->a:I

    move v1, p3

    .line 152365
    iget p3, v3, LX/0gz;->a:I

    move v3, p3

    .line 152366
    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v1, v1

    .line 152367
    :cond_0
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    int-to-float v0, v0

    const v3, 0x3f4ccccd    # 0.8f

    mul-float/2addr v0, v3

    float-to-int v0, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 152368
    :cond_1
    int-to-float v0, v1

    const v3, 0x3f638e39

    mul-float/2addr v0, v3

    float-to-int v0, v0

    .line 152369
    new-instance v3, LX/0sd;

    invoke-direct {v3, v2, v0, v1}, LX/0sd;-><init>(III)V

    return-object v3
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 152370
    return-void
.end method
