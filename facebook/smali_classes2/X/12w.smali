.class public LX/12w;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static volatile m:LX/12w;


# instance fields
.field public final c:Landroid/content/Context;

.field private final d:LX/12x;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final f:LX/0Sg;

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/jewel/JewelCountHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/0xW;

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/ReactionUtil;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Z

.field public final l:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 175828
    const-class v0, LX/12w;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/12w;->b:Ljava/lang/String;

    .line 175829
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/12w;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".INITIATE_BACKGROUND_FETCH"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/12w;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/12x;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sg;LX/0xW;LX/0Or;)V
    .locals 1
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/common/alarm/FbAlarmManager;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "LX/0xW;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 175830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175831
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 175832
    iput-object v0, p0, LX/12w;->g:LX/0Ot;

    .line 175833
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 175834
    iput-object v0, p0, LX/12w;->j:LX/0Ot;

    .line 175835
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/12w;->k:Z

    .line 175836
    new-instance v0, Lcom/facebook/notifications/jewel/JewelCountFetcher$2;

    invoke-direct {v0, p0}, Lcom/facebook/notifications/jewel/JewelCountFetcher$2;-><init>(LX/12w;)V

    iput-object v0, p0, LX/12w;->l:Ljava/lang/Runnable;

    .line 175837
    iput-object p1, p0, LX/12w;->c:Landroid/content/Context;

    .line 175838
    iput-object p2, p0, LX/12w;->d:LX/12x;

    .line 175839
    iput-object p3, p0, LX/12w;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 175840
    iput-object p4, p0, LX/12w;->f:LX/0Sg;

    .line 175841
    iput-object p5, p0, LX/12w;->i:LX/0xW;

    .line 175842
    iput-object p6, p0, LX/12w;->h:LX/0Or;

    .line 175843
    return-void
.end method

.method public static a(LX/0QB;)LX/12w;
    .locals 10

    .prologue
    .line 175844
    sget-object v0, LX/12w;->m:LX/12w;

    if-nez v0, :cond_1

    .line 175845
    const-class v1, LX/12w;

    monitor-enter v1

    .line 175846
    :try_start_0
    sget-object v0, LX/12w;->m:LX/12w;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 175847
    if-eqz v2, :cond_0

    .line 175848
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 175849
    new-instance v3, LX/12w;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v5

    check-cast v5, LX/12x;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v7

    check-cast v7, LX/0Sg;

    invoke-static {v0}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v8

    check-cast v8, LX/0xW;

    const/16 v9, 0x15e7

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/12w;-><init>(Landroid/content/Context;LX/12x;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sg;LX/0xW;LX/0Or;)V

    .line 175850
    const/16 v4, 0xe3e

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1064

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    .line 175851
    iput-object v4, v3, LX/12w;->g:LX/0Ot;

    iput-object v5, v3, LX/12w;->j:LX/0Ot;

    .line 175852
    move-object v0, v3

    .line 175853
    sput-object v0, LX/12w;->m:LX/12w;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175854
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 175855
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 175856
    :cond_1
    sget-object v0, LX/12w;->m:LX/12w;

    return-object v0

    .line 175857
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 175858
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(J)V
    .locals 8

    .prologue
    .line 175859
    const-wide/16 v0, 0x3c

    div-long v0, p1, v0

    .line 175860
    const-wide/32 v2, 0x36ee80

    mul-long v4, v0, v2

    .line 175861
    invoke-virtual {p0}, LX/12w;->c()V

    .line 175862
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/12w;->a(LX/12w;Z)V

    .line 175863
    iget-object v0, p0, LX/12w;->d:LX/12x;

    const/4 v1, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    add-long/2addr v2, v4

    const/4 p2, 0x0

    .line 175864
    new-instance v6, Landroid/content/Intent;

    iget-object v7, p0, LX/12w;->c:Landroid/content/Context;

    const-class p1, Lcom/facebook/notifications/jewel/JewelCountFetcher$JewelCountBroadcastReceiver;

    invoke-direct {v6, v7, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 175865
    sget-object v7, LX/12w;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 175866
    iget-object v7, p0, LX/12w;->c:Landroid/content/Context;

    invoke-static {v7, p2, v6, p2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    move-object v6, v6

    .line 175867
    invoke-virtual/range {v0 .. v6}, LX/12x;->a(IJJLandroid/app/PendingIntent;)V

    .line 175868
    return-void
.end method

.method private static a(LX/12w;Z)V
    .locals 2

    .prologue
    .line 175869
    iget-object v0, p0, LX/12w;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0hM;->B:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 175870
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetInexactRepeatingArgs"
        }
    .end annotation

    .prologue
    .line 175871
    iget-object v0, p0, LX/12w;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->B:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    move v0, v0

    .line 175872
    if-nez v0, :cond_0

    .line 175873
    const-wide/16 v0, 0x1e0

    invoke-direct {p0, v0, v1}, LX/12w;->a(J)V

    .line 175874
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 175875
    invoke-static {p0, v3}, LX/12w;->a(LX/12w;Z)V

    .line 175876
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/12w;->c:Landroid/content/Context;

    const-class v2, Lcom/facebook/notifications/jewel/JewelCountFetcher$JewelCountBroadcastReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 175877
    sget-object v1, LX/12w;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 175878
    iget-object v1, p0, LX/12w;->c:Landroid/content/Context;

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 175879
    iget-object v1, p0, LX/12w;->d:LX/12x;

    invoke-virtual {v1, v0}, LX/12x;->a(Landroid/app/PendingIntent;)V

    .line 175880
    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 175881
    return-void
.end method
