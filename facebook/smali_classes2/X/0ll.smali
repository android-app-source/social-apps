.class public LX/0ll;
.super Ljava/text/DateFormat;
.source ""


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:Ljava/text/DateFormat;

.field public static final c:Ljava/text/DateFormat;

.field public static final d:Ljava/text/DateFormat;

.field public static final e:Ljava/text/DateFormat;

.field public static final f:LX/0ll;

.field private static final l:Ljava/util/TimeZone;


# instance fields
.field public transient g:Ljava/util/TimeZone;

.field public transient h:Ljava/text/DateFormat;

.field public transient i:Ljava/text/DateFormat;

.field public transient j:Ljava/text/DateFormat;

.field public transient k:Ljava/text/DateFormat;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 130487
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "yyyy-MM-dd\'T\'HH:mm:ss.SSSZ"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "EEE, dd MMM yyyy HH:mm:ss zzz"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "yyyy-MM-dd"

    aput-object v2, v0, v1

    sput-object v0, LX/0ll;->a:[Ljava/lang/String;

    .line 130488
    const-string v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, LX/0ll;->l:Ljava/util/TimeZone;

    .line 130489
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEE, dd MMM yyyy HH:mm:ss zzz"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 130490
    sput-object v0, LX/0ll;->b:Ljava/text/DateFormat;

    sget-object v1, LX/0ll;->l:Ljava/util/TimeZone;

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 130491
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSSZ"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 130492
    sput-object v0, LX/0ll;->c:Ljava/text/DateFormat;

    sget-object v1, LX/0ll;->l:Ljava/util/TimeZone;

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 130493
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 130494
    sput-object v0, LX/0ll;->d:Ljava/text/DateFormat;

    sget-object v1, LX/0ll;->l:Ljava/util/TimeZone;

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 130495
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 130496
    sput-object v0, LX/0ll;->e:Ljava/text/DateFormat;

    sget-object v1, LX/0ll;->l:Ljava/util/TimeZone;

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 130497
    new-instance v0, LX/0ll;

    invoke-direct {v0}, LX/0ll;-><init>()V

    sput-object v0, LX/0ll;->f:LX/0ll;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 130551
    invoke-direct {p0}, Ljava/text/DateFormat;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/util/TimeZone;)V
    .locals 0

    .prologue
    .line 130548
    invoke-direct {p0}, Ljava/text/DateFormat;-><init>()V

    .line 130549
    iput-object p1, p0, LX/0ll;->g:Ljava/util/TimeZone;

    .line 130550
    return-void
.end method

.method private static a()LX/0ll;
    .locals 1

    .prologue
    .line 130547
    new-instance v0, LX/0ll;

    invoke-direct {v0}, LX/0ll;-><init>()V

    return-object v0
.end method

.method public static a(Ljava/util/TimeZone;)LX/0ll;
    .locals 1

    .prologue
    .line 130544
    if-nez p0, :cond_0

    .line 130545
    sget-object p0, LX/0ll;->l:Ljava/util/TimeZone;

    .line 130546
    :cond_0
    new-instance v0, LX/0ll;

    invoke-direct {v0, p0}, LX/0ll;-><init>(Ljava/util/TimeZone;)V

    return-object v0
.end method

.method private final a(Ljava/text/DateFormat;)Ljava/text/DateFormat;
    .locals 1

    .prologue
    .line 130543
    iget-object v0, p0, LX/0ll;->g:Ljava/util/TimeZone;

    invoke-static {p1, v0}, LX/0ll;->a(Ljava/text/DateFormat;Ljava/util/TimeZone;)Ljava/text/DateFormat;

    move-result-object v0

    return-object v0
.end method

.method private static final a(Ljava/text/DateFormat;Ljava/util/TimeZone;)Ljava/text/DateFormat;
    .locals 1

    .prologue
    .line 130539
    invoke-virtual {p0}, Ljava/text/DateFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/DateFormat;

    .line 130540
    if-eqz p1, :cond_0

    .line 130541
    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 130542
    :cond_0
    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;
    .locals 5

    .prologue
    const/16 v4, 0x5a

    const/16 v3, 0x3a

    .line 130498
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 130499
    add-int/lit8 v0, v1, -0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 130500
    const/16 v2, 0xa

    if-gt v1, v2, :cond_1

    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 130501
    iget-object v0, p0, LX/0ll;->k:Ljava/text/DateFormat;

    .line 130502
    if-nez v0, :cond_0

    .line 130503
    sget-object v0, LX/0ll;->e:Ljava/text/DateFormat;

    invoke-direct {p0, v0}, LX/0ll;->a(Ljava/text/DateFormat;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, LX/0ll;->k:Ljava/text/DateFormat;

    .line 130504
    :cond_0
    :goto_0
    invoke-virtual {v0, p1, p2}, Ljava/text/DateFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;

    move-result-object v0

    return-object v0

    .line 130505
    :cond_1
    if-ne v0, v4, :cond_3

    .line 130506
    iget-object v0, p0, LX/0ll;->j:Ljava/text/DateFormat;

    .line 130507
    if-nez v0, :cond_2

    .line 130508
    sget-object v0, LX/0ll;->d:Ljava/text/DateFormat;

    invoke-direct {p0, v0}, LX/0ll;->a(Ljava/text/DateFormat;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, LX/0ll;->j:Ljava/text/DateFormat;

    .line 130509
    :cond_2
    add-int/lit8 v2, v1, -0x4

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-ne v2, v3, :cond_0

    .line 130510
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 130511
    add-int/lit8 v1, v1, -0x1

    const-string v3, ".000"

    invoke-virtual {v2, v1, v3}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 130512
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 130513
    :cond_3
    invoke-static {p1}, LX/0ll;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 130514
    add-int/lit8 v0, v1, -0x3

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 130515
    if-ne v0, v3, :cond_6

    .line 130516
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 130517
    add-int/lit8 v2, v1, -0x3

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 130518
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 130519
    :cond_4
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 130520
    add-int/lit8 v1, v0, -0x9

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 130521
    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 130522
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 130523
    add-int/lit8 v0, v0, -0x5

    const-string v2, ".000"

    invoke-virtual {v1, v0, v2}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 130524
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 130525
    :cond_5
    iget-object v0, p0, LX/0ll;->i:Ljava/text/DateFormat;

    .line 130526
    iget-object v1, p0, LX/0ll;->i:Ljava/text/DateFormat;

    if-nez v1, :cond_0

    .line 130527
    sget-object v0, LX/0ll;->c:Ljava/text/DateFormat;

    invoke-direct {p0, v0}, LX/0ll;->a(Ljava/text/DateFormat;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, LX/0ll;->i:Ljava/text/DateFormat;

    goto :goto_0

    .line 130528
    :cond_6
    const/16 v1, 0x2b

    if-eq v0, v1, :cond_7

    const/16 v1, 0x2d

    if-ne v0, v1, :cond_4

    .line 130529
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "00"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 130530
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 130531
    const/16 v2, 0x54

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    .line 130532
    const/16 v2, 0x8

    if-gt v1, v2, :cond_9

    .line 130533
    const-string v1, ".000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130534
    :cond_9
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130535
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 130536
    iget-object v0, p0, LX/0ll;->j:Ljava/text/DateFormat;

    .line 130537
    if-nez v0, :cond_0

    .line 130538
    sget-object v0, LX/0ll;->d:Ljava/text/DateFormat;

    invoke-direct {p0, v0}, LX/0ll;->a(Ljava/text/DateFormat;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, LX/0ll;->j:Ljava/text/DateFormat;

    goto/16 :goto_0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 130552
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x5

    if-lt v1, v2, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x2d

    if-ne v1, v2, :cond_0

    .line 130553
    const/4 v0, 0x1

    .line 130554
    :cond_0
    return v0
.end method

.method public static b(Ljava/util/TimeZone;)Ljava/text/DateFormat;
    .locals 1

    .prologue
    .line 130436
    sget-object v0, LX/0ll;->c:Ljava/text/DateFormat;

    invoke-static {v0, p0}, LX/0ll;->a(Ljava/text/DateFormat;Ljava/util/TimeZone;)Ljava/text/DateFormat;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;
    .locals 1

    .prologue
    .line 130437
    iget-object v0, p0, LX/0ll;->h:Ljava/text/DateFormat;

    if-nez v0, :cond_0

    .line 130438
    sget-object v0, LX/0ll;->b:Ljava/text/DateFormat;

    invoke-direct {p0, v0}, LX/0ll;->a(Ljava/text/DateFormat;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, LX/0ll;->h:Ljava/text/DateFormat;

    .line 130439
    :cond_0
    iget-object v0, p0, LX/0ll;->h:Ljava/text/DateFormat;

    invoke-virtual {v0, p1, p2}, Ljava/text/DateFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method private static final b(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/16 v4, 0x2d

    const/16 v3, 0x2b

    const/4 v0, 0x1

    .line 130440
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 130441
    const/4 v2, 0x6

    if-lt v1, v2, :cond_2

    .line 130442
    add-int/lit8 v2, v1, -0x6

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 130443
    if-eq v2, v3, :cond_0

    if-ne v2, v4, :cond_1

    .line 130444
    :cond_0
    :goto_0
    return v0

    .line 130445
    :cond_1
    add-int/lit8 v2, v1, -0x5

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 130446
    if-eq v2, v3, :cond_0

    if-eq v2, v4, :cond_0

    .line 130447
    add-int/lit8 v1, v1, -0x3

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 130448
    if-eq v1, v3, :cond_0

    if-eq v1, v4, :cond_0

    .line 130449
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 130450
    invoke-static {}, LX/0ll;->a()LX/0ll;

    move-result-object v0

    return-object v0
.end method

.method public final format(Ljava/util/Date;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 1

    .prologue
    .line 130451
    iget-object v0, p0, LX/0ll;->i:Ljava/text/DateFormat;

    if-nez v0, :cond_0

    .line 130452
    sget-object v0, LX/0ll;->c:Ljava/text/DateFormat;

    invoke-direct {p0, v0}, LX/0ll;->a(Ljava/text/DateFormat;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, LX/0ll;->i:Ljava/text/DateFormat;

    .line 130453
    :cond_0
    iget-object v0, p0, LX/0ll;->i:Ljava/text/DateFormat;

    invoke-virtual {v0, p1, p2, p3}, Ljava/text/DateFormat;->format(Ljava/util/Date;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public final parse(Ljava/lang/String;)Ljava/util/Date;
    .locals 10

    .prologue
    const/16 v9, 0x22

    const/4 v1, 0x0

    .line 130454
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 130455
    new-instance v3, Ljava/text/ParsePosition;

    invoke-direct {v3, v1}, Ljava/text/ParsePosition;-><init>(I)V

    .line 130456
    invoke-virtual {p0, v2, v3}, LX/0ll;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;

    move-result-object v0

    .line 130457
    if-eqz v0, :cond_0

    .line 130458
    return-object v0

    .line 130459
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 130460
    sget-object v5, LX/0ll;->a:[Ljava/lang/String;

    array-length v6, v5

    move v0, v1

    :goto_0
    if-ge v0, v6, :cond_2

    aget-object v7, v5, v0

    .line 130461
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_1

    .line 130462
    const-string v8, "\", \""

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130463
    :goto_1
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130464
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 130465
    :cond_1
    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 130466
    :cond_2
    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130467
    new-instance v0, Ljava/text/ParseException;

    const-string v5, "Can not parse date \"%s\": not compatible with any of standard forms (%s)"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v1

    const/4 v1, 0x1

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v1

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Ljava/text/ParsePosition;->getErrorIndex()I

    move-result v2

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0
.end method

.method public final parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;
    .locals 4

    .prologue
    .line 130468
    invoke-static {p1}, LX/0ll;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130469
    invoke-direct {p0, p1, p2}, LX/0ll;->a(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;

    move-result-object v0

    .line 130470
    :goto_0
    return-object v0

    .line 130471
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 130472
    :cond_1
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_3

    .line 130473
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 130474
    const/16 v2, 0x30

    if-lt v1, v2, :cond_2

    const/16 v2, 0x39

    if-le v1, v2, :cond_1

    .line 130475
    :cond_2
    if-gtz v0, :cond_3

    const/16 v2, 0x2d

    if-eq v1, v2, :cond_1

    .line 130476
    :cond_3
    if-gez v0, :cond_4

    .line 130477
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/16K;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 130478
    new-instance v0, Ljava/util/Date;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    goto :goto_0

    .line 130479
    :cond_4
    invoke-direct {p0, p1, p2}, LX/0ll;->b(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;

    move-result-object v0

    goto :goto_0
.end method

.method public final setTimeZone(Ljava/util/TimeZone;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 130480
    iget-object v0, p0, LX/0ll;->g:Ljava/util/TimeZone;

    if-eq p1, v0, :cond_0

    .line 130481
    iput-object v1, p0, LX/0ll;->h:Ljava/text/DateFormat;

    .line 130482
    iput-object v1, p0, LX/0ll;->i:Ljava/text/DateFormat;

    .line 130483
    iput-object v1, p0, LX/0ll;->j:Ljava/text/DateFormat;

    .line 130484
    iput-object v1, p0, LX/0ll;->k:Ljava/text/DateFormat;

    .line 130485
    iput-object p1, p0, LX/0ll;->g:Ljava/util/TimeZone;

    .line 130486
    :cond_0
    return-void
.end method
