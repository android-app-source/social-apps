.class public LX/1CN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public final a:LX/0bH;

.field public final b:LX/0So;

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1Ya",
            "<+",
            "LX/0bJ;",
            ">;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;>;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1CO",
            "<+",
            "LX/0bJ;",
            ">;>;"
        }
    .end annotation
.end field

.field public f:J

.field public g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<+",
            "LX/0bJ;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/03R;


# direct methods
.method public constructor <init>(LX/0bH;LX/0So;LX/03R;)V
    .locals 4
    .param p3    # LX/03R;
        .annotation runtime Lcom/facebook/feed/annotations/IsFeedReturnDetectorEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 216129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216130
    iput-object p1, p0, LX/1CN;->a:LX/0bH;

    .line 216131
    const/4 v0, 0x2

    new-array v0, v0, [LX/1CO;

    new-instance v1, LX/1CP;

    invoke-direct {v1, p0}, LX/1CP;-><init>(LX/1CN;)V

    aput-object v1, v0, v3

    const/4 v1, 0x1

    new-instance v2, LX/1CR;

    invoke-direct {v2, p0}, LX/1CR;-><init>(LX/1CN;)V

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/1CN;->e:Ljava/util/Set;

    .line 216132
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1CN;->c:Ljava/util/Set;

    .line 216133
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/1CN;->g:LX/0am;

    .line 216134
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1CN;->d:Ljava/util/Set;

    .line 216135
    iput-object p2, p0, LX/1CN;->b:LX/0So;

    .line 216136
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/1CN;->f:J

    .line 216137
    iput-object p3, p0, LX/1CN;->h:LX/03R;

    .line 216138
    return-void
.end method

.method public static a(LX/0QB;)LX/1CN;
    .locals 7

    .prologue
    .line 216159
    const-class v1, LX/1CN;

    monitor-enter v1

    .line 216160
    :try_start_0
    sget-object v0, LX/1CN;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 216161
    sput-object v2, LX/1CN;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 216162
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216163
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 216164
    new-instance v6, LX/1CN;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v3

    check-cast v3, LX/0bH;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    .line 216165
    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    const/16 p0, 0x83

    invoke-virtual {v5, p0}, LX/0Uh;->a(I)LX/03R;

    move-result-object v5

    move-object v5, v5

    .line 216166
    check-cast v5, LX/03R;

    invoke-direct {v6, v3, v4, v5}, LX/1CN;-><init>(LX/0bH;LX/0So;LX/03R;)V

    .line 216167
    move-object v0, v6

    .line 216168
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 216169
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1CN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216170
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 216171
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1CN;)Z
    .locals 2

    .prologue
    .line 216155
    iget-object v0, p0, LX/1CN;->h:LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method

.method public static a$redex0(LX/1CN;LX/0bJ;)V
    .locals 2

    .prologue
    .line 216156
    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/1CN;->g:LX/0am;

    .line 216157
    iget-object v0, p0, LX/1CN;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/1CN;->f:J

    .line 216158
    return-void
.end method


# virtual methods
.method public final a(LX/1Ya;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ya",
            "<+",
            "LX/0bJ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 216153
    iget-object v0, p0, LX/1CN;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 216154
    return-void
.end method

.method public final a(Ljava/lang/Class;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 216141
    invoke-static {p0}, LX/1CN;->a(LX/1CN;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 216142
    :goto_0
    return-void

    .line 216143
    :cond_0
    iget-object v0, p0, LX/1CN;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216144
    iget-object v0, p0, LX/1CN;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1CO;

    .line 216145
    iget-object v2, p0, LX/1CN;->a:LX/0bH;

    invoke-virtual {v2, v0}, LX/0b4;->a(LX/0b2;)Z

    goto :goto_1

    .line 216146
    :cond_1
    iget-object v0, p0, LX/1CN;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 216147
    iget-object v0, p0, LX/1CN;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 216148
    iget-object v0, p0, LX/1CN;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0bJ;

    iget-object v1, p0, LX/1CN;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/1CN;->f:J

    sub-long/2addr v2, v4

    .line 216149
    iget-object v1, p0, LX/1CN;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ya;

    .line 216150
    invoke-interface {v1}, LX/1Ya;->a()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {v5, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 216151
    invoke-interface {v1, v0, v2, v3}, LX/1Ya;->a(LX/0bJ;J)V

    .line 216152
    :cond_3
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/1CN;->g:LX/0am;

    goto :goto_0
.end method

.method public final b(LX/1Ya;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ya",
            "<+",
            "LX/0bJ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 216139
    iget-object v0, p0, LX/1CN;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 216140
    return-void
.end method
