.class public LX/1S2;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/34Z;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/34a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 247377
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1S2;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/34a;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 247378
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 247379
    iput-object p1, p0, LX/1S2;->b:LX/0Ot;

    .line 247380
    return-void
.end method

.method public static a(LX/0QB;)LX/1S2;
    .locals 4

    .prologue
    .line 247381
    const-class v1, LX/1S2;

    monitor-enter v1

    .line 247382
    :try_start_0
    sget-object v0, LX/1S2;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 247383
    sput-object v2, LX/1S2;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 247384
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247385
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 247386
    new-instance v3, LX/1S2;

    const/16 p0, 0x88b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1S2;-><init>(LX/0Ot;)V

    .line 247387
    move-object v0, v3

    .line 247388
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 247389
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1S2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 247390
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 247391
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 247392
    check-cast p2, LX/34Y;

    .line 247393
    iget-object v0, p0, LX/1S2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/34a;

    iget-object v1, p2, LX/34Y;->a:LX/2tX;

    const/16 p2, 0x44

    const/4 p0, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/high16 v7, 0x3f800000    # 1.0f

    .line 247394
    sget-object v2, LX/328;->DISMISSED:LX/328;

    .line 247395
    iget-object v3, v1, LX/2tX;->d:LX/328;

    move-object v3, v3

    .line 247396
    invoke-virtual {v2, v3}, LX/328;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 247397
    const/4 v2, 0x0

    .line 247398
    :goto_0
    move-object v0, v2

    .line 247399
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b0082

    invoke-interface {v3, v4}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const v4, 0x7f0a00d5

    invoke-interface {v2, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->e(F)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    const v5, 0x7f020aea

    invoke-virtual {v4, v5}, LX/1o5;->h(I)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/16 v5, 0xd

    invoke-interface {v4, v5}, LX/1Di;->j(I)LX/1Di;

    move-result-object v4

    const/16 v5, 0xe

    invoke-interface {v4, v5}, LX/1Di;->r(I)LX/1Di;

    move-result-object v4

    const/16 v5, 0x8

    const v6, 0x7f0b0060

    invoke-interface {v4, v5, v6}, LX/1Di;->n(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v8}, LX/1Di;->c(I)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b006b

    invoke-interface {v4, p0, v5}, LX/1Di;->l(II)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b0060

    invoke-interface {v4, v8, v5}, LX/1Di;->l(II)LX/1Di;

    move-result-object v4

    .line 247400
    const v5, -0x3fc11d0a

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 247401
    invoke-interface {v4, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v2, v0, LX/34a;->c:LX/3AZ;

    invoke-virtual {v2, p1}, LX/3AZ;->c(LX/1De;)LX/3Aa;

    move-result-object v5

    sget-object v2, LX/328;->UNANSWERED:LX/328;

    .line 247402
    iget-object v6, v1, LX/2tX;->d:LX/328;

    move-object v6, v6

    .line 247403
    invoke-virtual {v2, v6}, LX/328;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 247404
    iget-object v2, v1, LX/2tX;->c:Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;

    move-object v2, v2

    .line 247405
    invoke-virtual {v2}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;->a()Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    :goto_1
    invoke-virtual {v5, v2}, LX/3Aa;->a(LX/3Ab;)LX/3Aa;

    move-result-object v2

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v5}, LX/3Aa;->a(Landroid/text/Layout$Alignment;)LX/3Aa;

    move-result-object v2

    const v5, 0x7f0b0050

    invoke-virtual {v2, v5}, LX/3Aa;->n(I)LX/3Aa;

    move-result-object v2

    const v5, 0x7f0a010d

    invoke-virtual {v2, v5}, LX/3Aa;->j(I)LX/3Aa;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v5, 0x7f0b0060

    invoke-interface {v2, v8, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v5, 0x6

    const/16 v6, 0x24

    invoke-interface {v2, v5, v6}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    iget-object v4, v0, LX/34a;->c:LX/3AZ;

    invoke-virtual {v4, p1}, LX/3AZ;->c(LX/1De;)LX/3Aa;

    move-result-object v4

    .line 247406
    iget-object v5, v1, LX/2tX;->c:Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;

    move-object v5, v5

    .line 247407
    invoke-virtual {v5}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;->a()Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/3Aa;->a(LX/3Ab;)LX/3Aa;

    move-result-object v4

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v4, v5}, LX/3Aa;->a(Landroid/text/Layout$Alignment;)LX/3Aa;

    move-result-object v4

    const v5, 0x7f0b004e

    invoke-virtual {v4, v5}, LX/3Aa;->n(I)LX/3Aa;

    move-result-object v4

    const v5, 0x7f0a010e

    invoke-virtual {v4, v5}, LX/3Aa;->j(I)LX/3Aa;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x3

    const v6, 0x7f0b0060

    invoke-interface {v4, v5, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    const/4 v5, 0x6

    const v6, 0x7f0b0060

    invoke-interface {v4, v5, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/3Ac;->c(LX/1De;)LX/3Ad;

    move-result-object v4

    invoke-virtual {v4, v8}, LX/3Ad;->h(I)LX/3Ad;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    sget-object v2, LX/328;->UNANSWERED:LX/328;

    .line 247408
    iget-object v5, v1, LX/2tX;->d:LX/328;

    move-object v5, v5

    .line 247409
    invoke-virtual {v2, v5}, LX/328;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v5, 0x4

    invoke-interface {v2, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->e(F)LX/1Dh;

    move-result-object v2

    iget-object v5, v0, LX/34a;->e:LX/2g9;

    invoke-virtual {v5, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v5

    invoke-virtual {v5, p2}, LX/2gA;->h(I)LX/2gA;

    move-result-object v5

    const v6, 0x7f080020

    invoke-virtual {v5, v6}, LX/2gA;->i(I)LX/2gA;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    .line 247410
    const v6, -0x4e5e18c6

    const/4 v8, 0x0

    invoke-static {p1, v6, v8}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 247411
    invoke-interface {v5, v6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Di;->b(F)LX/1Di;

    move-result-object v5

    invoke-interface {v2, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/3Ac;->c(LX/1De;)LX/3Ad;

    move-result-object v5

    invoke-virtual {v5, v9}, LX/3Ad;->h(I)LX/3Ad;

    move-result-object v5

    const v6, 0x7f0b0062

    invoke-virtual {v5, v6}, LX/3Ad;->i(I)LX/3Ad;

    move-result-object v5

    invoke-interface {v2, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    iget-object v5, v0, LX/34a;->e:LX/2g9;

    invoke-virtual {v5, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v5

    invoke-virtual {v5, p2}, LX/2gA;->h(I)LX/2gA;

    move-result-object v5

    const v6, 0x7f080021

    invoke-virtual {v5, v6}, LX/2gA;->i(I)LX/2gA;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    .line 247412
    const v6, -0x4e5e0fef

    const/4 v8, 0x0

    invoke-static {p1, v6, v8}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 247413
    invoke-interface {v5, v6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Di;->b(F)LX/1Di;

    move-result-object v5

    invoke-interface {v2, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/3Ac;->c(LX/1De;)LX/3Ad;

    move-result-object v5

    invoke-virtual {v5, v9}, LX/3Ad;->h(I)LX/3Ad;

    move-result-object v5

    const v6, 0x7f0b0062

    invoke-virtual {v5, v6}, LX/3Ad;->i(I)LX/3Ad;

    move-result-object v5

    invoke-interface {v2, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    iget-object v5, v0, LX/34a;->e:LX/2g9;

    invoke-virtual {v5, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v5

    invoke-virtual {v5, p2}, LX/2gA;->h(I)LX/2gA;

    move-result-object v5

    const v6, 0x7f08001d

    invoke-virtual {v5, v6}, LX/2gA;->i(I)LX/2gA;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    .line 247414
    const v6, -0x4e5e0ce8

    const/4 v8, 0x0

    invoke-static {p1, v6, v8}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 247415
    invoke-interface {v5, v6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Di;->b(F)LX/1Di;

    move-result-object v5

    invoke-interface {v2, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    :goto_2
    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b0082

    invoke-interface {v3, v4}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto/16 :goto_0

    .line 247416
    :cond_1
    iget-object v2, v1, LX/2tX;->c:Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;

    move-object v2, v2

    .line 247417
    invoke-virtual {v2}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;->a()Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->l()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    goto/16 :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 247418
    invoke-static {}, LX/1dS;->b()V

    .line 247419
    iget v0, p1, LX/1dQ;->b:I

    .line 247420
    sparse-switch v0, :sswitch_data_0

    .line 247421
    :goto_0
    return-object v2

    .line 247422
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 247423
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 247424
    check-cast v1, LX/34Y;

    .line 247425
    iget-object p1, p0, LX/1S2;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/34Y;->a:LX/2tX;

    .line 247426
    sget-object p0, LX/328;->ANSWERED_YES:LX/328;

    invoke-virtual {p1, p0}, LX/2tX;->a(LX/328;)V

    .line 247427
    goto :goto_0

    .line 247428
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 247429
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 247430
    check-cast v1, LX/34Y;

    .line 247431
    iget-object p1, p0, LX/1S2;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/34Y;->a:LX/2tX;

    .line 247432
    sget-object p0, LX/328;->ANSWERED_NO:LX/328;

    invoke-virtual {p1, p0}, LX/2tX;->a(LX/328;)V

    .line 247433
    goto :goto_0

    .line 247434
    :sswitch_2
    check-cast p2, LX/3Ae;

    .line 247435
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 247436
    check-cast v1, LX/34Y;

    .line 247437
    iget-object p1, p0, LX/1S2;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/34Y;->a:LX/2tX;

    .line 247438
    sget-object p0, LX/328;->DISMISSED:LX/328;

    invoke-virtual {p1, p0}, LX/2tX;->a(LX/328;)V

    .line 247439
    goto :goto_0

    .line 247440
    :sswitch_3
    check-cast p2, LX/3Ae;

    .line 247441
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 247442
    check-cast v1, LX/34Y;

    .line 247443
    iget-object v3, p0, LX/1S2;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/34a;

    iget-object p1, v1, LX/34Y;->b:Landroid/content/Context;

    .line 247444
    iget-object p2, v3, LX/34a;->f:LX/0if;

    sget-object p0, LX/0ig;->av:LX/0ih;

    const-string v0, "tap_chevron"

    invoke-virtual {p2, p0, v0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 247445
    iget-object p2, v3, LX/34a;->b:LX/0Or;

    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/3Af;

    .line 247446
    new-instance p0, LX/34b;

    invoke-direct {p0, p1}, LX/34b;-><init>(Landroid/content/Context;)V

    .line 247447
    const v0, 0x7f082efc

    invoke-virtual {p0, v0}, LX/34c;->e(I)LX/3Ai;

    move-result-object v0

    const v1, 0x7f0208ed

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, LX/34e;

    invoke-direct {v1, v3, p1}, LX/34e;-><init>(LX/34a;Landroid/content/Context;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 247448
    invoke-virtual {p2, p0}, LX/3Af;->a(LX/1OM;)V

    .line 247449
    invoke-virtual {p2}, LX/3Af;->show()V

    .line 247450
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x4e5e18c6 -> :sswitch_0
        -0x4e5e0fef -> :sswitch_1
        -0x4e5e0ce8 -> :sswitch_2
        -0x3fc11d0a -> :sswitch_3
    .end sparse-switch
.end method
