.class public abstract LX/0Op;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54386
    new-instance v0, LX/0Oq;

    invoke-direct {v0}, LX/0Oq;-><init>()V

    sput-object v0, LX/0Op;->a:LX/0Or;

    .line 54387
    new-instance v0, LX/0Os;

    invoke-direct {v0}, LX/0Os;-><init>()V

    sput-object v0, LX/0Op;->b:LX/0Ot;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "LX/0Ot",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 54389
    sget-object v0, LX/0Op;->b:LX/0Ot;

    return-object v0
.end method

.method public static c()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()TT;"
        }
    .end annotation

    .prologue
    .line 54390
    new-instance v0, LX/4nU;

    const-string v1, "A local injection was attempted before the constructor completed or before injectMe was called."

    invoke-direct {v0, v1}, LX/4nU;-><init>(Ljava/lang/String;)V

    throw v0
.end method
