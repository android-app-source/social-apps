.class public LX/0uM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0p1;
.implements LX/0u4;
.implements LX/0p2;


# instance fields
.field private final a:LX/0kb;

.field private final b:LX/0oz;

.field private c:LX/0uE;

.field private d:LX/0uE;

.field private e:LX/0uE;

.field private f:LX/0uE;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:LX/0p3;


# direct methods
.method public constructor <init>(LX/0p7;LX/0p8;LX/0kb;LX/0oz;)V
    .locals 3

    .prologue
    .line 156259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156260
    iput-object p3, p0, LX/0uM;->a:LX/0kb;

    .line 156261
    iput-object p4, p0, LX/0uM;->b:LX/0oz;

    .line 156262
    new-instance v0, LX/0uE;

    invoke-virtual {p1, p0}, LX/0p7;->a(LX/0p2;)LX/0p3;

    move-result-object v1

    invoke-virtual {v1}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0uE;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0uM;->c:LX/0uE;

    .line 156263
    new-instance v0, LX/0uE;

    invoke-virtual {p2, p0}, LX/0p8;->a(LX/0p1;)LX/0p3;

    move-result-object v1

    invoke-virtual {v1}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0uE;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0uM;->d:LX/0uE;

    .line 156264
    iget-object v0, p0, LX/0uM;->b:LX/0oz;

    invoke-virtual {v0}, LX/0oz;->c()LX/0p3;

    move-result-object v0

    iput-object v0, p0, LX/0uM;->i:LX/0p3;

    .line 156265
    new-instance v0, LX/0uE;

    iget-object v1, p0, LX/0uM;->i:LX/0p3;

    invoke-virtual {v1}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0uE;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0uM;->f:LX/0uE;

    .line 156266
    return-void
.end method


# virtual methods
.method public final a(J)LX/0uE;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 156267
    const-wide/16 v2, -0x1

    and-long/2addr v2, p1

    long-to-int v1, v2

    packed-switch v1, :pswitch_data_0

    .line 156268
    :goto_0
    return-object v0

    .line 156269
    :pswitch_0
    iget-object v0, p0, LX/0uM;->c:LX/0uE;

    goto :goto_0

    .line 156270
    :pswitch_1
    iget-object v0, p0, LX/0uM;->d:LX/0uE;

    goto :goto_0

    .line 156271
    :pswitch_2
    iget-object v1, p0, LX/0uM;->a:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->b()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 156272
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 156273
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v1

    .line 156274
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v0

    .line 156275
    :goto_1
    iget-object v2, p0, LX/0uM;->g:Ljava/lang/String;

    if-ne v1, v2, :cond_0

    iget-object v2, p0, LX/0uM;->h:Ljava/lang/String;

    if-eq v0, v2, :cond_2

    .line 156276
    :cond_0
    iput-object v1, p0, LX/0uM;->g:Ljava/lang/String;

    .line 156277
    iput-object v0, p0, LX/0uM;->h:Ljava/lang/String;

    .line 156278
    iget-object v0, p0, LX/0uM;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0uM;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 156279
    :cond_1
    new-instance v0, LX/0uE;

    const-string v1, "disconnected"

    invoke-direct {v0, v1}, LX/0uE;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0uM;->e:LX/0uE;

    .line 156280
    :cond_2
    :goto_2
    iget-object v0, p0, LX/0uM;->e:LX/0uE;

    goto :goto_0

    .line 156281
    :cond_3
    iget-object v0, p0, LX/0uM;->h:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/0uM;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 156282
    :cond_4
    new-instance v0, LX/0uE;

    iget-object v1, p0, LX/0uM;->g:Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0uE;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0uM;->e:LX/0uE;

    goto :goto_2

    .line 156283
    :cond_5
    new-instance v0, LX/0uE;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/0uM;->g:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/0uM;->h:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0uE;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0uM;->e:LX/0uE;

    goto :goto_2

    .line 156284
    :pswitch_3
    iget-object v0, p0, LX/0uM;->b:LX/0oz;

    invoke-virtual {v0}, LX/0oz;->c()LX/0p3;

    move-result-object v0

    .line 156285
    iget-object v1, p0, LX/0uM;->i:LX/0p3;

    if-eq v0, v1, :cond_6

    .line 156286
    iput-object v0, p0, LX/0uM;->i:LX/0p3;

    .line 156287
    new-instance v1, LX/0uE;

    invoke-virtual {v0}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/0uE;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, LX/0uM;->f:LX/0uE;

    .line 156288
    :cond_6
    iget-object v0, p0, LX/0uM;->f:LX/0uE;

    goto/16 :goto_0

    :cond_7
    move-object v1, v0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/0u5;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156289
    new-instance v0, LX/0u5;

    const-string v1, "current_bandwidth_class"

    const-wide/16 v2, 0x1

    invoke-direct {v0, v1, p0, v2, v3}, LX/0u5;-><init>(Ljava/lang/String;LX/0u4;J)V

    new-instance v1, LX/0u5;

    const-string v2, "latency_class"

    const-wide/16 v4, 0x2

    invoke-direct {v1, v2, p0, v4, v5}, LX/0u5;-><init>(Ljava/lang/String;LX/0u4;J)V

    new-instance v2, LX/0u5;

    const-string v3, "connection_type"

    const-wide/16 v4, 0x3

    invoke-direct {v2, v3, p0, v4, v5}, LX/0u5;-><init>(Ljava/lang/String;LX/0u4;J)V

    new-instance v3, LX/0u5;

    const-string v4, "bandwidth_class"

    const-wide/16 v6, 0x4

    invoke-direct {v3, v4, p0, v6, v7}, LX/0u5;-><init>(Ljava/lang/String;LX/0u4;J)V

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0p3;)V
    .locals 3

    .prologue
    .line 156290
    new-instance v0, LX/0uE;

    invoke-virtual {p1}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0uE;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0uM;->c:LX/0uE;

    .line 156291
    return-void
.end method

.method public final b(LX/0p3;)V
    .locals 3

    .prologue
    .line 156292
    new-instance v0, LX/0uE;

    invoke-virtual {p1}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0uE;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0uM;->d:LX/0uE;

    .line 156293
    return-void
.end method
