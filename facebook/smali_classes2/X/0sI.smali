.class public LX/0sI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 151397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v1, 0x15

    .line 151398
    const-string v0, "tl_PH"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 151399
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v1, :cond_0

    const-string v0, "fil"

    .line 151400
    :goto_0
    return-object v0

    .line 151401
    :cond_0
    const-string v0, "tl"

    goto :goto_0

    .line 151402
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 151403
    const-string v0, "en"

    goto :goto_0

    .line 151404
    :sswitch_0
    const-string v1, "af_ZA"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v1, "ar_AR"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v1, "as_IN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_3
    const-string v1, "az_AZ"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_4
    const-string v1, "be_BY"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_5
    const-string v1, "bg_BG"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_6
    const-string v1, "bn_IN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_7
    const-string v1, "bs_BA"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x7

    goto :goto_1

    :sswitch_8
    const-string v1, "ca_ES"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x8

    goto :goto_1

    :sswitch_9
    const-string v1, "cb_IQ"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x9

    goto :goto_1

    :sswitch_a
    const-string v1, "ck_US"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0xa

    goto :goto_1

    :sswitch_b
    const-string v1, "cs_CZ"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0xb

    goto/16 :goto_1

    :sswitch_c
    const-string v1, "cx_PH"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0xc

    goto/16 :goto_1

    :sswitch_d
    const-string v1, "cy_GB"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0xd

    goto/16 :goto_1

    :sswitch_e
    const-string v1, "da_DK"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0xe

    goto/16 :goto_1

    :sswitch_f
    const-string v1, "de_DE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0xf

    goto/16 :goto_1

    :sswitch_10
    const-string v1, "el_GR"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x10

    goto/16 :goto_1

    :sswitch_11
    const-string v1, "en_GB"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x11

    goto/16 :goto_1

    :sswitch_12
    const-string v1, "eo_EO"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x12

    goto/16 :goto_1

    :sswitch_13
    const-string v1, "es_ES"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x13

    goto/16 :goto_1

    :sswitch_14
    const-string v1, "es_LA"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x14

    goto/16 :goto_1

    :sswitch_15
    const-string v2, "et_EE"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto/16 :goto_1

    :sswitch_16
    const-string v1, "eu_ES"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x16

    goto/16 :goto_1

    :sswitch_17
    const-string v1, "fa_IR"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x17

    goto/16 :goto_1

    :sswitch_18
    const-string v1, "fb_HA"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x18

    goto/16 :goto_1

    :sswitch_19
    const-string v1, "fb_LS"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x19

    goto/16 :goto_1

    :sswitch_1a
    const-string v1, "fi_FI"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x1a

    goto/16 :goto_1

    :sswitch_1b
    const-string v1, "fo_FO"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x1b

    goto/16 :goto_1

    :sswitch_1c
    const-string v1, "fr_CA"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x1c

    goto/16 :goto_1

    :sswitch_1d
    const-string v1, "fr_FR"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x1d

    goto/16 :goto_1

    :sswitch_1e
    const-string v1, "fy_NL"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x1e

    goto/16 :goto_1

    :sswitch_1f
    const-string v1, "ga_IE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x1f

    goto/16 :goto_1

    :sswitch_20
    const-string v1, "gl_ES"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x20

    goto/16 :goto_1

    :sswitch_21
    const-string v1, "gn_PY"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x21

    goto/16 :goto_1

    :sswitch_22
    const-string v1, "gu_IN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x22

    goto/16 :goto_1

    :sswitch_23
    const-string v1, "he_IL"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x23

    goto/16 :goto_1

    :sswitch_24
    const-string v1, "hi_IN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x24

    goto/16 :goto_1

    :sswitch_25
    const-string v1, "hr_HR"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x25

    goto/16 :goto_1

    :sswitch_26
    const-string v1, "hu_HU"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x26

    goto/16 :goto_1

    :sswitch_27
    const-string v1, "hy_AM"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x27

    goto/16 :goto_1

    :sswitch_28
    const-string v1, "id_ID"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x28

    goto/16 :goto_1

    :sswitch_29
    const-string v1, "is_IS"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x29

    goto/16 :goto_1

    :sswitch_2a
    const-string v1, "it_IT"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x2a

    goto/16 :goto_1

    :sswitch_2b
    const-string v1, "ja_JP"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x2b

    goto/16 :goto_1

    :sswitch_2c
    const-string v1, "jv_ID"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x2c

    goto/16 :goto_1

    :sswitch_2d
    const-string v1, "ka_GE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x2d

    goto/16 :goto_1

    :sswitch_2e
    const-string v1, "km_KH"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x2e

    goto/16 :goto_1

    :sswitch_2f
    const-string v1, "kn_IN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x2f

    goto/16 :goto_1

    :sswitch_30
    const-string v1, "ko_KR"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x30

    goto/16 :goto_1

    :sswitch_31
    const-string v1, "ku_TR"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x31

    goto/16 :goto_1

    :sswitch_32
    const-string v1, "la_VA"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x32

    goto/16 :goto_1

    :sswitch_33
    const-string v1, "lt_LT"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x33

    goto/16 :goto_1

    :sswitch_34
    const-string v1, "lv_LV"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x34

    goto/16 :goto_1

    :sswitch_35
    const-string v1, "mg_MG"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x35

    goto/16 :goto_1

    :sswitch_36
    const-string v1, "mk_MK"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x36

    goto/16 :goto_1

    :sswitch_37
    const-string v1, "ml_IN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x37

    goto/16 :goto_1

    :sswitch_38
    const-string v1, "mn_MN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x38

    goto/16 :goto_1

    :sswitch_39
    const-string v1, "mr_IN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x39

    goto/16 :goto_1

    :sswitch_3a
    const-string v1, "ms_MY"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x3a

    goto/16 :goto_1

    :sswitch_3b
    const-string v1, "my_MM"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x3b

    goto/16 :goto_1

    :sswitch_3c
    const-string v1, "nb_NO"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x3c

    goto/16 :goto_1

    :sswitch_3d
    const-string v1, "ne_NP"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x3d

    goto/16 :goto_1

    :sswitch_3e
    const-string v1, "nl_NL"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x3e

    goto/16 :goto_1

    :sswitch_3f
    const-string v1, "nn_NO"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x3f

    goto/16 :goto_1

    :sswitch_40
    const-string v1, "or_IN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x40

    goto/16 :goto_1

    :sswitch_41
    const-string v1, "pa_IN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x41

    goto/16 :goto_1

    :sswitch_42
    const-string v1, "pl_PL"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x42

    goto/16 :goto_1

    :sswitch_43
    const-string v1, "ps_AF"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x43

    goto/16 :goto_1

    :sswitch_44
    const-string v1, "pt_BR"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x44

    goto/16 :goto_1

    :sswitch_45
    const-string v1, "pt_PT"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x45

    goto/16 :goto_1

    :sswitch_46
    const-string v1, "qz_MM"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x46

    goto/16 :goto_1

    :sswitch_47
    const-string v1, "ro_RO"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x47

    goto/16 :goto_1

    :sswitch_48
    const-string v1, "ru_RU"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x48

    goto/16 :goto_1

    :sswitch_49
    const-string v1, "rw_RW"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x49

    goto/16 :goto_1

    :sswitch_4a
    const-string v1, "si_LK"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x4a

    goto/16 :goto_1

    :sswitch_4b
    const-string v1, "sk_SK"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x4b

    goto/16 :goto_1

    :sswitch_4c
    const-string v1, "sl_SI"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x4c

    goto/16 :goto_1

    :sswitch_4d
    const-string v1, "sq_AL"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x4d

    goto/16 :goto_1

    :sswitch_4e
    const-string v1, "sr_RS"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x4e

    goto/16 :goto_1

    :sswitch_4f
    const-string v1, "sv_SE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x4f

    goto/16 :goto_1

    :sswitch_50
    const-string v1, "sw_KE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x50

    goto/16 :goto_1

    :sswitch_51
    const-string v1, "ta_IN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x51

    goto/16 :goto_1

    :sswitch_52
    const-string v1, "te_IN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x52

    goto/16 :goto_1

    :sswitch_53
    const-string v1, "th_TH"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x53

    goto/16 :goto_1

    :sswitch_54
    const-string v1, "tl_PH"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x54

    goto/16 :goto_1

    :sswitch_55
    const-string v1, "tr_TR"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x55

    goto/16 :goto_1

    :sswitch_56
    const-string v1, "uk_UA"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x56

    goto/16 :goto_1

    :sswitch_57
    const-string v1, "ur_PK"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x57

    goto/16 :goto_1

    :sswitch_58
    const-string v1, "vi_VN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x58

    goto/16 :goto_1

    :sswitch_59
    const-string v1, "zh_CN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x59

    goto/16 :goto_1

    :sswitch_5a
    const-string v1, "zh_HK"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x5a

    goto/16 :goto_1

    :sswitch_5b
    const-string v1, "zh_TW"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x5b

    goto/16 :goto_1

    .line 151405
    :pswitch_0
    const-string v0, "af"

    goto/16 :goto_0

    .line 151406
    :pswitch_1
    const-string v0, "ar"

    goto/16 :goto_0

    .line 151407
    :pswitch_2
    const-string v0, "as"

    goto/16 :goto_0

    .line 151408
    :pswitch_3
    const-string v0, "az"

    goto/16 :goto_0

    .line 151409
    :pswitch_4
    const-string v0, "be"

    goto/16 :goto_0

    .line 151410
    :pswitch_5
    const-string v0, "bg"

    goto/16 :goto_0

    .line 151411
    :pswitch_6
    const-string v0, "bn"

    goto/16 :goto_0

    .line 151412
    :pswitch_7
    const-string v0, "bs"

    goto/16 :goto_0

    .line 151413
    :pswitch_8
    const-string v0, "ca"

    goto/16 :goto_0

    .line 151414
    :pswitch_9
    const-string v0, "cb"

    goto/16 :goto_0

    .line 151415
    :pswitch_a
    const-string v0, "ck"

    goto/16 :goto_0

    .line 151416
    :pswitch_b
    const-string v0, "cs"

    goto/16 :goto_0

    .line 151417
    :pswitch_c
    const-string v0, "cx"

    goto/16 :goto_0

    .line 151418
    :pswitch_d
    const-string v0, "cy"

    goto/16 :goto_0

    .line 151419
    :pswitch_e
    const-string v0, "da"

    goto/16 :goto_0

    .line 151420
    :pswitch_f
    const-string v0, "de"

    goto/16 :goto_0

    .line 151421
    :pswitch_10
    const-string v0, "el"

    goto/16 :goto_0

    .line 151422
    :pswitch_11
    const-string v0, "en_GB"

    goto/16 :goto_0

    .line 151423
    :pswitch_12
    const-string v0, "eo"

    goto/16 :goto_0

    .line 151424
    :pswitch_13
    const-string v0, "es_ES"

    goto/16 :goto_0

    .line 151425
    :pswitch_14
    const-string v0, "es"

    goto/16 :goto_0

    .line 151426
    :pswitch_15
    const-string v0, "et"

    goto/16 :goto_0

    .line 151427
    :pswitch_16
    const-string v0, "eu"

    goto/16 :goto_0

    .line 151428
    :pswitch_17
    const-string v0, "fa"

    goto/16 :goto_0

    .line 151429
    :pswitch_18
    const-string v0, "fb"

    goto/16 :goto_0

    .line 151430
    :pswitch_19
    const-string v0, "fb_LS"

    goto/16 :goto_0

    .line 151431
    :pswitch_1a
    const-string v0, "fi"

    goto/16 :goto_0

    .line 151432
    :pswitch_1b
    const-string v0, "fo"

    goto/16 :goto_0

    .line 151433
    :pswitch_1c
    const-string v0, "fr_CA"

    goto/16 :goto_0

    .line 151434
    :pswitch_1d
    const-string v0, "fr"

    goto/16 :goto_0

    .line 151435
    :pswitch_1e
    const-string v0, "fy"

    goto/16 :goto_0

    .line 151436
    :pswitch_1f
    const-string v0, "ga"

    goto/16 :goto_0

    .line 151437
    :pswitch_20
    const-string v0, "gl"

    goto/16 :goto_0

    .line 151438
    :pswitch_21
    const-string v0, "gn"

    goto/16 :goto_0

    .line 151439
    :pswitch_22
    const-string v0, "gu"

    goto/16 :goto_0

    .line 151440
    :pswitch_23
    const-string v0, "iw"

    goto/16 :goto_0

    .line 151441
    :pswitch_24
    const-string v0, "hi"

    goto/16 :goto_0

    .line 151442
    :pswitch_25
    const-string v0, "hr"

    goto/16 :goto_0

    .line 151443
    :pswitch_26
    const-string v0, "hu"

    goto/16 :goto_0

    .line 151444
    :pswitch_27
    const-string v0, "hy"

    goto/16 :goto_0

    .line 151445
    :pswitch_28
    const-string v0, "in"

    goto/16 :goto_0

    .line 151446
    :pswitch_29
    const-string v0, "is"

    goto/16 :goto_0

    .line 151447
    :pswitch_2a
    const-string v0, "it"

    goto/16 :goto_0

    .line 151448
    :pswitch_2b
    const-string v0, "ja"

    goto/16 :goto_0

    .line 151449
    :pswitch_2c
    const-string v0, "jv"

    goto/16 :goto_0

    .line 151450
    :pswitch_2d
    const-string v0, "ka"

    goto/16 :goto_0

    .line 151451
    :pswitch_2e
    const-string v0, "km"

    goto/16 :goto_0

    .line 151452
    :pswitch_2f
    const-string v0, "kn"

    goto/16 :goto_0

    .line 151453
    :pswitch_30
    const-string v0, "ko"

    goto/16 :goto_0

    .line 151454
    :pswitch_31
    const-string v0, "ku"

    goto/16 :goto_0

    .line 151455
    :pswitch_32
    const-string v0, "la"

    goto/16 :goto_0

    .line 151456
    :pswitch_33
    const-string v0, "lt"

    goto/16 :goto_0

    .line 151457
    :pswitch_34
    const-string v0, "lv"

    goto/16 :goto_0

    .line 151458
    :pswitch_35
    const-string v0, "mg"

    goto/16 :goto_0

    .line 151459
    :pswitch_36
    const-string v0, "mk"

    goto/16 :goto_0

    .line 151460
    :pswitch_37
    const-string v0, "ml"

    goto/16 :goto_0

    .line 151461
    :pswitch_38
    const-string v0, "mn"

    goto/16 :goto_0

    .line 151462
    :pswitch_39
    const-string v0, "mr"

    goto/16 :goto_0

    .line 151463
    :pswitch_3a
    const-string v0, "ms"

    goto/16 :goto_0

    .line 151464
    :pswitch_3b
    const-string v0, "my"

    goto/16 :goto_0

    .line 151465
    :pswitch_3c
    const-string v0, "nb"

    goto/16 :goto_0

    .line 151466
    :pswitch_3d
    const-string v0, "ne"

    goto/16 :goto_0

    .line 151467
    :pswitch_3e
    const-string v0, "nl"

    goto/16 :goto_0

    .line 151468
    :pswitch_3f
    const-string v0, "nn"

    goto/16 :goto_0

    .line 151469
    :pswitch_40
    const-string v0, "or"

    goto/16 :goto_0

    .line 151470
    :pswitch_41
    const-string v0, "pa"

    goto/16 :goto_0

    .line 151471
    :pswitch_42
    const-string v0, "pl"

    goto/16 :goto_0

    .line 151472
    :pswitch_43
    const-string v0, "ps"

    goto/16 :goto_0

    .line 151473
    :pswitch_44
    const-string v0, "pt"

    goto/16 :goto_0

    .line 151474
    :pswitch_45
    const-string v0, "pt_PT"

    goto/16 :goto_0

    .line 151475
    :pswitch_46
    const-string v0, "qz"

    goto/16 :goto_0

    .line 151476
    :pswitch_47
    const-string v0, "ro"

    goto/16 :goto_0

    .line 151477
    :pswitch_48
    const-string v0, "ru"

    goto/16 :goto_0

    .line 151478
    :pswitch_49
    const-string v0, "rw"

    goto/16 :goto_0

    .line 151479
    :pswitch_4a
    const-string v0, "si"

    goto/16 :goto_0

    .line 151480
    :pswitch_4b
    const-string v0, "sk"

    goto/16 :goto_0

    .line 151481
    :pswitch_4c
    const-string v0, "sl"

    goto/16 :goto_0

    .line 151482
    :pswitch_4d
    const-string v0, "sq"

    goto/16 :goto_0

    .line 151483
    :pswitch_4e
    const-string v0, "sr"

    goto/16 :goto_0

    .line 151484
    :pswitch_4f
    const-string v0, "sv"

    goto/16 :goto_0

    .line 151485
    :pswitch_50
    const-string v0, "sw"

    goto/16 :goto_0

    .line 151486
    :pswitch_51
    const-string v0, "ta"

    goto/16 :goto_0

    .line 151487
    :pswitch_52
    const-string v0, "te"

    goto/16 :goto_0

    .line 151488
    :pswitch_53
    const-string v0, "th"

    goto/16 :goto_0

    .line 151489
    :pswitch_54
    const-string v0, "tl"

    goto/16 :goto_0

    .line 151490
    :pswitch_55
    const-string v0, "tr"

    goto/16 :goto_0

    .line 151491
    :pswitch_56
    const-string v0, "uk"

    goto/16 :goto_0

    .line 151492
    :pswitch_57
    const-string v0, "ur"

    goto/16 :goto_0

    .line 151493
    :pswitch_58
    const-string v0, "vi"

    goto/16 :goto_0

    .line 151494
    :pswitch_59
    const-string v0, "zh_CN"

    goto/16 :goto_0

    .line 151495
    :pswitch_5a
    const-string v0, "zh_HK"

    goto/16 :goto_0

    .line 151496
    :pswitch_5b
    const-string v0, "zh_TW"

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x586b581 -> :sswitch_0
        0x58c26ff -> :sswitch_1
        0x58c9c52 -> :sswitch_2
        0x58fc9ff -> :sswitch_3
        0x59455d3 -> :sswitch_4
        0x5953e7f -> :sswitch_5
        0x5986df8 -> :sswitch_6
        0x59ab2ed -> :sswitch_7
        0x5a09c2f -> :sswitch_8
        0x5a11108 -> :sswitch_9
        0x5a529d5 -> :sswitch_a
        0x5a8caa6 -> :sswitch_b
        0x5ab1202 -> :sswitch_c
        0x5ab8544 -> :sswitch_d
        0x5aeb389 -> :sswitch_e
        0x5b084ff -> :sswitch_f
        0x5c1cb83 -> :sswitch_10
        0x5c2b431 -> :sswitch_11
        0x5c3285f -> :sswitch_12
        0x5c4f9df -> :sswitch_13
        0x5c4faa6 -> :sswitch_14
        0x5c56e30 -> :sswitch_15
        0x5c5e29d -> :sswitch_16
        0x5cae32d -> :sswitch_17
        0x5cb575c -> :sswitch_18
        0x5cb57ea -> :sswitch_19
        0x5ce85bf -> :sswitch_1a
        0x5d13fff -> :sswitch_1b
        0x5d29cb1 -> :sswitch_1c
        0x5d29d1f -> :sswitch_1d
        0x5d5ccaa -> :sswitch_1e
        0x5d8faa1 -> :sswitch_1f
        0x5ddfa48 -> :sswitch_20
        0x5dee461 -> :sswitch_21
        0x5e21216 -> :sswitch_22
        0x5e8e3a5 -> :sswitch_23
        0x5eab523 -> :sswitch_24
        0x5eecc5f -> :sswitch_25
        0x5f0297f -> :sswitch_26
        0x5f1fa1a -> :sswitch_27
        0x5f686bf -> :sswitch_28
        0x5fd585f -> :sswitch_29
        0x5fdccbf -> :sswitch_2a
        0x603414e -> :sswitch_2b
        0x60cccee -> :sswitch_2c
        0x6115867 -> :sswitch_2d
        0x616cd5a -> :sswitch_2e
        0x6174181 -> :sswitch_2f
        0x617b622 -> :sswitch_30
        0x61a7173 -> :sswitch_31
        0x61f71b5 -> :sswitch_32
        0x628139f -> :sswitch_33
        0x628fc5f -> :sswitch_34
        0x630425f -> :sswitch_35
        0x63213df -> :sswitch_36
        0x63287c5 -> :sswitch_37
        0x63370ff -> :sswitch_38
        0x63541ff -> :sswitch_39
        0x635b6e5 -> :sswitch_3a
        0x6387113 -> :sswitch_3b
        0x63c142c -> :sswitch_3c
        0x63d714a -> :sswitch_3d
        0x6409fdf -> :sswitch_3e
        0x64188a0 -> :sswitch_3f
        0x6517101 -> :sswitch_40
        0x657ce33 -> :sswitch_41
        0x65ccf1f -> :sswitch_42
        0x65ffbe1 -> :sswitch_43
        0x660706b -> :sswitch_44
        0x660721f -> :sswitch_45
        0x6714376 -> :sswitch_46
        0x67a5b7f -> :sswitch_47
        0x67d15bf -> :sswitch_48
        0x67dfe7f -> :sswitch_49
        0x685b808 -> :sswitch_4a
        0x686a19f -> :sswitch_4b
        0x68715fc -> :sswitch_4c
        0x68959ac -> :sswitch_4d
        0x689d021 -> :sswitch_4e
        0x68ba1ae -> :sswitch_4f
        0x68c1515 -> :sswitch_50
        0x6902c37 -> :sswitch_51
        0x691fdb3 -> :sswitch_52
        0x6935c1f -> :sswitch_53
        0x6952d1f -> :sswitch_54
        0x697e7df -> :sswitch_55
        0x6a2d0d5 -> :sswitch_56
        0x6a5fedd -> :sswitch_57
        0x6afffc4 -> :sswitch_58
        0x6e7e71c -> :sswitch_59
        0x6e7e7b4 -> :sswitch_5a
        0x6e7e934 -> :sswitch_5b
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_4a
        :pswitch_4b
        :pswitch_4c
        :pswitch_4d
        :pswitch_4e
        :pswitch_4f
        :pswitch_50
        :pswitch_51
        :pswitch_52
        :pswitch_53
        :pswitch_54
        :pswitch_55
        :pswitch_56
        :pswitch_57
        :pswitch_58
        :pswitch_59
        :pswitch_5a
        :pswitch_5b
    .end packed-switch
.end method

.method public static a(Ljava/util/Locale;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 151497
    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 151498
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 151499
    const/4 v1, 0x0

    :goto_1
    move-object v0, v1

    .line 151500
    if-nez v0, :cond_2

    .line 151501
    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 151502
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_1

    :cond_1
    :goto_2
    packed-switch v1, :pswitch_data_1

    .line 151503
    const/4 v1, 0x0

    :goto_3
    move-object v0, v1

    .line 151504
    :cond_2
    if-nez v0, :cond_3

    .line 151505
    const-string v0, "en_US"

    .line 151506
    :cond_3
    return-object v0

    .line 151507
    :sswitch_0
    const-string v2, "en_GB"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "es_ES"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "fb_LS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "fr_CA"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "pt_PT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "zh_CN"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v2, "zh_HK"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v2, "zh_TW"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    .line 151508
    :pswitch_0
    const-string v1, "en_GB"

    goto :goto_1

    .line 151509
    :pswitch_1
    const-string v1, "es_ES"

    goto :goto_1

    .line 151510
    :pswitch_2
    const-string v1, "fb_LS"

    goto :goto_1

    .line 151511
    :pswitch_3
    const-string v1, "fr_CA"

    goto :goto_1

    .line 151512
    :pswitch_4
    const-string v1, "pt_PT"

    goto :goto_1

    .line 151513
    :pswitch_5
    const-string v1, "zh_CN"

    goto :goto_1

    .line 151514
    :pswitch_6
    const-string v1, "zh_HK"

    goto :goto_1

    .line 151515
    :pswitch_7
    const-string v1, "zh_TW"

    goto :goto_1

    .line 151516
    :sswitch_8
    const-string v2, "af"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    goto :goto_2

    :sswitch_9
    const-string v2, "ar"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    goto/16 :goto_2

    :sswitch_a
    const-string v2, "as"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x2

    goto/16 :goto_2

    :sswitch_b
    const-string v2, "az"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x3

    goto/16 :goto_2

    :sswitch_c
    const-string v2, "be"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x4

    goto/16 :goto_2

    :sswitch_d
    const-string v2, "bg"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x5

    goto/16 :goto_2

    :sswitch_e
    const-string v2, "bn"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x6

    goto/16 :goto_2

    :sswitch_f
    const-string v2, "bs"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x7

    goto/16 :goto_2

    :sswitch_10
    const-string v2, "ca"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x8

    goto/16 :goto_2

    :sswitch_11
    const-string v2, "cb"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x9

    goto/16 :goto_2

    :sswitch_12
    const-string v2, "ck"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0xa

    goto/16 :goto_2

    :sswitch_13
    const-string v2, "cs"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0xb

    goto/16 :goto_2

    :sswitch_14
    const-string v2, "cx"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0xc

    goto/16 :goto_2

    :sswitch_15
    const-string v2, "cy"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0xd

    goto/16 :goto_2

    :sswitch_16
    const-string v2, "da"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0xe

    goto/16 :goto_2

    :sswitch_17
    const-string v2, "de"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0xf

    goto/16 :goto_2

    :sswitch_18
    const-string v2, "el"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x10

    goto/16 :goto_2

    :sswitch_19
    const-string v2, "eo"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x11

    goto/16 :goto_2

    :sswitch_1a
    const-string v2, "es"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x12

    goto/16 :goto_2

    :sswitch_1b
    const-string v2, "et"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x13

    goto/16 :goto_2

    :sswitch_1c
    const-string v2, "eu"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x14

    goto/16 :goto_2

    :sswitch_1d
    const-string v2, "fa"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x15

    goto/16 :goto_2

    :sswitch_1e
    const-string v2, "fb"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x16

    goto/16 :goto_2

    :sswitch_1f
    const-string v2, "fi"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x17

    goto/16 :goto_2

    :sswitch_20
    const-string v2, "fil"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x18

    goto/16 :goto_2

    :sswitch_21
    const-string v2, "fo"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x19

    goto/16 :goto_2

    :sswitch_22
    const-string v2, "fr"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x1a

    goto/16 :goto_2

    :sswitch_23
    const-string v2, "fy"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x1b

    goto/16 :goto_2

    :sswitch_24
    const-string v2, "ga"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x1c

    goto/16 :goto_2

    :sswitch_25
    const-string v2, "gl"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x1d

    goto/16 :goto_2

    :sswitch_26
    const-string v2, "gn"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x1e

    goto/16 :goto_2

    :sswitch_27
    const-string v2, "gu"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x1f

    goto/16 :goto_2

    :sswitch_28
    const-string v2, "hi"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x20

    goto/16 :goto_2

    :sswitch_29
    const-string v2, "hr"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x21

    goto/16 :goto_2

    :sswitch_2a
    const-string v2, "hu"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x22

    goto/16 :goto_2

    :sswitch_2b
    const-string v2, "hy"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x23

    goto/16 :goto_2

    :sswitch_2c
    const-string v2, "in"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x24

    goto/16 :goto_2

    :sswitch_2d
    const-string v2, "is"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x25

    goto/16 :goto_2

    :sswitch_2e
    const-string v2, "it"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x26

    goto/16 :goto_2

    :sswitch_2f
    const-string v2, "iw"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x27

    goto/16 :goto_2

    :sswitch_30
    const-string v2, "ja"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x28

    goto/16 :goto_2

    :sswitch_31
    const-string v2, "jv"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x29

    goto/16 :goto_2

    :sswitch_32
    const-string v2, "ka"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x2a

    goto/16 :goto_2

    :sswitch_33
    const-string v2, "km"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x2b

    goto/16 :goto_2

    :sswitch_34
    const-string v2, "kn"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x2c

    goto/16 :goto_2

    :sswitch_35
    const-string v2, "ko"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x2d

    goto/16 :goto_2

    :sswitch_36
    const-string v2, "ku"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x2e

    goto/16 :goto_2

    :sswitch_37
    const-string v2, "la"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x2f

    goto/16 :goto_2

    :sswitch_38
    const-string v2, "lt"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x30

    goto/16 :goto_2

    :sswitch_39
    const-string v2, "lv"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x31

    goto/16 :goto_2

    :sswitch_3a
    const-string v2, "mg"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x32

    goto/16 :goto_2

    :sswitch_3b
    const-string v2, "mk"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x33

    goto/16 :goto_2

    :sswitch_3c
    const-string v2, "ml"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x34

    goto/16 :goto_2

    :sswitch_3d
    const-string v2, "mn"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x35

    goto/16 :goto_2

    :sswitch_3e
    const-string v2, "mr"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x36

    goto/16 :goto_2

    :sswitch_3f
    const-string v2, "ms"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x37

    goto/16 :goto_2

    :sswitch_40
    const-string v2, "my"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x38

    goto/16 :goto_2

    :sswitch_41
    const-string v2, "nb"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x39

    goto/16 :goto_2

    :sswitch_42
    const-string v2, "ne"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x3a

    goto/16 :goto_2

    :sswitch_43
    const-string v2, "nl"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x3b

    goto/16 :goto_2

    :sswitch_44
    const-string v2, "nn"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x3c

    goto/16 :goto_2

    :sswitch_45
    const-string v2, "or"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x3d

    goto/16 :goto_2

    :sswitch_46
    const-string v2, "pa"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x3e

    goto/16 :goto_2

    :sswitch_47
    const-string v2, "pl"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x3f

    goto/16 :goto_2

    :sswitch_48
    const-string v2, "ps"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x40

    goto/16 :goto_2

    :sswitch_49
    const-string v2, "pt"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x41

    goto/16 :goto_2

    :sswitch_4a
    const-string v2, "qz"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x42

    goto/16 :goto_2

    :sswitch_4b
    const-string v2, "ro"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x43

    goto/16 :goto_2

    :sswitch_4c
    const-string v2, "ru"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x44

    goto/16 :goto_2

    :sswitch_4d
    const-string v2, "rw"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x45

    goto/16 :goto_2

    :sswitch_4e
    const-string v2, "si"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x46

    goto/16 :goto_2

    :sswitch_4f
    const-string v2, "sk"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x47

    goto/16 :goto_2

    :sswitch_50
    const-string v2, "sl"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x48

    goto/16 :goto_2

    :sswitch_51
    const-string v2, "sq"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x49

    goto/16 :goto_2

    :sswitch_52
    const-string v2, "sr"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x4a

    goto/16 :goto_2

    :sswitch_53
    const-string v2, "sv"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x4b

    goto/16 :goto_2

    :sswitch_54
    const-string v2, "sw"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x4c

    goto/16 :goto_2

    :sswitch_55
    const-string v2, "ta"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x4d

    goto/16 :goto_2

    :sswitch_56
    const-string v2, "te"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x4e

    goto/16 :goto_2

    :sswitch_57
    const-string v2, "th"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x4f

    goto/16 :goto_2

    :sswitch_58
    const-string v2, "tl"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x50

    goto/16 :goto_2

    :sswitch_59
    const-string v2, "tr"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x51

    goto/16 :goto_2

    :sswitch_5a
    const-string v2, "uk"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x52

    goto/16 :goto_2

    :sswitch_5b
    const-string v2, "ur"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x53

    goto/16 :goto_2

    :sswitch_5c
    const-string v2, "vi"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x54

    goto/16 :goto_2

    :sswitch_5d
    const-string v2, "zh"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x55

    goto/16 :goto_2

    .line 151517
    :pswitch_8
    const-string v1, "af_ZA"

    goto/16 :goto_3

    .line 151518
    :pswitch_9
    const-string v1, "ar_AR"

    goto/16 :goto_3

    .line 151519
    :pswitch_a
    const-string v1, "as_IN"

    goto/16 :goto_3

    .line 151520
    :pswitch_b
    const-string v1, "az_AZ"

    goto/16 :goto_3

    .line 151521
    :pswitch_c
    const-string v1, "be_BY"

    goto/16 :goto_3

    .line 151522
    :pswitch_d
    const-string v1, "bg_BG"

    goto/16 :goto_3

    .line 151523
    :pswitch_e
    const-string v1, "bn_IN"

    goto/16 :goto_3

    .line 151524
    :pswitch_f
    const-string v1, "bs_BA"

    goto/16 :goto_3

    .line 151525
    :pswitch_10
    const-string v1, "ca_ES"

    goto/16 :goto_3

    .line 151526
    :pswitch_11
    const-string v1, "cb_IQ"

    goto/16 :goto_3

    .line 151527
    :pswitch_12
    const-string v1, "ck_US"

    goto/16 :goto_3

    .line 151528
    :pswitch_13
    const-string v1, "cs_CZ"

    goto/16 :goto_3

    .line 151529
    :pswitch_14
    const-string v1, "cx_PH"

    goto/16 :goto_3

    .line 151530
    :pswitch_15
    const-string v1, "cy_GB"

    goto/16 :goto_3

    .line 151531
    :pswitch_16
    const-string v1, "da_DK"

    goto/16 :goto_3

    .line 151532
    :pswitch_17
    const-string v1, "de_DE"

    goto/16 :goto_3

    .line 151533
    :pswitch_18
    const-string v1, "el_GR"

    goto/16 :goto_3

    .line 151534
    :pswitch_19
    const-string v1, "eo_EO"

    goto/16 :goto_3

    .line 151535
    :pswitch_1a
    const-string v1, "es_LA"

    goto/16 :goto_3

    .line 151536
    :pswitch_1b
    const-string v1, "et_EE"

    goto/16 :goto_3

    .line 151537
    :pswitch_1c
    const-string v1, "eu_ES"

    goto/16 :goto_3

    .line 151538
    :pswitch_1d
    const-string v1, "fa_IR"

    goto/16 :goto_3

    .line 151539
    :pswitch_1e
    const-string v1, "fb_HA"

    goto/16 :goto_3

    .line 151540
    :pswitch_1f
    const-string v1, "fi_FI"

    goto/16 :goto_3

    .line 151541
    :pswitch_20
    const-string v1, "tl_PH"

    goto/16 :goto_3

    .line 151542
    :pswitch_21
    const-string v1, "fo_FO"

    goto/16 :goto_3

    .line 151543
    :pswitch_22
    const-string v1, "fr_FR"

    goto/16 :goto_3

    .line 151544
    :pswitch_23
    const-string v1, "fy_NL"

    goto/16 :goto_3

    .line 151545
    :pswitch_24
    const-string v1, "ga_IE"

    goto/16 :goto_3

    .line 151546
    :pswitch_25
    const-string v1, "gl_ES"

    goto/16 :goto_3

    .line 151547
    :pswitch_26
    const-string v1, "gn_PY"

    goto/16 :goto_3

    .line 151548
    :pswitch_27
    const-string v1, "gu_IN"

    goto/16 :goto_3

    .line 151549
    :pswitch_28
    const-string v1, "hi_IN"

    goto/16 :goto_3

    .line 151550
    :pswitch_29
    const-string v1, "hr_HR"

    goto/16 :goto_3

    .line 151551
    :pswitch_2a
    const-string v1, "hu_HU"

    goto/16 :goto_3

    .line 151552
    :pswitch_2b
    const-string v1, "hy_AM"

    goto/16 :goto_3

    .line 151553
    :pswitch_2c
    const-string v1, "id_ID"

    goto/16 :goto_3

    .line 151554
    :pswitch_2d
    const-string v1, "is_IS"

    goto/16 :goto_3

    .line 151555
    :pswitch_2e
    const-string v1, "it_IT"

    goto/16 :goto_3

    .line 151556
    :pswitch_2f
    const-string v1, "he_IL"

    goto/16 :goto_3

    .line 151557
    :pswitch_30
    const-string v1, "ja_JP"

    goto/16 :goto_3

    .line 151558
    :pswitch_31
    const-string v1, "jv_ID"

    goto/16 :goto_3

    .line 151559
    :pswitch_32
    const-string v1, "ka_GE"

    goto/16 :goto_3

    .line 151560
    :pswitch_33
    const-string v1, "km_KH"

    goto/16 :goto_3

    .line 151561
    :pswitch_34
    const-string v1, "kn_IN"

    goto/16 :goto_3

    .line 151562
    :pswitch_35
    const-string v1, "ko_KR"

    goto/16 :goto_3

    .line 151563
    :pswitch_36
    const-string v1, "ku_TR"

    goto/16 :goto_3

    .line 151564
    :pswitch_37
    const-string v1, "la_VA"

    goto/16 :goto_3

    .line 151565
    :pswitch_38
    const-string v1, "lt_LT"

    goto/16 :goto_3

    .line 151566
    :pswitch_39
    const-string v1, "lv_LV"

    goto/16 :goto_3

    .line 151567
    :pswitch_3a
    const-string v1, "mg_MG"

    goto/16 :goto_3

    .line 151568
    :pswitch_3b
    const-string v1, "mk_MK"

    goto/16 :goto_3

    .line 151569
    :pswitch_3c
    const-string v1, "ml_IN"

    goto/16 :goto_3

    .line 151570
    :pswitch_3d
    const-string v1, "mn_MN"

    goto/16 :goto_3

    .line 151571
    :pswitch_3e
    const-string v1, "mr_IN"

    goto/16 :goto_3

    .line 151572
    :pswitch_3f
    const-string v1, "ms_MY"

    goto/16 :goto_3

    .line 151573
    :pswitch_40
    const-string v1, "my_MM"

    goto/16 :goto_3

    .line 151574
    :pswitch_41
    const-string v1, "nb_NO"

    goto/16 :goto_3

    .line 151575
    :pswitch_42
    const-string v1, "ne_NP"

    goto/16 :goto_3

    .line 151576
    :pswitch_43
    const-string v1, "nl_NL"

    goto/16 :goto_3

    .line 151577
    :pswitch_44
    const-string v1, "nn_NO"

    goto/16 :goto_3

    .line 151578
    :pswitch_45
    const-string v1, "or_IN"

    goto/16 :goto_3

    .line 151579
    :pswitch_46
    const-string v1, "pa_IN"

    goto/16 :goto_3

    .line 151580
    :pswitch_47
    const-string v1, "pl_PL"

    goto/16 :goto_3

    .line 151581
    :pswitch_48
    const-string v1, "ps_AF"

    goto/16 :goto_3

    .line 151582
    :pswitch_49
    const-string v1, "pt_BR"

    goto/16 :goto_3

    .line 151583
    :pswitch_4a
    const-string v1, "qz_MM"

    goto/16 :goto_3

    .line 151584
    :pswitch_4b
    const-string v1, "ro_RO"

    goto/16 :goto_3

    .line 151585
    :pswitch_4c
    const-string v1, "ru_RU"

    goto/16 :goto_3

    .line 151586
    :pswitch_4d
    const-string v1, "rw_RW"

    goto/16 :goto_3

    .line 151587
    :pswitch_4e
    const-string v1, "si_LK"

    goto/16 :goto_3

    .line 151588
    :pswitch_4f
    const-string v1, "sk_SK"

    goto/16 :goto_3

    .line 151589
    :pswitch_50
    const-string v1, "sl_SI"

    goto/16 :goto_3

    .line 151590
    :pswitch_51
    const-string v1, "sq_AL"

    goto/16 :goto_3

    .line 151591
    :pswitch_52
    const-string v1, "sr_RS"

    goto/16 :goto_3

    .line 151592
    :pswitch_53
    const-string v1, "sv_SE"

    goto/16 :goto_3

    .line 151593
    :pswitch_54
    const-string v1, "sw_KE"

    goto/16 :goto_3

    .line 151594
    :pswitch_55
    const-string v1, "ta_IN"

    goto/16 :goto_3

    .line 151595
    :pswitch_56
    const-string v1, "te_IN"

    goto/16 :goto_3

    .line 151596
    :pswitch_57
    const-string v1, "th_TH"

    goto/16 :goto_3

    .line 151597
    :pswitch_58
    const-string v1, "tl_PH"

    goto/16 :goto_3

    .line 151598
    :pswitch_59
    const-string v1, "tr_TR"

    goto/16 :goto_3

    .line 151599
    :pswitch_5a
    const-string v1, "uk_UA"

    goto/16 :goto_3

    .line 151600
    :pswitch_5b
    const-string v1, "ur_PK"

    goto/16 :goto_3

    .line 151601
    :pswitch_5c
    const-string v1, "vi_VN"

    goto/16 :goto_3

    .line 151602
    :pswitch_5d
    const-string v1, "zh_CN"

    goto/16 :goto_3

    :sswitch_data_0
    .sparse-switch
        0x5c2b431 -> :sswitch_0
        0x5c4f9df -> :sswitch_1
        0x5cb57ea -> :sswitch_2
        0x5d29cb1 -> :sswitch_3
        0x660721f -> :sswitch_4
        0x6e7e71c -> :sswitch_5
        0x6e7e7b4 -> :sswitch_6
        0x6e7e934 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0xc25 -> :sswitch_8
        0xc31 -> :sswitch_9
        0xc32 -> :sswitch_a
        0xc39 -> :sswitch_b
        0xc43 -> :sswitch_c
        0xc45 -> :sswitch_d
        0xc4c -> :sswitch_e
        0xc51 -> :sswitch_f
        0xc5e -> :sswitch_10
        0xc5f -> :sswitch_11
        0xc68 -> :sswitch_12
        0xc70 -> :sswitch_13
        0xc75 -> :sswitch_14
        0xc76 -> :sswitch_15
        0xc7d -> :sswitch_16
        0xc81 -> :sswitch_17
        0xca7 -> :sswitch_18
        0xcaa -> :sswitch_19
        0xcae -> :sswitch_1a
        0xcaf -> :sswitch_1b
        0xcb0 -> :sswitch_1c
        0xcbb -> :sswitch_1d
        0xcbc -> :sswitch_1e
        0xcc3 -> :sswitch_1f
        0xcc9 -> :sswitch_21
        0xccc -> :sswitch_22
        0xcd3 -> :sswitch_23
        0xcda -> :sswitch_24
        0xce5 -> :sswitch_25
        0xce7 -> :sswitch_26
        0xcee -> :sswitch_27
        0xd01 -> :sswitch_28
        0xd0a -> :sswitch_29
        0xd0d -> :sswitch_2a
        0xd11 -> :sswitch_2b
        0xd25 -> :sswitch_2c
        0xd2a -> :sswitch_2d
        0xd2b -> :sswitch_2e
        0xd2e -> :sswitch_2f
        0xd37 -> :sswitch_30
        0xd4c -> :sswitch_31
        0xd56 -> :sswitch_32
        0xd62 -> :sswitch_33
        0xd63 -> :sswitch_34
        0xd64 -> :sswitch_35
        0xd6a -> :sswitch_36
        0xd75 -> :sswitch_37
        0xd88 -> :sswitch_38
        0xd8a -> :sswitch_39
        0xd9a -> :sswitch_3a
        0xd9e -> :sswitch_3b
        0xd9f -> :sswitch_3c
        0xda1 -> :sswitch_3d
        0xda5 -> :sswitch_3e
        0xda6 -> :sswitch_3f
        0xdac -> :sswitch_40
        0xdb4 -> :sswitch_41
        0xdb7 -> :sswitch_42
        0xdbe -> :sswitch_43
        0xdc0 -> :sswitch_44
        0xde3 -> :sswitch_45
        0xdf1 -> :sswitch_46
        0xdfc -> :sswitch_47
        0xe03 -> :sswitch_48
        0xe04 -> :sswitch_49
        0xe29 -> :sswitch_4a
        0xe3d -> :sswitch_4b
        0xe43 -> :sswitch_4c
        0xe45 -> :sswitch_4d
        0xe56 -> :sswitch_4e
        0xe58 -> :sswitch_4f
        0xe59 -> :sswitch_50
        0xe5e -> :sswitch_51
        0xe5f -> :sswitch_52
        0xe63 -> :sswitch_53
        0xe64 -> :sswitch_54
        0xe6d -> :sswitch_55
        0xe71 -> :sswitch_56
        0xe74 -> :sswitch_57
        0xe78 -> :sswitch_58
        0xe7e -> :sswitch_59
        0xe96 -> :sswitch_5a
        0xe9d -> :sswitch_5b
        0xeb3 -> :sswitch_5c
        0xf2e -> :sswitch_5d
        0x18c09 -> :sswitch_20
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_4a
        :pswitch_4b
        :pswitch_4c
        :pswitch_4d
        :pswitch_4e
        :pswitch_4f
        :pswitch_50
        :pswitch_51
        :pswitch_52
        :pswitch_53
        :pswitch_54
        :pswitch_55
        :pswitch_56
        :pswitch_57
        :pswitch_58
        :pswitch_59
        :pswitch_5a
        :pswitch_5b
        :pswitch_5c
        :pswitch_5d
    .end packed-switch
.end method
