.class public LX/0nB;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static volatile a:Z


# instance fields
.field private volatile b:Ljava/lang/Throwable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 134866
    const/4 v0, 0x0

    sput-boolean v0, LX/0nB;->a:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 134865
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0nB;Ljava/lang/String;)LX/0nB;
    .locals 3
    .param p0    # LX/0nB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 134867
    if-nez p1, :cond_0

    .line 134868
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "closer == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134869
    :cond_0
    sget-boolean v0, LX/0nB;->a:Z

    if-nez v0, :cond_1

    .line 134870
    const/4 p0, 0x0

    .line 134871
    :goto_0
    return-object p0

    .line 134872
    :cond_1
    if-nez p0, :cond_3

    .line 134873
    new-instance p0, LX/0nB;

    invoke-direct {p0}, LX/0nB;-><init>()V

    .line 134874
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Explicit termination method \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' not called"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 134875
    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1, v0}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, LX/0nB;->b:Ljava/lang/Throwable;

    goto :goto_0

    .line 134876
    :cond_3
    iget-object v0, p0, LX/0nB;->b:Ljava/lang/Throwable;

    if-eqz v0, :cond_2

    .line 134877
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "closeGuard was never released before calling open."

    iget-object v2, p0, LX/0nB;->b:Ljava/lang/Throwable;

    invoke-direct {v0, v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static a(LX/0nB;)V
    .locals 1
    .param p0    # LX/0nB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 134862
    if-eqz p0, :cond_0

    .line 134863
    const/4 v0, 0x0

    iput-object v0, p0, LX/0nB;->b:Ljava/lang/Throwable;

    .line 134864
    :cond_0
    return-void
.end method

.method public static b(LX/0nB;)V
    .locals 4
    .param p0    # LX/0nB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 134855
    if-eqz p0, :cond_0

    iget-object v0, p0, LX/0nB;->b:Ljava/lang/Throwable;

    if-nez v0, :cond_1

    .line 134856
    :cond_0
    :goto_0
    return-void

    .line 134857
    :cond_1
    const/4 v0, 0x1

    const-string v1, "A resource was acquired and never released."

    iget-object v2, p0, LX/0nB;->b:Ljava/lang/Throwable;

    .line 134858
    sget-object v3, LX/48Y;->a:LX/48W;

    iget v3, v3, LX/48W;->b:I

    and-int/2addr v3, v0

    if-nez v3, :cond_3

    .line 134859
    :cond_2
    :goto_1
    goto :goto_0

    .line 134860
    :cond_3
    sget-object v3, LX/48Y;->a:LX/48W;

    iget v3, v3, LX/48W;->b:I

    const/high16 p0, 0x10000

    and-int/2addr v3, p0

    if-eqz v3, :cond_2

    .line 134861
    const-string v3, "AppStrictMode"

    invoke-static {v3, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method


# virtual methods
.method public final finalize()V
    .locals 1

    .prologue
    .line 134851
    :try_start_0
    invoke-static {p0}, LX/0nB;->b(LX/0nB;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134852
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 134853
    return-void

    .line 134854
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method
