.class public LX/1J6;
.super LX/1J4;
.source ""

# interfaces
.implements LX/1Cf;
.implements LX/1J7;


# annotations
.annotation build Lcom/facebook/controllercallbacks/api/ControllerConfig;
.end annotation


# instance fields
.field public final a:LX/0Yi;

.field public b:LX/1UQ;

.field public c:LX/0g2;

.field public d:LX/0gC;

.field private final e:LX/0ad;

.field public f:I

.field public g:I

.field public h:I

.field private i:Z

.field public j:I

.field public k:Z

.field private l:I

.field private m:I

.field public n:Z

.field public o:I

.field public p:LX/1J8;

.field public q:Landroid/os/Handler;

.field public final r:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/0Yi;LX/0ad;Landroid/os/Handler;)V
    .locals 4
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 229780
    invoke-direct {p0}, LX/1J4;-><init>()V

    .line 229781
    new-instance v0, Lcom/facebook/feed/ui/NewsFeedListViewScrollFetcher$1;

    invoke-direct {v0, p0}, Lcom/facebook/feed/ui/NewsFeedListViewScrollFetcher$1;-><init>(LX/1J6;)V

    iput-object v0, p0, LX/1J6;->r:Ljava/lang/Runnable;

    .line 229782
    iput-object p1, p0, LX/1J6;->a:LX/0Yi;

    .line 229783
    iput-object p2, p0, LX/1J6;->e:LX/0ad;

    .line 229784
    iput-object p3, p0, LX/1J6;->q:Landroid/os/Handler;

    .line 229785
    iget-object v0, p0, LX/1J6;->e:LX/0ad;

    sget-short v1, LX/0fe;->bF:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/1J6;->i:Z

    .line 229786
    iget-object v0, p0, LX/1J6;->e:LX/0ad;

    sget v1, LX/0fe;->bK:I

    invoke-interface {v0, v1, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/1J6;->j:I

    .line 229787
    iget-object v0, p0, LX/1J6;->e:LX/0ad;

    sget-short v1, LX/0fe;->bL:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/1J6;->k:Z

    .line 229788
    iget-object v0, p0, LX/1J6;->e:LX/0ad;

    sget v1, LX/0fe;->bC:I

    const/4 v2, 0x5

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/1J6;->l:I

    .line 229789
    iget-object v0, p0, LX/1J6;->e:LX/0ad;

    sget v1, LX/0fe;->bB:I

    invoke-interface {v0, v1, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/1J6;->m:I

    .line 229790
    iget-object v0, p0, LX/1J6;->e:LX/0ad;

    sget-short v1, LX/0fe;->bH:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/1J6;->n:Z

    .line 229791
    sget-object v0, LX/1J8;->TOP_OF_FEED:LX/1J8;

    .line 229792
    iput-object v0, p0, LX/1J6;->p:LX/1J8;

    .line 229793
    return-void
.end method

.method public static a(LX/0QB;)LX/1J6;
    .locals 4

    .prologue
    .line 229777
    new-instance v3, LX/1J6;

    invoke-static {p0}, LX/0Yi;->a(LX/0QB;)LX/0Yi;

    move-result-object v0

    check-cast v0, LX/0Yi;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v2

    check-cast v2, Landroid/os/Handler;

    invoke-direct {v3, v0, v1, v2}, LX/1J6;-><init>(LX/0Yi;LX/0ad;Landroid/os/Handler;)V

    .line 229778
    move-object v0, v3

    .line 229779
    return-object v0
.end method

.method private a(LX/0g8;)V
    .locals 3

    .prologue
    .line 229687
    const-string v1, "NewsFeedListViewScrollFetcher.updateHeadFetchState"

    const v2, 0x25492678

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 229688
    :try_start_0
    instance-of v1, p1, LX/0g5;

    if-eqz v1, :cond_1

    move-object v0, p1

    check-cast v0, LX/0g5;

    move-object v1, v0

    invoke-virtual {v1}, LX/0g5;->D()I

    move-result v1

    :goto_0
    iput v1, p0, LX/1J6;->o:I

    .line 229689
    sget-object v1, LX/1fs;->a:[I

    iget-object v2, p0, LX/1J6;->p:LX/1J8;

    invoke-virtual {v2}, LX/1J8;->ordinal()I

    move-result v2

    aget v1, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v1, :pswitch_data_0

    .line 229690
    :cond_0
    :goto_1
    const v1, -0x58e5a24d

    invoke-static {v1}, LX/02m;->a(I)V

    .line 229691
    return-void

    .line 229692
    :cond_1
    const/4 v1, -0x1

    goto :goto_0

    .line 229693
    :pswitch_0
    :try_start_1
    invoke-static {p0}, LX/1J6;->h(LX/1J6;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 229694
    :catchall_0
    move-exception v1

    const v2, -0x439227a7

    invoke-static {v2}, LX/02m;->a(I)V

    throw v1

    .line 229695
    :pswitch_1
    :try_start_2
    iget v1, p0, LX/1J6;->o:I

    iget v2, p0, LX/1J6;->l:I

    if-lt v1, v2, :cond_0

    .line 229696
    sget-object v1, LX/1J8;->DOWN_ENOUGH:LX/1J8;

    .line 229697
    iput-object v1, p0, LX/1J6;->p:LX/1J8;

    .line 229698
    goto :goto_1

    .line 229699
    :pswitch_2
    iget-boolean v0, p0, LX/1J6;->k:Z

    if-eqz v0, :cond_2

    check-cast p1, LX/0g5;

    invoke-virtual {p1}, LX/0g7;->q()I

    move-result v0

    .line 229700
    :goto_2
    iget v1, p0, LX/1J6;->j:I

    if-gt v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_3
    move v1, v0

    .line 229701
    if-eqz v1, :cond_0

    .line 229702
    invoke-static {p0}, LX/1J6;->h(LX/1J6;)V

    goto :goto_1

    .line 229703
    :pswitch_3
    iget v1, p0, LX/1J6;->o:I

    if-nez v1, :cond_0

    .line 229704
    sget-object v1, LX/1J8;->TOP_OF_FEED:LX/1J8;

    .line 229705
    iput-object v1, p0, LX/1J6;->p:LX/1J8;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 229706
    goto :goto_1

    .line 229707
    :cond_2
    check-cast p1, LX/0g5;

    invoke-virtual {p1}, LX/0g5;->D()I

    move-result v0

    goto :goto_2

    .line 229708
    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private static h(LX/1J6;)V
    .locals 5

    .prologue
    .line 229771
    iget v0, p0, LX/1J6;->m:I

    if-lez v0, :cond_0

    .line 229772
    sget-object v0, LX/1J8;->TIMER_STARTED:LX/1J8;

    .line 229773
    iput-object v0, p0, LX/1J6;->p:LX/1J8;

    .line 229774
    iget-object v0, p0, LX/1J6;->q:Landroid/os/Handler;

    iget-object v1, p0, LX/1J6;->r:Ljava/lang/Runnable;

    iget v2, p0, LX/1J6;->m:I

    int-to-long v2, v2

    const v4, -0x2736aed4

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 229775
    :goto_0
    return-void

    .line 229776
    :cond_0
    iget-object v0, p0, LX/1J6;->r:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public static i(LX/1J6;)V
    .locals 3

    .prologue
    .line 229763
    iget-object v0, p0, LX/1J6;->c:LX/0g2;

    if-eqz v0, :cond_1

    .line 229764
    iget-object v0, p0, LX/1J6;->c:LX/0g2;

    .line 229765
    iget-object v1, v0, LX/0g2;->o:LX/0ad;

    sget-short v2, LX/0fe;->bD:S

    const/4 p0, 0x1

    invoke-interface {v1, v2, p0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/0gf;->SCROLL_TOP_MANUAL:LX/0gf;

    .line 229766
    :goto_0
    invoke-static {v0}, LX/0g2;->H(LX/0g2;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 229767
    sget-object v1, LX/0gf;->ONE_WAY_FEED_BACK:LX/0gf;

    .line 229768
    :cond_0
    iget-object v2, v0, LX/0g2;->b:LX/0gC;

    invoke-interface {v2, v1}, LX/0gC;->a(LX/0gf;)Z

    .line 229769
    :cond_1
    return-void

    .line 229770
    :cond_2
    sget-object v1, LX/0gf;->SCROLL_TOP:LX/0gf;

    goto :goto_0
.end method

.method private static j(LX/1J6;)Z
    .locals 3

    .prologue
    .line 229762
    iget-object v0, p0, LX/1J6;->e:LX/0ad;

    sget-short v1, LX/0fe;->aG:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method private k()V
    .locals 4

    .prologue
    .line 229746
    const-string v0, "NewsFeedListViewScrollFetcher.maybeHeadFetch"

    const v1, 0x5be8b9af

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 229747
    :try_start_0
    invoke-static {p0}, LX/1J6;->j(LX/1J6;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1J6;->b:LX/1UQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1J6;->d:LX/0gC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 229748
    :cond_0
    const v0, -0x7ed70d68

    invoke-static {v0}, LX/02m;->a(I)V

    .line 229749
    :goto_0
    return-void

    .line 229750
    :cond_1
    :try_start_1
    sget-object v0, LX/1J5;->SCROLLING_UP:LX/1J5;

    .line 229751
    iget-object v1, p0, LX/1J4;->a:LX/1J5;

    move-object v1, v1

    .line 229752
    if-ne v0, v1, :cond_3

    const/4 v0, 0x0

    .line 229753
    iget v1, p0, LX/1J6;->h:I

    if-lez v1, :cond_2

    iget v1, p0, LX/1J6;->f:I

    if-gtz v1, :cond_4

    .line 229754
    :cond_2
    :goto_1
    move v0, v0

    .line 229755
    if-eqz v0, :cond_3

    .line 229756
    invoke-static {p0}, LX/1J6;->i(LX/1J6;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 229757
    :cond_3
    const v0, -0x172f3e9a

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x72b2e34e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 229758
    :cond_4
    iget-object v1, p0, LX/1J6;->d:LX/0gC;

    invoke-interface {v1}, LX/0gC;->h()LX/0fz;

    move-result-object v1

    invoke-virtual {v1}, LX/0fz;->size()I

    move-result v1

    if-eqz v1, :cond_2

    .line 229759
    iget-object v1, p0, LX/1J6;->b:LX/1UQ;

    invoke-virtual {v1}, LX/1UQ;->b()I

    move-result v1

    iget v2, p0, LX/1J6;->g:I

    iget-object v3, p0, LX/1J6;->b:LX/1UQ;

    invoke-virtual {v3}, LX/1UQ;->c()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 229760
    iget-object v2, p0, LX/1J6;->b:LX/1UQ;

    invoke-interface {v2, v1}, LX/1Qr;->h_(I)I

    move-result v1

    .line 229761
    const/16 v2, 0x9

    if-ge v1, v2, :cond_2

    const/4 v0, 0x1

    goto :goto_1
.end method

.method private m()Z
    .locals 2

    .prologue
    .line 229745
    iget v0, p0, LX/1J6;->g:I

    iget v1, p0, LX/1J6;->f:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 229744
    const/4 v0, -0x1

    return v0
.end method

.method public final a(LX/0g8;III)V
    .locals 1

    .prologue
    .line 229735
    invoke-super {p0, p1, p2, p3, p4}, LX/1J4;->a(LX/0g8;III)V

    .line 229736
    iput p4, p0, LX/1J6;->f:I

    .line 229737
    iput p3, p0, LX/1J6;->h:I

    .line 229738
    add-int v0, p2, p3

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1J6;->g:I

    .line 229739
    iget-boolean v0, p0, LX/1J6;->i:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/1J6;->j(LX/1J6;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 229740
    invoke-direct {p0, p1}, LX/1J6;->a(LX/0g8;)V

    .line 229741
    :cond_0
    invoke-direct {p0}, LX/1J6;->k()V

    .line 229742
    invoke-virtual {p0}, LX/1J6;->d()V

    .line 229743
    return-void
.end method

.method public final d()V
    .locals 7

    .prologue
    .line 229709
    const-string v0, "NewsFeedListViewScrollFetcher.maybeTailFetch"

    const v1, -0x69503914

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 229710
    :try_start_0
    iget-object v0, p0, LX/1J6;->b:LX/1UQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1J6;->d:LX/0gC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 229711
    :cond_0
    const v0, -0x2d8e6788

    invoke-static {v0}, LX/02m;->a(I)V

    .line 229712
    :goto_0
    return-void

    .line 229713
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/1J6;->a:LX/0Yi;

    iget v1, p0, LX/1J6;->g:I

    const/4 v3, -0x1

    .line 229714
    iget v2, v0, LX/0Yi;->H:I

    if-eq v2, v3, :cond_2

    iget v2, v0, LX/0Yi;->H:I

    if-gt v2, v1, :cond_2

    .line 229715
    iput v3, v0, LX/0Yi;->H:I

    .line 229716
    iget-object v2, v0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0xa0032

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ISI)V

    .line 229717
    :cond_2
    invoke-direct {p0}, LX/1J6;->m()Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, LX/1J5;->SCROLLING_DOWN:LX/1J5;

    .line 229718
    iget-object v1, p0, LX/1J4;->a:LX/1J5;

    move-object v1, v1

    .line 229719
    if-ne v0, v1, :cond_6

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 229720
    iget v2, p0, LX/1J6;->h:I

    if-lez v2, :cond_3

    iget v2, p0, LX/1J6;->f:I

    if-gtz v2, :cond_7

    :cond_3
    move v0, v1

    .line 229721
    :cond_4
    :goto_1
    move v0, v0

    .line 229722
    if-eqz v0, :cond_6

    .line 229723
    :cond_5
    invoke-direct {p0}, LX/1J6;->m()Z

    move-result v0

    .line 229724
    iget-object v1, p0, LX/1J6;->c:LX/0g2;

    if-eqz v1, :cond_6

    .line 229725
    iget-object v1, p0, LX/1J6;->c:LX/0g2;

    iget v2, p0, LX/1J6;->f:I

    .line 229726
    sget-object p0, LX/1lo;->CACHED_ONLY:LX/1lo;

    invoke-static {v1, v0, v2, p0}, LX/0g2;->a(LX/0g2;ZILX/1lo;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 229727
    :cond_6
    const v0, 0x24cb1c7b

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x4de50ffb

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 229728
    :cond_7
    iget-object v2, p0, LX/1J6;->d:LX/0gC;

    invoke-interface {v2}, LX/0gC;->h()LX/0fz;

    move-result-object v2

    invoke-virtual {v2}, LX/0fz;->size()I

    move-result v2

    if-eqz v2, :cond_4

    .line 229729
    iget-object v2, p0, LX/1J6;->d:LX/0gC;

    invoke-interface {v2}, LX/0gC;->h()LX/0fz;

    move-result-object v2

    invoke-virtual {v2}, LX/0fz;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .line 229730
    iget-object v3, p0, LX/1J6;->d:LX/0gC;

    invoke-interface {v3}, LX/0gC;->A()I

    move-result v3

    .line 229731
    iget-object v4, p0, LX/1J6;->b:LX/1UQ;

    invoke-virtual {v4}, LX/1UQ;->b()I

    move-result v4

    iget v5, p0, LX/1J6;->g:I

    iget-object v6, p0, LX/1J6;->b:LX/1UQ;

    invoke-virtual {v6}, LX/1UQ;->c()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 229732
    iget-object v5, p0, LX/1J6;->b:LX/1UQ;

    invoke-interface {v5, v4}, LX/1Qr;->h_(I)I

    move-result v4

    .line 229733
    sub-int/2addr v2, v3

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 229734
    if-ge v4, v2, :cond_4

    move v0, v1

    goto :goto_1
.end method
