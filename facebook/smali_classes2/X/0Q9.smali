.class public abstract LX/0Q9;
.super LX/0QA;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57910
    invoke-direct {p0}, LX/0QA;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Class;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation
.end method

.method public abstract getInstance(LX/0RI;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)TT;"
        }
    .end annotation
.end method

.method public final getInstance(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 57908
    invoke-static {p1}, LX/0RI;->a(Ljava/lang/Class;)LX/0RI;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Q9;->getInstance(LX/0RI;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)TT;"
        }
    .end annotation

    .prologue
    .line 57911
    invoke-static {p1, p2}, LX/0RI;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/0RI;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Q9;->getInstance(LX/0RI;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public abstract getLazy(LX/0RI;)LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Ot",
            "<TT;>;"
        }
    .end annotation
.end method

.method public final getLazy(Ljava/lang/Class;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LX/0Ot",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57912
    invoke-static {p1}, LX/0RI;->a(Ljava/lang/Class;)LX/0RI;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Q9;->getLazy(LX/0RI;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public final getLazy(Ljava/lang/Class;Ljava/lang/Class;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0Ot",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57913
    invoke-static {p1, p2}, LX/0RI;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/0RI;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Q9;->getLazy(LX/0RI;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public final getLazySet(LX/0RI;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 57914
    invoke-static {p1}, LX/0QA;->b(LX/0RI;)LX/0RI;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Q9;->getLazy(LX/0RI;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public final getLazySet(Ljava/lang/Class;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 57915
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/0QA;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/0RI;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Q9;->getLazy(LX/0RI;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public final getLazySet(Ljava/lang/Class;Ljava/lang/Class;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 57909
    invoke-static {p1, p2}, LX/0QA;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/0RI;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Q9;->getLazy(LX/0RI;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getModuleInjector(Ljava/lang/Class;)LX/0QA;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;)",
            "LX/0QA;"
        }
    .end annotation

    .prologue
    .line 57916
    return-object p0
.end method

.method public abstract getProvider(LX/0RI;)LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation
.end method

.method public final getProvider(Ljava/lang/Class;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57899
    invoke-static {p1}, LX/0RI;->a(Ljava/lang/Class;)LX/0RI;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Q9;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public final getProvider(Ljava/lang/Class;Ljava/lang/Class;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57900
    invoke-static {p1, p2}, LX/0RI;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/0RI;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Q9;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public final getSet(LX/0RI;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "Ljava/util/Set",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57901
    invoke-static {p1}, LX/0QA;->b(LX/0RI;)LX/0RI;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Q9;->getInstance(LX/0RI;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public final getSet(Ljava/lang/Class;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/util/Set",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57902
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/0QA;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/0RI;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Q9;->getInstance(LX/0RI;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public final getSet(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "Ljava/util/Set",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57903
    invoke-static {p1, p2}, LX/0QA;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/0RI;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Q9;->getInstance(LX/0RI;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public final getSetProvider(LX/0RI;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 57904
    invoke-static {p1}, LX/0QA;->b(LX/0RI;)LX/0RI;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Q9;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public final getSetProvider(Ljava/lang/Class;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 57905
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/0QA;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/0RI;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Q9;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public final getSetProvider(Ljava/lang/Class;Ljava/lang/Class;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 57906
    invoke-static {p1, p2}, LX/0QA;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/0RI;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Q9;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public final hasBinding(Ljava/lang/Class;Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 57907
    invoke-static {p1, p2}, LX/0RI;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/0RI;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0QA;->a(LX/0RI;)Z

    move-result v0

    return v0
.end method
