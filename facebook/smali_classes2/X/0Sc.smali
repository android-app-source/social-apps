.class public final LX/0Sc;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Sc;


# instance fields
.field public final b:Ljava/lang/Throwable;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 61521
    new-instance v0, LX/0Sc;

    new-instance v1, LX/28z;

    const-string v2, "Failure occurred while trying to finish a future."

    invoke-direct {v1, v2}, LX/28z;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, LX/0Sc;-><init>(Ljava/lang/Throwable;)V

    sput-object v0, LX/0Sc;->a:LX/0Sc;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 61522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61523
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    iput-object v0, p0, LX/0Sc;->b:Ljava/lang/Throwable;

    .line 61524
    return-void
.end method
