.class public LX/1Ad;
.super LX/1Ae;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1Ae",
        "<",
        "LX/1Ad;",
        "LX/1bf;",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;",
        "Lcom/facebook/imagepipeline/image/ImageInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1HI;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1Aj;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/1Al;

.field private final f:LX/1Am;

.field public final g:LX/1Ao;

.field public h:Z

.field public i:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/1Aj;Ljava/util/Set;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/1Al;LX/1Am;LX/1Ao;)V
    .locals 0
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/IsDialtonePhotoFeatureEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/1HI;",
            ">;",
            "LX/1Aj;",
            "Ljava/util/Set",
            "<",
            "LX/1Ai;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1Al;",
            "LX/1Am;",
            "Lcom/facebook/imagepipeline/cache/CacheKeyFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 210949
    invoke-direct {p0, p1, p4}, LX/1Ae;-><init>(Landroid/content/Context;Ljava/util/Set;)V

    .line 210950
    iput-object p2, p0, LX/1Ad;->a:LX/0Ot;

    .line 210951
    iput-object p3, p0, LX/1Ad;->b:LX/1Aj;

    .line 210952
    iput-object p5, p0, LX/1Ad;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 210953
    iput-object p6, p0, LX/1Ad;->d:LX/0Or;

    .line 210954
    iput-object p7, p0, LX/1Ad;->e:LX/1Al;

    .line 210955
    iput-object p8, p0, LX/1Ad;->f:LX/1Am;

    .line 210956
    iput-object p9, p0, LX/1Ad;->g:LX/1Ao;

    .line 210957
    invoke-direct {p0}, LX/1Ad;->r()V

    .line 210958
    return-void
.end method

.method public static A(LX/1Ad;)Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;
    .locals 2

    .prologue
    .line 210931
    invoke-static {p0}, LX/1Ad;->s(LX/1Ad;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iget-boolean v1, p0, LX/1Ad;->h:Z

    .line 210932
    if-nez v0, :cond_0

    .line 210933
    const/4 p0, 0x0

    .line 210934
    :goto_0
    move-object v0, p0

    .line 210935
    return-object v0

    :cond_0
    new-instance p0, Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    invoke-direct {p0, v0, v1}, Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;-><init>(Lcom/facebook/common/callercontext/CallerContext;Z)V

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1Ad;
    .locals 1

    .prologue
    .line 210936
    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1Ad;
    .locals 10

    .prologue
    .line 210937
    new-instance v0, LX/1Ad;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0xb99

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const-class v3, LX/1Aj;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1Aj;

    .line 210938
    new-instance v4, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance v6, LX/1Ak;

    invoke-direct {v6, p0}, LX/1Ak;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, v6}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v4

    .line 210939
    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v6, 0x1488

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const-class v7, LX/1Al;

    invoke-interface {p0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/1Al;

    const-class v8, LX/1Am;

    invoke-interface {p0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/1Am;

    invoke-static {p0}, LX/1An;->a(LX/0QB;)LX/1An;

    move-result-object v9

    check-cast v9, LX/1Ao;

    invoke-direct/range {v0 .. v9}, LX/1Ad;-><init>(Landroid/content/Context;LX/0Ot;LX/1Aj;Ljava/util/Set;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/1Al;LX/1Am;LX/1Ao;)V

    .line 210940
    return-object v0
.end method

.method private f(Ljava/lang/Object;)LX/1Ad;
    .locals 2

    .prologue
    .line 210941
    instance-of v0, p1, Lcom/facebook/common/callercontext/CallerContext;

    const-string v1, "callerContext must be instance of CallerContext"

    invoke-static {v0, v1}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 210942
    invoke-super {p0, p1}, LX/1Ae;->b(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    return-object v0
.end method

.method private r()V
    .locals 1

    .prologue
    .line 210943
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Ad;->h:Z

    .line 210944
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Ad;->i:Landroid/graphics/drawable/Drawable;

    .line 210945
    return-void
.end method

.method public static s(LX/1Ad;)Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 210960
    invoke-super {p0}, LX/1Ae;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;
    .locals 1

    .prologue
    .line 210946
    invoke-super {p0, p1}, LX/1Ae;->b(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    return-object v0
.end method

.method public final synthetic a(Landroid/net/Uri;)LX/1Af;
    .locals 1
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 210947
    invoke-virtual {p0, p1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)LX/1Af;
    .locals 1

    .prologue
    .line 210948
    invoke-direct {p0, p1}, LX/1Ad;->f(Ljava/lang/Object;)LX/1Ad;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1bj;)LX/1Gd;
    .locals 3

    .prologue
    .line 210919
    check-cast p1, LX/1bf;

    .line 210920
    invoke-static {p0}, LX/1Ad;->s(LX/1Ad;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    .line 210921
    iget-object v0, p0, LX/1Ad;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HI;

    invoke-static {p2}, LX/1bk;->a(LX/1bj;)LX/1bY;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, LX/1HI;->a(LX/1bf;Ljava/lang/Object;LX/1bY;)LX/1Gd;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a()LX/1aZ;
    .locals 1

    .prologue
    .line 210959
    invoke-virtual {p0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;LX/1bj;)LX/1ca;
    .locals 1

    .prologue
    .line 210922
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(Landroid/net/Uri;)LX/1Ad;
    .locals 2
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 210923
    if-nez p1, :cond_0

    .line 210924
    const/4 v0, 0x0

    invoke-super {p0, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 210925
    :goto_0
    return-object v0

    .line 210926
    :cond_0
    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-static {}, LX/1bd;->c()LX/1bd;

    move-result-object v1

    .line 210927
    iput-object v1, v0, LX/1bX;->d:LX/1bd;

    .line 210928
    move-object v0, v0

    .line 210929
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 210930
    invoke-super {p0, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)LX/1Ad;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 210833
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 210834
    :cond_0
    invoke-static {p1}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    invoke-super {p0, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 210835
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic b()LX/1Ae;
    .locals 1

    .prologue
    .line 210836
    invoke-virtual {p0}, LX/1Ad;->o()LX/1Ad;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/1Ae;
    .locals 1

    .prologue
    .line 210837
    invoke-direct {p0, p1}, LX/1Ad;->f(Ljava/lang/Object;)LX/1Ad;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 210838
    invoke-static {p0}, LX/1Ad;->s(LX/1Ad;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/1bp;
    .locals 1

    .prologue
    .line 210839
    invoke-virtual {p0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    return-object v0
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 210840
    invoke-static {p0}, LX/1Ad;->s(LX/1Ad;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    const-string v1, "CallerContext not specified!"

    invoke-static {v0, v1}, LX/03g;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210841
    invoke-super {p0}, LX/1Ae;->i()V

    .line 210842
    return-void
.end method

.method public final j()LX/1bp;
    .locals 8

    .prologue
    .line 210843
    iget-object v0, p0, LX/1Ad;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 210844
    const/4 v6, 0x0

    .line 210845
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "res:///"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v2, 0x7f0203c7

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v1, v1

    .line 210846
    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    .line 210847
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/1Ae;->e(Ljava/lang/Object;)LX/1Gd;

    move-result-object v3

    .line 210848
    sget-object v4, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 210849
    iget-object v1, p0, LX/1Ae;->f:Ljava/lang/Object;

    move-object v1, v1

    .line 210850
    if-eqz v1, :cond_4

    .line 210851
    iget-object v1, p0, LX/1Ae;->f:Ljava/lang/Object;

    move-object v1, v1

    .line 210852
    check-cast v1, LX/1bf;

    .line 210853
    iget-object v2, v1, LX/1bf;->b:Landroid/net/Uri;

    move-object v4, v2

    .line 210854
    :cond_0
    :goto_0
    iget-object v1, p0, LX/1Ae;->q:LX/1aZ;

    move-object v1, v1

    .line 210855
    instance-of v1, v1, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    if-eqz v1, :cond_6

    .line 210856
    iget-object v1, p0, LX/1Ae;->q:LX/1aZ;

    move-object v1, v1

    .line 210857
    check-cast v1, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    .line 210858
    invoke-virtual {p0}, LX/1Ad;->l()LX/1Gd;

    move-result-object v2

    invoke-static {}, LX/1Ae;->k()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0}, LX/1Ad;->A(LX/1Ad;)Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    move-result-object v7

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->a(LX/1Gd;LX/1Gd;Landroid/net/Uri;Ljava/lang/String;LX/1bh;Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;)V

    .line 210859
    :goto_1
    invoke-virtual {p0, v1}, LX/1Ae;->c(LX/1bp;)V

    .line 210860
    invoke-virtual {p0, v1}, LX/1Ae;->b(LX/1bp;)V

    .line 210861
    invoke-virtual {p0, v1}, LX/1Ae;->a(LX/1bp;)V

    .line 210862
    move-object v0, v1

    .line 210863
    :goto_2
    return-object v0

    .line 210864
    :cond_1
    invoke-super {p0}, LX/1Ae;->j()LX/1bp;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    .line 210865
    iget-object v1, p0, LX/1Ad;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0eJ;->p:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 210866
    const/4 v1, 0x1

    .line 210867
    iput-boolean v1, v0, LX/1bo;->h:Z

    .line 210868
    :cond_2
    iget-boolean v1, p0, LX/1Ad;->h:Z

    if-eqz v1, :cond_3

    .line 210869
    invoke-virtual {p0, v0}, LX/1Ae;->c(LX/1bp;)V

    .line 210870
    iget-object v1, p0, LX/1Ad;->i:Landroid/graphics/drawable/Drawable;

    .line 210871
    iput-object v1, v0, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->d:Landroid/graphics/drawable/Drawable;

    .line 210872
    :cond_3
    move-object v0, v0

    .line 210873
    goto :goto_2

    .line 210874
    :cond_4
    iget-object v1, p0, LX/1Ae;->g:Ljava/lang/Object;

    move-object v1, v1

    .line 210875
    if-eqz v1, :cond_5

    .line 210876
    iget-object v1, p0, LX/1Ae;->g:Ljava/lang/Object;

    move-object v1, v1

    .line 210877
    check-cast v1, LX/1bf;

    .line 210878
    iget-object v2, v1, LX/1bf;->b:Landroid/net/Uri;

    move-object v4, v2

    .line 210879
    goto :goto_0

    .line 210880
    :cond_5
    iget-object v1, p0, LX/1Ae;->h:[Ljava/lang/Object;

    move-object v1, v1

    .line 210881
    if-eqz v1, :cond_0

    .line 210882
    iget-object v1, p0, LX/1Ae;->h:[Ljava/lang/Object;

    move-object v1, v1

    .line 210883
    check-cast v1, [LX/1bf;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    .line 210884
    iget-object v2, v1, LX/1bf;->b:Landroid/net/Uri;

    move-object v4, v2

    .line 210885
    goto :goto_0

    .line 210886
    :cond_6
    iget-object v1, p0, LX/1Ad;->e:LX/1Al;

    invoke-virtual {p0}, LX/1Ad;->l()LX/1Gd;

    move-result-object v2

    invoke-static {}, LX/1Ae;->k()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0}, LX/1Ad;->A(LX/1Ad;)Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    move-result-object v7

    invoke-virtual/range {v1 .. v7}, LX/1Al;->a(LX/1Gd;LX/1Gd;Landroid/net/Uri;Ljava/lang/String;LX/1bh;Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;)Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    move-result-object v1

    goto :goto_1
.end method

.method public final l()LX/1Gd;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 210887
    iget-boolean v0, p0, LX/1Ad;->h:Z

    if-eqz v0, :cond_0

    .line 210888
    iget-object v2, p0, LX/1Ad;->f:LX/1Am;

    .line 210889
    iget-object v0, p0, LX/1Ae;->f:Ljava/lang/Object;

    move-object v0, v0

    .line 210890
    check-cast v0, LX/1bf;

    .line 210891
    iget-object v1, p0, LX/1Ae;->g:Ljava/lang/Object;

    move-object v1, v1

    .line 210892
    check-cast v1, LX/1bf;

    invoke-static {p0}, LX/1Ad;->s(LX/1Ad;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    .line 210893
    new-instance v4, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;

    const/16 p0, 0xb99

    invoke-static {v2, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, p0, v0, v1, v3}, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;-><init>(LX/0Ot;LX/1bf;LX/1bf;Ljava/lang/Object;)V

    .line 210894
    move-object v0, v4

    .line 210895
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, LX/1Ae;->l()LX/1Gd;

    move-result-object v0

    goto :goto_0
.end method

.method public final m()LX/1bp;
    .locals 5

    .prologue
    .line 210896
    iget-object v0, p0, LX/1Ae;->q:LX/1aZ;

    move-object v0, v0

    .line 210897
    iget-object v1, p0, LX/1Ae;->f:Ljava/lang/Object;

    move-object v1, v1

    .line 210898
    check-cast v1, LX/1bf;

    .line 210899
    const/4 v2, 0x0

    .line 210900
    iget-object v3, p0, LX/1Ad;->g:LX/1Ao;

    if-eqz v3, :cond_2

    if-eqz v1, :cond_2

    .line 210901
    invoke-static {p0}, LX/1Ad;->s(LX/1Ad;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    .line 210902
    iget-object v3, v1, LX/1bf;->m:LX/33B;

    move-object v3, v3

    .line 210903
    if-eqz v3, :cond_1

    .line 210904
    iget-object v3, p0, LX/1Ad;->g:LX/1Ao;

    invoke-virtual {v3, v1, v2}, LX/1Ao;->b(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v1

    .line 210905
    :goto_0
    move-object v1, v1

    .line 210906
    instance-of v2, v0, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    if-eqz v2, :cond_0

    .line 210907
    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    .line 210908
    invoke-virtual {p0}, LX/1Ad;->l()LX/1Gd;

    move-result-object v2

    invoke-static {}, LX/1Ae;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, LX/1Ad;->A(LX/1Ad;)Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, LX/1bo;->a(LX/1Gd;Ljava/lang/String;LX/1bh;Ljava/lang/Object;)V

    .line 210909
    :goto_1
    return-object v0

    .line 210910
    :cond_0
    iget-object v0, p0, LX/1Ad;->b:LX/1Aj;

    invoke-virtual {p0}, LX/1Ad;->l()LX/1Gd;

    move-result-object v2

    invoke-static {}, LX/1Ae;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, LX/1Ad;->A(LX/1Ad;)Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, LX/1Aj;->a(LX/1Gd;Ljava/lang/String;LX/1bh;Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;)Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    goto :goto_1

    .line 210911
    :cond_1
    iget-object v3, p0, LX/1Ad;->g:LX/1Ao;

    invoke-virtual {v3, v1, v2}, LX/1Ao;->a(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v1

    goto :goto_0

    :cond_2
    move-object v1, v2

    goto :goto_0
.end method

.method public final synthetic n()LX/1Ae;
    .locals 1

    .prologue
    .line 210912
    move-object v0, p0

    .line 210913
    return-object v0
.end method

.method public final o()LX/1Ad;
    .locals 1

    .prologue
    .line 210914
    invoke-direct {p0}, LX/1Ad;->r()V

    .line 210915
    invoke-super {p0}, LX/1Ae;->b()LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    return-object v0
.end method

.method public final p()LX/1Ad;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 210916
    move-object v0, p0

    .line 210917
    return-object v0
.end method

.method public final q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;
    .locals 1

    .prologue
    .line 210918
    invoke-super {p0}, LX/1Ae;->h()LX/1bp;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    return-object v0
.end method
