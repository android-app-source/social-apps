.class public LX/0v6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0zO",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0zO;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0zO;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/11X;

.field private final f:LX/0v7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0v7",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:LX/0vU;

.field public k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0zO;",
            "LX/0v7;",
            ">;"
        }
    .end annotation
.end field

.field public final n:I

.field public o:LX/0vV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vV",
            "<",
            "LX/0zO;",
            "LX/0zO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 157564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157565
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0v6;->a:Ljava/util/Map;

    .line 157566
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0v6;->b:Ljava/util/List;

    .line 157567
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/0v6;->d:Ljava/util/Set;

    .line 157568
    invoke-static {}, LX/0v7;->d()LX/0v7;

    move-result-object v0

    iput-object v0, p0, LX/0v6;->f:LX/0v7;

    .line 157569
    iput-boolean v1, p0, LX/0v6;->h:Z

    .line 157570
    iput-boolean v1, p0, LX/0v6;->i:Z

    .line 157571
    sget-object v0, LX/0vU;->UNSPECIFIED:LX/0vU;

    iput-object v0, p0, LX/0v6;->j:LX/0vU;

    .line 157572
    const/4 v0, 0x0

    iput-object v0, p0, LX/0v6;->k:LX/0Px;

    .line 157573
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0v6;->l:Z

    .line 157574
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0v6;->m:Ljava/util/Map;

    .line 157575
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v0

    iput-object v0, p0, LX/0v6;->o:LX/0vV;

    .line 157576
    iput-object p1, p0, LX/0v6;->c:Ljava/lang/String;

    .line 157577
    invoke-static {}, LX/0tX;->c()I

    move-result v0

    iput v0, p0, LX/0v6;->n:I

    .line 157578
    return-void
.end method

.method private f(LX/0zO;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 157545
    iget-object v0, p0, LX/0v6;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157546
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t add two of the same request to a batch request."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157547
    :cond_0
    iget-object v0, p1, LX/0zO;->a:LX/0zS;

    move-object v0, v0

    .line 157548
    iget-boolean v0, v0, LX/0zS;->i:Z

    if-nez v0, :cond_1

    .line 157549
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Batch request is a network level optimization, it doesnot make sense to have a request to have a no network cache policy."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157550
    :cond_1
    iget-object v0, p1, LX/0zO;->m:LX/0gW;

    move-object v0, v0

    .line 157551
    invoke-virtual {v0}, LX/0gW;->u()Z

    move-result v0

    if-nez v0, :cond_2

    .line 157552
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0v6;->l:Z

    .line 157553
    :cond_2
    const/4 v0, 0x1

    .line 157554
    invoke-virtual {p1}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v1

    .line 157555
    :goto_0
    iget-object v2, p0, LX/0v6;->a:Ljava/util/Map;

    invoke-virtual {p1}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 157556
    add-int/lit8 v0, v0, 0x1

    .line 157557
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 157558
    iput-object v2, p1, LX/0zO;->i:Ljava/lang/String;

    .line 157559
    goto :goto_0

    .line 157560
    :cond_3
    iget-object v0, p0, LX/0v6;->a:Ljava/util/Map;

    invoke-virtual {p1}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157561
    iget-object v0, p0, LX/0v6;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157562
    invoke-direct {p0, p1}, LX/0v6;->g(LX/0zO;)V

    .line 157563
    return-void
.end method

.method private g(LX/0zO;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 157522
    const/4 v3, 0x0

    .line 157523
    iget-object v0, p1, LX/0zO;->m:LX/0gW;

    move-object v0, v0

    .line 157524
    if-nez v0, :cond_0

    .line 157525
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Got request with no query."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157526
    :cond_0
    invoke-virtual {p1}, LX/0zO;->d()LX/0w7;

    move-result-object v0

    invoke-virtual {v0}, LX/0w7;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 157527
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 157528
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4a1;

    .line 157529
    iget-object v2, v0, LX/4a1;->a:LX/0zO;

    check-cast v2, LX/0zO;

    .line 157530
    iget-object v5, p0, LX/0v6;->a:Ljava/util/Map;

    invoke-virtual {v2}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 157531
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Dependent request \'"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' is not in this batch."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157532
    :cond_1
    iget-object v5, p0, LX/0v6;->a:Ljava/util/Map;

    iget-object v6, v0, LX/4a1;->a:LX/0zO;

    invoke-virtual {v6}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eq v5, v2, :cond_2

    .line 157533
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Depends on a request with the same name "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but is not in batch "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157534
    :cond_2
    invoke-virtual {v2}, LX/0zO;->l()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 157535
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Depending on a query with an \'each\' fan out style is not supported."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157536
    :cond_3
    invoke-virtual {p1}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, LX/4a1;->a:LX/0zO;

    invoke-virtual {v6}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 157537
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A query cannot depend on itself."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157538
    :cond_4
    iget-object v0, v0, LX/4a1;->c:LX/4Zz;

    sget-object v5, LX/4Zz;->EACH:LX/4Zz;

    if-ne v0, v5, :cond_5

    .line 157539
    if-eqz v3, :cond_6

    .line 157540
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "You can only have one ref param using the \'each\' fan out style. "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " both use \'each\' fan out style."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move-object v1, v3

    .line 157541
    :cond_6
    iget-object v0, p0, LX/0v6;->o:LX/0vV;

    invoke-virtual {v0, p1, v2}, LX/0Xs;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 157542
    iget-object v0, p0, LX/0v6;->o:LX/0vV;

    invoke-virtual {v0, v2, p1}, LX/0Xs;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-object v3, v1

    .line 157543
    goto/16 :goto_0

    .line 157544
    :cond_7
    return-void
.end method

.method private h(LX/0zO;)LX/0v7;
    .locals 1
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 157518
    iget-object v0, p0, LX/0v6;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v7;

    .line 157519
    if-nez v0, :cond_0

    .line 157520
    iget-object v0, p0, LX/0v6;->f:LX/0v7;

    .line 157521
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)I
    .locals 4

    .prologue
    .line 157492
    const/4 v0, 0x0

    .line 157493
    iget-object v1, p0, LX/0v6;->m:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 157494
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0zO;

    invoke-virtual {p0, v1}, LX/0v6;->d(LX/0zO;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 157495
    add-int/lit8 v2, v2, 0x1

    .line 157496
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v7;

    invoke-virtual {v0, p1}, LX/0v7;->a(Ljava/lang/Throwable;)V

    :cond_0
    move v0, v2

    move v2, v0

    .line 157497
    goto :goto_0

    .line 157498
    :cond_1
    return v2
.end method

.method public final a(LX/0zO;)LX/0zX;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;)",
            "LX/0zX",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 157514
    invoke-direct {p0, p1}, LX/0v6;->f(LX/0zO;)V

    .line 157515
    invoke-static {}, LX/0v7;->d()LX/0v7;

    move-result-object v0

    .line 157516
    iget-object v1, p0, LX/0v6;->m:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157517
    new-instance v1, LX/0zX;

    invoke-direct {v1, v0}, LX/0zX;-><init>(LX/0v9;)V

    return-object v1
.end method

.method public final a(LX/0vU;)V
    .locals 0

    .prologue
    .line 157579
    iput-object p1, p0, LX/0v6;->j:LX/0vU;

    .line 157580
    return-void
.end method

.method public final a(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 1

    .prologue
    .line 157512
    invoke-direct {p0, p1}, LX/0v6;->h(LX/0zO;)LX/0v7;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0v7;->a(Ljava/lang/Object;)V

    .line 157513
    return-void
.end method

.method public final a(LX/0zO;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 157510
    invoke-direct {p0, p1}, LX/0v6;->h(LX/0zO;)LX/0v7;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0v7;->a(Ljava/lang/Throwable;)V

    .line 157511
    return-void
.end method

.method public final b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 157504
    invoke-virtual {p1}, LX/0zO;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157505
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot add a request with an \'EACH\' fan out style using futures. Use GraphQLRequest.addRequestObserve instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157506
    :cond_0
    invoke-virtual {p0, p1}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v0

    .line 157507
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    .line 157508
    new-instance v2, LX/3TZ;

    invoke-direct {v2, p0, v1}, LX/3TZ;-><init>(LX/0v6;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v0, v2}, LX/0zX;->a(LX/0rl;)LX/0zi;

    .line 157509
    return-object v1
.end method

.method public final d(LX/0zO;)Z
    .locals 1

    .prologue
    .line 157503
    iget-object v0, p0, LX/0v6;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final e()LX/11X;
    .locals 1
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 157502
    iget-object v0, p0, LX/0v6;->e:LX/11X;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11X;

    return-object v0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 157499
    iget-object v0, p0, LX/0v6;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v7;

    .line 157500
    invoke-virtual {v0}, LX/0v7;->R_()V

    goto :goto_0

    .line 157501
    :cond_0
    return-void
.end method
