.class public LX/138;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/03V;

.field private b:Ljava/lang/StringBuilder;

.field public c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/03V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 176025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176026
    iput-object p1, p0, LX/138;->a:LX/03V;

    .line 176027
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/138;->c:Ljava/util/Set;

    .line 176028
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/138;->b:Ljava/lang/StringBuilder;

    .line 176029
    return-void
.end method

.method public static a(LX/138;)V
    .locals 3

    .prologue
    .line 176023
    iget-object v0, p0, LX/138;->a:LX/03V;

    const-string v1, "SearchAwareness"

    iget-object v2, p0, LX/138;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 176024
    return-void
.end method

.method public static a(LX/138;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 176030
    iget-object v0, p0, LX/138;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176031
    return-void
.end method

.method public static a(LX/138;LX/A0Z;)Z
    .locals 4

    .prologue
    .line 175988
    sget-object v0, LX/Fax;->a:[I

    invoke-interface {p1}, LX/A0Z;->e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 175989
    const-string v0, "Unsupported template.\n"

    invoke-static {p0, v0}, LX/138;->a(LX/138;Ljava/lang/String;)V

    .line 175990
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 175991
    :pswitch_0
    const/4 v0, 0x1

    .line 175992
    invoke-interface {p1}, LX/A0Z;->fQ_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, LX/A0Z;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 175993
    const-string v0, "Both title and description are empty for tooltip unit.\n"

    invoke-static {p0, v0}, LX/138;->a(LX/138;Ljava/lang/String;)V

    .line 175994
    const/4 v0, 0x0

    .line 175995
    :cond_0
    move v0, v0

    .line 175996
    goto :goto_0

    .line 175997
    :pswitch_1
    const/4 v0, 0x1

    .line 175998
    invoke-interface {p1}, LX/A0Z;->fQ_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, LX/A0Z;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 175999
    const-string v0, "Both title and description are empty for tooltip unit.\n"

    invoke-static {p0, v0}, LX/138;->a(LX/138;Ljava/lang/String;)V

    .line 176000
    const/4 v0, 0x0

    .line 176001
    :cond_1
    move v0, v0

    .line 176002
    goto :goto_0

    .line 176003
    :pswitch_2
    const/4 v1, 0x0

    .line 176004
    const/4 v0, 0x1

    .line 176005
    invoke-interface {p1}, LX/A0Z;->fQ_()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 176006
    const-string v0, "Missing title for null state interstitial unit.\n"

    invoke-static {p0, v0}, LX/138;->a(LX/138;Ljava/lang/String;)V

    move v0, v1

    .line 176007
    :cond_2
    invoke-interface {p1}, LX/A0Z;->c()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, LX/A0Z;->c()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x3

    if-le v2, v3, :cond_4

    .line 176008
    :cond_3
    const-string v0, "Invalid number of keyword suggestions for null state interstitial unit.\n"

    invoke-static {p0, v0}, LX/138;->a(LX/138;Ljava/lang/String;)V

    move v0, v1

    .line 176009
    :cond_4
    move v0, v0

    .line 176010
    goto :goto_0

    .line 176011
    :pswitch_3
    const/4 v0, 0x1

    .line 176012
    invoke-interface {p1}, LX/A0Z;->fQ_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 176013
    const-string v0, "Missing title for search results awareness unit.\n"

    invoke-static {p0, v0}, LX/138;->a(LX/138;Ljava/lang/String;)V

    .line 176014
    const/4 v0, 0x0

    .line 176015
    :cond_5
    move v0, v0

    .line 176016
    goto :goto_0

    .line 176017
    :pswitch_4
    const/4 v0, 0x1

    .line 176018
    invoke-interface {p1}, LX/A0Z;->fQ_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p1}, LX/A0Z;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 176019
    const-string v0, "Both title and description are empty for typeahead tooltip unit.\n"

    invoke-static {p0, v0}, LX/138;->a(LX/138;Ljava/lang/String;)V

    .line 176020
    const/4 v0, 0x0

    .line 176021
    :cond_6
    move v0, v0

    .line 176022
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static b(LX/138;)V
    .locals 1

    .prologue
    .line 175985
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/138;->b:Ljava/lang/StringBuilder;

    .line 175986
    iget-object v0, p0, LX/138;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 175987
    return-void
.end method
