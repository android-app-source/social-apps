.class public final LX/1KE;
.super LX/1KF;
.source ""


# instance fields
.field public final synthetic a:LX/1KA;


# direct methods
.method public constructor <init>(LX/1KA;)V
    .locals 0

    .prologue
    .line 231407
    iput-object p1, p0, LX/1KE;->a:LX/1KA;

    invoke-direct {p0}, LX/1KF;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 231404
    iget-object v0, p0, LX/1KE;->a:LX/1KA;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 231405
    iput-wide v2, v0, LX/1KA;->h:J

    .line 231406
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 231396
    iget-object v0, p0, LX/1KE;->a:LX/1KA;

    iget-object v0, v0, LX/1KA;->c:LX/198;

    invoke-virtual {v0}, LX/198;->a()LX/1YD;

    move-result-object v0

    .line 231397
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/1dj;

    if-nez v1, :cond_1

    .line 231398
    :cond_0
    :goto_0
    return-void

    .line 231399
    :cond_1
    check-cast v0, LX/1dj;

    .line 231400
    iget v1, v0, LX/1dj;->b:I

    move v0, v1

    .line 231401
    iget-object v1, p0, LX/1KE;->a:LX/1KA;

    iget-object v1, v1, LX/1KA;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    .line 231402
    add-int/2addr v1, p1

    .line 231403
    iget-object v2, p0, LX/1KE;->a:LX/1KA;

    iget-object v2, v2, LX/1KA;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0, v1}, Landroid/util/SparseIntArray;->put(II)V

    goto :goto_0
.end method

.method public final a(LX/0oG;)V
    .locals 6

    .prologue
    .line 231375
    iget-object v0, p0, LX/1KE;->a:LX/1KA;

    iget-wide v0, v0, LX/1KA;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 231376
    :goto_0
    iget-object v0, p0, LX/1KE;->a:LX/1KA;

    iget-object v0, v0, LX/1KA;->e:Landroid/util/SparseIntArray;

    const/4 v1, 0x0

    .line 231377
    const/4 v2, -0x1

    .line 231378
    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v5

    move v4, v1

    move v3, v1

    move v1, v2

    :goto_1
    if-ge v4, v5, :cond_0

    .line 231379
    invoke-virtual {v0, v4}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v2

    .line 231380
    if-le v2, v3, :cond_4

    .line 231381
    invoke-virtual {v0, v4}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v1

    .line 231382
    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    goto :goto_1

    .line 231383
    :cond_0
    move v0, v1

    .line 231384
    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    .line 231385
    :cond_1
    :goto_3
    return-void

    .line 231386
    :cond_2
    iget-object v0, p0, LX/1KE;->a:LX/1KA;

    iget-object v0, v0, LX/1KA;->d:Landroid/content/res/Resources;

    iget-object v1, p0, LX/1KE;->a:LX/1KA;

    iget v1, v1, LX/1KA;->g:I

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->b(Landroid/content/res/Resources;F)I

    move-result v0

    mul-int/lit16 v0, v0, 0x2710

    int-to-long v0, v0

    iget-object v2, p0, LX/1KE;->a:LX/1KA;

    iget-wide v2, v2, LX/1KA;->i:J

    div-long/2addr v0, v2

    .line 231387
    const-string v2, "avg_scroll_speed"

    invoke-virtual {p1, v2, v0, v1}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    goto :goto_0

    .line 231388
    :cond_3
    iget-object v1, p0, LX/1KE;->a:LX/1KA;

    iget-object v1, v1, LX/1KA;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    .line 231389
    if-eqz v1, :cond_1

    .line 231390
    iget-object v1, p0, LX/1KE;->a:LX/1KA;

    iget-object v1, v1, LX/1KA;->b:LX/1KB;

    invoke-virtual {v1, v0}, LX/1KB;->a(I)LX/1Cz;

    move-result-object v0

    .line 231391
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getEnclosingClass()Ljava/lang/Class;

    move-result-object v1

    .line 231392
    if-nez v1, :cond_5

    .line 231393
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    .line 231394
    :goto_4
    move-object v0, v1

    .line 231395
    const-string v1, "worst_row_key"

    invoke-virtual {p1, v1, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    goto :goto_3

    :cond_4
    move v2, v3

    goto :goto_2

    :cond_5
    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    goto :goto_4
.end method

.method public final b()V
    .locals 9

    .prologue
    .line 231364
    iget-object v0, p0, LX/1KE;->a:LX/1KA;

    iget-object v1, p0, LX/1KE;->a:LX/1KA;

    iget v1, v1, LX/1KA;->f:I

    .line 231365
    iget v2, v0, LX/1KA;->g:I

    add-int/2addr v2, v1

    iput v2, v0, LX/1KA;->g:I

    .line 231366
    iget-object v0, p0, LX/1KE;->a:LX/1KA;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-object v1, p0, LX/1KE;->a:LX/1KA;

    iget-wide v4, v1, LX/1KA;->h:J

    sub-long/2addr v2, v4

    .line 231367
    iget-wide v6, v0, LX/1KA;->i:J

    add-long/2addr v6, v2

    iput-wide v6, v0, LX/1KA;->i:J

    .line 231368
    return-void
.end method

.method public final d()V
    .locals 5

    .prologue
    .line 231369
    iget-object v0, p0, LX/1KE;->a:LX/1KA;

    iget-object v0, v0, LX/1KA;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 231370
    iget-object v0, p0, LX/1KE;->a:LX/1KA;

    const/4 v1, 0x0

    .line 231371
    iput v1, v0, LX/1KA;->g:I

    .line 231372
    iget-object v0, p0, LX/1KE;->a:LX/1KA;

    const-wide/16 v2, 0x0

    .line 231373
    iput-wide v2, v0, LX/1KA;->i:J

    .line 231374
    return-void
.end method
