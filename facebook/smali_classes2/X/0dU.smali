.class public LX/0dU;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final A:LX/0Tn;

.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;

.field public static final o:LX/0Tn;

.field public static final p:LX/0Tn;

.field public static final q:LX/0Tn;

.field public static final r:LX/0Tn;

.field public static final s:LX/0Tn;

.field public static final t:LX/0Tn;

.field public static final u:LX/0Tn;

.field public static final v:LX/0Tn;

.field public static final w:LX/0Tn;

.field public static final x:LX/0Tn;

.field public static final y:LX/0Tn;

.field public static final z:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 90376
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "http/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 90377
    sput-object v0, LX/0dU;->a:LX/0Tn;

    const-string v1, "php_profiling"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->b:LX/0Tn;

    .line 90378
    sget-object v0, LX/0dU;->a:LX/0Tn;

    const-string v1, "wirehog_profiling"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->c:LX/0Tn;

    .line 90379
    sget-object v0, LX/0dU;->a:LX/0Tn;

    const-string v1, "teak_profiling"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->d:LX/0Tn;

    .line 90380
    sget-object v0, LX/0dU;->a:LX/0Tn;

    const-string v1, "fbtrace"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->e:LX/0Tn;

    .line 90381
    sget-object v0, LX/0dU;->a:LX/0Tn;

    const-string v1, "artillery"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->f:LX/0Tn;

    .line 90382
    sget-object v0, LX/0dU;->a:LX/0Tn;

    const-string v1, "debug_show_queue"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->g:LX/0Tn;

    .line 90383
    sget-object v0, LX/0dU;->a:LX/0Tn;

    const-string v1, "log_http_queue_events"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->h:LX/0Tn;

    .line 90384
    sget-object v0, LX/0dU;->a:LX/0Tn;

    const-string v1, "carrier_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->i:LX/0Tn;

    .line 90385
    sget-object v0, LX/0dU;->a:LX/0Tn;

    const-string v1, "check_certs"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->j:LX/0Tn;

    .line 90386
    sget-object v0, LX/0dU;->a:LX/0Tn;

    const-string v1, "user_certs"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->k:LX/0Tn;

    .line 90387
    sget-object v0, LX/0dU;->a:LX/0Tn;

    const-string v1, "http_proxy"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->l:LX/0Tn;

    .line 90388
    sget-object v0, LX/0dU;->a:LX/0Tn;

    const-string v1, "empathy"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->m:LX/0Tn;

    .line 90389
    sget-object v0, LX/0dU;->a:LX/0Tn;

    const-string v1, "liger_trace_event"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->n:LX/0Tn;

    .line 90390
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "sandbox/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 90391
    sput-object v0, LX/0dU;->o:LX/0Tn;

    const-string v1, "web/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 90392
    sput-object v0, LX/0dU;->p:LX/0Tn;

    const-string v1, "server_tier"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->q:LX/0Tn;

    .line 90393
    sget-object v0, LX/0dU;->p:LX/0Tn;

    const-string v1, "sandbox"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->r:LX/0Tn;

    .line 90394
    sget-object v0, LX/0dU;->p:LX/0Tn;

    const-string v1, "weinre"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->s:LX/0Tn;

    .line 90395
    sget-object v0, LX/0dU;->p:LX/0Tn;

    const-string v1, "upload"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->t:LX/0Tn;

    .line 90396
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "fetch_alerts/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 90397
    sput-object v0, LX/0dU;->u:LX/0Tn;

    const-string v1, "fetch_thread_list"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->v:LX/0Tn;

    .line 90398
    sget-object v0, LX/0dU;->u:LX/0Tn;

    const-string v1, "fetch_more_threads"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->w:LX/0Tn;

    .line 90399
    sget-object v0, LX/0dU;->u:LX/0Tn;

    const-string v1, "fetch_thread"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->x:LX/0Tn;

    .line 90400
    sget-object v0, LX/0dU;->u:LX/0Tn;

    const-string v1, "fetch_multiple_threads"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->y:LX/0Tn;

    .line 90401
    sget-object v0, LX/0dU;->u:LX/0Tn;

    const-string v1, "fetch_group_threads"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->z:LX/0Tn;

    .line 90402
    sget-object v0, LX/0dU;->u:LX/0Tn;

    const-string v1, "fetch_more_messages"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dU;->A:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 90403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
