.class public LX/1Kj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Pq;

.field public static final b:LX/0Pq;

.field private static volatile h:LX/1Kj;


# instance fields
.field public final c:Lcom/facebook/performancelogger/PerformanceLogger;

.field public final d:LX/11i;

.field public final e:LX/0id;

.field public final f:Landroid/content/Context;

.field public g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 233108
    new-instance v0, LX/1Kk;

    const v1, 0xe0001

    const-string v2, "LaunchTextComposerSequence"

    const-string v3, "com.facebook.composer.activity.ComposerFragment"

    const-string v4, "com.facebook.composer.activity.ComposerActivity"

    invoke-static {v3, v4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v3

    invoke-direct {v0, v1, v2, v5, v3}, LX/1Kk;-><init>(ILjava/lang/String;ZLX/0Rf;)V

    sput-object v0, LX/1Kj;->a:LX/0Pq;

    .line 233109
    new-instance v0, LX/1Kl;

    const v1, 0xe0007

    const-string v2, "ComposerPostDrawPhase"

    const-string v3, "com.facebook.composer.activity.ComposerFragment"

    const-string v4, "com.facebook.composer.activity.ComposerActivity"

    invoke-static {v3, v4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v3

    invoke-direct {v0, v1, v2, v5, v3}, LX/1Kl;-><init>(ILjava/lang/String;ZLX/0Rf;)V

    sput-object v0, LX/1Kj;->b:LX/0Pq;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;LX/11i;LX/0id;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 233102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233103
    iput-object p1, p0, LX/1Kj;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 233104
    iput-object p2, p0, LX/1Kj;->d:LX/11i;

    .line 233105
    iput-object p3, p0, LX/1Kj;->e:LX/0id;

    .line 233106
    iput-object p4, p0, LX/1Kj;->f:Landroid/content/Context;

    .line 233107
    return-void
.end method

.method public static a(LX/0QB;)LX/1Kj;
    .locals 7

    .prologue
    .line 233089
    sget-object v0, LX/1Kj;->h:LX/1Kj;

    if-nez v0, :cond_1

    .line 233090
    const-class v1, LX/1Kj;

    monitor-enter v1

    .line 233091
    :try_start_0
    sget-object v0, LX/1Kj;->h:LX/1Kj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 233092
    if-eqz v2, :cond_0

    .line 233093
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 233094
    new-instance p0, LX/1Kj;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v4

    check-cast v4, LX/11i;

    invoke-static {v0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v5

    check-cast v5, LX/0id;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1Kj;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;LX/11i;LX/0id;Landroid/content/Context;)V

    .line 233095
    move-object v0, p0

    .line 233096
    sput-object v0, LX/1Kj;->h:LX/1Kj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 233097
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 233098
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 233099
    :cond_1
    sget-object v0, LX/1Kj;->h:LX/1Kj;

    return-object v0

    .line 233100
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 233101
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 233084
    iget-boolean v0, p0, LX/1Kj;->g:Z

    const-string v1, "Can\'t start a TTI marker before or after the composer launch phase"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 233085
    iget-object v0, p0, LX/1Kj;->d:LX/11i;

    sget-object v1, LX/1Kj;->a:LX/0Pq;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 233086
    if-eqz v0, :cond_0

    .line 233087
    const v1, 0x37231c64

    invoke-static {v0, p1, v1}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 233088
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 233081
    iget-object v0, p0, LX/1Kj;->e:LX/0id;

    invoke-virtual {v0, p1, p2}, LX/0id;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 233082
    iget-object v0, p0, LX/1Kj;->d:LX/11i;

    sget-object v1, LX/1Kj;->a:LX/0Pq;

    invoke-interface {v0, v1}, LX/11i;->a(LX/0Pq;)LX/11o;

    move-result-object v0

    const-string v1, "RefHandOff"

    const v2, 0x5684ea99

    invoke-static {v0, v1, v2}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 233083
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 233065
    iget-boolean v0, p0, LX/1Kj;->g:Z

    const-string v1, "Can\'t stop a TTI marker before or after the composer launch phase"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 233066
    iget-object v0, p0, LX/1Kj;->d:LX/11i;

    sget-object v1, LX/1Kj;->a:LX/0Pq;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 233067
    if-eqz v0, :cond_0

    .line 233068
    const v1, 0x9faed69

    invoke-static {v0, p1, v1}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 233069
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 233078
    iget-object v0, p0, LX/1Kj;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xe0008

    const-string v2, "ComposerActionButtonPressed"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 233079
    iget-object v0, p0, LX/1Kj;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xe0009

    const-string v2, "ComposerSelectedPrivacyAvailable"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 233080
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 233074
    iget-object v0, p0, LX/1Kj;->d:LX/11i;

    sget-object v1, LX/1Kj;->b:LX/0Pq;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 233075
    if-eqz v0, :cond_0

    .line 233076
    const v1, -0x735cce9c

    invoke-static {v0, p1, v1}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 233077
    :cond_0
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 233070
    iget-object v0, p0, LX/1Kj;->d:LX/11i;

    sget-object v1, LX/1Kj;->b:LX/0Pq;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 233071
    if-eqz v0, :cond_0

    .line 233072
    const v1, 0x1139748

    invoke-static {v0, p1, v1}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 233073
    :cond_0
    return-void
.end method
