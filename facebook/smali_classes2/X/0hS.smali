.class public final LX/0hS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/0h8;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0hR;

.field public final synthetic b:LX/0em;


# direct methods
.method public constructor <init>(LX/0em;LX/0hR;)V
    .locals 0

    .prologue
    .line 117668
    iput-object p1, p0, LX/0hS;->b:LX/0em;

    iput-object p2, p0, LX/0hS;->a:LX/0hR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 10

    .prologue
    .line 117669
    check-cast p1, LX/0h8;

    check-cast p2, LX/0h8;

    const-wide/16 v8, 0x0

    const/4 v0, 0x1

    const/4 v1, -0x1

    .line 117670
    iget-object v2, p0, LX/0hS;->a:LX/0hR;

    sget-object v3, LX/0hR;->ASCENDING:LX/0hR;

    if-ne v2, v3, :cond_1

    move v2, v0

    .line 117671
    :goto_0
    if-nez p1, :cond_2

    .line 117672
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 117673
    goto :goto_0

    .line 117674
    :cond_2
    if-nez p2, :cond_3

    move v0, v1

    goto :goto_1

    .line 117675
    :cond_3
    iget-wide v4, p1, LX/0h8;->c:J

    .line 117676
    cmp-long v3, v4, v8

    if-lez v3, :cond_0

    .line 117677
    iget-wide v6, p2, LX/0h8;->c:J

    .line 117678
    cmp-long v0, v6, v8

    if-gtz v0, :cond_4

    move v0, v1

    goto :goto_1

    .line 117679
    :cond_4
    cmp-long v0, v4, v6

    if-gez v0, :cond_5

    neg-int v0, v2

    goto :goto_1

    .line 117680
    :cond_5
    cmp-long v0, v4, v6

    if-lez v0, :cond_6

    move v0, v2

    goto :goto_1

    .line 117681
    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method
