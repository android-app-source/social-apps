.class public final LX/0bR;
.super LX/0b1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b1",
        "<",
        "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
        "LX/8Mf;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0b3;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0b3;",
            "LX/0Ot",
            "<",
            "LX/8Mf;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 86954
    invoke-direct {p0, p1, p2}, LX/0b1;-><init>(LX/0b4;LX/0Ot;)V

    .line 86955
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86956
    const-class v0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    return-object v0
.end method

.method public final a(LX/0b7;Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 86957
    check-cast p1, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    check-cast p2, LX/8Mf;

    .line 86958
    iget-object v1, p2, LX/8Mf;->c:LX/7m8;

    sget-object v2, LX/7m7;->SUCCESS:LX/7m7;

    .line 86959
    iget-object v0, p1, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->a:Ljava/lang/String;

    move-object v3, v0

    .line 86960
    iget-object v0, p1, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->c:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v4, v0

    .line 86961
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 86962
    iget-object v5, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v5, v5

    .line 86963
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 86964
    iget-wide v9, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v6, v9

    .line 86965
    new-instance v0, LX/2rc;

    invoke-direct {v0}, LX/2rc;-><init>()V

    const/4 v8, 0x0

    .line 86966
    iput-boolean v8, v0, LX/2rc;->a:Z

    .line 86967
    move-object v0, v0

    .line 86968
    invoke-virtual {v0}, LX/2rc;->a()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v8

    invoke-virtual/range {v1 .. v8}, LX/7m8;->a(LX/7m7;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;JLcom/facebook/composer/publish/common/ErrorDetails;)V

    .line 86969
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 86970
    iget-object v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 86971
    iget-object v0, p2, LX/8Mf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7md;

    .line 86972
    iget-object v2, p1, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->a:Ljava/lang/String;

    move-object v2, v2

    .line 86973
    iget-object v3, p1, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->c:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v3, v3

    .line 86974
    invoke-virtual {v0, v1, v2, v3}, LX/7md;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 86975
    iget-object v0, p2, LX/8Mf;->a:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->b(Ljava/lang/String;)V

    .line 86976
    return-void
.end method
