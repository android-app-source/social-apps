.class public LX/0fP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static t:LX/0Xm;


# instance fields
.field public final b:LX/0Sh;

.field public final c:Landroid/view/inputmethod/InputMethodManager;

.field private final d:LX/0Zb;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hI;",
            ">;"
        }
    .end annotation
.end field

.field public f:Landroid/app/Activity;

.field public g:Landroid/widget/FrameLayout;

.field public h:LX/A7T;

.field public i:LX/A7N;

.field public j:LX/A7O;

.field public k:LX/A7O;

.field public l:LX/A7Q;

.field public m:Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;

.field public n:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public o:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/ui/drawers/DrawerAnimationStateListener;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/A7R;

.field public q:LX/10e;

.field public r:Z

.field public s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 107966
    const-class v0, LX/0fP;

    sput-object v0, LX/0fP;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;LX/0Sh;Landroid/view/inputmethod/InputMethodManager;LX/0Zb;LX/0Ot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Landroid/view/inputmethod/InputMethodManager;",
            "LX/0Zb;",
            "LX/0Ot",
            "<",
            "LX/0hI;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 107967
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107968
    iput-object v1, p0, LX/0fP;->f:Landroid/app/Activity;

    .line 107969
    iput-object v1, p0, LX/0fP;->g:Landroid/widget/FrameLayout;

    .line 107970
    iput-object v1, p0, LX/0fP;->h:LX/A7T;

    .line 107971
    iput-object v1, p0, LX/0fP;->j:LX/A7O;

    .line 107972
    iput-object v1, p0, LX/0fP;->k:LX/A7O;

    .line 107973
    iput-object v1, p0, LX/0fP;->l:LX/A7Q;

    .line 107974
    iput-object v1, p0, LX/0fP;->m:Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;

    .line 107975
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/0fP;->n:Ljava/util/Set;

    .line 107976
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/0fP;->o:Ljava/util/Set;

    .line 107977
    iput-object v1, p0, LX/0fP;->p:LX/A7R;

    .line 107978
    sget-object v0, LX/10e;->CLOSED:LX/10e;

    iput-object v0, p0, LX/0fP;->q:LX/10e;

    .line 107979
    iput-boolean v2, p0, LX/0fP;->r:Z

    .line 107980
    iput-boolean v2, p0, LX/0fP;->s:Z

    .line 107981
    iput-object p2, p0, LX/0fP;->b:LX/0Sh;

    .line 107982
    iput-object p3, p0, LX/0fP;->c:Landroid/view/inputmethod/InputMethodManager;

    .line 107983
    iput-object p4, p0, LX/0fP;->d:LX/0Zb;

    .line 107984
    iput-object p5, p0, LX/0fP;->e:LX/0Ot;

    .line 107985
    instance-of v0, p1, LX/0ew;

    if-eqz v0, :cond_1

    .line 107986
    iput-object p1, p0, LX/0fP;->f:Landroid/app/Activity;

    .line 107987
    :cond_0
    :goto_0
    return-void

    .line 107988
    :cond_1
    if-eqz p1, :cond_0

    .line 107989
    sget-object v0, LX/0fP;->a:Ljava/lang/Class;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DrawerController created with unknown activity type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/0fP;
    .locals 9

    .prologue
    .line 107990
    const-class v1, LX/0fP;

    monitor-enter v1

    .line 107991
    :try_start_0
    sget-object v0, LX/0fP;->t:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 107992
    sput-object v2, LX/0fP;->t:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 107993
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107994
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 107995
    new-instance v3, LX/0fP;

    invoke-static {v0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {v0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v6

    check-cast v6, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    const/16 v8, 0x12be

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/0fP;-><init>(Landroid/app/Activity;LX/0Sh;Landroid/view/inputmethod/InputMethodManager;LX/0Zb;LX/0Ot;)V

    .line 107996
    move-object v0, v3

    .line 107997
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 107998
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/0fP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 107999
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 108000
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/0fP;LX/10b;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/10b;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108001
    const/4 v0, 0x0

    .line 108002
    sget-object v1, LX/A7M;->a:[I

    invoke-virtual {p1}, LX/10b;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 108003
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 108004
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Tried to open an invalid drawer."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108005
    :pswitch_0
    iget-object v1, p0, LX/0fP;->j:LX/A7O;

    if-eqz v1, :cond_0

    .line 108006
    sget-object v0, LX/10e;->SHOWING_LEFT:LX/10e;

    goto :goto_0

    .line 108007
    :pswitch_1
    iget-object v1, p0, LX/0fP;->k:LX/A7O;

    if-eqz v1, :cond_0

    .line 108008
    sget-object v0, LX/10e;->SHOWING_RIGHT:LX/10e;

    goto :goto_0

    .line 108009
    :cond_1
    const/4 v1, 0x1

    invoke-direct {p0, v1}, LX/0fP;->d(Z)V

    .line 108010
    invoke-direct {p0, v0, p2}, LX/0fP;->a(LX/10e;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(LX/10e;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/10e;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 108011
    iget-object v0, p0, LX/0fP;->p:LX/A7R;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0fP;->p:LX/A7R;

    .line 108012
    iget-object v2, v0, LX/A7R;->a:LX/10e;

    move-object v0, v2

    .line 108013
    if-ne v0, p1, :cond_0

    .line 108014
    iget-object v0, p0, LX/0fP;->p:LX/A7R;

    .line 108015
    iget-object v1, v0, LX/A7R;->b:Lcom/google/common/util/concurrent/SettableFuture;

    move-object v0, v1

    .line 108016
    :goto_0
    return-object v0

    .line 108017
    :cond_0
    invoke-static {p0, p1}, LX/0fP;->c(LX/0fP;LX/10e;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108018
    iget-boolean v0, p0, LX/0fP;->r:Z

    move v0, v0

    .line 108019
    if-nez v0, :cond_1

    .line 108020
    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 108021
    :cond_1
    iget-object v0, p0, LX/0fP;->p:LX/A7R;

    if-eqz v0, :cond_2

    .line 108022
    iget-object v0, p0, LX/0fP;->p:LX/A7R;

    invoke-virtual {v0}, LX/A7R;->c()V

    .line 108023
    iput-object v1, p0, LX/0fP;->p:LX/A7R;

    .line 108024
    :cond_2
    new-instance v0, LX/A7R;

    invoke-direct {v0, p1}, LX/A7R;-><init>(LX/10e;)V

    .line 108025
    iput-object v0, p0, LX/0fP;->p:LX/A7R;

    .line 108026
    iget-object v1, p0, LX/0fP;->h:LX/A7T;

    invoke-virtual {v1, p1, p2}, LX/A7T;->a(LX/10e;Z)V

    .line 108027
    iget-object v1, p0, LX/0fP;->m:Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/0fP;->l:LX/A7Q;

    if-eqz v1, :cond_3

    invoke-static {p0}, LX/0fP;->k(LX/0fP;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 108028
    iget-object v1, p0, LX/0fP;->l:LX/A7Q;

    .line 108029
    invoke-static {v1}, LX/A7Q;->g(LX/A7Q;)Landroid/os/Bundle;

    move-result-object v2

    .line 108030
    const-string p0, "drawer_state"

    invoke-virtual {v2, p0, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 108031
    :cond_3
    iget-object v1, v0, LX/A7R;->b:Lcom/google/common/util/concurrent/SettableFuture;

    move-object v0, v1

    .line 108032
    goto :goto_0
.end method

.method public static a(LX/0fP;LX/0fM;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 108126
    iget-object v0, p0, LX/0fP;->j:LX/A7O;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0fP;->j:LX/A7O;

    invoke-virtual {v0, p1}, LX/A7O;->a(LX/0fM;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108127
    sget-object v0, LX/10e;->SHOWING_LEFT:LX/10e;

    invoke-static {p0, v0}, LX/0fP;->c(LX/0fP;LX/10e;)Z

    move-result v0

    move v0, v0

    .line 108128
    if-eqz v0, :cond_0

    .line 108129
    invoke-virtual {p0, v2}, LX/0fP;->a(Z)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 108130
    :cond_0
    iget-object v0, p0, LX/0fP;->h:LX/A7T;

    invoke-virtual {v0, v2}, LX/A7T;->setLeftDrawerWidth(I)V

    .line 108131
    iget-object v0, p0, LX/0fP;->j:LX/A7O;

    .line 108132
    iput-object v1, p0, LX/0fP;->j:LX/A7O;

    .line 108133
    :goto_0
    if-nez v0, :cond_3

    .line 108134
    :goto_1
    return-void

    .line 108135
    :cond_1
    iget-object v0, p0, LX/0fP;->k:LX/A7O;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/0fP;->k:LX/A7O;

    invoke-virtual {v0, p1}, LX/A7O;->a(LX/0fM;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 108136
    sget-object v0, LX/10e;->SHOWING_RIGHT:LX/10e;

    invoke-static {p0, v0}, LX/0fP;->c(LX/0fP;LX/10e;)Z

    move-result v0

    move v0, v0

    .line 108137
    if-eqz v0, :cond_2

    .line 108138
    invoke-virtual {p0, v2}, LX/0fP;->a(Z)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 108139
    :cond_2
    iget-object v0, p0, LX/0fP;->h:LX/A7T;

    invoke-virtual {v0, v2}, LX/A7T;->setRightDrawerWidth(I)V

    .line 108140
    iget-object v0, p0, LX/0fP;->k:LX/A7O;

    .line 108141
    iput-object v1, p0, LX/0fP;->k:LX/A7O;

    goto :goto_0

    .line 108142
    :cond_3
    const/4 v2, 0x0

    .line 108143
    iget-object v1, v0, LX/A7O;->c:Lcom/facebook/widget/CustomFrameLayout;

    invoke-static {v1}, LX/A7O;->a(Landroid/view/View;)V

    .line 108144
    iget-object v1, v0, LX/A7O;->g:Landroid/view/View;

    invoke-static {v1}, LX/A7O;->a(Landroid/view/View;)V

    .line 108145
    iput-object v2, v0, LX/A7O;->c:Lcom/facebook/widget/CustomFrameLayout;

    .line 108146
    iput-object v2, v0, LX/A7O;->g:Landroid/view/View;

    .line 108147
    iput-object v2, v0, LX/A7O;->d:LX/0fM;

    .line 108148
    const/4 v1, -0x1

    iput v1, v0, LX/A7O;->e:I

    .line 108149
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/A7O;->i:Z

    .line 108150
    invoke-virtual {p1, p0}, LX/0fM;->b(LX/0fP;)V

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(LX/0fP;LX/A7N;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 108033
    invoke-virtual {p0}, LX/0fP;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108034
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This DrawerController is already attached to an activity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108035
    :cond_0
    iget-object v0, p0, LX/0fP;->f:Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 108036
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This DrawerController was injected without an Activity Context. So it cannot be attached."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108037
    :cond_1
    const-string v0, "BackgroundStrategy cannot be null"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108038
    iget-object v0, p0, LX/0fP;->f:Landroid/app/Activity;

    invoke-static {v0}, LX/18w;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 108039
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_2

    move v1, v2

    :goto_0
    const-string v4, "Called DrawerController.attachToActivity before Activity.setContentView"

    invoke-static {v1, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 108040
    new-instance v1, LX/A7T;

    iget-object v4, p0, LX/0fP;->f:Landroid/app/Activity;

    invoke-direct {v1, v4}, LX/A7T;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/0fP;->h:LX/A7T;

    .line 108041
    iget-object v1, p0, LX/0fP;->h:LX/A7T;

    const v4, 0x7f0d01bc

    invoke-virtual {v1, v4}, LX/A7T;->setId(I)V

    .line 108042
    iget-object v1, p0, LX/0fP;->h:LX/A7T;

    .line 108043
    iget-object v4, v1, LX/A7T;->i:Ljava/util/Set;

    invoke-interface {v4, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 108044
    :goto_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_3

    .line 108045
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 108046
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 108047
    iget-object v4, p0, LX/0fP;->h:LX/A7T;

    invoke-virtual {v4, v1}, LX/A7T;->addView(Landroid/view/View;)V

    goto :goto_1

    :cond_2
    move v1, v3

    .line 108048
    goto :goto_0

    .line 108049
    :cond_3
    iput-object p1, p0, LX/0fP;->i:LX/A7N;

    .line 108050
    instance-of v1, v0, Landroid/widget/FrameLayout;

    if-eqz v1, :cond_5

    .line 108051
    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/0fP;->g:Landroid/widget/FrameLayout;

    .line 108052
    :cond_4
    :goto_2
    iget-object v0, p0, LX/0fP;->g:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/0fP;->h:LX/A7T;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 108053
    invoke-virtual {p0}, LX/0fP;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 108054
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "attached to activity "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0fP;->f:Landroid/app/Activity;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108055
    return-void

    .line 108056
    :cond_5
    new-instance v1, Landroid/widget/FrameLayout;

    iget-object v3, p0, LX/0fP;->f:Landroid/app/Activity;

    invoke-direct {v1, v3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/0fP;->g:Landroid/widget/FrameLayout;

    .line 108057
    iget-object v1, p0, LX/0fP;->g:Landroid/widget/FrameLayout;

    const v3, 0x7f0d01b9

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setId(I)V

    .line 108058
    iget-object v1, p0, LX/0fP;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 108059
    iget-object v1, p0, LX/0fP;->d:LX/0Zb;

    const-string v3, "unknown_activity_root"

    invoke-interface {v1, v3, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 108060
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 108061
    const-string v2, "drawer_controller"

    invoke-virtual {v1, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 108062
    const-string v2, "activity_root_view_type"

    invoke-virtual {v0}, Landroid/view/ViewGroup;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 108063
    invoke-virtual {v1}, LX/0oG;->d()V

    goto :goto_2

    .line 108064
    :cond_6
    iget-object v0, p0, LX/0fP;->l:LX/A7Q;

    if-nez v0, :cond_7

    .line 108065
    new-instance v0, LX/A7Q;

    invoke-direct {v0, p0}, LX/A7Q;-><init>(LX/0fP;)V

    iput-object v0, p0, LX/0fP;->l:LX/A7Q;

    .line 108066
    :cond_7
    invoke-virtual {p0}, LX/0fP;->d()LX/0gc;

    move-result-object v0

    iget-object v1, p0, LX/0fP;->l:LX/A7Q;

    const-string v2, "drawers:fragment:lifecycle"

    invoke-static {v0, v1, v2}, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->a(LX/0gc;LX/A7P;Ljava/lang/String;)Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;

    move-result-object v0

    iput-object v0, p0, LX/0fP;->m:Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;

    goto :goto_3
.end method

.method public static a$redex0(LX/0fP;Landroid/view/View;Landroid/view/View;Ljava/lang/Integer;)V
    .locals 4
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 108067
    invoke-virtual {p2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 108068
    :cond_0
    :goto_0
    return-void

    .line 108069
    :cond_1
    if-eqz p3, :cond_2

    .line 108070
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 108071
    :cond_2
    iget-object v0, p0, LX/0fP;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x1010054

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 108072
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 108073
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 108074
    if-nez v1, :cond_3

    .line 108075
    const v0, 0x7f0a0723

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 108076
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_4

    .line 108077
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 108078
    :cond_4
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static b(LX/0fP;LX/10e;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 108079
    iget-object v3, p0, LX/0fP;->q:LX/10e;

    .line 108080
    iget-boolean v4, p0, LX/0fP;->r:Z

    .line 108081
    if-eq v3, p1, :cond_3

    move v2, v0

    .line 108082
    :goto_0
    if-eq v4, p2, :cond_4

    .line 108083
    :goto_1
    if-eqz v2, :cond_0

    .line 108084
    iput-object p1, p0, LX/0fP;->q:LX/10e;

    .line 108085
    :cond_0
    if-eqz v0, :cond_8

    .line 108086
    iput-boolean p2, p0, LX/0fP;->r:Z

    .line 108087
    if-eqz p2, :cond_5

    .line 108088
    invoke-virtual {p0}, LX/0fP;->b()Z

    move-result v0

    if-nez v0, :cond_e

    .line 108089
    :goto_2
    iget-object v0, p0, LX/0fP;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10p;

    .line 108090
    iget-object v2, p0, LX/0fP;->q:LX/10e;

    .line 108091
    iget-object v4, v0, LX/10p;->c:LX/10e;

    if-eq v3, v4, :cond_1

    iget-object v4, v0, LX/10p;->c:LX/10e;

    if-ne v2, v4, :cond_f

    .line 108092
    :cond_1
    :goto_4
    goto :goto_3

    .line 108093
    :cond_2
    :goto_5
    return-void

    :cond_3
    move v2, v1

    .line 108094
    goto :goto_0

    :cond_4
    move v0, v1

    .line 108095
    goto :goto_1

    .line 108096
    :cond_5
    iget-object v0, p0, LX/0fP;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10p;

    .line 108097
    iget-object v2, p0, LX/0fP;->q:LX/10e;

    .line 108098
    iget-object v3, v0, LX/10p;->c:LX/10e;

    if-eq v2, v3, :cond_6

    iget-boolean v3, v0, LX/10p;->d:Z

    if-nez v3, :cond_10

    .line 108099
    :cond_6
    :goto_7
    goto :goto_6

    .line 108100
    :cond_7
    goto :goto_5

    .line 108101
    :cond_8
    if-eqz v2, :cond_2

    .line 108102
    if-eqz p2, :cond_b

    .line 108103
    iget-object v0, p0, LX/0fP;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10p;

    .line 108104
    iget-object v2, p0, LX/0fP;->q:LX/10e;

    .line 108105
    iget-boolean v3, v0, LX/10p;->d:Z

    if-eqz v3, :cond_11

    iget-object v3, v0, LX/10p;->c:LX/10e;

    if-ne v2, v3, :cond_11

    .line 108106
    const/4 v3, 0x0

    iput-boolean v3, v0, LX/10p;->d:Z

    .line 108107
    sget-object v3, LX/A7U;->CLOSED:LX/A7U;

    invoke-virtual {v0, v3}, LX/10p;->b(LX/A7U;)V

    .line 108108
    :cond_9
    :goto_9
    goto :goto_8

    .line 108109
    :cond_a
    goto :goto_5

    .line 108110
    :cond_b
    iget-object v0, p0, LX/0fP;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10p;

    .line 108111
    iget-object v2, p0, LX/0fP;->q:LX/10e;

    .line 108112
    const/4 v4, 0x0

    iput-boolean v4, v0, LX/10p;->d:Z

    .line 108113
    iget-object v4, v0, LX/10p;->c:LX/10e;

    if-eq v3, v4, :cond_c

    iget-object v4, v0, LX/10p;->c:LX/10e;

    if-ne v2, v4, :cond_12

    .line 108114
    :cond_c
    :goto_b
    goto :goto_a

    .line 108115
    :cond_d
    goto :goto_5

    .line 108116
    :cond_e
    iget-object v0, p0, LX/0fP;->c:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, LX/0fP;->f:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto/16 :goto_2

    .line 108117
    :cond_f
    const/4 v4, 0x1

    iput-boolean v4, v0, LX/10p;->d:Z

    .line 108118
    invoke-static {v3}, LX/10p;->c(LX/10e;)LX/A7U;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/10p;->a(LX/A7U;)V

    goto/16 :goto_4

    .line 108119
    :cond_10
    const/4 v3, 0x0

    iput-boolean v3, v0, LX/10p;->d:Z

    .line 108120
    invoke-static {v2}, LX/10p;->c(LX/10e;)LX/A7U;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/10p;->b(LX/A7U;)V

    goto :goto_7

    .line 108121
    :cond_11
    iget-boolean v3, v0, LX/10p;->d:Z

    if-nez v3, :cond_9

    iget-object v3, v0, LX/10p;->b:LX/10e;

    if-ne v2, v3, :cond_9

    .line 108122
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/10p;->d:Z

    .line 108123
    sget-object v3, LX/A7U;->CLOSED:LX/A7U;

    invoke-virtual {v0, v3}, LX/10p;->a(LX/A7U;)V

    goto :goto_9

    .line 108124
    :cond_12
    invoke-static {v2}, LX/10p;->c(LX/10e;)LX/A7U;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/10p;->c(LX/A7U;)V

    goto :goto_b
.end method

.method public static c(LX/0fP;LX/10e;)Z
    .locals 1

    .prologue
    .line 108125
    iget-object v0, p0, LX/0fP;->q:LX/10e;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Z)V
    .locals 2

    .prologue
    .line 107957
    invoke-virtual {p0}, LX/0fP;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 107958
    :cond_0
    :goto_0
    return-void

    .line 107959
    :cond_1
    iget-object v0, p0, LX/0fP;->j:LX/A7O;

    if-eqz v0, :cond_3

    if-nez p1, :cond_2

    iget-object v0, p0, LX/0fP;->q:LX/10e;

    sget-object v1, LX/10e;->SHOWING_LEFT:LX/10e;

    if-ne v0, v1, :cond_3

    .line 107960
    :cond_2
    iget-object v0, p0, LX/0fP;->j:LX/A7O;

    invoke-virtual {v0}, LX/A7O;->b()V

    .line 107961
    :cond_3
    iget-object v0, p0, LX/0fP;->k:LX/A7O;

    if-eqz v0, :cond_0

    if-nez p1, :cond_4

    iget-object v0, p0, LX/0fP;->q:LX/10e;

    sget-object v1, LX/10e;->SHOWING_RIGHT:LX/10e;

    if-ne v0, v1, :cond_0

    .line 107962
    :cond_4
    iget-object v0, p0, LX/0fP;->k:LX/A7O;

    invoke-virtual {v0}, LX/A7O;->b()V

    goto :goto_0
.end method

.method public static e(LX/0fP;Z)V
    .locals 1

    .prologue
    .line 107963
    iget-object v0, p0, LX/0fP;->j:LX/A7O;

    if-eqz v0, :cond_0

    .line 107964
    iget-object v0, p0, LX/0fP;->j:LX/A7O;

    invoke-virtual {v0, p1}, LX/A7O;->a(Z)V

    .line 107965
    :cond_0
    return-void
.end method

.method public static f(LX/0fP;Z)V
    .locals 1

    .prologue
    .line 107900
    iget-object v0, p0, LX/0fP;->k:LX/A7O;

    if-eqz v0, :cond_0

    .line 107901
    iget-object v0, p0, LX/0fP;->k:LX/A7O;

    invoke-virtual {v0, p1}, LX/A7O;->a(Z)V

    .line 107902
    :cond_0
    return-void
.end method

.method public static g(LX/0fP;Z)V
    .locals 1

    .prologue
    .line 107903
    iget-object v0, p0, LX/0fP;->j:LX/A7O;

    if-eqz v0, :cond_0

    .line 107904
    iget-object v0, p0, LX/0fP;->j:LX/A7O;

    invoke-virtual {v0, p1}, LX/A7O;->b(Z)V

    .line 107905
    :cond_0
    return-void
.end method

.method public static h(LX/0fP;Z)V
    .locals 1

    .prologue
    .line 107906
    iget-object v0, p0, LX/0fP;->k:LX/A7O;

    if-eqz v0, :cond_0

    .line 107907
    iget-object v0, p0, LX/0fP;->k:LX/A7O;

    invoke-virtual {v0, p1}, LX/A7O;->b(Z)V

    .line 107908
    :cond_0
    return-void
.end method

.method public static k(LX/0fP;)Z
    .locals 1

    .prologue
    .line 107954
    iget-object v0, p0, LX/0fP;->m:Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0fP;->m:Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;

    .line 107955
    iget-boolean p0, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, p0

    .line 107956
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static q(LX/0fP;)V
    .locals 1

    .prologue
    .line 107909
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/0fP;->d(Z)V

    .line 107910
    return-void
.end method


# virtual methods
.method public final a(LX/0fM;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/drawers/DrawerController$DrawerContentController;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107911
    iget-object v0, p0, LX/0fP;->j:LX/A7O;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0fP;->j:LX/A7O;

    invoke-virtual {v0, p1}, LX/A7O;->a(LX/0fM;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107912
    sget-object v0, LX/10b;->LEFT:LX/10b;

    invoke-static {p0, v0, p2}, LX/0fP;->a(LX/0fP;LX/10b;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 107913
    :goto_0
    return-object v0

    .line 107914
    :cond_0
    iget-object v0, p0, LX/0fP;->k:LX/A7O;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0fP;->k:LX/A7O;

    invoke-virtual {v0, p1}, LX/A7O;->a(LX/0fM;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107915
    sget-object v0, LX/10b;->RIGHT:LX/10b;

    invoke-static {p0, v0, p2}, LX/0fP;->a(LX/0fP;LX/10b;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 107916
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown drawer content controller"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107917
    sget-object v0, LX/10e;->CLOSED:LX/10e;

    invoke-direct {p0, v0, p1}, LX/0fP;->a(LX/10e;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0fR;)V
    .locals 2

    .prologue
    .line 107918
    invoke-virtual {p0}, LX/0fP;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 107919
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t add a drawer interceptor until after the DrawerController has been attached."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107920
    :cond_0
    iget-object v0, p0, LX/0fP;->h:LX/A7T;

    .line 107921
    iget-object v1, v0, LX/A7T;->k:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 107922
    return-void
.end method

.method public final a(LX/10b;LX/0fM;Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 107923
    invoke-virtual {p0}, LX/0fP;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 107924
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must attach DrawerController to an Activity before attaching content controllers"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107925
    :cond_0
    const-string v2, "Cannot attach a null DrawerContentController to DrawerController"

    invoke-static {p2, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107926
    sget-object v2, LX/A7M;->a:[I

    invoke-virtual {p1}, LX/10b;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 107927
    :goto_0
    invoke-virtual {p2, p0}, LX/0fM;->a(LX/0fP;)V

    .line 107928
    sget-object v0, LX/10b;->LEFT:LX/10b;

    if-ne p1, v0, :cond_4

    iget-object v0, p0, LX/0fP;->q:LX/10e;

    sget-object v1, LX/10e;->SHOWING_LEFT:LX/10e;

    if-eq v0, v1, :cond_4

    .line 107929
    :cond_1
    :goto_1
    return-void

    .line 107930
    :pswitch_0
    iget-object v2, p0, LX/0fP;->j:LX/A7O;

    if-nez v2, :cond_2

    move v0, v1

    :cond_2
    const-string v2, "A left content controller is already attached to the DrawerController"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 107931
    new-instance v0, LX/A7O;

    invoke-direct {v0, p0, p1, p2, p3}, LX/A7O;-><init>(LX/0fP;LX/10b;LX/0fM;Z)V

    iput-object v0, p0, LX/0fP;->j:LX/A7O;

    .line 107932
    iget-object v0, p0, LX/0fP;->j:LX/A7O;

    .line 107933
    iput-boolean v1, v0, LX/A7O;->k:Z

    .line 107934
    goto :goto_0

    .line 107935
    :pswitch_1
    iget-object v2, p0, LX/0fP;->k:LX/A7O;

    if-nez v2, :cond_3

    move v0, v1

    :cond_3
    const-string v2, "A right content controller is already attached to the DrawerController"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 107936
    new-instance v0, LX/A7O;

    invoke-direct {v0, p0, p1, p2, p3}, LX/A7O;-><init>(LX/0fP;LX/10b;LX/0fM;Z)V

    iput-object v0, p0, LX/0fP;->k:LX/A7O;

    .line 107937
    iget-object v0, p0, LX/0fP;->k:LX/A7O;

    .line 107938
    iput-boolean v1, v0, LX/A7O;->k:Z

    .line 107939
    goto :goto_0

    .line 107940
    :cond_4
    sget-object v0, LX/10b;->RIGHT:LX/10b;

    if-ne p1, v0, :cond_5

    iget-object v0, p0, LX/0fP;->q:LX/10e;

    sget-object v1, LX/10e;->SHOWING_RIGHT:LX/10e;

    if-ne v0, v1, :cond_1

    .line 107941
    :cond_5
    sget-object v0, LX/A7M;->a:[I

    invoke-virtual {p1}, LX/10b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 107942
    const/4 v0, 0x0

    :goto_2
    move-object v0, v0

    .line 107943
    if-eqz v0, :cond_1

    .line 107944
    invoke-virtual {v0}, LX/A7O;->b()V

    goto :goto_1

    .line 107945
    :pswitch_2
    iget-object v0, p0, LX/0fP;->j:LX/A7O;

    goto :goto_2

    .line 107946
    :pswitch_3
    iget-object v0, p0, LX/0fP;->k:LX/A7O;

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b(LX/0fR;)V
    .locals 2

    .prologue
    .line 107947
    invoke-virtual {p0}, LX/0fP;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 107948
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t remove a drawer interceptor until after the DrawerController has been attached."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107949
    :cond_0
    iget-object v0, p0, LX/0fP;->h:LX/A7T;

    .line 107950
    iget-object v1, v0, LX/A7T;->k:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 107951
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 107952
    iget-object v0, p0, LX/0fP;->g:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0fP;->h:LX/A7T;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()LX/0gc;
    .locals 1

    .prologue
    .line 107953
    iget-object v0, p0, LX/0fP;->f:Landroid/app/Activity;

    check-cast v0, LX/0ew;

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    return-object v0
.end method
