.class public LX/0Vs;
.super LX/0Vt;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/0Vs;


# instance fields
.field private final a:LX/0Vv;

.field private final b:LX/0Vw;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Vy;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0Vu;LX/0Vv;LX/0Or;LX/0Vw;LX/0Vy;LX/0Or;)V
    .locals 2
    .param p1    # Landroid/content/res/Resources;
        .annotation runtime Lcom/facebook/resources/BaseResources;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/resources/impl/analytics/IsResourceUsageLoggingEnabled;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/resources/impl/IsClearPreloadedResourcesEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/0Vu;",
            "LX/0Vv;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Vw;",
            "LX/0Vy;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 74855
    invoke-direct {p0, p1, p2}, LX/0Vt;-><init>(Landroid/content/res/Resources;LX/0Vu;)V

    .line 74856
    iput-object p3, p0, LX/0Vs;->a:LX/0Vv;

    .line 74857
    iput-object p4, p0, LX/0Vs;->c:LX/0Or;

    .line 74858
    iput-object p5, p0, LX/0Vs;->b:LX/0Vw;

    .line 74859
    iput-object p6, p0, LX/0Vs;->d:LX/0Vy;

    .line 74860
    invoke-interface {p7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74861
    :try_start_0
    invoke-static {}, LX/0Vy;->d()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "sPreloadedDrawables"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 74862
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 74863
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 74864
    instance-of p1, v1, Landroid/util/LongSparseArray;

    if-eqz p1, :cond_2

    .line 74865
    const/4 v1, 0x0

    new-instance p1, Landroid/util/LongSparseArray;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Landroid/util/LongSparseArray;-><init>(I)V

    invoke-virtual {v0, v1, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 74866
    :cond_0
    :goto_0
    :try_start_1
    invoke-static {}, LX/0Vy;->d()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "sPreloadedDrawables"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 74867
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 74868
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 74869
    instance-of p1, v0, Landroid/util/LongSparseArray;

    if-eqz p1, :cond_3

    .line 74870
    instance-of p1, v0, LX/0Vz;

    if-nez p1, :cond_1

    .line 74871
    const/4 p1, 0x0

    new-instance p2, LX/0Vz;

    check-cast v0, Landroid/util/LongSparseArray;

    invoke-direct {p2, v0, p6, p0}, LX/0Vz;-><init>(Landroid/util/LongSparseArray;LX/0Vy;Landroid/content/res/Resources;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 74872
    :cond_1
    :goto_1
    :try_start_2
    const-class v0, Landroid/content/res/AssetManager;

    const-string v1, "mThemeCookies"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 74873
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 74874
    invoke-virtual {p0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 74875
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 74876
    :goto_2
    return-void

    .line 74877
    :cond_2
    :try_start_3
    instance-of v1, v1, [Landroid/util/LongSparseArray;

    if-eqz v1, :cond_0

    .line 74878
    const/4 v1, 0x0

    const/4 p1, 0x0

    new-array p1, p1, [Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 74879
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 74880
    iget-object v0, p6, LX/0Vy;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class p1, LX/0Vy;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 74881
    :cond_3
    :try_start_4
    instance-of v1, v0, [Landroid/util/LongSparseArray;

    if-eqz v1, :cond_1

    .line 74882
    check-cast v0, [Landroid/util/LongSparseArray;

    check-cast v0, [Landroid/util/LongSparseArray;

    .line 74883
    const/4 v1, 0x0

    :goto_3
    array-length p1, v0

    if-ge v1, p1, :cond_1

    .line 74884
    aget-object p1, v0, v1

    instance-of p1, p1, LX/0Vz;

    if-nez p1, :cond_4

    .line 74885
    new-instance p1, LX/0Vz;

    aget-object p2, v0, v1

    invoke-direct {p1, p2, p6, p0}, LX/0Vz;-><init>(Landroid/util/LongSparseArray;LX/0Vy;Landroid/content/res/Resources;)V

    aput-object p1, v0, v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 74886
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 74887
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 74888
    iget-object v0, p6, LX/0Vy;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class p1, LX/0Vy;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 74889
    :catch_2
    goto :goto_2
.end method

.method public static a(LX/0QB;)LX/0Vs;
    .locals 14

    .prologue
    .line 74896
    sget-object v0, LX/0Vs;->e:LX/0Vs;

    if-nez v0, :cond_1

    .line 74897
    const-class v1, LX/0Vs;

    monitor-enter v1

    .line 74898
    :try_start_0
    sget-object v0, LX/0Vs;->e:LX/0Vs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 74899
    if-eqz v2, :cond_0

    .line 74900
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 74901
    new-instance v3, LX/0Vs;

    invoke-static {v0}, LX/0W0;->b(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0Vu;->a(LX/0QB;)LX/0Vu;

    move-result-object v5

    check-cast v5, LX/0Vu;

    invoke-static {v0}, LX/0Vv;->a(LX/0QB;)LX/0Vv;

    move-result-object v6

    check-cast v6, LX/0Vv;

    const/16 v7, 0x358

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0Vw;->a(LX/0QB;)LX/0Vw;

    move-result-object v8

    check-cast v8, LX/0Vw;

    .line 74902
    new-instance v11, LX/0Vy;

    const/16 v9, 0xb99

    invoke-static {v0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static {v0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/Executor;

    const/16 v10, 0x259

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v10

    check-cast v10, LX/0W3;

    invoke-direct {v11, v12, v9, v13, v10}, LX/0Vy;-><init>(LX/0Or;Ljava/util/concurrent/Executor;LX/0Ot;LX/0W3;)V

    .line 74903
    move-object v9, v11

    .line 74904
    check-cast v9, LX/0Vy;

    const/16 v10, 0x357

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/0Vs;-><init>(Landroid/content/res/Resources;LX/0Vu;LX/0Vv;LX/0Or;LX/0Vw;LX/0Vy;LX/0Or;)V

    .line 74905
    move-object v0, v3

    .line 74906
    sput-object v0, LX/0Vs;->e:LX/0Vs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 74907
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 74908
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 74909
    :cond_1
    sget-object v0, LX/0Vs;->e:LX/0Vs;

    return-object v0

    .line 74910
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 74911
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(I)V
    .locals 2

    .prologue
    .line 74893
    if-nez p0, :cond_0

    .line 74894
    new-instance v0, Landroid/content/res/Resources$NotFoundException;

    const-string v1, "Resource id 0x0 requested, this probably means a string resource is missing"

    invoke-direct {v0, v1}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74895
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Locale;)V
    .locals 1

    .prologue
    .line 74890
    invoke-super {p0, p1}, LX/0Vt;->a(Ljava/util/Locale;)V

    .line 74891
    iget-object v0, p0, LX/0Vs;->a:LX/0Vv;

    invoke-virtual {v0}, LX/0Vv;->g()V

    .line 74892
    return-void
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74829
    iget-object v0, p0, LX/0Vs;->a:LX/0Vv;

    .line 74830
    iget-object p0, v0, LX/0Vv;->w:Lcom/google/common/util/concurrent/SettableFuture;

    move-object v0, p0

    .line 74831
    return-object v0
.end method

.method public final getDrawable(I)Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 74845
    iget-object v0, p0, LX/0Vs;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 74846
    iget-object v0, p0, LX/0Vs;->b:LX/0Vw;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "resource_usage_drawable_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, LX/0Vs;->getResourceName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 74847
    :cond_0
    invoke-static {p1}, LX/0W5;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 74848
    if-eqz v0, :cond_1

    .line 74849
    :goto_0
    return-object v0

    .line 74850
    :cond_1
    iget-object v0, p0, LX/0Vs;->d:LX/0Vy;

    invoke-virtual {v0, p1}, LX/0Vy;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 74851
    iget-object v0, p0, LX/0Vs;->d:LX/0Vy;

    invoke-virtual {v0, p1, p0}, LX/0Vy;->a(ILandroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 74852
    :cond_2
    iget-object v0, p0, LX/0Vs;->d:LX/0Vy;

    invoke-virtual {v0, p1}, LX/0Vy;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 74853
    invoke-static {p1, p0}, LX/0Vy;->b(ILandroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 74854
    :cond_3
    invoke-super {p0, p1}, LX/0Vt;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public final getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 74835
    iget-object v0, p0, LX/0Vs;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 74836
    iget-object v0, p0, LX/0Vs;->b:LX/0Vw;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "resource_usage_drawable_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, LX/0Vs;->getResourceName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 74837
    :cond_0
    invoke-static {p1}, LX/0W5;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 74838
    if-eqz v0, :cond_1

    .line 74839
    :goto_0
    return-object v0

    .line 74840
    :cond_1
    iget-object v0, p0, LX/0Vs;->d:LX/0Vy;

    invoke-virtual {v0, p1}, LX/0Vy;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 74841
    iget-object v0, p0, LX/0Vs;->d:LX/0Vy;

    invoke-virtual {v0, p1, p0}, LX/0Vy;->a(ILandroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 74842
    :cond_2
    iget-object v0, p0, LX/0Vs;->d:LX/0Vy;

    invoke-virtual {v0, p1}, LX/0Vy;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 74843
    invoke-static {p1, p0}, LX/0Vy;->b(ILandroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 74844
    :cond_3
    invoke-super {p0, p1, p2}, LX/0Vt;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public final getLayout(I)Landroid/content/res/XmlResourceParser;
    .locals 4

    .prologue
    .line 74832
    iget-object v0, p0, LX/0Vs;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 74833
    iget-object v0, p0, LX/0Vs;->b:LX/0Vw;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "resource_usage_layout_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, LX/0Vs;->getResourceName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 74834
    :cond_0
    invoke-super {p0, p1}, LX/0Vt;->getLayout(I)Landroid/content/res/XmlResourceParser;

    move-result-object v0

    return-object v0
.end method

.method public final getQuantityString(II)Ljava/lang/String;
    .locals 1

    .prologue
    .line 74912
    invoke-virtual {p0, p1, p2}, LX/0Vs;->getQuantityText(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final varargs getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 74805
    invoke-virtual {p0, p1, p2}, LX/0Vs;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    .line 74806
    iget-object v1, p0, LX/0Vs;->a:LX/0Vv;

    invoke-virtual {v1}, LX/0Vv;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1, v0, p3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getQuantityText(II)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 74807
    invoke-static {p1}, LX/0Vs;->a(I)V

    .line 74808
    const/high16 v0, 0x7f0f0000

    invoke-super {p0, v0, p2}, LX/0Vt;->getQuantityText(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 74809
    invoke-static {v0}, LX/0W6;->fromFakeText(Ljava/lang/CharSequence;)LX/0W6;

    move-result-object v0

    .line 74810
    iget-object v1, p0, LX/0Vs;->a:LX/0Vv;

    .line 74811
    iget-object p0, v1, LX/0Vv;->B:LX/0W7;

    invoke-static {v1, p0, p1, p2, v0}, LX/0Vv;->a(LX/0Vv;LX/0W7;IILX/0W6;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    move-object v0, p0

    .line 74812
    return-object v0
.end method

.method public final getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 74813
    invoke-virtual {p0, p1}, LX/0Vs;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final varargs getString(I[Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 74814
    invoke-virtual {p0, p1}, LX/0Vs;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 74815
    iget-object v1, p0, LX/0Vs;->a:LX/0Vv;

    invoke-virtual {v1}, LX/0Vv;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1, v0, p2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getStringArray(I)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 74816
    invoke-static {p1}, LX/0Vs;->a(I)V

    .line 74817
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, LX/0Vt;->a(J)V

    .line 74818
    iget-object v0, p0, LX/0Vs;->a:LX/0Vv;

    .line 74819
    iget-object v1, v0, LX/0Vv;->C:LX/0W7;

    const/4 v2, 0x0

    const/4 p0, 0x0

    invoke-static {v0, v1, p1, v2, p0}, LX/0Vv;->a(LX/0Vv;LX/0W7;IILX/0W6;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    move-object v0, v1

    .line 74820
    return-object v0
.end method

.method public final getText(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 74821
    invoke-static {p1}, LX/0Vs;->a(I)V

    .line 74822
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, LX/0Vt;->a(J)V

    .line 74823
    iget-object v0, p0, LX/0Vs;->a:LX/0Vv;

    invoke-virtual {v0, p1}, LX/0Vv;->a(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final getText(ILjava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 74824
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, LX/0Vt;->a(J)V

    .line 74825
    if-eqz p1, :cond_1

    iget-object v0, p0, LX/0Vs;->a:LX/0Vv;

    invoke-virtual {v0, p1}, LX/0Vv;->a(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 74826
    :goto_0
    if-eqz v0, :cond_0

    move-object p2, v0

    :cond_0
    return-object p2

    .line 74827
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getTextArray(I)[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 74828
    invoke-virtual {p0, p1}, LX/0Vs;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
