.class public LX/0qK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/0SG;

.field private final b:I

.field private final c:J

.field private d:D

.field private e:J


# direct methods
.method public constructor <init>(LX/0SG;IJ)V
    .locals 3

    .prologue
    .line 147358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147359
    iput-object p1, p0, LX/0qK;->a:LX/0SG;

    .line 147360
    iput p2, p0, LX/0qK;->b:I

    .line 147361
    iput-wide p3, p0, LX/0qK;->c:J

    .line 147362
    int-to-double v0, p2

    iput-wide v0, p0, LX/0qK;->d:D

    .line 147363
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Z
    .locals 10

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 147364
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0qK;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 147365
    iget-wide v2, p0, LX/0qK;->e:J

    sub-long v2, v0, v2

    .line 147366
    iput-wide v0, p0, LX/0qK;->e:J

    .line 147367
    iget-wide v0, p0, LX/0qK;->d:D

    long-to-double v2, v2

    iget v4, p0, LX/0qK;->b:I

    int-to-double v4, v4

    iget-wide v6, p0, LX/0qK;->c:J

    long-to-double v6, v6

    div-double/2addr v4, v6

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iput-wide v0, p0, LX/0qK;->d:D

    .line 147368
    iget-wide v0, p0, LX/0qK;->d:D

    iget v2, p0, LX/0qK;->b:I

    int-to-double v2, v2

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 147369
    iget v0, p0, LX/0qK;->b:I

    int-to-double v0, v0

    iput-wide v0, p0, LX/0qK;->d:D

    .line 147370
    :cond_0
    iget-wide v0, p0, LX/0qK;->d:D
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmpg-double v0, v0, v8

    if-gez v0, :cond_1

    .line 147371
    const/4 v0, 0x0

    .line 147372
    :goto_0
    monitor-exit p0

    return v0

    .line 147373
    :cond_1
    :try_start_1
    iget-wide v0, p0, LX/0qK;->d:D

    sub-double/2addr v0, v8

    iput-wide v0, p0, LX/0qK;->d:D
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147374
    const/4 v0, 0x1

    goto :goto_0

    .line 147375
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
