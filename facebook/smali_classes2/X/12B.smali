.class public LX/12B;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:[[B

.field public final b:[[C


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 174149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174150
    invoke-static {}, LX/12C;->values()[LX/12C;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [[B

    iput-object v0, p0, LX/12B;->a:[[B

    .line 174151
    invoke-static {}, LX/12D;->values()[LX/12D;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [[C

    iput-object v0, p0, LX/12B;->b:[[C

    .line 174152
    return-void
.end method


# virtual methods
.method public final a(LX/12C;[B)V
    .locals 2

    .prologue
    .line 174171
    iget-object v0, p0, LX/12B;->a:[[B

    invoke-virtual {p1}, LX/12C;->ordinal()I

    move-result v1

    aput-object p2, v0, v1

    .line 174172
    return-void
.end method

.method public final a(LX/12D;[C)V
    .locals 2

    .prologue
    .line 174162
    iget-object v0, p0, LX/12B;->b:[[C

    invoke-virtual {p1}, LX/12D;->ordinal()I

    move-result v1

    aput-object p2, v0, v1

    .line 174163
    return-void
.end method

.method public final a(LX/12C;)[B
    .locals 4

    .prologue
    .line 174164
    invoke-virtual {p1}, LX/12C;->ordinal()I

    move-result v1

    .line 174165
    iget-object v0, p0, LX/12B;->a:[[B

    aget-object v0, v0, v1

    .line 174166
    if-nez v0, :cond_0

    .line 174167
    iget v0, p1, LX/12C;->size:I

    .line 174168
    new-array v1, v0, [B

    move-object v0, v1

    .line 174169
    :goto_0
    return-object v0

    .line 174170
    :cond_0
    iget-object v2, p0, LX/12B;->a:[[B

    const/4 v3, 0x0

    aput-object v3, v2, v1

    goto :goto_0
.end method

.method public final a(LX/12D;)[C
    .locals 1

    .prologue
    .line 174161
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/12B;->a(LX/12D;I)[C

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/12D;I)[C
    .locals 4

    .prologue
    .line 174153
    iget v0, p1, LX/12D;->size:I

    if-le v0, p2, :cond_0

    .line 174154
    iget p2, p1, LX/12D;->size:I

    .line 174155
    :cond_0
    invoke-virtual {p1}, LX/12D;->ordinal()I

    move-result v1

    .line 174156
    iget-object v0, p0, LX/12B;->b:[[C

    aget-object v0, v0, v1

    .line 174157
    if-eqz v0, :cond_1

    array-length v2, v0

    if-ge v2, p2, :cond_2

    .line 174158
    :cond_1
    new-array v0, p2, [C

    move-object v0, v0

    .line 174159
    :goto_0
    return-object v0

    .line 174160
    :cond_2
    iget-object v2, p0, LX/12B;->b:[[C

    const/4 v3, 0x0

    aput-object v3, v2, v1

    goto :goto_0
.end method
