.class public LX/0Sm;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/common/time/AwakeTimeSinceBootClock;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/common/time/AwakeTimeSinceBootClock;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62192
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;
    .locals 3

    .prologue
    .line 62193
    sget-object v0, LX/0Sm;->a:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    if-nez v0, :cond_1

    .line 62194
    const-class v1, LX/0Sm;

    monitor-enter v1

    .line 62195
    :try_start_0
    sget-object v0, LX/0Sm;->a:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 62196
    if-eqz v2, :cond_0

    .line 62197
    :try_start_1
    invoke-static {}, LX/0Sn;->b()Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v0

    sput-object v0, LX/0Sm;->a:Lcom/facebook/common/time/AwakeTimeSinceBootClock;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62198
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 62199
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 62200
    :cond_1
    sget-object v0, LX/0Sm;->a:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    return-object v0

    .line 62201
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 62202
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 62203
    invoke-static {}, LX/0Sn;->b()Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v0

    return-object v0
.end method
