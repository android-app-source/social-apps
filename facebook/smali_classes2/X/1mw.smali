.class public final LX/1mw;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/graphics/drawable/Drawable;",
        ">",
        "LX/1X1",
        "<",
        "LX/1dT;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<TT;>;"
        }
    .end annotation
.end field

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>(LX/1dc;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 314274
    invoke-static {}, LX/1dT;->q()LX/1dT;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 314275
    iput-object p1, p0, LX/1mw;->a:LX/1dc;

    .line 314276
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 314277
    iget-object v0, p0, LX/1mw;->a:LX/1dc;

    invoke-virtual {v0}, LX/1dc;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 314278
    if-ne p0, p1, :cond_0

    .line 314279
    const/4 v0, 0x1

    .line 314280
    :goto_0
    return v0

    .line 314281
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 314282
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 314283
    :cond_2
    check-cast p1, LX/1mw;

    .line 314284
    iget-object v0, p0, LX/1mw;->a:LX/1dc;

    iget-object v1, p1, LX/1mw;->a:LX/1dc;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 314285
    iget-object v0, p0, LX/1mw;->a:LX/1dc;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
