.class public LX/0rC;
.super LX/0rB;
.source ""


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/80R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 149135
    sget-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->b:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {p0, v0, p1}, LX/0rB;-><init>(Lcom/facebook/api/feedtype/FeedType$Name;LX/0Ot;)V

    .line 149136
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Lcom/facebook/api/feedtype/FeedType;
    .locals 3

    .prologue
    .line 149137
    const-string v0, "friend_list_feed_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 149138
    new-instance v1, Lcom/facebook/api/feedtype/FeedType;

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->b:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v1, v0, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    return-object v1
.end method

.method public final a(Landroid/content/Intent;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 149139
    const-string v0, "friend_list_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
