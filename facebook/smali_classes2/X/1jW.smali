.class public LX/1jW;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1jf;

.field private final d:LX/1KX;

.field public final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0y5;

.field public final g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/7zo;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/1jY;

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/16u;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0pJ;

.field public final k:LX/0pV;

.field private final l:LX/1jZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 300424
    const-class v0, LX/1jW;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1jW;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/api/feedtype/FeedType;LX/1jX;LX/0y5;LX/1jY;LX/1jd;LX/0Ot;LX/0pV;LX/0pJ;LX/1jZ;LX/0ad;)V
    .locals 3
    .param p1    # Lcom/facebook/api/feedtype/FeedType;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feedtype/FeedType;",
            "LX/1jX;",
            "LX/0y5;",
            "LX/1jY;",
            "LX/1jd;",
            "LX/0Ot",
            "<",
            "LX/16u;",
            ">;",
            "LX/0pV;",
            "LX/0pJ;",
            "LX/1jZ;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 300425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 300426
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/1jW;->a:Ljava/util/ArrayList;

    .line 300427
    iput-object p3, p0, LX/1jW;->f:LX/0y5;

    .line 300428
    invoke-static {p1}, LX/0pO;->a(Lcom/facebook/api/feedtype/FeedType;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2, v2}, LX/1jX;->a(I)LX/1jf;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/1jW;->c:LX/1jf;

    .line 300429
    sget-object v0, LX/1KX;->a:LX/1KX;

    move-object v0, v0

    .line 300430
    iput-object v0, p0, LX/1jW;->d:LX/1KX;

    .line 300431
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1jW;->e:Ljava/util/Set;

    .line 300432
    iput-object p4, p0, LX/1jW;->h:LX/1jY;

    .line 300433
    iput-object p6, p0, LX/1jW;->i:LX/0Ot;

    .line 300434
    iput-object p7, p0, LX/1jW;->k:LX/0pV;

    .line 300435
    iput-object p8, p0, LX/1jW;->j:LX/0pJ;

    .line 300436
    iput-object p9, p0, LX/1jW;->l:LX/1jZ;

    .line 300437
    sget-short v0, LX/0fe;->as:S

    invoke-interface {p10, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 300438
    new-instance v2, LX/7zo;

    invoke-static {p5}, LX/1jZ;->a(LX/0QB;)LX/1jZ;

    move-result-object v0

    check-cast v0, LX/1jZ;

    invoke-static {p5}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v2, p4, v0, v1}, LX/7zo;-><init>(LX/1jY;LX/1jZ;LX/0ad;)V

    .line 300439
    move-object v0, v2

    .line 300440
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/1jW;->g:LX/0am;

    .line 300441
    :goto_1
    return-void

    .line 300442
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, LX/1jX;->a(I)LX/1jf;

    move-result-object v0

    goto :goto_0

    .line 300443
    :cond_1
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/1jW;->g:LX/0am;

    goto :goto_1
.end method

.method private static a(LX/1jW;Lcom/facebook/feed/model/ClientFeedUnitEdge;I)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 300444
    invoke-static {p2}, LX/1jh;->c(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/1jW;->e:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 300445
    :goto_0
    return-void

    .line 300446
    :cond_0
    iget-object v2, p0, LX/1jW;->e:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 300447
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v2

    .line 300448
    iget-object v3, p0, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 300449
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 300450
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 300451
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    .line 300452
    :goto_1
    move-object v3, v3

    .line 300453
    if-eqz v3, :cond_4

    move v2, v0

    .line 300454
    :goto_2
    if-eqz v3, :cond_5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->n()Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_UNREAD:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    if-ne v4, v5, :cond_5

    .line 300455
    invoke-virtual {v3}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, LX/0x1;->c(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    move v1, v2

    .line 300456
    :goto_3
    iget-object v2, p0, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 300457
    iget-object v2, p0, LX/1jW;->e:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 300458
    iget-object v2, p0, LX/1jW;->f:LX/0y5;

    invoke-virtual {v2, p1, p2}, LX/0y5;->a(Lcom/facebook/feed/model/ClientFeedUnitEdge;I)LX/14s;

    .line 300459
    iget-object v2, p0, LX/1jW;->f:LX/0y5;

    .line 300460
    iget-object v3, v2, LX/0y5;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/14u;

    .line 300461
    iget-boolean v4, v3, LX/14u;->g:Z

    if-nez v4, :cond_8

    .line 300462
    :cond_2
    :goto_4
    iget-object v2, p0, LX/1jW;->l:LX/1jZ;

    iget-object v3, p0, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 300463
    invoke-virtual {v2}, LX/1jZ;->a()Z

    move-result v4

    if-nez v4, :cond_9

    .line 300464
    :cond_3
    :goto_5
    goto :goto_0

    :cond_4
    move v2, v1

    .line 300465
    goto :goto_2

    :cond_5
    move v0, v1

    move v1, v2

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_3

    :cond_7
    const/4 v3, 0x0

    goto :goto_1

    .line 300466
    :cond_8
    invoke-static {v3, p1}, LX/14u;->c(LX/14u;Lcom/facebook/feed/model/ClientFeedUnitEdge;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 300467
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/14w;->p(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    .line 300468
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    .line 300469
    iget-object p2, v3, LX/14u;->b:LX/14v;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v2, v3, LX/14u;->d:LX/157;

    invoke-virtual {p2, v4, v2}, LX/14v;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/157;)V

    .line 300470
    iget-object v4, v3, LX/14u;->e:Ljava/util/Map;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v4, v5, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 300471
    :cond_9
    invoke-static {v2, p1}, LX/1jZ;->a(LX/1jZ;Lcom/facebook/feed/model/ClientFeedUnitEdge;)LX/2xl;

    move-result-object v4

    .line 300472
    if-eqz v4, :cond_3

    .line 300473
    invoke-virtual {v4}, LX/2xl;->k()V

    .line 300474
    invoke-virtual {v4, v3}, LX/2xl;->c(I)V

    .line 300475
    invoke-virtual {v4, v1}, LX/2xl;->d(Z)V

    .line 300476
    invoke-virtual {v4, v0}, LX/2xl;->e(Z)V

    goto :goto_5
.end method

.method private static b(LX/1jW;Lcom/facebook/feed/model/ClientFeedUnitEdge;I)V
    .locals 2

    .prologue
    .line 300371
    invoke-static {p0}, LX/1jW;->l(LX/1jW;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 300372
    :goto_0
    return-void

    .line 300373
    :cond_0
    iget-object v0, p0, LX/1jW;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7zo;

    .line 300374
    invoke-static {p2}, LX/1jh;->c(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/7zo;->e:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v1, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 300375
    :goto_1
    goto :goto_0

    .line 300376
    :cond_1
    iget-object v1, v0, LX/7zo;->e:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v1, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 300377
    if-eqz v1, :cond_2

    .line 300378
    iget-object p0, v0, LX/7zo;->f:Ljava/util/List;

    invoke-interface {p0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 300379
    :cond_2
    iget-object v1, v0, LX/7zo;->e:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v1, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300380
    iget-object v1, v0, LX/7zo;->f:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private static g(LX/1jW;)V
    .locals 10

    .prologue
    .line 300477
    const-string v0, "FreshFeedStoryCollection.clearFeedUnitsNotInTopSnapshot"

    const v1, 0x630ed768

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 300478
    :try_start_0
    invoke-static {p0}, LX/1jW;->h(LX/1jW;)J

    move-result-wide v2

    .line 300479
    iget-object v0, p0, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    iget-object v0, p0, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 300480
    iget-wide v8, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->D:J

    move-wide v6, v8

    .line 300481
    cmp-long v5, v6, v2

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 300482
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->a(Lcom/facebook/graphql/model/FeedUnit;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300483
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 300484
    :cond_1
    const v0, -0x4fc4291d

    invoke-static {v0}, LX/02m;->a(I)V

    .line 300485
    return-void

    .line 300486
    :catchall_0
    move-exception v0

    const v1, -0x7d9c0361

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static h(LX/1jW;)J
    .locals 10

    .prologue
    .line 300487
    const-wide/16 v2, 0x0

    .line 300488
    iget-object v0, p0, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v5, :cond_0

    iget-object v0, p0, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 300489
    iget-wide v8, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->D:J

    move-wide v6, v8

    .line 300490
    cmp-long v1, v2, v6

    if-gez v1, :cond_1

    .line 300491
    iget-wide v8, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->D:J

    move-wide v0, v8

    .line 300492
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-wide v2, v0

    goto :goto_0

    .line 300493
    :cond_0
    return-wide v2

    :cond_1
    move-wide v0, v2

    goto :goto_1
.end method

.method private static i(LX/1jW;)Lcom/facebook/feed/model/ClientFeedUnitEdge;
    .locals 3

    .prologue
    .line 300494
    const-string v0, "FreshFeedStoryCollection.getNextBestStory"

    const v1, -0x6715ca02

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 300495
    const/4 v0, 0x0

    .line 300496
    :try_start_0
    iget-object v1, p0, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 300497
    invoke-static {p0}, LX/1jW;->j(LX/1jW;)V

    .line 300498
    iget-object v0, p0, LX/1jW;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 300499
    iget-object v1, p0, LX/1jW;->e:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 300500
    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300501
    :cond_0
    const v1, 0x594092ed

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0x19b0d3c5

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static j(LX/1jW;)V
    .locals 2

    .prologue
    .line 300502
    iget-object v0, p0, LX/1jW;->c:LX/1jf;

    iget-object v1, p0, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, LX/1jf;->a(Ljava/util/List;)V

    .line 300503
    invoke-static {p0}, LX/1jW;->l(LX/1jW;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300504
    iget-object v0, p0, LX/1jW;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7zo;

    .line 300505
    iget-object v1, v0, LX/7zo;->f:Ljava/util/List;

    sget-object p0, LX/7zo;->a:LX/7zn;

    invoke-static {v1, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 300506
    :cond_0
    return-void
.end method

.method public static l(LX/1jW;)Z
    .locals 1

    .prologue
    .line 300507
    iget-object v0, p0, LX/1jW;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    return v0
.end method

.method private static o(LX/1jW;)V
    .locals 6

    .prologue
    .line 300381
    const-string v0, "FreshFeedStoryCollection.updateDebugStats"

    const v1, 0x5cc6c301

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 300382
    :try_start_0
    iget-object v0, p0, LX/1jW;->d:LX/1KX;

    .line 300383
    iget-boolean v1, v0, LX/1KX;->e:Z

    move v0, v1

    .line 300384
    if-eqz v0, :cond_0

    .line 300385
    iget-object v0, p0, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 300386
    invoke-static {p0}, LX/1jW;->l(LX/1jW;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 300387
    iget-object v0, p0, LX/1jW;->h:LX/1jY;

    .line 300388
    iget-object v2, v0, LX/1jY;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    move v3, v2

    .line 300389
    :goto_0
    iget-object v0, p0, LX/1jW;->d:LX/1KX;

    invoke-virtual {p0}, LX/1jW;->a()I

    move-result v2

    .line 300390
    iget-object v4, p0, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-static {v4}, LX/AjT;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    move-object v4, v4

    .line 300391
    invoke-static {p0}, LX/1jW;->l(LX/1jW;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 300392
    const/4 v5, 0x0

    .line 300393
    :goto_1
    move-object v5, v5

    .line 300394
    iput v1, v0, LX/1KX;->c:I

    .line 300395
    iput v2, v0, LX/1KX;->b:I

    .line 300396
    iput v3, v0, LX/1KX;->d:I

    .line 300397
    iput-object v4, v0, LX/1KX;->f:Ljava/util/List;

    .line 300398
    iput-object v5, v0, LX/1KX;->g:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300399
    :cond_0
    const v0, 0x69a72fe0

    invoke-static {v0}, LX/02m;->a(I)V

    .line 300400
    return-void

    .line 300401
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/1jW;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7zo;

    .line 300402
    iget-object v2, v0, LX/7zo;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    move v3, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 300403
    goto :goto_0

    .line 300404
    :catchall_0
    move-exception v0

    const v1, -0x3cffa2ad

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_2
    iget-object v5, p0, LX/1jW;->g:LX/0am;

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/7zo;

    .line 300405
    iget-object p0, v5, LX/7zo;->f:Ljava/util/List;

    invoke-static {p0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object p0

    move-object v5, p0

    .line 300406
    invoke-static {v5}, LX/AjT;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 300407
    const-string v1, "FreshFeedStoryCollection.getFreshStoryCount"

    const v2, -0x31bbb27d

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 300408
    :try_start_0
    iget-object v1, p0, LX/1jW;->h:LX/1jY;

    .line 300409
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 300410
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    iget-object v2, v1, LX/1jY;->c:LX/0qp;

    invoke-virtual {v2}, LX/0qp;->size()I

    move-result v2

    if-ge v3, v2, :cond_0

    .line 300411
    iget-object v2, v1, LX/1jY;->c:LX/0qp;

    .line 300412
    iget-object v5, v2, LX/0qp;->e:Ljava/util/List;

    move-object v2, v5

    .line 300413
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1jg;

    iget-object v2, v2, LX/1jg;->d:Ljava/lang/String;

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 300414
    iget-object v2, v1, LX/1jY;->c:LX/0qp;

    .line 300415
    iget-object v5, v2, LX/0qp;->e:Ljava/util/List;

    move-object v2, v5

    .line 300416
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1jg;

    iget-boolean v2, v2, LX/1jg;->g:Z

    if-nez v2, :cond_0

    .line 300417
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 300418
    :cond_0
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v3, v2

    .line 300419
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    :goto_1
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 300420
    iget-object v5, p0, LX/1jW;->e:Ljava/util/Set;

    invoke-interface {v5, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 300421
    add-int/lit8 v0, v1, 0x1

    .line 300422
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 300423
    :cond_1
    const v0, -0x1e42b640

    invoke-static {v0}, LX/02m;->a(I)V

    return v1

    :catchall_0
    move-exception v0

    const v1, -0x65ce4613

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final a(Z)Lcom/facebook/feed/model/ClientFeedUnitEdge;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 300230
    const-string v0, "FreshFeedStoryCollection.getNextBest"

    const v2, 0x41d453a6

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 300231
    const/4 v2, 0x0

    .line 300232
    :try_start_0
    invoke-static {p0}, LX/1jW;->l(LX/1jW;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 300233
    if-nez p1, :cond_c

    .line 300234
    iget-object v0, p0, LX/1jW;->h:LX/1jY;

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 300235
    iget v4, v0, LX/1jY;->a:I

    iget-object v5, v0, LX/1jY;->c:LX/0qp;

    invoke-virtual {v5}, LX/0qp;->size()I

    move-result v5

    if-lt v4, v5, :cond_d

    .line 300236
    :cond_0
    :goto_0
    move v0, v2

    .line 300237
    if-eqz v0, :cond_b

    .line 300238
    iget-object v2, p0, LX/1jW;->h:LX/1jY;

    .line 300239
    const/4 v3, 0x0

    .line 300240
    iget v4, v2, LX/1jY;->a:I

    invoke-static {v2, v4}, LX/1jY;->b(LX/1jY;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 300241
    iget-object v3, v2, LX/1jY;->d:Ljava/util/Map;

    iget v4, v2, LX/1jY;->a:I

    invoke-static {v2, v4}, LX/1jY;->c(LX/1jY;I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 300242
    :cond_1
    iget v4, v2, LX/1jY;->a:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v2, LX/1jY;->a:I

    .line 300243
    invoke-static {v2}, LX/1jY;->i(LX/1jY;)V

    .line 300244
    move-object v2, v3

    .line 300245
    move v4, v0

    move-object v0, v2

    move v2, v4

    .line 300246
    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    if-nez v3, :cond_3

    .line 300247
    :cond_2
    invoke-static {p0}, LX/1jW;->i(LX/1jW;)Lcom/facebook/feed/model/ClientFeedUnitEdge;

    move-result-object v0

    .line 300248
    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    if-nez v3, :cond_9

    .line 300249
    :cond_4
    const v0, -0x5c6a3c4

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v1

    :goto_2
    return-object v0

    .line 300250
    :cond_5
    :try_start_1
    iget-object v0, p0, LX/1jW;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7zo;

    const/4 v5, 0x0

    .line 300251
    const/4 v4, 0x0

    .line 300252
    :goto_3
    iget-object v3, v0, LX/7zo;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    .line 300253
    iget-object v3, v0, LX/7zo;->f:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 300254
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/7zo;->a(LX/7zo;Ljava/lang/String;)I

    move-result v3

    .line 300255
    if-gez v3, :cond_7

    .line 300256
    iget-object v3, v0, LX/7zo;->f:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 300257
    :cond_6
    const/4 v3, -0x1

    :cond_7
    move v4, v3

    .line 300258
    if-ltz v4, :cond_8

    iget v3, v0, LX/7zo;->g:I

    if-ge v3, v4, :cond_11

    .line 300259
    :cond_8
    iget v3, v0, LX/7zo;->g:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, LX/7zo;->g:I

    .line 300260
    const/4 v3, 0x0

    .line 300261
    :goto_4
    move-object v0, v3

    .line 300262
    goto :goto_1

    .line 300263
    :cond_9
    invoke-static {p0}, LX/1jW;->o(LX/1jW;)V

    .line 300264
    invoke-static {p0}, LX/1jW;->g(LX/1jW;)V

    .line 300265
    iget-object v1, p0, LX/1jW;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/16u;

    invoke-virtual {v1, v0}, LX/16u;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 300266
    iget-object v1, p0, LX/1jW;->l:LX/1jZ;

    .line 300267
    invoke-virtual {v1}, LX/1jZ;->a()Z

    move-result v3

    if-nez v3, :cond_14
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 300268
    :cond_a
    :goto_5
    const v1, -0xfd3cf00

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_2

    :catchall_0
    move-exception v0

    const v1, 0x7e410679

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_b
    move v2, v0

    move-object v0, v1

    goto/16 :goto_1

    :cond_c
    move-object v0, v1

    goto/16 :goto_1

    .line 300269
    :cond_d
    :try_start_2
    iget-object v4, v0, LX/1jY;->g:LX/0qj;

    invoke-virtual {v4}, LX/0qj;->a()Z

    move-result v4

    .line 300270
    iget-object v5, v0, LX/1jY;->h:LX/0ad;

    sget-short v6, LX/0fe;->X:S

    invoke-interface {v5, v6, v3}, LX/0ad;->a(SZ)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 300271
    if-eqz v4, :cond_e

    iget v4, v0, LX/1jY;->a:I

    .line 300272
    const/4 v5, 0x0

    move v6, v5

    :goto_6
    iget-object v5, v0, LX/1jY;->c:LX/0qp;

    invoke-virtual {v5}, LX/0qp;->size()I

    move-result v5

    if-ge v6, v5, :cond_10

    .line 300273
    iget-object v5, v0, LX/1jY;->c:LX/0qp;

    .line 300274
    iget-object v7, v5, LX/0qp;->e:Ljava/util/List;

    move-object v5, v7

    .line 300275
    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1jg;

    iget-boolean v5, v5, LX/1jg;->g:Z

    if-eqz v5, :cond_f

    .line 300276
    :goto_7
    move v5, v6

    .line 300277
    if-gt v4, v5, :cond_0

    :cond_e
    move v2, v3

    .line 300278
    goto/16 :goto_0

    .line 300279
    :cond_f
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_6

    .line 300280
    :cond_10
    iget-object v5, v0, LX/1jY;->c:LX/0qp;

    invoke-virtual {v5}, LX/0qp;->size()I

    move-result v6

    goto :goto_7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 300281
    :cond_11
    :try_start_3
    iput v5, v0, LX/7zo;->g:I

    .line 300282
    iget-object v3, v0, LX/7zo;->f:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 300283
    iget-object v5, v0, LX/7zo;->e:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 300284
    iget-object v5, v0, LX/7zo;->c:LX/1jZ;

    .line 300285
    invoke-virtual {v5}, LX/1jZ;->a()Z

    move-result v6

    if-nez v6, :cond_13

    .line 300286
    :cond_12
    :goto_8
    goto/16 :goto_4

    .line 300287
    :cond_13
    invoke-static {v5, v3}, LX/1jZ;->a(LX/1jZ;Lcom/facebook/feed/model/ClientFeedUnitEdge;)LX/2xl;

    move-result-object v6

    .line 300288
    if-eqz v6, :cond_12

    .line 300289
    invoke-virtual {v6, v4}, LX/2xl;->d(I)V

    goto :goto_8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 300290
    :cond_14
    invoke-static {v1, v0}, LX/1jZ;->a(LX/1jZ;Lcom/facebook/feed/model/ClientFeedUnitEdge;)LX/2xl;

    move-result-object v3

    .line 300291
    if-eqz v3, :cond_a

    .line 300292
    invoke-virtual {v3, p1}, LX/2xl;->f(Z)V

    .line 300293
    invoke-virtual {v3, v2}, LX/2xl;->g(Z)V

    goto/16 :goto_5
.end method

.method public final a(ILjava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 300173
    move v2, v1

    .line 300174
    :goto_0
    if-gt v2, p1, :cond_4

    .line 300175
    invoke-static {p0}, LX/1jW;->l(LX/1jW;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 300176
    iget-object v0, p0, LX/1jW;->h:LX/1jY;

    .line 300177
    iget v3, v0, LX/1jY;->a:I

    add-int/2addr v3, v2

    .line 300178
    invoke-static {v0, v3}, LX/1jY;->b(LX/1jY;I)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 300179
    iget-object v4, v0, LX/1jY;->d:Ljava/util/Map;

    invoke-static {v0, v3}, LX/1jY;->c(LX/1jY;I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 300180
    :goto_1
    move-object v0, v3

    .line 300181
    :goto_2
    if-nez v0, :cond_0

    iget-object v3, p0, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 300182
    iget-object v0, p0, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 300183
    add-int/lit8 v1, v1, 0x1

    .line 300184
    :cond_0
    if-eqz v0, :cond_1

    .line 300185
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 300186
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 300187
    :cond_2
    iget-object v0, p0, LX/1jW;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7zo;

    .line 300188
    iget v3, v0, LX/7zo;->g:I

    neg-int v3, v3

    .line 300189
    iget-object v4, v0, LX/7zo;->f:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v4, v3

    :cond_3
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 300190
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, LX/7zo;->a(LX/7zo;Ljava/lang/String;)I

    move-result v6

    .line 300191
    if-ltz v6, :cond_3

    .line 300192
    add-int/2addr v4, v6

    .line 300193
    if-gt v4, v2, :cond_7

    .line 300194
    if-ne v4, v2, :cond_6

    .line 300195
    :goto_4
    move-object v0, v3

    .line 300196
    goto :goto_2

    .line 300197
    :cond_4
    return-void

    :cond_5
    const/4 v3, 0x0

    goto :goto_1

    .line 300198
    :cond_6
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    .line 300199
    goto :goto_3

    .line 300200
    :cond_7
    const/4 v3, 0x0

    goto :goto_4
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 300201
    const-string v0, "FreshFeedStoryCollection.clearGapBetweenCursors"

    const v1, 0x30befdec

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 300202
    :try_start_0
    iget-object v0, p0, LX/1jW;->h:LX/1jY;

    .line 300203
    invoke-static {v0, p1}, LX/1jY;->d(LX/1jY;Ljava/lang/String;)LX/1jg;

    move-result-object v1

    .line 300204
    invoke-static {v0, p2}, LX/1jY;->d(LX/1jY;Ljava/lang/String;)LX/1jg;

    move-result-object v2

    .line 300205
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 300206
    :cond_0
    iget-object v1, v0, LX/1jY;->f:LX/0pV;

    sget-object v2, LX/1jY;->b:Ljava/lang/String;

    const-string v3, "Unable to clear GAPS because storyInfo is null"

    invoke-virtual {v1, v2, v3}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300207
    :goto_0
    const v0, -0x3c732fd1

    invoke-static {v0}, LX/02m;->a(I)V

    .line 300208
    return-void

    .line 300209
    :catchall_0
    move-exception v0

    const v1, -0x4dde8b97

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 300210
    :cond_1
    iget-object v3, v0, LX/1jY;->c:LX/0qp;

    invoke-virtual {v3, v1}, LX/0qp;->a(Ljava/lang/Object;)I

    move-result v1

    .line 300211
    iget-object v3, v0, LX/1jY;->c:LX/0qp;

    invoke-virtual {v3, v2}, LX/0qp;->a(Ljava/lang/Object;)I

    move-result v2

    .line 300212
    if-ge v2, v1, :cond_2

    .line 300213
    iget-object v1, v0, LX/1jY;->f:LX/0pV;

    sget-object v2, LX/1jY;->b:Ljava/lang/String;

    const-string v3, "Unable to clear GAPS because endIndex < startIndex"

    invoke-virtual {v1, v2, v3}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 300214
    :cond_2
    :goto_1
    if-gt v1, v2, :cond_4

    .line 300215
    iget-object v3, v0, LX/1jY;->c:LX/0qp;

    .line 300216
    iget-object p0, v3, LX/0qp;->e:Ljava/util/List;

    move-object v3, p0

    .line 300217
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1jg;

    iget-boolean v3, v3, LX/1jg;->g:Z

    if-eqz v3, :cond_3

    .line 300218
    iget-object v3, v0, LX/1jY;->f:LX/0pV;

    sget-object p0, LX/1jY;->b:Ljava/lang/String;

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, "Clearing gap at "

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p0, p1}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 300219
    :cond_3
    iget-object v3, v0, LX/1jY;->c:LX/0qp;

    .line 300220
    iget-object p0, v3, LX/0qp;->e:Ljava/util/List;

    move-object v3, p0

    .line 300221
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1jg;

    const/4 p0, 0x0

    iput-boolean p0, v3, LX/1jg;->g:Z

    .line 300222
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 300223
    :cond_4
    invoke-static {v0}, LX/1jY;->i(LX/1jY;)V

    .line 300224
    goto :goto_0
.end method

.method public final a(LX/0Px;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 300225
    const-string v1, "FreshFeedStoryCollection.unStageStoriesToCollection"

    const v2, -0x7e2d16bd

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 300226
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 300227
    :cond_0
    const v1, -0x25c91a5b

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_0
    return v0

    .line 300228
    :cond_1
    :try_start_1
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/1jW;->b(LX/0Px;I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 300229
    const v0, 0x46237a4f

    invoke-static {v0}, LX/02m;->a(I)V

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x6ead2787

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(LX/0Px;I)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 300294
    const-string v0, "FreshFeedStoryCollection.addCachedStoriesToCollection"

    const v2, 0x4773d172

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 300295
    :try_start_0
    iget-object v0, p0, LX/1jW;->j:LX/0pJ;

    const/4 v2, 0x0

    .line 300296
    sget-wide v7, LX/0X5;->dz:J

    const/4 v9, 0x7

    invoke-virtual {v0, v7, v8, v9, v2}, LX/0pK;->a(JIZ)Z

    move-result v7

    move v0, v7

    .line 300297
    if-eqz v0, :cond_3

    .line 300298
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 300299
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 300300
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 300301
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->n()Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_UNREAD:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    if-ne v6, v7, :cond_0

    invoke-virtual {v0}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A()I

    move-result v6

    if-ne v6, v8, :cond_0

    .line 300302
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 300303
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 300304
    :cond_1
    iget-object v0, p0, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    :goto_1
    if-ge v2, v3, :cond_3

    iget-object v0, p0, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 300305
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A()I

    move-result v5

    if-nez v5, :cond_2

    .line 300306
    const-string v5, "1"

    invoke-static {v0, v5}, LX/0x1;->c(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 300307
    iget-object v5, p0, LX/1jW;->f:LX/0y5;

    .line 300308
    iget-object v6, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->C:Ljava/lang/String;

    move-object v0, v6

    .line 300309
    invoke-virtual {v5, v0, v8}, LX/0y5;->a(Ljava/lang/String;I)V

    .line 300310
    iget-object v0, p0, LX/1jW;->k:LX/0pV;

    sget-object v5, LX/1jW;->b:Ljava/lang/String;

    sget-object v6, LX/1gs;->SYNC_SEEN_STATE_FROM_DB:LX/1gs;

    invoke-virtual {v0, v5, v6}, LX/0pV;->a(Ljava/lang/String;LX/1gt;)V

    .line 300311
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 300312
    :cond_3
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v4, :cond_6

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 300313
    const-string v5, "Ad"

    .line 300314
    iget-object v6, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A:Ljava/lang/String;

    move-object v6, v6

    .line 300315
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 300316
    invoke-static {p0, v0, p2}, LX/1jW;->a(LX/1jW;Lcom/facebook/feed/model/ClientFeedUnitEdge;I)V

    .line 300317
    :goto_3
    add-int/lit8 v3, v3, 0x1

    .line 300318
    iget-object v5, p0, LX/1jW;->h:LX/1jY;

    invoke-static {p0}, LX/1jW;->l(LX/1jW;)Z

    move-result v6

    invoke-virtual {v5, v0, p2, v6}, LX/1jY;->a(Lcom/facebook/feed/model/ClientFeedUnitEdge;IZ)V

    .line 300319
    iget-boolean v5, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->E:Z

    move v5, v5

    .line 300320
    if-eqz v5, :cond_4

    .line 300321
    iget-object v5, p0, LX/1jW;->h:LX/1jY;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/1jY;->b(Ljava/lang/String;)V

    .line 300322
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 300323
    :cond_5
    invoke-static {p0, v0, p2}, LX/1jW;->b(LX/1jW;Lcom/facebook/feed/model/ClientFeedUnitEdge;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 300324
    :catchall_0
    move-exception v0

    const v1, -0x3dbe304c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 300325
    :cond_6
    :try_start_1
    invoke-static {p0}, LX/1jW;->j(LX/1jW;)V

    .line 300326
    invoke-static {p0}, LX/1jW;->o(LX/1jW;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 300327
    if-lez v3, :cond_8

    const/4 v0, 0x3

    if-eq p2, v0, :cond_7

    const/4 v0, 0x5

    if-ne p2, v0, :cond_8

    :cond_7
    const/4 v0, 0x1

    .line 300328
    :goto_4
    const v1, 0x275cc2c3

    invoke-static {v1}, LX/02m;->a(I)V

    return v0

    :cond_8
    move v0, v1

    .line 300329
    goto :goto_4
.end method

.method public final b(LX/0Px;I)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 300330
    const-string v0, "FreshFeedStoryCollection.addFreshStoriesToCollection"

    const v2, -0x3e348ce3

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 300331
    :try_start_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    move v3, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 300332
    const-string v5, "Ad"

    .line 300333
    iget-object v6, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A:Ljava/lang/String;

    move-object v6, v6

    .line 300334
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 300335
    invoke-static {p0, v0, p2}, LX/1jW;->a(LX/1jW;Lcom/facebook/feed/model/ClientFeedUnitEdge;I)V

    .line 300336
    :goto_1
    add-int/lit8 v3, v3, 0x1

    .line 300337
    iget-object v5, p0, LX/1jW;->h:LX/1jY;

    invoke-static {p0}, LX/1jW;->l(LX/1jW;)Z

    move-result v6

    invoke-virtual {v5, v0, p2, v6}, LX/1jY;->a(Lcom/facebook/feed/model/ClientFeedUnitEdge;IZ)V

    .line 300338
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 300339
    :cond_0
    invoke-static {p0, v0, p2}, LX/1jW;->b(LX/1jW;Lcom/facebook/feed/model/ClientFeedUnitEdge;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 300340
    :catchall_0
    move-exception v0

    const v1, -0x4ac1148f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 300341
    :cond_1
    :try_start_1
    invoke-static {p0}, LX/1jW;->j(LX/1jW;)V

    .line 300342
    invoke-static {p0}, LX/1jW;->o(LX/1jW;)V

    .line 300343
    if-gtz v3, :cond_2

    .line 300344
    if-nez v3, :cond_4

    if-nez p2, :cond_4

    const/4 v0, 0x1

    :goto_2
    move v0, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 300345
    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 300346
    :goto_3
    const v1, -0x6a217d11

    invoke-static {v1}, LX/02m;->a(I)V

    return v0

    :cond_3
    move v0, v1

    .line 300347
    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 300348
    const-string v0, "FreshFeedStoryCollection.setGapAtCursor"

    const v1, -0x782b2ed

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 300349
    :try_start_0
    iget-object v0, p0, LX/1jW;->h:LX/1jY;

    invoke-virtual {v0, p1}, LX/1jY;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300350
    const v0, 0x7583c82f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 300351
    return-void

    .line 300352
    :catchall_0
    move-exception v0

    const v1, 0x5804f095

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 300353
    iget-object v0, p0, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 300354
    const-string v0, "FreshFeedStoryCollection.clear"

    const v1, 0x7ea1cf05

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 300355
    :try_start_0
    iget-object v0, p0, LX/1jW;->h:LX/1jY;

    .line 300356
    const/4 v1, 0x0

    iput v1, v0, LX/1jY;->a:I

    .line 300357
    iget-object v1, v0, LX/1jY;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 300358
    iget-object v1, v0, LX/1jY;->c:LX/0qp;

    invoke-virtual {v1}, LX/0qp;->clear()V

    .line 300359
    iget-object v1, v0, LX/1jY;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 300360
    invoke-static {v0}, LX/1jY;->i(LX/1jY;)V

    .line 300361
    iget-object v0, p0, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 300362
    iget-object v0, p0, LX/1jW;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 300363
    iget-object v0, p0, LX/1jW;->l:LX/1jZ;

    invoke-virtual {v0}, LX/1jZ;->b()V

    .line 300364
    invoke-static {p0}, LX/1jW;->l(LX/1jW;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300365
    iget-object v0, p0, LX/1jW;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7zo;

    .line 300366
    iget-object v1, v0, LX/7zo;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 300367
    iget-object v1, v0, LX/7zo;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300368
    :cond_0
    const v0, 0x77cc4d23

    invoke-static {v0}, LX/02m;->a(I)V

    .line 300369
    return-void

    .line 300370
    :catchall_0
    move-exception v0

    const v1, 0x443bd486

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
