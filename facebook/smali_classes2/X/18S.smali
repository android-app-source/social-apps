.class public LX/18S;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:LX/0Wd;

.field public final d:LX/18T;

.field public final e:LX/18U;

.field public final f:LX/18V;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0Wd;LX/18T;LX/18U;LX/18V;)V
    .locals 1
    .param p2    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 206524
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206525
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 206526
    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    sparse-switch v0, :sswitch_data_0

    .line 206527
    sget v0, LX/18c;->a:I

    :goto_0
    move v0, v0

    .line 206528
    iput v0, p0, LX/18S;->a:I

    .line 206529
    const v0, 0x7f0b00b6

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v0, v0

    .line 206530
    iput v0, p0, LX/18S;->b:I

    .line 206531
    iput-object p2, p0, LX/18S;->c:LX/0Wd;

    .line 206532
    iput-object p3, p0, LX/18S;->d:LX/18T;

    .line 206533
    iput-object p4, p0, LX/18S;->e:LX/18U;

    .line 206534
    iput-object p5, p0, LX/18S;->f:LX/18V;

    .line 206535
    return-void

    .line 206536
    :sswitch_0
    sget v0, LX/18c;->b:I

    goto :goto_0

    .line 206537
    :sswitch_1
    sget v0, LX/18c;->c:I

    goto :goto_0

    .line 206538
    :sswitch_2
    sget v0, LX/18c;->d:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x78 -> :sswitch_2
        0xa0 -> :sswitch_1
        0xf0 -> :sswitch_0
    .end sparse-switch
.end method
