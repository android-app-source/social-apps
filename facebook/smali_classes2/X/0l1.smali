.class public LX/0l1;
.super LX/0T0;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static s:LX/0Xm;


# instance fields
.field public a:LX/HeQ;

.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/HeR;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Landroid/app/Activity;

.field public final e:LX/0WJ;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6G2;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0hx;

.field public final h:LX/03V;

.field public final i:LX/0l2;

.field public final j:LX/0jh;

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/10M;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Lcom/facebook/content/SecureContextHelper;

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2lP;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/33u;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fjp;",
            ">;"
        }
    .end annotation
.end field

.field public final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17X;",
            ">;"
        }
    .end annotation
.end field

.field public final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/katana/urimap/IntentHandlerUtil;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;LX/0WJ;LX/0Ot;LX/0hx;LX/03V;LX/0l2;LX/0jh;LX/0Ot;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 2
    .param p12    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/0Ot",
            "<",
            "LX/6G2;",
            ">;",
            "LX/0hx;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0l2;",
            "LX/0jh;",
            "LX/0Ot",
            "<",
            "LX/10M;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/2lP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/33u;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fjp;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17X;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/katana/urimap/IntentHandlerUtil;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 128077
    invoke-direct {p0}, LX/0T0;-><init>()V

    .line 128078
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v1, p0, LX/0l1;->b:Ljava/util/Map;

    .line 128079
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, LX/0l1;->c:Ljava/util/Map;

    .line 128080
    iput-object p1, p0, LX/0l1;->d:Landroid/app/Activity;

    .line 128081
    iput-object p2, p0, LX/0l1;->e:LX/0WJ;

    .line 128082
    iput-object p3, p0, LX/0l1;->f:LX/0Ot;

    .line 128083
    iput-object p4, p0, LX/0l1;->g:LX/0hx;

    .line 128084
    iput-object p5, p0, LX/0l1;->h:LX/03V;

    .line 128085
    iput-object p6, p0, LX/0l1;->i:LX/0l2;

    .line 128086
    iput-object p7, p0, LX/0l1;->j:LX/0jh;

    .line 128087
    iput-object p9, p0, LX/0l1;->l:Lcom/facebook/content/SecureContextHelper;

    .line 128088
    iput-object p8, p0, LX/0l1;->k:LX/0Ot;

    .line 128089
    iput-object p10, p0, LX/0l1;->m:LX/0Ot;

    .line 128090
    iput-object p11, p0, LX/0l1;->n:LX/0Ot;

    .line 128091
    iput-object p12, p0, LX/0l1;->o:LX/0Or;

    .line 128092
    iput-object p13, p0, LX/0l1;->p:LX/0Ot;

    .line 128093
    move-object/from16 v0, p14

    iput-object v0, p0, LX/0l1;->q:LX/0Ot;

    .line 128094
    move-object/from16 v0, p15

    iput-object v0, p0, LX/0l1;->r:LX/0Ot;

    .line 128095
    return-void
.end method

.method public static a(LX/0QB;)LX/0l1;
    .locals 3

    .prologue
    .line 128173
    const-class v1, LX/0l1;

    monitor-enter v1

    .line 128174
    :try_start_0
    sget-object v0, LX/0l1;->s:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 128175
    sput-object v2, LX/0l1;->s:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 128176
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128177
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/0l1;->b(LX/0QB;)LX/0l1;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 128178
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/0l1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 128179
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 128180
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/0l1;
    .locals 18

    .prologue
    .line 128171
    new-instance v2, LX/0l1;

    invoke-static/range {p0 .. p0}, LX/0kU;->a(LX/0QB;)Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    invoke-static/range {p0 .. p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v4

    check-cast v4, LX/0WJ;

    const/16 v5, 0x186a

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {p0 .. p0}, LX/0hx;->a(LX/0QB;)LX/0hx;

    move-result-object v6

    check-cast v6, LX/0hx;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0l2;->a(LX/0QB;)LX/0l2;

    move-result-object v8

    check-cast v8, LX/0l2;

    invoke-static/range {p0 .. p0}, LX/0jg;->a(LX/0QB;)LX/0jh;

    move-result-object v9

    check-cast v9, LX/0jh;

    const/16 v10, 0x4d6

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v11

    check-cast v11, Lcom/facebook/content/SecureContextHelper;

    const/16 v12, 0xc59

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x53c

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x2fd

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    const/16 v15, 0x3524

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0xc49

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0xc4b

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-direct/range {v2 .. v17}, LX/0l1;-><init>(Landroid/app/Activity;LX/0WJ;LX/0Ot;LX/0hx;LX/03V;LX/0l2;LX/0jh;LX/0Ot;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 128172
    return-object v2
.end method

.method public static j(LX/0l1;Landroid/app/Activity;)Z
    .locals 3

    .prologue
    .line 128170
    iget-object v0, p0, LX/0l1;->j:LX/0jh;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/facebook/katana/features/bugreporter/annotations/BugReportingNotRequired;

    invoke-virtual {v0, v1, v2}, LX/0jh;->a(Ljava/lang/reflect/AnnotatedElement;Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;ILandroid/view/KeyEvent;)LX/0am;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I",
            "Landroid/view/KeyEvent;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v2, 0x52

    const/4 v3, 0x1

    .line 128123
    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    .line 128124
    invoke-static {p1}, LX/14o;->a(Landroid/content/Context;)LX/0gh;

    move-result-object v0

    const-string v1, "tap_back_button"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 128125
    iget-object v0, p0, LX/0l1;->a:LX/HeQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0l1;->a:LX/HeQ;

    .line 128126
    iget-boolean v1, v0, LX/HeQ;->h:Z

    move v0, v1

    .line 128127
    if-eqz v0, :cond_0

    .line 128128
    iget-object v0, p0, LX/0l1;->a:LX/HeQ;

    invoke-virtual {v0}, LX/HeQ;->d()V

    .line 128129
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 128130
    :goto_0
    return-object v0

    .line 128131
    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_b

    .line 128132
    invoke-virtual {p3}, Landroid/view/KeyEvent;->startTracking()V

    .line 128133
    const/4 v0, 0x1

    .line 128134
    :goto_1
    move v0, v0

    .line 128135
    if-eqz v0, :cond_1

    .line 128136
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0

    .line 128137
    :cond_1
    if-ne p2, v2, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 128138
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0

    .line 128139
    :cond_2
    if-ne p2, v2, :cond_9

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_9

    .line 128140
    instance-of v0, p1, Lcom/facebook/katana/LogoutActivity;

    move v0, v0

    .line 128141
    if-eqz v0, :cond_3

    .line 128142
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0

    .line 128143
    :cond_3
    iget-object v0, p0, LX/0l1;->a:LX/HeQ;

    if-nez v0, :cond_4

    .line 128144
    iget-object v0, p0, LX/0l1;->a:LX/HeQ;

    if-nez v0, :cond_4

    .line 128145
    new-instance v0, LX/JcB;

    iget-object v1, p0, LX/0l1;->d:Landroid/app/Activity;

    iget-object v2, p0, LX/0l1;->d:Landroid/app/Activity;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/JcB;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;)V

    iput-object v0, p0, LX/0l1;->a:LX/HeQ;

    .line 128146
    :cond_4
    iget-object v0, p0, LX/0l1;->a:LX/HeQ;

    .line 128147
    iget-boolean v1, v0, LX/HeQ;->h:Z

    move v0, v1

    .line 128148
    if-eqz v0, :cond_5

    .line 128149
    invoke-static {p1}, LX/14o;->a(Landroid/content/Context;)LX/0gh;

    move-result-object v0

    sget-object v1, LX/CIo;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0gh;->d(Ljava/lang/String;)V

    .line 128150
    iget-object v0, p0, LX/0l1;->a:LX/HeQ;

    invoke-virtual {v0}, LX/HeQ;->d()V

    .line 128151
    :goto_2
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0

    .line 128152
    :cond_5
    invoke-static {p1}, LX/14o;->a(Landroid/content/Context;)LX/0gh;

    move-result-object v0

    sget-object v1, LX/CIo;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, LX/0gh;->a(Ljava/lang/String;Z)V

    .line 128153
    instance-of v0, p1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;

    if-nez v0, :cond_c

    .line 128154
    const/4 v0, 0x0

    .line 128155
    :goto_3
    move v0, v0

    .line 128156
    if-eqz v0, :cond_7

    .line 128157
    instance-of v0, p1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;

    if-nez v0, :cond_d

    .line 128158
    :cond_6
    :goto_4
    goto :goto_2

    .line 128159
    :cond_7
    const-class v0, LX/0ew;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ew;

    .line 128160
    const/4 v1, 0x0

    .line 128161
    if-eqz v0, :cond_a

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 128162
    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "chromeless:content:fragment:tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 128163
    :goto_5
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 128164
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 128165
    :goto_6
    iget-object v1, p0, LX/0l1;->a:LX/HeQ;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, LX/HeQ;->a(Landroid/view/View;ZZ)V

    goto :goto_2

    .line 128166
    :cond_8
    invoke-static {p1}, LX/18w;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    goto :goto_6

    .line 128167
    :cond_9
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto/16 :goto_0

    :cond_a
    move-object v0, v1

    goto :goto_5

    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_c
    instance-of v0, p1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;

    goto :goto_3

    .line 128168
    :cond_d
    instance-of v0, p1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;

    if-eqz v0, :cond_6

    .line 128169
    check-cast p1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;

    invoke-virtual {p1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->b()V

    goto :goto_4
.end method

.method public final a(III)V
    .locals 1

    .prologue
    .line 128121
    iget-object v0, p0, LX/0l1;->d:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3}, LX/0l1;->a(ILjava/lang/String;I)V

    .line 128122
    return-void
.end method

.method public final a(ILjava/lang/String;I)V
    .locals 3

    .prologue
    .line 128114
    iget-object v0, p0, LX/0l1;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128115
    :goto_0
    return-void

    .line 128116
    :cond_0
    new-instance v0, LX/HeR;

    invoke-direct {v0}, LX/HeR;-><init>()V

    .line 128117
    iput p1, v0, LX/HeR;->c:I

    .line 128118
    iput-object p2, v0, LX/HeR;->a:Ljava/lang/String;

    .line 128119
    iput p3, v0, LX/HeR;->b:I

    .line 128120
    iget-object v1, p0, LX/0l1;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(IZ)V
    .locals 2

    .prologue
    .line 128110
    iget-object v0, p0, LX/0l1;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HeR;

    .line 128111
    if-eqz v0, :cond_0

    .line 128112
    iput-boolean p2, v0, LX/HeR;->d:Z

    .line 128113
    :cond_0
    return-void
.end method

.method public final a(Landroid/app/Activity;Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    .line 128101
    invoke-super {p0, p1, p2}, LX/0T0;->a(Landroid/app/Activity;Landroid/content/res/Configuration;)V

    .line 128102
    iget-object v0, p0, LX/0l1;->a:LX/HeQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0l1;->a:LX/HeQ;

    .line 128103
    iget-boolean v1, v0, LX/HeQ;->h:Z

    move v0, v1

    .line 128104
    if-eqz v0, :cond_0

    .line 128105
    iget-object v0, p0, LX/0l1;->a:LX/HeQ;

    invoke-virtual {v0}, LX/HeQ;->d()V

    .line 128106
    iget-object v0, p0, LX/0l1;->a:LX/HeQ;

    invoke-static {p1}, LX/18w;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, LX/0l1;->a:LX/HeQ;

    .line 128107
    iget-boolean v3, v2, LX/HeQ;->i:Z

    move v2, v3

    .line 128108
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/HeQ;->a(Landroid/view/View;ZZ)V

    .line 128109
    :cond_0
    return-void
.end method

.method public final d(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 128096
    iget-object v0, p0, LX/0l1;->a:LX/HeQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0l1;->a:LX/HeQ;

    .line 128097
    iget-boolean p1, v0, LX/HeQ;->h:Z

    move v0, p1

    .line 128098
    if-eqz v0, :cond_0

    .line 128099
    iget-object v0, p0, LX/0l1;->a:LX/HeQ;

    invoke-virtual {v0}, LX/HeQ;->d()V

    .line 128100
    :cond_0
    return-void
.end method
