.class public LX/0uB;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0uD;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0uD;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 156079
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0uD;
    .locals 3

    .prologue
    .line 156080
    sget-object v0, LX/0uB;->a:LX/0uD;

    if-nez v0, :cond_1

    .line 156081
    const-class v1, LX/0uB;

    monitor-enter v1

    .line 156082
    :try_start_0
    sget-object v0, LX/0uB;->a:LX/0uD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 156083
    if-eqz v2, :cond_0

    .line 156084
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 156085
    const-class p0, Landroid/content/Context;

    invoke-interface {v0, p0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    invoke-static {p0}, LX/0uC;->a(Landroid/content/Context;)LX/0uD;

    move-result-object p0

    move-object v0, p0

    .line 156086
    sput-object v0, LX/0uB;->a:LX/0uD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 156087
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 156088
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 156089
    :cond_1
    sget-object v0, LX/0uB;->a:LX/0uD;

    return-object v0

    .line 156090
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 156091
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 156092
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, LX/0uC;->a(Landroid/content/Context;)LX/0uD;

    move-result-object v0

    return-object v0
.end method
