.class public LX/0gh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile I:LX/0gh;


# instance fields
.field public A:Z

.field public B:Z

.field public C:Z

.field public D:Ljava/lang/Runnable;

.field private E:I

.field private F:Lcom/facebook/device/resourcemonitor/DataUsageBytes;

.field public G:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:J

.field public final a:LX/0SG;

.field public final b:Landroid/os/Handler;

.field private final c:Landroid/os/PowerManager;

.field private final d:LX/0ka;

.field private final e:LX/0kv;

.field private final f:LX/0kw;

.field private final g:LX/0Uo;

.field private final h:LX/0kx;

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Landroid/content/Context;

.field private final k:Ljava/util/Random;

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13u;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13t;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0q5;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0mM;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13x;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field private s:LX/0Yb;

.field private t:LX/0Xl;

.field public final u:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final v:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field public w:Ljava/lang/String;

.field public x:Ljava/lang/String;

.field public y:Ljava/lang/String;

.field private z:I


# direct methods
.method private constructor <init>(LX/0SG;Landroid/os/Handler;Landroid/os/PowerManager;LX/0ka;LX/0kv;LX/0kw;LX/0Uo;LX/0kx;Landroid/content/Context;LX/0Or;LX/0Xl;Ljava/util/Random;)V
    .locals 2
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/analytics/config/IsInChatHeadBackgroundGateKeeper;
        .end annotation
    .end param
    .param p11    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbAppBroadcast;
        .end annotation
    .end param
    .param p12    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "Landroid/os/Handler;",
            "Landroid/os/PowerManager;",
            "LX/0ka;",
            "LX/0kv;",
            "LX/0kw;",
            "LX/0Uo;",
            "LX/0kx;",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Xl;",
            "Ljava/util/Random;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 113168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113169
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 113170
    iput-object v0, p0, LX/0gh;->l:LX/0Ot;

    .line 113171
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 113172
    iput-object v0, p0, LX/0gh;->m:LX/0Ot;

    .line 113173
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 113174
    iput-object v0, p0, LX/0gh;->n:LX/0Ot;

    .line 113175
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 113176
    iput-object v0, p0, LX/0gh;->o:LX/0Ot;

    .line 113177
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 113178
    iput-object v0, p0, LX/0gh;->p:LX/0Ot;

    .line 113179
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 113180
    iput-object v0, p0, LX/0gh;->q:LX/0Ot;

    .line 113181
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 113182
    iput-object v0, p0, LX/0gh;->r:LX/0Ot;

    .line 113183
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0gh;->u:Ljava/util/Map;

    .line 113184
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/0gh;->v:Ljava/util/Set;

    .line 113185
    const/4 v0, -0x1

    iput v0, p0, LX/0gh;->z:I

    .line 113186
    iput-boolean v1, p0, LX/0gh;->A:Z

    .line 113187
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0gh;->B:Z

    .line 113188
    const/4 v0, 0x0

    iput-object v0, p0, LX/0gh;->D:Ljava/lang/Runnable;

    .line 113189
    iput v1, p0, LX/0gh;->E:I

    .line 113190
    iput-object p1, p0, LX/0gh;->a:LX/0SG;

    .line 113191
    iput-object p2, p0, LX/0gh;->b:Landroid/os/Handler;

    .line 113192
    iput-object p3, p0, LX/0gh;->c:Landroid/os/PowerManager;

    .line 113193
    iput-object p4, p0, LX/0gh;->d:LX/0ka;

    .line 113194
    iput-object p5, p0, LX/0gh;->e:LX/0kv;

    .line 113195
    iput-object p6, p0, LX/0gh;->f:LX/0kw;

    .line 113196
    iput-object p7, p0, LX/0gh;->g:LX/0Uo;

    .line 113197
    iput-object p8, p0, LX/0gh;->h:LX/0kx;

    .line 113198
    iput-object p9, p0, LX/0gh;->j:Landroid/content/Context;

    .line 113199
    iput-object p10, p0, LX/0gh;->i:LX/0Or;

    .line 113200
    iput-object p11, p0, LX/0gh;->t:LX/0Xl;

    .line 113201
    iput-object p12, p0, LX/0gh;->k:Ljava/util/Random;

    .line 113202
    return-void
.end method

.method private static a(Ljava/lang/Object;Ljava/util/Map;)LX/0P1;
    .locals 3
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation

    .prologue
    .line 113203
    invoke-static {p0}, LX/0gh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 113204
    if-nez v0, :cond_0

    .line 113205
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 113206
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v1, p1}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v1

    const-string v2, "dest_module_class"

    invoke-static {v0}, LX/13w;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_0

    .line 113207
    :cond_1
    sget-object v2, LX/0Rg;->a:LX/0Rg;

    move-object p1, v2

    .line 113208
    goto :goto_1
.end method

.method private static a(Ljava/util/Map;LX/0f1;)LX/0P1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "LX/0f1;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation

    .prologue
    .line 113209
    invoke-interface {p1}, LX/0f1;->b()Ljava/util/Map;

    move-result-object v0

    .line 113210
    if-nez v0, :cond_0

    .line 113211
    invoke-static {p0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    .line 113212
    :goto_0
    return-object v0

    .line 113213
    :cond_0
    if-nez p0, :cond_1

    .line 113214
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    goto :goto_0

    .line 113215
    :cond_1
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/0gh;
    .locals 3

    .prologue
    .line 113216
    sget-object v0, LX/0gh;->I:LX/0gh;

    if-nez v0, :cond_1

    .line 113217
    const-class v1, LX/0gh;

    monitor-enter v1

    .line 113218
    :try_start_0
    sget-object v0, LX/0gh;->I:LX/0gh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 113219
    if-eqz v2, :cond_0

    .line 113220
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/0gh;->b(LX/0QB;)LX/0gh;

    move-result-object v0

    sput-object v0, LX/0gh;->I:LX/0gh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 113221
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 113222
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 113223
    :cond_1
    sget-object v0, LX/0gh;->I:LX/0gh;

    return-object v0

    .line 113224
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 113225
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 113226
    move-object v1, p0

    :goto_0
    instance-of v0, v1, LX/0f4;

    if-nez v0, :cond_0

    instance-of v0, v1, Lcom/facebook/katana/fragment/FbChromeFragment;

    if-eqz v0, :cond_2

    .line 113227
    :cond_0
    instance-of v0, v1, LX/0f4;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 113228
    check-cast v0, LX/0f4;

    invoke-interface {v0}, LX/0f4;->c()Lcom/facebook/base/fragment/FbFragment;

    move-result-object p0

    .line 113229
    :goto_1
    if-eqz p0, :cond_2

    move-object v1, p0

    .line 113230
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 113231
    check-cast v0, Lcom/facebook/katana/fragment/FbChromeFragment;

    .line 113232
    invoke-virtual {v0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object p0

    move-object p0, p0

    .line 113233
    goto :goto_1

    .line 113234
    :cond_2
    return-object v1
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 113235
    const/4 v0, 0x0

    .line 113236
    instance-of v1, p0, LX/0f2;

    if-eqz v1, :cond_0

    .line 113237
    check-cast p0, LX/0f2;

    invoke-interface {p0}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v0

    .line 113238
    :cond_0
    if-eqz v0, :cond_1

    :goto_0
    return-object v0

    :cond_1
    const-string v0, "unknown"

    goto :goto_0
.end method

.method private static a(LX/0gh;LX/0f2;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Z)V
    .locals 14
    .param p0    # LX/0gh;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/0f2;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0f2;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;Z)V"
        }
    .end annotation

    .prologue
    .line 113095
    monitor-enter p0

    .line 113096
    :try_start_0
    move-object/from16 v0, p6

    invoke-direct {p0, v0}, LX/0gh;->c(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    .line 113097
    iget-object v2, p0, LX/0gh;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v8

    .line 113098
    const/4 v4, 0x0

    .line 113099
    if-eqz p1, :cond_10

    .line 113100
    if-eqz p4, :cond_0

    const-string v2, "unknown"

    move-object/from16 v0, p4

    if-ne v0, v2, :cond_f

    .line 113101
    :cond_0
    invoke-interface {p1}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v2

    .line 113102
    :goto_0
    invoke-static {p1, v3}, LX/0gh;->a(Ljava/lang/Object;Ljava/util/Map;)LX/0P1;

    move-result-object v3

    .line 113103
    :goto_1
    iget-object v5, p0, LX/0gh;->h:LX/0kx;

    invoke-virtual {v5}, LX/0kx;->c()LX/148;

    move-result-object v5

    .line 113104
    if-nez p3, :cond_e

    if-eqz v5, :cond_e

    .line 113105
    invoke-virtual {v5}, LX/148;->a()Ljava/lang/String;

    move-result-object p3

    .line 113106
    invoke-virtual {v5}, LX/148;->b()Ljava/lang/String;

    move-result-object v4

    move-object v6, v4

    move-object/from16 v4, p3

    .line 113107
    :goto_2
    const-string v5, "unknown"

    if-ne v4, v5, :cond_1

    .line 113108
    const/4 v4, 0x0

    .line 113109
    :cond_1
    const-string v5, "unknown"

    if-ne v2, v5, :cond_d

    .line 113110
    const/4 v2, 0x0

    move-object v5, v2

    .line 113111
    :goto_3
    iput-object v5, p0, LX/0gh;->G:Ljava/lang/String;

    .line 113112
    invoke-static {}, Lcom/facebook/common/time/RealtimeSinceBootClock;->get()Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v10

    iput-wide v10, p0, LX/0gh;->H:J

    .line 113113
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "navigation"

    invoke-direct {v7, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    if-eqz v4, :cond_a

    move-object v2, v4

    :goto_4
    invoke-virtual {v7, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->f(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v7, "source_module"

    invoke-virtual {v2, v7, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v7, "source_module_class"

    invoke-virtual {v2, v7, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v6, "dest_module"

    invoke-virtual {v2, v6, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v6, "seq"

    iget v7, p0, LX/0gh;->E:I

    invoke-virtual {v2, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 113114
    iget-object v2, p0, LX/0gh;->q:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/13x;

    invoke-virtual {v2, v6}, LX/13x;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    .line 113115
    if-eqz p2, :cond_2

    .line 113116
    iget-object v2, p0, LX/0gh;->e:LX/0kv;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->j(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 113117
    :cond_2
    instance-of v2, p1, LX/13o;

    if-eqz v2, :cond_3

    .line 113118
    move-object v0, p1

    check-cast v0, LX/13o;

    move-object v2, v0

    invoke-interface {v2}, LX/13o;->a()Ljava/lang/String;

    move-result-object v2

    .line 113119
    if-eqz v2, :cond_3

    .line 113120
    const-string v7, "dest_module_uri"

    invoke-virtual {v6, v7, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 113121
    :cond_3
    instance-of v2, p1, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    if-eqz v2, :cond_4

    .line 113122
    move-object v0, p1

    check-cast v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    move-object v2, v0

    invoke-virtual {v2}, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->c()LX/3zk;

    move-result-object v2

    .line 113123
    if-eqz v2, :cond_4

    .line 113124
    invoke-virtual {v2}, LX/3zk;->getTypeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->g(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 113125
    move-object v0, p1

    check-cast v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    move-object v2, v0

    invoke-virtual {v2}, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->h(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 113126
    :cond_4
    instance-of v2, p1, LX/0f1;

    if-eqz v2, :cond_c

    .line 113127
    check-cast p1, LX/0f1;

    invoke-static {v3, p1}, LX/0gh;->a(Ljava/util/Map;LX/0f1;)LX/0P1;

    move-result-object v2

    .line 113128
    :goto_5
    if-eqz p5, :cond_5

    .line 113129
    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->k(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 113130
    :cond_5
    invoke-direct {p0, v2}, LX/0gh;->b(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    .line 113131
    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 113132
    invoke-virtual {v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 113133
    :cond_6
    iget-object v2, p0, LX/0gh;->F:Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    if-eqz v2, :cond_7

    .line 113134
    invoke-direct {p0}, LX/0gh;->n()Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    move-result-object v2

    .line 113135
    const-string v7, "bytes_rx"

    invoke-virtual {v2}, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->a()J

    move-result-wide v10

    iget-object v12, p0, LX/0gh;->F:Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    invoke-virtual {v12}, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->a()J

    move-result-wide v12

    sub-long/2addr v10, v12

    invoke-virtual {v6, v7, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 113136
    const-string v7, "bytes_tx"

    invoke-virtual {v2}, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->b()J

    move-result-wide v10

    iget-object v2, p0, LX/0gh;->F:Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    invoke-virtual {v2}, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->b()J

    move-result-wide v12

    sub-long/2addr v10, v12

    invoke-virtual {v6, v7, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 113137
    const/4 v2, 0x0

    iput-object v2, p0, LX/0gh;->F:Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    .line 113138
    :cond_7
    iget-object v2, p0, LX/0gh;->k:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    rem-int/lit16 v2, v2, 0x3e8

    if-nez v2, :cond_8

    .line 113139
    invoke-direct {p0}, LX/0gh;->n()Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    move-result-object v2

    iput-object v2, p0, LX/0gh;->F:Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    .line 113140
    :cond_8
    iget-object v2, p0, LX/0gh;->m:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-virtual {v6, v8, v9}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(J)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    move-result-object v6

    const-string v7, "resume_upload"

    const-string v8, "1"

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    move-result-object v6

    invoke-interface {v2, v6}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 113141
    if-eqz p7, :cond_b

    .line 113142
    iget-object v2, p0, LX/0gh;->h:LX/0kx;

    invoke-virtual {v2, v4, v5, v3}, LX/0kx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)LX/149;

    move-result-object v2

    .line 113143
    :goto_6
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 113144
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, LX/0gh;->c(Ljava/lang/String;)LX/0gh;

    .line 113145
    iget v3, p0, LX/0gh;->E:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/0gh;->E:I

    .line 113146
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113147
    if-eqz v2, :cond_9

    .line 113148
    invoke-virtual {v2}, LX/149;->a()V

    .line 113149
    :cond_9
    return-void

    .line 113150
    :cond_a
    :try_start_1
    const-string v2, "unknown"

    goto/16 :goto_4

    .line 113151
    :cond_b
    iget-object v2, p0, LX/0gh;->h:LX/0kx;

    invoke-virtual {v2, v4, v5, v3}, LX/0kx;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)LX/149;

    move-result-object v2

    goto :goto_6

    .line 113152
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_c
    move-object v2, v3

    goto/16 :goto_5

    :cond_d
    move-object v5, v2

    goto/16 :goto_3

    :cond_e
    move-object v6, v4

    move-object/from16 v4, p3

    goto/16 :goto_2

    :cond_f
    move-object/from16 v2, p4

    goto/16 :goto_0

    :cond_10
    move-object/from16 v2, p4

    goto/16 :goto_1
.end method

.method public static a(LX/0gh;Landroid/support/v4/app/Fragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 8
    .param p0    # LX/0gh;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Landroid/support/v4/app/Fragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/Fragment;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 113239
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/0f2;

    if-nez v0, :cond_0

    .line 113240
    invoke-static {p1, p6}, LX/0gh;->a(Ljava/lang/Object;Ljava/util/Map;)LX/0P1;

    move-result-object p6

    .line 113241
    const/4 p1, 0x0

    move-object v6, p6

    move-object v1, p1

    .line 113242
    :goto_0
    check-cast v1, LX/0f2;

    const/4 v7, 0x1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v7}, LX/0gh;->a(LX/0gh;LX/0f2;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Z)V

    .line 113243
    return-void

    :cond_0
    move-object v6, p6

    move-object v1, p1

    goto :goto_0
.end method

.method public static a$redex0(LX/0gh;JLX/14j;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 6

    .prologue
    .line 113244
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "app_state"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "state"

    invoke-virtual {p3}, LX/14j;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "app"

    .line 113245
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 113246
    move-object v1, v0

    .line 113247
    iput-wide p1, v1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    .line 113248
    iget-object v0, p0, LX/0gh;->d:LX/0ka;

    invoke-virtual {v0}, LX/0ka;->c()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 113249
    const-string v2, "connection"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 113250
    sget-object v0, LX/14j;->FOREGROUNDED:LX/14j;

    if-ne p3, v0, :cond_0

    iget-object v0, p0, LX/0gh;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v2, 0x9d

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113251
    const-string v0, "upload_this_event_now"

    const-string v2, "true"

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    .line 113252
    :cond_0
    return-object v1

    .line 113253
    :cond_1
    const-string v0, "null"

    goto :goto_0
.end method

.method public static declared-synchronized a$redex0(LX/0gh;J)V
    .locals 5

    .prologue
    .line 113254
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0gh;->B:Z

    if-nez v0, :cond_2

    .line 113255
    const/4 v3, 0x1

    iput-boolean v3, p0, LX/0gh;->B:Z

    .line 113256
    const/4 v3, 0x0

    iput-object v3, p0, LX/0gh;->G:Ljava/lang/String;

    .line 113257
    sget-object v3, Lcom/facebook/common/time/RealtimeSinceBootClock;->a:Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-object v3, v3

    .line 113258
    invoke-virtual {v3}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v3

    iput-wide v3, p0, LX/0gh;->H:J

    .line 113259
    sget-object v0, LX/14j;->BACKGROUNDED:LX/14j;

    .line 113260
    iget-object v1, p0, LX/0gh;->c:Landroid/os/PowerManager;

    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    if-nez v1, :cond_0

    .line 113261
    sget-object v0, LX/14j;->RESIGN:LX/14j;

    .line 113262
    :cond_0
    invoke-static {p0, p1, p2, v0}, LX/0gh;->a$redex0(LX/0gh;JLX/14j;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 113263
    const-string v0, "upload_batch_now"

    const-string v2, "1"

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    .line 113264
    iget-object v0, p0, LX/0gh;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13u;

    iget-object v2, p0, LX/0gh;->n:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {v0, v1}, LX/13u;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 113265
    iget-object v0, p0, LX/0gh;->w:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 113266
    const-string v0, "click_point"

    iget-object v2, p0, LX/0gh;->w:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 113267
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 113268
    :cond_1
    invoke-static {p0, v1}, LX/0gh;->b$redex0(LX/0gh;Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 113269
    iget-object v0, p0, LX/0gh;->g:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->o()Z

    move-result v0

    if-nez v0, :cond_2

    .line 113270
    iget-object v0, p0, LX/0gh;->h:LX/0kx;

    invoke-virtual {v0}, LX/0kx;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113271
    :cond_2
    monitor-exit p0

    return-void

    .line 113272
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(LX/0QB;)LX/0gh;
    .locals 13

    .prologue
    .line 113273
    new-instance v0, LX/0gh;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v2

    check-cast v2, Landroid/os/Handler;

    invoke-static {p0}, LX/0kZ;->b(LX/0QB;)Landroid/os/PowerManager;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    invoke-static {p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v4

    check-cast v4, LX/0ka;

    invoke-static {p0}, LX/0kv;->a(LX/0QB;)LX/0kv;

    move-result-object v5

    check-cast v5, LX/0kv;

    .line 113274
    new-instance v7, LX/0kw;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v7, v6}, LX/0kw;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 113275
    move-object v6, v7

    .line 113276
    check-cast v6, LX/0kw;

    invoke-static {p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v7

    check-cast v7, LX/0Uo;

    invoke-static {p0}, LX/0kx;->a(LX/0QB;)LX/0kx;

    move-result-object v8

    check-cast v8, LX/0kx;

    const-class v9, Landroid/content/Context;

    invoke-interface {p0, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    const/16 v10, 0x143e

    invoke-static {p0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {p0}, LX/0aQ;->a(LX/0QB;)LX/0aQ;

    move-result-object v11

    check-cast v11, LX/0Xl;

    invoke-static {p0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v12

    check-cast v12, Ljava/util/Random;

    invoke-direct/range {v0 .. v12}, LX/0gh;-><init>(LX/0SG;Landroid/os/Handler;Landroid/os/PowerManager;LX/0ka;LX/0kv;LX/0kw;LX/0Uo;LX/0kx;Landroid/content/Context;LX/0Or;LX/0Xl;Ljava/util/Random;)V

    .line 113277
    const/16 v1, 0xd0

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0xbc

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x8c

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x4bc

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x81

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xc0

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xac0

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    .line 113278
    iput-object v1, v0, LX/0gh;->l:LX/0Ot;

    iput-object v2, v0, LX/0gh;->m:LX/0Ot;

    iput-object v3, v0, LX/0gh;->n:LX/0Ot;

    iput-object v4, v0, LX/0gh;->o:LX/0Ot;

    iput-object v5, v0, LX/0gh;->p:LX/0Ot;

    iput-object v6, v0, LX/0gh;->q:LX/0Ot;

    iput-object v7, v0, LX/0gh;->r:LX/0Ot;

    .line 113279
    return-object v0
.end method

.method private b(Ljava/util/Map;)Ljava/util/Map;
    .locals 3
    .param p1    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation

    .prologue
    .line 113280
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 113281
    if-eqz p1, :cond_0

    .line 113282
    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 113283
    :cond_0
    const-string v1, "click_point"

    .line 113284
    iget-object v2, p0, LX/0gh;->w:Ljava/lang/String;

    move-object v2, v2

    .line 113285
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113286
    const-string v1, "last_tracking_code"

    .line 113287
    iget-object v2, p0, LX/0gh;->y:Ljava/lang/String;

    move-object v2, v2

    .line 113288
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113289
    return-object v0
.end method

.method public static b$redex0(LX/0gh;Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    .locals 1

    .prologue
    .line 113290
    iget-object v0, p0, LX/0gh;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0mM;

    invoke-virtual {v0}, LX/0mM;->b()V

    .line 113291
    iget-object v0, p0, LX/0gh;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 113292
    return-void
.end method

.method private c(Ljava/util/Map;)Ljava/util/Map;
    .locals 2
    .param p1    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation

    .prologue
    .line 112980
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 112981
    iget-object v1, p0, LX/0gh;->u:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 112982
    if-eqz p1, :cond_0

    .line 112983
    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 112984
    :cond_0
    return-object v0
.end method

.method private k()Z
    .locals 1

    .prologue
    .line 113293
    iget-object v0, p0, LX/0gh;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private declared-synchronized m()V
    .locals 6

    .prologue
    .line 113294
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0gh;->B:Z

    if-eqz v0, :cond_1

    .line 113295
    const/4 v4, 0x0

    iput-boolean v4, p0, LX/0gh;->B:Z

    .line 113296
    sget-object v4, Lcom/facebook/common/time/RealtimeSinceBootClock;->a:Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-object v4, v4

    .line 113297
    invoke-virtual {v4}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    iput-wide v4, p0, LX/0gh;->H:J

    .line 113298
    iget-object v0, p0, LX/0gh;->w:Ljava/lang/String;

    move-object v0, v0

    .line 113299
    if-nez v0, :cond_0

    .line 113300
    const-string v0, "foreground"

    invoke-virtual {p0, v0}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 113301
    :cond_0
    iget-object v0, p0, LX/0gh;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 113302
    iget-object v2, p0, LX/0gh;->b:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/analytics/NavigationLogger$3;

    invoke-direct {v3, p0, v0, v1}, Lcom/facebook/analytics/NavigationLogger$3;-><init>(LX/0gh;J)V

    const v0, -0x7a7dffb2

    invoke-static {v2, v3, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113303
    :cond_1
    monitor-exit p0

    return-void

    .line 113304
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private n()Lcom/facebook/device/resourcemonitor/DataUsageBytes;
    .locals 3

    .prologue
    .line 113305
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    .line 113306
    iget-object v0, p0, LX/0gh;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0q5;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0q5;->a(II)Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0gh;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 113307
    if-nez p1, :cond_0

    .line 113308
    iget-object v0, p0, LX/0gh;->w:Ljava/lang/String;

    iput-object v0, p0, LX/0gh;->x:Ljava/lang/String;

    .line 113309
    :goto_0
    iput-object p1, p0, LX/0gh;->w:Ljava/lang/String;

    .line 113310
    return-object p0

    .line 113311
    :cond_0
    iput-object p1, p0, LX/0gh;->x:Ljava/lang/String;

    goto :goto_0
.end method

.method public final declared-synchronized a(IZ)V
    .locals 4

    .prologue
    .line 113312
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0gh;->h:LX/0kx;

    const-string v1, "unknown"

    invoke-virtual {v0, v1}, LX/0kx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 113313
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "orientation"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "module"

    const-string v3, "device"

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "containermodule"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "orientation_start"

    iget v2, p0, LX/0gh;->z:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "orientation_end"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "event_trigger"

    if-eqz p2, :cond_0

    const-string v0, "foreground"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 113314
    iget-object v0, p0, LX/0gh;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 113315
    iput p1, p0, LX/0gh;->z:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113316
    monitor-exit p0

    return-void

    .line 113317
    :cond_0
    :try_start_1
    const-string v0, "orientation_change"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 113318
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/0f2;Ljava/util/Map;)V
    .locals 8
    .param p1    # LX/0f2;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0f2;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 113153
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 113154
    if-eqz p1, :cond_2

    .line 113155
    invoke-interface {p1}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v3

    .line 113156
    invoke-static {p1}, LX/0gh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/13w;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 113157
    :goto_0
    if-eqz v0, :cond_0

    .line 113158
    const-string v1, "source_module_class"

    invoke-interface {v6, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113159
    :cond_0
    if-eqz p2, :cond_1

    .line 113160
    invoke-interface {v6, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 113161
    :cond_1
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v2

    invoke-static/range {v0 .. v7}, LX/0gh;->a(LX/0gh;LX/0f2;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Z)V

    .line 113162
    return-void

    .line 113163
    :cond_2
    iget-object v0, p0, LX/0gh;->h:LX/0kx;

    invoke-virtual {v0}, LX/0kx;->c()LX/148;

    move-result-object v0

    .line 113164
    if-eqz v0, :cond_3

    .line 113165
    iget-object v1, v0, LX/148;->a:Ljava/lang/String;

    move-object v3, v1

    .line 113166
    iget-object v1, v0, LX/148;->b:Ljava/lang/String;

    move-object v0, v1

    .line 113167
    const-string v1, "dest_module_class"

    invoke-interface {v6, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    move-object v0, v2

    move-object v3, v2

    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 112958
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, LX/0gh;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 112959
    return-void
.end method

.method public final a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 8
    .param p1    # Landroid/app/Activity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 112960
    if-nez p5, :cond_1

    .line 112961
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v6, v0

    .line 112962
    :goto_0
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/0f2;

    if-nez v0, :cond_0

    .line 112963
    invoke-static {p1, v6}, LX/0gh;->a(Ljava/lang/Object;Ljava/util/Map;)LX/0P1;

    move-result-object v6

    .line 112964
    const/4 p1, 0x0

    move-object v2, p1

    :goto_1
    move-object v1, v2

    .line 112965
    check-cast v1, LX/0f2;

    const/4 v7, 0x1

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v7}, LX/0gh;->a(LX/0gh;LX/0f2;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Z)V

    .line 112966
    return-void

    :cond_0
    move-object v2, p1

    goto :goto_1

    :cond_1
    move-object v6, p5

    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;Ljava/util/Map;)V
    .locals 6
    .param p1    # Landroid/app/Activity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 112967
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, LX/0gh;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 112968
    return-void
.end method

.method public final declared-synchronized a(Landroid/content/Context;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 3

    .prologue
    .line 112969
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0gh;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 112970
    iput-wide v0, p2, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    .line 112971
    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 112972
    check-cast p1, Landroid/app/Activity;

    .line 112973
    iget-object v0, p0, LX/0gh;->e:LX/0kv;

    invoke-virtual {v0, p1}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 112974
    iput-object v0, p2, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 112975
    :cond_0
    iget-object v0, p0, LX/0gh;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, p2}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112976
    monitor-exit p0

    return-void

    .line 112977
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 112978
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-static/range {v0 .. v6}, LX/0gh;->a(LX/0gh;Landroid/support/v4/app/Fragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 112979
    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 112985
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0gh;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p1, p2, p3, p4}, LX/3zi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112986
    monitor-exit p0

    return-void

    .line 112987
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 9
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 112988
    iget-object v0, p0, LX/0gh;->h:LX/0kx;

    invoke-virtual {v0, p1, p2}, LX/0kx;->b(Ljava/lang/String;Ljava/util/Map;)LX/149;

    .line 112989
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 112990
    iget-object v0, p0, LX/0gh;->h:LX/0kx;

    invoke-virtual {v0}, LX/0kx;->c()LX/148;

    move-result-object v0

    .line 112991
    if-eqz v0, :cond_2

    .line 112992
    iget-object v2, v0, LX/148;->a:Ljava/lang/String;

    move-object v4, v2

    .line 112993
    const-string v2, "dest_module_class"

    .line 112994
    iget-object v3, v0, LX/148;->b:Ljava/lang/String;

    move-object v3, v3

    .line 112995
    invoke-interface {v6, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112996
    iget-object v2, v0, LX/148;->c:Ljava/util/Map;

    move-object v2, v2

    .line 112997
    if-eqz v2, :cond_0

    .line 112998
    iget-object v2, v0, LX/148;->c:Ljava/util/Map;

    move-object v0, v2

    .line 112999
    invoke-interface {v6, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 113000
    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    .line 113001
    invoke-interface {v6, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 113002
    :cond_1
    iget-object v0, p0, LX/0gh;->h:LX/0kx;

    invoke-virtual {v0}, LX/0kx;->a()Ljava/util/Stack;

    move-result-object v8

    .line 113003
    const/4 v7, 0x0

    move-object v0, p0

    move-object v2, v1

    move-object v3, p1

    move-object v5, v1

    invoke-static/range {v0 .. v7}, LX/0gh;->a(LX/0gh;LX/0f2;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Z)V

    .line 113004
    iget-object v0, p0, LX/0gh;->h:LX/0kx;

    invoke-virtual {v0, v8}, LX/0kx;->a(Ljava/util/Stack;)V

    .line 113005
    return-void

    :cond_2
    move-object v4, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 113006
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/0gh;->a(Ljava/lang/String;ZLjava/util/Map;)V

    .line 113007
    return-void
.end method

.method public final a(Ljava/lang/String;ZLjava/util/Map;)V
    .locals 9
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 113008
    iget-object v0, p0, LX/0gh;->h:LX/0kx;

    invoke-virtual {v0}, LX/0kx;->c()LX/148;

    move-result-object v0

    .line 113009
    if-eqz v0, :cond_1

    .line 113010
    iget-object v2, v0, LX/148;->a:Ljava/lang/String;

    move-object v3, v2

    .line 113011
    iget-object v2, v0, LX/148;->b:Ljava/lang/String;

    move-object v0, v2

    .line 113012
    :goto_0
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 113013
    const-string v2, "is_modal"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v6, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113014
    const-string v2, "source_module_class"

    invoke-interface {v6, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113015
    if-eqz p3, :cond_0

    .line 113016
    invoke-interface {v6, p3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 113017
    :cond_0
    iget-object v0, p0, LX/0gh;->h:LX/0kx;

    invoke-virtual {v0}, LX/0kx;->a()Ljava/util/Stack;

    move-result-object v8

    .line 113018
    const/4 v7, 0x0

    move-object v0, p0

    move-object v2, v1

    move-object v4, p1

    move-object v5, v1

    invoke-static/range {v0 .. v7}, LX/0gh;->a(LX/0gh;LX/0f2;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Z)V

    .line 113019
    iget-object v0, p0, LX/0gh;->h:LX/0kx;

    invoke-virtual {v0, v8}, LX/0kx;->a(Ljava/util/Stack;)V

    .line 113020
    iget-object v0, p0, LX/0gh;->h:LX/0kx;

    invoke-virtual {v0, p1, p3}, LX/0kx;->a(Ljava/lang/String;Ljava/util/Map;)LX/149;

    .line 113021
    return-void

    :cond_1
    move-object v0, v1

    move-object v3, v1

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 113022
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0gh;->f:LX/0kw;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0kw;->a(Z)V

    .line 113023
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "log_in"

    invoke-direct {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 113024
    const-string v0, "resume_upload"

    const-string v1, "1"

    invoke-virtual {v2, v0, v1}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    .line 113025
    if-eqz p1, :cond_0

    .line 113026
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 113027
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 113028
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 113029
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0gh;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 113030
    monitor-exit p0

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113031
    iget-object v0, p0, LX/0gh;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 113032
    monitor-enter p0

    .line 113033
    :try_start_0
    iget-object v0, p0, LX/0gh;->v:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 113034
    iget-object v0, p0, LX/0gh;->D:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 113035
    iget-object v0, p0, LX/0gh;->b:Landroid/os/Handler;

    iget-object v1, p0, LX/0gh;->D:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 113036
    const/4 v0, 0x0

    iput-object v0, p0, LX/0gh;->D:Ljava/lang/Runnable;

    .line 113037
    :cond_0
    invoke-direct {p0}, LX/0gh;->m()V

    .line 113038
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113039
    instance-of v0, p1, LX/13v;

    if-nez v0, :cond_1

    .line 113040
    invoke-virtual {p0, p1}, LX/0gh;->a(Landroid/app/Activity;)V

    .line 113041
    :cond_1
    iget-object v0, p0, LX/0gh;->j:Landroid/content/Context;

    const v1, 0x7f080011

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "messenger"

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 113042
    :cond_2
    :goto_0
    return-void

    .line 113043
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 113044
    :cond_3
    iget-object v0, p0, LX/0gh;->s:LX/0Yb;

    if-nez v0, :cond_4

    .line 113045
    iget-object v0, p0, LX/0gh;->t:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "chat_heads_status_change"

    new-instance v2, LX/14M;

    invoke-direct {v2, p0}, LX/14M;-><init>(LX/0gh;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/0gh;->s:LX/0Yb;

    .line 113046
    :cond_4
    invoke-direct {p0}, LX/0gh;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 113047
    iget-object v0, p0, LX/0gh;->s:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    goto :goto_0
.end method

.method public final declared-synchronized b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 113048
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0gh;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p1, p2, p3, p4}, LX/3zi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113049
    monitor-exit p0

    return-void

    .line 113050
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c(Ljava/lang/String;)LX/0gh;
    .locals 0

    .prologue
    .line 113051
    iput-object p1, p0, LX/0gh;->y:Ljava/lang/String;

    .line 113052
    return-object p0
.end method

.method public final declared-synchronized c(Landroid/app/Activity;)V
    .locals 13

    .prologue
    .line 113053
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0gh;->v:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0gh;->v:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 113054
    iget-object v0, p0, LX/0gh;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 113055
    iget-object v2, p0, LX/0gh;->D:Ljava/lang/Runnable;

    if-eqz v2, :cond_0

    .line 113056
    const-string v2, "NavigationLogger"

    const-string v3, "Previous sendToBackgroundDetector is still alive"

    invoke-static {v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 113057
    iget-object v2, p0, LX/0gh;->b:Landroid/os/Handler;

    iget-object v3, p0, LX/0gh;->D:Ljava/lang/Runnable;

    invoke-static {v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 113058
    const/4 v2, 0x0

    iput-object v2, p0, LX/0gh;->D:Ljava/lang/Runnable;

    .line 113059
    :cond_0
    iget-boolean v2, p0, LX/0gh;->C:Z

    if-nez v2, :cond_1

    .line 113060
    new-instance v2, Lcom/facebook/analytics/NavigationLogger$2;

    invoke-direct {v2, p0, v0, v1}, Lcom/facebook/analytics/NavigationLogger$2;-><init>(LX/0gh;J)V

    iput-object v2, p0, LX/0gh;->D:Ljava/lang/Runnable;

    .line 113061
    iget-object v2, p0, LX/0gh;->b:Landroid/os/Handler;

    iget-object v3, p0, LX/0gh;->D:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1388

    const v6, -0x4467581b

    invoke-static {v2, v3, v4, v5, v6}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 113062
    :cond_1
    iget-object v2, p0, LX/0gh;->e:LX/0kv;

    invoke-virtual {v2, p1}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 113063
    iget-boolean v3, p0, LX/0gh;->C:Z

    if-eqz v3, :cond_2

    .line 113064
    new-instance v8, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "session_end"

    invoke-direct {v8, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 113065
    iput-object v2, v8, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 113066
    move-object v7, v8

    .line 113067
    const-string v9, "session_timeout"

    const-string v10, "1"

    invoke-virtual {v7, v9, v10}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    move-result-object v7

    const-string v9, "stop_upload"

    const-string v10, "1"

    invoke-virtual {v7, v9, v10}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    move-result-object v7

    .line 113068
    iput-wide v0, v7, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    .line 113069
    iget-object v7, p0, LX/0gh;->l:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/13u;

    iget-object v9, p0, LX/0gh;->n:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {v7, v8}, LX/13u;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 113070
    iget-object v7, p0, LX/0gh;->m:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-interface {v7, v8}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 113071
    iget-object v7, p0, LX/0gh;->n:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/13t;

    const/4 v8, 0x1

    .line 113072
    iput-boolean v8, v7, LX/13t;->j:Z

    .line 113073
    const/4 v0, 0x0

    .line 113074
    iput-boolean v0, p0, LX/0gh;->C:Z

    .line 113075
    :cond_2
    invoke-direct {p0}, LX/0gh;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/0gh;->s:LX/0Yb;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/0gh;->s:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 113076
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0gh;->A:Z

    .line 113077
    iget-object v0, p0, LX/0gh;->s:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113078
    :cond_3
    monitor-exit p0

    return-void

    .line 113079
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 113080
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/0gh;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 113081
    return-void
.end method

.method public final declared-synchronized f()V
    .locals 3

    .prologue
    .line 113082
    monitor-enter p0

    const/4 v0, 0x1

    .line 113083
    :try_start_0
    iput-boolean v0, p0, LX/0gh;->C:Z

    .line 113084
    iget-object v0, p0, LX/0gh;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "log_out"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 113085
    iget-object v0, p0, LX/0gh;->f:LX/0kw;

    .line 113086
    iget-object v1, v0, LX/0kw;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0uQ;->g:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113087
    monitor-exit p0

    return-void

    .line 113088
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()V
    .locals 4

    .prologue
    .line 113089
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0gh;->f:LX/0kw;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0kw;->a(Z)V

    .line 113090
    const/4 v0, 0x0

    .line 113091
    iput-boolean v0, p0, LX/0gh;->C:Z

    .line 113092
    iget-object v0, p0, LX/0gh;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "silent_login"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "resume_upload"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113093
    monitor-exit p0

    return-void

    .line 113094
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
