.class public final enum LX/0uO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0uO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0uO;

.field public static final enum ALREADY_SCHEDULED:LX/0uO;

.field public static final enum END_OF_CACHED_FEED:LX/0uO;

.field public static final enum END_OF_FEED:LX/0uO;

.field public static final enum SUCCESS:LX/0uO;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 156302
    new-instance v0, LX/0uO;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, LX/0uO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0uO;->SUCCESS:LX/0uO;

    .line 156303
    new-instance v0, LX/0uO;

    const-string v1, "ALREADY_SCHEDULED"

    invoke-direct {v0, v1, v3}, LX/0uO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0uO;->ALREADY_SCHEDULED:LX/0uO;

    .line 156304
    new-instance v0, LX/0uO;

    const-string v1, "END_OF_FEED"

    invoke-direct {v0, v1, v4}, LX/0uO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0uO;->END_OF_FEED:LX/0uO;

    .line 156305
    new-instance v0, LX/0uO;

    const-string v1, "END_OF_CACHED_FEED"

    invoke-direct {v0, v1, v5}, LX/0uO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0uO;->END_OF_CACHED_FEED:LX/0uO;

    .line 156306
    const/4 v0, 0x4

    new-array v0, v0, [LX/0uO;

    sget-object v1, LX/0uO;->SUCCESS:LX/0uO;

    aput-object v1, v0, v2

    sget-object v1, LX/0uO;->ALREADY_SCHEDULED:LX/0uO;

    aput-object v1, v0, v3

    sget-object v1, LX/0uO;->END_OF_FEED:LX/0uO;

    aput-object v1, v0, v4

    sget-object v1, LX/0uO;->END_OF_CACHED_FEED:LX/0uO;

    aput-object v1, v0, v5

    sput-object v0, LX/0uO;->$VALUES:[LX/0uO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 156307
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0uO;
    .locals 1

    .prologue
    .line 156308
    const-class v0, LX/0uO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0uO;

    return-object v0
.end method

.method public static values()[LX/0uO;
    .locals 1

    .prologue
    .line 156309
    sget-object v0, LX/0uO;->$VALUES:[LX/0uO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0uO;

    return-object v0
.end method
