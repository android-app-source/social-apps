.class public abstract LX/13D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/13E;
.implements LX/13F;
.implements LX/13G;
.implements LX/13H;


# instance fields
.field public final a:LX/13K;


# direct methods
.method public constructor <init>(LX/13J;)V
    .locals 13

    .prologue
    .line 176100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176101
    new-instance v1, LX/13K;

    invoke-static {p1}, LX/13L;->b(LX/0QB;)LX/13L;

    move-result-object v3

    check-cast v3, LX/13H;

    const/16 v2, 0x1058

    invoke-static {p1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p1}, LX/13M;->b(LX/0QB;)LX/13M;

    move-result-object v5

    check-cast v5, LX/13H;

    const/16 v2, 0x1057

    invoke-static {p1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v2, 0x104c

    invoke-static {p1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    const/16 v2, 0x1052

    invoke-static {p1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {p1}, LX/13N;->a(LX/0QB;)LX/13N;

    move-result-object v11

    check-cast v11, LX/13N;

    const/16 v2, 0x3039

    invoke-static {p1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    move-object v2, p0

    invoke-direct/range {v1 .. v12}, LX/13K;-><init>(LX/13D;LX/13H;LX/0Ot;LX/13H;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0SG;LX/0Ot;LX/13N;LX/0Ot;)V

    .line 176102
    move-object v0, v1

    .line 176103
    iput-object v0, p0, LX/13D;->a:LX/13K;

    .line 176104
    return-void
.end method

.method public static b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)LX/1Y7;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 176191
    new-instance v0, LX/1Y8;

    invoke-direct {v0, v4}, LX/1Y8;-><init>(Z)V

    const-string v1, "Invalid template for promotion %s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 176192
    iput-object v1, v0, LX/1Y8;->e:Ljava/lang/String;

    .line 176193
    move-object v0, v0

    .line 176194
    invoke-virtual {v0}, LX/1Y8;->a()LX/1Y7;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 176190
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final a(ILandroid/content/Intent;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Intent;",
            ")",
            "LX/0am",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176189
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 176182
    iget-object v0, p0, LX/13D;->a:LX/13K;

    .line 176183
    invoke-static {v0, p1}, LX/13K;->d(LX/13K;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    move-result-object p0

    iput-object p0, v0, LX/13K;->q:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 176184
    iget-object p0, v0, LX/13K;->q:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    if-eqz p0, :cond_0

    .line 176185
    iput-object p1, v0, LX/13K;->r:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 176186
    sget-object p0, LX/10S;->ELIGIBLE:LX/10S;

    .line 176187
    :goto_0
    move-object v0, p0

    .line 176188
    return-object v0

    :cond_0
    sget-object p0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/1Y7;
    .locals 3
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 176156
    invoke-virtual {p0}, LX/13D;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176157
    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v0

    .line 176158
    invoke-virtual {p0}, LX/13D;->l()Ljava/util/Set;

    move-result-object v1

    .line 176159
    iget-object p2, p0, LX/13D;->a:LX/13K;

    .line 176160
    iget-object p0, p2, LX/13K;->i:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/2hf;

    .line 176161
    iget-object p2, p0, LX/2hf;->a:LX/0P1;

    invoke-virtual {p2, v0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result p2

    move p0, p2

    .line 176162
    move p2, p0

    .line 176163
    if-eqz p2, :cond_4

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 176164
    :cond_0
    sget-object v0, LX/1Y7;->a:LX/1Y7;

    .line 176165
    :goto_0
    move-object v0, v0

    .line 176166
    :goto_1
    return-object v0

    .line 176167
    :cond_1
    invoke-virtual {p0}, LX/13D;->l()Ljava/util/Set;

    move-result-object v0

    .line 176168
    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 176169
    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v0

    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->CUSTOM_RENDERED:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, LX/13D;->a:LX/13K;

    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->g()Lcom/facebook/quickpromotion/customrender/CustomRenderType;

    move-result-object v1

    .line 176170
    iget-object p0, v0, LX/13K;->l:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/76X;

    .line 176171
    sget-object v0, Lcom/facebook/quickpromotion/customrender/CustomRenderType;->UNKNOWN:Lcom/facebook/quickpromotion/customrender/CustomRenderType;

    if-eq v1, v0, :cond_5

    iget-object v0, p0, LX/76X;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    move p0, v0

    .line 176172
    move v0, p0

    .line 176173
    if-nez v0, :cond_2

    .line 176174
    const/4 p2, 0x0

    .line 176175
    new-instance v0, LX/1Y8;

    invoke-direct {v0, p2}, LX/1Y8;-><init>(Z)V

    const-string v1, "Invalid custom render type for promotion %s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object p0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    aput-object p0, v2, p2

    const/4 p0, 0x1

    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->g()Lcom/facebook/quickpromotion/customrender/CustomRenderType;

    move-result-object p2

    aput-object p2, v2, p0

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 176176
    iput-object v1, v0, LX/1Y8;->e:Ljava/lang/String;

    .line 176177
    move-object v0, v0

    .line 176178
    invoke-virtual {v0}, LX/1Y8;->a()LX/1Y7;

    move-result-object v0

    move-object v0, v0

    .line 176179
    goto :goto_1

    .line 176180
    :cond_2
    sget-object v0, LX/1Y7;->a:LX/1Y7;

    goto :goto_1

    .line 176181
    :cond_3
    invoke-static {p1}, LX/13D;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)LX/1Y7;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-static {p1}, LX/13D;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)LX/1Y7;

    move-result-object v0

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 176154
    invoke-virtual {p0, p1}, LX/13D;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 176155
    iget-object v1, p0, LX/13D;->a:LX/13K;

    invoke-virtual {v1, v0}, LX/13K;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 2

    .prologue
    .line 176151
    iget-object v0, p0, LX/13D;->a:LX/13K;

    .line 176152
    iput-wide p1, v0, LX/13K;->p:J

    .line 176153
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 8

    .prologue
    .line 176121
    check-cast p1, Lcom/facebook/quickpromotion/protocol/QuickPromotionDefinitionsFetchResult;

    .line 176122
    iget-object v0, p0, LX/13D;->a:LX/13K;

    const/4 v4, 0x0

    .line 176123
    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/facebook/quickpromotion/protocol/QuickPromotionDefinitionsFetchResult;->mQuickPromotionDefinitions:Ljava/util/List;

    if-nez v1, :cond_1

    .line 176124
    :cond_0
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 176125
    iput-object v1, v0, LX/13K;->m:LX/0Px;

    .line 176126
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 176127
    iput-object v1, v0, LX/13K;->o:LX/0Px;

    .line 176128
    :goto_0
    invoke-virtual {p0}, LX/13D;->h()V

    .line 176129
    return-void

    .line 176130
    :cond_1
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 176131
    iget-object v1, v0, LX/13K;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/13P;

    .line 176132
    iget-object v2, p1, Lcom/facebook/quickpromotion/protocol/QuickPromotionDefinitionsFetchResult;->mQuickPromotionDefinitions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v3, v4

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 176133
    iget-object v5, v0, LX/13K;->c:LX/13H;

    invoke-interface {v5, v2, v4}, LX/13H;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/1Y7;

    move-result-object v5

    iget-boolean v5, v5, LX/1Y7;->c:Z

    .line 176134
    if-eqz v5, :cond_2

    .line 176135
    const-string v5, "client_controller_validator"

    invoke-virtual {v1, v2, v5}, LX/13P;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;)V

    .line 176136
    iget-object v5, v0, LX/13K;->b:LX/13D;

    invoke-virtual {v5, v2, v4}, LX/13D;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/1Y7;

    move-result-object v5

    iget-boolean v5, v5, LX/1Y7;->c:Z

    .line 176137
    :cond_2
    if-eqz v5, :cond_3

    .line 176138
    const-string v5, "client_promotion_valid"

    invoke-virtual {v1, v2, v5}, LX/13P;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;)V

    .line 176139
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 176140
    :cond_3
    if-nez v3, :cond_4

    .line 176141
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 176142
    :cond_4
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 176143
    :cond_5
    new-instance v1, LX/1Y9;

    invoke-direct {v1, v0}, LX/1Y9;-><init>(LX/13K;)V

    invoke-static {v6, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 176144
    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/13K;->m:LX/0Px;

    .line 176145
    if-eqz v3, :cond_6

    .line 176146
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/13K;->n:LX/0Px;

    .line 176147
    :cond_6
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 176148
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 176149
    invoke-virtual {v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 176150
    :cond_7
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/13K;->o:LX/0Px;

    goto/16 :goto_0
.end method

.method public abstract b(Landroid/content/Context;)Landroid/content/Intent;
.end method

.method public final b(Lcom/facebook/interstitial/manager/InterstitialTrigger;)V
    .locals 1

    .prologue
    .line 176119
    iget-object v0, p0, LX/13D;->a:LX/13K;

    invoke-virtual {v0, p1}, LX/13K;->c(Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    .line 176120
    return-void
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176116
    iget-object v0, p0, LX/13D;->a:LX/13K;

    .line 176117
    iget-object p0, v0, LX/13K;->o:LX/0Px;

    move-object v0, p0

    .line 176118
    return-object v0
.end method

.method public final d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176115
    const-class v0, Lcom/facebook/quickpromotion/protocol/QuickPromotionDefinitionsFetchResult;

    return-object v0
.end method

.method public abstract f()J
.end method

.method public abstract g()Ljava/lang/String;
.end method

.method public h()V
    .locals 0

    .prologue
    .line 176114
    return-void
.end method

.method public final i()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176111
    iget-object v0, p0, LX/13D;->a:LX/13K;

    .line 176112
    iget-object p0, v0, LX/13K;->m:LX/0Px;

    move-object v0, p0

    .line 176113
    return-object v0
.end method

.method public final j()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176108
    iget-object v0, p0, LX/13D;->a:LX/13K;

    .line 176109
    iget-object p0, v0, LX/13K;->n:LX/0Px;

    move-object v0, p0

    .line 176110
    return-object v0
.end method

.method public l()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176106
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 176107
    return-object v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 176105
    const/4 v0, 0x0

    return v0
.end method
