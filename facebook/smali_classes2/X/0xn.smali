.class public final enum LX/0xn;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0xn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0xn;

.field public static final enum CUSTOM_ABR:LX/0xn;

.field public static final enum MANUAL:LX/0xn;

.field public static final enum RANDOM_ABR:LX/0xn;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 163474
    new-instance v0, LX/0xn;

    const-string v1, "MANUAL"

    invoke-direct {v0, v1, v2}, LX/0xn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0xn;->MANUAL:LX/0xn;

    .line 163475
    new-instance v0, LX/0xn;

    const-string v1, "CUSTOM_ABR"

    invoke-direct {v0, v1, v3}, LX/0xn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0xn;->CUSTOM_ABR:LX/0xn;

    .line 163476
    new-instance v0, LX/0xn;

    const-string v1, "RANDOM_ABR"

    invoke-direct {v0, v1, v4}, LX/0xn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0xn;->RANDOM_ABR:LX/0xn;

    .line 163477
    const/4 v0, 0x3

    new-array v0, v0, [LX/0xn;

    sget-object v1, LX/0xn;->MANUAL:LX/0xn;

    aput-object v1, v0, v2

    sget-object v1, LX/0xn;->CUSTOM_ABR:LX/0xn;

    aput-object v1, v0, v3

    sget-object v1, LX/0xn;->RANDOM_ABR:LX/0xn;

    aput-object v1, v0, v4

    sput-object v0, LX/0xn;->$VALUES:[LX/0xn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 163478
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static of(Ljava/lang/String;)LX/0xn;
    .locals 1

    .prologue
    .line 163479
    :try_start_0
    invoke-static {p0}, LX/0xn;->valueOf(Ljava/lang/String;)LX/0xn;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 163480
    :goto_0
    return-object v0

    :catch_0
    sget-object v0, LX/0xn;->MANUAL:LX/0xn;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0xn;
    .locals 1

    .prologue
    .line 163481
    const-class v0, LX/0xn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0xn;

    return-object v0
.end method

.method public static values()[LX/0xn;
    .locals 1

    .prologue
    .line 163482
    sget-object v0, LX/0xn;->$VALUES:[LX/0xn;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0xn;

    return-object v0
.end method
