.class public final LX/0PP;
.super LX/0PO;
.source ""


# instance fields
.field public final synthetic this$0:LX/0PO;

.field public final synthetic val$nullText:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0PO;LX/0PO;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 56217
    iput-object p1, p0, LX/0PP;->this$0:LX/0PO;

    iput-object p3, p0, LX/0PP;->val$nullText:Ljava/lang/String;

    invoke-direct {p0, p2}, LX/0PO;-><init>(LX/0PO;)V

    return-void
.end method


# virtual methods
.method public skipNulls()LX/0PO;
    .locals 2

    .prologue
    .line 56216
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "already specified useForNull"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 56218
    if-nez p1, :cond_0

    iget-object v0, p0, LX/0PP;->val$nullText:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0PP;->this$0:LX/0PO;

    invoke-virtual {v0, p1}, LX/0PO;->toString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public useForNull(Ljava/lang/String;)LX/0PO;
    .locals 2

    .prologue
    .line 56215
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "already specified useForNull"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
