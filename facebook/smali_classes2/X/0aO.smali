.class public final LX/0aO;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/0aa;

.field private static b:LX/0aa;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 84700
    sput-object v0, LX/0aO;->a:LX/0aa;

    .line 84701
    sput-object v0, LX/0aO;->b:LX/0aa;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 84698
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84699
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)LX/0aa;
    .locals 3

    .prologue
    .line 84670
    const-class v1, LX/0aO;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/0aO;->a:LX/0aa;

    if-nez v0, :cond_0

    .line 84671
    const-string v0, "sessionless_index.bin"

    const/16 v2, 0x2b4

    invoke-static {p0, v0, v2}, LX/0aO;->a(Landroid/content/Context;Ljava/lang/String;I)[B

    move-result-object v0

    .line 84672
    new-instance v2, LX/0ab;

    invoke-direct {v2, v0}, LX/0ab;-><init>([B)V

    sput-object v2, LX/0aO;->a:LX/0aa;

    .line 84673
    :cond_0
    sget-object v0, LX/0aO;->a:LX/0aa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 84674
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;I)[B
    .locals 5

    .prologue
    .line 84687
    const/4 v0, 0x0

    .line 84688
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 84689
    :try_start_1
    invoke-static {v1, p2}, LX/0aP;->a(Ljava/io/InputStream;I)[B
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 84690
    invoke-static {v1}, LX/0VN;->a(Ljava/io/InputStream;)V

    :goto_0
    return-object v0

    .line 84691
    :catch_0
    move-object v1, v0

    :goto_1
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "/assets/"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, LX/0aO;->a(Ljava/lang/String;I)[B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 84692
    invoke-static {v1}, LX/0VN;->a(Ljava/io/InputStream;)V

    goto :goto_0

    .line 84693
    :catch_1
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    .line 84694
    :goto_2
    :try_start_3
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "IOException encountered while reading asset"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 84695
    :catchall_0
    move-exception v0

    :goto_3
    invoke-static {v1}, LX/0VN;->a(Ljava/io/InputStream;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_3

    .line 84696
    :catch_2
    move-exception v0

    goto :goto_2

    .line 84697
    :catch_3
    goto :goto_1
.end method

.method private static a(Ljava/lang/String;I)[B
    .locals 4

    .prologue
    .line 84680
    const/4 v1, 0x0

    .line 84681
    :try_start_0
    const-class v0, LX/0aO;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 84682
    invoke-static {v1, p1}, LX/0aP;->a(Ljava/io/InputStream;I)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 84683
    invoke-static {v1}, LX/0VN;->a(Ljava/io/InputStream;)V

    return-object v0

    .line 84684
    :catch_0
    move-exception v0

    .line 84685
    :try_start_1
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "IOException encountered while reading resource"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84686
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/0VN;->a(Ljava/io/InputStream;)V

    throw v0
.end method

.method public static declared-synchronized b(Landroid/content/Context;)LX/0aa;
    .locals 3

    .prologue
    .line 84675
    const-class v1, LX/0aO;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/0aO;->b:LX/0aa;

    if-nez v0, :cond_0

    .line 84676
    const-string v0, "sessioned_index.bin"

    const v2, 0x5b6fc

    invoke-static {p0, v0, v2}, LX/0aO;->a(Landroid/content/Context;Ljava/lang/String;I)[B

    move-result-object v0

    .line 84677
    new-instance v2, LX/0aZ;

    invoke-direct {v2, v0}, LX/0aZ;-><init>([B)V

    sput-object v2, LX/0aO;->b:LX/0aa;

    .line 84678
    :cond_0
    sget-object v0, LX/0aO;->b:LX/0aa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 84679
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
