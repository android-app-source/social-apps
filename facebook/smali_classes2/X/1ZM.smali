.class public final LX/1ZM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1ZK;


# direct methods
.method public constructor <init>(LX/1ZK;)V
    .locals 0

    .prologue
    .line 274515
    iput-object p1, p0, LX/1ZM;->a:LX/1ZK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 274516
    iget-object v0, p0, LX/1ZM;->a:LX/1ZK;

    iget-object v0, v0, LX/1ZK;->f:LX/0bW;

    const-string v1, "Failed to load pulsarDetectionEnabledFuture"

    invoke-interface {v0, p1, v1}, LX/0bW;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 274517
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x5
    .end annotation

    .prologue
    .line 274518
    check-cast p1, Ljava/lang/Boolean;

    .line 274519
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 274520
    :cond_0
    :goto_0
    return-void

    .line 274521
    :cond_1
    iget-object v0, p0, LX/1ZM;->a:LX/1ZK;

    iget-object v0, v0, LX/1ZK;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothAdapter;

    .line 274522
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274523
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/1ZM;->a:LX/1ZK;

    iget-object v1, v1, LX/1ZK;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/placetips/pulsarcore/service/PulsarService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 274524
    iget-object v1, p0, LX/1ZM;->a:LX/1ZK;

    iget-object v1, v1, LX/1ZK;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/1ZM;->a:LX/1ZK;

    iget-object v2, v2, LX/1ZK;->b:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    goto :goto_0
.end method
