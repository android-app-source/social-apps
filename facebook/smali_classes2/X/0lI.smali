.class public abstract LX/0lI;
.super LX/0lJ;
.source ""

# interfaces
.implements LX/0gT;


# static fields
.field private static final serialVersionUID:J = -0x31b2f999be0e855dL


# instance fields
.field public volatile transient a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/Class;ILjava/lang/Object;Ljava/lang/Object;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;I",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 128833
    invoke-direct/range {p0 .. p5}, LX/0lJ;-><init>(Ljava/lang/Class;ILjava/lang/Object;Ljava/lang/Object;Z)V

    .line 128834
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128829
    iget-object v0, p0, LX/0lI;->a:Ljava/lang/String;

    .line 128830
    if-nez v0, :cond_0

    .line 128831
    invoke-virtual {p0}, LX/0lI;->v()Ljava/lang/String;

    move-result-object v0

    .line 128832
    :cond_0
    return-object v0
.end method

.method public final serialize(LX/0nX;LX/0my;)V
    .locals 1

    .prologue
    .line 128827
    invoke-virtual {p0}, LX/0lK;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 128828
    return-void
.end method

.method public final serializeWithType(LX/0nX;LX/0my;LX/4qz;)V
    .locals 0

    .prologue
    .line 128823
    invoke-virtual {p3, p0, p1}, LX/4qz;->a(Ljava/lang/Object;LX/0nX;)V

    .line 128824
    invoke-virtual {p0, p1, p2}, LX/0lI;->serialize(LX/0nX;LX/0my;)V

    .line 128825
    invoke-virtual {p3, p0, p1}, LX/4qz;->d(Ljava/lang/Object;LX/0nX;)V

    .line 128826
    return-void
.end method

.method public final t()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()TT;"
        }
    .end annotation

    .prologue
    .line 128822
    iget-object v0, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    return-object v0
.end method

.method public final u()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()TT;"
        }
    .end annotation

    .prologue
    .line 128821
    iget-object v0, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    return-object v0
.end method

.method public abstract v()Ljava/lang/String;
.end method
