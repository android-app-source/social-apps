.class public final LX/16b;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field private final b:Ljava/nio/ByteBuffer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/util/SparseArray",
            "<",
            "LX/25f;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/25g;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:Z

.field private final h:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 185771
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/16b;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 185764
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185765
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/16b;->g:Z

    .line 185766
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/16b;->h:Ljava/lang/Object;

    .line 185767
    iput-object v1, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    .line 185768
    iput-object v1, p0, LX/16b;->c:Landroid/util/SparseArray;

    .line 185769
    iput p1, p0, LX/16b;->f:I

    .line 185770
    return-void
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 185729
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185730
    iput-boolean v3, p0, LX/16b;->g:Z

    .line 185731
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/16b;->h:Ljava/lang/Object;

    .line 185732
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    .line 185733
    iget-object v0, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_5

    .line 185734
    iget-object v0, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move v0, v3

    .line 185735
    :goto_1
    const/4 v2, 0x4

    if-ge v0, v2, :cond_2

    .line 185736
    iget-object v2, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    const-string v4, "DELT"

    invoke-virtual {v4, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    int-to-byte v4, v4

    if-eq v2, v4, :cond_1

    .line 185737
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Delta buffer header is invalid"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, v1

    .line 185738
    goto :goto_0

    .line 185739
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 185740
    :cond_2
    iget-object v2, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    iput v2, p0, LX/16b;->f:I

    .line 185741
    add-int/lit8 v0, v0, 0x4

    .line 185742
    iget-object v2, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v5

    .line 185743
    add-int/lit8 v0, v0, 0x4

    .line 185744
    if-lez v5, :cond_5

    .line 185745
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    move v4, v0

    move-object v0, v1

    .line 185746
    :goto_2
    if-ge v3, v5, :cond_6

    .line 185747
    iget-object v6, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v6, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v6

    .line 185748
    add-int/lit8 v4, v4, 0x4

    .line 185749
    iget-object v7, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v7, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v7

    .line 185750
    add-int/lit8 v4, v4, 0x4

    .line 185751
    new-instance v8, LX/25f;

    iget-object v9, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    invoke-direct {v8, v9, v4}, LX/25f;-><init>(Ljava/nio/ByteBuffer;I)V

    .line 185752
    add-int/lit8 v4, v4, 0x10

    .line 185753
    invoke-static {v2, v6, v7, v8}, LX/16b;->a(Landroid/util/SparseArray;IILX/25f;)V

    .line 185754
    iget v6, v8, LX/25f;->d:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_4

    .line 185755
    iget-object v6, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    iget v7, v8, LX/25f;->c:I

    invoke-static {v6, v7}, LX/25g;->b(Ljava/nio/ByteBuffer;I)LX/25g;

    move-result-object v6

    .line 185756
    if-nez v0, :cond_3

    .line 185757
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 185758
    :cond_3
    iget v7, v6, LX/25g;->a:I

    invoke-virtual {v0, v7, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 185759
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    move-object v0, v1

    move-object v2, v1

    .line 185760
    :cond_6
    iput-object v2, p0, LX/16b;->c:Landroid/util/SparseArray;

    .line 185761
    iput-object v0, p0, LX/16b;->e:Landroid/util/SparseArray;

    .line 185762
    iput-object v1, p0, LX/16b;->d:Landroid/util/SparseArray;

    .line 185763
    return-void
.end method

.method public static a(LX/16b;IILjava/lang/Object;)V
    .locals 3
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 185719
    iget-object v1, p0, LX/16b;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 185720
    :try_start_0
    iget-object v0, p0, LX/16b;->d:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    .line 185721
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/16b;->d:Landroid/util/SparseArray;

    .line 185722
    :cond_0
    iget-object v0, p0, LX/16b;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 185723
    if-nez v0, :cond_1

    .line 185724
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 185725
    iget-object v2, p0, LX/16b;->d:Landroid/util/SparseArray;

    invoke-virtual {v2, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 185726
    :cond_1
    invoke-virtual {v0, p2, p3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 185727
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/16b;->g:Z

    .line 185728
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static a(LX/16b;LX/25g;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 185696
    invoke-virtual {p1}, LX/25g;->b()V

    .line 185697
    iget-object v0, p0, LX/16b;->d:Landroid/util/SparseArray;

    if-eqz v0, :cond_2

    .line 185698
    iget-object v0, p0, LX/16b;->d:Landroid/util/SparseArray;

    iget v1, p1, LX/25g;->a:I

    invoke-static {v0, v1}, LX/3dP;->b(Landroid/util/SparseArray;I)I

    move-result v0

    move v2, v0

    .line 185699
    :goto_0
    iget-object v0, p0, LX/16b;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    iget-object v0, p0, LX/16b;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    invoke-virtual {p1}, LX/25g;->a()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 185700
    iget-object v0, p0, LX/16b;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    move v3, v4

    .line 185701
    :goto_1
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v3, v1, :cond_1

    .line 185702
    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    .line 185703
    instance-of v5, v1, LX/25g;

    if-eqz v5, :cond_0

    .line 185704
    check-cast v1, LX/25g;

    invoke-static {p0, v1}, LX/16b;->a(LX/16b;LX/25g;)V

    .line 185705
    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 185706
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 185707
    :cond_2
    iget-object v0, p0, LX/16b;->c:Landroid/util/SparseArray;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_5

    .line 185708
    iget-object v0, p0, LX/16b;->c:Landroid/util/SparseArray;

    iget v1, p1, LX/25g;->a:I

    invoke-static {v0, v1}, LX/3dP;->b(Landroid/util/SparseArray;I)I

    move-result v0

    move v2, v0

    .line 185709
    :goto_2
    iget-object v0, p0, LX/16b;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    iget-object v0, p0, LX/16b;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    invoke-virtual {p1}, LX/25g;->a()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 185710
    iget-object v0, p0, LX/16b;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    move v3, v4

    .line 185711
    :goto_3
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 185712
    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/25f;

    .line 185713
    iget v5, v1, LX/25f;->d:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_3

    .line 185714
    iget-object v5, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    iget v1, v1, LX/25f;->c:I

    invoke-static {v5, v1}, LX/25g;->c(Ljava/nio/ByteBuffer;I)I

    move-result v1

    .line 185715
    invoke-virtual {p0, v1}, LX/16b;->a(I)LX/25g;

    move-result-object v1

    invoke-static {p0, v1}, LX/16b;->a(LX/16b;LX/25g;)V

    .line 185716
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    .line 185717
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 185718
    :cond_5
    return-void
.end method

.method private static a(Landroid/util/SparseArray;IILX/25f;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/util/SparseArray",
            "<",
            "LX/25f;",
            ">;>;II",
            "LX/25f;",
            ")V"
        }
    .end annotation

    .prologue
    .line 185690
    invoke-virtual {p0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 185691
    if-nez v0, :cond_0

    .line 185692
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 185693
    invoke-virtual {p0, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 185694
    :cond_0
    invoke-virtual {v0, p2, p3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 185695
    return-void
.end method

.method private static b(LX/16b;I)LX/25g;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 185676
    iget-object v2, p0, LX/16b;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 185677
    :try_start_0
    iget-object v0, p0, LX/16b;->e:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    .line 185678
    monitor-exit v2

    move-object v0, v1

    .line 185679
    :goto_0
    return-object v0

    .line 185680
    :cond_0
    iget-object v0, p0, LX/16b;->e:Landroid/util/SparseArray;

    .line 185681
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v3

    .line 185682
    if-ltz v3, :cond_3

    :goto_1
    move v0, v3

    .line 185683
    if-gez v0, :cond_1

    .line 185684
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    .line 185685
    :cond_1
    iget-object v3, p0, LX/16b;->e:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/25g;

    .line 185686
    iget v3, v0, LX/25g;->a:I

    if-lt p1, v3, :cond_4

    iget v3, v0, LX/25g;->a:I

    iget p0, v0, LX/25g;->e:I

    add-int/2addr v3, p0

    if-ge p1, v3, :cond_4

    const/4 v3, 0x1

    :goto_2
    move v3, v3

    .line 185687
    if-eqz v3, :cond_2

    :goto_3
    monitor-exit v2

    goto :goto_0

    .line 185688
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move-object v0, v1

    .line 185689
    goto :goto_3

    :cond_3
    :try_start_1
    neg-int v3, v3

    add-int/lit8 v3, v3, -0x2

    goto :goto_1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_2
.end method

.method private static k(LX/16b;II)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 185669
    iget-object v0, p0, LX/16b;->d:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    .line 185670
    sget-object v0, LX/16b;->a:Ljava/lang/Object;

    .line 185671
    :goto_0
    return-object v0

    .line 185672
    :cond_0
    iget-object v0, p0, LX/16b;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 185673
    if-nez v0, :cond_1

    .line 185674
    sget-object v0, LX/16b;->a:Ljava/lang/Object;

    goto :goto_0

    .line 185675
    :cond_1
    sget-object v1, LX/16b;->a:Ljava/lang/Object;

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method private static l(LX/16b;II)LX/25f;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 185661
    iget-object v0, p0, LX/16b;->c:Landroid/util/SparseArray;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 185662
    :cond_0
    :goto_0
    return-object v0

    .line 185663
    :cond_1
    iget-object v0, p0, LX/16b;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 185664
    if-nez v0, :cond_2

    move-object v0, v1

    .line 185665
    goto :goto_0

    .line 185666
    :cond_2
    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/25f;

    .line 185667
    if-eqz v0, :cond_3

    iget v2, v0, LX/25f;->c:I

    if-nez v2, :cond_0

    :cond_3
    move-object v0, v1

    .line 185668
    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/25g;
    .locals 3

    .prologue
    .line 185657
    invoke-static {p0, p1}, LX/16b;->b(LX/16b;I)LX/25g;

    move-result-object v0

    .line 185658
    if-nez v0, :cond_0

    .line 185659
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No extension for position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185660
    :cond_0
    return-object v0
.end method

.method public final a(III)V
    .locals 1

    .prologue
    .line 185617
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p0, p1, p2, v0}, LX/16b;->a(LX/16b;IILjava/lang/Object;)V

    .line 185618
    return-void
.end method

.method public final a(II[B)V
    .locals 12
    .param p3    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 185631
    iget-object v1, p0, LX/16b;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 185632
    :try_start_0
    invoke-virtual {p0, p1, p2}, LX/16b;->f(II)I

    move-result v0

    .line 185633
    if-eqz v0, :cond_0

    .line 185634
    invoke-virtual {p0, v0}, LX/16b;->a(I)LX/25g;

    move-result-object v0

    invoke-static {p0, v0}, LX/16b;->a(LX/16b;LX/25g;)V

    .line 185635
    :cond_0
    const/4 v0, 0x0

    .line 185636
    if-eqz p3, :cond_4

    .line 185637
    invoke-static {p3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 185638
    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 185639
    iget v2, p0, LX/16b;->f:I

    .line 185640
    new-instance v6, LX/25g;

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v7

    add-int v8, v2, v7

    const/4 v10, 0x0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v11

    move v7, v2

    move-object v9, v0

    invoke-direct/range {v6 .. v11}, LX/25g;-><init>(IILjava/nio/ByteBuffer;II)V

    move-object v0, v6

    .line 185641
    iget v2, p0, LX/16b;->f:I

    int-to-long v2, v2

    iget v4, v0, LX/25g;->e:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 185642
    long-to-int v4, v2

    int-to-long v4, v4

    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    .line 185643
    const-string v0, "DeltaBuffer"

    const-string v2, "Too many mutations! Overflow of global position space."

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 185644
    new-instance v0, Ljava/nio/BufferOverflowException;

    invoke-direct {v0}, Ljava/nio/BufferOverflowException;-><init>()V

    throw v0

    .line 185645
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 185646
    :cond_1
    long-to-int v2, v2

    :try_start_1
    iput v2, p0, LX/16b;->f:I

    .line 185647
    invoke-static {p0, p1}, LX/16b;->b(LX/16b;I)LX/25g;

    move-result-object v2

    .line 185648
    if-eqz v2, :cond_2

    .line 185649
    iget-boolean v3, v2, LX/25g;->f:Z

    move v2, v3

    .line 185650
    if-eqz v2, :cond_2

    .line 185651
    invoke-virtual {v0}, LX/25g;->b()V

    .line 185652
    :cond_2
    iget-object v2, p0, LX/16b;->e:Landroid/util/SparseArray;

    if-nez v2, :cond_3

    .line 185653
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    iput-object v2, p0, LX/16b;->e:Landroid/util/SparseArray;

    .line 185654
    :cond_3
    iget-object v2, p0, LX/16b;->e:Landroid/util/SparseArray;

    iget v3, v0, LX/25g;->a:I

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 185655
    :cond_4
    invoke-static {p0, p1, p2, v0}, LX/16b;->a(LX/16b;IILjava/lang/Object;)V

    .line 185656
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 185772
    iget-object v1, p0, LX/16b;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 185773
    :try_start_0
    iget-boolean v0, p0, LX/16b;->g:Z

    monitor-exit v1

    return v0

    .line 185774
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(II)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 185619
    iget-object v2, p0, LX/16b;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 185620
    :try_start_0
    iget-object v0, p0, LX/16b;->d:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    .line 185621
    iget-object v0, p0, LX/16b;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 185622
    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-ltz v0, :cond_0

    .line 185623
    monitor-exit v2

    move v0, v1

    .line 185624
    :goto_0
    return v0

    .line 185625
    :cond_0
    iget-object v0, p0, LX/16b;->c:Landroid/util/SparseArray;

    if-eqz v0, :cond_1

    .line 185626
    iget-object v0, p0, LX/16b;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 185627
    if-eqz v0, :cond_1

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-ltz v0, :cond_1

    .line 185628
    monitor-exit v2

    move v0, v1

    goto :goto_0

    .line 185629
    :cond_1
    const/4 v0, 0x0

    monitor-exit v2

    goto :goto_0

    .line 185630
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(II)I
    .locals 3

    .prologue
    .line 185607
    iget-object v1, p0, LX/16b;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 185608
    :try_start_0
    invoke-static {p0, p1, p2}, LX/16b;->k(LX/16b;II)Ljava/lang/Object;

    move-result-object v0

    .line 185609
    sget-object v2, LX/16b;->a:Ljava/lang/Object;

    if-eq v0, v2, :cond_0

    .line 185610
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    monitor-exit v1

    .line 185611
    :goto_0
    return v0

    .line 185612
    :cond_0
    invoke-static {p0, p1, p2}, LX/16b;->l(LX/16b;II)LX/25f;

    move-result-object v0

    .line 185613
    if-eqz v0, :cond_1

    iget-object v2, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    if-eqz v2, :cond_1

    .line 185614
    iget-object v2, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    iget v0, v0, LX/25f;->c:I

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 185615
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 185616
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final b()Ljava/nio/ByteBuffer;
    .locals 17
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 185504
    move-object/from16 v0, p0

    iget-object v7, v0, LX/16b;->h:Ljava/lang/Object;

    monitor-enter v7

    .line 185505
    const/4 v2, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/16b;->g:Z

    .line 185506
    move-object/from16 v0, p0

    iget-object v2, v0, LX/16b;->d:Landroid/util/SparseArray;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/16b;->d:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 185507
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/16b;->b:Ljava/nio/ByteBuffer;

    monitor-exit v7

    .line 185508
    :goto_0
    return-object v2

    .line 185509
    :cond_1
    new-instance v8, Landroid/util/SparseArray;

    invoke-direct {v8}, Landroid/util/SparseArray;-><init>()V

    .line 185510
    new-instance v9, LX/186;

    const/16 v2, 0x100

    invoke-direct {v9, v2}, LX/186;-><init>(I)V

    .line 185511
    const/4 v2, 0x0

    move v6, v2

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/16b;->d:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v6, v2, :cond_10

    .line 185512
    move-object/from16 v0, p0

    iget-object v2, v0, LX/16b;->d:Landroid/util/SparseArray;

    invoke-virtual {v2, v6}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v10

    .line 185513
    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/16b;->b(LX/16b;I)LX/25g;

    move-result-object v2

    .line 185514
    if-eqz v2, :cond_2

    invoke-virtual {v2}, LX/25g;->c()Z

    move-result v2

    if-nez v2, :cond_f

    .line 185515
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/16b;->d:Landroid/util/SparseArray;

    invoke-virtual {v2, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/SparseArray;

    .line 185516
    const/4 v3, 0x0

    move v5, v3

    :goto_2
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v5, v3, :cond_f

    .line 185517
    invoke-virtual {v2, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v11

    .line 185518
    invoke-virtual {v2, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    .line 185519
    const/4 v4, 0x0

    .line 185520
    if-nez v3, :cond_4

    .line 185521
    new-instance v3, LX/25f;

    const/4 v4, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-direct {v3, v4, v12, v13, v14}, LX/25f;-><init>(IIII)V

    invoke-static {v8, v10, v11, v3}, LX/16b;->a(Landroid/util/SparseArray;IILX/25f;)V

    .line 185522
    :cond_3
    :goto_3
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_2

    .line 185523
    :cond_4
    instance-of v12, v3, Ljava/lang/Integer;

    if-eqz v12, :cond_5

    .line 185524
    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v9, v3}, LX/186;->a(I)V

    .line 185525
    const/4 v3, 0x4

    .line 185526
    :goto_4
    invoke-virtual {v9}, LX/186;->b()I

    move-result v12

    .line 185527
    new-instance v13, LX/25f;

    invoke-direct {v13, v12, v3, v12, v4}, LX/25f;-><init>(IIII)V

    .line 185528
    invoke-static {v8, v10, v11, v13}, LX/16b;->a(Landroid/util/SparseArray;IILX/25f;)V

    goto :goto_3

    .line 185529
    :catchall_0
    move-exception v2

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 185530
    :cond_5
    :try_start_1
    instance-of v12, v3, Ljava/lang/Boolean;

    if-eqz v12, :cond_7

    .line 185531
    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x1

    :goto_5
    invoke-virtual {v9, v3}, LX/186;->a(B)V

    .line 185532
    const/4 v3, 0x1

    goto :goto_4

    .line 185533
    :cond_6
    const/4 v3, 0x0

    goto :goto_5

    .line 185534
    :cond_7
    instance-of v12, v3, Ljava/lang/Long;

    if-eqz v12, :cond_8

    .line 185535
    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-virtual {v9, v12, v13}, LX/186;->a(J)V

    .line 185536
    const/16 v3, 0x8

    goto :goto_4

    .line 185537
    :cond_8
    instance-of v12, v3, Ljava/lang/String;

    if-eqz v12, :cond_9

    .line 185538
    invoke-virtual {v9}, LX/186;->b()I

    move-result v12

    .line 185539
    check-cast v3, Ljava/lang/String;

    invoke-virtual {v9, v3}, LX/186;->b(Ljava/lang/String;)I

    .line 185540
    invoke-virtual {v9}, LX/186;->b()I

    move-result v3

    sub-int/2addr v3, v12

    .line 185541
    goto :goto_4

    :cond_9
    instance-of v12, v3, Ljava/lang/Byte;

    if-eqz v12, :cond_a

    .line 185542
    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    invoke-virtual {v9, v3}, LX/186;->a(B)V

    .line 185543
    const/4 v3, 0x1

    goto :goto_4

    .line 185544
    :cond_a
    instance-of v12, v3, Ljava/lang/Short;

    if-eqz v12, :cond_b

    .line 185545
    check-cast v3, Ljava/lang/Short;

    invoke-virtual {v3}, Ljava/lang/Short;->shortValue()S

    move-result v3

    invoke-virtual {v9, v3}, LX/186;->a(S)V

    .line 185546
    const/4 v3, 0x2

    goto :goto_4

    .line 185547
    :cond_b
    instance-of v12, v3, Ljava/lang/Float;

    if-eqz v12, :cond_c

    .line 185548
    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v9, v3}, LX/186;->a(F)V

    .line 185549
    const/4 v3, 0x4

    goto :goto_4

    .line 185550
    :cond_c
    instance-of v12, v3, Ljava/lang/Double;

    if-eqz v12, :cond_d

    .line 185551
    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v12

    invoke-virtual {v9, v12, v13}, LX/186;->a(D)V

    .line 185552
    const/16 v3, 0x8

    goto/16 :goto_4

    .line 185553
    :cond_d
    instance-of v4, v3, LX/25g;

    if-eqz v4, :cond_e

    .line 185554
    move-object v0, v3

    check-cast v0, LX/25g;

    move-object v4, v0

    invoke-virtual {v4}, LX/25g;->c()Z

    move-result v4

    if-nez v4, :cond_3

    .line 185555
    invoke-virtual {v9}, LX/186;->b()I

    move-result v4

    .line 185556
    check-cast v3, LX/25g;

    invoke-static {v3, v9}, LX/25g;->a(LX/25g;LX/186;)I

    .line 185557
    invoke-virtual {v9}, LX/186;->b()I

    move-result v3

    sub-int/2addr v3, v4

    .line 185558
    const/4 v4, 0x1

    .line 185559
    goto/16 :goto_4

    .line 185560
    :cond_e
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Type not supported in DeltaBuffer:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 185561
    :cond_f
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto/16 :goto_1

    .line 185562
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, LX/16b;->c:Landroid/util/SparseArray;

    if-eqz v2, :cond_17

    .line 185563
    move-object/from16 v0, p0

    iget-object v2, v0, LX/16b;->b:Ljava/nio/ByteBuffer;

    if-nez v2, :cond_11

    .line 185564
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "mByteBuffer for DeltaBuffer should not be null"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 185565
    :cond_11
    const/4 v2, 0x0

    move v6, v2

    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, LX/16b;->c:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v6, v2, :cond_17

    .line 185566
    move-object/from16 v0, p0

    iget-object v2, v0, LX/16b;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v6}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v10

    .line 185567
    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/16b;->b(LX/16b;I)LX/25g;

    move-result-object v2

    .line 185568
    if-eqz v2, :cond_12

    invoke-virtual {v2}, LX/25g;->c()Z

    move-result v2

    if-nez v2, :cond_16

    .line 185569
    :cond_12
    invoke-virtual {v8, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/SparseArray;

    .line 185570
    move-object/from16 v0, p0

    iget-object v3, v0, LX/16b;->c:Landroid/util/SparseArray;

    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/SparseArray;

    .line 185571
    const/4 v4, 0x0

    move v5, v4

    :goto_7
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-ge v5, v4, :cond_16

    .line 185572
    invoke-virtual {v3, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v11

    .line 185573
    if-eqz v2, :cond_13

    invoke-virtual {v2, v11}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v4

    if-gez v4, :cond_14

    .line 185574
    :cond_13
    invoke-virtual {v3, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/25f;

    .line 185575
    iget v12, v4, LX/25f;->c:I

    if-nez v12, :cond_15

    .line 185576
    invoke-static {v8, v10, v11, v4}, LX/16b;->a(Landroid/util/SparseArray;IILX/25f;)V

    .line 185577
    :cond_14
    :goto_8
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_7

    .line 185578
    :cond_15
    invoke-virtual {v4}, LX/25f;->a()I

    move-result v12

    iget v13, v4, LX/25f;->b:I

    invoke-virtual {v9, v12, v13}, LX/186;->a(II)V

    .line 185579
    move-object/from16 v0, p0

    iget-object v12, v0, LX/16b;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v12}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v12

    iget v13, v4, LX/25f;->a:I

    iget v14, v4, LX/25f;->b:I

    invoke-virtual {v9, v12, v13, v14}, LX/186;->a([BII)V

    .line 185580
    invoke-virtual {v9}, LX/186;->b()I

    move-result v12

    .line 185581
    new-instance v13, LX/25f;

    iget v14, v4, LX/25f;->b:I

    iget v15, v4, LX/25f;->c:I

    iget v0, v4, LX/25f;->a:I

    move/from16 v16, v0

    sub-int v15, v15, v16

    sub-int v15, v12, v15

    iget v4, v4, LX/25f;->d:I

    invoke-direct {v13, v12, v14, v15, v4}, LX/25f;-><init>(IIII)V

    invoke-static {v8, v10, v11, v13}, LX/16b;->a(Landroid/util/SparseArray;IILX/25f;)V

    goto :goto_8

    .line 185582
    :cond_16
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto/16 :goto_6

    .line 185583
    :cond_17
    const/4 v3, 0x0

    .line 185584
    const/4 v2, 0x0

    move v6, v3

    move v3, v2

    :goto_9
    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v3, v2, :cond_18

    .line 185585
    invoke-virtual {v8, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    add-int v4, v6, v2

    .line 185586
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v6, v4

    goto :goto_9

    .line 185587
    :cond_18
    mul-int/lit8 v2, v6, 0x18

    add-int/lit8 v2, v2, 0x8

    add-int/lit8 v2, v2, 0x4

    .line 185588
    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-virtual {v9, v3, v4}, LX/186;->a(II)V

    .line 185589
    invoke-virtual {v9}, LX/186;->c()I

    move-result v3

    invoke-virtual {v9, v3, v2}, LX/186;->a(II)V

    .line 185590
    const/4 v2, 0x0

    move v5, v2

    :goto_a
    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v5, v2, :cond_1a

    .line 185591
    invoke-virtual {v8, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v10

    .line 185592
    invoke-virtual {v8, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/SparseArray;

    .line 185593
    const/4 v3, 0x0

    move v4, v3

    :goto_b
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v4, v3, :cond_19

    .line 185594
    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v11

    .line 185595
    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/25f;

    .line 185596
    invoke-virtual {v3, v9}, LX/25f;->a(LX/186;)V

    .line 185597
    invoke-virtual {v9, v11}, LX/186;->a(I)V

    .line 185598
    invoke-virtual {v9, v10}, LX/186;->a(I)V

    .line 185599
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_b

    .line 185600
    :cond_19
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_a

    .line 185601
    :cond_1a
    invoke-virtual {v9, v6}, LX/186;->a(I)V

    .line 185602
    move-object/from16 v0, p0

    iget v2, v0, LX/16b;->f:I

    invoke-virtual {v9, v2}, LX/186;->a(I)V

    .line 185603
    const/4 v2, 0x3

    :goto_c
    if-ltz v2, :cond_1b

    .line 185604
    const-string v3, "DELT"

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    invoke-virtual {v9, v3}, LX/186;->a(B)V

    .line 185605
    add-int/lit8 v2, v2, -0x1

    goto :goto_c

    .line 185606
    :cond_1b
    invoke-virtual {v9}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public final c(II)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 185493
    iget-object v3, p0, LX/16b;->h:Ljava/lang/Object;

    monitor-enter v3

    .line 185494
    :try_start_0
    invoke-static {p0, p1, p2}, LX/16b;->k(LX/16b;II)Ljava/lang/Object;

    move-result-object v0

    .line 185495
    sget-object v4, LX/16b;->a:Ljava/lang/Object;

    if-eq v0, v4, :cond_0

    .line 185496
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    monitor-exit v3

    .line 185497
    :goto_0
    return v0

    .line 185498
    :cond_0
    invoke-static {p0, p1, p2}, LX/16b;->l(LX/16b;II)LX/25f;

    move-result-object v0

    .line 185499
    if-eqz v0, :cond_2

    iget-object v4, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    if-eqz v4, :cond_2

    .line 185500
    iget-object v4, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    iget v0, v0, LX/25f;->c:I

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v2, :cond_1

    move v0, v2

    :goto_1
    monitor-exit v3

    goto :goto_0

    .line 185501
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    .line 185502
    goto :goto_1

    .line 185503
    :cond_2
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    goto :goto_0
.end method

.method public final d(II)J
    .locals 3

    .prologue
    .line 185483
    iget-object v2, p0, LX/16b;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 185484
    :try_start_0
    invoke-static {p0, p1, p2}, LX/16b;->k(LX/16b;II)Ljava/lang/Object;

    move-result-object v0

    .line 185485
    sget-object v1, LX/16b;->a:Ljava/lang/Object;

    if-eq v0, v1, :cond_0

    .line 185486
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    monitor-exit v2

    .line 185487
    :goto_0
    return-wide v0

    .line 185488
    :cond_0
    invoke-static {p0, p1, p2}, LX/16b;->l(LX/16b;II)LX/25f;

    move-result-object v0

    .line 185489
    if-eqz v0, :cond_1

    iget-object v1, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_1

    .line 185490
    iget-object v1, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    iget v0, v0, LX/25f;->c:I

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    monitor-exit v2

    goto :goto_0

    .line 185491
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 185492
    :cond_1
    const-wide/16 v0, 0x0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final e(II)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 185473
    iget-object v1, p0, LX/16b;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 185474
    :try_start_0
    invoke-static {p0, p1, p2}, LX/16b;->k(LX/16b;II)Ljava/lang/Object;

    move-result-object v0

    .line 185475
    sget-object v2, LX/16b;->a:Ljava/lang/Object;

    if-eq v0, v2, :cond_0

    .line 185476
    check-cast v0, Ljava/lang/String;

    monitor-exit v1

    .line 185477
    :goto_0
    return-object v0

    .line 185478
    :cond_0
    invoke-static {p0, p1, p2}, LX/16b;->l(LX/16b;II)LX/25f;

    move-result-object v0

    .line 185479
    if-eqz v0, :cond_1

    iget-object v2, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    if-eqz v2, :cond_1

    .line 185480
    iget-object v2, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    iget v0, v0, LX/25f;->c:I

    invoke-static {v2, v0}, LX/0ah;->c(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 185481
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 185482
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final f(II)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 185462
    iget-object v2, p0, LX/16b;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 185463
    :try_start_0
    invoke-static {p0, p1, p2}, LX/16b;->k(LX/16b;II)Ljava/lang/Object;

    move-result-object v0

    .line 185464
    sget-object v3, LX/16b;->a:Ljava/lang/Object;

    if-eq v0, v3, :cond_1

    .line 185465
    instance-of v3, v0, LX/25g;

    if-eqz v3, :cond_0

    check-cast v0, LX/25g;

    iget v0, v0, LX/25g;->b:I

    :goto_0
    monitor-exit v2

    .line 185466
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 185467
    goto :goto_0

    .line 185468
    :cond_1
    invoke-static {p0, p1, p2}, LX/16b;->l(LX/16b;II)LX/25f;

    move-result-object v0

    .line 185469
    if-eqz v0, :cond_2

    iget v3, v0, LX/25f;->d:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    iget-object v3, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    if-eqz v3, :cond_2

    .line 185470
    iget-object v1, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    iget v0, v0, LX/25f;->c:I

    invoke-static {v1, v0}, LX/25g;->c(Ljava/nio/ByteBuffer;I)I

    move-result v0

    monitor-exit v2

    goto :goto_1

    .line 185471
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 185472
    :cond_2
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    goto :goto_1
.end method

.method public final g(II)B
    .locals 3

    .prologue
    .line 185452
    iget-object v1, p0, LX/16b;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 185453
    :try_start_0
    invoke-static {p0, p1, p2}, LX/16b;->k(LX/16b;II)Ljava/lang/Object;

    move-result-object v0

    .line 185454
    sget-object v2, LX/16b;->a:Ljava/lang/Object;

    if-eq v0, v2, :cond_0

    .line 185455
    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    monitor-exit v1

    .line 185456
    :goto_0
    return v0

    .line 185457
    :cond_0
    invoke-static {p0, p1, p2}, LX/16b;->l(LX/16b;II)LX/25f;

    move-result-object v0

    .line 185458
    if-eqz v0, :cond_1

    iget-object v2, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    if-eqz v2, :cond_1

    .line 185459
    iget-object v2, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    iget v0, v0, LX/25f;->c:I

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 185460
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 185461
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final h(II)S
    .locals 3

    .prologue
    .line 185442
    iget-object v1, p0, LX/16b;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 185443
    :try_start_0
    invoke-static {p0, p1, p2}, LX/16b;->k(LX/16b;II)Ljava/lang/Object;

    move-result-object v0

    .line 185444
    sget-object v2, LX/16b;->a:Ljava/lang/Object;

    if-eq v0, v2, :cond_0

    .line 185445
    check-cast v0, Ljava/lang/Short;

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v0

    monitor-exit v1

    .line 185446
    :goto_0
    return v0

    .line 185447
    :cond_0
    invoke-static {p0, p1, p2}, LX/16b;->l(LX/16b;II)LX/25f;

    move-result-object v0

    .line 185448
    if-eqz v0, :cond_1

    iget-object v2, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    if-eqz v2, :cond_1

    .line 185449
    iget-object v2, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    iget v0, v0, LX/25f;->c:I

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 185450
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 185451
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final i(II)F
    .locals 3

    .prologue
    .line 185432
    iget-object v1, p0, LX/16b;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 185433
    :try_start_0
    invoke-static {p0, p1, p2}, LX/16b;->k(LX/16b;II)Ljava/lang/Object;

    move-result-object v0

    .line 185434
    sget-object v2, LX/16b;->a:Ljava/lang/Object;

    if-eq v0, v2, :cond_0

    .line 185435
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    monitor-exit v1

    .line 185436
    :goto_0
    return v0

    .line 185437
    :cond_0
    invoke-static {p0, p1, p2}, LX/16b;->l(LX/16b;II)LX/25f;

    move-result-object v0

    .line 185438
    if-eqz v0, :cond_1

    iget-object v2, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    if-eqz v2, :cond_1

    .line 185439
    iget-object v2, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    iget v0, v0, LX/25f;->c:I

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 185440
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 185441
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final j(II)D
    .locals 3

    .prologue
    .line 185422
    iget-object v2, p0, LX/16b;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 185423
    :try_start_0
    invoke-static {p0, p1, p2}, LX/16b;->k(LX/16b;II)Ljava/lang/Object;

    move-result-object v0

    .line 185424
    sget-object v1, LX/16b;->a:Ljava/lang/Object;

    if-eq v0, v1, :cond_0

    .line 185425
    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    monitor-exit v2

    .line 185426
    :goto_0
    return-wide v0

    .line 185427
    :cond_0
    invoke-static {p0, p1, p2}, LX/16b;->l(LX/16b;II)LX/25f;

    move-result-object v0

    .line 185428
    if-eqz v0, :cond_1

    iget-object v1, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_1

    .line 185429
    iget-object v1, p0, LX/16b;->b:Ljava/nio/ByteBuffer;

    iget v0, v0, LX/25f;->c:I

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getDouble(I)D

    move-result-wide v0

    monitor-exit v2

    goto :goto_0

    .line 185430
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 185431
    :cond_1
    const-wide/16 v0, 0x0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
