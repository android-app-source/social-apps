.class public LX/0ce;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0ce;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Landroid/net/Uri;

.field public final c:LX/0cf;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 89129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".experiments"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0ce;->a:Ljava/lang/String;

    .line 89131
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "content://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0ce;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/0ce;->b:Landroid/net/Uri;

    .line 89132
    new-instance v0, LX/0cf;

    iget-object v1, p0, LX/0ce;->b:Landroid/net/Uri;

    invoke-direct {v0, v1}, LX/0cf;-><init>(Landroid/net/Uri;)V

    iput-object v0, p0, LX/0ce;->c:LX/0cf;

    .line 89133
    return-void
.end method

.method public static a(LX/0QB;)LX/0ce;
    .locals 4

    .prologue
    .line 89134
    sget-object v0, LX/0ce;->d:LX/0ce;

    if-nez v0, :cond_1

    .line 89135
    const-class v1, LX/0ce;

    monitor-enter v1

    .line 89136
    :try_start_0
    sget-object v0, LX/0ce;->d:LX/0ce;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 89137
    if-eqz v2, :cond_0

    .line 89138
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 89139
    new-instance p0, LX/0ce;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/0ce;-><init>(Landroid/content/Context;)V

    .line 89140
    move-object v0, p0

    .line 89141
    sput-object v0, LX/0ce;->d:LX/0ce;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89142
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 89143
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 89144
    :cond_1
    sget-object v0, LX/0ce;->d:LX/0ce;

    return-object v0

    .line 89145
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 89146
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
