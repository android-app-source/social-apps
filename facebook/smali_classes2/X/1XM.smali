.class public final LX/1XM;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/1XK;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/1XL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1XK",
            "<TE;>.UFIFeedbackFlyout",
            "LauncherComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/1XK;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/1XK;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 271031
    iput-object p1, p0, LX/1XM;->b:LX/1XK;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 271032
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "delegate"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "storyProps"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/1XM;->c:[Ljava/lang/String;

    .line 271033
    iput v3, p0, LX/1XM;->d:I

    .line 271034
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/1XM;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/1XM;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/1XM;LX/1De;IILX/1XL;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/1XK",
            "<TE;>.UFIFeedbackFlyout",
            "LauncherComponentImpl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 271035
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 271036
    iput-object p4, p0, LX/1XM;->a:LX/1XL;

    .line 271037
    iget-object v0, p0, LX/1XM;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 271038
    return-void
.end method


# virtual methods
.method public final a(LX/1Po;)LX/1XM;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/1XK",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 271025
    iget-object v0, p0, LX/1XM;->a:LX/1XL;

    iput-object p1, v0, LX/1XL;->c:LX/1Po;

    .line 271026
    iget-object v0, p0, LX/1XM;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 271027
    return-object p0
.end method

.method public final a(LX/1X1;)LX/1XM;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;)",
            "LX/1XK",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 271019
    iget-object v0, p0, LX/1XM;->a:LX/1XL;

    iput-object p1, v0, LX/1XL;->a:LX/1X1;

    .line 271020
    iget-object v0, p0, LX/1XM;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 271021
    return-object p0
.end method

.method public final a(LX/1X5;)LX/1XM;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X5",
            "<*>;)",
            "LX/1XK",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 271028
    iget-object v0, p0, LX/1XM;->a:LX/1XL;

    invoke-virtual {p1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    iput-object v1, v0, LX/1XL;->a:LX/1X1;

    .line 271029
    iget-object v0, p0, LX/1XM;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 271030
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XM;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/1XK",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 271022
    iget-object v0, p0, LX/1XM;->a:LX/1XL;

    iput-object p1, v0, LX/1XL;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 271023
    iget-object v0, p0, LX/1XM;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 271024
    return-object p0
.end method

.method public final a(Z)LX/1XM;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1XK",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 271017
    iget-object v0, p0, LX/1XM;->a:LX/1XL;

    iput-boolean p1, v0, LX/1XL;->b:Z

    .line 271018
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 271013
    invoke-super {p0}, LX/1X5;->a()V

    .line 271014
    const/4 v0, 0x0

    iput-object v0, p0, LX/1XM;->a:LX/1XL;

    .line 271015
    iget-object v0, p0, LX/1XM;->b:LX/1XK;

    iget-object v0, v0, LX/1XK;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 271016
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/1XK;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 271003
    iget-object v1, p0, LX/1XM;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1XM;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/1XM;->d:I

    if-ge v1, v2, :cond_2

    .line 271004
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 271005
    :goto_0
    iget v2, p0, LX/1XM;->d:I

    if-ge v0, v2, :cond_1

    .line 271006
    iget-object v2, p0, LX/1XM;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 271007
    iget-object v2, p0, LX/1XM;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271008
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 271009
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 271010
    :cond_2
    iget-object v0, p0, LX/1XM;->a:LX/1XL;

    .line 271011
    invoke-virtual {p0}, LX/1XM;->a()V

    .line 271012
    return-object v0
.end method
