.class public final enum LX/1Zn;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1Zn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1Zn;

.field public static final enum BLE:LX/1Zn;

.field public static final enum NSD:LX/1Zn;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 275428
    new-instance v0, LX/1Zn;

    const-string v1, "NSD"

    invoke-direct {v0, v1, v2}, LX/1Zn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Zn;->NSD:LX/1Zn;

    .line 275429
    new-instance v0, LX/1Zn;

    const-string v1, "BLE"

    invoke-direct {v0, v1, v3}, LX/1Zn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Zn;->BLE:LX/1Zn;

    .line 275430
    const/4 v0, 0x2

    new-array v0, v0, [LX/1Zn;

    sget-object v1, LX/1Zn;->NSD:LX/1Zn;

    aput-object v1, v0, v2

    sget-object v1, LX/1Zn;->BLE:LX/1Zn;

    aput-object v1, v0, v3

    sput-object v0, LX/1Zn;->$VALUES:[LX/1Zn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 275431
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1Zn;
    .locals 1

    .prologue
    .line 275432
    const-class v0, LX/1Zn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1Zn;

    return-object v0
.end method

.method public static values()[LX/1Zn;
    .locals 1

    .prologue
    .line 275433
    sget-object v0, LX/1Zn;->$VALUES:[LX/1Zn;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1Zn;

    return-object v0
.end method
