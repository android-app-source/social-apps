.class public LX/1tL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1tL;


# instance fields
.field public final a:LX/0W3;

.field public final b:LX/01T;


# direct methods
.method public constructor <init>(LX/0W3;LX/01T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 336414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336415
    iput-object p1, p0, LX/1tL;->a:LX/0W3;

    .line 336416
    iput-object p2, p0, LX/1tL;->b:LX/01T;

    .line 336417
    return-void
.end method

.method public static a(LX/0QB;)LX/1tL;
    .locals 5

    .prologue
    .line 336401
    sget-object v0, LX/1tL;->c:LX/1tL;

    if-nez v0, :cond_1

    .line 336402
    const-class v1, LX/1tL;

    monitor-enter v1

    .line 336403
    :try_start_0
    sget-object v0, LX/1tL;->c:LX/1tL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 336404
    if-eqz v2, :cond_0

    .line 336405
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 336406
    new-instance p0, LX/1tL;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v4

    check-cast v4, LX/01T;

    invoke-direct {p0, v3, v4}, LX/1tL;-><init>(LX/0W3;LX/01T;)V

    .line 336407
    move-object v0, p0

    .line 336408
    sput-object v0, LX/1tL;->c:LX/1tL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 336409
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 336410
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 336411
    :cond_1
    sget-object v0, LX/1tL;->c:LX/1tL;

    return-object v0

    .line 336412
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 336413
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 336399
    iget-object v1, p0, LX/1tL;->b:LX/01T;

    sget-object v2, LX/01T;->MESSENGER:LX/01T;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/1tL;->b:LX/01T;

    sget-object v2, LX/01T;->PAA:LX/01T;

    if-eq v1, v2, :cond_0

    .line 336400
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/1tL;->a:LX/0W3;

    sget-wide v2, LX/0X5;->ga:J

    invoke-interface {v1, v2, v3, v0}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
