.class public final LX/0cP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/0cI;


# direct methods
.method public constructor <init>(LX/0cI;)V
    .locals 0

    .prologue
    .line 88258
    iput-object p1, p0, LX/0cP;->a:LX/0cI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x78857b0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 88259
    iget-object v1, p0, LX/0cP;->a:LX/0cI;

    .line 88260
    iget-object v2, v1, LX/0cI;->a:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 88261
    iget-boolean v2, v1, LX/0cI;->p:Z

    if-eqz v2, :cond_2

    .line 88262
    iget-object v2, v1, LX/0cI;->o:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 88263
    if-nez v2, :cond_1

    .line 88264
    :cond_0
    :goto_0
    const/16 v1, 0x27

    const v2, -0x59f8a6a8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 88265
    :cond_1
    const-string p0, "__KEY_LOGGED_USER_ID__"

    invoke-virtual {p2, p0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 88266
    invoke-static {v2, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 88267
    :cond_2
    const-string v2, "peer_info"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 88268
    if-nez v2, :cond_3

    .line 88269
    iget-object v2, v1, LX/0cI;->e:LX/03V;

    const-class p0, LX/0cJ;

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p3, "Peer info bundle should be in the broadcast intent with action "

    invoke-direct {p1, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p3, v1, LX/0cI;->a:Ljava/lang/String;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 88270
    :cond_3
    :try_start_0
    invoke-static {v2}, LX/0cK;->a(Landroid/os/Bundle;)LX/0cK;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 88271
    invoke-virtual {v1}, LX/0cI;->a()LX/0cK;

    move-result-object p0

    .line 88272
    iget p1, v2, LX/0cK;->b:I

    iget p3, p0, LX/0cK;->b:I

    if-eq p1, p3, :cond_0

    .line 88273
    iget-object p1, v1, LX/0cI;->k:Ljava/util/concurrent/ConcurrentMap;

    iget p3, v2, LX/0cK;->b:I

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-interface {p1, p3}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 88274
    iget-object p1, p0, LX/0cK;->a:Landroid/os/Messenger;

    const-string p3, "The mMessenger member should have been set in init()"

    invoke-static {p1, p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88275
    const/4 p1, 0x0

    const/4 p3, 0x0

    invoke-static {p1, p3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object p1

    .line 88276
    invoke-virtual {p0}, LX/0cK;->a()Landroid/os/Bundle;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 88277
    :try_start_1
    iget-object p0, v2, LX/0cK;->a:Landroid/os/Messenger;

    invoke-virtual {p0, p1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 88278
    sget-object p0, LX/1gR;->Incoming:LX/1gR;

    invoke-static {v1, v2, p0}, LX/0cI;->a$redex0(LX/0cI;LX/0cK;LX/1gR;)V

    goto :goto_0

    .line 88279
    :catch_0
    iget-object v2, v1, LX/0cI;->e:LX/03V;

    const-class p0, LX/0cJ;

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p3, "Peer info bundle in the broadcast intent with action "

    invoke-direct {p1, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p3, v1, LX/0cI;->a:Ljava/lang/String;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    const-string p3, " was malformed"

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 88280
    :catch_1
    goto/16 :goto_0
.end method
