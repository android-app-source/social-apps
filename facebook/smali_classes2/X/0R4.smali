.class public final LX/0R4;
.super LX/0R5;
.source ""

# interfaces
.implements LX/0R6;


# instance fields
.field public final synthetic a:LX/0Q8;

.field private final b:LX/0Q8;


# direct methods
.method public constructor <init>(LX/0Q8;LX/0Q8;)V
    .locals 0

    .prologue
    .line 59345
    iput-object p1, p0, LX/0R4;->a:LX/0Q8;

    .line 59346
    invoke-direct {p0, p2}, LX/0R5;-><init>(LX/0QA;)V

    .line 59347
    iput-object p2, p0, LX/0R4;->b:LX/0Q8;

    .line 59348
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 59336
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 59344
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "injectComponent should only be called on ContextScope"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 59343
    return-void
.end method

.method public final b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 59342
    iget-object v0, p0, LX/0R4;->a:LX/0Q8;

    iget-object v0, v0, LX/0Q8;->d:LX/0S6;

    invoke-virtual {v0}, LX/0S6;->b()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/0SI;
    .locals 1

    .prologue
    .line 59341
    invoke-static {p0}, LX/0WI;->a(LX/0QB;)LX/0SI;

    move-result-object v0

    check-cast v0, LX/0SI;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 59340
    const/4 v0, 0x1

    return v0
.end method

.method public final getInstance(LX/0RI;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 59339
    iget-object v0, p0, LX/0R4;->b:LX/0Q8;

    invoke-virtual {v0, p1}, LX/0Q9;->getInstance(LX/0RI;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getLazy(LX/0RI;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Ot",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 59338
    iget-object v0, p0, LX/0R4;->b:LX/0Q8;

    invoke-virtual {v0, p1}, LX/0Q9;->getLazy(LX/0RI;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public final getProvider(LX/0RI;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 59337
    iget-object v0, p0, LX/0R4;->b:LX/0Q8;

    invoke-virtual {v0, p1}, LX/0Q9;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    return-object v0
.end method
