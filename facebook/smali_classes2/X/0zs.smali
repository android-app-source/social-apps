.class public final LX/0zs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0zt;


# static fields
.field public static final a:LX/0zs;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 168059
    new-instance v0, LX/0zs;

    invoke-direct {v0}, LX/0zs;-><init>()V

    sput-object v0, LX/0zs;->a:LX/0zs;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 168060
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168061
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;II)I
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 168062
    add-int v2, p2, p3

    move v0, v1

    :goto_0
    if-ge p2, v2, :cond_0

    if-ne v0, v1, :cond_0

    .line 168063
    invoke-interface {p1, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->getDirectionality(C)B

    move-result v0

    .line 168064
    sparse-switch v0, :sswitch_data_0

    .line 168065
    const/4 p0, 0x2

    :goto_1
    move v0, p0

    .line 168066
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 168067
    :cond_0
    return v0

    .line 168068
    :sswitch_0
    const/4 p0, 0x1

    goto :goto_1

    .line 168069
    :sswitch_1
    const/4 p0, 0x0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0xe -> :sswitch_0
        0xf -> :sswitch_0
        0x10 -> :sswitch_1
        0x11 -> :sswitch_1
    .end sparse-switch
.end method
