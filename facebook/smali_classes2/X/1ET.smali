.class public LX/1ET;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static volatile c:LX/1ET;


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 219950
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, LX/1ET;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1ET;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 219951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219952
    iput-object p1, p0, LX/1ET;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 219953
    return-void
.end method

.method public static a(LX/0QB;)LX/1ET;
    .locals 4

    .prologue
    .line 219954
    sget-object v0, LX/1ET;->c:LX/1ET;

    if-nez v0, :cond_1

    .line 219955
    const-class v1, LX/1ET;

    monitor-enter v1

    .line 219956
    :try_start_0
    sget-object v0, LX/1ET;->c:LX/1ET;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 219957
    if-eqz v2, :cond_0

    .line 219958
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 219959
    new-instance p0, LX/1ET;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/1ET;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 219960
    move-object v0, p0

    .line 219961
    sput-object v0, LX/1ET;->c:LX/1ET;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 219962
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 219963
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 219964
    :cond_1
    sget-object v0, LX/1ET;->c:LX/1ET;

    return-object v0

    .line 219965
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 219966
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
