.class public abstract LX/1FZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/1Ff;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 222698
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222699
    return-void
.end method

.method private a(IILandroid/graphics/Bitmap$Config;Ljava/lang/Object;)LX/1FJ;
    .locals 2
    .param p4    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/graphics/Bitmap$Config;",
            "Ljava/lang/Object;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222695
    invoke-virtual {p0, p1, p2, p3}, LX/1FZ;->b(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v1

    .line 222696
    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v0, p4}, LX/1FZ;->b(Landroid/graphics/Bitmap;Ljava/lang/Object;)V

    .line 222697
    return-object v1
.end method

.method private a(IILandroid/graphics/Bitmap$Config;ZLjava/lang/Object;)LX/1FJ;
    .locals 7
    .param p5    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/graphics/Bitmap$Config;",
            "Z",
            "Ljava/lang/Object;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222683
    move-object v0, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    .line 222684
    invoke-static {v2, v3}, LX/1FZ;->b(II)V

    .line 222685
    invoke-virtual {v0, v2, v3, v4}, LX/1FZ;->b(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object p1

    .line 222686
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/graphics/Bitmap;

    .line 222687
    goto :goto_0

    .line 222688
    :goto_0
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p3, 0xc

    if-lt p2, p3, :cond_0

    .line 222689
    invoke-virtual {p0, v5}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 222690
    :cond_0
    sget-object p2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-ne v4, p2, :cond_1

    if-nez v5, :cond_1

    .line 222691
    const/high16 p2, -0x1000000

    invoke-virtual {p0, p2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 222692
    :cond_1
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/graphics/Bitmap;

    invoke-static {p0, v6}, LX/1FZ;->b(Landroid/graphics/Bitmap;Ljava/lang/Object;)V

    .line 222693
    move-object v0, p1

    .line 222694
    return-object v0
.end method

.method private static a(LX/1FZ;Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;ZLjava/lang/Object;)LX/1FJ;
    .locals 12
    .param p5    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "IIII",
            "Landroid/graphics/Matrix;",
            "Z",
            "Ljava/lang/Object;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222648
    const-string v1, "Source bitmap cannot be null"

    invoke-static {p1, v1}, LX/03g;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222649
    invoke-static {p2, p3}, LX/1FZ;->c(II)V

    .line 222650
    invoke-static/range {p4 .. p5}, LX/1FZ;->b(II)V

    .line 222651
    invoke-static/range {p1 .. p5}, LX/1FZ;->b(Landroid/graphics/Bitmap;IIII)V

    .line 222652
    new-instance v8, Landroid/graphics/Canvas;

    invoke-direct {v8}, Landroid/graphics/Canvas;-><init>()V

    .line 222653
    new-instance v9, Landroid/graphics/Rect;

    add-int v1, p2, p4

    add-int v2, p3, p5

    invoke-direct {v9, p2, p3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 222654
    new-instance v10, Landroid/graphics/RectF;

    const/4 v1, 0x0

    const/4 v2, 0x0

    move/from16 v0, p4

    int-to-float v3, v0

    move/from16 v0, p5

    int-to-float v4, v0

    invoke-direct {v10, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 222655
    invoke-static {p1}, LX/1FZ;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap$Config;

    move-result-object v4

    .line 222656
    if-eqz p6, :cond_0

    invoke-virtual/range {p6 .. p6}, Landroid/graphics/Matrix;->isIdentity()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 222657
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v5

    move-object v1, p0

    move/from16 v2, p4

    move/from16 v3, p5

    move-object/from16 v6, p8

    invoke-direct/range {v1 .. v6}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;ZLjava/lang/Object;)LX/1FJ;

    move-result-object v2

    .line 222658
    const/4 v1, 0x0

    move-object v3, v2

    move-object v2, v1

    .line 222659
    :goto_0
    invoke-virtual {v3}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 222660
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getDensity()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 222661
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xc

    if-lt v4, v5, :cond_1

    .line 222662
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v4

    invoke-virtual {v1, v4}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 222663
    :cond_1
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x13

    if-lt v4, v5, :cond_2

    .line 222664
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isPremultiplied()Z

    move-result v4

    invoke-virtual {v1, v4}, Landroid/graphics/Bitmap;->setPremultiplied(Z)V

    .line 222665
    :cond_2
    invoke-virtual {v8, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 222666
    invoke-virtual {v8, p1, v9, v10, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 222667
    const/4 v1, 0x0

    invoke-virtual {v8, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 222668
    return-object v3

    .line 222669
    :cond_3
    invoke-virtual/range {p6 .. p6}, Landroid/graphics/Matrix;->rectStaysRect()Z

    move-result v1

    if-nez v1, :cond_7

    const/4 v1, 0x1

    move v7, v1

    .line 222670
    :goto_1
    new-instance v11, Landroid/graphics/RectF;

    invoke-direct {v11}, Landroid/graphics/RectF;-><init>()V

    .line 222671
    move-object/from16 v0, p6

    invoke-virtual {v0, v11, v10}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 222672
    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 222673
    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 222674
    if-eqz v7, :cond_4

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    :cond_4
    if-nez v7, :cond_5

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_5
    const/4 v5, 0x1

    :goto_2
    move-object v1, p0

    move-object/from16 v6, p8

    invoke-direct/range {v1 .. v6}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;ZLjava/lang/Object;)LX/1FJ;

    move-result-object v2

    .line 222675
    iget v1, v11, Landroid/graphics/RectF;->left:F

    neg-float v1, v1

    iget v3, v11, Landroid/graphics/RectF;->top:F

    neg-float v3, v3

    invoke-virtual {v8, v1, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 222676
    move-object/from16 v0, p6

    invoke-virtual {v8, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 222677
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 222678
    move/from16 v0, p7

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 222679
    if-eqz v7, :cond_6

    .line 222680
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    :cond_6
    move-object v3, v2

    move-object v2, v1

    goto/16 :goto_0

    .line 222681
    :cond_7
    const/4 v1, 0x0

    move v7, v1

    goto :goto_1

    .line 222682
    :cond_8
    const/4 v5, 0x0

    goto :goto_2
.end method

.method public static a(LX/1FZ;Landroid/graphics/Bitmap;IIZLjava/lang/Object;)LX/1FJ;
    .locals 9
    .param p4    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "IIZ",
            "Ljava/lang/Object;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 222640
    invoke-static {p2, p3}, LX/1FZ;->b(II)V

    .line 222641
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 222642
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 222643
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 222644
    int-to-float v0, p2

    int-to-float v1, v4

    div-float/2addr v0, v1

    .line 222645
    int-to-float v1, p3

    int-to-float v3, v5

    div-float/2addr v1, v3

    .line 222646
    invoke-virtual {v6, v0, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v7, p4

    move-object v8, p5

    .line 222647
    invoke-static/range {v0 .. v8}, LX/1FZ;->a(LX/1FZ;Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;ZLjava/lang/Object;)LX/1FJ;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/1FZ;[IIILandroid/graphics/Bitmap$Config;Ljava/lang/Object;)LX/1FJ;
    .locals 9
    .param p4    # Landroid/graphics/Bitmap$Config;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([III",
            "Landroid/graphics/Bitmap$Config;",
            "Ljava/lang/Object;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 222635
    invoke-virtual {p0, p2, p3, p4}, LX/1FZ;->b(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v8

    .line 222636
    invoke-virtual {v8}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, p1

    move v3, p2

    move v4, v2

    move v5, v2

    move v6, p2

    move v7, p3

    .line 222637
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 222638
    invoke-virtual {v8}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v0, p5}, LX/1FZ;->b(Landroid/graphics/Bitmap;Ljava/lang/Object;)V

    .line 222639
    return-object v8
.end method

.method private static b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap$Config;
    .locals 2

    .prologue
    .line 222627
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 222628
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    .line 222629
    if-eqz v1, :cond_0

    .line 222630
    sget-object v0, LX/1Fe;->a:[I

    invoke-virtual {v1}, Landroid/graphics/Bitmap$Config;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 222631
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 222632
    :cond_0
    :goto_0
    return-object v0

    .line 222633
    :pswitch_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    goto :goto_0

    .line 222634
    :pswitch_1
    sget-object v0, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(II)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 222599
    if-lez p0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "width must be > 0"

    invoke-static {v0, v3}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 222600
    if-lez p1, :cond_1

    :goto_1
    const-string v0, "height must be > 0"

    invoke-static {v1, v0}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 222601
    return-void

    :cond_0
    move v0, v2

    .line 222602
    goto :goto_0

    :cond_1
    move v1, v2

    .line 222603
    goto :goto_1
.end method

.method private static b(Landroid/graphics/Bitmap;IIII)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 222622
    add-int v0, p1, p3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-gt v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "x + width must be <= bitmap.width()"

    invoke-static {v0, v3}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 222623
    add-int v0, p2, p4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-gt v0, v3, :cond_1

    :goto_1
    const-string v0, "y + height must be <= bitmap.height()"

    invoke-static {v1, v0}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 222624
    return-void

    :cond_0
    move v0, v2

    .line 222625
    goto :goto_0

    :cond_1
    move v1, v2

    .line 222626
    goto :goto_1
.end method

.method public static b(Landroid/graphics/Bitmap;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 222618
    sget-object v0, LX/1FZ;->a:LX/1Ff;

    if-eqz v0, :cond_0

    .line 222619
    sget-object v0, LX/1FZ;->a:LX/1Ff;

    .line 222620
    iget-object v1, v0, LX/1Ff;->a:LX/1Fg;

    iget-object v1, v1, LX/1Fg;->d:Ljava/util/Map;

    invoke-interface {v1, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222621
    :cond_0
    return-void
.end method

.method private static c(II)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 222613
    if-ltz p0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "x must be >= 0"

    invoke-static {v0, v3}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 222614
    if-ltz p1, :cond_1

    :goto_1
    const-string v0, "y must be >= 0"

    invoke-static {v1, v0}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 222615
    return-void

    :cond_0
    move v0, v2

    .line 222616
    goto :goto_0

    :cond_1
    move v1, v2

    .line 222617
    goto :goto_1
.end method


# virtual methods
.method public final a(II)LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222598
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {p0, p1, p2, v0}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v0

    return-object v0
.end method

.method public final a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/graphics/Bitmap$Config;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222604
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;Ljava/lang/Object;)LX/1FJ;

    move-result-object v0

    return-object v0
.end method

.method public final a(IILjava/lang/Object;)LX/1FJ;
    .locals 1
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/Object;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222605
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0, p1, p2, v0, p3}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;Ljava/lang/Object;)LX/1FJ;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/graphics/Bitmap;)LX/1FJ;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222606
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 222607
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    move-object v1, p0

    move-object v2, p1

    move v4, v3

    move-object v7, v0

    invoke-virtual/range {v1 .. v7}, LX/1FZ;->a(Landroid/graphics/Bitmap;IIIILjava/lang/Object;)LX/1FJ;

    move-result-object v1

    move-object v0, v1

    .line 222608
    return-object v0
.end method

.method public final a(Landroid/graphics/Bitmap;IIII)LX/1FJ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "IIII)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222609
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, LX/1FZ;->a(Landroid/graphics/Bitmap;IIIILjava/lang/Object;)LX/1FJ;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)LX/1FJ;
    .locals 9
    .param p6    # Landroid/graphics/Matrix;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "IIII",
            "Landroid/graphics/Matrix;",
            "Z)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222610
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move/from16 v7, p7

    invoke-static/range {v0 .. v8}, LX/1FZ;->a(LX/1FZ;Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;ZLjava/lang/Object;)LX/1FJ;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/graphics/Bitmap;IIIILjava/lang/Object;)LX/1FJ;
    .locals 9
    .param p6    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "IIII",
            "Ljava/lang/Object;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222611
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v8, p6

    invoke-static/range {v0 .. v8}, LX/1FZ;->a(LX/1FZ;Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;ZLjava/lang/Object;)LX/1FJ;

    move-result-object v0

    return-object v0
.end method

.method public final a([IIILandroid/graphics/Bitmap$Config;)LX/1FJ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([III",
            "Landroid/graphics/Bitmap$Config;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222612
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, LX/1FZ;->a(LX/1FZ;[IIILandroid/graphics/Bitmap$Config;Ljava/lang/Object;)LX/1FJ;

    move-result-object v0

    return-object v0
.end method

.method public abstract b(IILandroid/graphics/Bitmap$Config;)LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/graphics/Bitmap$Config;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end method
