.class public LX/1Y4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/reflect/Type;

.field public final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/reflect/ParameterizedType;

.field public d:LX/1Y4;

.field public e:LX/1Y4;


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Type;)V
    .locals 3

    .prologue
    .line 273084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273085
    iput-object p1, p0, LX/1Y4;->a:Ljava/lang/reflect/Type;

    .line 273086
    instance-of v0, p1, Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 273087
    check-cast p1, Ljava/lang/Class;

    iput-object p1, p0, LX/1Y4;->b:Ljava/lang/Class;

    .line 273088
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Y4;->c:Ljava/lang/reflect/ParameterizedType;

    .line 273089
    :goto_0
    return-void

    .line 273090
    :cond_0
    instance-of v0, p1, Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_1

    .line 273091
    check-cast p1, Ljava/lang/reflect/ParameterizedType;

    iput-object p1, p0, LX/1Y4;->c:Ljava/lang/reflect/ParameterizedType;

    .line 273092
    iget-object v0, p0, LX/1Y4;->c:Ljava/lang/reflect/ParameterizedType;

    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, LX/1Y4;->b:Ljava/lang/Class;

    goto :goto_0

    .line 273093
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " can not be used to construct HierarchicType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private constructor <init>(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/ParameterizedType;LX/1Y4;LX/1Y4;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/reflect/ParameterizedType;",
            "LX/1Y4;",
            "LX/1Y4;",
            ")V"
        }
    .end annotation

    .prologue
    .line 273094
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273095
    iput-object p1, p0, LX/1Y4;->a:Ljava/lang/reflect/Type;

    .line 273096
    iput-object p2, p0, LX/1Y4;->b:Ljava/lang/Class;

    .line 273097
    iput-object p3, p0, LX/1Y4;->c:Ljava/lang/reflect/ParameterizedType;

    .line 273098
    iput-object p4, p0, LX/1Y4;->d:LX/1Y4;

    .line 273099
    iput-object p5, p0, LX/1Y4;->e:LX/1Y4;

    .line 273100
    return-void
.end method


# virtual methods
.method public final a()LX/1Y4;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 273101
    iget-object v0, p0, LX/1Y4;->d:LX/1Y4;

    if-nez v0, :cond_1

    move-object v4, v5

    .line 273102
    :goto_0
    new-instance v0, LX/1Y4;

    iget-object v1, p0, LX/1Y4;->a:Ljava/lang/reflect/Type;

    iget-object v2, p0, LX/1Y4;->b:Ljava/lang/Class;

    iget-object v3, p0, LX/1Y4;->c:Ljava/lang/reflect/ParameterizedType;

    invoke-direct/range {v0 .. v5}, LX/1Y4;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/ParameterizedType;LX/1Y4;LX/1Y4;)V

    .line 273103
    if-eqz v4, :cond_0

    .line 273104
    iput-object v0, v4, LX/1Y4;->e:LX/1Y4;

    .line 273105
    :cond_0
    return-object v0

    .line 273106
    :cond_1
    iget-object v0, p0, LX/1Y4;->d:LX/1Y4;

    invoke-virtual {v0}, LX/1Y4;->a()LX/1Y4;

    move-result-object v4

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 273107
    iget-object v0, p0, LX/1Y4;->c:Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273108
    iget-object v0, p0, LX/1Y4;->c:Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_0

    .line 273109
    iget-object v0, p0, LX/1Y4;->c:Ljava/lang/reflect/ParameterizedType;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 273110
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1Y4;->b:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
