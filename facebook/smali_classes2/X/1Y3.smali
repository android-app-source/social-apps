.class public LX/1Y3;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0lJ;

.field private static final g:[LX/0lJ;


# instance fields
.field public final b:LX/0li;

.field public final c:LX/0lJ;

.field public final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0lJ;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/1Y3;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 273020
    const/4 v0, 0x0

    new-array v0, v0, [LX/0lJ;

    sput-object v0, LX/1Y3;->g:[LX/0lJ;

    .line 273021
    new-instance v0, LX/0lH;

    const-class v1, Ljava/lang/Object;

    invoke-direct {v0, v1}, LX/0lH;-><init>(Ljava/lang/Class;)V

    sput-object v0, LX/1Y3;->a:LX/0lJ;

    return-void
.end method

.method public constructor <init>(LX/0li;LX/0lJ;)V
    .locals 2

    .prologue
    .line 273022
    const/4 v0, 0x0

    .line 273023
    iget-object v1, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v1

    .line 273024
    invoke-direct {p0, p1, v0, v1, p2}, LX/1Y3;-><init>(LX/0li;LX/1Y3;Ljava/lang/Class;LX/0lJ;)V

    .line 273025
    return-void
.end method

.method private constructor <init>(LX/0li;LX/1Y3;Ljava/lang/Class;LX/0lJ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0li;",
            "LX/1Y3;",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0lJ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 273026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273027
    iput-object p1, p0, LX/1Y3;->b:LX/0li;

    .line 273028
    iput-object p2, p0, LX/1Y3;->h:LX/1Y3;

    .line 273029
    iput-object p3, p0, LX/1Y3;->d:Ljava/lang/Class;

    .line 273030
    iput-object p4, p0, LX/1Y3;->c:LX/0lJ;

    .line 273031
    return-void
.end method

.method public constructor <init>(LX/0li;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0li;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 273082
    invoke-direct {p0, p1, v0, p2, v0}, LX/1Y3;-><init>(LX/0li;LX/1Y3;Ljava/lang/Class;LX/0lJ;)V

    .line 273083
    return-void
.end method

.method private b(Ljava/lang/reflect/Type;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 273032
    if-nez p1, :cond_1

    .line 273033
    :cond_0
    return-void

    .line 273034
    :cond_1
    instance-of v0, p1, Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_6

    .line 273035
    check-cast p1, Ljava/lang/reflect/ParameterizedType;

    .line 273036
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v1

    .line 273037
    if-eqz v1, :cond_5

    array-length v0, v1

    if-lez v0, :cond_5

    .line 273038
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 273039
    invoke-virtual {v0}, Ljava/lang/Class;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;

    move-result-object v3

    .line 273040
    array-length v4, v3

    array-length v5, v1

    if-eq v4, v5, :cond_2

    .line 273041
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Strange parametrized type (in class "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "): number of type arguments != number of type parameters ("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " vs "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 273042
    :cond_2
    array-length v4, v1

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_5

    .line 273043
    aget-object v5, v3, v0

    .line 273044
    invoke-interface {v5}, Ljava/lang/reflect/TypeVariable;->getName()Ljava/lang/String;

    move-result-object v5

    .line 273045
    iget-object v6, p0, LX/1Y3;->e:Ljava/util/Map;

    if-nez v6, :cond_4

    .line 273046
    new-instance v6, Ljava/util/LinkedHashMap;

    invoke-direct {v6}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v6, p0, LX/1Y3;->e:Ljava/util/Map;

    .line 273047
    :goto_1
    invoke-virtual {p0, v5}, LX/1Y3;->b(Ljava/lang/String;)V

    .line 273048
    iget-object v6, p0, LX/1Y3;->e:Ljava/util/Map;

    iget-object v7, p0, LX/1Y3;->b:LX/0li;

    aget-object v8, v1, v0

    invoke-virtual {v7, v8, p0}, LX/0li;->b(Ljava/lang/reflect/Type;LX/1Y3;)LX/0lJ;

    move-result-object v7

    invoke-interface {v6, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273049
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 273050
    :cond_4
    iget-object v6, p0, LX/1Y3;->e:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    goto :goto_1

    .line 273051
    :cond_5
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 273052
    :goto_2
    invoke-virtual {v0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-direct {p0, v1}, LX/1Y3;->b(Ljava/lang/reflect/Type;)V

    .line 273053
    invoke-virtual {v0}, Ljava/lang/Class;->getGenericInterfaces()[Ljava/lang/reflect/Type;

    move-result-object v0

    array-length v1, v0

    :goto_3
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 273054
    invoke-direct {p0, v3}, LX/1Y3;->b(Ljava/lang/reflect/Type;)V

    .line 273055
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 273056
    :cond_6
    instance-of v0, p1, Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 273057
    check-cast p1, Ljava/lang/Class;

    .line 273058
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    .line 273059
    if-eqz v0, :cond_7

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 273060
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1Y3;->b(Ljava/lang/reflect/Type;)V

    .line 273061
    :cond_7
    invoke-virtual {p1}, Ljava/lang/Class;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;

    move-result-object v3

    .line 273062
    if-eqz v3, :cond_c

    array-length v0, v3

    if-lez v0, :cond_c

    .line 273063
    const/4 v0, 0x0

    .line 273064
    iget-object v1, p0, LX/1Y3;->c:LX/0lJ;

    if-eqz v1, :cond_8

    iget-object v1, p0, LX/1Y3;->c:LX/0lJ;

    .line 273065
    iget-object v4, v1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v4

    .line 273066
    invoke-virtual {p1, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 273067
    iget-object v0, p0, LX/1Y3;->b:LX/0li;

    iget-object v1, p0, LX/1Y3;->c:LX/0lJ;

    invoke-virtual {v0, v1, p1}, LX/0li;->b(LX/0lJ;Ljava/lang/Class;)[LX/0lJ;

    move-result-object v0

    :cond_8
    move v1, v2

    .line 273068
    :goto_4
    array-length v4, v3

    if-ge v1, v4, :cond_c

    .line 273069
    aget-object v4, v3, v1

    .line 273070
    invoke-interface {v4}, Ljava/lang/reflect/TypeVariable;->getName()Ljava/lang/String;

    move-result-object v5

    .line 273071
    invoke-interface {v4}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v4

    aget-object v4, v4, v2

    .line 273072
    if-eqz v4, :cond_9

    .line 273073
    iget-object v6, p0, LX/1Y3;->e:Ljava/util/Map;

    if-nez v6, :cond_a

    .line 273074
    new-instance v6, Ljava/util/LinkedHashMap;

    invoke-direct {v6}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v6, p0, LX/1Y3;->e:Ljava/util/Map;

    .line 273075
    :goto_5
    invoke-virtual {p0, v5}, LX/1Y3;->b(Ljava/lang/String;)V

    .line 273076
    if-eqz v0, :cond_b

    .line 273077
    iget-object v4, p0, LX/1Y3;->e:Ljava/util/Map;

    aget-object v6, v0, v1

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273078
    :cond_9
    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 273079
    :cond_a
    iget-object v6, p0, LX/1Y3;->e:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    goto :goto_5

    .line 273080
    :cond_b
    iget-object v6, p0, LX/1Y3;->e:Ljava/util/Map;

    iget-object v7, p0, LX/1Y3;->b:LX/0li;

    invoke-virtual {v7, v4, p0}, LX/0li;->b(Ljava/lang/reflect/Type;LX/1Y3;)LX/0lJ;

    move-result-object v4

    invoke-interface {v6, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    :cond_c
    move-object v0, p1

    .line 273081
    goto/16 :goto_2
.end method

.method private c()V
    .locals 4

    .prologue
    .line 272965
    iget-object v0, p0, LX/1Y3;->d:Ljava/lang/Class;

    invoke-direct {p0, v0}, LX/1Y3;->b(Ljava/lang/reflect/Type;)V

    .line 272966
    iget-object v0, p0, LX/1Y3;->c:LX/0lJ;

    if-eqz v0, :cond_0

    .line 272967
    iget-object v0, p0, LX/1Y3;->c:LX/0lJ;

    invoke-virtual {v0}, LX/0lJ;->s()I

    move-result v1

    .line 272968
    if-lez v1, :cond_0

    .line 272969
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 272970
    iget-object v2, p0, LX/1Y3;->c:LX/0lJ;

    invoke-virtual {v2, v0}, LX/0lJ;->b(I)Ljava/lang/String;

    move-result-object v2

    .line 272971
    iget-object v3, p0, LX/1Y3;->c:LX/0lJ;

    invoke-virtual {v3, v0}, LX/0lJ;->a(I)LX/0lJ;

    move-result-object v3

    .line 272972
    invoke-virtual {p0, v2, v3}, LX/1Y3;->a(Ljava/lang/String;LX/0lJ;)V

    .line 272973
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 272974
    :cond_0
    iget-object v0, p0, LX/1Y3;->e:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 272975
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/1Y3;->e:Ljava/util/Map;

    .line 272976
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0lJ;
    .locals 4

    .prologue
    .line 273000
    iget-object v0, p0, LX/1Y3;->e:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 273001
    invoke-direct {p0}, LX/1Y3;->c()V

    .line 273002
    :cond_0
    iget-object v0, p0, LX/1Y3;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lJ;

    .line 273003
    if-eqz v0, :cond_1

    .line 273004
    :goto_0
    return-object v0

    .line 273005
    :cond_1
    iget-object v0, p0, LX/1Y3;->f:Ljava/util/HashSet;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1Y3;->f:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 273006
    sget-object v0, LX/1Y3;->a:LX/0lJ;

    goto :goto_0

    .line 273007
    :cond_2
    iget-object v0, p0, LX/1Y3;->h:LX/1Y3;

    if-eqz v0, :cond_3

    .line 273008
    iget-object v0, p0, LX/1Y3;->h:LX/1Y3;

    invoke-virtual {v0, p1}, LX/1Y3;->a(Ljava/lang/String;)LX/0lJ;

    move-result-object v0

    goto :goto_0

    .line 273009
    :cond_3
    iget-object v0, p0, LX/1Y3;->d:Ljava/lang/Class;

    if-eqz v0, :cond_4

    .line 273010
    iget-object v0, p0, LX/1Y3;->d:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnclosingClass()Ljava/lang/Class;

    move-result-object v0

    .line 273011
    if-eqz v0, :cond_4

    .line 273012
    iget-object v0, p0, LX/1Y3;->d:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 273013
    sget-object v0, LX/1Y3;->a:LX/0lJ;

    goto :goto_0

    .line 273014
    :cond_4
    iget-object v0, p0, LX/1Y3;->d:Ljava/lang/Class;

    if-eqz v0, :cond_5

    .line 273015
    iget-object v0, p0, LX/1Y3;->d:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 273016
    :goto_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Type variable \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' can not be resolved (with context of class "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 273017
    :cond_5
    iget-object v0, p0, LX/1Y3;->c:LX/0lJ;

    if-eqz v0, :cond_6

    .line 273018
    iget-object v0, p0, LX/1Y3;->c:LX/0lJ;

    invoke-virtual {v0}, LX/0lJ;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 273019
    :cond_6
    const-string v0, "UNKNOWN"

    goto :goto_1
.end method

.method public final a(Ljava/lang/reflect/Type;)LX/0lJ;
    .locals 1

    .prologue
    .line 272999
    iget-object v0, p0, LX/1Y3;->b:LX/0li;

    invoke-virtual {v0, p1, p0}, LX/0li;->b(Ljava/lang/reflect/Type;LX/1Y3;)LX/0lJ;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/1Y3;
    .locals 4

    .prologue
    .line 272998
    new-instance v0, LX/1Y3;

    iget-object v1, p0, LX/1Y3;->b:LX/0li;

    iget-object v2, p0, LX/1Y3;->d:Ljava/lang/Class;

    iget-object v3, p0, LX/1Y3;->c:LX/0lJ;

    invoke-direct {v0, v1, p0, v2, v3}, LX/1Y3;-><init>(LX/0li;LX/1Y3;Ljava/lang/Class;LX/0lJ;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/0lJ;)V
    .locals 1

    .prologue
    .line 272994
    iget-object v0, p0, LX/1Y3;->e:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Y3;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 272995
    :cond_0
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/1Y3;->e:Ljava/util/Map;

    .line 272996
    :cond_1
    iget-object v0, p0, LX/1Y3;->e:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272997
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 272990
    iget-object v0, p0, LX/1Y3;->f:Ljava/util/HashSet;

    if-nez v0, :cond_0

    .line 272991
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1Y3;->f:Ljava/util/HashSet;

    .line 272992
    :cond_0
    iget-object v0, p0, LX/1Y3;->f:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 272993
    return-void
.end method

.method public final b()[LX/0lJ;
    .locals 2

    .prologue
    .line 272985
    iget-object v0, p0, LX/1Y3;->e:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 272986
    invoke-direct {p0}, LX/1Y3;->c()V

    .line 272987
    :cond_0
    iget-object v0, p0, LX/1Y3;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 272988
    sget-object v0, LX/1Y3;->g:[LX/0lJ;

    .line 272989
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/1Y3;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, LX/1Y3;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    new-array v1, v1, [LX/0lJ;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0lJ;

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 272977
    iget-object v0, p0, LX/1Y3;->e:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 272978
    invoke-direct {p0}, LX/1Y3;->c()V

    .line 272979
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[TypeBindings for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 272980
    iget-object v1, p0, LX/1Y3;->c:LX/0lJ;

    if-eqz v1, :cond_1

    .line 272981
    iget-object v1, p0, LX/1Y3;->c:LX/0lJ;

    invoke-virtual {v1}, LX/0lJ;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272982
    :goto_0
    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/1Y3;->e:Ljava/util/Map;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272983
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 272984
    :cond_1
    iget-object v1, p0, LX/1Y3;->d:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
