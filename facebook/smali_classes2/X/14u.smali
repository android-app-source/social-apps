.class public LX/14u;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile h:LX/14u;


# instance fields
.field public final b:LX/14v;

.field private final c:LX/14w;

.field public final d:LX/157;

.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/6VM;

.field public g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 179330
    const-class v0, LX/14u;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/14u;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/14v;LX/14w;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 179331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179332
    iput-object p1, p0, LX/14u;->b:LX/14v;

    .line 179333
    iput-object p2, p0, LX/14u;->c:LX/14w;

    .line 179334
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/14u;->e:Ljava/util/Map;

    .line 179335
    new-instance v0, LX/156;

    invoke-direct {v0, p0}, LX/156;-><init>(LX/14u;)V

    iput-object v0, p0, LX/14u;->d:LX/157;

    .line 179336
    return-void
.end method

.method public static a(LX/0QB;)LX/14u;
    .locals 5

    .prologue
    .line 179337
    sget-object v0, LX/14u;->h:LX/14u;

    if-nez v0, :cond_1

    .line 179338
    const-class v1, LX/14u;

    monitor-enter v1

    .line 179339
    :try_start_0
    sget-object v0, LX/14u;->h:LX/14u;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 179340
    if-eqz v2, :cond_0

    .line 179341
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 179342
    new-instance p0, LX/14u;

    invoke-static {v0}, LX/14v;->a(LX/0QB;)LX/14v;

    move-result-object v3

    check-cast v3, LX/14v;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v4

    check-cast v4, LX/14w;

    invoke-direct {p0, v3, v4}, LX/14u;-><init>(LX/14v;LX/14w;)V

    .line 179343
    move-object v0, p0

    .line 179344
    sput-object v0, LX/14u;->h:LX/14u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 179345
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 179346
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 179347
    :cond_1
    sget-object v0, LX/14u;->h:LX/14u;

    return-object v0

    .line 179348
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 179349
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(LX/14u;Lcom/facebook/feed/model/ClientFeedUnitEdge;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 179350
    iget-boolean v0, p1, Lcom/facebook/feed/model/ClientFeedUnitEdge;->B:Z

    move v0, v0

    .line 179351
    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 179352
    :goto_0
    return v0

    .line 179353
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_2

    .line 179354
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/14w;->p(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 179355
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 179356
    iget-object v2, p0, LX/14u;->b:LX/14v;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/14v;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 179357
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 179358
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 179359
    iget-boolean v0, p0, LX/14u;->g:Z

    if-nez v0, :cond_0

    .line 179360
    :goto_0
    return-void

    .line 179361
    :cond_0
    iget-object v0, p0, LX/14u;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179362
    iget-object v0, p0, LX/14u;->b:LX/14v;

    iget-object v1, p0, LX/14u;->d:LX/157;

    invoke-virtual {v0, p1, v1}, LX/14v;->a(Ljava/lang/String;LX/157;)V

    goto :goto_0
.end method
