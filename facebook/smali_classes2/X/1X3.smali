.class public LX/1X3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/1xp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1xp",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final c:LX/1xq;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 270385
    const v0, 0x7f0b091d

    sput v0, LX/1X3;->a:I

    return-void
.end method

.method public constructor <init>(LX/1xp;LX/1xq;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 270386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 270387
    iput-object p1, p0, LX/1X3;->b:LX/1xp;

    .line 270388
    iput-object p2, p0, LX/1X3;->c:LX/1xq;

    .line 270389
    return-void
.end method

.method public static a(LX/0QB;)LX/1X3;
    .locals 5

    .prologue
    .line 270390
    const-class v1, LX/1X3;

    monitor-enter v1

    .line 270391
    :try_start_0
    sget-object v0, LX/1X3;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 270392
    sput-object v2, LX/1X3;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 270393
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270394
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 270395
    new-instance p0, LX/1X3;

    invoke-static {v0}, LX/1xp;->a(LX/0QB;)LX/1xp;

    move-result-object v3

    check-cast v3, LX/1xp;

    invoke-static {v0}, LX/1xq;->a(LX/0QB;)LX/1xq;

    move-result-object v4

    check-cast v4, LX/1xq;

    invoke-direct {p0, v3, v4}, LX/1X3;-><init>(LX/1xp;LX/1xq;)V

    .line 270396
    move-object v0, p0

    .line 270397
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 270398
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1X3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270399
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 270400
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
