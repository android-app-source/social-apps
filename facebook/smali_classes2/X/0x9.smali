.class public LX/0x9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile r:LX/0x9;


# instance fields
.field private final a:LX/133;

.field private final b:LX/134;

.field private c:LX/135;

.field private final d:LX/136;

.field private final e:LX/137;

.field private final f:LX/138;

.field public final g:LX/0ad;

.field public final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;",
            "LX/CwT;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;",
            "LX/FaB;",
            ">;"
        }
    .end annotation
.end field

.field public j:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/139;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fas;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FaR;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field public q:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;


# direct methods
.method public constructor <init>(LX/133;LX/134;LX/135;LX/136;LX/137;LX/138;LX/0ad;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 162163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162164
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 162165
    iput-object v0, p0, LX/0x9;->k:LX/0Ot;

    .line 162166
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 162167
    iput-object v0, p0, LX/0x9;->l:LX/0Ot;

    .line 162168
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 162169
    iput-object v0, p0, LX/0x9;->m:LX/0Ot;

    .line 162170
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 162171
    iput-object v0, p0, LX/0x9;->n:LX/0Ot;

    .line 162172
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 162173
    iput-object v0, p0, LX/0x9;->o:LX/0Ot;

    .line 162174
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 162175
    iput-object v0, p0, LX/0x9;->p:LX/0Ot;

    .line 162176
    iput-object p1, p0, LX/0x9;->a:LX/133;

    .line 162177
    iput-object p2, p0, LX/0x9;->b:LX/134;

    .line 162178
    iput-object p3, p0, LX/0x9;->c:LX/135;

    .line 162179
    iput-object p4, p0, LX/0x9;->d:LX/136;

    .line 162180
    iput-object p5, p0, LX/0x9;->e:LX/137;

    .line 162181
    iput-object p6, p0, LX/0x9;->f:LX/138;

    .line 162182
    iput-object p7, p0, LX/0x9;->g:LX/0ad;

    .line 162183
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0x9;->h:Ljava/util/Map;

    .line 162184
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0x9;->i:Ljava/util/Map;

    .line 162185
    return-void
.end method

.method public static a(LX/0QB;)LX/0x9;
    .locals 11

    .prologue
    .line 162145
    sget-object v0, LX/0x9;->r:LX/0x9;

    if-nez v0, :cond_1

    .line 162146
    const-class v1, LX/0x9;

    monitor-enter v1

    .line 162147
    :try_start_0
    sget-object v0, LX/0x9;->r:LX/0x9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 162148
    if-eqz v2, :cond_0

    .line 162149
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 162150
    new-instance v3, LX/0x9;

    const-class v4, LX/133;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/133;

    const-class v5, LX/134;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/134;

    const-class v6, LX/135;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/135;

    const-class v7, LX/136;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/136;

    const-class v8, LX/137;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/137;

    .line 162151
    new-instance v10, LX/138;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-direct {v10, v9}, LX/138;-><init>(LX/03V;)V

    .line 162152
    move-object v9, v10

    .line 162153
    check-cast v9, LX/138;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-direct/range {v3 .. v10}, LX/0x9;-><init>(LX/133;LX/134;LX/135;LX/136;LX/137;LX/138;LX/0ad;)V

    .line 162154
    const/16 v4, 0x1149

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x32fa

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x32f5

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x259

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xf9a

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2e3

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    .line 162155
    iput-object v4, v3, LX/0x9;->k:LX/0Ot;

    iput-object v5, v3, LX/0x9;->l:LX/0Ot;

    iput-object v6, v3, LX/0x9;->m:LX/0Ot;

    iput-object v7, v3, LX/0x9;->n:LX/0Ot;

    iput-object v8, v3, LX/0x9;->o:LX/0Ot;

    iput-object v9, v3, LX/0x9;->p:LX/0Ot;

    .line 162156
    move-object v0, v3

    .line 162157
    sput-object v0, LX/0x9;->r:LX/0x9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 162158
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 162159
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 162160
    :cond_1
    sget-object v0, LX/0x9;->r:LX/0x9;

    return-object v0

    .line 162161
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 162162
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private d(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)V
    .locals 1

    .prologue
    .line 162141
    iget-object v0, p0, LX/0x9;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FaB;

    .line 162142
    if-eqz v0, :cond_0

    .line 162143
    invoke-interface {v0}, LX/FaB;->e()V

    .line 162144
    :cond_0
    return-void
.end method

.method private e(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)V
    .locals 13

    .prologue
    .line 162113
    iget-object v0, p0, LX/0x9;->j:Landroid/view/View;

    if-nez v0, :cond_0

    .line 162114
    :goto_0
    return-void

    .line 162115
    :cond_0
    iget-object v0, p0, LX/0x9;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CwT;

    .line 162116
    sget-object v1, LX/FaK;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 162117
    :pswitch_0
    iget-object v1, p0, LX/0x9;->i:Ljava/util/Map;

    iget-object v2, p0, LX/0x9;->a:LX/133;

    iget-object v3, p0, LX/0x9;->j:Landroid/view/View;

    .line 162118
    new-instance v4, LX/Fb4;

    const-class v5, Landroid/content/Context;

    invoke-interface {v2, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v2}, LX/FaY;->a(LX/0QB;)LX/FaY;

    move-result-object v8

    check-cast v8, LX/FaY;

    invoke-static {v2}, LX/13B;->b(LX/0QB;)LX/13B;

    move-result-object v9

    check-cast v9, LX/13B;

    invoke-static {v2}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    move-object v6, v3

    move-object v7, v0

    invoke-direct/range {v4 .. v10}, LX/Fb4;-><init>(Landroid/content/Context;Landroid/view/View;LX/CwT;LX/FaY;LX/13B;LX/03V;)V

    .line 162119
    move-object v0, v4

    .line 162120
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 162121
    :pswitch_1
    iget-object v1, p0, LX/0x9;->i:Ljava/util/Map;

    iget-object v2, p0, LX/0x9;->b:LX/134;

    iget-object v3, p0, LX/0x9;->j:Landroid/view/View;

    .line 162122
    new-instance v4, LX/Fb2;

    invoke-direct {v4, v3, v0, p0}, LX/Fb2;-><init>(Landroid/view/View;LX/CwT;LX/0x9;)V

    .line 162123
    const/16 v5, 0x32f3

    invoke-static {v2, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const-class v6, Landroid/content/Context;

    invoke-interface {v2, v6}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1141

    invoke-static {v2, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x259

    invoke-static {v2, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x32f6

    invoke-static {v2, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xc49

    invoke-static {v2, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x455

    invoke-static {v2, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0xac0

    invoke-static {v2, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    .line 162124
    iput-object v5, v4, LX/Fb2;->a:LX/0Or;

    iput-object v6, v4, LX/Fb2;->b:LX/0Ot;

    iput-object v7, v4, LX/Fb2;->c:LX/0Ot;

    iput-object v8, v4, LX/Fb2;->d:LX/0Ot;

    iput-object v9, v4, LX/Fb2;->e:LX/0Ot;

    iput-object v10, v4, LX/Fb2;->f:LX/0Ot;

    iput-object v11, v4, LX/Fb2;->g:LX/0Ot;

    iput-object v12, v4, LX/Fb2;->h:LX/0Ot;

    .line 162125
    move-object v0, v4

    .line 162126
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 162127
    :pswitch_2
    iget-object v1, p0, LX/0x9;->i:Ljava/util/Map;

    iget-object v2, p0, LX/0x9;->c:LX/135;

    .line 162128
    new-instance v4, LX/FaS;

    invoke-static {v2}, LX/13B;->b(LX/0QB;)LX/13B;

    move-result-object v6

    check-cast v6, LX/13B;

    invoke-static {v2}, LX/FaY;->a(LX/0QB;)LX/FaY;

    move-result-object v7

    check-cast v7, LX/FaY;

    invoke-static {v2}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {v2}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    move-object v5, v0

    invoke-direct/range {v4 .. v9}, LX/FaS;-><init>(LX/CwT;LX/13B;LX/FaY;LX/0ad;LX/0SG;)V

    .line 162129
    move-object v0, v4

    .line 162130
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 162131
    :pswitch_3
    iget-object v1, p0, LX/0x9;->i:Ljava/util/Map;

    iget-object v2, p0, LX/0x9;->e:LX/137;

    .line 162132
    new-instance v6, LX/FaZ;

    invoke-static {v2}, LX/13B;->b(LX/0QB;)LX/13B;

    move-result-object v3

    check-cast v3, LX/13B;

    invoke-static {v2}, LX/FaY;->a(LX/0QB;)LX/FaY;

    move-result-object v4

    check-cast v4, LX/FaY;

    invoke-static {v2}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {v6, v0, v3, v4, v5}, LX/FaZ;-><init>(LX/CwT;LX/13B;LX/FaY;LX/0ad;)V

    .line 162133
    move-object v0, v6

    .line 162134
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 162135
    :pswitch_4
    iget-object v1, p0, LX/0x9;->i:Ljava/util/Map;

    iget-object v2, p0, LX/0x9;->d:LX/136;

    .line 162136
    new-instance v3, LX/Faw;

    invoke-direct {v3, v0}, LX/Faw;-><init>(LX/CwT;)V

    .line 162137
    const-class v4, Landroid/content/Context;

    invoke-interface {v2, v4}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1141

    invoke-static {v2, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1032

    invoke-static {v2, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    .line 162138
    iput-object v4, v3, LX/Faw;->a:LX/0Ot;

    iput-object v5, v3, LX/Faw;->b:LX/0Ot;

    iput-object v6, v3, LX/Faw;->c:LX/0Ot;

    .line 162139
    move-object v0, v3

    .line 162140
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 162108
    iget-object v0, p0, LX/0x9;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CwT;

    .line 162109
    iget-boolean p0, v0, LX/CwT;->e:Z

    move v0, p0

    .line 162110
    if-eqz v0, :cond_0

    .line 162111
    const/4 v0, 0x1

    .line 162112
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 161977
    iget-object v0, p0, LX/0x9;->g:LX/0ad;

    sget-short v1, LX/100;->af:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 161978
    if-nez v0, :cond_1

    .line 161979
    :cond_0
    :goto_0
    return-void

    .line 161980
    :cond_1
    invoke-direct {p0}, LX/0x9;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 161981
    iget-object v0, p0, LX/0x9;->q:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0x9;->q:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->k()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 161982
    iget-object v0, p0, LX/0x9;->q:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->k()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0x9;->b(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;)V

    .line 161983
    iget-object v0, p0, LX/0x9;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/7CP;->f:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 161984
    const/4 v0, 0x0

    iput-object v0, p0, LX/0x9;->q:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;

    .line 161985
    :cond_2
    iget-object v0, p0, LX/0x9;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/7CP;->e:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    iget-object v0, p0, LX/0x9;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-gtz v0, :cond_0

    .line 161986
    iget-object v0, p0, LX/0x9;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FaR;

    .line 161987
    iget-object v1, v0, LX/FaR;->d:LX/0x9;

    if-eqz v1, :cond_3

    .line 161988
    iget-object v1, v0, LX/FaR;->b:LX/1Ck;

    const-string v2, "search_awareness_learning_nux_config_loader_key"

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 161989
    :cond_3
    iput-object p0, v0, LX/FaR;->d:LX/0x9;

    .line 161990
    iget-object v0, p0, LX/0x9;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FaR;

    .line 161991
    iget-object v4, v0, LX/FaR;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/7CP;->f:LX/0Tn;

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v4

    .line 161992
    new-instance v5, LX/A0V;

    invoke-direct {v5}, LX/A0V;-><init>()V

    move-object v5, v5

    .line 161993
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    if-eqz v4, :cond_4

    sget-object v4, LX/0zS;->c:LX/0zS;

    :goto_1
    invoke-virtual {v5, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    sget-object v5, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v4, v5}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v4

    const-wide/32 v6, 0x5265c00

    invoke-virtual {v4, v6, v7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v4

    .line 161994
    const-string v5, "search_awareness_learning_nux_config_loader_key"

    .line 161995
    new-instance v6, LX/FaP;

    invoke-direct {v6, v0, v4}, LX/FaP;-><init>(LX/FaR;LX/0zO;)V

    .line 161996
    new-instance v7, LX/FaQ;

    invoke-direct {v7, v0}, LX/FaQ;-><init>(LX/FaR;)V

    .line 161997
    iget-object v8, v0, LX/FaR;->b:LX/1Ck;

    invoke-virtual {v8, v5, v6, v7}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 161998
    goto/16 :goto_0

    .line 161999
    :cond_4
    sget-object v4, LX/0zS;->a:LX/0zS;

    goto :goto_1
.end method

.method public final a(LX/A0Z;)V
    .locals 2

    .prologue
    .line 162096
    iget-object v0, p0, LX/0x9;->h:Ljava/util/Map;

    invoke-interface {p1}, LX/A0Z;->e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CwT;

    .line 162097
    if-nez v0, :cond_1

    .line 162098
    :cond_0
    return-void

    .line 162099
    :cond_1
    iget-object v1, p0, LX/0x9;->h:Ljava/util/Map;

    .line 162100
    iget-object p1, v0, LX/CwT;->b:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-object v0, p1

    .line 162101
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CwT;

    .line 162102
    :goto_0
    if-eqz v0, :cond_0

    .line 162103
    const/4 v1, 0x0

    .line 162104
    iput-boolean v1, v0, LX/CwT;->e:Z

    .line 162105
    iget-object v1, p0, LX/0x9;->h:Ljava/util/Map;

    .line 162106
    iget-object p1, v0, LX/CwT;->b:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-object v0, p1

    .line 162107
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CwT;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;LX/0P1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 162078
    iget-object v0, p0, LX/0x9;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FaB;

    .line 162079
    iget-object v1, p0, LX/0x9;->h:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CwT;

    .line 162080
    if-eqz v0, :cond_0

    .line 162081
    invoke-interface {v0, p2}, LX/FaB;->a(LX/0P1;)V

    .line 162082
    if-eqz v1, :cond_0

    .line 162083
    iget-object v0, p0, LX/0x9;->h:Ljava/util/Map;

    .line 162084
    iget-object p1, v1, LX/CwT;->c:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-object p1, p1

    .line 162085
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CwT;

    .line 162086
    if-nez v0, :cond_1

    .line 162087
    :cond_0
    return-void

    .line 162088
    :cond_1
    :goto_0
    if-eqz v0, :cond_0

    .line 162089
    iget-boolean p1, v0, LX/CwT;->e:Z

    move p1, p1

    .line 162090
    if-eqz p1, :cond_0

    .line 162091
    const/4 p1, 0x0

    .line 162092
    iput-boolean p1, v0, LX/CwT;->e:Z

    .line 162093
    iget-object p1, p0, LX/0x9;->h:Ljava/util/Map;

    .line 162094
    iget-object v1, v0, LX/CwT;->c:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-object v0, v1

    .line 162095
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CwT;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;Z)V
    .locals 2

    .prologue
    .line 162070
    iget-object v0, p0, LX/0x9;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FaB;

    .line 162071
    iget-object v1, p0, LX/0x9;->h:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CwT;

    .line 162072
    if-eqz v0, :cond_0

    .line 162073
    invoke-interface {v0}, LX/FaB;->d()V

    .line 162074
    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    .line 162075
    iget-object v0, v1, LX/CwT;->a:LX/A0Z;

    move-object v0, v0

    .line 162076
    invoke-virtual {p0, v0}, LX/0x9;->a(LX/A0Z;)V

    .line 162077
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)Z
    .locals 1

    .prologue
    .line 162068
    iget-object v0, p0, LX/0x9;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FaB;

    .line 162069
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/FaB;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;)Z
    .locals 10

    .prologue
    .line 162034
    iget-object v0, p0, LX/0x9;->f:LX/138;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 162035
    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 162036
    const-string v1, "Missing test name.\n"

    invoke-static {v0, v1}, LX/138;->a(LX/138;Ljava/lang/String;)V

    .line 162037
    const/4 v1, 0x0

    .line 162038
    :goto_0
    move v1, v1

    .line 162039
    if-eqz v1, :cond_5

    invoke-static {v0, p1}, LX/138;->a(LX/138;LX/A0Z;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v4, v2

    .line 162040
    :goto_1
    if-nez v4, :cond_0

    .line 162041
    invoke-static {v0}, LX/138;->a(LX/138;)V

    .line 162042
    invoke-static {v0}, LX/138;->b(LX/138;)V

    .line 162043
    :cond_0
    iget-object v1, v0, LX/138;->c:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 162044
    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->j()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_7

    .line 162045
    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->j()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const-class v6, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessSuggestionFieldsFragmentModel;

    invoke-virtual {v5, v1, v3, v6}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v1

    .line 162046
    if-eqz v1, :cond_6

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 162047
    :goto_2
    if-eqz v4, :cond_8

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 162048
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v9

    move v8, v7

    move v5, v6

    :goto_3
    if-ge v8, v9, :cond_3

    invoke-virtual {v1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/A0Z;

    .line 162049
    if-eqz v5, :cond_2

    invoke-static {v0, v4}, LX/138;->a(LX/138;LX/A0Z;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v5, v6

    .line 162050
    :goto_4
    iget-object p0, v0, LX/138;->c:Ljava/util/Set;

    invoke-interface {v4}, LX/A0Z;->e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object p1

    invoke-interface {p0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 162051
    new-instance v5, Ljava/lang/StringBuilder;

    const-string p0, "Duplicate configuration for template: "

    invoke-direct {v5, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, LX/A0Z;->e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object p0

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, LX/138;->a(LX/138;Ljava/lang/String;)V

    move v5, v7

    .line 162052
    :cond_1
    iget-object p0, v0, LX/138;->c:Ljava/util/Set;

    invoke-interface {v4}, LX/A0Z;->e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object v4

    invoke-interface {p0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 162053
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_3

    :cond_2
    move v5, v7

    .line 162054
    goto :goto_4

    .line 162055
    :cond_3
    move v1, v5

    .line 162056
    if-eqz v1, :cond_8

    move v1, v2

    .line 162057
    :goto_5
    if-nez v1, :cond_4

    .line 162058
    invoke-static {v0}, LX/138;->a(LX/138;)V

    .line 162059
    :cond_4
    invoke-static {v0}, LX/138;->b(LX/138;)V

    .line 162060
    move v0, v1

    .line 162061
    return v0

    :cond_5
    move v4, v3

    .line 162062
    goto/16 :goto_1

    .line 162063
    :cond_6
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 162064
    goto :goto_2

    .line 162065
    :cond_7
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 162066
    goto :goto_2

    :cond_8
    move v1, v3

    .line 162067
    goto :goto_5

    :cond_9
    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method public final b(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)V
    .locals 1

    .prologue
    .line 162031
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 162032
    invoke-virtual {p0, p1, v0}, LX/0x9;->a(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;LX/0P1;)V

    .line 162033
    return-void
.end method

.method public final b(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;Z)V
    .locals 0

    .prologue
    .line 162028
    invoke-virtual {p0, p1, p2}, LX/0x9;->a(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;Z)V

    .line 162029
    invoke-direct {p0, p1}, LX/0x9;->d(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)V

    .line 162030
    return-void
.end method

.method public final b(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 162005
    iget-object v0, p0, LX/0x9;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 162006
    iget-object v0, p0, LX/0x9;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 162007
    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 162008
    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessSuggestionFieldsFragmentModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 162009
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    move-object v1, v0

    .line 162010
    :goto_1
    new-instance v0, LX/CwT;

    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, p1, v3, v4}, LX/CwT;-><init>(LX/A0Z;Ljava/lang/String;Ljava/lang/String;)V

    .line 162011
    iget-object v3, p0, LX/0x9;->h:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162012
    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object v3

    invoke-direct {p0, v3}, LX/0x9;->e(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)V

    .line 162013
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    move-object v3, v0

    :goto_2
    if-ge v2, v5, :cond_2

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A0Z;

    .line 162014
    invoke-interface {v0}, LX/A0Z;->e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object v4

    .line 162015
    iput-object v4, v3, LX/CwT;->b:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    .line 162016
    new-instance v4, LX/CwT;

    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->k()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v0, v6, v7}, LX/CwT;-><init>(LX/A0Z;Ljava/lang/String;Ljava/lang/String;)V

    .line 162017
    iget-object v6, p0, LX/0x9;->h:Ljava/util/Map;

    invoke-interface {v0}, LX/A0Z;->e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object v7

    invoke-interface {v6, v7, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162018
    iget-object v6, v3, LX/CwT;->a:LX/A0Z;

    move-object v3, v6

    .line 162019
    invoke-interface {v3}, LX/A0Z;->e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object v3

    .line 162020
    iput-object v3, v4, LX/CwT;->c:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    .line 162021
    invoke-interface {v0}, LX/A0Z;->e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0x9;->e(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)V

    .line 162022
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-object v3, v4

    goto :goto_2

    .line 162023
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 162024
    goto :goto_0

    .line 162025
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 162026
    move-object v1, v0

    goto :goto_1

    .line 162027
    :cond_2
    return-void
.end method

.method public final c(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)V
    .locals 3

    .prologue
    .line 162000
    const/4 v0, 0x0

    .line 162001
    iget-object v1, p0, LX/0x9;->g:LX/0ad;

    sget-short v2, LX/100;->ae:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/0x9;->g:LX/0ad;

    sget-short v2, LX/100;->af:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    move v0, v0

    .line 162002
    if-eqz v0, :cond_2

    .line 162003
    invoke-direct {p0, p1}, LX/0x9;->d(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)V

    .line 162004
    :cond_2
    return-void
.end method
