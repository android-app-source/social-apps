.class public LX/1JR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Boolean;

.field private h:Ljava/lang/Integer;

.field public i:LX/0W3;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 230077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230078
    return-void
.end method

.method public static b(LX/0QB;)LX/1JR;
    .locals 2

    .prologue
    .line 230079
    new-instance v1, LX/1JR;

    invoke-direct {v1}, LX/1JR;-><init>()V

    .line 230080
    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v0

    check-cast v0, LX/0W3;

    .line 230081
    iput-object v0, v1, LX/1JR;->i:LX/0W3;

    .line 230082
    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 230083
    iget-object v0, p0, LX/1JR;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 230084
    iget-object v0, p0, LX/1JR;->i:LX/0W3;

    sget-wide v2, LX/0X5;->eB:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1JR;->a:Ljava/lang/Boolean;

    .line 230085
    :cond_0
    iget-object v0, p0, LX/1JR;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 230086
    iget-object v0, p0, LX/1JR;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 230087
    iget-object v0, p0, LX/1JR;->i:LX/0W3;

    sget-wide v2, LX/0X5;->eI:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1JR;->b:Ljava/lang/Boolean;

    .line 230088
    :cond_0
    iget-object v0, p0, LX/1JR;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final h()I
    .locals 4

    .prologue
    .line 230089
    iget-object v0, p0, LX/1JR;->h:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 230090
    iget-object v0, p0, LX/1JR;->i:LX/0W3;

    sget-wide v2, LX/0X5;->eG:J

    const/16 v1, 0xa

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/1JR;->h:Ljava/lang/Integer;

    .line 230091
    :cond_0
    iget-object v0, p0, LX/1JR;->h:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
