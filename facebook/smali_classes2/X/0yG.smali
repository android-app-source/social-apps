.class public final enum LX/0yG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0yG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0yG;

.field public static final enum LOCATION_DISABLED:LX/0yG;

.field public static final enum LOCATION_UNSUPPORTED:LX/0yG;

.field public static final enum OKAY:LX/0yG;

.field public static final enum PERMISSION_DENIED:LX/0yG;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 164237
    new-instance v0, LX/0yG;

    const-string v1, "PERMISSION_DENIED"

    invoke-direct {v0, v1, v2}, LX/0yG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yG;->PERMISSION_DENIED:LX/0yG;

    .line 164238
    new-instance v0, LX/0yG;

    const-string v1, "LOCATION_UNSUPPORTED"

    invoke-direct {v0, v1, v3}, LX/0yG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yG;->LOCATION_UNSUPPORTED:LX/0yG;

    .line 164239
    new-instance v0, LX/0yG;

    const-string v1, "LOCATION_DISABLED"

    invoke-direct {v0, v1, v4}, LX/0yG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yG;->LOCATION_DISABLED:LX/0yG;

    .line 164240
    new-instance v0, LX/0yG;

    const-string v1, "OKAY"

    invoke-direct {v0, v1, v5}, LX/0yG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yG;->OKAY:LX/0yG;

    .line 164241
    const/4 v0, 0x4

    new-array v0, v0, [LX/0yG;

    sget-object v1, LX/0yG;->PERMISSION_DENIED:LX/0yG;

    aput-object v1, v0, v2

    sget-object v1, LX/0yG;->LOCATION_UNSUPPORTED:LX/0yG;

    aput-object v1, v0, v3

    sget-object v1, LX/0yG;->LOCATION_DISABLED:LX/0yG;

    aput-object v1, v0, v4

    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    aput-object v1, v0, v5

    sput-object v0, LX/0yG;->$VALUES:[LX/0yG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 164242
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0yG;
    .locals 1

    .prologue
    .line 164243
    const-class v0, LX/0yG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0yG;

    return-object v0
.end method

.method public static values()[LX/0yG;
    .locals 1

    .prologue
    .line 164244
    sget-object v0, LX/0yG;->$VALUES:[LX/0yG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0yG;

    return-object v0
.end method
