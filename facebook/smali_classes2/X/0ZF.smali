.class public LX/0ZF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0VI;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 83097
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83098
    iput-object p1, p0, LX/0ZF;->a:LX/0Ot;

    .line 83099
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83100
    const-string v0, "loom_config"

    return-object v0
.end method

.method public final a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 83096
    iget-object v0, p0, LX/0ZF;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->ib:J

    const-string v1, "<no config>"

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
