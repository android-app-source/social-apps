.class public LX/0tc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/graphql/executor/GraphQLResult;

.field private static volatile n:LX/0tc;


# instance fields
.field public volatile b:J
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public volatile c:J

.field public volatile d:J

.field public volatile e:J

.field public final f:Ljava/util/List;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1NB;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/List;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1NB;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0t5;

.field public final i:LX/0sT;

.field private final j:LX/0Uh;

.field public final k:Ljava/util/concurrent/locks/Lock;

.field public final l:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/1NB;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/1NB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 155036
    new-instance v0, Lcom/facebook/graphql/executor/GraphQLResult;

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    sget-object v2, LX/0ta;->NO_DATA:LX/0ta;

    const-wide/16 v4, 0x0

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;J)V

    sput-object v0, LX/0tc;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    return-void
.end method

.method public constructor <init>(LX/0t5;LX/0sT;LX/0Uh;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v0, 0x0

    .line 155037
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155038
    iput-wide v0, p0, LX/0tc;->b:J

    .line 155039
    iput-wide v0, p0, LX/0tc;->c:J

    .line 155040
    iput-wide v0, p0, LX/0tc;->d:J

    .line 155041
    iput-wide v0, p0, LX/0tc;->e:J

    .line 155042
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/0tc;->f:Ljava/util/List;

    .line 155043
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, LX/0tc;->k:Ljava/util/concurrent/locks/Lock;

    .line 155044
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/0tc;->l:Ljava/util/LinkedList;

    .line 155045
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/0tc;->m:Ljava/util/LinkedList;

    .line 155046
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/0tc;->g:Ljava/util/List;

    .line 155047
    iput-object p1, p0, LX/0tc;->h:LX/0t5;

    .line 155048
    iput-object p2, p0, LX/0tc;->i:LX/0sT;

    .line 155049
    iput-object p3, p0, LX/0tc;->j:LX/0Uh;

    .line 155050
    return-void
.end method

.method public static a(LX/0QB;)LX/0tc;
    .locals 6

    .prologue
    .line 155051
    sget-object v0, LX/0tc;->n:LX/0tc;

    if-nez v0, :cond_1

    .line 155052
    const-class v1, LX/0tc;

    monitor-enter v1

    .line 155053
    :try_start_0
    sget-object v0, LX/0tc;->n:LX/0tc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 155054
    if-eqz v2, :cond_0

    .line 155055
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 155056
    new-instance p0, LX/0tc;

    invoke-static {v0}, LX/0t5;->a(LX/0QB;)LX/0t5;

    move-result-object v3

    check-cast v3, LX/0t5;

    invoke-static {v0}, LX/0sT;->a(LX/0QB;)LX/0sT;

    move-result-object v4

    check-cast v4, LX/0sT;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, LX/0tc;-><init>(LX/0t5;LX/0sT;LX/0Uh;)V

    .line 155057
    move-object v0, p0

    .line 155058
    sput-object v0, LX/0tc;->n:LX/0tc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155059
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 155060
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 155061
    :cond_1
    sget-object v0, LX/0tc;->n:LX/0tc;

    return-object v0

    .line 155062
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 155063
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static synthetic a(Lcom/facebook/quicklog/QuickPerformanceLogger;IISLjava/lang/String;I)V
    .locals 6

    .prologue
    .line 155064
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 155065
    if-eqz p0, :cond_0

    .line 155066
    if-eq p1, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 155067
    if-eq p2, v3, :cond_2

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 155068
    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IISLjava/lang/String;Ljava/lang/String;)V

    .line 155069
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 155070
    goto :goto_0

    :cond_2
    move v1, v2

    .line 155071
    goto :goto_1
.end method


# virtual methods
.method public final a(Z)LX/1NB;
    .locals 3

    .prologue
    .line 155072
    new-instance v0, LX/1NB;

    new-instance v1, LX/1NC;

    invoke-direct {v1, p1}, LX/1NC;-><init>(Z)V

    invoke-direct {v0, p0, v1}, LX/1NB;-><init>(LX/0tc;LX/1NC;)V

    return-object v0
.end method

.method public final a(LX/3Bq;)LX/37X;
    .locals 3

    .prologue
    .line 155073
    new-instance v0, LX/37X;

    new-instance v1, LX/3Bw;

    invoke-direct {v1, p1}, LX/3Bw;-><init>(LX/3Bq;)V

    invoke-direct {v0, p0, v1}, LX/37X;-><init>(LX/0tc;LX/3Bw;)V

    return-object v0
.end method

.method public final varargs a([LX/4VT;)LX/37X;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "LX/4VT",
            "<**>;)",
            "LX/37X;"
        }
    .end annotation

    .prologue
    .line 155074
    new-instance v0, LX/4VL;

    iget-object v1, p0, LX/0tc;->j:LX/0Uh;

    invoke-direct {v0, v1, p1}, LX/4VL;-><init>(LX/0Uh;[LX/4VT;)V

    invoke-virtual {p0, v0}, LX/0tc;->a(LX/3Bq;)LX/37X;

    move-result-object v0

    return-object v0
.end method

.method public final a(ZLjava/util/Collection;)LX/3U2;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/3U2;"
        }
    .end annotation

    .prologue
    .line 155075
    new-instance v0, LX/3U2;

    new-instance v1, LX/1NC;

    invoke-direct {v1, p1}, LX/1NC;-><init>(Z)V

    invoke-direct {v0, p0, v1}, LX/3U2;-><init>(LX/0tc;LX/1NC;)V

    move-object v1, v0

    .line 155076
    iget-object v0, v1, LX/1NB;->f:LX/1ND;

    check-cast v0, LX/1NC;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, p2}, LX/1NC;->a(Ljava/util/Collection;Ljava/util/Collection;)V

    .line 155077
    return-object v1
.end method

.method public final b(Z)LX/1NA;
    .locals 2

    .prologue
    .line 155078
    new-instance v0, LX/1NA;

    new-instance v1, LX/1NC;

    invoke-direct {v1, p1}, LX/1NC;-><init>(Z)V

    invoke-direct {v0, p0, v1}, LX/1NA;-><init>(LX/0tc;LX/1NC;)V

    return-object v0
.end method

.method public final b(ZLjava/util/Collection;)LX/1NB;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/1NB;"
        }
    .end annotation

    .prologue
    .line 155079
    invoke-virtual {p0, p1}, LX/0tc;->a(Z)LX/1NB;

    move-result-object v1

    .line 155080
    iget-object v0, v1, LX/1NB;->f:LX/1ND;

    check-cast v0, LX/1NC;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, p2}, LX/1NC;->a(Ljava/util/Collection;Ljava/util/Collection;)V

    .line 155081
    return-object v1
.end method
