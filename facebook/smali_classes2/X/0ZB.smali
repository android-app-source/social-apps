.class public final LX/0ZB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0VI;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0ZB;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0pV;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 83017
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83018
    iput-object p1, p0, LX/0ZB;->a:LX/0Ot;

    .line 83019
    return-void
.end method

.method public static a(LX/0QB;)LX/0ZB;
    .locals 4

    .prologue
    .line 83020
    sget-object v0, LX/0ZB;->b:LX/0ZB;

    if-nez v0, :cond_1

    .line 83021
    const-class v1, LX/0ZB;

    monitor-enter v1

    .line 83022
    :try_start_0
    sget-object v0, LX/0ZB;->b:LX/0ZB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 83023
    if-eqz v2, :cond_0

    .line 83024
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 83025
    new-instance v3, LX/0ZB;

    const/16 p0, 0x685

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/0ZB;-><init>(LX/0Ot;)V

    .line 83026
    move-object v0, v3

    .line 83027
    sput-object v0, LX/0ZB;->b:LX/0ZB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83028
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 83029
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 83030
    :cond_1
    sget-object v0, LX/0ZB;->b:LX/0ZB;

    return-object v0

    .line 83031
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 83032
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83033
    const-string v0, "news_feed_events"

    return-object v0
.end method

.method public final a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 83034
    iget-object v0, p0, LX/0ZB;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pV;

    .line 83035
    instance-of p0, p1, Ljava/lang/IllegalStateException;

    if-eqz p0, :cond_0

    .line 83036
    iget-object p0, v0, LX/0pV;->b:LX/0Yw;

    invoke-virtual {p0}, LX/0Yw;->toString()Ljava/lang/String;

    move-result-object p0

    .line 83037
    :goto_0
    move-object v0, p0

    .line 83038
    return-object v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method
