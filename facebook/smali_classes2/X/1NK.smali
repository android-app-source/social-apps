.class public final LX/1NK;
.super LX/1CO;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1CO",
        "<",
        "LX/1NL;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0g2;


# direct methods
.method public constructor <init>(LX/0g2;)V
    .locals 0

    .prologue
    .line 237354
    iput-object p1, p0, LX/1NK;->a:LX/0g2;

    invoke-direct {p0}, LX/1CO;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/1NL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237325
    const-class v0, LX/1NL;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 9

    .prologue
    .line 237326
    check-cast p1, LX/1NL;

    const/4 v5, 0x1

    .line 237327
    iget-object v0, p0, LX/1NK;->a:LX/0g2;

    iget-object v0, v0, LX/0g2;->b:LX/0gC;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1NK;->a:LX/0g2;

    iget-object v0, v0, LX/0g2;->b:LX/0gC;

    invoke-interface {v0}, LX/0gC;->u()Z

    move-result v0

    if-nez v0, :cond_1

    .line 237328
    :cond_0
    :goto_0
    return-void

    .line 237329
    :cond_1
    iget-object v0, p0, LX/1NK;->a:LX/0g2;

    iget-object v0, v0, LX/0g2;->b:LX/0gC;

    invoke-interface {v0}, LX/0gC;->h()LX/0fz;

    move-result-object v0

    invoke-virtual {v0}, LX/0fz;->size()I

    move-result v0

    iget v1, p1, LX/1NL;->a:I

    add-int/lit8 v1, v1, 0x4

    if-gt v0, v1, :cond_2

    .line 237330
    const-string v0, "Too few stories. Load more tail stories."

    .line 237331
    iget-object v1, p0, LX/1NK;->a:LX/0g2;

    iget-object v1, v1, LX/0g2;->T:Landroid/content/Context;

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 237332
    :cond_2
    iget-object v0, p0, LX/1NK;->a:LX/0g2;

    iget-object v0, v0, LX/0g2;->b:LX/0gC;

    invoke-interface {v0}, LX/0gC;->h()LX/0fz;

    move-result-object v0

    invoke-virtual {v0}, LX/0fz;->size()I

    move-result v0

    .line 237333
    iget-object v1, p0, LX/1NK;->a:LX/0g2;

    iget-object v1, v1, LX/0g2;->c:LX/0fz;

    iget v2, p1, LX/1NL;->a:I

    .line 237334
    if-gtz v2, :cond_3

    .line 237335
    :goto_1
    iget-object v1, p0, LX/1NK;->a:LX/0g2;

    invoke-static {v1}, LX/0g2;->R(LX/0g2;)V

    .line 237336
    iget-object v1, p0, LX/1NK;->a:LX/0g2;

    iget-object v1, v1, LX/0g2;->F:LX/0aG;

    const-string v2, "feed_clear_cache"

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const v4, -0x2a283a1

    invoke-static {v1, v2, v3, v4}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v1

    invoke-interface {v1, v5}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    .line 237337
    iget-object v1, p0, LX/1NK;->a:LX/0g2;

    iget-object v1, v1, LX/0g2;->af:LX/0g5;

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0g7;->d(II)V

    .line 237338
    iget-object v1, p0, LX/1NK;->a:LX/0g2;

    iget-object v1, v1, LX/0g2;->b:LX/0gC;

    invoke-interface {v1}, LX/0gC;->h()LX/0fz;

    move-result-object v1

    invoke-virtual {v1}, LX/0fz;->size()I

    move-result v1

    sub-int/2addr v0, v1

    .line 237339
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cleared "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " stories"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 237340
    iget-object v1, p0, LX/1NK;->a:LX/0g2;

    iget-object v1, v1, LX/0g2;->T:Landroid/content/Context;

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 237341
    iget-object v0, p0, LX/1NK;->a:LX/0g2;

    invoke-static {v0}, LX/0g2;->af(LX/0g2;)V

    goto/16 :goto_0

    .line 237342
    :cond_3
    invoke-virtual {v1}, LX/0fz;->size()I

    move-result v3

    if-lt v2, v3, :cond_4

    .line 237343
    invoke-virtual {v1}, LX/0fz;->m()V

    goto :goto_1

    .line 237344
    :cond_4
    iget-object v3, v1, LX/0fz;->g:LX/0qu;

    invoke-virtual {v3}, LX/0qu;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 237345
    const/4 v3, 0x0

    move v4, v3

    :goto_2
    if-ge v4, v2, :cond_6

    .line 237346
    iget-object v7, v1, LX/0fz;->j:LX/0qx;

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 237347
    iget-object v8, v7, LX/0qx;->c:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    sget p1, LX/0qx;->a:I

    if-ge v8, p1, :cond_5

    .line 237348
    iget-object v8, v7, LX/0qx;->c:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237349
    :cond_5
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v1, v3}, LX/0fz;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 237350
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 237351
    :cond_6
    invoke-virtual {v1}, LX/0fz;->v()I

    move-result v3

    if-lez v3, :cond_7

    .line 237352
    const/4 v3, 0x1

    iput-boolean v3, v1, LX/0fz;->n:Z

    goto/16 :goto_1

    .line 237353
    :cond_7
    invoke-virtual {v1}, LX/0fz;->m()V

    goto/16 :goto_1
.end method
