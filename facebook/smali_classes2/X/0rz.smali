.class public LX/0rz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/0rz;


# instance fields
.field private final a:Z

.field public b:Ljava/lang/String;

.field public c:Ljava/util/concurrent/ExecutorService;

.field private d:LX/0ad;

.field private e:LX/0dx;


# direct methods
.method public constructor <init>(LX/0s1;Ljava/util/concurrent/ExecutorService;LX/0ad;LX/0dx;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 151096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151097
    invoke-interface {p1}, LX/0s1;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0rz;->b:Ljava/lang/String;

    .line 151098
    iput-object p2, p0, LX/0rz;->c:Ljava/util/concurrent/ExecutorService;

    .line 151099
    iput-object p3, p0, LX/0rz;->d:LX/0ad;

    .line 151100
    sget-object v0, LX/007;->m:Ljava/lang/String;

    move-object v0, v0

    .line 151101
    if-eqz v0, :cond_0

    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, p1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string p1, "x86"

    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151102
    const/4 v0, 0x1

    .line 151103
    :goto_0
    move v0, v0

    .line 151104
    iput-boolean v0, p0, LX/0rz;->a:Z

    .line 151105
    iput-object p4, p0, LX/0rz;->e:LX/0dx;

    .line 151106
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/0rz;
    .locals 7

    .prologue
    .line 151061
    sget-object v0, LX/0rz;->f:LX/0rz;

    if-nez v0, :cond_1

    .line 151062
    const-class v1, LX/0rz;

    monitor-enter v1

    .line 151063
    :try_start_0
    sget-object v0, LX/0rz;->f:LX/0rz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 151064
    if-eqz v2, :cond_0

    .line 151065
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 151066
    new-instance p0, LX/0rz;

    invoke-static {v0}, LX/0s0;->a(LX/0QB;)LX/0s0;

    move-result-object v3

    check-cast v3, LX/0s1;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/0dt;->b(LX/0QB;)LX/0dx;

    move-result-object v6

    check-cast v6, LX/0dx;

    invoke-direct {p0, v3, v4, v5, v6}, LX/0rz;-><init>(LX/0s1;Ljava/util/concurrent/ExecutorService;LX/0ad;LX/0dx;)V

    .line 151067
    move-object v0, p0

    .line 151068
    sput-object v0, LX/0rz;->f:LX/0rz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151069
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 151070
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 151071
    :cond_1
    sget-object v0, LX/0rz;->f:LX/0rz;

    return-object v0

    .line 151072
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 151073
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 151074
    if-eqz p0, :cond_0

    const-string v0, ".facebook.com"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 151075
    :cond_0
    :goto_0
    return-void

    .line 151076
    :cond_1
    :try_start_0
    invoke-static {p0}, Ljava/net/InetAddress;->getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 151077
    if-nez v0, :cond_2

    .line 151078
    new-instance v0, Ljava/net/UnknownHostException;

    invoke-direct {v0}, Ljava/net/UnknownHostException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 151079
    :catch_0
    move-exception v0

    .line 151080
    const-string v1, "UDPPrimingHelper"

    const-string v2, "Probably bad host name"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 151081
    :cond_2
    :try_start_1
    sget-object v1, LX/00d;->a:LX/00d;

    move-object v1, v1

    .line 151082
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    .line 151083
    if-eqz p0, :cond_3

    if-nez v0, :cond_4
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    .line 151084
    :cond_3
    :goto_1
    goto :goto_0

    .line 151085
    :catch_1
    move-exception v0

    .line 151086
    const-string v1, "UDPPrimingHelper"

    const-string v2, "Permission error"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 151087
    :cond_4
    iput-object p0, v1, LX/00d;->c:Ljava/lang/String;

    .line 151088
    iput-object v0, v1, LX/00d;->d:[B

    .line 151089
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, v1, LX/00d;->e:J

    .line 151090
    iget-object v3, v1, LX/00d;->b:Landroid/content/SharedPreferences;

    if-eqz v3, :cond_3

    .line 151091
    iget-object v3, v1, LX/00d;->b:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 151092
    const-string v4, "UDP_PRIMING_DNS/HOST_NAME"

    iget-object v5, v1, LX/00d;->c:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 151093
    const-string v4, "UDP_PRIMING_DNS/HOST_IP"

    iget-object v5, v1, LX/00d;->d:[B

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 151094
    const-string v4, "UDP_PRIMING_DNS/HOST_TIMESTAMP"

    iget-wide v5, v1, LX/00d;->e:J

    invoke-interface {v3, v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 151095
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1
.end method

.method public static a(Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 3

    .prologue
    .line 151052
    const-string v0, "X-FB-Priming-Channel-ID"

    .line 151053
    sget-object v1, LX/00c;->a:LX/00c;

    move-object v1, v1

    .line 151054
    iget-object v2, v1, LX/00c;->c:Ljava/lang/String;

    move-object v1, v2

    .line 151055
    invoke-interface {p0, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 151056
    return-void
.end method

.method public static a(Lorg/apache/http/HttpResponse;)Z
    .locals 2

    .prologue
    .line 151057
    const-string v0, "udp_prime_succeeded"

    invoke-interface {p0, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 151058
    if-eqz v0, :cond_0

    .line 151059
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 151060
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/0rz;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 151051
    iget-object v1, p0, LX/0rz;->e:LX/0dx;

    invoke-interface {v1}, LX/0dy;->a()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/0rz;->d:LX/0ad;

    sget-short v2, LX/0by;->aa:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static f(LX/0rz;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 151050
    iget-object v1, p0, LX/0rz;->e:LX/0dx;

    invoke-interface {v1}, LX/0dy;->a()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/0rz;->d:LX/0ad;

    sget-short v2, LX/0by;->ab:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public final a(LX/0n9;)V
    .locals 2

    .prologue
    .line 151044
    const-string v0, "primed"

    const-string v1, "true"

    .line 151045
    invoke-static {p1, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 151046
    const-string v0, "user-agent"

    .line 151047
    iget-object v1, p0, LX/0rz;->b:Ljava/lang/String;

    move-object v1, v1

    .line 151048
    invoke-static {p1, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 151049
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 7

    .prologue
    .line 151034
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 151035
    if-nez v0, :cond_1

    .line 151036
    :cond_0
    :goto_0
    return-void

    .line 151037
    :cond_1
    sget-object v1, LX/00d;->a:LX/00d;

    move-object v1, v1

    .line 151038
    iget-object v3, v1, LX/00d;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-wide v3, v1, LX/00d;->e:J

    const-wide/32 v5, 0x6ddd00

    invoke-static {v3, v4, v5, v6}, LX/00d;->a(JJ)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 151039
    :cond_2
    const/4 v3, 0x1

    .line 151040
    :goto_1
    move v1, v3

    .line 151041
    if-eqz v1, :cond_0

    .line 151042
    iget-object v1, p0, LX/0rz;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/http/protocol/UDPPrimingHelper$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/http/protocol/UDPPrimingHelper$1;-><init>(LX/0rz;Ljava/lang/String;)V

    const v0, -0x75e619ea

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 151043
    invoke-static {p0}, LX/0rz;->e(LX/0rz;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/0rz;->f(LX/0rz;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
