.class public LX/0sl;
.super LX/0Tr;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0sl;


# instance fields
.field private final a:LX/0V8;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/0sp;LX/0V8;Ljava/lang/String;)V
    .locals 1
    .param p2    # LX/0Tt;
        .annotation runtime Lcom/facebook/database/threadchecker/AllowAnyThread;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/graphql/executor/annotations/GraphQLDiskCacheDatabaseName;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 153214
    invoke-static {p3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p5}, LX/0Tr;-><init>(Landroid/content/Context;LX/0Tt;LX/0Px;Ljava/lang/String;)V

    .line 153215
    iput-object p4, p0, LX/0sl;->a:LX/0V8;

    .line 153216
    return-void
.end method

.method public static a(LX/0QB;)LX/0sl;
    .locals 9

    .prologue
    .line 153217
    sget-object v0, LX/0sl;->b:LX/0sl;

    if-nez v0, :cond_1

    .line 153218
    const-class v1, LX/0sl;

    monitor-enter v1

    .line 153219
    :try_start_0
    sget-object v0, LX/0sl;->b:LX/0sl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 153220
    if-eqz v2, :cond_0

    .line 153221
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 153222
    new-instance v3, LX/0sl;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0sm;->a(LX/0QB;)LX/0Tt;

    move-result-object v5

    check-cast v5, LX/0Tt;

    invoke-static {v0}, LX/0sp;->a(LX/0QB;)LX/0sp;

    move-result-object v6

    check-cast v6, LX/0sp;

    invoke-static {v0}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v7

    check-cast v7, LX/0V8;

    .line 153223
    const-string v8, "graphql_cache"

    move-object v8, v8

    .line 153224
    move-object v8, v8

    .line 153225
    check-cast v8, Ljava/lang/String;

    invoke-direct/range {v3 .. v8}, LX/0sl;-><init>(Landroid/content/Context;LX/0Tt;LX/0sp;LX/0V8;Ljava/lang/String;)V

    .line 153226
    move-object v0, v3

    .line 153227
    sput-object v0, LX/0sl;->b:LX/0sl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 153228
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 153229
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 153230
    :cond_1
    sget-object v0, LX/0sl;->b:LX/0sl;

    return-object v0

    .line 153231
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 153232
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final c()J
    .locals 8

    .prologue
    .line 153233
    iget-object v0, p0, LX/0sl;->a:LX/0V8;

    sget-object v1, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v0, v1}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v0

    .line 153234
    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x64

    div-long v2, v0, v2

    .line 153235
    const-wide/32 v0, 0x500000

    const-wide/32 v4, 0x1900000

    .line 153236
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    move-wide v0, v6

    .line 153237
    return-wide v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 153238
    const/16 v0, 0x6400

    return v0
.end method

.method public final j()Landroid/database/DatabaseErrorHandler;
    .locals 2

    .prologue
    .line 153239
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    .line 153240
    new-instance v0, LX/255;

    invoke-direct {v0}, LX/255;-><init>()V

    .line 153241
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, LX/0Tr;->j()Landroid/database/DatabaseErrorHandler;

    move-result-object v0

    goto :goto_0
.end method
