.class public LX/1fC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Gd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1Gd",
        "<",
        "LX/1ca",
        "<TT;>;>;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<TT;>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<TT;>;>;>;)V"
        }
    .end annotation

    .prologue
    .line 291094
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291095
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "List of suppliers is empty!"

    invoke-static {v0, v1}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 291096
    iput-object p1, p0, LX/1fC;->a:Ljava/util/List;

    .line 291097
    return-void

    .line 291098
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)LX/1fC;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<TT;>;>;>;)",
            "LX/1fC",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 291082
    new-instance v0, LX/1fC;

    invoke-direct {v0, p0}, LX/1fC;-><init>(Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 291093
    invoke-virtual {p0}, LX/1fC;->b()LX/1ca;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/1ca;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1ca",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 291092
    new-instance v0, LX/1fk;

    invoke-direct {v0, p0}, LX/1fk;-><init>(LX/1fC;)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 291085
    if-ne p1, p0, :cond_0

    .line 291086
    const/4 v0, 0x1

    .line 291087
    :goto_0
    return v0

    .line 291088
    :cond_0
    instance-of v0, p1, LX/1fC;

    if-nez v0, :cond_1

    .line 291089
    const/4 v0, 0x0

    goto :goto_0

    .line 291090
    :cond_1
    check-cast p1, LX/1fC;

    .line 291091
    iget-object v0, p0, LX/1fC;->a:Ljava/util/List;

    iget-object v1, p1, LX/1fC;->a:Ljava/util/List;

    invoke-static {v0, v1}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 291084
    iget-object v0, p0, LX/1fC;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 291083
    invoke-static {p0}, LX/15f;->a(Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "list"

    iget-object v2, p0, LX/1fC;->a:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    invoke-virtual {v0}, LX/15g;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
