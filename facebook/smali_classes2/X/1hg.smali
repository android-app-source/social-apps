.class public LX/1hg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:J

.field private b:J

.field private c:J

.field private d:J

.field private e:J

.field private final f:LX/0p7;

.field private final g:LX/0So;


# direct methods
.method public constructor <init>(LX/0p7;LX/0So;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const-wide/16 v0, -0x1

    .line 296267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296268
    iput-wide v2, p0, LX/1hg;->a:J

    .line 296269
    iput-wide v0, p0, LX/1hg;->c:J

    .line 296270
    iput-wide v0, p0, LX/1hg;->d:J

    .line 296271
    iput-wide v0, p0, LX/1hg;->e:J

    .line 296272
    iput-wide v2, p0, LX/1hg;->b:J

    .line 296273
    iput-object p1, p0, LX/1hg;->f:LX/0p7;

    .line 296274
    iput-object p2, p0, LX/1hg;->g:LX/0So;

    .line 296275
    return-void
.end method

.method private static b(LX/1hg;JJ)Z
    .locals 3

    .prologue
    .line 296276
    const-wide/16 v0, 0x2710

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const-wide/16 v0, 0xa

    cmp-long v0, p3, v0

    if-gez v0, :cond_1

    :cond_0
    const-wide/32 v0, 0xc350

    cmp-long v0, p1, v0

    if-ltz v0, :cond_2

    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-lez v0, :cond_2

    .line 296277
    :cond_1
    iget-object v0, p0, LX/1hg;->f:LX/0p7;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/0p7;->a(JJ)V

    .line 296278
    const/4 v0, 0x1

    .line 296279
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(LX/1hg;JJ)V
    .locals 3

    .prologue
    .line 296280
    invoke-static {p0, p1, p2, p3, p4}, LX/1hg;->b(LX/1hg;JJ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 296281
    :cond_0
    :goto_0
    return-void

    .line 296282
    :cond_1
    const-wide/16 v0, 0x2710

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-lez v0, :cond_0

    .line 296283
    iget-object v0, p0, LX/1hg;->f:LX/0p7;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/0p7;->a(JJ)V

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 6

    .prologue
    .line 296284
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/1hg;->a:J

    iget-wide v2, p0, LX/1hg;->b:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, LX/1hg;->d:J

    iget-wide v4, p0, LX/1hg;->e:J

    sub-long/2addr v2, v4

    invoke-static {p0, v0, v1, v2, v3}, LX/1hg;->c(LX/1hg;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296285
    monitor-exit p0

    return-void

    .line 296286
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(J)V
    .locals 7

    .prologue
    .line 296287
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/1hg;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 296288
    iget-object v0, p0, LX/1hg;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/1hg;->c:J

    .line 296289
    iget-wide v0, p0, LX/1hg;->c:J

    iput-wide v0, p0, LX/1hg;->e:J

    .line 296290
    iget-wide v0, p0, LX/1hg;->e:J

    iput-wide v0, p0, LX/1hg;->d:J

    .line 296291
    :cond_0
    iget-wide v0, p0, LX/1hg;->a:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/1hg;->a:J

    .line 296292
    iget-wide v0, p0, LX/1hg;->a:J

    iget-wide v2, p0, LX/1hg;->b:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x2710

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 296293
    iget-object v0, p0, LX/1hg;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/1hg;->d:J

    .line 296294
    :cond_1
    iget-wide v0, p0, LX/1hg;->a:J

    iget-wide v2, p0, LX/1hg;->b:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, LX/1hg;->d:J

    iget-wide v4, p0, LX/1hg;->e:J

    sub-long/2addr v2, v4

    invoke-static {p0, v0, v1, v2, v3}, LX/1hg;->b(LX/1hg;JJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 296295
    iget-wide v0, p0, LX/1hg;->a:J

    iput-wide v0, p0, LX/1hg;->b:J

    .line 296296
    iget-wide v0, p0, LX/1hg;->d:J

    iput-wide v0, p0, LX/1hg;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296297
    :cond_2
    monitor-exit p0

    return-void

    .line 296298
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(JJ)V
    .locals 7

    .prologue
    .line 296299
    const-wide/16 v0, 0x4e20

    cmp-long v0, p1, v0

    if-ltz v0, :cond_1

    const-wide/16 v0, 0x14

    cmp-long v0, p3, v0

    if-ltz v0, :cond_1

    .line 296300
    const-wide/16 v0, 0x2710

    div-long v0, p1, v0

    const-wide/16 v2, 0xa

    div-long v2, p3, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    .line 296301
    int-to-long v2, v0

    div-long v2, p1, v2

    .line 296302
    int-to-long v4, v0

    div-long v4, p3, v4

    .line 296303
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-lez v0, :cond_0

    .line 296304
    invoke-static {p0, v2, v3, v4, v5}, LX/1hg;->b(LX/1hg;JJ)Z

    .line 296305
    add-int/lit8 v0, v0, -0x1

    sub-long/2addr p1, v2

    sub-long/2addr p3, v4

    goto :goto_0

    .line 296306
    :cond_0
    invoke-static {p0, p1, p2, p3, p4}, LX/1hg;->c(LX/1hg;JJ)V

    .line 296307
    :goto_1
    return-void

    .line 296308
    :cond_1
    invoke-static {p0, p1, p2, p3, p4}, LX/1hg;->c(LX/1hg;JJ)V

    goto :goto_1
.end method
