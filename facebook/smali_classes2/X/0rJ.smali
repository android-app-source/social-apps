.class public LX/0rJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0rH;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0rM;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0ad;

.field private final d:LX/0qe;

.field private final e:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "LX/0rK;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/clashmanagement/manager/ClashUnit;",
            "LX/0rK;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public g:LX/0qg;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public i:LX/0rL;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public j:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private k:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private l:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public m:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0rH;LX/0Or;LX/0ad;JLX/0qe;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0rH;",
            "LX/0Or",
            "<",
            "LX/0rM;",
            ">;",
            "LX/0ad;",
            "J",
            "LX/0qe;",
            ")V"
        }
    .end annotation

    .prologue
    .line 149318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149319
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, LX/0rJ;->e:Ljava/util/TreeSet;

    .line 149320
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0rJ;->f:Ljava/util/Map;

    .line 149321
    iput-object p1, p0, LX/0rJ;->a:LX/0rH;

    .line 149322
    iput-object p2, p0, LX/0rJ;->b:LX/0Or;

    .line 149323
    iput-object p3, p0, LX/0rJ;->c:LX/0ad;

    .line 149324
    iput-wide p4, p0, LX/0rJ;->l:J

    .line 149325
    iput-object p6, p0, LX/0rJ;->d:LX/0qe;

    .line 149326
    return-void
.end method

.method private static declared-synchronized a(LX/0rJ;JLX/0qg;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/facebook/clashmanagement/manager/ClashUnit;",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "LX/0qi;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 149327
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/1gw;

    invoke-direct {v0, p0, p1, p2, p3}, LX/1gw;-><init>(LX/0rJ;JLX/0qg;)V

    invoke-static {}, LX/0TA;->b()LX/0TD;

    move-result-object v1

    invoke-static {p4, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149328
    monitor-exit p0

    return-void

    .line 149329
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(LX/0rJ;JLX/0rK;)V
    .locals 3

    .prologue
    .line 149330
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0rJ;->e:Ljava/util/TreeSet;

    invoke-virtual {v0, p3}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 149331
    iget-object v0, p3, LX/0rK;->a:LX/0qg;

    iput-object v0, p0, LX/0rJ;->g:LX/0qg;

    .line 149332
    const/4 v0, 0x0

    iput-object v0, p0, LX/0rJ;->h:Ljava/lang/String;

    .line 149333
    iput-wide p1, p0, LX/0rJ;->j:J

    .line 149334
    iget v0, p3, LX/0rK;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p3, LX/0rK;->d:I

    .line 149335
    iget-object v0, p0, LX/0rJ;->d:LX/0qe;

    invoke-virtual {v0}, LX/0qe;->b()J

    move-result-wide v0

    iput-wide v0, p0, LX/0rJ;->k:J

    .line 149336
    iget-object v0, p0, LX/0rJ;->e:Ljava/util/TreeSet;

    invoke-virtual {v0, p3}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 149337
    iget-object v1, p3, LX/0rK;->a:LX/0qg;

    iget-object v0, p3, LX/0rK;->a:LX/0qg;

    check-cast v0, LX/0qf;

    .line 149338
    iget-object v2, v0, LX/0qf;->b:Lcom/google/common/util/concurrent/SettableFuture;

    move-object v0, v2

    .line 149339
    invoke-static {p0, p1, p2, v1, v0}, LX/0rJ;->a(LX/0rJ;JLX/0qg;Lcom/google/common/util/concurrent/SettableFuture;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149340
    monitor-exit p0

    return-void

    .line 149341
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(LX/0rJ;JLX/0rK;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 149342
    monitor-enter p0

    :try_start_0
    iget-object v0, p3, LX/0rK;->a:LX/0qg;

    invoke-static {p0, p1, p2, v0, p4}, LX/0rJ;->a(LX/0rJ;JLX/0qg;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 149343
    :goto_0
    monitor-exit p0

    return-void

    .line 149344
    :cond_0
    :try_start_1
    invoke-static {p0, p1, p2, p3, p4}, LX/0rJ;->b(LX/0rJ;JLX/0rK;Ljava/lang/String;)V

    .line 149345
    iget v0, p0, LX/0rJ;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0rJ;->m:I

    .line 149346
    sget-object v0, LX/0rL;->REQUIRED:LX/0rL;

    iput-object v0, p0, LX/0rJ;->i:LX/0rL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 149347
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(LX/0rJ;LX/0qg;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/clashmanagement/manager/ClashUnit;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 149348
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0rJ;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rK;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149349
    if-nez v0, :cond_0

    .line 149350
    :goto_0
    monitor-exit p0

    return-void

    .line 149351
    :cond_0
    :try_start_1
    iget-object v1, v0, LX/0rK;->f:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 149352
    iget-object v0, v0, LX/0rK;->f:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 149353
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(LX/0rJ;LX/0qg;LX/2uF;)V
    .locals 7
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "updatePrioritySubUnits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 149354
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0rJ;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rK;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149355
    if-nez v0, :cond_1

    .line 149356
    :cond_0
    monitor-exit p0

    return-void

    .line 149357
    :cond_1
    :try_start_1
    iget-object v1, v0, LX/0rK;->e:LX/01J;

    invoke-virtual {v1}, LX/01J;->clear()V

    .line 149358
    invoke-virtual {p2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 149359
    iget-object v4, v0, LX/0rK;->e:LX/01J;

    const/4 v5, 0x1

    invoke-virtual {v3, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v2, v6}, LX/15i;->j(II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 149360
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(LX/0rJ;JLX/0qg;Ljava/lang/String;)Z
    .locals 7
    .param p3    # LX/0qg;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 149361
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, LX/0rJ;->h:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/0rJ;->h:Ljava/lang/String;

    invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 149362
    :goto_0
    iget-object v3, p0, LX/0rJ;->g:LX/0qg;

    if-ne v3, p3, :cond_2

    if-eqz v2, :cond_2

    iget-wide v2, p0, LX/0rJ;->j:J

    sub-long v2, p1, v2

    iget-wide v4, p0, LX/0rJ;->k:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    :goto_1
    monitor-exit p0

    return v0

    .line 149363
    :cond_0
    if-nez p4, :cond_1

    move v2, v0

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 149364
    goto :goto_1

    .line 149365
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/0rJ;LX/0rK;Ljava/lang/String;)LX/0rL;
    .locals 1

    .prologue
    .line 149366
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, LX/0rK;->f:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149367
    sget-object v0, LX/0rL;->REQUIRED:LX/0rL;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149368
    :goto_0
    monitor-exit p0

    return-object v0

    .line 149369
    :cond_0
    :try_start_1
    iget-object v0, p1, LX/0rK;->e:LX/01J;

    invoke-virtual {v0, p2}, LX/01J;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149370
    sget-object v0, LX/0rL;->PRIORITY:LX/0rL;

    goto :goto_0

    .line 149371
    :cond_1
    sget-object v0, LX/0rL;->NORMAL:LX/0rL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 149372
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(LX/0rJ;JLX/0rK;Ljava/lang/String;)V
    .locals 3
    .param p3    # LX/0rK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 149373
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0rJ;->e:Ljava/util/TreeSet;

    invoke-virtual {v0, p3}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 149374
    iget-object v0, p3, LX/0rK;->a:LX/0qg;

    iput-object v0, p0, LX/0rJ;->g:LX/0qg;

    .line 149375
    iput-object p4, p0, LX/0rJ;->h:Ljava/lang/String;

    .line 149376
    iput-wide p1, p0, LX/0rJ;->j:J

    .line 149377
    iget v0, p3, LX/0rK;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p3, LX/0rK;->b:I

    .line 149378
    iget-object v0, p0, LX/0rJ;->d:LX/0qe;

    invoke-virtual {v0}, LX/0qe;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/0rJ;->k:J

    .line 149379
    iget-object v0, p0, LX/0rJ;->e:Ljava/util/TreeSet;

    invoke-virtual {v0, p3}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149380
    monitor-exit p0

    return-void

    .line 149381
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(LX/0rJ;J)Z
    .locals 5

    .prologue
    .line 149382
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0rJ;->g:LX/0qg;

    if-eqz v0, :cond_0

    iget-wide v0, p0, LX/0rJ;->j:J

    sub-long v0, p1, v0

    iget-wide v2, p0, LX/0rJ;->k:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b(LX/0rJ;JLX/0qg;)Z
    .locals 5

    .prologue
    .line 149383
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/0rJ;->b(LX/0rJ;J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, LX/0rJ;->a(LX/0rJ;JLX/0qg;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 149384
    const/4 v0, 0x0

    .line 149385
    :goto_0
    monitor-exit p0

    return v0

    .line 149386
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0rJ;->f:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rK;

    .line 149387
    iget-object v1, p0, LX/0rJ;->e:Ljava/util/TreeSet;

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 149388
    iget v1, v0, LX/0rK;->d:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, LX/0rK;->d:I

    .line 149389
    const/4 v1, 0x0

    iput-object v1, p0, LX/0rJ;->g:LX/0qg;

    .line 149390
    iget-object v1, p0, LX/0rJ;->d:LX/0qe;

    invoke-virtual {v1}, LX/0qe;->a()J

    move-result-wide v2

    iput-wide v2, p0, LX/0rJ;->k:J

    .line 149391
    iget-object v1, p0, LX/0rJ;->e:Ljava/util/TreeSet;

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 149392
    const/4 v0, 0x1

    goto :goto_0

    .line 149393
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b(LX/0rJ;JLX/0qg;Ljava/lang/String;)Z
    .locals 5
    .param p3    # LX/0qg;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 149394
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/0rJ;->b(LX/0rJ;J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, LX/0rJ;->a(LX/0rJ;JLX/0qg;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 149395
    const/4 v0, 0x0

    .line 149396
    :goto_0
    monitor-exit p0

    return v0

    .line 149397
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0rJ;->f:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rK;

    .line 149398
    iget-object v1, p0, LX/0rJ;->e:Ljava/util/TreeSet;

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 149399
    iget-object v1, v0, LX/0rK;->a:LX/0qg;

    iput-object v1, p0, LX/0rJ;->g:LX/0qg;

    .line 149400
    iput-object p4, p0, LX/0rJ;->h:Ljava/lang/String;

    .line 149401
    iput-wide p1, p0, LX/0rJ;->j:J

    .line 149402
    iget v1, v0, LX/0rK;->d:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, LX/0rK;->d:I

    .line 149403
    iget v1, v0, LX/0rK;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0rK;->b:I

    .line 149404
    iget-object v1, p0, LX/0rJ;->d:LX/0qe;

    invoke-virtual {v1}, LX/0qe;->a()J

    move-result-wide v2

    iput-wide v2, p0, LX/0rJ;->k:J

    .line 149405
    iget-object v1, p0, LX/0rJ;->e:Ljava/util/TreeSet;

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 149406
    const/4 v0, 0x1

    goto :goto_0

    .line 149407
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(LX/0rJ;JLX/0rK;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 149197
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0rJ;->c:LX/0ad;

    sget-short v2, LX/0rI;->b:S

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 149198
    if-eqz v0, :cond_0

    iget-object v0, p3, LX/0rK;->a:LX/0qg;

    instance-of v0, v0, LX/0qf;

    if-eqz v0, :cond_0

    iget-object v0, p3, LX/0rK;->a:LX/0qg;

    check-cast v0, LX/0qf;

    invoke-virtual {v0}, LX/0qf;->c()LX/0qi;

    move-result-object v0

    .line 149199
    iget-boolean v2, v0, LX/0qi;->a:Z

    move v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149200
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 149201
    :goto_0
    monitor-exit p0

    return v0

    .line 149202
    :cond_1
    :try_start_1
    iget-object v0, p3, LX/0rK;->a:LX/0qg;

    check-cast v0, LX/0qf;

    invoke-virtual {v0}, LX/0qf;->c()LX/0qi;

    move-result-object v0

    .line 149203
    iget-object v1, v0, LX/0qi;->b:Ljava/lang/String;

    move-object v0, v1

    .line 149204
    invoke-static {p0, p1, p2, p3, v0}, LX/0rJ;->b(LX/0rJ;JLX/0rK;Ljava/lang/String;)V

    .line 149205
    iget v0, p0, LX/0rJ;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0rJ;->m:I

    .line 149206
    sget-object v0, LX/0rL;->CACHED:LX/0rL;

    iput-object v0, p0, LX/0rJ;->i:LX/0rL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 149207
    const/4 v0, 0x1

    goto :goto_0

    .line 149208
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized c(LX/0rJ;)V
    .locals 3

    .prologue
    .line 149308
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0rJ;->c:LX/0ad;

    sget-short v1, LX/0rI;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 149309
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 149310
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/0rJ;->g:LX/0qg;

    instance-of v0, v0, LX/0qf;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0rJ;->i:LX/0rL;

    sget-object v1, LX/0rL;->CACHED:LX/0rL;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/0rJ;->g:LX/0qg;

    check-cast v0, LX/0qf;

    invoke-virtual {v0}, LX/0qf;->c()LX/0qi;

    move-result-object v0

    .line 149311
    iget-boolean v1, v0, LX/0qi;->a:Z

    move v0, v1

    .line 149312
    if-nez v0, :cond_0

    .line 149313
    const/4 v0, 0x0

    iput-object v0, p0, LX/0rJ;->g:LX/0qg;

    .line 149314
    iget-object v0, p0, LX/0rJ;->d:LX/0qe;

    invoke-virtual {v0}, LX/0qe;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/0rJ;->k:J

    .line 149315
    sget-object v0, LX/0rL;->NORMAL:LX/0rL;

    iput-object v0, p0, LX/0rJ;->i:LX/0rL;

    .line 149316
    const/4 v0, 0x0

    iput-object v0, p0, LX/0rJ;->h:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 149317
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized c(LX/0rJ;J)V
    .locals 13

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 149408
    monitor-enter p0

    const v0, 0x7fffffff

    .line 149409
    :try_start_0
    iget-object v1, p0, LX/0rJ;->e:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v3, v2

    move-object v6, v5

    move-object v7, v5

    move v8, v0

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rK;

    .line 149410
    invoke-static {p0, p1, p2, v0}, LX/0rJ;->b(LX/0rJ;JLX/0rK;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    .line 149411
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 149412
    :cond_2
    :try_start_1
    iget-object v1, v0, LX/0rK;->a:LX/0qg;

    invoke-static {v1}, LX/0qb;->a(LX/0qg;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 149413
    if-nez v7, :cond_0

    .line 149414
    const/4 v3, 0x1

    .line 149415
    sget-object v5, LX/0rL;->NORMAL:LX/0rL;

    move-object v7, v0

    goto :goto_0

    .line 149416
    :cond_3
    iget-object v1, v0, LX/0rK;->a:LX/0qg;

    iget-object v4, p0, LX/0rJ;->a:LX/0rH;

    invoke-virtual {v1, v4}, LX/0qg;->a(LX/0rH;)LX/0qi;

    move-result-object v1

    .line 149417
    iget-boolean v4, v1, LX/0qi;->a:Z

    move v4, v4

    .line 149418
    if-eqz v4, :cond_0

    .line 149419
    iget-object v4, v1, LX/0qi;->b:Ljava/lang/String;

    move-object v4, v4

    .line 149420
    iget-object v1, v0, LX/0rK;->f:Ljava/util/Set;

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 149421
    sget-object v1, LX/0rL;->REQUIRED:LX/0rL;

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    .line 149422
    :goto_2
    if-eqz v1, :cond_1

    .line 149423
    if-eqz v2, :cond_5

    .line 149424
    invoke-static {p0, p1, p2, v1}, LX/0rJ;->a(LX/0rJ;JLX/0rK;)V

    .line 149425
    :goto_3
    iget v1, p0, LX/0rJ;->m:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0rJ;->m:I

    .line 149426
    iput-object v0, p0, LX/0rJ;->i:LX/0rL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 149427
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 149428
    :cond_4
    :try_start_2
    iget-object v1, v0, LX/0rK;->e:LX/01J;

    invoke-virtual {v1, v4}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 149429
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v10

    if-ge v10, v8, :cond_6

    .line 149430
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 149431
    sget-object v5, LX/0rL;->PRIORITY:LX/0rL;

    move v1, v2

    move-object v3, v5

    move-object v6, v0

    move v7, v8

    move-object v5, v4

    .line 149432
    :goto_4
    if-nez v6, :cond_8

    .line 149433
    sget-object v1, LX/0rL;->NORMAL:LX/0rL;

    move-object v3, v4

    move-object v4, v0

    move v0, v2

    :goto_5
    move-object v5, v1

    move-object v6, v3

    move v8, v7

    move-object v7, v4

    move v3, v0

    .line 149434
    goto :goto_0

    .line 149435
    :cond_5
    invoke-static {p0, p1, p2, v1, v4}, LX/0rJ;->b(LX/0rJ;JLX/0rK;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    :cond_6
    move v1, v3

    move-object v3, v5

    move-object v5, v6

    move-object v6, v7

    move v7, v8

    goto :goto_4

    :cond_7
    move v2, v3

    move-object v0, v5

    move-object v4, v6

    move-object v1, v7

    goto :goto_2

    :cond_8
    move v0, v1

    move-object v4, v6

    move-object v1, v3

    move-object v3, v5

    goto :goto_5
.end method

.method private static declared-synchronized d(LX/0rJ;J)V
    .locals 11

    .prologue
    .line 149187
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/0rJ;->l:J

    sub-long v0, p1, v0

    iget-object v2, p0, LX/0rJ;->d:LX/0qe;

    .line 149188
    iget-object v5, v2, LX/0qe;->a:LX/0W3;

    sget-wide v7, LX/0X5;->iD:J

    const-wide/32 v9, 0x5265c00

    invoke-interface {v5, v7, v8, v9, v10}, LX/0W4;->a(JJ)J

    move-result-wide v5

    move-wide v2, v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149189
    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 149190
    :goto_0
    monitor-exit p0

    return-void

    .line 149191
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0rJ;->e:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->clear()V

    .line 149192
    iget-object v0, p0, LX/0rJ;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 149193
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0rK;

    const/4 v3, 0x0

    iput v3, v1, LX/0rK;->b:I

    .line 149194
    iget-object v1, p0, LX/0rJ;->e:Ljava/util/TreeSet;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 149195
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 149196
    :cond_1
    :try_start_2
    iput-wide p1, p0, LX/0rJ;->l:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized e(LX/0rJ;J)LX/0rN;
    .locals 5

    .prologue
    .line 149209
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/0rJ;->b(LX/0rJ;J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 149210
    sget-object v0, LX/0rN;->AVAILABLE:LX/0rN;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149211
    :goto_0
    monitor-exit p0

    return-object v0

    .line 149212
    :cond_0
    :try_start_1
    iget-wide v0, p0, LX/0rJ;->k:J

    iget-object v2, p0, LX/0rJ;->d:LX/0qe;

    invoke-virtual {v2}, LX/0qe;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 149213
    sget-object v0, LX/0rN;->PROVISIONAL:LX/0rN;

    goto :goto_0

    .line 149214
    :cond_1
    sget-object v0, LX/0rN;->TAKEN:LX/0rN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 149215
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()I
    .locals 1

    .prologue
    .line 149216
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0rJ;->m:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(J)Ljava/lang/String;
    .locals 7

    .prologue
    .line 149217
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/0rJ;->b(LX/0rJ;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149218
    iget-object v0, p0, LX/0rJ;->d:LX/0qe;

    invoke-virtual {v0}, LX/0qe;->a()J

    move-result-wide v0

    sub-long/2addr v0, p1

    iget-wide v2, p0, LX/0rJ;->j:J

    add-long/2addr v0, v2

    long-to-float v0, v0

    const v1, 0x476a6000    # 60000.0f

    div-float/2addr v0, v1

    .line 149219
    const-string v1, "%.2fm"

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 149220
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Slot taken by: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/0rJ;->g:LX/0qg;

    invoke-virtual {v2}, LX/0qg;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nTaken at: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v2

    iget-wide v4, p0, LX/0rJ;->j:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nSession time left: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " min"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 149221
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    const-string v0, "Slot not taken!"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 149222
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0qg;)V
    .locals 3

    .prologue
    .line 149223
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0rJ;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rK;

    .line 149224
    if-nez v0, :cond_0

    .line 149225
    new-instance v0, LX/0rK;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, LX/0rK;-><init>(LX/0qg;II)V

    .line 149226
    iget-object v1, p0, LX/0rJ;->e:Ljava/util/TreeSet;

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 149227
    iget-object v1, p0, LX/0rJ;->f:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149228
    :cond_0
    monitor-exit p0

    return-void

    .line 149229
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0qg;Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel$EdgesModel$NodeModel;)V
    .locals 3

    .prologue
    .line 149230
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0rJ;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rK;

    .line 149231
    if-nez v0, :cond_1

    .line 149232
    new-instance v0, LX/0rK;

    const/4 v1, 0x0

    invoke-virtual {p2}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel$EdgesModel$NodeModel;->k()I

    move-result v2

    invoke-direct {v0, p1, v1, v2}, LX/0rK;-><init>(LX/0qg;II)V

    .line 149233
    iget-object v1, p0, LX/0rJ;->e:Ljava/util/TreeSet;

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 149234
    iget-object v1, p0, LX/0rJ;->f:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149235
    :cond_0
    :goto_0
    invoke-virtual {p2}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel$EdgesModel$NodeModel;->m()LX/0Px;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/0rJ;->a(LX/0rJ;LX/0qg;LX/0Px;)V

    .line 149236
    invoke-virtual {p2}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel$EdgesModel$NodeModel;->l()LX/2uF;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/0rJ;->a(LX/0rJ;LX/0qg;LX/2uF;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149237
    monitor-exit p0

    return-void

    .line 149238
    :cond_1
    :try_start_1
    iget v1, v0, LX/0rK;->c:I

    invoke-virtual {p2}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel$EdgesModel$NodeModel;->k()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 149239
    iget-object v1, p0, LX/0rJ;->e:Ljava/util/TreeSet;

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 149240
    invoke-virtual {p2}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel$EdgesModel$NodeModel;->k()I

    move-result v1

    iput v1, v0, LX/0rK;->c:I

    .line 149241
    iget-object v1, p0, LX/0rJ;->e:Ljava/util/TreeSet;

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 149242
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 4

    .prologue
    .line 149243
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/0qh;->a:LX/0Tn;

    const-string v1, "stored_clash_session"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v1, p0, LX/0rJ;->a:LX/0rH;

    invoke-virtual {v1}, LX/0rH;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 149244
    invoke-interface {p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    const-string v1, "stored_clash_unit_name"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    iget-object v2, p0, LX/0rJ;->g:LX/0qg;

    if-nez v2, :cond_0

    const-string v2, ""

    :goto_0
    invoke-interface {v3, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v3

    const-string v1, "stored_sub_unit_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    iget-object v2, p0, LX/0rJ;->h:Ljava/lang/String;

    if-nez v2, :cond_1

    const-string v2, ""

    :goto_1
    invoke-interface {v3, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    const-string v2, "stored_slot_taken_time_stamp"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-wide v2, p0, LX/0rJ;->j:J

    invoke-interface {v1, v0, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149245
    monitor-exit p0

    return-void

    .line 149246
    :cond_0
    :try_start_1
    iget-object v2, p0, LX/0rJ;->g:LX/0qg;

    invoke-virtual {v2}, LX/0qg;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    iget-object v2, p0, LX/0rJ;->h:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 149247
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/Set;J)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/clashmanagement/manager/ClashUnit;",
            ">;J)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 149248
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0rJ;->g:LX/0qg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 149249
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 149250
    :cond_1
    :try_start_1
    sget-object v0, LX/0qh;->a:LX/0Tn;

    const-string v1, "stored_clash_session"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v1, p0, LX/0rJ;->a:LX/0rH;

    invoke-virtual {v1}, LX/0rH;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 149251
    const-string v1, "stored_slot_taken_time_stamp"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {p1, v1, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 149252
    cmp-long v1, v4, p3

    if-gtz v1, :cond_0

    sub-long v6, p3, v4

    iget-object v1, p0, LX/0rJ;->d:LX/0qe;

    invoke-virtual {v1}, LX/0qe;->a()J

    move-result-wide v8

    cmp-long v1, v6, v8

    if-gtz v1, :cond_0

    .line 149253
    const-string v1, "stored_clash_unit_name"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    const-string v2, ""

    invoke-interface {p1, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 149254
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v2, v3

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0qg;

    .line 149255
    invoke-virtual {v1}, LX/0qg;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    :goto_2
    move-object v2, v1

    .line 149256
    goto :goto_1

    .line 149257
    :cond_2
    if-eqz v2, :cond_0

    .line 149258
    const-string v1, "stored_sub_unit_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 149259
    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v0, v3

    .line 149260
    :cond_3
    iput-object v2, p0, LX/0rJ;->g:LX/0qg;

    .line 149261
    iput-object v0, p0, LX/0rJ;->h:Ljava/lang/String;

    .line 149262
    iput-wide v4, p0, LX/0rJ;->j:J

    .line 149263
    iget-object v0, p0, LX/0rJ;->d:LX/0qe;

    invoke-virtual {v0}, LX/0qe;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/0rJ;->k:J

    .line 149264
    sget-object v0, LX/0rL;->RESTORED:LX/0rL;

    iput-object v0, p0, LX/0rJ;->i:LX/0rL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 149265
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move-object v1, v2

    goto :goto_2
.end method

.method public final declared-synchronized a(JLX/0qg;)Z
    .locals 5

    .prologue
    .line 149266
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0rJ;->g:LX/0qg;

    if-ne v0, p3, :cond_0

    iget-wide v0, p0, LX/0rJ;->j:J

    sub-long v0, p1, v0

    iget-wide v2, p0, LX/0rJ;->k:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(JLX/0qg;LX/0qi;)Z
    .locals 11

    .prologue
    const/4 v9, 0x1

    .line 149267
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0rJ;->f:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rK;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149268
    if-nez v0, :cond_0

    .line 149269
    const/4 v0, 0x0

    .line 149270
    :goto_0
    monitor-exit p0

    return v0

    .line 149271
    :cond_0
    :try_start_1
    invoke-static {p0}, LX/0rJ;->c(LX/0rJ;)V

    .line 149272
    invoke-static {p0, p1, p2}, LX/0rJ;->b(LX/0rJ;J)Z

    move-result v2

    .line 149273
    if-nez v2, :cond_1

    .line 149274
    const/4 v1, 0x0

    iput-object v1, p0, LX/0rJ;->g:LX/0qg;

    .line 149275
    invoke-static {p0, p1, p2}, LX/0rJ;->d(LX/0rJ;J)V

    .line 149276
    :cond_1
    iget-object v1, p4, LX/0qi;->b:Ljava/lang/String;

    move-object v3, v1

    .line 149277
    invoke-static {p0, v0, v3}, LX/0rJ;->a$redex0(LX/0rJ;LX/0rK;Ljava/lang/String;)LX/0rL;

    move-result-object v4

    .line 149278
    iget-object v1, p0, LX/0rJ;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0rM;

    .line 149279
    iget-object v5, p0, LX/0rJ;->a:LX/0rH;

    invoke-virtual {v1, v5}, LX/0rM;->a(LX/0rH;)LX/0rM;

    move-result-object v5

    invoke-virtual {v5, p3, v3, v4}, LX/0rM;->a(LX/0qg;Ljava/lang/String;LX/0rL;)LX/0rM;

    move-result-object v5

    iget-object v6, p0, LX/0rJ;->g:LX/0qg;

    iget-object v7, p0, LX/0rJ;->h:Ljava/lang/String;

    iget-object v8, p0, LX/0rJ;->i:LX/0rL;

    invoke-static {p0, p1, p2}, LX/0rJ;->e(LX/0rJ;J)LX/0rN;

    move-result-object v10

    invoke-virtual {v5, v6, v7, v8, v10}, LX/0rM;->a(LX/0qg;Ljava/lang/String;LX/0rL;LX/0rN;)LX/0rM;

    .line 149280
    sget-object v5, LX/0rL;->REQUIRED:LX/0rL;

    if-ne v4, v5, :cond_2

    .line 149281
    invoke-static {p0, p1, p2, v0, v3}, LX/0rJ;->a(LX/0rJ;JLX/0rK;Ljava/lang/String;)V

    .line 149282
    iget-object v2, p0, LX/0rJ;->g:LX/0qg;

    iget-object v3, p0, LX/0rJ;->h:Ljava/lang/String;

    iget-object v4, p0, LX/0rJ;->i:LX/0rL;

    sget-object v5, LX/0rN;->TAKEN:LX/0rN;

    iget-wide v6, p0, LX/0rJ;->j:J

    iget v8, p0, LX/0rJ;->m:I

    invoke-virtual/range {v1 .. v8}, LX/0rM;->a(LX/0qg;Ljava/lang/String;LX/0rL;LX/0rN;JI)LX/0rM;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0rM;->a(Z)LX/0rM;

    move-result-object v0

    invoke-virtual {v0}, LX/0rM;->a()V

    move v0, v9

    .line 149283
    goto :goto_0

    .line 149284
    :cond_2
    if-nez v2, :cond_3

    .line 149285
    invoke-static {p0, p1, p2}, LX/0rJ;->c(LX/0rJ;J)V

    .line 149286
    :cond_3
    invoke-static {p0, p1, p2, p3, v3}, LX/0rJ;->a(LX/0rJ;JLX/0qg;Ljava/lang/String;)Z

    move-result v0

    .line 149287
    iget-object v2, p0, LX/0rJ;->g:LX/0qg;

    iget-object v3, p0, LX/0rJ;->h:Ljava/lang/String;

    iget-object v4, p0, LX/0rJ;->i:LX/0rL;

    invoke-static {p0, p1, p2}, LX/0rJ;->e(LX/0rJ;J)LX/0rN;

    move-result-object v5

    iget-wide v6, p0, LX/0rJ;->j:J

    iget v8, p0, LX/0rJ;->m:I

    invoke-virtual/range {v1 .. v8}, LX/0rM;->a(LX/0qg;Ljava/lang/String;LX/0rL;LX/0rN;JI)LX/0rM;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0rM;->a(Z)LX/0rM;

    move-result-object v1

    invoke-virtual {v1}, LX/0rM;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 149288
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/clashmanagement/manager/ClashUnit;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 149289
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/TreeMap;

    new-instance v0, LX/AP1;

    invoke-direct {v0, p0}, LX/AP1;-><init>(LX/0rJ;)V

    invoke-direct {v1, v0}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    .line 149290
    iget-object v0, p0, LX/0rJ;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rK;

    .line 149291
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Priority: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v0, LX/0rK;->c:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\nSlot taken count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, LX/0rK;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 149292
    iget-object v0, v0, LX/0rK;->a:LX/0qg;

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 149293
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 149294
    :cond_0
    monitor-exit p0

    return-object v1
.end method

.method public final declared-synchronized b(LX/0qg;)V
    .locals 2

    .prologue
    .line 149295
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0rJ;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rK;

    .line 149296
    if-eqz v0, :cond_0

    .line 149297
    iget-object v1, p0, LX/0rJ;->e:Ljava/util/TreeSet;

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 149298
    iget-object v0, p0, LX/0rJ;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149299
    :cond_0
    monitor-exit p0

    return-void

    .line 149300
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 2

    .prologue
    .line 149301
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/0rJ;->g:LX/0qg;

    .line 149302
    iget-object v0, p0, LX/0rJ;->d:LX/0qe;

    invoke-virtual {v0}, LX/0qe;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/0rJ;->k:J

    .line 149303
    sget-object v0, LX/0rL;->NORMAL:LX/0rL;

    iput-object v0, p0, LX/0rJ;->i:LX/0rL;

    .line 149304
    const/4 v0, 0x0

    iput-object v0, p0, LX/0rJ;->h:Ljava/lang/String;

    .line 149305
    invoke-virtual {p0, p1}, LX/0rJ;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149306
    monitor-exit p0

    return-void

    .line 149307
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
