.class public LX/1e5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 287304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 287305
    iput-object p1, p0, LX/1e5;->a:LX/0tX;

    .line 287306
    return-void
.end method

.method public static a(LX/0QB;)LX/1e5;
    .locals 2

    .prologue
    .line 287307
    new-instance v1, LX/1e5;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-direct {v1, v0}, LX/1e5;-><init>(LX/0tX;)V

    .line 287308
    move-object v0, v1

    .line 287309
    return-object v0
.end method

.method public static a(LX/1e5;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 7

    .prologue
    .line 287310
    new-instance v0, LX/4Cy;

    invoke-direct {v0}, LX/4Cy;-><init>()V

    .line 287311
    const-string v1, "article_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 287312
    move-object v0, v0

    .line 287313
    const-string v1, "reason"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 287314
    move-object v0, v0

    .line 287315
    new-instance v1, LX/BxR;

    invoke-direct {v1}, LX/BxR;-><init>()V

    move-object v1, v1

    .line 287316
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/BxR;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 287317
    iget-object v1, p0, LX/1e5;->a:LX/0tX;

    new-instance v2, LX/3G1;

    invoke-direct {v2}, LX/3G1;-><init>()V

    invoke-virtual {v2, v0}, LX/3G1;->a(LX/399;)LX/3G1;

    move-result-object v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 287318
    iput-wide v2, v0, LX/3G2;->d:J

    .line 287319
    move-object v0, v0

    .line 287320
    const/16 v2, 0x64

    .line 287321
    iput v2, v0, LX/3G2;->f:I

    .line 287322
    move-object v0, v0

    .line 287323
    invoke-virtual {v0}, LX/3G2;->a()LX/3G3;

    move-result-object v0

    check-cast v0, LX/3G4;

    sget-object v2, LX/3Fz;->b:LX/3Fz;

    invoke-virtual {v1, v0, v2}, LX/0tX;->a(LX/3G4;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 287324
    return-void
.end method
