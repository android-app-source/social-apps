.class public LX/1eg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TResult:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/concurrent/ExecutorService;

.field public static final b:Ljava/util/concurrent/Executor;

.field private static final c:Ljava/util/concurrent/Executor;

.field public static volatile d:LX/3xz;

.field private static m:LX/1eg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1eg",
            "<*>;"
        }
    .end annotation
.end field

.field private static n:LX/1eg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1eg",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static o:LX/1eg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1eg",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static p:LX/1eg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1eg",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final e:Ljava/lang/Object;

.field private f:Z

.field private g:Z

.field private h:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTResult;"
        }
    .end annotation
.end field

.field private i:Ljava/lang/Exception;

.field private j:Z

.field private k:LX/3y0;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1ex",
            "<TTResult;",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 288837
    invoke-static {}, LX/1eh;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, LX/1eg;->a:Ljava/util/concurrent/ExecutorService;

    .line 288838
    sget-object v0, LX/1eh;->a:LX/1eh;

    iget-object v0, v0, LX/1eh;->d:Ljava/util/concurrent/Executor;

    move-object v0, v0

    .line 288839
    sput-object v0, LX/1eg;->c:Ljava/util/concurrent/Executor;

    .line 288840
    sget-object v0, LX/1ei;->c:LX/1ei;

    iget-object v0, v0, LX/1ei;->d:Ljava/util/concurrent/Executor;

    move-object v0, v0

    .line 288841
    sput-object v0, LX/1eg;->b:Ljava/util/concurrent/Executor;

    .line 288842
    new-instance v0, LX/1eg;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/1eg;-><init>(Ljava/lang/Object;)V

    sput-object v0, LX/1eg;->m:LX/1eg;

    .line 288843
    new-instance v0, LX/1eg;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1eg;-><init>(Ljava/lang/Object;)V

    sput-object v0, LX/1eg;->n:LX/1eg;

    .line 288844
    new-instance v0, LX/1eg;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1eg;-><init>(Ljava/lang/Object;)V

    sput-object v0, LX/1eg;->o:LX/1eg;

    .line 288845
    new-instance v0, LX/1eg;

    invoke-direct {v0, v2}, LX/1eg;-><init>(Z)V

    sput-object v0, LX/1eg;->p:LX/1eg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 288873
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 288874
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/1eg;->e:Ljava/lang/Object;

    .line 288875
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1eg;->l:Ljava/util/List;

    .line 288876
    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTResult;)V"
        }
    .end annotation

    .prologue
    .line 288868
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 288869
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/1eg;->e:Ljava/lang/Object;

    .line 288870
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1eg;->l:Ljava/util/List;

    .line 288871
    invoke-virtual {p0, p1}, LX/1eg;->b(Ljava/lang/Object;)Z

    .line 288872
    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    .line 288861
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 288862
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/1eg;->e:Ljava/lang/Object;

    .line 288863
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1eg;->l:Ljava/util/List;

    .line 288864
    if-eqz p1, :cond_0

    .line 288865
    invoke-virtual {p0}, LX/1eg;->f()Z

    .line 288866
    :goto_0
    return-void

    .line 288867
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/1eg;->b(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(LX/1ex;Ljava/util/concurrent/Executor;LX/3xt;)LX/1eg;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TContinuationResult:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1ex",
            "<TTResult;TTContinuationResult;>;",
            "Ljava/util/concurrent/Executor;",
            "LX/3xt;",
            ")",
            "LX/1eg",
            "<TTContinuationResult;>;"
        }
    .end annotation

    .prologue
    .line 288850
    new-instance v2, LX/1el;

    invoke-direct {v2}, LX/1el;-><init>()V

    .line 288851
    iget-object v6, p0, LX/1eg;->e:Ljava/lang/Object;

    monitor-enter v6

    .line 288852
    :try_start_0
    invoke-static {p0}, LX/1eg;->g(LX/1eg;)Z

    move-result v7

    .line 288853
    if-nez v7, :cond_0

    .line 288854
    iget-object v8, p0, LX/1eg;->l:Ljava/util/List;

    new-instance v0, LX/1ey;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/1ey;-><init>(LX/1eg;LX/1el;LX/1ex;Ljava/util/concurrent/Executor;LX/3xt;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 288855
    :cond_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288856
    if-eqz v7, :cond_1

    .line 288857
    invoke-static {v2, p1, p0, p2, p3}, LX/1eg;->c(LX/1el;LX/1ex;LX/1eg;Ljava/util/concurrent/Executor;LX/3xt;)V

    .line 288858
    :cond_1
    iget-object v0, v2, LX/1el;->a:LX/1eg;

    move-object v0, v0

    .line 288859
    return-object v0

    .line 288860
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(Ljava/lang/Exception;)LX/1eg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TResult:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Exception;",
            ")",
            "LX/1eg",
            "<TTResult;>;"
        }
    .end annotation

    .prologue
    .line 288846
    new-instance v0, LX/1el;

    invoke-direct {v0}, LX/1el;-><init>()V

    .line 288847
    invoke-virtual {v0, p0}, LX/1el;->a(Ljava/lang/Exception;)V

    .line 288848
    iget-object p0, v0, LX/1el;->a:LX/1eg;

    move-object v0, p0

    .line 288849
    return-object v0
.end method

.method public static a(Ljava/lang/Object;)LX/1eg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TResult:",
            "Ljava/lang/Object;",
            ">(TTResult;)",
            "LX/1eg",
            "<TTResult;>;"
        }
    .end annotation

    .prologue
    .line 288828
    if-nez p0, :cond_0

    .line 288829
    sget-object v0, LX/1eg;->m:LX/1eg;

    .line 288830
    :goto_0
    return-object v0

    .line 288831
    :cond_0
    instance-of v0, p0, Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 288832
    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/1eg;->n:LX/1eg;

    goto :goto_0

    :cond_1
    sget-object v0, LX/1eg;->o:LX/1eg;

    goto :goto_0

    .line 288833
    :cond_2
    new-instance v0, LX/1el;

    invoke-direct {v0}, LX/1el;-><init>()V

    .line 288834
    invoke-virtual {v0, p0}, LX/1el;->a(Ljava/lang/Object;)V

    .line 288835
    iget-object p0, v0, LX/1el;->a:LX/1eg;

    move-object v0, p0

    .line 288836
    goto :goto_0
.end method

.method public static a(Ljava/util/concurrent/Callable;Ljava/util/concurrent/Executor;)LX/1eg;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TResult:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TTResult;>;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "LX/1eg",
            "<TTResult;>;"
        }
    .end annotation

    .prologue
    .line 288820
    const/4 v0, 0x0

    .line 288821
    new-instance v2, LX/1el;

    invoke-direct {v2}, LX/1el;-><init>()V

    .line 288822
    :try_start_0
    new-instance v1, Lbolts/Task$4;

    invoke-direct {v1, v0, v2, p0}, Lbolts/Task$4;-><init>(LX/3xt;LX/1el;Ljava/util/concurrent/Callable;)V

    const v3, -0x72c3cd07

    invoke-static {p1, v1, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 288823
    :goto_0
    iget-object v1, v2, LX/1el;->a:LX/1eg;

    move-object v1, v1

    .line 288824
    move-object v0, v1

    .line 288825
    return-object v0

    .line 288826
    :catch_0
    move-exception v1

    .line 288827
    new-instance v3, LX/3xw;

    invoke-direct {v3, v1}, LX/3xw;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v2, v3}, LX/1el;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private b(LX/1ex;Ljava/util/concurrent/Executor;LX/3xt;)LX/1eg;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TContinuationResult:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1ex",
            "<TTResult;",
            "LX/1eg",
            "<TTContinuationResult;>;>;",
            "Ljava/util/concurrent/Executor;",
            "LX/3xt;",
            ")",
            "LX/1eg",
            "<TTContinuationResult;>;"
        }
    .end annotation

    .prologue
    .line 288809
    new-instance v2, LX/1el;

    invoke-direct {v2}, LX/1el;-><init>()V

    .line 288810
    iget-object v6, p0, LX/1eg;->e:Ljava/lang/Object;

    monitor-enter v6

    .line 288811
    :try_start_0
    invoke-static {p0}, LX/1eg;->g(LX/1eg;)Z

    move-result v7

    .line 288812
    if-nez v7, :cond_0

    .line 288813
    iget-object v8, p0, LX/1eg;->l:Ljava/util/List;

    new-instance v0, LX/3xx;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/3xx;-><init>(LX/1eg;LX/1el;LX/1ex;Ljava/util/concurrent/Executor;LX/3xt;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 288814
    :cond_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288815
    if-eqz v7, :cond_1

    .line 288816
    invoke-static {v2, p1, p0, p2, p3}, LX/1eg;->d(LX/1el;LX/1ex;LX/1eg;Ljava/util/concurrent/Executor;LX/3xt;)V

    .line 288817
    :cond_1
    iget-object v0, v2, LX/1el;->a:LX/1eg;

    move-object v0, v0

    .line 288818
    return-object v0

    .line 288819
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static c(LX/1el;LX/1ex;LX/1eg;Ljava/util/concurrent/Executor;LX/3xt;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TContinuationResult:",
            "Ljava/lang/Object;",
            "TResult:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1el",
            "<TTContinuationResult;>;",
            "LX/1ex",
            "<TTResult;TTContinuationResult;>;",
            "LX/1eg",
            "<TTResult;>;",
            "Ljava/util/concurrent/Executor;",
            "LX/3xt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 288805
    :try_start_0
    new-instance v0, Lbolts/Task$14;

    invoke-direct {v0, p4, p0, p1, p2}, Lbolts/Task$14;-><init>(LX/3xt;LX/1el;LX/1ex;LX/1eg;)V

    const v1, 0x694dfe4b

    invoke-static {p3, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 288806
    :goto_0
    return-void

    .line 288807
    :catch_0
    move-exception v0

    .line 288808
    new-instance v1, LX/3xw;

    invoke-direct {v1, v0}, LX/3xw;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {p0, v1}, LX/1el;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static d(LX/1el;LX/1ex;LX/1eg;Ljava/util/concurrent/Executor;LX/3xt;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TContinuationResult:",
            "Ljava/lang/Object;",
            "TResult:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1el",
            "<TTContinuationResult;>;",
            "LX/1ex",
            "<TTResult;",
            "LX/1eg",
            "<TTContinuationResult;>;>;",
            "LX/1eg",
            "<TTResult;>;",
            "Ljava/util/concurrent/Executor;",
            "LX/3xt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 288877
    :try_start_0
    new-instance v0, Lbolts/Task$15;

    invoke-direct {v0, p4, p0, p1, p2}, Lbolts/Task$15;-><init>(LX/3xt;LX/1el;LX/1ex;LX/1eg;)V

    const v1, 0x6c96b112

    invoke-static {p3, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 288878
    :goto_0
    return-void

    .line 288879
    :catch_0
    move-exception v0

    .line 288880
    new-instance v1, LX/3xw;

    invoke-direct {v1, v0}, LX/3xw;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {p0, v1}, LX/1el;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private static g(LX/1eg;)Z
    .locals 2

    .prologue
    .line 288737
    iget-object v1, p0, LX/1eg;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 288738
    :try_start_0
    iget-boolean v0, p0, LX/1eg;->f:Z

    monitor-exit v1

    return v0

    .line 288739
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static h(LX/1eg;)V
    .locals 3

    .prologue
    .line 288755
    iget-object v1, p0, LX/1eg;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 288756
    :try_start_0
    iget-object v0, p0, LX/1eg;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288757
    :try_start_1
    invoke-interface {v0, p0}, LX/1ex;->a(LX/1eg;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 288758
    :catch_0
    move-exception v0

    .line 288759
    :try_start_2
    throw v0

    .line 288760
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 288761
    :catch_1
    move-exception v0

    .line 288762
    :try_start_3
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 288763
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/1eg;->l:Ljava/util/List;

    .line 288764
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method


# virtual methods
.method public final a(LX/1ex;)LX/1eg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TContinuationResult:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1ex",
            "<TTResult;TTContinuationResult;>;)",
            "LX/1eg",
            "<TTContinuationResult;>;"
        }
    .end annotation

    .prologue
    .line 288765
    sget-object v0, LX/1eg;->c:Ljava/util/concurrent/Executor;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/1eg;->a(LX/1ex;Ljava/util/concurrent/Executor;LX/3xt;)LX/1eg;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/1ex;)LX/1eg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TContinuationResult:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1ex",
            "<TTResult;",
            "LX/1eg",
            "<TTContinuationResult;>;>;)",
            "LX/1eg",
            "<TTContinuationResult;>;"
        }
    .end annotation

    .prologue
    .line 288766
    sget-object v0, LX/1eg;->c:Ljava/util/concurrent/Executor;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/1eg;->b(LX/1ex;Ljava/util/concurrent/Executor;LX/3xt;)LX/1eg;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 288767
    iget-object v1, p0, LX/1eg;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 288768
    :try_start_0
    iget-boolean v0, p0, LX/1eg;->g:Z

    monitor-exit v1

    return v0

    .line 288769
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Ljava/lang/Exception;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 288740
    iget-object v2, p0, LX/1eg;->e:Ljava/lang/Object;

    monitor-enter v2

    .line 288741
    :try_start_0
    iget-boolean v3, p0, LX/1eg;->f:Z

    if-eqz v3, :cond_0

    .line 288742
    monitor-exit v2

    .line 288743
    :goto_0
    return v0

    .line 288744
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1eg;->f:Z

    .line 288745
    iput-object p1, p0, LX/1eg;->i:Ljava/lang/Exception;

    .line 288746
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1eg;->j:Z

    .line 288747
    iget-object v0, p0, LX/1eg;->e:Ljava/lang/Object;

    const v3, -0x31d8d80d

    invoke-static {v0, v3}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 288748
    invoke-static {p0}, LX/1eg;->h(LX/1eg;)V

    .line 288749
    iget-boolean v0, p0, LX/1eg;->j:Z

    if-nez v0, :cond_1

    .line 288750
    sget-object v0, LX/1eg;->d:LX/3xz;

    move-object v0, v0

    .line 288751
    if-eqz v0, :cond_1

    .line 288752
    new-instance v0, LX/3y0;

    invoke-direct {v0, p0}, LX/3y0;-><init>(LX/1eg;)V

    iput-object v0, p0, LX/1eg;->k:LX/3y0;

    .line 288753
    :cond_1
    monitor-exit v2

    move v0, v1

    goto :goto_0

    .line 288754
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Ljava/lang/Object;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTResult;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 288770
    iget-object v1, p0, LX/1eg;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 288771
    :try_start_0
    iget-boolean v2, p0, LX/1eg;->f:Z

    if-eqz v2, :cond_0

    .line 288772
    const/4 v0, 0x0

    monitor-exit v1

    .line 288773
    :goto_0
    return v0

    .line 288774
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/1eg;->f:Z

    .line 288775
    iput-object p1, p0, LX/1eg;->h:Ljava/lang/Object;

    .line 288776
    iget-object v2, p0, LX/1eg;->e:Ljava/lang/Object;

    const v3, -0x657f77ba

    invoke-static {v2, v3}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 288777
    invoke-static {p0}, LX/1eg;->h(LX/1eg;)V

    .line 288778
    monitor-exit v1

    goto :goto_0

    .line 288779
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 288780
    iget-object v1, p0, LX/1eg;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 288781
    :try_start_0
    invoke-virtual {p0}, LX/1eg;->e()Ljava/lang/Exception;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 288782
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTResult;"
        }
    .end annotation

    .prologue
    .line 288783
    iget-object v1, p0, LX/1eg;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 288784
    :try_start_0
    iget-object v0, p0, LX/1eg;->h:Ljava/lang/Object;

    monitor-exit v1

    return-object v0

    .line 288785
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final e()Ljava/lang/Exception;
    .locals 3

    .prologue
    .line 288786
    iget-object v1, p0, LX/1eg;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 288787
    :try_start_0
    iget-object v0, p0, LX/1eg;->i:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    .line 288788
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1eg;->j:Z

    .line 288789
    iget-object v0, p0, LX/1eg;->k:LX/3y0;

    if-eqz v0, :cond_0

    .line 288790
    iget-object v0, p0, LX/1eg;->k:LX/3y0;

    .line 288791
    const/4 v2, 0x0

    iput-object v2, v0, LX/3y0;->a:LX/1eg;

    .line 288792
    const/4 v0, 0x0

    iput-object v0, p0, LX/1eg;->k:LX/3y0;

    .line 288793
    :cond_0
    iget-object v0, p0, LX/1eg;->i:Ljava/lang/Exception;

    monitor-exit v1

    return-object v0

    .line 288794
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final f()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 288795
    iget-object v1, p0, LX/1eg;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 288796
    :try_start_0
    iget-boolean v2, p0, LX/1eg;->f:Z

    if-eqz v2, :cond_0

    .line 288797
    const/4 v0, 0x0

    monitor-exit v1

    .line 288798
    :goto_0
    return v0

    .line 288799
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/1eg;->f:Z

    .line 288800
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/1eg;->g:Z

    .line 288801
    iget-object v2, p0, LX/1eg;->e:Ljava/lang/Object;

    const v3, -0x43db8538

    invoke-static {v2, v3}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 288802
    invoke-static {p0}, LX/1eg;->h(LX/1eg;)V

    .line 288803
    monitor-exit v1

    goto :goto_0

    .line 288804
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
