.class public LX/10j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/10k;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/10j;


# instance fields
.field private final a:LX/0gh;

.field private final b:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private c:Z


# direct methods
.method public constructor <init>(LX/0gh;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 169281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169282
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/10j;->c:Z

    .line 169283
    iput-object p1, p0, LX/10j;->a:LX/0gh;

    .line 169284
    iput-object p2, p0, LX/10j;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 169285
    return-void
.end method

.method public static a(LX/0QB;)LX/10j;
    .locals 5

    .prologue
    .line 169286
    sget-object v0, LX/10j;->d:LX/10j;

    if-nez v0, :cond_1

    .line 169287
    const-class v1, LX/10j;

    monitor-enter v1

    .line 169288
    :try_start_0
    sget-object v0, LX/10j;->d:LX/10j;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 169289
    if-eqz v2, :cond_0

    .line 169290
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 169291
    new-instance p0, LX/10j;

    invoke-static {v0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v3

    check-cast v3, LX/0gh;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v4

    check-cast v4, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {p0, v3, v4}, LX/10j;-><init>(LX/0gh;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 169292
    move-object v0, p0

    .line 169293
    sput-object v0, LX/10j;->d:LX/10j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169294
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 169295
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 169296
    :cond_1
    sget-object v0, LX/10j;->d:LX/10j;

    return-object v0

    .line 169297
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 169298
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/ui/drawers/DrawerContentFragment;
    .locals 2

    .prologue
    .line 169277
    iget-boolean v0, p0, LX/10j;->c:Z

    if-nez v0, :cond_0

    .line 169278
    iget-object v0, p0, LX/10j;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x5f0001

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 169279
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/10j;->c:Z

    .line 169280
    :cond_0
    new-instance v0, Lcom/facebook/divebar/contacts/DivebarFragment;

    invoke-direct {v0}, Lcom/facebook/divebar/contacts/DivebarFragment;-><init>()V

    return-object v0
.end method

.method public final a(LX/0ex;LX/0fK;)V
    .locals 0

    .prologue
    .line 169276
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 169299
    if-eqz p1, :cond_0

    .line 169300
    iget-object v0, p0, LX/10j;->a:LX/0gh;

    const-string v1, "tap_dive_bar"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    move-result-object v0

    const-string v1, "divebar"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0gh;->a(Ljava/lang/String;Z)V

    .line 169301
    :goto_0
    return-void

    .line 169302
    :cond_0
    iget-object v0, p0, LX/10j;->a:LX/0gh;

    .line 169303
    iget-object v1, v0, LX/0gh;->w:Ljava/lang/String;

    move-object v0, v1

    .line 169304
    if-nez v0, :cond_1

    .line 169305
    iget-object v0, p0, LX/10j;->a:LX/0gh;

    const-string v1, "tap_outside"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 169306
    :cond_1
    iget-object v0, p0, LX/10j;->a:LX/0gh;

    const-string v1, "divebar"

    invoke-virtual {v0, v1}, LX/0gh;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(LX/10b;)Z
    .locals 1

    .prologue
    .line 169275
    sget-object v0, LX/10b;->RIGHT:LX/10b;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 169274
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 169272
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 169273
    const/4 v0, 0x0

    return v0
.end method
