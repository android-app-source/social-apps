.class public LX/1EM;
.super LX/1Cd;
.source ""

# interfaces
.implements LX/1Ce;


# annotations
.annotation build Lcom/facebook/controllercallbacks/api/ControllerConfig;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Dispatcher::",
        "LX/0pR;",
        ">",
        "LX/1Cd;",
        "LX/1Ce;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0SG;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/0ad;

.field private final e:LX/0pJ;

.field public f:LX/0pR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TDispatcher;"
        }
    .end annotation
.end field

.field public g:LX/0pR;

.field public h:Lcom/facebook/api/feedtype/FeedType;

.field public i:J

.field private j:Ljava/lang/Long;

.field public k:Ljava/lang/Long;

.field public l:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 219675
    const-class v0, LX/1EM;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1EM;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0pJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 219630
    invoke-direct {p0}, LX/1Cd;-><init>()V

    .line 219631
    iput-object p1, p0, LX/1EM;->d:LX/0ad;

    .line 219632
    iput-object p2, p0, LX/1EM;->b:LX/0SG;

    .line 219633
    iput-object p3, p0, LX/1EM;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 219634
    iput-object p4, p0, LX/1EM;->e:LX/0pJ;

    .line 219635
    return-void
.end method

.method public static a(LX/0QB;)LX/1EM;
    .locals 5

    .prologue
    .line 219672
    new-instance v4, LX/1EM;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0pJ;->a(LX/0QB;)LX/0pJ;

    move-result-object v3

    check-cast v3, LX/0pJ;

    invoke-direct {v4, v0, v1, v2, v3}, LX/1EM;-><init>(LX/0ad;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0pJ;)V

    .line 219673
    move-object v0, v4

    .line 219674
    return-object v0
.end method

.method private f()J
    .locals 4

    .prologue
    .line 219669
    iget-object v0, p0, LX/1EM;->k:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 219670
    iget-object v0, p0, LX/1EM;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0pP;->c:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LX/1EM;->k:Ljava/lang/Long;

    .line 219671
    :cond_0
    iget-object v0, p0, LX/1EM;->k:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 219666
    iget-object v0, p0, LX/1EM;->j:Ljava/lang/Long;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LX/1EM;->j:Ljava/lang/Long;

    .line 219667
    return-void

    .line 219668
    :cond_0
    iget-object v0, p0, LX/1EM;->j:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 14

    .prologue
    .line 219645
    invoke-static {p1}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 219646
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v1, :cond_1

    .line 219647
    :cond_0
    :goto_0
    return-void

    .line 219648
    :cond_1
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 219649
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aM()J

    move-result-wide v2

    .line 219650
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    .line 219651
    iget-object v0, p0, LX/1EM;->j:Ljava/lang/Long;

    if-nez v0, :cond_5

    move-wide v0, v2

    :goto_1
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LX/1EM;->j:Ljava/lang/Long;

    .line 219652
    invoke-virtual {p0}, LX/1EM;->c()Z

    move-result v0

    if-nez v0, :cond_4

    iget-wide v0, p0, LX/1EM;->i:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_4

    invoke-direct {p0}, LX/1EM;->f()J

    move-result-wide v0

    iget-wide v4, p0, LX/1EM;->i:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_4

    .line 219653
    iget-object v8, p0, LX/1EM;->b:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    iput-object v8, p0, LX/1EM;->l:Ljava/lang/Long;

    .line 219654
    iget-object v8, p0, LX/1EM;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v8

    sget-object v9, LX/0pP;->d:LX/0Tn;

    iget-object v10, p0, LX/1EM;->l:Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-interface {v8, v9, v10, v11}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v8

    invoke-interface {v8}, LX/0hN;->commit()V

    .line 219655
    iget-wide v6, p0, LX/1EM;->i:J

    .line 219656
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    iput-object v8, p0, LX/1EM;->k:Ljava/lang/Long;

    .line 219657
    iget-object v8, p0, LX/1EM;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v8

    sget-object v9, LX/0pP;->c:LX/0Tn;

    invoke-interface {v8, v9, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v8

    invoke-interface {v8}, LX/0hN;->commit()V

    .line 219658
    iget-object v8, p0, LX/1EM;->f:LX/0pR;

    if-eqz v8, :cond_2

    .line 219659
    iget-object v8, p0, LX/1EM;->f:LX/0pR;

    invoke-interface {v8}, LX/0pR;->p()V

    .line 219660
    :cond_2
    iget-object v8, p0, LX/1EM;->g:LX/0pR;

    if-eqz v8, :cond_3

    .line 219661
    iget-object v8, p0, LX/1EM;->g:LX/0pR;

    invoke-interface {v8}, LX/0pR;->p()V

    .line 219662
    :cond_3
    const/4 v8, 0x3

    invoke-static {v8}, LX/01m;->b(I)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 219663
    new-instance v8, Ljava/text/SimpleDateFormat;

    const-string v9, "MM/dd HH:mm"

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v8, v9, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance v9, Ljava/util/Date;

    iget-object v10, p0, LX/1EM;->k:Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v8, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    .line 219664
    :cond_4
    iput-wide v2, p0, LX/1EM;->i:J

    goto/16 :goto_0

    .line 219665
    :cond_5
    iget-object v0, p0, LX/1EM;->j:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto/16 :goto_1
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 219642
    iget-object v0, p0, LX/1EM;->h:Lcom/facebook/api/feedtype/FeedType;

    invoke-static {v0}, LX/0pO;->a(Lcom/facebook/api/feedtype/FeedType;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1EM;->e:LX/0pJ;

    invoke-virtual {v0, v2}, LX/0pJ;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219643
    iget-object v0, p0, LX/1EM;->e:LX/0pJ;

    invoke-virtual {v0, v2}, LX/0pJ;->a(Z)Z

    move-result v0

    .line 219644
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1EM;->d:LX/0ad;

    sget-short v1, LX/0fe;->ay:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()J
    .locals 4

    .prologue
    .line 219639
    iget-object v0, p0, LX/1EM;->l:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 219640
    iget-object v0, p0, LX/1EM;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0pP;->d:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LX/1EM;->l:Ljava/lang/Long;

    .line 219641
    :cond_0
    iget-object v0, p0, LX/1EM;->l:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 219638
    iget-object v0, p0, LX/1EM;->j:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/1EM;->f()J

    move-result-wide v0

    iget-object v2, p0, LX/1EM;->j:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 4

    .prologue
    .line 219637
    invoke-direct {p0}, LX/1EM;->f()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 219636
    iget-object v0, p0, LX/1EM;->j:Ljava/lang/Long;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/1EM;->j:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method
