.class public LX/0tn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/0tn;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;",
            "LX/0uh;",
            ">;"
        }
    .end annotation
.end field

.field private final b:[LX/0ui;

.field private final c:LX/0ad;

.field public final d:LX/0tr;


# direct methods
.method public constructor <init>(LX/0to;LX/0tp;LX/0tq;LX/0ad;LX/0tr;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v8, 0x4

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 155652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155653
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    sget-object v1, LX/0uh;->UNKNOWN:LX/0uh;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->LOW:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    sget-object v3, LX/0uh;->LOW:LX/0uh;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->HIGH:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    sget-object v5, LX/0uh;->HIGH:LX/0uh;

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/0tn;->a:Ljava/util/Map;

    .line 155654
    iput-object p4, p0, LX/0tn;->c:LX/0ad;

    .line 155655
    iput-object p5, p0, LX/0tn;->d:LX/0tr;

    .line 155656
    new-array v0, v8, [LX/0ui;

    iput-object v0, p0, LX/0tn;->b:[LX/0ui;

    move v2, v7

    .line 155657
    :goto_0
    if-ge v2, v8, :cond_2

    .line 155658
    and-int/lit8 v0, v2, 0x2

    if-eqz v0, :cond_0

    move v1, v6

    .line 155659
    :goto_1
    and-int/lit8 v0, v2, 0x1

    if-eqz v0, :cond_1

    move v0, v6

    .line 155660
    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v1, v0}, LX/0tp;->a(Ljava/lang/Boolean;Ljava/lang/Boolean;)LX/0uj;

    move-result-object v0

    .line 155661
    new-instance v3, LX/0ul;

    invoke-static {p1}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a(LX/0QB;)Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    invoke-direct {v3, v1, v0}, LX/0ul;-><init>(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;)V

    .line 155662
    move-object v1, v3

    .line 155663
    new-instance v4, LX/0up;

    invoke-static {p3}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a(LX/0QB;)Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    move-result-object v3

    check-cast v3, Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    invoke-direct {v4, v3, v0}, LX/0up;-><init>(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;)V

    .line 155664
    move-object v3, v4

    .line 155665
    iget-object v4, p0, LX/0tn;->b:[LX/0ui;

    new-instance v5, LX/0ui;

    invoke-direct {v5, v0, v3, v1}, LX/0ui;-><init>(LX/0uj;LX/0up;LX/0ul;)V

    aput-object v5, v4, v2

    .line 155666
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v1, v7

    .line 155667
    goto :goto_1

    :cond_1
    move v0, v7

    .line 155668
    goto :goto_2

    .line 155669
    :cond_2
    return-void
.end method

.method public static a(LX/0QB;)LX/0tn;
    .locals 9

    .prologue
    .line 155670
    sget-object v0, LX/0tn;->e:LX/0tn;

    if-nez v0, :cond_1

    .line 155671
    const-class v1, LX/0tn;

    monitor-enter v1

    .line 155672
    :try_start_0
    sget-object v0, LX/0tn;->e:LX/0tn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 155673
    if-eqz v2, :cond_0

    .line 155674
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 155675
    new-instance v3, LX/0tn;

    const-class v4, LX/0to;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/0to;

    const-class v5, LX/0tp;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/0tp;

    const-class v6, LX/0tq;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/0tq;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {v0}, LX/0tr;->b(LX/0QB;)LX/0tr;

    move-result-object v8

    check-cast v8, LX/0tr;

    invoke-direct/range {v3 .. v8}, LX/0tn;-><init>(LX/0to;LX/0tp;LX/0tq;LX/0ad;LX/0tr;)V

    .line 155676
    move-object v0, v3

    .line 155677
    sput-object v0, LX/0tn;->e:LX/0tn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155678
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 155679
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 155680
    :cond_1
    sget-object v0, LX/0tn;->e:LX/0tn;

    return-object v0

    .line 155681
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 155682
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/0tn;Lcom/facebook/graphql/model/GraphQLStory;I)V
    .locals 10

    .prologue
    .line 155683
    iget-object v0, p0, LX/0tn;->d:LX/0tr;

    .line 155684
    iget-object v4, v0, LX/0tr;->a:LX/0uf;

    sget-wide v6, LX/0X5;->cV:J

    const-wide/16 v8, -0x1

    invoke-virtual {v4, v6, v7, v8, v9}, LX/0uf;->a(JJ)J

    move-result-wide v4

    move-wide v0, v4

    .line 155685
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_2

    .line 155686
    int-to-long v2, p2

    cmp-long v0, v2, v0

    if-gez v0, :cond_1

    sget-object v0, LX/0uh;->HIGH:LX/0uh;

    :goto_0
    invoke-virtual {p0, p1, v0}, LX/0tn;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/0uh;)V

    .line 155687
    :cond_0
    :goto_1
    return-void

    .line 155688
    :cond_1
    sget-object v0, LX/0uh;->LOW:LX/0uh;

    goto :goto_0

    .line 155689
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    .line 155690
    if-eqz v0, :cond_0

    .line 155691
    iget-object v1, p0, LX/0tn;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->o()Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0uh;

    .line 155692
    if-nez v0, :cond_3

    .line 155693
    sget-object v0, LX/0uh;->UNKNOWN:LX/0uh;

    .line 155694
    :cond_3
    invoke-virtual {p0, p1, v0}, LX/0tn;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/0uh;)V

    goto :goto_1
.end method

.method private static a(LX/0tn;LX/0ul;Lcom/facebook/graphql/model/GraphQLFeedback;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 155695
    invoke-static {p2}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, LX/0tn;->c:LX/0ad;

    sget-short v3, LX/0fe;->w:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    .line 155696
    :goto_0
    if-eqz v2, :cond_0

    .line 155697
    invoke-virtual {p1, p2}, LX/0ul;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 155698
    :cond_0
    invoke-static {p2}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    if-lez v3, :cond_3

    iget-object v3, p0, LX/0tn;->c:LX/0ad;

    sget-short v4, LX/0fe;->w:S

    invoke-interface {v3, v4, v1}, LX/0ad;->a(SZ)Z

    move-result v3

    if-nez v3, :cond_3

    .line 155699
    :goto_1
    if-eqz v0, :cond_1

    .line 155700
    iget-object v0, p1, LX/0ul;->a:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    iget-object v1, p1, LX/0ul;->b:LX/0uk;

    .line 155701
    iget-object v3, v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v4, LX/1w7;

    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {v4, p2, v1, p1}, LX/1w7;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;LX/0uk;Ljava/lang/Integer;)V

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    .line 155702
    invoke-static {v0}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->b(Lcom/facebook/api/prefetch/GraphQLPrefetchController;)V

    .line 155703
    :cond_1
    return v2

    :cond_2
    move v2, v1

    .line 155704
    goto :goto_0

    :cond_3
    move v0, v1

    .line 155705
    goto :goto_1
.end method

.method public static b(LX/0tn;Lcom/facebook/graphql/model/GraphQLStory;)LX/0ul;
    .locals 2

    .prologue
    .line 155706
    invoke-static {p1}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    .line 155707
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    .line 155708
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->n()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 155709
    :goto_0
    invoke-static {p0, v1, v0}, LX/0tn;->c(LX/0tn;ZZ)LX/0ui;

    move-result-object v0

    .line 155710
    iget-object v0, v0, LX/0ui;->c:LX/0ul;

    return-object v0

    .line 155711
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/0tn;ZZ)LX/0ui;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 155712
    if-eqz p1, :cond_0

    move v2, v0

    :goto_0
    shl-int/lit8 v2, v2, 0x1

    or-int/lit8 v2, v2, 0x0

    .line 155713
    if-eqz p2, :cond_1

    :goto_1
    or-int/2addr v0, v2

    .line 155714
    iget-object v1, p0, LX/0tn;->b:[LX/0ui;

    aget-object v0, v1, v0

    return-object v0

    :cond_0
    move v2, v1

    .line 155715
    goto :goto_0

    :cond_1
    move v0, v1

    .line 155716
    goto :goto_1
.end method


# virtual methods
.method public final a(ZZ)LX/0up;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)",
            "LX/0up",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155717
    invoke-static {p0, p1, p2}, LX/0tn;->c(LX/0tn;ZZ)LX/0ui;

    move-result-object v0

    iget-object v0, v0, LX/0ui;->b:LX/0up;

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;ZLjava/lang/String;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 155718
    if-eqz p3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {p0, p2, v0}, LX/0tn;->c(LX/0tn;ZZ)LX/0ui;

    move-result-object v0

    .line 155719
    if-eqz p3, :cond_0

    .line 155720
    iget-object v1, v0, LX/0ui;->a:LX/0uj;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p3}, LX/0uj;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 155721
    :cond_0
    iget-object v1, v0, LX/0ui;->c:LX/0ul;

    invoke-virtual {v1, p1}, LX/0ul;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 155722
    iget-object v0, v0, LX/0ui;->a:LX/0uj;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uj;->c(Ljava/lang/String;)V

    .line 155723
    return-void

    .line 155724
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;ILX/0ta;)V
    .locals 11

    .prologue
    .line 155725
    invoke-static {p0, p1}, LX/0tn;->b(LX/0tn;Lcom/facebook/graphql/model/GraphQLStory;)LX/0ul;

    move-result-object v0

    .line 155726
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 155727
    if-eqz v1, :cond_0

    invoke-static {p0, v0, v1}, LX/0tn;->a(LX/0tn;LX/0ul;Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iget-object v2, p0, LX/0tn;->c:LX/0ad;

    sget-short v3, LX/0fe;->u:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 155728
    :cond_1
    :goto_0
    return-void

    .line 155729
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v5

    .line 155730
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->n()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p0, LX/0tn;->d:LX/0tr;

    .line 155731
    iget-object v7, v5, LX/0tr;->a:LX/0uf;

    sget-wide v9, LX/0X5;->cU:J

    const/4 v8, 0x0

    invoke-virtual {v7, v9, v10, v8}, LX/0uf;->a(JZ)Z

    move-result v7

    move v5, v7

    .line 155732
    if-eqz v5, :cond_4

    .line 155733
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    .line 155734
    :goto_1
    if-nez v5, :cond_3

    .line 155735
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    .line 155736
    if-eqz v6, :cond_3

    .line 155737
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    goto :goto_1

    .line 155738
    :cond_3
    if-eqz v5, :cond_4

    .line 155739
    sget-object v6, LX/0uh;->HIGH:LX/0uh;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v6, v5}, LX/0ul;->a(LX/0uh;Ljava/lang/String;)V

    .line 155740
    :cond_4
    if-eqz v1, :cond_1

    .line 155741
    invoke-static {p0, p1, p2}, LX/0tn;->a(LX/0tn;Lcom/facebook/graphql/model/GraphQLStory;I)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/0uh;)V
    .locals 2

    .prologue
    .line 155742
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 155743
    if-eqz v0, :cond_0

    .line 155744
    invoke-static {p0, p1}, LX/0tn;->b(LX/0tn;Lcom/facebook/graphql/model/GraphQLStory;)LX/0ul;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p2, v0}, LX/0ul;->a(LX/0uh;Ljava/lang/String;)V

    .line 155745
    :cond_0
    return-void
.end method
