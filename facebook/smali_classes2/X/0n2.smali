.class public abstract LX/0n2;
.super LX/0n3;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public transient e:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "LX/4pR;",
            "LX/4qM;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0n2;LX/0mu;LX/15w;LX/4po;)V
    .locals 0

    .prologue
    .line 133677
    invoke-direct {p0, p1, p2, p3, p4}, LX/0n3;-><init>(LX/0n3;LX/0mu;LX/15w;LX/4po;)V

    .line 133678
    return-void
.end method

.method public constructor <init>(LX/0n2;LX/0n6;)V
    .locals 0

    .prologue
    .line 133675
    invoke-direct {p0, p1, p2}, LX/0n3;-><init>(LX/0n3;LX/0n6;)V

    .line 133676
    return-void
.end method

.method public constructor <init>(LX/0n6;LX/0nP;)V
    .locals 0

    .prologue
    .line 133673
    invoke-direct {p0, p1, p2}, LX/0n3;-><init>(LX/0n6;LX/0nP;)V

    .line 133674
    return-void
.end method


# virtual methods
.method public abstract a(LX/0mu;LX/15w;LX/4po;)LX/0n2;
.end method

.method public abstract a(LX/0n6;)LX/0n2;
.end method

.method public final a(Ljava/lang/Object;LX/4pS;)LX/4qM;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "LX/4pS",
            "<*>;)",
            "LX/4qM;"
        }
    .end annotation

    .prologue
    .line 133665
    invoke-virtual {p2, p1}, LX/4pS;->a(Ljava/lang/Object;)LX/4pR;

    move-result-object v1

    .line 133666
    iget-object v0, p0, LX/0n2;->e:Ljava/util/LinkedHashMap;

    if-nez v0, :cond_1

    .line 133667
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/0n2;->e:Ljava/util/LinkedHashMap;

    .line 133668
    :cond_0
    new-instance v0, LX/4qM;

    invoke-direct {v0, p1}, LX/4qM;-><init>(Ljava/lang/Object;)V

    .line 133669
    iget-object v2, p0, LX/0n2;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133670
    :goto_0
    return-object v0

    .line 133671
    :cond_1
    iget-object v0, p0, LX/0n2;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4qM;

    .line 133672
    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method public final b(LX/0lO;Ljava/lang/Object;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 133648
    if-nez p2, :cond_1

    .line 133649
    :cond_0
    :goto_0
    return-object v1

    .line 133650
    :cond_1
    instance-of v0, p2, Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v0, :cond_3

    .line 133651
    check-cast p2, Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-object v1, p2

    .line 133652
    :cond_2
    :goto_1
    instance-of v0, v1, LX/1Xx;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 133653
    check-cast v0, LX/1Xx;

    invoke-interface {v0, p0}, LX/1Xx;->a(LX/0n3;)V

    goto :goto_0

    .line 133654
    :cond_3
    instance-of v0, p2, Ljava/lang/Class;

    if-nez v0, :cond_4

    .line 133655
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AnnotationIntrospector returned deserializer definition of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; expected type JsonDeserializer or Class<JsonDeserializer> instead"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133656
    :cond_4
    check-cast p2, Ljava/lang/Class;

    .line 133657
    const-class v0, Lcom/fasterxml/jackson/databind/JsonDeserializer$None;

    if-eq p2, v0, :cond_0

    const-class v0, LX/1Xp;

    if-eq p2, v0, :cond_0

    .line 133658
    const-class v0, Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 133659
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AnnotationIntrospector returned Class "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; expected Class<JsonDeserializer>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133660
    :cond_5
    iget-object v0, p0, LX/0n3;->_config:LX/0mu;

    invoke-virtual {v0}, LX/0m4;->l()LX/4py;

    move-result-object v0

    .line 133661
    if-nez v0, :cond_6

    .line 133662
    :goto_2
    if-nez v1, :cond_2

    .line 133663
    iget-object v0, p0, LX/0n3;->_config:LX/0mu;

    invoke-virtual {v0}, LX/0m4;->h()Z

    move-result v0

    invoke-static {p2, v0}, LX/1Xw;->b(Ljava/lang/Class;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-object v1, v0

    goto :goto_1

    .line 133664
    :cond_6
    invoke-virtual {v0}, LX/4py;->a()Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v1

    goto :goto_2
.end method

.method public final c(LX/0lO;Ljava/lang/Object;)LX/1Xt;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 133631
    if-nez p2, :cond_1

    .line 133632
    :cond_0
    :goto_0
    return-object v1

    .line 133633
    :cond_1
    instance-of v0, p2, LX/1Xt;

    if-eqz v0, :cond_3

    .line 133634
    check-cast p2, LX/1Xt;

    move-object v1, p2

    .line 133635
    :cond_2
    :goto_1
    instance-of v0, v1, LX/1Xx;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 133636
    check-cast v0, LX/1Xx;

    invoke-interface {v0, p0}, LX/1Xx;->a(LX/0n3;)V

    goto :goto_0

    .line 133637
    :cond_3
    instance-of v0, p2, Ljava/lang/Class;

    if-nez v0, :cond_4

    .line 133638
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AnnotationIntrospector returned key deserializer definition of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; expected type KeyDeserializer or Class<KeyDeserializer> instead"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133639
    :cond_4
    check-cast p2, Ljava/lang/Class;

    .line 133640
    const-class v0, LX/1Xs;

    if-eq p2, v0, :cond_0

    const-class v0, LX/1Xp;

    if-eq p2, v0, :cond_0

    .line 133641
    const-class v0, LX/1Xt;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 133642
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AnnotationIntrospector returned Class "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; expected Class<KeyDeserializer>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133643
    :cond_5
    iget-object v0, p0, LX/0n3;->_config:LX/0mu;

    invoke-virtual {v0}, LX/0m4;->l()LX/4py;

    move-result-object v0

    .line 133644
    if-nez v0, :cond_6

    .line 133645
    :goto_2
    if-nez v1, :cond_2

    .line 133646
    iget-object v0, p0, LX/0n3;->_config:LX/0mu;

    invoke-virtual {v0}, LX/0m4;->h()Z

    move-result v0

    invoke-static {p2, v0}, LX/1Xw;->b(Ljava/lang/Class;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Xt;

    move-object v1, v0

    goto :goto_1

    .line 133647
    :cond_6
    invoke-virtual {v0}, LX/4py;->b()LX/1Xt;

    move-result-object v1

    goto :goto_2
.end method
