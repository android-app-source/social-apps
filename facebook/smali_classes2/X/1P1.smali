.class public LX/1P1;
.super LX/1OR;
.source ""

# interfaces
.implements LX/1P2;


# instance fields
.field public a:LX/1ZW;

.field private b:Z

.field private c:Z

.field public d:Z

.field private e:Z

.field private f:Z

.field public j:I

.field public k:LX/1P5;

.field public l:Z

.field public m:I

.field public n:I

.field public o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

.field public final p:LX/1P3;


# direct methods
.method public constructor <init>(IZ)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 243422
    invoke-direct {p0}, LX/1OR;-><init>()V

    .line 243423
    iput-boolean v0, p0, LX/1P1;->c:Z

    .line 243424
    iput-boolean v0, p0, LX/1P1;->l:Z

    .line 243425
    iput-boolean v0, p0, LX/1P1;->d:Z

    .line 243426
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1P1;->e:Z

    .line 243427
    const/4 v0, -0x1

    iput v0, p0, LX/1P1;->m:I

    .line 243428
    const/high16 v0, -0x80000000

    iput v0, p0, LX/1P1;->n:I

    .line 243429
    const/4 v0, 0x0

    iput-object v0, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    .line 243430
    new-instance v0, LX/1P3;

    invoke-direct {v0, p0}, LX/1P3;-><init>(LX/1P1;)V

    iput-object v0, p0, LX/1P1;->p:LX/1P3;

    .line 243431
    invoke-virtual {p0, p1}, LX/1P1;->b(I)V

    .line 243432
    invoke-virtual {p0, p2}, LX/1P1;->b(Z)V

    .line 243433
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 243420
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/1P1;-><init>(IZ)V

    .line 243421
    return-void
.end method

.method public static I(LX/1P1;)Landroid/view/View;
    .locals 1

    .prologue
    .line 243419
    iget-boolean v0, p0, LX/1P1;->l:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    invoke-virtual {p0, v0}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static J(LX/1P1;)Landroid/view/View;
    .locals 1

    .prologue
    .line 243418
    iget-boolean v0, p0, LX/1P1;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private a(ILX/1Od;LX/1Ok;Z)I
    .locals 3

    .prologue
    .line 243407
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->d()I

    move-result v0

    sub-int/2addr v0, p1

    .line 243408
    if-lez v0, :cond_1

    .line 243409
    neg-int v0, v0

    invoke-direct {p0, v0, p2, p3}, LX/1P1;->d(ILX/1Od;LX/1Ok;)I

    move-result v0

    neg-int v0, v0

    .line 243410
    add-int v1, p1, v0

    .line 243411
    if-eqz p4, :cond_0

    .line 243412
    iget-object v2, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->d()I

    move-result v2

    sub-int v1, v2, v1

    .line 243413
    if-lez v1, :cond_0

    .line 243414
    iget-object v2, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v2, v1}, LX/1P5;->a(I)V

    .line 243415
    add-int/2addr v0, v1

    .line 243416
    :cond_0
    :goto_0
    return v0

    .line 243417
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/1P1;LX/1Od;LX/1ZW;LX/1Ok;Z)I
    .locals 7

    .prologue
    const/high16 v6, -0x80000000

    .line 243379
    iget v1, p2, LX/1ZW;->c:I

    .line 243380
    iget v0, p2, LX/1ZW;->g:I

    if-eq v0, v6, :cond_1

    .line 243381
    iget v0, p2, LX/1ZW;->c:I

    if-gez v0, :cond_0

    .line 243382
    iget v0, p2, LX/1ZW;->g:I

    iget v2, p2, LX/1ZW;->c:I

    add-int/2addr v0, v2

    iput v0, p2, LX/1ZW;->g:I

    .line 243383
    :cond_0
    invoke-direct {p0, p1, p2}, LX/1P1;->a(LX/1Od;LX/1ZW;)V

    .line 243384
    :cond_1
    iget v0, p2, LX/1ZW;->c:I

    iget v2, p2, LX/1ZW;->h:I

    add-int/2addr v0, v2

    .line 243385
    new-instance v2, LX/1Zz;

    invoke-direct {v2}, LX/1Zz;-><init>()V

    .line 243386
    :cond_2
    if-lez v0, :cond_7

    invoke-virtual {p2, p3}, LX/1ZW;->a(LX/1Ok;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 243387
    const/4 v3, 0x0

    .line 243388
    iput v3, v2, LX/1Zz;->a:I

    .line 243389
    iput-boolean v3, v2, LX/1Zz;->b:Z

    .line 243390
    iput-boolean v3, v2, LX/1Zz;->c:Z

    .line 243391
    iput-boolean v3, v2, LX/1Zz;->d:Z

    .line 243392
    invoke-virtual {p0, p1, p3, p2, v2}, LX/1P1;->a(LX/1Od;LX/1Ok;LX/1ZW;LX/1Zz;)V

    .line 243393
    iget-boolean v3, v2, LX/1Zz;->b:Z

    if-nez v3, :cond_7

    .line 243394
    iget v3, p2, LX/1ZW;->b:I

    iget v4, v2, LX/1Zz;->a:I

    iget v5, p2, LX/1ZW;->f:I

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    iput v3, p2, LX/1ZW;->b:I

    .line 243395
    iget-boolean v3, v2, LX/1Zz;->c:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, LX/1P1;->a:LX/1ZW;

    iget-object v3, v3, LX/1ZW;->k:Ljava/util/List;

    if-nez v3, :cond_3

    .line 243396
    iget-boolean v3, p3, LX/1Ok;->k:Z

    move v3, v3

    .line 243397
    if-nez v3, :cond_4

    .line 243398
    :cond_3
    iget v3, p2, LX/1ZW;->c:I

    iget v4, v2, LX/1Zz;->a:I

    sub-int/2addr v3, v4

    iput v3, p2, LX/1ZW;->c:I

    .line 243399
    iget v3, v2, LX/1Zz;->a:I

    sub-int/2addr v0, v3

    .line 243400
    :cond_4
    iget v3, p2, LX/1ZW;->g:I

    if-eq v3, v6, :cond_6

    .line 243401
    iget v3, p2, LX/1ZW;->g:I

    iget v4, v2, LX/1Zz;->a:I

    add-int/2addr v3, v4

    iput v3, p2, LX/1ZW;->g:I

    .line 243402
    iget v3, p2, LX/1ZW;->c:I

    if-gez v3, :cond_5

    .line 243403
    iget v3, p2, LX/1ZW;->g:I

    iget v4, p2, LX/1ZW;->c:I

    add-int/2addr v3, v4

    iput v3, p2, LX/1ZW;->g:I

    .line 243404
    :cond_5
    invoke-direct {p0, p1, p2}, LX/1P1;->a(LX/1Od;LX/1ZW;)V

    .line 243405
    :cond_6
    if-eqz p4, :cond_2

    iget-boolean v3, v2, LX/1Zz;->d:Z

    if-eqz v3, :cond_2

    .line 243406
    :cond_7
    iget v0, p2, LX/1ZW;->c:I

    sub-int v0, v1, v0

    return v0
.end method

.method private a(IIZZ)Landroid/view/View;
    .locals 7

    .prologue
    .line 243238
    invoke-virtual {p0}, LX/1P1;->k()V

    .line 243239
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->c()I

    move-result v3

    .line 243240
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->d()I

    move-result v4

    .line 243241
    if-le p2, p1, :cond_1

    const/4 v0, 0x1

    .line 243242
    :goto_0
    const/4 v2, 0x0

    .line 243243
    :goto_1
    if-eq p1, p2, :cond_3

    .line 243244
    invoke-virtual {p0, p1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v1

    .line 243245
    iget-object v5, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v5, v1}, LX/1P5;->a(Landroid/view/View;)I

    move-result v5

    .line 243246
    iget-object v6, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v6, v1}, LX/1P5;->b(Landroid/view/View;)I

    move-result v6

    .line 243247
    if-ge v5, v4, :cond_4

    if-le v6, v3, :cond_4

    .line 243248
    if-eqz p3, :cond_0

    .line 243249
    if-lt v5, v3, :cond_2

    if-gt v6, v4, :cond_2

    .line 243250
    :cond_0
    :goto_2
    return-object v1

    .line 243251
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 243252
    :cond_2
    if-eqz p4, :cond_4

    if-nez v2, :cond_4

    .line 243253
    :goto_3
    add-int/2addr p1, v0

    move-object v2, v1

    goto :goto_1

    :cond_3
    move-object v1, v2

    .line 243254
    goto :goto_2

    :cond_4
    move-object v1, v2

    goto :goto_3
.end method

.method private a(ZZ)Landroid/view/View;
    .locals 2

    .prologue
    .line 243338
    iget-boolean v0, p0, LX/1P1;->l:Z

    if-eqz v0, :cond_0

    .line 243339
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1, p1, p2}, LX/1P1;->a(IIZZ)Landroid/view/View;

    move-result-object v0

    .line 243340
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v1

    invoke-direct {p0, v0, v1, p1, p2}, LX/1P1;->a(IIZZ)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private a(IIZLX/1Ok;)V
    .locals 6

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x1

    .line 243316
    iget-object v2, p0, LX/1P1;->a:LX/1ZW;

    invoke-virtual {p0, p4}, LX/1P1;->a(LX/1Ok;)I

    move-result v3

    iput v3, v2, LX/1ZW;->h:I

    .line 243317
    iget-object v2, p0, LX/1P1;->a:LX/1ZW;

    iput p1, v2, LX/1ZW;->f:I

    .line 243318
    if-ne p1, v1, :cond_2

    .line 243319
    iget-object v2, p0, LX/1P1;->a:LX/1ZW;

    iget v3, v2, LX/1ZW;->h:I

    iget-object v4, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v4}, LX/1P5;->g()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v2, LX/1ZW;->h:I

    .line 243320
    invoke-static {p0}, LX/1P1;->J(LX/1P1;)Landroid/view/View;

    move-result-object v2

    .line 243321
    iget-object v3, p0, LX/1P1;->a:LX/1ZW;

    iget-boolean v4, p0, LX/1P1;->l:Z

    if-eqz v4, :cond_1

    :goto_0
    iput v0, v3, LX/1ZW;->e:I

    .line 243322
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    invoke-static {v2}, LX/1OR;->d(Landroid/view/View;)I

    move-result v1

    iget-object v3, p0, LX/1P1;->a:LX/1ZW;

    iget v3, v3, LX/1ZW;->e:I

    add-int/2addr v1, v3

    iput v1, v0, LX/1ZW;->d:I

    .line 243323
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iget-object v1, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v1, v2}, LX/1P5;->b(Landroid/view/View;)I

    move-result v1

    iput v1, v0, LX/1ZW;->b:I

    .line 243324
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0, v2}, LX/1P5;->b(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v1}, LX/1P5;->d()I

    move-result v1

    sub-int/2addr v0, v1

    .line 243325
    :goto_1
    iget-object v1, p0, LX/1P1;->a:LX/1ZW;

    iput p2, v1, LX/1ZW;->c:I

    .line 243326
    if-eqz p3, :cond_0

    .line 243327
    iget-object v1, p0, LX/1P1;->a:LX/1ZW;

    iget v2, v1, LX/1ZW;->c:I

    sub-int/2addr v2, v0

    iput v2, v1, LX/1ZW;->c:I

    .line 243328
    :cond_0
    iget-object v1, p0, LX/1P1;->a:LX/1ZW;

    iput v0, v1, LX/1ZW;->g:I

    .line 243329
    return-void

    :cond_1
    move v0, v1

    .line 243330
    goto :goto_0

    .line 243331
    :cond_2
    invoke-static {p0}, LX/1P1;->I(LX/1P1;)Landroid/view/View;

    move-result-object v2

    .line 243332
    iget-object v3, p0, LX/1P1;->a:LX/1ZW;

    iget v4, v3, LX/1ZW;->h:I

    iget-object v5, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v5}, LX/1P5;->c()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, v3, LX/1ZW;->h:I

    .line 243333
    iget-object v3, p0, LX/1P1;->a:LX/1ZW;

    iget-boolean v4, p0, LX/1P1;->l:Z

    if-eqz v4, :cond_3

    :goto_2
    iput v1, v3, LX/1ZW;->e:I

    .line 243334
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    invoke-static {v2}, LX/1OR;->d(Landroid/view/View;)I

    move-result v1

    iget-object v3, p0, LX/1P1;->a:LX/1ZW;

    iget v3, v3, LX/1ZW;->e:I

    add-int/2addr v1, v3

    iput v1, v0, LX/1ZW;->d:I

    .line 243335
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iget-object v1, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v1, v2}, LX/1P5;->a(Landroid/view/View;)I

    move-result v1

    iput v1, v0, LX/1ZW;->b:I

    .line 243336
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0, v2}, LX/1P5;->a(Landroid/view/View;)I

    move-result v0

    neg-int v0, v0

    iget-object v1, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v1}, LX/1P5;->c()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_3
    move v1, v0

    .line 243337
    goto :goto_2
.end method

.method private a(LX/1Od;LX/1ZW;)V
    .locals 6

    .prologue
    .line 243282
    iget-boolean v0, p2, LX/1ZW;->a:Z

    if-nez v0, :cond_0

    .line 243283
    :goto_0
    return-void

    .line 243284
    :cond_0
    iget v0, p2, LX/1ZW;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 243285
    iget v0, p2, LX/1ZW;->g:I

    const/4 v2, 0x0

    .line 243286
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v3

    .line 243287
    if-gez v0, :cond_4

    .line 243288
    :cond_1
    :goto_1
    goto :goto_0

    .line 243289
    :cond_2
    iget v0, p2, LX/1ZW;->g:I

    const/4 v2, 0x0

    .line 243290
    if-gez v0, :cond_8

    .line 243291
    :cond_3
    :goto_2
    goto :goto_0

    .line 243292
    :cond_4
    iget-object v1, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v1}, LX/1P5;->e()I

    move-result v1

    sub-int v4, v1, v0

    .line 243293
    iget-boolean v1, p0, LX/1P1;->l:Z

    if-eqz v1, :cond_6

    move v1, v2

    .line 243294
    :goto_3
    if-ge v1, v3, :cond_1

    .line 243295
    invoke-virtual {p0, v1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v5

    .line 243296
    iget-object p2, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {p2, v5}, LX/1P5;->a(Landroid/view/View;)I

    move-result v5

    if-ge v5, v4, :cond_5

    .line 243297
    invoke-static {p0, p1, v2, v1}, LX/1P1;->a(LX/1P1;LX/1Od;II)V

    goto :goto_1

    .line 243298
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 243299
    :cond_6
    add-int/lit8 v1, v3, -0x1

    :goto_4
    if-ltz v1, :cond_1

    .line 243300
    invoke-virtual {p0, v1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v2

    .line 243301
    iget-object v5, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v5, v2}, LX/1P5;->a(Landroid/view/View;)I

    move-result v2

    if-ge v2, v4, :cond_7

    .line 243302
    add-int/lit8 v2, v3, -0x1

    invoke-static {p0, p1, v2, v1}, LX/1P1;->a(LX/1P1;LX/1Od;II)V

    goto :goto_1

    .line 243303
    :cond_7
    add-int/lit8 v1, v1, -0x1

    goto :goto_4

    .line 243304
    :cond_8
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v3

    .line 243305
    iget-boolean v1, p0, LX/1P1;->l:Z

    if-eqz v1, :cond_a

    .line 243306
    add-int/lit8 v1, v3, -0x1

    :goto_5
    if-ltz v1, :cond_3

    .line 243307
    invoke-virtual {p0, v1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v2

    .line 243308
    iget-object v4, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v4, v2}, LX/1P5;->b(Landroid/view/View;)I

    move-result v2

    if-le v2, v0, :cond_9

    .line 243309
    add-int/lit8 v2, v3, -0x1

    invoke-static {p0, p1, v2, v1}, LX/1P1;->a(LX/1P1;LX/1Od;II)V

    goto :goto_2

    .line 243310
    :cond_9
    add-int/lit8 v1, v1, -0x1

    goto :goto_5

    :cond_a
    move v1, v2

    .line 243311
    :goto_6
    if-ge v1, v3, :cond_3

    .line 243312
    invoke-virtual {p0, v1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v4

    .line 243313
    iget-object v5, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v5, v4}, LX/1P5;->b(Landroid/view/View;)I

    move-result v4

    if-le v4, v0, :cond_b

    .line 243314
    invoke-static {p0, p1, v2, v1}, LX/1P1;->a(LX/1P1;LX/1Od;II)V

    goto :goto_2

    .line 243315
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_6
.end method

.method public static a(LX/1P1;LX/1Od;II)V
    .locals 1

    .prologue
    .line 243273
    if-ne p2, p3, :cond_1

    .line 243274
    :cond_0
    return-void

    .line 243275
    :cond_1
    if-le p3, p2, :cond_2

    .line 243276
    add-int/lit8 v0, p3, -0x1

    :goto_0
    if-lt v0, p2, :cond_0

    .line 243277
    invoke-virtual {p0, v0, p1}, LX/1OR;->a(ILX/1Od;)V

    .line 243278
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 243279
    :cond_2
    :goto_1
    if-le p2, p3, :cond_0

    .line 243280
    invoke-virtual {p0, p2, p1}, LX/1OR;->a(ILX/1Od;)V

    .line 243281
    add-int/lit8 p2, p2, -0x1

    goto :goto_1
.end method

.method private a(LX/1P3;)V
    .locals 2

    .prologue
    .line 243271
    iget v0, p1, LX/1P3;->a:I

    iget v1, p1, LX/1P3;->b:I

    invoke-static {p0, v0, v1}, LX/1P1;->f(LX/1P1;II)V

    .line 243272
    return-void
.end method

.method private b(ILX/1Od;LX/1Ok;Z)I
    .locals 4

    .prologue
    .line 243260
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->c()I

    move-result v0

    sub-int v0, p1, v0

    .line 243261
    if-lez v0, :cond_1

    .line 243262
    invoke-direct {p0, v0, p2, p3}, LX/1P1;->d(ILX/1Od;LX/1Ok;)I

    move-result v0

    neg-int v0, v0

    .line 243263
    add-int v1, p1, v0

    .line 243264
    if-eqz p4, :cond_0

    .line 243265
    iget-object v2, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->c()I

    move-result v2

    sub-int/2addr v1, v2

    .line 243266
    if-lez v1, :cond_0

    .line 243267
    iget-object v2, p0, LX/1P1;->k:LX/1P5;

    neg-int v3, v1

    invoke-virtual {v2, v3}, LX/1P5;->a(I)V

    .line 243268
    sub-int/2addr v0, v1

    .line 243269
    :cond_0
    :goto_0
    return v0

    .line 243270
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(ZZ)Landroid/view/View;
    .locals 2

    .prologue
    .line 243257
    iget-boolean v0, p0, LX/1P1;->l:Z

    if-eqz v0, :cond_0

    .line 243258
    const/4 v0, 0x0

    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v1

    invoke-direct {p0, v0, v1, p1, p2}, LX/1P1;->a(IIZZ)Landroid/view/View;

    move-result-object v0

    .line 243259
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1, p1, p2}, LX/1P1;->a(IIZZ)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private b(LX/1P3;)V
    .locals 2

    .prologue
    .line 243255
    iget v0, p1, LX/1P3;->a:I

    iget v1, p1, LX/1P3;->b:I

    invoke-static {p0, v0, v1}, LX/1P1;->g(LX/1P1;II)V

    .line 243256
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 243552
    iget v1, p0, LX/1P1;->j:I

    if-eq v1, v0, :cond_0

    invoke-virtual {p0}, LX/1P1;->j()Z

    move-result v1

    if-nez v1, :cond_2

    .line 243553
    :cond_0
    iget-boolean v0, p0, LX/1P1;->c:Z

    .line 243554
    :cond_1
    :goto_0
    iput-boolean v0, p0, LX/1P1;->l:Z

    .line 243555
    return-void

    .line 243556
    :cond_2
    iget-boolean v1, p0, LX/1P1;->c:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/1P1;LX/1Od;LX/1Ok;LX/1P3;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 243438
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v2

    if-nez v2, :cond_1

    .line 243439
    :cond_0
    :goto_0
    return v0

    .line 243440
    :cond_1
    invoke-virtual {p0}, LX/1OR;->C()Landroid/view/View;

    move-result-object v2

    .line 243441
    if-eqz v2, :cond_3

    invoke-static {p3, v2, p2}, LX/1P3;->a(LX/1P3;Landroid/view/View;LX/1Ok;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 243442
    const/4 p1, 0x0

    .line 243443
    iget-object v0, p3, LX/1P3;->d:LX/1P1;

    iget-object v0, v0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->b()I

    move-result v0

    .line 243444
    if-ltz v0, :cond_9

    .line 243445
    invoke-virtual {p3, v2}, LX/1P3;->b(Landroid/view/View;)V

    .line 243446
    :cond_2
    :goto_1
    move v0, v1

    .line 243447
    goto :goto_0

    .line 243448
    :cond_3
    iget-boolean v2, p0, LX/1P1;->b:Z

    iget-boolean v3, p0, LX/1P1;->d:Z

    if-ne v2, v3, :cond_0

    .line 243449
    iget-boolean v2, p3, LX/1P3;->c:Z

    if-eqz v2, :cond_7

    invoke-static {p0, p1, p2}, LX/1P1;->d(LX/1P1;LX/1Od;LX/1Ok;)Landroid/view/View;

    move-result-object v2

    .line 243450
    :goto_2
    if-eqz v2, :cond_0

    .line 243451
    invoke-virtual {p3, v2}, LX/1P3;->b(Landroid/view/View;)V

    .line 243452
    iget-boolean v3, p2, LX/1Ok;->k:Z

    move v3, v3

    .line 243453
    if-nez v3, :cond_6

    invoke-virtual {p0}, LX/1OR;->e()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 243454
    iget-object v3, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v3, v2}, LX/1P5;->a(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v4}, LX/1P5;->d()I

    move-result v4

    if-ge v3, v4, :cond_4

    iget-object v3, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v3, v2}, LX/1P5;->b(Landroid/view/View;)I

    move-result v2

    iget-object v3, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v3}, LX/1P5;->c()I

    move-result v3

    if-ge v2, v3, :cond_5

    :cond_4
    move v0, v1

    .line 243455
    :cond_5
    if-eqz v0, :cond_6

    .line 243456
    iget-boolean v0, p3, LX/1P3;->c:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->d()I

    move-result v0

    :goto_3
    iput v0, p3, LX/1P3;->b:I

    :cond_6
    move v0, v1

    .line 243457
    goto :goto_0

    .line 243458
    :cond_7
    invoke-static {p0, p1, p2}, LX/1P1;->e(LX/1P1;LX/1Od;LX/1Ok;)Landroid/view/View;

    move-result-object v2

    goto :goto_2

    .line 243459
    :cond_8
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->c()I

    move-result v0

    goto :goto_3

    .line 243460
    :cond_9
    invoke-static {v2}, LX/1OR;->d(Landroid/view/View;)I

    move-result v3

    iput v3, p3, LX/1P3;->a:I

    .line 243461
    iget-boolean v3, p3, LX/1P3;->c:Z

    if-eqz v3, :cond_a

    .line 243462
    iget-object v3, p3, LX/1P3;->d:LX/1P1;

    iget-object v3, v3, LX/1P1;->k:LX/1P5;

    invoke-virtual {v3}, LX/1P5;->d()I

    move-result v3

    sub-int v0, v3, v0

    .line 243463
    iget-object v3, p3, LX/1P3;->d:LX/1P1;

    iget-object v3, v3, LX/1P1;->k:LX/1P5;

    invoke-virtual {v3, v2}, LX/1P5;->b(Landroid/view/View;)I

    move-result v3

    .line 243464
    sub-int/2addr v0, v3

    .line 243465
    iget-object v3, p3, LX/1P3;->d:LX/1P1;

    iget-object v3, v3, LX/1P1;->k:LX/1P5;

    invoke-virtual {v3}, LX/1P5;->d()I

    move-result v3

    sub-int/2addr v3, v0

    iput v3, p3, LX/1P3;->b:I

    .line 243466
    if-lez v0, :cond_2

    .line 243467
    iget-object v3, p3, LX/1P3;->d:LX/1P1;

    iget-object v3, v3, LX/1P1;->k:LX/1P5;

    invoke-virtual {v3, v2}, LX/1P5;->c(Landroid/view/View;)I

    move-result v3

    .line 243468
    iget v4, p3, LX/1P3;->b:I

    sub-int v3, v4, v3

    .line 243469
    iget-object v4, p3, LX/1P3;->d:LX/1P1;

    iget-object v4, v4, LX/1P1;->k:LX/1P5;

    invoke-virtual {v4}, LX/1P5;->c()I

    move-result v4

    .line 243470
    iget-object p0, p3, LX/1P3;->d:LX/1P1;

    iget-object p0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {p0, v2}, LX/1P5;->a(Landroid/view/View;)I

    move-result p0

    sub-int/2addr p0, v4

    .line 243471
    invoke-static {p0, p1}, Ljava/lang/Math;->min(II)I

    move-result p0

    add-int/2addr v4, p0

    .line 243472
    sub-int/2addr v3, v4

    .line 243473
    if-gez v3, :cond_2

    .line 243474
    iget v4, p3, LX/1P3;->b:I

    neg-int v3, v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/2addr v0, v4

    iput v0, p3, LX/1P3;->b:I

    goto/16 :goto_1

    .line 243475
    :cond_a
    iget-object v3, p3, LX/1P3;->d:LX/1P1;

    iget-object v3, v3, LX/1P1;->k:LX/1P5;

    invoke-virtual {v3, v2}, LX/1P5;->a(Landroid/view/View;)I

    move-result v3

    .line 243476
    iget-object v4, p3, LX/1P3;->d:LX/1P1;

    iget-object v4, v4, LX/1P1;->k:LX/1P5;

    invoke-virtual {v4}, LX/1P5;->c()I

    move-result v4

    sub-int v4, v3, v4

    .line 243477
    iput v3, p3, LX/1P3;->b:I

    .line 243478
    if-lez v4, :cond_2

    .line 243479
    iget-object p0, p3, LX/1P3;->d:LX/1P1;

    iget-object p0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {p0, v2}, LX/1P5;->c(Landroid/view/View;)I

    move-result p0

    add-int/2addr v3, p0

    .line 243480
    iget-object p0, p3, LX/1P3;->d:LX/1P1;

    iget-object p0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {p0}, LX/1P5;->d()I

    move-result p0

    sub-int v0, p0, v0

    .line 243481
    iget-object p0, p3, LX/1P3;->d:LX/1P1;

    iget-object p0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {p0, v2}, LX/1P5;->b(Landroid/view/View;)I

    move-result p0

    sub-int/2addr v0, p0

    .line 243482
    iget-object p0, p3, LX/1P3;->d:LX/1P1;

    iget-object p0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {p0}, LX/1P5;->d()I

    move-result p0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v0, p0, v0

    .line 243483
    sub-int/2addr v0, v3

    .line 243484
    if-gez v0, :cond_2

    .line 243485
    iget v3, p3, LX/1P3;->b:I

    neg-int v0, v0

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v0, v3, v0

    iput v0, p3, LX/1P3;->b:I

    goto/16 :goto_1
.end method

.method private d(ILX/1Od;LX/1Ok;)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 243486
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move p1, v2

    .line 243487
    :goto_0
    return p1

    .line 243488
    :cond_1
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iput-boolean v1, v0, LX/1ZW;->a:Z

    .line 243489
    invoke-virtual {p0}, LX/1P1;->k()V

    .line 243490
    if-lez p1, :cond_2

    move v0, v1

    .line 243491
    :goto_1
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 243492
    invoke-direct {p0, v0, v3, v1, p3}, LX/1P1;->a(IIZLX/1Ok;)V

    .line 243493
    iget-object v1, p0, LX/1P1;->a:LX/1ZW;

    iget v1, v1, LX/1ZW;->g:I

    .line 243494
    iget-object v4, p0, LX/1P1;->a:LX/1ZW;

    invoke-static {p0, p2, v4, p3, v2}, LX/1P1;->a(LX/1P1;LX/1Od;LX/1ZW;LX/1Ok;Z)I

    move-result v4

    add-int/2addr v1, v4

    .line 243495
    if-gez v1, :cond_3

    move p1, v2

    .line 243496
    goto :goto_0

    .line 243497
    :cond_2
    const/4 v0, -0x1

    goto :goto_1

    .line 243498
    :cond_3
    if-le v3, v1, :cond_4

    mul-int p1, v0, v1

    .line 243499
    :cond_4
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    neg-int v1, p1

    invoke-virtual {v0, v1}, LX/1P5;->a(I)V

    .line 243500
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iput p1, v0, LX/1ZW;->j:I

    goto :goto_0
.end method

.method private static d(LX/1P1;LX/1Od;LX/1Ok;)Landroid/view/View;
    .locals 1

    .prologue
    .line 243501
    iget-boolean v0, p0, LX/1P1;->l:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, LX/1P1;->f(LX/1Od;LX/1Ok;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2}, LX/1P1;->g(LX/1Od;LX/1Ok;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private static e(LX/1P1;LX/1Od;LX/1Ok;)Landroid/view/View;
    .locals 1

    .prologue
    .line 243502
    iget-boolean v0, p0, LX/1P1;->l:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, LX/1P1;->g(LX/1Od;LX/1Ok;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2}, LX/1P1;->f(LX/1Od;LX/1Ok;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private f(LX/1Od;LX/1Ok;)Landroid/view/View;
    .locals 6

    .prologue
    .line 243503
    const/4 v3, 0x0

    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v4

    invoke-virtual {p2}, LX/1Ok;->e()I

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, LX/1P1;->a(LX/1Od;LX/1Ok;III)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static f(LX/1P1;II)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 243504
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iget-object v2, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->d()I

    move-result v2

    sub-int/2addr v2, p2

    iput v2, v0, LX/1ZW;->c:I

    .line 243505
    iget-object v2, p0, LX/1P1;->a:LX/1ZW;

    iget-boolean v0, p0, LX/1P1;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    iput v0, v2, LX/1ZW;->e:I

    .line 243506
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iput p1, v0, LX/1ZW;->d:I

    .line 243507
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iput v1, v0, LX/1ZW;->f:I

    .line 243508
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iput p2, v0, LX/1ZW;->b:I

    .line 243509
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    const/high16 v1, -0x80000000

    iput v1, v0, LX/1ZW;->g:I

    .line 243510
    return-void

    :cond_0
    move v0, v1

    .line 243511
    goto :goto_0
.end method

.method private g(LX/1Od;LX/1Ok;)Landroid/view/View;
    .locals 6

    .prologue
    .line 243512
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    add-int/lit8 v3, v0, -0x1

    const/4 v4, -0x1

    invoke-virtual {p2}, LX/1Ok;->e()I

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, LX/1P1;->a(LX/1Od;LX/1Ok;III)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static g(LX/1P1;II)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 243513
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iget-object v2, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->c()I

    move-result v2

    sub-int v2, p2, v2

    iput v2, v0, LX/1ZW;->c:I

    .line 243514
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iput p1, v0, LX/1ZW;->d:I

    .line 243515
    iget-object v2, p0, LX/1P1;->a:LX/1ZW;

    iget-boolean v0, p0, LX/1P1;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, v2, LX/1ZW;->e:I

    .line 243516
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iput v1, v0, LX/1ZW;->f:I

    .line 243517
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iput p2, v0, LX/1ZW;->b:I

    .line 243518
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    const/high16 v1, -0x80000000

    iput v1, v0, LX/1ZW;->g:I

    .line 243519
    return-void

    :cond_0
    move v0, v1

    .line 243520
    goto :goto_0
.end method

.method private h(LX/1Ok;)I
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 243521
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    if-nez v0, :cond_0

    .line 243522
    :goto_0
    return v4

    .line 243523
    :cond_0
    invoke-virtual {p0}, LX/1P1;->k()V

    .line 243524
    iget-object v1, p0, LX/1P1;->k:LX/1P5;

    iget-boolean v0, p0, LX/1P1;->e:Z

    if-nez v0, :cond_2

    move v0, v3

    :goto_1
    invoke-direct {p0, v0, v3}, LX/1P1;->a(ZZ)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, LX/1P1;->e:Z

    if-nez v0, :cond_1

    move v4, v3

    :cond_1
    invoke-direct {p0, v4, v3}, LX/1P1;->b(ZZ)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, LX/1P1;->e:Z

    iget-boolean v6, p0, LX/1P1;->l:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v6}, LX/2fZ;->a(LX/1Ok;LX/1P5;Landroid/view/View;Landroid/view/View;LX/1OR;ZZ)I

    move-result v4

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1
.end method

.method private i(LX/1Ok;)I
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 243140
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    if-nez v0, :cond_0

    .line 243141
    :goto_0
    return v4

    .line 243142
    :cond_0
    invoke-virtual {p0}, LX/1P1;->k()V

    .line 243143
    iget-object v1, p0, LX/1P1;->k:LX/1P5;

    iget-boolean v0, p0, LX/1P1;->e:Z

    if-nez v0, :cond_2

    move v0, v3

    :goto_1
    invoke-direct {p0, v0, v3}, LX/1P1;->a(ZZ)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, LX/1P1;->e:Z

    if-nez v0, :cond_1

    move v4, v3

    :cond_1
    invoke-direct {p0, v4, v3}, LX/1P1;->b(ZZ)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, LX/1P1;->e:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v5}, LX/2fZ;->a(LX/1Ok;LX/1P5;Landroid/view/View;Landroid/view/View;LX/1OR;Z)I

    move-result v4

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1
.end method

.method private j(LX/1Ok;)I
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 243525
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    if-nez v0, :cond_0

    .line 243526
    :goto_0
    return v4

    .line 243527
    :cond_0
    invoke-virtual {p0}, LX/1P1;->k()V

    .line 243528
    iget-object v1, p0, LX/1P1;->k:LX/1P5;

    iget-boolean v0, p0, LX/1P1;->e:Z

    if-nez v0, :cond_2

    move v0, v3

    :goto_1
    invoke-direct {p0, v0, v3}, LX/1P1;->a(ZZ)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, LX/1P1;->e:Z

    if-nez v0, :cond_1

    move v4, v3

    :cond_1
    invoke-direct {p0, v4, v3}, LX/1P1;->b(ZZ)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, LX/1P1;->e:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v5}, LX/2fZ;->b(LX/1Ok;LX/1P5;Landroid/view/View;Landroid/view/View;LX/1OR;Z)I

    move-result v4

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1
.end method


# virtual methods
.method public final a(ILX/1Od;LX/1Ok;)I
    .locals 2

    .prologue
    .line 243529
    iget v0, p0, LX/1P1;->j:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 243530
    const/4 v0, 0x0

    .line 243531
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, LX/1P1;->d(ILX/1Od;LX/1Ok;)I

    move-result v0

    goto :goto_0
.end method

.method public a(LX/1Ok;)I
    .locals 1

    .prologue
    .line 243532
    invoke-virtual {p1}, LX/1Ok;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243533
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->f()I

    move-result v0

    .line 243534
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LX/1Od;LX/1Ok;III)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 243535
    invoke-virtual {p0}, LX/1P1;->k()V

    .line 243536
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->c()I

    move-result v5

    .line 243537
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->d()I

    move-result v6

    .line 243538
    if-le p4, p3, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    move-object v4, v2

    .line 243539
    :goto_1
    if-eq p3, p4, :cond_3

    .line 243540
    invoke-virtual {p0, p3}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v3

    .line 243541
    invoke-static {v3}, LX/1OR;->d(Landroid/view/View;)I

    move-result v0

    .line 243542
    if-ltz v0, :cond_6

    if-ge v0, p5, :cond_6

    .line 243543
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    invoke-virtual {v0}, LX/1a3;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243544
    if-nez v4, :cond_6

    move-object v0, v2

    .line 243545
    :goto_2
    add-int/2addr p3, v1

    move-object v2, v0

    move-object v4, v3

    goto :goto_1

    .line 243546
    :cond_0
    const/4 v0, -0x1

    move v1, v0

    goto :goto_0

    .line 243547
    :cond_1
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0, v3}, LX/1P5;->a(Landroid/view/View;)I

    move-result v0

    if-ge v0, v6, :cond_2

    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0, v3}, LX/1P5;->b(Landroid/view/View;)I

    move-result v0

    if-ge v0, v5, :cond_4

    .line 243548
    :cond_2
    if-nez v2, :cond_6

    move-object v0, v3

    move-object v3, v4

    .line 243549
    goto :goto_2

    .line 243550
    :cond_3
    if-eqz v2, :cond_5

    move-object v3, v2

    :cond_4
    :goto_3
    return-object v3

    :cond_5
    move-object v3, v4

    goto :goto_3

    :cond_6
    move-object v0, v2

    move-object v3, v4

    goto :goto_2
.end method

.method public a(LX/1Od;LX/1Ok;LX/1P3;)V
    .locals 0

    .prologue
    .line 243551
    return-void
.end method

.method public a(LX/1Od;LX/1Ok;LX/1ZW;LX/1Zz;)V
    .locals 8

    .prologue
    const/4 v4, -0x1

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 243341
    invoke-virtual {p3, p1}, LX/1ZW;->a(LX/1Od;)Landroid/view/View;

    move-result-object v1

    .line 243342
    if-nez v1, :cond_0

    .line 243343
    iput-boolean v7, p4, LX/1Zz;->b:Z

    .line 243344
    :goto_0
    return-void

    .line 243345
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/1a3;

    .line 243346
    iget-object v0, p3, LX/1ZW;->k:Ljava/util/List;

    if-nez v0, :cond_5

    .line 243347
    iget-boolean v3, p0, LX/1P1;->l:Z

    iget v0, p3, LX/1ZW;->f:I

    if-ne v0, v4, :cond_3

    move v0, v7

    :goto_1
    if-ne v3, v0, :cond_4

    .line 243348
    invoke-virtual {p0, v1}, LX/1OR;->b(Landroid/view/View;)V

    .line 243349
    :goto_2
    invoke-virtual {p0, v1, v2, v2}, LX/1OR;->a(Landroid/view/View;II)V

    .line 243350
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0, v1}, LX/1P5;->c(Landroid/view/View;)I

    move-result v0

    iput v0, p4, LX/1Zz;->a:I

    .line 243351
    iget v0, p0, LX/1P1;->j:I

    if-ne v0, v7, :cond_a

    .line 243352
    invoke-virtual {p0}, LX/1P1;->j()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 243353
    invoke-virtual {p0}, LX/1OR;->w()I

    move-result v0

    invoke-virtual {p0}, LX/1OR;->A()I

    move-result v2

    sub-int v2, v0, v2

    .line 243354
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0, v1}, LX/1P5;->d(Landroid/view/View;)I

    move-result v0

    sub-int v0, v2, v0

    .line 243355
    :goto_3
    iget v3, p3, LX/1ZW;->f:I

    if-ne v3, v4, :cond_9

    .line 243356
    iget v4, p3, LX/1ZW;->b:I

    .line 243357
    iget v3, p3, LX/1ZW;->b:I

    iget v5, p4, LX/1Zz;->a:I

    sub-int/2addr v3, v5

    move v5, v4

    move v4, v2

    move v2, v0

    move v0, v3

    .line 243358
    :goto_4
    iget v3, v6, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v2, v3

    iget v3, v6, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v3, v0

    iget v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v4, v0

    iget v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int/2addr v5, v0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/1OR;->a(Landroid/view/View;IIII)V

    .line 243359
    invoke-virtual {v6}, LX/1a3;->c()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v6}, LX/1a3;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 243360
    :cond_1
    iput-boolean v7, p4, LX/1Zz;->c:Z

    .line 243361
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->isFocusable()Z

    move-result v0

    iput-boolean v0, p4, LX/1Zz;->d:Z

    goto :goto_0

    :cond_3
    move v0, v2

    .line 243362
    goto :goto_1

    .line 243363
    :cond_4
    invoke-virtual {p0, v1, v2}, LX/1OR;->b(Landroid/view/View;I)V

    goto :goto_2

    .line 243364
    :cond_5
    iget-boolean v3, p0, LX/1P1;->l:Z

    iget v0, p3, LX/1ZW;->f:I

    if-ne v0, v4, :cond_6

    move v0, v7

    :goto_5
    if-ne v3, v0, :cond_7

    .line 243365
    invoke-virtual {p0, v1}, LX/1OR;->a(Landroid/view/View;)V

    goto :goto_2

    :cond_6
    move v0, v2

    .line 243366
    goto :goto_5

    .line 243367
    :cond_7
    invoke-virtual {p0, v1, v2}, LX/1OR;->a(Landroid/view/View;I)V

    goto :goto_2

    .line 243368
    :cond_8
    invoke-virtual {p0}, LX/1OR;->y()I

    move-result v0

    .line 243369
    iget-object v2, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v2, v1}, LX/1P5;->d(Landroid/view/View;)I

    move-result v2

    add-int/2addr v2, v0

    goto :goto_3

    .line 243370
    :cond_9
    iget v3, p3, LX/1ZW;->b:I

    .line 243371
    iget v4, p3, LX/1ZW;->b:I

    iget v5, p4, LX/1Zz;->a:I

    add-int/2addr v4, v5

    move v5, v4

    move v4, v2

    move v2, v0

    move v0, v3

    goto :goto_4

    .line 243372
    :cond_a
    invoke-virtual {p0}, LX/1OR;->z()I

    move-result v0

    .line 243373
    iget-object v2, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v2, v1}, LX/1P5;->d(Landroid/view/View;)I

    move-result v2

    add-int/2addr v2, v0

    .line 243374
    iget v3, p3, LX/1ZW;->f:I

    if-ne v3, v4, :cond_b

    .line 243375
    iget v4, p3, LX/1ZW;->b:I

    .line 243376
    iget v3, p3, LX/1ZW;->b:I

    iget v5, p4, LX/1Zz;->a:I

    sub-int/2addr v3, v5

    move v5, v2

    move v2, v3

    goto :goto_4

    .line 243377
    :cond_b
    iget v3, p3, LX/1ZW;->b:I

    .line 243378
    iget v4, p3, LX/1ZW;->b:I

    iget v5, p4, LX/1Zz;->a:I

    add-int/2addr v4, v5

    move v5, v2

    move v2, v3

    goto :goto_4
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 243434
    instance-of v0, p1, Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-eqz v0, :cond_0

    .line 243435
    check-cast p1, Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iput-object p1, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    .line 243436
    invoke-virtual {p0}, LX/1OR;->p()V

    .line 243437
    :cond_0
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;LX/1Od;)V
    .locals 1

    .prologue
    .line 243081
    invoke-super {p0, p1, p2}, LX/1OR;->a(Landroid/support/v7/widget/RecyclerView;LX/1Od;)V

    .line 243082
    iget-boolean v0, p0, LX/1P1;->f:Z

    if-eqz v0, :cond_0

    .line 243083
    invoke-virtual {p0, p2}, LX/1OR;->c(LX/1Od;)V

    .line 243084
    invoke-virtual {p2}, LX/1Od;->a()V

    .line 243085
    :cond_0
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;LX/1Ok;I)V
    .locals 2

    .prologue
    .line 243176
    new-instance v0, LX/3ww;

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/3ww;-><init>(LX/1P1;Landroid/content/Context;)V

    .line 243177
    iput p3, v0, LX/25Y;->a:I

    .line 243178
    invoke-virtual {p0, v0}, LX/1OR;->a(LX/25Y;)V

    .line 243179
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, -0x1

    .line 243161
    const-string v0, "Cannot drop a view during a scroll or layout calculation"

    invoke-virtual {p0, v0}, LX/1OR;->a(Ljava/lang/String;)V

    .line 243162
    invoke-virtual {p0}, LX/1P1;->k()V

    .line 243163
    invoke-direct {p0}, LX/1P1;->c()V

    .line 243164
    invoke-static {p1}, LX/1OR;->d(Landroid/view/View;)I

    move-result v0

    .line 243165
    invoke-static {p2}, LX/1OR;->d(Landroid/view/View;)I

    move-result v3

    .line 243166
    if-ge v0, v3, :cond_0

    move v0, v1

    .line 243167
    :goto_0
    iget-boolean v4, p0, LX/1P1;->l:Z

    if-eqz v4, :cond_2

    .line 243168
    if-ne v0, v1, :cond_1

    .line 243169
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->d()I

    move-result v0

    iget-object v1, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v1, p2}, LX/1P5;->a(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v2, p1}, LX/1P5;->c(Landroid/view/View;)I

    move-result v2

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    invoke-virtual {p0, v3, v0}, LX/1P1;->d(II)V

    .line 243170
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 243171
    goto :goto_0

    .line 243172
    :cond_1
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->d()I

    move-result v0

    iget-object v1, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v1, p2}, LX/1P5;->b(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v3, v0}, LX/1P1;->d(II)V

    goto :goto_1

    .line 243173
    :cond_2
    if-ne v0, v2, :cond_3

    .line 243174
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0, p2}, LX/1P5;->a(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, v3, v0}, LX/1P1;->d(II)V

    goto :goto_1

    .line 243175
    :cond_3
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0, p2}, LX/1P5;->b(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v1, p1}, LX/1P5;->c(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v3, v0}, LX/1P1;->d(II)V

    goto :goto_1
.end method

.method public final a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 243155
    invoke-super {p0, p1}, LX/1OR;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 243156
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    if-lez v0, :cond_0

    .line 243157
    invoke-static {p1}, LX/2fV;->a(Landroid/view/accessibility/AccessibilityEvent;)LX/2fO;

    move-result-object v0

    .line 243158
    invoke-virtual {p0}, LX/1P1;->l()I

    move-result v1

    invoke-virtual {v0, v1}, LX/2fO;->b(I)V

    .line 243159
    invoke-virtual {p0}, LX/1P1;->n()I

    move-result v1

    invoke-virtual {v0, v1}, LX/2fO;->c(I)V

    .line 243160
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 243152
    iget-object v0, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-nez v0, :cond_0

    .line 243153
    invoke-super {p0, p1}, LX/1OR;->a(Ljava/lang/String;)V

    .line 243154
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 243147
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/1OR;->a(Ljava/lang/String;)V

    .line 243148
    iget-boolean v0, p0, LX/1P1;->d:Z

    if-ne v0, p1, :cond_0

    .line 243149
    :goto_0
    return-void

    .line 243150
    :cond_0
    iput-boolean p1, p0, LX/1P1;->d:Z

    .line 243151
    invoke-virtual {p0}, LX/1OR;->p()V

    goto :goto_0
.end method

.method public b(ILX/1Od;LX/1Ok;)I
    .locals 1

    .prologue
    .line 243144
    iget v0, p0, LX/1P1;->j:I

    if-nez v0, :cond_0

    .line 243145
    const/4 v0, 0x0

    .line 243146
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, LX/1P1;->d(ILX/1Od;LX/1Ok;)I

    move-result v0

    goto :goto_0
.end method

.method public final b(LX/1Ok;)I
    .locals 1

    .prologue
    .line 243139
    invoke-direct {p0, p1}, LX/1P1;->h(LX/1Ok;)I

    move-result v0

    return v0
.end method

.method public b()LX/1a3;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 243138
    new-instance v0, LX/1a3;

    invoke-direct {v0, v1, v1}, LX/1a3;-><init>(II)V

    return-object v0
.end method

.method public b(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 243130
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 243131
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid orientation:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 243132
    :cond_0
    invoke-virtual {p0, v1}, LX/1OR;->a(Ljava/lang/String;)V

    .line 243133
    iget v0, p0, LX/1P1;->j:I

    if-ne p1, v0, :cond_1

    .line 243134
    :goto_0
    return-void

    .line 243135
    :cond_1
    iput p1, p0, LX/1P1;->j:I

    .line 243136
    iput-object v1, p0, LX/1P1;->k:LX/1P5;

    .line 243137
    invoke-virtual {p0}, LX/1OR;->p()V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 243125
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/1OR;->a(Ljava/lang/String;)V

    .line 243126
    iget-boolean v0, p0, LX/1P1;->c:Z

    if-ne p1, v0, :cond_0

    .line 243127
    :goto_0
    return-void

    .line 243128
    :cond_0
    iput-boolean p1, p0, LX/1P1;->c:Z

    .line 243129
    invoke-virtual {p0}, LX/1OR;->p()V

    goto :goto_0
.end method

.method public c(LX/1Ok;)I
    .locals 1

    .prologue
    .line 243124
    invoke-direct {p0, p1}, LX/1P1;->h(LX/1Ok;)I

    move-result v0

    return v0
.end method

.method public final c(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 243114
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    .line 243115
    if-nez v0, :cond_1

    .line 243116
    const/4 v0, 0x0

    .line 243117
    :cond_0
    :goto_0
    return-object v0

    .line 243118
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, LX/1OR;->d(Landroid/view/View;)I

    move-result v1

    .line 243119
    sub-int v1, p1, v1

    .line 243120
    if-ltz v1, :cond_2

    if-ge v1, v0, :cond_2

    .line 243121
    invoke-virtual {p0, v1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v0

    .line 243122
    invoke-static {v0}, LX/1OR;->d(Landroid/view/View;)I

    move-result v1

    if-eq v1, p1, :cond_0

    .line 243123
    :cond_2
    invoke-super {p0, p1}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(ILX/1Od;LX/1Ok;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    const/high16 v5, -0x80000000

    const/4 v0, 0x0

    .line 243086
    invoke-direct {p0}, LX/1P1;->c()V

    .line 243087
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v1

    if-nez v1, :cond_1

    .line 243088
    :cond_0
    :goto_0
    return-object v0

    .line 243089
    :cond_1
    const/4 v1, -0x1

    const/4 v2, 0x1

    const/high16 v3, -0x80000000

    .line 243090
    sparse-switch p1, :sswitch_data_0

    move v1, v3

    .line 243091
    :cond_2
    :goto_1
    :sswitch_0
    move v3, v1

    .line 243092
    if-eq v3, v5, :cond_0

    .line 243093
    invoke-virtual {p0}, LX/1P1;->k()V

    .line 243094
    if-ne v3, v6, :cond_3

    .line 243095
    invoke-static {p0, p2, p3}, LX/1P1;->e(LX/1P1;LX/1Od;LX/1Ok;)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    .line 243096
    :goto_2
    if-eqz v2, :cond_0

    .line 243097
    invoke-virtual {p0}, LX/1P1;->k()V

    .line 243098
    const v1, 0x3ea8f5c3    # 0.33f

    iget-object v4, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v4}, LX/1P5;->f()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v1, v4

    float-to-int v1, v1

    .line 243099
    invoke-direct {p0, v3, v1, v7, p3}, LX/1P1;->a(IIZLX/1Ok;)V

    .line 243100
    iget-object v1, p0, LX/1P1;->a:LX/1ZW;

    iput v5, v1, LX/1ZW;->g:I

    .line 243101
    iget-object v1, p0, LX/1P1;->a:LX/1ZW;

    iput-boolean v7, v1, LX/1ZW;->a:Z

    .line 243102
    iget-object v1, p0, LX/1P1;->a:LX/1ZW;

    const/4 v4, 0x1

    invoke-static {p0, p2, v1, p3, v4}, LX/1P1;->a(LX/1P1;LX/1Od;LX/1ZW;LX/1Ok;Z)I

    .line 243103
    if-ne v3, v6, :cond_4

    .line 243104
    invoke-static {p0}, LX/1P1;->I(LX/1P1;)Landroid/view/View;

    move-result-object v1

    .line 243105
    :goto_3
    if-eq v1, v2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 243106
    goto :goto_0

    .line 243107
    :cond_3
    invoke-static {p0, p2, p3}, LX/1P1;->d(LX/1P1;LX/1Od;LX/1Ok;)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    goto :goto_2

    .line 243108
    :cond_4
    invoke-static {p0}, LX/1P1;->J(LX/1P1;)Landroid/view/View;

    move-result-object v1

    goto :goto_3

    :sswitch_1
    move v1, v2

    .line 243109
    goto :goto_1

    .line 243110
    :sswitch_2
    iget v4, p0, LX/1P1;->j:I

    if-eq v4, v2, :cond_2

    move v1, v3

    goto :goto_1

    .line 243111
    :sswitch_3
    iget v1, p0, LX/1P1;->j:I

    if-ne v1, v2, :cond_5

    move v1, v2

    goto :goto_1

    :cond_5
    move v1, v3

    goto :goto_1

    .line 243112
    :sswitch_4
    iget v2, p0, LX/1P1;->j:I

    if-eqz v2, :cond_2

    move v1, v3

    goto :goto_1

    .line 243113
    :sswitch_5
    iget v1, p0, LX/1P1;->j:I

    if-nez v1, :cond_6

    move v1, v2

    goto :goto_1

    :cond_6
    move v1, v3

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x11 -> :sswitch_4
        0x21 -> :sswitch_2
        0x42 -> :sswitch_5
        0x82 -> :sswitch_3
    .end sparse-switch
.end method

.method public c(LX/1Od;LX/1Ok;)V
    .locals 13

    .prologue
    const/4 v8, 0x1

    const/high16 v7, -0x80000000

    const/4 v6, -0x1

    const/4 v1, 0x0

    .line 242909
    iget-object v0, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-nez v0, :cond_0

    iget v0, p0, LX/1P1;->m:I

    if-eq v0, v6, :cond_1

    .line 242910
    :cond_0
    invoke-virtual {p2}, LX/1Ok;->e()I

    move-result v0

    if-nez v0, :cond_1

    .line 242911
    invoke-virtual {p0, p1}, LX/1OR;->c(LX/1Od;)V

    .line 242912
    :goto_0
    return-void

    .line 242913
    :cond_1
    iget-object v0, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 242914
    iget-object v0, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->a:I

    iput v0, p0, LX/1P1;->m:I

    .line 242915
    :cond_2
    invoke-virtual {p0}, LX/1P1;->k()V

    .line 242916
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iput-boolean v1, v0, LX/1ZW;->a:Z

    .line 242917
    invoke-direct {p0}, LX/1P1;->c()V

    .line 242918
    iget-object v0, p0, LX/1P1;->p:LX/1P3;

    .line 242919
    const/4 v2, -0x1

    iput v2, v0, LX/1P3;->a:I

    .line 242920
    const/high16 v2, -0x80000000

    iput v2, v0, LX/1P3;->b:I

    .line 242921
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/1P3;->c:Z

    .line 242922
    iget-object v0, p0, LX/1P1;->p:LX/1P3;

    iget-boolean v2, p0, LX/1P1;->l:Z

    iget-boolean v3, p0, LX/1P1;->d:Z

    xor-int/2addr v2, v3

    iput-boolean v2, v0, LX/1P3;->c:Z

    .line 242923
    iget-object v0, p0, LX/1P1;->p:LX/1P3;

    .line 242924
    const/4 v10, -0x1

    const/high16 v9, -0x80000000

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 242925
    iget-boolean v2, p2, LX/1Ok;->k:Z

    move v2, v2

    .line 242926
    if-nez v2, :cond_3

    iget v2, p0, LX/1P1;->m:I

    if-ne v2, v10, :cond_14

    :cond_3
    move v3, v4

    .line 242927
    :goto_1
    move v2, v3

    .line 242928
    if-eqz v2, :cond_12

    .line 242929
    :cond_4
    :goto_2
    invoke-virtual {p0, p2}, LX/1P1;->a(LX/1Ok;)I

    move-result v0

    .line 242930
    iget-object v2, p0, LX/1P1;->a:LX/1ZW;

    iget v2, v2, LX/1ZW;->j:I

    if-ltz v2, :cond_b

    move v2, v0

    move v0, v1

    .line 242931
    :goto_3
    iget-object v3, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v3}, LX/1P5;->c()I

    move-result v3

    add-int/2addr v0, v3

    .line 242932
    iget-object v3, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v3}, LX/1P5;->g()I

    move-result v3

    add-int/2addr v2, v3

    .line 242933
    iget-boolean v3, p2, LX/1Ok;->k:Z

    move v3, v3

    .line 242934
    if-eqz v3, :cond_5

    iget v3, p0, LX/1P1;->m:I

    if-eq v3, v6, :cond_5

    iget v3, p0, LX/1P1;->n:I

    if-eq v3, v7, :cond_5

    .line 242935
    iget v3, p0, LX/1P1;->m:I

    invoke-virtual {p0, v3}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v3

    .line 242936
    if-eqz v3, :cond_5

    .line 242937
    iget-boolean v4, p0, LX/1P1;->l:Z

    if-eqz v4, :cond_c

    .line 242938
    iget-object v4, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v4}, LX/1P5;->d()I

    move-result v4

    iget-object v5, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v5, v3}, LX/1P5;->b(Landroid/view/View;)I

    move-result v3

    sub-int v3, v4, v3

    .line 242939
    iget v4, p0, LX/1P1;->n:I

    sub-int/2addr v3, v4

    .line 242940
    :goto_4
    if-lez v3, :cond_d

    .line 242941
    add-int/2addr v0, v3

    .line 242942
    :cond_5
    :goto_5
    iget-object v3, p0, LX/1P1;->p:LX/1P3;

    invoke-virtual {p0, p1, p2, v3}, LX/1P1;->a(LX/1Od;LX/1Ok;LX/1P3;)V

    .line 242943
    invoke-virtual {p0, p1}, LX/1OR;->a(LX/1Od;)V

    .line 242944
    iget-object v3, p0, LX/1P1;->a:LX/1ZW;

    .line 242945
    iget-boolean v4, p2, LX/1Ok;->k:Z

    move v4, v4

    .line 242946
    iput-boolean v4, v3, LX/1ZW;->i:Z

    .line 242947
    iget-object v3, p0, LX/1P1;->p:LX/1P3;

    iget-boolean v3, v3, LX/1P3;->c:Z

    if-eqz v3, :cond_e

    .line 242948
    iget-object v3, p0, LX/1P1;->p:LX/1P3;

    invoke-direct {p0, v3}, LX/1P1;->b(LX/1P3;)V

    .line 242949
    iget-object v3, p0, LX/1P1;->a:LX/1ZW;

    iput v0, v3, LX/1ZW;->h:I

    .line 242950
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    invoke-static {p0, p1, v0, p2, v1}, LX/1P1;->a(LX/1P1;LX/1Od;LX/1ZW;LX/1Ok;Z)I

    .line 242951
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iget v0, v0, LX/1ZW;->b:I

    .line 242952
    iget-object v3, p0, LX/1P1;->a:LX/1ZW;

    iget v3, v3, LX/1ZW;->d:I

    .line 242953
    iget-object v4, p0, LX/1P1;->a:LX/1ZW;

    iget v4, v4, LX/1ZW;->c:I

    if-lez v4, :cond_6

    .line 242954
    iget-object v4, p0, LX/1P1;->a:LX/1ZW;

    iget v4, v4, LX/1ZW;->c:I

    add-int/2addr v2, v4

    .line 242955
    :cond_6
    iget-object v4, p0, LX/1P1;->p:LX/1P3;

    invoke-direct {p0, v4}, LX/1P1;->a(LX/1P3;)V

    .line 242956
    iget-object v4, p0, LX/1P1;->a:LX/1ZW;

    iput v2, v4, LX/1ZW;->h:I

    .line 242957
    iget-object v2, p0, LX/1P1;->a:LX/1ZW;

    iget v4, v2, LX/1ZW;->d:I

    iget-object v5, p0, LX/1P1;->a:LX/1ZW;

    iget v5, v5, LX/1ZW;->e:I

    add-int/2addr v4, v5

    iput v4, v2, LX/1ZW;->d:I

    .line 242958
    iget-object v2, p0, LX/1P1;->a:LX/1ZW;

    invoke-static {p0, p1, v2, p2, v1}, LX/1P1;->a(LX/1P1;LX/1Od;LX/1ZW;LX/1Ok;Z)I

    .line 242959
    iget-object v2, p0, LX/1P1;->a:LX/1ZW;

    iget v2, v2, LX/1ZW;->b:I

    .line 242960
    iget-object v4, p0, LX/1P1;->a:LX/1ZW;

    iget v4, v4, LX/1ZW;->c:I

    if-lez v4, :cond_7

    .line 242961
    iget-object v4, p0, LX/1P1;->a:LX/1ZW;

    iget v4, v4, LX/1ZW;->c:I

    .line 242962
    invoke-static {p0, v3, v0}, LX/1P1;->g(LX/1P1;II)V

    .line 242963
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iput v4, v0, LX/1ZW;->h:I

    .line 242964
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    invoke-static {p0, p1, v0, p2, v1}, LX/1P1;->a(LX/1P1;LX/1Od;LX/1ZW;LX/1Ok;Z)I

    .line 242965
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iget v0, v0, LX/1ZW;->b:I

    :cond_7
    move v9, v2

    move v2, v0

    move v0, v9

    .line 242966
    :goto_6
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v3

    if-lez v3, :cond_8

    .line 242967
    iget-boolean v3, p0, LX/1P1;->l:Z

    iget-boolean v4, p0, LX/1P1;->d:Z

    xor-int/2addr v3, v4

    if-eqz v3, :cond_10

    .line 242968
    invoke-direct {p0, v0, p1, p2, v8}, LX/1P1;->a(ILX/1Od;LX/1Ok;Z)I

    move-result v3

    .line 242969
    add-int/2addr v2, v3

    .line 242970
    add-int/2addr v0, v3

    .line 242971
    invoke-direct {p0, v2, p1, p2, v1}, LX/1P1;->b(ILX/1Od;LX/1Ok;Z)I

    move-result v1

    .line 242972
    add-int/2addr v2, v1

    .line 242973
    add-int/2addr v0, v1

    .line 242974
    :cond_8
    :goto_7
    iget-boolean v1, p2, LX/1Ok;->m:Z

    move v1, v1

    .line 242975
    if-eqz v1, :cond_9

    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v1

    if-eqz v1, :cond_9

    .line 242976
    iget-boolean v1, p2, LX/1Ok;->k:Z

    move v1, v1

    .line 242977
    if-nez v1, :cond_9

    invoke-virtual {p0}, LX/1OR;->e()Z

    move-result v1

    if-nez v1, :cond_23

    .line 242978
    :cond_9
    :goto_8
    iget-boolean v0, p2, LX/1Ok;->k:Z

    move v0, v0

    .line 242979
    if-nez v0, :cond_a

    .line 242980
    iput v6, p0, LX/1P1;->m:I

    .line 242981
    iput v7, p0, LX/1P1;->n:I

    .line 242982
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    .line 242983
    invoke-virtual {v0}, LX/1P5;->f()I

    move-result v1

    iput v1, v0, LX/1P5;->b:I

    .line 242984
    :cond_a
    iget-boolean v0, p0, LX/1P1;->d:Z

    iput-boolean v0, p0, LX/1P1;->b:Z

    .line 242985
    const/4 v0, 0x0

    iput-object v0, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    goto/16 :goto_0

    :cond_b
    move v2, v1

    .line 242986
    goto/16 :goto_3

    .line 242987
    :cond_c
    iget-object v4, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v4, v3}, LX/1P5;->a(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v4}, LX/1P5;->c()I

    move-result v4

    sub-int/2addr v3, v4

    .line 242988
    iget v4, p0, LX/1P1;->n:I

    sub-int v3, v4, v3

    goto/16 :goto_4

    .line 242989
    :cond_d
    sub-int/2addr v2, v3

    goto/16 :goto_5

    .line 242990
    :cond_e
    iget-object v3, p0, LX/1P1;->p:LX/1P3;

    invoke-direct {p0, v3}, LX/1P1;->a(LX/1P3;)V

    .line 242991
    iget-object v3, p0, LX/1P1;->a:LX/1ZW;

    iput v2, v3, LX/1ZW;->h:I

    .line 242992
    iget-object v2, p0, LX/1P1;->a:LX/1ZW;

    invoke-static {p0, p1, v2, p2, v1}, LX/1P1;->a(LX/1P1;LX/1Od;LX/1ZW;LX/1Ok;Z)I

    .line 242993
    iget-object v2, p0, LX/1P1;->a:LX/1ZW;

    iget v2, v2, LX/1ZW;->b:I

    .line 242994
    iget-object v3, p0, LX/1P1;->a:LX/1ZW;

    iget v4, v3, LX/1ZW;->d:I

    .line 242995
    iget-object v3, p0, LX/1P1;->a:LX/1ZW;

    iget v3, v3, LX/1ZW;->c:I

    if-lez v3, :cond_f

    .line 242996
    iget-object v3, p0, LX/1P1;->a:LX/1ZW;

    iget v3, v3, LX/1ZW;->c:I

    add-int/2addr v0, v3

    .line 242997
    :cond_f
    iget-object v3, p0, LX/1P1;->p:LX/1P3;

    invoke-direct {p0, v3}, LX/1P1;->b(LX/1P3;)V

    .line 242998
    iget-object v3, p0, LX/1P1;->a:LX/1ZW;

    iput v0, v3, LX/1ZW;->h:I

    .line 242999
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iget v3, v0, LX/1ZW;->d:I

    iget-object v5, p0, LX/1P1;->a:LX/1ZW;

    iget v5, v5, LX/1ZW;->e:I

    add-int/2addr v3, v5

    iput v3, v0, LX/1ZW;->d:I

    .line 243000
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    invoke-static {p0, p1, v0, p2, v1}, LX/1P1;->a(LX/1P1;LX/1Od;LX/1ZW;LX/1Ok;Z)I

    .line 243001
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iget v3, v0, LX/1ZW;->b:I

    .line 243002
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iget v0, v0, LX/1ZW;->c:I

    if-lez v0, :cond_11

    .line 243003
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iget v0, v0, LX/1ZW;->c:I

    .line 243004
    invoke-static {p0, v4, v2}, LX/1P1;->f(LX/1P1;II)V

    .line 243005
    iget-object v2, p0, LX/1P1;->a:LX/1ZW;

    iput v0, v2, LX/1ZW;->h:I

    .line 243006
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    invoke-static {p0, p1, v0, p2, v1}, LX/1P1;->a(LX/1P1;LX/1Od;LX/1ZW;LX/1Ok;Z)I

    .line 243007
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    iget v0, v0, LX/1ZW;->b:I

    move v2, v3

    goto/16 :goto_6

    .line 243008
    :cond_10
    invoke-direct {p0, v2, p1, p2, v8}, LX/1P1;->b(ILX/1Od;LX/1Ok;Z)I

    move-result v3

    .line 243009
    add-int/2addr v2, v3

    .line 243010
    add-int/2addr v0, v3

    .line 243011
    invoke-direct {p0, v0, p1, p2, v1}, LX/1P1;->a(ILX/1Od;LX/1Ok;Z)I

    move-result v1

    .line 243012
    add-int/2addr v2, v1

    .line 243013
    add-int/2addr v0, v1

    goto/16 :goto_7

    :cond_11
    move v0, v2

    move v2, v3

    goto/16 :goto_6

    .line 243014
    :cond_12
    invoke-static {p0, p1, p2, v0}, LX/1P1;->c(LX/1P1;LX/1Od;LX/1Ok;LX/1P3;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 243015
    invoke-virtual {v0}, LX/1P3;->b()V

    .line 243016
    iget-boolean v2, p0, LX/1P1;->d:Z

    if-eqz v2, :cond_13

    invoke-virtual {p2}, LX/1Ok;->e()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_9
    iput v2, v0, LX/1P3;->a:I

    goto/16 :goto_2

    :cond_13
    const/4 v2, 0x0

    goto :goto_9

    .line 243017
    :cond_14
    iget v2, p0, LX/1P1;->m:I

    if-ltz v2, :cond_15

    iget v2, p0, LX/1P1;->m:I

    invoke-virtual {p2}, LX/1Ok;->e()I

    move-result v5

    if-lt v2, v5, :cond_16

    .line 243018
    :cond_15
    iput v10, p0, LX/1P1;->m:I

    .line 243019
    iput v9, p0, LX/1P1;->n:I

    move v3, v4

    .line 243020
    goto/16 :goto_1

    .line 243021
    :cond_16
    iget v2, p0, LX/1P1;->m:I

    iput v2, v0, LX/1P3;->a:I

    .line 243022
    iget-object v2, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-eqz v2, :cond_18

    iget-object v2, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-virtual {v2}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->a()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 243023
    iget-object v2, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iget-boolean v2, v2, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->c:Z

    iput-boolean v2, v0, LX/1P3;->c:Z

    .line 243024
    iget-boolean v2, v0, LX/1P3;->c:Z

    if-eqz v2, :cond_17

    .line 243025
    iget-object v2, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->d()I

    move-result v2

    iget-object v4, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iget v4, v4, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->b:I

    sub-int/2addr v2, v4

    iput v2, v0, LX/1P3;->b:I

    goto/16 :goto_1

    .line 243026
    :cond_17
    iget-object v2, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->c()I

    move-result v2

    iget-object v4, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iget v4, v4, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->b:I

    add-int/2addr v2, v4

    iput v2, v0, LX/1P3;->b:I

    goto/16 :goto_1

    .line 243027
    :cond_18
    iget v2, p0, LX/1P1;->n:I

    if-ne v2, v9, :cond_21

    .line 243028
    iget v2, p0, LX/1P1;->m:I

    invoke-virtual {p0, v2}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v2

    .line 243029
    if-eqz v2, :cond_1d

    .line 243030
    iget-object v5, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v5, v2}, LX/1P5;->c(Landroid/view/View;)I

    move-result v5

    .line 243031
    iget-object v9, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v9}, LX/1P5;->f()I

    move-result v9

    if-le v5, v9, :cond_19

    .line 243032
    invoke-virtual {v0}, LX/1P3;->b()V

    goto/16 :goto_1

    .line 243033
    :cond_19
    iget-object v5, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v5, v2}, LX/1P5;->a(Landroid/view/View;)I

    move-result v5

    iget-object v9, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v9}, LX/1P5;->c()I

    move-result v9

    sub-int/2addr v5, v9

    .line 243034
    if-gez v5, :cond_1a

    .line 243035
    iget-object v2, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->c()I

    move-result v2

    iput v2, v0, LX/1P3;->b:I

    .line 243036
    iput-boolean v4, v0, LX/1P3;->c:Z

    goto/16 :goto_1

    .line 243037
    :cond_1a
    iget-object v4, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v4}, LX/1P5;->d()I

    move-result v4

    iget-object v5, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v5, v2}, LX/1P5;->b(Landroid/view/View;)I

    move-result v5

    sub-int/2addr v4, v5

    .line 243038
    if-gez v4, :cond_1b

    .line 243039
    iget-object v2, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->d()I

    move-result v2

    iput v2, v0, LX/1P3;->b:I

    .line 243040
    iput-boolean v3, v0, LX/1P3;->c:Z

    goto/16 :goto_1

    .line 243041
    :cond_1b
    iget-boolean v4, v0, LX/1P3;->c:Z

    if-eqz v4, :cond_1c

    iget-object v4, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v4, v2}, LX/1P5;->b(Landroid/view/View;)I

    move-result v2

    iget-object v4, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v4}, LX/1P5;->b()I

    move-result v4

    add-int/2addr v2, v4

    :goto_a
    iput v2, v0, LX/1P3;->b:I

    goto/16 :goto_1

    :cond_1c
    iget-object v4, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v4, v2}, LX/1P5;->a(Landroid/view/View;)I

    move-result v2

    goto :goto_a

    .line 243042
    :cond_1d
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v2

    if-lez v2, :cond_1f

    .line 243043
    invoke-virtual {p0, v4}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, LX/1OR;->d(Landroid/view/View;)I

    move-result v2

    .line 243044
    iget v5, p0, LX/1P1;->m:I

    if-ge v5, v2, :cond_20

    move v2, v3

    :goto_b
    iget-boolean v5, p0, LX/1P1;->l:Z

    if-ne v2, v5, :cond_1e

    move v4, v3

    :cond_1e
    iput-boolean v4, v0, LX/1P3;->c:Z

    .line 243045
    :cond_1f
    invoke-virtual {v0}, LX/1P3;->b()V

    goto/16 :goto_1

    :cond_20
    move v2, v4

    .line 243046
    goto :goto_b

    .line 243047
    :cond_21
    iget-boolean v2, p0, LX/1P1;->l:Z

    iput-boolean v2, v0, LX/1P3;->c:Z

    .line 243048
    iget-boolean v2, p0, LX/1P1;->l:Z

    if-eqz v2, :cond_22

    .line 243049
    iget-object v2, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->d()I

    move-result v2

    iget v4, p0, LX/1P1;->n:I

    sub-int/2addr v2, v4

    iput v2, v0, LX/1P3;->b:I

    goto/16 :goto_1

    .line 243050
    :cond_22
    iget-object v2, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->c()I

    move-result v2

    iget v4, p0, LX/1P1;->n:I

    add-int/2addr v2, v4

    iput v2, v0, LX/1P3;->b:I

    goto/16 :goto_1

    .line 243051
    :cond_23
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 243052
    iget-object v1, p1, LX/1Od;->e:Ljava/util/List;

    move-object v9, v1

    .line 243053
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    .line 243054
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, LX/1OR;->d(Landroid/view/View;)I

    move-result v11

    .line 243055
    const/4 v1, 0x0

    move v8, v1

    :goto_c
    if-ge v8, v10, :cond_27

    .line 243056
    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1a1;

    .line 243057
    invoke-virtual {v1}, LX/1a1;->q()Z

    move-result v3

    if-nez v3, :cond_2a

    .line 243058
    invoke-virtual {v1}, LX/1a1;->d()I

    move-result v3

    .line 243059
    if-ge v3, v11, :cond_24

    const/4 v3, 0x1

    :goto_d
    iget-boolean v12, p0, LX/1P1;->l:Z

    if-eq v3, v12, :cond_25

    const/4 v3, -0x1

    .line 243060
    :goto_e
    const/4 v12, -0x1

    if-ne v3, v12, :cond_26

    .line 243061
    iget-object v3, p0, LX/1P1;->k:LX/1P5;

    iget-object v1, v1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v3, v1}, LX/1P5;->c(Landroid/view/View;)I

    move-result v1

    add-int/2addr v1, v5

    move v3, v1

    move v1, v4

    .line 243062
    :goto_f
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move v5, v3

    move v4, v1

    goto :goto_c

    .line 243063
    :cond_24
    const/4 v3, 0x0

    goto :goto_d

    :cond_25
    const/4 v3, 0x1

    goto :goto_e

    .line 243064
    :cond_26
    iget-object v3, p0, LX/1P1;->k:LX/1P5;

    iget-object v1, v1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v3, v1}, LX/1P5;->c(Landroid/view/View;)I

    move-result v1

    add-int/2addr v1, v4

    move v3, v5

    goto :goto_f

    .line 243065
    :cond_27
    iget-object v1, p0, LX/1P1;->a:LX/1ZW;

    iput-object v9, v1, LX/1ZW;->k:Ljava/util/List;

    .line 243066
    if-lez v5, :cond_28

    .line 243067
    invoke-static {p0}, LX/1P1;->I(LX/1P1;)Landroid/view/View;

    move-result-object v1

    .line 243068
    invoke-static {v1}, LX/1OR;->d(Landroid/view/View;)I

    move-result v1

    invoke-static {p0, v1, v2}, LX/1P1;->g(LX/1P1;II)V

    .line 243069
    iget-object v1, p0, LX/1P1;->a:LX/1ZW;

    iput v5, v1, LX/1ZW;->h:I

    .line 243070
    iget-object v1, p0, LX/1P1;->a:LX/1ZW;

    const/4 v3, 0x0

    iput v3, v1, LX/1ZW;->c:I

    .line 243071
    iget-object v1, p0, LX/1P1;->a:LX/1ZW;

    invoke-virtual {v1}, LX/1ZW;->a()V

    .line 243072
    iget-object v1, p0, LX/1P1;->a:LX/1ZW;

    const/4 v3, 0x0

    invoke-static {p0, p1, v1, p2, v3}, LX/1P1;->a(LX/1P1;LX/1Od;LX/1ZW;LX/1Ok;Z)I

    .line 243073
    :cond_28
    if-lez v4, :cond_29

    .line 243074
    invoke-static {p0}, LX/1P1;->J(LX/1P1;)Landroid/view/View;

    move-result-object v1

    .line 243075
    invoke-static {v1}, LX/1OR;->d(Landroid/view/View;)I

    move-result v1

    invoke-static {p0, v1, v0}, LX/1P1;->f(LX/1P1;II)V

    .line 243076
    iget-object v1, p0, LX/1P1;->a:LX/1ZW;

    iput v4, v1, LX/1ZW;->h:I

    .line 243077
    iget-object v1, p0, LX/1P1;->a:LX/1ZW;

    const/4 v3, 0x0

    iput v3, v1, LX/1ZW;->c:I

    .line 243078
    iget-object v1, p0, LX/1P1;->a:LX/1ZW;

    invoke-virtual {v1}, LX/1ZW;->a()V

    .line 243079
    iget-object v1, p0, LX/1P1;->a:LX/1ZW;

    const/4 v3, 0x0

    invoke-static {p0, p1, v1, p2, v3}, LX/1P1;->a(LX/1P1;LX/1Od;LX/1ZW;LX/1Ok;Z)I

    .line 243080
    :cond_29
    iget-object v1, p0, LX/1P1;->a:LX/1ZW;

    const/4 v3, 0x0

    iput-object v3, v1, LX/1ZW;->k:Ljava/util/List;

    goto/16 :goto_8

    :cond_2a
    move v1, v4

    move v3, v5

    goto :goto_f
.end method

.method public final d(LX/1Ok;)I
    .locals 1

    .prologue
    .line 243198
    invoke-direct {p0, p1}, LX/1P1;->i(LX/1Ok;)I

    move-result v0

    return v0
.end method

.method public final d(I)Landroid/graphics/PointF;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 243230
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v2

    if-nez v2, :cond_0

    .line 243231
    const/4 v0, 0x0

    .line 243232
    :goto_0
    return-object v0

    .line 243233
    :cond_0
    invoke-virtual {p0, v0}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, LX/1OR;->d(Landroid/view/View;)I

    move-result v2

    .line 243234
    if-ge p1, v2, :cond_1

    move v0, v1

    :cond_1
    iget-boolean v2, p0, LX/1P1;->l:Z

    if-eq v0, v2, :cond_2

    const/4 v1, -0x1

    .line 243235
    :cond_2
    iget v0, p0, LX/1P1;->j:I

    if-nez v0, :cond_3

    .line 243236
    new-instance v0, Landroid/graphics/PointF;

    int-to-float v1, v1

    invoke-direct {v0, v1, v3}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_0

    .line 243237
    :cond_3
    new-instance v0, Landroid/graphics/PointF;

    int-to-float v1, v1

    invoke-direct {v0, v3, v1}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_0
.end method

.method public d(II)V
    .locals 1

    .prologue
    .line 243224
    iput p1, p0, LX/1P1;->m:I

    .line 243225
    iput p2, p0, LX/1P1;->n:I

    .line 243226
    iget-object v0, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-eqz v0, :cond_0

    .line 243227
    iget-object v0, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->b()V

    .line 243228
    :cond_0
    invoke-virtual {p0}, LX/1OR;->p()V

    .line 243229
    return-void
.end method

.method public e(LX/1Ok;)I
    .locals 1

    .prologue
    .line 243223
    invoke-direct {p0, p1}, LX/1P1;->i(LX/1Ok;)I

    move-result v0

    return v0
.end method

.method public e(I)V
    .locals 1

    .prologue
    .line 243217
    iput p1, p0, LX/1P1;->m:I

    .line 243218
    const/high16 v0, -0x80000000

    iput v0, p0, LX/1P1;->n:I

    .line 243219
    iget-object v0, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-eqz v0, :cond_0

    .line 243220
    iget-object v0, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->b()V

    .line 243221
    :cond_0
    invoke-virtual {p0}, LX/1OR;->p()V

    .line 243222
    return-void
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 243216
    iget-object v0, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/1P1;->b:Z

    iget-boolean v1, p0, LX/1P1;->d:Z

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(LX/1Ok;)I
    .locals 1

    .prologue
    .line 243215
    invoke-direct {p0, p1}, LX/1P1;->j(LX/1Ok;)I

    move-result v0

    return v0
.end method

.method public final f()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 243199
    iget-object v0, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-eqz v0, :cond_0

    .line 243200
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iget-object v1, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-direct {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;-><init>(Landroid/support/v7/widget/LinearLayoutManager$SavedState;)V

    .line 243201
    :goto_0
    return-object v0

    .line 243202
    :cond_0
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-direct {v0}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;-><init>()V

    .line 243203
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v1

    if-lez v1, :cond_2

    .line 243204
    invoke-virtual {p0}, LX/1P1;->k()V

    .line 243205
    iget-boolean v1, p0, LX/1P1;->b:Z

    iget-boolean v2, p0, LX/1P1;->l:Z

    xor-int/2addr v1, v2

    .line 243206
    iput-boolean v1, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->c:Z

    .line 243207
    if-eqz v1, :cond_1

    .line 243208
    invoke-static {p0}, LX/1P1;->J(LX/1P1;)Landroid/view/View;

    move-result-object v1

    .line 243209
    iget-object v2, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->d()I

    move-result v2

    iget-object v3, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v3, v1}, LX/1P5;->b(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->b:I

    .line 243210
    invoke-static {v1}, LX/1OR;->d(Landroid/view/View;)I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->a:I

    goto :goto_0

    .line 243211
    :cond_1
    invoke-static {p0}, LX/1P1;->I(LX/1P1;)Landroid/view/View;

    move-result-object v1

    .line 243212
    invoke-static {v1}, LX/1OR;->d(Landroid/view/View;)I

    move-result v2

    iput v2, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->a:I

    .line 243213
    iget-object v2, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v2, v1}, LX/1P5;->a(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->c()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->b:I

    goto :goto_0

    .line 243214
    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->b()V

    goto :goto_0
.end method

.method public g(LX/1Ok;)I
    .locals 1

    .prologue
    .line 243180
    invoke-direct {p0, p1}, LX/1P1;->j(LX/1Ok;)I

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 243197
    iget v0, p0, LX/1P1;->j:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 243196
    iget v1, p0, LX/1P1;->j:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 243195
    invoke-virtual {p0}, LX/1OR;->t()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 243189
    iget-object v0, p0, LX/1P1;->a:LX/1ZW;

    if-nez v0, :cond_0

    .line 243190
    new-instance v0, LX/1ZW;

    invoke-direct {v0}, LX/1ZW;-><init>()V

    move-object v0, v0

    .line 243191
    iput-object v0, p0, LX/1P1;->a:LX/1ZW;

    .line 243192
    :cond_0
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    if-nez v0, :cond_1

    .line 243193
    iget v0, p0, LX/1P1;->j:I

    invoke-static {p0, v0}, LX/1P5;->a(LX/1OR;I)LX/1P5;

    move-result-object v0

    iput-object v0, p0, LX/1P1;->k:LX/1P5;

    .line 243194
    :cond_1
    return-void
.end method

.method public l()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 243187
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v2, v0, v2, v1}, LX/1P1;->a(IIZZ)Landroid/view/View;

    move-result-object v0

    .line 243188
    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, LX/1OR;->d(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method public m()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 243185
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v2, v0, v1, v2}, LX/1P1;->a(IIZZ)Landroid/view/View;

    move-result-object v0

    .line 243186
    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, LX/1OR;->d(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method public n()I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 243183
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {p0, v1, v0, v2, v3}, LX/1P1;->a(IIZZ)Landroid/view/View;

    move-result-object v1

    .line 243184
    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {v1}, LX/1OR;->d(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method public o()I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 243181
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, v1, v0, v2, v3}, LX/1P1;->a(IIZZ)Landroid/view/View;

    move-result-object v1

    .line 243182
    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {v1}, LX/1OR;->d(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method
