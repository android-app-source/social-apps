.class public final LX/1XN;
.super LX/1OD;
.source ""


# instance fields
.field public final synthetic a:LX/1UR;

.field private final b:I

.field private c:I


# direct methods
.method public constructor <init>(LX/1UR;I)V
    .locals 1

    .prologue
    .line 271039
    iput-object p1, p0, LX/1XN;->a:LX/1UR;

    invoke-direct {p0}, LX/1OD;-><init>()V

    .line 271040
    const/4 v0, -0x1

    iput v0, p0, LX/1XN;->c:I

    .line 271041
    iput p2, p0, LX/1XN;->b:I

    .line 271042
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 271043
    iget v0, p0, LX/1XN;->c:I

    add-int/2addr v0, p1

    iget-object v1, p0, LX/1XN;->a:LX/1UR;

    iget-object v1, v1, LX/1UR;->a:LX/1UW;

    .line 271044
    iget v2, v1, LX/1UW;->h:I

    move v1, v2

    .line 271045
    if-le v0, v1, :cond_0

    .line 271046
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t remove/change "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " items from position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/1XN;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " when size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/1XN;->a:LX/1UR;

    iget-object v2, v2, LX/1UR;->a:LX/1UW;

    .line 271047
    iget p0, v2, LX/1UW;->h:I

    move v2, p0

    .line 271048
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 271049
    :cond_0
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 271050
    iget v0, p0, LX/1XN;->c:I

    iget-object v1, p0, LX/1XN;->a:LX/1UR;

    iget-object v1, v1, LX/1UR;->a:LX/1UW;

    .line 271051
    iget v2, v1, LX/1UW;->h:I

    move v1, v2

    .line 271052
    if-lt v0, v1, :cond_0

    .line 271053
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t insert at "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/1XN;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " when size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/1XN;->a:LX/1UR;

    iget-object v2, v2, LX/1UR;->a:LX/1UW;

    .line 271054
    iget p0, v2, LX/1UW;->h:I

    move v2, p0

    .line 271055
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 271056
    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 271057
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    const/4 v1, 0x0

    .line 271058
    iput-boolean v1, v0, LX/1UR;->d:Z

    .line 271059
    const/4 v0, -0x1

    iput v0, p0, LX/1XN;->c:I

    .line 271060
    return-void
.end method

.method private d(II)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 271061
    if-ltz p1, :cond_0

    if-ltz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 271062
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    iget-object v0, v0, LX/1UR;->a:LX/1UW;

    iget v2, p0, LX/1XN;->b:I

    invoke-virtual {v0, v2}, LX/1UW;->b(I)V

    .line 271063
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    .line 271064
    iput-boolean v1, v0, LX/1UR;->d:Z

    .line 271065
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    iget-object v0, v0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0}, LX/1UW;->e()I

    move-result v0

    add-int/2addr v0, p1

    iput v0, p0, LX/1XN;->c:I

    .line 271066
    return-void

    .line 271067
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 271068
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    iget-boolean v0, v0, LX/1UR;->d:Z

    if-eqz v0, :cond_0

    .line 271069
    :goto_0
    return-void

    .line 271070
    :cond_0
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    const/4 v1, 0x1

    .line 271071
    iput-boolean v1, v0, LX/1UR;->d:Z

    .line 271072
    :try_start_0
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271073
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    .line 271074
    iput-boolean v2, v0, LX/1UR;->d:Z

    .line 271075
    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1XN;->a:LX/1UR;

    .line 271076
    iput-boolean v2, v1, LX/1UR;->d:Z

    .line 271077
    throw v0
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 271078
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    iget-boolean v0, v0, LX/1UR;->d:Z

    if-eqz v0, :cond_0

    .line 271079
    :goto_0
    return-void

    .line 271080
    :cond_0
    invoke-direct {p0, p1, p2}, LX/1XN;->d(II)V

    .line 271081
    :try_start_0
    invoke-direct {p0, p2}, LX/1XN;->a(I)V

    .line 271082
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    iget-object v0, v0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0}, LX/1UW;->a()V

    .line 271083
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    iget v1, p0, LX/1XN;->c:I

    invoke-virtual {v0, v1, p2}, LX/1OM;->a(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271084
    invoke-direct {p0}, LX/1XN;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, LX/1XN;->c()V

    throw v0
.end method

.method public final a(III)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 271085
    iget-object v2, p0, LX/1XN;->a:LX/1UR;

    iget-boolean v2, v2, LX/1UR;->d:Z

    if-eqz v2, :cond_0

    .line 271086
    :goto_0
    return-void

    .line 271087
    :cond_0
    invoke-direct {p0, p1, p3}, LX/1XN;->d(II)V

    .line 271088
    :try_start_0
    invoke-direct {p0, p3}, LX/1XN;->a(I)V

    .line 271089
    if-ne p3, v0, :cond_1

    move v2, v0

    :goto_1
    const-string v3, "RecyclerView.Adapter.notifyItemRangedMoved() does not exist"

    invoke-static {v2, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 271090
    if-ltz p2, :cond_2

    iget-object v2, p0, LX/1XN;->a:LX/1UR;

    iget-object v2, v2, LX/1UR;->a:LX/1UW;

    .line 271091
    iget-object v3, v2, LX/1UW;->b:LX/0Px;

    iget v4, v2, LX/1UW;->e:I

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1UX;

    iget v3, v3, LX/1UX;->c:I

    move v2, v3

    .line 271092
    if-ge p2, v2, :cond_2

    move v2, v0

    :goto_2
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 271093
    iget-object v2, p0, LX/1XN;->a:LX/1UR;

    iget-object v2, v2, LX/1UR;->a:LX/1UW;

    invoke-virtual {v2}, LX/1UW;->e()I

    move-result v2

    add-int/2addr v2, p2

    .line 271094
    iget v3, p0, LX/1XN;->c:I

    iget-object v4, p0, LX/1XN;->a:LX/1UR;

    iget-object v4, v4, LX/1UR;->a:LX/1UW;

    .line 271095
    iget p1, v4, LX/1UW;->h:I

    move v4, p1

    .line 271096
    if-ge v3, v4, :cond_3

    :goto_3
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 271097
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    iget-object v0, v0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0}, LX/1UW;->a()V

    .line 271098
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    iget v1, p0, LX/1XN;->c:I

    invoke-virtual {v0, v1, v2}, LX/1OM;->b(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271099
    invoke-direct {p0}, LX/1XN;->c()V

    goto :goto_0

    :cond_1
    move v2, v1

    .line 271100
    goto :goto_1

    :cond_2
    move v2, v1

    .line 271101
    goto :goto_2

    :cond_3
    move v0, v1

    .line 271102
    goto :goto_3

    .line 271103
    :catchall_0
    move-exception v0

    invoke-direct {p0}, LX/1XN;->c()V

    throw v0
.end method

.method public final b(II)V
    .locals 2

    .prologue
    .line 271104
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    iget-boolean v0, v0, LX/1UR;->d:Z

    if-eqz v0, :cond_0

    .line 271105
    :goto_0
    return-void

    .line 271106
    :cond_0
    invoke-direct {p0, p1, p2}, LX/1XN;->d(II)V

    .line 271107
    :try_start_0
    invoke-direct {p0}, LX/1XN;->b()V

    .line 271108
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    iget-object v0, v0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0}, LX/1UW;->a()V

    .line 271109
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    iget v1, p0, LX/1XN;->c:I

    invoke-virtual {v0, v1, p2}, LX/1OM;->c(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271110
    invoke-direct {p0}, LX/1XN;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, LX/1XN;->c()V

    throw v0
.end method

.method public final c(II)V
    .locals 2

    .prologue
    .line 271111
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    iget-boolean v0, v0, LX/1UR;->d:Z

    if-eqz v0, :cond_0

    .line 271112
    :goto_0
    return-void

    .line 271113
    :cond_0
    invoke-direct {p0, p1, p2}, LX/1XN;->d(II)V

    .line 271114
    :try_start_0
    invoke-direct {p0, p2}, LX/1XN;->a(I)V

    .line 271115
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    iget-object v0, v0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0}, LX/1UW;->a()V

    .line 271116
    iget-object v0, p0, LX/1XN;->a:LX/1UR;

    iget v1, p0, LX/1XN;->c:I

    invoke-virtual {v0, v1, p2}, LX/1OM;->d(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271117
    invoke-direct {p0}, LX/1XN;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, LX/1XN;->c()V

    throw v0
.end method
