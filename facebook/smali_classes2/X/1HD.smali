.class public LX/1HD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1HC;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Z

.field private final d:Z

.field private final e:Z

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1HC;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2O6;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/compactdisk/StoreManagerFactory;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0ad;LX/0Or;)V
    .locals 2
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/1HC;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 226589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226590
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 226591
    iput-object v0, p0, LX/1HD;->h:LX/0Ot;

    .line 226592
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 226593
    iput-object v0, p0, LX/1HD;->i:LX/0Ot;

    .line 226594
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 226595
    iput-object v0, p0, LX/1HD;->j:LX/0Ot;

    .line 226596
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 226597
    iput-object v0, p0, LX/1HD;->k:LX/0Ot;

    .line 226598
    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1HD;->a:Ljava/lang/String;

    .line 226599
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1HD;->b:Ljava/lang/String;

    .line 226600
    iput-object p3, p0, LX/1HD;->f:LX/0Or;

    .line 226601
    sget-short v0, LX/1HE;->c:S

    invoke-interface {p2, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/1HD;->c:Z

    .line 226602
    sget-short v0, LX/1HE;->b:S

    invoke-interface {p2, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/1HD;->d:Z

    .line 226603
    sget-short v0, LX/1HE;->a:S

    invoke-interface {p2, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/1HD;->e:Z

    .line 226604
    return-void
.end method

.method public static a(Ljava/io/File;)Z
    .locals 1

    .prologue
    .line 226545
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 226546
    const/4 v0, 0x1

    .line 226547
    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, LX/2W9;->b(Ljava/io/File;)Z

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/1Gf;)Z
    .locals 3

    .prologue
    .line 226584
    new-instance v1, Ljava/io/File;

    .line 226585
    iget-object v0, p0, LX/1Gf;->c:LX/1Gd;

    move-object v0, v0

    .line 226586
    invoke-interface {v0}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 226587
    iget-object v2, p0, LX/1Gf;->b:Ljava/lang/String;

    move-object v2, v2

    .line 226588
    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, LX/1HD;->a(Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method private e(LX/1Gf;)LX/1Ha;
    .locals 14

    .prologue
    .line 226605
    new-instance v1, LX/488;

    invoke-static {p1}, LX/1HD;->i(LX/1Gf;)LX/0Tn;

    move-result-object v2

    iget-object v3, p0, LX/1HD;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v0, p0, LX/1HD;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/compactdisk/StoreManagerFactory;

    const/4 v11, 0x1

    .line 226606
    new-instance v5, Lcom/facebook/compactdisk/DiskCacheConfig;

    invoke-direct {v5}, Lcom/facebook/compactdisk/DiskCacheConfig;-><init>()V

    .line 226607
    iget-object v6, p1, LX/1Gf;->b:Ljava/lang/String;

    move-object v6, v6

    .line 226608
    invoke-virtual {v5, v6}, Lcom/facebook/compactdisk/DiskCacheConfig;->name(Ljava/lang/String;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v5

    invoke-virtual {v5, v11}, Lcom/facebook/compactdisk/DiskCacheConfig;->sessionScoped(Z)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v5

    invoke-static {p0, p1}, LX/1HD;->g(LX/1HD;LX/1Gf;)Lcom/facebook/compactdisk/DiskArea;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/facebook/compactdisk/DiskCacheConfig;->diskArea(Lcom/facebook/compactdisk/DiskArea;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v5

    .line 226609
    iget v6, p1, LX/1Gf;->a:I

    move v6, v6

    .line 226610
    int-to-long v7, v6

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/facebook/compactdisk/DiskCacheConfig;->version(Ljava/lang/Long;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v7

    new-instance v5, Lcom/facebook/compactdisk/ManagedConfig;

    invoke-direct {v5}, Lcom/facebook/compactdisk/ManagedConfig;-><init>()V

    new-instance v6, Lcom/facebook/compactdisk/StalePruningConfig;

    invoke-direct {v6}, Lcom/facebook/compactdisk/StalePruningConfig;-><init>()V

    const-wide/32 v9, 0x4f1a00

    invoke-virtual {v6, v9, v10}, Lcom/facebook/compactdisk/StalePruningConfig;->a(J)Lcom/facebook/compactdisk/StalePruningConfig;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/facebook/compactdisk/ManagedConfig;->a(Lcom/facebook/compactdisk/StalePruningConfig;)Lcom/facebook/compactdisk/ManagedConfig;

    move-result-object v5

    new-instance v6, Lcom/facebook/compactdisk/EvictionConfig;

    invoke-direct {v6}, Lcom/facebook/compactdisk/EvictionConfig;-><init>()V

    .line 226611
    iget-wide v12, p1, LX/1Gf;->d:J

    move-wide v9, v12

    .line 226612
    invoke-virtual {v6, v9, v10}, Lcom/facebook/compactdisk/EvictionConfig;->maxSize(J)Lcom/facebook/compactdisk/EvictionConfig;

    move-result-object v6

    .line 226613
    iget-wide v12, p1, LX/1Gf;->e:J

    move-wide v9, v12

    .line 226614
    invoke-virtual {v6, v9, v10}, Lcom/facebook/compactdisk/EvictionConfig;->lowSpaceMaxSize(J)Lcom/facebook/compactdisk/EvictionConfig;

    move-result-object v6

    invoke-virtual {v6, v11}, Lcom/facebook/compactdisk/EvictionConfig;->strictEnforcement(Z)Lcom/facebook/compactdisk/EvictionConfig;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/facebook/compactdisk/ManagedConfig;->a(Lcom/facebook/compactdisk/EvictionConfig;)Lcom/facebook/compactdisk/ManagedConfig;

    move-result-object v8

    new-array v9, v11, [Lcom/facebook/compactdisk/DiskCacheEventListener;

    const/4 v5, 0x0

    new-instance v6, LX/485;

    .line 226615
    iget-object v10, p1, LX/1Gf;->i:LX/1GE;

    move-object v10, v10

    .line 226616
    invoke-direct {v6, v10}, LX/485;-><init>(LX/1GE;)V

    aput-object v6, v9, v5

    iget-object v5, p0, LX/1HD;->j:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v6, p0, LX/1HD;->i:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0So;

    invoke-virtual {v8, v9, v5, v6}, Lcom/facebook/compactdisk/ManagedConfig;->a([Lcom/facebook/compactdisk/DiskCacheEventListener;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;)Lcom/facebook/compactdisk/ManagedConfig;

    move-result-object v5

    invoke-virtual {v7, v5}, Lcom/facebook/compactdisk/DiskCacheConfig;->subConfig(Lcom/facebook/compactdisk/SubConfig;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v5

    move-object v4, v5

    .line 226617
    invoke-direct {v1, v2, v3, v0, v4}, LX/488;-><init>(LX/0Tn;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/compactdisk/StoreManagerFactory;Lcom/facebook/compactdisk/DiskCacheConfig;)V

    .line 226618
    iget-object v0, p0, LX/1HD;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2O6;

    invoke-virtual {v0, v1}, LX/2O6;->a(LX/2FC;)V

    .line 226619
    new-instance v0, LX/487;

    invoke-direct {v0, v1}, LX/487;-><init>(LX/0Or;)V

    return-object v0
.end method

.method public static g(LX/1HD;LX/1Gf;)Lcom/facebook/compactdisk/DiskArea;
    .locals 4

    .prologue
    .line 226576
    iget-object v0, p1, LX/1Gf;->c:LX/1Gd;

    move-object v0, v0

    .line 226577
    invoke-interface {v0}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 226578
    iget-object v1, p0, LX/1HD;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226579
    sget-object v0, Lcom/facebook/compactdisk/DiskArea;->CACHES:Lcom/facebook/compactdisk/DiskArea;

    .line 226580
    :goto_0
    return-object v0

    .line 226581
    :cond_0
    iget-object v1, p0, LX/1HD;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 226582
    sget-object v0, Lcom/facebook/compactdisk/DiskArea;->DOCUMENTS:Lcom/facebook/compactdisk/DiskArea;

    goto :goto_0

    .line 226583
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized Base Directory: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static i(LX/1Gf;)LX/0Tn;
    .locals 3

    .prologue
    .line 226571
    sget-object v1, LX/0Tm;->a:LX/0Tn;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "compactdisk_fresco_cache_dir_"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 226572
    iget-object v0, p0, LX/1Gf;->c:LX/1Gd;

    move-object v0, v0

    .line 226573
    invoke-interface {v0}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 226574
    iget-object v2, p0, LX/1Gf;->b:Ljava/lang/String;

    move-object v2, v2

    .line 226575
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method


# virtual methods
.method public final a(LX/1Gf;)LX/1Ha;
    .locals 3

    .prologue
    .line 226548
    sget-object v1, LX/0Tm;->a:LX/0Tn;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "compactdisk_fresco_has_cleared_default_cache_"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 226549
    iget-object v0, p1, LX/1Gf;->c:LX/1Gd;

    move-object v0, v0

    .line 226550
    invoke-interface {v0}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 226551
    iget-object v2, p1, LX/1Gf;->b:Ljava/lang/String;

    move-object v2, v2

    .line 226552
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    move-object v0, v0

    .line 226553
    iget-boolean v1, p0, LX/1HD;->d:Z

    if-nez v1, :cond_1

    .line 226554
    iget-object v1, p0, LX/1HD;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 226555
    :cond_0
    :goto_0
    iget-boolean v0, p0, LX/1HD;->c:Z

    if-eqz v0, :cond_2

    .line 226556
    invoke-static {p1}, LX/1HD;->b(LX/1Gf;)Z

    .line 226557
    invoke-direct {p0, p1}, LX/1HD;->e(LX/1Gf;)LX/1Ha;

    move-result-object v0

    .line 226558
    :goto_1
    return-object v0

    .line 226559
    :cond_1
    iget-object v1, p0, LX/1HD;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 226560
    invoke-static {p1}, LX/1HD;->b(LX/1Gf;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226561
    iget-object v1, p0, LX/1HD;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0

    .line 226562
    :cond_2
    iget-boolean v0, p0, LX/1HD;->e:Z

    if-eqz v0, :cond_3

    .line 226563
    invoke-static {p1}, LX/1HD;->i(LX/1Gf;)LX/0Tn;

    move-result-object v0

    .line 226564
    iget-object v1, p0, LX/1HD;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 226565
    if-eqz v1, :cond_5

    .line 226566
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, LX/1HD;->a(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 226567
    :cond_3
    :goto_2
    iget-object v0, p0, LX/1HD;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HC;

    invoke-interface {v0, p1}, LX/1HC;->a(LX/1Gf;)LX/1Ha;

    move-result-object v0

    move-object v0, v0

    .line 226568
    goto :goto_1

    .line 226569
    :cond_4
    iget-object v1, p0, LX/1HD;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 226570
    :cond_5
    goto :goto_2
.end method
