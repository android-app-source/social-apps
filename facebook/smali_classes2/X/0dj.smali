.class public LX/0dj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static volatile d:LX/0dj;


# instance fields
.field private final b:LX/0dk;

.field private final c:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 90745
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "ComponentScriptHiPriINeedInit"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dj;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0dk;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 90746
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90747
    iput-object p1, p0, LX/0dj;->b:LX/0dk;

    .line 90748
    iput-object p2, p0, LX/0dj;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 90749
    return-void
.end method

.method public static a(LX/0QB;)LX/0dj;
    .locals 5

    .prologue
    .line 90750
    sget-object v0, LX/0dj;->d:LX/0dj;

    if-nez v0, :cond_1

    .line 90751
    const-class v1, LX/0dj;

    monitor-enter v1

    .line 90752
    :try_start_0
    sget-object v0, LX/0dj;->d:LX/0dj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 90753
    if-eqz v2, :cond_0

    .line 90754
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 90755
    new-instance p0, LX/0dj;

    invoke-static {v0}, LX/0dk;->a(LX/0QB;)LX/0dk;

    move-result-object v3

    check-cast v3, LX/0dk;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4}, LX/0dj;-><init>(LX/0dk;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 90756
    move-object v0, p0

    .line 90757
    sput-object v0, LX/0dj;->d:LX/0dj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90758
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 90759
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 90760
    :cond_1
    sget-object v0, LX/0dj;->d:LX/0dj;

    return-object v0

    .line 90761
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 90762
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 90739
    iget-object v0, p0, LX/0dj;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dj;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90740
    const-string v0, "ComponentScriptHiPriINeedInit.init"

    const v1, -0xa2f2c74

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 90741
    :try_start_0
    iget-object v0, p0, LX/0dj;->b:LX/0dk;

    invoke-virtual {v0}, LX/0dk;->a()Lcom/facebook/java2js/JSContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/java2js/JSContext;->garbageCollect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90742
    const v0, 0x7440579c

    invoke-static {v0}, LX/02m;->a(I)V

    .line 90743
    :cond_0
    return-void

    .line 90744
    :catchall_0
    move-exception v0

    const v1, 0x49b42ff0    # 1476094.0f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
