.class public LX/0gt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0gu;


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/1ww;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1x7;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7FO;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/rapidfeedback/RapidFeedbackController;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7Fv;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1x6;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0SG;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/0SG;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1x7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7FO;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/rapidfeedback/RapidFeedbackController;",
            ">;",
            "LX/0Or",
            "<",
            "LX/7Fv;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1x6;",
            ">;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 114189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114190
    new-instance v0, LX/1ww;

    invoke-direct {v0}, LX/1ww;-><init>()V

    iput-object v0, p0, LX/0gt;->b:LX/1ww;

    .line 114191
    iput-object p1, p0, LX/0gt;->c:LX/0Ot;

    .line 114192
    iput-object p2, p0, LX/0gt;->d:LX/0Ot;

    .line 114193
    iput-object p3, p0, LX/0gt;->e:LX/0Ot;

    .line 114194
    iput-object p4, p0, LX/0gt;->f:LX/0Or;

    .line 114195
    iput-object p5, p0, LX/0gt;->g:LX/0Or;

    .line 114196
    iput-object p6, p0, LX/0gt;->h:LX/0Or;

    .line 114197
    iput-object p7, p0, LX/0gt;->i:LX/0SG;

    .line 114198
    return-void
.end method

.method public static b(LX/0QB;)LX/0gt;
    .locals 8

    .prologue
    .line 114199
    new-instance v0, LX/0gt;

    const/16 v1, 0xbc

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x122c

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x35b8

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x105e

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x35bc

    invoke-static {p0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x122e

    invoke-static {p0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-direct/range {v0 .. v7}, LX/0gt;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/0SG;)V

    .line 114200
    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 114205
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "survey_integration_touched"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 114206
    const-string v0, "integration_point_id"

    iget-object v2, p0, LX/0gt;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "action"

    const-string v3, "endpoint_hit"

    invoke-virtual {v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "reason"

    invoke-virtual {v0, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 114207
    iget-object v0, p0, LX/0gt;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 114208
    return-void
.end method


# virtual methods
.method public final a(LX/1x2;)LX/0gt;
    .locals 1

    .prologue
    .line 114201
    invoke-virtual {p1}, LX/1x2;->getThemeId()I

    move-result v0

    .line 114202
    iget-object p1, p0, LX/0gt;->b:LX/1ww;

    .line 114203
    iput v0, p1, LX/1ww;->a:I

    .line 114204
    return-object p0
.end method

.method public final a(Landroid/os/Bundle;)LX/0gt;
    .locals 3

    .prologue
    .line 114127
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 114128
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    goto :goto_0

    .line 114129
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;
    .locals 2

    .prologue
    .line 114186
    iget-object v0, p0, LX/0gt;->b:LX/1ww;

    .line 114187
    iget-object v1, v0, LX/1ww;->d:Ljava/util/Map;

    invoke-interface {v1, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114188
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114185
    iget-object v0, p0, LX/0gt;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114181
    iget-object v0, p0, LX/0gt;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1x6;

    invoke-virtual {v0, p0}, LX/1x6;->b(LX/0gu;)V

    .line 114182
    iget-object v0, p0, LX/0gt;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/0gt;->a:Ljava/lang/String;

    iget-object v1, p0, LX/0gt;->b:LX/1ww;

    invoke-virtual {v1}, LX/1ww;->k()LX/1x4;

    move-result-object v1

    .line 114183
    sget-object v2, LX/7Fv;->a:Ljava/util/Map;

    new-instance p0, LX/7Fu;

    invoke-direct {p0, p1, v1}, LX/7Fu;-><init>(LX/0Px;LX/1x4;)V

    invoke-interface {v2, v0, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114184
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 114178
    const-string v0, "prepare_and_show"

    invoke-direct {p0, v0}, LX/0gt;->c(Ljava/lang/String;)V

    .line 114179
    iget-object v0, p0, LX/0gt;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidfeedback/RapidFeedbackController;

    iget-object v1, p0, LX/0gt;->a:Ljava/lang/String;

    iget-object v2, p0, LX/0gt;->b:LX/1ww;

    invoke-virtual {v2}, LX/1ww;->k()LX/1x4;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(Ljava/lang/String;Landroid/content/Context;LX/1x4;)V

    .line 114180
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 114172
    iget-object v0, p0, LX/0gt;->a:Ljava/lang/String;

    invoke-static {v0}, LX/7Fv;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 114173
    iget-object v0, p0, LX/0gt;->a:Ljava/lang/String;

    const-string v1, "Integration point not set, cannot fetch survey from server"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114174
    iget-object v0, p0, LX/0gt;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1x6;

    invoke-virtual {v0, p0}, LX/1x6;->a(LX/0gu;)V

    .line 114175
    const-string v0, "prepare"

    invoke-direct {p0, v0}, LX/0gt;->c(Ljava/lang/String;)V

    .line 114176
    iget-object v0, p0, LX/0gt;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1x7;

    iget-object v1, p0, LX/0gt;->a:Ljava/lang/String;

    iget-object v2, p0, LX/0gt;->b:LX/1ww;

    invoke-virtual {v2}, LX/1ww;->k()LX/1x4;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/1x7;->a(Ljava/lang/String;LX/1x4;Landroid/content/Context;)V

    .line 114177
    :cond_0
    return-void
.end method

.method public final b(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 114130
    iget-object v0, p0, LX/0gt;->a:Ljava/lang/String;

    const-string v1, "Integration point not set, cannot show survey"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114131
    iget-object v0, p0, LX/0gt;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/0gt;->a:Ljava/lang/String;

    .line 114132
    sget-object v1, LX/7Fv;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Fu;

    move-object v1, v1

    .line 114133
    if-nez v1, :cond_1

    .line 114134
    :cond_0
    :goto_0
    return-void

    .line 114135
    :cond_1
    new-instance v0, LX/1ww;

    .line 114136
    iget-object v2, v1, LX/7Fu;->b:LX/1x4;

    move-object v2, v2

    .line 114137
    invoke-direct {v0, v2}, LX/1ww;-><init>(LX/1x4;)V

    iget-object v2, p0, LX/0gt;->b:LX/1ww;

    .line 114138
    iget v3, v2, LX/1ww;->a:I

    move v3, v3

    .line 114139
    if-eqz v3, :cond_2

    .line 114140
    iget v3, v2, LX/1ww;->a:I

    move v3, v3

    .line 114141
    iput v3, v0, LX/1ww;->a:I

    .line 114142
    :cond_2
    iget-object v3, v2, LX/1ww;->b:LX/1x3;

    move-object v3, v3

    .line 114143
    if-eqz v3, :cond_3

    .line 114144
    iget-object v3, v2, LX/1ww;->b:LX/1x3;

    move-object v3, v3

    .line 114145
    iput-object v3, v0, LX/1ww;->b:LX/1x3;

    .line 114146
    :cond_3
    iget-boolean v3, v0, LX/1ww;->c:Z

    if-nez v3, :cond_4

    .line 114147
    iget-boolean v3, v2, LX/1ww;->c:Z

    move v3, v3

    .line 114148
    if-eqz v3, :cond_6

    :cond_4
    const/4 v3, 0x1

    :goto_1
    iput-boolean v3, v0, LX/1ww;->c:Z

    .line 114149
    iget-object v3, v0, LX/1ww;->d:Ljava/util/Map;

    invoke-virtual {v2}, LX/1ww;->d()LX/0P1;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 114150
    iget-object v3, v0, LX/1ww;->e:Ljava/util/Map;

    invoke-virtual {v2}, LX/1ww;->e()LX/0P1;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 114151
    iget-object v3, v0, LX/1ww;->f:Ljava/util/Map;

    invoke-virtual {v2}, LX/1ww;->f()LX/0P1;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 114152
    iget-object v3, v2, LX/1ww;->g:Ljava/lang/Runnable;

    move-object v3, v3

    .line 114153
    if-eqz v3, :cond_5

    .line 114154
    iget-object v3, v2, LX/1ww;->g:Ljava/lang/Runnable;

    move-object v3, v3

    .line 114155
    iput-object v3, v0, LX/1ww;->g:Ljava/lang/Runnable;

    .line 114156
    :cond_5
    iget-boolean v3, v2, LX/1ww;->h:Z

    move v3, v3

    .line 114157
    iput-boolean v3, v0, LX/1ww;->h:Z

    .line 114158
    iget-object v3, v2, LX/1ww;->i:LX/1bH;

    move-object v3, v3

    .line 114159
    iput-object v3, v0, LX/1ww;->i:LX/1bH;

    .line 114160
    iget-object v3, v2, LX/1ww;->j:LX/7FE;

    move-object v3, v3

    .line 114161
    iput-object v3, v0, LX/1ww;->j:LX/7FE;

    .line 114162
    move-object v2, v0

    .line 114163
    iget-object v0, p0, LX/0gt;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7FO;

    iget-object v3, p0, LX/0gt;->a:Ljava/lang/String;

    .line 114164
    iget-object v4, v1, LX/7Fu;->a:LX/0Px;

    move-object v1, v4

    .line 114165
    invoke-virtual {v2}, LX/1ww;->k()LX/1x4;

    move-result-object v4

    invoke-virtual {v0, v3, v1, v4}, LX/7FO;->a(Ljava/lang/String;LX/0Px;LX/1x4;)Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    move-result-object v1

    .line 114166
    if-eqz v1, :cond_0

    .line 114167
    iget-object v0, p0, LX/0gt;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidfeedback/RapidFeedbackController;

    iget-object v3, p0, LX/0gt;->a:Ljava/lang/String;

    invoke-virtual {v2}, LX/1ww;->k()LX/1x4;

    move-result-object v2

    .line 114168
    invoke-static {v0, p1, v3, v2}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(Lcom/facebook/rapidfeedback/RapidFeedbackController;Landroid/content/Context;Ljava/lang/String;LX/1x4;)Ljava/lang/Runnable;

    move-result-object v4

    iput-object v4, v0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->d:Ljava/lang/Runnable;

    .line 114169
    iget-object v4, v0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    invoke-virtual {v4, p1, v3, v2}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->a(Landroid/content/Context;Ljava/lang/String;LX/1x4;)Lcom/facebook/structuredsurvey/StructuredSurveyController;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->a(Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;)Lcom/facebook/structuredsurvey/StructuredSurveyController;

    move-result-object v4

    iget-object p0, v0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->d:Ljava/lang/Runnable;

    invoke-virtual {v4, p0}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->a(Ljava/lang/Runnable;)V

    .line 114170
    goto/16 :goto_0

    .line 114171
    :cond_6
    const/4 v3, 0x0

    goto :goto_1
.end method
