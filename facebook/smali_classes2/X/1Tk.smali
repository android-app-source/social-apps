.class public LX/1Tk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;
.implements LX/1TG;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 254219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254220
    return-void
.end method

.method public static a(LX/0QB;)LX/1Tk;
    .locals 3

    .prologue
    .line 254221
    const-class v1, LX/1Tk;

    monitor-enter v1

    .line 254222
    :try_start_0
    sget-object v0, LX/1Tk;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 254223
    sput-object v2, LX/1Tk;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 254224
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254225
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 254226
    new-instance v0, LX/1Tk;

    invoke-direct {v0}, LX/1Tk;-><init>()V

    .line 254227
    move-object v0, v0

    .line 254228
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 254229
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Tk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 254230
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 254231
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 1

    .prologue
    .line 254232
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->a:LX/1Cz;

    invoke-static {p1, v0}, LX/99p;->a(LX/0ja;LX/1Cz;)V

    .line 254233
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;->a:LX/1Cz;

    invoke-static {p1, v0}, LX/99p;->a(LX/0ja;LX/1Cz;)V

    .line 254234
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->a:LX/1Cz;

    invoke-static {p1, v0}, LX/99p;->a(LX/0ja;LX/1Cz;)V

    .line 254235
    return-void
.end method

.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 254236
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 254237
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 254238
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 254239
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 0

    .prologue
    .line 254240
    return-void
.end method
