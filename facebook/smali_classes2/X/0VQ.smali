.class public LX/0VQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static c:Landroid/content/SharedPreferences;

.field private static d:LX/03V;

.field public static e:Z

.field private static volatile g:LX/0VQ;


# instance fields
.field public f:LX/0WV;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 67747
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "hprof_dump_metadata"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0VQ;->a:LX/0Tn;

    .line 67748
    const-class v0, LX/0VQ;

    sput-object v0, LX/0VQ;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/03V;LX/0WV;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 67735
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67736
    sput-object p2, LX/0VQ;->d:LX/03V;

    .line 67737
    const/4 p2, 0x0

    .line 67738
    sput-boolean p2, LX/0VQ;->e:Z

    .line 67739
    sget-object v0, LX/0VQ;->c:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    .line 67740
    sget-object v0, LX/0VQ;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 67741
    :cond_0
    const-string v0, "dump_metadata"

    invoke-virtual {p1, v0, p2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 67742
    sput-object v0, LX/0VQ;->c:Landroid/content/SharedPreferences;

    if-nez v0, :cond_1

    .line 67743
    const/4 v0, 0x1

    sput-boolean v0, LX/0VQ;->e:Z

    .line 67744
    const-string v0, "Error@updateContext isInvalid is true"

    const/4 p2, 0x0

    invoke-static {v0, p2}, LX/0VQ;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 67745
    :cond_1
    iput-object p3, p0, LX/0VQ;->f:LX/0WV;

    .line 67746
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0Tn;
    .locals 1

    .prologue
    .line 67708
    sget-object v0, LX/0VQ;->a:LX/0Tn;

    invoke-virtual {v0, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static a(LX/0QB;)LX/0VQ;
    .locals 6

    .prologue
    .line 67722
    sget-object v0, LX/0VQ;->g:LX/0VQ;

    if-nez v0, :cond_1

    .line 67723
    const-class v1, LX/0VQ;

    monitor-enter v1

    .line 67724
    :try_start_0
    sget-object v0, LX/0VQ;->g:LX/0VQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 67725
    if-eqz v2, :cond_0

    .line 67726
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 67727
    new-instance p0, LX/0VQ;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v5

    check-cast v5, LX/0WV;

    invoke-direct {p0, v3, v4, v5}, LX/0VQ;-><init>(Landroid/content/Context;LX/03V;LX/0WV;)V

    .line 67728
    move-object v0, p0

    .line 67729
    sput-object v0, LX/0VQ;->g:LX/0VQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67730
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 67731
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 67732
    :cond_1
    sget-object v0, LX/0VQ;->g:LX/0VQ;

    return-object v0

    .line 67733
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 67734
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 67715
    sget-object v0, LX/0VQ;->b:Ljava/lang/Class;

    invoke-static {v0, p0, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 67716
    sget-object v0, LX/0VQ;->d:LX/03V;

    if-nez v0, :cond_0

    .line 67717
    :goto_0
    return-void

    .line 67718
    :cond_0
    if-nez p1, :cond_1

    .line 67719
    :goto_1
    sget-object v0, LX/0VQ;->b:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, LX/0VG;->b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;

    move-result-object v0

    .line 67720
    sget-object v1, LX/0VQ;->d:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    goto :goto_0

    .line 67721
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method


# virtual methods
.method public final b()LX/0VQ;
    .locals 3

    .prologue
    .line 67709
    sget-boolean v0, LX/0VQ;->e:Z

    if-eqz v0, :cond_0

    .line 67710
    :goto_0
    return-object p0

    .line 67711
    :cond_0
    sget-object v0, LX/0VQ;->c:Landroid/content/SharedPreferences;

    sget-object v1, LX/0VQ;->a:LX/0Tn;

    invoke-virtual {v1}, LX/0Tn;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 67712
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 67713
    const-string v0, "Warning@deleteDumpMetadata(): No dump id found"

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0VQ;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 67714
    :cond_1
    sget-object v1, LX/0VQ;->c:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    sget-object v2, LX/0VQ;->a:LX/0Tn;

    invoke-virtual {v2}, LX/0Tn;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {v0}, LX/0VQ;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    invoke-virtual {v0}, LX/0Tn;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method
