.class public abstract enum LX/0QX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0QX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0QX;

.field public static final enum SOFT:LX/0QX;

.field public static final enum STRONG:LX/0QX;

.field public static final enum WEAK:LX/0QX;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 58174
    new-instance v0, LX/0QY;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v2}, LX/0QY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0QX;->STRONG:LX/0QX;

    .line 58175
    new-instance v0, LX/0QZ;

    const-string v1, "SOFT"

    invoke-direct {v0, v1, v3}, LX/0QZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0QX;->SOFT:LX/0QX;

    .line 58176
    new-instance v0, LX/0Qa;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v4}, LX/0Qa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0QX;->WEAK:LX/0QX;

    .line 58177
    const/4 v0, 0x3

    new-array v0, v0, [LX/0QX;

    sget-object v1, LX/0QX;->STRONG:LX/0QX;

    aput-object v1, v0, v2

    sget-object v1, LX/0QX;->SOFT:LX/0QX;

    aput-object v1, v0, v3

    sget-object v1, LX/0QX;->WEAK:LX/0QX;

    aput-object v1, v0, v4

    sput-object v0, LX/0QX;->$VALUES:[LX/0QX;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 58172
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0QX;
    .locals 1

    .prologue
    .line 58173
    const-class v0, LX/0QX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0QX;

    return-object v0
.end method

.method public static values()[LX/0QX;
    .locals 1

    .prologue
    .line 58171
    sget-object v0, LX/0QX;->$VALUES:[LX/0QX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0QX;

    return-object v0
.end method


# virtual methods
.method public abstract defaultEquivalence()LX/0Qj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Qj",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract referenceValue(LX/0Qx;LX/0R1;Ljava/lang/Object;I)LX/0Qf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Qx",
            "<TK;TV;>;",
            "LX/0R1",
            "<TK;TV;>;TV;I)",
            "LX/0Qf",
            "<TK;TV;>;"
        }
    .end annotation
.end method
