.class public LX/0rE;
.super LX/0rB;
.source ""


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/80m;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 149144
    sget-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->d:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {p0, v0, p1}, LX/0rB;-><init>(Lcom/facebook/api/feedtype/FeedType$Name;LX/0Ot;)V

    .line 149145
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Lcom/facebook/api/feedtype/FeedType;
    .locals 3

    .prologue
    .line 149146
    const-string v0, "hashtag_feed_hashtag"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 149147
    new-instance v1, Lcom/facebook/api/feedtype/FeedType;

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->d:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v1, v0, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    return-object v1
.end method

.method public final a(Landroid/content/Intent;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 149148
    const-string v0, "hashtag_feed_title"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 149149
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 149150
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 149151
    :cond_0
    return-object v0
.end method
