.class public LX/1jY;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field public a:I
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field public final c:LX/0qp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qp",
            "<",
            "Ljava/lang/String;",
            "LX/1jg;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0pV;

.field public final g:LX/0qj;

.field public final h:LX/0ad;

.field private final i:LX/1jZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 300515
    const-class v0, LX/1jY;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1jY;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0pV;LX/0qj;LX/0ad;LX/1jZ;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/16 v3, 0xc8

    const/4 v2, 0x0

    .line 300571
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 300572
    iput v2, p0, LX/1jY;->a:I

    .line 300573
    iput-object p2, p0, LX/1jY;->g:LX/0qj;

    .line 300574
    iput-object p3, p0, LX/1jY;->h:LX/0ad;

    .line 300575
    new-instance v0, LX/0qp;

    new-instance v1, LX/1jc;

    invoke-direct {v1, p0}, LX/1jc;-><init>(LX/1jY;)V

    invoke-direct {v0, v1}, LX/0qp;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, LX/1jY;->c:LX/0qp;

    .line 300576
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/1jY;->d:Ljava/util/Map;

    .line 300577
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/1jY;->e:Ljava/util/Map;

    .line 300578
    iput-object p1, p0, LX/1jY;->f:LX/0pV;

    .line 300579
    iput-object p4, p0, LX/1jY;->i:LX/1jZ;

    .line 300580
    return-void
.end method

.method private static a(Lcom/facebook/feed/model/ClientFeedUnitEdge;)Z
    .locals 2

    .prologue
    .line 300568
    const-string v0, "Ad"

    .line 300569
    iget-object v1, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A:Ljava/lang/String;

    move-object v1, v1

    .line 300570
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static b(LX/1jY;I)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 300562
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 300563
    iget-object v0, p0, LX/1jY;->c:LX/0qp;

    invoke-virtual {v0}, LX/0qp;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 300564
    :goto_1
    return v1

    :cond_0
    move v0, v1

    .line 300565
    goto :goto_0

    .line 300566
    :cond_1
    invoke-static {p0, p1}, LX/1jY;->c(LX/1jY;I)Ljava/lang/String;

    move-result-object v0

    .line 300567
    iget-object v1, p0, LX/1jY;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1
.end method

.method public static c(LX/1jY;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 300559
    iget-object v0, p0, LX/1jY;->c:LX/0qp;

    .line 300560
    iget-object p0, v0, LX/0qp;->e:Ljava/util/List;

    move-object v0, p0

    .line 300561
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1jg;

    iget-object v0, v0, LX/1jg;->d:Ljava/lang/String;

    return-object v0
.end method

.method public static d(LX/1jY;Ljava/lang/String;)LX/1jg;
    .locals 3

    .prologue
    .line 300554
    iget-object v0, p0, LX/1jY;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 300555
    const/4 v1, 0x0

    .line 300556
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 300557
    iget-object v1, p0, LX/1jY;->c:LX/0qp;

    invoke-virtual {v1, v0}, LX/0qp;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1jg;

    .line 300558
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public static i(LX/1jY;)V
    .locals 3

    .prologue
    .line 300545
    sget-object v0, LX/1KX;->a:LX/1KX;

    move-object v0, v0

    .line 300546
    iget-boolean v1, v0, LX/1KX;->e:Z

    move v1, v1

    .line 300547
    if-eqz v1, :cond_0

    .line 300548
    iget-object v1, p0, LX/1jY;->c:LX/0qp;

    .line 300549
    iget-object v2, v1, LX/0qp;->e:Ljava/util/List;

    move-object v1, v2

    .line 300550
    iput-object v1, v0, LX/1KX;->i:Ljava/util/List;

    .line 300551
    iget v1, p0, LX/1jY;->a:I

    .line 300552
    iput v1, v0, LX/1KX;->j:I

    .line 300553
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/model/ClientFeedUnitEdge;IZ)V
    .locals 18

    .prologue
    .line 300522
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1jY;->c:LX/0qp;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0qp;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1jg;

    .line 300523
    const/4 v3, 0x0

    .line 300524
    if-eqz v2, :cond_4

    iget-object v4, v2, LX/1jg;->c:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-gtz v4, :cond_4

    .line 300525
    iget-boolean v3, v2, LX/1jg;->g:Z

    if-eqz v3, :cond_0

    .line 300526
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1jY;->c:LX/0qp;

    invoke-virtual {v3}, LX/0qp;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 300527
    if-lez v2, :cond_0

    .line 300528
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1jY;->c:LX/0qp;

    invoke-virtual {v3}, LX/0qp;->a()Ljava/util/List;

    move-result-object v3

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1jg;

    const/4 v3, 0x1

    iput-boolean v3, v2, LX/1jg;->g:Z

    .line 300529
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1jY;->c:LX/0qp;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0qp;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1jg;

    .line 300530
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1jY;->d:Ljava/util/Map;

    iget-object v4, v2, LX/1jg;->d:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 300531
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1jY;->d:Ljava/util/Map;

    iget-object v2, v2, LX/1jg;->d:Ljava/lang/String;

    invoke-interface {v3, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 300532
    :cond_1
    const/4 v3, 0x0

    .line 300533
    const/4 v2, 0x1

    move/from16 v17, v2

    move-object v2, v3

    .line 300534
    :goto_0
    if-nez v2, :cond_3

    .line 300535
    new-instance v2, LX/1jg;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {p1 .. p1}, LX/1jY;->a(Lcom/facebook/feed/model/ClientFeedUnitEdge;)Z

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->E()J

    move-result-wide v8

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->I()D

    move-result-wide v10

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->e()D

    move-result-wide v12

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A()I

    move-result v14

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->C()I

    move-result v15

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->D()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v16}, LX/1jg;-><init>(Lcom/facebook/feed/model/ClientFeedUnitEdge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJDDIILjava/lang/String;)V

    .line 300536
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1jY;->c:LX/0qp;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, LX/0qp;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300537
    invoke-static/range {p1 .. p1}, LX/1jY;->a(Lcom/facebook/feed/model/ClientFeedUnitEdge;)Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez p3, :cond_2

    .line 300538
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1jY;->d:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300539
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1jY;->e:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300540
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1jY;->i:LX/1jZ;

    invoke-virtual {v3}, LX/1jZ;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 300541
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1jY;->c:LX/0qp;

    invoke-virtual {v3, v2}, LX/0qp;->a(Ljava/lang/Object;)I

    move-result v5

    .line 300542
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1jY;->i:LX/1jZ;

    move-object/from16 v0, p0

    iget v6, v0, LX/1jY;->a:I

    move-object/from16 v0, p0

    iget-object v3, v0, LX/1jY;->c:LX/0qp;

    invoke-virtual {v3}, LX/0qp;->size()I

    move-result v7

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v8, v17

    invoke-virtual/range {v2 .. v8}, LX/1jZ;->a(Lcom/facebook/feed/model/ClientFeedUnitEdge;IIIIZ)V

    .line 300543
    :cond_3
    invoke-static/range {p0 .. p0}, LX/1jY;->i(LX/1jY;)V

    .line 300544
    return-void

    :cond_4
    move/from16 v17, v3

    goto/16 :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 300516
    invoke-static {p0, p1}, LX/1jY;->d(LX/1jY;Ljava/lang/String;)LX/1jg;

    move-result-object v0

    .line 300517
    if-eqz v0, :cond_0

    .line 300518
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/1jg;->g:Z

    .line 300519
    :goto_0
    invoke-static {p0}, LX/1jY;->i(LX/1jY;)V

    .line 300520
    return-void

    .line 300521
    :cond_0
    iget-object v0, p0, LX/1jY;->f:LX/0pV;

    sget-object v1, LX/1jY;->b:Ljava/lang/String;

    const-string v2, "Unable to set GAP for cursor"

    invoke-virtual {v0, v1, v2, p1}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
