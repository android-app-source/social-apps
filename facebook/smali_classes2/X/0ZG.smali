.class public LX/0ZG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0VI;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Lt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2Lt;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 83101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83102
    iput-object p1, p0, LX/0ZG;->a:LX/0Ot;

    .line 83103
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83104
    const-string v0, "loom_state"

    return-object v0
.end method

.method public final a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 83105
    const/16 v2, 0x20

    .line 83106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 83107
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83108
    const-string v1, "async "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83109
    :cond_0
    const/4 v1, 0x4

    invoke-static {v1}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 83110
    const-string v1, "qpl "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83111
    :cond_1
    const/16 v1, 0x8

    invoke-static {v1}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 83112
    const-string v1, "other "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83113
    :cond_2
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 83114
    const-string v1, "lifecycle "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83115
    :cond_3
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 83116
    const-string v1, "fbtrace "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83117
    :cond_4
    invoke-static {v2}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 83118
    const-string v1, "user_counters "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83119
    :cond_5
    const/16 v1, 0x40

    invoke-static {v1}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 83120
    const-string v1, "system_counters "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83121
    :cond_6
    const/16 v1, 0x80

    invoke-static {v1}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 83122
    const-string v1, "stack_frame "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83123
    :cond_7
    const/16 v1, 0x100

    invoke-static {v1}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 83124
    const-string v1, "liger "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83125
    :cond_8
    const/16 v1, 0x200

    invoke-static {v1}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 83126
    const-string v1, "major_faults "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83127
    :cond_9
    const/16 v1, 0x400

    invoke-static {v1}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 83128
    const-string v1, "thread_schedule "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83129
    :cond_a
    const/16 v1, 0x800

    invoke-static {v1}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 83130
    const-string v1, "class_load "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83131
    :cond_b
    const/16 v1, 0x1000

    invoke-static {v1}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 83132
    const-string v1, "native_stack "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83133
    :cond_c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 83134
    invoke-static {}, LX/02o;->b()LX/02o;

    move-result-object v0

    invoke-virtual {v0}, LX/02o;->f()Ljava/lang/Iterable;

    move-result-object v3

    .line 83135
    const/4 v0, 0x0

    .line 83136
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 83137
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    move v2, v0

    .line 83138
    goto :goto_0

    .line 83139
    :cond_d
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 83140
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 83141
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 83142
    :cond_e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 83143
    const-string v3, "<none>"

    .line 83144
    sget-object v0, LX/02r;->b:LX/02r;

    move-object v0, v0

    .line 83145
    if-eqz v0, :cond_f

    .line 83146
    invoke-virtual {v0}, LX/02r;->d()Ljava/lang/String;

    move-result-object v0

    .line 83147
    const-string v4, "AAAAAAAAAAA"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_f

    .line 83148
    :goto_2
    move-object v3, v0

    .line 83149
    const-string v4, "Trace ID: %s\nNotification visible: %s\nEnabled providers: %s\nTrace files: %s"

    iget-object v0, p0, LX/0ZG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Lt;

    invoke-virtual {v0}, LX/2Lt;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v4, v3, v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_f
    move-object v0, v3

    goto :goto_2
.end method
