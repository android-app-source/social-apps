.class public LX/1GT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1GU;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1GT;


# instance fields
.field private final a:LX/0ad;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1GD;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1GD;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 225516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225517
    iput-object p1, p0, LX/1GT;->b:LX/0Ot;

    .line 225518
    iput-object p2, p0, LX/1GT;->c:LX/0Ot;

    .line 225519
    iput-object p3, p0, LX/1GT;->a:LX/0ad;

    .line 225520
    return-void
.end method

.method public static a(LX/0QB;)LX/1GT;
    .locals 6

    .prologue
    .line 225521
    sget-object v0, LX/1GT;->d:LX/1GT;

    if-nez v0, :cond_1

    .line 225522
    const-class v1, LX/1GT;

    monitor-enter v1

    .line 225523
    :try_start_0
    sget-object v0, LX/1GT;->d:LX/1GT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 225524
    if-eqz v2, :cond_0

    .line 225525
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 225526
    new-instance v4, LX/1GT;

    const/16 v3, 0xb9f

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x251c

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v4, v5, p0, v3}, LX/1GT;-><init>(LX/0Ot;LX/0Ot;LX/0ad;)V

    .line 225527
    move-object v0, v4

    .line 225528
    sput-object v0, LX/1GT;->d:LX/1GT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225529
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 225530
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 225531
    :cond_1
    sget-object v0, LX/1GT;->d:LX/1GT;

    return-object v0

    .line 225532
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 225533
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/43d;
    .locals 4

    .prologue
    .line 225513
    iget-object v0, p0, LX/1GT;->a:LX/0ad;

    sget-short v1, LX/1FD;->n:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225514
    new-instance v0, LX/4eH;

    iget-object v1, p0, LX/1GT;->b:LX/0Ot;

    iget-object v2, p0, LX/1GT;->c:LX/0Ot;

    iget-object v3, p0, LX/1GT;->a:LX/0ad;

    invoke-direct {v0, v1, v2, v3}, LX/4eH;-><init>(LX/0Ot;LX/0Ot;LX/0ad;)V

    invoke-virtual {v0}, LX/4eH;->a()LX/43d;

    move-result-object v0

    .line 225515
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/1Gh;

    invoke-direct {v0}, LX/1Gh;-><init>()V

    invoke-virtual {v0}, LX/1Gh;->a()LX/43d;

    move-result-object v0

    goto :goto_0
.end method
