.class public LX/0RI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/0RK;

.field public final b:LX/0RL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0RL",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final c:I


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 59843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59844
    sget-object v0, LX/0RJ;->INSTANCE:LX/0RJ;

    iput-object v0, p0, LX/0RI;->a:LX/0RK;

    .line 59845
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 59846
    new-instance v1, LX/0RL;

    invoke-static {v0}, LX/0RL;->b(Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0RL;-><init>(Ljava/lang/reflect/Type;)V

    move-object v0, v1

    .line 59847
    iput-object v0, p0, LX/0RI;->b:LX/0RL;

    .line 59848
    invoke-direct {p0}, LX/0RI;->d()I

    move-result v0

    iput v0, p0, LX/0RI;->c:I

    .line 59849
    return-void
.end method

.method public constructor <init>(LX/0RL;LX/0RK;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0RL",
            "<TT;>;",
            "LX/0RK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59856
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59857
    iput-object p2, p0, LX/0RI;->a:LX/0RK;

    .line 59858
    invoke-static {p1}, LX/0RM;->a(LX/0RL;)LX/0RL;

    move-result-object v0

    iput-object v0, p0, LX/0RI;->b:LX/0RL;

    .line 59859
    invoke-direct {p0}, LX/0RI;->d()I

    move-result v0

    iput v0, p0, LX/0RI;->c:I

    .line 59860
    return-void
.end method

.method public constructor <init>(Ljava/lang/reflect/Type;LX/0RK;)V
    .locals 1

    .prologue
    .line 59851
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59852
    iput-object p2, p0, LX/0RI;->a:LX/0RK;

    .line 59853
    invoke-static {p1}, LX/0RL;->a(Ljava/lang/reflect/Type;)LX/0RL;

    move-result-object v0

    invoke-static {v0}, LX/0RM;->a(LX/0RL;)LX/0RL;

    move-result-object v0

    iput-object v0, p0, LX/0RI;->b:LX/0RL;

    .line 59854
    invoke-direct {p0}, LX/0RI;->d()I

    move-result v0

    iput v0, p0, LX/0RI;->c:I

    .line 59855
    return-void
.end method

.method public static a(LX/0RL;)LX/0RI;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RL",
            "<TS;>;)",
            "LX/0RI",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 59850
    new-instance v0, LX/0RI;

    sget-object v1, LX/0RJ;->INSTANCE:LX/0RJ;

    invoke-direct {v0, p0, v1}, LX/0RI;-><init>(LX/0RL;LX/0RK;)V

    return-object v0
.end method

.method public static a(LX/0RL;Ljava/lang/Class;)LX/0RI;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RL",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0RI",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 59823
    new-instance v0, LX/0RI;

    invoke-static {p1}, LX/0RI;->b(Ljava/lang/Class;)LX/0RK;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/0RI;-><init>(LX/0RL;LX/0RK;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;)LX/0RI;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LX/0RI",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 59842
    new-instance v0, LX/0RI;

    sget-object v1, LX/0RJ;->INSTANCE:LX/0RJ;

    invoke-direct {v0, p0, v1}, LX/0RI;-><init>(Ljava/lang/reflect/Type;LX/0RK;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/Class;)LX/0RI;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0RI",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 59841
    new-instance v0, LX/0RI;

    invoke-static {p1}, LX/0RI;->b(Ljava/lang/Class;)LX/0RK;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/0RI;-><init>(Ljava/lang/reflect/Type;LX/0RK;)V

    return-object v0
.end method

.method public static a(Ljava/lang/annotation/Annotation;)LX/0RK;
    .locals 2

    .prologue
    .line 59861
    const-string v0, "annotation"

    invoke-static {p0, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59862
    invoke-interface {p0}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v1

    .line 59863
    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 59864
    if-eqz v0, :cond_0

    .line 59865
    new-instance v0, LX/0RW;

    invoke-direct {v0, v1, p0}, LX/0RW;-><init>(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)V

    .line 59866
    :goto_1
    return-object v0

    :cond_0
    new-instance v0, LX/52V;

    .line 59867
    instance-of v1, p0, Ljavax/inject/Named;

    if-eqz v1, :cond_1

    .line 59868
    check-cast p0, Ljavax/inject/Named;

    invoke-interface {p0}, Ljavax/inject/Named;->value()Ljava/lang/String;

    move-result-object v1

    .line 59869
    new-instance p0, LX/52a;

    invoke-direct {p0, v1}, LX/52a;-><init>(Ljava/lang/String;)V

    move-object p0, p0

    .line 59870
    :cond_1
    move-object v1, p0

    .line 59871
    invoke-direct {v0, v1}, LX/52V;-><init>(Ljava/lang/annotation/Annotation;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/Class;)LX/0RK;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0RK;"
        }
    .end annotation

    .prologue
    .line 59835
    const-string v0, "annotation type"

    invoke-static {p0, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59836
    new-instance v0, LX/0RW;

    .line 59837
    const-class v1, Ljavax/inject/Named;

    if-ne p0, v1, :cond_0

    .line 59838
    const-class p0, Lcom/google/inject/name/Named;

    .line 59839
    :cond_0
    move-object v1, p0

    .line 59840
    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/0RW;-><init>(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)V

    return-object v0
.end method

.method private d()I
    .locals 2

    .prologue
    .line 59834
    iget-object v0, p0, LX/0RI;->b:LX/0RL;

    invoke-virtual {v0}, LX/0RL;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/0RI;->a:LX/0RK;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59833
    iget-object v0, p0, LX/0RI;->a:LX/0RK;

    invoke-interface {v0}, LX/0RK;->getAnnotationType()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/annotation/Annotation;
    .locals 1

    .prologue
    .line 59832
    iget-object v0, p0, LX/0RI;->a:LX/0RK;

    invoke-interface {v0}, LX/0RK;->getAnnotation()Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 59826
    if-ne p1, p0, :cond_1

    .line 59827
    :cond_0
    :goto_0
    return v0

    .line 59828
    :cond_1
    instance-of v2, p1, LX/0RI;

    if-nez v2, :cond_2

    move v0, v1

    .line 59829
    goto :goto_0

    .line 59830
    :cond_2
    check-cast p1, LX/0RI;

    .line 59831
    iget-object v2, p0, LX/0RI;->a:LX/0RK;

    iget-object v3, p1, LX/0RI;->a:LX/0RK;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/0RI;->b:LX/0RL;

    iget-object v3, p1, LX/0RI;->b:LX/0RL;

    invoke-virtual {v2, v3}, LX/0RL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 59825
    iget v0, p0, LX/0RI;->c:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59824
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Key[type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0RI;->b:LX/0RL;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", annotation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0RI;->a:LX/0RK;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
