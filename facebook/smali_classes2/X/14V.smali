.class public final enum LX/14V;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/14V;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/14V;

.field public static final enum BOOTSTRAP:LX/14V;

.field public static final enum DEFAULT:LX/14V;

.field public static final enum PROD:LX/14V;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 178913
    new-instance v0, LX/14V;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, LX/14V;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/14V;->DEFAULT:LX/14V;

    .line 178914
    new-instance v0, LX/14V;

    const-string v1, "PROD"

    invoke-direct {v0, v1, v3}, LX/14V;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/14V;->PROD:LX/14V;

    .line 178915
    new-instance v0, LX/14V;

    const-string v1, "BOOTSTRAP"

    invoke-direct {v0, v1, v4}, LX/14V;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/14V;->BOOTSTRAP:LX/14V;

    .line 178916
    const/4 v0, 0x3

    new-array v0, v0, [LX/14V;

    sget-object v1, LX/14V;->DEFAULT:LX/14V;

    aput-object v1, v0, v2

    sget-object v1, LX/14V;->PROD:LX/14V;

    aput-object v1, v0, v3

    sget-object v1, LX/14V;->BOOTSTRAP:LX/14V;

    aput-object v1, v0, v4

    sput-object v0, LX/14V;->$VALUES:[LX/14V;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 178912
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/14V;
    .locals 1

    .prologue
    .line 178911
    const-class v0, LX/14V;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/14V;

    return-object v0
.end method

.method public static values()[LX/14V;
    .locals 1

    .prologue
    .line 178910
    sget-object v0, LX/14V;->$VALUES:[LX/14V;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/14V;

    return-object v0
.end method
