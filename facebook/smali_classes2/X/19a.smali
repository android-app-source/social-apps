.class public LX/19a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/19a;


# instance fields
.field public final a:J

.field public final b:I


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 4
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 208141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208142
    sget-wide v0, LX/0ws;->eI:J

    const-wide/16 v2, 0x14

    invoke-interface {p1, v0, v1, v2, v3}, LX/0ad;->a(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/19a;->a:J

    .line 208143
    sget v0, LX/0ws;->eH:I

    const/4 v1, 0x5

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/19a;->b:I

    .line 208144
    return-void
.end method

.method public static a(LX/0QB;)LX/19a;
    .locals 4

    .prologue
    .line 208145
    sget-object v0, LX/19a;->c:LX/19a;

    if-nez v0, :cond_1

    .line 208146
    const-class v1, LX/19a;

    monitor-enter v1

    .line 208147
    :try_start_0
    sget-object v0, LX/19a;->c:LX/19a;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 208148
    if-eqz v2, :cond_0

    .line 208149
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 208150
    new-instance p0, LX/19a;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/19a;-><init>(LX/0ad;)V

    .line 208151
    move-object v0, p0

    .line 208152
    sput-object v0, LX/19a;->c:LX/19a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208153
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 208154
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 208155
    :cond_1
    sget-object v0, LX/19a;->c:LX/19a;

    return-object v0

    .line 208156
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 208157
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
