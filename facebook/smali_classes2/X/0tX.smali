.class public LX/0tX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/graphql/executor/GraphQLResult;

.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final c:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public static volatile t:Ljava/util/concurrent/locks/ReadWriteLock;

.field private static final u:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static v:LX/0Xm;


# instance fields
.field public final d:LX/0TD;

.field private final e:LX/0tc;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0sf;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0SI;

.field public final h:LX/0SI;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3G7;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0td;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1ku;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Kz;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Sj;

.field private final n:LX/0tg;

.field private final o:LX/0th;

.field private final p:LX/0ti;

.field private final q:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final r:LX/0ad;

.field private final s:LX/0tk;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 154761
    const-class v0, LX/0tX;

    sput-object v0, LX/0tX;->b:Ljava/lang/Class;

    .line 154762
    new-instance v0, LX/0tY;

    invoke-direct {v0}, LX/0tY;-><init>()V

    sput-object v0, LX/0tX;->c:LX/0QK;

    .line 154763
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    sput-object v0, LX/0tX;->t:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 154764
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, LX/0tX;->u:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 154765
    new-instance v0, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    sget-object v2, LX/0ta;->NO_DATA:LX/0ta;

    const-wide/16 v4, 0x0

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;J)V

    sput-object v0, LX/0tX;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    return-void
.end method

.method public constructor <init>(LX/0TD;LX/0tc;LX/0Ot;LX/0SI;LX/0SI;LX/0Ot;LX/0Ot;LX/0Sj;LX/0Ot;LX/0td;LX/0tg;LX/0th;LX/0ti;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;LX/0tk;)V
    .locals 4
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .param p5    # LX/0SI;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TD;",
            "LX/0tc;",
            "LX/0Ot",
            "<",
            "LX/0sf;",
            ">;",
            "LX/0SI;",
            "LX/0SI;",
            "LX/0Ot",
            "<",
            "LX/1ku;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Kz;",
            ">;",
            "LX/0Sj;",
            "LX/0Ot",
            "<",
            "LX/3G7;",
            ">;",
            "LX/0td;",
            "LX/0tg;",
            "LX/0th;",
            "LX/0ti;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0ad;",
            "LX/0tk;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 154766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154767
    iput-object p1, p0, LX/0tX;->d:LX/0TD;

    .line 154768
    iput-object p2, p0, LX/0tX;->e:LX/0tc;

    .line 154769
    iput-object p3, p0, LX/0tX;->f:LX/0Ot;

    .line 154770
    iput-object p4, p0, LX/0tX;->g:LX/0SI;

    .line 154771
    iput-object p5, p0, LX/0tX;->h:LX/0SI;

    .line 154772
    iput-object p6, p0, LX/0tX;->k:LX/0Ot;

    .line 154773
    iput-object p7, p0, LX/0tX;->l:LX/0Ot;

    .line 154774
    iput-object p8, p0, LX/0tX;->m:LX/0Sj;

    .line 154775
    iput-object p9, p0, LX/0tX;->i:LX/0Ot;

    .line 154776
    iput-object p10, p0, LX/0tX;->j:LX/0td;

    .line 154777
    iput-object p11, p0, LX/0tX;->n:LX/0tg;

    .line 154778
    move-object/from16 v0, p12

    iput-object v0, p0, LX/0tX;->o:LX/0th;

    .line 154779
    move-object/from16 v0, p13

    iput-object v0, p0, LX/0tX;->p:LX/0ti;

    .line 154780
    move-object/from16 v0, p14

    iput-object v0, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 154781
    move-object/from16 v0, p15

    iput-object v0, p0, LX/0tX;->r:LX/0ad;

    .line 154782
    move-object/from16 v0, p16

    iput-object v0, p0, LX/0tX;->s:LX/0tk;

    .line 154783
    iget-object v1, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310026    # 4.499993E-39f

    const/16 v3, 0xfa

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(II)V

    .line 154784
    iget-object v1, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310029    # 4.499997E-39f

    const/16 v3, 0xfa

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(II)V

    .line 154785
    iget-object v1, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310027    # 4.499994E-39f

    const/16 v3, 0xfa

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(II)V

    .line 154786
    return-void
.end method

.method public static a(LX/0QB;)LX/0tX;
    .locals 3

    .prologue
    .line 154787
    const-class v1, LX/0tX;

    monitor-enter v1

    .line 154788
    :try_start_0
    sget-object v0, LX/0tX;->v:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 154789
    sput-object v2, LX/0tX;->v:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 154790
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154791
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->b(LX/0QB;)LX/0tX;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 154792
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/0tX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154793
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 154794
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/0tX;LX/0zO;LX/1kt;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 8
    .param p1    # LX/0zO;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;",
            "LX/1kt",
            "<TT;>;)",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 154795
    if-nez p2, :cond_0

    .line 154796
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must call GraphQLRequest.setCacheProcessor() for this to do anything"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154797
    :cond_0
    new-instance v3, LX/1NA;

    iget-object v1, p0, LX/0tX;->e:LX/0tc;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, LX/1NC;

    const/4 v4, 0x0

    invoke-direct {v2, v4}, LX/1NC;-><init>(Z)V

    invoke-direct {v3, v1, v2}, LX/1NA;-><init>(LX/0tc;LX/1NC;)V

    .line 154798
    sget-object v6, LX/1NE;->MEMORY_CACHE:LX/1NE;

    iput-object v6, v3, LX/1NA;->d:LX/1NE;

    .line 154799
    iget-object v6, v3, LX/1NA;->a:LX/0tc;

    iget-wide v6, v6, LX/0tc;->d:J

    iput-wide v6, v3, LX/1NA;->b:J

    .line 154800
    iget-object v1, p0, LX/0tX;->g:LX/0SI;

    invoke-interface {v1}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 154801
    iget-object v2, p1, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v2, v2

    .line 154802
    if-eqz v2, :cond_8

    .line 154803
    iget-object v1, p1, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v1, v1

    .line 154804
    move-object v2, v1

    .line 154805
    :goto_0
    if-eqz v2, :cond_1

    .line 154806
    iget-object v1, p0, LX/0tX;->h:LX/0SI;

    invoke-interface {v1, v2}, LX/0SI;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;

    .line 154807
    :cond_1
    :try_start_0
    invoke-interface {p2}, LX/1kt;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v1

    .line 154808
    sget-object v4, LX/0tX;->a:Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v1, v4, :cond_2

    if-nez v1, :cond_4

    .line 154809
    :cond_2
    if-eqz v2, :cond_3

    .line 154810
    iget-object v1, p0, LX/0tX;->h:LX/0SI;

    invoke-interface {v1}, LX/0SI;->f()V

    :cond_3
    :goto_1
    return-object v0

    .line 154811
    :cond_4
    :try_start_1
    iget-object v4, v3, LX/1NB;->f:LX/1ND;

    invoke-virtual {v1}, Lcom/facebook/graphql/executor/GraphQLResult;->e()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1ND;->a(Ljava/util/Set;)LX/1ND;

    .line 154812
    invoke-virtual {v3, v1}, LX/1NB;->c(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v1

    .line 154813
    iget-object v3, v1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v3, v3

    .line 154814
    sget-object v4, LX/0ta;->FROM_CACHE_STALE:LX/0ta;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-ne v3, v4, :cond_5

    .line 154815
    if-eqz v2, :cond_3

    .line 154816
    iget-object v1, p0, LX/0tX;->h:LX/0SI;

    invoke-interface {v1}, LX/0SI;->f()V

    goto :goto_1

    .line 154817
    :cond_5
    if-eqz v2, :cond_6

    .line 154818
    iget-object v0, p0, LX/0tX;->h:LX/0SI;

    invoke-interface {v0}, LX/0SI;->f()V

    :cond_6
    move-object v0, v1

    goto :goto_1

    .line 154819
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_7

    .line 154820
    iget-object v1, p0, LX/0tX;->h:LX/0SI;

    invoke-interface {v1}, LX/0SI;->f()V

    :cond_7
    throw v0

    :cond_8
    move-object v2, v1

    goto :goto_0
.end method

.method private a(LX/3G4;LX/3Fz;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3G4;",
            "LX/3Fz;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    const v8, 0x310029    # 4.499997E-39f

    .line 154821
    :try_start_0
    invoke-virtual {p1}, LX/3G4;->d()LX/399;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 154822
    iget-object v0, v2, LX/399;->f:LX/0Px;

    move-object v0, v0

    .line 154823
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 154824
    sget-object v0, LX/3Fz;->a:LX/3Fz;

    if-ne p2, v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    const-string v1, "File attachments not yet supported with offline retries"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 154825
    :cond_0
    invoke-static {}, LX/0tX;->c()I

    move-result v6

    .line 154826
    iget-object v0, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v8, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 154827
    iget-object v0, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "mutation_name"

    iget-object v3, v2, LX/399;->a:LX/0zP;

    .line 154828
    iget-object v4, v3, LX/0gW;->f:Ljava/lang/String;

    move-object v3, v4

    .line 154829
    invoke-interface {v0, v8, v6, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 154830
    sget-object v0, LX/3Fz;->a:LX/3Fz;

    if-eq p2, v0, :cond_1

    .line 154831
    iget-object v0, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "offline_supported"

    invoke-interface {v0, v8, v6, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 154832
    iget-object v0, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "attempt_number"

    iget v3, p1, LX/3G3;->f:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v8, v6, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 154833
    :cond_1
    if-nez v2, :cond_6

    .line 154834
    :goto_1
    iget-object v0, p0, LX/0tX;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Kz;

    .line 154835
    invoke-virtual {v0}, LX/2Kz;->init()V

    .line 154836
    iget-object v1, v0, LX/2Kz;->r:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/37X;

    move-object v4, v1

    .line 154837
    if-eqz v4, :cond_4

    .line 154838
    :goto_2
    new-instance v0, LX/3G6;

    sget-object v5, LX/0tX;->t:Ljava/util/concurrent/locks/ReadWriteLock;

    const/4 v7, 0x0

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v7}, LX/3G6;-><init>(LX/3G4;LX/399;LX/3Fz;LX/37X;Ljava/util/concurrent/locks/ReadWriteLock;ILX/4V2;)V

    .line 154839
    iget-object v1, v2, LX/399;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v1, v1

    .line 154840
    if-nez v1, :cond_2

    iget-object v1, p0, LX/0tX;->g:LX/0SI;

    invoke-interface {v1}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 154841
    iget-object v1, p0, LX/0tX;->g:LX/0SI;

    invoke-interface {v1}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 154842
    iput-object v1, v2, LX/399;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 154843
    :cond_2
    if-eqz p3, :cond_5

    .line 154844
    iget-object v1, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "service_enabled"

    invoke-interface {v1, v8, v6, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 154845
    iget-object v1, p0, LX/0tX;->j:LX/0td;

    invoke-virtual {v1, v0}, LX/0td;->a(LX/3G6;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 154846
    :goto_3
    return-object v0

    .line 154847
    :catch_0
    move-exception v0

    .line 154848
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 154849
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 154850
    :cond_4
    iget-object v0, p0, LX/0tX;->e:LX/0tc;

    iget-object v1, p0, LX/0tX;->s:LX/0tk;

    invoke-virtual {v1, v2}, LX/0tk;->a(LX/399;)LX/3Bq;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tc;->a(LX/3Bq;)LX/37X;

    move-result-object v4

    goto :goto_2

    .line 154851
    :cond_5
    iget-object v1, p0, LX/0tX;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3G7;

    .line 154852
    const/4 v2, 0x0

    .line 154853
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v3

    .line 154854
    iget-object v4, v1, LX/3G7;->a:LX/0TD;

    new-instance v5, Lcom/facebook/graphql/executor/MutationRunner$1;

    invoke-direct {v5, v1, v0, v3, v2}, Lcom/facebook/graphql/executor/MutationRunner$1;-><init>(LX/3G7;LX/3G6;Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/String;)V

    const v6, 0x2c96a549

    invoke-static {v4, v5, v6}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 154855
    move-object v2, v3

    .line 154856
    move-object v0, v2

    .line 154857
    goto :goto_3

    .line 154858
    :cond_6
    iget-object v0, v2, LX/399;->a:LX/0zP;

    const-string v1, "client_mutation_id"

    sget-object v3, LX/0sO;->b:Ljava/util/concurrent/Callable;

    invoke-static {v0, v1, v3}, LX/0sO;->a(LX/0zP;Ljava/lang/String;Ljava/util/concurrent/Callable;)V

    .line 154859
    iget-object v0, v2, LX/399;->a:LX/0zP;

    const-string v1, "actor_id"

    new-instance v3, LX/3G5;

    invoke-direct {v3, p0, v2}, LX/3G5;-><init>(LX/0tX;LX/399;)V

    invoke-static {v0, v1, v3}, LX/0sO;->a(LX/0zP;Ljava/lang/String;Ljava/util/concurrent/Callable;)V

    goto/16 :goto_1
.end method

.method public static a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 154860
    new-instance v0, LX/4Uo;

    invoke-direct {v0}, LX/4Uo;-><init>()V

    invoke-static {p0, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/0QB;)LX/0tX;
    .locals 18

    .prologue
    .line 154861
    new-instance v1, LX/0tX;

    invoke-static/range {p0 .. p0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/0tc;->a(LX/0QB;)LX/0tc;

    move-result-object v3

    check-cast v3, LX/0tc;

    const/16 v4, 0xaf7

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/0WG;->a(LX/0QB;)LX/0SI;

    move-result-object v5

    check-cast v5, LX/0SI;

    invoke-interface/range {p0 .. p0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v6

    invoke-static {v6}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v6

    check-cast v6, LX/0SI;

    const/16 v7, 0xaf5

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xb06

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v9

    check-cast v9, LX/0Sj;

    const/16 v10, 0xb04

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static/range {p0 .. p0}, LX/0td;->a(LX/0QB;)LX/0td;

    move-result-object v11

    check-cast v11, LX/0td;

    const-class v12, LX/0tg;

    move-object/from16 v0, p0

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/0tg;

    const-class v13, LX/0th;

    move-object/from16 v0, p0

    invoke-interface {v0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/0th;

    invoke-static/range {p0 .. p0}, LX/0ti;->a(LX/0QB;)LX/0ti;

    move-result-object v14

    check-cast v14, LX/0ti;

    invoke-static/range {p0 .. p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v15

    check-cast v15, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v16

    check-cast v16, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0tk;->a(LX/0QB;)LX/0tk;

    move-result-object v17

    check-cast v17, LX/0tk;

    invoke-direct/range {v1 .. v17}, LX/0tX;-><init>(LX/0TD;LX/0tc;LX/0Ot;LX/0SI;LX/0SI;LX/0Ot;LX/0Ot;LX/0Sj;LX/0Ot;LX/0td;LX/0tg;LX/0th;LX/0ti;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;LX/0tk;)V

    .line 154862
    return-object v1
.end method

.method public static b(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TR;>;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 154863
    new-instance v0, LX/4Up;

    invoke-direct {v0}, LX/4Up;-><init>()V

    invoke-static {p0, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private b(LX/0v6;)V
    .locals 5

    .prologue
    const v4, 0x310027    # 4.499994E-39f

    .line 154864
    iget v0, p1, LX/0v6;->n:I

    move v0, v0

    .line 154865
    iget-object v1, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v4, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 154866
    iget-object v1, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v4, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154867
    iget-object v1, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "batch_name"

    .line 154868
    iget-object v3, p1, LX/0v6;->c:Ljava/lang/String;

    move-object v3, v3

    .line 154869
    invoke-interface {v1, v4, v0, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 154870
    iget-object v1, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "num_queries"

    .line 154871
    iget-object v3, p1, LX/0v6;->b:Ljava/util/List;

    move-object v3, v3

    .line 154872
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v4, v0, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 154873
    :cond_0
    return-void
.end method

.method public static c()I
    .locals 1

    .prologue
    .line 154874
    sget-object v0, LX/0tX;->u:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    return v0
.end method

.method private c(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 154721
    invoke-virtual {p1}, LX/0zO;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154722
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "GraphQLQueryExecutor.start() cannot be used with mutations, use .mutate() instead"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154723
    :cond_0
    iget-object v0, p0, LX/0tX;->r:LX/0ad;

    sget-short v1, LX/1NG;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154724
    const/4 v0, 0x1

    .line 154725
    iput-boolean v0, p1, LX/0zO;->q:Z

    .line 154726
    :cond_1
    const-string v0, "GraphQLQueryExecutor.startInner"

    const v1, -0x640555b9

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 154727
    :try_start_0
    iget-object v0, p1, LX/0zO;->g:LX/1kt;

    .line 154728
    if-nez v0, :cond_6

    .line 154729
    iget-object v0, p0, LX/0tX;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sf;

    invoke-virtual {v0, p1}, LX/0sf;->a(LX/0zO;)LX/1ks;

    move-result-object v0

    move-object v1, v0

    .line 154730
    :goto_0
    iget v0, p1, LX/0zO;->w:I

    move v2, v0

    .line 154731
    iget-object v0, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x310026    # 4.499993E-39f

    invoke-interface {v0, v3, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 154732
    iget-object v0, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x310026    # 4.499993E-39f

    const-string v4, "query_name"

    .line 154733
    iget-object v5, p1, LX/0zO;->m:LX/0gW;

    move-object v5, v5

    .line 154734
    iget-object v6, v5, LX/0gW;->f:Ljava/lang/String;

    move-object v5, v6

    .line 154735
    invoke-interface {v0, v3, v2, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 154736
    iget-object v0, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x310026    # 4.499993E-39f

    const-string v4, "cache_policy"

    .line 154737
    iget-object v5, p1, LX/0zO;->a:LX/0zS;

    move-object v5, v5

    .line 154738
    iget-object v5, v5, LX/0zS;->f:Ljava/lang/String;

    invoke-interface {v0, v3, v2, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 154739
    iget-boolean v0, p1, LX/0zO;->p:Z

    move v0, v0

    .line 154740
    if-eqz v0, :cond_2

    .line 154741
    iget-object v0, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x310026    # 4.499993E-39f

    const-string v4, "consistency_enabled"

    invoke-interface {v0, v3, v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 154742
    :cond_2
    iget-object v0, p1, LX/0zO;->z:Ljava/lang/String;

    move-object v0, v0

    .line 154743
    iget-object v3, p1, LX/0zO;->m:LX/0gW;

    move-object v3, v3

    .line 154744
    iget-object v4, v3, LX/0gW;->f:Ljava/lang/String;

    move-object v3, v4

    .line 154745
    invoke-static {v0, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 154746
    iget-object v0, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x310026    # 4.499993E-39f

    const-string v4, "logging_token"

    .line 154747
    iget-object v5, p1, LX/0zO;->z:Ljava/lang/String;

    move-object v5, v5

    .line 154748
    invoke-interface {v0, v3, v2, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 154749
    :cond_3
    invoke-virtual {p1}, LX/0zO;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 154750
    iget-object v0, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x310026    # 4.499993E-39f

    const-string v4, "scrape_consistency_enabled"

    invoke-interface {v0, v3, v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 154751
    :cond_4
    iget-boolean v0, p1, LX/0zO;->k:Z

    if-eqz v0, :cond_5

    .line 154752
    iget-object v0, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x310026    # 4.499993E-39f

    const-string v4, "subscription_rerun"

    invoke-interface {v0, v3, v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 154753
    :cond_5
    iget-object v0, p0, LX/0tX;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ku;

    sget-object v3, LX/0tX;->t:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-virtual {v0, v3, p1, v1, v2}, LX/1ku;->a(Ljava/util/concurrent/locks/ReadWriteLock;LX/0zO;LX/1kt;I)Lcom/facebook/graphql/executor/CacheReadRunner;

    move-result-object v0

    .line 154754
    new-instance v1, LX/1ky;

    invoke-direct {v1}, LX/1ky;-><init>()V

    .line 154755
    iget-object v2, p0, LX/0tX;->d:LX/0TD;

    invoke-interface {v2, v0}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 154756
    iput-object v2, v1, LX/1ky;->a:Ljava/util/concurrent/Future;

    .line 154757
    iget-object v2, v0, Lcom/facebook/graphql/executor/CacheReadRunner;->l:Lcom/google/common/util/concurrent/SettableFuture;

    move-object v2, v2

    .line 154758
    move-object v0, v2

    .line 154759
    new-instance v2, LX/1kz;

    invoke-direct {v2, p0, v1}, LX/1kz;-><init>(LX/0tX;LX/1ky;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    invoke-static {v0, v2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154760
    const v1, -0x3b30bed4

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0x66b3ef70

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_6
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public static c(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TR;>;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 154666
    new-instance v0, LX/4Uq;

    invoke-direct {v0}, LX/4Uq;-><init>()V

    invoke-static {p0, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static d(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/16i;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TR;>;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "TT;>;>;"
        }
    .end annotation

    .prologue
    .line 154875
    new-instance v0, LX/4Ur;

    invoke-direct {v0}, LX/4Ur;-><init>()V

    invoke-static {p0, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/0zO;)LX/1Zp;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;)",
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 154667
    invoke-direct {p0, p1}, LX/0tX;->c(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 154668
    new-instance v1, LX/1Zp;

    invoke-direct {v1, v0, p1}, LX/1Zp;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0zO;)V

    return-object v1
.end method

.method public final a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/399",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 154669
    sget-object v0, LX/3Fz;->a:LX/3Fz;

    invoke-virtual {p0, p1, v0}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/399",
            "<TT;>;",
            "LX/3Fz;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 154670
    new-instance v0, LX/3G1;

    invoke-direct {v0}, LX/3G1;-><init>()V

    invoke-virtual {v0, p1}, LX/3G1;->a(LX/399;)LX/3G1;

    move-result-object v0

    invoke-virtual {v0}, LX/3G1;->b()LX/3G4;

    move-result-object v0

    .line 154671
    invoke-virtual {p0, v0, p2}, LX/0tX;->a(LX/3G4;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/3G4;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3G4;",
            "LX/3Fz;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 154672
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/0tX;->a(LX/3G4;LX/3Fz;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/4V2;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/4V2",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const v4, 0x310029    # 4.499997E-39f

    .line 154673
    invoke-static {}, LX/0tX;->c()I

    move-result v6

    .line 154674
    iget-object v0, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v4, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 154675
    iget-object v0, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "mutation_name"

    invoke-interface {p1}, LX/4V2;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v4, v6, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 154676
    iget-object v0, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "legacy"

    invoke-interface {v0, v4, v6, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 154677
    iget-object v0, p0, LX/0tX;->e:LX/0tc;

    iget-object v2, p0, LX/0tX;->s:LX/0tk;

    const/4 v3, 0x0

    .line 154678
    invoke-interface {p1}, LX/4V2;->b()LX/0jT;

    move-result-object v4

    .line 154679
    if-eqz v4, :cond_0

    .line 154680
    invoke-static {v2, v3, v4, v3}, LX/0tk;->a(LX/0tk;LX/0Rf;LX/0jT;LX/2lk;)LX/3Bq;

    move-result-object v3

    .line 154681
    :cond_0
    move-object v2, v3

    .line 154682
    invoke-virtual {v0, v2}, LX/0tc;->a(LX/3Bq;)LX/37X;

    move-result-object v4

    .line 154683
    new-instance v0, LX/3G6;

    sget-object v3, LX/3Fz;->a:LX/3Fz;

    sget-object v5, LX/0tX;->t:Ljava/util/concurrent/locks/ReadWriteLock;

    move-object v2, v1

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, LX/3G6;-><init>(LX/3G4;LX/399;LX/3Fz;LX/37X;Ljava/util/concurrent/locks/ReadWriteLock;ILX/4V2;)V

    .line 154684
    iget-object v1, p0, LX/0tX;->j:LX/0td;

    invoke-virtual {v1, v0}, LX/0td;->a(LX/3G6;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0v6;)V
    .locals 1

    .prologue
    .line 154685
    iget-object v0, p0, LX/0tX;->d:LX/0TD;

    invoke-virtual {p0, p1, v0}, LX/0tX;->a(LX/0v6;Ljava/util/concurrent/ExecutorService;)V

    .line 154686
    return-void
.end method

.method public final a(LX/0v6;Ljava/util/concurrent/ExecutorService;)V
    .locals 2

    .prologue
    .line 154687
    invoke-direct {p0, p1}, LX/0tX;->b(LX/0v6;)V

    .line 154688
    iget-object v0, p0, LX/0tX;->n:LX/0tg;

    sget-object v1, LX/0tX;->t:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-virtual {v0, v1, p1}, LX/0tg;->a(Ljava/util/concurrent/locks/ReadWriteLock;LX/0v6;)Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    move-result-object v0

    .line 154689
    const v1, 0xaca74b0

    invoke-static {p2, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 154690
    return-void
.end method

.method public final a(LX/37X;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 154691
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/0tX;->a(LX/37X;LX/2lk;)V

    .line 154692
    return-void
.end method

.method public final a(LX/37X;LX/2lk;)V
    .locals 2
    .param p2    # LX/2lk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 154693
    iget-object v0, p0, LX/0tX;->g:LX/0SI;

    invoke-interface {v0}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 154694
    if-eqz v1, :cond_0

    .line 154695
    iget-object v0, p0, LX/0tX;->h:LX/0SI;

    invoke-interface {v0, v1}, LX/0SI;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;

    .line 154696
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/0tX;->p:LX/0ti;

    invoke-virtual {v0, p1}, LX/0ti;->a(LX/37X;)V

    .line 154697
    if-eqz p2, :cond_1

    invoke-interface {p2}, LX/2lk;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 154698
    invoke-interface {p2}, LX/2lk;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154699
    :cond_1
    if-eqz v1, :cond_2

    .line 154700
    iget-object v0, p0, LX/0tX;->h:LX/0SI;

    invoke-interface {v0}, LX/0SI;->f()V

    .line 154701
    :cond_2
    return-void

    .line 154702
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 154703
    iget-object v1, p0, LX/0tX;->h:LX/0SI;

    invoke-interface {v1}, LX/0SI;->f()V

    :cond_3
    throw v0
.end method

.method public final b(LX/0zO;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;)",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 154704
    iget-object v0, p1, LX/0zO;->g:LX/1kt;

    invoke-static {p0, p1, v0}, LX/0tX;->a(LX/0tX;LX/0zO;LX/1kt;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/399",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 154705
    new-instance v0, LX/3G1;

    invoke-direct {v0}, LX/3G1;-><init>()V

    invoke-virtual {v0, p1}, LX/3G1;->a(LX/399;)LX/3G1;

    move-result-object v0

    invoke-virtual {v0}, LX/3G1;->b()LX/3G4;

    move-result-object v0

    .line 154706
    sget-object v1, LX/3Fz;->a:LX/3Fz;

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, LX/0tX;->a(LX/3G4;LX/3Fz;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/0v6;Ljava/util/concurrent/ExecutorService;)V
    .locals 11

    .prologue
    .line 154707
    iget-object v0, p0, LX/0tX;->g:LX/0SI;

    invoke-interface {v0}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 154708
    invoke-direct {p0, p1}, LX/0tX;->b(LX/0v6;)V

    .line 154709
    iget-object v1, p0, LX/0tX;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310027    # 4.499994E-39f

    .line 154710
    iget v3, p1, LX/0v6;->n:I

    move v3, v3

    .line 154711
    const-string v4, "network_only_runner"

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 154712
    iget-object v1, p0, LX/0tX;->o:LX/0th;

    .line 154713
    new-instance v5, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;

    invoke-static {v1}, LX/11E;->b(LX/0QB;)LX/11E;

    move-result-object v7

    check-cast v7, LX/11E;

    invoke-static {v1}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v8

    check-cast v8, LX/11H;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v1}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v10

    check-cast v10, Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-object v6, p1

    invoke-direct/range {v5 .. v10}, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;-><init>(LX/0v6;LX/11E;LX/11H;LX/03V;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 154714
    move-object v1, v5

    .line 154715
    if-nez p2, :cond_0

    .line 154716
    iget-object p2, p0, LX/0tX;->d:LX/0TD;

    .line 154717
    :cond_0
    if-eqz v0, :cond_1

    .line 154718
    new-instance v2, Lcom/facebook/graphql/executor/GraphQLQueryExecutor$5;

    invoke-direct {v2, p0, v0, v1}, Lcom/facebook/graphql/executor/GraphQLQueryExecutor$5;-><init>(LX/0tX;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;)V

    const v0, -0x1f7b5096

    invoke-static {p2, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 154719
    :goto_0
    return-void

    .line 154720
    :cond_1
    const v0, 0x44c13622

    invoke-static {p2, v1, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method
