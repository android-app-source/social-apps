.class public LX/14f;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 179066
    const-class v0, LX/14f;

    sput-object v0, LX/14f;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 179067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Lorg/apache/http/client/methods/HttpUriRequest;Lcom/facebook/http/interfaces/RequestPriority;LX/14Q;Lorg/apache/http/client/ResponseHandler;LX/14U;Lcom/facebook/common/callercontext/CallerContext;LX/14P;)LX/15D;
    .locals 3
    .param p6    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            "LX/14Q;",
            "Lorg/apache/http/client/ResponseHandler",
            "<TT;>;",
            "LX/14U;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/14P;",
            ")",
            "LX/15D",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 179068
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v0

    .line 179069
    iput-object p0, v0, LX/15E;->c:Ljava/lang/String;

    .line 179070
    move-object v0, v0

    .line 179071
    iput-object p1, v0, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 179072
    move-object v0, v0

    .line 179073
    iput-object p2, v0, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 179074
    move-object v0, v0

    .line 179075
    iput-object p3, v0, LX/15E;->f:LX/14Q;

    .line 179076
    move-object v0, v0

    .line 179077
    iput-object p4, v0, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 179078
    move-object v0, v0

    .line 179079
    iput-object p7, v0, LX/15E;->j:LX/14P;

    .line 179080
    move-object v1, v0

    .line 179081
    iget-object v0, p5, LX/14U;->d:Ljava/util/List;

    move-object v0, v0

    .line 179082
    if-eqz v0, :cond_0

    .line 179083
    invoke-virtual {v1, v0}, LX/15E;->a(Ljava/util/List;)LX/15E;

    .line 179084
    :cond_0
    iget-object v0, p5, LX/14U;->a:LX/4ck;

    move-object v0, v0

    .line 179085
    if-eqz v0, :cond_1

    instance-of v2, v0, LX/2BD;

    if-eqz v2, :cond_1

    .line 179086
    check-cast v0, LX/2BD;

    .line 179087
    iput-object v0, v1, LX/15E;->q:LX/2BD;

    .line 179088
    :cond_1
    if-eqz p6, :cond_2

    .line 179089
    iput-object p6, v1, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 179090
    :cond_2
    invoke-virtual {v1}, LX/15E;->a()LX/15D;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lorg/apache/http/HttpEntity;)Lorg/apache/http/HttpEntity;
    .locals 2

    .prologue
    .line 179091
    invoke-interface {p0}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v0

    const-string v1, "Unexpected entity with no Content-Type defined"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/Header;

    .line 179092
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 179093
    const-string v1, "application/x-www-form-urlencoded"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179094
    new-instance v0, LX/14g;

    invoke-direct {v0, p0}, LX/14g;-><init>(Lorg/apache/http/HttpEntity;)V

    move-object p0, v0

    .line 179095
    :cond_0
    return-object p0
.end method

.method public static a(Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 2

    .prologue
    .line 179096
    instance-of v0, p0, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-eqz v0, :cond_1

    .line 179097
    check-cast p0, Lorg/apache/http/HttpEntityEnclosingRequest;

    invoke-interface {p0}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 179098
    :goto_0
    instance-of v1, v0, LX/14h;

    if-eqz v1, :cond_0

    .line 179099
    check-cast v0, LX/14h;

    invoke-virtual {v0}, LX/14h;->a()Lorg/apache/http/HttpEntity;

    move-result-object v0

    goto :goto_0

    .line 179100
    :cond_0
    instance-of v1, v0, LX/14e;

    if-eqz v1, :cond_1

    .line 179101
    check-cast v0, LX/14e;

    invoke-interface {v0}, LX/14e;->a()V

    .line 179102
    :cond_1
    return-void
.end method
