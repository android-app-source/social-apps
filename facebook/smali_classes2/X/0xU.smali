.class public LX/0xU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0hL;

.field private final b:Z

.field private final c:Z

.field private final d:Z

.field private e:LX/0xW;

.field private f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0xC;


# direct methods
.method public constructor <init>(LX/0hL;Ljava/lang/Boolean;LX/0xW;LX/0Or;LX/0xX;LX/0xC;Ljava/lang/Boolean;)V
    .locals 1
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/work/groupstab/annotations/ShouldShowWorkGroupTab;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/fbreact/annotations/IsFb4aReactNativeEnabled;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/work/reducedinstance/bridge/IsNewsFeedDisabledForWork;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0hL;",
            "Ljava/lang/Boolean;",
            "LX/0xW;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0xX;",
            "LX/0xC;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 163111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163112
    iput-object p1, p0, LX/0xU;->a:LX/0hL;

    .line 163113
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/0xU;->b:Z

    .line 163114
    iput-object p3, p0, LX/0xU;->e:LX/0xW;

    .line 163115
    iput-object p4, p0, LX/0xU;->f:LX/0Or;

    .line 163116
    invoke-virtual {p5}, LX/0xX;->a()Z

    move-result v0

    iput-boolean v0, p0, LX/0xU;->c:Z

    .line 163117
    iput-object p6, p0, LX/0xU;->g:LX/0xC;

    .line 163118
    invoke-virtual {p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/0xU;->d:Z

    .line 163119
    return-void
.end method

.method public static b(LX/0QB;)LX/0xU;
    .locals 8

    .prologue
    .line 163120
    new-instance v0, LX/0xU;

    invoke-static {p0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v1

    check-cast v1, LX/0hL;

    .line 163121
    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v2, v3}, LX/0xV;->a(Ljava/lang/Boolean;LX/0Uh;)Ljava/lang/Boolean;

    move-result-object v2

    move-object v2, v2

    .line 163122
    check-cast v2, Ljava/lang/Boolean;

    invoke-static {p0}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v3

    check-cast v3, LX/0xW;

    const/16 v4, 0x148e

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v5

    check-cast v5, LX/0xX;

    invoke-static {p0}, LX/0xC;->a(LX/0QB;)LX/0xC;

    move-result-object v6

    check-cast v6, LX/0xC;

    invoke-static {p0}, LX/0gq;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-direct/range {v0 .. v7}, LX/0xU;-><init>(LX/0hL;Ljava/lang/Boolean;LX/0xW;LX/0Or;LX/0xX;LX/0xC;Ljava/lang/Boolean;)V

    .line 163123
    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/facebook/apptab/state/NavigationConfig;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 163124
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0xU;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/0xU;->g:LX/0xC;

    invoke-virtual {v0}, LX/0xC;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    move v3, v1

    .line 163125
    :goto_0
    iget-object v0, p0, LX/0xU;->e:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->e()Z

    move-result v0

    .line 163126
    iget-object v4, p0, LX/0xU;->e:LX/0xW;

    invoke-virtual {v4}, LX/0xW;->d()Z

    move-result v4

    .line 163127
    if-nez v0, :cond_6

    move v0, v1

    .line 163128
    :goto_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 163129
    iget-boolean v2, p0, LX/0xU;->d:Z

    if-nez v2, :cond_0

    .line 163130
    sget-object v2, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 163131
    :cond_0
    if-eqz v0, :cond_1

    .line 163132
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 163133
    if-nez v0, :cond_1

    .line 163134
    sget-object v0, Lcom/facebook/friending/tab/FriendRequestsTab;->l:Lcom/facebook/friending/tab/FriendRequestsTab;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 163135
    :cond_1
    iget-boolean v0, p0, LX/0xU;->c:Z

    if-eqz v0, :cond_2

    .line 163136
    sget-object v0, Lcom/facebook/video/videohome/tab/VideoHomeTab;->l:Lcom/facebook/video/videohome/tab/VideoHomeTab;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 163137
    :cond_2
    if-eqz v3, :cond_3

    .line 163138
    sget-object v0, Lcom/facebook/marketplace/tab/MarketplaceTab;->m:Lcom/facebook/marketplace/tab/MarketplaceTab;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 163139
    :cond_3
    iget-boolean v0, p0, LX/0xU;->b:Z

    if-eqz v0, :cond_4

    .line 163140
    sget-object v0, Lcom/facebook/work/groupstab/WorkGroupsTab;->l:Lcom/facebook/work/groupstab/WorkGroupsTab;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 163141
    :cond_4
    if-eqz v4, :cond_7

    iget-object v0, p0, LX/0xU;->e:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->y()Z

    move-result v0

    if-nez v0, :cond_7

    .line 163142
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 163143
    if-nez v0, :cond_7

    .line 163144
    sget-object v0, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 163145
    :goto_2
    sget-object v0, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 163146
    new-instance v0, Lcom/facebook/apptab/state/NavigationConfig;

    iget-object v2, p0, LX/0xU;->a:LX/0hL;

    invoke-virtual {v2}, LX/0hL;->a()Z

    move-result v2

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/facebook/apptab/state/NavigationConfig;-><init>(ZLX/0Px;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_5
    move v3, v2

    .line 163147
    goto :goto_0

    :cond_6
    move v0, v2

    .line 163148
    goto :goto_1

    .line 163149
    :cond_7
    :try_start_1
    sget-object v0, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 163150
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
