.class public interface abstract LX/11i;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# virtual methods
.method public abstract a(LX/0Pq;)LX/11o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(LX/0Pq;LX/0P1;)LX/11o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(LX/0Pq;LX/0P1;J)LX/11o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(LX/0Pq;Ljava/lang/String;LX/0P1;J)LX/11o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(LX/0Pq;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract b(LX/0Pq;Ljava/lang/String;)LX/11o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "Ljava/lang/String;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract b(LX/0Pq;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;)V"
        }
    .end annotation
.end method

.method public abstract b(LX/0Pq;LX/0P1;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract b(LX/0Pq;LX/0P1;J)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation
.end method

.method public abstract b(LX/0Pq;Ljava/lang/String;LX/0P1;J)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation
.end method

.method public abstract c(LX/0Pq;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;)V"
        }
    .end annotation
.end method

.method public abstract d(LX/0Pq;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;)V"
        }
    .end annotation
.end method

.method public abstract e(LX/0Pq;)LX/11o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ">(TT;)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
