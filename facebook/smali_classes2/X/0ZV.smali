.class public LX/0ZV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0ZV;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/abtest/qe/annotations/ShouldPersistRecentExperiments;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 83326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83327
    iput-object p1, p0, LX/0ZV;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 83328
    iput-object p2, p0, LX/0ZV;->b:LX/0Or;

    .line 83329
    return-void
.end method

.method public static a(LX/0QB;)LX/0ZV;
    .locals 5

    .prologue
    .line 83330
    sget-object v0, LX/0ZV;->c:LX/0ZV;

    if-nez v0, :cond_1

    .line 83331
    const-class v1, LX/0ZV;

    monitor-enter v1

    .line 83332
    :try_start_0
    sget-object v0, LX/0ZV;->c:LX/0ZV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 83333
    if-eqz v2, :cond_0

    .line 83334
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 83335
    new-instance v4, LX/0ZV;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 p0, 0x1438

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/0ZV;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V

    .line 83336
    move-object v0, v4

    .line 83337
    sput-object v0, LX/0ZV;->c:LX/0ZV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83338
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 83339
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 83340
    :cond_1
    sget-object v0, LX/0ZV;->c:LX/0ZV;

    return-object v0

    .line 83341
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 83342
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/util/List;)Ljava/lang/String;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 83343
    const-string v0, ","

    invoke-static {v0}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x5

    const/4 v1, 0x0

    .line 83344
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, v3, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Too many UIDs were initially stored in the system."

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 83345
    instance-of v0, p1, LX/0Px;

    if-eqz v0, :cond_0

    .line 83346
    invoke-static {p1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object p1

    .line 83347
    :cond_0
    invoke-interface {p1, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 83348
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, v3, :cond_1

    .line 83349
    const/4 v0, 0x4

    invoke-interface {p1, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    .line 83350
    :cond_1
    invoke-interface {p1, v1, p0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 83351
    return-object p1

    :cond_2
    move v0, v1

    .line 83352
    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83353
    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83354
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    .line 83355
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ","

    invoke-static {v0}, LX/2Cb;->on(Ljava/lang/String;)LX/2Cb;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->b(Ljava/lang/Iterable;)Ljava/util/LinkedList;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83356
    iget-object v0, p0, LX/0ZV;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83357
    iget-object v0, p0, LX/0ZV;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2ab;->b:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0ZV;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 83358
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method
