.class public final LX/1UU;
.super Landroid/database/DataSetObserver;
.source ""


# instance fields
.field public final synthetic a:LX/1UT;


# direct methods
.method public constructor <init>(LX/1UT;)V
    .locals 0

    .prologue
    .line 256072
    iput-object p1, p0, LX/1UU;->a:LX/1UT;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onChanged()V
    .locals 3

    .prologue
    .line 256073
    iget-object v0, p0, LX/1UU;->a:LX/1UT;

    iget-boolean v0, v0, LX/1UT;->f:Z

    if-eqz v0, :cond_0

    .line 256074
    :goto_0
    return-void

    .line 256075
    :cond_0
    iget-object v0, p0, LX/1UU;->a:LX/1UT;

    const/4 v1, 0x1

    .line 256076
    iput-boolean v1, v0, LX/1UT;->f:Z

    .line 256077
    iget-object v0, p0, LX/1UU;->a:LX/1UT;

    iget-object v0, v0, LX/1UT;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 256078
    iget-object v0, p0, LX/1UU;->a:LX/1UT;

    iget-object v0, v0, LX/1UT;->b:LX/03V;

    const-class v1, LX/1UT;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Adapter.notifyDataSetChanged() must be called from the UI thread."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 256079
    :cond_1
    iget-object v0, p0, LX/1UU;->a:LX/1UT;

    iget-object v0, v0, LX/1UT;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    .line 256080
    if-eqz v0, :cond_2

    .line 256081
    const-string v1, "Do not call notifyDataSetChanged() while scrolling or in layout."

    invoke-virtual {v0, v1}, LX/1OR;->a(Ljava/lang/String;)V

    .line 256082
    :cond_2
    iget-object v0, p0, LX/1UU;->a:LX/1UT;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 256083
    iget-object v0, p0, LX/1UU;->a:LX/1UT;

    const/4 v1, 0x0

    .line 256084
    iput-boolean v1, v0, LX/1UT;->f:Z

    .line 256085
    goto :goto_0
.end method

.method public final onInvalidated()V
    .locals 1

    .prologue
    .line 256086
    iget-object v0, p0, LX/1UU;->a:LX/1UT;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 256087
    return-void
.end method
