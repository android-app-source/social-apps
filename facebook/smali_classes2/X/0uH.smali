.class public LX/0uH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0u4;


# instance fields
.field private a:LX/0tK;


# direct methods
.method public constructor <init>(LX/0tK;)V
    .locals 0

    .prologue
    .line 156215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156216
    iput-object p1, p0, LX/0uH;->a:LX/0tK;

    .line 156217
    return-void
.end method


# virtual methods
.method public final a(J)LX/0uE;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 156210
    long-to-int v0, p1

    packed-switch v0, :pswitch_data_0

    .line 156211
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 156212
    :pswitch_0
    new-instance v0, LX/0uE;

    iget-object v1, p0, LX/0uH;->a:LX/0tK;

    invoke-virtual {v1, v2}, LX/0tK;->a(Z)Z

    move-result v1

    invoke-direct {v0, v1}, LX/0uE;-><init>(Z)V

    goto :goto_0

    .line 156213
    :pswitch_1
    new-instance v0, LX/0uE;

    iget-object v1, p0, LX/0uH;->a:LX/0tK;

    invoke-virtual {v1, v2}, LX/0tK;->b(Z)Z

    move-result v1

    invoke-direct {v0, v1}, LX/0uE;-><init>(Z)V

    goto :goto_0

    .line 156214
    :pswitch_2
    new-instance v0, LX/0uE;

    iget-object v1, p0, LX/0uH;->a:LX/0tK;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/0tK;->c(Z)Z

    move-result v1

    invoke-direct {v0, v1}, LX/0uE;-><init>(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/0u5;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156209
    new-instance v0, LX/0u5;

    const-string v1, "dsm_mode_enabled"

    const-wide/16 v2, 0x1

    invoke-direct {v0, v1, p0, v2, v3}, LX/0u5;-><init>(Ljava/lang/String;LX/0u4;J)V

    new-instance v1, LX/0u5;

    const-string v2, "dsm_mode_active"

    const-wide/16 v4, 0x2

    invoke-direct {v1, v2, p0, v4, v5}, LX/0u5;-><init>(Ljava/lang/String;LX/0u4;J)V

    new-instance v2, LX/0u5;

    const-string v3, "dsm_auto_mode_enabled"

    const-wide/16 v4, 0x3

    invoke-direct {v2, v3, p0, v4, v5}, LX/0u5;-><init>(Ljava/lang/String;LX/0u4;J)V

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
