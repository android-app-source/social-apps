.class public LX/0hL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0hL;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 115823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115824
    iput-object p1, p0, LX/0hL;->a:Landroid/content/Context;

    .line 115825
    return-void
.end method

.method public static a(LX/0QB;)LX/0hL;
    .locals 4

    .prologue
    .line 115810
    sget-object v0, LX/0hL;->b:LX/0hL;

    if-nez v0, :cond_1

    .line 115811
    const-class v1, LX/0hL;

    monitor-enter v1

    .line 115812
    :try_start_0
    sget-object v0, LX/0hL;->b:LX/0hL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 115813
    if-eqz v2, :cond_0

    .line 115814
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 115815
    new-instance p0, LX/0hL;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/0hL;-><init>(Landroid/content/Context;)V

    .line 115816
    move-object v0, p0

    .line 115817
    sput-object v0, LX/0hL;->b:LX/0hL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 115818
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 115819
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 115820
    :cond_1
    sget-object v0, LX/0hL;->b:LX/0hL;

    return-object v0

    .line 115821
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 115822
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 115803
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 115804
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v3, v4, :cond_1

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v3, 0x400000

    and-int/2addr v2, v3

    if-eqz v2, :cond_1

    .line 115805
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v2

    .line 115806
    if-ne v2, v0, :cond_0

    .line 115807
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 115808
    goto :goto_0

    :cond_1
    move v0, v1

    .line 115809
    goto :goto_0
.end method


# virtual methods
.method public final a(I)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 115798
    invoke-virtual {p0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115799
    iget-object v0, p0, LX/0hL;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 115800
    new-instance v0, LX/0wK;

    iget-object v2, p0, LX/0hL;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, p0, v2, v1, v1}, LX/0wK;-><init>(LX/0hL;Landroid/content/res/Resources;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 115801
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0hL;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 115802
    iget-object v0, p0, LX/0hL;->a:Landroid/content/Context;

    invoke-static {v0}, LX/0hL;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
