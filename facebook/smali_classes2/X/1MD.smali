.class public LX/1MD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/1MD;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final c:LX/0SG;

.field private d:LX/0lB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 234900
    const-class v0, LX/1MD;

    sput-object v0, LX/1MD;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/0lB;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 234901
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 234902
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1MD;->b:Ljava/util/Map;

    .line 234903
    iput-object p1, p0, LX/1MD;->c:LX/0SG;

    .line 234904
    iput-object p2, p0, LX/1MD;->d:LX/0lB;

    .line 234905
    return-void
.end method

.method public static a(LX/0QB;)LX/1MD;
    .locals 5

    .prologue
    .line 234906
    sget-object v0, LX/1MD;->e:LX/1MD;

    if-nez v0, :cond_1

    .line 234907
    const-class v1, LX/1MD;

    monitor-enter v1

    .line 234908
    :try_start_0
    sget-object v0, LX/1MD;->e:LX/1MD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 234909
    if-eqz v2, :cond_0

    .line 234910
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 234911
    new-instance p0, LX/1MD;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lB;

    invoke-direct {p0, v3, v4}, LX/1MD;-><init>(LX/0SG;LX/0lB;)V

    .line 234912
    move-object v0, p0

    .line 234913
    sput-object v0, LX/1MD;->e:LX/1MD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 234914
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 234915
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 234916
    :cond_1
    sget-object v0, LX/1MD;->e:LX/1MD;

    return-object v0

    .line 234917
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 234918
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/lang/String;
    .locals 6

    .prologue
    .line 234919
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234920
    :try_start_1
    iget-object v0, p0, LX/1MD;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 234921
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, LX/1MD;->d:LX/0lB;

    iget-object v5, p0, LX/1MD;->b:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch LX/28F; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 234922
    :catch_0
    move-exception v0

    .line 234923
    :try_start_2
    sget-object v1, LX/1MD;->a:Ljava/lang/Class;

    const-string v2, "Couldn\'t dump composer instances"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 234924
    const-string v0, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 234925
    :goto_1
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 234926
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;)V
    .locals 6

    .prologue
    .line 234927
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->model:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getSessionId()Ljava/lang/String;

    move-result-object v1

    .line 234928
    iget-object v0, p0, LX/1MD;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;

    .line 234929
    if-eqz v0, :cond_0

    .line 234930
    iget-object v2, p0, LX/1MD;->b:Ljava/util/Map;

    new-instance v3, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;

    iget-wide v4, v0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;->startTime:J

    iget-object v0, v0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;->configuration:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-direct {v3, v4, v5, v0, p1}, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;-><init>(JLcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;)V

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234931
    :cond_0
    monitor-exit p0

    return-void

    .line 234932
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 234933
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1MD;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234934
    monitor-exit p0

    return-void

    .line 234935
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V
    .locals 5

    .prologue
    .line 234936
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1MD;->b:Ljava/util/Map;

    new-instance v1, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;

    iget-object v2, p0, LX/1MD;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, p2, v4}, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;-><init>(JLcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234937
    monitor-exit p0

    return-void

    .line 234938
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
