.class public LX/10V;
.super LX/0xh;
.source ""


# instance fields
.field public a:Landroid/app/Activity;

.field public b:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/uicontrib/fab/FabView;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0wd;

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 169159
    invoke-direct {p0}, LX/0xh;-><init>()V

    .line 169160
    return-void
.end method

.method public static e(LX/10V;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 169161
    iget-object v0, p0, LX/10V;->b:LX/0zw;

    if-nez v0, :cond_0

    .line 169162
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 169163
    iget-object v1, p0, LX/10V;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 169164
    const v2, 0x7f010284

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 169165
    iget v1, v0, Landroid/util/TypedValue;->data:I

    .line 169166
    iget-object v0, p0, LX/10V;->a:Landroid/app/Activity;

    const v2, 0x7f0d03af

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 169167
    new-instance v2, LX/0zw;

    new-instance v3, LX/Gty;

    invoke-direct {v3, p0, v1}, LX/Gty;-><init>(LX/10V;I)V

    invoke-direct {v2, v0, v3}, LX/0zw;-><init>(Landroid/view/ViewStub;LX/0zy;)V

    iput-object v2, p0, LX/10V;->b:LX/0zw;

    .line 169168
    :cond_0
    iget-object v0, p0, LX/10V;->c:LX/0wd;

    if-nez v0, :cond_1

    .line 169169
    iget-object v0, p0, LX/10V;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x4044000000000000L    # 40.0

    const-wide/high16 v4, 0x401c000000000000L    # 7.0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/10V;->c:LX/0wd;

    .line 169170
    iget-object v0, p0, LX/10V;->c:LX/0wd;

    invoke-virtual {v0, p0}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 169171
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 4

    .prologue
    .line 169172
    iget-object v0, p0, LX/10V;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/fab/FabView;

    .line 169173
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v1, v2

    .line 169174
    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setAlpha(F)V

    .line 169175
    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setScaleX(F)V

    .line 169176
    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setScaleY(F)V

    .line 169177
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 2

    .prologue
    .line 169178
    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, LX/0wd;->g(D)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169179
    iget-object v0, p0, LX/10V;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169180
    iget-object v0, p0, LX/10V;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/fab/FabView;

    .line 169181
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setVisibility(I)V

    .line 169182
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169183
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 169184
    invoke-static {p0}, LX/10V;->e(LX/10V;)V

    .line 169185
    iget-object v0, p0, LX/10V;->c:LX/0wd;

    invoke-virtual {v0, v2, v3}, LX/0wd;->g(D)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169186
    iget-object v0, p0, LX/10V;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169187
    iget-object v0, p0, LX/10V;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/fab/FabView;

    .line 169188
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setVisibility(I)V

    .line 169189
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169190
    :cond_0
    iget-object v0, p0, LX/10V;->c:LX/0wd;

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 169191
    return-void
.end method
