.class public LX/1nF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/1nF;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field private final d:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 315584
    const-class v0, LX/1nF;

    sput-object v0, LX/1nF;->e:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0ad;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/ReactFragmentActivity;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/fbreact/annotations/IsFb4aReactNativeEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 315614
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 315615
    iput-object p1, p0, LX/1nF;->a:LX/0Or;

    .line 315616
    iput-object p2, p0, LX/1nF;->b:LX/0Or;

    .line 315617
    iput-object p3, p0, LX/1nF;->c:Lcom/facebook/content/SecureContextHelper;

    .line 315618
    iput-object p4, p0, LX/1nF;->d:LX/0ad;

    .line 315619
    return-void
.end method

.method public static a(LX/0QB;)LX/1nF;
    .locals 7

    .prologue
    .line 315601
    sget-object v0, LX/1nF;->f:LX/1nF;

    if-nez v0, :cond_1

    .line 315602
    const-class v1, LX/1nF;

    monitor-enter v1

    .line 315603
    :try_start_0
    sget-object v0, LX/1nF;->f:LX/1nF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 315604
    if-eqz v2, :cond_0

    .line 315605
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 315606
    new-instance v5, LX/1nF;

    const/16 v3, 0xd

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v3, 0x148e

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {v5, v6, p0, v3, v4}, LX/1nF;-><init>(LX/0Or;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0ad;)V

    .line 315607
    move-object v0, v5

    .line 315608
    sput-object v0, LX/1nF;->f:LX/1nF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 315609
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 315610
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 315611
    :cond_1
    sget-object v0, LX/1nF;->f:LX/1nF;

    return-object v0

    .line 315612
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 315613
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1nF;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 315595
    iget-object v0, p0, LX/1nF;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 315596
    sget-object v0, LX/1nF;->e:Ljava/lang/Class;

    const-string v3, "React Native is turned off, not launching route: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    invoke-static {v3, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    move v0, v1

    .line 315597
    :goto_0
    return v0

    .line 315598
    :cond_0
    const-string v0, "FBGroupStorySeenByRoute"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 315599
    iget-object v0, p0, LX/1nF;->d:LX/0ad;

    sget-short v3, LX/88o;->a:S

    invoke-interface {v0, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/1nF;->d:LX/0ad;

    sget-short v3, LX/88j;->a:S

    invoke-interface {v0, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 315600
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 315585
    invoke-static {p0, p2}, LX/1nF;->a(LX/1nF;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 315586
    const/4 v0, 0x0

    .line 315587
    :cond_0
    :goto_0
    return-object v0

    .line 315588
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/1nF;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 315589
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->GENERAL_GROUPS_REACT_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 315590
    const-string v1, "show_search"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 315591
    if-eqz p1, :cond_2

    .line 315592
    const-string v1, "uri"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 315593
    :cond_2
    if-eqz p2, :cond_0

    .line 315594
    const-string v1, "route"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method
