.class public LX/1GR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0VI;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1GR;


# instance fields
.field private final a:I

.field private final b:I

.field public final c:LX/0Yw;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/16 v2, 0x1000

    const/16 v1, 0x14

    .line 225478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225479
    iput v2, p0, LX/1GR;->a:I

    .line 225480
    iput v1, p0, LX/1GR;->b:I

    .line 225481
    new-instance v0, LX/0Yw;

    invoke-direct {v0, v2, v1}, LX/0Yw;-><init>(II)V

    iput-object v0, p0, LX/1GR;->c:LX/0Yw;

    .line 225482
    return-void
.end method

.method public static a(LX/0QB;)LX/1GR;
    .locals 3

    .prologue
    .line 225483
    sget-object v0, LX/1GR;->d:LX/1GR;

    if-nez v0, :cond_1

    .line 225484
    const-class v1, LX/1GR;

    monitor-enter v1

    .line 225485
    :try_start_0
    sget-object v0, LX/1GR;->d:LX/1GR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 225486
    if-eqz v2, :cond_0

    .line 225487
    :try_start_1
    new-instance v0, LX/1GR;

    invoke-direct {v0}, LX/1GR;-><init>()V

    .line 225488
    move-object v0, v0

    .line 225489
    sput-object v0, LX/1GR;->d:LX/1GR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225490
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 225491
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 225492
    :cond_1
    sget-object v0, LX/1GR;->d:LX/1GR;

    return-object v0

    .line 225493
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 225494
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 225495
    const-string v0, "cache_log"

    return-object v0
.end method

.method public final a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 225496
    iget-object v0, p0, LX/1GR;->c:LX/0Yw;

    invoke-virtual {v0}, LX/0Yw;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
