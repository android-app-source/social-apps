.class public final LX/12v;
.super LX/0xA;
.source ""


# instance fields
.field public final synthetic a:LX/0iD;


# direct methods
.method public constructor <init>(LX/0iD;)V
    .locals 0

    .prologue
    .line 175827
    iput-object p1, p0, LX/12v;->a:LX/0iD;

    invoke-direct {p0}, LX/0xA;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/12j;I)V
    .locals 6

    .prologue
    .line 175815
    sget-object v0, LX/12j;->FRIEND_REQUESTS:LX/12j;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/12v;->a:LX/0iD;

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 175816
    iget-object v1, v0, LX/0iD;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0xW;

    invoke-virtual {v1}, LX/0xW;->d()Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    .line 175817
    :goto_0
    move v0, v1

    .line 175818
    if-eqz v0, :cond_0

    .line 175819
    iget-object v0, p0, LX/12v;->a:LX/0iD;

    sget-object v1, LX/12j;->NOTIFICATIONS:LX/12j;

    iget-object v2, p0, LX/12v;->a:LX/0iD;

    invoke-static {v2}, LX/0iD;->g(LX/0iD;)I

    move-result v2

    invoke-static {v0, v1, v2}, LX/0iD;->a$redex0(LX/0iD;LX/12j;I)V

    .line 175820
    :cond_0
    iget-object v0, p0, LX/12v;->a:LX/0iD;

    sget-object v1, LX/12j;->NOTIFICATIONS:LX/12j;

    if-ne p1, v1, :cond_1

    iget-object v1, p0, LX/12v;->a:LX/0iD;

    invoke-static {v1}, LX/0iD;->g(LX/0iD;)I

    move-result p2

    :cond_1
    invoke-static {v0, p1, p2}, LX/0iD;->a$redex0(LX/0iD;LX/12j;I)V

    .line 175821
    return-void

    .line 175822
    :cond_2
    iget-object v1, v0, LX/0iD;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0xW;

    invoke-virtual {v1}, LX/0xW;->o()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v3

    .line 175823
    goto :goto_0

    .line 175824
    :cond_3
    iget-object v1, v0, LX/0iD;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0xW;

    invoke-virtual {v1}, LX/0xW;->p()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 175825
    iget-object v1, v0, LX/0iD;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0xB;

    sget-object v4, LX/12j;->FRIEND_REQUESTS:LX/12j;

    invoke-virtual {v1, v4}, LX/0xB;->a(LX/12j;)I

    move-result v4

    iget-object v1, v0, LX/0iD;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0xB;

    sget-object v5, LX/12j;->NOTIFICATIONS:LX/12j;

    invoke-virtual {v1, v5}, LX/0xB;->a(LX/12j;)I

    move-result v1

    if-le v4, v1, :cond_4

    move v1, v3

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_0

    :cond_5
    move v1, v2

    .line 175826
    goto :goto_0
.end method
