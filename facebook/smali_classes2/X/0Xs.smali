.class public abstract LX/0Xs;
.super LX/0Xt;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Xt",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public transient a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field public transient b:I


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 79686
    invoke-direct {p0}, LX/0Xt;-><init>()V

    .line 79687
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 79688
    iput-object p1, p0, LX/0Xs;->a:Ljava/util/Map;

    .line 79689
    return-void
.end method

.method public static synthetic a(LX/0Xs;I)I
    .locals 1

    .prologue
    .line 79674
    iget v0, p0, LX/0Xs;->b:I

    add-int/2addr v0, p1

    iput v0, p0, LX/0Xs;->b:I

    return v0
.end method

.method private static a(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<TV;>;)",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 79675
    instance-of v0, p0, Ljava/util/SortedSet;

    if-eqz v0, :cond_0

    .line 79676
    check-cast p0, Ljava/util/SortedSet;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    .line 79677
    :goto_0
    return-object v0

    .line 79678
    :cond_0
    instance-of v0, p0, Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 79679
    check-cast p0, Ljava/util/Set;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    .line 79680
    :cond_1
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_2

    .line 79681
    check-cast p0, Ljava/util/List;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 79682
    :cond_2
    invoke-static {p0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public static a$redex0(LX/0Xs;Ljava/lang/Object;Ljava/util/List;LX/11e;)Ljava/util/List;
    .locals 1
    .param p0    # LX/0Xs;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/List",
            "<TV;>;",
            "LX/0Xs",
            "<TK;TV;>.WrappedCollection;)",
            "Ljava/util/List",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 79683
    instance-of v0, p2, Ljava/util/RandomAccess;

    if-eqz v0, :cond_0

    new-instance v0, LX/11c;

    invoke-direct {v0, p0, p1, p2, p3}, LX/11c;-><init>(LX/0Xs;Ljava/lang/Object;Ljava/util/List;LX/11e;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/11d;

    invoke-direct {v0, p0, p1, p2, p3}, LX/11d;-><init>(LX/0Xs;Ljava/lang/Object;Ljava/util/List;LX/11e;)V

    goto :goto_0
.end method

.method public static synthetic b(LX/0Xs;)I
    .locals 2

    .prologue
    .line 79684
    iget v0, p0, LX/0Xs;->b:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, LX/0Xs;->b:I

    return v0
.end method

.method public static synthetic b(LX/0Xs;I)I
    .locals 1

    .prologue
    .line 79705
    iget v0, p0, LX/0Xs;->b:I

    sub-int/2addr v0, p1

    iput v0, p0, LX/0Xs;->b:I

    return v0
.end method

.method public static synthetic c(LX/0Xs;)I
    .locals 2

    .prologue
    .line 79685
    iget v0, p0, LX/0Xs;->b:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/0Xs;->b:I

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/Collection",
            "<TV;>;)",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 79690
    instance-of v0, p2, Ljava/util/SortedSet;

    if-eqz v0, :cond_0

    .line 79691
    new-instance v0, LX/4ww;

    check-cast p2, Ljava/util/SortedSet;

    invoke-direct {v0, p0, p1, p2, v1}, LX/4ww;-><init>(LX/0Xs;Ljava/lang/Object;Ljava/util/SortedSet;LX/11e;)V

    .line 79692
    :goto_0
    return-object v0

    .line 79693
    :cond_0
    instance-of v0, p2, Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 79694
    new-instance v0, LX/1fF;

    check-cast p2, Ljava/util/Set;

    invoke-direct {v0, p0, p1, p2}, LX/1fF;-><init>(LX/0Xs;Ljava/lang/Object;Ljava/util/Set;)V

    goto :goto_0

    .line 79695
    :cond_1
    instance-of v0, p2, Ljava/util/List;

    if-eqz v0, :cond_2

    .line 79696
    check-cast p2, Ljava/util/List;

    invoke-static {p0, p1, p2, v1}, LX/0Xs;->a$redex0(LX/0Xs;Ljava/lang/Object;Ljava/util/List;LX/11e;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 79697
    :cond_2
    new-instance v0, LX/11e;

    invoke-direct {v0, p0, p1, p2, v1}, LX/11e;-><init>(LX/0Xs;Ljava/lang/Object;Ljava/util/Collection;LX/11e;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 79698
    iput-object p1, p0, LX/0Xs;->a:Ljava/util/Map;

    .line 79699
    iput v2, p0, LX/0Xs;->b:I

    .line 79700
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 79701
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 79702
    iget v1, p0, LX/0Xs;->b:I

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/2addr v0, v1

    iput v0, p0, LX/0Xs;->b:I

    goto :goto_0

    :cond_0
    move v1, v2

    .line 79703
    goto :goto_1

    .line 79704
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 79662
    iget-object v0, p0, LX/0Xs;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 79663
    if-nez v0, :cond_1

    .line 79664
    invoke-virtual {p0, p1}, LX/0Xs;->e(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 79665
    invoke-interface {v0, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 79666
    iget v2, p0, LX/0Xs;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/0Xs;->b:I

    .line 79667
    iget-object v2, p0, LX/0Xs;->a:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 79668
    :goto_0
    return v0

    .line 79669
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "New Collection violated the Collection spec"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 79670
    :cond_1
    invoke-interface {v0, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79671
    iget v0, p0, LX/0Xs;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Xs;->b:I

    move v0, v1

    .line 79672
    goto :goto_0

    .line 79673
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract c()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 79658
    iget-object v0, p0, LX/0Xs;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 79659
    if-nez v0, :cond_0

    .line 79660
    invoke-virtual {p0, p1}, LX/0Xs;->e(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 79661
    :cond_0
    invoke-virtual {p0, p1, v0}, LX/0Xs;->a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 79657
    invoke-virtual {p0}, LX/0Xs;->c()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0Xs;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 79648
    iget-object v0, p0, LX/0Xs;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 79649
    if-nez v0, :cond_0

    .line 79650
    invoke-virtual {p0}, LX/0Xs;->d()Ljava/util/Collection;

    move-result-object v0

    .line 79651
    :goto_0
    return-object v0

    .line 79652
    :cond_0
    invoke-virtual {p0}, LX/0Xs;->c()Ljava/util/Collection;

    move-result-object v1

    .line 79653
    invoke-interface {v1, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 79654
    iget v2, p0, LX/0Xs;->b:I

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, p0, LX/0Xs;->b:I

    .line 79655
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 79656
    invoke-static {v1}, LX/0Xs;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public e(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 79647
    invoke-virtual {p0}, LX/0Xs;->c()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 79636
    iget v0, p0, LX/0Xs;->b:I

    return v0
.end method

.method public f(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 79646
    iget-object v0, p0, LX/0Xs;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 79641
    iget-object v0, p0, LX/0Xs;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 79642
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    goto :goto_0

    .line 79643
    :cond_0
    iget-object v0, p0, LX/0Xs;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 79644
    const/4 v0, 0x0

    iput v0, p0, LX/0Xs;->b:I

    .line 79645
    return-void
.end method

.method public final h()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 79640
    iget-object v0, p0, LX/0Xs;->a:Ljava/util/Map;

    instance-of v0, v0, Ljava/util/SortedMap;

    if-eqz v0, :cond_0

    new-instance v1, LX/4wu;

    iget-object v0, p0, LX/0Xs;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-direct {v1, p0, v0}, LX/4wu;-><init>(LX/0Xs;Ljava/util/SortedMap;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/1r6;

    iget-object v1, p0, LX/0Xs;->a:Ljava/util/Map;

    invoke-direct {v0, p0, v1}, LX/1r6;-><init>(LX/0Xs;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public j()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 79639
    new-instance v0, LX/1r4;

    invoke-direct {v0, p0}, LX/1r4;-><init>(LX/0Xs;)V

    return-object v0
.end method

.method public l()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 79638
    new-instance v0, LX/2Tj;

    invoke-direct {v0, p0}, LX/2Tj;-><init>(LX/0Xs;)V

    return-object v0
.end method

.method public final m()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 79637
    iget-object v0, p0, LX/0Xs;->a:Ljava/util/Map;

    instance-of v0, v0, Ljava/util/SortedMap;

    if-eqz v0, :cond_0

    new-instance v1, LX/4wt;

    iget-object v0, p0, LX/0Xs;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-direct {v1, p0, v0}, LX/4wt;-><init>(LX/0Xs;Ljava/util/SortedMap;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/1pz;

    iget-object v1, p0, LX/0Xs;->a:Ljava/util/Map;

    invoke-direct {v0, p0, v1}, LX/1pz;-><init>(LX/0Xs;Ljava/util/Map;)V

    goto :goto_0
.end method
