.class public LX/1Gn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Go;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1Gn;


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/cdn/gk/IsCdnCacheLoggingEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 226098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226099
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/1Gn;->a:Z

    .line 226100
    return-void
.end method

.method public static a(LX/0QB;)LX/1Gn;
    .locals 6

    .prologue
    .line 226059
    sget-object v0, LX/1Gn;->b:LX/1Gn;

    if-nez v0, :cond_1

    .line 226060
    const-class v1, LX/1Gn;

    monitor-enter v1

    .line 226061
    :try_start_0
    sget-object v0, LX/1Gn;->b:LX/1Gn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 226062
    if-eqz v2, :cond_0

    .line 226063
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 226064
    new-instance v4, LX/1Gn;

    .line 226065
    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    const/16 v5, 0x476

    const/4 p0, 0x0

    invoke-virtual {v3, v5, p0}, LX/0Uh;->a(IZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object v3, v3

    .line 226066
    check-cast v3, Ljava/lang/Boolean;

    invoke-direct {v4, v3}, LX/1Gn;-><init>(Ljava/lang/Boolean;)V

    .line 226067
    move-object v0, v4

    .line 226068
    sput-object v0, LX/1Gn;->b:LX/1Gn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 226069
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 226070
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 226071
    :cond_1
    sget-object v0, LX/1Gn;->b:LX/1Gn;

    return-object v0

    .line 226072
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 226073
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lorg/apache/http/HttpResponse;)LX/2ur;
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 226074
    iget-boolean v0, p0, LX/1Gn;->a:Z

    if-nez v0, :cond_0

    .line 226075
    sget-object v0, LX/2ur;->NOT_IN_GK:LX/2ur;

    .line 226076
    :goto_0
    return-object v0

    .line 226077
    :cond_0
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v7

    .line 226078
    sget-object v5, LX/03R;->UNSET:LX/03R;

    .line 226079
    sget-object v4, LX/03R;->UNSET:LX/03R;

    .line 226080
    sget-object v3, LX/03R;->UNSET:LX/03R;

    .line 226081
    array-length v8, v7

    move v6, v2

    :goto_1
    if-ge v6, v8, :cond_6

    aget-object v0, v7, v6

    .line 226082
    invoke-interface {v0}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v9

    .line 226083
    const-string v10, "x-edge-hit"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 226084
    const-string v5, "0"

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    move-object v11, v3

    move-object v3, v4

    move-object v4, v0

    move-object v0, v11

    .line 226085
    :goto_3
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move-object v5, v4

    move-object v4, v3

    move-object v3, v0

    goto :goto_1

    :cond_1
    move v0, v2

    .line 226086
    goto :goto_2

    .line 226087
    :cond_2
    const-string v10, "x-cache"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 226088
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    const-string v9, "tcp_miss"

    invoke-virtual {v4, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    const-string v4, "TCP_MISS"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_4
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    move-object v4, v5

    move-object v11, v0

    move-object v0, v3

    move-object v3, v11

    goto :goto_3

    :cond_3
    move v0, v2

    goto :goto_4

    .line 226089
    :cond_4
    const-string v10, "x-cache-remote"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 226090
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    const-string v9, "tcp_miss"

    invoke-virtual {v3, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    const-string v3, "TCP_MISS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    move-object v3, v4

    move-object v4, v5

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_5

    .line 226091
    :cond_6
    sget-object v0, LX/03R;->UNSET:LX/03R;

    if-ne v5, v0, :cond_7

    sget-object v0, LX/03R;->UNSET:LX/03R;

    if-ne v4, v0, :cond_7

    sget-object v0, LX/03R;->UNSET:LX/03R;

    if-ne v3, v0, :cond_7

    .line 226092
    sget-object v0, LX/2ur;->NO_HEADER:LX/2ur;

    goto/16 :goto_0

    .line 226093
    :cond_7
    sget-object v0, LX/03R;->UNSET:LX/03R;

    if-eq v5, v0, :cond_9

    .line 226094
    invoke-virtual {v5}, LX/03R;->asBoolean()Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, LX/2ur;->FB_CDN_CACHE_HIT:LX/2ur;

    goto/16 :goto_0

    :cond_8
    sget-object v0, LX/2ur;->FB_CDN_CACHE_MISS:LX/2ur;

    goto/16 :goto_0

    .line 226095
    :cond_9
    sget-object v0, LX/03R;->NO:LX/03R;

    if-ne v4, v0, :cond_a

    sget-object v0, LX/03R;->NO:LX/03R;

    if-ne v3, v0, :cond_a

    .line 226096
    sget-object v0, LX/2ur;->AKAMAI_CDN_CACHE_MISS:LX/2ur;

    goto/16 :goto_0

    .line 226097
    :cond_a
    invoke-virtual {v4}, LX/03R;->asBoolean()Z

    move-result v0

    if-eqz v0, :cond_b

    sget-object v0, LX/2ur;->AKAMAI_CDN_CACHE_EDGE_HIT:LX/2ur;

    goto/16 :goto_0

    :cond_b
    sget-object v0, LX/2ur;->AKAMAI_CDN_CACHE_MIDGRESS_HIT:LX/2ur;

    goto/16 :goto_0

    :cond_c
    move-object v0, v3

    move-object v3, v4

    move-object v4, v5

    goto/16 :goto_3
.end method

.method public final a(Lorg/apache/http/HttpRequest;)V
    .locals 2

    .prologue
    .line 226050
    iget-boolean v0, p0, LX/1Gn;->a:Z

    if-nez v0, :cond_1

    .line 226051
    :cond_0
    :goto_0
    return-void

    .line 226052
    :cond_1
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    .line 226053
    const-string v1, ".webp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ".jpeg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ".png"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 226054
    if-eqz v1, :cond_0

    .line 226055
    const-string v1, ".akamaihd.net"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ".fbcdn.net"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_3
    const/4 v1, 0x1

    :goto_2
    move v0, v1

    .line 226056
    if-eqz v0, :cond_0

    .line 226057
    const-string v0, "Pragma"

    const-string v1, "akamai-x-cache-on, akamai-x-cache-remote-on"

    invoke-interface {p1, v0, v1}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 226058
    const-string v0, "X-FB-Debug"

    const-string v1, "True"

    invoke-interface {p1, v0, v1}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 226049
    iget-boolean v0, p0, LX/1Gn;->a:Z

    return v0
.end method
