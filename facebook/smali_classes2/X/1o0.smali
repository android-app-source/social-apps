.class public LX/1o0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/1Up;

.field public static final b:LX/1Up;

.field public static final c:LX/1Up;

.field public static final d:LX/1Up;

.field public static final e:LX/1Up;

.field private static final f:LX/1Up;

.field private static final g:LX/1Up;

.field private static volatile i:LX/1o0;


# instance fields
.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 318092
    sget-object v0, LX/1Uo;->b:LX/1Up;

    sput-object v0, LX/1o0;->f:LX/1Up;

    .line 318093
    sget-object v0, LX/1Uo;->a:LX/1Up;

    sput-object v0, LX/1o0;->g:LX/1Up;

    .line 318094
    sget-object v0, LX/1o0;->f:LX/1Up;

    sput-object v0, LX/1o0;->a:LX/1Up;

    .line 318095
    sget-object v0, LX/1o0;->g:LX/1Up;

    sput-object v0, LX/1o0;->b:LX/1Up;

    .line 318096
    sget-object v0, LX/1o0;->g:LX/1Up;

    sput-object v0, LX/1o0;->c:LX/1Up;

    .line 318097
    sget-object v0, LX/1o0;->g:LX/1Up;

    sput-object v0, LX/1o0;->d:LX/1Up;

    .line 318098
    sget-object v0, LX/1o0;->g:LX/1Up;

    sput-object v0, LX/1o0;->e:LX/1Up;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 318124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 318125
    iput-object p1, p0, LX/1o0;->h:LX/0Or;

    .line 318126
    return-void
.end method

.method public static a(LX/0QB;)LX/1o0;
    .locals 4

    .prologue
    .line 318111
    sget-object v0, LX/1o0;->i:LX/1o0;

    if-nez v0, :cond_1

    .line 318112
    const-class v1, LX/1o0;

    monitor-enter v1

    .line 318113
    :try_start_0
    sget-object v0, LX/1o0;->i:LX/1o0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 318114
    if-eqz v2, :cond_0

    .line 318115
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 318116
    new-instance v3, LX/1o0;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1o0;-><init>(LX/0Or;)V

    .line 318117
    move-object v0, v3

    .line 318118
    sput-object v0, LX/1o0;->i:LX/1o0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 318119
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 318120
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 318121
    :cond_1
    sget-object v0, LX/1o0;->i:LX/1o0;

    return-object v0

    .line 318122
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 318123
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1De;LX/1oI;LX/4Ab;Landroid/graphics/ColorFilter;LX/1dc;LX/1Up;LX/1dc;LX/1Up;LX/1dc;LX/1Up;ILX/1dc;LX/1Up;ILX/1dc;)V
    .locals 1
    .param p2    # LX/4Ab;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/ColorFilter;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p5    # LX/1Up;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p7    # LX/1Up;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p9    # LX/1Up;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p11    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p12    # LX/1Up;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p13    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p14    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1oI",
            "<",
            "LX/1oH;",
            ">;",
            "LX/4Ab;",
            "Landroid/graphics/ColorFilter;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1Up;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1Up;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1Up;",
            "I",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1Up;",
            "I",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 318099
    invoke-virtual {p1}, LX/1oI;->d()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1oH;

    .line 318100
    iput-object p0, v0, LX/1oH;->b:LX/1De;

    .line 318101
    invoke-virtual {v0, p4, p5}, LX/1oH;->a(LX/1dc;LX/1Up;)V

    .line 318102
    invoke-virtual {v0, p6, p7}, LX/1oH;->c(LX/1dc;LX/1Up;)V

    .line 318103
    invoke-virtual {v0, p8, p9, p10}, LX/1oH;->a(LX/1dc;LX/1Up;I)V

    .line 318104
    invoke-virtual {v0, p11, p12}, LX/1oH;->b(LX/1dc;LX/1Up;)V

    .line 318105
    invoke-virtual {v0, p2}, LX/1oH;->a(LX/4Ab;)V

    .line 318106
    invoke-virtual {v0, p3}, LX/1oH;->a(Landroid/graphics/ColorFilter;)V

    .line 318107
    invoke-virtual {v0, p14}, LX/1oH;->a(LX/1dc;)V

    .line 318108
    invoke-virtual {v0, p13}, LX/1oH;->a(I)V

    .line 318109
    invoke-virtual {p1}, LX/1oI;->b()V

    .line 318110
    return-void
.end method
