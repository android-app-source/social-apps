.class public LX/1rU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1rU;


# instance fields
.field public final a:LX/0xW;

.field private final b:LX/0Uh;

.field private final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/0xW;LX/0Uh;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 332270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 332271
    iput-object p1, p0, LX/1rU;->a:LX/0xW;

    .line 332272
    iput-object p2, p0, LX/1rU;->b:LX/0Uh;

    .line 332273
    iput-object p3, p0, LX/1rU;->c:LX/0ad;

    .line 332274
    return-void
.end method

.method public static a(LX/0QB;)LX/1rU;
    .locals 6

    .prologue
    .line 332257
    sget-object v0, LX/1rU;->d:LX/1rU;

    if-nez v0, :cond_1

    .line 332258
    const-class v1, LX/1rU;

    monitor-enter v1

    .line 332259
    :try_start_0
    sget-object v0, LX/1rU;->d:LX/1rU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 332260
    if-eqz v2, :cond_0

    .line 332261
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 332262
    new-instance p0, LX/1rU;

    invoke-static {v0}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v3

    check-cast v3, LX/0xW;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/1rU;-><init>(LX/0xW;LX/0Uh;LX/0ad;)V

    .line 332263
    move-object v0, p0

    .line 332264
    sput-object v0, LX/1rU;->d:LX/1rU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 332265
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 332266
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 332267
    :cond_1
    sget-object v0, LX/1rU;->d:LX/1rU;

    return-object v0

    .line 332268
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 332269
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 332244
    iget-object v0, p0, LX/1rU;->a:LX/0xW;

    .line 332245
    iget-object v1, v0, LX/0xW;->a:LX/0ad;

    sget-short v2, LX/0xc;->z:S

    const/4 p0, 0x0

    invoke-interface {v1, v2, p0}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 332246
    return v0
.end method

.method public final c()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 332254
    iget-object v1, p0, LX/1rU;->a:LX/0xW;

    invoke-virtual {v1}, LX/0xW;->e()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/1rU;->a:LX/0xW;

    .line 332255
    iget-object v2, v1, LX/0xW;->a:LX/0ad;

    sget-short v3, LX/0xc;->t:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v1, v2

    .line 332256
    if-nez v1, :cond_0

    iget-object v1, p0, LX/1rU;->b:LX/0Uh;

    const/16 v2, 0x222

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 332251
    iget-object v0, p0, LX/1rU;->a:LX/0xW;

    .line 332252
    iget-object v1, v0, LX/0xW;->a:LX/0ad;

    sget-short v2, LX/0xc;->x:S

    const/4 p0, 0x0

    invoke-interface {v1, v2, p0}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 332253
    return v0
.end method

.method public final f()Z
    .locals 3

    .prologue
    .line 332248
    iget-object v0, p0, LX/1rU;->a:LX/0xW;

    .line 332249
    iget-object v1, v0, LX/0xW;->a:LX/0ad;

    sget-short v2, LX/0xc;->D:S

    const/4 p0, 0x0

    invoke-interface {v1, v2, p0}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 332250
    return v0
.end method

.method public final g()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 332247
    invoke-virtual {p0}, LX/1rU;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1rU;->c:LX/0ad;

    sget-short v2, LX/15r;->d:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
