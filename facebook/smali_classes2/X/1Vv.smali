.class public LX/1Vv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 266819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 266820
    iput-object p1, p0, LX/1Vv;->a:LX/0Zb;

    .line 266821
    return-void
.end method

.method public static b(LX/0QB;)LX/1Vv;
    .locals 2

    .prologue
    .line 266822
    new-instance v1, LX/1Vv;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/1Vv;-><init>(LX/0Zb;)V

    .line 266823
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 266824
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 266825
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 266826
    const-string v1, "display_time_invalidation"

    .line 266827
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 266828
    const-string v1, "content_id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 266829
    iget-object v1, p0, LX/1Vv;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 266830
    :cond_0
    return-void
.end method
