.class public LX/1Bv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final e:Ljava/lang/String;

.field private static volatile x:LX/1Bv;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public b:LX/1mC;

.field public c:J

.field public d:LX/1mD;

.field public f:LX/121;

.field private g:LX/0dN;

.field public h:LX/0kb;

.field public i:LX/0oz;

.field public final j:LX/1m0;

.field public k:LX/0ka;

.field public l:Landroid/net/NetworkInfo;

.field private final m:LX/0Xl;

.field public n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/1mC;

.field public final p:LX/0u7;

.field public final q:Landroid/os/PowerManager;

.field private final r:LX/0So;

.field private s:LX/19m;

.field private t:LX/1Bh;

.field private u:LX/19j;

.field private v:LX/0tK;

.field private final w:LX/0tQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 214537
    const-class v0, LX/1Bv;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1Bv;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/121;LX/0kb;LX/0oz;LX/1m0;LX/0ka;LX/0Or;LX/1mC;LX/1mD;LX/0u7;Landroid/os/PowerManager;LX/0So;LX/19m;LX/19j;LX/0tK;LX/0Xl;LX/0tQ;)V
    .locals 7
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/IsDialtonePhotoFeatureEnabled;
        .end annotation
    .end param
    .param p8    # LX/1mC;
        .annotation runtime Lcom/facebook/video/settings/DefaultAutoPlaySettingsFromServer;
        .end annotation
    .end param
    .param p16    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/121;",
            "LX/0kb;",
            "LX/0oz;",
            "LX/1m0;",
            "LX/0ka;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1mC;",
            "LX/1mD;",
            "Lcom/facebook/common/hardware/BatteryStateManager;",
            "Landroid/os/PowerManager;",
            "LX/0So;",
            "LX/19m;",
            "LX/19j;",
            "LX/0tK;",
            "LX/0Xl;",
            "LX/0tQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 214515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214516
    iput-object p1, p0, LX/1Bv;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 214517
    iput-object p2, p0, LX/1Bv;->f:LX/121;

    .line 214518
    iput-object p3, p0, LX/1Bv;->h:LX/0kb;

    .line 214519
    iput-object p4, p0, LX/1Bv;->i:LX/0oz;

    .line 214520
    iput-object p5, p0, LX/1Bv;->j:LX/1m0;

    .line 214521
    iput-object p6, p0, LX/1Bv;->k:LX/0ka;

    .line 214522
    iput-object p7, p0, LX/1Bv;->n:LX/0Or;

    .line 214523
    iput-object p8, p0, LX/1Bv;->o:LX/1mC;

    .line 214524
    move-object/from16 v0, p9

    iput-object v0, p0, LX/1Bv;->d:LX/1mD;

    .line 214525
    iget-object v1, p0, LX/1Bv;->h:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->b()Landroid/net/NetworkInfo;

    move-result-object v1

    iput-object v1, p0, LX/1Bv;->l:Landroid/net/NetworkInfo;

    .line 214526
    move-object/from16 v0, p10

    iput-object v0, p0, LX/1Bv;->p:LX/0u7;

    .line 214527
    move-object/from16 v0, p11

    iput-object v0, p0, LX/1Bv;->q:Landroid/os/PowerManager;

    .line 214528
    move-object/from16 v0, p12

    iput-object v0, p0, LX/1Bv;->r:LX/0So;

    .line 214529
    move-object/from16 v0, p13

    iput-object v0, p0, LX/1Bv;->s:LX/19m;

    .line 214530
    new-instance v1, LX/1Bh;

    sget-object v2, LX/0p3;->MODERATE:LX/0p3;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/1Bh;-><init>(LX/0p3;IZZZ)V

    iput-object v1, p0, LX/1Bv;->t:LX/1Bh;

    .line 214531
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1Bv;->u:LX/19j;

    .line 214532
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1Bv;->v:LX/0tK;

    .line 214533
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1Bv;->m:LX/0Xl;

    .line 214534
    move-object/from16 v0, p17

    iput-object v0, p0, LX/1Bv;->w:LX/0tQ;

    .line 214535
    invoke-direct {p0}, LX/1Bv;->d()V

    .line 214536
    return-void
.end method

.method public static a(LX/0QB;)LX/1Bv;
    .locals 3

    .prologue
    .line 214505
    sget-object v0, LX/1Bv;->x:LX/1Bv;

    if-nez v0, :cond_1

    .line 214506
    const-class v1, LX/1Bv;

    monitor-enter v1

    .line 214507
    :try_start_0
    sget-object v0, LX/1Bv;->x:LX/1Bv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 214508
    if-eqz v2, :cond_0

    .line 214509
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1Bv;->b(LX/0QB;)LX/1Bv;

    move-result-object v0

    sput-object v0, LX/1Bv;->x:LX/1Bv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 214510
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 214511
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 214512
    :cond_1
    sget-object v0, LX/1Bv;->x:LX/1Bv;

    return-object v0

    .line 214513
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 214514
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/1Bh;)Z
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 214479
    iget-object v0, p0, LX/1Bv;->l:Landroid/net/NetworkInfo;

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/1Bv;->l:Landroid/net/NetworkInfo;

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 214480
    if-nez v0, :cond_0

    iget-boolean v0, p1, LX/1Bh;->d:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 214481
    :goto_1
    return v0

    .line 214482
    :cond_0
    iget-object v0, p0, LX/1Bv;->w:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 214483
    goto :goto_1

    .line 214484
    :cond_1
    iget v0, p1, LX/1Bh;->b:I

    if-lez v0, :cond_5

    .line 214485
    invoke-direct {p0, p1}, LX/1Bv;->b(LX/1Bh;)LX/1mE;

    move-result-object v0

    .line 214486
    sget-object v3, LX/1mF;->a:[I

    invoke-virtual {v0}, LX/1mE;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    .line 214487
    iget-object v0, p0, LX/1Bv;->u:LX/19j;

    iget-boolean v0, v0, LX/19j;->g:Z

    if-eqz v0, :cond_4

    .line 214488
    iget-object v0, p0, LX/1Bv;->i:LX/0oz;

    invoke-virtual {v0}, LX/0oz;->c()LX/0p3;

    move-result-object v0

    .line 214489
    sget-object v3, LX/0p3;->UNKNOWN:LX/0p3;

    if-ne v0, v3, :cond_2

    .line 214490
    iget-object v0, p0, LX/1Bv;->i:LX/0oz;

    invoke-virtual {v0}, LX/0oz;->e()LX/0p3;

    move-result-object v0

    .line 214491
    :cond_2
    sget-object v3, LX/0p3;->UNKNOWN:LX/0p3;

    if-eq v0, v3, :cond_4

    .line 214492
    iget-object v3, p1, LX/1Bh;->a:LX/0p3;

    invoke-virtual {v0, v3}, LX/0p3;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_3

    move v0, v2

    goto :goto_1

    :pswitch_0
    move v0, v2

    .line 214493
    goto :goto_1

    :pswitch_1
    move v0, v1

    .line 214494
    goto :goto_1

    :cond_3
    move v0, v1

    .line 214495
    goto :goto_1

    .line 214496
    :cond_4
    iget-boolean v0, p1, LX/1Bh;->f:Z

    if-nez v0, :cond_5

    move v0, v1

    .line 214497
    goto :goto_1

    .line 214498
    :cond_5
    iget v0, p1, LX/1Bh;->b:I

    if-lez v0, :cond_6

    iget-boolean v0, p1, LX/1Bh;->f:Z

    if-eqz v0, :cond_8

    .line 214499
    :cond_6
    iget-object v0, p0, LX/1Bv;->i:LX/0oz;

    invoke-virtual {v0}, LX/0oz;->c()LX/0p3;

    move-result-object v0

    .line 214500
    sget-object v3, LX/0p3;->UNKNOWN:LX/0p3;

    if-ne v0, v3, :cond_7

    .line 214501
    iget-object v0, p0, LX/1Bv;->u:LX/19j;

    iget-boolean v0, v0, LX/19j;->d:Z

    goto :goto_1

    .line 214502
    :cond_7
    iget-object v3, p1, LX/1Bh;->a:LX/0p3;

    invoke-virtual {v0, v3}, LX/0p3;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-gez v0, :cond_8

    move v0, v1

    .line 214503
    goto :goto_1

    :cond_8
    move v0, v2

    .line 214504
    goto :goto_1

    :cond_9
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static b(LX/0QB;)LX/1Bv;
    .locals 19

    .prologue
    .line 214477
    new-instance v1, LX/1Bv;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/128;->a(LX/0QB;)LX/128;

    move-result-object v3

    check-cast v3, LX/121;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-static/range {p0 .. p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v5

    check-cast v5, LX/0oz;

    invoke-static/range {p0 .. p0}, LX/1m0;->a(LX/0QB;)LX/1m0;

    move-result-object v6

    check-cast v6, LX/1m0;

    invoke-static/range {p0 .. p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v7

    check-cast v7, LX/0ka;

    const/16 v8, 0x1488

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/1mG;->a(LX/0QB;)LX/1mC;

    move-result-object v9

    check-cast v9, LX/1mC;

    invoke-static/range {p0 .. p0}, LX/1mD;->a(LX/0QB;)LX/1mD;

    move-result-object v10

    check-cast v10, LX/1mD;

    invoke-static/range {p0 .. p0}, LX/0u7;->a(LX/0QB;)LX/0u7;

    move-result-object v11

    check-cast v11, LX/0u7;

    invoke-static/range {p0 .. p0}, LX/0kZ;->a(LX/0QB;)Landroid/os/PowerManager;

    move-result-object v12

    check-cast v12, Landroid/os/PowerManager;

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v13

    check-cast v13, LX/0So;

    invoke-static/range {p0 .. p0}, LX/19m;->a(LX/0QB;)LX/19m;

    move-result-object v14

    check-cast v14, LX/19m;

    invoke-static/range {p0 .. p0}, LX/19j;->a(LX/0QB;)LX/19j;

    move-result-object v15

    check-cast v15, LX/19j;

    invoke-static/range {p0 .. p0}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v16

    check-cast v16, LX/0tK;

    invoke-static/range {p0 .. p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v17

    check-cast v17, LX/0Xl;

    invoke-static/range {p0 .. p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v18

    check-cast v18, LX/0tQ;

    invoke-direct/range {v1 .. v18}, LX/1Bv;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/121;LX/0kb;LX/0oz;LX/1m0;LX/0ka;LX/0Or;LX/1mC;LX/1mD;LX/0u7;Landroid/os/PowerManager;LX/0So;LX/19m;LX/19j;LX/0tK;LX/0Xl;LX/0tQ;)V

    .line 214478
    return-object v1
.end method

.method private b(LX/1Bh;)LX/1mE;
    .locals 8

    .prologue
    .line 214445
    iget v0, p1, LX/1Bh;->b:I

    if-gtz v0, :cond_0

    .line 214446
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "minConnectionBandwidthKbps must be positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 214447
    :cond_0
    iget-object v4, p0, LX/1Bv;->j:LX/1m0;

    if-eqz v4, :cond_1

    iget-object v4, p0, LX/1Bv;->j:LX/1m0;

    .line 214448
    iget-object v5, v4, LX/1m0;->o:LX/1m5;

    move-object v4, v5

    .line 214449
    if-nez v4, :cond_5

    .line 214450
    :cond_1
    sget-object v4, LX/1mE;->UNKNOWN:LX/1mE;

    .line 214451
    :goto_0
    move-object v1, v4

    .line 214452
    iget-object v4, p0, LX/1Bv;->i:LX/0oz;

    const-wide v6, 0x3fe999999999999aL    # 0.8

    invoke-virtual {v4, v6, v7}, LX/0oz;->a(D)I

    move-result v4

    .line 214453
    const/4 v5, -0x1

    if-ne v4, v5, :cond_7

    .line 214454
    sget-object v4, LX/1mE;->UNKNOWN:LX/1mE;

    .line 214455
    :goto_1
    move-object v0, v4

    .line 214456
    iget-object v2, p0, LX/1Bv;->u:LX/19j;

    iget-boolean v2, v2, LX/19j;->i:Z

    .line 214457
    iget-object v3, p0, LX/1Bv;->i:LX/0oz;

    invoke-virtual {v3}, LX/0oz;->l()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 214458
    const/4 v2, 0x0

    move v3, v2

    .line 214459
    :goto_2
    if-eqz v3, :cond_2

    move-object v2, v1

    :goto_3
    if-eqz v3, :cond_3

    :goto_4
    iget-object v1, p0, LX/1Bv;->u:LX/19j;

    iget-boolean v1, v1, LX/19j;->h:Z

    .line 214460
    sget-object v3, LX/1mE;->UNKNOWN:LX/1mE;

    if-eq v2, v3, :cond_a

    .line 214461
    :goto_5
    move-object v0, v2

    .line 214462
    return-object v0

    :cond_2
    move-object v2, v0

    goto :goto_3

    :cond_3
    move-object v0, v1

    goto :goto_4

    :cond_4
    move v3, v2

    goto :goto_2

    .line 214463
    :cond_5
    iget-object v4, p0, LX/1Bv;->j:LX/1m0;

    .line 214464
    iget-object v5, v4, LX/1m0;->o:LX/1m5;

    move-object v4, v5

    .line 214465
    invoke-interface {v4}, LX/04m;->a()J

    move-result-wide v4

    .line 214466
    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    iget v6, p1, LX/1Bh;->b:I

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-ltz v4, :cond_6

    .line 214467
    sget-object v4, LX/1mE;->YES:LX/1mE;

    goto :goto_0

    .line 214468
    :cond_6
    sget-object v4, LX/1mE;->UNKNOWN:LX/1mE;

    goto :goto_0

    .line 214469
    :cond_7
    iget v5, p1, LX/1Bh;->b:I

    if-lt v4, v5, :cond_8

    .line 214470
    sget-object v4, LX/1mE;->YES:LX/1mE;

    goto :goto_1

    .line 214471
    :cond_8
    iget v5, p1, LX/1Bh;->b:I

    if-ge v4, v5, :cond_9

    iget-object v4, p0, LX/1Bv;->i:LX/0oz;

    invoke-virtual {v4}, LX/0oz;->l()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 214472
    sget-object v4, LX/1mE;->NO:LX/1mE;

    goto :goto_1

    .line 214473
    :cond_9
    sget-object v4, LX/1mE;->UNKNOWN:LX/1mE;

    goto :goto_1

    .line 214474
    :cond_a
    if-eqz v1, :cond_b

    move-object v2, v0

    .line 214475
    goto :goto_5

    .line 214476
    :cond_b
    sget-object v2, LX/1mE;->UNKNOWN:LX/1mE;

    goto :goto_5
.end method

.method private d()V
    .locals 3

    .prologue
    .line 214439
    invoke-virtual {p0}, LX/1Bv;->b()LX/1mC;

    .line 214440
    new-instance v0, LX/1mH;

    invoke-direct {v0, p0}, LX/1mH;-><init>(LX/1Bv;)V

    iput-object v0, p0, LX/1Bv;->g:LX/0dN;

    .line 214441
    iget-object v0, p0, LX/1Bv;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1mI;->g:LX/0Tn;

    iget-object v2, p0, LX/1Bv;->g:LX/0dN;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 214442
    new-instance v0, LX/1mJ;

    invoke-direct {v0, p0}, LX/1mJ;-><init>(LX/1Bv;)V

    .line 214443
    iget-object v1, p0, LX/1Bv;->m:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 214444
    return-void
.end method

.method private h()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 214375
    iget-object v1, p0, LX/1Bv;->v:LX/0tK;

    invoke-virtual {v1, v0}, LX/0tK;->b(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1Bv;->v:LX/0tK;

    const/4 p0, 0x0

    .line 214376
    invoke-virtual {v1}, LX/0tK;->q()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v1, LX/0tK;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-short v3, LX/0wm;->y:S

    invoke-interface {v2, v3, p0}, LX/0ad;->a(SZ)Z

    move-result v2

    :goto_0
    move v1, v2

    .line 214377
    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    iget-object v2, v1, LX/0tK;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-short v3, LX/0wm;->e:S

    invoke-interface {v2, v3, p0}, LX/0ad;->a(SZ)Z

    move-result v2

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 214438
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/1Bv;->a(Ljava/util/Set;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/util/Set;)Z
    .locals 2
    .param p1    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/046;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 214437
    iget-object v0, p0, LX/1Bv;->t:LX/1Bh;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/1Bv;->a(Ljava/util/Set;LX/1Bh;Z)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/util/Set;LX/1Bh;)Z
    .locals 1
    .param p1    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/046;",
            ">;",
            "LX/1Bh;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 214436
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/1Bv;->a(Ljava/util/Set;LX/1Bh;Z)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/util/Set;LX/1Bh;Z)Z
    .locals 3
    .param p1    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/046;",
            ">;",
            "LX/1Bh;",
            "Z)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 214386
    invoke-virtual {p0}, LX/1Bv;->b()LX/1mC;

    move-result-object v1

    .line 214387
    sget-object v2, LX/1mC;->OFF:LX/1mC;

    if-ne v1, v2, :cond_2

    .line 214388
    if-nez p1, :cond_1

    .line 214389
    :cond_0
    :goto_0
    return v0

    .line 214390
    :cond_1
    sget-object v1, LX/046;->DISABLED_BY_AUTOPLAY_SETTING:LX/046;

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 214391
    if-nez p3, :cond_0

    .line 214392
    :cond_2
    iget-boolean v1, p2, LX/1Bh;->c:Z

    if-nez v1, :cond_9

    .line 214393
    iget-object v1, p0, LX/1Bv;->f:LX/121;

    sget-object v2, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    invoke-virtual {v1, v2}, LX/121;->a(LX/0yY;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, LX/1Bv;->n:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 214394
    :cond_3
    const/4 v1, 0x1

    .line 214395
    :goto_1
    move v1, v1

    .line 214396
    if-eqz v1, :cond_4

    .line 214397
    if-eqz p1, :cond_0

    .line 214398
    sget-object v1, LX/046;->DISABLED_BY_ZERORATING:LX/046;

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 214399
    if-nez p3, :cond_0

    .line 214400
    :cond_4
    invoke-direct {p0}, LX/1Bv;->h()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 214401
    if-eqz p1, :cond_0

    .line 214402
    sget-object v1, LX/046;->DISABLED_BY_DATA_SAVINGS_MODE:LX/046;

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 214403
    if-nez p3, :cond_0

    .line 214404
    :cond_5
    iget-object v1, p0, LX/1Bv;->b:LX/1mC;

    invoke-virtual {v1}, LX/1mC;->isWifiOnly()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 214405
    iget-object v1, p0, LX/1Bv;->k:LX/0ka;

    invoke-virtual {v1}, LX/0ka;->b()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, LX/1Bv;->h:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->h()Z

    move-result v1

    if-eqz v1, :cond_10

    :cond_6
    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 214406
    if-eqz v1, :cond_f

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 214407
    if-eqz v1, :cond_7

    .line 214408
    if-eqz p1, :cond_0

    .line 214409
    sget-object v1, LX/046;->DISABLED_BY_METERED_NETWORK:LX/046;

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 214410
    if-nez p3, :cond_0

    .line 214411
    :cond_7
    iget-object v1, p0, LX/1Bv;->l:Landroid/net/NetworkInfo;

    if-eqz v1, :cond_11

    iget-object v1, p0, LX/1Bv;->l:Landroid/net/NetworkInfo;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v1

    if-eqz v1, :cond_11

    const/4 v1, 0x1

    :goto_4
    move v1, v1

    .line 214412
    if-nez v1, :cond_8

    invoke-direct {p0, p2}, LX/1Bv;->a(LX/1Bh;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 214413
    :cond_8
    if-eqz p1, :cond_0

    .line 214414
    sget-object v1, LX/046;->DISABLED_BY_CONNECTION:LX/046;

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 214415
    if-nez p3, :cond_0

    .line 214416
    :cond_9
    iget-object v1, p0, LX/1Bv;->p:LX/0u7;

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, LX/0u7;->a(I)Z

    move-result v1

    move v1, v1

    .line 214417
    if-eqz v1, :cond_a

    .line 214418
    iget-object v1, p0, LX/1Bv;->p:LX/0u7;

    invoke-virtual {v1}, LX/0u7;->b()LX/0y1;

    move-result-object v1

    .line 214419
    sget-object v2, LX/1mF;->b:[I

    invoke-virtual {v1}, LX/0y1;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 214420
    const/4 v1, 0x0

    :goto_5
    move v1, v1

    .line 214421
    if-nez v1, :cond_a

    .line 214422
    if-eqz p1, :cond_0

    .line 214423
    sget-object v1, LX/046;->DISABLED_BY_LOW_BATTERY:LX/046;

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 214424
    if-nez p3, :cond_0

    .line 214425
    :cond_a
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_12

    iget-object v1, p0, LX/1Bv;->q:Landroid/os/PowerManager;

    invoke-virtual {v1}, Landroid/os/PowerManager;->isPowerSaveMode()Z

    move-result v1

    if-eqz v1, :cond_12

    const/4 v1, 0x1

    :goto_6
    move v1, v1

    .line 214426
    if-eqz v1, :cond_b

    .line 214427
    if-eqz p1, :cond_0

    .line 214428
    sget-object v1, LX/046;->DISABLED_BY_POWER_SAVING:LX/046;

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 214429
    if-nez p3, :cond_0

    .line 214430
    :cond_b
    iget-boolean v1, p2, LX/1Bh;->e:Z

    if-eqz v1, :cond_c

    .line 214431
    if-eqz p1, :cond_0

    .line 214432
    sget-object v1, LX/046;->DISABLED_BY_STREAM_NOT_FOUND:LX/046;

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 214433
    if-nez p3, :cond_0

    .line 214434
    :cond_c
    if-eqz p1, :cond_d

    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_d
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_e
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_f
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_10
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_11
    const/4 v1, 0x0

    goto :goto_4

    .line 214435
    :pswitch_0
    const/4 v1, 0x1

    goto :goto_5

    :cond_12
    const/4 v1, 0x0

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final b()LX/1mC;
    .locals 6

    .prologue
    .line 214380
    iget-object v0, p0, LX/1Bv;->r:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 214381
    iget-object v2, p0, LX/1Bv;->b:LX/1mC;

    if-eqz v2, :cond_0

    iget-wide v2, p0, LX/1Bv;->c:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0x5265c00

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 214382
    :cond_0
    iget-object v2, p0, LX/1Bv;->d:LX/1mD;

    iget-object v3, p0, LX/1Bv;->o:LX/1mC;

    iget-object v4, p0, LX/1Bv;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-virtual {v2, v3, v4}, LX/1mD;->a(LX/1mC;Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/1mC;

    move-result-object v2

    move-object v2, v2

    .line 214383
    iput-object v2, p0, LX/1Bv;->b:LX/1mC;

    .line 214384
    iput-wide v0, p0, LX/1Bv;->c:J

    .line 214385
    :cond_1
    iget-object v0, p0, LX/1Bv;->b:LX/1mC;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 214378
    invoke-virtual {p0}, LX/1Bv;->b()LX/1mC;

    move-result-object v0

    .line 214379
    invoke-virtual {v0}, LX/1mC;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
