.class public final enum LX/1hG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1hG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1hG;

.field public static final enum ERROR:LX/1hG;

.field public static final enum FATAL:LX/1hG;

.field public static final enum INFO:LX/1hG;

.field public static final enum WARNING:LX/1hG;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 295678
    new-instance v0, LX/1hG;

    const-string v1, "INFO"

    invoke-direct {v0, v1, v2}, LX/1hG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1hG;->INFO:LX/1hG;

    .line 295679
    new-instance v0, LX/1hG;

    const-string v1, "WARNING"

    invoke-direct {v0, v1, v3}, LX/1hG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1hG;->WARNING:LX/1hG;

    .line 295680
    new-instance v0, LX/1hG;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v4}, LX/1hG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1hG;->ERROR:LX/1hG;

    .line 295681
    new-instance v0, LX/1hG;

    const-string v1, "FATAL"

    invoke-direct {v0, v1, v5}, LX/1hG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1hG;->FATAL:LX/1hG;

    .line 295682
    const/4 v0, 0x4

    new-array v0, v0, [LX/1hG;

    sget-object v1, LX/1hG;->INFO:LX/1hG;

    aput-object v1, v0, v2

    sget-object v1, LX/1hG;->WARNING:LX/1hG;

    aput-object v1, v0, v3

    sget-object v1, LX/1hG;->ERROR:LX/1hG;

    aput-object v1, v0, v4

    sget-object v1, LX/1hG;->FATAL:LX/1hG;

    aput-object v1, v0, v5

    sput-object v0, LX/1hG;->$VALUES:[LX/1hG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 295677
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1hG;
    .locals 1

    .prologue
    .line 295676
    const-class v0, LX/1hG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1hG;

    return-object v0
.end method

.method public static values()[LX/1hG;
    .locals 1

    .prologue
    .line 295675
    sget-object v0, LX/1hG;->$VALUES:[LX/1hG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1hG;

    return-object v0
.end method
