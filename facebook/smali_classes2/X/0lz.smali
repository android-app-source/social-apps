.class public LX/0lz;
.super LX/0m0;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public _registeredSubtypes:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "LX/4qu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 131536
    invoke-direct {p0}, LX/0m0;-><init>()V

    return-void
.end method

.method private a(LX/0lN;LX/4qu;LX/0m4;LX/0lU;Ljava/util/HashMap;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lN;",
            "LX/4qu;",
            "LX/0m4",
            "<*>;",
            "LX/0lU;",
            "Ljava/util/HashMap",
            "<",
            "LX/4qu;",
            "LX/4qu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 131513
    invoke-virtual {p2}, LX/4qu;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 131514
    invoke-virtual {p4, p1}, LX/0lU;->f(LX/0lN;)Ljava/lang/String;

    move-result-object v1

    .line 131515
    if-eqz v1, :cond_0

    .line 131516
    new-instance v0, LX/4qu;

    .line 131517
    iget-object v2, p2, LX/4qu;->_class:Ljava/lang/Class;

    move-object v2, v2

    .line 131518
    invoke-direct {v0, v2, v1}, LX/4qu;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    move-object p2, v0

    .line 131519
    :cond_0
    invoke-virtual {p5, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 131520
    invoke-virtual {p2}, LX/4qu;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 131521
    invoke-virtual {p5, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4qu;

    .line 131522
    invoke-virtual {v0}, LX/4qu;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 131523
    invoke-virtual {p5, p2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131524
    :cond_1
    return-void

    .line 131525
    :cond_2
    invoke-virtual {p5, p2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131526
    invoke-virtual {p4, p1}, LX/0lU;->c(LX/0lO;)Ljava/util/List;

    move-result-object v0

    .line 131527
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 131528
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4qu;

    .line 131529
    iget-object v1, v0, LX/4qu;->_class:Ljava/lang/Class;

    move-object v1, v1

    .line 131530
    invoke-static {v1, p4, p3}, LX/0lN;->b(Ljava/lang/Class;LX/0lU;LX/0m5;)LX/0lN;

    move-result-object v1

    .line 131531
    invoke-virtual {v0}, LX/4qu;->c()Z

    move-result v2

    if-nez v2, :cond_3

    .line 131532
    new-instance v2, LX/4qu;

    .line 131533
    iget-object v3, v0, LX/4qu;->_class:Ljava/lang/Class;

    move-object v0, v3

    .line 131534
    invoke-virtual {p4, v1}, LX/0lU;->f(LX/0lN;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, LX/4qu;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    :goto_1
    move-object v0, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 131535
    invoke-direct/range {v0 .. v5}, LX/0lz;->a(LX/0lN;LX/4qu;LX/0m4;LX/0lU;Ljava/util/HashMap;)V

    goto :goto_0

    :cond_3
    move-object v2, v0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0lN;LX/0m4;LX/0lU;)Ljava/util/Collection;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lN;",
            "LX/0m4",
            "<*>;",
            "LX/0lU;",
            ")",
            "Ljava/util/Collection",
            "<",
            "LX/4qu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131501
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 131502
    iget-object v0, p0, LX/0lz;->_registeredSubtypes:Ljava/util/LinkedHashSet;

    if-eqz v0, :cond_1

    .line 131503
    invoke-virtual {p1}, LX/0lO;->d()Ljava/lang/Class;

    move-result-object v6

    .line 131504
    iget-object v0, p0, LX/0lz;->_registeredSubtypes:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4qu;

    .line 131505
    iget-object v0, v2, LX/4qu;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 131506
    invoke-virtual {v6, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131507
    iget-object v0, v2, LX/4qu;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 131508
    invoke-static {v0, p3, p2}, LX/0lN;->b(Ljava/lang/Class;LX/0lU;LX/0m5;)LX/0lN;

    move-result-object v1

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    .line 131509
    invoke-direct/range {v0 .. v5}, LX/0lz;->a(LX/0lN;LX/4qu;LX/0m4;LX/0lU;Ljava/util/HashMap;)V

    goto :goto_0

    .line 131510
    :cond_1
    new-instance v2, LX/4qu;

    invoke-virtual {p1}, LX/0lO;->d()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v2, v0, v1}, LX/4qu;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    .line 131511
    invoke-direct/range {v0 .. v5}, LX/0lz;->a(LX/0lN;LX/4qu;LX/0m4;LX/0lU;Ljava/util/HashMap;)V

    .line 131512
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final a(LX/2An;LX/0m4;LX/0lU;LX/0lJ;)Ljava/util/Collection;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2An;",
            "LX/0m4",
            "<*>;",
            "LX/0lU;",
            "LX/0lJ;",
            ")",
            "Ljava/util/Collection",
            "<",
            "LX/4qu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131480
    if-nez p4, :cond_1

    invoke-virtual {p1}, LX/0lO;->d()Ljava/lang/Class;

    move-result-object v0

    move-object v6, v0

    .line 131481
    :goto_0
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 131482
    iget-object v0, p0, LX/0lz;->_registeredSubtypes:Ljava/util/LinkedHashSet;

    if-eqz v0, :cond_2

    .line 131483
    iget-object v0, p0, LX/0lz;->_registeredSubtypes:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4qu;

    .line 131484
    iget-object v0, v2, LX/4qu;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 131485
    invoke-virtual {v6, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131486
    iget-object v0, v2, LX/4qu;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 131487
    invoke-static {v0, p3, p2}, LX/0lN;->b(Ljava/lang/Class;LX/0lU;LX/0m5;)LX/0lN;

    move-result-object v1

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    .line 131488
    invoke-direct/range {v0 .. v5}, LX/0lz;->a(LX/0lN;LX/4qu;LX/0m4;LX/0lU;Ljava/util/HashMap;)V

    goto :goto_1

    .line 131489
    :cond_1
    iget-object v0, p4, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 131490
    move-object v6, v0

    goto :goto_0

    .line 131491
    :cond_2
    invoke-virtual {p3, p1}, LX/0lU;->c(LX/0lO;)Ljava/util/List;

    move-result-object v0

    .line 131492
    if-eqz v0, :cond_3

    .line 131493
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4qu;

    .line 131494
    iget-object v0, v2, LX/4qu;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 131495
    invoke-static {v0, p3, p2}, LX/0lN;->b(Ljava/lang/Class;LX/0lU;LX/0m5;)LX/0lN;

    move-result-object v1

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    .line 131496
    invoke-direct/range {v0 .. v5}, LX/0lz;->a(LX/0lN;LX/4qu;LX/0m4;LX/0lU;Ljava/util/HashMap;)V

    goto :goto_2

    .line 131497
    :cond_3
    new-instance v2, LX/4qu;

    const/4 v0, 0x0

    invoke-direct {v2, v6, v0}, LX/4qu;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    .line 131498
    invoke-static {v6, p3, p2}, LX/0lN;->b(Ljava/lang/Class;LX/0lU;LX/0m5;)LX/0lN;

    move-result-object v1

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    .line 131499
    invoke-direct/range {v0 .. v5}, LX/0lz;->a(LX/0lN;LX/4qu;LX/0m4;LX/0lU;Ljava/util/HashMap;)V

    .line 131500
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method
