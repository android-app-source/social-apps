.class public abstract LX/1Xf;
.super LX/12V;
.source ""


# instance fields
.field public final c:LX/1Xf;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILX/1Xf;)V
    .locals 1

    .prologue
    .line 271569
    invoke-direct {p0}, LX/12V;-><init>()V

    .line 271570
    iput p1, p0, LX/1Xf;->a:I

    .line 271571
    const/4 v0, -0x1

    iput v0, p0, LX/1Xf;->b:I

    .line 271572
    iput-object p2, p0, LX/1Xf;->c:LX/1Xf;

    .line 271573
    return-void
.end method


# virtual methods
.method public synthetic a()LX/12V;
    .locals 1

    .prologue
    .line 271567
    iget-object v0, p0, LX/1Xf;->c:LX/1Xf;

    move-object v0, v0

    .line 271568
    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271566
    iget-object v0, p0, LX/1Xf;->d:Ljava/lang/String;

    return-object v0
.end method

.method public abstract j()LX/15z;
.end method

.method public abstract k()LX/15z;
.end method

.method public abstract l()LX/0lF;
.end method

.method public abstract m()Z
.end method

.method public final n()LX/1Xf;
    .locals 4

    .prologue
    .line 271558
    invoke-virtual {p0}, LX/1Xf;->l()LX/0lF;

    move-result-object v1

    .line 271559
    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No current node"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 271560
    :cond_0
    invoke-virtual {v1}, LX/0lF;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 271561
    new-instance v0, LX/1Xi;

    invoke-direct {v0, v1, p0}, LX/1Xi;-><init>(LX/0lF;LX/1Xf;)V

    .line 271562
    :goto_0
    return-object v0

    .line 271563
    :cond_1
    invoke-virtual {v1}, LX/0lF;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 271564
    new-instance v0, LX/1Xe;

    invoke-direct {v0, v1, p0}, LX/1Xe;-><init>(LX/0lF;LX/1Xf;)V

    goto :goto_0

    .line 271565
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Current node of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
