.class public final LX/0vc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 158023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/view/View;Landroid/view/View;)I
    .locals 4

    .prologue
    .line 158024
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/18u;

    .line 158025
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/18u;

    .line 158026
    iget-boolean v2, v0, LX/18u;->a:Z

    iget-boolean v3, v1, LX/18u;->a:Z

    if-eq v2, v3, :cond_1

    .line 158027
    iget-boolean v0, v0, LX/18u;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 158028
    :goto_0
    return v0

    .line 158029
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 158030
    :cond_1
    iget v0, v0, LX/18u;->e:I

    iget v1, v1, LX/18u;->e:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 158031
    check-cast p1, Landroid/view/View;

    check-cast p2, Landroid/view/View;

    invoke-static {p1, p2}, LX/0vc;->a(Landroid/view/View;Landroid/view/View;)I

    move-result v0

    return v0
.end method
