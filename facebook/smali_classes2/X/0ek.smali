.class public LX/0ek;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0el;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0Wn;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/resources/impl/loading/LanguagePackDownloader;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0em;

.field private final e:LX/0en;

.field private final f:LX/0eo;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 103892
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "i18n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, LX/0ek;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0ek;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Wn;LX/0Ot;LX/0em;LX/0en;LX/0eo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Wn;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/resources/impl/loading/LanguagePackDownloader;",
            ">;",
            "LX/0em;",
            "LX/0en;",
            "LX/0eo;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 103882
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103883
    iput-object p1, p0, LX/0ek;->b:LX/0Wn;

    .line 103884
    iput-object p2, p0, LX/0ek;->c:LX/0Ot;

    .line 103885
    iput-object p3, p0, LX/0ek;->d:LX/0em;

    .line 103886
    iput-object p4, p0, LX/0ek;->e:LX/0en;

    .line 103887
    iput-object p5, p0, LX/0ek;->f:LX/0eo;

    .line 103888
    return-void
.end method


# virtual methods
.method public final a(LX/0ed;)Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 103877
    iget-object v0, p0, LX/0ek;->f:LX/0eo;

    .line 103878
    iget-object v1, v0, LX/0eo;->b:LX/0en;

    iget-object v3, v0, LX/0eo;->e:Landroid/content/Context;

    invoke-static {v3}, LX/0em;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    new-instance v3, LX/0fX;

    invoke-direct {v3, v0}, LX/0fX;-><init>(LX/0eo;)V

    invoke-virtual {v1, v2, v3}, LX/0en;->a(Ljava/io/File;LX/0Rl;)Z

    .line 103879
    iget-object v0, p0, LX/0ek;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->a(LX/0ed;)Ljava/io/File;

    move-result-object v0

    .line 103880
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    .line 103881
    :cond_0
    invoke-static {v0}, LX/0en;->a(Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 103889
    iget-object v0, p0, LX/0ek;->b:LX/0Wn;

    .line 103890
    const v1, 0x440002

    const-string p0, "FbResourcesLoadingDownloadedStrings"

    invoke-static {v0, v1, p0}, LX/0Wn;->a(LX/0Wn;ILjava/lang/String;)V

    .line 103891
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 103874
    iget-object v0, p0, LX/0ek;->b:LX/0Wn;

    .line 103875
    const v1, 0x440002

    const-string p0, "FbResourcesLoadingDownloadedStrings"

    invoke-static {v0, v1, p0}, LX/0Wn;->b(LX/0Wn;ILjava/lang/String;)V

    .line 103876
    return-void
.end method

.method public final b(LX/0ed;)V
    .locals 5

    .prologue
    .line 103858
    sget-object v0, LX/0ek;->a:Ljava/lang/String;

    const-string v1, "onFailure() called"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 103859
    iget-object v0, p0, LX/0ek;->d:LX/0em;

    .line 103860
    const/4 v1, 0x0

    .line 103861
    iget-object v2, p1, LX/0ed;->e:LX/0eh;

    move-object v2, v2

    .line 103862
    sget-object v3, LX/0eh;->FBSTR:LX/0eh;

    if-ne v2, v3, :cond_2

    .line 103863
    invoke-virtual {v0, p1}, LX/0em;->a(LX/0ed;)Ljava/io/File;

    move-result-object v1

    .line 103864
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 103865
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 103866
    sget-object v2, LX/0em;->a:Ljava/lang/String;

    const-string v3, "Deleted potentially corrupted lang file: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 p0, 0x0

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, p0

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103867
    :cond_1
    return-void

    .line 103868
    :cond_2
    iget-object v2, p1, LX/0ed;->e:LX/0eh;

    move-object v2, v2

    .line 103869
    sget-object v3, LX/0eh;->LANGPACK:LX/0eh;

    if-ne v2, v3, :cond_0

    .line 103870
    invoke-virtual {v0, p1}, LX/0em;->b(LX/0ed;)LX/0ff;

    move-result-object v2

    .line 103871
    if-eqz v2, :cond_0

    .line 103872
    iget-object v1, p1, LX/0ed;->a:Landroid/content/Context;

    move-object v1, v1

    .line 103873
    invoke-virtual {v0, v1, v2}, LX/0em;->a(Landroid/content/Context;LX/0ff;)Ljava/io/File;

    move-result-object v1

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 103854
    sget-object v0, LX/0ek;->a:Ljava/lang/String;

    const-string v1, "onParseFailure() called"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 103855
    iget-object v0, p0, LX/0ek;->b:LX/0Wn;

    .line 103856
    const v1, 0x440002

    const-string p0, "FbResourcesLoadingDownloadedStrings"

    invoke-static {v0, v1, p0}, LX/0Wn;->c(LX/0Wn;ILjava/lang/String;)V

    .line 103857
    return-void
.end method
