.class public final enum LX/1K2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1K2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1K2;

.field public static final enum NEWSFEED:LX/1K2;

.field public static final enum PERMALINK:LX/1K2;

.field public static final enum TIMELINE:LX/1K2;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 230820
    new-instance v0, LX/1K2;

    const-string v1, "NEWSFEED"

    invoke-direct {v0, v1, v2}, LX/1K2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1K2;->NEWSFEED:LX/1K2;

    .line 230821
    new-instance v0, LX/1K2;

    const-string v1, "TIMELINE"

    invoke-direct {v0, v1, v3}, LX/1K2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1K2;->TIMELINE:LX/1K2;

    .line 230822
    new-instance v0, LX/1K2;

    const-string v1, "PERMALINK"

    invoke-direct {v0, v1, v4}, LX/1K2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1K2;->PERMALINK:LX/1K2;

    .line 230823
    const/4 v0, 0x3

    new-array v0, v0, [LX/1K2;

    sget-object v1, LX/1K2;->NEWSFEED:LX/1K2;

    aput-object v1, v0, v2

    sget-object v1, LX/1K2;->TIMELINE:LX/1K2;

    aput-object v1, v0, v3

    sget-object v1, LX/1K2;->PERMALINK:LX/1K2;

    aput-object v1, v0, v4

    sput-object v0, LX/1K2;->$VALUES:[LX/1K2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 230824
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1K2;
    .locals 1

    .prologue
    .line 230825
    const-class v0, LX/1K2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1K2;

    return-object v0
.end method

.method public static values()[LX/1K2;
    .locals 1

    .prologue
    .line 230826
    sget-object v0, LX/1K2;->$VALUES:[LX/1K2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1K2;

    return-object v0
.end method
