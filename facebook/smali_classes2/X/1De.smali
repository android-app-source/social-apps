.class public LX/1De;
.super Landroid/content/ContextWrapper;
.source ""


# static fields
.field public static final a:LX/1Dg;


# instance fields
.field public final b:LX/1Dq;

.field public final c:Ljava/lang/String;

.field public final d:LX/1cp;

.field public final e:LX/1mg;

.field public f:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public g:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public h:LX/1Ds;

.field public i:I

.field public j:I

.field public k:LX/1dV;

.field private l:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field

.field private m:I
    .annotation build Landroid/support/annotation/AttrRes;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 217862
    new-instance v0, LX/1Df;

    invoke-direct {v0}, LX/1Df;-><init>()V

    sput-object v0, LX/1De;->a:LX/1Dg;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 217860
    invoke-direct {p0, p1, v0, v0, v0}, LX/1De;-><init>(Landroid/content/Context;Ljava/lang/String;LX/1cp;LX/1mg;)V

    .line 217861
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1mg;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 217858
    invoke-direct {p0, p1, v0, v0, p2}, LX/1De;-><init>(Landroid/content/Context;Ljava/lang/String;LX/1cp;LX/1mg;)V

    .line 217859
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;LX/1cp;)V
    .locals 1

    .prologue
    .line 217856
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/1De;-><init>(Landroid/content/Context;Ljava/lang/String;LX/1cp;LX/1mg;)V

    .line 217857
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;LX/1cp;LX/1mg;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 217831
    instance-of v0, p1, LX/1De;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, LX/1De;

    invoke-virtual {v0}, LX/1De;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 217832
    new-instance v0, LX/1Dq;

    invoke-direct {v0}, LX/1Dq;-><init>()V

    iput-object v0, p0, LX/1De;->b:LX/1Dq;

    .line 217833
    iput v2, p0, LX/1De;->l:I

    .line 217834
    iput v2, p0, LX/1De;->m:I

    .line 217835
    if-eqz p3, :cond_1

    if-nez p2, :cond_1

    .line 217836
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "When a ComponentsLogger is set, a LogTag must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p1

    .line 217837
    goto :goto_0

    .line 217838
    :cond_1
    instance-of v0, p1, LX/1De;

    if-eqz v0, :cond_5

    move-object v0, p1

    check-cast v0, LX/1De;

    move-object v4, v0

    .line 217839
    :goto_1
    if-eqz v4, :cond_6

    if-nez p2, :cond_6

    if-nez p3, :cond_6

    move v3, v1

    .line 217840
    :goto_2
    if-eqz v4, :cond_7

    if-nez p4, :cond_7

    move v0, v1

    .line 217841
    :goto_3
    if-eqz v4, :cond_8

    .line 217842
    iget-object v1, v4, LX/1De;->f:LX/1X1;

    iput-object v1, p0, LX/1De;->f:LX/1X1;

    .line 217843
    iget-object v1, v4, LX/1De;->h:LX/1Ds;

    iput-object v1, p0, LX/1De;->h:LX/1Ds;

    .line 217844
    iget v1, v4, LX/1De;->i:I

    iput v1, p0, LX/1De;->i:I

    .line 217845
    iget v1, v4, LX/1De;->j:I

    iput v1, p0, LX/1De;->j:I

    .line 217846
    iget-object v1, v4, LX/1De;->g:LX/1X1;

    iput-object v1, p0, LX/1De;->g:LX/1X1;

    .line 217847
    iget-object v1, v4, LX/1De;->k:LX/1dV;

    iput-object v1, p0, LX/1De;->k:LX/1dV;

    .line 217848
    :goto_4
    if-eqz v3, :cond_2

    iget-object p3, v4, LX/1De;->d:LX/1cp;

    :cond_2
    iput-object p3, p0, LX/1De;->d:LX/1cp;

    .line 217849
    if-eqz v3, :cond_3

    iget-object p2, v4, LX/1De;->c:Ljava/lang/String;

    :cond_3
    iput-object p2, p0, LX/1De;->c:Ljava/lang/String;

    .line 217850
    if-eqz v0, :cond_4

    iget-object p4, v4, LX/1De;->e:LX/1mg;

    :cond_4
    iput-object p4, p0, LX/1De;->e:LX/1mg;

    .line 217851
    return-void

    .line 217852
    :cond_5
    const/4 v0, 0x0

    move-object v4, v0

    goto :goto_1

    :cond_6
    move v3, v2

    .line 217853
    goto :goto_2

    :cond_7
    move v0, v2

    .line 217854
    goto :goto_3

    .line 217855
    :cond_8
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-static {v1}, LX/1Ds;->a(Landroid/content/res/Configuration;)LX/1Ds;

    move-result-object v1

    iput-object v1, p0, LX/1De;->h:LX/1Ds;

    goto :goto_4
.end method

.method public static a(LX/1De;LX/1X1;)LX/1De;
    .locals 2

    .prologue
    .line 217827
    invoke-static {p0}, LX/1De;->i(LX/1De;)LX/1De;

    move-result-object v0

    .line 217828
    iput-object p1, v0, LX/1De;->g:LX/1X1;

    .line 217829
    iget-object v1, p0, LX/1De;->k:LX/1dV;

    iput-object v1, v0, LX/1De;->k:LX/1dV;

    .line 217830
    return-object v0
.end method

.method public static a(LX/1De;LX/1Dg;II)V
    .locals 3
    .param p1    # LX/1Dg;
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 217863
    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    .line 217864
    :cond_0
    invoke-virtual {p0, p2, p3}, LX/1De;->a(II)V

    .line 217865
    const/4 v0, 0x0

    sget-object v1, LX/03r;->ComponentLayout:[I

    invoke-virtual {p0, v0, v1, p2, p3}, LX/1De;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 217866
    invoke-virtual {p1, v0}, LX/1Dg;->a(Landroid/content/res/TypedArray;)V

    .line 217867
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 217868
    invoke-virtual {p0, v2, v2}, LX/1De;->a(II)V

    .line 217869
    :cond_1
    return-void
.end method

.method public static i(LX/1De;)LX/1De;
    .locals 1

    .prologue
    .line 217810
    new-instance v0, LX/1De;

    invoke-direct {v0, p0}, LX/1De;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final a([II)Landroid/content/res/TypedArray;
    .locals 2
    .param p2    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param

    .prologue
    .line 217811
    const/4 v0, 0x0

    if-eqz p2, :cond_0

    :goto_0
    iget v1, p0, LX/1De;->l:I

    invoke-virtual {p0, v0, p1, p2, v1}, LX/1De;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    return-object v0

    :cond_0
    iget p2, p0, LX/1De;->m:I

    goto :goto_0
.end method

.method public final a(II)V
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    .line 217812
    iput p1, p0, LX/1De;->m:I

    .line 217813
    iput p2, p0, LX/1De;->l:I

    .line 217814
    return-void
.end method

.method public final a(LX/48B;)V
    .locals 2

    .prologue
    .line 217815
    iget-object v0, p0, LX/1De;->k:LX/1dV;

    if-nez v0, :cond_0

    .line 217816
    :goto_0
    return-void

    .line 217817
    :cond_0
    iget-object v0, p0, LX/1De;->k:LX/1dV;

    iget-object v1, p0, LX/1De;->g:LX/1X1;

    .line 217818
    iget-object p0, v1, LX/1X1;->c:Ljava/lang/String;

    move-object v1, p0

    .line 217819
    const/4 p0, 0x0

    invoke-static {v0, v1, p1, p0}, LX/1dV;->a(LX/1dV;Ljava/lang/String;LX/48B;Z)V

    .line 217820
    goto :goto_0
.end method

.method public final b(LX/48B;)V
    .locals 2

    .prologue
    .line 217821
    iget-object v0, p0, LX/1De;->k:LX/1dV;

    if-nez v0, :cond_0

    .line 217822
    :goto_0
    return-void

    .line 217823
    :cond_0
    iget-object v0, p0, LX/1De;->k:LX/1dV;

    iget-object v1, p0, LX/1De;->g:LX/1X1;

    .line 217824
    iget-object p0, v1, LX/1X1;->c:Ljava/lang/String;

    move-object v1, p0

    .line 217825
    invoke-virtual {v0, v1, p1}, LX/1dV;->b(Ljava/lang/String;LX/48B;)V

    goto :goto_0
.end method

.method public final c()LX/1cp;
    .locals 1

    .prologue
    .line 217826
    iget-object v0, p0, LX/1De;->d:LX/1cp;

    return-object v0
.end method
