.class public final LX/14m;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public final synthetic a:LX/14l;


# direct methods
.method public constructor <init>(LX/14l;)V
    .locals 0

    .prologue
    .line 179184
    iput-object p1, p0, LX/14m;->a:LX/14l;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x790c745c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 179185
    iget-object v1, p0, LX/14m;->a:LX/14l;

    iget-boolean v1, v1, LX/14l;->f:Z

    if-nez v1, :cond_0

    .line 179186
    sget-object v1, LX/14l;->a:Ljava/lang/Class;

    const-string v2, "Called onReceive after it was unregistered."

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 179187
    const/16 v1, 0x27

    const v2, 0x77431c26    # 3.9573E33f

    invoke-static {p2, v3, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 179188
    :goto_0
    return-void

    .line 179189
    :cond_0
    iget-object v1, p0, LX/14m;->a:LX/14l;

    invoke-virtual {v1, p1, p2}, LX/14l;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 179190
    const v1, -0x1bba38f7

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0
.end method
