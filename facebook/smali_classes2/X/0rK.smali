.class public final LX/0rK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LX/0rK;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0qg;

.field public b:I

.field public c:I

.field public d:I

.field public e:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0qg;II)V
    .locals 1

    .prologue
    .line 149436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149437
    const/4 v0, 0x0

    iput v0, p0, LX/0rK;->d:I

    .line 149438
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/0rK;->e:LX/01J;

    .line 149439
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, LX/0rK;->f:Ljava/util/Set;

    .line 149440
    iput-object p1, p0, LX/0rK;->a:LX/0qg;

    .line 149441
    iput p2, p0, LX/0rK;->b:I

    .line 149442
    iput p3, p0, LX/0rK;->c:I

    .line 149443
    return-void
.end method

.method private a(LX/0rK;)I
    .locals 3

    .prologue
    .line 149444
    iget v0, p0, LX/0rK;->b:I

    iget v1, p0, LX/0rK;->d:I

    add-int/2addr v0, v1

    .line 149445
    iget v1, p1, LX/0rK;->b:I

    iget v2, p1, LX/0rK;->d:I

    add-int/2addr v1, v2

    .line 149446
    if-ne v0, v1, :cond_0

    .line 149447
    iget v0, p0, LX/0rK;->c:I

    iget v1, p1, LX/0rK;->c:I

    sub-int/2addr v0, v1

    .line 149448
    :goto_0
    return v0

    :cond_0
    sub-int/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 149449
    check-cast p1, LX/0rK;

    invoke-direct {p0, p1}, LX/0rK;->a(LX/0rK;)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 149450
    if-ne p0, p1, :cond_1

    .line 149451
    :cond_0
    :goto_0
    return v0

    .line 149452
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 149453
    goto :goto_0

    .line 149454
    :cond_3
    check-cast p1, LX/0rK;

    .line 149455
    iget-object v2, p0, LX/0rK;->a:LX/0qg;

    iget-object v3, p1, LX/0rK;->a:LX/0qg;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-direct {p0, p1}, LX/0rK;->a(LX/0rK;)I

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 149456
    iget-object v0, p0, LX/0rK;->a:LX/0qg;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
