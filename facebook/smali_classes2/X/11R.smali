.class public LX/11R;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/11S;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;

.field public static b:LX/11S;

.field private static volatile j:LX/11R;


# instance fields
.field public final c:Landroid/content/Context;

.field public final d:LX/0SG;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/11T;

.field public g:Ljava/util/Calendar;

.field public h:Ljava/util/Calendar;

.field private i:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 172530
    const-class v0, LX/11R;

    sput-object v0, LX/11R;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0SG;LX/0Or;LX/11T;LX/03V;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;",
            "LX/11T;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 172449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172450
    iput-object p1, p0, LX/11R;->c:Landroid/content/Context;

    .line 172451
    iput-object p2, p0, LX/11R;->d:LX/0SG;

    .line 172452
    iput-object p5, p0, LX/11R;->i:LX/03V;

    .line 172453
    iput-object p3, p0, LX/11R;->e:LX/0Or;

    .line 172454
    iput-object p4, p0, LX/11R;->f:LX/11T;

    .line 172455
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    .line 172456
    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, LX/11R;->g:Ljava/util/Calendar;

    .line 172457
    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/11R;->h:Ljava/util/Calendar;

    .line 172458
    return-void
.end method

.method public static a(LX/0QB;)LX/11R;
    .locals 9

    .prologue
    .line 172517
    sget-object v0, LX/11R;->j:LX/11R;

    if-nez v0, :cond_1

    .line 172518
    const-class v1, LX/11R;

    monitor-enter v1

    .line 172519
    :try_start_0
    sget-object v0, LX/11R;->j:LX/11R;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 172520
    if-eqz v2, :cond_0

    .line 172521
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 172522
    new-instance v3, LX/11R;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    const/16 v6, 0x161a

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/11T;->a(LX/0QB;)LX/11T;

    move-result-object v7

    check-cast v7, LX/11T;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-direct/range {v3 .. v8}, LX/11R;-><init>(Landroid/content/Context;LX/0SG;LX/0Or;LX/11T;LX/03V;)V

    .line 172523
    move-object v0, v3

    .line 172524
    sput-object v0, LX/11R;->j:LX/11R;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 172525
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 172526
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 172527
    :cond_1
    sget-object v0, LX/11R;->j:LX/11R;

    return-object v0

    .line 172528
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 172529
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(JJJ)LX/1yz;
    .locals 5

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x1

    .line 172499
    const-wide/32 v0, 0xea60

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 172500
    sget-object v0, LX/1yz;->LESS_THAN_MIN:LX/1yz;

    .line 172501
    :goto_0
    return-object v0

    .line 172502
    :cond_0
    const-wide/32 v0, 0x493e0

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    .line 172503
    sget-object v0, LX/1yz;->LESS_THAN_5_MINS:LX/1yz;

    goto :goto_0

    .line 172504
    :cond_1
    const-wide/32 v0, 0x36ee80

    cmp-long v0, p1, v0

    if-gez v0, :cond_2

    .line 172505
    sget-object v0, LX/1yz;->LESS_THAN_HOUR:LX/1yz;

    goto :goto_0

    .line 172506
    :cond_2
    const-wide/32 v0, 0x5265c00

    cmp-long v0, p1, v0

    if-gez v0, :cond_4

    .line 172507
    invoke-static {p0, p3, p4, p5, p6}, LX/11R;->d(LX/11R;JJ)V

    .line 172508
    iget-object v0, p0, LX/11R;->g:Ljava/util/Calendar;

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v1, p0, LX/11R;->h:Ljava/util/Calendar;

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 172509
    sget-object v0, LX/1yz;->SAME_DAY:LX/1yz;

    goto :goto_0

    .line 172510
    :cond_3
    sget-object v0, LX/1yz;->LESS_THAN_ONE_DAY:LX/1yz;

    goto :goto_0

    .line 172511
    :cond_4
    const-wide/32 v0, 0x14997000

    cmp-long v0, p1, v0

    if-gez v0, :cond_5

    .line 172512
    sget-object v0, LX/1yz;->LESS_THAN_4_DAYS:LX/1yz;

    goto :goto_0

    .line 172513
    :cond_5
    invoke-static {p0, p3, p4, p5, p6}, LX/11R;->d(LX/11R;JJ)V

    .line 172514
    iget-object v0, p0, LX/11R;->g:Ljava/util/Calendar;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v1, p0, LX/11R;->h:Ljava/util/Calendar;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_6

    .line 172515
    sget-object v0, LX/1yz;->SAME_YEAR:LX/1yz;

    goto :goto_0

    .line 172516
    :cond_6
    sget-object v0, LX/1yz;->DIFFERENT_YEAR:LX/1yz;

    goto :goto_0
.end method

.method public static a(LX/11R;JII)Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0x4a01

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 172490
    invoke-virtual {p0, p1, p2}, LX/11R;->a(J)J

    move-result-wide v0

    .line 172491
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 172492
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    invoke-static {v2, p1, p2, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, p3, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 172493
    :goto_0
    return-object v0

    .line 172494
    :cond_0
    const-wide/16 v2, 0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 172495
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    invoke-static {v2, p1, p2, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, p4, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172496
    :cond_1
    const-wide/16 v2, 0x7

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 172497
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    const v1, 0xca03

    invoke-static {v0, p1, p2, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172498
    :cond_2
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    const v1, 0x14a11

    invoke-static {v0, p1, p2, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/util/Calendar;J)Ljava/util/Calendar;
    .locals 2

    .prologue
    .line 172485
    iget-object v0, p0, LX/11R;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    .line 172486
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    if-eq v1, v0, :cond_0

    .line 172487
    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object p1

    .line 172488
    :cond_0
    invoke-virtual {p1, p2, p3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 172489
    return-object p1
.end method

.method private b(JJ)J
    .locals 9

    .prologue
    const/16 v5, 0xe

    const/16 v4, 0xd

    const/16 v3, 0xc

    const/16 v2, 0xb

    const/4 v1, 0x0

    .line 172464
    iget-object v0, p0, LX/11R;->g:Ljava/util/Calendar;

    invoke-direct {p0, v0, p1, p2}, LX/11R;->a(Ljava/util/Calendar;J)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/11R;->g:Ljava/util/Calendar;

    .line 172465
    iget-object v0, p0, LX/11R;->g:Ljava/util/Calendar;

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    .line 172466
    iget-object v0, p0, LX/11R;->g:Ljava/util/Calendar;

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->set(II)V

    .line 172467
    iget-object v0, p0, LX/11R;->g:Ljava/util/Calendar;

    invoke-virtual {v0, v4, v1}, Ljava/util/Calendar;->set(II)V

    .line 172468
    iget-object v0, p0, LX/11R;->g:Ljava/util/Calendar;

    invoke-virtual {v0, v5, v1}, Ljava/util/Calendar;->set(II)V

    .line 172469
    iget-object v0, p0, LX/11R;->h:Ljava/util/Calendar;

    invoke-direct {p0, v0, p3, p4}, LX/11R;->a(Ljava/util/Calendar;J)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/11R;->h:Ljava/util/Calendar;

    .line 172470
    iget-object v0, p0, LX/11R;->h:Ljava/util/Calendar;

    invoke-virtual {v0, p3, p4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 172471
    iget-object v0, p0, LX/11R;->h:Ljava/util/Calendar;

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    .line 172472
    iget-object v0, p0, LX/11R;->h:Ljava/util/Calendar;

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->set(II)V

    .line 172473
    iget-object v0, p0, LX/11R;->h:Ljava/util/Calendar;

    invoke-virtual {v0, v4, v1}, Ljava/util/Calendar;->set(II)V

    .line 172474
    iget-object v0, p0, LX/11R;->h:Ljava/util/Calendar;

    invoke-virtual {v0, v5, v1}, Ljava/util/Calendar;->set(II)V

    .line 172475
    iget-object v0, p0, LX/11R;->h:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iget-object v2, p0, LX/11R;->g:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 172476
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    iget-object v3, p0, LX/11R;->g:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v2

    .line 172477
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    iget-object v4, p0, LX/11R;->h:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v3

    .line 172478
    if-eqz v2, :cond_2

    if-nez v3, :cond_2

    .line 172479
    const-wide/32 v2, 0x36ee80

    sub-long/2addr v0, v2

    .line 172480
    :cond_0
    :goto_0
    const-wide/32 v2, 0x5265c00

    rem-long v2, v0, v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/11R;->i:LX/03V;

    if-eqz v2, :cond_1

    .line 172481
    iget-object v2, p0, LX/11R;->i:LX/03V;

    sget-object v3, LX/11R;->a:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getDayAlignedDiffMs result isn\'t aligned to day. It\'s off by "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/32 v6, 0x5265c00

    rem-long v6, v0, v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 172482
    :cond_1
    return-wide v0

    .line 172483
    :cond_2
    if-nez v2, :cond_0

    if-eqz v3, :cond_0

    .line 172484
    const-wide/32 v2, 0x36ee80

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public static c(JJ)I
    .locals 2

    .prologue
    .line 172463
    const-wide/16 v0, 0x2

    div-long v0, p2, v0

    add-long/2addr v0, p0

    div-long/2addr v0, p2

    long-to-int v0, v0

    return v0
.end method

.method public static c(LX/11R;J)J
    .locals 3

    .prologue
    .line 172462
    iget-object v0, p0, LX/11R;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1, p2}, LX/11R;->b(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public static d(LX/11R;JJ)V
    .locals 1

    .prologue
    .line 172459
    iget-object v0, p0, LX/11R;->g:Ljava/util/Calendar;

    invoke-direct {p0, v0, p3, p4}, LX/11R;->a(Ljava/util/Calendar;J)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/11R;->g:Ljava/util/Calendar;

    .line 172460
    iget-object v0, p0, LX/11R;->h:Ljava/util/Calendar;

    invoke-direct {p0, v0, p1, p2}, LX/11R;->a(Ljava/util/Calendar;J)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/11R;->h:Ljava/util/Calendar;

    .line 172461
    return-void
.end method

.method public static g(LX/11R;J)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 172189
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 172190
    iget-object v1, p0, LX/11R;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 172191
    sub-long/2addr v2, p1

    .line 172192
    invoke-virtual {p0, p1, p2}, LX/11R;->b(J)LX/1yz;

    move-result-object v1

    .line 172193
    sget-object v4, LX/3DJ;->a:[I

    invoke-virtual {v1}, LX/1yz;->ordinal()I

    move-result v1

    aget v1, v4, v1

    packed-switch v1, :pswitch_data_0

    .line 172194
    iget-object v0, p0, LX/11R;->f:LX/11T;

    invoke-virtual {v0}, LX/11T;->b()Ljava/text/DateFormat;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 172195
    :pswitch_0
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    const v1, 0x7f080097

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172196
    :pswitch_1
    invoke-static {v2, v3}, LX/1lQ;->d(J)J

    move-result-wide v2

    long-to-int v1, v2

    .line 172197
    const v2, 0x7f08008f

    const v3, 0x7f080090

    invoke-static {v0, v2, v3, v1}, LX/1z0;->a(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172198
    :pswitch_2
    invoke-static {v2, v3}, LX/1lQ;->a(J)J

    move-result-wide v2

    long-to-int v1, v2

    .line 172199
    const v2, 0x7f08008b

    const v3, 0x7f08008d

    invoke-static {v0, v2, v3, v1}, LX/1z0;->a(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172200
    :pswitch_3
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    const v1, 0x7f0800a6

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, LX/11R;->f:LX/11T;

    invoke-virtual {v3}, LX/11T;->a()Ljava/text/DateFormat;

    move-result-object v3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172201
    :pswitch_4
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    const v1, 0x7f0800a5

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, LX/11R;->f:LX/11T;

    invoke-virtual {v3}, LX/11T;->d()Ljava/text/SimpleDateFormat;

    move-result-object v3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    iget-object v3, p0, LX/11R;->f:LX/11T;

    invoke-virtual {v3}, LX/11T;->a()Ljava/text/DateFormat;

    move-result-object v3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172202
    :pswitch_5
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    const v1, 0x7f080086

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, LX/11R;->f:LX/11T;

    invoke-virtual {v3}, LX/11T;->g()Ljava/text/SimpleDateFormat;

    move-result-object v3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    iget-object v3, p0, LX/11R;->f:LX/11T;

    invoke-virtual {v3}, LX/11T;->a()Ljava/text/DateFormat;

    move-result-object v3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private h(J)Ljava/lang/String;
    .locals 11

    .prologue
    const-wide/16 v6, -0x1

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 172203
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 172204
    iget-object v1, p0, LX/11R;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 172205
    sub-long v4, v2, p1

    .line 172206
    invoke-virtual {p0, v2, v3, p1, p2}, LX/11R;->a(JJ)J

    move-result-wide v2

    .line 172207
    cmp-long v1, v2, v6

    if-nez v1, :cond_0

    .line 172208
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    const v1, 0x7f0800a6

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v3, p0, LX/11R;->f:LX/11T;

    invoke-virtual {v3}, LX/11T;->a()Ljava/text/DateFormat;

    move-result-object v3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 172209
    :goto_0
    return-object v0

    .line 172210
    :cond_0
    cmp-long v1, v2, v6

    if-gez v1, :cond_1

    const-wide/16 v6, -0x4

    cmp-long v1, v2, v6

    if-ltz v1, :cond_1

    .line 172211
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    const v1, 0x7f0800a5

    new-array v2, v10, [Ljava/lang/Object;

    iget-object v3, p0, LX/11R;->f:LX/11T;

    invoke-virtual {v3}, LX/11T;->d()Ljava/text/SimpleDateFormat;

    move-result-object v3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    iget-object v3, p0, LX/11R;->f:LX/11T;

    invoke-virtual {v3}, LX/11T;->a()Ljava/text/DateFormat;

    move-result-object v3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172212
    :cond_1
    invoke-virtual {p0, p1, p2}, LX/11R;->b(J)LX/1yz;

    move-result-object v1

    .line 172213
    sget-object v2, LX/3DJ;->a:[I

    invoke-virtual {v1}, LX/1yz;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 172214
    :pswitch_0
    iget-object v0, p0, LX/11R;->f:LX/11T;

    invoke-virtual {v0}, LX/11T;->b()Ljava/text/DateFormat;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172215
    :pswitch_1
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    const v1, 0x7f080097

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172216
    :pswitch_2
    invoke-static {v4, v5}, LX/1lQ;->d(J)J

    move-result-wide v2

    long-to-int v1, v2

    .line 172217
    const v2, 0x7f08008f

    const v3, 0x7f080090

    invoke-static {v0, v2, v3, v1}, LX/1z0;->a(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172218
    :pswitch_3
    invoke-static {v4, v5}, LX/1lQ;->a(J)J

    move-result-wide v2

    long-to-int v1, v2

    .line 172219
    const v2, 0x7f08008b

    const v3, 0x7f08008d

    invoke-static {v0, v2, v3, v1}, LX/1z0;->a(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172220
    :pswitch_4
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    const v1, 0x7f080086

    new-array v2, v10, [Ljava/lang/Object;

    iget-object v3, p0, LX/11R;->f:LX/11T;

    invoke-virtual {v3}, LX/11T;->g()Ljava/text/SimpleDateFormat;

    move-result-object v3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    iget-object v3, p0, LX/11R;->f:LX/11T;

    invoke-virtual {v3}, LX/11T;->a()Ljava/text/DateFormat;

    move-result-object v3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static i(LX/11R;J)LX/1yz;
    .locals 9

    .prologue
    .line 172221
    iget-object v0, p0, LX/11R;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 172222
    sub-long v2, p1, v6

    move-object v1, p0

    move-wide v4, p1

    .line 172223
    invoke-direct/range {v1 .. v7}, LX/11R;->a(JJJ)LX/1yz;

    move-result-object v0

    return-object v0
.end method

.method private s(J)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 172224
    invoke-static {p1, p2}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    .line 172225
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 172226
    const-wide/32 v4, 0xea60

    cmp-long v3, v0, v4

    if-gez v3, :cond_0

    .line 172227
    invoke-static {v0, v1}, LX/1lQ;->m(J)J

    move-result-wide v0

    long-to-int v0, v0

    .line 172228
    const v1, 0x7f0f0007

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v1, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 172229
    :goto_0
    return-object v0

    .line 172230
    :cond_0
    const-wide/32 v4, 0x36ee80

    cmp-long v3, v0, v4

    if-gez v3, :cond_1

    .line 172231
    invoke-static {v0, v1}, LX/1lQ;->d(J)J

    move-result-wide v0

    long-to-int v0, v0

    .line 172232
    const v1, 0x7f0f0008

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v1, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172233
    :cond_1
    const-wide/32 v4, 0x5265c00

    cmp-long v3, v0, v4

    if-gez v3, :cond_2

    .line 172234
    invoke-static {v0, v1}, LX/1lQ;->a(J)J

    move-result-wide v0

    long-to-int v0, v0

    .line 172235
    const v1, 0x7f0f0009

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v1, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172236
    :cond_2
    const-wide/32 v4, 0x240c8400

    cmp-long v3, v0, v4

    if-gez v3, :cond_3

    .line 172237
    invoke-static {v0, v1}, LX/1lQ;->f(J)J

    move-result-wide v0

    long-to-int v0, v0

    .line 172238
    const v1, 0x7f0f000a

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v1, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172239
    :cond_3
    const-wide v4, 0x9a7ec800L

    cmp-long v3, v0, v4

    if-gez v3, :cond_4

    .line 172240
    const-wide/32 v9, 0x240c8400

    div-long v9, v0, v9

    move-wide v0, v9

    .line 172241
    long-to-int v0, v0

    .line 172242
    const v1, 0x7f0f000b

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v1, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172243
    :cond_4
    const-wide v4, 0x757b12c00L

    cmp-long v3, v0, v4

    if-gez v3, :cond_5

    .line 172244
    const-wide v9, 0x9a7ec800L

    div-long v9, v0, v9

    move-wide v0, v9

    .line 172245
    long-to-int v0, v0

    .line 172246
    const v1, 0x7f0f000c

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v1, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 172247
    :cond_5
    invoke-static {v0, v1}, LX/1lQ;->k(J)J

    move-result-wide v0

    long-to-int v0, v0

    .line 172248
    const v1, 0x7f0f000d

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v1, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(J)J
    .locals 5

    .prologue
    .line 172249
    invoke-static {p0, p1, p2}, LX/11R;->c(LX/11R;J)J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public final a(JJ)J
    .locals 5

    .prologue
    .line 172250
    invoke-direct {p0, p1, p2, p3, p4}, LX/11R;->b(JJ)J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public final a(LX/1lB;J)Ljava/lang/String;
    .locals 11

    .prologue
    .line 172251
    sget-object v0, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    if-ne p1, v0, :cond_0

    .line 172252
    invoke-static {p0, p2, p3}, LX/11R;->g(LX/11R;J)Ljava/lang/String;

    move-result-object v0

    .line 172253
    :goto_0
    return-object v0

    .line 172254
    :cond_0
    sget-object v0, LX/1lB;->NOTIFICATIONS_STREAM_RELATIVE_STYLE:LX/1lB;

    if-ne p1, v0, :cond_1

    .line 172255
    invoke-direct {p0, p2, p3}, LX/11R;->h(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172256
    :cond_1
    sget-object v0, LX/1lB;->EXACT_STREAM_RELATIVE_STYLE:LX/1lB;

    if-ne p1, v0, :cond_2

    .line 172257
    invoke-virtual {p0, p2, p3}, LX/11R;->b(J)LX/1yz;

    move-result-object v2

    sget-object v3, LX/1yz;->LESS_THAN_MIN:LX/1yz;

    if-ne v2, v3, :cond_1c

    .line 172258
    iget-object v2, p0, LX/11R;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long/2addr v2, p2

    invoke-static {v2, v3}, LX/1lQ;->m(J)J

    move-result-wide v2

    long-to-int v2, v2

    .line 172259
    if-gez v2, :cond_1b

    .line 172260
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    const v3, 0x7f080097

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 172261
    :goto_1
    move-object v0, v2

    .line 172262
    goto :goto_0

    .line 172263
    :cond_2
    sget-object v0, LX/1lB;->EVENTS_RELATIVE_STYLE:LX/1lB;

    if-ne p1, v0, :cond_3

    .line 172264
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 172265
    iget-object v3, p0, LX/11R;->d:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    sub-long v4, p2, v4

    .line 172266
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gtz v3, :cond_1d

    .line 172267
    sget-object v2, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    invoke-virtual {p0, v2, p2, p3}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v2

    .line 172268
    :goto_2
    move-object v0, v2

    .line 172269
    goto :goto_0

    .line 172270
    :cond_3
    sget-object v0, LX/1lB;->FUZZY_RELATIVE_DATE_STYLE:LX/1lB;

    if-ne p1, v0, :cond_4

    .line 172271
    iget-object v2, p0, LX/11R;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 172272
    sub-long/2addr v2, p2

    .line 172273
    const-wide/32 v4, 0xea60

    cmp-long v4, v2, v4

    if-gez v4, :cond_22

    .line 172274
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    const v3, 0x7f080097

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 172275
    :goto_3
    move-object v0, v2

    .line 172276
    goto :goto_0

    .line 172277
    :cond_4
    sget-object v0, LX/1lB;->EVENTS_RELATIVE_DATE_STYLE:LX/1lB;

    if-ne p1, v0, :cond_5

    .line 172278
    const-wide/16 v6, 0x0

    .line 172279
    invoke-virtual {p0, p2, p3}, LX/11R;->a(J)J

    move-result-wide v2

    .line 172280
    cmp-long v4, v2, v6

    if-gez v4, :cond_29

    .line 172281
    iget-object v2, p0, LX/11R;->f:LX/11T;

    invoke-virtual {v2}, LX/11T;->b()Ljava/text/DateFormat;

    move-result-object v2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 172282
    :goto_4
    move-object v0, v2

    .line 172283
    goto/16 :goto_0

    .line 172284
    :cond_5
    sget-object v0, LX/1lB;->EXACT_TIME_DATE_STYLE:LX/1lB;

    if-ne p1, v0, :cond_6

    .line 172285
    const v0, 0x7f08009c

    const v1, 0x7f0800a0

    invoke-static {p0, p2, p3, v0, v1}, LX/11R;->a(LX/11R;JII)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 172286
    goto/16 :goto_0

    .line 172287
    :cond_6
    sget-object v0, LX/1lB;->EXACT_TIME_DATE_DOT_STYLE:LX/1lB;

    if-ne p1, v0, :cond_7

    .line 172288
    const v0, 0x7f08009d

    const v1, 0x7f0800a1

    invoke-static {p0, p2, p3, v0, v1}, LX/11R;->a(LX/11R;JII)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 172289
    :cond_7
    sget-object v0, LX/1lB;->EXACT_TIME_DATE_LOWERCASE_STYLE:LX/1lB;

    if-ne p1, v0, :cond_8

    .line 172290
    const v0, 0x7f08009e

    const v1, 0x7f0800a2

    invoke-static {p0, p2, p3, v0, v1}, LX/11R;->a(LX/11R;JII)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 172291
    :cond_8
    sget-object v0, LX/1lB;->HOUR_MINUTE_STYLE:LX/1lB;

    if-ne p1, v0, :cond_9

    .line 172292
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    const/16 v1, 0xa01

    invoke-static {v0, p2, p3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 172293
    :cond_9
    sget-object v0, LX/1lB;->WEEK_DAY_STYLE:LX/1lB;

    if-ne p1, v0, :cond_a

    .line 172294
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    const v1, 0x8002

    invoke-static {v0, p2, p3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 172295
    :cond_a
    sget-object v0, LX/1lB;->MONTH_DAY_YEAR_STYLE:LX/1lB;

    if-ne p1, v0, :cond_b

    .line 172296
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    const v1, 0x10014

    invoke-static {v0, p2, p3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 172297
    :cond_b
    sget-object v0, LX/1lB;->NUMERIC_MONTH_DAY_YEAR_STYLE:LX/1lB;

    if-ne p1, v0, :cond_c

    .line 172298
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    const v1, 0x20014

    invoke-static {v0, p2, p3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 172299
    :cond_c
    sget-object v0, LX/1lB;->DATE_PICKER_STYLE:LX/1lB;

    if-ne p1, v0, :cond_d

    .line 172300
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    const v1, 0x18016

    invoke-static {v0, p2, p3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 172301
    :cond_d
    sget-object v0, LX/1lB;->MONTH_DAY_YEAR_LONG_STYLE:LX/1lB;

    if-ne p1, v0, :cond_e

    .line 172302
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    const/16 v1, 0x14

    invoke-static {v0, p2, p3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 172303
    :cond_e
    sget-object v0, LX/1lB;->MONTH_DAY_LONG_STYLE:LX/1lB;

    if-ne p1, v0, :cond_f

    .line 172304
    iget-object v0, p0, LX/11R;->c:Landroid/content/Context;

    const/16 v1, 0x10

    invoke-static {v0, p2, p3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 172305
    :cond_f
    sget-object v0, LX/1lB;->SHORT_DATE_STYLE:LX/1lB;

    if-ne p1, v0, :cond_10

    .line 172306
    const-wide/16 v8, 0x0

    const v6, 0x10018

    .line 172307
    invoke-virtual {p0, p2, p3}, LX/11R;->a(J)J

    move-result-wide v2

    .line 172308
    cmp-long v4, v2, v8

    if-gez v4, :cond_2d

    .line 172309
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    invoke-static {v2, p2, p3, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    .line 172310
    :goto_5
    move-object v0, v2

    .line 172311
    goto/16 :goto_0

    .line 172312
    :cond_10
    sget-object v0, LX/1lB;->THREAD_DATE_STYLE:LX/1lB;

    if-ne p1, v0, :cond_11

    .line 172313
    iget-object v2, p0, LX/11R;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long/2addr v2, p2

    .line 172314
    const-wide/32 v4, 0x5265c00

    cmp-long v4, v2, v4

    if-gez v4, :cond_30

    .line 172315
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    const/16 v3, 0xa01

    invoke-static {v2, p2, p3, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    .line 172316
    :goto_6
    move-object v0, v2

    .line 172317
    goto/16 :goto_0

    .line 172318
    :cond_11
    sget-object v0, LX/1lB;->SHORTEST_RELATIVE_PAST_STYLE:LX/1lB;

    if-ne p1, v0, :cond_13

    .line 172319
    const/4 v8, 0x0

    const/4 v3, 0x1

    .line 172320
    invoke-virtual {p0, p2, p3}, LX/11R;->b(J)LX/1yz;

    move-result-object v2

    .line 172321
    iget-object v4, p0, LX/11R;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 172322
    iget-object v5, p0, LX/11R;->d:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    .line 172323
    sub-long/2addr v6, p2

    .line 172324
    sget-object v5, LX/3DJ;->a:[I

    invoke-virtual {v2}, LX/1yz;->ordinal()I

    move-result v2

    aget v2, v5, v2

    packed-switch v2, :pswitch_data_0

    .line 172325
    invoke-static {v6, v7}, LX/1lQ;->k(J)J

    move-result-wide v6

    long-to-int v2, v6

    .line 172326
    if-nez v2, :cond_12

    move v2, v3

    .line 172327
    :cond_12
    const v5, 0x7f0800ab

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v8

    invoke-virtual {v4, v5, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_7
    move-object v0, v2

    .line 172328
    goto/16 :goto_0

    .line 172329
    :cond_13
    sget-object v0, LX/1lB;->SHORTEST_RELATIVE_FUTURE_STYLE:LX/1lB;

    if-ne p1, v0, :cond_15

    .line 172330
    const/4 v8, 0x0

    const/4 v3, 0x1

    .line 172331
    invoke-static {p0, p2, p3}, LX/11R;->i(LX/11R;J)LX/1yz;

    move-result-object v2

    .line 172332
    iget-object v4, p0, LX/11R;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 172333
    iget-object v5, p0, LX/11R;->d:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    .line 172334
    sub-long v6, p2, v6

    .line 172335
    sget-object v5, LX/3DJ;->a:[I

    invoke-virtual {v2}, LX/1yz;->ordinal()I

    move-result v2

    aget v2, v5, v2

    packed-switch v2, :pswitch_data_1

    .line 172336
    const-wide v9, 0x757b12c00L

    invoke-static {v6, v7, v9, v10}, LX/1lQ;->a(JJ)J

    move-result-wide v9

    move-wide v6, v9

    .line 172337
    long-to-int v2, v6

    .line 172338
    if-nez v2, :cond_14

    move v2, v3

    .line 172339
    :cond_14
    const v5, 0x7f0800b0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v8

    invoke-virtual {v4, v5, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_8
    move-object v0, v2

    .line 172340
    goto/16 :goto_0

    .line 172341
    :cond_15
    sget-object v0, LX/1lB;->DAY_HOUR_FUTURE_STYLE:LX/1lB;

    if-ne p1, v0, :cond_17

    .line 172342
    const/4 v8, 0x0

    const/4 v3, 0x1

    .line 172343
    invoke-static {p0, p2, p3}, LX/11R;->i(LX/11R;J)LX/1yz;

    move-result-object v2

    .line 172344
    iget-object v4, p0, LX/11R;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 172345
    iget-object v5, p0, LX/11R;->d:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    .line 172346
    sub-long v6, p2, v6

    .line 172347
    sget-object v5, LX/3DJ;->a:[I

    invoke-virtual {v2}, LX/1yz;->ordinal()I

    move-result v2

    aget v2, v5, v2

    packed-switch v2, :pswitch_data_2

    .line 172348
    invoke-static {v6, v7}, LX/1lQ;->h(J)J

    move-result-wide v6

    long-to-int v2, v6

    .line 172349
    if-nez v2, :cond_16

    move v2, v3

    .line 172350
    :cond_16
    const v5, 0x7f0f0006

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v8

    invoke-virtual {v4, v5, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_9
    move-object v0, v2

    .line 172351
    goto/16 :goto_0

    .line 172352
    :cond_17
    sget-object v0, LX/1lB;->MONTH_DAY_YEAR_SHORT_STYLE:LX/1lB;

    if-ne p1, v0, :cond_18

    .line 172353
    const/4 v4, 0x1

    .line 172354
    iget-object v2, p0, LX/11R;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {p0, p2, p3, v2, v3}, LX/11R;->d(LX/11R;JJ)V

    .line 172355
    iget-object v2, p0, LX/11R;->g:Ljava/util/Calendar;

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v3, p0, LX/11R;->h:Ljava/util/Calendar;

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v2, v3, :cond_3a

    .line 172356
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    const v3, 0x10018

    invoke-static {v2, p2, p3, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    .line 172357
    :goto_a
    move-object v0, v2

    .line 172358
    goto/16 :goto_0

    .line 172359
    :cond_18
    sget-object v0, LX/1lB;->RFC1123_STYLE:LX/1lB;

    if-ne p1, v0, :cond_19

    .line 172360
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEE, dd MMM yyyy HH:mm:ss zzz"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 172361
    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 172362
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p2, p3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 172363
    goto/16 :goto_0

    .line 172364
    :cond_19
    sget-object v0, LX/1lB;->DURATION_LARGEST_UNIT_STYLE:LX/1lB;

    if-ne p1, v0, :cond_1a

    .line 172365
    invoke-direct {p0, p2, p3}, LX/11R;->s(J)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 172366
    :cond_1a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown style"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172367
    :cond_1b
    iget-object v3, p0, LX/11R;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08008a

    const v5, 0x7f080089

    invoke-static {v3, v4, v5, v2}, LX/1z0;->a(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 172368
    :cond_1c
    invoke-static {p0, p2, p3}, LX/11R;->g(LX/11R;J)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 172369
    :cond_1d
    const-wide/32 v6, 0xea60

    cmp-long v3, v4, v6

    if-gez v3, :cond_1e

    .line 172370
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    const v3, 0x7f080097

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 172371
    :cond_1e
    const-wide/32 v6, 0x36ee80

    cmp-long v3, v4, v6

    if-gez v3, :cond_1f

    .line 172372
    const-wide/32 v6, 0xea60

    div-long/2addr v4, v6

    long-to-int v3, v4

    .line 172373
    const v4, 0x7f080095

    const v5, 0x7f080096

    invoke-static {v2, v4, v5, v3}, LX/1z0;->a(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 172374
    :cond_1f
    const-wide/32 v6, 0x5265c00

    cmp-long v3, v4, v6

    if-gez v3, :cond_21

    .line 172375
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    .line 172376
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6, p2, p3}, Ljava/util/Date;-><init>(J)V

    .line 172377
    invoke-virtual {v6}, Ljava/util/Date;->getDate()I

    move-result v6

    invoke-virtual {v3}, Ljava/util/Date;->getDate()I

    move-result v3

    if-ne v6, v3, :cond_20

    .line 172378
    const-wide/32 v6, 0x36ee80

    div-long/2addr v4, v6

    long-to-int v3, v4

    .line 172379
    const v4, 0x7f080093

    const v5, 0x7f080094

    invoke-static {v2, v4, v5, v3}, LX/1z0;->a(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 172380
    :cond_20
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    const v3, 0x7f0800a0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/11R;->c:Landroid/content/Context;

    const/16 v7, 0xa01

    invoke-static {v6, p2, p3, v7}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 172381
    :cond_21
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    const v3, 0x7f080086

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/11R;->c:Landroid/content/Context;

    const v7, 0x10018

    invoke-static {v6, p2, p3, v7}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, LX/11R;->c:Landroid/content/Context;

    const/16 v7, 0xa01

    invoke-static {v6, p2, p3, v7}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 172382
    :cond_22
    const-wide/32 v4, 0x36ee80

    cmp-long v4, v2, v4

    if-gez v4, :cond_23

    .line 172383
    const-wide/32 v4, 0xea60

    invoke-static {v2, v3, v4, v5}, LX/11R;->c(JJ)I

    move-result v4

    .line 172384
    int-to-long v6, v4

    const-wide/16 v8, 0x3c

    cmp-long v5, v6, v8

    if-gez v5, :cond_23

    .line 172385
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08008f

    const v5, 0x7f080090

    invoke-static {v2, v3, v5, v4}, LX/1z0;->a(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 172386
    :cond_23
    const-wide/32 v4, 0x5265c00

    cmp-long v4, v2, v4

    if-gez v4, :cond_24

    .line 172387
    const-wide/32 v4, 0x36ee80

    invoke-static {v2, v3, v4, v5}, LX/11R;->c(JJ)I

    move-result v2

    .line 172388
    int-to-long v4, v2

    const-wide/16 v6, 0x18

    cmp-long v3, v4, v6

    if-gez v3, :cond_24

    .line 172389
    iget-object v3, p0, LX/11R;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08008b

    const v5, 0x7f08008d

    invoke-static {v3, v4, v5, v2}, LX/1z0;->a(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 172390
    :cond_24
    invoke-static {p0, p2, p3}, LX/11R;->c(LX/11R;J)J

    move-result-wide v2

    neg-long v2, v2

    .line 172391
    const-wide/32 v4, 0xa4cb800

    cmp-long v4, v2, v4

    if-gez v4, :cond_25

    .line 172392
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    const v3, 0x7f0800a7

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 172393
    :cond_25
    const-wide/32 v4, 0x240c8400

    cmp-long v4, v2, v4

    if-gez v4, :cond_26

    .line 172394
    iget-object v2, p0, LX/11R;->f:LX/11T;

    invoke-virtual {v2}, LX/11T;->c()Ljava/text/SimpleDateFormat;

    move-result-object v2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 172395
    :cond_26
    const-wide v4, 0x9a7ec800L

    cmp-long v4, v2, v4

    if-gez v4, :cond_27

    .line 172396
    const-wide/32 v4, 0x240c8400

    invoke-static {v2, v3, v4, v5}, LX/11R;->c(JJ)I

    move-result v2

    .line 172397
    iget-object v3, p0, LX/11R;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0002

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v2, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 172398
    :cond_27
    const-wide v4, 0x757b12c00L

    cmp-long v4, v2, v4

    if-gez v4, :cond_28

    .line 172399
    const-wide v4, 0x9a7ec800L

    invoke-static {v2, v3, v4, v5}, LX/11R;->c(JJ)I

    move-result v4

    .line 172400
    int-to-long v6, v4

    const-wide/16 v8, 0xc

    cmp-long v5, v6, v8

    if-gez v5, :cond_28

    .line 172401
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0003

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 172402
    :cond_28
    const-wide v4, 0x757b12c00L

    invoke-static {v2, v3, v4, v5}, LX/11R;->c(JJ)I

    move-result v2

    .line 172403
    iget-object v3, p0, LX/11R;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0004

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v2, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 172404
    :cond_29
    cmp-long v4, v2, v6

    if-nez v4, :cond_2a

    .line 172405
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    const v3, 0x7f08009b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 172406
    :cond_2a
    const-wide/16 v4, 0x1

    cmp-long v4, v2, v4

    if-nez v4, :cond_2b

    .line 172407
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    const v3, 0x7f08009f

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 172408
    :cond_2b
    const-wide/16 v4, 0x7

    cmp-long v2, v2, v4

    if-gez v2, :cond_2c

    .line 172409
    iget-object v2, p0, LX/11R;->f:LX/11T;

    invoke-virtual {v2}, LX/11T;->c()Ljava/text/SimpleDateFormat;

    move-result-object v2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 172410
    :cond_2c
    iget-object v2, p0, LX/11R;->f:LX/11T;

    invoke-virtual {v2}, LX/11T;->b()Ljava/text/DateFormat;

    move-result-object v2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 172411
    :cond_2d
    cmp-long v4, v2, v8

    if-nez v4, :cond_2e

    .line 172412
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    const v3, 0x7f08009b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5

    .line 172413
    :cond_2e
    const-wide/16 v4, 0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_2f

    .line 172414
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    const v3, 0x7f08009f

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5

    .line 172415
    :cond_2f
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    invoke-static {v2, p2, p3, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5

    .line 172416
    :cond_30
    const-wide/32 v4, 0x14997000

    cmp-long v4, v2, v4

    if-gez v4, :cond_31

    .line 172417
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    const v3, 0x8002

    invoke-static {v2, p2, p3, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 172418
    :cond_31
    const-wide v4, 0x39ef8b000L

    cmp-long v2, v2, v4

    if-gez v2, :cond_32

    .line 172419
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    const v3, 0x10018

    invoke-static {v2, p2, p3, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 172420
    :cond_32
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    const v3, 0x10014

    invoke-static {v2, p2, p3, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 172421
    :pswitch_0
    invoke-static {v6, v7}, LX/1lQ;->d(J)J

    move-result-wide v6

    long-to-int v2, v6

    .line 172422
    if-gtz v2, :cond_33

    move v2, v3

    .line 172423
    :cond_33
    const v5, 0x7f0800a8

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v8

    invoke-virtual {v4, v5, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_7

    .line 172424
    :pswitch_1
    invoke-static {v6, v7}, LX/1lQ;->a(J)J

    move-result-wide v6

    long-to-int v2, v6

    .line 172425
    if-nez v2, :cond_34

    move v2, v3

    .line 172426
    :cond_34
    const v5, 0x7f0800a9

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v8

    invoke-virtual {v4, v5, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_7

    .line 172427
    :pswitch_2
    invoke-static {v6, v7}, LX/1lQ;->f(J)J

    move-result-wide v6

    long-to-int v2, v6

    .line 172428
    if-nez v2, :cond_35

    move v2, v3

    .line 172429
    :cond_35
    const v5, 0x7f0800aa

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v8

    invoke-virtual {v4, v5, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_7

    .line 172430
    :pswitch_3
    const v2, 0x7f0800ac

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_8

    .line 172431
    :pswitch_4
    const-wide/32 v9, 0xea60

    invoke-static {v6, v7, v9, v10}, LX/1lQ;->a(JJ)J

    move-result-wide v9

    move-wide v6, v9

    .line 172432
    long-to-int v2, v6

    .line 172433
    if-gtz v2, :cond_36

    move v2, v3

    .line 172434
    :cond_36
    const v5, 0x7f0800ad

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v8

    invoke-virtual {v4, v5, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_8

    .line 172435
    :pswitch_5
    const-wide/32 v9, 0x36ee80

    invoke-static {v6, v7, v9, v10}, LX/1lQ;->a(JJ)J

    move-result-wide v9

    move-wide v6, v9

    .line 172436
    long-to-int v2, v6

    .line 172437
    if-nez v2, :cond_37

    move v2, v3

    .line 172438
    :cond_37
    const v5, 0x7f0800ae

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v8

    invoke-virtual {v4, v5, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_8

    .line 172439
    :pswitch_6
    const-wide/32 v9, 0x5265c00

    invoke-static {v6, v7, v9, v10}, LX/1lQ;->a(JJ)J

    move-result-wide v9

    move-wide v6, v9

    .line 172440
    long-to-int v2, v6

    .line 172441
    if-nez v2, :cond_38

    move v2, v3

    .line 172442
    :cond_38
    const v5, 0x7f0800af

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v8

    invoke-virtual {v4, v5, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_8

    .line 172443
    :pswitch_7
    invoke-static {v6, v7}, LX/1lQ;->c(J)J

    move-result-wide v6

    long-to-int v2, v6

    .line 172444
    if-nez v2, :cond_39

    move v2, v3

    .line 172445
    :cond_39
    const v5, 0x7f0f0005

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v8

    invoke-virtual {v4, v5, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_9

    :cond_3a
    iget-object v2, p0, LX/11R;->c:Landroid/content/Context;

    const v3, 0x10014

    invoke-static {v2, p2, p3, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_a

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method public final b(J)LX/1yz;
    .locals 9

    .prologue
    .line 172446
    iget-object v0, p0, LX/11R;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 172447
    sub-long v2, v6, p1

    move-object v1, p0

    move-wide v4, p1

    .line 172448
    invoke-direct/range {v1 .. v7}, LX/11R;->a(JJJ)LX/1yz;

    move-result-object v0

    return-object v0
.end method
