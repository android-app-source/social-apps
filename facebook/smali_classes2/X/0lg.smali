.class public final LX/0lg;
.super LX/0le;
.source ""


# static fields
.field public static final a:LX/0lg;

.field public static final b:[C

.field private static final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 130094
    new-instance v0, LX/0lg;

    invoke-direct {v0}, LX/0lg;-><init>()V

    sput-object v0, LX/0lg;->a:LX/0lg;

    .line 130095
    const/4 v0, 0x0

    .line 130096
    :try_start_0
    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 130097
    :goto_0
    if-nez v0, :cond_0

    const-string v0, "\n"

    :cond_0
    sput-object v0, LX/0lg;->d:Ljava/lang/String;

    .line 130098
    const/16 v0, 0x40

    new-array v0, v0, [C

    .line 130099
    sput-object v0, LX/0lg;->b:[C

    const/16 v1, 0x20

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([CC)V

    .line 130100
    return-void

    :catch_0
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 130101
    invoke-direct {p0}, LX/0le;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0nX;I)V
    .locals 4

    .prologue
    const/16 v3, 0x40

    const/4 v2, 0x0

    .line 130102
    sget-object v0, LX/0lg;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/0nX;->c(Ljava/lang/String;)V

    .line 130103
    if-lez p2, :cond_1

    .line 130104
    add-int v0, p2, p2

    .line 130105
    :goto_0
    if-le v0, v3, :cond_0

    .line 130106
    sget-object v1, LX/0lg;->b:[C

    invoke-virtual {p1, v1, v2, v3}, LX/0nX;->b([CII)V

    .line 130107
    sget-object v1, LX/0lg;->b:[C

    array-length v1, v1

    sub-int/2addr v0, v1

    goto :goto_0

    .line 130108
    :cond_0
    sget-object v1, LX/0lg;->b:[C

    invoke-virtual {p1, v1, v2, v0}, LX/0nX;->b([CII)V

    .line 130109
    :cond_1
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 130110
    const/4 v0, 0x0

    return v0
.end method
