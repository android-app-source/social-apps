.class public final LX/0wR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/0h3;


# direct methods
.method public constructor <init>(LX/0h3;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 160004
    iput-object p1, p0, LX/0wR;->b:LX/0h3;

    iput-object p2, p0, LX/0wR;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x1

    const v0, 0x31e3af76

    invoke-static {v3, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 160005
    iget-object v1, p0, LX/0wR;->b:LX/0h3;

    iget-object v1, v1, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->j:LX/0wI;

    sget-object v2, LX/0wI;->TEXT:LX/0wI;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/0wR;->b:LX/0h3;

    iget-boolean v1, v1, LX/0h3;->o:Z

    if-nez v1, :cond_1

    .line 160006
    :cond_0
    const v1, 0x76cd0bdc

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 160007
    :goto_0
    return-void

    .line 160008
    :cond_1
    iget-object v1, p0, LX/0wR;->b:LX/0h3;

    iget-object v1, v1, LX/0h3;->n:LX/0hs;

    if-nez v1, :cond_2

    .line 160009
    iget-object v1, p0, LX/0wR;->b:LX/0h3;

    new-instance v2, LX/0hs;

    iget-object v3, p0, LX/0wR;->a:Landroid/content/Context;

    invoke-direct {v2, v3, v4}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 160010
    iput-object v2, v1, LX/0h3;->n:LX/0hs;

    .line 160011
    iget-object v1, p0, LX/0wR;->b:LX/0h3;

    iget-object v1, v1, LX/0h3;->n:LX/0hs;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0ht;->b(F)V

    .line 160012
    iget-object v1, p0, LX/0wR;->b:LX/0h3;

    iget-object v1, v1, LX/0h3;->n:LX/0hs;

    sget-object v2, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v1, v2}, LX/0ht;->a(LX/3AV;)V

    .line 160013
    iget-object v1, p0, LX/0wR;->b:LX/0h3;

    iget-object v1, v1, LX/0h3;->n:LX/0hs;

    iget-object v2, p0, LX/0wR;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b104c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 160014
    iput v2, v1, LX/0hs;->r:I

    .line 160015
    iget-object v1, p0, LX/0wR;->b:LX/0h3;

    iget-object v1, v1, LX/0h3;->n:LX/0hs;

    invoke-virtual {v1, v4}, LX/0ht;->d(Z)V

    .line 160016
    iget-object v1, p0, LX/0wR;->b:LX/0h3;

    iget-object v1, v1, LX/0h3;->n:LX/0hs;

    iget-object v2, p0, LX/0wR;->b:LX/0h3;

    iget-object v2, v2, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->e:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 160017
    :cond_2
    iget-object v1, p0, LX/0wR;->b:LX/0h3;

    iget-object v1, v1, LX/0h3;->n:LX/0hs;

    iget-object v2, p0, LX/0wR;->b:LX/0h3;

    invoke-virtual {v1, v2}, LX/0ht;->f(Landroid/view/View;)V

    .line 160018
    const v1, 0x647757c6

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
