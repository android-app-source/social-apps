.class public LX/1B0;
.super Ljava/lang/Number;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final b:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicLongFieldUpdater",
            "<",
            "LX/1B0;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private volatile transient a:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 211565
    const-class v0, LX/1B0;

    const-string v1, "a"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    move-result-object v0

    sput-object v0, LX/1B0;->b:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 211563
    invoke-direct {p0}, Ljava/lang/Number;-><init>()V

    .line 211564
    return-void
.end method

.method private a()D
    .locals 2

    .prologue
    .line 211562
    iget-wide v0, p0, LX/1B0;->a:J

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 211559
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 211560
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readDouble()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LX/1B0;->a(D)V

    .line 211561
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2

    .prologue
    .line 211566
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 211567
    invoke-direct {p0}, LX/1B0;->a()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeDouble(D)V

    .line 211568
    return-void
.end method


# virtual methods
.method public final a(D)V
    .locals 3

    .prologue
    .line 211556
    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    .line 211557
    iput-wide v0, p0, LX/1B0;->a:J

    .line 211558
    return-void
.end method

.method public final doubleValue()D
    .locals 2

    .prologue
    .line 211555
    invoke-direct {p0}, LX/1B0;->a()D

    move-result-wide v0

    return-wide v0
.end method

.method public final floatValue()F
    .locals 2

    .prologue
    .line 211554
    invoke-direct {p0}, LX/1B0;->a()D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public final intValue()I
    .locals 2

    .prologue
    .line 211553
    invoke-direct {p0}, LX/1B0;->a()D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public final longValue()J
    .locals 2

    .prologue
    .line 211552
    invoke-direct {p0}, LX/1B0;->a()D

    move-result-wide v0

    double-to-long v0, v0

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 211551
    invoke-direct {p0}, LX/1B0;->a()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
