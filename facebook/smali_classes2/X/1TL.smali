.class public LX/1TL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;
.implements LX/1TG;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 250571
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 250572
    iput-object p1, p0, LX/1TL;->a:LX/0Ot;

    .line 250573
    return-void
.end method

.method public static a(LX/0QB;)LX/1TL;
    .locals 4

    .prologue
    .line 250560
    const-class v1, LX/1TL;

    monitor-enter v1

    .line 250561
    :try_start_0
    sget-object v0, LX/1TL;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 250562
    sput-object v2, LX/1TL;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 250563
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250564
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 250565
    new-instance v3, LX/1TL;

    const/16 p0, 0x8d6

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1TL;-><init>(LX/0Ot;)V

    .line 250566
    move-object v0, v3

    .line 250567
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 250568
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1TL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 250569
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 250570
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 0

    .prologue
    .line 250555
    return-void
.end method

.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 250558
    sget-object v0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250559
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 250556
    const-class v0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    iget-object v1, p0, LX/1TL;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 250557
    return-void
.end method
