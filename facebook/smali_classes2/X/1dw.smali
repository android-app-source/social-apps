.class public LX/1dw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1dw;


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field public final b:LX/1dx;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Kz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/1dx;LX/0Ot;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/1dx;",
            "LX/0Ot",
            "<",
            "LX/2Kz;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 287084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 287085
    iput-object p2, p0, LX/1dw;->b:LX/1dx;

    .line 287086
    iput-object p1, p0, LX/1dw;->a:Ljava/util/concurrent/ExecutorService;

    .line 287087
    iput-object p3, p0, LX/1dw;->c:LX/0Ot;

    .line 287088
    return-void
.end method

.method public static a(LX/0QB;)LX/1dw;
    .locals 6

    .prologue
    .line 287089
    sget-object v0, LX/1dw;->d:LX/1dw;

    if-nez v0, :cond_1

    .line 287090
    const-class v1, LX/1dw;

    monitor-enter v1

    .line 287091
    :try_start_0
    sget-object v0, LX/1dw;->d:LX/1dw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 287092
    if-eqz v2, :cond_0

    .line 287093
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 287094
    new-instance v5, LX/1dw;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/1dx;->a(LX/0QB;)LX/1dx;

    move-result-object v4

    check-cast v4, LX/1dx;

    const/16 p0, 0xb06

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/1dw;-><init>(Ljava/util/concurrent/ExecutorService;LX/1dx;LX/0Ot;)V

    .line 287095
    move-object v0, v5

    .line 287096
    sput-object v0, LX/1dw;->d:LX/1dw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 287097
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 287098
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 287099
    :cond_1
    sget-object v0, LX/1dw;->d:LX/1dw;

    return-object v0

    .line 287100
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 287101
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1MF;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 287102
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 287103
    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 287104
    sget-wide v11, LX/3G3;->a:J

    move-wide v6, v11

    .line 287105
    const/4 v8, 0x0

    .line 287106
    const/16 v0, 0x3e8

    move v9, v0

    .line 287107
    sget-object v10, LX/4Vg;->NEVER_FINISH:LX/4Vg;

    move-object v1, p0

    move-object v2, p1

    invoke-virtual/range {v1 .. v10}, LX/1dw;->a(LX/1MF;Ljava/lang/String;JJIILX/4Vg;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1MF;JILX/4Vg;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1MF;",
            "JI",
            "LX/4Vg;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 287108
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 287109
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 287110
    if-ltz p4, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 287111
    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-wide v6, p2

    move/from16 v9, p4

    move-object/from16 v10, p5

    invoke-virtual/range {v1 .. v10}, LX/1dw;->a(LX/1MF;Ljava/lang/String;JJIILX/4Vg;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 287112
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 287113
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(LX/1MF;Ljava/lang/String;JJIILX/4Vg;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 15
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1MF;",
            "Ljava/lang/String;",
            "JJII",
            "LX/4Vg;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 287114
    invoke-interface/range {p1 .. p1}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 287115
    const-wide/16 v2, 0x0

    cmp-long v1, p3, v2

    if-gtz v1, :cond_0

    const/4 v3, 0x1

    .line 287116
    :goto_0
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v12

    .line 287117
    new-instance v1, LX/4Vf;

    move-object v2, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move/from16 v10, p7

    move/from16 v11, p8

    move-object/from16 v13, p9

    invoke-direct/range {v1 .. v13}, LX/4Vf;-><init>(LX/1dw;ZLX/1MF;Ljava/lang/String;JJIILcom/google/common/util/concurrent/SettableFuture;LX/4Vg;)V

    iget-object v2, p0, LX/1dw;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 287118
    return-object v12

    .line 287119
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method
