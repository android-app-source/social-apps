.class public LX/192;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile i:LX/192;


# instance fields
.field public final b:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final c:LX/195;

.field public final d:LX/195;

.field public final e:LX/0So;

.field private final f:LX/0Yb;

.field public g:J

.field public h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 207179
    const-class v0, LX/192;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/192;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/193;LX/0So;LX/0Xl;)V
    .locals 4
    .param p4    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 207202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207203
    invoke-interface {p4}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.threadview.DIVEBAR_STATE_CHANGED"

    new-instance v2, LX/194;

    invoke-direct {v2, p0}, LX/194;-><init>(LX/192;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/192;->f:LX/0Yb;

    .line 207204
    iget-object v0, p0, LX/192;->f:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 207205
    iput-object p1, p0, LX/192;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 207206
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "inspiration_divebar_camera_to_feed"

    invoke-virtual {p2, v0, v1}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    iput-object v0, p0, LX/192;->d:LX/195;

    .line 207207
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "inspiration_divebar_feed_to_camera"

    invoke-virtual {p2, v0, v1}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    iput-object v0, p0, LX/192;->c:LX/195;

    .line 207208
    iput-object p3, p0, LX/192;->e:LX/0So;

    .line 207209
    iput v3, p0, LX/192;->h:I

    .line 207210
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/192;->g:J

    .line 207211
    return-void
.end method

.method public static a(LX/0QB;)LX/192;
    .locals 7

    .prologue
    .line 207212
    sget-object v0, LX/192;->i:LX/192;

    if-nez v0, :cond_1

    .line 207213
    const-class v1, LX/192;

    monitor-enter v1

    .line 207214
    :try_start_0
    sget-object v0, LX/192;->i:LX/192;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 207215
    if-eqz v2, :cond_0

    .line 207216
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 207217
    new-instance p0, LX/192;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-class v4, LX/193;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/193;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v6

    check-cast v6, LX/0Xl;

    invoke-direct {p0, v3, v4, v5, v6}, LX/192;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/193;LX/0So;LX/0Xl;)V

    .line 207218
    move-object v0, p0

    .line 207219
    sput-object v0, LX/192;->i:LX/192;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207220
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 207221
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 207222
    :cond_1
    sget-object v0, LX/192;->i:LX/192;

    return-object v0

    .line 207223
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 207224
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/192;IS)V
    .locals 1

    .prologue
    .line 207195
    iget-object v0, p0, LX/192;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207196
    iget-object v0, p0, LX/192;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 207197
    :cond_0
    return-void
.end method

.method public static b(LX/192;I)V
    .locals 3

    .prologue
    .line 207198
    iget-object v0, p0, LX/192;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 207199
    iget-object v0, p0, LX/192;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 207200
    iget-object v0, p0, LX/192;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "product_name"

    sget-object v2, LX/6KV;->PURPLE_RAIN:LX/6KV;

    invoke-virtual {v2}, LX/6KV;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 207201
    :cond_0
    return-void
.end method

.method public static c(LX/192;I)V
    .locals 1

    .prologue
    .line 207192
    iget-object v0, p0, LX/192;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207193
    iget-object v0, p0, LX/192;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 207194
    :cond_0
    return-void
.end method

.method public static d(LX/192;)V
    .locals 1

    .prologue
    .line 207189
    iget-object v0, p0, LX/192;->d:LX/195;

    invoke-virtual {v0}, LX/195;->b()V

    .line 207190
    iget-object v0, p0, LX/192;->c:LX/195;

    invoke-virtual {v0}, LX/195;->b()V

    .line 207191
    return-void
.end method


# virtual methods
.method public final a(LX/0gs;)V
    .locals 4

    .prologue
    const v3, 0xa00ab

    const/4 v2, 0x2

    .line 207180
    sget-object v0, LX/ArE;->a:[I

    invoke-virtual {p1}, LX/0gs;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 207181
    :goto_0
    return-void

    .line 207182
    :pswitch_0
    monitor-enter p0

    .line 207183
    :try_start_0
    iget-object v0, p0, LX/192;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xa00ab

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207184
    iget v0, p0, LX/192;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/192;->h:I

    .line 207185
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207186
    invoke-static {p0, v3, v2}, LX/192;->a(LX/192;IS)V

    goto :goto_0

    .line 207187
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 207188
    :pswitch_1
    const v0, 0xa00ac

    invoke-static {p0, v0, v2}, LX/192;->a(LX/192;IS)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
