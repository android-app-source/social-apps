.class public abstract LX/0wa;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/lang/Runnable;

.field private b:Landroid/view/Choreographer$FrameCallback;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 160218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Landroid/view/Choreographer$FrameCallback;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 160219
    iget-object v0, p0, LX/0wa;->b:Landroid/view/Choreographer$FrameCallback;

    if-nez v0, :cond_0

    .line 160220
    new-instance v0, LX/0zB;

    invoke-direct {v0, p0}, LX/0zB;-><init>(LX/0wa;)V

    iput-object v0, p0, LX/0wa;->b:Landroid/view/Choreographer$FrameCallback;

    .line 160221
    :cond_0
    iget-object v0, p0, LX/0wa;->b:Landroid/view/Choreographer$FrameCallback;

    return-object v0
.end method

.method public abstract a(J)V
.end method

.method public final b()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 160222
    iget-object v0, p0, LX/0wa;->a:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 160223
    new-instance v0, Lcom/facebook/ui/choreographer/FrameCallbackWrapper$2;

    invoke-direct {v0, p0}, Lcom/facebook/ui/choreographer/FrameCallbackWrapper$2;-><init>(LX/0wa;)V

    iput-object v0, p0, LX/0wa;->a:Ljava/lang/Runnable;

    .line 160224
    :cond_0
    iget-object v0, p0, LX/0wa;->a:Ljava/lang/Runnable;

    return-object v0
.end method
