.class public LX/0R2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:Z

.field private final c:LX/4ff;

.field public final d:LX/4fe;


# direct methods
.method public constructor <init>(ZZLX/4ff;LX/4fe;)V
    .locals 0
    .param p3    # LX/4ff;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/4fe;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 59326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59327
    iput-boolean p1, p0, LX/0R2;->a:Z

    .line 59328
    iput-boolean p2, p0, LX/0R2;->b:Z

    .line 59329
    iput-object p3, p0, LX/0R2;->c:LX/4ff;

    .line 59330
    iput-object p4, p0, LX/0R2;->d:LX/4fe;

    .line 59331
    return-void
.end method


# virtual methods
.method public final d()LX/4ff;
    .locals 2

    .prologue
    .line 59332
    iget-boolean v0, p0, LX/0R2;->a:Z

    const-string v1, "Verification mode is disabled."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 59333
    iget-object v0, p0, LX/0R2;->c:LX/4ff;

    return-object v0
.end method
