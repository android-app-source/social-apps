.class public LX/1ie;
.super LX/1iY;
.source ""


# instance fields
.field private final a:Z

.field private final b:LX/0Zb;


# direct methods
.method public constructor <init>(LX/03R;LX/0Zb;)V
    .locals 1
    .param p1    # LX/03R;
        .annotation runtime Lcom/facebook/analytics/config/IsStaticMapLoggingEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 298778
    invoke-direct {p0}, LX/1iY;-><init>()V

    .line 298779
    iput-object p2, p0, LX/1ie;->b:LX/0Zb;

    .line 298780
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/03R;->asBoolean(Z)Z

    move-result v0

    iput-boolean v0, p0, LX/1ie;->a:Z

    .line 298781
    return-void
.end method


# virtual methods
.method public final a(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 4

    .prologue
    .line 298782
    invoke-super {p0, p1, p2}, LX/1iY;->a(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V

    .line 298783
    iget-boolean v0, p0, LX/1ie;->a:Z

    if-nez v0, :cond_1

    .line 298784
    :cond_0
    :goto_0
    return-void

    .line 298785
    :cond_1
    invoke-virtual {p0}, LX/1iY;->b()Lorg/apache/http/HttpRequest;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 298786
    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 298787
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 298788
    const-string v3, "maps.google.com"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "maps.googleapis.com"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "/maps/static"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "/static_map.php"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 298789
    :goto_1
    if-eqz v0, :cond_0

    .line 298790
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "static_map_http_flow"

    invoke-direct {v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "oxygen_map"

    .line 298791
    iput-object v2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 298792
    move-object v0, v0

    .line 298793
    const-string v2, "uri"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "status_code"

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 298794
    iget-object v1, p0, LX/1ie;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 298795
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
