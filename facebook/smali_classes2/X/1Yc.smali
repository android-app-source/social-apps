.class public LX/1Yc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1YR;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pi;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "LX/1YR",
        "<TE;>;",
        "Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoElapsedMonitor$ElapsedTimeListener;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static h:LX/0Xm;


# instance fields
.field public final b:Landroid/content/res/Resources;

.field private final c:LX/0iA;

.field private final d:LX/1Yd;

.field public final e:LX/1YX;

.field public final f:LX/0ad;

.field public g:LX/1Pi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 273717
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_CARET:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/1Yc;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0iA;LX/1Yd;LX/1YX;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 273718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273719
    iput-object p1, p0, LX/1Yc;->b:Landroid/content/res/Resources;

    .line 273720
    iput-object p2, p0, LX/1Yc;->c:LX/0iA;

    .line 273721
    iput-object p3, p0, LX/1Yc;->d:LX/1Yd;

    .line 273722
    iput-object p4, p0, LX/1Yc;->e:LX/1YX;

    .line 273723
    iput-object p5, p0, LX/1Yc;->f:LX/0ad;

    .line 273724
    return-void
.end method

.method public static a(LX/0QB;)LX/1Yc;
    .locals 9

    .prologue
    .line 273725
    const-class v1, LX/1Yc;

    monitor-enter v1

    .line 273726
    :try_start_0
    sget-object v0, LX/1Yc;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 273727
    sput-object v2, LX/1Yc;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 273728
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273729
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 273730
    new-instance v3, LX/1Yc;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v5

    check-cast v5, LX/0iA;

    invoke-static {v0}, LX/1Yd;->a(LX/0QB;)LX/1Yd;

    move-result-object v6

    check-cast v6, LX/1Yd;

    const-class v7, LX/1YX;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/1YX;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/1Yc;-><init>(Landroid/content/res/Resources;LX/0iA;LX/1Yd;LX/1YX;LX/0ad;)V

    .line 273731
    move-object v0, v3

    .line 273732
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 273733
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Yc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 273734
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 273735
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1PW;)V
    .locals 1

    .prologue
    .line 273736
    check-cast p1, LX/1Pi;

    .line 273737
    iput-object p1, p0, LX/1Yc;->g:LX/1Pi;

    .line 273738
    iget-object v0, p0, LX/1Yc;->d:LX/1Yd;

    .line 273739
    iget-object p1, v0, LX/1Yd;->d:Ljava/util/Set;

    invoke-interface {p1, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 273740
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 273741
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Yc;->g:LX/1Pi;

    .line 273742
    iget-object v0, p0, LX/1Yc;->d:LX/1Yd;

    .line 273743
    iget-object v1, v0, LX/1Yd;->d:Ljava/util/Set;

    invoke-interface {v1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 273744
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 273745
    iget-object v0, p0, LX/1Yc;->c:LX/0iA;

    sget-object v1, LX/1Yc;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-virtual {v0, v1}, LX/0iA;->b(Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v0

    return v0
.end method
