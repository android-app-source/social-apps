.class public LX/18r;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/18r;


# instance fields
.field public a:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/03V;


# direct methods
.method public constructor <init>(LX/03V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 207032
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207033
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/18r;->a:Ljava/util/WeakHashMap;

    .line 207034
    iput-object p1, p0, LX/18r;->b:LX/03V;

    .line 207035
    return-void
.end method

.method public static a(LX/0QB;)LX/18r;
    .locals 4

    .prologue
    .line 207036
    sget-object v0, LX/18r;->c:LX/18r;

    if-nez v0, :cond_1

    .line 207037
    const-class v1, LX/18r;

    monitor-enter v1

    .line 207038
    :try_start_0
    sget-object v0, LX/18r;->c:LX/18r;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 207039
    if-eqz v2, :cond_0

    .line 207040
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 207041
    new-instance p0, LX/18r;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/18r;-><init>(LX/03V;)V

    .line 207042
    move-object v0, p0

    .line 207043
    sput-object v0, LX/18r;->c:LX/18r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207044
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 207045
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 207046
    :cond_1
    sget-object v0, LX/18r;->c:LX/18r;

    return-object v0

    .line 207047
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 207048
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;I)V
    .locals 3

    .prologue
    .line 207049
    instance-of v0, p1, Landroid/support/v4/app/Fragment;

    if-nez v0, :cond_0

    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_2

    .line 207050
    :cond_0
    if-lez p2, :cond_3

    .line 207051
    iget-object v0, p0, LX/18r;->a:Ljava/util/WeakHashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207052
    :goto_0
    iget-object v0, p0, LX/18r;->b:LX/03V;

    const-string v1, "fb_fragment_stack_size"

    .line 207053
    iget-object v2, p0, LX/18r;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v2}, Ljava/util/WeakHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    .line 207054
    const/4 v2, 0x0

    move p1, v2

    .line 207055
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 207056
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v2, p1

    move p1, v2

    goto :goto_1

    .line 207057
    :cond_1
    move v2, p1

    .line 207058
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 207059
    :cond_2
    return-void

    .line 207060
    :cond_3
    iget-object v0, p0, LX/18r;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
