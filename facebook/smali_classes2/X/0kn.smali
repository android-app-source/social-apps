.class public final LX/0kn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/0kb;


# direct methods
.method public constructor <init>(LX/0kb;)V
    .locals 0

    .prologue
    .line 127857
    iput-object p1, p0, LX/0kn;->a:LX/0kb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a$redex0(LX/0kn;Landroid/content/Intent;LX/0Yf;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 127858
    const-string v0, "networkInfo"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 127859
    const-string v1, "inetCondition"

    const/4 v3, -0x1

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 127860
    iget-object v1, p0, LX/0kn;->a:LX/0kb;

    iget-object v1, v1, LX/0kb;->t:LX/0kh;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0kn;->a:LX/0kb;

    iget-object v1, v1, LX/0kb;->t:LX/0kh;

    .line 127861
    iget v4, v1, LX/0kh;->c:I

    move v1, v4

    .line 127862
    if-eq v3, v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    .line 127863
    :goto_0
    invoke-interface {p2}, LX/0Yf;->isInitialStickyBroadcast()Z

    move-result v4

    .line 127864
    iget-object v5, p0, LX/0kn;->a:LX/0kb;

    invoke-static {v5, v2}, LX/0kb;->a(LX/0kb;Z)Landroid/net/NetworkInfo;

    .line 127865
    iget-object v2, p0, LX/0kn;->a:LX/0kb;

    .line 127866
    const-wide/16 v8, 0x0

    .line 127867
    if-nez v3, :cond_7

    .line 127868
    iput-wide v8, v2, LX/0kb;->w:J

    .line 127869
    :cond_1
    :goto_1
    iget-object v2, p0, LX/0kn;->a:LX/0kb;

    invoke-static {v2, v0, v3}, LX/0kb;->a$redex0(LX/0kb;Landroid/net/NetworkInfo;I)V

    .line 127870
    if-eqz v4, :cond_4

    .line 127871
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v1, v2

    .line 127872
    goto :goto_0

    .line 127873
    :cond_4
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 127874
    iget-object v0, p0, LX/0kn;->a:LX/0kb;

    invoke-static {v0}, LX/0kb;->C(LX/0kb;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 127875
    iget-object v0, p0, LX/0kn;->a:LX/0kb;

    iget-object v2, p0, LX/0kn;->a:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->d()Z

    move-result v2

    invoke-static {v0, v2}, LX/0kb;->b(LX/0kb;Z)V

    .line 127876
    :cond_5
    :goto_3
    if-eqz v1, :cond_2

    .line 127877
    iget-object v0, p0, LX/0kn;->a:LX/0kb;

    .line 127878
    invoke-static {v0}, LX/0kb;->B(LX/0kb;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 127879
    invoke-static {v0, v3}, LX/0kb;->b$redex0(LX/0kb;I)V

    .line 127880
    :goto_4
    goto :goto_2

    .line 127881
    :cond_6
    iget-object v0, p0, LX/0kn;->a:LX/0kb;

    invoke-static {v0}, LX/0kb;->A(LX/0kb;)V

    goto :goto_3

    .line 127882
    :cond_7
    iget-wide v6, v2, LX/0kb;->w:J

    cmp-long v6, v6, v8

    if-nez v6, :cond_1

    .line 127883
    iget-object v6, v2, LX/0kb;->e:LX/0So;

    invoke-interface {v6}, LX/0So;->now()J

    move-result-wide v6

    iput-wide v6, v2, LX/0kb;->w:J

    goto :goto_1

    .line 127884
    :cond_8
    iget-object v1, v0, LX/0kb;->o:LX/0Wd;

    new-instance v2, Lcom/facebook/common/network/FbNetworkManager$5;

    invoke-direct {v2, v0, v3}, Lcom/facebook/common/network/FbNetworkManager$5;-><init>(LX/0kb;I)V

    const v4, 0xb0b44f0

    invoke-static {v1, v2, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_4
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, 0x39e8c170

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 127885
    iget-object v1, p0, LX/0kn;->a:LX/0kb;

    invoke-static {v1}, LX/0kb;->C(LX/0kb;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 127886
    invoke-static {p0, p2, p3}, LX/0kn;->a$redex0(LX/0kn;Landroid/content/Intent;LX/0Yf;)V

    .line 127887
    :goto_0
    const v1, 0x1953006d

    invoke-static {v1, v0}, LX/02F;->e(II)V

    return-void

    .line 127888
    :cond_0
    iget-object v1, p0, LX/0kn;->a:LX/0kb;

    iget-object v1, v1, LX/0kb;->o:LX/0Wd;

    new-instance v2, Lcom/facebook/common/network/FbNetworkManager$NetworkChangedActionReceiver$1;

    invoke-direct {v2, p0, p2, p3}, Lcom/facebook/common/network/FbNetworkManager$NetworkChangedActionReceiver$1;-><init>(LX/0kn;Landroid/content/Intent;LX/0Yf;)V

    const v3, 0x674b228

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method
