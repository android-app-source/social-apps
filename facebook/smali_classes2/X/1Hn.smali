.class public LX/1Hn;
.super LX/1Ho;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final c:LX/1Hp;

.field private static volatile d:LX/1Hn;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 227695
    new-instance v0, LX/1Hp;

    invoke-direct {v0}, LX/1Hp;-><init>()V

    sput-object v0, LX/1Hn;->c:LX/1Hp;

    return-void
.end method

.method public constructor <init>(LX/1Hq;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 227696
    new-instance v0, LX/1Hr;

    invoke-direct {v0}, LX/1Hr;-><init>()V

    invoke-static {}, LX/1Hr;->a()Ljava/security/SecureRandom;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/1Ho;-><init>(LX/1Hq;Ljava/security/SecureRandom;)V

    .line 227697
    return-void
.end method

.method public static a(LX/0QB;)LX/1Hn;
    .locals 4

    .prologue
    .line 227698
    sget-object v0, LX/1Hn;->d:LX/1Hn;

    if-nez v0, :cond_1

    .line 227699
    const-class v1, LX/1Hn;

    monitor-enter v1

    .line 227700
    :try_start_0
    sget-object v0, LX/1Hn;->d:LX/1Hn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 227701
    if-eqz v2, :cond_0

    .line 227702
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 227703
    new-instance p0, LX/1Hn;

    invoke-static {v0}, LX/1Hq;->a(LX/0QB;)LX/1Hq;

    move-result-object v3

    check-cast v3, LX/1Hq;

    invoke-direct {p0, v3}, LX/1Hn;-><init>(LX/1Hq;)V

    .line 227704
    move-object v0, p0

    .line 227705
    sput-object v0, LX/1Hn;->d:LX/1Hn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 227706
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 227707
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 227708
    :cond_1
    sget-object v0, LX/1Hn;->d:LX/1Hn;

    return-object v0

    .line 227709
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 227710
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
