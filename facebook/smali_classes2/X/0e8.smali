.class public LX/0e8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0e8;


# instance fields
.field public final a:LX/0W9;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0W9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 91516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91517
    iput-object p1, p0, LX/0e8;->b:Landroid/content/Context;

    .line 91518
    iput-object p2, p0, LX/0e8;->a:LX/0W9;

    .line 91519
    return-void
.end method

.method private static a(LX/0Rf;LX/0Rf;)LX/0Py;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0Py",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91504
    invoke-static {}, LX/0PM;->f()Ljava/util/TreeMap;

    move-result-object v1

    .line 91505
    invoke-virtual {p1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 91506
    invoke-static {v0}, LX/0e9;->a(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    .line 91507
    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 91508
    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 91509
    :cond_1
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 91510
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "zh"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 91511
    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 91512
    :cond_2
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v3, v4}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 91513
    :cond_3
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "fb"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "qz"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "my"

    invoke-virtual {p0, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 91514
    :cond_4
    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 91515
    :cond_5
    invoke-static {v1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    return-object v0
.end method

.method public static a([Ljava/util/Locale;[Ljava/lang/String;)LX/0Rf;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/Locale;",
            "[",
            "Ljava/lang/String;",
            ")",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 91492
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 91493
    array-length v3, p0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, p0, v1

    .line 91494
    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    const-string v6, "gu"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    const-string v6, "pa"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 91495
    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 91496
    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 91497
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 91498
    :cond_1
    array-length v1, p1

    :goto_1
    if-ge v0, v1, :cond_2

    aget-object v3, p1, v0

    .line 91499
    invoke-static {v3}, LX/0e9;->a(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v3

    .line 91500
    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 91501
    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 91502
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 91503
    :cond_2
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/0e8;
    .locals 5

    .prologue
    .line 91520
    sget-object v0, LX/0e8;->c:LX/0e8;

    if-nez v0, :cond_1

    .line 91521
    const-class v1, LX/0e8;

    monitor-enter v1

    .line 91522
    :try_start_0
    sget-object v0, LX/0e8;->c:LX/0e8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 91523
    if-eqz v2, :cond_0

    .line 91524
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 91525
    new-instance p0, LX/0e8;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v4

    check-cast v4, LX/0W9;

    invoke-direct {p0, v3, v4}, LX/0e8;-><init>(Landroid/content/Context;LX/0W9;)V

    .line 91526
    move-object v0, p0

    .line 91527
    sput-object v0, LX/0e8;->c:LX/0e8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91528
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 91529
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 91530
    :cond_1
    sget-object v0, LX/0e8;->c:LX/0e8;

    return-object v0

    .line 91531
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 91532
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0Py;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Py",
            "<",
            "Ljava/util/Locale;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 91484
    invoke-static {p1}, LX/0e9;->a(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    .line 91485
    invoke-virtual {p0, v0}, LX/0Py;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91486
    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 91487
    :goto_0
    return-object v0

    .line 91488
    :cond_0
    new-instance v1, Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 91489
    invoke-virtual {p0, v1}, LX/0Py;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91490
    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 91491
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/util/Locale;)Ljava/util/Locale;
    .locals 3

    .prologue
    .line 91475
    iget-object v0, p0, LX/0e8;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 91476
    iget-object v1, p0, LX/0e8;->a:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->g()LX/0Rf;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/0e8;->a:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->g()LX/0Rf;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v1, 0x400000

    and-int/2addr v0, v1

    if-nez v0, :cond_2

    .line 91477
    :cond_1
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 91478
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_3

    .line 91479
    iget-object v0, p0, LX/0e8;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 91480
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 91481
    invoke-virtual {v1, p1}, Landroid/content/res/Configuration;->setLayoutDirection(Ljava/util/Locale;)V

    .line 91482
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 91483
    :cond_3
    return-object p1
.end method


# virtual methods
.method public final a(Ljava/util/Locale;)V
    .locals 3

    .prologue
    .line 91467
    iget-object v0, p0, LX/0e8;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 91468
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 91469
    iget-object v2, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 91470
    iput-object p1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 91471
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 91472
    :cond_0
    invoke-direct {p0, p1}, LX/0e8;->b(Ljava/util/Locale;)Ljava/util/Locale;

    .line 91473
    invoke-static {p1}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 91474
    return-void
.end method

.method public final c()LX/0Py;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Py",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91464
    invoke-static {}, Ljava/util/Locale;->getAvailableLocales()[Ljava/util/Locale;

    move-result-object v0

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/AssetManager;->getLocales()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0e8;->a([Ljava/util/Locale;[Ljava/lang/String;)LX/0Rf;

    move-result-object v0

    move-object v0, v0

    .line 91465
    iget-object v1, p0, LX/0e8;->a:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->g()LX/0Rf;

    move-result-object v1

    .line 91466
    invoke-static {v0, v1}, LX/0e8;->a(LX/0Rf;LX/0Rf;)LX/0Py;

    move-result-object v0

    return-object v0
.end method
