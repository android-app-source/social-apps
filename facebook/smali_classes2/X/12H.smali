.class public final LX/12H;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[I

.field public static final b:[I

.field public static final c:[I

.field public static final d:[I

.field public static final e:[I

.field public static final f:[I

.field public static final g:[I

.field private static final h:[C

.field private static final i:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0xa

    const/16 v7, 0x100

    const/4 v1, -0x1

    const/16 v4, 0x80

    const/4 v2, 0x0

    .line 175070
    const-string v0, "0123456789ABCDEF"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 175071
    sput-object v0, LX/12H;->h:[C

    array-length v3, v0

    .line 175072
    new-array v0, v3, [B

    sput-object v0, LX/12H;->i:[B

    move v0, v2

    .line 175073
    :goto_0
    if-ge v0, v3, :cond_0

    .line 175074
    sget-object v5, LX/12H;->i:[B

    sget-object v6, LX/12H;->h:[C

    aget-char v6, v6, v0

    int-to-byte v6, v6

    aput-byte v6, v5, v0

    .line 175075
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 175076
    :cond_0
    new-array v3, v7, [I

    move v0, v2

    .line 175077
    :goto_1
    const/16 v5, 0x20

    if-ge v0, v5, :cond_1

    .line 175078
    aput v1, v3, v0

    .line 175079
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 175080
    :cond_1
    const/16 v0, 0x22

    const/4 v5, 0x1

    aput v5, v3, v0

    .line 175081
    const/16 v0, 0x5c

    const/4 v5, 0x1

    aput v5, v3, v0

    .line 175082
    sput-object v3, LX/12H;->a:[I

    array-length v0, v3

    new-array v5, v0, [I

    .line 175083
    sget-object v0, LX/12H;->a:[I

    sget-object v3, LX/12H;->a:[I

    array-length v3, v3

    invoke-static {v0, v2, v5, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v3, v4

    .line 175084
    :goto_2
    if-ge v3, v7, :cond_5

    .line 175085
    and-int/lit16 v0, v3, 0xe0

    const/16 v6, 0xc0

    if-ne v0, v6, :cond_2

    .line 175086
    const/4 v0, 0x2

    .line 175087
    :goto_3
    aput v0, v5, v3

    .line 175088
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 175089
    :cond_2
    and-int/lit16 v0, v3, 0xf0

    const/16 v6, 0xe0

    if-ne v0, v6, :cond_3

    .line 175090
    const/4 v0, 0x3

    goto :goto_3

    .line 175091
    :cond_3
    and-int/lit16 v0, v3, 0xf8

    const/16 v6, 0xf0

    if-ne v0, v6, :cond_4

    .line 175092
    const/4 v0, 0x4

    goto :goto_3

    :cond_4
    move v0, v1

    .line 175093
    goto :goto_3

    .line 175094
    :cond_5
    sput-object v5, LX/12H;->b:[I

    .line 175095
    new-array v3, v7, [I

    .line 175096
    invoke-static {v3, v1}, Ljava/util/Arrays;->fill([II)V

    .line 175097
    const/16 v0, 0x21

    :goto_4
    if-ge v0, v7, :cond_7

    .line 175098
    int-to-char v5, v0

    invoke-static {v5}, Ljava/lang/Character;->isJavaIdentifierPart(C)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 175099
    aput v2, v3, v0

    .line 175100
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 175101
    :cond_7
    const/16 v0, 0x40

    aput v2, v3, v0

    .line 175102
    const/16 v0, 0x23

    aput v2, v3, v0

    .line 175103
    const/16 v0, 0x2a

    aput v2, v3, v0

    .line 175104
    const/16 v0, 0x2d

    aput v2, v3, v0

    .line 175105
    const/16 v0, 0x2b

    aput v2, v3, v0

    .line 175106
    sput-object v3, LX/12H;->c:[I

    .line 175107
    new-array v0, v7, [I

    .line 175108
    sget-object v3, LX/12H;->c:[I

    sget-object v5, LX/12H;->c:[I

    array-length v5, v5

    invoke-static {v3, v2, v0, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 175109
    invoke-static {v0, v4, v4, v2}, Ljava/util/Arrays;->fill([IIII)V

    .line 175110
    sput-object v0, LX/12H;->d:[I

    .line 175111
    new-array v0, v7, [I

    sput-object v0, LX/12H;->e:[I

    .line 175112
    sget-object v0, LX/12H;->b:[I

    sget-object v3, LX/12H;->e:[I

    invoke-static {v0, v4, v3, v4, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 175113
    sget-object v0, LX/12H;->e:[I

    const/16 v3, 0x20

    invoke-static {v0, v2, v3, v1}, Ljava/util/Arrays;->fill([IIII)V

    .line 175114
    sget-object v0, LX/12H;->e:[I

    const/16 v3, 0x9

    aput v2, v0, v3

    .line 175115
    sget-object v0, LX/12H;->e:[I

    aput v8, v0, v8

    .line 175116
    sget-object v0, LX/12H;->e:[I

    const/16 v3, 0xd

    const/16 v5, 0xd

    aput v5, v0, v3

    .line 175117
    sget-object v0, LX/12H;->e:[I

    const/16 v3, 0x2a

    const/16 v5, 0x2a

    aput v5, v0, v3

    .line 175118
    new-array v3, v4, [I

    move v0, v2

    .line 175119
    :goto_5
    const/16 v5, 0x20

    if-ge v0, v5, :cond_8

    .line 175120
    aput v1, v3, v0

    .line 175121
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 175122
    :cond_8
    const/16 v0, 0x22

    const/16 v5, 0x22

    aput v5, v3, v0

    .line 175123
    const/16 v0, 0x5c

    const/16 v5, 0x5c

    aput v5, v3, v0

    .line 175124
    const/16 v0, 0x8

    const/16 v5, 0x62

    aput v5, v3, v0

    .line 175125
    const/16 v0, 0x9

    const/16 v5, 0x74

    aput v5, v3, v0

    .line 175126
    const/16 v0, 0xc

    const/16 v5, 0x66

    aput v5, v3, v0

    .line 175127
    const/16 v0, 0x6e

    aput v0, v3, v8

    .line 175128
    const/16 v0, 0xd

    const/16 v5, 0x72

    aput v5, v3, v0

    .line 175129
    sput-object v3, LX/12H;->f:[I

    .line 175130
    new-array v0, v4, [I

    .line 175131
    sput-object v0, LX/12H;->g:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    move v0, v2

    .line 175132
    :goto_6
    if-ge v0, v8, :cond_9

    .line 175133
    sget-object v1, LX/12H;->g:[I

    add-int/lit8 v3, v0, 0x30

    aput v0, v1, v3

    .line 175134
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    move v0, v2

    .line 175135
    :goto_7
    const/4 v1, 0x6

    if-ge v0, v1, :cond_a

    .line 175136
    sget-object v1, LX/12H;->g:[I

    add-int/lit8 v2, v0, 0x61

    add-int/lit8 v3, v0, 0xa

    aput v3, v1, v2

    .line 175137
    sget-object v1, LX/12H;->g:[I

    add-int/lit8 v2, v0, 0x41

    add-int/lit8 v3, v0, 0xa

    aput v3, v1, v2

    .line 175138
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 175139
    :cond_a
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 175052
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)I
    .locals 1

    .prologue
    .line 175051
    const/16 v0, 0x7f

    if-le p0, v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/12H;->g:[I

    aget v0, v0, p0

    goto :goto_0
.end method

.method public static a(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/16 v7, 0x30

    .line 175053
    sget-object v1, LX/12H;->f:[I

    .line 175054
    array-length v2, v1

    .line 175055
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    :goto_0
    if-ge v0, v3, :cond_3

    .line 175056
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 175057
    if-ge v4, v2, :cond_0

    aget v5, v1, v4

    if-nez v5, :cond_1

    .line 175058
    :cond_0
    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 175059
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 175060
    :cond_1
    const/16 v5, 0x5c

    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 175061
    aget v5, v1, v4

    .line 175062
    if-gez v5, :cond_2

    .line 175063
    const/16 v5, 0x75

    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 175064
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 175065
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 175066
    sget-object v5, LX/12H;->h:[C

    shr-int/lit8 v6, v4, 0x4

    aget-char v5, v5, v6

    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 175067
    sget-object v5, LX/12H;->h:[C

    and-int/lit8 v4, v4, 0xf

    aget-char v4, v5, v4

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 175068
    :cond_2
    int-to-char v4, v5

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 175069
    :cond_3
    return-void
.end method

.method public static g()[C
    .locals 1

    .prologue
    .line 175050
    sget-object v0, LX/12H;->h:[C

    invoke-virtual {v0}, [C->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [C

    check-cast v0, [C

    return-object v0
.end method

.method public static h()[B
    .locals 1

    .prologue
    .line 175049
    sget-object v0, LX/12H;->i:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method
