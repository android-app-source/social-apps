.class public LX/1R8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1R9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1R9;"
    }
.end annotation


# instance fields
.field public final a:LX/1R2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1R2",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Wd;

.field private c:LX/1Rl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1R8",
            "<TT;>.PrepareTask;"
        }
    .end annotation
.end field

.field public d:I


# direct methods
.method public constructor <init>(LX/1R2;LX/0Wd;)V
    .locals 1
    .param p1    # LX/1R2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1R2",
            "<TT;>;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 245554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245555
    const/4 v0, 0x0

    iput v0, p0, LX/1R8;->d:I

    .line 245556
    iput-object p1, p0, LX/1R8;->a:LX/1R2;

    .line 245557
    iput-object p2, p0, LX/1R8;->b:LX/0Wd;

    .line 245558
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 245559
    invoke-virtual {p0}, LX/1R8;->b()V

    .line 245560
    new-instance v0, LX/1Rl;

    iget-object v1, p0, LX/1R8;->b:LX/0Wd;

    invoke-direct {v0, p0, p0, v1}, LX/1Rl;-><init>(LX/1R8;LX/1R8;Ljava/util/concurrent/ExecutorService;)V

    iput-object v0, p0, LX/1R8;->c:LX/1Rl;

    .line 245561
    iget-object v0, p0, LX/1R8;->c:LX/1Rl;

    invoke-virtual {v0}, LX/1Rm;->a()V

    .line 245562
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 245563
    iget-object v0, p0, LX/1R8;->c:LX/1Rl;

    if-eqz v0, :cond_0

    .line 245564
    iget-object v0, p0, LX/1R8;->c:LX/1Rl;

    invoke-virtual {v0}, LX/1Rm;->b()V

    .line 245565
    const/4 v0, 0x0

    iput v0, p0, LX/1R8;->d:I

    .line 245566
    :cond_0
    return-void
.end method
