.class public final LX/0gb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/audience/direct/protocol/SnacksBadgeSubscriptionModels$SnacksBadgeSubscriptionModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0gN;


# direct methods
.method public constructor <init>(LX/0gN;)V
    .locals 0

    .prologue
    .line 112280
    iput-object p1, p0, LX/0gb;->a:LX/0gN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 112281
    sget-object v0, LX/0gN;->a:Ljava/lang/String;

    const-string v1, "badge count not available"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 112282
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 112283
    check-cast p1, Lcom/facebook/audience/direct/protocol/SnacksBadgeSubscriptionModels$SnacksBadgeSubscriptionModel;

    .line 112284
    if-nez p1, :cond_1

    .line 112285
    :cond_0
    :goto_0
    return-void

    .line 112286
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/audience/direct/protocol/SnacksBadgeSubscriptionModels$SnacksBadgeSubscriptionModel;->a()I

    .line 112287
    iget-object v0, p0, LX/0gb;->a:LX/0gN;

    iget-object v0, v0, LX/0gN;->g:LX/AEx;

    if-eqz v0, :cond_0

    .line 112288
    iget-object v0, p0, LX/0gb;->a:LX/0gN;

    iget-object v0, v0, LX/0gN;->g:LX/AEx;

    invoke-virtual {p1}, Lcom/facebook/audience/direct/protocol/SnacksBadgeSubscriptionModels$SnacksBadgeSubscriptionModel;->a()I

    move-result v1

    invoke-virtual {v0, v1}, LX/AEx;->a(I)V

    goto :goto_0
.end method
