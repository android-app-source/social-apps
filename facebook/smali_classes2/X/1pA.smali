.class public LX/1pA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/telephony/TelephonyManager;

.field private final b:LX/0kb;


# direct methods
.method public constructor <init>(Landroid/telephony/TelephonyManager;LX/0kb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 327882
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 327883
    iput-object p1, p0, LX/1pA;->a:Landroid/telephony/TelephonyManager;

    .line 327884
    iput-object p2, p0, LX/1pA;->b:LX/0kb;

    .line 327885
    return-void
.end method

.method private static a(Ljava/lang/String;)Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 327886
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 327887
    new-instance v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 327888
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    const-string v1, "0"

    const-string v2, "0"

    invoke-direct {v0, v1, v2}, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/1pA;
    .locals 3

    .prologue
    .line 327889
    new-instance v2, LX/1pA;

    invoke-static {p0}, LX/0e7;->b(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v1

    check-cast v1, LX/0kb;

    invoke-direct {v2, v0, v1}, LX/1pA;-><init>(Landroid/telephony/TelephonyManager;LX/0kb;)V

    .line 327890
    return-object v2
.end method


# virtual methods
.method public final a()Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;
    .locals 3

    .prologue
    .line 327891
    iget-object v0, p0, LX/1pA;->a:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1pA;->a(Ljava/lang/String;)Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    move-result-object v0

    .line 327892
    iget-object v1, p0, LX/1pA;->a:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1pA;->a(Ljava/lang/String;)Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    move-result-object v1

    .line 327893
    new-instance v2, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    invoke-direct {v2, v0, v1}, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;-><init>(Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;)V

    return-object v2
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 327894
    iget-object v0, p0, LX/1pA;->b:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 327895
    const/16 v0, 0x8

    .line 327896
    if-eqz v1, :cond_0

    .line 327897
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    .line 327898
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 327899
    :pswitch_0
    const-string v1, "none"

    :goto_0
    move-object v0, v1

    .line 327900
    return-object v0

    .line 327901
    :pswitch_1
    const-string v1, "bluetooth"

    goto :goto_0

    .line 327902
    :pswitch_2
    const-string v1, "ethernet"

    goto :goto_0

    .line 327903
    :pswitch_3
    const-string v1, "mobile"

    goto :goto_0

    .line 327904
    :pswitch_4
    const-string v1, "mobile_dun"

    goto :goto_0

    .line 327905
    :pswitch_5
    const-string v1, "mobile_hipri"

    goto :goto_0

    .line 327906
    :pswitch_6
    const-string v1, "mobile_mms"

    goto :goto_0

    .line 327907
    :pswitch_7
    const-string v1, "mobile_supl"

    goto :goto_0

    .line 327908
    :pswitch_8
    const-string v1, "wifi"

    goto :goto_0

    .line 327909
    :pswitch_9
    const-string v1, "wimax"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_8
        :pswitch_6
        :pswitch_7
        :pswitch_4
        :pswitch_5
        :pswitch_9
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
