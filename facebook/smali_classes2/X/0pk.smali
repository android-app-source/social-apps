.class public LX/0pk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0pj;

.field public final b:Ljava/lang/String;

.field private final c:LX/0Zm;

.field private final d:LX/0So;

.field private final e:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/17N;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0pj;Ljava/lang/String;LX/0Zm;LX/0So;)V
    .locals 1

    .prologue
    .line 145200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145201
    iput-object p1, p0, LX/0pk;->a:LX/0pj;

    .line 145202
    iput-object p2, p0, LX/0pk;->b:Ljava/lang/String;

    .line 145203
    iput-object p3, p0, LX/0pk;->c:LX/0Zm;

    .line 145204
    iput-object p4, p0, LX/0pk;->d:LX/0So;

    .line 145205
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0pk;->f:Ljava/util/Map;

    .line 145206
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, LX/0pk;->e:Ljava/lang/ThreadLocal;

    .line 145207
    return-void
.end method

.method public static a(LX/0pk;LX/17N;J)V
    .locals 2

    .prologue
    .line 145197
    invoke-direct {p0}, LX/0pk;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145198
    iget-object v0, p0, LX/0pk;->a:LX/0pj;

    invoke-virtual {p0, p1}, LX/0pk;->a(LX/17N;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 145199
    :cond_0
    return-void
.end method

.method public static b(LX/0pk;LX/17N;)V
    .locals 8

    .prologue
    .line 145191
    iget-object v0, p0, LX/0pk;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 145192
    iget-object v0, p0, LX/0pk;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 145193
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    .line 145194
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    invoke-static {p0, p1, v0, v1}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    .line 145195
    invoke-direct {p0}, LX/0pk;->h()V

    .line 145196
    :cond_0
    return-void
.end method

.method public static b(LX/0pk;LX/17N;J)V
    .locals 2

    .prologue
    .line 145182
    invoke-direct {p0}, LX/0pk;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145183
    iget-object v0, p0, LX/0pk;->a:LX/0pj;

    invoke-virtual {p0, p1}, LX/0pk;->a(LX/17N;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, LX/0Vx;->b(Ljava/lang/String;J)V

    .line 145184
    :cond_0
    return-void
.end method

.method private h()V
    .locals 1

    .prologue
    .line 145180
    iget-object v0, p0, LX/0pk;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    .line 145181
    return-void
.end method

.method private i()Z
    .locals 2

    .prologue
    .line 145179
    iget-object v0, p0, LX/0pk;->c:LX/0Zm;

    invoke-virtual {v0}, LX/0Zm;->a()LX/17O;

    move-result-object v0

    sget-object v1, LX/17O;->CORE_AND_SAMPLED:LX/17O;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(LX/17N;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 145185
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0pk;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145186
    iget-object v0, p0, LX/0pk;->f:Ljava/util/Map;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 145187
    iget-object v2, p0, LX/0pk;->b:Ljava/lang/String;

    move-object v2, v2

    .line 145188
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, LX/17N;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145189
    :cond_0
    iget-object v0, p0, LX/0pk;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 145190
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 145177
    iget-object v0, p0, LX/0pk;->e:Ljava/lang/ThreadLocal;

    iget-object v1, p0, LX/0pk;->d:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 145178
    return-void
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 145175
    sget-object v0, LX/17N;->HITS_COUNT:LX/17N;

    invoke-static {p0, v0, p1, p2}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    .line 145176
    return-void
.end method

.method public final a(LX/37E;IJ)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x1

    .line 145161
    sget-object v0, LX/37F;->a:[I

    invoke-virtual {p1}, LX/37E;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 145162
    :goto_0
    return-void

    .line 145163
    :pswitch_0
    sget-object v0, LX/17N;->EVICTIONS_ON_CACHE_FULL_CALL:LX/17N;

    invoke-static {p0, v0, v2, v3}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    .line 145164
    sget-object v0, LX/17N;->EVICTIONS_ON_CACHE_FULL_ITEM:LX/17N;

    int-to-long v2, p2

    invoke-static {p0, v0, v2, v3}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    .line 145165
    sget-object v0, LX/17N;->EVICTIONS_ON_CACHE_FULL_SIZE:LX/17N;

    invoke-static {p0, v0, p3, p4}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    goto :goto_0

    .line 145166
    :pswitch_1
    sget-object v0, LX/17N;->EVICTIONS_ON_CONTENT_STALE_CALL:LX/17N;

    invoke-static {p0, v0, v2, v3}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    .line 145167
    sget-object v0, LX/17N;->EVICTIONS_ON_CONTENT_STALE_ITEM:LX/17N;

    int-to-long v2, p2

    invoke-static {p0, v0, v2, v3}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    .line 145168
    sget-object v0, LX/17N;->EVICTIONS_ON_CONTENT_STALE_SIZE:LX/17N;

    invoke-static {p0, v0, p3, p4}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    goto :goto_0

    .line 145169
    :pswitch_2
    sget-object v0, LX/17N;->EVICTIONS_ON_USER_FORCED_CALL:LX/17N;

    invoke-static {p0, v0, v2, v3}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    .line 145170
    sget-object v0, LX/17N;->EVICTIONS_ON_USER_FORCED_ITEM:LX/17N;

    int-to-long v2, p2

    invoke-static {p0, v0, v2, v3}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    .line 145171
    sget-object v0, LX/17N;->EVICTIONS_ON_USER_FORCED_SIZE:LX/17N;

    invoke-static {p0, v0, p3, p4}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    goto :goto_0

    .line 145172
    :pswitch_3
    sget-object v0, LX/17N;->EVICTIONS_ON_CACHE_MANAGER_TRIMMED_CALL:LX/17N;

    invoke-static {p0, v0, v2, v3}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    .line 145173
    sget-object v0, LX/17N;->EVICTIONS_ON_CACHE_MANAGER_TRIMMED_ITEM:LX/17N;

    int-to-long v2, p2

    invoke-static {p0, v0, v2, v3}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    .line 145174
    sget-object v0, LX/17N;->EVICTIONS_ON_CACHE_MANAGER_TRIMMED_SIZE:LX/17N;

    invoke-static {p0, v0, p3, p4}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 145153
    sget-object v0, LX/17N;->HIT_TIME_MS:LX/17N;

    invoke-static {p0, v0}, LX/0pk;->b(LX/0pk;LX/17N;)V

    .line 145154
    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, LX/0pk;->a(J)V

    .line 145155
    return-void
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 145159
    sget-object v0, LX/17N;->MISSES_COUNT:LX/17N;

    invoke-static {p0, v0, p1, p2}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    .line 145160
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 145156
    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, LX/0pk;->b(J)V

    .line 145157
    invoke-direct {p0}, LX/0pk;->h()V

    .line 145158
    return-void
.end method
