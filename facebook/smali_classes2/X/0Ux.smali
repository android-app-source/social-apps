.class public final LX/0Ux;
.super LX/0T0;
.source ""


# instance fields
.field public final synthetic a:LX/0Uo;


# direct methods
.method public constructor <init>(LX/0Uo;)V
    .locals 0

    .prologue
    .line 67242
    iput-object p1, p0, LX/0Ux;->a:LX/0Uo;

    invoke-direct {p0}, LX/0T0;-><init>()V

    return-void
.end method

.method private static j(Landroid/app/Activity;)Z
    .locals 1

    .prologue
    .line 67212
    instance-of v0, p0, LX/15n;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 5
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 67213
    iget-object v0, p0, LX/0Ux;->a:LX/0Uo;

    iget-wide v0, v0, LX/0Uo;->L:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 67214
    iget-object v1, p0, LX/0Ux;->a:LX/0Uo;

    iget-object v0, p0, LX/0Ux;->a:LX/0Uo;

    iget-object v0, v0, LX/0Uo;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 67215
    iput-wide v2, v1, LX/0Uo;->L:J

    .line 67216
    :cond_0
    return-void
.end method

.method public final c(Landroid/app/Activity;)V
    .locals 5

    .prologue
    .line 67217
    invoke-static {p1}, LX/0Ux;->j(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67218
    :goto_0
    return-void

    .line 67219
    :cond_0
    iget-object v0, p0, LX/0Ux;->a:LX/0Uo;

    invoke-static {v0}, LX/0Uo;->O(LX/0Uo;)V

    .line 67220
    iget-object v0, p0, LX/0Ux;->a:LX/0Uo;

    const/4 v1, 0x1

    .line 67221
    iput-boolean v1, v0, LX/0Uo;->S:Z

    .line 67222
    iget-object v1, p0, LX/0Ux;->a:LX/0Uo;

    iget-object v0, p0, LX/0Ux;->a:LX/0Uo;

    iget-object v0, v0, LX/0Uo;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 67223
    iput-wide v2, v1, LX/0Uo;->N:J

    .line 67224
    iget-object v0, p0, LX/0Ux;->a:LX/0Uo;

    invoke-static {v0}, LX/0Uo;->w$redex0(LX/0Uo;)V

    goto :goto_0
.end method

.method public final d(Landroid/app/Activity;)V
    .locals 5

    .prologue
    .line 67225
    invoke-static {p1}, LX/0Ux;->j(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67226
    :goto_0
    return-void

    .line 67227
    :cond_0
    iget-object v0, p0, LX/0Ux;->a:LX/0Uo;

    invoke-static {v0}, LX/0Uo;->P(LX/0Uo;)V

    .line 67228
    iget-object v1, p0, LX/0Ux;->a:LX/0Uo;

    iget-object v0, p0, LX/0Ux;->a:LX/0Uo;

    iget-object v0, v0, LX/0Uo;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 67229
    iput-wide v2, v1, LX/0Uo;->M:J

    .line 67230
    iget-object v0, p0, LX/0Ux;->a:LX/0Uo;

    const/4 v1, 0x0

    .line 67231
    iput-boolean v1, v0, LX/0Uo;->S:Z

    .line 67232
    iget-object v0, p0, LX/0Ux;->a:LX/0Uo;

    invoke-static {v0}, LX/0Uo;->w$redex0(LX/0Uo;)V

    goto :goto_0
.end method

.method public final h(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 67233
    instance-of v0, p1, LX/15n;

    if-nez v0, :cond_1

    .line 67234
    const/4 v0, 0x1

    .line 67235
    :goto_0
    move v0, v0

    .line 67236
    if-eqz v0, :cond_0

    .line 67237
    iget-object v0, p0, LX/0Ux;->a:LX/0Uo;

    .line 67238
    iget-object v1, v0, LX/0Uo;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 67239
    iget-object v1, v0, LX/0Uo;->q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v1

    iput-wide v1, v0, LX/0Uo;->Q:J

    .line 67240
    invoke-static {v0}, LX/0Uo;->K(LX/0Uo;)V

    .line 67241
    :cond_0
    return-void

    :cond_1
    check-cast p1, LX/15n;

    invoke-interface {p1}, LX/15n;->a()Z

    move-result v0

    goto :goto_0
.end method
