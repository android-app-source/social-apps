.class public final LX/1oN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fu;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/0g2;


# direct methods
.method public constructor <init>(LX/0g2;I)V
    .locals 0

    .prologue
    .line 319063
    iput-object p1, p0, LX/1oN;->b:LX/0g2;

    iput p2, p0, LX/1oN;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final ko_()Z
    .locals 8

    .prologue
    .line 319064
    iget-object v0, p0, LX/1oN;->b:LX/0g2;

    iget-object v0, v0, LX/0g2;->u:LX/0Yi;

    iget v1, p0, LX/1oN;->a:I

    const p0, 0xa003f

    const v7, 0xa003e

    const v6, 0xa0032

    const/4 v2, 0x1

    .line 319065
    iget-object v3, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v4, "NNFTailFetchTime"

    invoke-interface {v3, v7, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v3

    .line 319066
    if-eqz v3, :cond_2

    iget-object v4, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v5, "NNFTailFetchRenderTime"

    invoke-interface {v4, p0, v5}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 319067
    iget-object v3, v0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v3, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 319068
    iput v1, v0, LX/0Yi;->H:I

    .line 319069
    :cond_0
    iget-object v3, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v4, "NNFTailFetchTime"

    invoke-interface {v3, v7, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 319070
    iget-object v3, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v4, "NNFTailFetchRenderTime"

    invoke-interface {v3, p0, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 319071
    iget-object v3, v0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v4, 0x2

    invoke-interface {v3, v6, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 319072
    :cond_1
    :goto_0
    move v0, v2

    .line 319073
    return v0

    :cond_2
    if-eqz v3, :cond_1

    const/4 v2, 0x0

    goto :goto_0
.end method
