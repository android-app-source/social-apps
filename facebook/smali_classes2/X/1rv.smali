.class public LX/1rv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/0yG;

.field public final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0yG;LX/0Rf;LX/0Rf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0yG;",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 333549
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333550
    iput-object p1, p0, LX/1rv;->a:LX/0yG;

    .line 333551
    iput-object p2, p0, LX/1rv;->b:LX/0Rf;

    .line 333552
    iput-object p3, p0, LX/1rv;->c:LX/0Rf;

    .line 333553
    return-void
.end method

.method public static a(LX/0yG;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 333559
    sget-object v2, LX/3K7;->a:[I

    invoke-virtual {p0}, LX/0yG;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 333560
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 333561
    :pswitch_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-ge v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(LX/0yG;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 333556
    sget-object v2, LX/3K7;->a:[I

    invoke-virtual {p0}, LX/0yG;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 333557
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 333558
    :pswitch_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-ge v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 333562
    if-ne p1, p0, :cond_1

    .line 333563
    :cond_0
    :goto_0
    return v0

    .line 333564
    :cond_1
    instance-of v2, p1, LX/1rv;

    if-nez v2, :cond_2

    move v0, v1

    .line 333565
    goto :goto_0

    .line 333566
    :cond_2
    check-cast p1, LX/1rv;

    .line 333567
    iget-object v2, p0, LX/1rv;->a:LX/0yG;

    iget-object v3, p1, LX/1rv;->a:LX/0yG;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, LX/1rv;->b:LX/0Rf;

    iget-object v3, p1, LX/1rv;->b:LX/0Rf;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/1rv;->c:LX/0Rf;

    iget-object v3, p1, LX/1rv;->c:LX/0Rf;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 333555
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/1rv;->a:LX/0yG;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/1rv;->b:LX/0Rf;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/1rv;->c:LX/0Rf;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 333554
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "state"

    iget-object v2, p0, LX/1rv;->a:LX/0yG;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "userEnabledProviders"

    iget-object v2, p0, LX/1rv;->b:LX/0Rf;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "userDisabledProviders"

    iget-object v2, p0, LX/1rv;->c:LX/0Rf;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
