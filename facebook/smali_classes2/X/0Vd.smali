.class public abstract LX/0Vd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Ve",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private volatile mDisposed:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static of(LX/0TF;)LX/0Vd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0TF",
            "<TT;>;)",
            "LX/0Vd",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 67958
    new-instance v0, LX/44v;

    invoke-direct {v0, p0}, LX/44v;-><init>(LX/0TF;)V

    return-object v0
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .prologue
    .line 67959
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Vd;->mDisposed:Z

    .line 67960
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 67961
    iget-boolean v0, p0, LX/0Vd;->mDisposed:Z

    return v0
.end method

.method public onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 0

    .prologue
    .line 67962
    return-void
.end method

.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 67963
    iget-boolean v0, p0, LX/0Vd;->mDisposed:Z

    if-eqz v0, :cond_0

    .line 67964
    :goto_0
    return-void

    .line 67965
    :cond_0
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_1

    .line 67966
    check-cast p1, Ljava/util/concurrent/CancellationException;

    invoke-virtual {p0, p1}, LX/0Vd;->onCancel(Ljava/util/concurrent/CancellationException;)V

    goto :goto_0

    .line 67967
    :cond_1
    invoke-virtual {p0, p1}, LX/0Vd;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public abstract onNonCancellationFailure(Ljava/lang/Throwable;)V
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 67968
    iget-boolean v0, p0, LX/0Vd;->mDisposed:Z

    if-eqz v0, :cond_0

    .line 67969
    :goto_0
    return-void

    .line 67970
    :cond_0
    invoke-virtual {p0, p1}, LX/0Vd;->onSuccessfulResult(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public abstract onSuccessfulResult(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method
