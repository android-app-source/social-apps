.class public LX/0vy;
.super LX/0vz;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 158421
    invoke-direct {p0}, LX/0vz;-><init>()V

    return-void
.end method


# virtual methods
.method public final C(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 158419
    invoke-virtual {p1}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    .line 158420
    return-void
.end method

.method public final a(II)I
    .locals 1

    .prologue
    .line 158417
    invoke-static {p1, p2}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v0

    move v0, v0

    .line 158418
    return v0
.end method

.method public final a(III)I
    .locals 1

    .prologue
    .line 158415
    invoke-static {p1, p2, p3}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v0

    move v0, v0

    .line 158416
    return v0
.end method

.method public final a()J
    .locals 4

    .prologue
    .line 158380
    invoke-static {}, Landroid/animation/ValueAnimator;->getFrameDelay()J

    move-result-wide v2

    move-wide v0, v2

    .line 158381
    return-wide v0
.end method

.method public final a(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 158411
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationX(F)V

    .line 158412
    return-void
.end method

.method public final a(Landroid/view/View;ILandroid/graphics/Paint;)V
    .locals 0

    .prologue
    .line 158409
    invoke-virtual {p1, p2, p3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 158410
    return-void
.end method

.method public a(Landroid/view/View;Landroid/graphics/Paint;)V
    .locals 1

    .prologue
    .line 158406
    invoke-virtual {p0, p1}, LX/0vy;->g(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, p1, v0, p2}, LX/0vy;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 158407
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 158408
    return-void
.end method

.method public final b(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 158404
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationY(F)V

    .line 158405
    return-void
.end method

.method public final b(Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 158402
    invoke-virtual {p1, p2}, Landroid/view/View;->setActivated(Z)V

    .line 158403
    return-void
.end method

.method public final c(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 158413
    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    .line 158414
    return-void
.end method

.method public final d(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 158400
    invoke-virtual {p1, p2}, Landroid/view/View;->setScaleX(F)V

    .line 158401
    return-void
.end method

.method public final e(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 158398
    invoke-virtual {p1, p2}, Landroid/view/View;->setScaleY(F)V

    .line 158399
    return-void
.end method

.method public final f(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 158396
    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result v0

    move v0, v0

    .line 158397
    return v0
.end method

.method public final g(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158394
    invoke-virtual {p1}, Landroid/view/View;->getLayerType()I

    move-result v0

    move v0, v0

    .line 158395
    return v0
.end method

.method public final k(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158392
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidthAndState()I

    move-result v0

    move v0, v0

    .line 158393
    return v0
.end method

.method public final l(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158390
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeightAndState()I

    move-result v0

    move v0, v0

    .line 158391
    return v0
.end method

.method public final m(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 158388
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredState()I

    move-result v0

    move v0, v0

    .line 158389
    return v0
.end method

.method public final r(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 158386
    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v0

    move v0, v0

    .line 158387
    return v0
.end method

.method public final s(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 158384
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v0

    move v0, v0

    .line 158385
    return v0
.end method

.method public final t(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 158382
    invoke-virtual {p1}, Landroid/view/View;->getScaleX()F

    move-result v0

    move v0, v0

    .line 158383
    return v0
.end method
