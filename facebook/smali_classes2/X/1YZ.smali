.class public LX/1YZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ya;
.implements LX/1YR;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pi;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "LX/1Ya",
        "<",
        "LX/1ZT;",
        ">;",
        "LX/1YR",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static h:LX/0Xm;


# instance fields
.field public final b:Landroid/content/res/Resources;

.field private final c:LX/0iA;

.field private final d:LX/1CN;

.field public final e:LX/1YX;

.field public final f:LX/0ad;

.field private g:LX/1Pi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 273677
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_CARET:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/1YZ;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0iA;LX/1CN;LX/1YX;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 273670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273671
    iput-object p1, p0, LX/1YZ;->b:Landroid/content/res/Resources;

    .line 273672
    iput-object p2, p0, LX/1YZ;->c:LX/0iA;

    .line 273673
    iput-object p3, p0, LX/1YZ;->d:LX/1CN;

    .line 273674
    iput-object p4, p0, LX/1YZ;->e:LX/1YX;

    .line 273675
    iput-object p5, p0, LX/1YZ;->f:LX/0ad;

    .line 273676
    return-void
.end method

.method public static a(LX/0QB;)LX/1YZ;
    .locals 9

    .prologue
    .line 273659
    const-class v1, LX/1YZ;

    monitor-enter v1

    .line 273660
    :try_start_0
    sget-object v0, LX/1YZ;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 273661
    sput-object v2, LX/1YZ;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 273662
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273663
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 273664
    new-instance v3, LX/1YZ;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v5

    check-cast v5, LX/0iA;

    invoke-static {v0}, LX/1CN;->a(LX/0QB;)LX/1CN;

    move-result-object v6

    check-cast v6, LX/1CN;

    const-class v7, LX/1YX;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/1YX;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/1YZ;-><init>(Landroid/content/res/Resources;LX/0iA;LX/1CN;LX/1YX;LX/0ad;)V

    .line 273665
    move-object v0, v3

    .line 273666
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 273667
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1YZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 273668
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 273669
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/1ZT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 273678
    const-class v0, LX/1ZT;

    return-object v0
.end method

.method public final a(LX/0bJ;J)V
    .locals 7

    .prologue
    .line 273649
    iget-object v0, p0, LX/1YZ;->g:LX/1Pi;

    check-cast p1, LX/1ZT;

    .line 273650
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 273651
    iget-object v2, p1, LX/1ZT;->a:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 273652
    iget-object v2, p1, LX/1ZT;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 273653
    :cond_0
    iget-object v2, p1, LX/1ZT;->b:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 273654
    iget-object v2, p1, LX/1ZT;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 273655
    :cond_1
    iget-object v2, p0, LX/1YZ;->f:LX/0ad;

    sget-char v3, LX/34q;->h:C

    const v4, 0x7f080d32

    iget-object v5, p0, LX/1YZ;->b:Landroid/content/res/Resources;

    invoke-interface {v2, v3, v4, v5}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    .line 273656
    iget-object v3, p0, LX/1YZ;->e:LX/1YX;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;->POST_SAVE_WELCOME:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    invoke-virtual {v3, v2, v4, v1, v5}, LX/1YX;->a(Ljava/lang/String;Ljava/lang/Long;LX/0Rf;Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;)LX/34n;

    move-result-object v1

    move-object v1, v1

    .line 273657
    invoke-interface {v0, v1}, LX/1Pi;->a(LX/34p;)V

    .line 273658
    return-void
.end method

.method public final a(LX/1PW;)V
    .locals 1

    .prologue
    .line 273645
    check-cast p1, LX/1Pi;

    .line 273646
    iput-object p1, p0, LX/1YZ;->g:LX/1Pi;

    .line 273647
    iget-object v0, p0, LX/1YZ;->d:LX/1CN;

    invoke-virtual {v0, p0}, LX/1CN;->a(LX/1Ya;)V

    .line 273648
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 273642
    const/4 v0, 0x0

    iput-object v0, p0, LX/1YZ;->g:LX/1Pi;

    .line 273643
    iget-object v0, p0, LX/1YZ;->d:LX/1CN;

    invoke-virtual {v0, p0}, LX/1CN;->b(LX/1Ya;)V

    .line 273644
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 273641
    iget-object v0, p0, LX/1YZ;->c:LX/0iA;

    sget-object v1, LX/1YZ;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-virtual {v0, v1}, LX/0iA;->b(Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v0

    return v0
.end method
