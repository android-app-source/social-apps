.class public LX/1PI;
.super LX/0hD;
.source ""

# interfaces
.implements LX/1K5;
.implements LX/1KT;
.implements LX/0fm;
.implements LX/0fy;


# instance fields
.field public final a:LX/1PJ;

.field private b:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(LX/1PJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 243742
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 243743
    iput-object p1, p0, LX/1PI;->a:LX/1PJ;

    .line 243744
    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 2

    .prologue
    .line 243738
    if-nez p2, :cond_0

    .line 243739
    iget-object v0, p0, LX/1PI;->a:LX/1PJ;

    sget-object v1, LX/1PL;->IS_SCROLLING:LX/1PL;

    invoke-virtual {v0, v1}, LX/1PJ;->b(LX/1PL;)V

    .line 243740
    :goto_0
    return-void

    .line 243741
    :cond_0
    iget-object v0, p0, LX/1PI;->a:LX/1PJ;

    sget-object v1, LX/1PL;->IS_SCROLLING:LX/1PL;

    invoke-virtual {v0, v1}, LX/1PJ;->a(LX/1PL;)V

    goto :goto_0
.end method

.method public final a(LX/0g8;III)V
    .locals 1

    .prologue
    .line 243736
    iget-object v0, p0, LX/1PI;->a:LX/1PJ;

    invoke-virtual {v0}, LX/1PJ;->b()V

    .line 243737
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 243734
    iget-object v0, p0, LX/1PI;->a:LX/1PJ;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1PJ;->a(Z)V

    .line 243735
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 243732
    iget-object v0, p0, LX/1PI;->a:LX/1PJ;

    iget-object v1, p0, LX/1PI;->b:Landroid/view/ViewGroup;

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_LOADED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-virtual {v0, v1, v2}, LX/1PJ;->a(Landroid/view/ViewGroup;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    .line 243733
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 243727
    if-eqz p1, :cond_0

    .line 243728
    iget-object v0, p0, LX/1PI;->a:LX/1PJ;

    iget-object v1, p0, LX/1PI;->b:Landroid/view/ViewGroup;

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_LOADED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-virtual {v0, v1, v2}, LX/1PJ;->a(Landroid/view/ViewGroup;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    .line 243729
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 243730
    iget-object v0, p0, LX/1PI;->a:LX/1PJ;

    iget-object v1, p0, LX/1PI;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, LX/1PJ;->a(Landroid/view/ViewGroup;)V

    .line 243731
    return-void
.end method
