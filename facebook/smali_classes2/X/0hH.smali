.class public abstract LX/0hH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 115632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115633
    return-void
.end method


# virtual methods
.method public abstract a()LX/0hH;
.end method

.method public abstract a(I)LX/0hH;
.end method

.method public abstract a(II)LX/0hH;
    .param p1    # I
        .annotation build Landroid/support/annotation/AnimRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/AnimRes;
        .end annotation
    .end param
.end method

.method public abstract a(IIII)LX/0hH;
    .param p1    # I
        .annotation build Landroid/support/annotation/AnimRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/AnimRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/AnimRes;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroid/support/annotation/AnimRes;
        .end annotation
    .end param
.end method

.method public abstract a(ILandroid/support/v4/app/Fragment;)LX/0hH;
    .param p1    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param
.end method

.method public abstract a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;
    .param p1    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract a(Landroid/support/v4/app/Fragment;)LX/0hH;
.end method

.method public abstract a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;
.end method

.method public abstract a(Ljava/lang/String;)LX/0hH;
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract b()I
.end method

.method public abstract b(ILandroid/support/v4/app/Fragment;)LX/0hH;
    .param p1    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param
.end method

.method public abstract b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;
    .param p1    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract b(Landroid/support/v4/app/Fragment;)LX/0hH;
.end method

.method public abstract c()I
.end method

.method public abstract c(Landroid/support/v4/app/Fragment;)LX/0hH;
.end method

.method public abstract d(Landroid/support/v4/app/Fragment;)LX/0hH;
.end method

.method public abstract e(Landroid/support/v4/app/Fragment;)LX/0hH;
.end method

.method public abstract e()Z
.end method
