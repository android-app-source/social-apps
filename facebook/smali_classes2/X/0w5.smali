.class public abstract LX/0w5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 159211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(TT;)",
            "LX/0w5",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 159212
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 159213
    invoke-static {v0}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v0

    return-object v0
.end method

.method public static a([B)LX/0w5;
    .locals 2
    .param p0    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 159214
    if-nez p0, :cond_0

    .line 159215
    const/4 v0, 0x0

    .line 159216
    :goto_0
    return-object v0

    .line 159217
    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 159218
    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v1

    .line 159219
    invoke-static {v0, v1}, LX/1lI;->a(Ljava/nio/ByteBuffer;I)LX/0w5;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Ljava/lang/Class;)LX/0w5;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LX/0w5",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 159220
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/0w6;

    invoke-direct {v0, p0}, LX/0w6;-><init>(Ljava/lang/Class;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract a()Ljava/lang/Integer;
.end method

.method public abstract a(LX/15i;I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15i;",
            "I)TT;"
        }
    .end annotation
.end method

.method public abstract a(LX/15w;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            ")TT;"
        }
    .end annotation
.end method

.method public final a(Ljava/nio/ByteBuffer;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            ")TT;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 159221
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v1, p1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    invoke-virtual {p0, v0}, LX/0w5;->b(LX/15i;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(LX/0lC;Ljava/lang/String;)Ljava/util/HashMap;
.end method

.method public abstract a(LX/15i;II)Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15i;",
            "II)",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(LX/15i;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15i;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Class;)Z
.end method

.method public abstract b(LX/0lC;Ljava/lang/String;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lC;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract b(LX/15i;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15i;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public abstract e()[B
.end method
