.class public LX/1CL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 216093
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216094
    iput-object p1, p0, LX/1CL;->a:Landroid/content/Context;

    .line 216095
    iput-object p2, p0, LX/1CL;->b:LX/0Or;

    .line 216096
    return-void
.end method

.method public static b(LX/0QB;)LX/1CL;
    .locals 3

    .prologue
    .line 216097
    new-instance v1, LX/1CL;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const/16 v2, 0x122d

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/1CL;-><init>(Landroid/content/Context;LX/0Or;)V

    .line 216098
    return-object v1
.end method
