.class public abstract LX/0hQ;
.super LX/0gG;
.source ""


# instance fields
.field private final a:LX/0gc;

.field private b:LX/0hH;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/Fragment$SavedState;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/support/v4/app/Fragment;


# direct methods
.method public constructor <init>(LX/0gc;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 116233
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 116234
    iput-object v1, p0, LX/0hQ;->b:LX/0hH;

    .line 116235
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0hQ;->c:Ljava/util/ArrayList;

    .line 116236
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0hQ;->d:Ljava/util/ArrayList;

    .line 116237
    iput-object v1, p0, LX/0hQ;->e:Landroid/support/v4/app/Fragment;

    .line 116238
    iput-object p1, p0, LX/0hQ;->a:LX/0gc;

    .line 116239
    return-void
.end method


# virtual methods
.method public abstract a(I)Landroid/support/v4/app/Fragment;
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 116213
    iget-object v0, p0, LX/0hQ;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p2, :cond_0

    .line 116214
    iget-object v0, p0, LX/0hQ;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 116215
    if-eqz v0, :cond_0

    .line 116216
    :goto_0
    return-object v0

    .line 116217
    :cond_0
    iget-object v0, p0, LX/0hQ;->b:LX/0hH;

    if-nez v0, :cond_1

    .line 116218
    iget-object v0, p0, LX/0hQ;->a:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iput-object v0, p0, LX/0hQ;->b:LX/0hH;

    .line 116219
    :cond_1
    invoke-virtual {p0, p2}, LX/0hQ;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 116220
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Adding item #"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": f="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 116221
    iget-object v0, p0, LX/0hQ;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p2, :cond_2

    .line 116222
    iget-object v0, p0, LX/0hQ;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment$SavedState;

    .line 116223
    if-eqz v0, :cond_2

    .line 116224
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setInitialSavedState(Landroid/support/v4/app/Fragment$SavedState;)V

    .line 116225
    :cond_2
    :goto_1
    iget-object v0, p0, LX/0hQ;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, p2, :cond_3

    .line 116226
    iget-object v0, p0, LX/0hQ;->d:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 116227
    :cond_3
    invoke-virtual {v1, v3}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 116228
    invoke-virtual {v1, v3}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 116229
    iget-object v0, p0, LX/0hQ;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 116230
    iget-object v0, p0, LX/0hQ;->b:LX/0hH;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v2

    invoke-virtual {v0, v2, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-object v0, v1

    .line 116231
    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 116200
    move-object v0, p2

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 116201
    iget-object v1, p0, LX/0hQ;->b:LX/0hH;

    if-nez v1, :cond_0

    .line 116202
    iget-object v1, p0, LX/0hQ;->a:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    iput-object v1, p0, LX/0hQ;->b:LX/0hH;

    .line 116203
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Removing item #"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ": f="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " v="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    check-cast p2, Landroid/support/v4/app/Fragment;

    .line 116204
    iget-object v3, p2, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v3, v3

    .line 116205
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 116206
    :goto_0
    iget-object v1, p0, LX/0hQ;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gt v1, p1, :cond_1

    .line 116207
    iget-object v1, p0, LX/0hQ;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 116208
    :cond_1
    iget-object v3, p0, LX/0hQ;->c:Ljava/util/ArrayList;

    if-eqz p3, :cond_2

    iget-object v1, p0, LX/0hQ;->a:LX/0gc;

    invoke-virtual {v1, v0}, LX/0gc;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment$SavedState;

    move-result-object v1

    :goto_1
    invoke-virtual {v3, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 116209
    iget-object v1, p0, LX/0hQ;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, p1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 116210
    iget-object v1, p0, LX/0hQ;->b:LX/0hH;

    invoke-virtual {v1, v0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 116211
    return-void

    :cond_2
    move-object v1, v2

    .line 116212
    goto :goto_1
.end method

.method public a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 116178
    if-eqz p1, :cond_4

    .line 116179
    check-cast p1, Landroid/os/Bundle;

    .line 116180
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 116181
    const-string v0, "states"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    .line 116182
    iget-object v0, p0, LX/0hQ;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 116183
    iget-object v0, p0, LX/0hQ;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 116184
    if-eqz v3, :cond_0

    move v1, v2

    .line 116185
    :goto_0
    array-length v0, v3

    if-ge v1, v0, :cond_0

    .line 116186
    iget-object v4, p0, LX/0hQ;->c:Ljava/util/ArrayList;

    aget-object v0, v3, v1

    check-cast v0, Landroid/support/v4/app/Fragment$SavedState;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116187
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 116188
    :cond_0
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 116189
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 116190
    const-string v3, "f"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 116191
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 116192
    iget-object v4, p0, LX/0hQ;->a:LX/0gc;

    invoke-virtual {v4, p1, v0}, LX/0gc;->a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    .line 116193
    if-eqz v4, :cond_3

    .line 116194
    :goto_2
    iget-object v0, p0, LX/0hQ;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, v3, :cond_2

    .line 116195
    iget-object v0, p0, LX/0hQ;->d:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 116196
    :cond_2
    invoke-virtual {v4, v2}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 116197
    iget-object v0, p0, LX/0hQ;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v3, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 116198
    :cond_3
    const-string v3, "FragmentStatePagerAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Bad fragment at key "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 116199
    :cond_4
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 116232
    return-void
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 116176
    const/4 v0, 0x1

    invoke-virtual {p0, p2, p3, v0}, LX/0hQ;->a(ILjava/lang/Object;Z)V

    .line 116177
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 116173
    check-cast p2, Landroid/support/v4/app/Fragment;

    .line 116174
    iget-object v0, p2, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 116175
    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)Landroid/support/v4/app/Fragment;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 116170
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/0hQ;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 116171
    iget-object v0, p0, LX/0hQ;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 116172
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 116165
    iget-object v0, p0, LX/0hQ;->b:LX/0hH;

    if-eqz v0, :cond_0

    .line 116166
    iget-object v0, p0, LX/0hQ;->b:LX/0hH;

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 116167
    const/4 v0, 0x0

    iput-object v0, p0, LX/0hQ;->b:LX/0hH;

    .line 116168
    iget-object v0, p0, LX/0hQ;->a:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 116169
    :cond_0
    return-void
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 116140
    check-cast p3, Landroid/support/v4/app/Fragment;

    .line 116141
    iget-object v0, p0, LX/0hQ;->e:Landroid/support/v4/app/Fragment;

    if-eq p3, v0, :cond_2

    .line 116142
    iget-object v0, p0, LX/0hQ;->e:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    .line 116143
    iget-object v0, p0, LX/0hQ;->e:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 116144
    iget-object v0, p0, LX/0hQ;->e:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 116145
    :cond_0
    if-eqz p3, :cond_1

    .line 116146
    invoke-virtual {p3, v2}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 116147
    invoke-virtual {p3, v2}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 116148
    :cond_1
    iput-object p3, p0, LX/0hQ;->e:Landroid/support/v4/app/Fragment;

    .line 116149
    :cond_2
    return-void
.end method

.method public lf_()Landroid/os/Parcelable;
    .locals 5

    .prologue
    .line 116150
    const/4 v0, 0x0

    .line 116151
    iget-object v1, p0, LX/0hQ;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 116152
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 116153
    iget-object v1, p0, LX/0hQ;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/support/v4/app/Fragment$SavedState;

    .line 116154
    iget-object v2, p0, LX/0hQ;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 116155
    const-string v2, "states"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 116156
    :cond_0
    const/4 v1, 0x0

    move-object v2, v0

    :goto_0
    iget-object v0, p0, LX/0hQ;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 116157
    iget-object v0, p0, LX/0hQ;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 116158
    if-eqz v0, :cond_2

    .line 116159
    if-nez v2, :cond_1

    .line 116160
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 116161
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "f"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 116162
    iget-object v4, p0, LX/0hQ;->a:LX/0gc;

    invoke-virtual {v4, v2, v3, v0}, LX/0gc;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 116163
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 116164
    :cond_3
    return-object v2
.end method
