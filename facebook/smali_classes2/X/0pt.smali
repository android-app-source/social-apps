.class public LX/0pt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile q:LX/0pt;


# instance fields
.field public b:LX/0ps;

.field private final c:Ljava/lang/Runnable;

.field private final d:Ljava/lang/Runnable;

.field private e:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private f:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field public g:Z

.field public h:Z

.field private final i:Ljava/util/concurrent/ScheduledExecutorService;

.field public final j:LX/0pu;

.field public final k:LX/0Uo;

.field public final l:LX/0q0;

.field private final m:LX/0Xl;

.field public final n:LX/0Yb;

.field private final o:LX/0YZ;

.field private final p:LX/0YZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 146741
    const-class v0, LX/0pt;

    sput-object v0, LX/0pt;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0pu;LX/0Uo;LX/0Xl;Ljava/util/concurrent/ScheduledExecutorService;Landroid/os/Handler;)V
    .locals 3
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p5    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 146742
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146743
    iput-object p1, p0, LX/0pt;->j:LX/0pu;

    .line 146744
    iput-object p2, p0, LX/0pt;->k:LX/0Uo;

    .line 146745
    iput-object p3, p0, LX/0pt;->m:LX/0Xl;

    .line 146746
    iput-object p4, p0, LX/0pt;->i:Ljava/util/concurrent/ScheduledExecutorService;

    .line 146747
    new-instance v0, Lcom/facebook/device/resourcemonitor/ResourceMonitor$1;

    invoke-direct {v0, p0}, Lcom/facebook/device/resourcemonitor/ResourceMonitor$1;-><init>(LX/0pt;)V

    iput-object v0, p0, LX/0pt;->c:Ljava/lang/Runnable;

    .line 146748
    new-instance v0, Lcom/facebook/device/resourcemonitor/ResourceMonitor$2;

    invoke-direct {v0, p0}, Lcom/facebook/device/resourcemonitor/ResourceMonitor$2;-><init>(LX/0pt;)V

    iput-object v0, p0, LX/0pt;->d:Ljava/lang/Runnable;

    .line 146749
    new-instance v0, LX/0pz;

    invoke-direct {v0, p0}, LX/0pz;-><init>(LX/0pt;)V

    iput-object v0, p0, LX/0pt;->l:LX/0q0;

    .line 146750
    new-instance v0, LX/0q1;

    invoke-direct {v0, p0}, LX/0q1;-><init>(LX/0pt;)V

    iput-object v0, p0, LX/0pt;->o:LX/0YZ;

    .line 146751
    new-instance v0, LX/0q2;

    invoke-direct {v0, p0}, LX/0q2;-><init>(LX/0pt;)V

    iput-object v0, p0, LX/0pt;->p:LX/0YZ;

    .line 146752
    iget-object v0, p0, LX/0pt;->m:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    iget-object v2, p0, LX/0pt;->o:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    iget-object v2, p0, LX/0pt;->p:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0, p5}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/0pt;->n:LX/0Yb;

    .line 146753
    return-void
.end method

.method public static a(LX/0QB;)LX/0pt;
    .locals 9

    .prologue
    .line 146754
    sget-object v0, LX/0pt;->q:LX/0pt;

    if-nez v0, :cond_1

    .line 146755
    const-class v1, LX/0pt;

    monitor-enter v1

    .line 146756
    :try_start_0
    sget-object v0, LX/0pt;->q:LX/0pt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 146757
    if-eqz v2, :cond_0

    .line 146758
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 146759
    new-instance v3, LX/0pt;

    invoke-static {v0}, LX/0pu;->a(LX/0QB;)LX/0pu;

    move-result-object v4

    check-cast v4, LX/0pu;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v5

    check-cast v5, LX/0Uo;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v6

    check-cast v6, LX/0Xl;

    invoke-static {v0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0Zw;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v8

    check-cast v8, Landroid/os/Handler;

    invoke-direct/range {v3 .. v8}, LX/0pt;-><init>(LX/0pu;LX/0Uo;LX/0Xl;Ljava/util/concurrent/ScheduledExecutorService;Landroid/os/Handler;)V

    .line 146760
    move-object v0, v3

    .line 146761
    sput-object v0, LX/0pt;->q:LX/0pt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146762
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 146763
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 146764
    :cond_1
    sget-object v0, LX/0pt;->q:LX/0pt;

    return-object v0

    .line 146765
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 146766
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized b$redex0(LX/0pt;)V
    .locals 7

    .prologue
    .line 146767
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0pt;->g:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/0pt;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0pt;->e:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_1

    .line 146768
    iget-object v0, p0, LX/0pt;->e:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_0

    .line 146769
    iget-object v0, p0, LX/0pt;->i:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/0pt;->c:Ljava/lang/Runnable;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0xbb8

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/0pt;->e:Ljava/util/concurrent/ScheduledFuture;

    .line 146770
    :cond_0
    iget-object v0, p0, LX/0pt;->f:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_1

    .line 146771
    iget-object v0, p0, LX/0pt;->i:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/0pt;->d:Ljava/lang/Runnable;

    const-wide/16 v2, 0x0

    const-wide/32 v4, 0xea60

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/0pt;->f:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146772
    :cond_1
    monitor-exit p0

    return-void

    .line 146773
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized c$redex0(LX/0pt;)V
    .locals 2

    .prologue
    .line 146774
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0pt;->e:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0pt;->f:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_2

    .line 146775
    :cond_0
    iget-object v0, p0, LX/0pt;->e:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_1

    .line 146776
    iget-object v0, p0, LX/0pt;->e:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 146777
    const/4 v0, 0x0

    iput-object v0, p0, LX/0pt;->e:Ljava/util/concurrent/ScheduledFuture;

    .line 146778
    :cond_1
    iget-object v0, p0, LX/0pt;->f:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_2

    .line 146779
    iget-object v0, p0, LX/0pt;->f:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 146780
    const/4 v0, 0x0

    iput-object v0, p0, LX/0pt;->f:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146781
    :cond_2
    monitor-exit p0

    return-void

    .line 146782
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
