.class public LX/0hn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/0ho;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final d:Ljava/lang/String;

.field private static volatile y:LX/0hn;


# instance fields
.field public a:Lcom/facebook/common/async/CancellableRunnable;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/0gM;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Z
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ETB;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7R6;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2nv;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2xj;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hp;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0xB;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3AW;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/14v;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0xX;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3AX;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Ljava/lang/String;

.field private final r:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/0Wd;

.field private u:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/videohome/metadata/VideoHomeMetadataListener;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:I

.field public w:LX/095;

.field public x:LX/EUc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 118503
    const-class v0, LX/0hn;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0hn;->d:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/String;LX/0Or;LX/0Or;LX/0Wd;)V
    .locals 2
    .param p13    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p14    # LX/0Or;
        .annotation runtime Lcom/facebook/video/videohome/prefetching/IsVideoHomeForcePrefetchEnabled;
        .end annotation
    .end param
    .param p15    # LX/0Or;
        .annotation runtime Lcom/facebook/video/videohome/prefetching/VideoHomePrefetchInterval;
        .end annotation
    .end param
    .param p16    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/ETB;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7R6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2nv;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2xj;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0hp;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0xB;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3AW;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/14v;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0xX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3AX;",
            ">;",
            "Ljava/lang/String;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 118484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118485
    const/16 v1, 0x64

    iput v1, p0, LX/0hn;->v:I

    .line 118486
    iput-object p1, p0, LX/0hn;->e:LX/0Ot;

    .line 118487
    iput-object p2, p0, LX/0hn;->f:LX/0Ot;

    .line 118488
    iput-object p3, p0, LX/0hn;->g:LX/0Ot;

    .line 118489
    iput-object p4, p0, LX/0hn;->h:LX/0Ot;

    .line 118490
    iput-object p5, p0, LX/0hn;->i:LX/0Ot;

    .line 118491
    iput-object p6, p0, LX/0hn;->j:LX/0Ot;

    .line 118492
    iput-object p7, p0, LX/0hn;->k:LX/0Ot;

    .line 118493
    iput-object p8, p0, LX/0hn;->l:LX/0Ot;

    .line 118494
    iput-object p9, p0, LX/0hn;->m:LX/0Ot;

    .line 118495
    iput-object p13, p0, LX/0hn;->q:Ljava/lang/String;

    .line 118496
    move-object/from16 v0, p14

    iput-object v0, p0, LX/0hn;->r:LX/0Or;

    .line 118497
    move-object/from16 v0, p15

    iput-object v0, p0, LX/0hn;->s:LX/0Or;

    .line 118498
    move-object/from16 v0, p16

    iput-object v0, p0, LX/0hn;->t:LX/0Wd;

    .line 118499
    iput-object p10, p0, LX/0hn;->n:LX/0Ot;

    .line 118500
    iput-object p11, p0, LX/0hn;->o:LX/0Ot;

    .line 118501
    iput-object p12, p0, LX/0hn;->p:LX/0Ot;

    .line 118502
    return-void
.end method

.method public static a(LX/0QB;)LX/0hn;
    .locals 3

    .prologue
    .line 118363
    sget-object v0, LX/0hn;->y:LX/0hn;

    if-nez v0, :cond_1

    .line 118364
    const-class v1, LX/0hn;

    monitor-enter v1

    .line 118365
    :try_start_0
    sget-object v0, LX/0hn;->y:LX/0hn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 118366
    if-eqz v2, :cond_0

    .line 118367
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/0hn;->b(LX/0QB;)LX/0hn;

    move-result-object v0

    sput-object v0, LX/0hn;->y:LX/0hn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118368
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 118369
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 118370
    :cond_1
    sget-object v0, LX/0hn;->y:LX/0hn;

    return-object v0

    .line 118371
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 118372
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0hn;LX/095;)Lcom/facebook/common/async/CancellableRunnable;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 118481
    iget-boolean v0, p0, LX/0hn;->c:Z

    if-eqz v0, :cond_0

    .line 118482
    new-instance v0, Lcom/facebook/common/async/CancellableRunnable;

    new-instance v1, Lcom/facebook/video/videohome/metadata/VideoHomeMetadataFetcher$4;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/videohome/metadata/VideoHomeMetadataFetcher$4;-><init>(LX/0hn;LX/095;)V

    invoke-direct {v0, v1}, Lcom/facebook/common/async/CancellableRunnable;-><init>(Ljava/lang/Runnable;)V

    .line 118483
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/0hn;LX/EUc;LX/0JU;)V
    .locals 6

    .prologue
    .line 118474
    iget-object v0, p0, LX/0hn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETB;

    .line 118475
    iget-wide v4, p1, LX/EUc;->e:J

    move-wide v2, v4

    .line 118476
    invoke-virtual {v0, v2, v3}, LX/ETB;->a(J)V

    .line 118477
    iget-object v0, p0, LX/0hn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETB;

    .line 118478
    iget v1, p1, LX/EUc;->c:I

    move v1, v1

    .line 118479
    invoke-virtual {v0, p0, v1, p2}, LX/ETB;->a(LX/0ho;ILX/0JU;)V

    .line 118480
    return-void
.end method

.method private static b(LX/0QB;)LX/0hn;
    .locals 19

    .prologue
    .line 118472
    new-instance v2, LX/0hn;

    const/16 v3, 0x380a

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x383e

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1383    # 7.0E-42f

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1387

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x271

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1365

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xe34

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x1369

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x1367

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x1364

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x245

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x1381

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/0dG;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    const/16 v16, 0x159b

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v16

    const/16 v17, 0x1609

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v18

    check-cast v18, LX/0Wd;

    invoke-direct/range {v2 .. v18}, LX/0hn;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/String;LX/0Or;LX/0Or;LX/0Wd;)V

    .line 118473
    return-object v2
.end method

.method private static b(I)Z
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 118471
    if-lez p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)Z
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 118466
    iget-object v0, p0, LX/0hn;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hp;

    invoke-virtual {v0, p1}, LX/0hp;->b(I)Z

    move-result v1

    .line 118467
    iget-object v0, p0, LX/0hn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETB;

    invoke-virtual {v0}, LX/ETB;->d()Z

    move-result v2

    .line 118468
    iget-object v0, p0, LX/0hn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETB;

    invoke-virtual {v0}, LX/ETB;->e()Z

    move-result v0

    .line 118469
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 118470
    invoke-virtual {p0}, LX/0hn;->b()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {p0}, LX/0hn;->p(LX/0hn;)Z

    move-result v3

    if-nez v3, :cond_1

    if-nez v1, :cond_0

    if-eqz v2, :cond_0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0hn;->w:LX/095;

    sget-object v1, LX/095;->PERIODIC:LX/095;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/0hn;->w:LX/095;

    sget-object v1, LX/095;->MQTT:LX/095;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()LX/0TF;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0TF",
            "<",
            "Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118465
    new-instance v0, LX/2nw;

    invoke-direct {v0, p0}, LX/2nw;-><init>(LX/0hn;)V

    return-object v0
.end method

.method public static m(LX/0hn;)V
    .locals 2

    .prologue
    .line 118460
    iget-object v0, p0, LX/0hn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETB;

    const/4 v1, 0x1

    .line 118461
    iget-object p0, v0, LX/ETB;->w:LX/ETH;

    invoke-virtual {p0, v1}, LX/ETH;->a(Z)V

    .line 118462
    iget-object p0, v0, LX/ETB;->q:LX/0xX;

    invoke-virtual {p0}, LX/0xX;->r()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 118463
    invoke-static {v0}, LX/ETB;->r(LX/ETB;)V

    .line 118464
    :cond_0
    return-void
.end method

.method private n()LX/0TF;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeBadgeQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 118459
    new-instance v0, LX/EUd;

    invoke-direct {v0, p0}, LX/EUd;-><init>(LX/0hn;)V

    return-object v0
.end method

.method private o()LX/0TF;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 118458
    new-instance v0, LX/EUe;

    invoke-direct {v0, p0}, LX/EUe;-><init>(LX/0hn;)V

    return-object v0
.end method

.method public static p(LX/0hn;)Z
    .locals 1

    .prologue
    .line 118457
    iget-object v0, p0, LX/0hn;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(J)J
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 118455
    iget-object v0, p0, LX/0hn;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118456
    :goto_0
    return-wide p1

    :cond_0
    iget-object v0, p0, LX/0hn;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide p1

    goto :goto_0
.end method

.method public final a()V
    .locals 11
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 118437
    invoke-static {p0}, LX/0hn;->p(LX/0hn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118438
    :goto_0
    return-void

    .line 118439
    :cond_0
    iget-object v0, p0, LX/0hn;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7R6;

    invoke-direct {p0}, LX/0hn;->n()LX/0TF;

    move-result-object v1

    invoke-direct {p0}, LX/0hn;->o()LX/0TF;

    move-result-object v2

    sget-object v3, LX/0zS;->c:LX/0zS;

    .line 118440
    new-instance v4, LX/0v6;

    const-string v5, "VideohomeMetadata"

    invoke-direct {v4, v5}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 118441
    new-instance v7, LX/7R2;

    invoke-direct {v7}, LX/7R2;-><init>()V

    move-object v7, v7

    .line 118442
    invoke-static {v7}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v7

    invoke-virtual {v7, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v7

    const-wide/16 v9, 0xa

    invoke-virtual {v7, v9, v10}, LX/0zO;->a(J)LX/0zO;

    move-result-object v7

    .line 118443
    move-object v5, v7

    .line 118444
    invoke-virtual {v4, v5}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 118445
    iget-object v6, v0, LX/7R6;->b:Ljava/util/concurrent/Executor;

    invoke-static {v5, v1, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 118446
    new-instance v7, LX/7R4;

    invoke-direct {v7}, LX/7R4;-><init>()V

    move-object v7, v7

    .line 118447
    invoke-static {v7}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v7

    invoke-virtual {v7, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v7

    const-wide/16 v9, 0xa

    invoke-virtual {v7, v9, v10}, LX/0zO;->a(J)LX/0zO;

    move-result-object v7

    .line 118448
    move-object v5, v7

    .line 118449
    invoke-virtual {v4, v5}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 118450
    iget-object v6, v0, LX/7R6;->b:Ljava/util/concurrent/Executor;

    invoke-static {v5, v2, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 118451
    move-object v1, v4

    .line 118452
    iget-object v0, p0, LX/0hn;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7R6;

    .line 118453
    iget-object v2, v0, LX/7R6;->a:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0v6;)V

    .line 118454
    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 118434
    invoke-virtual {p0}, LX/0hn;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118435
    :goto_0
    return-void

    .line 118436
    :cond_0
    iget-object v0, p0, LX/0hn;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hp;

    invoke-virtual {v0, p1}, LX/0hp;->a(I)V

    goto :goto_0
.end method

.method public final a(LX/EUc;)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 118424
    iget-object v0, p0, LX/0hn;->u:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 118425
    iget-object v0, p0, LX/0hn;->u:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    .line 118426
    if-eqz v0, :cond_0

    .line 118427
    iget-object v1, p1, LX/EUc;->f:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    move-object v1, v1

    .line 118428
    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    invoke-virtual {v1}, LX/2xj;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118429
    iget-object v1, p1, LX/EUc;->f:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    move-object v1, v1

    .line 118430
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->LIVE_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    if-ne v1, v2, :cond_0

    .line 118431
    sget-object v1, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->X:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment$10;

    invoke-direct {v2, v0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment$10;-><init>(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    const v3, -0x3a633068

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 118432
    :cond_0
    iput-object p1, p0, LX/0hn;->x:LX/EUc;

    .line 118433
    return-void
.end method

.method public final a(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V
    .locals 1

    .prologue
    .line 118422
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/0hn;->u:Ljava/lang/ref/WeakReference;

    .line 118423
    return-void
.end method

.method public final a(II)Z
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 118416
    iget-object v0, p0, LX/0hn;->r:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 118417
    if-eqz v0, :cond_1

    .line 118418
    invoke-virtual {p0}, LX/0hn;->b()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 118419
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 118420
    goto :goto_0

    .line 118421
    :cond_1
    invoke-direct {p0, p2}, LX/0hn;->c(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, LX/0hn;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 118415
    iget-object v0, p0, LX/0hn;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2xj;

    invoke-virtual {v0}, LX/2xj;->i()Z

    move-result v0

    return v0
.end method

.method public final b(LX/EUc;)Z
    .locals 2
    .param p1    # LX/EUc;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 118412
    iget-object v0, p0, LX/0hn;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xX;

    sget-object v1, LX/1vy;->PREFETCHING:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 118413
    iget v0, p1, LX/EUc;->c:I

    move v0, v0

    .line 118414
    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 118404
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0hn;->c:Z

    .line 118405
    iget-object v0, p0, LX/0hn;->a:Lcom/facebook/common/async/CancellableRunnable;

    if-nez v0, :cond_0

    .line 118406
    sget-object v0, LX/095;->INITIAL:LX/095;

    invoke-static {p0, v0}, LX/0hn;->a(LX/0hn;LX/095;)Lcom/facebook/common/async/CancellableRunnable;

    move-result-object v0

    iput-object v0, p0, LX/0hn;->a:Lcom/facebook/common/async/CancellableRunnable;

    .line 118407
    iget-object v0, p0, LX/0hn;->a:Lcom/facebook/common/async/CancellableRunnable;

    if-eqz v0, :cond_0

    .line 118408
    iget-object v0, p0, LX/0hn;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xX;

    sget-object v1, LX/1vy;->APP_STATE_MANAGER:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118409
    iget-object v0, p0, LX/0hn;->a:Lcom/facebook/common/async/CancellableRunnable;

    invoke-virtual {v0}, Lcom/facebook/common/async/CancellableRunnable;->run()V

    .line 118410
    :cond_0
    :goto_0
    return-void

    .line 118411
    :cond_1
    iget-object v0, p0, LX/0hn;->t:LX/0Wd;

    iget-object v1, p0, LX/0hn;->a:Lcom/facebook/common/async/CancellableRunnable;

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 118402
    iget-object v0, p0, LX/0hn;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hp;

    invoke-virtual {v0}, LX/0hp;->a()V

    .line 118403
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 118397
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0hn;->c:Z

    .line 118398
    iget-object v0, p0, LX/0hn;->a:Lcom/facebook/common/async/CancellableRunnable;

    if-eqz v0, :cond_0

    .line 118399
    iget-object v0, p0, LX/0hn;->a:Lcom/facebook/common/async/CancellableRunnable;

    invoke-virtual {v0}, Lcom/facebook/common/async/CancellableRunnable;->a()V

    .line 118400
    const/4 v0, 0x0

    iput-object v0, p0, LX/0hn;->a:Lcom/facebook/common/async/CancellableRunnable;

    .line 118401
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 7

    .prologue
    .line 118381
    iget-object v0, p0, LX/0hn;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xX;

    invoke-virtual {v0}, LX/0xX;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118382
    iget-object v0, p0, LX/0hn;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nv;

    iget-object v1, p0, LX/0hn;->q:Ljava/lang/String;

    invoke-direct {p0}, LX/0hn;->l()LX/0TF;

    move-result-object v2

    .line 118383
    new-instance v3, LX/2nx;

    invoke-direct {v3}, LX/2nx;-><init>()V

    .line 118384
    const-string v4, "user_id"

    invoke-virtual {v3, v4, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 118385
    move-object v3, v3

    .line 118386
    new-instance v4, LX/2ny;

    invoke-direct {v4}, LX/2ny;-><init>()V

    move-object v4, v4

    .line 118387
    const-string v5, "input"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 118388
    move-object v4, v4

    .line 118389
    const/4 v3, 0x0

    .line 118390
    :try_start_0
    iget-object v5, v0, LX/2nv;->b:LX/0gX;

    invoke-virtual {v5, v4, v2}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;
    :try_end_0
    .catch LX/31B; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 118391
    :goto_0
    move-object v0, v3

    .line 118392
    iput-object v0, p0, LX/0hn;->b:LX/0gM;

    .line 118393
    :cond_0
    invoke-virtual {p0}, LX/0hn;->c()V

    .line 118394
    return-void

    .line 118395
    :catch_0
    move-exception v4

    .line 118396
    sget-object v5, LX/2nv;->a:Ljava/lang/String;

    const-string v6, "Video Home badge update subscription failed. %s"

    invoke-static {v5, v6, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 118373
    iget-object v0, p0, LX/0hn;->b:LX/0gM;

    if-eqz v0, :cond_0

    .line 118374
    iget-object v0, p0, LX/0hn;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nv;

    iget-object v1, p0, LX/0hn;->b:LX/0gM;

    .line 118375
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 118376
    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 118377
    iget-object v3, v0, LX/2nv;->b:LX/0gX;

    invoke-virtual {v3, v2}, LX/0gX;->a(Ljava/util/Set;)V

    .line 118378
    const/4 v0, 0x0

    iput-object v0, p0, LX/0hn;->b:LX/0gM;

    .line 118379
    :cond_0
    invoke-virtual {p0}, LX/0hn;->d()V

    .line 118380
    return-void
.end method
