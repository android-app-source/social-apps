.class public final enum LX/1bY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1bY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1bY;

.field public static final enum BITMAP_MEMORY_CACHE:LX/1bY;

.field public static final enum DISK_CACHE:LX/1bY;

.field public static final enum ENCODED_MEMORY_CACHE:LX/1bY;

.field public static final enum FULL_FETCH:LX/1bY;


# instance fields
.field private mValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 280664
    new-instance v0, LX/1bY;

    const-string v1, "FULL_FETCH"

    invoke-direct {v0, v1, v5, v2}, LX/1bY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1bY;->FULL_FETCH:LX/1bY;

    .line 280665
    new-instance v0, LX/1bY;

    const-string v1, "DISK_CACHE"

    invoke-direct {v0, v1, v2, v3}, LX/1bY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1bY;->DISK_CACHE:LX/1bY;

    .line 280666
    new-instance v0, LX/1bY;

    const-string v1, "ENCODED_MEMORY_CACHE"

    invoke-direct {v0, v1, v3, v4}, LX/1bY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1bY;->ENCODED_MEMORY_CACHE:LX/1bY;

    .line 280667
    new-instance v0, LX/1bY;

    const-string v1, "BITMAP_MEMORY_CACHE"

    invoke-direct {v0, v1, v4, v6}, LX/1bY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1bY;->BITMAP_MEMORY_CACHE:LX/1bY;

    .line 280668
    new-array v0, v6, [LX/1bY;

    sget-object v1, LX/1bY;->FULL_FETCH:LX/1bY;

    aput-object v1, v0, v5

    sget-object v1, LX/1bY;->DISK_CACHE:LX/1bY;

    aput-object v1, v0, v2

    sget-object v1, LX/1bY;->ENCODED_MEMORY_CACHE:LX/1bY;

    aput-object v1, v0, v3

    sget-object v1, LX/1bY;->BITMAP_MEMORY_CACHE:LX/1bY;

    aput-object v1, v0, v4

    sput-object v0, LX/1bY;->$VALUES:[LX/1bY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 280669
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 280670
    iput p3, p0, LX/1bY;->mValue:I

    .line 280671
    return-void
.end method

.method public static getMax(LX/1bY;LX/1bY;)LX/1bY;
    .locals 2

    .prologue
    .line 280672
    invoke-virtual {p0}, LX/1bY;->getValue()I

    move-result v0

    invoke-virtual {p1}, LX/1bY;->getValue()I

    move-result v1

    if-le v0, v1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    move-object p0, p1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/1bY;
    .locals 1

    .prologue
    .line 280673
    const-class v0, LX/1bY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1bY;

    return-object v0
.end method

.method public static values()[LX/1bY;
    .locals 1

    .prologue
    .line 280674
    sget-object v0, LX/1bY;->$VALUES:[LX/1bY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1bY;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 280675
    iget v0, p0, LX/1bY;->mValue:I

    return v0
.end method
