.class public LX/0eH;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 91647
    const-string v0, "af"

    const-string v1, "ar"

    const-string v2, "bn"

    const-string v3, "cs"

    const-string v4, "da"

    const-string v5, "de"

    const/16 v6, 0x29

    new-array v6, v6, [Ljava/lang/String;

    const-string v7, "el"

    aput-object v7, v6, v9

    const-string v7, "en_GB"

    aput-object v7, v6, v10

    const/4 v7, 0x2

    const-string v8, "es"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "es_ES"

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string v8, "fi"

    aput-object v8, v6, v7

    const/4 v7, 0x5

    const-string v8, "fil"

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const-string v8, "fr"

    aput-object v8, v6, v7

    const/4 v7, 0x7

    const-string v8, "gu"

    aput-object v8, v6, v7

    const/16 v7, 0x8

    const-string v8, "hi"

    aput-object v8, v6, v7

    const/16 v7, 0x9

    const-string v8, "hr"

    aput-object v8, v6, v7

    const/16 v7, 0xa

    const-string v8, "hu"

    aput-object v8, v6, v7

    const/16 v7, 0xb

    const-string v8, "in"

    aput-object v8, v6, v7

    const/16 v7, 0xc

    const-string v8, "it"

    aput-object v8, v6, v7

    const/16 v7, 0xd

    const-string v8, "iw"

    aput-object v8, v6, v7

    const/16 v7, 0xe

    const-string v8, "ja"

    aput-object v8, v6, v7

    const/16 v7, 0xf

    const-string v8, "kn"

    aput-object v8, v6, v7

    const/16 v7, 0x10

    const-string v8, "ko"

    aput-object v8, v6, v7

    const/16 v7, 0x11

    const-string v8, "ml"

    aput-object v8, v6, v7

    const/16 v7, 0x12

    const-string v8, "mr"

    aput-object v8, v6, v7

    const/16 v7, 0x13

    const-string v8, "ms"

    aput-object v8, v6, v7

    const/16 v7, 0x14

    const-string v8, "nb"

    aput-object v8, v6, v7

    const/16 v7, 0x15

    const-string v8, "nl"

    aput-object v8, v6, v7

    const/16 v7, 0x16

    const-string v8, "pa"

    aput-object v8, v6, v7

    const/16 v7, 0x17

    const-string v8, "pl"

    aput-object v8, v6, v7

    const/16 v7, 0x18

    const-string v8, "pt"

    aput-object v8, v6, v7

    const/16 v7, 0x19

    const-string v8, "pt_PT"

    aput-object v8, v6, v7

    const/16 v7, 0x1a

    const-string v8, "ro"

    aput-object v8, v6, v7

    const/16 v7, 0x1b

    const-string v8, "ru"

    aput-object v8, v6, v7

    const/16 v7, 0x1c

    const-string v8, "sk"

    aput-object v8, v6, v7

    const/16 v7, 0x1d

    const-string v8, "sv"

    aput-object v8, v6, v7

    const/16 v7, 0x1e

    const-string v8, "sw"

    aput-object v8, v6, v7

    const/16 v7, 0x1f

    const-string v8, "ta"

    aput-object v8, v6, v7

    const/16 v7, 0x20

    const-string v8, "te"

    aput-object v8, v6, v7

    const/16 v7, 0x21

    const-string v8, "th"

    aput-object v8, v6, v7

    const/16 v7, 0x22

    const-string v8, "tl"

    aput-object v8, v6, v7

    const/16 v7, 0x23

    const-string v8, "tr"

    aput-object v8, v6, v7

    const/16 v7, 0x24

    const-string v8, "ur"

    aput-object v8, v6, v7

    const/16 v7, 0x25

    const-string v8, "vi"

    aput-object v8, v6, v7

    const/16 v7, 0x26

    const-string v8, "zh_CN"

    aput-object v8, v6, v7

    const/16 v7, 0x27

    const-string v8, "zh_HK"

    aput-object v8, v6, v7

    const/16 v7, 0x28

    const-string v8, "zh_TW"

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/0eH;->a:LX/0Rf;

    .line 91648
    const-string v0, "bg"

    const-string v1, "fb"

    const-string v2, "lt"

    const-string v3, "my"

    const-string v4, "qz"

    const-string v5, "sq"

    new-array v6, v10, [Ljava/lang/String;

    const-string v7, "sr"

    aput-object v7, v6, v9

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/0eH;->b:LX/0Rf;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
