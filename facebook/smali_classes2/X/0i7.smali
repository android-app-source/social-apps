.class public final enum LX/0i7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0i7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0i7;

.field public static final enum FETCHED:LX/0i7;

.field public static final enum FETCHED_BUT_STALE:LX/0i7;

.field public static final enum FETCHING:LX/0i7;

.field public static final enum FETCH_FAILURE:LX/0i7;

.field public static final enum NO_DATA:LX/0i7;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 119650
    new-instance v0, LX/0i7;

    const-string v1, "NO_DATA"

    invoke-direct {v0, v1, v2}, LX/0i7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0i7;->NO_DATA:LX/0i7;

    .line 119651
    new-instance v0, LX/0i7;

    const-string v1, "FETCHING"

    invoke-direct {v0, v1, v3}, LX/0i7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0i7;->FETCHING:LX/0i7;

    .line 119652
    new-instance v0, LX/0i7;

    const-string v1, "FETCH_FAILURE"

    invoke-direct {v0, v1, v4}, LX/0i7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0i7;->FETCH_FAILURE:LX/0i7;

    .line 119653
    new-instance v0, LX/0i7;

    const-string v1, "FETCHED"

    invoke-direct {v0, v1, v5}, LX/0i7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0i7;->FETCHED:LX/0i7;

    .line 119654
    new-instance v0, LX/0i7;

    const-string v1, "FETCHED_BUT_STALE"

    invoke-direct {v0, v1, v6}, LX/0i7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0i7;->FETCHED_BUT_STALE:LX/0i7;

    .line 119655
    const/4 v0, 0x5

    new-array v0, v0, [LX/0i7;

    sget-object v1, LX/0i7;->NO_DATA:LX/0i7;

    aput-object v1, v0, v2

    sget-object v1, LX/0i7;->FETCHING:LX/0i7;

    aput-object v1, v0, v3

    sget-object v1, LX/0i7;->FETCH_FAILURE:LX/0i7;

    aput-object v1, v0, v4

    sget-object v1, LX/0i7;->FETCHED:LX/0i7;

    aput-object v1, v0, v5

    sget-object v1, LX/0i7;->FETCHED_BUT_STALE:LX/0i7;

    aput-object v1, v0, v6

    sput-object v0, LX/0i7;->$VALUES:[LX/0i7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 119647
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0i7;
    .locals 1

    .prologue
    .line 119649
    const-class v0, LX/0i7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0i7;

    return-object v0
.end method

.method public static values()[LX/0i7;
    .locals 1

    .prologue
    .line 119648
    sget-object v0, LX/0i7;->$VALUES:[LX/0i7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0i7;

    return-object v0
.end method
