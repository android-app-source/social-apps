.class public LX/0ht;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Landroid/graphics/Rect;


# instance fields
.field public A:I

.field public B:I

.field public C:I

.field public D:I

.field public E:Z

.field public F:F

.field private G:LX/3AV;

.field public H:LX/2dD;

.field public I:LX/2yQ;

.field public final J:Ljava/lang/Runnable;

.field private final K:LX/0xh;

.field private L:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field public b:Z

.field public c:I

.field public d:Z

.field public e:Z

.field public f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

.field public g:LX/5OY;

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:Landroid/content/Context;

.field public m:Landroid/view/WindowManager;

.field private n:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public o:Z

.field public p:I

.field public q:I

.field public r:Z

.field public s:I

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:Z

.field public y:I

.field public z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119138
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/0ht;->a:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 119001
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/0ht;-><init>(Landroid/content/Context;I)V

    .line 119002
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 119003
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119004
    const/16 v0, 0x3e8

    iput v0, p0, LX/0ht;->s:I

    .line 119005
    iput-boolean v1, p0, LX/0ht;->w:Z

    .line 119006
    iput-boolean v1, p0, LX/0ht;->x:Z

    .line 119007
    sget-object v0, LX/3AV;->BELOW:LX/3AV;

    iput-object v0, p0, LX/0ht;->G:LX/3AV;

    .line 119008
    new-instance v0, Lcom/facebook/fbui/popover/PopoverWindow$1;

    invoke-direct {v0, p0}, Lcom/facebook/fbui/popover/PopoverWindow$1;-><init>(LX/0ht;)V

    iput-object v0, p0, LX/0ht;->J:Ljava/lang/Runnable;

    .line 119009
    new-instance v0, LX/5OW;

    invoke-direct {v0, p0}, LX/5OW;-><init>(LX/0ht;)V

    iput-object v0, p0, LX/0ht;->K:LX/0xh;

    .line 119010
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-static {p1, p2}, LX/0ht;->a(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/0ht;->l:Landroid/content/Context;

    .line 119011
    const/4 v1, 0x1

    const/4 p1, 0x0

    .line 119012
    iget-object v0, p0, LX/0ht;->l:Landroid/content/Context;

    const-string p2, "window"

    invoke-virtual {v0, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, LX/0ht;->m:Landroid/view/WindowManager;

    .line 119013
    const v0, 0x3ecccccd    # 0.4f

    iput v0, p0, LX/0ht;->F:F

    .line 119014
    iput-boolean p1, p0, LX/0ht;->o:Z

    .line 119015
    iput-boolean p1, p0, LX/0ht;->t:Z

    .line 119016
    iput-boolean v1, p0, LX/0ht;->b:Z

    .line 119017
    iput-boolean p1, p0, LX/0ht;->d:Z

    .line 119018
    iput-boolean p1, p0, LX/0ht;->u:Z

    .line 119019
    iput-boolean p1, p0, LX/0ht;->E:Z

    .line 119020
    iput-boolean p1, p0, LX/0ht;->v:Z

    .line 119021
    iget-object v0, p0, LX/0ht;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 119022
    const p2, 0x7f0b01d4

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, LX/0ht;->p:I

    .line 119023
    const p2, 0x7f0b01d5

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/0ht;->q:I

    .line 119024
    invoke-static {p0}, LX/0ht;->b$redex0(LX/0ht;)V

    .line 119025
    invoke-static {p0}, LX/0ht;->y(LX/0ht;)I

    move-result v0

    iput v0, p0, LX/0ht;->c:I

    .line 119026
    iget v0, p0, LX/0ht;->c:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/0ht;->e:Z

    .line 119027
    return-void

    :cond_0
    move v0, p1

    .line 119028
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;I)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 119029
    if-ne p1, v3, :cond_1

    .line 119030
    const p1, 0x7f0e0251

    .line 119031
    :cond_0
    :goto_0
    return p1

    .line 119032
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 119033
    const p1, 0x7f0e0252

    goto :goto_0

    .line 119034
    :cond_2
    const/high16 v0, 0x1000000

    if-ge p1, v0, :cond_0

    .line 119035
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 119036
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010243

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 119037
    iget p1, v0, Landroid/util/TypedValue;->resourceId:I

    .line 119038
    if-nez p1, :cond_0

    const p1, 0x7f0e0251

    goto :goto_0
.end method

.method public static a(LX/0ht;Landroid/view/ViewTreeObserver;)V
    .locals 2

    .prologue
    .line 119039
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/0ht;->E:Z

    if-eqz v0, :cond_0

    .line 119040
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 119041
    invoke-direct {p0}, LX/0ht;->c()Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 119042
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0ht;->E:Z

    .line 119043
    :cond_0
    return-void

    .line 119044
    :cond_1
    invoke-direct {p0}, LX/0ht;->c()Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 119045
    invoke-static {}, LX/0ht;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119046
    invoke-static {p0, p1, p2}, LX/0ht;->b(LX/0ht;Landroid/view/View;Z)V

    .line 119047
    :goto_0
    return-void

    .line 119048
    :cond_0
    iget-object v0, p0, LX/0ht;->J:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public static b(LX/0ht;Landroid/view/View;Z)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 119049
    const/4 v2, 0x0

    .line 119050
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 119051
    iget v1, p0, LX/0ht;->s:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 119052
    const v1, 0x40202

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 119053
    iget-boolean v1, p0, LX/0ht;->b:Z

    if-nez v1, :cond_0

    .line 119054
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v1, v1, 0x20

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 119055
    :cond_0
    const/16 v1, 0x10

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 119056
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 119057
    iget v1, p0, LX/0ht;->F:F

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 119058
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 119059
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 119060
    move-object v1, v0

    .line 119061
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    iput-object v0, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 119062
    invoke-virtual {p0, p1, p2, v1}, LX/0ht;->a(Landroid/view/View;ZLandroid/view/WindowManager$LayoutParams;)V

    .line 119063
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 119064
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, LX/0ht;->E:Z

    if-nez v2, :cond_1

    .line 119065
    invoke-direct {p0}, LX/0ht;->c()Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 119066
    iput-boolean v3, p0, LX/0ht;->E:Z

    .line 119067
    :cond_1
    iget-boolean v2, p0, LX/0ht;->r:Z

    move v2, v2

    .line 119068
    if-nez v2, :cond_5

    .line 119069
    invoke-static {}, LX/0ht;->r()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 119070
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, p0, LX/0ht;->g:LX/5OY;

    invoke-virtual {v0, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 119071
    :goto_0
    iput-boolean v3, p0, LX/0ht;->r:Z

    .line 119072
    :cond_2
    :goto_1
    return-void

    .line 119073
    :cond_3
    iget-object v2, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    if-nez v2, :cond_4

    .line 119074
    invoke-static {p0, v0}, LX/0ht;->a(LX/0ht;Landroid/view/ViewTreeObserver;)V

    goto :goto_1

    .line 119075
    :cond_4
    iget-object v0, p0, LX/0ht;->m:Landroid/view/WindowManager;

    iget-object v2, p0, LX/0ht;->g:LX/5OY;

    invoke-interface {v0, v2, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 119076
    :cond_5
    invoke-static {}, LX/0ht;->r()Z

    move-result v2

    if-nez v2, :cond_2

    .line 119077
    iget-object v2, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 119078
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 119079
    iget v5, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 119080
    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 119081
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 119082
    const/4 v7, 0x2

    new-array v7, v7, [I

    .line 119083
    invoke-virtual {p1, v6}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 119084
    invoke-virtual {p1, v7}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 119085
    aget p2, v7, v3

    aget v7, v7, v2

    invoke-virtual {v6, p2, v7}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 119086
    iget v7, v6, Landroid/graphics/Rect;->left:I

    if-lez v7, :cond_6

    iget v7, v6, Landroid/graphics/Rect;->left:I

    if-lt v7, v5, :cond_7

    :cond_6
    iget v7, v6, Landroid/graphics/Rect;->right:I

    if-lez v7, :cond_c

    iget v7, v6, Landroid/graphics/Rect;->right:I

    if-ge v7, v5, :cond_c

    :cond_7
    iget v5, v6, Landroid/graphics/Rect;->top:I

    if-lez v5, :cond_8

    iget v5, v6, Landroid/graphics/Rect;->top:I

    if-lt v5, v4, :cond_9

    :cond_8
    iget v5, v6, Landroid/graphics/Rect;->bottom:I

    if-lez v5, :cond_c

    iget v5, v6, Landroid/graphics/Rect;->bottom:I

    if-ge v5, v4, :cond_c

    :cond_9
    :goto_2
    move v2, v2

    .line 119087
    if-nez v2, :cond_b

    .line 119088
    :cond_a
    invoke-static {p0, v0}, LX/0ht;->a(LX/0ht;Landroid/view/ViewTreeObserver;)V

    .line 119089
    iget-boolean v0, p0, LX/0ht;->r:Z

    if-eqz v0, :cond_2

    .line 119090
    invoke-virtual {p0}, LX/0ht;->l()V

    goto :goto_1

    .line 119091
    :cond_b
    iget-object v0, p0, LX/0ht;->m:Landroid/view/WindowManager;

    iget-object v2, p0, LX/0ht;->g:LX/5OY;

    invoke-interface {v0, v2, v1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    :cond_c
    move v2, v3

    goto :goto_2
.end method

.method public static b$redex0(LX/0ht;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 119092
    new-instance v0, LX/5OY;

    iget-object v1, p0, LX/0ht;->l:Landroid/content/Context;

    invoke-direct {v0, p0, v1, v3}, LX/5OY;-><init>(LX/0ht;Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, LX/0ht;->g:LX/5OY;

    .line 119093
    iget-object v0, p0, LX/0ht;->l:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0}, LX/0ht;->g()I

    move-result v1

    iget-object v2, p0, LX/0ht;->g:LX/5OY;

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 119094
    iget-object v0, p0, LX/0ht;->g:LX/5OY;

    const v1, 0x7f0d10ef

    invoke-virtual {v0, v1}, LX/5OY;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/popover/PopoverViewFlipper;

    iput-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    .line 119095
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingTop()I

    move-result v0

    iput v0, p0, LX/0ht;->y:I

    .line 119096
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingBottom()I

    move-result v0

    iput v0, p0, LX/0ht;->z:I

    .line 119097
    invoke-static {p0}, LX/0ht;->y(LX/0ht;)I

    move-result v0

    iput v0, p0, LX/0ht;->c:I

    .line 119098
    iget v0, p0, LX/0ht;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/0ht;->e:Z

    .line 119099
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->PopoverWindow:[I

    invoke-virtual {v0, v3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 119100
    const/16 v1, 0x0

    iget-boolean v2, p0, LX/0ht;->e:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/0ht;->e:Z

    .line 119101
    const/16 v1, 0x1

    iget-boolean v2, p0, LX/0ht;->d:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/0ht;->d:Z

    .line 119102
    const/16 v1, 0x2

    iget-boolean v2, p0, LX/0ht;->u:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/0ht;->u:Z

    .line 119103
    const/16 v1, 0x3

    iget-boolean v2, p0, LX/0ht;->v:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/0ht;->v:Z

    .line 119104
    const/16 v1, 0x8

    iget v2, p0, LX/0ht;->y:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, LX/0ht;->y:I

    .line 119105
    const/16 v1, 0x9

    iget v2, p0, LX/0ht;->z:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, LX/0ht;->z:I

    .line 119106
    const/16 v1, 0x5

    iget v2, p0, LX/0ht;->A:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, LX/0ht;->A:I

    .line 119107
    const/16 v1, 0x4

    iget v2, p0, LX/0ht;->B:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, LX/0ht;->B:I

    .line 119108
    const/16 v1, 0x7

    iget v2, p0, LX/0ht;->C:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, LX/0ht;->C:I

    .line 119109
    const/16 v1, 0x6

    iget v2, p0, LX/0ht;->D:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, LX/0ht;->D:I

    .line 119110
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 119111
    return-void

    .line 119112
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    .locals 1

    .prologue
    .line 119113
    iget-object v0, p0, LX/0ht;->L:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-nez v0, :cond_0

    .line 119114
    invoke-virtual {p0}, LX/0ht;->k()Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    move-result-object v0

    iput-object v0, p0, LX/0ht;->L:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 119115
    :cond_0
    iget-object v0, p0, LX/0ht;->L:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    return-object v0
.end method

.method public static r()Z
    .locals 1

    .prologue
    .line 119116
    const-string v0, "popover_attach_to_activity"

    invoke-static {v0}, Ljava/lang/Boolean;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, LX/0i9;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private s()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 119117
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 119118
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 119119
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 119120
    return-void
.end method

.method private t()Z
    .locals 2

    .prologue
    .line 119121
    invoke-static {p0}, LX/0ht;->y(LX/0ht;)I

    move-result v0

    .line 119122
    iget-boolean v1, p0, LX/0ht;->e:Z

    if-eqz v1, :cond_0

    if-lez v0, :cond_0

    iget-object v1, p0, LX/0ht;->l:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ge v0, v1, :cond_0

    .line 119123
    const/4 v0, 0x0

    .line 119124
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, LX/0ht;->e:Z

    goto :goto_0
.end method

.method public static w(LX/0ht;)V
    .locals 1

    .prologue
    .line 119125
    iget-boolean v0, p0, LX/0ht;->r:Z

    move v0, v0

    .line 119126
    if-eqz v0, :cond_1

    .line 119127
    iget-object v0, p0, LX/0ht;->I:LX/2yQ;

    if-eqz v0, :cond_0

    .line 119128
    iget-object v0, p0, LX/0ht;->I:LX/2yQ;

    invoke-interface {v0}, LX/2yQ;->a()Z

    .line 119129
    :cond_0
    invoke-virtual {p0}, LX/0ht;->l()V

    .line 119130
    :cond_1
    return-void
.end method

.method public static x(LX/0ht;)V
    .locals 2

    .prologue
    .line 119131
    iget-boolean v0, p0, LX/0ht;->r:Z

    move v0, v0

    .line 119132
    if-nez v0, :cond_1

    .line 119133
    :cond_0
    :goto_0
    return-void

    .line 119134
    :cond_1
    iget-object v0, p0, LX/0ht;->n:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 119135
    :goto_1
    if-eqz v0, :cond_0

    .line 119136
    iget-boolean v1, p0, LX/0ht;->o:Z

    invoke-direct {p0, v0, v1}, LX/0ht;->a(Landroid/view/View;Z)V

    goto :goto_0

    .line 119137
    :cond_2
    iget-object v0, p0, LX/0ht;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_1
.end method

.method public static y(LX/0ht;)I
    .locals 2

    .prologue
    .line 119139
    iget-object v0, p0, LX/0ht;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final a(IIII)V
    .locals 0

    .prologue
    .line 119140
    iput p1, p0, LX/0ht;->A:I

    .line 119141
    iput p2, p0, LX/0ht;->B:I

    .line 119142
    iput p3, p0, LX/0ht;->C:I

    .line 119143
    iput p4, p0, LX/0ht;->D:I

    .line 119144
    return-void
.end method

.method public final a(LX/2yQ;)V
    .locals 0

    .prologue
    .line 119145
    iput-object p1, p0, LX/0ht;->I:LX/2yQ;

    .line 119146
    return-void
.end method

.method public a(LX/3AV;)V
    .locals 1

    .prologue
    .line 119147
    iget-object v0, p0, LX/0ht;->G:LX/3AV;

    if-eq v0, p1, :cond_0

    .line 119148
    iput-object p1, p0, LX/0ht;->G:LX/3AV;

    .line 119149
    :cond_0
    return-void
.end method

.method public final a(LX/5OV;)V
    .locals 1

    .prologue
    .line 119150
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setTransitionType(LX/5OV;)V

    .line 119151
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 119152
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 119153
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 118984
    iget-boolean v0, p0, LX/0ht;->u:Z

    if-eqz v0, :cond_0

    .line 118985
    invoke-virtual {p0, p1}, LX/0ht;->f(Landroid/view/View;)V

    .line 118986
    :goto_0
    return-void

    .line 118987
    :cond_0
    if-eqz p1, :cond_1

    .line 118988
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/0ht;->n:Ljava/lang/ref/WeakReference;

    .line 118989
    :goto_1
    iput-boolean v1, p0, LX/0ht;->o:Z

    .line 118990
    invoke-direct {p0, p1, v1}, LX/0ht;->a(Landroid/view/View;Z)V

    goto :goto_0

    .line 118991
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/0ht;->n:Ljava/lang/ref/WeakReference;

    goto :goto_1
.end method

.method public final a(Landroid/view/View;IIII)V
    .locals 1

    .prologue
    .line 118992
    if-eqz p1, :cond_0

    .line 118993
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/0ht;->n:Ljava/lang/ref/WeakReference;

    .line 118994
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0ht;->o:Z

    .line 118995
    iput p2, p0, LX/0ht;->h:I

    .line 118996
    iput p3, p0, LX/0ht;->i:I

    .line 118997
    iput p4, p0, LX/0ht;->j:I

    .line 118998
    iput p5, p0, LX/0ht;->k:I

    .line 118999
    return-void

    .line 119000
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/0ht;->n:Ljava/lang/ref/WeakReference;

    goto :goto_0
.end method

.method public a(Landroid/view/View;ZLandroid/view/WindowManager$LayoutParams;)V
    .locals 20

    .prologue
    .line 118796
    invoke-direct/range {p0 .. p0}, LX/0ht;->s()V

    .line 118797
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 118798
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 118799
    move-object/from16 v0, p0

    iget v14, v0, LX/0ht;->j:I

    .line 118800
    move-object/from16 v0, p0

    iget v6, v0, LX/0ht;->k:I

    .line 118801
    const/4 v2, 0x0

    aget v2, v1, v2

    move-object/from16 v0, p0

    iget v3, v0, LX/0ht;->h:I

    add-int v8, v2, v3

    .line 118802
    const/4 v2, 0x1

    aget v1, v1, v2

    move-object/from16 v0, p0

    iget v2, v0, LX/0ht;->i:I

    add-int v15, v1, v2

    .line 118803
    invoke-virtual/range {p0 .. p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 118804
    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v16, v0

    .line 118805
    iget v11, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 118806
    add-int v1, v15, v6

    sub-int v2, v11, v15

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 118807
    const/4 v2, 0x0

    invoke-static {v14, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 118808
    const/high16 v3, -0x80000000

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 118809
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0ht;->g:LX/5OY;

    invoke-virtual {v3, v2, v1}, LX/5OY;->measure(II)V

    .line 118810
    move-object/from16 v0, p0

    iget-object v1, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getMeasuredWidth()I

    move-result v2

    .line 118811
    move-object/from16 v0, p0

    iget-object v1, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getMeasuredHeight()I

    move-result v3

    .line 118812
    move-object/from16 v0, p0

    iget-object v1, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingLeft()I

    move-result v7

    .line 118813
    move-object/from16 v0, p0

    iget-object v1, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingRight()I

    move-result v17

    .line 118814
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v18

    .line 118815
    add-int v1, v2, v7

    add-int v1, v1, v17

    move/from16 v0, v16

    if-lt v1, v0, :cond_1

    const/4 v1, 0x1

    move v13, v1

    .line 118816
    :goto_0
    if-eqz v13, :cond_2

    move v1, v2

    :goto_1
    move-object/from16 v0, p3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 118817
    move-object/from16 v0, p0

    iget-boolean v1, v0, LX/0ht;->b:Z

    if-eqz v1, :cond_3

    const/4 v1, -0x1

    :goto_2
    move-object/from16 v0, p3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 118818
    move-object/from16 v0, p0

    iget-object v1, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 118819
    if-nez p2, :cond_4

    .line 118820
    const v2, 0x7f0e028a

    move-object/from16 v0, p3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 118821
    const/16 v2, 0x11

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    move-object/from16 v0, p3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 118822
    div-int/lit8 v4, v16, 0x2

    .line 118823
    div-int/lit8 v1, v3, 0x2

    .line 118824
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    sget-object v3, LX/5OS;->NONE:LX/5OS;

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setNubShown(LX/5OS;)V

    .line 118825
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v2}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 118826
    const/4 v2, 0x0

    move-object/from16 v0, p3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 118827
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v2, v4, v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->a(II)V

    .line 118828
    return-void

    .line 118829
    :cond_1
    const/4 v1, 0x0

    move v13, v1

    goto :goto_0

    .line 118830
    :cond_2
    const/4 v1, -0x1

    goto :goto_1

    :cond_3
    move v1, v3

    .line 118831
    goto :goto_2

    .line 118832
    :cond_4
    div-int/lit8 v4, v14, 0x2

    add-int v9, v8, v4

    .line 118833
    if-gt v3, v15, :cond_f

    const/4 v4, 0x1

    .line 118834
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/0ht;->d:Z

    if-eqz v5, :cond_5

    .line 118835
    add-int v4, v15, v6

    if-gt v3, v4, :cond_10

    const/4 v4, 0x1

    .line 118836
    :cond_5
    :goto_5
    add-int v5, v15, v6

    add-int/2addr v5, v3

    move-object/from16 v0, p0

    iget v10, v0, LX/0ht;->D:I

    sub-int v10, v11, v10

    if-gt v5, v10, :cond_11

    const/4 v5, 0x1

    .line 118837
    :goto_6
    move-object/from16 v0, p0

    iget-boolean v10, v0, LX/0ht;->d:Z

    if-eqz v10, :cond_2a

    .line 118838
    add-int v5, v15, v3

    move-object/from16 v0, p0

    iget v10, v0, LX/0ht;->D:I

    sub-int v10, v11, v10

    if-gt v5, v10, :cond_12

    const/4 v5, 0x1

    :goto_7
    move v10, v5

    .line 118839
    :goto_8
    if-eqz v4, :cond_13

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0ht;->G:LX/3AV;

    sget-object v12, LX/3AV;->CENTER:LX/3AV;

    if-eq v5, v12, :cond_13

    const/4 v5, 0x1

    .line 118840
    :goto_9
    if-eqz v10, :cond_14

    move-object/from16 v0, p0

    iget-object v10, v0, LX/0ht;->G:LX/3AV;

    sget-object v12, LX/3AV;->CENTER:LX/3AV;

    if-eq v10, v12, :cond_14

    move-object/from16 v0, p0

    iget-object v10, v0, LX/0ht;->G:LX/3AV;

    sget-object v12, LX/3AV;->BELOW:LX/3AV;

    if-eq v10, v12, :cond_6

    move-object/from16 v0, p0

    iget-object v10, v0, LX/0ht;->G:LX/3AV;

    sget-object v12, LX/3AV;->ABOVE:LX/3AV;

    if-ne v10, v12, :cond_14

    if-nez v4, :cond_14

    :cond_6
    const/4 v4, 0x1

    .line 118841
    :goto_a
    move-object/from16 v0, p0

    iget v10, v0, LX/0ht;->B:I

    if-lt v15, v10, :cond_7

    move-object/from16 v0, p0

    iget v10, v0, LX/0ht;->D:I

    sub-int v10, v11, v10

    if-lt v15, v10, :cond_29

    .line 118842
    :cond_7
    const/4 v4, 0x0

    .line 118843
    const/4 v5, 0x0

    move v11, v4

    move v12, v5

    .line 118844
    :goto_b
    if-nez v12, :cond_15

    if-nez v11, :cond_15

    const/4 v4, 0x1

    .line 118845
    :goto_c
    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/0ht;->t:Z

    if-eqz v5, :cond_16

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/0ht;->d:Z

    if-nez v5, :cond_16

    const/4 v5, 0x1

    move v10, v5

    .line 118846
    :goto_d
    if-eqz v11, :cond_19

    .line 118847
    move-object/from16 v0, p0

    iget v5, v0, LX/0ht;->y:I

    .line 118848
    if-eqz v10, :cond_17

    move-object/from16 v0, p0

    iget v3, v0, LX/0ht;->q:I

    :goto_e
    sub-int v3, v5, v3

    .line 118849
    add-int v5, v15, v6

    sub-int v3, v5, v3

    move-object/from16 v0, p3

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 118850
    move-object/from16 v0, p3

    iget v5, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/0ht;->d:Z

    if-eqz v3, :cond_18

    move v3, v6

    :goto_f
    sub-int v3, v5, v3

    move-object/from16 v0, p3

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 118851
    const/4 v3, 0x0

    .line 118852
    sget-object v5, LX/0ht;->a:Landroid/graphics/Rect;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 118853
    sget-object v5, LX/0ht;->a:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    if-lez v5, :cond_8

    .line 118854
    sget-object v3, LX/0ht;->a:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    .line 118855
    :cond_8
    move-object/from16 v0, p3

    iget v5, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    move-object/from16 v0, p0

    iget v6, v0, LX/0ht;->B:I

    add-int/2addr v6, v3

    if-ge v5, v6, :cond_9

    .line 118856
    move-object/from16 v0, p0

    iget v5, v0, LX/0ht;->B:I

    add-int/2addr v3, v5

    move-object/from16 v0, p3

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 118857
    :cond_9
    const/4 v3, 0x0

    .line 118858
    const v5, 0x7f0e028b

    move-object/from16 v0, p3

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 118859
    const/16 v5, 0x31

    iput v5, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    move-object/from16 v0, p3

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 118860
    if-eqz v10, :cond_a

    .line 118861
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    sget-object v6, LX/5OS;->BELOW:LX/5OS;

    invoke-virtual {v5, v6}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setNubShown(LX/5OS;)V

    :cond_a
    move v6, v3

    move v5, v9

    .line 118862
    :goto_10
    if-nez v4, :cond_c

    .line 118863
    if-nez v13, :cond_b

    invoke-direct/range {p0 .. p0}, LX/0ht;->t()Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 118864
    :cond_b
    move-object/from16 v0, p3

    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    or-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p3

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 118865
    iget v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 118866
    :cond_c
    :goto_11
    if-nez v4, :cond_25

    if-nez v13, :cond_25

    .line 118867
    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/0ht;->v:Z

    if-nez v3, :cond_22

    .line 118868
    div-int/lit8 v3, v2, 0x2

    sub-int v3, v9, v3

    .line 118869
    invoke-direct/range {p0 .. p0}, LX/0ht;->t()Z

    move-result v4

    if-nez v4, :cond_20

    move-object/from16 v0, p0

    iget v4, v0, LX/0ht;->c:I

    if-lez v4, :cond_20

    .line 118870
    if-ge v3, v7, :cond_1f

    move v3, v7

    .line 118871
    :cond_d
    :goto_12
    div-int/lit8 v4, v2, 0x2

    add-int/2addr v4, v3

    .line 118872
    :cond_e
    :goto_13
    sub-int v2, v16, v2

    sub-int v5, v2, v3

    .line 118873
    sub-int v2, v9, v3

    .line 118874
    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 118875
    iput v5, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 118876
    :goto_14
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v3, v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 118877
    move-object/from16 v0, p0

    iget-object v1, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setNubOffset(I)V

    move v1, v6

    goto/16 :goto_3

    .line 118878
    :cond_f
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 118879
    :cond_10
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 118880
    :cond_11
    const/4 v5, 0x0

    goto/16 :goto_6

    .line 118881
    :cond_12
    const/4 v5, 0x0

    goto/16 :goto_7

    .line 118882
    :cond_13
    const/4 v5, 0x0

    goto/16 :goto_9

    .line 118883
    :cond_14
    const/4 v4, 0x0

    goto/16 :goto_a

    .line 118884
    :cond_15
    const/4 v4, 0x0

    goto/16 :goto_c

    .line 118885
    :cond_16
    const/4 v5, 0x0

    move v10, v5

    goto/16 :goto_d

    .line 118886
    :cond_17
    const/4 v3, 0x0

    goto/16 :goto_e

    .line 118887
    :cond_18
    const/4 v3, 0x0

    goto/16 :goto_f

    .line 118888
    :cond_19
    if-eqz v12, :cond_1d

    .line 118889
    move-object/from16 v0, p0

    iget v11, v0, LX/0ht;->z:I

    .line 118890
    if-eqz v10, :cond_1b

    move-object/from16 v0, p0

    iget v5, v0, LX/0ht;->p:I

    :goto_15
    sub-int v5, v11, v5

    .line 118891
    sub-int v11, v18, v15

    sub-int v5, v11, v5

    move-object/from16 v0, p3

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 118892
    move-object/from16 v0, p3

    iget v5, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    move-object/from16 v0, p0

    iget-boolean v11, v0, LX/0ht;->d:Z

    if-eqz v11, :cond_1c

    :goto_16
    sub-int/2addr v5, v6

    move-object/from16 v0, p3

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 118893
    move-object/from16 v0, p3

    iget v5, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    move-object/from16 v0, p0

    iget v6, v0, LX/0ht;->D:I

    if-ge v5, v6, :cond_1a

    .line 118894
    move-object/from16 v0, p0

    iget v5, v0, LX/0ht;->D:I

    move-object/from16 v0, p3

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 118895
    :cond_1a
    const v5, 0x7f0e028c

    move-object/from16 v0, p3

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 118896
    const/16 v5, 0x51

    iput v5, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    move-object/from16 v0, p3

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 118897
    if-eqz v10, :cond_28

    .line 118898
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    sget-object v6, LX/5OS;->ABOVE:LX/5OS;

    invoke-virtual {v5, v6}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setNubShown(LX/5OS;)V

    move v6, v3

    move v5, v9

    goto/16 :goto_10

    .line 118899
    :cond_1b
    const/4 v5, 0x0

    goto :goto_15

    .line 118900
    :cond_1c
    const/4 v6, 0x0

    goto :goto_16

    .line 118901
    :cond_1d
    div-int/lit8 v3, v3, 0x2

    .line 118902
    div-int/lit8 v5, v16, 0x2

    .line 118903
    const v6, 0x7f0e028a

    move-object/from16 v0, p3

    iput v6, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 118904
    const/16 v6, 0x11

    iput v6, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    move-object/from16 v0, p3

    iput v6, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 118905
    move-object/from16 v0, p0

    iget-object v6, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    sget-object v10, LX/5OS;->NONE:LX/5OS;

    invoke-virtual {v6, v10}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setNubShown(LX/5OS;)V

    move v6, v3

    goto/16 :goto_10

    .line 118906
    :cond_1e
    move-object/from16 v0, p3

    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    or-int/lit8 v3, v3, 0x3

    move-object/from16 v0, p3

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 118907
    iget v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    or-int/lit8 v3, v3, 0x3

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto/16 :goto_11

    .line 118908
    :cond_1f
    add-int v4, v3, v2

    add-int v5, v16, v17

    if-le v4, v5, :cond_d

    .line 118909
    sub-int v3, v16, v2

    add-int v3, v3, v17

    goto/16 :goto_12

    .line 118910
    :cond_20
    div-int/lit8 v4, v2, 0x2

    add-int/2addr v4, v3

    .line 118911
    neg-int v5, v7

    if-ge v3, v5, :cond_21

    .line 118912
    const/4 v3, 0x0

    .line 118913
    const/4 v4, 0x0

    .line 118914
    :cond_21
    add-int v5, v3, v2

    add-int v7, v16, v17

    if-le v5, v7, :cond_e

    .line 118915
    sub-int v3, v16, v2

    add-int v3, v3, v17

    .line 118916
    add-int v4, v3, v2

    goto/16 :goto_13

    .line 118917
    :cond_22
    add-int v4, v8, v14

    .line 118918
    move-object/from16 v0, p0

    iget v3, v0, LX/0ht;->A:I

    if-ge v8, v3, :cond_27

    .line 118919
    move-object/from16 v0, p0

    iget v3, v0, LX/0ht;->A:I

    .line 118920
    :goto_17
    add-int v5, v3, v2

    move-object/from16 v0, p0

    iget v7, v0, LX/0ht;->C:I

    add-int/2addr v5, v7

    move/from16 v0, v16

    if-le v5, v0, :cond_26

    .line 118921
    sub-int v3, v16, v4

    .line 118922
    move-object/from16 v0, p0

    iget v4, v0, LX/0ht;->C:I

    if-ge v3, v4, :cond_23

    .line 118923
    move-object/from16 v0, p0

    iget v3, v0, LX/0ht;->C:I

    .line 118924
    :cond_23
    add-int v4, v3, v2

    move-object/from16 v0, p0

    iget v5, v0, LX/0ht;->A:I

    add-int/2addr v4, v5

    move/from16 v0, v16

    if-le v4, v0, :cond_24

    .line 118925
    sub-int v3, v16, v2

    div-int/lit8 v3, v3, 0x2

    .line 118926
    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v3

    .line 118927
    :goto_18
    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 118928
    const/4 v3, 0x0

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    move v4, v2

    move v2, v9

    .line 118929
    goto/16 :goto_14

    .line 118930
    :cond_24
    sub-int v2, v16, v2

    sub-int/2addr v2, v3

    .line 118931
    sub-int v3, v16, v3

    move/from16 v19, v2

    move v2, v3

    move/from16 v3, v19

    goto :goto_18

    .line 118932
    :cond_25
    add-int v3, v9, v7

    .line 118933
    sub-int v2, v16, v2

    div-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    move v2, v3

    move v4, v5

    goto/16 :goto_14

    :cond_26
    move v2, v3

    goto :goto_18

    :cond_27
    move v3, v8

    goto :goto_17

    :cond_28
    move v6, v3

    move v5, v9

    goto/16 :goto_10

    :cond_29
    move v11, v4

    move v12, v5

    goto/16 :goto_b

    :cond_2a
    move v10, v5

    goto/16 :goto_8
.end method

.method public final b(F)V
    .locals 1

    .prologue
    .line 118966
    iget v0, p0, LX/0ht;->F:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 118967
    iput p1, p0, LX/0ht;->F:F

    .line 118968
    :cond_0
    return-void
.end method

.method public final c(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 118960
    if-nez p1, :cond_0

    move v4, v2

    .line 118961
    :goto_0
    if-nez p1, :cond_1

    move v5, v2

    :goto_1
    move-object v0, p0

    move-object v1, p1

    move v3, v2

    .line 118962
    invoke-virtual/range {v0 .. v5}, LX/0ht;->a(Landroid/view/View;IIII)V

    .line 118963
    return-void

    .line 118964
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    goto :goto_0

    .line 118965
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    goto :goto_1
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 118955
    iput-boolean p1, p0, LX/0ht;->t:Z

    .line 118956
    iget-boolean v0, p0, LX/0ht;->r:Z

    move v0, v0

    .line 118957
    if-eqz v0, :cond_0

    .line 118958
    invoke-static {p0}, LX/0ht;->x(LX/0ht;)V

    .line 118959
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 118950
    iget-object v0, p0, LX/0ht;->n:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    .line 118951
    :cond_0
    :goto_0
    return-void

    .line 118952
    :cond_1
    iget-object v0, p0, LX/0ht;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 118953
    if-eqz v0, :cond_0

    .line 118954
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LX/0ht;->a(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public d(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 118948
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setContentView(Landroid/view/View;)V

    .line 118949
    return-void
.end method

.method public final d(Z)V
    .locals 1

    .prologue
    .line 118943
    iput-boolean p1, p0, LX/0ht;->b:Z

    .line 118944
    iget-boolean v0, p0, LX/0ht;->r:Z

    move v0, v0

    .line 118945
    if-eqz v0, :cond_0

    .line 118946
    invoke-static {p0}, LX/0ht;->x(LX/0ht;)V

    .line 118947
    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 118941
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->d()V

    .line 118942
    return-void
.end method

.method public e(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 118982
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->a(Landroid/view/View;)V

    .line 118983
    return-void
.end method

.method public final e(Z)V
    .locals 1

    .prologue
    .line 118936
    iput-boolean p1, p0, LX/0ht;->d:Z

    .line 118937
    iget-boolean v0, p0, LX/0ht;->r:Z

    move v0, v0

    .line 118938
    if-eqz v0, :cond_0

    .line 118939
    invoke-static {p0}, LX/0ht;->x(LX/0ht;)V

    .line 118940
    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 118934
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->e()V

    .line 118935
    return-void
.end method

.method public f(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 118793
    invoke-virtual {p0, p1}, LX/0ht;->c(Landroid/view/View;)V

    .line 118794
    invoke-virtual {p0}, LX/0ht;->d()V

    .line 118795
    return-void
.end method

.method public g()I
    .locals 1

    .prologue
    .line 118969
    const v0, 0x7f030612

    return v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 118970
    iget-object v0, p0, LX/0ht;->l:Landroid/content/Context;

    return-object v0
.end method

.method public final h()Landroid/view/View;
    .locals 1

    .prologue
    .line 118971
    iget-object v0, p0, LX/0ht;->n:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0ht;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0
.end method

.method public final i()LX/3AV;
    .locals 1

    .prologue
    .line 118972
    iget-object v0, p0, LX/0ht;->G:LX/3AV;

    return-object v0
.end method

.method public k()Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    .locals 1

    .prologue
    .line 118973
    new-instance v0, LX/5OX;

    invoke-direct {v0, p0}, LX/5OX;-><init>(LX/0ht;)V

    return-object v0
.end method

.method public l()V
    .locals 2

    .prologue
    .line 118974
    iget-boolean v0, p0, LX/0ht;->r:Z

    move v0, v0

    .line 118975
    if-nez v0, :cond_0

    .line 118976
    :goto_0
    return-void

    .line 118977
    :cond_0
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    iget-object v1, p0, LX/0ht;->K:LX/0xh;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->a(LX/0xh;)V

    goto :goto_0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 118978
    iget v0, p0, LX/0ht;->A:I

    return v0
.end method

.method public final o()I
    .locals 1

    .prologue
    .line 118979
    iget v0, p0, LX/0ht;->C:I

    return v0
.end method

.method public final p()I
    .locals 1

    .prologue
    .line 118980
    iget v0, p0, LX/0ht;->B:I

    return v0
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 118981
    iget v0, p0, LX/0ht;->D:I

    return v0
.end method
