.class public final LX/11Y;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/0if;


# direct methods
.method public constructor <init>(LX/0if;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 172611
    iput-object p1, p0, LX/11Y;->a:LX/0if;

    .line 172612
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 172613
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 11

    .prologue
    .line 172614
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 172615
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown what="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172616
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/15o;

    .line 172617
    iget-object v1, p0, LX/11Y;->a:LX/0if;

    invoke-static {v0}, LX/0if;->a(LX/15o;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, LX/15o;->a:LX/0ih;

    iget-object v4, v0, LX/15o;->b:Ljava/lang/Long;

    if-nez v4, :cond_0

    iget-object v4, p0, LX/11Y;->a:LX/0if;

    iget-object v4, v4, LX/0if;->e:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    :goto_0
    iget-object v0, v0, LX/15o;->g:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static/range {v1 .. v7}, LX/0if;->a(LX/0if;Ljava/lang/String;LX/0ih;JJ)V

    .line 172618
    :goto_1
    return-void

    .line 172619
    :cond_0
    iget-object v4, v0, LX/15o;->b:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    goto :goto_0

    .line 172620
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/15o;

    .line 172621
    iget-object v1, p0, LX/11Y;->a:LX/0if;

    invoke-static {v0}, LX/0if;->a(LX/15o;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, LX/15o;->a:LX/0ih;

    iget-object v4, v0, LX/15o;->b:Ljava/lang/Long;

    if-nez v4, :cond_1

    iget-object v4, p0, LX/11Y;->a:LX/0if;

    iget-object v4, v4, LX/0if;->e:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    :goto_2
    iget-object v0, v0, LX/15o;->g:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 172622
    invoke-static/range {v1 .. v7}, LX/0if;->b$redex0(LX/0if;Ljava/lang/String;LX/0ih;JJ)V

    .line 172623
    goto :goto_1

    :cond_1
    iget-object v4, v0, LX/15o;->b:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    goto :goto_2

    .line 172624
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/15o;

    .line 172625
    iget-object v1, p0, LX/11Y;->a:LX/0if;

    invoke-static {v0}, LX/0if;->a(LX/15o;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, LX/15o;->c:Ljava/lang/String;

    iget-object v0, v0, LX/15o;->g:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 172626
    invoke-static {v1}, LX/0if;->b$redex0(LX/0if;)V

    .line 172627
    iget-object v8, v1, LX/0if;->i:Ljava/util/Map;

    invoke-interface {v8, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/1Zr;

    .line 172628
    if-eqz v8, :cond_2

    .line 172629
    if-nez v3, :cond_4

    .line 172630
    :cond_2
    :goto_3
    goto :goto_1

    .line 172631
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/15o;

    .line 172632
    iget-object v1, p0, LX/11Y;->a:LX/0if;

    invoke-static {v0}, LX/0if;->a(LX/15o;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, LX/15o;->d:Ljava/lang/String;

    iget-object v4, v0, LX/15o;->e:Ljava/lang/String;

    iget-object v5, v0, LX/15o;->f:LX/1rQ;

    iget-object v0, v0, LX/15o;->g:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static/range {v1 .. v7}, LX/0if;->a(LX/0if;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1rQ;J)V

    goto :goto_1

    .line 172633
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/15o;

    .line 172634
    iget-object v1, p0, LX/11Y;->a:LX/0if;

    invoke-static {v0}, LX/0if;->a(LX/15o;)Ljava/lang/String;

    move-result-object v0

    .line 172635
    invoke-static {v1}, LX/0if;->b$redex0(LX/0if;)V

    .line 172636
    iget-object v2, v1, LX/0if;->i:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Zr;

    .line 172637
    iget-object v3, v1, LX/0if;->i:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 172638
    iget-object v3, v1, LX/0if;->h:LX/11D;

    sget-object v4, LX/1Zs;->FUNNEL_CANCELLED:LX/1Zs;

    invoke-virtual {v3, v4, v2}, LX/11D;->a(LX/1Zs;LX/1Zr;)V

    .line 172639
    goto/16 :goto_1

    .line 172640
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/15o;

    .line 172641
    iget-object v1, p0, LX/11Y;->a:LX/0if;

    invoke-static {v0}, LX/0if;->a(LX/15o;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, LX/15o;->g:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 172642
    invoke-static {v1}, LX/0if;->b$redex0(LX/0if;)V

    .line 172643
    iget-object v0, v1, LX/0if;->i:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Zr;

    .line 172644
    if-eqz v0, :cond_3

    .line 172645
    iget-object v3, v1, LX/0if;->i:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 172646
    sget-object v3, LX/2o8;->EXPLICIT:LX/2o8;

    invoke-static {v1, v0, v3, v4, v5}, LX/0if;->a(LX/0if;LX/1Zr;LX/2o8;J)V

    .line 172647
    :cond_3
    goto/16 :goto_1

    .line 172648
    :pswitch_6
    iget-object v0, p0, LX/11Y;->a:LX/0if;

    .line 172649
    invoke-static {v0}, LX/0if;->b$redex0(LX/0if;)V

    .line 172650
    invoke-static {v0}, LX/0if;->d(LX/0if;)V

    .line 172651
    :try_start_0
    iget-object v1, v0, LX/0if;->f:LX/11C;

    iget-object v2, v0, LX/0if;->i:Ljava/util/Map;

    invoke-virtual {v1, v2}, LX/11C;->a(Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172652
    :goto_4
    goto/16 :goto_1

    .line 172653
    :cond_4
    iput-wide v4, v8, LX/1Zr;->f:J

    .line 172654
    iget-boolean v9, v8, LX/1Zr;->e:Z

    move v9, v9

    .line 172655
    if-nez v9, :cond_2

    .line 172656
    iget-object v9, v8, LX/1Zr;->g:LX/0UE;

    if-nez v9, :cond_5

    .line 172657
    new-instance v9, LX/0UE;

    invoke-direct {v9}, LX/0UE;-><init>()V

    iput-object v9, v8, LX/1Zr;->g:LX/0UE;

    .line 172658
    :cond_5
    iget-object v9, v8, LX/1Zr;->g:LX/0UE;

    invoke-virtual {v9, v3}, LX/0UE;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 172659
    :catch_0
    move-exception v1

    .line 172660
    sget-object v2, LX/0if;->a:Ljava/lang/String;

    const-string v3, "Failed to save funnels!"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_6
        :pswitch_1
    .end packed-switch
.end method
