.class public LX/0Vv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile D:LX/0Vv;

.field public static final a:LX/0Tn;


# instance fields
.field private final A:LX/0W7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0W7",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final B:LX/0W7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0W7",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final C:LX/0W7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0W7",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field public final c:Landroid/content/res/Resources;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0WV;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Wn;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0eZ;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Wk;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2z0;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ek;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0XJ;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Uh;

.field private final l:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public m:LX/0ed;

.field private n:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0Wo;

.field private volatile p:Z

.field private volatile q:Z

.field public final r:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/0lA;",
            ">;"
        }
    .end annotation
.end field

.field public final s:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/0lA;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/0ep;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/0ep;",
            ">;"
        }
    .end annotation
.end field

.field public v:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final w:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final x:LX/03V;

.field private final y:LX/0W9;

.field private final z:LX/0WC;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 75160
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "resources/impl/string_resources_key"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0Vv;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;LX/0Ot;LX/0Ot;LX/0Wk;LX/0Ot;LX/0Ot;LX/0Wn;LX/03V;LX/0W9;LX/0WC;LX/0Or;LX/0Uh;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .param p2    # Landroid/content/res/Resources;
        .annotation runtime Lcom/facebook/resources/BaseResources;
        .end annotation
    .end param
    .param p12    # LX/0Or;
        .annotation runtime Lcom/facebook/user/gender/UserGender;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/res/Resources;",
            "LX/0Ot",
            "<",
            "LX/0WV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0eZ;",
            ">;",
            "LX/0Wk;",
            "LX/0Ot",
            "<",
            "LX/2z0;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0ek;",
            ">;",
            "LX/0Wn;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0W9;",
            "LX/0WC;",
            "LX/0Or",
            "<",
            "LX/0XJ;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 75132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75133
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, LX/0Vv;->n:Ljava/util/concurrent/atomic/AtomicReference;

    .line 75134
    new-instance v0, LX/0Wo;

    invoke-direct {v0, p0}, LX/0Wo;-><init>(LX/0Vv;)V

    iput-object v0, p0, LX/0Vv;->o:LX/0Wo;

    .line 75135
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Vv;->p:Z

    .line 75136
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Vv;->q:Z

    .line 75137
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, LX/0Vv;->r:Ljava/util/concurrent/atomic/AtomicReference;

    .line 75138
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, LX/0Vv;->s:Ljava/util/concurrent/atomic/AtomicReference;

    .line 75139
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, LX/0Vv;->t:Ljava/util/concurrent/atomic/AtomicReference;

    .line 75140
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, LX/0Vv;->u:Ljava/util/concurrent/atomic/AtomicReference;

    .line 75141
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/0Vv;->w:Lcom/google/common/util/concurrent/SettableFuture;

    .line 75142
    new-instance v0, LX/0Wp;

    invoke-direct {v0, p0}, LX/0Wp;-><init>(LX/0Vv;)V

    iput-object v0, p0, LX/0Vv;->A:LX/0W7;

    .line 75143
    new-instance v0, LX/0Wq;

    invoke-direct {v0, p0}, LX/0Wq;-><init>(LX/0Vv;)V

    iput-object v0, p0, LX/0Vv;->B:LX/0W7;

    .line 75144
    new-instance v0, LX/0Wr;

    invoke-direct {v0, p0}, LX/0Wr;-><init>(LX/0Vv;)V

    iput-object v0, p0, LX/0Vv;->C:LX/0W7;

    .line 75145
    iput-object p1, p0, LX/0Vv;->b:Landroid/content/Context;

    .line 75146
    iput-object p2, p0, LX/0Vv;->c:Landroid/content/res/Resources;

    .line 75147
    iput-object p3, p0, LX/0Vv;->d:LX/0Ot;

    .line 75148
    iput-object p8, p0, LX/0Vv;->e:LX/0Wn;

    .line 75149
    iput-object p5, p0, LX/0Vv;->g:LX/0Wk;

    .line 75150
    iput-object p6, p0, LX/0Vv;->h:LX/0Ot;

    .line 75151
    iput-object p7, p0, LX/0Vv;->i:LX/0Ot;

    .line 75152
    iput-object p4, p0, LX/0Vv;->f:LX/0Ot;

    .line 75153
    iput-object p9, p0, LX/0Vv;->x:LX/03V;

    .line 75154
    iput-object p10, p0, LX/0Vv;->y:LX/0W9;

    .line 75155
    iput-object p11, p0, LX/0Vv;->z:LX/0WC;

    .line 75156
    iput-object p12, p0, LX/0Vv;->j:LX/0Or;

    .line 75157
    iput-object p13, p0, LX/0Vv;->k:LX/0Uh;

    .line 75158
    iput-object p14, p0, LX/0Vv;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 75159
    return-void
.end method

.method public static a(LX/0QB;)LX/0Vv;
    .locals 3

    .prologue
    .line 75122
    sget-object v0, LX/0Vv;->D:LX/0Vv;

    if-nez v0, :cond_1

    .line 75123
    const-class v1, LX/0Vv;

    monitor-enter v1

    .line 75124
    :try_start_0
    sget-object v0, LX/0Vv;->D:LX/0Vv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 75125
    if-eqz v2, :cond_0

    .line 75126
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/0Vv;->b(LX/0QB;)LX/0Vv;

    move-result-object v0

    sput-object v0, LX/0Vv;->D:LX/0Vv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75127
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 75128
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 75129
    :cond_1
    sget-object v0, LX/0Vv;->D:LX/0Vv;

    return-object v0

    .line 75130
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 75131
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0Vv;LX/0W7;IILX/0W6;)Ljava/lang/Object;
    .locals 5
    .param p3    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0W7",
            "<TT;>;II",
            "LX/0W6;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 75094
    iget-boolean v0, p0, LX/0Vv;->q:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p2}, LX/0Vv;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75095
    iget-object v0, p0, LX/0Vv;->c:Landroid/content/res/Resources;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    .line 75096
    const-string v1, "string/common_google_play_services_unknown_issue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    move v0, v0

    .line 75097
    if-nez v0, :cond_0

    .line 75098
    const/4 v4, 0x1

    .line 75099
    iget-object v0, p0, LX/0Vv;->c:Landroid/content/res/Resources;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    .line 75100
    const-string v1, "StringResourcesDelegate used before initialized: resource %s requested"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 75101
    iget-object v1, p0, LX/0Vv;->x:LX/03V;

    const-string v2, "string_resources_delegate"

    invoke-static {v2, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    .line 75102
    iput-boolean v4, v0, LX/0VK;->d:Z

    .line 75103
    move-object v0, v0

    .line 75104
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 75105
    :cond_0
    iget-boolean v0, p0, LX/0Vv;->p:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p2}, LX/0Vv;->c(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 75106
    :cond_1
    invoke-interface {p1, p2, p3}, LX/0W7;->a(II)Ljava/lang/Object;

    move-result-object v0

    .line 75107
    :goto_0
    return-object v0

    .line 75108
    :cond_2
    iget-object v0, p0, LX/0Vv;->s:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lA;

    .line 75109
    iget-object v1, p0, LX/0Vv;->r:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lA;

    .line 75110
    if-nez v1, :cond_3

    if-nez v0, :cond_3

    .line 75111
    iget-object v0, p0, LX/0Vv;->e:LX/0Wn;

    .line 75112
    iget-boolean v1, v0, LX/0Wn;->c:Z

    if-eqz v1, :cond_6

    .line 75113
    :goto_1
    invoke-interface {p1, p2, p3}, LX/0W7;->a(II)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 75114
    :cond_3
    if-eqz v0, :cond_4

    .line 75115
    :try_start_0
    iget-object v0, p0, LX/0Vv;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0XJ;

    invoke-interface {p1, p2, p4, v0}, LX/0W7;->b(ILX/0W6;LX/0XJ;)Ljava/lang/Object;
    :try_end_0
    .catch LX/0xp; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 75116
    :catch_0
    :cond_4
    if-eqz v1, :cond_5

    .line 75117
    :try_start_1
    iget-object v0, p0, LX/0Vv;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0XJ;

    invoke-interface {p1, p2, p4, v0}, LX/0W7;->a(ILX/0W6;LX/0XJ;)Ljava/lang/Object;
    :try_end_1
    .catch LX/0xp; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 75118
    :catch_1
    :cond_5
    invoke-interface {p1, p2, p3}, LX/0W7;->a(II)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 75119
    :cond_6
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "fbresources_not_available"

    invoke-direct {v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 75120
    iget-object v1, v0, LX/0Wn;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 75121
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0Wn;->c:Z

    goto :goto_1
.end method

.method private a(LX/0ec;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 75060
    sget-object v0, LX/0ec;->ASSET:LX/0ec;

    if-ne p1, v0, :cond_2

    .line 75061
    iget-object v0, p0, LX/0Vv;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0eZ;

    invoke-virtual {p0}, LX/0Vv;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0eZ;->a(Ljava/util/Locale;)Z

    move-result v2

    .line 75062
    iget-object v1, p0, LX/0Vv;->r:Ljava/util/concurrent/atomic/AtomicReference;

    .line 75063
    iget-object v0, p0, LX/0Vv;->t:Ljava/util/concurrent/atomic/AtomicReference;

    move-object v7, v0

    move-object v8, v1

    move v0, v2

    .line 75064
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 75065
    :cond_0
    if-nez v0, :cond_1

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 75066
    invoke-virtual {v8, v3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 75067
    :cond_1
    :goto_1
    return-void

    .line 75068
    :cond_2
    iget-object v0, p0, LX/0Vv;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0eZ;

    invoke-virtual {p0}, LX/0Vv;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0eZ;->b(Ljava/util/Locale;)Z

    move-result v2

    .line 75069
    iget-object v1, p0, LX/0Vv;->s:Ljava/util/concurrent/atomic/AtomicReference;

    .line 75070
    iget-object v0, p0, LX/0Vv;->u:Ljava/util/concurrent/atomic/AtomicReference;

    move-object v7, v0

    move-object v8, v1

    move v0, v2

    goto :goto_0

    .line 75071
    :cond_3
    monitor-enter p0

    .line 75072
    :try_start_0
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 75073
    monitor-exit p0

    goto :goto_1

    .line 75074
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 75075
    :cond_4
    :try_start_1
    invoke-direct {p0}, LX/0Vv;->j()V

    .line 75076
    new-instance v0, LX/0ed;

    sget-object v1, LX/0ee;->NORMAL:LX/0ee;

    iget-object v2, p0, LX/0Vv;->b:Landroid/content/Context;

    invoke-virtual {p0}, LX/0Vv;->b()Ljava/util/Locale;

    move-result-object v3

    iget-object v4, p0, LX/0Vv;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0WV;

    .line 75077
    sget-object v5, LX/0ec;->ASSET:LX/0ec;

    if-ne p1, v5, :cond_5

    .line 75078
    sget-object v5, LX/0eh;->FBSTR:LX/0eh;

    .line 75079
    :goto_2
    move-object v5, v5

    .line 75080
    iget-object v6, p0, LX/0Vv;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v9, LX/0Vv;->a:LX/0Tn;

    const/4 v10, 0x0

    invoke-interface {v6, v9, v10}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/0ed;-><init>(LX/0ee;Landroid/content/Context;Ljava/util/Locale;LX/0WV;LX/0eh;Ljava/lang/String;)V

    .line 75081
    iput-object v0, p0, LX/0Vv;->m:LX/0ed;

    .line 75082
    iget-object v1, p0, LX/0Vv;->g:LX/0Wk;

    .line 75083
    sget-object v2, LX/0ec;->ASSET:LX/0ec;

    if-ne p1, v2, :cond_6

    iget-object v2, p0, LX/0Vv;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0el;

    :goto_3
    move-object v2, v2

    .line 75084
    invoke-virtual {v1, v0, v2}, LX/0Wk;->a(LX/0ed;LX/0el;)LX/0ep;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 75085
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ep;

    .line 75086
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v3

    .line 75087
    iget-object v2, v1, LX/0ep;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0TD;

    new-instance v4, Lcom/facebook/resources/impl/loading/LanguagePackLoader$1;

    invoke-direct {v4, v1, v3}, Lcom/facebook/resources/impl/loading/LanguagePackLoader$1;-><init>(LX/0ep;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-interface {v2, v4}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 75088
    move-object v9, v3

    .line 75089
    new-instance v1, LX/0et;

    move-object v2, p0

    move-object v3, v7

    move-object v4, p1

    move-object v5, v0

    move-object v6, v8

    invoke-direct/range {v1 .. v6}, LX/0et;-><init>(LX/0Vv;Ljava/util/concurrent/atomic/AtomicReference;LX/0ec;LX/0ed;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v0

    invoke-static {v9, v1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 75090
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 75091
    :cond_5
    :try_start_2
    sget-object v5, LX/0ef;->a:[I

    iget-object v6, p0, LX/0Vv;->k:LX/0Uh;

    const/16 v9, 0x4fb

    invoke-virtual {v6, v9}, LX/0Uh;->a(I)LX/03R;

    move-result-object v6

    invoke-virtual {v6}, LX/03R;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 75092
    sget-object v5, LX/0eh;->FBSTR:LX/0eh;

    goto :goto_2

    .line 75093
    :pswitch_0
    sget-object v5, LX/0eh;->LANGPACK:LX/0eh;

    goto :goto_2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_6
    iget-object v2, p0, LX/0Vv;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0el;

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static declared-synchronized a$redex0(LX/0Vv;LX/0ec;LX/0ed;LX/0lA;Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ec;",
            "LX/0ed;",
            "LX/0lA;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/0lA;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75049
    monitor-enter p0

    :try_start_0
    invoke-virtual {p4, p3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 75050
    iget-object v0, p0, LX/0Vv;->e:LX/0Wn;

    .line 75051
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "fbresources_loading_success"

    invoke-direct {v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 75052
    const-string v1, "source"

    invoke-virtual {p1}, LX/0ec;->getLoggingValue()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v2, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 75053
    const-string v1, "locale"

    invoke-virtual {p2}, LX/0ed;->e()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v2, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 75054
    iget-object v1, v0, LX/0Wn;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 75055
    invoke-virtual {p0}, LX/0Vv;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75056
    iget-object v0, p0, LX/0Vv;->v:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    const v2, 0x144cd1f4

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 75057
    :cond_0
    invoke-static {p0}, LX/0Vv;->k(LX/0Vv;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75058
    monitor-exit p0

    return-void

    .line 75059
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(LX/0QB;)LX/0Vv;
    .locals 15

    .prologue
    .line 75047
    new-instance v0, LX/0Vv;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0W0;->b(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    const/16 v3, 0x3e5

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x10c9

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const-class v5, LX/0Wk;

    invoke-interface {p0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/0Wk;

    const/16 v6, 0x10ca

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x10cb

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/0Wn;->a(LX/0QB;)LX/0Wn;

    move-result-object v8

    check-cast v8, LX/0Wn;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v10

    check-cast v10, LX/0W9;

    invoke-static {p0}, LX/0WA;->a(LX/0QB;)LX/0WA;

    move-result-object v11

    check-cast v11, LX/0WC;

    const/16 v12, 0x12c8

    invoke-static {p0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v13

    check-cast v13, LX/0Uh;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v14

    check-cast v14, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct/range {v0 .. v14}, LX/0Vv;-><init>(Landroid/content/Context;Landroid/content/res/Resources;LX/0Ot;LX/0Ot;LX/0Wk;LX/0Ot;LX/0Ot;LX/0Wn;LX/03V;LX/0W9;LX/0WC;LX/0Or;LX/0Uh;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 75048
    return-object v0
.end method

.method private c(I)Z
    .locals 2

    .prologue
    .line 75045
    iget-object v0, p0, LX/0Vv;->c:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v0

    .line 75046
    iget-object v1, p0, LX/0Vv;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private declared-synchronized j()V
    .locals 1

    .prologue
    .line 75041
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Vv;->v:Lcom/google/common/util/concurrent/SettableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Vv;->v:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0}, LX/0SQ;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75042
    :cond_0
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/0Vv;->v:Lcom/google/common/util/concurrent/SettableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75043
    :cond_1
    monitor-exit p0

    return-void

    .line 75044
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static k(LX/0Vv;)V
    .locals 3

    .prologue
    .line 74994
    iget-boolean v0, p0, LX/0Vv;->q:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0Vv;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74995
    iget-object v0, p0, LX/0Vv;->w:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    const v2, -0x5e8a6e23

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 74996
    :cond_0
    return-void
.end method

.method private l()Ljava/util/Locale;
    .locals 3

    .prologue
    .line 75037
    iget-object v0, p0, LX/0Vv;->y:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->b()Ljava/util/Locale;

    move-result-object v1

    .line 75038
    const-string v0, "fil"

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75039
    new-instance v0, Ljava/util/Locale;

    const-string v2, "tl"

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 75040
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public static m(LX/0Vv;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 75033
    iget-object v0, p0, LX/0Vv;->r:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 75034
    iget-object v0, p0, LX/0Vv;->s:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 75035
    invoke-virtual {p0}, LX/0Vv;->a()V

    .line 75036
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 75032
    iget-object v0, p0, LX/0Vv;->A:LX/0W7;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p0, v0, p1, v1, v2}, LX/0Vv;->a(LX/0Vv;LX/0W7;IILX/0W6;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 75026
    invoke-virtual {p0}, LX/0Vv;->b()Ljava/util/Locale;

    move-result-object v1

    .line 75027
    iget-object v0, p0, LX/0Vv;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0eZ;

    invoke-virtual {p0}, LX/0Vv;->b()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0eZ;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Vv;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0eZ;

    invoke-virtual {v0, v1}, LX/0eZ;->b(Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 75028
    iput-boolean v0, p0, LX/0Vv;->p:Z

    .line 75029
    sget-object v0, LX/0ec;->ASSET:LX/0ec;

    invoke-direct {p0, v0}, LX/0Vv;->a(LX/0ec;)V

    .line 75030
    sget-object v0, LX/0ec;->DOWNLOADED:LX/0ec;

    invoke-direct {p0, v0}, LX/0Vv;->a(LX/0ec;)V

    .line 75031
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 75022
    iget-object v0, p0, LX/0Vv;->n:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    .line 75023
    if-nez v0, :cond_0

    .line 75024
    invoke-direct {p0}, LX/0Vv;->l()Ljava/util/Locale;

    move-result-object v0

    .line 75025
    :cond_0
    return-object v0
.end method

.method public final declared-synchronized d()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75021
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Vv;->v:Lcom/google/common/util/concurrent/SettableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final f()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 75007
    iget-boolean v0, p0, LX/0Vv;->p:Z

    if-nez v0, :cond_1

    .line 75008
    :cond_0
    :goto_0
    return v1

    .line 75009
    :cond_1
    iget-object v0, p0, LX/0Vv;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0eZ;

    invoke-virtual {p0}, LX/0Vv;->b()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0eZ;->b(Ljava/util/Locale;)Z

    move-result v4

    .line 75010
    iget-object v0, p0, LX/0Vv;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0eZ;

    invoke-virtual {p0}, LX/0Vv;->b()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0eZ;->a(Ljava/util/Locale;)Z

    move-result v5

    .line 75011
    iget-object v0, p0, LX/0Vv;->s:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 75012
    :goto_1
    iget-object v3, p0, LX/0Vv;->r:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_3

    move v3, v1

    .line 75013
    :goto_2
    if-eqz v4, :cond_4

    if-eqz v5, :cond_4

    .line 75014
    if-nez v3, :cond_0

    if-nez v0, :cond_0

    move v1, v2

    goto :goto_0

    :cond_2
    move v0, v2

    .line 75015
    goto :goto_1

    :cond_3
    move v3, v2

    .line 75016
    goto :goto_2

    .line 75017
    :cond_4
    if-eqz v4, :cond_5

    move v1, v0

    .line 75018
    goto :goto_0

    .line 75019
    :cond_5
    if-eqz v5, :cond_0

    move v1, v3

    .line 75020
    goto :goto_0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 75003
    invoke-direct {p0}, LX/0Vv;->l()Ljava/util/Locale;

    move-result-object v0

    .line 75004
    iget-boolean v1, p0, LX/0Vv;->q:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0Vv;->n:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 75005
    invoke-static {p0}, LX/0Vv;->m(LX/0Vv;)V

    .line 75006
    :cond_0
    return-void
.end method

.method public final init()V
    .locals 2

    .prologue
    .line 74997
    iget-object v0, p0, LX/0Vv;->y:LX/0W9;

    iget-object v1, p0, LX/0Vv;->o:LX/0Wo;

    invoke-virtual {v0, v1}, LX/0W9;->a(LX/0Wo;)V

    .line 74998
    iget-object v0, p0, LX/0Vv;->n:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p0}, LX/0Vv;->l()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 74999
    invoke-virtual {p0}, LX/0Vv;->a()V

    .line 75000
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Vv;->q:Z

    .line 75001
    invoke-static {p0}, LX/0Vv;->k(LX/0Vv;)V

    .line 75002
    return-void
.end method
