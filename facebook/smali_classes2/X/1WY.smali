.class public final LX/1WY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 268909
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;ZZ)Z
    .locals 2

    .prologue
    .line 268910
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 268911
    invoke-static {v0}, LX/16z;->p(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    if-gtz v1, :cond_1

    invoke-static {v0}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    if-gtz v1, :cond_1

    invoke-static {v0}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    if-gtz v0, :cond_1

    if-eqz p1, :cond_0

    invoke-static {p0}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/0sa;->e(Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->A()I

    move-result v0

    if-lez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
