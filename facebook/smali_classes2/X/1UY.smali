.class public final LX/1UY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/1UZ;

.field public b:F

.field public c:F

.field public d:F

.field public e:F

.field private f:Z


# direct methods
.method public constructor <init>(LX/1UZ;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 256240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256241
    iput v0, p0, LX/1UY;->b:F

    .line 256242
    iput v0, p0, LX/1UY;->c:F

    .line 256243
    iput v0, p0, LX/1UY;->d:F

    .line 256244
    iput v0, p0, LX/1UY;->e:F

    .line 256245
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1UY;->f:Z

    .line 256246
    iput-object p1, p0, LX/1UY;->a:LX/1UZ;

    .line 256247
    return-void
.end method

.method public static a()LX/1UY;
    .locals 2

    .prologue
    .line 256248
    new-instance v0, LX/1UY;

    sget-object v1, LX/1UZ;->DEFAULT:LX/1UZ;

    invoke-direct {v0, v1}, LX/1UY;-><init>(LX/1UZ;)V

    return-object v0
.end method

.method public static b()LX/1UY;
    .locals 2

    .prologue
    .line 256250
    new-instance v0, LX/1UY;

    sget-object v1, LX/1UZ;->DEFAULT_TEXT:LX/1UZ;

    invoke-direct {v0, v1}, LX/1UY;-><init>(LX/1UZ;)V

    return-object v0
.end method

.method public static c()LX/1UY;
    .locals 2

    .prologue
    .line 256249
    new-instance v0, LX/1UY;

    sget-object v1, LX/1UZ;->ZERO:LX/1UZ;

    invoke-direct {v0, v1}, LX/1UY;-><init>(LX/1UZ;)V

    return-object v0
.end method

.method public static d()LX/1UY;
    .locals 2

    .prologue
    .line 256238
    new-instance v0, LX/1UY;

    sget-object v1, LX/1UZ;->DEFAULT_HEADER:LX/1UZ;

    invoke-direct {v0, v1}, LX/1UY;-><init>(LX/1UZ;)V

    return-object v0
.end method

.method public static e()LX/1UY;
    .locals 2

    .prologue
    .line 256239
    new-instance v0, LX/1UY;

    sget-object v1, LX/1UZ;->LEGACY_ZERO:LX/1UZ;

    invoke-direct {v0, v1}, LX/1UY;-><init>(LX/1UZ;)V

    return-object v0
.end method

.method public static f()LX/1UY;
    .locals 2

    .prologue
    .line 256227
    new-instance v0, LX/1UY;

    sget-object v1, LX/1UZ;->STORY_EDGE:LX/1UZ;

    invoke-direct {v0, v1}, LX/1UY;-><init>(LX/1UZ;)V

    return-object v0
.end method

.method public static g()LX/1UY;
    .locals 2

    .prologue
    .line 256228
    new-instance v0, LX/1UY;

    sget-object v1, LX/1UZ;->LEGACY_DEFAULT:LX/1UZ;

    invoke-direct {v0, v1}, LX/1UY;-><init>(LX/1UZ;)V

    return-object v0
.end method


# virtual methods
.method public final a(F)LX/1UY;
    .locals 0

    .prologue
    .line 256229
    iput p1, p0, LX/1UY;->b:F

    .line 256230
    return-object p0
.end method

.method public final b(I)LX/1UY;
    .locals 1

    .prologue
    .line 256231
    mul-int/lit8 v0, p1, 0x4

    int-to-float v0, v0

    .line 256232
    iput v0, p0, LX/1UY;->c:F

    .line 256233
    move-object v0, p0

    .line 256234
    return-object v0
.end method

.method public final h()LX/1UY;
    .locals 1

    .prologue
    .line 256235
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1UY;->f:Z

    .line 256236
    return-object p0
.end method

.method public final i()LX/1Ua;
    .locals 9

    .prologue
    .line 256237
    new-instance v0, LX/1Ua;

    iget-object v1, p0, LX/1UY;->a:LX/1UZ;

    new-instance v2, LX/1Ub;

    iget v3, p0, LX/1UY;->b:F

    iget v4, p0, LX/1UY;->c:F

    iget v5, p0, LX/1UY;->e:F

    new-instance v6, LX/1Ue;

    iget v7, p0, LX/1UY;->d:F

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8}, LX/1Ue;-><init>(FF)V

    invoke-direct {v2, v3, v4, v5, v6}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    iget-boolean v3, p0, LX/1UY;->f:Z

    invoke-direct {v0, v1, v2, v3}, LX/1Ua;-><init>(LX/1UZ;LX/1Ub;Z)V

    return-object v0
.end method
