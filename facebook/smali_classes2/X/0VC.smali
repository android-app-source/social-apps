.class public LX/0VC;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Ljava/util/concurrent/ExecutorService;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67502
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/concurrent/ExecutorService;
    .locals 3

    .prologue
    .line 67503
    sget-object v0, LX/0VC;->a:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_1

    .line 67504
    const-class v1, LX/0VC;

    monitor-enter v1

    .line 67505
    :try_start_0
    sget-object v0, LX/0VC;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 67506
    if-eqz v2, :cond_0

    .line 67507
    :try_start_1
    invoke-static {}, LX/0VD;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, LX/0VC;->a:Ljava/util/concurrent/ExecutorService;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67508
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 67509
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 67510
    :cond_1
    sget-object v0, LX/0VC;->a:Ljava/util/concurrent/ExecutorService;

    return-object v0

    .line 67511
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 67512
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 67513
    invoke-static {}, LX/0VD;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method
