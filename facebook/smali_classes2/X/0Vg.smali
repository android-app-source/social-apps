.class public final LX/0Vg;
.super LX/0Vh;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/Beta;
.end annotation

.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation


# static fields
.field public static final a:LX/0Vj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vj",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68016
    new-instance v0, LX/0Vi;

    invoke-direct {v0}, LX/0Vi;-><init>()V

    sput-object v0, LX/0Vg;->a:LX/0Vj;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 68015
    invoke-direct {p0}, LX/0Vh;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Exception;)LX/52H;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "TODO"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            "X:",
            "Ljava/lang/Exception;",
            ">(TX;)",
            "Lcom/google/common/util/concurrent/CheckedFuture",
            "<TV;TX;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 68013
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68014
    new-instance v0, LX/52H;

    invoke-direct {v0, p0}, LX/52H;-><init>(Ljava/lang/Exception;)V

    return-object v0
.end method

.method public static a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "TODO"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 68012
    new-instance v0, LX/3FF;

    invoke-direct {v0}, LX/3FF;-><init>()V

    return-object v0
.end method

.method public static a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TI;>;",
            "LX/0QK",
            "<-TI;+TO;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 68007
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68008
    new-instance v0, Lcom/google/common/util/concurrent/Futures$ChainingFuture;

    invoke-direct {v0, p0, p1}, Lcom/google/common/util/concurrent/Futures$ChainingFuture;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)V

    .line 68009
    sget-object v1, LX/131;->INSTANCE:LX/131;

    move-object v1, v1

    .line 68010
    invoke-interface {p0, v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 68011
    return-object v0
.end method

.method public static a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TI;>;",
            "LX/0QK",
            "<-TI;+TO;>;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 68003
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68004
    new-instance v0, Lcom/google/common/util/concurrent/Futures$ChainingFuture;

    invoke-direct {v0, p0, p1}, Lcom/google/common/util/concurrent/Futures$ChainingFuture;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)V

    .line 68005
    invoke-static {p2, v0}, LX/0Vg;->a(Ljava/util/concurrent/Executor;LX/0SQ;)Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 68006
    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation build Lcom/google/common/annotations/Beta;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<TV;>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 68002
    new-instance v0, LX/1kg;

    invoke-static {p0}, LX/0Px;->copyOf(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/1kg;-><init>(LX/0Py;Z)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(TV;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 67999
    if-nez p0, :cond_0

    .line 68000
    sget-object v0, LX/0eA;->a:LX/0eA;

    .line 68001
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/0eA;

    invoke-direct {v0, p0}, LX/0eA;-><init>(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 67992
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67993
    new-instance v0, LX/52I;

    invoke-direct {v0, p0}, LX/52I;-><init>(Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static varargs a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation build Lcom/google/common/annotations/Beta;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">([",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<TV;>;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/SafeVarargs;
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 67998
    new-instance v0, LX/1kg;

    invoke-static {p0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/1kg;-><init>(LX/0Py;Z)V

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "TODO"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Future",
            "<TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 68017
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68018
    :try_start_0
    invoke-static {p0}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 68019
    :catch_0
    move-exception v0

    .line 68020
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->b(Ljava/lang/Throwable;)V

    .line 68021
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private static a(Ljava/util/concurrent/Executor;LX/0SQ;)Ljava/util/concurrent/Executor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "LX/0SQ",
            "<*>;)",
            "Ljava/util/concurrent/Executor;"
        }
    .end annotation

    .prologue
    .line 67994
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67995
    sget-object v0, LX/131;->INSTANCE:LX/131;

    move-object v0, v0

    .line 67996
    if-ne p0, v0, :cond_0

    .line 67997
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/1l2;

    invoke-direct {v0, p0, p1}, LX/1l2;-><init>(Ljava/util/concurrent/Executor;LX/0SQ;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;",
            "LX/0TF",
            "<-TV;>;)V"
        }
    .end annotation

    .prologue
    .line 67989
    sget-object v0, LX/131;->INSTANCE:LX/131;

    move-object v0, v0

    .line 67990
    invoke-static {p0, p1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 67991
    return-void
.end method

.method public static a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;",
            "LX/0TF",
            "<-TV;>;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 67985
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67986
    new-instance v0, Lcom/google/common/util/concurrent/Futures$6;

    invoke-direct {v0, p0, p1}, Lcom/google/common/util/concurrent/Futures$6;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 67987
    invoke-interface {p0, v0, p2}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 67988
    return-void
.end method

.method public static b(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "TODO"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 67984
    new-instance v0, LX/52J;

    invoke-direct {v0, p0}, LX/52J;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;)V

    return-object v0
.end method

.method public static b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TI;>;",
            "LX/0Vj",
            "<-TI;+TO;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 67980
    new-instance v0, Lcom/google/common/util/concurrent/Futures$AsyncChainingFuture;

    invoke-direct {v0, p0, p1}, Lcom/google/common/util/concurrent/Futures$AsyncChainingFuture;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;)V

    .line 67981
    sget-object v1, LX/131;->INSTANCE:LX/131;

    move-object v1, v1

    .line 67982
    invoke-interface {p0, v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 67983
    return-object v0
.end method

.method public static b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TI;>;",
            "LX/0Vj",
            "<-TI;+TO;>;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 67976
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67977
    new-instance v0, Lcom/google/common/util/concurrent/Futures$AsyncChainingFuture;

    invoke-direct {v0, p0, p1}, Lcom/google/common/util/concurrent/Futures$AsyncChainingFuture;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;)V

    .line 67978
    invoke-static {p2, v0}, LX/0Vg;->a(Ljava/util/concurrent/Executor;LX/0SQ;)Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 67979
    return-object v0
.end method

.method public static b(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation build Lcom/google/common/annotations/Beta;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<TV;>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 67975
    new-instance v0, LX/1kg;

    invoke-static {p0}, LX/0Px;->copyOf(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/1kg;-><init>(LX/0Py;Z)V

    return-object v0
.end method

.method public static varargs b([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation build Lcom/google/common/annotations/Beta;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">([",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<TV;>;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/SafeVarargs;
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 67974
    new-instance v0, LX/1kg;

    invoke-static {p0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/1kg;-><init>(LX/0Py;Z)V

    return-object v0
.end method

.method private static b(Ljava/lang/Throwable;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "TODO"
    .end annotation

    .prologue
    .line 67971
    instance-of v0, p0, Ljava/lang/Error;

    if-eqz v0, :cond_0

    .line 67972
    new-instance v0, LX/52G;

    check-cast p0, Ljava/lang/Error;

    invoke-direct {v0, p0}, LX/52G;-><init>(Ljava/lang/Error;)V

    throw v0

    .line 67973
    :cond_0
    new-instance v0, LX/52T;

    invoke-direct {v0, p0}, LX/52T;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method
