.class public LX/0V2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0V2;


# instance fields
.field public final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 67290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67291
    iput-object p1, p0, LX/0V2;->a:LX/0Uh;

    .line 67292
    return-void
.end method

.method public static a(LX/0QB;)LX/0V2;
    .locals 4

    .prologue
    .line 67293
    sget-object v0, LX/0V2;->b:LX/0V2;

    if-nez v0, :cond_1

    .line 67294
    const-class v1, LX/0V2;

    monitor-enter v1

    .line 67295
    :try_start_0
    sget-object v0, LX/0V2;->b:LX/0V2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 67296
    if-eqz v2, :cond_0

    .line 67297
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 67298
    new-instance p0, LX/0V2;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/0V2;-><init>(LX/0Uh;)V

    .line 67299
    move-object v0, p0

    .line 67300
    sput-object v0, LX/0V2;->b:LX/0V2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67301
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 67302
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 67303
    :cond_1
    sget-object v0, LX/0V2;->b:LX/0V2;

    return-object v0

    .line 67304
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 67305
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final c()Z
    .locals 3

    .prologue
    .line 67306
    iget-object v0, p0, LX/0V2;->a:LX/0Uh;

    const/16 v1, 0xa1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
