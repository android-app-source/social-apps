.class public final enum LX/10b;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/10b;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/10b;

.field public static final enum LEFT:LX/10b;

.field public static final enum RIGHT:LX/10b;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 169204
    new-instance v0, LX/10b;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v2}, LX/10b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/10b;->LEFT:LX/10b;

    .line 169205
    new-instance v0, LX/10b;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v3}, LX/10b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/10b;->RIGHT:LX/10b;

    .line 169206
    const/4 v0, 0x2

    new-array v0, v0, [LX/10b;

    sget-object v1, LX/10b;->LEFT:LX/10b;

    aput-object v1, v0, v2

    sget-object v1, LX/10b;->RIGHT:LX/10b;

    aput-object v1, v0, v3

    sput-object v0, LX/10b;->$VALUES:[LX/10b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 169208
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/10b;
    .locals 1

    .prologue
    .line 169209
    const-class v0, LX/10b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/10b;

    return-object v0
.end method

.method public static values()[LX/10b;
    .locals 1

    .prologue
    .line 169207
    sget-object v0, LX/10b;->$VALUES:[LX/10b;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/10b;

    return-object v0
.end method
