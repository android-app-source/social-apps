.class public LX/0bg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0bg;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0bi;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperListeners;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0bi;",
            ">;>;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperListeners;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 87155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87156
    iput-object p1, p0, LX/0bg;->a:LX/0Ot;

    .line 87157
    iput-object p2, p0, LX/0bg;->b:LX/0Ot;

    .line 87158
    return-void
.end method

.method public static a(LX/0QB;)LX/0bg;
    .locals 5

    .prologue
    .line 87159
    sget-object v0, LX/0bg;->c:LX/0bg;

    if-nez v0, :cond_1

    .line 87160
    const-class v1, LX/0bg;

    monitor-enter v1

    .line 87161
    :try_start_0
    sget-object v0, LX/0bg;->c:LX/0bg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 87162
    if-eqz v2, :cond_0

    .line 87163
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 87164
    new-instance v3, LX/0bg;

    .line 87165
    new-instance v4, LX/0bh;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-direct {v4, p0}, LX/0bh;-><init>(LX/0QB;)V

    move-object v4, v4

    .line 87166
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-static {v4, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v4

    move-object v4, v4

    .line 87167
    const/16 p0, 0xaca

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/0bg;-><init>(LX/0Ot;LX/0Ot;)V

    .line 87168
    move-object v0, v3

    .line 87169
    sput-object v0, LX/0bg;->c:LX/0bg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87170
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 87171
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 87172
    :cond_1
    sget-object v0, LX/0bg;->c:LX/0bg;

    return-object v0

    .line 87173
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 87174
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 87175
    iget-object v0, p0, LX/0bg;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 87176
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 87177
    :cond_0
    return-void

    .line 87178
    :cond_1
    iget-object v1, p0, LX/0bg;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0a8;

    .line 87179
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0bi;

    .line 87180
    invoke-virtual {v0, v1}, LX/0bi;->a(LX/0a8;)V

    goto :goto_0
.end method
