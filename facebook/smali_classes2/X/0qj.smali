.class public LX/0qj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 148302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148303
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 148304
    iput-object v0, p0, LX/0qj;->a:LX/0Ot;

    .line 148305
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 148306
    iput-object v0, p0, LX/0qj;->b:LX/0Ot;

    .line 148307
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 148308
    iput-object v0, p0, LX/0qj;->c:LX/0Ot;

    .line 148309
    return-void
.end method

.method public static a(LX/0QB;)LX/0qj;
    .locals 1

    .prologue
    .line 148310
    invoke-static {p0}, LX/0qj;->b(LX/0QB;)LX/0qj;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/0qj;
    .locals 4

    .prologue
    .line 148311
    new-instance v0, LX/0qj;

    invoke-direct {v0}, LX/0qj;-><init>()V

    .line 148312
    const/16 v1, 0x2ca

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x24e

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0xac0

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 148313
    iput-object v1, v0, LX/0qj;->a:LX/0Ot;

    iput-object v2, v0, LX/0qj;->b:LX/0Ot;

    iput-object v3, v0, LX/0qj;->c:LX/0Ot;

    .line 148314
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 148315
    iget-object v0, p0, LX/0qj;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v2, 0x396

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 148316
    iget-object v0, p0, LX/0qj;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oz;

    invoke-virtual {v0}, LX/0oz;->e()LX/0p3;

    move-result-object v0

    sget-object v2, LX/0p3;->DEGRADED:LX/0p3;

    invoke-virtual {v0, v2}, LX/0p3;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/0qj;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oz;

    invoke-virtual {v0}, LX/0oz;->b()LX/0p3;

    move-result-object v0

    sget-object v2, LX/0p3;->DEGRADED:LX/0p3;

    invoke-virtual {v0, v2}, LX/0p3;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    move v0, v1

    .line 148317
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/0qj;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    goto :goto_0
.end method
