.class public LX/1c3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1G9;

.field public final b:LX/1c2;

.field public final c:LX/1bw;

.field public final d:Ljava/util/concurrent/ScheduledExecutorService;

.field public final e:LX/0So;

.field public final f:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/1G9;LX/1c2;LX/1bw;Ljava/util/concurrent/ScheduledExecutorService;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 281497
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 281498
    iput-object p1, p0, LX/1c3;->a:LX/1G9;

    .line 281499
    iput-object p2, p0, LX/1c3;->b:LX/1c2;

    .line 281500
    iput-object p3, p0, LX/1c3;->c:LX/1bw;

    .line 281501
    iput-object p4, p0, LX/1c3;->d:Ljava/util/concurrent/ScheduledExecutorService;

    .line 281502
    new-instance v0, LX/1c4;

    invoke-direct {v0, p0}, LX/1c4;-><init>(LX/1c3;)V

    iput-object v0, p0, LX/1c3;->e:LX/0So;

    .line 281503
    iput-object p5, p0, LX/1c3;->f:Landroid/content/res/Resources;

    .line 281504
    return-void
.end method


# virtual methods
.method public final a(LX/1ln;)Landroid/graphics/drawable/Drawable;
    .locals 6

    .prologue
    .line 281505
    instance-of v0, p1, LX/4e7;

    if-eqz v0, :cond_0

    .line 281506
    check-cast p1, LX/4e7;

    invoke-virtual {p1}, LX/4e7;->a()LX/4dO;

    move-result-object v0

    .line 281507
    sget-object v1, LX/4dM;->a:LX/4dM;

    const/4 v5, 0x0

    .line 281508
    iget-object v2, v0, LX/4dO;->a:LX/1GB;

    move-object v2, v2

    .line 281509
    new-instance v3, Landroid/graphics/Rect;

    invoke-interface {v2}, LX/1GB;->a()I

    move-result v4

    invoke-interface {v2}, LX/1GB;->b()I

    move-result v2

    invoke-direct {v3, v5, v5, v4, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 281510
    iget-object v2, p0, LX/1c3;->a:LX/1G9;

    invoke-interface {v2, v0, v3}, LX/1G9;->a(LX/4dO;Landroid/graphics/Rect;)LX/4dG;

    move-result-object v2

    .line 281511
    iget-object v3, p0, LX/1c3;->f:Landroid/content/res/Resources;

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 281512
    iget-object v3, p0, LX/1c3;->b:LX/1c2;

    invoke-interface {v3, v2, v1}, LX/1c2;->a(LX/4dG;LX/4dM;)LX/4dZ;

    move-result-object v5

    .line 281513
    iget-boolean v3, v1, LX/4dM;->e:Z

    if-eqz v3, :cond_1

    .line 281514
    new-instance v3, LX/4da;

    iget-object p1, p0, LX/1c3;->c:LX/1bw;

    invoke-direct {v3, p1, v4}, LX/4da;-><init>(LX/1bw;Landroid/util/DisplayMetrics;)V

    .line 281515
    :goto_0
    new-instance v4, LX/4dF;

    iget-object p1, p0, LX/1c3;->d:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v0, p0, LX/1c3;->e:LX/0So;

    invoke-direct {v4, p1, v5, v3, v0}, LX/4dF;-><init>(Ljava/util/concurrent/ScheduledExecutorService;LX/4dH;LX/4dI;LX/0So;)V

    move-object v2, v4

    .line 281516
    move-object v0, v2

    .line 281517
    return-object v0

    .line 281518
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized image class: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 281519
    :cond_1
    sget-object v3, LX/4db;->a:LX/4db;

    move-object v3, v3

    .line 281520
    goto :goto_0
.end method
