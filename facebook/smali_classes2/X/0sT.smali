.class public LX/0sT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0sT;


# instance fields
.field public final a:LX/0ad;

.field public final b:LX/0W3;


# direct methods
.method public constructor <init>(LX/0ad;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 152016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152017
    iput-object p1, p0, LX/0sT;->a:LX/0ad;

    .line 152018
    iput-object p2, p0, LX/0sT;->b:LX/0W3;

    .line 152019
    return-void
.end method

.method public static a(LX/0QB;)LX/0sT;
    .locals 5

    .prologue
    .line 152021
    sget-object v0, LX/0sT;->c:LX/0sT;

    if-nez v0, :cond_1

    .line 152022
    const-class v1, LX/0sT;

    monitor-enter v1

    .line 152023
    :try_start_0
    sget-object v0, LX/0sT;->c:LX/0sT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 152024
    if-eqz v2, :cond_0

    .line 152025
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 152026
    new-instance p0, LX/0sT;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-direct {p0, v3, v4}, LX/0sT;-><init>(LX/0ad;LX/0W3;)V

    .line 152027
    move-object v0, p0

    .line 152028
    sput-object v0, LX/0sT;->c:LX/0sT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152029
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 152030
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 152031
    :cond_1
    sget-object v0, LX/0sT;->c:LX/0sT;

    return-object v0

    .line 152032
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 152033
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 152020
    iget-object v0, p0, LX/0sT;->a:LX/0ad;

    sget-short v1, LX/1NG;->e:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 152015
    iget-object v0, p0, LX/0sT;->b:LX/0W3;

    sget-wide v2, LX/0X5;->fb:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 4

    .prologue
    .line 152034
    iget-object v0, p0, LX/0sT;->b:LX/0W3;

    sget-wide v2, LX/0X5;->fd:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 3

    .prologue
    .line 152014
    iget-object v0, p0, LX/0sT;->a:LX/0ad;

    sget-short v1, LX/1NG;->m:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 152011
    invoke-virtual {p0}, LX/0sT;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0sT;->a:LX/0ad;

    sget-short v2, LX/1NG;->j:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final j()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 152013
    iget-object v1, p0, LX/0sT;->a:LX/0ad;

    sget-short v2, LX/1NG;->f:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/0sT;->a:LX/0ad;

    sget-short v2, LX/1NG;->c:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final k()Z
    .locals 3

    .prologue
    .line 152012
    iget-object v0, p0, LX/0sT;->a:LX/0ad;

    sget-short v1, LX/1NG;->c:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
