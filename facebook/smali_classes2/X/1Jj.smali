.class public LX/1Jj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:LX/0Sh;

.field private final b:LX/1JV;

.field private final c:LX/1Ji;

.field private final d:LX/1JZ;

.field private final e:LX/1Jk;

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/photos/prefetch/PrefetchParams;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/photos/prefetch/PrefetchParams;",
            ">;"
        }
    .end annotation
.end field

.field public h:I

.field public i:I

.field public volatile j:Z


# direct methods
.method public constructor <init>(LX/0Sh;LX/1JV;LX/1Ji;LX/1JZ;)V
    .locals 1
    .param p2    # LX/1JV;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1Ji;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1JZ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 230345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230346
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Jj;->f:Ljava/util/ArrayList;

    .line 230347
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Jj;->g:Ljava/util/ArrayList;

    .line 230348
    iput-object p1, p0, LX/1Jj;->a:LX/0Sh;

    .line 230349
    iput-object p2, p0, LX/1Jj;->b:LX/1JV;

    .line 230350
    iput-object p3, p0, LX/1Jj;->c:LX/1Ji;

    .line 230351
    iput-object p4, p0, LX/1Jj;->d:LX/1JZ;

    .line 230352
    new-instance v0, LX/1Jk;

    invoke-direct {v0}, LX/1Jk;-><init>()V

    iput-object v0, p0, LX/1Jj;->e:LX/1Jk;

    .line 230353
    return-void
.end method

.method private static a(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 230354
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 230355
    :cond_0
    :goto_0
    return-void

    .line 230356
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 230357
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 230358
    :cond_2
    invoke-interface {p0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public static d(LX/1Jj;)V
    .locals 7

    .prologue
    .line 230359
    iget-boolean v0, p0, LX/1Jj;->j:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1Jj;->c:LX/1Ji;

    .line 230360
    iget-object v1, v0, LX/1Ji;->a:LX/1Jf;

    iget-object v1, v1, LX/1Jf;->a:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 230361
    iget-object v1, v0, LX/1Ji;->a:LX/1Jf;

    iget-object v1, v1, LX/1Jf;->j:LX/1R4;

    invoke-interface {v1}, LX/1R4;->a()I

    move-result v1

    move v0, v1

    .line 230362
    :goto_0
    iget-object v1, p0, LX/1Jj;->f:Ljava/util/ArrayList;

    .line 230363
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 230364
    move-object v1, v1

    .line 230365
    iput-object v1, p0, LX/1Jj;->f:Ljava/util/ArrayList;

    .line 230366
    iget-object v1, p0, LX/1Jj;->g:Ljava/util/ArrayList;

    .line 230367
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 230368
    move-object v1, v1

    .line 230369
    iput-object v1, p0, LX/1Jj;->g:Ljava/util/ArrayList;

    .line 230370
    iget-object v1, p0, LX/1Jj;->b:LX/1JV;

    iget v1, v1, LX/1JV;->c:I

    iget-object v2, p0, LX/1Jj;->b:LX/1JV;

    iget v2, v2, LX/1JV;->a:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v2, p0, LX/1Jj;->b:LX/1JV;

    iget v2, v2, LX/1JV;->d:I

    iget-object v3, p0, LX/1Jj;->b:LX/1JV;

    iget v3, v3, LX/1JV;->b:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 230371
    const/4 v1, 0x1

    :goto_1
    if-gt v1, v2, :cond_5

    .line 230372
    iget v3, p0, LX/1Jj;->h:I

    sub-int/2addr v3, v1

    .line 230373
    iget v4, p0, LX/1Jj;->i:I

    add-int/2addr v4, v1

    .line 230374
    if-ltz v4, :cond_0

    if-ge v4, v0, :cond_0

    .line 230375
    iget-object v5, p0, LX/1Jj;->b:LX/1JV;

    iget v5, v5, LX/1JV;->d:I

    if-gt v1, v5, :cond_3

    .line 230376
    iget-object v5, p0, LX/1Jj;->g:Ljava/util/ArrayList;

    iget-object v6, p0, LX/1Jj;->c:LX/1Ji;

    invoke-virtual {v6, v4}, LX/1Ji;->a(I)Ljava/util/List;

    move-result-object v4

    invoke-static {v5, v4}, LX/1Jj;->a(Ljava/util/List;Ljava/util/List;)V

    .line 230377
    :cond_0
    :goto_2
    if-ltz v3, :cond_1

    if-ge v3, v0, :cond_1

    .line 230378
    iget-object v4, p0, LX/1Jj;->b:LX/1JV;

    iget v4, v4, LX/1JV;->b:I

    if-gt v1, v4, :cond_4

    .line 230379
    iget-object v4, p0, LX/1Jj;->g:Ljava/util/ArrayList;

    iget-object v5, p0, LX/1Jj;->c:LX/1Ji;

    invoke-virtual {v5, v3}, LX/1Ji;->a(I)Ljava/util/List;

    move-result-object v3

    invoke-static {v4, v3}, LX/1Jj;->a(Ljava/util/List;Ljava/util/List;)V

    .line 230380
    :cond_1
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 230381
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 230382
    :cond_3
    iget-object v5, p0, LX/1Jj;->b:LX/1JV;

    iget v5, v5, LX/1JV;->c:I

    if-gt v1, v5, :cond_0

    .line 230383
    iget-object v5, p0, LX/1Jj;->f:Ljava/util/ArrayList;

    iget-object v6, p0, LX/1Jj;->c:LX/1Ji;

    invoke-virtual {v6, v4}, LX/1Ji;->a(I)Ljava/util/List;

    move-result-object v4

    invoke-static {v5, v4}, LX/1Jj;->a(Ljava/util/List;Ljava/util/List;)V

    goto :goto_2

    .line 230384
    :cond_4
    iget-object v4, p0, LX/1Jj;->b:LX/1JV;

    iget v4, v4, LX/1JV;->a:I

    if-gt v1, v4, :cond_1

    .line 230385
    iget-object v4, p0, LX/1Jj;->f:Ljava/util/ArrayList;

    iget-object v5, p0, LX/1Jj;->c:LX/1Ji;

    invoke-virtual {v5, v3}, LX/1Ji;->a(I)Ljava/util/List;

    move-result-object v3

    invoke-static {v4, v3}, LX/1Jj;->a(Ljava/util/List;Ljava/util/List;)V

    goto :goto_3

    .line 230386
    :cond_5
    iget-object v0, p0, LX/1Jj;->d:LX/1JZ;

    iget-object v1, p0, LX/1Jj;->g:Ljava/util/ArrayList;

    iget-object v2, p0, LX/1Jj;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, LX/1JZ;->b(Ljava/util/List;Ljava/util/List;)V

    .line 230387
    return-void
.end method
