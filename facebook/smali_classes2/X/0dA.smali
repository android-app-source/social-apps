.class public LX/0dA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;


# instance fields
.field public final c:LX/03V;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0dC;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0WV;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final h:LX/0dD;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 90030
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "app_version_name_current"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dA;->a:LX/0Tn;

    .line 90031
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "app_version_name_prev"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dA;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0Or;LX/0dC;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0dD;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/0Ot",
            "<",
            "LX/0WV;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0dD;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 90057
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90058
    iput-object p1, p0, LX/0dA;->c:LX/03V;

    .line 90059
    iput-object p2, p0, LX/0dA;->d:LX/0Or;

    .line 90060
    iput-object p3, p0, LX/0dA;->e:LX/0dC;

    .line 90061
    iput-object p4, p0, LX/0dA;->f:LX/0Ot;

    .line 90062
    iput-object p5, p0, LX/0dA;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 90063
    iput-object p6, p0, LX/0dA;->h:LX/0dD;

    .line 90064
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 5

    .prologue
    .line 90032
    iget-object v0, p0, LX/0dA;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 90033
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 90034
    iget-object v1, p0, LX/0dA;->c:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->c(Ljava/lang/String;)V

    .line 90035
    :cond_0
    iget-object v0, p0, LX/0dA;->e:LX/0dC;

    invoke-virtual {v0}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v0

    .line 90036
    if-eqz v0, :cond_1

    .line 90037
    iget-object v1, p0, LX/0dA;->c:LX/03V;

    const-string v2, "marauder_device_id"

    invoke-virtual {v1, v2, v0}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 90038
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ErrorReporter DEVICE_ID_KEY set to: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90039
    :cond_1
    iget-object v0, p0, LX/0dA;->c:LX/03V;

    const-string v1, "installed_fb_apks"

    iget-object v2, p0, LX/0dA;->h:LX/0dD;

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;LX/0VI;)V

    .line 90040
    iget-object v0, p0, LX/0dA;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WV;

    invoke-virtual {v0}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v0

    .line 90041
    if-nez v0, :cond_2

    const-string v0, "unknown"

    .line 90042
    :cond_2
    iget-object v1, p0, LX/0dA;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0dA;->a:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 90043
    iget-object v1, p0, LX/0dA;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0dA;->a:LX/0Tn;

    const-string v3, "DUMMY"

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 90044
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 90045
    iget-object v0, p0, LX/0dA;->c:LX/03V;

    const-string v1, "app_version_name_prev"

    iget-object v2, p0, LX/0dA;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0dA;->b:LX/0Tn;

    const-string v4, "unknown"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 90046
    :goto_0
    return-void

    .line 90047
    :cond_3
    iget-object v2, p0, LX/0dA;->c:LX/03V;

    const-string v3, "app_version_name_prev"

    invoke-virtual {v2, v3, v1}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 90048
    iget-object v2, p0, LX/0dA;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    .line 90049
    sget-object v3, LX/0dA;->b:LX/0Tn;

    invoke-interface {v2, v3, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 90050
    sget-object v1, LX/0dA;->a:LX/0Tn;

    invoke-interface {v2, v1, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 90051
    invoke-interface {v2}, LX/0hN;->commit()V

    goto :goto_0

    .line 90052
    :cond_4
    iget-object v1, p0, LX/0dA;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 90053
    sget-object v2, LX/0dA;->b:LX/0Tn;

    const-string v3, "unknown"

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 90054
    sget-object v2, LX/0dA;->a:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 90055
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 90056
    iget-object v0, p0, LX/0dA;->c:LX/03V;

    const-string v1, "app_version_name_prev"

    const-string v2, "unknown"

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
