.class public final LX/0Yb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/0Xk;

.field private final b:LX/0Yd;

.field private final c:Landroid/content/IntentFilter;

.field private final d:Landroid/os/Handler;

.field private e:Z


# direct methods
.method public constructor <init>(LX/0Xk;Ljava/util/Map;Landroid/content/IntentFilter;Landroid/os/Handler;)V
    .locals 2
    .param p3    # Landroid/content/IntentFilter;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/os/Handler;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0YZ;",
            ">;",
            "Landroid/content/IntentFilter;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 81744
    iput-object p1, p0, LX/0Yb;->a:LX/0Xk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81745
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yb;->e:Z

    .line 81746
    new-instance v0, LX/0Yc;

    iget-object v1, p1, LX/0Xk;->a:LX/0Sk;

    invoke-direct {v0, p0, p2, v1, p1}, LX/0Yc;-><init>(LX/0Yb;Ljava/util/Map;LX/0Sk;LX/0Xk;)V

    iput-object v0, p0, LX/0Yb;->b:LX/0Yd;

    .line 81747
    iput-object p3, p0, LX/0Yb;->c:Landroid/content/IntentFilter;

    .line 81748
    iput-object p4, p0, LX/0Yb;->d:Landroid/os/Handler;

    .line 81749
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 81750
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0Yb;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 4

    .prologue
    .line 81751
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0Yb;->e:Z

    if-eqz v0, :cond_0

    .line 81752
    iget-object v0, p0, LX/0Yb;->a:LX/0Xk;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Called registerBroadcastReceiver twice."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81753
    :goto_0
    monitor-exit p0

    return-void

    .line 81754
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0Yb;->c:Landroid/content/IntentFilter;

    .line 81755
    if-nez v0, :cond_1

    .line 81756
    iget-object v0, p0, LX/0Yb;->b:LX/0Yd;

    invoke-virtual {v0}, LX/0Yd;->a()Landroid/content/IntentFilter;

    move-result-object v0

    .line 81757
    :cond_1
    iget-object v1, p0, LX/0Yb;->a:LX/0Xk;

    iget-object v2, p0, LX/0Yb;->b:LX/0Yd;

    iget-object v3, p0, LX/0Yb;->d:Landroid/os/Handler;

    invoke-virtual {v1, v2, v0, v3}, LX/0Xk;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Landroid/os/Handler;)V

    .line 81758
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Yb;->e:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 81759
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 81760
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0Yb;->e:Z

    if-eqz v0, :cond_0

    .line 81761
    iget-object v0, p0, LX/0Yb;->a:LX/0Xk;

    iget-object v1, p0, LX/0Yb;->b:LX/0Yd;

    invoke-virtual {v0, v1}, LX/0Xk;->a(Landroid/content/BroadcastReceiver;)V

    .line 81762
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yb;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81763
    :cond_0
    monitor-exit p0

    return-void

    .line 81764
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
