.class public LX/1nz;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1o1;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/1nz;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1o0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 317927
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1nz;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1o0;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 317924
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 317925
    iput-object p1, p0, LX/1nz;->b:LX/0Ot;

    .line 317926
    return-void
.end method

.method public static a(LX/0QB;)LX/1nz;
    .locals 4

    .prologue
    .line 317911
    sget-object v0, LX/1nz;->c:LX/1nz;

    if-nez v0, :cond_1

    .line 317912
    const-class v1, LX/1nz;

    monitor-enter v1

    .line 317913
    :try_start_0
    sget-object v0, LX/1nz;->c:LX/1nz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 317914
    if-eqz v2, :cond_0

    .line 317915
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 317916
    new-instance v3, LX/1nz;

    const/16 p0, 0x3a7

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1nz;-><init>(LX/0Ot;)V

    .line 317917
    move-object v0, v3

    .line 317918
    sput-object v0, LX/1nz;->c:LX/1nz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 317919
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 317920
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 317921
    :cond_1
    sget-object v0, LX/1nz;->c:LX/1nz;

    return-object v0

    .line 317922
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 317923
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 317909
    invoke-static {}, LX/1dS;->b()V

    .line 317910
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4379f5e3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 317905
    check-cast p6, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    .line 317906
    iget-object v1, p0, LX/1nz;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    iget v1, p6, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->a:F

    .line 317907
    invoke-static {p3, p4, v1, p5}, LX/1oC;->a(IIFLX/1no;)V

    .line 317908
    const/16 v1, 0x1f

    const v2, 0x44884cee

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(LX/1De;LX/1Dg;LX/1X1;)V
    .locals 7

    .prologue
    .line 317886
    check-cast p3, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    .line 317887
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 317888
    iget-object v0, p0, LX/1nz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1o0;

    iget-object v2, p3, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->b:Landroid/net/Uri;

    iget-object v3, p3, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v4, p3, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->d:Ljava/lang/String;

    move-object v1, p2

    .line 317889
    invoke-static {v2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v6

    invoke-static {v4}, LX/1ny;->a(Ljava/lang/String;)LX/1ny;

    move-result-object p0

    .line 317890
    iput-object p0, v6, LX/1bX;->m:LX/1ny;

    .line 317891
    move-object v6, v6

    .line 317892
    new-instance p0, LX/1o9;

    invoke-virtual {v1}, LX/1Dg;->c()I

    move-result p1

    invoke-virtual {v1}, LX/1Dg;->d()I

    move-result p2

    invoke-direct {p0, p1, p2}, LX/1o9;-><init>(II)V

    .line 317893
    iput-object p0, v6, LX/1bX;->c:LX/1o9;

    .line 317894
    move-object v6, v6

    .line 317895
    invoke-static {}, LX/1bd;->c()LX/1bd;

    move-result-object p0

    .line 317896
    iput-object p0, v6, LX/1bX;->d:LX/1bd;

    .line 317897
    move-object v6, v6

    .line 317898
    invoke-virtual {v6}, LX/1bX;->n()LX/1bf;

    move-result-object p0

    .line 317899
    iget-object v6, v0, LX/1o0;->h:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1Ad;

    invoke-virtual {v6, p0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v6

    check-cast v6, LX/1Ad;

    invoke-virtual {v6, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v6

    invoke-virtual {v6}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v6

    .line 317900
    iput-object v6, v5, LX/1np;->a:Ljava/lang/Object;

    .line 317901
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 317902
    check-cast v0, LX/1aZ;

    iput-object v0, p3, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->e:LX/1aZ;

    .line 317903
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 317904
    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 317882
    iget-object v0, p0, LX/1nz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 317883
    new-instance v0, LX/1oH;

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-static {p0}, LX/1Uo;->a(Landroid/content/res/Resources;)LX/1Uo;

    move-result-object p0

    invoke-virtual {p0}, LX/1Uo;->u()LX/1af;

    move-result-object p0

    invoke-direct {v0, p0}, LX/1oH;-><init>(LX/1af;)V

    .line 317884
    new-instance p0, LX/1oI;

    invoke-direct {p0, p1, v0}, LX/1oI;-><init>(Landroid/content/Context;LX/1aY;)V

    move-object v0, p0

    .line 317885
    return-object v0
.end method

.method public final c(LX/1De;)LX/1o1;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 317874
    new-instance v1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    invoke-direct {v1, p0}, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;-><init>(LX/1nz;)V

    .line 317875
    sget-object v2, LX/1nz;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1o1;

    .line 317876
    if-nez v2, :cond_0

    .line 317877
    new-instance v2, LX/1o1;

    invoke-direct {v2}, LX/1o1;-><init>()V

    .line 317878
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/1o1;->a$redex0(LX/1o1;LX/1De;IILcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;)V

    .line 317879
    move-object v1, v2

    .line 317880
    move-object v0, v1

    .line 317881
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 317873
    const/4 v0, 0x1

    return v0
.end method

.method public final c(LX/1X1;LX/1X1;)Z
    .locals 13

    .prologue
    .line 317928
    check-cast p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    .line 317929
    check-cast p2, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    .line 317930
    iget-object v0, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->j:LX/4Ab;

    iget-object v1, p2, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->j:LX/4Ab;

    invoke-static {v0, v1}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v0

    .line 317931
    iget-object v1, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->k:Landroid/graphics/ColorFilter;

    iget-object v2, p2, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->k:Landroid/graphics/ColorFilter;

    invoke-static {v1, v2}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v1

    .line 317932
    iget-object v2, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->l:LX/1dc;

    iget-object v3, p2, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->l:LX/1dc;

    invoke-static {v2, v3}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v2

    .line 317933
    iget-object v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->f:LX/1Up;

    iget-object v4, p2, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->f:LX/1Up;

    invoke-static {v3, v4}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v3

    .line 317934
    iget-object v4, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->m:LX/1dc;

    iget-object v5, p2, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->m:LX/1dc;

    invoke-static {v4, v5}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v4

    .line 317935
    iget-object v5, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->n:LX/1Up;

    iget-object v6, p2, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->n:LX/1Up;

    invoke-static {v5, v6}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v5

    .line 317936
    iget-object v6, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->o:LX/1dc;

    iget-object v7, p2, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->o:LX/1dc;

    invoke-static {v6, v7}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v6

    .line 317937
    iget-object v7, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->p:LX/1Up;

    iget-object v8, p2, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->p:LX/1Up;

    invoke-static {v7, v8}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v7

    .line 317938
    iget-object v8, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->r:LX/1dc;

    iget-object v9, p2, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->r:LX/1dc;

    invoke-static {v8, v9}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v8

    .line 317939
    iget-object v9, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->s:LX/1Up;

    iget-object v10, p2, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->s:LX/1Up;

    invoke-static {v9, v10}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v9

    .line 317940
    iget v10, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->t:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iget v11, p2, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->t:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-static {v10, v11}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v10

    .line 317941
    iget-object v11, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->u:LX/1dc;

    iget-object v12, p2, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->u:LX/1dc;

    invoke-static {v11, v12}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v11

    .line 317942
    iget-object v12, p0, LX/1nz;->b:LX/0Ot;

    invoke-interface {v12}, LX/0Ot;->get()Ljava/lang/Object;

    const/4 p1, 0x1

    .line 317943
    iget-object v12, v0, LX/3lz;->b:Ljava/lang/Object;

    move-object v12, v12

    .line 317944
    iget-object p0, v0, LX/3lz;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 317945
    invoke-static {v12, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_0

    move v12, p1

    .line 317946
    :goto_0
    move v12, v12

    .line 317947
    invoke-static {v0}, LX/1cy;->a(LX/3lz;)V

    .line 317948
    invoke-static {v1}, LX/1cy;->a(LX/3lz;)V

    .line 317949
    invoke-static {v2}, LX/1cy;->a(LX/3lz;)V

    .line 317950
    invoke-static {v3}, LX/1cy;->a(LX/3lz;)V

    .line 317951
    invoke-static {v4}, LX/1cy;->a(LX/3lz;)V

    .line 317952
    invoke-static {v5}, LX/1cy;->a(LX/3lz;)V

    .line 317953
    invoke-static {v6}, LX/1cy;->a(LX/3lz;)V

    .line 317954
    invoke-static {v7}, LX/1cy;->a(LX/3lz;)V

    .line 317955
    invoke-static {v8}, LX/1cy;->a(LX/3lz;)V

    .line 317956
    invoke-static {v9}, LX/1cy;->a(LX/3lz;)V

    .line 317957
    invoke-static {v10}, LX/1cy;->a(LX/3lz;)V

    .line 317958
    invoke-static {v11}, LX/1cy;->a(LX/3lz;)V

    .line 317959
    return v12

    .line 317960
    :cond_0
    iget-object v12, v1, LX/3lz;->b:Ljava/lang/Object;

    move-object v12, v12

    .line 317961
    iget-object p0, v1, LX/3lz;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 317962
    invoke-static {v12, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    move v12, p1

    .line 317963
    goto :goto_0

    .line 317964
    :cond_1
    iget-object v12, v2, LX/3lz;->a:Ljava/lang/Object;

    move-object v12, v12

    .line 317965
    check-cast v12, LX/1dc;

    .line 317966
    iget-object p0, v2, LX/3lz;->b:Ljava/lang/Object;

    move-object p0, p0

    .line 317967
    check-cast p0, LX/1dc;

    invoke-static {v12, p0}, LX/1dc;->a(LX/1dc;LX/1dc;)Z

    move-result v12

    if-eqz v12, :cond_2

    move v12, p1

    .line 317968
    goto :goto_0

    .line 317969
    :cond_2
    iget-object v12, v3, LX/3lz;->b:Ljava/lang/Object;

    move-object v12, v12

    .line 317970
    iget-object p0, v3, LX/3lz;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 317971
    if-eq v12, p0, :cond_3

    move v12, p1

    .line 317972
    goto :goto_0

    .line 317973
    :cond_3
    iget-object v12, v4, LX/3lz;->a:Ljava/lang/Object;

    move-object v12, v12

    .line 317974
    check-cast v12, LX/1dc;

    .line 317975
    iget-object p0, v4, LX/3lz;->b:Ljava/lang/Object;

    move-object p0, p0

    .line 317976
    check-cast p0, LX/1dc;

    invoke-static {v12, p0}, LX/1dc;->a(LX/1dc;LX/1dc;)Z

    move-result v12

    if-eqz v12, :cond_4

    move v12, p1

    .line 317977
    goto :goto_0

    .line 317978
    :cond_4
    iget-object v12, v5, LX/3lz;->b:Ljava/lang/Object;

    move-object v12, v12

    .line 317979
    iget-object p0, v5, LX/3lz;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 317980
    if-eq v12, p0, :cond_5

    move v12, p1

    .line 317981
    goto :goto_0

    .line 317982
    :cond_5
    iget-object v12, v6, LX/3lz;->a:Ljava/lang/Object;

    move-object v12, v12

    .line 317983
    check-cast v12, LX/1dc;

    .line 317984
    iget-object p0, v6, LX/3lz;->b:Ljava/lang/Object;

    move-object p0, p0

    .line 317985
    check-cast p0, LX/1dc;

    invoke-static {v12, p0}, LX/1dc;->a(LX/1dc;LX/1dc;)Z

    move-result v12

    if-eqz v12, :cond_6

    move v12, p1

    .line 317986
    goto :goto_0

    .line 317987
    :cond_6
    iget-object v12, v7, LX/3lz;->b:Ljava/lang/Object;

    move-object v12, v12

    .line 317988
    iget-object p0, v7, LX/3lz;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 317989
    if-eq v12, p0, :cond_7

    move v12, p1

    .line 317990
    goto/16 :goto_0

    .line 317991
    :cond_7
    iget-object v12, v8, LX/3lz;->a:Ljava/lang/Object;

    move-object v12, v12

    .line 317992
    check-cast v12, LX/1dc;

    .line 317993
    iget-object p0, v8, LX/3lz;->b:Ljava/lang/Object;

    move-object p0, p0

    .line 317994
    check-cast p0, LX/1dc;

    invoke-static {v12, p0}, LX/1dc;->a(LX/1dc;LX/1dc;)Z

    move-result v12

    if-eqz v12, :cond_8

    move v12, p1

    .line 317995
    goto/16 :goto_0

    .line 317996
    :cond_8
    iget-object v12, v9, LX/3lz;->b:Ljava/lang/Object;

    move-object v12, v12

    .line 317997
    iget-object p0, v9, LX/3lz;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 317998
    if-eq v12, p0, :cond_9

    move v12, p1

    .line 317999
    goto/16 :goto_0

    .line 318000
    :cond_9
    iget-object v12, v10, LX/3lz;->b:Ljava/lang/Object;

    move-object v12, v12

    .line 318001
    iget-object p0, v10, LX/3lz;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 318002
    invoke-static {v12, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_a

    move v12, p1

    .line 318003
    goto/16 :goto_0

    .line 318004
    :cond_a
    iget-object v12, v11, LX/3lz;->a:Ljava/lang/Object;

    move-object v12, v12

    .line 318005
    check-cast v12, LX/1dc;

    .line 318006
    iget-object p0, v11, LX/3lz;->b:Ljava/lang/Object;

    move-object p0, p0

    .line 318007
    check-cast p0, LX/1dc;

    invoke-static {v12, p0}, LX/1dc;->a(LX/1dc;LX/1dc;)Z

    move-result v12

    if-eqz v12, :cond_b

    move v12, p1

    .line 318008
    goto/16 :goto_0

    .line 318009
    :cond_b
    const/4 v12, 0x0

    goto/16 :goto_0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 16

    .prologue
    .line 317870
    check-cast p3, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    .line 317871
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1nz;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-object/from16 v2, p2

    check-cast v2, LX/1oI;

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->j:LX/4Ab;

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->k:Landroid/graphics/ColorFilter;

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->l:LX/1dc;

    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->f:LX/1Up;

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->m:LX/1dc;

    move-object/from16 v0, p3

    iget-object v8, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->n:LX/1Up;

    move-object/from16 v0, p3

    iget-object v9, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->o:LX/1dc;

    move-object/from16 v0, p3

    iget-object v10, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->p:LX/1Up;

    move-object/from16 v0, p3

    iget v11, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->q:I

    move-object/from16 v0, p3

    iget-object v12, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->r:LX/1dc;

    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->s:LX/1Up;

    move-object/from16 v0, p3

    iget v14, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->t:I

    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->u:LX/1dc;

    move-object/from16 v1, p1

    invoke-static/range {v1 .. v15}, LX/1o0;->a(LX/1De;LX/1oI;LX/4Ab;Landroid/graphics/ColorFilter;LX/1dc;LX/1Up;LX/1dc;LX/1Up;LX/1dc;LX/1Up;ILX/1dc;LX/1Up;ILX/1dc;)V

    .line 317872
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 317869
    sget-object v0, LX/1mv;->DRAWABLE:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 317866
    iget-object v0, p0, LX/1nz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, LX/1oI;

    .line 317867
    invoke-virtual {p2}, LX/1oI;->c()V

    .line 317868
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 6

    .prologue
    .line 317854
    check-cast p3, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    .line 317855
    iget-object v0, p0, LX/1nz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-object v0, p2

    check-cast v0, LX/1oI;

    iget-object v1, p3, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->f:LX/1Up;

    iget-object v2, p3, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->g:Landroid/graphics/PointF;

    iget-object v3, p3, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->h:Landroid/graphics/PointF;

    iget-object v4, p3, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->i:LX/1Up;

    iget-object v5, p3, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->e:LX/1aZ;

    .line 317856
    invoke-virtual {v0}, LX/1oI;->d()LX/1aY;

    move-result-object p0

    check-cast p0, LX/1oH;

    .line 317857
    if-eqz v3, :cond_2

    .line 317858
    sget-object p1, LX/1Up;->h:LX/1Up;

    invoke-virtual {p0, p1}, LX/1oH;->a(LX/1Up;)V

    .line 317859
    invoke-virtual {p0, v3}, LX/1oH;->b(Landroid/graphics/PointF;)V

    .line 317860
    :cond_0
    :goto_0
    sget-object p1, LX/1Up;->h:LX/1Up;

    if-ne v1, p1, :cond_1

    .line 317861
    invoke-virtual {p0, v2}, LX/1oH;->a(Landroid/graphics/PointF;)V

    .line 317862
    :cond_1
    invoke-virtual {v0, v5}, LX/1oI;->a(LX/1aZ;)V

    .line 317863
    return-void

    .line 317864
    :cond_2
    if-eqz v4, :cond_0

    .line 317865
    invoke-virtual {p0, v4}, LX/1oH;->a(LX/1Up;)V

    goto :goto_0
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 317851
    iget-object v0, p0, LX/1nz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, LX/1oI;

    .line 317852
    invoke-virtual {p2}, LX/1oI;->e()V

    .line 317853
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 317850
    const/4 v0, 0x1

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 317849
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 317848
    const/16 v0, 0xf

    return v0
.end method
