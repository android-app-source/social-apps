.class public LX/0pP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pQ;


# static fields
.field public static final A:LX/0Tn;

.field public static final B:LX/0Tn;

.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;

.field public static final o:LX/0Tn;

.field public static final p:LX/0Tn;

.field public static final q:LX/0Tn;

.field public static final r:LX/0Tn;

.field public static final s:LX/0Tn;

.field public static final t:LX/0Tn;

.field public static final u:LX/0Tn;

.field public static final v:LX/0Tn;

.field public static final w:LX/0Tn;

.field public static final x:LX/0Tn;

.field public static final y:LX/0Tn;

.field public static final z:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 144352
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "feed/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 144353
    sput-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "permalink_pref"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->b:LX/0Tn;

    .line 144354
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "story_ranking_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->c:LX/0Tn;

    .line 144355
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "last_hit_previously_ranked_stories_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->d:LX/0Tn;

    .line 144356
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "last_head_fetch_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->e:LX/0Tn;

    .line 144357
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "last_interaction_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->f:LX/0Tn;

    .line 144358
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "clear_stories_cache"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->g:LX/0Tn;

    .line 144359
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "always_do_fresh_fetch_on_cold_start"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->h:LX/0Tn;

    .line 144360
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "topics_prediction_visual_feedback_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->i:LX/0Tn;

    .line 144361
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "vpvd_visual_feedback_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->j:LX/0Tn;

    .line 144362
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "spam_reporting"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->k:LX/0Tn;

    .line 144363
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "enable_place_save_nux_history"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->l:LX/0Tn;

    .line 144364
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "follow_videos_nux_history"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->m:LX/0Tn;

    .line 144365
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "page_story_admin_attr_nux_history"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->n:LX/0Tn;

    .line 144366
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "privacy_editing"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->o:LX/0Tn;

    .line 144367
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "demo_ad_invalidation"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->p:LX/0Tn;

    .line 144368
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "bookmarks/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 144369
    sput-object v0, LX/0pP;->q:LX/0Tn;

    const-string v1, "newsfeed_filter_type_key"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->r:LX/0Tn;

    .line 144370
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "music_preview_nux_history"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->s:LX/0Tn;

    .line 144371
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "debug_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->t:LX/0Tn;

    .line 144372
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "impression_logging_visual_feedback_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->u:LX/0Tn;

    .line 144373
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "time_since_last_photo_uploaded"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->v:LX/0Tn;

    .line 144374
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "is_flat_buffer_corrupt"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->w:LX/0Tn;

    .line 144375
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "time_last_adchaining_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->x:LX/0Tn;

    .line 144376
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "show_comment_cache_state"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->y:LX/0Tn;

    .line 144377
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "inline_feed_survey_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->z:LX/0Tn;

    .line 144378
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "fresh_feed_override"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->A:LX/0Tn;

    .line 144379
    sget-object v0, LX/0pP;->a:LX/0Tn;

    const-string v1, "rapid_feedback_survey"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0pP;->B:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 144381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144382
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144380
    sget-object v0, LX/0pP;->l:LX/0Tn;

    sget-object v1, LX/0pP;->n:LX/0Tn;

    sget-object v2, LX/0pP;->s:LX/0Tn;

    sget-object v3, LX/0pP;->B:LX/0Tn;

    invoke-static {v0, v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
