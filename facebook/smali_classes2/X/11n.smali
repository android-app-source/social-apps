.class public LX/11n;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/11o;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/sequencelogger/SequenceDefinition;",
        ">",
        "Ljava/lang/Object;",
        "LX/11o",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 173186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/11o;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173187
    return-object p0
.end method

.method public final a(Ljava/lang/String;LX/0P1;J)LX/11o;
    .locals 0
    .param p2    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173188
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/11o;
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173189
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;J)LX/11o;
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173190
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/0P1;)LX/11o;
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173179
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/0P1;J)LX/11o;
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173191
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 173192
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 173193
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Ljava/lang/String;)LX/11o;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173184
    return-object p0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)LX/11o;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173185
    return-object p0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;LX/0P1;)LX/11o;
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173183
    return-object p0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;LX/0P1;J)LX/11o;
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173182
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/11o;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173181
    return-object p0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;LX/0P1;)LX/11o;
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173180
    return-object p0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;LX/0P1;J)LX/11o;
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173178
    return-object p0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 173177
    const/4 v0, 0x0

    return v0
.end method

.method public final d(Ljava/lang/String;)LX/11o;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173176
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/11o;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173175
    return-object p0
.end method

.method public final f(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 173174
    const/4 v0, 0x0

    return v0
.end method
