.class public LX/111;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile h:LX/111;


# instance fields
.field public final b:LX/0Sg;

.field public final c:LX/0Sh;

.field public final d:Ljava/util/concurrent/ScheduledExecutorService;

.field public e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui-thread"
    .end annotation
.end field

.field public f:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui-thread"
    .end annotation
.end field

.field public g:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui-thread"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 169709
    const-class v0, LX/111;

    sput-object v0, LX/111;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Sg;LX/0Sh;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 169710
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169711
    iput-object p1, p0, LX/111;->b:LX/0Sg;

    .line 169712
    iput-object p2, p0, LX/111;->c:LX/0Sh;

    .line 169713
    iput-object p3, p0, LX/111;->d:Ljava/util/concurrent/ScheduledExecutorService;

    .line 169714
    return-void
.end method

.method public static a(LX/0QB;)LX/111;
    .locals 6

    .prologue
    .line 169696
    sget-object v0, LX/111;->h:LX/111;

    if-nez v0, :cond_1

    .line 169697
    const-class v1, LX/111;

    monitor-enter v1

    .line 169698
    :try_start_0
    sget-object v0, LX/111;->h:LX/111;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 169699
    if-eqz v2, :cond_0

    .line 169700
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 169701
    new-instance p0, LX/111;

    invoke-static {v0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v3

    check-cast v3, LX/0Sg;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {p0, v3, v4, v5}, LX/111;-><init>(LX/0Sg;LX/0Sh;Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 169702
    move-object v0, p0

    .line 169703
    sput-object v0, LX/111;->h:LX/111;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169704
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 169705
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 169706
    :cond_1
    sget-object v0, LX/111;->h:LX/111;

    return-object v0

    .line 169707
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 169708
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/111;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 169693
    iget-object v0, p0, LX/111;->e:Ljava/lang/Class;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, LX/111;->f:Lcom/google/common/util/concurrent/SettableFuture;

    if-nez v3, :cond_1

    move v3, v1

    :goto_1
    if-ne v0, v3, :cond_2

    :goto_2
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 169694
    return-void

    :cond_0
    move v0, v2

    .line 169695
    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public final b(Ljava/lang/Class;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 169681
    iget-object v0, p0, LX/111;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 169682
    invoke-static {p0}, LX/111;->a(LX/111;)V

    .line 169683
    iget-object v0, p0, LX/111;->g:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 169684
    iget-object v0, p0, LX/111;->g:Ljava/util/concurrent/Future;

    invoke-interface {v0, v3}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 169685
    iput-object v4, p0, LX/111;->g:Ljava/util/concurrent/Future;

    .line 169686
    :cond_0
    iget-object v0, p0, LX/111;->f:Lcom/google/common/util/concurrent/SettableFuture;

    if-nez v0, :cond_1

    .line 169687
    :goto_0
    return-void

    .line 169688
    :cond_1
    iget-object v0, p0, LX/111;->f:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const v2, 0x6b1f3a0b

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 169689
    iput-object v4, p0, LX/111;->f:Lcom/google/common/util/concurrent/SettableFuture;

    .line 169690
    iget-object v0, p0, LX/111;->e:Ljava/lang/Class;

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 169691
    sget-object v0, LX/111;->a:Ljava/lang/Class;

    const-string v1, "stopTrackingActivityLaunch called for %s while tracking %s."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    iget-object v3, p0, LX/111;->e:Ljava/lang/Class;

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 169692
    :cond_2
    iput-object v4, p0, LX/111;->e:Ljava/lang/Class;

    goto :goto_0
.end method
