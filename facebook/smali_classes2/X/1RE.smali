.class public LX/1RE;
.super LX/1RF;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1PW;",
        ">",
        "LX/1RF",
        "<TE;>;",
        "Lcom/facebook/multirow/api/RowViewData;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<TP;TS;TE;>;"
        }
    .end annotation
.end field

.field public final b:LX/1RE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1RE",
            "<***>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:LX/0Pz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Pz",
            "<",
            "LX/1RZ",
            "<****>;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field public final e:LX/1PW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final f:LX/1Qx;

.field public g:Z

.field public h:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field

.field private i:Landroid/view/View;

.field private j:LX/1Cz;


# direct methods
.method private constructor <init>(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;LX/1RE;LX/0Pz;Ljava/lang/Object;LX/1PW;LX/1Qx;)V
    .locals 0
    .param p2    # LX/1RE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<TP;TS;TE;>;",
            "LX/1RE",
            "<***>;",
            "LX/0Pz",
            "<",
            "LX/1RZ",
            "<****>;>;TP;TE;",
            "LX/1Qx;",
            ")V"
        }
    .end annotation

    .prologue
    .line 245759
    invoke-direct {p0}, LX/1RF;-><init>()V

    .line 245760
    iput-object p1, p0, LX/1RE;->a:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    .line 245761
    iput-object p4, p0, LX/1RE;->d:Ljava/lang/Object;

    .line 245762
    iput-object p2, p0, LX/1RE;->b:LX/1RE;

    .line 245763
    iput-object p5, p0, LX/1RE;->e:LX/1PW;

    .line 245764
    iput-object p3, p0, LX/1RE;->c:LX/0Pz;

    .line 245765
    iput-object p6, p0, LX/1RE;->f:LX/1Qx;

    .line 245766
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 245767
    return-void
.end method

.method public static a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;LX/1RE;LX/0Pz;Ljava/lang/Object;LX/1PW;LX/1Qx;)LX/1RE;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            "E::",
            "LX/1PW;",
            ">(",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<TP;TS;TE;>;",
            "LX/1RE",
            "<***>;",
            "LX/0Pz",
            "<",
            "LX/1RZ",
            "<****>;>;TP;TE;",
            "LX/1Qx;",
            ")",
            "LX/1RE",
            "<TP;TS;TE;>;"
        }
    .end annotation

    .prologue
    .line 245768
    new-instance v0, LX/1RE;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/1RE;-><init>(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;LX/1RE;LX/0Pz;Ljava/lang/Object;LX/1PW;LX/1Qx;)V

    .line 245769
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/1RE;->g:Z

    .line 245770
    iget-object v1, v0, LX/1RE;->a:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    iget-object v2, v0, LX/1RE;->d:Ljava/lang/Object;

    iget-object v3, v0, LX/1RE;->e:LX/1PW;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;->a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, LX/1RE;->h:Ljava/lang/Object;

    .line 245771
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/1RE;->g:Z

    .line 245772
    return-object v0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 2

    .prologue
    .line 245773
    iget-object v0, p0, LX/1RE;->i:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can only call getView() during bind() or unbind()"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 245774
    iget-object v0, p0, LX/1RE;->i:Landroid/view/View;

    return-object v0

    .line 245775
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;LX/1Cz;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x6

    .line 245776
    iget-object v0, p0, LX/1RE;->b:LX/1RE;

    if-eqz v0, :cond_0

    .line 245777
    iget-object v0, p0, LX/1RE;->b:LX/1RE;

    invoke-virtual {v0, p1, p2}, LX/1RE;->a(Landroid/view/View;LX/1Cz;)V

    .line 245778
    :cond_0
    iput-object p1, p0, LX/1RE;->i:Landroid/view/View;

    .line 245779
    iput-object p2, p0, LX/1RE;->j:LX/1Cz;

    .line 245780
    iget-object v0, p0, LX/1RE;->f:LX/1Qx;

    iget-object v1, p0, LX/1RE;->a:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    invoke-interface {v0, v1, v4}, LX/1Qx;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;I)V

    .line 245781
    iget-object v0, p0, LX/1RE;->a:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    iget-object v1, p0, LX/1RE;->d:Ljava/lang/Object;

    iget-object v2, p0, LX/1RE;->h:Ljava/lang/Object;

    iget-object v3, p0, LX/1RE;->e:LX/1PW;

    invoke-interface {v0, v1, v2, v3, p0}, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V

    .line 245782
    iget-object v0, p0, LX/1RE;->f:LX/1Qx;

    invoke-interface {v0, v4}, LX/1Qx;->a(I)V

    .line 245783
    iput-object v5, p0, LX/1RE;->i:Landroid/view/View;

    .line 245784
    iput-object v5, p0, LX/1RE;->j:LX/1Cz;

    .line 245785
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/multirow/api/SinglePartDefinitionWithViewTypeAndIsNeeded",
            "<TP;*-TE;*>;TP;)Z"
        }
    .end annotation

    .prologue
    .line 245786
    iget-boolean v0, p0, LX/1RE;->g:Z

    if-nez v0, :cond_0

    .line 245787
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "mIsPreparing is false"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 245788
    :cond_0
    const/4 v2, 0x0

    .line 245789
    iget-object v0, p0, LX/1RE;->f:LX/1Qx;

    invoke-interface {v0, p1, v2}, LX/1Qx;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;I)V

    .line 245790
    invoke-interface {p1, p2}, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;->a(Ljava/lang/Object;)Z

    move-result v0

    .line 245791
    iget-object v1, p0, LX/1RE;->f:LX/1Qx;

    invoke-interface {v1, v2}, LX/1Qx;->a(I)V

    .line 245792
    move v0, v0

    .line 245793
    if-nez v0, :cond_1

    .line 245794
    const/4 v0, 0x0

    .line 245795
    :goto_0
    return v0

    .line 245796
    :cond_1
    iget-object v0, p0, LX/1RE;->c:LX/0Pz;

    new-instance v1, LX/1RZ;

    iget-object v2, p0, LX/1RE;->f:LX/1Qx;

    invoke-direct {v1, p1, p0, p2, v2}, LX/1RZ;-><init>(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;LX/1RE;Ljava/lang/Object;LX/1Qx;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 245797
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<TP;*-TE;>;TP;)Z"
        }
    .end annotation

    .prologue
    .line 245798
    iget-boolean v0, p0, LX/1RE;->g:Z

    if-nez v0, :cond_0

    .line 245799
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "mIsPreparing is false"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 245800
    :cond_0
    invoke-interface {p1, p2}, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;->a(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 245801
    if-nez v0, :cond_1

    .line 245802
    const/4 v0, 0x0

    .line 245803
    :goto_0
    return v0

    .line 245804
    :cond_1
    iget-object v2, p0, LX/1RE;->c:LX/0Pz;

    iget-object v4, p0, LX/1RE;->e:LX/1PW;

    iget-object v5, p0, LX/1RE;->f:LX/1Qx;

    move-object v0, p1

    move-object v1, p0

    move-object v3, p2

    invoke-static/range {v0 .. v5}, LX/1RE;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;LX/1RE;LX/0Pz;Ljava/lang/Object;LX/1PW;LX/1Qx;)LX/1RE;

    .line 245805
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Landroid/view/View;LX/1Cz;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x7

    .line 245806
    iput-object p1, p0, LX/1RE;->i:Landroid/view/View;

    .line 245807
    iput-object p2, p0, LX/1RE;->j:LX/1Cz;

    .line 245808
    iget-object v0, p0, LX/1RE;->f:LX/1Qx;

    iget-object v1, p0, LX/1RE;->a:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    invoke-interface {v0, v1, v4}, LX/1Qx;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;I)V

    .line 245809
    iget-object v0, p0, LX/1RE;->a:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    iget-object v1, p0, LX/1RE;->d:Ljava/lang/Object;

    iget-object v2, p0, LX/1RE;->h:Ljava/lang/Object;

    iget-object v3, p0, LX/1RE;->e:LX/1PW;

    invoke-interface {v0, v1, v2, v3, p0}, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;->b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V

    .line 245810
    iget-object v0, p0, LX/1RE;->f:LX/1Qx;

    invoke-interface {v0, v4}, LX/1Qx;->a(I)V

    .line 245811
    iput-object v5, p0, LX/1RE;->i:Landroid/view/View;

    .line 245812
    iput-object v5, p0, LX/1RE;->j:LX/1Cz;

    .line 245813
    iget-object v0, p0, LX/1RE;->b:LX/1RE;

    if-eqz v0, :cond_0

    .line 245814
    iget-object v0, p0, LX/1RE;->b:LX/1RE;

    invoke-virtual {v0, p1, p2}, LX/1RE;->b(Landroid/view/View;LX/1Cz;)V

    .line 245815
    :cond_0
    return-void
.end method
