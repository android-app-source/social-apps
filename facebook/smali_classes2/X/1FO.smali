.class public LX/1FO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final a:LX/1FP;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field private final b:LX/1FN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FN",
            "<[B>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0rb;LX/1F7;)V
    .locals 2

    .prologue
    .line 222293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222294
    iget v0, p2, LX/1F7;->f:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 222295
    new-instance v0, LX/1FP;

    invoke-static {}, LX/1FT;->a()LX/1FT;

    move-result-object v1

    invoke-direct {v0, p1, p2, v1}, LX/1FP;-><init>(LX/0rb;LX/1F7;LX/1F0;)V

    iput-object v0, p0, LX/1FO;->a:LX/1FP;

    .line 222296
    new-instance v0, LX/1FW;

    invoke-direct {v0, p0}, LX/1FW;-><init>(LX/1FO;)V

    iput-object v0, p0, LX/1FO;->b:LX/1FN;

    .line 222297
    return-void

    .line 222298
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/1FJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1FJ",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 222292
    iget-object v0, p0, LX/1FO;->a:LX/1FP;

    invoke-virtual {v0, p1}, LX/1FR;->a(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LX/1FO;->b:LX/1FN;

    invoke-static {v0, v1}, LX/1FJ;->a(Ljava/lang/Object;LX/1FN;)LX/1FJ;

    move-result-object v0

    return-object v0
.end method
