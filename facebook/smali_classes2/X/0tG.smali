.class public LX/0tG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0tG;


# instance fields
.field private final a:LX/0sa;

.field private final b:LX/0tH;


# direct methods
.method public constructor <init>(LX/0sa;LX/0tH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 154225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154226
    iput-object p1, p0, LX/0tG;->a:LX/0sa;

    .line 154227
    iput-object p2, p0, LX/0tG;->b:LX/0tH;

    .line 154228
    return-void
.end method

.method public static a(LX/0QB;)LX/0tG;
    .locals 5

    .prologue
    .line 154237
    sget-object v0, LX/0tG;->c:LX/0tG;

    if-nez v0, :cond_1

    .line 154238
    const-class v1, LX/0tG;

    monitor-enter v1

    .line 154239
    :try_start_0
    sget-object v0, LX/0tG;->c:LX/0tG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 154240
    if-eqz v2, :cond_0

    .line 154241
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 154242
    new-instance p0, LX/0tG;

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v3

    check-cast v3, LX/0sa;

    invoke-static {v0}, LX/0tH;->a(LX/0QB;)LX/0tH;

    move-result-object v4

    check-cast v4, LX/0tH;

    invoke-direct {p0, v3, v4}, LX/0tG;-><init>(LX/0sa;LX/0tH;)V

    .line 154243
    move-object v0, p0

    .line 154244
    sput-object v0, LX/0tG;->c:LX/0tG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154245
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 154246
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 154247
    :cond_1
    sget-object v0, LX/0tG;->c:LX/0tG;

    return-object v0

    .line 154248
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 154249
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0gW;)V
    .locals 2

    .prologue
    .line 154233
    const-string v0, "enable_comment_reactions"

    iget-object v1, p0, LX/0tG;->b:LX/0tH;

    invoke-virtual {v1}, LX/0tH;->d()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 154234
    const-string v0, "enable_comment_reactions_icons"

    iget-object v1, p0, LX/0tG;->b:LX/0tH;

    invoke-virtual {v1}, LX/0tH;->e()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 154235
    const-string v0, "max_reactors"

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 154236
    return-void
.end method

.method public final b(LX/0gW;)V
    .locals 2

    .prologue
    .line 154229
    const-string v0, "reactors_profile_image_size"

    .line 154230
    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object p0

    move-object v1, p0

    .line 154231
    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 154232
    return-void
.end method
