.class public final LX/0x1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 161802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161803
    return-void
.end method

.method public static a(Ljava/lang/Object;)LX/0x2;
    .locals 3

    .prologue
    .line 161799
    instance-of v0, p0, LX/0jR;

    if-nez v0, :cond_0

    .line 161800
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "class doesn\'t implement PropertyBag.HasProperty:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 161801
    :cond_0
    check-cast p0, LX/0jR;

    invoke-interface {p0}, LX/0jR;->L_()LX/0x2;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/16g;)LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 161796
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161797
    iget-object p0, v0, LX/0x2;->c:LX/162;

    move-object v0, p0

    .line 161798
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 161793
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161794
    iget-object p0, v0, LX/0x2;->d:Landroid/net/Uri;

    move-object v0, p0

    .line 161795
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 161790
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161791
    iget-object p0, v0, LX/0x2;->k:Lcom/facebook/graphql/model/FeedUnit;

    move-object v0, p0

    .line 161792
    return-object v0
.end method

.method public static a(LX/16o;)Lcom/facebook/graphql/model/SponsoredImpression;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 161787
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161788
    iget-object p0, v0, LX/0x2;->b:Lcom/facebook/graphql/model/SponsoredImpression;

    move-object v0, p0

    .line 161789
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Ljava/lang/Integer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 161757
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161758
    iget-object p0, v0, LX/0x2;->z:Ljava/lang/Integer;

    move-object v0, p0

    .line 161759
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 161781
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161782
    iget-object p0, v0, LX/0x2;->f:Ljava/lang/String;

    move-object v0, p0

    .line 161783
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 161778
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161779
    iget-object p0, v0, LX/0x2;->e:Ljava/lang/String;

    move-object v0, p0

    .line 161780
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 161775
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161776
    iget-object p0, v0, LX/0x2;->j:Ljava/lang/String;

    move-object v0, p0

    .line 161777
    return-object v0
.end method

.method public static a(LX/16g;LX/162;)V
    .locals 1
    .param p1    # LX/162;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 161772
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161773
    iput-object p1, v0, LX/0x2;->c:LX/162;

    .line 161774
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;I)V
    .locals 1

    .prologue
    .line 161769
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161770
    iput p1, v0, LX/0x2;->x:I

    .line 161771
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;LX/0Rf;)V
    .locals 1
    .param p1    # LX/0Rf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 161766
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161767
    iput-object p1, v0, LX/0x2;->y:LX/0Rf;

    .line 161768
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 161763
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161764
    iput-object p1, v0, LX/0x2;->f:Ljava/lang/String;

    .line 161765
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 161784
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161785
    iput-object p1, v0, LX/0x2;->e:Ljava/lang/String;

    .line 161786
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 161760
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161761
    iput-object p1, v0, LX/0x2;->a:Ljava/lang/String;

    .line 161762
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 161831
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161832
    iput-object p1, v0, LX/0x2;->k:Lcom/facebook/graphql/model/FeedUnit;

    .line 161833
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;)V
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 161855
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161856
    iput-object p1, v0, LX/0x2;->u:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 161857
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 161706
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161707
    iput-object p1, v0, LX/0x2;->r:Ljava/lang/String;

    .line 161708
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Z)V
    .locals 1

    .prologue
    .line 161852
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161853
    iput-boolean p1, v0, LX/0x2;->n:Z

    .line 161854
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/Sponsorable;I)V
    .locals 1

    .prologue
    .line 161849
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161850
    iput p1, v0, LX/0x2;->o:I

    .line 161851
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/Sponsorable;Z)V
    .locals 1

    .prologue
    .line 161846
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161847
    iput-boolean p1, v0, LX/0x2;->n:Z

    .line 161848
    return-void
.end method

.method public static a(LX/17w;)Z
    .locals 1

    .prologue
    .line 161843
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161844
    iget-boolean p0, v0, LX/0x2;->m:Z

    move v0, p0

    .line 161845
    return v0
.end method

.method public static a(LX/254;)Z
    .locals 1

    .prologue
    .line 161840
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161841
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161842
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;)Z
    .locals 1

    .prologue
    .line 161837
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161838
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161839
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;)Z
    .locals 1

    .prologue
    .line 161834
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161835
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161836
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;)Z
    .locals 1

    .prologue
    .line 161807
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161808
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161809
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;)Z
    .locals 1

    .prologue
    .line 161828
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161829
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161830
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;)Z
    .locals 1

    .prologue
    .line 161825
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161826
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161827
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;)Z
    .locals 1

    .prologue
    .line 161822
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161823
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161824
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;)Z
    .locals 1

    .prologue
    .line 161819
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161820
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161821
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;)Z
    .locals 1

    .prologue
    .line 161816
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161817
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161818
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;)Z
    .locals 1

    .prologue
    .line 161813
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161814
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161815
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)Z
    .locals 1

    .prologue
    .line 161810
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161811
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161812
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;)Z
    .locals 1

    .prologue
    .line 161804
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161805
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161806
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;)Z
    .locals 1

    .prologue
    .line 161700
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161701
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161702
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)Z
    .locals 1

    .prologue
    .line 161697
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161698
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161699
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;)Z
    .locals 1

    .prologue
    .line 161694
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161695
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161696
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;)Z
    .locals 1

    .prologue
    .line 161691
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161692
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161693
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Z
    .locals 1

    .prologue
    .line 161688
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161689
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161690
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;)Z
    .locals 1

    .prologue
    .line 161685
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161686
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161687
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;)Z
    .locals 1

    .prologue
    .line 161682
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161683
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161684
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z
    .locals 1

    .prologue
    .line 161679
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161680
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161681
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;)Z
    .locals 1

    .prologue
    .line 161676
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161677
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161678
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;)Z
    .locals 1

    .prologue
    .line 161673
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161674
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161675
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;)Z
    .locals 1

    .prologue
    .line 161670
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161671
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161672
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 161667
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161668
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161669
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;)Z
    .locals 1

    .prologue
    .line 161664
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161665
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161666
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/Sponsorable;)Z
    .locals 1

    .prologue
    .line 161661
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161662
    iget-boolean p0, v0, LX/0x2;->n:Z

    move v0, p0

    .line 161663
    return v0
.end method

.method public static b(Lcom/facebook/graphql/model/Sponsorable;)I
    .locals 1

    .prologue
    .line 161658
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161659
    iget p0, v0, LX/0x2;->o:I

    move v0, p0

    .line 161660
    return v0
.end method

.method public static b(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 161655
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161656
    iget-object p0, v0, LX/0x2;->h:Ljava/lang/String;

    move-object v0, p0

    .line 161657
    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 161730
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161731
    iget-object p0, v0, LX/0x2;->g:Ljava/lang/String;

    move-object v0, p0

    .line 161732
    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 161754
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161755
    iput-object p1, v0, LX/0x2;->h:Ljava/lang/String;

    .line 161756
    return-void
.end method

.method public static b(Lcom/facebook/graphql/model/FeedUnit;Z)V
    .locals 1

    .prologue
    .line 161751
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161752
    iput-boolean p1, v0, LX/0x2;->t:Z

    .line 161753
    return-void
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 161748
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161749
    iput-object p1, v0, LX/0x2;->g:Ljava/lang/String;

    .line 161750
    return-void
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 161745
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161746
    iget-boolean p0, v0, LX/0x2;->l:Z

    move v0, p0

    .line 161747
    return v0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 161742
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161743
    iget-object p0, v0, LX/0x2;->h:Ljava/lang/String;

    move-object v0, p0

    .line 161744
    return-object v0
.end method

.method public static c(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 161739
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161740
    iput-object p1, v0, LX/0x2;->i:Ljava/lang/String;

    .line 161741
    return-void
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 161736
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161737
    iput-object p1, v0, LX/0x2;->h:Ljava/lang/String;

    .line 161738
    return-void
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLStory;Z)V
    .locals 1

    .prologue
    .line 161733
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161734
    iput-boolean p1, v0, LX/0x2;->v:Z

    .line 161735
    return-void
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 161703
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161704
    iget-boolean p0, v0, LX/0x2;->q:Z

    move v0, p0

    .line 161705
    return v0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 161727
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161728
    iget-object p0, v0, LX/0x2;->r:Ljava/lang/String;

    move-object v0, p0

    .line 161729
    return-object v0
.end method

.method public static d(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 161724
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161725
    iput-object p1, v0, LX/0x2;->w:Ljava/lang/String;

    .line 161726
    return-void
.end method

.method public static d(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 161721
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161722
    iget-boolean p0, v0, LX/0x2;->p:Z

    move v0, p0

    .line 161723
    return v0
.end method

.method public static e(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/OrganicImpression;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 161718
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161719
    iget-object p0, v0, LX/0x2;->s:Lcom/facebook/graphql/model/OrganicImpression;

    move-object v0, p0

    .line 161720
    return-object v0
.end method

.method public static f(Lcom/facebook/graphql/model/FeedUnit;)I
    .locals 1

    .prologue
    .line 161715
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161716
    iget p0, v0, LX/0x2;->x:I

    move v0, p0

    .line 161717
    return v0
.end method

.method public static f(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 161712
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161713
    iget-object p0, v0, LX/0x2;->u:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-object v0, p0

    .line 161714
    return-object v0
.end method

.method public static g(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 161709
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 161710
    iget-boolean p0, v0, LX/0x2;->v:Z

    move v0, p0

    .line 161711
    return v0
.end method
