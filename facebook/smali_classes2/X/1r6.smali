.class public LX/1r6;
.super LX/1r7;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1r7",
        "<TK;",
        "Ljava/util/Collection",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Xs;


# direct methods
.method public constructor <init>(LX/0Xs;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 331710
    iput-object p1, p0, LX/1r6;->a:LX/0Xs;

    .line 331711
    invoke-direct {p0, p2}, LX/1r7;-><init>(Ljava/util/Map;)V

    .line 331712
    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 1

    .prologue
    .line 331713
    invoke-virtual {p0}, LX/1r6;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0RZ;->h(Ljava/util/Iterator;)V

    .line 331714
    return-void
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 331715
    invoke-virtual {p0}, LX/1r7;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 331716
    if-eq p0, p1, :cond_0

    invoke-virtual {p0}, LX/1r7;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 331717
    invoke-virtual {p0}, LX/1r7;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->hashCode()I

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 331718
    invoke-virtual {p0}, LX/1r7;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 331719
    new-instance v1, LX/1r8;

    invoke-direct {v1, p0, v0}, LX/1r8;-><init>(LX/1r6;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 331720
    invoke-virtual {p0}, LX/1r7;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 331721
    if-eqz v0, :cond_1

    .line 331722
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    .line 331723
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 331724
    iget-object v0, p0, LX/1r6;->a:LX/0Xs;

    invoke-static {v0, v2}, LX/0Xs;->b(LX/0Xs;I)I

    move v0, v2

    .line 331725
    :goto_0
    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method
