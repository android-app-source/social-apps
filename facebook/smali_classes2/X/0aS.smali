.class public abstract LX/0aS;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0aT",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0Zz;

.field private final b:Landroid/os/Handler;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/0Zz;LX/0Ot;Landroid/os/Handler;Ljava/util/List;)V
    .locals 1
    .param p3    # Landroid/os/Handler;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zz;",
            "LX/0Ot",
            "<TT;>;",
            "Landroid/os/Handler;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 84786
    invoke-direct {p0, p2}, LX/0aT;-><init>(LX/0Ot;)V

    .line 84787
    iput-object p1, p0, LX/0aS;->a:LX/0Zz;

    .line 84788
    iput-object p3, p0, LX/0aS;->b:Landroid/os/Handler;

    .line 84789
    invoke-static {p4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/0aS;->c:LX/0Px;

    .line 84790
    return-void
.end method

.method private varargs constructor <init>(LX/0Zz;LX/0Ot;Landroid/os/Handler;[Ljava/lang/String;)V
    .locals 1
    .param p3    # Landroid/os/Handler;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zz;",
            "LX/0Ot",
            "<TT;>;",
            "Landroid/os/Handler;",
            "[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 84791
    invoke-static {p4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, LX/0aS;-><init>(LX/0Zz;LX/0Ot;Landroid/os/Handler;Ljava/util/List;)V

    .line 84792
    return-void
.end method

.method public varargs constructor <init>(LX/0Zz;LX/0Ot;[Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zz;",
            "LX/0Ot",
            "<TT;>;[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 84793
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, LX/0aS;-><init>(LX/0Zz;LX/0Ot;Landroid/os/Handler;[Ljava/lang/String;)V

    .line 84794
    return-void
.end method
