.class public LX/1pa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1pb;


# instance fields
.field private final a:D

.field private b:D


# direct methods
.method public constructor <init>(D)V
    .locals 3

    .prologue
    .line 329520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 329521
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, LX/1pa;->b:D

    .line 329522
    iput-wide p1, p0, LX/1pa;->a:D

    .line 329523
    return-void
.end method


# virtual methods
.method public final a()D
    .locals 2

    .prologue
    .line 329524
    iget-wide v0, p0, LX/1pa;->b:D

    return-wide v0
.end method

.method public final a(D)V
    .locals 7

    .prologue
    .line 329525
    iget-wide v0, p0, LX/1pa;->b:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, LX/1pa;->b:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget-wide v4, p0, LX/1pa;->a:D

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    iget-wide v2, p0, LX/1pa;->a:D

    mul-double/2addr v2, p1

    add-double p1, v0, v2

    :cond_0
    iput-wide p1, p0, LX/1pa;->b:D

    .line 329526
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 329527
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, LX/1pa;->b:D

    .line 329528
    return-void
.end method
