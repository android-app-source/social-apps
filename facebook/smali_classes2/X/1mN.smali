.class public final LX/1mN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "Lcom/facebook/fbservice/service/BlueServiceQueueHook;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "Lcom/facebook/fbservice/service/BlueServiceQueueHook;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final mInjector:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 313742
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313743
    iput-object p1, p0, LX/1mN;->mInjector:LX/0QB;

    .line 313744
    return-void
.end method

.method public static getLazySet(LX/0QB;)LX/0Ot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/fbservice/service/BlueServiceQueueHook;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 313754
    invoke-static {p0}, LX/1mN;->getSetProvider(LX/0QB;)LX/0Or;

    move-result-object v0

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public static getSet(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/fbservice/service/BlueServiceQueueHook;",
            ">;"
        }
    .end annotation

    .prologue
    .line 313753
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/1mN;

    invoke-direct {v2, p0}, LX/1mN;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public static getSetProvider(LX/0QB;)LX/0Or;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/fbservice/service/BlueServiceQueueHook;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 313752
    new-instance v0, LX/1mN;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1mN;-><init>(LX/0QB;)V

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 313750
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/1mN;->mInjector:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v0, v0

    .line 313751
    return-object v0
.end method

.method public final provide(LX/0QC;I)LX/1qI;
    .locals 2

    .prologue
    .line 313747
    packed-switch p2, :pswitch_data_0

    .line 313748
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 313749
    :pswitch_0
    invoke-static {p1}, LX/1qI;->a(LX/0QB;)LX/1qI;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final bridge synthetic provide(LX/0QC;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 313746
    invoke-virtual {p0, p1, p2}, LX/1mN;->provide(LX/0QC;I)LX/1qI;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 313745
    const/4 v0, 0x1

    return v0
.end method
