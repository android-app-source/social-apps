.class public final enum LX/1fy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1fy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1fy;

.field public static final enum AFTER_ENTER_VIEWPORT:LX/1fy;

.field public static final enum BEFORE_DEDUP:LX/1fy;

.field public static final enum BEFORE_EXIT_VIEWPORT:LX/1fy;

.field public static final enum BEFORE_SENT_TO_MARAUDER:LX/1fy;

.field public static final enum BEFORE_SENT_TO_QUEUE:LX/1fy;

.field public static final enum BEFORE_TIME_THRESHOLD:LX/1fy;

.field public static final enum BEFORE_VALIDATION:LX/1fy;

.field public static final enum IN_VIEWPORT_FOR_MORE_THAN_VPV_DURATION:LX/1fy;

.field public static final enum VPVD_ELIGIBLE:LX/1fy;

.field public static final enum VPVD_INELIGIBLE:LX/1fy;


# instance fields
.field private code:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 292548
    new-instance v0, LX/1fy;

    const-string v1, "AFTER_ENTER_VIEWPORT"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v4, v2}, LX/1fy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1fy;->AFTER_ENTER_VIEWPORT:LX/1fy;

    .line 292549
    new-instance v0, LX/1fy;

    const-string v1, "VPVD_ELIGIBLE"

    const/16 v2, 0x7d

    invoke-direct {v0, v1, v5, v2}, LX/1fy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1fy;->VPVD_ELIGIBLE:LX/1fy;

    .line 292550
    new-instance v0, LX/1fy;

    const-string v1, "IN_VIEWPORT_FOR_MORE_THAN_VPV_DURATION"

    const/16 v2, 0x96

    invoke-direct {v0, v1, v6, v2}, LX/1fy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1fy;->IN_VIEWPORT_FOR_MORE_THAN_VPV_DURATION:LX/1fy;

    .line 292551
    new-instance v0, LX/1fy;

    const-string v1, "VPVD_INELIGIBLE"

    const/16 v2, 0xaf

    invoke-direct {v0, v1, v7, v2}, LX/1fy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1fy;->VPVD_INELIGIBLE:LX/1fy;

    .line 292552
    new-instance v0, LX/1fy;

    const-string v1, "BEFORE_EXIT_VIEWPORT"

    const/16 v2, 0xc8

    invoke-direct {v0, v1, v8, v2}, LX/1fy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1fy;->BEFORE_EXIT_VIEWPORT:LX/1fy;

    .line 292553
    new-instance v0, LX/1fy;

    const-string v1, "BEFORE_TIME_THRESHOLD"

    const/4 v2, 0x5

    const/16 v3, 0x12c

    invoke-direct {v0, v1, v2, v3}, LX/1fy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1fy;->BEFORE_TIME_THRESHOLD:LX/1fy;

    .line 292554
    new-instance v0, LX/1fy;

    const-string v1, "BEFORE_DEDUP"

    const/4 v2, 0x6

    const/16 v3, 0x190

    invoke-direct {v0, v1, v2, v3}, LX/1fy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1fy;->BEFORE_DEDUP:LX/1fy;

    .line 292555
    new-instance v0, LX/1fy;

    const-string v1, "BEFORE_VALIDATION"

    const/4 v2, 0x7

    const/16 v3, 0x1f4

    invoke-direct {v0, v1, v2, v3}, LX/1fy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1fy;->BEFORE_VALIDATION:LX/1fy;

    .line 292556
    new-instance v0, LX/1fy;

    const-string v1, "BEFORE_SENT_TO_QUEUE"

    const/16 v2, 0x8

    const/16 v3, 0x258

    invoke-direct {v0, v1, v2, v3}, LX/1fy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1fy;->BEFORE_SENT_TO_QUEUE:LX/1fy;

    .line 292557
    new-instance v0, LX/1fy;

    const-string v1, "BEFORE_SENT_TO_MARAUDER"

    const/16 v2, 0x9

    const/16 v3, 0x2bc

    invoke-direct {v0, v1, v2, v3}, LX/1fy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1fy;->BEFORE_SENT_TO_MARAUDER:LX/1fy;

    .line 292558
    const/16 v0, 0xa

    new-array v0, v0, [LX/1fy;

    sget-object v1, LX/1fy;->AFTER_ENTER_VIEWPORT:LX/1fy;

    aput-object v1, v0, v4

    sget-object v1, LX/1fy;->VPVD_ELIGIBLE:LX/1fy;

    aput-object v1, v0, v5

    sget-object v1, LX/1fy;->IN_VIEWPORT_FOR_MORE_THAN_VPV_DURATION:LX/1fy;

    aput-object v1, v0, v6

    sget-object v1, LX/1fy;->VPVD_INELIGIBLE:LX/1fy;

    aput-object v1, v0, v7

    sget-object v1, LX/1fy;->BEFORE_EXIT_VIEWPORT:LX/1fy;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/1fy;->BEFORE_TIME_THRESHOLD:LX/1fy;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1fy;->BEFORE_DEDUP:LX/1fy;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/1fy;->BEFORE_VALIDATION:LX/1fy;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/1fy;->BEFORE_SENT_TO_QUEUE:LX/1fy;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/1fy;->BEFORE_SENT_TO_MARAUDER:LX/1fy;

    aput-object v2, v0, v1

    sput-object v0, LX/1fy;->$VALUES:[LX/1fy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 292559
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 292560
    iput p3, p0, LX/1fy;->code:I

    .line 292561
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1fy;
    .locals 1

    .prologue
    .line 292562
    const-class v0, LX/1fy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1fy;

    return-object v0
.end method

.method public static values()[LX/1fy;
    .locals 1

    .prologue
    .line 292563
    sget-object v0, LX/1fy;->$VALUES:[LX/1fy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1fy;

    return-object v0
.end method


# virtual methods
.method public final getCode()I
    .locals 1

    .prologue
    .line 292564
    iget v0, p0, LX/1fy;->code:I

    return v0
.end method
