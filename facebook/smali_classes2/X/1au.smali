.class public LX/1au;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 278801
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 278802
    iput-boolean v0, p0, LX/1au;->a:Z

    .line 278803
    iput-boolean v0, p0, LX/1au;->b:Z

    .line 278804
    iput-boolean v0, p0, LX/1au;->c:Z

    .line 278805
    const/4 v0, -0x1

    iput v0, p0, LX/1au;->d:I

    .line 278806
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 278813
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 278814
    iput-boolean v2, p0, LX/1au;->a:Z

    .line 278815
    iput-boolean v2, p0, LX/1au;->b:Z

    .line 278816
    iput-boolean v2, p0, LX/1au;->c:Z

    .line 278817
    iput v3, p0, LX/1au;->d:I

    .line 278818
    sget-object v0, LX/03r;->ImageBlockLayout_LayoutParams:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 278819
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/1au;->a:Z

    .line 278820
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/1au;->b:Z

    .line 278821
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/1au;->c:Z

    .line 278822
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, LX/1au;->d:I

    .line 278823
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 278824
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 278807
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 278808
    iput-boolean v0, p0, LX/1au;->a:Z

    .line 278809
    iput-boolean v0, p0, LX/1au;->b:Z

    .line 278810
    iput-boolean v0, p0, LX/1au;->c:Z

    .line 278811
    const/4 v0, -0x1

    iput v0, p0, LX/1au;->d:I

    .line 278812
    return-void
.end method
