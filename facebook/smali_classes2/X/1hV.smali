.class public final LX/1hV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public aeads:[Ljava/lang/String;

.field public cacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

.field private enableTCPFastOpen:Z

.field public enabled:Z

.field public enforceExpiration:Z

.field public hostnamePolicy:Ljava/lang/String;

.field public persistentCacheEnabled:Z

.field public retryEnabled:Z

.field public tlsFallback:I

.field public zeroRttEnabled:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 296084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296085
    iput-boolean v1, p0, LX/1hV;->enforceExpiration:Z

    .line 296086
    iput-boolean v1, p0, LX/1hV;->zeroRttEnabled:Z

    .line 296087
    iput-boolean v0, p0, LX/1hV;->persistentCacheEnabled:Z

    .line 296088
    iput-boolean v0, p0, LX/1hV;->retryEnabled:Z

    .line 296089
    iput v0, p0, LX/1hV;->tlsFallback:I

    .line 296090
    iput-boolean v0, p0, LX/1hV;->enableTCPFastOpen:Z

    return-void
.end method


# virtual methods
.method public build()Lcom/facebook/proxygen/ZeroProtocolSettings;
    .locals 11

    .prologue
    .line 296081
    new-instance v0, Lcom/facebook/proxygen/ZeroProtocolSettings;

    iget-boolean v1, p0, LX/1hV;->enabled:Z

    iget-boolean v2, p0, LX/1hV;->enforceExpiration:Z

    iget-boolean v3, p0, LX/1hV;->zeroRttEnabled:Z

    iget-boolean v4, p0, LX/1hV;->persistentCacheEnabled:Z

    iget-object v5, p0, LX/1hV;->cacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    iget-object v6, p0, LX/1hV;->aeads:[Ljava/lang/String;

    iget-object v7, p0, LX/1hV;->hostnamePolicy:Ljava/lang/String;

    iget-boolean v8, p0, LX/1hV;->retryEnabled:Z

    iget v9, p0, LX/1hV;->tlsFallback:I

    iget-boolean v10, p0, LX/1hV;->enableTCPFastOpen:Z

    invoke-direct/range {v0 .. v10}, Lcom/facebook/proxygen/ZeroProtocolSettings;-><init>(ZZZZLcom/facebook/proxygen/PersistentSSLCacheSettings;[Ljava/lang/String;Ljava/lang/String;ZIZ)V

    return-object v0
.end method

.method public enableTCPFastOpen(Z)LX/1hV;
    .locals 0

    .prologue
    .line 296082
    iput-boolean p1, p0, LX/1hV;->enableTCPFastOpen:Z

    .line 296083
    return-object p0
.end method
