.class public final LX/0Rp;
.super LX/0Ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Ro",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/Set;

.field public final synthetic b:Ljava/util/Set;

.field public final synthetic c:Ljava/util/Set;


# direct methods
.method public constructor <init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    .locals 1

    .prologue
    .line 60471
    iput-object p1, p0, LX/0Rp;->a:Ljava/util/Set;

    iput-object p2, p0, LX/0Rp;->b:Ljava/util/Set;

    iput-object p3, p0, LX/0Rp;->c:Ljava/util/Set;

    invoke-direct {p0}, LX/0Ro;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 60463
    new-instance v0, LX/0cA;

    invoke-direct {v0}, LX/0cA;-><init>()V

    iget-object v1, p0, LX/0Rp;->a:Ljava/util/Set;

    invoke-virtual {v0, v1}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v0

    iget-object v1, p0, LX/0Rp;->c:Ljava/util/Set;

    invoke-virtual {v0, v1}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v0

    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 60470
    iget-object v0, p0, LX/0Rp;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Rp;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 60472
    iget-object v0, p0, LX/0Rp;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Rp;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 60465
    iget-object v0, p0, LX/0Rp;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, LX/0Rp;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 60466
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60467
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60468
    new-instance v2, LX/0Rt;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/util/Iterator;

    const/4 p0, 0x0

    aput-object v0, v3, p0

    const/4 p0, 0x1

    aput-object v1, v3, p0

    invoke-direct {v2, v3}, LX/0Rt;-><init>([Ljava/lang/Object;)V

    invoke-static {v2}, LX/0RZ;->e(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v2

    move-object v0, v2

    .line 60469
    invoke-static {v0}, LX/0RZ;->a(Ljava/util/Iterator;)LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 2

    .prologue
    .line 60464
    iget-object v0, p0, LX/0Rp;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    iget-object v1, p0, LX/0Rp;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
