.class public LX/0p9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field private static final c:LX/0Tn;

.field public static final d:Ljava/lang/String;

.field private static volatile n:LX/0p9;


# instance fields
.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0pB;

.field public final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0p3;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private volatile l:D

.field private volatile m:D


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 143926
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "connection_manager/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 143927
    sput-object v0, LX/0p9;->c:LX/0Tn;

    const-string v1, "history/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0p9;->a:LX/0Tn;

    .line 143928
    sget-object v0, LX/0p9;->c:LX/0Tn;

    const-string v1, "conn_tracking/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0p9;->b:LX/0Tn;

    .line 143929
    const-class v0, LX/0p9;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0p9;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0pA;)V
    .locals 3
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 143909
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143910
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 143911
    iput-object v0, p0, LX/0p9;->e:LX/0Ot;

    .line 143912
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 143913
    iput-object v0, p0, LX/0p9;->f:LX/0Ot;

    .line 143914
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 143915
    iput-object v0, p0, LX/0p9;->g:LX/0Ot;

    .line 143916
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 143917
    iput-object v0, p0, LX/0p9;->h:LX/0Ot;

    .line 143918
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/0p9;->j:Ljava/util/Map;

    .line 143919
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/0p9;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 143920
    const-wide/32 v0, 0x36ee80

    .line 143921
    new-instance v2, LX/0pB;

    invoke-direct {v2, v0, v1}, LX/0pB;-><init>(J)V

    .line 143922
    move-object v0, v2

    .line 143923
    iput-object v0, p0, LX/0p9;->i:LX/0pB;

    .line 143924
    invoke-direct {p0}, LX/0p9;->f()V

    .line 143925
    return-void
.end method

.method public static a(LX/0QB;)LX/0p9;
    .locals 7

    .prologue
    .line 143894
    sget-object v0, LX/0p9;->n:LX/0p9;

    if-nez v0, :cond_1

    .line 143895
    const-class v1, LX/0p9;

    monitor-enter v1

    .line 143896
    :try_start_0
    sget-object v0, LX/0p9;->n:LX/0p9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 143897
    if-eqz v2, :cond_0

    .line 143898
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 143899
    new-instance v4, LX/0p9;

    const-class v3, LX/0pA;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/0pA;

    invoke-direct {v4, v3}, LX/0p9;-><init>(LX/0pA;)V

    .line 143900
    const/16 v3, 0xf9a

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v5, 0x1644

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xac0

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0xbc

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 143901
    iput-object v3, v4, LX/0p9;->e:LX/0Ot;

    iput-object v5, v4, LX/0p9;->f:LX/0Ot;

    iput-object v6, v4, LX/0p9;->g:LX/0Ot;

    iput-object p0, v4, LX/0p9;->h:LX/0Ot;

    .line 143902
    move-object v0, v4

    .line 143903
    sput-object v0, LX/0p9;->n:LX/0p9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 143904
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 143905
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 143906
    :cond_1
    sget-object v0, LX/0p9;->n:LX/0p9;

    return-object v0

    .line 143907
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 143908
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0p9;LX/0p3;LX/0p3;ZZ)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    .line 143884
    if-eqz p3, :cond_2

    sget-object v0, LX/0p3;->DEGRADED:LX/0p3;

    invoke-virtual {p1, v0}, LX/0p3;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-lez v0, :cond_2

    sget-object v0, LX/0p3;->DEGRADED:LX/0p3;

    invoke-virtual {p2, v0}, LX/0p3;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    .line 143885
    :goto_0
    iget-object v2, p0, LX/0p9;->i:LX/0pB;

    invoke-virtual {v2, v0, p4}, LX/0pB;->a(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143886
    iget-object v0, p0, LX/0p9;->i:LX/0pB;

    invoke-virtual {v0}, LX/0pB;->a()[J

    move-result-object v0

    .line 143887
    if-eqz v0, :cond_0

    .line 143888
    invoke-static {p0, v0}, LX/0p9;->a$redex0(LX/0p9;[J)V

    .line 143889
    :cond_0
    iget-object v0, p0, LX/0p9;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 143890
    iget-object v3, p0, LX/0p9;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Uh;

    const/16 v4, 0x397

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-nez v3, :cond_3

    .line 143891
    :cond_1
    :goto_1
    return-void

    .line 143892
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 143893
    :cond_3
    iget-object v3, p0, LX/0p9;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v4, Lcom/facebook/common/network/HistoricalConnectionQuality$1;

    invoke-direct {v4, p0}, Lcom/facebook/common/network/HistoricalConnectionQuality$1;-><init>(LX/0p9;)V

    const-wide/16 v5, 0x3e8

    const-wide/32 v7, 0x36ee80

    sget-object v9, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v3 .. v9}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_1
.end method

.method public static a$redex0(LX/0p9;[J)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 143864
    iget-object v0, p0, LX/0p9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 143865
    :cond_0
    :goto_0
    return-void

    .line 143866
    :cond_1
    sget-object v0, LX/0p9;->b:LX/0Tn;

    const-string v1, "index"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 143867
    iget-object v1, p0, LX/0p9;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-wide/16 v2, 0x0

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 143868
    const-wide/16 v6, 0x1

    add-long/2addr v6, v2

    const-wide/16 v8, 0x30

    rem-long/2addr v6, v8

    .line 143869
    sget-object v1, LX/0p9;->b:LX/0Tn;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2f

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    .line 143870
    iget-object v2, p0, LX/0p9;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    .line 143871
    invoke-interface {v5, v1}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move v3, v4

    .line 143872
    :goto_1
    array-length v2, p1

    if-ge v3, v2, :cond_2

    .line 143873
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v2

    check-cast v2, LX/0Tn;

    .line 143874
    aget-wide v8, p1, v3

    invoke-interface {v5, v2, v8, v9}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 143875
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 143876
    :cond_2
    invoke-interface {v5, v0, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 143877
    invoke-interface {v5}, LX/0hN;->commit()V

    .line 143878
    invoke-direct {p0}, LX/0p9;->f()V

    .line 143879
    iget-object v0, p0, LX/0p9;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const-string v1, "fb4a_offline_fraction"

    invoke-interface {v0, v1, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 143880
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 143881
    const-string v1, "foregroundFraction"

    invoke-virtual {p0}, LX/0p9;->b()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    .line 143882
    const-string v1, "backgroundFraction"

    invoke-virtual {p0}, LX/0p9;->a()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    .line 143883
    invoke-virtual {v0}, LX/0oG;->d()V

    goto/16 :goto_0
.end method

.method private d()Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<[J>;"
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x0

    const/4 v4, 0x0

    .line 143849
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move-wide v6, v8

    .line 143850
    :goto_0
    const-wide/16 v0, 0x30

    cmp-long v0, v6, v0

    if-gez v0, :cond_2

    .line 143851
    sget-object v0, LX/0p9;->b:LX/0Tn;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    move v3, v4

    .line 143852
    :goto_1
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    .line 143853
    iget-object v2, p0, LX/0p9;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 143854
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 143855
    :cond_0
    if-eqz v3, :cond_2

    .line 143856
    new-array v10, v3, [J

    move v3, v4

    .line 143857
    :goto_2
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    .line 143858
    iget-object v2, p0, LX/0p9;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 143859
    iget-object v2, p0, LX/0p9;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2, v1, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v12

    aput-wide v12, v10, v3

    .line 143860
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 143861
    :cond_1
    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143862
    const-wide/16 v0, 0x1

    add-long/2addr v0, v6

    move-wide v6, v0

    goto :goto_0

    .line 143863
    :cond_2
    return-object v5
.end method

.method private f()V
    .locals 2

    .prologue
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 143811
    iput-wide v0, p0, LX/0p9;->m:D

    .line 143812
    iput-wide v0, p0, LX/0p9;->l:D

    .line 143813
    return-void
.end method

.method private g()V
    .locals 12

    .prologue
    .line 143831
    invoke-direct {p0}, LX/0p9;->d()Ljava/util/ArrayList;

    move-result-object v0

    .line 143832
    const-wide/16 v10, 0x0

    const/4 v8, 0x0

    .line 143833
    invoke-static {v0}, LX/0pB;->c(Ljava/util/ArrayList;)[J

    move-result-object v4

    .line 143834
    invoke-static {v8, v8}, LX/0pB;->b(ZZ)I

    move-result v5

    aget-wide v6, v4, v5

    .line 143835
    const/4 v5, 0x1

    invoke-static {v5, v8}, LX/0pB;->b(ZZ)I

    move-result v5

    aget-wide v4, v4, v5

    .line 143836
    cmp-long v8, v6, v10

    if-nez v8, :cond_0

    cmp-long v8, v4, v10

    if-nez v8, :cond_0

    .line 143837
    const-wide/16 v4, 0x0

    .line 143838
    :goto_0
    move-wide v2, v4

    .line 143839
    iput-wide v2, p0, LX/0p9;->m:D

    .line 143840
    const-wide/16 v10, 0x0

    const/4 v8, 0x1

    .line 143841
    invoke-static {v0}, LX/0pB;->c(Ljava/util/ArrayList;)[J

    move-result-object v4

    .line 143842
    const/4 v5, 0x0

    invoke-static {v5, v8}, LX/0pB;->b(ZZ)I

    move-result v5

    aget-wide v6, v4, v5

    .line 143843
    invoke-static {v8, v8}, LX/0pB;->b(ZZ)I

    move-result v5

    aget-wide v4, v4, v5

    .line 143844
    cmp-long v8, v6, v10

    if-nez v8, :cond_1

    cmp-long v8, v4, v10

    if-nez v8, :cond_1

    .line 143845
    const-wide/16 v4, 0x0

    .line 143846
    :goto_1
    move-wide v0, v4

    .line 143847
    iput-wide v0, p0, LX/0p9;->l:D

    .line 143848
    return-void

    :cond_0
    long-to-double v8, v6

    add-long/2addr v4, v6

    long-to-double v4, v4

    div-double v4, v8, v4

    goto :goto_0

    :cond_1
    long-to-double v8, v6

    add-long/2addr v4, v6

    long-to-double v4, v4

    div-double v4, v8, v4

    goto :goto_1
.end method


# virtual methods
.method public final a()D
    .locals 4

    .prologue
    .line 143828
    iget-wide v0, p0, LX/0p9;->m:D

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 143829
    invoke-direct {p0}, LX/0p9;->g()V

    .line 143830
    :cond_0
    iget-wide v0, p0, LX/0p9;->m:D

    return-wide v0
.end method

.method public final a(Ljava/lang/String;)LX/0p3;
    .locals 3

    .prologue
    .line 143817
    iget-object v0, p0, LX/0p9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 143818
    sget-object v0, LX/0p3;->UNKNOWN:LX/0p3;

    .line 143819
    :goto_0
    return-object v0

    .line 143820
    :cond_0
    iget-object v0, p0, LX/0p9;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143821
    iget-object v0, p0, LX/0p9;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0p3;

    goto :goto_0

    .line 143822
    :cond_1
    sget-object v0, LX/0p9;->a:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 143823
    iget-object v1, p0, LX/0p9;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-string v2, ""

    invoke-interface {v1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 143824
    sget-object v0, LX/0p3;->UNKNOWN:LX/0p3;

    .line 143825
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 143826
    :try_start_0
    invoke-static {v1}, LX/0p3;->valueOf(Ljava/lang/String;)LX/0p3;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 143827
    :cond_2
    :goto_1
    iget-object v1, p0, LX/0p9;->j:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :catch_0
    goto :goto_1
.end method

.method public final b()D
    .locals 4

    .prologue
    .line 143814
    iget-wide v0, p0, LX/0p9;->l:D

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 143815
    invoke-direct {p0}, LX/0p9;->g()V

    .line 143816
    :cond_0
    iget-wide v0, p0, LX/0p9;->l:D

    return-wide v0
.end method
