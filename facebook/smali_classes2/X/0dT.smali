.class public final LX/0dT;
.super LX/0dM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0dM",
        "<",
        "LX/1ER;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1ER;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 90369
    sget-object v0, LX/0dU;->m:LX/0Tn;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0dM;-><init>(LX/0Ot;Ljava/util/Set;)V

    .line 90370
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 90371
    check-cast p3, LX/1ER;

    .line 90372
    const/4 v0, 0x0

    invoke-interface {p1, p2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 90373
    if-eqz v0, :cond_0

    .line 90374
    iget-object v1, p3, LX/1ER;->a:LX/1ES;

    const-class v2, Lcom/facebook/http/common/DelayEmpathyDelayWorker;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/32 v5, 0x5265c0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v5, v6, v4}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, LX/1ES;->a(Ljava/lang/Class;J)V

    .line 90375
    :cond_0
    return-void
.end method
