.class public final LX/0vK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0vL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0vL",
        "<TT;>;"
    }
.end annotation


# static fields
.field public static final b:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater",
            "<",
            "LX/0vK;",
            "LX/0vN;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater",
            "<",
            "LX/0vK;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public volatile a:LX/0vN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vN",
            "<TT;>;"
        }
    .end annotation
.end field

.field public volatile c:Ljava/lang/Object;

.field public e:Z

.field public f:LX/0vM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vM",
            "<",
            "LX/0vO",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field public g:LX/0vM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vM",
            "<",
            "LX/0vO",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field public h:LX/0vM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vM",
            "<",
            "LX/0vO",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field public final i:LX/0vH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vH",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 157796
    const-class v0, LX/0vK;

    const-class v1, LX/0vN;

    const-string v2, "a"

    invoke-static {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v0

    sput-object v0, LX/0vK;->b:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    .line 157797
    const-class v0, LX/0vK;

    const-class v1, Ljava/lang/Object;

    const-string v2, "c"

    invoke-static {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v0

    sput-object v0, LX/0vK;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 157798
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157799
    sget-object v0, LX/0vN;->e:LX/0vN;

    iput-object v0, p0, LX/0vK;->a:LX/0vN;

    .line 157800
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0vK;->e:Z

    .line 157801
    sget-object v0, LX/0vP;->a:LX/0vQ;

    move-object v0, v0

    .line 157802
    iput-object v0, p0, LX/0vK;->f:LX/0vM;

    .line 157803
    sget-object v0, LX/0vP;->a:LX/0vQ;

    move-object v0, v0

    .line 157804
    iput-object v0, p0, LX/0vK;->g:LX/0vM;

    .line 157805
    sget-object v0, LX/0vP;->a:LX/0vQ;

    move-object v0, v0

    .line 157806
    iput-object v0, p0, LX/0vK;->h:LX/0vM;

    .line 157807
    sget-object v0, LX/0vH;->a:LX/0vH;

    move-object v0, v0

    .line 157808
    iput-object v0, p0, LX/0vK;->i:LX/0vH;

    .line 157809
    return-void
.end method


# virtual methods
.method public final a(LX/0vO;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0vO",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 157810
    :cond_0
    iget-object v0, p0, LX/0vK;->a:LX/0vN;

    .line 157811
    iget-boolean v1, v0, LX/0vN;->a:Z

    if-eqz v1, :cond_2

    .line 157812
    :cond_1
    :goto_0
    return-void

    .line 157813
    :cond_2
    invoke-virtual {v0, p1}, LX/0vN;->b(LX/0vO;)LX/0vN;

    move-result-object v1

    .line 157814
    if-eq v1, v0, :cond_1

    sget-object v2, LX/0vK;->b:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v2, p0, v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 157815
    check-cast p1, LX/0zZ;

    .line 157816
    new-instance v0, LX/0vO;

    invoke-direct {v0, p1}, LX/0vO;-><init>(LX/0vA;)V

    .line 157817
    new-instance v1, LX/0zd;

    invoke-direct {v1, p0, v0}, LX/0zd;-><init>(LX/0vK;LX/0vO;)V

    invoke-static {v1}, LX/0ze;->a(LX/0vR;)LX/0za;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/0zZ;->a(LX/0za;)V

    .line 157818
    iget-object v1, p0, LX/0vK;->f:LX/0vM;

    invoke-interface {v1, v0}, LX/0vM;->a(Ljava/lang/Object;)V

    .line 157819
    invoke-virtual {p1}, LX/0zZ;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 157820
    :cond_0
    iget-object v1, p0, LX/0vK;->a:LX/0vN;

    .line 157821
    iget-boolean v2, v1, LX/0vN;->a:Z

    if-eqz v2, :cond_2

    .line 157822
    iget-object v1, p0, LX/0vK;->h:LX/0vM;

    invoke-interface {v1, v0}, LX/0vM;->a(Ljava/lang/Object;)V

    .line 157823
    const/4 v1, 0x0

    .line 157824
    :goto_0
    move v1, v1

    .line 157825
    if-eqz v1, :cond_1

    invoke-virtual {p1}, LX/0zZ;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 157826
    invoke-virtual {p0, v0}, LX/0vK;->a(LX/0vO;)V

    .line 157827
    :cond_1
    return-void

    .line 157828
    :cond_2
    const/4 v5, 0x0

    .line 157829
    iget-object v2, v1, LX/0vN;->b:[LX/0vO;

    .line 157830
    array-length v2, v2

    .line 157831
    add-int/lit8 v3, v2, 0x1

    new-array v3, v3, [LX/0vO;

    .line 157832
    iget-object v4, v1, LX/0vN;->b:[LX/0vO;

    invoke-static {v4, v5, v3, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 157833
    aput-object v0, v3, v2

    .line 157834
    new-instance v2, LX/0vN;

    iget-boolean v4, v1, LX/0vN;->a:Z

    invoke-direct {v2, v4, v3}, LX/0vN;-><init>(Z[LX/0vO;)V

    move-object v2, v2

    .line 157835
    sget-object v3, LX/0vK;->b:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v3, p0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157836
    iget-object v1, p0, LX/0vK;->g:LX/0vM;

    invoke-interface {v1, v0}, LX/0vM;->a(Ljava/lang/Object;)V

    .line 157837
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)[LX/0vO;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")[",
            "LX/0vO",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 157838
    iput-object p1, p0, LX/0vK;->c:Ljava/lang/Object;

    .line 157839
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0vK;->e:Z

    .line 157840
    iget-object v0, p0, LX/0vK;->a:LX/0vN;

    .line 157841
    iget-boolean v0, v0, LX/0vN;->a:Z

    if-eqz v0, :cond_0

    .line 157842
    sget-object v0, LX/0vN;->c:[LX/0vO;

    .line 157843
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/0vK;->b:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    sget-object v1, LX/0vN;->d:LX/0vN;

    invoke-virtual {v0, p0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->getAndSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0vN;

    iget-object v0, v0, LX/0vN;->b:[LX/0vO;

    goto :goto_0
.end method
