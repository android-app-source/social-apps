.class public LX/0sg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0si;

.field private final b:LX/0t8;

.field private final c:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final d:LX/0sT;


# direct methods
.method private constructor <init>(LX/0si;LX/0t8;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0sT;)V
    .locals 0
    .param p2    # LX/0t8;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 152444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152445
    iput-object p1, p0, LX/0sg;->a:LX/0si;

    .line 152446
    iput-object p2, p0, LX/0sg;->b:LX/0t8;

    .line 152447
    iput-object p3, p0, LX/0sg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 152448
    iput-object p4, p0, LX/0sg;->d:LX/0sT;

    .line 152449
    return-void
.end method

.method public constructor <init>(LX/0si;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0sT;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 152441
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, LX/0sg;-><init>(LX/0si;LX/0t8;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0sT;)V

    .line 152442
    return-void
.end method

.method public static a(LX/0QB;)LX/0sg;
    .locals 1

    .prologue
    .line 152443
    invoke-static {p0}, LX/0sg;->b(LX/0QB;)LX/0sg;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/0sg;
    .locals 4

    .prologue
    .line 152439
    new-instance v3, LX/0sg;

    invoke-static {p0}, LX/0sh;->a(LX/0QB;)LX/0sh;

    move-result-object v0

    check-cast v0, LX/0si;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0}, LX/0sT;->a(LX/0QB;)LX/0sT;

    move-result-object v2

    check-cast v2, LX/0sT;

    invoke-direct {v3, v0, v1, v2}, LX/0sg;-><init>(LX/0si;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0sT;)V

    .line 152440
    return-object v3
.end method


# virtual methods
.method public final a()LX/2lk;
    .locals 5

    .prologue
    .line 152438
    new-instance v1, LX/2lj;

    iget-object v0, p0, LX/0sg;->b:LX/0t8;

    if-nez v0, :cond_0

    sget-object v0, LX/0t7;->a:LX/0t8;

    :goto_0
    iget-object v2, p0, LX/0sg;->a:LX/0si;

    iget-object v3, p0, LX/0sg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v4, p0, LX/0sg;->d:LX/0sT;

    invoke-direct {v1, v0, v2, v3, v4}, LX/2lj;-><init>(LX/0t8;LX/0si;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0sT;)V

    return-object v1

    :cond_0
    iget-object v0, p0, LX/0sg;->b:LX/0t8;

    goto :goto_0
.end method

.method public final a(Ljava/util/Collection;)LX/2lk;
    .locals 6

    .prologue
    .line 152437
    new-instance v0, LX/2lj;

    iget-object v1, p0, LX/0sg;->b:LX/0t8;

    if-nez v1, :cond_0

    sget-object v1, LX/0t7;->a:LX/0t8;

    :goto_0
    iget-object v2, p0, LX/0sg;->a:LX/0si;

    iget-object v3, p0, LX/0sg;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v4, p0, LX/0sg;->d:LX/0sT;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/2lj;-><init>(LX/0t8;LX/0si;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0sT;Ljava/util/Collection;)V

    return-object v0

    :cond_0
    iget-object v1, p0, LX/0sg;->b:LX/0t8;

    goto :goto_0
.end method
