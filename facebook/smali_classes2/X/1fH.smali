.class public final LX/1fH;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:LX/1fG;


# direct methods
.method public constructor <init>(LX/1fG;)V
    .locals 0

    .prologue
    .line 291154
    iput-object p1, p0, LX/1fH;->a:LX/1fG;

    invoke-direct {p0}, LX/1cC;-><init>()V

    return-void
.end method

.method private d()V
    .locals 10

    .prologue
    .line 291171
    sget-boolean v0, LX/1fG;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1fH;->a:LX/1fG;

    iget-object v0, v0, LX/1fG;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291172
    :cond_0
    return-void

    .line 291173
    :cond_1
    iget-object v0, p0, LX/1fH;->a:LX/1fG;

    iget-object v0, v0, LX/1fG;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 291174
    iget-object v0, p0, LX/1fH;->a:LX/1fG;

    iget-object v0, v0, LX/1fG;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 291175
    iget-object v0, p0, LX/1fH;->a:LX/1fG;

    iget-object v0, v0, LX/1fG;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6VT;

    .line 291176
    iget-wide v8, v0, LX/6VT;->b:J

    move-wide v4, v8

    .line 291177
    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 291178
    iget-wide v8, v0, LX/6VT;->a:J

    move-wide v4, v8

    .line 291179
    cmp-long v4, v4, v2

    if-gtz v4, :cond_0

    .line 291180
    iput-wide v2, v0, LX/6VT;->b:J

    .line 291181
    iget-object v4, p0, LX/1fH;->a:LX/1fG;

    iget v4, v4, LX/1fG;->c:I

    .line 291182
    iput v4, v0, LX/6VT;->c:I

    .line 291183
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 291166
    iget-object v0, p0, LX/1fH;->a:LX/1fG;

    const/4 v1, 0x0

    .line 291167
    iput v1, v0, LX/1fG;->c:I

    .line 291168
    sget-boolean v0, LX/1fG;->i:Z

    if-eqz v0, :cond_0

    .line 291169
    iget-object v0, p0, LX/1fH;->a:LX/1fG;

    iget-object v0, v0, LX/1fG;->f:Ljava/util/ArrayList;

    new-instance v1, LX/6VT;

    iget-object v2, p0, LX/1fH;->a:LX/1fG;

    iget-object v2, v2, LX/1fG;->g:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, LX/6VT;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291170
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 291162
    iget-object v0, p0, LX/1fH;->a:LX/1fG;

    const/4 v1, 0x1

    .line 291163
    iput v1, v0, LX/1fG;->c:I

    .line 291164
    invoke-direct {p0}, LX/1fH;->d()V

    .line 291165
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 291159
    iget-object v0, p0, LX/1fH;->a:LX/1fG;

    const/4 v1, 0x1

    .line 291160
    iput v1, v0, LX/1fG;->c:I

    .line 291161
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 291155
    iget-object v0, p0, LX/1fH;->a:LX/1fG;

    const/4 v1, 0x2

    .line 291156
    iput v1, v0, LX/1fG;->c:I

    .line 291157
    invoke-direct {p0}, LX/1fH;->d()V

    .line 291158
    return-void
.end method
