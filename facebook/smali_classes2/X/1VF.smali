.class public LX/1VF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile h:LX/1VF;


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/14w;

.field private final d:LX/1VG;

.field public final e:LX/0qn;

.field private final f:LX/1CK;

.field private final g:LX/0tM;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 257766
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSPIRATION_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSPIRATION_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/1VF;->b:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/14w;LX/1VG;LX/0qn;LX/1CK;LX/0tM;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 257758
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 257759
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIFE_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_SELL_PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/1VF;->a:LX/0Px;

    .line 257760
    iput-object p1, p0, LX/1VF;->c:LX/14w;

    .line 257761
    iput-object p2, p0, LX/1VF;->d:LX/1VG;

    .line 257762
    iput-object p3, p0, LX/1VF;->e:LX/0qn;

    .line 257763
    iput-object p4, p0, LX/1VF;->f:LX/1CK;

    .line 257764
    iput-object p5, p0, LX/1VF;->g:LX/0tM;

    .line 257765
    return-void
.end method

.method public static a(LX/0QB;)LX/1VF;
    .locals 9

    .prologue
    .line 257745
    sget-object v0, LX/1VF;->h:LX/1VF;

    if-nez v0, :cond_1

    .line 257746
    const-class v1, LX/1VF;

    monitor-enter v1

    .line 257747
    :try_start_0
    sget-object v0, LX/1VF;->h:LX/1VF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 257748
    if-eqz v2, :cond_0

    .line 257749
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 257750
    new-instance v3, LX/1VF;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v4

    check-cast v4, LX/14w;

    invoke-static {v0}, LX/1VG;->a(LX/0QB;)LX/1VG;

    move-result-object v5

    check-cast v5, LX/1VG;

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v6

    check-cast v6, LX/0qn;

    invoke-static {v0}, LX/1CK;->a(LX/0QB;)LX/1CK;

    move-result-object v7

    check-cast v7, LX/1CK;

    invoke-static {v0}, LX/0tM;->a(LX/0QB;)LX/0tM;

    move-result-object v8

    check-cast v8, LX/0tM;

    invoke-direct/range {v3 .. v8}, LX/1VF;-><init>(LX/14w;LX/1VG;LX/0qn;LX/1CK;LX/0tM;)V

    .line 257751
    move-object v0, v3

    .line 257752
    sput-object v0, LX/1VF;->h:LX/1VF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 257753
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 257754
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 257755
    :cond_1
    sget-object v0, LX/1VF;->h:LX/1VF;

    return-object v0

    .line 257756
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 257757
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/9A9;)Z
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "isPopularObjectsStory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 257731
    invoke-interface {p0}, LX/9A9;->d()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/9A9;->d()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v1, v2

    .line 257732
    :cond_1
    :goto_0
    return v1

    .line 257733
    :cond_2
    invoke-interface {p0}, LX/9A9;->d()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    .line 257734
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 257735
    if-eqz v4, :cond_5

    .line 257736
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v3, v4, v2, v0}, LX/15i;->g(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 257737
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_7

    .line 257738
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v3, v4, v2, v0}, LX/15i;->g(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 257739
    if-eqz v0, :cond_6

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_3
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->POPULAR_OBJECTS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v0, v3, :cond_1

    move v1, v2

    goto :goto_0

    .line 257740
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 257741
    :cond_3
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 257742
    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_2

    .line 257743
    :cond_6
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 257744
    goto :goto_3

    :cond_7
    move v1, v2

    goto :goto_0
.end method

.method public static a(Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;)Z
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "shouldDisplayProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 257714
    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;->k()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 257715
    :goto_0
    return v0

    .line 257716
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$ActorsModel;

    .line 257717
    if-nez v0, :cond_2

    move v0, v2

    :goto_1
    if-eqz v0, :cond_4

    move v0, v1

    .line 257718
    goto :goto_0

    .line 257719
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$ActorsModel;->b()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_3

    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1

    .line 257720
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;->gd_()Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$FeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_5

    move v0, v2

    .line 257721
    goto :goto_0

    .line 257722
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;->j()Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$MessageModel;

    move-result-object v0

    if-eqz v0, :cond_6

    move v0, v2

    .line 257723
    goto :goto_0

    .line 257724
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;->ge_()Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AttachedStoryModel;

    move-result-object v0

    if-eqz v0, :cond_7

    move v0, v1

    .line 257725
    goto :goto_0

    .line 257726
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;->e()Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;->e()Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;->e()Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {p0}, LX/14w;->a(LX/1WQ;)Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    .line 257727
    goto :goto_0

    .line 257728
    :cond_8
    invoke-static {p0}, LX/1VF;->a(LX/9A9;)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    .line 257729
    goto :goto_0

    :cond_9
    move v0, v2

    .line 257730
    goto :goto_0
.end method

.method public static a(Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 257705
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;->a()Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel$AllSubstoriesModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;->a()Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel$AllSubstoriesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel$AllSubstoriesModel;->a()I

    move-result v1

    if-le v1, v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;->a()Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel$AllSubstoriesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel$AllSubstoriesModel;->b()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_1

    .line 257706
    :cond_0
    return v2

    .line 257707
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;->b()LX/0Px;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->COMPACTNESS:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    invoke-virtual {v1, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;->b()LX/0Px;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->POST_CHANNEL:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    invoke-virtual {v1, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 257708
    :goto_0
    if-eqz v0, :cond_0

    .line 257709
    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;->a()Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel$AllSubstoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel$AllSubstoriesModel;->b()LX/0Px;

    move-result-object v3

    .line 257710
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel$AllSubstoriesModel$NodesModel;

    .line 257711
    invoke-virtual {v0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel$AllSubstoriesModel$NodesModel;->a()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel$AllSubstoriesModel$NodesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 257712
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 257713
    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 257704
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aS()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->V()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/14w;->j(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 257703
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, LX/1VF;->b:LX/0Px;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 257691
    invoke-static {p0}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 257692
    :goto_0
    return v0

    .line 257693
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v3

    .line 257694
    if-nez v3, :cond_1

    move v0, v1

    .line 257695
    goto :goto_0

    .line 257696
    :cond_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    .line 257697
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 257698
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIFE_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_SELL_PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v5, p0}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v5

    move v0, v5

    .line 257699
    if-eqz v0, :cond_2

    move v0, v1

    .line 257700
    goto :goto_0

    .line 257701
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 257702
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 257686
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ay()Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    move-result-object v1

    .line 257687
    if-eqz v1, :cond_0

    .line 257688
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->j()Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    move-result-object v1

    .line 257689
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 257690
    :cond_0
    return-object v0
.end method

.method public static e(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 257767
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v0

    if-lez v0, :cond_0

    invoke-static {p0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 257768
    :goto_0
    return v0

    .line 257769
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aO()LX/0Px;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->COMPACTNESS:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    invoke-virtual {v0, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 257770
    if-nez v0, :cond_2

    move v0, v1

    .line 257771
    goto :goto_0

    .line 257772
    :cond_2
    invoke-static {p0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v3

    .line 257773
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_5

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 257774
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v0, v5}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    move v0, v1

    .line 257775
    goto :goto_0

    .line 257776
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 257777
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static f(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 257683
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257684
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 257685
    invoke-static {v0}, LX/14w;->s(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 257675
    if-eqz p0, :cond_0

    .line 257676
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257677
    if-nez v0, :cond_1

    .line 257678
    :cond_0
    const/4 v0, 0x0

    .line 257679
    :goto_0
    return v0

    .line 257680
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257681
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 257682
    invoke-static {v0}, LX/1VF;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 257673
    const v0, -0x667efae1

    invoke-static {p0, v0}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 257674
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bD()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 2

    .prologue
    .line 257594
    if-nez p0, :cond_1

    .line 257595
    const/4 v0, 0x0

    .line 257596
    :cond_0
    :goto_0
    return-object v0

    .line 257597
    :cond_1
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 257598
    if-eqz v0, :cond_2

    .line 257599
    const v1, -0x67292209

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 257600
    if-nez v0, :cond_0

    .line 257601
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/1VF;->h(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 257670
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257671
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 257672
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aX()Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aX()Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;->a()Lcom/facebook/graphql/model/GraphQLFollowableTopic;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 257663
    invoke-static {p1}, LX/14w;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, LX/14w;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 257664
    invoke-static {p1}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 257665
    if-nez v0, :cond_0

    .line 257666
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257667
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 257668
    :cond_0
    iget-object v1, p0, LX/1VF;->e:LX/0qn;

    invoke-virtual {v1, v0}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 257669
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;Z)Z
    .locals 5
    .param p3    # LX/1SX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/feed/ui/api/FeedMenuHelper;",
            "Z)Z"
        }
    .end annotation

    .prologue
    .line 257645
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257646
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 257647
    if-eqz p3, :cond_5

    invoke-virtual {p3, v0}, LX/1SX;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1wH;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 257648
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 257649
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 257650
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 257651
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 257652
    invoke-static {p1}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 257653
    if-nez v3, :cond_1

    const/4 v3, 0x0

    .line 257654
    invoke-static {p1}, LX/182;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v4

    .line 257655
    if-eqz v4, :cond_0

    .line 257656
    invoke-interface {v4}, LX/0jT;->f()I

    move-result v4

    .line 257657
    const p0, -0x70a4a2e1

    if-ne v4, p0, :cond_0

    const/4 v3, 0x1

    .line 257658
    :cond_0
    move v3, v3

    .line 257659
    if-eqz v3, :cond_2

    :cond_1
    invoke-static {p2}, LX/14w;->n(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v2}, LX/1WP;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1WQ;

    move-result-object v2

    invoke-static {v2}, LX/14w;->a(LX/1WQ;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v1}, LX/1wE;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;

    move-result-object v2

    invoke-static {v2}, LX/1VF;->a(Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v1}, LX/1VF;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_2
    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 257660
    if-eqz v1, :cond_5

    if-nez p4, :cond_3

    invoke-static {v0}, LX/1VF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 257661
    :cond_3
    iget-object v1, p3, LX/1SX;->J:LX/1Sp;

    invoke-interface {v1}, LX/1Sp;->a()LX/0wD;

    move-result-object v1

    sget-object v2, LX/0wD;->PERMALINK:LX/0wD;

    if-ne v1, v2, :cond_7

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 257662
    if-nez v1, :cond_4

    invoke-static {p1}, LX/1VF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {v0}, LX/1VF;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    const/4 v1, 0x0

    goto :goto_0

    :cond_7
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 257613
    if-eqz p1, :cond_0

    .line 257614
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257615
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->y()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 257616
    :goto_0
    return v0

    .line 257617
    :cond_1
    invoke-virtual {p0, p1}, LX/1VF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1VF;->g:LX/0tM;

    invoke-virtual {v0}, LX/0tM;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 257618
    goto :goto_0

    .line 257619
    :cond_2
    iget-object v0, p0, LX/1VF;->g:LX/0tM;

    invoke-virtual {v0}, LX/0tM;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 257620
    iget-object v0, p0, LX/1VF;->c:LX/14w;

    invoke-virtual {v0, p1}, LX/14w;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v0

    .line 257621
    if-nez v0, :cond_3

    invoke-static {p1}, LX/182;->m(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p1}, LX/182;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {p1}, LX/182;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_4

    :cond_3
    move v0, v2

    .line 257622
    goto :goto_0

    .line 257623
    :cond_4
    invoke-static {p1}, LX/182;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 257624
    invoke-static {v0}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    .line 257625
    if-eqz v3, :cond_7

    :goto_1
    const/4 v4, 0x0

    .line 257626
    if-eqz v0, :cond_5

    .line 257627
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 257628
    if-eqz v3, :cond_5

    .line 257629
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 257630
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 257631
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 257632
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v3

    if-eqz v3, :cond_b

    :cond_5
    move v3, v4

    .line 257633
    :goto_2
    move v0, v3

    .line 257634
    if-nez v0, :cond_6

    iget-object v3, p0, LX/1VF;->f:LX/1CK;

    .line 257635
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257636
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v3, v0}, LX/1CK;->a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    move-object v0, p1

    goto :goto_1

    :cond_8
    move v0, v2

    goto :goto_0

    .line 257637
    :cond_9
    invoke-static {p1}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_a

    move v0, v2

    .line 257638
    goto/16 :goto_0

    .line 257639
    :cond_a
    iget-object v1, p0, LX/1VF;->f:LX/1CK;

    .line 257640
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257641
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v1, v0}, LX/1CK;->a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto/16 :goto_0

    .line 257642
    :cond_b
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 257643
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->s()Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    move-result-object v3

    .line 257644
    if-eqz v3, :cond_c

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->HIGH:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    if-ne v3, v5, :cond_c

    const/4 v3, 0x1

    goto :goto_2

    :cond_c
    move v3, v4

    goto :goto_2
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 257607
    iget-object v1, p0, LX/1VF;->d:LX/1VG;

    .line 257608
    iget-object v2, v1, LX/1VG;->a:LX/0Uh;

    const/16 v3, 0x57e

    const/4 p0, 0x0

    invoke-virtual {v2, v3, p0}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v1, v2

    .line 257609
    if-nez v1, :cond_1

    .line 257610
    :cond_0
    :goto_0
    return v0

    .line 257611
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aj()Lcom/facebook/graphql/model/GraphQLStoryInsights;

    move-result-object v1

    .line 257612
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryInsights;->m()I

    move-result v1

    if-gtz v1, :cond_3

    :cond_2
    invoke-static {p1}, LX/1Wq;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    if-ne v1, v2, :cond_3

    invoke-static {p1}, LX/1VF;->d(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 257602
    iget-object v0, p0, LX/1VF;->g:LX/0tM;

    invoke-virtual {v0}, LX/0tM;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1VF;->g:LX/0tM;

    invoke-virtual {v0}, LX/0tM;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 257603
    :cond_0
    iget-object v1, p0, LX/1VF;->f:LX/1CK;

    .line 257604
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257605
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v1, v0}, LX/1CK;->a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 257606
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
