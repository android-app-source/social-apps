.class public LX/1Vc;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Brr;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Brt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 264845
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1Vc;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Brt;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 264846
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 264847
    iput-object p1, p0, LX/1Vc;->b:LX/0Ot;

    .line 264848
    return-void
.end method

.method public static a(LX/0QB;)LX/1Vc;
    .locals 4

    .prologue
    .line 264849
    const-class v1, LX/1Vc;

    monitor-enter v1

    .line 264850
    :try_start_0
    sget-object v0, LX/1Vc;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 264851
    sput-object v2, LX/1Vc;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 264852
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264853
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 264854
    new-instance v3, LX/1Vc;

    const/16 p0, 0x1d20

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1Vc;-><init>(LX/0Ot;)V

    .line 264855
    move-object v0, v3

    .line 264856
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 264857
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Vc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264858
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 264859
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 264860
    check-cast p2, LX/Brs;

    .line 264861
    iget-object v0, p0, LX/1Vc;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/Brs;->a:Ljava/lang/String;

    .line 264862
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p0

    sget p2, LX/Brt;->a:I

    invoke-virtual {p0, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    sget-object p2, LX/Brt;->b:Landroid/text/Layout$Alignment;

    invoke-virtual {p0, p2}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object p0

    .line 264863
    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 264864
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 264865
    invoke-static {}, LX/1dS;->b()V

    .line 264866
    const/4 v0, 0x0

    return-object v0
.end method
