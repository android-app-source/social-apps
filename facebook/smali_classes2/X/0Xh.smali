.class public LX/0Xh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static volatile d:LX/0Xh;


# instance fields
.field private b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private c:Lcom/facebook/common/perftest/PerfTestConfig;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 79233
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "perfmarker_to_logcat"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0Xh;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/common/perftest/PerfTestConfig;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 79234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79235
    iput-object p1, p0, LX/0Xh;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 79236
    iput-object p2, p0, LX/0Xh;->c:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 79237
    return-void
.end method

.method public static a(LX/0QB;)LX/0Xh;
    .locals 5

    .prologue
    .line 79238
    sget-object v0, LX/0Xh;->d:LX/0Xh;

    if-nez v0, :cond_1

    .line 79239
    const-class v1, LX/0Xh;

    monitor-enter v1

    .line 79240
    :try_start_0
    sget-object v0, LX/0Xh;->d:LX/0Xh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 79241
    if-eqz v2, :cond_0

    .line 79242
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 79243
    new-instance p0, LX/0Xh;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v4

    check-cast v4, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-direct {p0, v3, v4}, LX/0Xh;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/common/perftest/PerfTestConfig;)V

    .line 79244
    move-object v0, p0

    .line 79245
    sput-object v0, LX/0Xh;->d:LX/0Xh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79246
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 79247
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 79248
    :cond_1
    sget-object v0, LX/0Xh;->d:LX/0Xh;

    return-object v0

    .line 79249
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 79250
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/03R;
    .locals 3

    .prologue
    .line 79251
    iget-object v0, p0, LX/0Xh;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79252
    iget-object v0, p0, LX/0Xh;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0Xh;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/03R;->YES:LX/03R;

    .line 79253
    :goto_0
    return-object v0

    .line 79254
    :cond_0
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 79255
    :cond_1
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 79256
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 79257
    sget-boolean v0, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->r:Z

    move v0, v0

    .line 79258
    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 79259
    sget-boolean v0, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->s:Z

    move v0, v0

    .line 79260
    return v0
.end method
