.class public LX/0ay;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile c:LX/0ay;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0b1;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86433
    const-class v0, LX/0ay;

    sput-object v0, LX/0ay;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0b1;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 86434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86435
    iput-object p1, p0, LX/0ay;->b:LX/0Ot;

    .line 86436
    return-void
.end method

.method public static a(LX/0QB;)LX/0ay;
    .locals 5

    .prologue
    .line 86437
    sget-object v0, LX/0ay;->c:LX/0ay;

    if-nez v0, :cond_1

    .line 86438
    const-class v1, LX/0ay;

    monitor-enter v1

    .line 86439
    :try_start_0
    sget-object v0, LX/0ay;->c:LX/0ay;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 86440
    if-eqz v2, :cond_0

    .line 86441
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 86442
    new-instance v3, LX/0ay;

    .line 86443
    new-instance v4, LX/0az;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-direct {v4, p0}, LX/0az;-><init>(LX/0QB;)V

    move-object v4, v4

    .line 86444
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-static {v4, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v4

    move-object v4, v4

    .line 86445
    invoke-direct {v3, v4}, LX/0ay;-><init>(LX/0Ot;)V

    .line 86446
    move-object v0, v3

    .line 86447
    sput-object v0, LX/0ay;->c:LX/0ay;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 86448
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 86449
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 86450
    :cond_1
    sget-object v0, LX/0ay;->c:LX/0ay;

    return-object v0

    .line 86451
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 86452
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 86453
    const-string v0, "INeedInitForEventBusRegister-RegisterEventBusSubscribers"

    const v1, -0x4b94c0e0

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 86454
    :try_start_0
    invoke-static {p0}, LX/0ay;->b(LX/0ay;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0b1;

    .line 86455
    iget-object v2, v0, LX/0b1;->a:LX/0b4;

    move-object v2, v2

    .line 86456
    invoke-virtual {v2, v0}, LX/0b4;->a(LX/0b2;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 86457
    :catchall_0
    move-exception v0

    const v1, 0x7920560a

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_0
    const v0, 0x53d3eadd

    invoke-static {v0}, LX/02m;->a(I)V

    .line 86458
    return-void
.end method

.method private static b(LX/0ay;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0b1;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86459
    const-string v0, "INeedInitForEventBusRegister-RegisterEventBusSubscribers#construct"

    const v1, 0x1ad0770c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 86460
    :try_start_0
    iget-object v0, p0, LX/0ay;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86461
    const v1, 0x4b61d745    # 1.4800709E7f

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0x3d8cbe89

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 0

    .prologue
    .line 86462
    invoke-direct {p0}, LX/0ay;->a()V

    .line 86463
    return-void
.end method
