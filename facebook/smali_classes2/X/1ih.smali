.class public LX/1ih;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile l:LX/1ih;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Z

.field public d:Z

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0ad;

.field public final h:LX/0Xl;

.field public i:LX/0Yb;

.field private final j:Ljava/util/concurrent/ExecutorService;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 298833
    const-class v0, LX/1ih;

    sput-object v0, LX/1ih;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/0Xl;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Or;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/checkpoint/gk/IsLoggedInNonBlockingCheckpointEnabled;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/checkpoint/gk/IsLoggedInBlockingCheckpointEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/0Xl;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 298834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298835
    iput-object p1, p0, LX/1ih;->g:LX/0ad;

    .line 298836
    iput-object p2, p0, LX/1ih;->h:LX/0Xl;

    .line 298837
    iput-object p3, p0, LX/1ih;->j:Ljava/util/concurrent/ExecutorService;

    .line 298838
    iput-object p4, p0, LX/1ih;->k:LX/0Ot;

    .line 298839
    iput-object p5, p0, LX/1ih;->e:LX/0Or;

    .line 298840
    iput-object p6, p0, LX/1ih;->f:LX/0Or;

    .line 298841
    iget-object p1, p0, LX/1ih;->h:LX/0Xl;

    invoke-interface {p1}, LX/0Xl;->a()LX/0YX;

    move-result-object p1

    const-string p2, "com.facebook.http.protocol.CHECKPOINT_API_EXCEPTION"

    new-instance p3, LX/1ii;

    invoke-direct {p3, p0}, LX/1ii;-><init>(LX/1ih;)V

    invoke-interface {p1, p2, p3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object p1

    invoke-interface {p1}, LX/0YX;->a()LX/0Yb;

    move-result-object p1

    iput-object p1, p0, LX/1ih;->i:LX/0Yb;

    .line 298842
    iget-object p1, p0, LX/1ih;->i:LX/0Yb;

    invoke-virtual {p1}, LX/0Yb;->b()V

    .line 298843
    return-void
.end method

.method public static a(LX/0QB;)LX/1ih;
    .locals 10

    .prologue
    .line 298844
    sget-object v0, LX/1ih;->l:LX/1ih;

    if-nez v0, :cond_1

    .line 298845
    const-class v1, LX/1ih;

    monitor-enter v1

    .line 298846
    :try_start_0
    sget-object v0, LX/1ih;->l:LX/1ih;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 298847
    if-eqz v2, :cond_0

    .line 298848
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 298849
    new-instance v3, LX/1ih;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v5

    check-cast v5, LX/0Xl;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    const/16 v7, 0xafd

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1462

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x1461

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/1ih;-><init>(LX/0ad;LX/0Xl;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Or;LX/0Or;)V

    .line 298850
    move-object v0, v3

    .line 298851
    sput-object v0, LX/1ih;->l:LX/1ih;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 298852
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 298853
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 298854
    :cond_1
    sget-object v0, LX/1ih;->l:LX/1ih;

    return-object v0

    .line 298855
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 298856
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/1ih;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 298857
    iget-object v0, p0, LX/1ih;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1ih;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/1ih;->c:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 298858
    const/4 v0, 0x0

    iput-object v0, p0, LX/1ih;->b:Ljava/lang/String;

    .line 298859
    iput-boolean v1, p0, LX/1ih;->c:Z

    .line 298860
    iput-boolean v1, p0, LX/1ih;->d:Z

    .line 298861
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 298862
    iget-object v0, p0, LX/1ih;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/1ih;->d:Z

    if-eqz v0, :cond_1

    .line 298863
    :cond_0
    invoke-virtual {p0}, LX/1ih;->b()V

    .line 298864
    :goto_0
    return-void

    .line 298865
    :cond_1
    new-instance v0, LX/4DJ;

    invoke-direct {v0}, LX/4DJ;-><init>()V

    iget-object v1, p0, LX/1ih;->b:Ljava/lang/String;

    .line 298866
    const-string v2, "flow_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 298867
    move-object v0, v0

    .line 298868
    new-instance v1, LX/Ehf;

    invoke-direct {v1}, LX/Ehf;-><init>()V

    move-object v1, v1

    .line 298869
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 298870
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 298871
    iget-object v0, p0, LX/1ih;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 298872
    new-instance v1, LX/Ehd;

    invoke-direct {v1, p0}, LX/Ehd;-><init>(LX/1ih;)V

    iget-object v2, p0, LX/1ih;->j:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
