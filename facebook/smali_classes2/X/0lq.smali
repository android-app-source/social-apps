.class public final enum LX/0lq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0lq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0lq;

.field public static final enum CANONICALIZE_FIELD_NAMES:LX/0lq;

.field public static final enum INTERN_FIELD_NAMES:LX/0lq;


# instance fields
.field private final _defaultState:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 130904
    new-instance v0, LX/0lq;

    const-string v1, "INTERN_FIELD_NAMES"

    invoke-direct {v0, v1, v3, v2}, LX/0lq;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0lq;->INTERN_FIELD_NAMES:LX/0lq;

    .line 130905
    new-instance v0, LX/0lq;

    const-string v1, "CANONICALIZE_FIELD_NAMES"

    invoke-direct {v0, v1, v2, v2}, LX/0lq;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0lq;->CANONICALIZE_FIELD_NAMES:LX/0lq;

    .line 130906
    const/4 v0, 0x2

    new-array v0, v0, [LX/0lq;

    sget-object v1, LX/0lq;->INTERN_FIELD_NAMES:LX/0lq;

    aput-object v1, v0, v3

    sget-object v1, LX/0lq;->CANONICALIZE_FIELD_NAMES:LX/0lq;

    aput-object v1, v0, v2

    sput-object v0, LX/0lq;->$VALUES:[LX/0lq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 130901
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 130902
    iput-boolean p3, p0, LX/0lq;->_defaultState:Z

    .line 130903
    return-void
.end method

.method public static collectDefaults()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 130891
    invoke-static {}, LX/0lq;->values()[LX/0lq;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 130892
    invoke-virtual {v4}, LX/0lq;->enabledByDefault()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 130893
    invoke-virtual {v4}, LX/0lq;->getMask()I

    move-result v4

    or-int/2addr v0, v4

    .line 130894
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 130895
    :cond_1
    return v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0lq;
    .locals 1

    .prologue
    .line 130900
    const-class v0, LX/0lq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0lq;

    return-object v0
.end method

.method public static values()[LX/0lq;
    .locals 1

    .prologue
    .line 130899
    sget-object v0, LX/0lq;->$VALUES:[LX/0lq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0lq;

    return-object v0
.end method


# virtual methods
.method public final enabledByDefault()Z
    .locals 1

    .prologue
    .line 130898
    iget-boolean v0, p0, LX/0lq;->_defaultState:Z

    return v0
.end method

.method public final enabledIn(I)Z
    .locals 1

    .prologue
    .line 130897
    invoke-virtual {p0}, LX/0lq;->getMask()I

    move-result v0

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getMask()I
    .locals 2

    .prologue
    .line 130896
    const/4 v0, 0x1

    invoke-virtual {p0}, LX/0lq;->ordinal()I

    move-result v1

    shl-int/2addr v0, v1

    return v0
.end method
