.class public LX/1mS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 313835
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a(Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/03R;
    .locals 2

    .prologue
    .line 313834
    const-class v1, LX/1mS;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1mI;->i:LX/0Tn;

    invoke-interface {p0, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;)LX/03R;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(LX/1mC;Lcom/facebook/prefs/shared/FbSharedPreferences;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 313833
    sget-object v0, LX/1mI;->g:LX/0Tn;

    invoke-virtual {p0}, LX/1mC;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1mC;)V
    .locals 4

    .prologue
    .line 313824
    const-class v1, LX/1mS;

    monitor-enter v1

    :try_start_0
    invoke-interface {p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, LX/1mI;->g:LX/0Tn;

    invoke-virtual {p1}, LX/1mC;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313825
    monitor-exit v1

    return-void

    .line 313826
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Lcom/facebook/prefs/shared/FbSharedPreferences;Z)V
    .locals 3

    .prologue
    .line 313830
    const-class v1, LX/1mS;

    monitor-enter v1

    :try_start_0
    invoke-interface {p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, LX/1mI;->i:LX/0Tn;

    invoke-interface {v0, v2, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313831
    monitor-exit v1

    return-void

    .line 313832
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized b(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1mC;)V
    .locals 4

    .prologue
    .line 313827
    const-class v1, LX/1mS;

    monitor-enter v1

    :try_start_0
    invoke-interface {p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, LX/1mI;->h:LX/0Tn;

    invoke-virtual {p1}, LX/1mC;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313828
    monitor-exit v1

    return-void

    .line 313829
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
