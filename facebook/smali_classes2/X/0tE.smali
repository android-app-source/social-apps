.class public LX/0tE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0ad;

.field private final b:LX/0tF;

.field public c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Integer;

.field public e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(LX/0ad;LX/0tF;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 154195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154196
    iput-object v0, p0, LX/0tE;->c:Ljava/lang/Boolean;

    .line 154197
    iput-object v0, p0, LX/0tE;->d:Ljava/lang/Integer;

    .line 154198
    iput-object v0, p0, LX/0tE;->e:Ljava/lang/Boolean;

    .line 154199
    iput-object p1, p0, LX/0tE;->a:LX/0ad;

    .line 154200
    iput-object p2, p0, LX/0tE;->b:LX/0tF;

    .line 154201
    return-void
.end method

.method public static a(LX/0QB;)LX/0tE;
    .locals 1

    .prologue
    .line 154194
    invoke-static {p0}, LX/0tE;->b(LX/0QB;)LX/0tE;

    move-result-object v0

    return-object v0
.end method

.method private b()I
    .locals 4

    .prologue
    .line 154189
    iget-object v0, p0, LX/0tE;->d:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 154190
    iget-object v0, p0, LX/0tE;->b:LX/0tF;

    const/4 v1, 0x3

    .line 154191
    invoke-virtual {v0}, LX/0tF;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/0tF;->a:LX/0ad;

    sget v3, LX/0wn;->af:I

    invoke-interface {v2, v3, v1}, LX/0ad;->a(II)I

    move-result v1

    :cond_0
    move v0, v1

    .line 154192
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/0tE;->d:Ljava/lang/Integer;

    .line 154193
    :cond_1
    iget-object v0, p0, LX/0tE;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static b(LX/0QB;)LX/0tE;
    .locals 3

    .prologue
    .line 154202
    new-instance v2, LX/0tE;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0}, LX/0tF;->a(LX/0QB;)LX/0tF;

    move-result-object v1

    check-cast v1, LX/0tF;

    invoke-direct {v2, v0, v1}, LX/0tE;-><init>(LX/0ad;LX/0tF;)V

    .line 154203
    return-object v2
.end method


# virtual methods
.method public final a(LX/0gW;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 154173
    const-string v0, "enable_ranked_replies"

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 154174
    const-string v0, "enable_private_reply"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 154175
    const-string v0, "enable_comment_replies_most_recent"

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 154176
    if-nez p2, :cond_1

    .line 154177
    const-string v0, "max_comment_replies"

    invoke-direct {p0}, LX/0tE;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 154178
    :goto_0
    invoke-virtual {p0, p1}, LX/0tE;->c(LX/0gW;)V

    .line 154179
    const-string v0, "enable_attachments_for_reply_previews"

    iget-object v1, p0, LX/0tE;->b:LX/0tF;

    invoke-virtual {v1}, LX/0tF;->a()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 154180
    const-string v0, "enable_feedback_for_reply_previews"

    iget-object v1, p0, LX/0tE;->b:LX/0tF;

    invoke-virtual {v1}, LX/0tF;->a()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 154181
    const-string v0, "enable_page_info_for_reply_connection"

    iget-object v1, p0, LX/0tE;->b:LX/0tF;

    invoke-virtual {v1}, LX/0tF;->b()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 154182
    const-string v0, "enable_comment_shares"

    .line 154183
    iget-object v1, p0, LX/0tE;->e:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 154184
    iget-object v1, p0, LX/0tE;->a:LX/0ad;

    sget-short v2, LX/0wn;->u:S

    const/4 p2, 0x0

    invoke-interface {v1, v2, p2}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, LX/0tE;->e:Ljava/lang/Boolean;

    .line 154185
    :cond_0
    iget-object v1, p0, LX/0tE;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v1, v1

    .line 154186
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 154187
    return-void

    .line 154188
    :cond_1
    const-string v0, "surround_reply_id"

    invoke-virtual {p1, v0, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "num_before_surround_reply"

    invoke-direct {p0}, LX/0tE;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    div-int/lit8 v2, v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "surround_max_comment_replies"

    invoke-direct {p0}, LX/0tE;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    goto :goto_0
.end method

.method public final a(LX/0gW;Z)V
    .locals 1

    .prologue
    .line 154171
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, LX/0tE;->a(LX/0gW;Ljava/lang/String;Z)V

    .line 154172
    return-void
.end method

.method public final b(LX/0gW;)V
    .locals 1

    .prologue
    .line 154169
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/0tE;->a(LX/0gW;Z)V

    .line 154170
    return-void
.end method

.method public final c(LX/0gW;)V
    .locals 4

    .prologue
    .line 154163
    const-string v0, "include_replies_in_total_count"

    .line 154164
    iget-object v1, p0, LX/0tE;->c:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 154165
    iget-object v1, p0, LX/0tE;->a:LX/0ad;

    sget-short v2, LX/0wg;->i:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, LX/0tE;->c:Ljava/lang/Boolean;

    .line 154166
    :cond_0
    iget-object v1, p0, LX/0tE;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v1, v1

    .line 154167
    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 154168
    return-void
.end method
