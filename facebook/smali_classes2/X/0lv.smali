.class public final LX/0lv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0lv;

.field public final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/0lw;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Z

.field public d:I

.field public e:I

.field public f:I

.field public g:[I

.field public h:[LX/0lx;

.field public i:[LX/26S;

.field public j:I

.field public k:I

.field private final l:I

.field private transient m:Z

.field private n:Z

.field private o:Z

.field private p:Z


# direct methods
.method private constructor <init>(IZI)V
    .locals 2

    .prologue
    const/16 v0, 0x10

    .line 131330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131331
    const/4 v1, 0x0

    iput-object v1, p0, LX/0lv;->a:LX/0lv;

    .line 131332
    iput p3, p0, LX/0lv;->l:I

    .line 131333
    iput-boolean p2, p0, LX/0lv;->c:Z

    .line 131334
    if-ge p1, v0, :cond_1

    move p1, v0

    .line 131335
    :cond_0
    :goto_0
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {p1}, LX/0lv;->b(I)LX/0lw;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/0lv;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 131336
    return-void

    .line 131337
    :cond_1
    add-int/lit8 v1, p1, -0x1

    and-int/2addr v1, p1

    if-eqz v1, :cond_0

    .line 131338
    :goto_1
    if-ge v0, p1, :cond_2

    .line 131339
    add-int/2addr v0, v0

    goto :goto_1

    :cond_2
    move p1, v0

    .line 131340
    goto :goto_0
.end method

.method private constructor <init>(LX/0lv;ZILX/0lw;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 131341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131342
    iput-object p1, p0, LX/0lv;->a:LX/0lv;

    .line 131343
    iput p3, p0, LX/0lv;->l:I

    .line 131344
    iput-boolean p2, p0, LX/0lv;->c:Z

    .line 131345
    const/4 v0, 0x0

    iput-object v0, p0, LX/0lv;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 131346
    iget v0, p4, LX/0lw;->a:I

    iput v0, p0, LX/0lv;->d:I

    .line 131347
    iget v0, p4, LX/0lw;->b:I

    iput v0, p0, LX/0lv;->f:I

    .line 131348
    iget-object v0, p4, LX/0lw;->c:[I

    iput-object v0, p0, LX/0lv;->g:[I

    .line 131349
    iget-object v0, p4, LX/0lw;->d:[LX/0lx;

    iput-object v0, p0, LX/0lv;->h:[LX/0lx;

    .line 131350
    iget-object v0, p4, LX/0lw;->e:[LX/26S;

    iput-object v0, p0, LX/0lv;->i:[LX/26S;

    .line 131351
    iget v0, p4, LX/0lw;->f:I

    iput v0, p0, LX/0lv;->j:I

    .line 131352
    iget v0, p4, LX/0lw;->g:I

    iput v0, p0, LX/0lv;->k:I

    .line 131353
    iget v0, p4, LX/0lw;->h:I

    iput v0, p0, LX/0lv;->e:I

    .line 131354
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0lv;->m:Z

    .line 131355
    iput-boolean v1, p0, LX/0lv;->n:Z

    .line 131356
    iput-boolean v1, p0, LX/0lv;->o:Z

    .line 131357
    iput-boolean v1, p0, LX/0lv;->p:Z

    .line 131358
    return-void
.end method

.method public static a()LX/0lv;
    .locals 4

    .prologue
    .line 131359
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 131360
    long-to-int v2, v0

    const/16 v3, 0x20

    ushr-long/2addr v0, v3

    long-to-int v0, v0

    add-int/2addr v0, v2

    or-int/lit8 v0, v0, 0x1

    .line 131361
    invoke-static {v0}, LX/0lv;->c(I)LX/0lv;

    move-result-object v0

    return-object v0
.end method

.method private static a(ILjava/lang/String;II)LX/0lx;
    .locals 1

    .prologue
    .line 131362
    if-nez p3, :cond_0

    .line 131363
    new-instance v0, LX/2CO;

    invoke-direct {v0, p1, p0, p2}, LX/2CO;-><init>(Ljava/lang/String;II)V

    .line 131364
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/1pQ;

    invoke-direct {v0, p1, p0, p2, p3}, LX/1pQ;-><init>(Ljava/lang/String;III)V

    goto :goto_0
.end method

.method private static a(ILjava/lang/String;[II)LX/0lx;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 131365
    const/4 v0, 0x4

    if-ge p3, v0, :cond_0

    .line 131366
    packed-switch p3, :pswitch_data_0

    .line 131367
    :cond_0
    new-array v2, p3, [I

    move v0, v1

    .line 131368
    :goto_0
    if-ge v0, p3, :cond_1

    .line 131369
    aget v1, p2, v0

    aput v1, v2, v0

    .line 131370
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 131371
    :pswitch_0
    new-instance v0, LX/2CO;

    aget v1, p2, v1

    invoke-direct {v0, p1, p0, v1}, LX/2CO;-><init>(Ljava/lang/String;II)V

    .line 131372
    :goto_1
    return-object v0

    .line 131373
    :pswitch_1
    new-instance v0, LX/1pQ;

    aget v1, p2, v1

    aget v2, p2, v2

    invoke-direct {v0, p1, p0, v1, v2}, LX/1pQ;-><init>(Ljava/lang/String;III)V

    goto :goto_1

    .line 131374
    :pswitch_2
    new-instance v0, LX/263;

    aget v3, p2, v1

    aget v4, p2, v2

    const/4 v1, 0x2

    aget v5, p2, v1

    move-object v1, p1

    move v2, p0

    invoke-direct/range {v0 .. v5}, LX/263;-><init>(Ljava/lang/String;IIII)V

    goto :goto_1

    .line 131375
    :cond_1
    new-instance v0, LX/1pO;

    invoke-direct {v0, p1, p0, v2, p3}, LX/1pO;-><init>(Ljava/lang/String;I[II)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(ILX/0lx;)V
    .locals 7

    .prologue
    const/16 v6, 0xff

    const/4 v5, 0x1

    .line 131376
    iget-boolean v0, p0, LX/0lv;->n:Z

    if-eqz v0, :cond_0

    .line 131377
    invoke-direct {p0}, LX/0lv;->h()V

    .line 131378
    :cond_0
    iget-boolean v0, p0, LX/0lv;->m:Z

    if-eqz v0, :cond_1

    .line 131379
    invoke-direct {p0}, LX/0lv;->e()V

    .line 131380
    :cond_1
    iget v0, p0, LX/0lv;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0lv;->d:I

    .line 131381
    iget v0, p0, LX/0lv;->f:I

    and-int v1, p1, v0

    .line 131382
    iget-object v0, p0, LX/0lv;->h:[LX/0lx;

    aget-object v0, v0, v1

    if-nez v0, :cond_5

    .line 131383
    iget-object v0, p0, LX/0lv;->g:[I

    shl-int/lit8 v2, p1, 0x8

    aput v2, v0, v1

    .line 131384
    iget-boolean v0, p0, LX/0lv;->o:Z

    if-eqz v0, :cond_2

    .line 131385
    invoke-direct {p0}, LX/0lv;->j()V

    .line 131386
    :cond_2
    iget-object v0, p0, LX/0lv;->h:[LX/0lx;

    aput-object p2, v0, v1

    .line 131387
    :cond_3
    :goto_0
    iget-object v0, p0, LX/0lv;->g:[I

    array-length v0, v0

    .line 131388
    iget v1, p0, LX/0lv;->d:I

    shr-int/lit8 v2, v0, 0x1

    if-le v1, v2, :cond_4

    .line 131389
    shr-int/lit8 v1, v0, 0x2

    .line 131390
    iget v2, p0, LX/0lv;->d:I

    sub-int/2addr v0, v1

    if-le v2, v0, :cond_a

    .line 131391
    iput-boolean v5, p0, LX/0lv;->m:Z

    .line 131392
    :cond_4
    :goto_1
    return-void

    .line 131393
    :cond_5
    iget-boolean v0, p0, LX/0lv;->p:Z

    if-eqz v0, :cond_6

    .line 131394
    invoke-direct {p0}, LX/0lv;->i()V

    .line 131395
    :cond_6
    iget v0, p0, LX/0lv;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0lv;->j:I

    .line 131396
    iget-object v0, p0, LX/0lv;->g:[I

    aget v2, v0, v1

    .line 131397
    and-int/lit16 v0, v2, 0xff

    .line 131398
    if-nez v0, :cond_9

    .line 131399
    iget v0, p0, LX/0lv;->k:I

    const/16 v3, 0xfe

    if-gt v0, v3, :cond_8

    .line 131400
    iget v0, p0, LX/0lv;->k:I

    .line 131401
    iget v3, p0, LX/0lv;->k:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/0lv;->k:I

    .line 131402
    iget-object v3, p0, LX/0lv;->i:[LX/26S;

    array-length v3, v3

    if-lt v0, v3, :cond_7

    .line 131403
    invoke-direct {p0}, LX/0lv;->k()V

    .line 131404
    :cond_7
    :goto_2
    iget-object v3, p0, LX/0lv;->g:[I

    and-int/lit16 v2, v2, -0x100

    add-int/lit8 v4, v0, 0x1

    or-int/2addr v2, v4

    aput v2, v3, v1

    .line 131405
    :goto_3
    new-instance v1, LX/26S;

    iget-object v2, p0, LX/0lv;->i:[LX/26S;

    aget-object v2, v2, v0

    invoke-direct {v1, p2, v2}, LX/26S;-><init>(LX/0lx;LX/26S;)V

    .line 131406
    iget-object v2, p0, LX/0lv;->i:[LX/26S;

    aput-object v1, v2, v0

    .line 131407
    iget v0, v1, LX/26S;->c:I

    move v0, v0

    .line 131408
    iget v1, p0, LX/0lv;->e:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/0lv;->e:I

    .line 131409
    iget v0, p0, LX/0lv;->e:I

    if-le v0, v6, :cond_3

    .line 131410
    invoke-direct {p0, v6}, LX/0lv;->e(I)V

    goto :goto_0

    .line 131411
    :cond_8
    invoke-direct {p0}, LX/0lv;->g()I

    move-result v0

    goto :goto_2

    .line 131412
    :cond_9
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 131413
    :cond_a
    iget v0, p0, LX/0lv;->j:I

    if-lt v0, v1, :cond_4

    .line 131414
    iput-boolean v5, p0, LX/0lv;->m:Z

    goto :goto_1
.end method

.method private a(LX/0lw;)V
    .locals 3

    .prologue
    .line 131444
    iget v1, p1, LX/0lw;->a:I

    .line 131445
    iget-object v0, p0, LX/0lv;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lw;

    .line 131446
    iget v2, v0, LX/0lw;->a:I

    if-gt v1, v2, :cond_0

    .line 131447
    :goto_0
    return-void

    .line 131448
    :cond_0
    const/16 v2, 0x1770

    if-gt v1, v2, :cond_1

    iget v1, p1, LX/0lw;->h:I

    const/16 v2, 0x3f

    if-le v1, v2, :cond_2

    .line 131449
    :cond_1
    const/16 v1, 0x40

    invoke-static {v1}, LX/0lv;->b(I)LX/0lw;

    move-result-object p1

    .line 131450
    :cond_2
    iget-object v1, p0, LX/0lv;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private b(II)I
    .locals 2

    .prologue
    .line 131415
    ushr-int/lit8 v0, p1, 0xf

    xor-int/2addr v0, p1

    .line 131416
    mul-int/lit8 v1, p2, 0x21

    add-int/2addr v0, v1

    .line 131417
    iget v1, p0, LX/0lv;->l:I

    xor-int/2addr v0, v1

    .line 131418
    ushr-int/lit8 v1, v0, 0x7

    add-int/2addr v0, v1

    .line 131419
    return v0
.end method

.method private b([II)I
    .locals 3

    .prologue
    const/4 v0, 0x3

    .line 131420
    if-ge p2, v0, :cond_0

    .line 131421
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 131422
    :cond_0
    const/4 v1, 0x0

    aget v1, p1, v1

    iget v2, p0, LX/0lv;->l:I

    xor-int/2addr v1, v2

    .line 131423
    ushr-int/lit8 v2, v1, 0x9

    add-int/2addr v1, v2

    .line 131424
    mul-int/lit8 v1, v1, 0x21

    .line 131425
    const/4 v2, 0x1

    aget v2, p1, v2

    add-int/2addr v1, v2

    .line 131426
    const v2, 0x1003f

    mul-int/2addr v1, v2

    .line 131427
    ushr-int/lit8 v2, v1, 0xf

    add-int/2addr v1, v2

    .line 131428
    const/4 v2, 0x2

    aget v2, p1, v2

    xor-int/2addr v1, v2

    .line 131429
    ushr-int/lit8 v2, v1, 0x11

    add-int/2addr v1, v2

    .line 131430
    :goto_0
    if-ge v0, p2, :cond_1

    .line 131431
    mul-int/lit8 v1, v1, 0x1f

    aget v2, p1, v0

    xor-int/2addr v1, v2

    .line 131432
    ushr-int/lit8 v2, v1, 0x3

    add-int/2addr v1, v2

    .line 131433
    shl-int/lit8 v2, v1, 0x7

    xor-int/2addr v1, v2

    .line 131434
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 131435
    :cond_1
    ushr-int/lit8 v0, v1, 0xf

    add-int/2addr v0, v1

    .line 131436
    shl-int/lit8 v1, v0, 0x9

    xor-int/2addr v0, v1

    .line 131437
    return v0
.end method

.method private static b(I)LX/0lw;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 131438
    new-instance v0, LX/0lw;

    add-int/lit8 v2, p0, -0x1

    new-array v3, p0, [I

    new-array v4, p0, [LX/0lx;

    const/4 v5, 0x0

    move v6, v1

    move v7, v1

    move v8, v1

    invoke-direct/range {v0 .. v8}, LX/0lw;-><init>(II[I[LX/0lx;[LX/26S;III)V

    return-object v0
.end method

.method private static c(I)LX/0lv;
    .locals 3

    .prologue
    .line 131439
    new-instance v0, LX/0lv;

    const/16 v1, 0x40

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, p0}, LX/0lv;-><init>(IZI)V

    return-object v0
.end method

.method private d(I)I
    .locals 2

    .prologue
    .line 131440
    iget v0, p0, LX/0lv;->l:I

    xor-int/2addr v0, p1

    .line 131441
    ushr-int/lit8 v1, v0, 0xf

    add-int/2addr v0, v1

    .line 131442
    ushr-int/lit8 v1, v0, 0x9

    xor-int/2addr v0, v1

    .line 131443
    return v0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 131270
    iget-boolean v0, p0, LX/0lv;->n:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 131271
    iput-boolean v1, p0, LX/0lv;->m:Z

    .line 131272
    iput-boolean v1, p0, LX/0lv;->o:Z

    .line 131273
    iget-object v0, p0, LX/0lv;->g:[I

    .line 131274
    array-length v3, v0

    .line 131275
    add-int v0, v3, v3

    .line 131276
    const/high16 v2, 0x10000

    if-le v0, v2, :cond_1

    .line 131277
    invoke-direct {p0}, LX/0lv;->f()V

    .line 131278
    :cond_0
    :goto_0
    return-void

    .line 131279
    :cond_1
    new-array v2, v0, [I

    iput-object v2, p0, LX/0lv;->g:[I

    .line 131280
    add-int/lit8 v2, v0, -0x1

    iput v2, p0, LX/0lv;->f:I

    .line 131281
    iget-object v4, p0, LX/0lv;->h:[LX/0lx;

    .line 131282
    new-array v0, v0, [LX/0lx;

    iput-object v0, p0, LX/0lv;->h:[LX/0lx;

    move v2, v1

    move v0, v1

    .line 131283
    :goto_1
    if-ge v2, v3, :cond_3

    .line 131284
    aget-object v5, v4, v2

    .line 131285
    if-eqz v5, :cond_2

    .line 131286
    add-int/lit8 v0, v0, 0x1

    .line 131287
    invoke-virtual {v5}, LX/0lx;->hashCode()I

    move-result v6

    .line 131288
    iget v7, p0, LX/0lv;->f:I

    and-int/2addr v7, v6

    .line 131289
    iget-object v8, p0, LX/0lv;->h:[LX/0lx;

    aput-object v5, v8, v7

    .line 131290
    iget-object v5, p0, LX/0lv;->g:[I

    shl-int/lit8 v6, v6, 0x8

    aput v6, v5, v7

    .line 131291
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 131292
    :cond_3
    iget v5, p0, LX/0lv;->k:I

    .line 131293
    if-nez v5, :cond_4

    .line 131294
    iput v1, p0, LX/0lv;->e:I

    goto :goto_0

    .line 131295
    :cond_4
    iput v1, p0, LX/0lv;->j:I

    .line 131296
    iput v1, p0, LX/0lv;->k:I

    .line 131297
    iput-boolean v1, p0, LX/0lv;->p:Z

    .line 131298
    iget-object v6, p0, LX/0lv;->i:[LX/26S;

    .line 131299
    array-length v2, v6

    new-array v2, v2, [LX/26S;

    iput-object v2, p0, LX/0lv;->i:[LX/26S;

    move v4, v1

    move v2, v0

    .line 131300
    :goto_2
    if-ge v4, v5, :cond_a

    .line 131301
    aget-object v0, v6, v4

    move-object v12, v0

    move v0, v2

    move-object v2, v12

    :goto_3
    if-eqz v2, :cond_9

    .line 131302
    add-int/lit8 v3, v0, 0x1

    .line 131303
    iget-object v7, v2, LX/26S;->a:LX/0lx;

    .line 131304
    invoke-virtual {v7}, LX/0lx;->hashCode()I

    move-result v0

    .line 131305
    iget v8, p0, LX/0lv;->f:I

    and-int/2addr v8, v0

    .line 131306
    iget-object v9, p0, LX/0lv;->g:[I

    aget v9, v9, v8

    .line 131307
    iget-object v10, p0, LX/0lv;->h:[LX/0lx;

    aget-object v10, v10, v8

    if-nez v10, :cond_5

    .line 131308
    iget-object v9, p0, LX/0lv;->g:[I

    shl-int/lit8 v0, v0, 0x8

    aput v0, v9, v8

    .line 131309
    iget-object v0, p0, LX/0lv;->h:[LX/0lx;

    aput-object v7, v0, v8

    move v0, v1

    .line 131310
    :goto_4
    iget-object v1, v2, LX/26S;->b:LX/26S;

    move-object v2, v1

    move v1, v0

    move v0, v3

    goto :goto_3

    .line 131311
    :cond_5
    iget v0, p0, LX/0lv;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0lv;->j:I

    .line 131312
    and-int/lit16 v0, v9, 0xff

    .line 131313
    if-nez v0, :cond_8

    .line 131314
    iget v0, p0, LX/0lv;->k:I

    const/16 v10, 0xfe

    if-gt v0, v10, :cond_7

    .line 131315
    iget v0, p0, LX/0lv;->k:I

    .line 131316
    iget v10, p0, LX/0lv;->k:I

    add-int/lit8 v10, v10, 0x1

    iput v10, p0, LX/0lv;->k:I

    .line 131317
    iget-object v10, p0, LX/0lv;->i:[LX/26S;

    array-length v10, v10

    if-lt v0, v10, :cond_6

    .line 131318
    invoke-direct {p0}, LX/0lv;->k()V

    .line 131319
    :cond_6
    :goto_5
    iget-object v10, p0, LX/0lv;->g:[I

    and-int/lit16 v9, v9, -0x100

    add-int/lit8 v11, v0, 0x1

    or-int/2addr v9, v11

    aput v9, v10, v8

    .line 131320
    :goto_6
    new-instance v8, LX/26S;

    iget-object v9, p0, LX/0lv;->i:[LX/26S;

    aget-object v9, v9, v0

    invoke-direct {v8, v7, v9}, LX/26S;-><init>(LX/0lx;LX/26S;)V

    .line 131321
    iget-object v7, p0, LX/0lv;->i:[LX/26S;

    aput-object v8, v7, v0

    .line 131322
    iget v0, v8, LX/26S;->c:I

    move v0, v0

    .line 131323
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_4

    .line 131324
    :cond_7
    invoke-direct {p0}, LX/0lv;->g()I

    move-result v0

    goto :goto_5

    .line 131325
    :cond_8
    add-int/lit8 v0, v0, -0x1

    goto :goto_6

    .line 131326
    :cond_9
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v0

    goto :goto_2

    .line 131327
    :cond_a
    iput v1, p0, LX/0lv;->e:I

    .line 131328
    iget v0, p0, LX/0lv;->d:I

    if-eq v2, v0, :cond_0

    .line 131329
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Internal error: count after rehash "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; should be "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/0lv;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private e(I)V
    .locals 3

    .prologue
    .line 131148
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Longest collision chain in symbol table (of size "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/0lv;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") now exceeds maximum, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -- suspect a DoS attack based on hash collisions"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 131149
    iput v1, p0, LX/0lv;->d:I

    .line 131150
    iput v1, p0, LX/0lv;->e:I

    .line 131151
    iget-object v0, p0, LX/0lv;->g:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 131152
    iget-object v0, p0, LX/0lv;->h:[LX/0lx;

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 131153
    iget-object v0, p0, LX/0lv;->i:[LX/26S;

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 131154
    iput v1, p0, LX/0lv;->j:I

    .line 131155
    iput v1, p0, LX/0lv;->k:I

    .line 131156
    return-void
.end method

.method private g()I
    .locals 6

    .prologue
    .line 131157
    iget-object v4, p0, LX/0lv;->i:[LX/26S;

    .line 131158
    const v3, 0x7fffffff

    .line 131159
    const/4 v0, -0x1

    .line 131160
    const/4 v1, 0x0

    iget v5, p0, LX/0lv;->k:I

    :goto_0
    if-ge v1, v5, :cond_1

    .line 131161
    aget-object v2, v4, v1

    .line 131162
    iget p0, v2, LX/26S;->c:I

    move v2, p0

    .line 131163
    if-ge v2, v3, :cond_2

    .line 131164
    const/4 v0, 0x1

    if-ne v2, v0, :cond_0

    .line 131165
    :goto_1
    return v1

    :cond_0
    move v0, v1

    .line 131166
    :goto_2
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    goto :goto_0

    :cond_1
    move v1, v0

    .line 131167
    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2
.end method

.method private h()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 131168
    iget-object v0, p0, LX/0lv;->g:[I

    .line 131169
    iget-object v1, p0, LX/0lv;->g:[I

    array-length v1, v1

    .line 131170
    new-array v2, v1, [I

    iput-object v2, p0, LX/0lv;->g:[I

    .line 131171
    iget-object v2, p0, LX/0lv;->g:[I

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 131172
    iput-boolean v3, p0, LX/0lv;->n:Z

    .line 131173
    return-void
.end method

.method private i()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 131174
    iget-object v0, p0, LX/0lv;->i:[LX/26S;

    .line 131175
    if-nez v0, :cond_0

    .line 131176
    const/16 v0, 0x20

    new-array v0, v0, [LX/26S;

    iput-object v0, p0, LX/0lv;->i:[LX/26S;

    .line 131177
    :goto_0
    iput-boolean v3, p0, LX/0lv;->p:Z

    .line 131178
    return-void

    .line 131179
    :cond_0
    array-length v1, v0

    .line 131180
    new-array v2, v1, [LX/26S;

    iput-object v2, p0, LX/0lv;->i:[LX/26S;

    .line 131181
    iget-object v2, p0, LX/0lv;->i:[LX/26S;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method private j()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 131182
    iget-object v0, p0, LX/0lv;->h:[LX/0lx;

    .line 131183
    array-length v1, v0

    .line 131184
    new-array v2, v1, [LX/0lx;

    iput-object v2, p0, LX/0lv;->h:[LX/0lx;

    .line 131185
    iget-object v2, p0, LX/0lv;->h:[LX/0lx;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 131186
    iput-boolean v3, p0, LX/0lv;->o:Z

    .line 131187
    return-void
.end method

.method private k()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 131188
    iget-object v0, p0, LX/0lv;->i:[LX/26S;

    .line 131189
    array-length v1, v0

    .line 131190
    add-int v2, v1, v1

    new-array v2, v2, [LX/26S;

    iput-object v2, p0, LX/0lv;->i:[LX/26S;

    .line 131191
    iget-object v2, p0, LX/0lv;->i:[LX/26S;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 131192
    return-void
.end method


# virtual methods
.method public final a(Z)LX/0lv;
    .locals 3

    .prologue
    .line 131193
    new-instance v1, LX/0lv;

    iget v2, p0, LX/0lv;->l:I

    iget-object v0, p0, LX/0lv;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lw;

    invoke-direct {v1, p0, p1, v2, v0}, LX/0lv;-><init>(LX/0lv;ZILX/0lw;)V

    return-object v1
.end method

.method public final a(I)LX/0lx;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 131194
    invoke-direct {p0, p1}, LX/0lv;->d(I)I

    move-result v2

    .line 131195
    iget v1, p0, LX/0lv;->f:I

    and-int/2addr v1, v2

    .line 131196
    iget-object v3, p0, LX/0lv;->g:[I

    aget v3, v3, v1

    .line 131197
    shr-int/lit8 v4, v3, 0x8

    xor-int/2addr v4, v2

    shl-int/lit8 v4, v4, 0x8

    if-nez v4, :cond_2

    .line 131198
    iget-object v4, p0, LX/0lv;->h:[LX/0lx;

    aget-object v1, v4, v1

    .line 131199
    if-nez v1, :cond_1

    .line 131200
    :cond_0
    :goto_0
    return-object v0

    .line 131201
    :cond_1
    invoke-virtual {v1, p1}, LX/0lx;->a(I)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v0, v1

    .line 131202
    goto :goto_0

    .line 131203
    :cond_2
    if-eqz v3, :cond_0

    .line 131204
    :cond_3
    and-int/lit16 v1, v3, 0xff

    .line 131205
    if-lez v1, :cond_0

    .line 131206
    add-int/lit8 v1, v1, -0x1

    .line 131207
    iget-object v3, p0, LX/0lv;->i:[LX/26S;

    aget-object v1, v3, v1

    .line 131208
    if-eqz v1, :cond_0

    .line 131209
    const/4 v0, 0x0

    invoke-virtual {v1, v2, p1, v0}, LX/26S;->a(III)LX/0lx;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(II)LX/0lx;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 131210
    if-nez p2, :cond_0

    invoke-direct {p0, p1}, LX/0lv;->d(I)I

    move-result v0

    .line 131211
    :goto_0
    iget v2, p0, LX/0lv;->f:I

    and-int/2addr v2, v0

    .line 131212
    iget-object v3, p0, LX/0lv;->g:[I

    aget v3, v3, v2

    .line 131213
    shr-int/lit8 v4, v3, 0x8

    xor-int/2addr v4, v0

    shl-int/lit8 v4, v4, 0x8

    if-nez v4, :cond_2

    .line 131214
    iget-object v4, p0, LX/0lv;->h:[LX/0lx;

    aget-object v2, v4, v2

    .line 131215
    if-nez v2, :cond_1

    move-object v0, v1

    .line 131216
    :goto_1
    return-object v0

    .line 131217
    :cond_0
    invoke-direct {p0, p1, p2}, LX/0lv;->b(II)I

    move-result v0

    goto :goto_0

    .line 131218
    :cond_1
    invoke-virtual {v2, p1, p2}, LX/0lx;->a(II)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v0, v2

    .line 131219
    goto :goto_1

    .line 131220
    :cond_2
    if-nez v3, :cond_3

    move-object v0, v1

    .line 131221
    goto :goto_1

    .line 131222
    :cond_3
    and-int/lit16 v2, v3, 0xff

    .line 131223
    if-lez v2, :cond_4

    .line 131224
    add-int/lit8 v2, v2, -0x1

    .line 131225
    iget-object v3, p0, LX/0lv;->i:[LX/26S;

    aget-object v2, v3, v2

    .line 131226
    if-eqz v2, :cond_4

    .line 131227
    invoke-virtual {v2, v0, p1, p2}, LX/26S;->a(III)LX/0lx;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 131228
    goto :goto_1
.end method

.method public final a(Ljava/lang/String;II)LX/0lx;
    .locals 2

    .prologue
    .line 131229
    iget-boolean v0, p0, LX/0lv;->c:Z

    if-eqz v0, :cond_0

    .line 131230
    sget-object v0, LX/16J;->a:LX/16J;

    invoke-virtual {v0, p1}, LX/16J;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 131231
    :cond_0
    if-nez p3, :cond_1

    invoke-direct {p0, p2}, LX/0lv;->d(I)I

    move-result v0

    .line 131232
    :goto_0
    invoke-static {v0, p1, p2, p3}, LX/0lv;->a(ILjava/lang/String;II)LX/0lx;

    move-result-object v1

    .line 131233
    invoke-direct {p0, v0, v1}, LX/0lv;->a(ILX/0lx;)V

    .line 131234
    return-object v1

    .line 131235
    :cond_1
    invoke-direct {p0, p2, p3}, LX/0lv;->b(II)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[II)LX/0lx;
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 131236
    iget-boolean v0, p0, LX/0lv;->c:Z

    if-eqz v0, :cond_0

    .line 131237
    sget-object v0, LX/16J;->a:LX/16J;

    invoke-virtual {v0, p1}, LX/16J;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 131238
    :cond_0
    const/4 v0, 0x3

    if-ge p3, v0, :cond_2

    .line 131239
    if-ne p3, v2, :cond_1

    aget v0, p2, v1

    invoke-direct {p0, v0}, LX/0lv;->d(I)I

    move-result v0

    .line 131240
    :goto_0
    invoke-static {v0, p1, p2, p3}, LX/0lv;->a(ILjava/lang/String;[II)LX/0lx;

    move-result-object v1

    .line 131241
    invoke-direct {p0, v0, v1}, LX/0lv;->a(ILX/0lx;)V

    .line 131242
    return-object v1

    .line 131243
    :cond_1
    aget v0, p2, v1

    aget v1, p2, v2

    invoke-direct {p0, v0, v1}, LX/0lv;->b(II)I

    move-result v0

    goto :goto_0

    .line 131244
    :cond_2
    invoke-direct {p0, p2, p3}, LX/0lv;->b([II)I

    move-result v0

    goto :goto_0
.end method

.method public final a([II)LX/0lx;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 131245
    const/4 v2, 0x3

    if-ge p2, v2, :cond_2

    .line 131246
    aget v1, p1, v0

    const/4 v2, 0x2

    if-ge p2, v2, :cond_1

    :goto_0
    invoke-virtual {p0, v1, v0}, LX/0lv;->a(II)LX/0lx;

    move-result-object v0

    .line 131247
    :cond_0
    :goto_1
    return-object v0

    .line 131248
    :cond_1
    const/4 v0, 0x1

    aget v0, p1, v0

    goto :goto_0

    .line 131249
    :cond_2
    invoke-direct {p0, p1, p2}, LX/0lv;->b([II)I

    move-result v2

    .line 131250
    iget v0, p0, LX/0lv;->f:I

    and-int/2addr v0, v2

    .line 131251
    iget-object v3, p0, LX/0lv;->g:[I

    aget v3, v3, v0

    .line 131252
    shr-int/lit8 v4, v3, 0x8

    xor-int/2addr v4, v2

    shl-int/lit8 v4, v4, 0x8

    if-nez v4, :cond_4

    .line 131253
    iget-object v4, p0, LX/0lv;->h:[LX/0lx;

    aget-object v0, v4, v0

    .line 131254
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, LX/0lx;->a([II)Z

    move-result v4

    if-nez v4, :cond_0

    .line 131255
    :cond_3
    and-int/lit16 v0, v3, 0xff

    .line 131256
    if-lez v0, :cond_5

    .line 131257
    add-int/lit8 v0, v0, -0x1

    .line 131258
    iget-object v3, p0, LX/0lv;->i:[LX/26S;

    aget-object v0, v3, v0

    .line 131259
    if-eqz v0, :cond_5

    .line 131260
    invoke-virtual {v0, v2, p1, p2}, LX/26S;->a(I[II)LX/0lx;

    move-result-object v0

    goto :goto_1

    .line 131261
    :cond_4
    if-nez v3, :cond_3

    move-object v0, v1

    .line 131262
    goto :goto_1

    :cond_5
    move-object v0, v1

    .line 131263
    goto :goto_1
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 131264
    iget-object v0, p0, LX/0lv;->a:LX/0lv;

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/0lv;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131265
    iget-object v0, p0, LX/0lv;->a:LX/0lv;

    new-instance v1, LX/0lw;

    invoke-direct {v1, p0}, LX/0lw;-><init>(LX/0lv;)V

    invoke-direct {v0, v1}, LX/0lv;->a(LX/0lw;)V

    .line 131266
    iput-boolean v2, p0, LX/0lv;->n:Z

    .line 131267
    iput-boolean v2, p0, LX/0lv;->o:Z

    .line 131268
    iput-boolean v2, p0, LX/0lv;->p:Z

    .line 131269
    :cond_0
    return-void
.end method
