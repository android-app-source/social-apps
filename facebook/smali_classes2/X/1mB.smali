.class public LX/1mB;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/os/Handler;

.field private final c:LX/1mA;

.field private final d:LX/0wp;

.field private final e:LX/0ka;

.field private final f:LX/1AA;

.field private final g:LX/0hB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 313341
    const-class v0, LX/1mB;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1mB;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;LX/1mA;LX/0wp;LX/0ka;LX/1AA;LX/0hB;)V
    .locals 1
    .param p1    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param

    .prologue
    .line 313332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313333
    iput-object p1, p0, LX/1mB;->b:Landroid/os/Handler;

    .line 313334
    iget-boolean v0, p3, LX/0wp;->P:Z

    if-eqz v0, :cond_0

    :goto_0
    iput-object p2, p0, LX/1mB;->c:LX/1mA;

    .line 313335
    iput-object p3, p0, LX/1mB;->d:LX/0wp;

    .line 313336
    iput-object p4, p0, LX/1mB;->e:LX/0ka;

    .line 313337
    iput-object p5, p0, LX/1mB;->f:LX/1AA;

    .line 313338
    iput-object p6, p0, LX/1mB;->g:LX/0hB;

    .line 313339
    return-void

    .line 313340
    :cond_0
    const/4 p2, 0x0

    goto :goto_0
.end method

.method private a(LX/0AR;)I
    .locals 2

    .prologue
    .line 313331
    iget-object v0, p0, LX/1mB;->d:LX/0wp;

    iget-boolean v0, v0, LX/0wp;->G:Z

    if-eqz v0, :cond_0

    iget v0, p1, LX/0AR;->f:I

    iget v1, p1, LX/0AR;->g:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p1, LX/0AR;->f:I

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;LX/36s;IJ)LX/36s;
    .locals 6

    .prologue
    .line 313254
    new-instance v0, LX/36s;

    iget-object v1, p2, LX/36s;->a:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, LX/36s;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 313255
    sget-object v1, LX/36t;->DASH:LX/36t;

    .line 313256
    iput-object v1, v0, LX/36s;->h:LX/36t;

    .line 313257
    iget-object v1, p0, LX/1mB;->d:LX/0wp;

    iget v1, v1, LX/0wp;->g:I

    if-lez v1, :cond_0

    .line 313258
    int-to-long v2, p3

    .line 313259
    iput-wide v2, v0, LX/36s;->f:J

    .line 313260
    iput-wide p4, v0, LX/36s;->g:J

    .line 313261
    const-wide/16 v2, 0x400

    .line 313262
    iput-wide v2, v0, LX/36s;->e:J

    .line 313263
    :goto_0
    iget-boolean v1, p2, LX/36s;->i:Z

    move v1, v1

    .line 313264
    iput-boolean v1, v0, LX/36s;->i:Z

    .line 313265
    return-object v0

    .line 313266
    :cond_0
    iget-wide v4, p2, LX/36s;->f:J

    move-wide v2, v4

    .line 313267
    iput-wide v2, v0, LX/36s;->f:J

    .line 313268
    iget-wide v4, p2, LX/36s;->e:J

    move-wide v2, v4

    .line 313269
    iput-wide v2, v0, LX/36s;->e:J

    .line 313270
    iget-wide v4, p2, LX/36s;->g:J

    move-wide v2, v4

    .line 313271
    iput-wide v2, v0, LX/36s;->g:J

    .line 313272
    goto :goto_0
.end method

.method public static a(LX/1mB;LX/1M7;LX/36s;LX/0Ak;I)V
    .locals 10

    .prologue
    .line 313277
    iget-object v3, p3, LX/0Ak;->c:Ljava/util/List;

    .line 313278
    const/4 v2, 0x0

    .line 313279
    iget-object v0, p0, LX/1mB;->d:LX/0wp;

    invoke-virtual {v0}, LX/0wp;->d()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 313280
    iget-object v0, p0, LX/1mB;->d:LX/0wp;

    iget-object v1, p0, LX/1mB;->e:LX/0ka;

    .line 313281
    iget-boolean v4, p2, LX/36s;->i:Z

    move v4, v4

    .line 313282
    invoke-virtual {v0, v1, v4}, LX/0wp;->a(LX/0ka;Z)I

    move-result v1

    .line 313283
    iget-object v0, p0, LX/1mB;->c:LX/1mA;

    if-eqz v0, :cond_7

    if-nez p4, :cond_7

    .line 313284
    iget-object v0, p0, LX/1mB;->f:LX/1AA;

    iget-object v2, p2, LX/36s;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/1AA;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 313285
    :cond_0
    :goto_0
    return-void

    .line 313286
    :cond_1
    iget-object v0, p0, LX/1mB;->g:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    .line 313287
    if-eqz v1, :cond_2

    if-le v1, v0, :cond_11

    .line 313288
    :cond_2
    :goto_1
    iget-object v1, p0, LX/1mB;->c:LX/1mA;

    invoke-virtual {v1, v3, v0}, LX/1mA;->a(Ljava/util/List;I)LX/0Ah;

    move-result-object v2

    move-object v0, v2

    .line 313289
    :cond_3
    :goto_2
    if-nez v0, :cond_e

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    .line 313290
    const/4 v0, 0x0

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    move-object v6, v0

    .line 313291
    :goto_3
    if-eqz v6, :cond_0

    .line 313292
    iget-object v0, v6, LX/0Ah;->f:LX/0Au;

    move-object v0, v0

    .line 313293
    const/4 v1, 0x3

    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 313294
    invoke-virtual {v0}, LX/0Au;->b()Ljava/lang/String;

    .line 313295
    :cond_4
    invoke-virtual {v6}, LX/0Ah;->f()LX/0BX;

    move-result-object v7

    .line 313296
    if-eqz v7, :cond_c

    .line 313297
    invoke-virtual {v0}, LX/0Au;->a()Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, LX/1mB;->a(Landroid/net/Uri;LX/36s;IJ)LX/36s;

    move-result-object v0

    .line 313298
    iput p4, v0, LX/36s;->b:I

    .line 313299
    iget-object v1, v6, LX/0Ah;->e:Ljava/lang/String;

    move-object v1, v1

    .line 313300
    iput-object v1, v0, LX/36s;->j:Ljava/lang/String;

    .line 313301
    const/4 v1, 0x1

    new-array v1, v1, [LX/36s;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-interface {p1, v1}, LX/1M7;->b([LX/36s;)V

    .line 313302
    invoke-interface {v7}, LX/0BX;->a()I

    move-result v0

    .line 313303
    invoke-interface {v7, v0}, LX/0BX;->b(I)LX/0Au;

    move-result-object v1

    .line 313304
    const/4 v2, 0x3

    invoke-static {v2}, LX/01m;->b(I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 313305
    invoke-virtual {v1}, LX/0Au;->b()Ljava/lang/String;

    .line 313306
    :cond_5
    invoke-virtual {v1}, LX/0Au;->a()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, v6, LX/0Ah;->c:LX/0AR;

    iget v3, v2, LX/0AR;->c:I

    const-wide/16 v4, -0x1

    invoke-interface {v7, v0, v4, v5}, LX/0BX;->a(IJ)J

    move-result-wide v4

    const-wide/16 v8, 0x3e8

    mul-long/2addr v4, v8

    move-object v0, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, LX/1mB;->a(Landroid/net/Uri;LX/36s;IJ)LX/36s;

    move-result-object v0

    .line 313307
    iget-object v1, v6, LX/0Ah;->e:Ljava/lang/String;

    move-object v1, v1

    .line 313308
    iput-object v1, v0, LX/36s;->j:Ljava/lang/String;

    .line 313309
    iget-object v1, p0, LX/1mB;->c:LX/1mA;

    if-eqz v1, :cond_6

    if-nez p4, :cond_6

    .line 313310
    iget-object v1, v6, LX/0Ah;->c:LX/0AR;

    iget-object v1, v1, LX/0AR;->a:Ljava/lang/String;

    .line 313311
    iput-object v1, v0, LX/36s;->k:Ljava/lang/String;

    .line 313312
    :cond_6
    const/4 v1, 0x1

    new-array v1, v1, [LX/36s;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-interface {p1, v1}, LX/1M7;->b([LX/36s;)V

    goto/16 :goto_0

    .line 313313
    :cond_7
    if-gtz v1, :cond_a

    .line 313314
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    .line 313315
    iget-object v4, v0, LX/0Ah;->c:LX/0AR;

    iget-object v4, v4, LX/0AR;->a:Ljava/lang/String;

    const-string v5, "vd"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, v0, LX/0Ah;->c:LX/0AR;

    iget-object v4, v4, LX/0AR;->a:Ljava/lang/String;

    const-string v5, "ad"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    goto/16 :goto_2

    :cond_9
    move-object v0, v2

    .line 313316
    goto/16 :goto_2

    .line 313317
    :cond_a
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    .line 313318
    iget-object v5, v0, LX/0Ah;->c:LX/0AR;

    invoke-direct {p0, v5}, LX/1mB;->a(LX/0AR;)I

    move-result v5

    if-gt v5, v1, :cond_f

    if-eqz v2, :cond_b

    iget-object v5, v0, LX/0Ah;->c:LX/0AR;

    invoke-direct {p0, v5}, LX/1mB;->a(LX/0AR;)I

    move-result v5

    iget-object v6, v2, LX/0Ah;->c:LX/0AR;

    invoke-direct {p0, v6}, LX/1mB;->a(LX/0AR;)I

    move-result v6

    if-le v5, v6, :cond_f

    :cond_b
    :goto_5
    move-object v2, v0

    .line 313319
    goto :goto_4

    .line 313320
    :cond_c
    iget-object v1, p0, LX/1mB;->d:LX/0wp;

    iget-boolean v1, v1, LX/0wp;->F:Z

    if-eqz v1, :cond_0

    .line 313321
    invoke-virtual {v0}, LX/0Au;->a()Landroid/net/Uri;

    move-result-object v1

    iget-object v0, v6, LX/0Ah;->c:LX/0AR;

    iget v3, v0, LX/0AR;->c:I

    invoke-virtual {p2}, LX/36s;->d()J

    move-result-wide v4

    move-object v0, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, LX/1mB;->a(Landroid/net/Uri;LX/36s;IJ)LX/36s;

    move-result-object v0

    .line 313322
    iput p4, v0, LX/36s;->b:I

    .line 313323
    iget-object v1, v6, LX/0Ah;->e:Ljava/lang/String;

    move-object v1, v1

    .line 313324
    iput-object v1, v0, LX/36s;->j:Ljava/lang/String;

    .line 313325
    iget-object v1, p0, LX/1mB;->c:LX/1mA;

    if-eqz v1, :cond_d

    if-nez p4, :cond_d

    .line 313326
    iget-object v1, v6, LX/0Ah;->c:LX/0AR;

    iget-object v1, v1, LX/0AR;->a:Ljava/lang/String;

    .line 313327
    iput-object v1, v0, LX/36s;->k:Ljava/lang/String;

    .line 313328
    :cond_d
    const/4 v1, 0x1

    new-array v1, v1, [LX/36s;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-interface {p1, v1}, LX/1M7;->b([LX/36s;)V

    .line 313329
    invoke-virtual {v0}, LX/36s;->c()J

    invoke-virtual {v0}, LX/36s;->d()J

    .line 313330
    goto/16 :goto_0

    :cond_e
    move-object v6, v0

    goto/16 :goto_3

    :cond_f
    move-object v0, v2

    goto :goto_5

    :cond_10
    move-object v0, v2

    goto/16 :goto_2

    :cond_11
    move v0, v1

    goto/16 :goto_1

    :cond_12
    move-object v0, v2

    goto/16 :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/1M7;LX/36s;)V
    .locals 3

    .prologue
    .line 313274
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1mB;->d:LX/0wp;

    iget-boolean v0, v0, LX/0wp;->d:Z

    if-nez v0, :cond_1

    .line 313275
    :cond_0
    :goto_0
    return-void

    .line 313276
    :cond_1
    iget-object v0, p0, LX/1mB;->b:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/video/server/prefetcher/DashVideoPrefetchParser$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/video/server/prefetcher/DashVideoPrefetchParser$1;-><init>(LX/1mB;Ljava/lang/String;LX/1M7;LX/36s;)V

    const v2, 0x3b8a78e4

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 313273
    iget-object v0, p0, LX/1mB;->d:LX/0wp;

    iget-boolean v0, v0, LX/0wp;->h:Z

    return v0
.end method
