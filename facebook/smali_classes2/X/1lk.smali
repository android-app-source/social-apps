.class public LX/1lk;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1lk;


# instance fields
.field public b:I

.field public c:Z

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 312337
    const v0, 0x7fffffff

    invoke-static {v0, v1, v1}, LX/1lk;->a(IZZ)LX/1lk;

    move-result-object v0

    sput-object v0, LX/1lk;->a:LX/1lk;

    return-void
.end method

.method private constructor <init>(IZZ)V
    .locals 0

    .prologue
    .line 312338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 312339
    iput p1, p0, LX/1lk;->b:I

    .line 312340
    iput-boolean p2, p0, LX/1lk;->c:Z

    .line 312341
    iput-boolean p3, p0, LX/1lk;->d:Z

    .line 312342
    return-void
.end method

.method public static a(IZZ)LX/1lk;
    .locals 1

    .prologue
    .line 312343
    new-instance v0, LX/1lk;

    invoke-direct {v0, p0, p1, p2}, LX/1lk;-><init>(IZZ)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 312344
    if-ne p1, p0, :cond_1

    .line 312345
    :cond_0
    :goto_0
    return v0

    .line 312346
    :cond_1
    instance-of v2, p1, LX/1lk;

    if-nez v2, :cond_2

    move v0, v1

    .line 312347
    goto :goto_0

    .line 312348
    :cond_2
    check-cast p1, LX/1lk;

    .line 312349
    iget v2, p0, LX/1lk;->b:I

    iget v3, p1, LX/1lk;->b:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, LX/1lk;->c:Z

    iget-boolean v3, p1, LX/1lk;->c:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, LX/1lk;->d:Z

    iget-boolean v3, p1, LX/1lk;->d:Z

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 312350
    iget v2, p0, LX/1lk;->b:I

    iget-boolean v0, p0, LX/1lk;->c:Z

    if-eqz v0, :cond_1

    const/high16 v0, 0x400000

    :goto_0
    xor-int/2addr v0, v2

    iget-boolean v2, p0, LX/1lk;->d:Z

    if-eqz v2, :cond_0

    const/high16 v1, 0x800000

    :cond_0
    xor-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
