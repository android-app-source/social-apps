.class public final LX/0PA;
.super LX/0P1;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0P1",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final transient a:[Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final transient b:[LX/0P3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/0P3",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final transient c:I


# direct methods
.method private constructor <init>([Ljava/util/Map$Entry;[LX/0P3;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;[",
            "LX/0P3",
            "<TK;TV;>;I)V"
        }
    .end annotation

    .prologue
    .line 55015
    invoke-direct {p0}, LX/0P1;-><init>()V

    .line 55016
    iput-object p1, p0, LX/0PA;->a:[Ljava/util/Map$Entry;

    .line 55017
    iput-object p2, p0, LX/0PA;->b:[LX/0P3;

    .line 55018
    iput p3, p0, LX/0PA;->c:I

    .line 55019
    return-void
.end method

.method public static a(I[Ljava/util/Map$Entry;)LX/0PA;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(I[",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;)",
            "LX/0PA",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 55020
    array-length v0, p1

    invoke-static {p0, v0}, LX/0PB;->checkPositionIndex(II)I

    .line 55021
    array-length v0, p1

    if-ne p0, v0, :cond_0

    move-object v2, p1

    .line 55022
    :goto_0
    const-wide v0, 0x3ff3333333333333L    # 1.2

    invoke-static {p0, v0, v1}, LX/0PC;->a(ID)I

    move-result v0

    .line 55023
    new-array v1, v0, [LX/0P3;

    move-object v5, v1

    .line 55024
    add-int/lit8 v6, v0, -0x1

    move v4, v3

    .line 55025
    :goto_1
    if-ge v4, p0, :cond_4

    .line 55026
    aget-object v1, p1, v4

    .line 55027
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    .line 55028
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    .line 55029
    invoke-static {v7, v8}, LX/0P6;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 55030
    invoke-virtual {v7}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, LX/0PC;->a(I)I

    move-result v0

    and-int v9, v0, v6

    .line 55031
    aget-object v10, v5, v9

    .line 55032
    if-nez v10, :cond_3

    .line 55033
    instance-of v0, v1, LX/0P3;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, LX/0P3;

    invoke-virtual {v0}, LX/0P3;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 55034
    :goto_2
    if-eqz v0, :cond_2

    check-cast v1, LX/0P3;

    .line 55035
    :goto_3
    aput-object v1, v5, v9

    .line 55036
    aput-object v1, v2, v4

    .line 55037
    invoke-static {v7, v1, v10}, LX/0PA;->a(Ljava/lang/Object;Ljava/util/Map$Entry;LX/0P3;)V

    .line 55038
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 55039
    :cond_0
    new-array v0, p0, [LX/0P3;

    move-object v0, v0

    .line 55040
    move-object v2, v0

    goto :goto_0

    :cond_1
    move v0, v3

    .line 55041
    goto :goto_2

    .line 55042
    :cond_2
    new-instance v1, LX/0P3;

    invoke-direct {v1, v7, v8}, LX/0P3;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_3

    .line 55043
    :cond_3
    new-instance v1, LX/0PD;

    invoke-direct {v1, v7, v8, v10}, LX/0PD;-><init>(Ljava/lang/Object;Ljava/lang/Object;LX/0P3;)V

    goto :goto_3

    .line 55044
    :cond_4
    new-instance v0, LX/0PA;

    invoke-direct {v0, v2, v5, v6}, LX/0PA;-><init>([Ljava/util/Map$Entry;[LX/0P3;I)V

    return-object v0
.end method

.method public static varargs a([Ljava/util/Map$Entry;)LX/0PA;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">([",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;)",
            "LX/0PA",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 55045
    array-length v0, p0

    invoke-static {v0, p0}, LX/0PA;->a(I[Ljava/util/Map$Entry;)LX/0PA;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;[LX/0P3;I)Ljava/lang/Object;
    .locals 3
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "[",
            "LX/0P3",
            "<*TV;>;I)TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 55046
    if-nez p0, :cond_1

    .line 55047
    :cond_0
    :goto_0
    return-object v0

    .line 55048
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, LX/0PC;->a(I)I

    move-result v1

    and-int/2addr v1, p2

    .line 55049
    aget-object v1, p1, v1

    .line 55050
    :goto_1
    if-eqz v1, :cond_0

    .line 55051
    invoke-virtual {v1}, LX/0P4;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 55052
    invoke-virtual {p0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 55053
    invoke-virtual {v1}, LX/0P4;->getValue()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 55054
    :cond_2
    invoke-virtual {v1}, LX/0P3;->a()LX/0P3;

    move-result-object v1

    goto :goto_1
.end method

.method public static a(Ljava/lang/Object;Ljava/util/Map$Entry;LX/0P3;)V
    .locals 2
    .param p2    # LX/0P3;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/Map$Entry",
            "<**>;",
            "LX/0P3",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 55055
    :goto_0
    if-eqz p2, :cond_1

    .line 55056
    invoke-virtual {p2}, LX/0P4;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    const-string v1, "key"

    invoke-static {v0, v1, p1, p2}, LX/0P1;->checkNoConflict(ZLjava/lang/String;Ljava/util/Map$Entry;Ljava/util/Map$Entry;)V

    .line 55057
    invoke-virtual {p2}, LX/0P3;->a()LX/0P3;

    move-result-object p2

    goto :goto_0

    .line 55058
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 55059
    :cond_1
    return-void
.end method


# virtual methods
.method public final createEntrySet()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 55060
    new-instance v0, LX/12n;

    iget-object v1, p0, LX/0PA;->a:[Ljava/util/Map$Entry;

    invoke-direct {v0, p0, v1}, LX/12n;-><init>(LX/0P1;[Ljava/util/Map$Entry;)V

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 55061
    iget-object v0, p0, LX/0PA;->b:[LX/0P3;

    iget v1, p0, LX/0PA;->c:I

    invoke-static {p1, v0, v1}, LX/0PA;->a(Ljava/lang/Object;[LX/0P3;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 55062
    const/4 v0, 0x0

    return v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 55063
    iget-object v0, p0, LX/0PA;->a:[Ljava/util/Map$Entry;

    array-length v0, v0

    return v0
.end method
