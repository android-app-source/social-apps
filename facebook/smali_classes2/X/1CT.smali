.class public LX/1CT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1CT;


# instance fields
.field public a:LX/0pe;

.field private final b:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0pe;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 216185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216186
    iput-object p1, p0, LX/1CT;->a:LX/0pe;

    .line 216187
    iput-object p2, p0, LX/1CT;->b:LX/0Zb;

    .line 216188
    return-void
.end method

.method public static a(LX/0QB;)LX/1CT;
    .locals 5

    .prologue
    .line 216189
    sget-object v0, LX/1CT;->c:LX/1CT;

    if-nez v0, :cond_1

    .line 216190
    const-class v1, LX/1CT;

    monitor-enter v1

    .line 216191
    :try_start_0
    sget-object v0, LX/1CT;->c:LX/1CT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 216192
    if-eqz v2, :cond_0

    .line 216193
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 216194
    new-instance p0, LX/1CT;

    invoke-static {v0}, LX/0pe;->a(LX/0QB;)LX/0pe;

    move-result-object v3

    check-cast v3, LX/0pe;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {p0, v3, v4}, LX/1CT;-><init>(LX/0pe;LX/0Zb;)V

    .line 216195
    move-object v0, p0

    .line 216196
    sput-object v0, LX/1CT;->c:LX/1CT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216197
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 216198
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 216199
    :cond_1
    sget-object v0, LX/1CT;->c:LX/1CT;

    return-object v0

    .line 216200
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 216201
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1CT;IILX/0g1;)Landroid/util/Pair;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "LX/0g1;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 216202
    if-ge p1, p2, :cond_0

    move v0, v1

    :goto_0
    move v2, v3

    move v4, v3

    .line 216203
    :goto_1
    if-eq p1, p2, :cond_3

    .line 216204
    invoke-interface {p3, p1}, LX/0g1;->a(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 216205
    invoke-static {v5}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v5

    .line 216206
    if-nez v5, :cond_4

    .line 216207
    const/4 v5, 0x1

    .line 216208
    :goto_2
    move v5, v5

    .line 216209
    if-eqz v5, :cond_1

    move v4, v1

    move v6, v2

    move v2, v3

    move v3, v6

    .line 216210
    :goto_3
    add-int/2addr p1, v0

    move v6, v2

    move v2, v3

    move v3, v6

    goto :goto_1

    .line 216211
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 216212
    :cond_1
    if-nez v4, :cond_2

    .line 216213
    add-int/lit8 v2, v2, 0x1

    .line 216214
    :cond_2
    add-int/lit8 v3, v3, 0x1

    move v6, v3

    move v3, v2

    move v2, v6

    goto :goto_3

    .line 216215
    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    :cond_4
    iget-object v6, p0, LX/1CT;->a:LX/0pe;

    invoke-virtual {v6, v5}, LX/0pe;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v5

    goto :goto_2
.end method


# virtual methods
.method public final a(LX/0g1;IZ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 216216
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "feed_user_left_app"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 216217
    if-ltz p2, :cond_0

    invoke-interface {p1}, LX/0g1;->size()I

    move-result v1

    if-lt p2, v1, :cond_2

    .line 216218
    :cond_0
    :goto_0
    const-string v2, "unknown_story"

    .line 216219
    if-ltz p2, :cond_1

    invoke-interface {p1}, LX/0g1;->size()I

    move-result v1

    if-lt p2, v1, :cond_4

    .line 216220
    :cond_1
    :goto_1
    const-string v1, "native_newsfeed"

    .line 216221
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 216222
    iget-object v1, p0, LX/1CT;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 216223
    return-void

    .line 216224
    :cond_2
    const/4 v1, -0x1

    invoke-static {p0, p2, v1, p1}, LX/1CT;->a(LX/1CT;IILX/0g1;)Landroid/util/Pair;

    move-result-object v2

    .line 216225
    invoke-interface {p1}, LX/0g1;->size()I

    move-result v1

    invoke-static {p0, p2, v1, p1}, LX/1CT;->a(LX/1CT;IILX/0g1;)Landroid/util/Pair;

    move-result-object v3

    .line 216226
    const-string v1, "current_position"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "user_left_app"

    if-eqz p3, :cond_3

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v4, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v4, "closest_unseen_stories_above"

    iget-object v5, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v4, "closest_unseen_stories_below"

    iget-object v5, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v4, "total_unseen_stories_above"

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v1, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "total_unseen_stories_below"

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "total_stories_below"

    invoke-interface {p1}, LX/0g1;->size()I

    move-result v3

    sub-int/2addr v3, p2

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 216227
    :cond_4
    invoke-interface {p1, p2}, LX/0g1;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 216228
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    instance-of v3, v3, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v3, :cond_5

    .line 216229
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 216230
    invoke-static {v1}, LX/16y;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 216231
    const-string v2, "edge_story"

    .line 216232
    :goto_3
    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    .line 216233
    const-string v3, "tracking_data"

    invoke-virtual {v1}, LX/162;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 216234
    :cond_5
    const-string v1, "story_type"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto/16 :goto_1

    .line 216235
    :cond_6
    invoke-static {v1}, LX/17E;->z(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 216236
    const-string v2, "neko_ad"

    goto :goto_3

    .line 216237
    :cond_7
    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-static {v2}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 216238
    const-string v2, "sponsored_context"

    goto :goto_3

    .line 216239
    :cond_8
    invoke-static {v1}, LX/17E;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 216240
    const-string v2, "photo_story"

    goto :goto_3

    .line 216241
    :cond_9
    invoke-static {v1}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 216242
    const-string v2, "video_story"

    goto :goto_3

    .line 216243
    :cond_a
    invoke-static {v1}, LX/17E;->h(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 216244
    const-string v2, "external_url_attached_story"

    goto :goto_3

    .line 216245
    :cond_b
    invoke-static {v1}, LX/16y;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 216246
    const-string v2, "aggregated_story"

    goto :goto_3

    .line 216247
    :cond_c
    const-string v2, "simple_story"

    goto :goto_3
.end method
