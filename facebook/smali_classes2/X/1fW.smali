.class public LX/1fW;
.super LX/0TS;
.source ""

# interfaces
.implements LX/1fX;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/0Sj;LX/0TR;)V
    .locals 7

    .prologue
    .line 291796
    const-string v1, "SerialExecutor"

    const/4 v2, 0x1

    new-instance v4, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v4}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    move-object v0, p0

    move-object v3, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, LX/0TS;-><init>(Ljava/lang/String;ILjava/util/concurrent/Executor;Ljava/util/concurrent/BlockingQueue;LX/0Sj;LX/0TR;)V

    .line 291797
    return-void
.end method


# virtual methods
.method public final declared-synchronized execute(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 291798
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, LX/0TS;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291799
    monitor-exit p0

    return-void

    .line 291800
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
