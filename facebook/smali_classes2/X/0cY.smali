.class public LX/0cY;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/io/File;

.field public e:I

.field public f:Ljava/lang/String;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88944
    const-class v0, LX/0cY;

    sput-object v0, LX/0cY;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Ot;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p2    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/PhotoUploadSerialExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 88940
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88941
    iput-object p1, p0, LX/0cY;->b:LX/0Or;

    .line 88942
    iput-object p2, p0, LX/0cY;->c:LX/0Ot;

    .line 88943
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 88992
    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    return-object p0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 88990
    iget-object v0, p0, LX/0cY;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/photos/upload/manager/UploadQueueFileManager$3;

    invoke-direct {v1, p0}, Lcom/facebook/photos/upload/manager/UploadQueueFileManager$3;-><init>(LX/0cY;)V

    const v2, 0x6fa63bfd

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 88991
    return-void
.end method

.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 3

    .prologue
    .line 88988
    iget-object v0, p0, LX/0cY;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/photos/upload/manager/UploadQueueFileManager$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/photos/upload/manager/UploadQueueFileManager$1;-><init>(LX/0cY;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    const v2, 0x3c555c01

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 88989
    return-void
.end method

.method public final a(Ljava/io/File;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 88993
    iput-object p1, p0, LX/0cY;->d:Ljava/io/File;

    .line 88994
    iput p2, p0, LX/0cY;->e:I

    .line 88995
    invoke-static {p3}, LX/0cY;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0cY;->f:Ljava/lang/String;

    .line 88996
    return-void
.end method

.method public final b()I
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 88950
    iget-object v0, p0, LX/0cY;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 88951
    iput-object v3, p0, LX/0cY;->g:Ljava/util/List;

    move v0, v1

    .line 88952
    :goto_0
    return v0

    .line 88953
    :cond_0
    :try_start_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0cY;->g:Ljava/util/List;

    .line 88954
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v0, p0, LX/0cY;->d:Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 88955
    :try_start_1
    new-instance v4, Ljava/io/DataInputStream;

    invoke-direct {v4, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 88956
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    iget v3, p0, LX/0cY;->e:I

    if-eq v0, v3, :cond_2

    .line 88957
    new-instance v0, LX/8L5;

    const-string v1, "different app versions"

    invoke-direct {v0, p0, v1}, LX/8L5;-><init>(LX/0cY;Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 88958
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 88959
    :goto_1
    :try_start_2
    sget-object v2, LX/0cY;->a:Ljava/lang/Class;

    const-string v3, "Failed to read queued uploads: %s, %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 88960
    if-eqz v1, :cond_1

    .line 88961
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 88962
    :cond_1
    :goto_2
    iget-object v0, p0, LX/0cY;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 88963
    iget-object v0, p0, LX/0cY;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    .line 88964
    :cond_2
    :try_start_4
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LX/0cY;->f:Ljava/lang/String;

    invoke-static {v0, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 88965
    new-instance v0, LX/8L5;

    const-string v1, "different OS versions"

    invoke-direct {v0, p0, v1}, LX/8L5;-><init>(LX/0cY;Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 88966
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v2, :cond_3

    .line 88967
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 88968
    :cond_3
    :goto_4
    throw v0

    .line 88969
    :cond_4
    :try_start_6
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LX/0cY;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v0, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 88970
    new-instance v0, LX/8L5;

    const-string v1, "different users"

    invoke-direct {v0, p0, v1}, LX/8L5;-><init>(LX/0cY;Ljava/lang/String;)V

    throw v0

    .line 88971
    :cond_5
    :goto_5
    invoke-virtual {v2}, Ljava/io/FileInputStream;->available()I

    move-result v0

    if-lez v0, :cond_8

    .line 88972
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 88973
    sget-object v3, LX/8L6;->QueuedOperation:LX/8L6;

    invoke-virtual {v3}, LX/8L6;->ordinal()I

    move-result v3

    if-ne v0, v3, :cond_6

    .line 88974
    invoke-static {v4}, Lcom/facebook/photos/upload/operation/UploadOperation;->a(Ljava/io/DataInput;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v0

    .line 88975
    iget-object v3, p0, LX/0cY;->g:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 88976
    :cond_6
    sget-object v3, LX/8L6;->CompletedOperation:LX/8L6;

    invoke-virtual {v3}, LX/8L6;->ordinal()I

    move-result v3

    if-ne v0, v3, :cond_5

    .line 88977
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v5

    move v3, v1

    .line 88978
    :goto_6
    iget-object v0, p0, LX/0cY;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_5

    .line 88979
    iget-object v0, p0, LX/0cY;->g:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 88980
    iget-object v6, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v6

    .line 88981
    invoke-static {v0, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 88982
    iget-object v0, p0, LX/0cY;->g:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_5

    .line 88983
    :cond_7
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_6

    .line 88984
    :cond_8
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto/16 :goto_2

    .line 88985
    :catch_1
    goto/16 :goto_2

    :catch_2
    goto/16 :goto_2

    :catch_3
    goto :goto_4

    .line 88986
    :catchall_1
    move-exception v0

    move-object v2, v3

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v2, v1

    goto :goto_3

    .line 88987
    :catch_4
    move-exception v0

    move-object v1, v3

    goto/16 :goto_1
.end method

.method public final b(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 3

    .prologue
    .line 88948
    iget-object v0, p0, LX/0cY;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/photos/upload/manager/UploadQueueFileManager$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/photos/upload/manager/UploadQueueFileManager$2;-><init>(LX/0cY;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    const v2, -0x6c28db94

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 88949
    return-void
.end method

.method public final c()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88945
    iget-object v0, p0, LX/0cY;->g:Ljava/util/List;

    .line 88946
    const/4 v1, 0x0

    iput-object v1, p0, LX/0cY;->g:Ljava/util/List;

    .line 88947
    return-object v0
.end method
