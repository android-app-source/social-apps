.class public LX/1tw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1tw;


# instance fields
.field private a:LX/03R;

.field private b:LX/03R;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 336889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336890
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/1tw;->a:LX/03R;

    .line 336891
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/1tw;->b:LX/03R;

    .line 336892
    return-void
.end method

.method public static a(LX/0QB;)LX/1tw;
    .locals 3

    .prologue
    .line 336893
    sget-object v0, LX/1tw;->c:LX/1tw;

    if-nez v0, :cond_1

    .line 336894
    const-class v1, LX/1tw;

    monitor-enter v1

    .line 336895
    :try_start_0
    sget-object v0, LX/1tw;->c:LX/1tw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 336896
    if-eqz v2, :cond_0

    .line 336897
    :try_start_1
    new-instance v0, LX/1tw;

    invoke-direct {v0}, LX/1tw;-><init>()V

    .line 336898
    move-object v0, v0

    .line 336899
    sput-object v0, LX/1tw;->c:LX/1tw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 336900
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 336901
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 336902
    :cond_1
    sget-object v0, LX/1tw;->c:LX/1tw;

    return-object v0

    .line 336903
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 336904
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 336905
    iget-object v0, p0, LX/1tw;->a:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-ne v0, v1, :cond_0

    .line 336906
    invoke-static {}, Lcom/facebook/jni/CpuCapabilitiesJni;->nativeDeviceSupportsNeon()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/03R;->YES:LX/03R;

    :goto_0
    iput-object v0, p0, LX/1tw;->a:LX/03R;

    .line 336907
    :cond_0
    iget-object v0, p0, LX/1tw;->a:LX/03R;

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 336908
    :cond_1
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 336909
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 336910
    iget-object v0, p0, LX/1tw;->b:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-ne v0, v1, :cond_0

    .line 336911
    invoke-static {}, Lcom/facebook/jni/CpuCapabilitiesJni;->nativeDeviceSupportsX86()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/03R;->YES:LX/03R;

    :goto_0
    iput-object v0, p0, LX/1tw;->b:LX/03R;

    .line 336912
    :cond_0
    iget-object v0, p0, LX/1tw;->b:LX/03R;

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 336913
    :cond_1
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 336914
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
