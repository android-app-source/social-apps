.class public LX/195;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/196;


# instance fields
.field public a:Ljava/lang/ref/WeakReference;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/0Zb;

.field private final d:LX/19D;

.field public final e:Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;

.field public final f:LX/198;

.field public final g:Ljava/lang/String;

.field public final h:LX/19C;

.field public final i:LX/199;

.field public final j:LX/19A;

.field public k:LX/1KG;

.field public l:LX/0oG;

.field public m:J

.field public n:Z

.field public o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1YD;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1YD;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public q:Ljava/util/concurrent/Future;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0Zb;LX/197;LX/198;LX/199;LX/19A;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 207302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207303
    new-instance v0, LX/19C;

    invoke-direct {v0}, LX/19C;-><init>()V

    iput-object v0, p0, LX/195;->h:LX/19C;

    .line 207304
    iput-boolean v1, p0, LX/195;->n:Z

    .line 207305
    invoke-static {p8}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207306
    iput-object p1, p0, LX/195;->b:Ljava/util/concurrent/ExecutorService;

    .line 207307
    iput-object p2, p0, LX/195;->c:LX/0Zb;

    .line 207308
    invoke-virtual {p3, p7}, LX/197;->a(Ljava/lang/Boolean;)LX/19D;

    move-result-object v0

    iput-object v0, p0, LX/195;->d:LX/19D;

    .line 207309
    iget-object v0, p0, LX/195;->d:LX/19D;

    .line 207310
    iput-object p0, v0, LX/19D;->f:LX/196;

    .line 207311
    iput-object p4, p0, LX/195;->f:LX/198;

    .line 207312
    iput-object p5, p0, LX/195;->i:LX/199;

    .line 207313
    iput-object p6, p0, LX/195;->j:LX/19A;

    .line 207314
    iput-object p8, p0, LX/195;->g:Ljava/lang/String;

    .line 207315
    new-instance v0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;-><init>(LX/195;)V

    iput-object v0, p0, LX/195;->e:Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;

    .line 207316
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/195;->o:Ljava/util/Map;

    .line 207317
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/195;->p:Ljava/util/Map;

    .line 207318
    return-void
.end method

.method public static a(ILX/1YD;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/1YD;",
            "Ljava/util/Map",
            "<",
            "LX/1YD;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 207297
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 207298
    if-nez v0, :cond_0

    .line 207299
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 207300
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, p0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207301
    return-void
.end method

.method public static g(LX/195;)Z
    .locals 1

    .prologue
    .line 207296
    iget-object v0, p0, LX/195;->q:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/195;->q:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 207285
    invoke-static {p0}, LX/195;->g(LX/195;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207286
    :cond_0
    :goto_0
    return-void

    .line 207287
    :cond_1
    iget-object v0, p0, LX/195;->c:LX/0Zb;

    const-string v1, "feed_scroll_perf"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    iput-object v0, p0, LX/195;->l:LX/0oG;

    .line 207288
    iget-object v0, p0, LX/195;->l:LX/0oG;

    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207289
    iget-boolean v0, p0, LX/195;->n:Z

    if-nez v0, :cond_0

    .line 207290
    iget-object v0, p0, LX/195;->f:LX/198;

    invoke-virtual {v0}, LX/198;->c()V

    .line 207291
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/195;->n:Z

    .line 207292
    iget-object v0, p0, LX/195;->d:LX/19D;

    invoke-virtual {v0}, LX/19D;->a()V

    .line 207293
    new-instance v0, Ljava/lang/ref/WeakReference;

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/195;->a:Ljava/lang/ref/WeakReference;

    .line 207294
    iget-object v0, p0, LX/195;->k:LX/1KG;

    if-eqz v0, :cond_0

    .line 207295
    iget-object v0, p0, LX/195;->k:LX/1KG;

    invoke-interface {v0}, LX/1KG;->a()V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 13

    .prologue
    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    .line 207234
    const-string v0, "FrameRateLogger.onFrameRendered"

    const v1, 0x47fe987c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 207235
    :try_start_0
    iget-boolean v0, p0, LX/195;->n:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 207236
    const v0, 0x4954f12e    # 872210.9f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 207237
    :goto_0
    return-void

    .line 207238
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 207239
    iget-object v1, p0, LX/195;->j:LX/19A;

    invoke-virtual {v1}, LX/19A;->a()I

    move-result v2

    .line 207240
    invoke-static {v0, v2}, LX/3lv;->a(II)I

    move-result v3

    .line 207241
    iget-object v0, p0, LX/195;->h:LX/19C;

    .line 207242
    iget v1, v0, LX/19C;->a:I

    add-int/2addr v1, v3

    iput v1, v0, LX/19C;->a:I

    .line 207243
    iget-object v0, p0, LX/195;->h:LX/19C;

    int-to-float v1, v3

    .line 207244
    iget v4, v0, LX/19C;->b:F

    add-float/2addr v4, v1

    iput v4, v0, LX/19C;->b:F

    .line 207245
    iget-object v4, p0, LX/195;->h:LX/19C;

    const/4 v0, 0x4

    if-lt v3, v0, :cond_3

    int-to-double v0, v3

    mul-double/2addr v0, v8

    const-wide/high16 v6, 0x4010000000000000L    # 4.0

    div-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-double v0, v0

    div-double/2addr v0, v8

    .line 207246
    :goto_1
    iget v10, v4, LX/19C;->c:F

    float-to-double v10, v10

    add-double/2addr v10, v0

    double-to-float v10, v10

    iput v10, v4, LX/19C;->c:F

    .line 207247
    invoke-static {v3, v2}, LX/3lv;->b(II)I

    move-result v0

    .line 207248
    iget-wide v4, p0, LX/195;->m:J

    int-to-long v6, v0

    add-long/2addr v4, v6

    iput-wide v4, p0, LX/195;->m:J

    .line 207249
    iget-object v1, p0, LX/195;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 207250
    :goto_2
    if-gtz v3, :cond_6

    .line 207251
    :cond_1
    :goto_3
    iget-object v1, p0, LX/195;->k:LX/1KG;

    if-eqz v1, :cond_2

    .line 207252
    iget-object v1, p0, LX/195;->k:LX/1KG;

    invoke-interface {v1, v0}, LX/1KG;->a(I)V

    .line 207253
    :cond_2
    iget-object v0, p0, LX/195;->f:LX/198;

    invoke-virtual {v0}, LX/198;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207254
    const v0, -0x2ec9859

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 207255
    :cond_3
    const-wide/16 v0, 0x0

    goto :goto_1

    .line 207256
    :catchall_0
    move-exception v0

    const v1, 0x75b6816

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 207257
    :cond_4
    :try_start_2
    iget-object v1, p0, LX/195;->f:LX/198;

    .line 207258
    sget-object v2, LX/3nS;->a:LX/3nS;

    if-nez v2, :cond_5

    .line 207259
    new-instance v2, LX/3nS;

    invoke-direct {v2}, LX/3nS;-><init>()V

    sput-object v2, LX/3nS;->a:LX/3nS;

    .line 207260
    :cond_5
    sget-object v2, LX/3nS;->a:LX/3nS;

    move-object v2, v2

    .line 207261
    invoke-virtual {v1, v2}, LX/198;->c(LX/1YD;)V

    .line 207262
    new-instance v1, Ljava/lang/ref/WeakReference;

    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, LX/195;->a:Ljava/lang/ref/WeakReference;

    goto :goto_2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 207263
    :cond_6
    iget-object v1, p0, LX/195;->f:LX/198;

    invoke-virtual {v1}, LX/198;->a()LX/1YD;

    move-result-object v1

    .line 207264
    if-nez v1, :cond_7

    .line 207265
    const-string v1, "FrameRateLogger.updateBlameMarker.notBlamed"

    const v2, 0x30e05762

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 207266
    const v1, 0x28195ca1

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_3

    .line 207267
    :cond_7
    iget-object v2, p0, LX/195;->o:Ljava/util/Map;

    invoke-static {v3, v1, v2}, LX/195;->a(ILX/1YD;Ljava/util/Map;)V

    .line 207268
    iget-object v1, p0, LX/195;->f:LX/198;

    .line 207269
    iget-object v2, v1, LX/198;->a:Ljava/util/List;

    move-object v4, v2

    .line 207270
    const/4 v1, 0x0

    move v2, v1

    :goto_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 207271
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1YD;

    iget-object v5, p0, LX/195;->p:Ljava/util/Map;

    invoke-static {v3, v1, v5}, LX/195;->a(ILX/1YD;Ljava/util/Map;)V

    .line 207272
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4
.end method

.method public final a(LX/1KG;)V
    .locals 0

    .prologue
    .line 207283
    iput-object p1, p0, LX/195;->k:LX/1KG;

    .line 207284
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 207273
    iget-boolean v0, p0, LX/195;->n:Z

    if-nez v0, :cond_1

    .line 207274
    :cond_0
    :goto_0
    return-void

    .line 207275
    :cond_1
    invoke-static {p0}, LX/195;->g(LX/195;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 207276
    invoke-static {p0}, LX/195;->g(LX/195;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-wide v1, p0, LX/195;->m:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-gtz v1, :cond_3

    .line 207277
    :cond_2
    :goto_1
    iget-object v0, p0, LX/195;->d:LX/19D;

    invoke-virtual {v0}, LX/19D;->b()V

    .line 207278
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/195;->n:Z

    .line 207279
    iget-object v0, p0, LX/195;->f:LX/198;

    invoke-virtual {v0}, LX/198;->d()V

    .line 207280
    iget-object v0, p0, LX/195;->k:LX/1KG;

    if-eqz v0, :cond_0

    .line 207281
    iget-object v0, p0, LX/195;->k:LX/1KG;

    invoke-interface {v0}, LX/1KG;->b()V

    goto :goto_0

    .line 207282
    :cond_3
    iget-object v1, p0, LX/195;->b:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, LX/195;->e:Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;

    const v3, -0x6a0b08d8

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v1

    iput-object v1, p0, LX/195;->q:Ljava/util/concurrent/Future;

    goto :goto_1
.end method
