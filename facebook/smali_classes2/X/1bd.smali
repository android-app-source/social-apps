.class public LX/1bd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:Z


# direct methods
.method public constructor <init>(IZ)V
    .locals 0

    .prologue
    .line 280755
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 280756
    iput p1, p0, LX/1bd;->a:I

    .line 280757
    iput-boolean p2, p0, LX/1bd;->b:Z

    .line 280758
    return-void
.end method

.method public static a()LX/1bd;
    .locals 3

    .prologue
    .line 280759
    new-instance v0, LX/1bd;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/1bd;-><init>(IZ)V

    return-object v0
.end method

.method public static c()LX/1bd;
    .locals 3

    .prologue
    .line 280754
    new-instance v0, LX/1bd;

    const/4 v1, -0x1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/1bd;-><init>(IZ)V

    return-object v0
.end method


# virtual methods
.method public final d()Z
    .locals 2

    .prologue
    .line 280760
    iget v0, p0, LX/1bd;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 280748
    if-ne p1, p0, :cond_1

    .line 280749
    :cond_0
    :goto_0
    return v0

    .line 280750
    :cond_1
    instance-of v2, p1, LX/1bd;

    if-nez v2, :cond_2

    move v0, v1

    .line 280751
    goto :goto_0

    .line 280752
    :cond_2
    check-cast p1, LX/1bd;

    .line 280753
    iget v2, p0, LX/1bd;->a:I

    iget v3, p1, LX/1bd;->a:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, LX/1bd;->b:Z

    iget-boolean v3, p1, LX/1bd;->b:Z

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 280745
    invoke-virtual {p0}, LX/1bd;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280746
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Rotation is set to use EXIF"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280747
    :cond_0
    iget v0, p0, LX/1bd;->a:I

    return v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 280743
    iget v0, p0, LX/1bd;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-boolean v1, p0, LX/1bd;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, LX/1bi;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 280744
    const/4 v0, 0x0

    const-string v1, "%d defer:%b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, LX/1bd;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-boolean v4, p0, LX/1bd;->b:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
