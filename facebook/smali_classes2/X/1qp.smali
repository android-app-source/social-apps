.class public LX/1qp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qs;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 331496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331497
    return-void
.end method


# virtual methods
.method public final a()LX/0yY;
    .locals 1

    .prologue
    .line 331498
    sget-object v0, LX/0yY;->VIEW_MAP_INTERSTITIAL:LX/0yY;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    .line 331490
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 331491
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 331492
    :goto_0
    const-string v1, "target_fragment"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 331493
    const-class v2, Lcom/facebook/places/create/NewPlaceCreationActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/0cQ;->MAPS_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v0

    if-ne v1, v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 331494
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 331495
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
