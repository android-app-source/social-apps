.class public LX/1dK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field public a:J

.field public b:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public final c:Landroid/graphics/Rect;

.field private d:I

.field private e:I

.field private f:I

.field public g:J

.field private h:LX/1dQ;

.field private i:LX/1dQ;

.field private j:LX/1dQ;

.field private k:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/32D;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/32A;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/48D;",
            ">;"
        }
    .end annotation
.end field

.field private n:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/3CI;",
            ">;"
        }
    .end annotation
.end field

.field private o:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/2uO;",
            ">;"
        }
    .end annotation
.end field

.field private p:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/48E;",
            ">;"
        }
    .end annotation
.end field

.field private q:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/48F;",
            ">;"
        }
    .end annotation
.end field

.field private r:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/48G;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljava/lang/CharSequence;

.field private t:I

.field private u:Ljava/lang/Object;

.field private v:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public w:I

.field private x:I

.field public y:LX/1mp;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 285103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 285104
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1dK;->c:Landroid/graphics/Rect;

    .line 285105
    iput v1, p0, LX/1dK;->w:I

    .line 285106
    iput v1, p0, LX/1dK;->x:I

    .line 285107
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/1dK;->g:J

    .line 285108
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 285077
    iput p1, p0, LX/1dK;->d:I

    .line 285078
    return-void
.end method

.method public final a(IIII)V
    .locals 1

    .prologue
    .line 285079
    iget-object v0, p0, LX/1dK;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 285080
    return-void
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 285081
    iput-wide p1, p0, LX/1dK;->g:J

    .line 285082
    return-void
.end method

.method public final a(LX/1X1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 285083
    iput-object p1, p0, LX/1dK;->b:LX/1X1;

    .line 285084
    return-void
.end method

.method public final a(LX/1dQ;)V
    .locals 0

    .prologue
    .line 285085
    iput-object p1, p0, LX/1dK;->h:LX/1dQ;

    .line 285086
    return-void
.end method

.method public final a(LX/1mp;)V
    .locals 0

    .prologue
    .line 285087
    iput-object p1, p0, LX/1dK;->y:LX/1mp;

    .line 285088
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 285089
    iget-object v0, p0, LX/1dK;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget v1, p0, LX/1dK;->d:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 285090
    iget-object v0, p0, LX/1dK;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget v1, p0, LX/1dK;->e:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 285091
    iget-object v0, p0, LX/1dK;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget v1, p0, LX/1dK;->d:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 285092
    iget-object v0, p0, LX/1dK;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget v1, p0, LX/1dK;->e:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 285093
    return-void
.end method

.method public final a(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 285094
    iput-object p1, p0, LX/1dK;->v:Landroid/util/SparseArray;

    .line 285095
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 285096
    iput-object p1, p0, LX/1dK;->s:Ljava/lang/CharSequence;

    .line 285097
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 285098
    iput-object p1, p0, LX/1dK;->u:Ljava/lang/Object;

    .line 285099
    return-void
.end method

.method public final b()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 285100
    iget-object v0, p0, LX/1dK;->c:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 285122
    iput p1, p0, LX/1dK;->e:I

    .line 285123
    return-void
.end method

.method public b(IIII)V
    .locals 0

    .prologue
    .line 285101
    return-void
.end method

.method public final b(LX/1dQ;)V
    .locals 0

    .prologue
    .line 285120
    iput-object p1, p0, LX/1dK;->i:LX/1dQ;

    .line 285121
    return-void
.end method

.method public b(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 285118
    invoke-virtual {p1}, Landroid/graphics/Rect;->setEmpty()V

    .line 285119
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 285117
    iget v0, p0, LX/1dK;->f:I

    return v0
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 285115
    iput p1, p0, LX/1dK;->f:I

    .line 285116
    return-void
.end method

.method public final c(LX/1dQ;)V
    .locals 0

    .prologue
    .line 285113
    iput-object p1, p0, LX/1dK;->j:LX/1dQ;

    .line 285114
    return-void
.end method

.method public final d(I)V
    .locals 0

    .prologue
    .line 285111
    iput p1, p0, LX/1dK;->t:I

    .line 285112
    return-void
.end method

.method public final d(LX/1dQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dQ",
            "<",
            "LX/32D;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 285109
    iput-object p1, p0, LX/1dK;->k:LX/1dQ;

    .line 285110
    return-void
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 285102
    iget-wide v0, p0, LX/1dK;->a:J

    return-wide v0
.end method

.method public final e(LX/1dQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dQ",
            "<",
            "LX/32A;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 285074
    iput-object p1, p0, LX/1dK;->l:LX/1dQ;

    .line 285075
    return-void
.end method

.method public final f()LX/1dQ;
    .locals 1

    .prologue
    .line 285076
    iget-object v0, p0, LX/1dK;->h:LX/1dQ;

    return-object v0
.end method

.method public final f(I)V
    .locals 0

    .prologue
    .line 285020
    iput p1, p0, LX/1dK;->x:I

    .line 285021
    return-void
.end method

.method public final f(LX/1dQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dQ",
            "<",
            "LX/48D;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 285034
    iput-object p1, p0, LX/1dK;->m:LX/1dQ;

    .line 285035
    return-void
.end method

.method public final g()LX/1dQ;
    .locals 1

    .prologue
    .line 285073
    iget-object v0, p0, LX/1dK;->i:LX/1dQ;

    return-object v0
.end method

.method public final g(LX/1dQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dQ",
            "<",
            "LX/3CI;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 285032
    iput-object p1, p0, LX/1dK;->n:LX/1dQ;

    .line 285033
    return-void
.end method

.method public final h()LX/1dQ;
    .locals 1

    .prologue
    .line 285031
    iget-object v0, p0, LX/1dK;->j:LX/1dQ;

    return-object v0
.end method

.method public final h(LX/1dQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dQ",
            "<",
            "LX/2uO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 285029
    iput-object p1, p0, LX/1dK;->o:LX/1dQ;

    .line 285030
    return-void
.end method

.method public final i()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 285028
    iget-object v0, p0, LX/1dK;->s:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final i(LX/1dQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dQ",
            "<",
            "LX/48E;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 285026
    iput-object p1, p0, LX/1dK;->p:LX/1dQ;

    .line 285027
    return-void
.end method

.method public final j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 285025
    iget-object v0, p0, LX/1dK;->u:Ljava/lang/Object;

    return-object v0
.end method

.method public final j(LX/1dQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dQ",
            "<",
            "LX/48F;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 285023
    iput-object p1, p0, LX/1dK;->q:LX/1dQ;

    .line 285024
    return-void
.end method

.method public final k()Landroid/util/SparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285022
    iget-object v0, p0, LX/1dK;->v:Landroid/util/SparseArray;

    return-object v0
.end method

.method public final k(LX/1dQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dQ",
            "<",
            "LX/48G;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 285018
    iput-object p1, p0, LX/1dK;->r:LX/1dQ;

    .line 285019
    return-void
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 285036
    iget v0, p0, LX/1dK;->x:I

    return v0
.end method

.method public final n()LX/1dQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dQ",
            "<",
            "LX/32D;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285037
    iget-object v0, p0, LX/1dK;->k:LX/1dQ;

    return-object v0
.end method

.method public final o()LX/1dQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dQ",
            "<",
            "LX/32A;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285038
    iget-object v0, p0, LX/1dK;->l:LX/1dQ;

    return-object v0
.end method

.method public final p()LX/1dQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dQ",
            "<",
            "LX/48D;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285039
    iget-object v0, p0, LX/1dK;->m:LX/1dQ;

    return-object v0
.end method

.method public final q()LX/1dQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dQ",
            "<",
            "LX/3CI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285040
    iget-object v0, p0, LX/1dK;->n:LX/1dQ;

    return-object v0
.end method

.method public final r()LX/1dQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dQ",
            "<",
            "LX/2uO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285041
    iget-object v0, p0, LX/1dK;->o:LX/1dQ;

    return-object v0
.end method

.method public final s()LX/1dQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dQ",
            "<",
            "LX/48E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285042
    iget-object v0, p0, LX/1dK;->p:LX/1dQ;

    return-object v0
.end method

.method public final t()LX/1dQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dQ",
            "<",
            "LX/48F;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285043
    iget-object v0, p0, LX/1dK;->q:LX/1dQ;

    return-object v0
.end method

.method public final u()LX/1dQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dQ",
            "<",
            "LX/48G;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285044
    iget-object v0, p0, LX/1dK;->r:LX/1dQ;

    return-object v0
.end method

.method public final v()LX/1mp;
    .locals 1

    .prologue
    .line 285045
    iget-object v0, p0, LX/1dK;->y:LX/1mp;

    return-object v0
.end method

.method public w()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 285046
    iget-object v0, p0, LX/1dK;->b:LX/1X1;

    if-eqz v0, :cond_0

    .line 285047
    iget-object v0, p0, LX/1dK;->b:LX/1X1;

    .line 285048
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/1X1;->g:Z

    .line 285049
    iput-object v2, p0, LX/1dK;->b:LX/1X1;

    .line 285050
    :cond_0
    iput-object v2, p0, LX/1dK;->y:LX/1mp;

    .line 285051
    iget-object v0, p0, LX/1dK;->c:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 285052
    iput v3, p0, LX/1dK;->d:I

    .line 285053
    iput v3, p0, LX/1dK;->e:I

    .line 285054
    iput v3, p0, LX/1dK;->f:I

    .line 285055
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/1dK;->g:J

    .line 285056
    iput-object v2, p0, LX/1dK;->h:LX/1dQ;

    .line 285057
    iput-object v2, p0, LX/1dK;->i:LX/1dQ;

    .line 285058
    iput-object v2, p0, LX/1dK;->j:LX/1dQ;

    .line 285059
    iput-object v2, p0, LX/1dK;->k:LX/1dQ;

    .line 285060
    iput-object v2, p0, LX/1dK;->l:LX/1dQ;

    .line 285061
    iput-object v2, p0, LX/1dK;->m:LX/1dQ;

    .line 285062
    iput-object v2, p0, LX/1dK;->n:LX/1dQ;

    .line 285063
    iput-object v2, p0, LX/1dK;->o:LX/1dQ;

    .line 285064
    iput-object v2, p0, LX/1dK;->p:LX/1dQ;

    .line 285065
    iput-object v2, p0, LX/1dK;->q:LX/1dQ;

    .line 285066
    iput-object v2, p0, LX/1dK;->r:LX/1dQ;

    .line 285067
    iput-object v2, p0, LX/1dK;->s:Ljava/lang/CharSequence;

    .line 285068
    iput-object v2, p0, LX/1dK;->u:Ljava/lang/Object;

    .line 285069
    iput-object v2, p0, LX/1dK;->v:Landroid/util/SparseArray;

    .line 285070
    iput v3, p0, LX/1dK;->w:I

    .line 285071
    iput v3, p0, LX/1dK;->x:I

    .line 285072
    return-void
.end method
