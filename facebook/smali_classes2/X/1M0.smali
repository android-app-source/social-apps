.class public abstract LX/1M0;
.super Ljava/util/AbstractCollection;
.source ""

# interfaces
.implements LX/1M1;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractCollection",
        "<TE;>;",
        "LX/1M1",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private transient a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TE;>;"
        }
    .end annotation
.end field

.field private transient b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 234743
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    .line 234744
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)I
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 234723
    invoke-virtual {p0}, LX/1M0;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4wx;

    .line 234724
    invoke-virtual {v0}, LX/4wx;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 234725
    invoke-virtual {v0}, LX/4wx;->b()I

    move-result v0

    .line 234726
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;I)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)I"
        }
    .end annotation

    .prologue
    .line 234727
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 234728
    iget-object v0, p0, LX/1M0;->b:Ljava/util/Set;

    .line 234729
    if-nez v0, :cond_0

    .line 234730
    invoke-virtual {p0}, LX/1M0;->f()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/1M0;->b:Ljava/util/Set;

    .line 234731
    :cond_0
    return-object v0
.end method

.method public a(Ljava/lang/Object;II)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;II)Z"
        }
    .end annotation

    .prologue
    .line 234732
    const-string v0, "oldCount"

    invoke-static {p2, v0}, LX/0P6;->a(ILjava/lang/String;)I

    .line 234733
    const-string v0, "newCount"

    invoke-static {p3, v0}, LX/0P6;->a(ILjava/lang/String;)I

    .line 234734
    invoke-interface {p0, p1}, LX/1M1;->a(Ljava/lang/Object;)I

    move-result v0

    if-ne v0, p2, :cond_0

    .line 234735
    invoke-interface {p0, p1, p3}, LX/1M1;->c(Ljava/lang/Object;I)I

    .line 234736
    const/4 v0, 0x1

    .line 234737
    :goto_0
    move v0, v0

    .line 234738
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public add(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 234739
    invoke-virtual {p0, p1, v0}, LX/1M0;->a(Ljava/lang/Object;I)I

    .line 234740
    return v0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .prologue
    .line 234741
    invoke-static {p0, p1}, LX/2Tc;->a(LX/1M1;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/Object;I)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 234742
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public abstract b()Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;"
        }
    .end annotation
.end method

.method public abstract c()I
.end method

.method public c(Ljava/lang/Object;I)I
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)I"
        }
    .end annotation

    .prologue
    .line 234745
    const-string v0, "count"

    invoke-static {p2, v0}, LX/0P6;->a(ILjava/lang/String;)I

    .line 234746
    invoke-interface {p0, p1}, LX/1M1;->a(Ljava/lang/Object;)I

    move-result v0

    .line 234747
    sub-int v1, p2, v0

    .line 234748
    if-lez v1, :cond_1

    .line 234749
    invoke-interface {p0, p1, v1}, LX/1M1;->a(Ljava/lang/Object;I)I

    .line 234750
    :cond_0
    :goto_0
    move v0, v0

    .line 234751
    return v0

    .line 234752
    :cond_1
    if-gez v1, :cond_0

    .line 234753
    neg-int v1, v1

    invoke-interface {p0, p1, v1}, LX/1M1;->b(Ljava/lang/Object;I)I

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 234720
    invoke-virtual {p0}, LX/1M0;->b()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0RZ;->h(Ljava/util/Iterator;)V

    .line 234721
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 234722
    invoke-virtual {p0, p1}, LX/1M0;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 234700
    iget-object v0, p0, LX/1M0;->a:Ljava/util/Set;

    .line 234701
    if-nez v0, :cond_0

    .line 234702
    invoke-virtual {p0}, LX/1M0;->e()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/1M0;->a:Ljava/util/Set;

    .line 234703
    :cond_0
    return-object v0
.end method

.method public e()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 234704
    new-instance v0, LX/4x1;

    invoke-direct {v0, p0}, LX/4x1;-><init>(LX/1M0;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 234705
    invoke-static {p0, p1}, LX/2Tc;->a(LX/1M1;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public f()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 234706
    new-instance v0, LX/1M2;

    invoke-direct {v0, p0}, LX/1M2;-><init>(LX/1M0;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 234719
    invoke-virtual {p0}, LX/1M0;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->hashCode()I

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 234707
    invoke-virtual {p0}, LX/1M0;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 234708
    invoke-static {p0}, LX/2Tc;->a(LX/1M1;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 234709
    invoke-virtual {p0, p1, v0}, LX/1M0;->b(Ljava/lang/Object;I)I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 234710
    invoke-static {p0, p1}, LX/2Tc;->b(LX/1M1;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 234711
    invoke-static {p0, p1}, LX/2Tc;->c(LX/1M1;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public size()I
    .locals 6

    .prologue
    .line 234712
    const-wide/16 v1, 0x0

    .line 234713
    invoke-interface {p0}, LX/1M1;->a()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-wide v3, v1

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4wx;

    .line 234714
    invoke-virtual {v1}, LX/4wx;->b()I

    move-result v1

    int-to-long v1, v1

    add-long/2addr v1, v3

    move-wide v3, v1

    .line 234715
    goto :goto_0

    .line 234716
    :cond_0
    invoke-static {v3, v4}, LX/0a4;->b(J)I

    move-result v1

    move v0, v1

    .line 234717
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 234718
    invoke-virtual {p0}, LX/1M0;->a()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
