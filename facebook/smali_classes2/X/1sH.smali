.class public LX/1sH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile m:LX/1sH;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/0ps;

.field private final c:LX/1sz;

.field private final d:LX/0gh;

.field private final e:LX/0Uo;

.field public final f:Ljava/util/Random;

.field public final g:Z

.field public final h:Ljava/lang/Runnable;

.field public final i:Landroid/os/Handler;

.field private j:LX/1wN;

.field public k:J

.field public l:J


# direct methods
.method public constructor <init>(LX/0Zb;LX/0ps;LX/1sz;LX/0Ot;LX/0Uo;LX/1Fg;Ljava/util/Random;LX/0Or;Landroid/os/Handler;)V
    .locals 1
    .param p6    # LX/1Fg;
        .annotation runtime Lcom/facebook/imagepipeline/module/BitmapMemoryCache;
        .end annotation
    .end param
    .param p7    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/imagepipeline/abtest/IsFrescoBitmapCacheLoggingEnabled;
        .end annotation
    .end param
    .param p9    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/0ps;",
            "LX/1sz;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/0Uo;",
            "LX/1Fg;",
            "Ljava/util/Random;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 334522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 334523
    iput-object p1, p0, LX/1sH;->a:LX/0Zb;

    .line 334524
    iput-object p2, p0, LX/1sH;->b:LX/0ps;

    .line 334525
    iput-object p3, p0, LX/1sH;->c:LX/1sz;

    .line 334526
    invoke-interface {p4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    iput-object v0, p0, LX/1sH;->d:LX/0gh;

    .line 334527
    iput-object p5, p0, LX/1sH;->e:LX/0Uo;

    .line 334528
    iput-object p7, p0, LX/1sH;->f:Ljava/util/Random;

    .line 334529
    iput-object p9, p0, LX/1sH;->i:Landroid/os/Handler;

    .line 334530
    invoke-interface {p8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/1sH;->g:Z

    .line 334531
    iget-boolean v0, p0, LX/1sH;->g:Z

    if-eqz v0, :cond_0

    .line 334532
    invoke-direct {p0, p6}, LX/1sH;->a(LX/1Fg;)V

    .line 334533
    new-instance v0, Lcom/facebook/imagepipeline/instrumentation/BitmapCacheStatsLogger$1;

    invoke-direct {v0, p0}, Lcom/facebook/imagepipeline/instrumentation/BitmapCacheStatsLogger$1;-><init>(LX/1sH;)V

    iput-object v0, p0, LX/1sH;->h:Ljava/lang/Runnable;

    .line 334534
    :goto_0
    return-void

    .line 334535
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/1sH;->h:Ljava/lang/Runnable;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1sH;
    .locals 13

    .prologue
    .line 334479
    sget-object v0, LX/1sH;->m:LX/1sH;

    if-nez v0, :cond_1

    .line 334480
    const-class v1, LX/1sH;

    monitor-enter v1

    .line 334481
    :try_start_0
    sget-object v0, LX/1sH;->m:LX/1sH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 334482
    if-eqz v2, :cond_0

    .line 334483
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 334484
    new-instance v3, LX/1sH;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0ps;->a(LX/0QB;)LX/0ps;

    move-result-object v5

    check-cast v5, LX/0ps;

    invoke-static {v0}, LX/1sI;->a(LX/0QB;)LX/1sz;

    move-result-object v6

    check-cast v6, LX/1sz;

    const/16 v7, 0x97

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v8

    check-cast v8, LX/0Uo;

    invoke-static {v0}, LX/1t0;->a(LX/0QB;)LX/1Fg;

    move-result-object v9

    check-cast v9, LX/1Fg;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v10

    check-cast v10, Ljava/util/Random;

    const/16 v11, 0x14cd

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v12

    check-cast v12, Landroid/os/Handler;

    invoke-direct/range {v3 .. v12}, LX/1sH;-><init>(LX/0Zb;LX/0ps;LX/1sz;LX/0Ot;LX/0Uo;LX/1Fg;Ljava/util/Random;LX/0Or;Landroid/os/Handler;)V

    .line 334485
    move-object v0, v3

    .line 334486
    sput-object v0, LX/1sH;->m:LX/1sH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 334487
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 334488
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 334489
    :cond_1
    sget-object v0, LX/1sH;->m:LX/1sH;

    return-object v0

    .line 334490
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 334491
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a(LX/1Fg;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Fg",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 334518
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1sH;->j:LX/1wN;

    if-nez v0, :cond_0

    .line 334519
    new-instance v0, LX/1wN;

    iget-object v1, p0, LX/1sH;->b:LX/0ps;

    iget-object v2, p0, LX/1sH;->c:LX/1sz;

    iget-object v3, p0, LX/1sH;->e:LX/0Uo;

    iget-object v4, p0, LX/1sH;->d:LX/0gh;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/1wN;-><init>(LX/0ps;LX/1sz;LX/0Uo;LX/0gh;LX/1Fg;)V

    iput-object v0, p0, LX/1sH;->j:LX/1wN;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334520
    :cond_0
    monitor-exit p0

    return-void

    .line 334521
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static c(LX/1sH;)V
    .locals 14

    .prologue
    .line 334492
    iget-object v0, p0, LX/1sH;->a:LX/0Zb;

    const-string v1, "fresco_bitmap_cache_event"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 334493
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 334494
    iget-object v1, p0, LX/1sH;->j:LX/1wN;

    invoke-virtual {v1, v0}, LX/1wN;->a(LX/0oG;)V

    .line 334495
    iget-object v1, p0, LX/1sH;->j:LX/1wN;

    invoke-virtual {v1, v0}, LX/1wN;->b(LX/0oG;)V

    .line 334496
    iget-object v1, p0, LX/1sH;->j:LX/1wN;

    .line 334497
    const-string v6, "app_in_background"

    iget-object v7, v1, LX/1wN;->f:LX/0Uo;

    invoke-virtual {v7}, LX/0Uo;->j()Z

    move-result v7

    invoke-virtual {v0, v6, v7}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 334498
    const-string v6, "app_ever_in_foreground"

    iget-object v7, v1, LX/1wN;->f:LX/0Uo;

    invoke-virtual {v7}, LX/0Uo;->m()Z

    move-result v7

    invoke-virtual {v0, v6, v7}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 334499
    const-string v6, "top_analytics_activity"

    iget-object v7, v1, LX/1wN;->g:LX/0gh;

    .line 334500
    iget-object v8, v7, LX/0gh;->G:Ljava/lang/String;

    move-object v7, v8

    .line 334501
    invoke-virtual {v0, v6, v7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 334502
    const-string v6, "top_analytics_activity_duration"

    iget-object v7, v1, LX/1wN;->g:LX/0gh;

    .line 334503
    iget-boolean v10, v7, LX/0gh;->B:Z

    move v10, v10

    .line 334504
    if-eqz v10, :cond_1

    .line 334505
    sget-object v10, Lcom/facebook/common/time/RealtimeSinceBootClock;->a:Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-object v10, v10

    .line 334506
    invoke-virtual {v10}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v10

    iput-wide v10, v7, LX/0gh;->H:J

    .line 334507
    const-wide/16 v10, 0x0

    .line 334508
    :goto_0
    move-wide v8, v10

    .line 334509
    invoke-virtual {v0, v6, v8, v9}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 334510
    const-string v1, "foreground_log_duration"

    .line 334511
    sget-object v2, Lcom/facebook/common/time/RealtimeSinceBootClock;->a:Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-object v2, v2

    .line 334512
    invoke-virtual {v2}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/1sH;->k:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 334513
    const-string v1, "foreground_log_start"

    iget-wide v2, p0, LX/1sH;->k:J

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 334514
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 334515
    :cond_0
    return-void

    .line 334516
    :cond_1
    sget-object v10, Lcom/facebook/common/time/RealtimeSinceBootClock;->a:Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-object v10, v10

    .line 334517
    invoke-virtual {v10}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v10

    iget-wide v12, v7, LX/0gh;->H:J

    sub-long/2addr v10, v12

    goto :goto_0
.end method
