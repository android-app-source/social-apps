.class public LX/0Tx;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64001
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    sput-object v0, LX/0Tx;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 63979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63980
    iput-object p1, p0, LX/0Tx;->b:LX/0Ot;

    .line 63981
    return-void
.end method

.method public static a(LX/0Tx;Ljava/util/SortedMap;I)Ljava/util/SortedMap;
    .locals 11
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/SortedMap",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x5

    const/4 v3, 0x4

    const/4 v4, 0x3

    const/4 v5, 0x2

    .line 63926
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "/app_info"

    sget-object v7, LX/0Tx;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v7}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "//gk"

    sget-object v7, LX/0Tx;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v7}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    move-object v7, v0

    .line 63927
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "/auth/"

    const-string v8, "/auth/user_data/"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/auth_machine_id"

    const-string v8, "/auth/auth_machine_id"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/me_user_version"

    const-string v8, "/auth/me_user_version"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/app_info"

    const-string v8, "/config/app_info"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/gk/"

    const-string v8, "/config/gk/"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/gk_version"

    const-string v8, "/config/gk/version"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/rollout/"

    const-string v8, "/config/rollout"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/rollout_version"

    const-string v8, "/config/rollout/version"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/device_id/"

    const-string v8, "/shared/device_id"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/c2dm/"

    const-string v8, "/messenger/c2dm/"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/first_install_time"

    const-string v8, "/messenger/first_install_time"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/nux_completed"

    const-string v8, "/messenger/nux_completed"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/login_reminder_trigger_state"

    const-string v8, "/messenger/login_reminder_trigger_state"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/phone_confirm"

    const-string v8, "/messenger/phone_confirm"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/sms"

    const-string v8, "/messages/sms"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/ui_counters"

    const-string v8, "/messages/ui_counters"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/notifications/recent_threads"

    const-string v8, "/messages/notifications/recent_threads"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/preferences/notifications/location_services"

    const-string v8, "/settings/messages/location_services"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/preferences/notifications"

    const-string v8, "/settings/messages/notifications"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/preferences/threads"

    const-string v8, "/settings/messages/threads"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/internal/debug_logs"

    const-string v8, "/settings/logging/debug_logs"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/internal/logging_level"

    const-string v8, "/settings/logging/logging_level"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/internal/php_profiling"

    const-string v8, "/settings/http/php_profiling"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/internal/wirehog_profiling"

    const-string v8, "/settings/http/wirehog_profiling"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/internal/force_fb4a_look_and_feel"

    const-string v8, "/settings/messenger/force_fb4a_look_and_feel"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/internal/web"

    const-string v8, "/settings/sandbox/web"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/internal/mqtt"

    const-string v8, "/settings/sandbox/mqtt"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/device_id"

    const-string v8, "/shared/device_id"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/mqtt"

    const-string v8, "/mqtt"

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "/orca/top_last_active_sync_time"

    sget-object v8, LX/0Tx;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    move-object v8, v0

    .line 63928
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0, p1}, Ljava/util/TreeMap;-><init>(Ljava/util/SortedMap;)V

    move-object v0, v0

    .line 63929
    invoke-static {v0}, LX/0Tx;->b(Ljava/util/Map;)I

    move-result v1

    .line 63930
    invoke-static {}, LX/0Tx;->d()LX/0Tn;

    move-result-object v9

    .line 63931
    invoke-static {}, LX/0Tx;->e()LX/0Tn;

    move-result-object v10

    .line 63932
    if-nez v1, :cond_0

    .line 63933
    invoke-static {v0, v7}, LX/0Tx;->a(Ljava/util/SortedMap;Ljava/util/Map;)Ljava/util/SortedMap;

    move-result-object v0

    move v1, v6

    .line 63934
    :cond_0
    if-ne v1, v6, :cond_1

    if-ge v1, p2, :cond_1

    .line 63935
    invoke-interface {v0, v9}, Ljava/util/SortedMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 63936
    invoke-static {v0, v8}, LX/0Tx;->a(Ljava/util/SortedMap;Ljava/util/Map;)Ljava/util/SortedMap;

    move-result-object v0

    .line 63937
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v10, v1}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v5

    .line 63938
    :cond_1
    if-ne v1, v5, :cond_2

    if-ge v1, p2, :cond_2

    .line 63939
    new-instance v1, LX/0Tn;

    const-string v5, "/auth/user_data/fb_me_user"

    invoke-direct {v1, v5}, LX/0Tn;-><init>(Ljava/lang/String;)V

    .line 63940
    new-instance v6, LX/0Tn;

    const-string v5, "/auth/user_data/fb_uid"

    invoke-direct {v6, v5}, LX/0Tn;-><init>(Ljava/lang/String;)V

    .line 63941
    invoke-interface {v0, v1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 63942
    if-nez v1, :cond_9

    .line 63943
    :goto_0
    move-object v0, v0

    .line 63944
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v10, v1}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v4

    .line 63945
    :cond_2
    if-ne v1, v4, :cond_4

    if-ge v1, p2, :cond_4

    .line 63946
    new-instance v4, LX/0Tn;

    const-string v1, "/fb_android/bookmarks/newsfeed_filter_type_key"

    invoke-direct {v4, v1}, LX/0Tn;-><init>(Ljava/lang/String;)V

    .line 63947
    const-string v1, "most_recent"

    .line 63948
    const-string v5, "top_stories"

    .line 63949
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v6, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    move-object v5, v1

    .line 63950
    invoke-interface {v0, v4}, Ljava/util/SortedMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 63951
    instance-of v6, v1, Ljava/lang/String;

    if-eqz v6, :cond_a

    .line 63952
    invoke-interface {v0, v4, v1}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63953
    :cond_3
    :goto_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v10, v1}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v3

    .line 63954
    :cond_4
    if-ne v1, v3, :cond_6

    if-ge v1, p2, :cond_6

    .line 63955
    new-instance v1, LX/0Tn;

    const-string v3, "/fb_android/notifications/polling_interval"

    invoke-direct {v1, v3}, LX/0Tn;-><init>(Ljava/lang/String;)V

    .line 63956
    new-instance v3, LX/0Tn;

    const-string v4, "/notifications/polling_interval"

    invoke-direct {v3, v4}, LX/0Tn;-><init>(Ljava/lang/String;)V

    .line 63957
    invoke-interface {v0, v1}, Ljava/util/SortedMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 63958
    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_5

    .line 63959
    invoke-interface {v0, v3, v1}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63960
    :cond_5
    move-object v0, v0

    .line 63961
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v10, v1}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v2

    .line 63962
    :cond_6
    if-ne v1, v2, :cond_8

    if-ge v1, p2, :cond_8

    .line 63963
    new-instance v1, LX/0Tn;

    const-string v2, "/fb_android/uvm/sync"

    invoke-direct {v1, v2}, LX/0Tn;-><init>(Ljava/lang/String;)V

    .line 63964
    new-instance v2, LX/0Tn;

    const-string v3, "/contactsync/nux_shown"

    invoke-direct {v2, v3}, LX/0Tn;-><init>(Ljava/lang/String;)V

    .line 63965
    invoke-interface {v0, v1}, Ljava/util/SortedMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 63966
    if-eqz v1, :cond_7

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_7

    .line 63967
    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63968
    :cond_7
    move-object v0, v0

    .line 63969
    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v10, v1}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63970
    :cond_8
    return-object v0

    .line 63971
    :cond_9
    :try_start_0
    iget-object v5, p0, LX/0Tx;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0lC;

    invoke-virtual {v5, v1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 63972
    const-string v5, "uid"

    invoke-virtual {v1, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    const/4 v5, 0x0

    invoke-static {v1, v5}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 63973
    invoke-interface {v0, v6, v1}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    goto/16 :goto_0

    .line 63974
    :cond_a
    instance-of v6, v1, Ljava/lang/Integer;

    if-eqz v6, :cond_3

    .line 63975
    check-cast v1, Ljava/lang/Integer;

    .line 63976
    invoke-virtual {v5, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 63977
    if-eqz v1, :cond_3

    .line 63978
    invoke-interface {v0, v4, v1}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1
.end method

.method private static a(Ljava/util/SortedMap;Ljava/util/Map;)Ljava/util/SortedMap;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/SortedMap",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63982
    invoke-static {}, LX/0PM;->f()Ljava/util/TreeMap;

    move-result-object v2

    .line 63983
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 63984
    new-instance v4, LX/0Tn;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v4, v1}, LX/0Tn;-><init>(Ljava/lang/String;)V

    .line 63985
    const/4 v1, 0x0

    .line 63986
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    sget-object v6, LX/0Tx;->a:Ljava/lang/String;

    if-eq v5, v6, :cond_4

    .line 63987
    new-instance v1, LX/0Tn;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, LX/0Tn;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 63988
    :goto_1
    invoke-interface {p0, v4}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v6

    .line 63989
    invoke-interface {v6}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 63990
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Tn;

    invoke-virtual {v5, v4}, LX/0To;->a(LX/0To;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 63991
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v1

    .line 63992
    :goto_2
    move-object v6, v1

    .line 63993
    invoke-interface {v6}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 63994
    if-eqz v0, :cond_1

    .line 63995
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Tn;

    .line 63996
    invoke-virtual {v5, v4}, LX/0To;->b(LX/0To;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v5

    check-cast v5, LX/0Tn;

    .line 63997
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v2, v5, v1}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 63998
    :cond_2
    invoke-interface {v6}, Ljava/util/SortedMap;->clear()V

    .line 63999
    goto/16 :goto_0

    .line 64000
    :cond_3
    return-object v2

    :cond_4
    move-object v0, v1

    goto :goto_1

    :cond_5
    move-object v1, v6

    goto :goto_2
.end method

.method public static b(Ljava/util/Map;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Object;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 63918
    invoke-static {}, LX/0Tx;->e()LX/0Tn;

    move-result-object v0

    .line 63919
    invoke-static {}, LX/0Tx;->d()LX/0Tn;

    move-result-object v1

    .line 63920
    invoke-interface {p0, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 63921
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 63922
    :goto_0
    return v0

    .line 63923
    :cond_0
    invoke-interface {p0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63924
    const/4 v0, 0x1

    goto :goto_0

    .line 63925
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d()LX/0Tn;
    .locals 2

    .prologue
    .line 63917
    new-instance v0, LX/0Tn;

    const-string v1, "/orca/pref_version"

    invoke-direct {v0, v1}, LX/0Tn;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static e()LX/0Tn;
    .locals 2

    .prologue
    .line 63916
    new-instance v0, LX/0Tn;

    const-string v1, "/_meta_/prefs_version"

    invoke-direct {v0, v1}, LX/0Tn;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
