.class public LX/1oi;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static b:LX/1oi;


# instance fields
.field private a:LX/1oj;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/1oi;

    invoke-direct {v0}, LX/1oi;-><init>()V

    sput-object v0, LX/1oi;->b:LX/1oi;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, LX/1oi;->a:LX/1oj;

    return-void
.end method

.method public static a(Landroid/content/Context;)LX/1oj;
    .locals 1

    sget-object v0, LX/1oi;->b:LX/1oi;

    invoke-direct {v0, p0}, LX/1oi;->b(Landroid/content/Context;)LX/1oj;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized b(Landroid/content/Context;)LX/1oj;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1oi;->a:LX/1oj;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    :goto_0
    new-instance v0, LX/1oj;

    invoke-direct {v0, p1}, LX/1oj;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/1oi;->a:LX/1oj;

    :cond_0
    iget-object v0, p0, LX/1oi;->a:LX/1oj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object p1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
