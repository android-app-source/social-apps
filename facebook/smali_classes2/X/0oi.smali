.class public final LX/0oi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0hu;


# direct methods
.method public constructor <init>(LX/0hu;)V
    .locals 0

    .prologue
    .line 141888
    iput-object p1, p0, LX/0oi;->a:LX/0hu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 141889
    iget-object v0, p0, LX/0oi;->a:LX/0hu;

    iget-object v0, v0, LX/0hu;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v1, LX/0hu;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error pre-fetching Inspiration models"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 141890
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 141891
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    .line 141892
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 141893
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 141894
    iget-object v1, p0, LX/0oi;->a:LX/0hu;

    iget-object v1, v1, LX/0hu;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Av5;

    invoke-virtual {v1, v0}, LX/Av5;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)V

    .line 141895
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 141896
    :cond_0
    return-void
.end method
