.class public LX/16u;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/16u;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/16u;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 187702
    const-class v0, LX/16u;

    sput-object v0, LX/16u;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 187703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187704
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/16u;->b:Ljava/util/Map;

    .line 187705
    return-void
.end method

.method public static a(LX/0QB;)LX/16u;
    .locals 3

    .prologue
    .line 187706
    sget-object v0, LX/16u;->c:LX/16u;

    if-nez v0, :cond_1

    .line 187707
    const-class v1, LX/16u;

    monitor-enter v1

    .line 187708
    :try_start_0
    sget-object v0, LX/16u;->c:LX/16u;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 187709
    if-eqz v2, :cond_0

    .line 187710
    :try_start_1
    new-instance v0, LX/16u;

    invoke-direct {v0}, LX/16u;-><init>()V

    .line 187711
    move-object v0, v0

    .line 187712
    sput-object v0, LX/16u;->c:LX/16u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 187713
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 187714
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 187715
    :cond_1
    sget-object v0, LX/16u;->c:LX/16u;

    return-object v0

    .line 187716
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 187717
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 187718
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 187719
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 187720
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 187721
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j()Ljava/lang/String;

    move-result-object v0

    .line 187722
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 187723
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/16u;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 187724
    const/4 v0, 0x0

    .line 187725
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/16u;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 187726
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V
    .locals 2

    .prologue
    .line 187727
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 187728
    :cond_0
    :goto_0
    return-void

    .line 187729
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-static {v0}, LX/16u;->a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v0

    .line 187730
    if-eqz v0, :cond_0

    .line 187731
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/16u;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 187732
    monitor-enter p0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 187733
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 187734
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/16u;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 187735
    iget-object v0, p0, LX/16u;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 187736
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
