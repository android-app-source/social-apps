.class public LX/0iD;
.super LX/0hD;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0go;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0xB;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/12w;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hn;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0xC;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iE;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2t4;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0fO;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0he;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0xW;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0xD;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0f7;

.field public p:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

.field public q:Lcom/facebook/widget/CustomViewPager;

.field public r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/apptab/state/TabTag;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/12j;",
            "Lcom/facebook/apptab/state/TabTag;",
            ">;"
        }
    .end annotation
.end field

.field public t:LX/0xA;

.field public u:LX/0Yb;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 120452
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 120453
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 120454
    iput-object v0, p0, LX/0iD;->b:LX/0Ot;

    .line 120455
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 120456
    iput-object v0, p0, LX/0iD;->c:LX/0Ot;

    .line 120457
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 120458
    iput-object v0, p0, LX/0iD;->d:LX/0Ot;

    .line 120459
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 120460
    iput-object v0, p0, LX/0iD;->e:LX/0Ot;

    .line 120461
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 120462
    iput-object v0, p0, LX/0iD;->f:LX/0Ot;

    .line 120463
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 120464
    iput-object v0, p0, LX/0iD;->g:LX/0Ot;

    .line 120465
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 120466
    iput-object v0, p0, LX/0iD;->h:LX/0Ot;

    .line 120467
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 120468
    iput-object v0, p0, LX/0iD;->i:LX/0Ot;

    .line 120469
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 120470
    iput-object v0, p0, LX/0iD;->j:LX/0Ot;

    .line 120471
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 120472
    iput-object v0, p0, LX/0iD;->k:LX/0Ot;

    .line 120473
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 120474
    iput-object v0, p0, LX/0iD;->l:LX/0Ot;

    .line 120475
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 120476
    iput-object v0, p0, LX/0iD;->m:LX/0Ot;

    .line 120477
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 120478
    iput-object v0, p0, LX/0iD;->n:LX/0Ot;

    .line 120479
    return-void
.end method

.method public static a(LX/0iD;Lcom/facebook/apptab/state/TabTag;I)V
    .locals 2

    .prologue
    .line 120450
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v0, v1}, LX/0iD;->a$redex0(LX/0iD;Lcom/facebook/apptab/state/TabTag;IZLjava/lang/Boolean;)V

    .line 120451
    return-void
.end method

.method public static a$redex0(LX/0iD;LX/12j;I)V
    .locals 2

    .prologue
    .line 120438
    iget-object v0, p0, LX/0iD;->s:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120439
    iget-object v0, p0, LX/0iD;->s:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/TabTag;

    .line 120440
    invoke-static {p0, v0, p2}, LX/0iD;->a(LX/0iD;Lcom/facebook/apptab/state/TabTag;I)V

    .line 120441
    :cond_0
    const/4 v1, 0x0

    .line 120442
    sget-object v0, LX/12j;->INBOX:LX/12j;

    invoke-virtual {p1, v0}, LX/12j;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 120443
    iget-object v0, p0, LX/0iD;->p:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getBadgablePrimaryActionButtonView()Lcom/facebook/apptab/glyph/BadgableGlyphView;

    move-result-object v0

    .line 120444
    :goto_0
    if-eqz v0, :cond_1

    .line 120445
    invoke-virtual {v0, p2}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setUnreadCount(I)V

    .line 120446
    :cond_1
    return-void

    .line 120447
    :cond_2
    sget-object v0, LX/12j;->BACKSTAGE:LX/12j;

    invoke-virtual {p1, v0}, LX/12j;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/0iD;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fO;

    invoke-virtual {v0}, LX/0fO;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 120448
    iget-object v0, p0, LX/0iD;->p:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getBadgableSecondaryActionButtonView()Lcom/facebook/apptab/glyph/BadgableGlyphView;

    move-result-object v1

    .line 120449
    iget-object v0, p0, LX/0iD;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0he;

    invoke-virtual {v0, p2}, LX/0he;->a(I)V

    move-object v0, v1

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public static a$redex0(LX/0iD;Lcom/facebook/apptab/state/TabTag;IZLjava/lang/Boolean;)V
    .locals 4
    .param p3    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 120480
    if-lez p2, :cond_1

    iget-object v0, p0, LX/0iD;->o:LX/0f7;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0f7;->a(Lcom/facebook/base/fragment/FbFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, LX/0iD;->r:Ljava/util/List;

    iget-object v2, p0, LX/0iD;->q:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/TabTag;

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p3, :cond_1

    .line 120481
    :cond_0
    :goto_0
    return-void

    .line 120482
    :cond_1
    iget-object v0, p0, LX/0iD;->o:LX/0f7;

    invoke-virtual {p1}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0f7;->a(Ljava/lang/String;)LX/0hO;

    move-result-object v1

    .line 120483
    if-eqz v1, :cond_0

    .line 120484
    sget-object v0, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 120485
    :cond_2
    iget-object v0, p0, LX/0iD;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xD;

    .line 120486
    iget-object v2, v0, LX/0xD;->b:LX/0Uh;

    const/16 v3, 0x3bb

    const/4 p3, 0x0

    invoke-virtual {v2, v3, p3}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v0, LX/0xD;->f:Lcom/facebook/apptab/ui/DownloadableImageTabView;

    if-eqz v2, :cond_4

    .line 120487
    if-nez p2, :cond_a

    const-string v2, ""

    :goto_1
    iput-object v2, v0, LX/0xD;->d:Ljava/lang/String;

    .line 120488
    iget-object v2, v0, LX/0xD;->f:Lcom/facebook/apptab/ui/DownloadableImageTabView;

    iget-object v3, v0, LX/0xD;->d:Ljava/lang/String;

    .line 120489
    iput-object v3, v2, Lcom/facebook/apptab/ui/DownloadableImageTabView;->l:Ljava/lang/String;

    .line 120490
    iget-object p3, v2, Lcom/facebook/apptab/ui/DownloadableImageTabView;->l:Ljava/lang/String;

    if-eqz p3, :cond_3

    iget-object p3, v2, Lcom/facebook/apptab/ui/DownloadableImageTabView;->l:Ljava/lang/String;

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_b

    .line 120491
    :cond_3
    iget p3, v2, Lcom/facebook/apptab/ui/DownloadableImageTabView;->m:I

    if-nez p3, :cond_d

    .line 120492
    :cond_4
    :goto_2
    sget-object v0, Lcom/facebook/video/videohome/tab/VideoHomeTab;->l:Lcom/facebook/video/videohome/tab/VideoHomeTab;

    if-ne p1, v0, :cond_8

    .line 120493
    iget-object v0, p0, LX/0iD;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hn;

    .line 120494
    iget v2, v0, LX/0hn;->v:I

    const/16 v3, 0x63

    if-ge v2, v3, :cond_e

    const/4 v2, 0x1

    :goto_3
    move v0, v2

    .line 120495
    iput-boolean v0, v1, Lcom/facebook/apptab/glyph/BadgableGlyphView;->d:Z

    .line 120496
    :cond_5
    :goto_4
    invoke-virtual {v1, p2}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setUnreadCount(I)V

    .line 120497
    iget-object v0, p0, LX/0iD;->o:LX/0f7;

    invoke-interface {v0, p1, v1}, LX/0f7;->a(Lcom/facebook/apptab/state/TabTag;LX/0hO;)V

    .line 120498
    sget-object v0, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    if-eq p1, v0, :cond_6

    sget-object v0, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    if-ne p1, v0, :cond_9

    :cond_6
    const/4 v0, 0x1

    .line 120499
    :goto_5
    if-eqz p2, :cond_7

    if-eqz v0, :cond_7

    .line 120500
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "jewel_lit_up"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 120501
    const-string v0, "jewel_name"

    invoke-virtual {p1}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 120502
    const-string v0, "jewel_count"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 120503
    iget-object v0, p0, LX/0iD;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 120504
    :cond_7
    sget-object v0, Lcom/facebook/marketplace/tab/MarketplaceTab;->m:Lcom/facebook/marketplace/tab/MarketplaceTab;

    if-ne p1, v0, :cond_0

    .line 120505
    iget-object v0, p0, LX/0iD;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iE;

    invoke-interface {v0}, LX/0iE;->a()V

    goto/16 :goto_0

    .line 120506
    :cond_8
    if-eqz p4, :cond_5

    .line 120507
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 120508
    iput-boolean v0, v1, Lcom/facebook/apptab/glyph/BadgableGlyphView;->d:Z

    .line 120509
    goto :goto_4

    .line 120510
    :cond_9
    const/4 v0, 0x0

    goto :goto_5

    .line 120511
    :cond_a
    iget-object v2, v0, LX/0xD;->d:Ljava/lang/String;

    goto/16 :goto_1

    .line 120512
    :cond_b
    iget-object p3, v2, Lcom/facebook/apptab/ui/DownloadableImageTabView;->l:Ljava/lang/String;

    .line 120513
    iget-object v0, v2, Lcom/facebook/apptab/ui/DownloadableImageTabView;->n:LX/1aX;

    if-nez v0, :cond_c

    .line 120514
    new-instance v0, LX/1Uo;

    invoke-virtual {v2}, Lcom/facebook/apptab/ui/DownloadableImageTabView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v0, v3}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 120515
    invoke-virtual {v2}, Lcom/facebook/apptab/ui/DownloadableImageTabView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v0, v3}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, v2, Lcom/facebook/apptab/ui/DownloadableImageTabView;->n:LX/1aX;

    .line 120516
    :cond_c
    iget-object v0, v2, Lcom/facebook/apptab/ui/DownloadableImageTabView;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p3}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    sget-object v3, Lcom/facebook/apptab/ui/DownloadableImageTabView;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v3, v2, Lcom/facebook/apptab/ui/DownloadableImageTabView;->n:LX/1aX;

    .line 120517
    iget-object p3, v3, LX/1aX;->f:LX/1aZ;

    move-object v3, p3

    .line 120518
    invoke-virtual {v0, v3}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    new-instance v3, LX/7fy;

    invoke-direct {v3, v2}, LX/7fy;-><init>(Lcom/facebook/apptab/ui/DownloadableImageTabView;)V

    invoke-virtual {v0, v3}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 120519
    iget-object v3, v2, Lcom/facebook/apptab/ui/DownloadableImageTabView;->n:LX/1aX;

    invoke-virtual {v3, v0}, LX/1aX;->a(LX/1aZ;)V

    .line 120520
    goto/16 :goto_2

    .line 120521
    :cond_d
    iget p3, v2, Lcom/facebook/apptab/ui/DownloadableImageTabView;->m:I

    invoke-virtual {v2, p3}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setTabIconImageResource(I)V

    .line 120522
    iget-object p3, v2, Lcom/facebook/apptab/ui/DownloadableImageTabView;->n:LX/1aX;

    if-eqz p3, :cond_4

    .line 120523
    iget-object p3, v2, Lcom/facebook/apptab/ui/DownloadableImageTabView;->n:LX/1aX;

    const/4 v0, 0x0

    invoke-virtual {p3, v0}, LX/1aX;->a(LX/1aZ;)V

    goto/16 :goto_2

    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_3
.end method

.method public static g(LX/0iD;)I
    .locals 3

    .prologue
    .line 120429
    iget-object v0, p0, LX/0iD;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xB;

    sget-object v1, LX/12j;->NOTIFICATIONS:LX/12j;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/12j;)I

    move-result v1

    .line 120430
    iget-object v0, p0, LX/0iD;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xW;

    invoke-virtual {v0}, LX/0xW;->d()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 120431
    :goto_0
    return v0

    .line 120432
    :cond_0
    iget-object v0, p0, LX/0iD;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xB;

    sget-object v2, LX/12j;->FRIEND_REQUESTS:LX/12j;

    invoke-virtual {v0, v2}, LX/0xB;->a(LX/12j;)I

    move-result v2

    .line 120433
    iget-object v0, p0, LX/0iD;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xW;

    invoke-virtual {v0}, LX/0xW;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120434
    add-int v0, v1, v2

    goto :goto_0

    .line 120435
    :cond_1
    iget-object v0, p0, LX/0iD;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xW;

    invoke-virtual {v0}, LX/0xW;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 120436
    if-le v2, v1, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 120437
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 5

    .prologue
    .line 120385
    instance-of v0, p1, LX/0f7;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    move-object v0, p1

    .line 120386
    check-cast v0, LX/0f7;

    iput-object v0, p0, LX/0iD;->o:LX/0f7;

    .line 120387
    const v0, 0x7f0d00bc

    invoke-static {p1, v0}, LX/0jc;->a(Landroid/app/Activity;I)LX/0am;

    move-result-object v0

    .line 120388
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_0

    .line 120389
    :goto_0
    return-void

    .line 120390
    :cond_0
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    iput-object v0, p0, LX/0iD;->p:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    .line 120391
    const v0, 0x7f0d1a6b

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomViewPager;

    iput-object v0, p0, LX/0iD;->q:Lcom/facebook/widget/CustomViewPager;

    .line 120392
    iget-object v0, p0, LX/0iD;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0go;

    invoke-virtual {v0}, LX/0go;->a()Lcom/facebook/apptab/state/NavigationConfig;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/apptab/state/NavigationConfig;->a:LX/0Px;

    iput-object v0, p0, LX/0iD;->r:Ljava/util/List;

    .line 120393
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 120394
    sget-object v1, LX/12j;->FRIEND_REQUESTS:LX/12j;

    sget-object v2, Lcom/facebook/friending/tab/FriendRequestsTab;->l:Lcom/facebook/friending/tab/FriendRequestsTab;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 120395
    iget-object v1, p0, LX/0iD;->r:Ljava/util/List;

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 120396
    sget-object v1, LX/12j;->NOTIFICATIONS:LX/12j;

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 120397
    :goto_1
    iget-object v1, p0, LX/0iD;->r:Ljava/util/List;

    sget-object v2, Lcom/facebook/video/videohome/tab/VideoHomeTab;->l:Lcom/facebook/video/videohome/tab/VideoHomeTab;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 120398
    sget-object v1, LX/12j;->VIDEO_HOME:LX/12j;

    sget-object v2, Lcom/facebook/video/videohome/tab/VideoHomeTab;->l:Lcom/facebook/video/videohome/tab/VideoHomeTab;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 120399
    :cond_1
    iget-object v1, p0, LX/0iD;->r:Ljava/util/List;

    sget-object v2, Lcom/facebook/marketplace/tab/MarketplaceTab;->m:Lcom/facebook/marketplace/tab/MarketplaceTab;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 120400
    sget-object v1, LX/12j;->MARKETPLACE:LX/12j;

    sget-object v2, Lcom/facebook/marketplace/tab/MarketplaceTab;->m:Lcom/facebook/marketplace/tab/MarketplaceTab;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 120401
    :cond_2
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/0iD;->s:LX/0P1;

    .line 120402
    iget-object v0, p0, LX/0iD;->s:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12j;

    .line 120403
    sget-object v1, LX/12j;->VIDEO_HOME:LX/12j;

    if-ne v0, v1, :cond_3

    .line 120404
    iget-object v1, p0, LX/0iD;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0xB;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/0xB;->a(LX/12j;I)V

    goto :goto_2

    .line 120405
    :cond_4
    iget-object v0, p0, LX/0iD;->s:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12j;

    .line 120406
    iget-object v1, p0, LX/0iD;->s:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/apptab/state/TabTag;

    sget-object v2, LX/12j;->NOTIFICATIONS:LX/12j;

    if-ne v0, v2, :cond_5

    invoke-static {p0}, LX/0iD;->g(LX/0iD;)I

    move-result v0

    :goto_4
    invoke-static {p0, v1, v0}, LX/0iD;->a(LX/0iD;Lcom/facebook/apptab/state/TabTag;I)V

    goto :goto_3

    :cond_5
    iget-object v2, p0, LX/0iD;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0xB;

    invoke-virtual {v2, v0}, LX/0xB;->a(LX/12j;)I

    move-result v0

    goto :goto_4

    .line 120407
    :cond_6
    iget-object v0, p0, LX/0iD;->p:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getBadgablePrimaryActionButtonView()Lcom/facebook/apptab/glyph/BadgableGlyphView;

    move-result-object v1

    .line 120408
    if-eqz v1, :cond_7

    .line 120409
    iget-object v0, p0, LX/0iD;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xB;

    sget-object v2, LX/12j;->INBOX:LX/12j;

    invoke-virtual {v0, v2}, LX/0xB;->a(LX/12j;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setUnreadCount(I)V

    .line 120410
    :cond_7
    iget-object v0, p0, LX/0iD;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fO;

    invoke-virtual {v0}, LX/0fO;->p()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 120411
    iget-object v0, p0, LX/0iD;->p:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getBadgableSecondaryActionButtonView()Lcom/facebook/apptab/glyph/BadgableGlyphView;

    move-result-object v1

    .line 120412
    if-eqz v1, :cond_8

    .line 120413
    iget-object v0, p0, LX/0iD;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xB;

    sget-object v2, LX/12j;->BACKSTAGE:LX/12j;

    invoke-virtual {v0, v2}, LX/0xB;->a(LX/12j;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setUnreadCount(I)V

    .line 120414
    iget-object v0, p0, LX/0iD;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0he;

    iget-object v1, p0, LX/0iD;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0xB;

    sget-object v2, LX/12j;->BACKSTAGE:LX/12j;

    invoke-virtual {v1, v2}, LX/0xB;->a(LX/12j;)I

    move-result v1

    invoke-virtual {v0, v1}, LX/0he;->a(I)V

    .line 120415
    :cond_8
    new-instance v0, LX/12v;

    invoke-direct {v0, p0}, LX/12v;-><init>(LX/0iD;)V

    iput-object v0, p0, LX/0iD;->t:LX/0xA;

    .line 120416
    iget-object v0, p0, LX/0iD;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xB;

    iget-object v1, p0, LX/0iD;->t:LX/0xA;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/0xA;)V

    .line 120417
    iget-object v0, p0, LX/0iD;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12w;

    .line 120418
    iget-boolean v1, v0, LX/12w;->k:Z

    if-eqz v1, :cond_a

    .line 120419
    :goto_5
    iget-object v0, p0, LX/0iD;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    .line 120420
    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    .line 120421
    const-string v1, "com.facebook.feed.util.NEWS_FEED_NEW_STORIES"

    new-instance v2, LX/132;

    invoke-direct {v2, p0}, LX/132;-><init>(LX/0iD;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    .line 120422
    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/0iD;->u:LX/0Yb;

    .line 120423
    iget-object v0, p0, LX/0iD;->u:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 120424
    goto/16 :goto_0

    .line 120425
    :cond_9
    sget-object v1, LX/12j;->NOTIFICATIONS:LX/12j;

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto/16 :goto_1

    .line 120426
    :cond_a
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/12w;->k:Z

    .line 120427
    iget-object v1, v0, LX/12w;->f:LX/0Sg;

    const-string v2, "JewelCountFetcher-schedule"

    iget-object v3, v0, LX/12w;->l:Ljava/lang/Runnable;

    sget-object v4, LX/0VZ;->APPLICATION_LOADED_UI_IDLE_HIGH_PRIORITY:LX/0VZ;

    sget-object p1, LX/0Vm;->BACKGROUND:LX/0Vm;

    invoke-virtual {v1, v2, v3, v4, p1}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    move-result-object v1

    .line 120428
    new-instance v2, LX/130;

    invoke-direct {v2, v0}, LX/130;-><init>(LX/12w;)V

    invoke-static {v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_5
.end method
