.class public final LX/0Us;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/MessageQueue$IdleHandler;


# instance fields
.field public final synthetic a:LX/0Uo;


# direct methods
.method public constructor <init>(LX/0Uo;)V
    .locals 0

    .prologue
    .line 67153
    iput-object p1, p0, LX/0Us;->a:LX/0Uo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final queueIdle()Z
    .locals 10

    .prologue
    .line 67154
    iget-object v0, p0, LX/0Us;->a:LX/0Uo;

    const/4 v3, 0x1

    .line 67155
    iget-object v2, v0, LX/0Uo;->u:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ks;

    .line 67156
    iget-boolean v4, v2, LX/0ks;->b:Z

    move v2, v4

    .line 67157
    if-nez v2, :cond_0

    invoke-virtual {v0}, LX/0Uo;->l()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move v2, v3

    .line 67158
    :goto_0
    move v0, v2

    .line 67159
    if-eqz v0, :cond_1

    .line 67160
    iget-object v0, p0, LX/0Us;->a:LX/0Uo;

    iget-object v0, v0, LX/0Uo;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 67161
    iget-object v0, p0, LX/0Us;->a:LX/0Uo;

    iget-object v0, v0, LX/0Uo;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1rB;

    invoke-virtual {v0}, LX/1rB;->a()V

    .line 67162
    const/4 v0, 0x0

    .line 67163
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 67164
    :cond_2
    iget-object v2, v0, LX/0Uo;->q:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v4

    .line 67165
    iget-wide v8, v0, LX/0Uo;->J:J

    move-wide v6, v8

    .line 67166
    sub-long/2addr v4, v6

    const-wide/32 v6, 0xea60

    cmp-long v2, v4, v6

    if-ltz v2, :cond_3

    move v2, v3

    .line 67167
    goto :goto_0

    .line 67168
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method
