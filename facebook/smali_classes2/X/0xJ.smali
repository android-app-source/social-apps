.class public LX/0xJ;
.super Ljava/io/Writer;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 162922
    invoke-direct {p0}, Ljava/io/Writer;-><init>()V

    .line 162923
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, LX/0xJ;->b:Ljava/lang/StringBuilder;

    .line 162924
    iput-object p1, p0, LX/0xJ;->a:Ljava/lang/String;

    .line 162925
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 162919
    iget-object v0, p0, LX/0xJ;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 162920
    iget-object v0, p0, LX/0xJ;->b:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    iget-object v2, p0, LX/0xJ;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 162921
    :cond_0
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 0

    .prologue
    .line 162908
    invoke-direct {p0}, LX/0xJ;->a()V

    .line 162909
    return-void
.end method

.method public final flush()V
    .locals 0

    .prologue
    .line 162917
    invoke-direct {p0}, LX/0xJ;->a()V

    .line 162918
    return-void
.end method

.method public final write([CII)V
    .locals 3

    .prologue
    .line 162910
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_1

    .line 162911
    add-int v1, p2, v0

    aget-char v1, p1, v1

    .line 162912
    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    .line 162913
    invoke-direct {p0}, LX/0xJ;->a()V

    .line 162914
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 162915
    :cond_0
    iget-object v2, p0, LX/0xJ;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 162916
    :cond_1
    return-void
.end method
