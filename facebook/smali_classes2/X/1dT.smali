.class public LX/1dT;
.super LX/1S3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/graphics/drawable/Drawable;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field public static a:LX/1dT;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 286006
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 286007
    return-void
.end method

.method public static declared-synchronized q()LX/1dT;
    .locals 2

    .prologue
    .line 286009
    const-class v1, LX/1dT;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1dT;->a:LX/1dT;

    if-nez v0, :cond_0

    .line 286010
    new-instance v0, LX/1dT;

    invoke-direct {v0}, LX/1dT;-><init>()V

    sput-object v0, LX/1dT;->a:LX/1dT;

    .line 286011
    :cond_0
    sget-object v0, LX/1dT;->a:LX/1dT;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 286012
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1Dg;LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/components/ComponentLayout;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 285981
    check-cast p3, LX/1mw;

    .line 285982
    invoke-virtual {p2}, LX/1Dg;->c()I

    move-result v0

    .line 285983
    iput v0, p3, LX/1mw;->b:I

    .line 285984
    invoke-virtual {p2}, LX/1Dg;->d()I

    move-result v0

    .line 285985
    iput v0, p3, LX/1mw;->c:I

    .line 285986
    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 286008
    new-instance v0, LX/1oE;

    invoke-direct {v0}, LX/1oE;-><init>()V

    return-object v0
.end method

.method public final c(LX/1X1;LX/1X1;)Z
    .locals 2

    .prologue
    .line 285995
    check-cast p1, LX/1mw;

    .line 285996
    iget-object v0, p1, LX/1mw;->a:LX/1dc;

    move-object v0, v0

    .line 285997
    check-cast p2, LX/1mw;

    .line 285998
    iget-object v1, p2, LX/1mw;->a:LX/1dc;

    move-object v1, v1

    .line 285999
    invoke-static {v0, v1}, LX/1dc;->a(LX/1dc;LX/1dc;)Z

    move-result v0

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 286000
    check-cast p2, LX/1oE;

    .line 286001
    check-cast p3, LX/1mw;

    .line 286002
    iget-object v0, p3, LX/1mw;->a:LX/1dc;

    move-object v0, v0

    .line 286003
    invoke-static {p1, v0}, LX/1dc;->a(LX/1De;LX/1dc;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 286004
    const/4 p0, 0x0

    invoke-virtual {p2, v0, p0}, LX/1oE;->a(Landroid/graphics/drawable/Drawable;LX/1oA;)V

    .line 286005
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 285994
    sget-object v0, LX/1mv;->DRAWABLE:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/Object;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 285987
    check-cast p3, LX/1mw;

    .line 285988
    check-cast p2, LX/1oE;

    .line 285989
    iget-object v0, p2, LX/1oE;->a:Landroid/graphics/drawable/Drawable;

    move-object v0, v0

    .line 285990
    iget-object v1, p3, LX/1mw;->a:LX/1dc;

    move-object v1, v1

    .line 285991
    invoke-static {p1, v0, v1}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 285992
    invoke-virtual {p2}, LX/1oE;->a()V

    .line 285993
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/Object;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 285975
    check-cast p2, LX/1oE;

    .line 285976
    check-cast p3, LX/1mw;

    .line 285977
    iget v0, p3, LX/1mw;->b:I

    move v0, v0

    .line 285978
    iget v1, p3, LX/1mw;->c:I

    move v1, v1

    .line 285979
    invoke-virtual {p2, v0, v1}, LX/1oE;->a(II)V

    .line 285980
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 285974
    const/4 v0, 0x1

    return v0
.end method
