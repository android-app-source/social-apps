.class public LX/0h2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/10C;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/ArM;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Aw7;",
            ">;"
        }
    .end annotation
.end field

.field public volatile e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1vx;",
            ">;"
        }
    .end annotation
.end field

.field public volatile f:LX/0Or;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public volatile g:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0zF;",
            ">;"
        }
    .end annotation
.end field

.field public h:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FiT;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0i0;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0fO;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17X;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/23i;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2PR;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0he;",
            ">;"
        }
    .end annotation
.end field

.field public final r:LX/0fd;

.field public final s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

.field public t:I

.field public u:I


# direct methods
.method public constructor <init>(LX/0fd;Landroid/view/View;)V
    .locals 2
    .param p1    # LX/0fd;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 114626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114627
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 114628
    iput-object v0, p0, LX/0h2;->i:LX/0Ot;

    .line 114629
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 114630
    iput-object v0, p0, LX/0h2;->j:LX/0Ot;

    .line 114631
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 114632
    iput-object v0, p0, LX/0h2;->k:LX/0Ot;

    .line 114633
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 114634
    iput-object v0, p0, LX/0h2;->l:LX/0Ot;

    .line 114635
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 114636
    iput-object v0, p0, LX/0h2;->m:LX/0Ot;

    .line 114637
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 114638
    iput-object v0, p0, LX/0h2;->n:LX/0Ot;

    .line 114639
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 114640
    iput-object v0, p0, LX/0h2;->o:LX/0Ot;

    .line 114641
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 114642
    iput-object v0, p0, LX/0h2;->p:LX/0Ot;

    .line 114643
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 114644
    iput-object v0, p0, LX/0h2;->q:LX/0Ot;

    .line 114645
    iput v1, p0, LX/0h2;->t:I

    .line 114646
    iput v1, p0, LX/0h2;->u:I

    .line 114647
    iput-object p1, p0, LX/0h2;->r:LX/0fd;

    .line 114648
    check-cast p2, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    iput-object p2, p0, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    .line 114649
    return-void
.end method

.method public static a(LX/0h2;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0h2;",
            "LX/0Or",
            "<",
            "LX/10C;",
            ">;",
            "LX/0Or",
            "<",
            "LX/ArM;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0ad;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Aw7;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1vx;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0zF;",
            ">;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/FiT;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0i0;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0fO;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17X;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/23i;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2PR;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0he;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114650
    iput-object p1, p0, LX/0h2;->a:LX/0Or;

    iput-object p2, p0, LX/0h2;->b:LX/0Or;

    iput-object p3, p0, LX/0h2;->c:LX/0Or;

    iput-object p4, p0, LX/0h2;->d:LX/0Or;

    iput-object p5, p0, LX/0h2;->e:LX/0Or;

    iput-object p6, p0, LX/0h2;->f:LX/0Or;

    iput-object p7, p0, LX/0h2;->g:LX/0Or;

    iput-object p8, p0, LX/0h2;->h:Landroid/content/Context;

    iput-object p9, p0, LX/0h2;->i:LX/0Ot;

    iput-object p10, p0, LX/0h2;->j:LX/0Ot;

    iput-object p11, p0, LX/0h2;->k:LX/0Ot;

    iput-object p12, p0, LX/0h2;->l:LX/0Ot;

    iput-object p13, p0, LX/0h2;->m:LX/0Ot;

    iput-object p14, p0, LX/0h2;->n:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/0h2;->o:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/0h2;->p:LX/0Ot;

    move-object/from16 v0, p17

    iput-object v0, p0, LX/0h2;->q:LX/0Ot;

    return-void
.end method

.method private static h(LX/0h2;)V
    .locals 4

    .prologue
    .line 114651
    iget-object v0, p0, LX/0h2;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ArM;

    .line 114652
    iget-object v1, v0, LX/ArM;->a:LX/0Zb;

    sget-object v2, LX/ArH;->CAMERA_BUTTON_IN_FEED_IMPRESSION:LX/ArH;

    invoke-static {v2}, LX/ArM;->a(LX/ArH;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 114653
    iget-object v0, p0, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    .line 114654
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const v2, 0x7f0207b3

    .line 114655
    iput v2, v1, LX/108;->i:I

    .line 114656
    move-object v1, v1

    .line 114657
    iget-object v2, p0, LX/0h2;->h:Landroid/content/Context;

    const v3, 0x7f083710

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 114658
    iput-object v2, v1, LX/108;->j:Ljava/lang/String;

    .line 114659
    move-object v1, v1

    .line 114660
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    move-object v1, v1

    .line 114661
    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setLeftButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 114662
    iget-object v0, p0, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    new-instance v1, LX/HeX;

    invoke-direct {v1, p0}, LX/HeX;-><init>(LX/0h2;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setLeftActionButtonOnClickListener(LX/107;)V

    .line 114663
    return-void
.end method

.method public static j(LX/0h2;)Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
    .locals 5

    .prologue
    .line 114664
    iget-object v0, p0, LX/0h2;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10C;

    .line 114665
    iget-object v1, p0, LX/0h2;->h:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 114666
    iget-object v2, v0, LX/10C;->a:LX/10D;

    .line 114667
    iget-object v3, v2, LX/10D;->a:LX/0fO;

    invoke-virtual {v3}, LX/0fO;->p()Z

    move-result v3

    move v2, v3

    .line 114668
    if-eqz v2, :cond_0

    .line 114669
    iget-object v2, v0, LX/10C;->a:LX/10D;

    invoke-virtual {v2, v1}, LX/10D;->a(Landroid/content/res/Resources;)Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    .line 114670
    :goto_0
    move-object v0, v2

    .line 114671
    return-object v0

    .line 114672
    :cond_0
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    const v3, 0x7f081a16

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 114673
    iput-object v3, v2, LX/108;->j:Ljava/lang/String;

    .line 114674
    move-object v2, v2

    .line 114675
    iget-object v3, v0, LX/10C;->b:LX/0wM;

    const v4, 0x7f020897

    const/4 p0, -0x1

    invoke-virtual {v3, v4, p0}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 114676
    iput-object v3, v2, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 114677
    move-object v2, v2

    .line 114678
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    goto :goto_0
.end method

.method public static k(LX/0h2;)Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
    .locals 3

    .prologue
    .line 114679
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    iget-object v1, p0, LX/0h2;->h:Landroid/content/Context;

    const v2, 0x7f08370f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 114680
    iput-object v1, v0, LX/108;->j:Ljava/lang/String;

    .line 114681
    move-object v0, v0

    .line 114682
    invoke-static {}, LX/10A;->a()I

    move-result v1

    .line 114683
    iput v1, v0, LX/108;->i:I

    .line 114684
    move-object v0, v0

    .line 114685
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 114686
    const-string v0, "FbMainTabActivity.onCreateSetupTitleBar"

    const v1, -0x7d7e8997

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 114687
    :try_start_0
    iget-object v0, p0, LX/0h2;->r:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->f()LX/0Px;

    move-result-object v0

    sget-object v1, Lcom/facebook/video/videohome/tab/VideoHomeTab;->l:Lcom/facebook/video/videohome/tab/VideoHomeTab;

    invoke-virtual {v0, v1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, LX/0h2;->t:I

    .line 114688
    iget-object v0, p0, LX/0h2;->r:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->f()LX/0Px;

    move-result-object v0

    sget-object v1, Lcom/facebook/marketplace/tab/MarketplaceTab;->m:Lcom/facebook/marketplace/tab/MarketplaceTab;

    invoke-virtual {v0, v1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, LX/0h2;->u:I

    .line 114689
    iget-object v0, p0, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    new-instance v1, LX/0zD;

    invoke-direct {v1, p0}, LX/0zD;-><init>(LX/0h2;)V

    .line 114690
    iput-object v1, v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->l:LX/0zE;

    .line 114691
    iget-object v0, p0, LX/0h2;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zF;

    .line 114692
    iget-object v1, p0, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    .line 114693
    iput-object v1, v0, LX/0zF;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 114694
    iget-object v0, p0, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    new-instance v1, LX/0zH;

    invoke-direct {v1, p0}, LX/0zH;-><init>(LX/0h2;)V

    .line 114695
    iput-object v1, v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->k:Landroid/view/View$OnClickListener;

    .line 114696
    iget-object v0, p0, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    sget-object v1, LX/0zI;->DEFAULT:LX/0zI;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->a(LX/0zI;)V

    .line 114697
    iget-object v0, p0, LX/0h2;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 114698
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 114699
    new-instance v0, LX/HeW;

    invoke-direct {v0, p0}, LX/HeW;-><init>(LX/0h2;)V

    .line 114700
    iget-object v1, p0, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-static {p0}, LX/0h2;->k(LX/0h2;)Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 114701
    iget-object v1, p0, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 114702
    :cond_0
    :goto_0
    iget-object v0, p0, LX/0h2;->r:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->q()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 114703
    invoke-static {p0}, LX/0h2;->h(LX/0h2;)V

    .line 114704
    iget-object v0, p0, LX/0h2;->r:LX/0fd;

    .line 114705
    iget-object v1, v0, LX/0fd;->a:LX/0fF;

    invoke-interface {v1}, LX/0fF;->A()V

    .line 114706
    :cond_1
    :goto_1
    iget-object v0, p0, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    iget-object v1, p0, LX/0h2;->r:LX/0fd;

    invoke-virtual {v1}, LX/0fd;->b()Lcom/facebook/apptab/state/TabTag;

    move-result-object v1

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->j:I

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 114707
    iget-object v0, p0, LX/0h2;->r:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114708
    iget-object v0, p0, LX/0h2;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Aw7;

    .line 114709
    iget-object v1, p0, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getLeftActionButton()Landroid/view/View;

    move-result-object v1

    .line 114710
    iget-object v2, v0, LX/Aw7;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0iA;

    const-string v3, "4493"

    const-class p0, LX/Aw6;

    invoke-virtual {v2, v3, p0}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v2

    check-cast v2, LX/Aw6;

    .line 114711
    if-eqz v2, :cond_2

    .line 114712
    iput-object v1, v2, LX/Aw6;->b:Landroid/view/View;

    .line 114713
    iget-object v2, v0, LX/Aw7;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/15W;

    iget-object v3, v0, LX/Aw7;->d:Landroid/content/Context;

    sget-object p0, LX/Aw7;->c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 114714
    const-class v0, LX/0i1;

    const/4 v1, 0x0

    invoke-virtual {v2, v3, p0, v0, v1}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114715
    :cond_2
    const v0, 0x1647cf0

    invoke-static {v0}, LX/02m;->a(I)V

    .line 114716
    return-void

    .line 114717
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/0h2;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fO;

    invoke-virtual {v0}, LX/0fO;->p()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/0h2;->r:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114718
    :cond_4
    const/4 v4, 0x0

    .line 114719
    iget-object v0, p0, LX/0h2;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    .line 114720
    sget-short v1, LX/105;->a:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, LX/0h2;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0fO;

    invoke-virtual {v1}, LX/0fO;->p()Z

    move-result v1

    if-nez v1, :cond_5

    .line 114721
    new-instance v1, LX/106;

    invoke-direct {v1, p0}, LX/106;-><init>(LX/0h2;)V

    .line 114722
    iget-object v2, p0, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-static {p0}, LX/0h2;->k(LX/0h2;)Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 114723
    iget-object v2, p0, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v2, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 114724
    :cond_5
    sget-short v1, LX/105;->b:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/0h2;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fO;

    invoke-virtual {v0}, LX/0fO;->p()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 114725
    :cond_6
    new-instance v0, LX/10B;

    invoke-direct {v0, p0}, LX/10B;-><init>(LX/0h2;)V

    .line 114726
    iget-object v1, p0, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-static {p0}, LX/0h2;->j(LX/0h2;)Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSecondaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 114727
    iget-object v1, p0, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSecondaryActionButtonOnClickListener(LX/107;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114728
    :cond_7
    goto/16 :goto_0

    .line 114729
    :catchall_0
    move-exception v0

    const v1, 0x54dd012d    # 7.59366E12f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 114730
    :cond_8
    :try_start_2
    iget-object v0, p0, LX/0h2;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fO;

    .line 114731
    invoke-static {v0}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v1

    invoke-virtual {v1}, LX/0i8;->u()Z

    move-result v1

    move v0, v1

    .line 114732
    if-eqz v0, :cond_1

    .line 114733
    invoke-static {p0}, LX/0h2;->h(LX/0h2;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1
.end method
