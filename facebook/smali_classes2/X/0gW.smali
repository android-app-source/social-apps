.class public LX/0gW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/0w5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0w5",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/Integer;

.field public final d:I

.field public e:LX/0w7;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field private final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public k:Z

.field public l:Z

.field public final m:Z


# direct methods
.method public constructor <init>(LX/0w5;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V
    .locals 1
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0w5",
            "<TT;>;",
            "Ljava/lang/Integer;",
            "ZI",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112140
    new-instance v0, LX/0w7;

    invoke-direct {v0}, LX/0w7;-><init>()V

    iput-object v0, p0, LX/0gW;->e:LX/0w7;

    .line 112141
    iput-object p5, p0, LX/0gW;->f:Ljava/lang/String;

    .line 112142
    iput-object p6, p0, LX/0gW;->g:Ljava/lang/String;

    .line 112143
    iput-boolean p3, p0, LX/0gW;->m:Z

    .line 112144
    iput-object p8, p0, LX/0gW;->h:Ljava/lang/String;

    .line 112145
    iput-object p9, p0, LX/0gW;->i:Ljava/lang/String;

    .line 112146
    iput-object p10, p0, LX/0gW;->j:Ljava/util/Set;

    .line 112147
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0gW;->l:Z

    .line 112148
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0gW;->k:Z

    .line 112149
    iput p4, p0, LX/0gW;->d:I

    .line 112150
    iput-object p1, p0, LX/0gW;->a:LX/0w5;

    .line 112151
    iput-object p7, p0, LX/0gW;->b:Ljava/lang/String;

    .line 112152
    iput-object p2, p0, LX/0gW;->c:Ljava/lang/Integer;

    .line 112153
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V
    .locals 11
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class;",
            "Ljava/lang/Integer;",
            "ZI",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112154
    invoke-static {p1}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(LX/0w5;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 112155
    return-void
.end method

.method private static c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 112156
    const-string v0, "GRAPHQL_QUERY_STRING"

    const-string v1, "Trying to set unknown parameter \'%s\' on query \'%s\'"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 112157
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0gS;)LX/0gW;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0gS;",
            ")",
            "LX/0gW",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 112158
    invoke-virtual {p0, p1}, LX/0gW;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112159
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112160
    iget-object v1, p0, LX/0gW;->f:Ljava/lang/String;

    invoke-static {p1, v1}, LX/0gW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 112161
    :cond_0
    iget-object v1, p0, LX/0gW;->e:LX/0w7;

    invoke-virtual {v1, v0, p2}, LX/0w7;->a(Ljava/lang/String;LX/0gS;)LX/0w7;

    .line 112162
    return-object p0
.end method

.method public final a(Ljava/lang/String;LX/4a1;)LX/0gW;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/4a1;",
            ")",
            "LX/0gW",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 112163
    invoke-virtual {p0, p1}, LX/0gW;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112164
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112165
    iget-object v1, p0, LX/0gW;->f:Ljava/lang/String;

    invoke-static {p1, v1}, LX/0gW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 112166
    :cond_0
    iget-object v1, p0, LX/0gW;->e:LX/0w7;

    invoke-virtual {v1, v0, p2}, LX/0w7;->a(Ljava/lang/String;LX/4a1;)LX/0w7;

    .line 112167
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ")",
            "LX/0gW",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 112168
    invoke-virtual {p0, p1}, LX/0gW;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112169
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112170
    iget-object v1, p0, LX/0gW;->f:Ljava/lang/String;

    invoke-static {p1, v1}, LX/0gW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 112171
    :cond_0
    invoke-virtual {p0, v0, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 112172
    iget-object v1, p0, LX/0gW;->e:LX/0w7;

    invoke-virtual {v1, v0}, LX/0w7;->a(Ljava/lang/String;)LX/0w7;

    .line 112173
    :goto_0
    return-object p0

    .line 112174
    :cond_1
    iget-object v1, p0, LX/0gW;->e:LX/0w7;

    invoke-virtual {v1, v0, p2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0w7;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Enum;",
            ")",
            "LX/0gW",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 112175
    invoke-virtual {p0, p1}, LX/0gW;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112176
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112177
    iget-object v1, p0, LX/0gW;->f:Ljava/lang/String;

    invoke-static {p1, v1}, LX/0gW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 112178
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 112179
    iget-object v1, p0, LX/0gW;->e:LX/0w7;

    invoke-virtual {v1, v0}, LX/0w7;->a(Ljava/lang/String;)LX/0w7;

    .line 112180
    :goto_0
    return-object p0

    .line 112181
    :cond_1
    iget-object v1, p0, LX/0gW;->e:LX/0w7;

    invoke-virtual {v1, v0, p2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0w7;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Number;",
            ")",
            "LX/0gW",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 112182
    invoke-virtual {p0, p1}, LX/0gW;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112183
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112184
    iget-object v1, p0, LX/0gW;->f:Ljava/lang/String;

    invoke-static {p1, v1}, LX/0gW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 112185
    :cond_0
    invoke-virtual {p0, v0, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 112186
    iget-object v1, p0, LX/0gW;->e:LX/0w7;

    invoke-virtual {v1, v0}, LX/0w7;->a(Ljava/lang/String;)LX/0w7;

    .line 112187
    :goto_0
    return-object p0

    .line 112188
    :cond_1
    iget-object v1, p0, LX/0gW;->e:LX/0w7;

    invoke-virtual {v1, v0, p2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0w7;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 112189
    invoke-virtual {p0, p1}, LX/0gW;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112190
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112191
    iget-object v1, p0, LX/0gW;->f:Ljava/lang/String;

    invoke-static {p1, v1}, LX/0gW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 112192
    :cond_0
    invoke-virtual {p0, v0, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 112193
    iget-object v1, p0, LX/0gW;->e:LX/0w7;

    invoke-virtual {v1, v0}, LX/0w7;->a(Ljava/lang/String;)LX/0w7;

    .line 112194
    :goto_0
    return-object p0

    .line 112195
    :cond_1
    iget-object v1, p0, LX/0gW;->e:LX/0w7;

    invoke-virtual {v1, v0, p2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/String;)LX/0w7;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;)LX/0gW;
    .locals 4
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<TT;>;)",
            "LX/0gW",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 112196
    invoke-virtual {p0, p1}, LX/0gW;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 112197
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112198
    iget-object v0, p0, LX/0gW;->f:Ljava/lang/String;

    invoke-static {p1, v0}, LX/0gW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 112199
    :cond_0
    if-eqz p2, :cond_3

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/String;

    if-nez v0, :cond_3

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/0gS;

    if-nez v0, :cond_3

    .line 112200
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 112201
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 112202
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 112203
    :cond_2
    iget-object v0, p0, LX/0gW;->e:LX/0w7;

    invoke-virtual {v0, v1, v2}, LX/0w7;->a(Ljava/lang/String;Ljava/util/List;)LX/0w7;

    .line 112204
    :goto_2
    return-object p0

    .line 112205
    :cond_3
    iget-object v0, p0, LX/0gW;->e:LX/0w7;

    invoke-virtual {v0, v1, p2}, LX/0w7;->a(Ljava/lang/String;Ljava/util/List;)LX/0w7;

    goto :goto_2
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 112206
    return-object p1
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 112107
    const/4 v0, 0x0

    return v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 112207
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Number;",
            ")",
            "LX/0gW",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 112137
    iget-object v0, p0, LX/0gW;->e:LX/0w7;

    invoke-virtual {v0, p1, p2}, LX/0w7;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0w7;

    .line 112138
    return-object p0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")",
            "LX/0gW",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 112208
    invoke-virtual {p0, p1}, LX/0gW;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112209
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112210
    iget-object v1, p0, LX/0gW;->f:Ljava/lang/String;

    invoke-static {p1, v1}, LX/0gW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 112211
    :cond_0
    invoke-virtual {p0, v0, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 112212
    iget-object v1, p0, LX/0gW;->e:LX/0w7;

    invoke-virtual {v1, v0}, LX/0w7;->a(Ljava/lang/String;)LX/0w7;

    .line 112213
    :goto_0
    return-object p0

    .line 112214
    :cond_1
    iget-object v1, p0, LX/0gW;->e:LX/0w7;

    invoke-virtual {v1, v0, p2}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0w7;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)LX/0gW;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 112112
    iget-object v0, p0, LX/0gW;->e:LX/0w7;

    invoke-virtual {v0, p1, p2}, LX/0w7;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0w7;

    .line 112113
    return-object p0
.end method

.method public final d()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112108
    iget-object v0, p0, LX/0gW;->j:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 112109
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 112110
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0gW;->j:Ljava/util/Set;

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112111
    iget-object v0, p0, LX/0gW;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112114
    iget-object v0, p0, LX/0gW;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 112115
    iget-boolean v0, p0, LX/0gW;->l:Z

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 112116
    iget-boolean v0, p0, LX/0gW;->k:Z

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112117
    iget-object v0, p0, LX/0gW;->h:Ljava/lang/String;

    return-object v0
.end method

.method public l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 1

    .prologue
    .line 112118
    const/4 v0, 0x0

    return-object v0
.end method

.method public final m()LX/0w7;
    .locals 1

    .prologue
    .line 112119
    iget-object v0, p0, LX/0gW;->e:LX/0w7;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 112120
    iget v0, p0, LX/0gW;->d:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()LX/0gW;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0gW",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 112121
    new-instance v0, LX/0gW;

    iget-object v1, p0, LX/0gW;->a:LX/0w5;

    iget-object v2, p0, LX/0gW;->c:Ljava/lang/Integer;

    .line 112122
    iget-boolean v3, p0, LX/0gW;->m:Z

    move v3, v3

    .line 112123
    iget v4, p0, LX/0gW;->d:I

    .line 112124
    iget-object v5, p0, LX/0gW;->f:Ljava/lang/String;

    move-object v5, v5

    .line 112125
    iget-object v6, p0, LX/0gW;->g:Ljava/lang/String;

    move-object v6, v6

    .line 112126
    iget-object v7, p0, LX/0gW;->b:Ljava/lang/String;

    move-object v7, v7

    .line 112127
    iget-object v8, p0, LX/0gW;->h:Ljava/lang/String;

    move-object v8, v8

    .line 112128
    iget-object v9, p0, LX/0gW;->i:Ljava/lang/String;

    move-object v9, v9

    .line 112129
    invoke-virtual {p0}, LX/0gW;->d()Ljava/util/Set;

    move-result-object v10

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(LX/0w5;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 112130
    iget-object v1, p0, LX/0gW;->e:LX/0w7;

    iput-object v1, v0, LX/0gW;->e:LX/0w7;

    .line 112131
    return-object v0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 112132
    iget-object v0, p0, LX/0gW;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 112133
    iget-object v0, p0, LX/0gW;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u()Z
    .locals 2

    .prologue
    .line 112134
    const/4 v0, 0x1

    .line 112135
    iget v1, p0, LX/0gW;->d:I

    if-eq v1, v0, :cond_1

    :goto_0
    move v0, v0

    .line 112136
    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0gW;->o()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
