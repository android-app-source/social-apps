.class public LX/1hx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1hr;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1hx;


# instance fields
.field public final a:LX/0Uh;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 297364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297365
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1hx;->b:Ljava/util/List;

    .line 297366
    iput-object p1, p0, LX/1hx;->a:LX/0Uh;

    .line 297367
    return-void
.end method

.method public static a(LX/0QB;)LX/1hx;
    .locals 4

    .prologue
    .line 297368
    sget-object v0, LX/1hx;->c:LX/1hx;

    if-nez v0, :cond_1

    .line 297369
    const-class v1, LX/1hx;

    monitor-enter v1

    .line 297370
    :try_start_0
    sget-object v0, LX/1hx;->c:LX/1hx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 297371
    if-eqz v2, :cond_0

    .line 297372
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 297373
    new-instance p0, LX/1hx;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/1hx;-><init>(LX/0Uh;)V

    .line 297374
    move-object v0, p0

    .line 297375
    sput-object v0, LX/1hx;->c:LX/1hx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297376
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 297377
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 297378
    :cond_1
    sget-object v0, LX/1hx;->c:LX/1hx;

    return-object v0

    .line 297379
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 297380
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 297381
    const v0, 0x7fffffff

    return v0
.end method

.method public final a(Lorg/apache/http/impl/client/RequestWrapper;)V
    .locals 3

    .prologue
    .line 297382
    iget-object v0, p0, LX/1hx;->a:LX/0Uh;

    const/16 v1, 0x681

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 297383
    if-eqz v0, :cond_0

    .line 297384
    iget-object v0, p0, LX/1hx;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lorg/apache/http/impl/client/RequestWrapper;->getURI()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 297385
    :cond_0
    return-void
.end method
