.class public final LX/0mD;
.super LX/0mE;
.source ""


# static fields
.field public static final a:LX/0mD;


# instance fields
.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 132117
    new-instance v0, LX/0mD;

    const-string v1, ""

    invoke-direct {v0, v1}, LX/0mD;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/0mD;->a:LX/0mD;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 132199
    invoke-direct {p0}, LX/0mE;-><init>()V

    iput-object p1, p0, LX/0mD;->b:Ljava/lang/String;

    return-void
.end method

.method private static I()V
    .locals 3

    .prologue
    .line 132198
    new-instance v0, LX/2aQ;

    const-string v1, "Unexpected end-of-String when base64 content"

    sget-object v2, LX/28G;->a:LX/28G;

    invoke-direct {v0, v1, v2}, LX/2aQ;-><init>(Ljava/lang/String;LX/28G;)V

    throw v0
.end method

.method private static a(LX/0ln;CI)V
    .locals 1

    .prologue
    .line 132196
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/0mD;->a(LX/0ln;CILjava/lang/String;)V

    .line 132197
    return-void
.end method

.method private static a(LX/0ln;CILjava/lang/String;)V
    .locals 3

    .prologue
    .line 132184
    const/16 v0, 0x20

    if-gt p1, v0, :cond_1

    .line 132185
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Illegal white space character (code 0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") as character #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v1, p2, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " of 4-char base64 unit: can only used between units"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 132186
    :goto_0
    if-eqz p3, :cond_0

    .line 132187
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 132188
    :cond_0
    new-instance v1, LX/2aQ;

    sget-object v2, LX/28G;->a:LX/28G;

    invoke-direct {v1, v0, v2}, LX/2aQ;-><init>(Ljava/lang/String;LX/28G;)V

    throw v1

    .line 132189
    :cond_1
    invoke-virtual {p0, p1}, LX/0ln;->a(C)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 132190
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected padding character (\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 132191
    iget-char v1, p0, LX/0ln;->b:C

    move v1, v1

    .line 132192
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\') as character #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v1, p2, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " of 4-char base64 unit: padding only legal as 3rd or 4th character"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 132193
    :cond_2
    invoke-static {p1}, Ljava/lang/Character;->isDefined(C)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Ljava/lang/Character;->isISOControl(C)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 132194
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Illegal character (code 0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") in base64 content"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 132195
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Illegal character \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' (code 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") in base64 content"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static a(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/16 v0, 0x22

    .line 132180
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 132181
    invoke-static {p0, p1}, LX/12H;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 132182
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 132183
    return-void
.end method

.method private a(LX/0ln;)[B
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v2, 0x0

    const/4 v9, -0x2

    .line 132126
    new-instance v3, LX/2SG;

    const/16 v0, 0x64

    invoke-direct {v3, v0}, LX/2SG;-><init>(I)V

    .line 132127
    iget-object v4, p0, LX/0mD;->b:Ljava/lang/String;

    .line 132128
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    move v0, v2

    .line 132129
    :goto_0
    if-ge v0, v5, :cond_3

    .line 132130
    :goto_1
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {v4, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 132131
    if-ge v1, v5, :cond_3

    .line 132132
    const/16 v6, 0x20

    if-le v0, v6, :cond_e

    .line 132133
    invoke-virtual {p1, v0}, LX/0ln;->b(C)I

    move-result v6

    .line 132134
    if-gez v6, :cond_0

    .line 132135
    invoke-static {p1, v0, v2}, LX/0mD;->a(LX/0ln;CI)V

    .line 132136
    :cond_0
    if-lt v1, v5, :cond_1

    .line 132137
    invoke-static {}, LX/0mD;->I()V

    .line 132138
    :cond_1
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {v4, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 132139
    invoke-virtual {p1, v1}, LX/0ln;->b(C)I

    move-result v7

    .line 132140
    if-gez v7, :cond_2

    .line 132141
    const/4 v8, 0x1

    invoke-static {p1, v1, v8}, LX/0mD;->a(LX/0ln;CI)V

    .line 132142
    :cond_2
    shl-int/lit8 v1, v6, 0x6

    or-int/2addr v1, v7

    .line 132143
    if-lt v0, v5, :cond_5

    .line 132144
    iget-boolean v6, p1, LX/0ln;->a:Z

    move v6, v6

    .line 132145
    if-nez v6, :cond_4

    .line 132146
    shr-int/lit8 v0, v1, 0x4

    .line 132147
    invoke-virtual {v3, v0}, LX/2SG;->a(I)V

    .line 132148
    :cond_3
    :goto_2
    invoke-virtual {v3}, LX/2SG;->c()[B

    move-result-object v0

    return-object v0

    .line 132149
    :cond_4
    invoke-static {}, LX/0mD;->I()V

    .line 132150
    :cond_5
    add-int/lit8 v6, v0, 0x1

    invoke-virtual {v4, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 132151
    invoke-virtual {p1, v0}, LX/0ln;->b(C)I

    move-result v7

    .line 132152
    if-gez v7, :cond_9

    .line 132153
    if-eq v7, v9, :cond_6

    .line 132154
    const/4 v7, 0x2

    invoke-static {p1, v0, v7}, LX/0mD;->a(LX/0ln;CI)V

    .line 132155
    :cond_6
    if-lt v6, v5, :cond_7

    .line 132156
    invoke-static {}, LX/0mD;->I()V

    .line 132157
    :cond_7
    add-int/lit8 v0, v6, 0x1

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 132158
    invoke-virtual {p1, v6}, LX/0ln;->a(C)Z

    move-result v7

    if-nez v7, :cond_8

    .line 132159
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "expected padding character \'"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 132160
    iget-char v8, p1, LX/0ln;->b:C

    move v8, v8

    .line 132161
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v6, v10, v7}, LX/0mD;->a(LX/0ln;CILjava/lang/String;)V

    .line 132162
    :cond_8
    shr-int/lit8 v1, v1, 0x4

    .line 132163
    invoke-virtual {v3, v1}, LX/2SG;->a(I)V

    goto/16 :goto_0

    .line 132164
    :cond_9
    shl-int/lit8 v0, v1, 0x6

    or-int v1, v0, v7

    .line 132165
    if-lt v6, v5, :cond_b

    .line 132166
    iget-boolean v0, p1, LX/0ln;->a:Z

    move v0, v0

    .line 132167
    if-nez v0, :cond_a

    .line 132168
    shr-int/lit8 v0, v1, 0x2

    .line 132169
    invoke-virtual {v3, v0}, LX/2SG;->b(I)V

    goto :goto_2

    .line 132170
    :cond_a
    invoke-static {}, LX/0mD;->I()V

    .line 132171
    :cond_b
    add-int/lit8 v0, v6, 0x1

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 132172
    invoke-virtual {p1, v6}, LX/0ln;->b(C)I

    move-result v7

    .line 132173
    if-gez v7, :cond_d

    .line 132174
    if-eq v7, v9, :cond_c

    .line 132175
    invoke-static {p1, v6, v10}, LX/0mD;->a(LX/0ln;CI)V

    .line 132176
    :cond_c
    shr-int/lit8 v1, v1, 0x2

    .line 132177
    invoke-virtual {v3, v1}, LX/2SG;->b(I)V

    goto/16 :goto_0

    .line 132178
    :cond_d
    shl-int/lit8 v1, v1, 0x6

    or-int/2addr v1, v7

    .line 132179
    invoke-virtual {v3, v1}, LX/2SG;->c(I)V

    goto/16 :goto_0

    :cond_e
    move v0, v1

    goto/16 :goto_1
.end method

.method public static g(Ljava/lang/String;)LX/0mD;
    .locals 1

    .prologue
    .line 132120
    if-nez p0, :cond_0

    .line 132121
    const/4 v0, 0x0

    .line 132122
    :goto_0
    return-object v0

    .line 132123
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 132124
    sget-object v0, LX/0mD;->a:LX/0mD;

    goto :goto_0

    .line 132125
    :cond_1
    new-instance v0, LX/0mD;

    invoke-direct {v0, p0}, LX/0mD;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132119
    iget-object v0, p0, LX/0mD;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(D)D
    .locals 3

    .prologue
    .line 132118
    iget-object v0, p0, LX/0mD;->b:Ljava/lang/String;

    invoke-static {v0, p1, p2}, LX/16K;->a(Ljava/lang/String;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public final a(J)J
    .locals 3

    .prologue
    .line 132200
    iget-object v0, p0, LX/0mD;->b:Ljava/lang/String;

    invoke-static {v0, p1, p2}, LX/16K;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a()LX/15z;
    .locals 1

    .prologue
    .line 132092
    sget-object v0, LX/15z;->VALUE_STRING:LX/15z;

    return-object v0
.end method

.method public final a(Z)Z
    .locals 2

    .prologue
    .line 132093
    iget-object v0, p0, LX/0mD;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 132094
    const-string v0, "true"

    iget-object v1, p0, LX/0mD;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132095
    const/4 p1, 0x1

    .line 132096
    :cond_0
    return p1
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 132097
    iget-object v0, p0, LX/0mD;->b:Ljava/lang/String;

    invoke-static {v0, p1}, LX/16K;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 132098
    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    .line 132099
    :cond_0
    :goto_0
    return v0

    .line 132100
    :cond_1
    if-eqz p1, :cond_0

    .line 132101
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 132102
    check-cast p1, LX/0mD;

    iget-object v0, p1, LX/0mD;->b:Ljava/lang/String;

    iget-object v1, p0, LX/0mD;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 132103
    iget-object v0, p0, LX/0mD;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final k()LX/0nH;
    .locals 1

    .prologue
    .line 132104
    sget-object v0, LX/0nH;->STRING:LX/0nH;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132105
    iget-object v0, p0, LX/0mD;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final serialize(LX/0nX;LX/0my;)V
    .locals 1

    .prologue
    .line 132106
    iget-object v0, p0, LX/0mD;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 132107
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 132108
    :goto_0
    return-void

    .line 132109
    :cond_0
    iget-object v0, p0, LX/0mD;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final t()[B
    .locals 1

    .prologue
    .line 132110
    sget-object v0, LX/0lm;->b:LX/0ln;

    move-object v0, v0

    .line 132111
    invoke-direct {p0, v0}, LX/0mD;->a(LX/0ln;)[B

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 132112
    iget-object v0, p0, LX/0mD;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 132113
    add-int/lit8 v1, v0, 0x2

    shr-int/lit8 v0, v0, 0x4

    add-int/2addr v0, v1

    .line 132114
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 132115
    iget-object v0, p0, LX/0mD;->b:Ljava/lang/String;

    invoke-static {v1, v0}, LX/0mD;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 132116
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
