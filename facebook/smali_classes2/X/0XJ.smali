.class public final enum LX/0XJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0XJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0XJ;

.field public static final enum FEMALE:LX/0XJ;

.field public static final enum MALE:LX/0XJ;

.field public static final enum UNKNOWN:LX/0XJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 78132
    new-instance v0, LX/0XJ;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/0XJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0XJ;->UNKNOWN:LX/0XJ;

    .line 78133
    new-instance v0, LX/0XJ;

    const-string v1, "FEMALE"

    invoke-direct {v0, v1, v3}, LX/0XJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0XJ;->FEMALE:LX/0XJ;

    .line 78134
    new-instance v0, LX/0XJ;

    const-string v1, "MALE"

    invoke-direct {v0, v1, v4}, LX/0XJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0XJ;->MALE:LX/0XJ;

    .line 78135
    const/4 v0, 0x3

    new-array v0, v0, [LX/0XJ;

    sget-object v1, LX/0XJ;->UNKNOWN:LX/0XJ;

    aput-object v1, v0, v2

    sget-object v1, LX/0XJ;->FEMALE:LX/0XJ;

    aput-object v1, v0, v3

    sget-object v1, LX/0XJ;->MALE:LX/0XJ;

    aput-object v1, v0, v4

    sput-object v0, LX/0XJ;->$VALUES:[LX/0XJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 78136
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0XJ;
    .locals 1

    .prologue
    .line 78137
    const-class v0, LX/0XJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0XJ;

    return-object v0
.end method

.method public static values()[LX/0XJ;
    .locals 1

    .prologue
    .line 78138
    sget-object v0, LX/0XJ;->$VALUES:[LX/0XJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0XJ;

    return-object v0
.end method
