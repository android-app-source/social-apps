.class public final LX/1MK;
.super LX/1ML;
.source ""


# instance fields
.field public final synthetic this$0:LX/1MG;


# direct methods
.method public constructor <init>(LX/1MG;)V
    .locals 0

    .prologue
    .line 235154
    iput-object p1, p0, LX/1MK;->this$0:LX/1MG;

    invoke-direct {p0}, LX/1ML;-><init>()V

    return-void
.end method

.method public static checkThread(LX/1MK;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 235155
    invoke-virtual {p0}, LX/0SQ;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235156
    :goto_0
    return-void

    .line 235157
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v0, v3, :cond_3

    move v0, v1

    :goto_1
    const-string v3, "Cannot call get on main thread for unfinished operation"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 235158
    iget-object v0, p0, LX/1MK;->this$0:LX/1MG;

    iget-object v0, v0, LX/1MG;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1MK;->this$0:LX/1MG;

    iget-object v0, v0, LX/1MG;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v0, v3, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    const-string v0, "Cannot call get on the operation\'s handler thread for unfinished operation"

    invoke-static {v2, v0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 235159
    goto :goto_1
.end method


# virtual methods
.method public cancelOperation()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 235160
    invoke-virtual {p0}, LX/0SQ;->isDone()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 235161
    :cond_0
    :goto_0
    return v0

    .line 235162
    :cond_1
    :try_start_0
    iget-object v1, p0, LX/1MK;->this$0:LX/1MG;

    invoke-virtual {v1}, LX/1MG;->cancel()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 235163
    const/4 v1, 0x0

    invoke-super {p0, v1}, LX/1ML;->cancel(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 235164
    const/4 v0, 0x1

    goto :goto_0

    .line 235165
    :catch_0
    move-exception v1

    .line 235166
    const-string v2, "DefaultBlueServiceOperation"

    const-string v3, "Could not cancel operation"

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 235167
    invoke-static {p0}, LX/1MK;->checkThread(LX/1MK;)V

    .line 235168
    invoke-super {p0}, LX/1ML;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 235169
    return-object v0
.end method

.method public bridge synthetic get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 235170
    invoke-static {p0}, LX/1MK;->checkThread(LX/1MK;)V

    .line 235171
    invoke-super {p0, p1, p2, p3}, LX/1ML;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 235172
    return-object v0
.end method

.method public interruptTask()V
    .locals 3

    .prologue
    .line 235173
    invoke-virtual {p0}, LX/0SQ;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 235174
    :try_start_0
    iget-object v0, p0, LX/1MK;->this$0:LX/1MG;

    invoke-virtual {v0}, LX/1MG;->cancel()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 235175
    :cond_0
    :goto_0
    return-void

    .line 235176
    :catch_0
    move-exception v0

    .line 235177
    const-string v1, "DefaultBlueServiceOperation"

    const-string v2, "Could not cancel operation"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public set(Lcom/facebook/fbservice/service/OperationResult;)Z
    .locals 1
    .param p1    # Lcom/facebook/fbservice/service/OperationResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 235178
    invoke-super {p0, p1}, LX/1ML;->set(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic set(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 235179
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0, p1}, LX/1MK;->set(Lcom/facebook/fbservice/service/OperationResult;)Z

    move-result v0

    return v0
.end method

.method public setException(Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 235180
    invoke-super {p0, p1}, LX/1ML;->setException(Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method public updatePriority(Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 1

    .prologue
    .line 235181
    invoke-virtual {p0}, LX/0SQ;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235182
    :goto_0
    return-void

    .line 235183
    :cond_0
    iget-object v0, p0, LX/1MK;->this$0:LX/1MG;

    invoke-virtual {v0, p1}, LX/1MG;->changePriority(Lcom/facebook/http/interfaces/RequestPriority;)Z

    goto :goto_0
.end method
