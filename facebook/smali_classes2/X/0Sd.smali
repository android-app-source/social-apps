.class public LX/0Sd;
.super Ljava/lang/ref/WeakReference;
.source ""

# interfaces
.implements LX/0Qf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/ref/WeakReference",
        "<TV;>;",
        "LX/0Qf",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TV;>;TV;",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 61531
    invoke-direct {p0, p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 61532
    iput-object p3, p0, LX/0Sd;->a:LX/0R1;

    .line 61533
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 61526
    const/4 v0, 0x1

    return v0
.end method

.method public a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;LX/0R1;)LX/0Qf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TV;>;TV;",
            "LX/0R1",
            "<TK;TV;>;)",
            "LX/0Qf",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 61530
    new-instance v0, LX/0Sd;

    invoke-direct {v0, p1, p2, p3}, LX/0Sd;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;LX/0R1;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 61529
    return-void
.end method

.method public final b()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 61534
    iget-object v0, p0, LX/0Sd;->a:LX/0R1;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 61528
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 61527
    const/4 v0, 0x1

    return v0
.end method

.method public final e()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 61525
    invoke-virtual {p0}, LX/0Sd;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
