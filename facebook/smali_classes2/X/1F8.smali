.class public LX/1F8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/1F7;

.field public final b:LX/1F0;

.field public final c:LX/1F7;

.field public final d:LX/0rb;

.field public final e:LX/1F7;

.field public final f:LX/1F0;

.field public final g:LX/1F7;

.field public final h:LX/1F0;


# direct methods
.method public constructor <init>(LX/1F9;)V
    .locals 9

    .prologue
    .line 221605
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221606
    iget-object v0, p1, LX/1F9;->a:LX/1F7;

    if-nez v0, :cond_0

    .line 221607
    new-instance v1, LX/1F7;

    const/4 v2, 0x0

    .line 221608
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v5

    const-wide/32 v7, 0x7fffffff

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v5

    long-to-int v5, v5

    .line 221609
    const/high16 v6, 0x1000000

    if-le v5, v6, :cond_8

    .line 221610
    div-int/lit8 v5, v5, 0x4

    mul-int/lit8 v5, v5, 0x3

    .line 221611
    :goto_0
    move v3, v5

    .line 221612
    sget-object v4, LX/1FA;->a:Landroid/util/SparseIntArray;

    invoke-direct {v1, v2, v3, v4}, LX/1F7;-><init>(IILandroid/util/SparseIntArray;)V

    move-object v0, v1

    .line 221613
    :goto_1
    iput-object v0, p0, LX/1F8;->a:LX/1F7;

    .line 221614
    iget-object v0, p1, LX/1F9;->b:LX/1F0;

    if-nez v0, :cond_1

    invoke-static {}, LX/1FT;->a()LX/1FT;

    move-result-object v0

    :goto_2
    iput-object v0, p0, LX/1F8;->b:LX/1F0;

    .line 221615
    iget-object v0, p1, LX/1F9;->c:LX/1F7;

    if-nez v0, :cond_2

    const/high16 v5, 0x20000

    const/high16 v2, 0x400000

    .line 221616
    new-instance v1, LX/1F7;

    sget v3, LX/1F6;->a:I

    mul-int/2addr v3, v2

    sget v4, LX/1F6;->a:I

    invoke-static {v5, v2, v4}, LX/1F6;->a(III)Landroid/util/SparseIntArray;

    move-result-object v4

    sget v7, LX/1F6;->a:I

    move v6, v2

    invoke-direct/range {v1 .. v7}, LX/1F7;-><init>(IILandroid/util/SparseIntArray;III)V

    move-object v0, v1

    .line 221617
    :goto_3
    iput-object v0, p0, LX/1F8;->c:LX/1F7;

    .line 221618
    iget-object v0, p1, LX/1F9;->d:LX/0rb;

    if-nez v0, :cond_3

    invoke-static {}, LX/1IX;->a()LX/1IX;

    move-result-object v0

    :goto_4
    iput-object v0, p0, LX/1F8;->d:LX/0rb;

    .line 221619
    iget-object v0, p1, LX/1F9;->e:LX/1F7;

    if-nez v0, :cond_4

    const/4 v4, 0x2

    const/4 v3, 0x5

    .line 221620
    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    .line 221621
    const/16 v2, 0x400

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 221622
    const/16 v2, 0x800

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 221623
    const/16 v2, 0x1000

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 221624
    const/16 v2, 0x2000

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 221625
    const/16 v2, 0x4000

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 221626
    const v2, 0x8000

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 221627
    const/high16 v2, 0x10000

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 221628
    const/high16 v2, 0x20000

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 221629
    const/high16 v2, 0x40000

    invoke-virtual {v1, v2, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 221630
    const/high16 v2, 0x80000

    invoke-virtual {v1, v2, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 221631
    const/high16 v2, 0x100000

    invoke-virtual {v1, v2, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 221632
    new-instance v2, LX/1F7;

    .line 221633
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v5

    const-wide/32 v7, 0x7fffffff

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v5

    long-to-int v5, v5

    .line 221634
    const/high16 v6, 0x1000000

    if-ge v5, v6, :cond_9

    .line 221635
    const/high16 v5, 0x300000

    .line 221636
    :goto_5
    move v3, v5

    .line 221637
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v5

    const-wide/32 v7, 0x7fffffff

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v5

    long-to-int v5, v5

    .line 221638
    const/high16 v6, 0x1000000

    if-ge v5, v6, :cond_b

    .line 221639
    div-int/lit8 v5, v5, 0x2

    .line 221640
    :goto_6
    move v4, v5

    .line 221641
    invoke-direct {v2, v3, v4, v1}, LX/1F7;-><init>(IILandroid/util/SparseIntArray;)V

    move-object v0, v2

    .line 221642
    :goto_7
    iput-object v0, p0, LX/1F8;->e:LX/1F7;

    .line 221643
    iget-object v0, p1, LX/1F9;->f:LX/1F0;

    if-nez v0, :cond_5

    invoke-static {}, LX/1FT;->a()LX/1FT;

    move-result-object v0

    :goto_8
    iput-object v0, p0, LX/1F8;->f:LX/1F0;

    .line 221644
    iget-object v0, p1, LX/1F9;->g:LX/1F7;

    if-nez v0, :cond_6

    .line 221645
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 221646
    const/16 v1, 0x4000

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 221647
    new-instance v1, LX/1F7;

    const v2, 0x14000

    const/high16 v3, 0x100000

    invoke-direct {v1, v2, v3, v0}, LX/1F7;-><init>(IILandroid/util/SparseIntArray;)V

    move-object v0, v1

    .line 221648
    :goto_9
    iput-object v0, p0, LX/1F8;->g:LX/1F7;

    .line 221649
    iget-object v0, p1, LX/1F9;->h:LX/1F0;

    if-nez v0, :cond_7

    invoke-static {}, LX/1FT;->a()LX/1FT;

    move-result-object v0

    :goto_a
    iput-object v0, p0, LX/1F8;->h:LX/1F0;

    .line 221650
    return-void

    .line 221651
    :cond_0
    iget-object v0, p1, LX/1F9;->a:LX/1F7;

    goto/16 :goto_1

    .line 221652
    :cond_1
    iget-object v0, p1, LX/1F9;->b:LX/1F0;

    goto/16 :goto_2

    .line 221653
    :cond_2
    iget-object v0, p1, LX/1F9;->c:LX/1F7;

    goto/16 :goto_3

    .line 221654
    :cond_3
    iget-object v0, p1, LX/1F9;->d:LX/0rb;

    goto/16 :goto_4

    .line 221655
    :cond_4
    iget-object v0, p1, LX/1F9;->e:LX/1F7;

    goto :goto_7

    .line 221656
    :cond_5
    iget-object v0, p1, LX/1F9;->f:LX/1F0;

    goto :goto_8

    .line 221657
    :cond_6
    iget-object v0, p1, LX/1F9;->g:LX/1F7;

    goto :goto_9

    .line 221658
    :cond_7
    iget-object v0, p1, LX/1F9;->h:LX/1F0;

    goto :goto_a

    :cond_8
    div-int/lit8 v5, v5, 0x2

    goto/16 :goto_0

    .line 221659
    :cond_9
    const/high16 v6, 0x2000000

    if-ge v5, v6, :cond_a

    .line 221660
    const/high16 v5, 0x600000

    goto :goto_5

    .line 221661
    :cond_a
    const/high16 v5, 0xc00000

    goto :goto_5

    :cond_b
    div-int/lit8 v5, v5, 0x4

    mul-int/lit8 v5, v5, 0x3

    goto :goto_6
.end method

.method public static newBuilder()LX/1F9;
    .locals 2

    .prologue
    .line 221662
    new-instance v0, LX/1F9;

    invoke-direct {v0}, LX/1F9;-><init>()V

    return-object v0
.end method
