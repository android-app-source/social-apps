.class public LX/1o9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field public final c:F

.field public final d:F


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 318478
    const/high16 v0, 0x45000000    # 2048.0f

    invoke-direct {p0, p1, p2, v0}, LX/1o9;-><init>(IIF)V

    .line 318479
    return-void
.end method

.method private constructor <init>(IIF)V
    .locals 1

    .prologue
    .line 318476
    const v0, 0x3f2aaaab

    invoke-direct {p0, p1, p2, p3, v0}, LX/1o9;-><init>(IIFF)V

    .line 318477
    return-void
.end method

.method private constructor <init>(IIFF)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 318455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 318456
    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 318457
    if-lez p2, :cond_1

    :goto_1
    invoke-static {v1}, LX/03g;->a(Z)V

    .line 318458
    iput p1, p0, LX/1o9;->a:I

    .line 318459
    iput p2, p0, LX/1o9;->b:I

    .line 318460
    iput p3, p0, LX/1o9;->c:F

    .line 318461
    iput p4, p0, LX/1o9;->d:F

    .line 318462
    return-void

    :cond_0
    move v0, v2

    .line 318463
    goto :goto_0

    :cond_1
    move v1, v2

    .line 318464
    goto :goto_1
.end method

.method public static a(I)LX/1o9;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 318473
    if-gtz p0, :cond_0

    .line 318474
    const/4 v0, 0x0

    .line 318475
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/1o9;

    invoke-direct {v0, p0, p0}, LX/1o9;-><init>(II)V

    goto :goto_0
.end method

.method public static a(II)LX/1o9;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 318480
    if-lez p0, :cond_0

    if-gtz p1, :cond_1

    .line 318481
    :cond_0
    const/4 v0, 0x0

    .line 318482
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, LX/1o9;

    invoke-direct {v0, p0, p1}, LX/1o9;-><init>(II)V

    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 318467
    if-ne p1, p0, :cond_1

    .line 318468
    :cond_0
    :goto_0
    return v0

    .line 318469
    :cond_1
    instance-of v2, p1, LX/1o9;

    if-nez v2, :cond_2

    move v0, v1

    .line 318470
    goto :goto_0

    .line 318471
    :cond_2
    check-cast p1, LX/1o9;

    .line 318472
    iget v2, p0, LX/1o9;->a:I

    iget v3, p1, LX/1o9;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, LX/1o9;->b:I

    iget v3, p1, LX/1o9;->b:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 318466
    iget v0, p0, LX/1o9;->a:I

    iget v1, p0, LX/1o9;->b:I

    invoke-static {v0, v1}, LX/1bi;->a(II)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 318465
    const/4 v0, 0x0

    const-string v1, "%dx%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, LX/1o9;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, LX/1o9;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
