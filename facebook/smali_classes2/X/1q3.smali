.class public LX/1q3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1q2;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 329962
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 329963
    iput-object p1, p0, LX/1q3;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 329964
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1q3;->b:Z

    .line 329965
    return-void
.end method


# virtual methods
.method public final a(JZ)V
    .locals 0

    .prologue
    .line 329966
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 329967
    iget-boolean v0, p0, LX/1q3;->b:Z

    return v0
.end method

.method public final a(J)Z
    .locals 7

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x1

    .line 329968
    iget-object v1, p0, LX/1q3;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0dp;->o:LX/0Tn;

    invoke-interface {v1, v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 329969
    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 329970
    sub-long v2, p1, v2

    .line 329971
    const-wide/32 v4, 0xea60

    cmp-long v1, v4, v2

    if-gez v1, :cond_1

    .line 329972
    :cond_0
    :goto_0
    return v0

    .line 329973
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(J)V
    .locals 0

    .prologue
    .line 329974
    return-void
.end method
