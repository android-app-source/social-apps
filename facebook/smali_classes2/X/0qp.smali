.class public LX/0qp;
.super Ljava/util/AbstractMap;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<TK;TV;>;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TV;>;"
        }
    .end annotation
.end field

.field public f:LX/0qs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qs",
            "<TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 148521
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 148522
    iput-object p1, p0, LX/0qp;->a:Ljava/util/Comparator;

    .line 148523
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0qp;->b:Ljava/util/Map;

    .line 148524
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0qp;->c:Ljava/util/List;

    .line 148525
    iget-object v0, p0, LX/0qp;->b:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/0qp;->d:Ljava/util/Map;

    .line 148526
    iget-object v0, p0, LX/0qp;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/0qp;->e:Ljava/util/List;

    .line 148527
    return-void
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 148556
    iget-object v0, p0, LX/0qp;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 148557
    invoke-virtual {p0, v0}, LX/0qp;->a(Ljava/lang/Object;)I

    move-result v0

    .line 148558
    if-gez v0, :cond_0

    .line 148559
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "The collection is in an invalid state"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148560
    :cond_0
    iget-object v1, p0, LX/0qp;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 148561
    invoke-direct {p0, p2}, LX/0qp;->b(Ljava/lang/Object;)I

    move-result v1

    .line 148562
    iget-object v2, p0, LX/0qp;->b:Ljava/util/Map;

    invoke-interface {v2, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148563
    iget-object v2, p0, LX/0qp;->f:LX/0qs;

    if-eqz v2, :cond_1

    .line 148564
    iget-object v2, p0, LX/0qp;->f:LX/0qs;

    const/4 v3, 0x1

    invoke-interface {v2, v0, v1, p2, v3}, LX/0qs;->a(IILjava/lang/Object;Z)V

    .line 148565
    :cond_1
    return-void
.end method

.method private b(Ljava/lang/Object;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)I"
        }
    .end annotation

    .prologue
    .line 148548
    iget-object v0, p0, LX/0qp;->c:Ljava/util/List;

    iget-object v1, p0, LX/0qp;->a:Ljava/util/Comparator;

    invoke-static {v0, p1, v1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    .line 148549
    if-ltz v0, :cond_0

    .line 148550
    add-int/lit8 v0, v0, 0x1

    :goto_0
    iget-object v1, p0, LX/0qp;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 148551
    iget-object v1, p0, LX/0qp;->a:Ljava/util/Comparator;

    iget-object v2, p0, LX/0qp;->c:Ljava/util/List;

    add-int/lit8 v3, v0, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LX/0qp;->c:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    if-nez v1, :cond_1

    .line 148552
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148553
    :cond_0
    add-int/lit8 v0, v0, 0x1

    neg-int v0, v0

    .line 148554
    :cond_1
    iget-object v1, p0, LX/0qp;->c:Ljava/util/List;

    invoke-interface {v1, v0, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 148555
    return v0
.end method

.method private c(Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)I"
        }
    .end annotation

    .prologue
    .line 148543
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/0qp;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 148544
    iget-object v1, p0, LX/0qp;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_0

    .line 148545
    :goto_1
    return v0

    .line 148546
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148547
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)I"
        }
    .end annotation

    .prologue
    .line 148528
    iget-object v0, p0, LX/0qp;->c:Ljava/util/List;

    iget-object v1, p0, LX/0qp;->a:Ljava/util/Comparator;

    invoke-static {v0, p1, v1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v1

    .line 148529
    if-gez v1, :cond_1

    .line 148530
    invoke-direct {p0, p1}, LX/0qp;->c(Ljava/lang/Object;)I

    move-result v0

    .line 148531
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 148532
    :goto_1
    iget-object v2, p0, LX/0qp;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 148533
    iget-object v2, p0, LX/0qp;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 148534
    iget-object v3, p0, LX/0qp;->a:Ljava/util/Comparator;

    invoke-interface {v3, p1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-nez v3, :cond_2

    .line 148535
    if-eq p1, v2, :cond_0

    .line 148536
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 148537
    :cond_2
    add-int/lit8 v0, v1, -0x1

    :goto_2
    if-ltz v0, :cond_3

    .line 148538
    iget-object v1, p0, LX/0qp;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 148539
    iget-object v2, p0, LX/0qp;->a:Ljava/util/Comparator;

    invoke-interface {v2, p1, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-nez v2, :cond_3

    .line 148540
    if-eq p1, v1, :cond_0

    .line 148541
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 148542
    :cond_3
    invoke-direct {p0, p1}, LX/0qp;->c(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 148520
    iget-object v0, p0, LX/0qp;->e:Ljava/util/List;

    return-object v0
.end method

.method public final clear()V
    .locals 3

    .prologue
    .line 148566
    iget-object v0, p0, LX/0qp;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v1

    .line 148567
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 148568
    aget-object v2, v1, v0

    invoke-virtual {p0, v2}, LX/0qp;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148569
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148570
    :cond_0
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 148519
    iget-object v0, p0, LX/0qp;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 148490
    iget-object v0, p0, LX/0qp;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 148518
    iget-object v0, p0, LX/0qp;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 148502
    invoke-virtual {p0, p1}, LX/0qp;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 148503
    if-eqz v0, :cond_3

    .line 148504
    iget-object v1, p0, LX/0qp;->a:Ljava/util/Comparator;

    invoke-interface {v1, v0, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    if-nez v1, :cond_2

    .line 148505
    invoke-virtual {p0, v0}, LX/0qp;->a(Ljava/lang/Object;)I

    move-result v1

    .line 148506
    if-gez v1, :cond_0

    .line 148507
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "The collection is in an invalid state"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148508
    :cond_0
    iget-object v2, p0, LX/0qp;->c:Ljava/util/List;

    invoke-interface {v2, v1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 148509
    iget-object v2, p0, LX/0qp;->b:Ljava/util/Map;

    invoke-interface {v2, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148510
    iget-object v2, p0, LX/0qp;->f:LX/0qs;

    if-eqz v2, :cond_1

    .line 148511
    iget-object v2, p0, LX/0qp;->f:LX/0qs;

    invoke-interface {v2, v1, v0, p2, v3}, LX/0qs;->a(ILjava/lang/Object;Ljava/lang/Object;Z)V

    .line 148512
    :cond_1
    :goto_0
    return-object v0

    .line 148513
    :cond_2
    invoke-direct {p0, p1, p2}, LX/0qp;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 148514
    :cond_3
    invoke-direct {p0, p2}, LX/0qp;->b(Ljava/lang/Object;)I

    move-result v1

    .line 148515
    iget-object v2, p0, LX/0qp;->b:Ljava/util/Map;

    invoke-interface {v2, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148516
    iget-object v2, p0, LX/0qp;->f:LX/0qs;

    if-eqz v2, :cond_1

    .line 148517
    iget-object v2, p0, LX/0qp;->f:LX/0qs;

    invoke-interface {v2, v1, p2, v3}, LX/0qs;->a(ILjava/lang/Object;Z)V

    goto :goto_0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 148492
    iget-object v0, p0, LX/0qp;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 148493
    if-nez v0, :cond_1

    .line 148494
    const/4 v0, 0x0

    .line 148495
    :cond_0
    :goto_0
    return-object v0

    .line 148496
    :cond_1
    invoke-virtual {p0, v0}, LX/0qp;->a(Ljava/lang/Object;)I

    move-result v1

    .line 148497
    if-gez v1, :cond_2

    .line 148498
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "The collection is in an invalid state"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148499
    :cond_2
    iget-object v2, p0, LX/0qp;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 148500
    iget-object v2, p0, LX/0qp;->f:LX/0qs;

    if-eqz v2, :cond_0

    .line 148501
    iget-object v2, p0, LX/0qp;->f:LX/0qs;

    const/4 v3, 0x1

    invoke-interface {v2, v1, v0, v3}, LX/0qs;->b(ILjava/lang/Object;Z)V

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 148491
    iget-object v0, p0, LX/0qp;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method
