.class public LX/1By;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:I

.field public g:LX/0SG;

.field public h:J

.field public i:J


# direct methods
.method public constructor <init>(LX/0SG;Ljava/io/File;Ljava/lang/String;LX/7i0;I)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 214598
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214599
    const-string v0, ""

    iput-object v0, p0, LX/1By;->d:Ljava/lang/String;

    .line 214600
    const-string v0, ""

    iput-object v0, p0, LX/1By;->e:Ljava/lang/String;

    .line 214601
    iput-wide v6, p0, LX/1By;->i:J

    .line 214602
    const/4 v0, 0x0

    iput v0, p0, LX/1By;->a:I

    .line 214603
    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, LX/1By;->b:I

    .line 214604
    iput-object p3, p0, LX/1By;->c:Ljava/lang/String;

    .line 214605
    iput p5, p0, LX/1By;->f:I

    .line 214606
    iput-object p1, p0, LX/1By;->g:LX/0SG;

    .line 214607
    iget-object v0, p0, LX/1By;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/1By;->h:J

    .line 214608
    iget-object v0, p4, LX/7i0;->a:Ljava/lang/String;

    iput-object v0, p0, LX/1By;->d:Ljava/lang/String;

    .line 214609
    iget-object v0, p4, LX/7i0;->b:Ljava/lang/String;

    iput-object v0, p0, LX/1By;->e:Ljava/lang/String;

    .line 214610
    iget-wide v0, p4, LX/7i0;->c:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_0

    .line 214611
    iget-wide v0, p0, LX/1By;->h:J

    const-wide/32 v2, 0x240c8400

    iget-wide v4, p4, LX/7i0;->c:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/1By;->i:J

    .line 214612
    :cond_0
    return-void
.end method


# virtual methods
.method public final g()J
    .locals 4

    .prologue
    .line 214614
    iget-wide v0, p0, LX/1By;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget-wide v0, p0, LX/1By;->i:J

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LX/1By;->i:J

    iget-wide v2, p0, LX/1By;->h:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 214613
    iget-object v0, p0, LX/1By;->d:Ljava/lang/String;

    invoke-static {v0}, LX/3C8;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
