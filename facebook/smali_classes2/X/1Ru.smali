.class public LX/1Ru;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1R2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1PW;",
        ">",
        "Ljava/lang/Object;",
        "LX/1R2",
        "<TT;>;"
    }
.end annotation


# static fields
.field public static a:Ljava/lang/Runnable;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static final c:LX/1OD;


# instance fields
.field public b:LX/1Sx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ru",
            "<TT;TE;>.Adapter",
            "ListMutator;"
        }
    .end annotation
.end field

.field private final d:LX/03V;

.field public e:LX/0g0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0g0",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final f:LX/1R0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1R0",
            "<TT;TE;>;"
        }
    .end annotation
.end field

.field public final g:LX/1PW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private h:Z

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1RA",
            "<*TE;>;>;"
        }
    .end annotation
.end field

.field public j:LX/1Sw;

.field public k:LX/1Sy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ru",
            "<TT;TE;>.",
            "ListItemCollectionObserver;"
        }
    .end annotation
.end field

.field private final l:LX/1R4;

.field private final m:LX/1R6;

.field public n:LX/1OD;

.field public o:I

.field public p:Ljava/lang/Exception;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/widget/recyclerview/BetterRecyclerView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 247246
    new-instance v0, LX/1Sv;

    invoke-direct {v0}, LX/1Sv;-><init>()V

    sput-object v0, LX/1Ru;->c:LX/1OD;

    return-void
.end method

.method public constructor <init>(LX/1R0;LX/1PW;LX/0g0;LX/03V;)V
    .locals 2
    .param p1    # LX/1R0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1PW;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0g0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1R0;",
            "TE;",
            "LX/0g0",
            "<TT;>;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 247231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247232
    iput-boolean v1, p0, LX/1Ru;->h:Z

    .line 247233
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Ru;->i:Ljava/util/List;

    .line 247234
    new-instance v0, LX/1Sw;

    invoke-direct {v0}, LX/1Sw;-><init>()V

    iput-object v0, p0, LX/1Ru;->j:LX/1Sw;

    .line 247235
    new-instance v0, LX/1Sx;

    invoke-direct {v0, p0}, LX/1Sx;-><init>(LX/1Ru;)V

    iput-object v0, p0, LX/1Ru;->b:LX/1Sx;

    .line 247236
    new-instance v0, LX/1Sy;

    invoke-direct {v0, p0}, LX/1Sy;-><init>(LX/1Ru;)V

    iput-object v0, p0, LX/1Ru;->k:LX/1Sy;

    .line 247237
    new-instance v0, LX/1Sz;

    invoke-direct {v0, p0}, LX/1Sz;-><init>(LX/1Ru;)V

    iput-object v0, p0, LX/1Ru;->l:LX/1R4;

    .line 247238
    new-instance v0, LX/1T0;

    invoke-direct {v0, p0}, LX/1T0;-><init>(LX/1Ru;)V

    iput-object v0, p0, LX/1Ru;->m:LX/1R6;

    .line 247239
    sget-object v0, LX/1Ru;->c:LX/1OD;

    iput-object v0, p0, LX/1Ru;->n:LX/1OD;

    .line 247240
    iput v1, p0, LX/1Ru;->o:I

    .line 247241
    iput-object p4, p0, LX/1Ru;->d:LX/03V;

    .line 247242
    iput-object p3, p0, LX/1Ru;->e:LX/0g0;

    .line 247243
    iput-object p2, p0, LX/1Ru;->g:LX/1PW;

    .line 247244
    iput-object p1, p0, LX/1Ru;->f:LX/1R0;

    .line 247245
    return-void
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 247094
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public static a$redex0(LX/1Ru;IZ)V
    .locals 3

    .prologue
    .line 247219
    if-eqz p2, :cond_0

    .line 247220
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 247221
    if-nez v0, :cond_1

    .line 247222
    :cond_0
    return-void

    .line 247223
    :cond_1
    iget-object v0, p0, LX/1Ru;->e:LX/0g0;

    invoke-interface {v0}, LX/0g1;->size()I

    move-result v0

    iget-object v1, p0, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 247224
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ListItemCollection has size "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/1Ru;->e:LX/0g0;

    invoke-interface {v2}, LX/0g1;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but AdaptersCollection has size "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 247225
    :cond_2
    iget-object v0, p0, LX/1Ru;->e:LX/0g0;

    invoke-interface {v0, p1}, LX/0g1;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 247226
    invoke-static {v0}, LX/1Ru;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 247227
    iget-object v0, p0, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RA;

    .line 247228
    iget-object v2, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v2

    .line 247229
    if-eq v1, v0, :cond_0

    .line 247230
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Item "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in ListItemCollection does not match item in AdaptersCollection"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b$redex0(LX/1Ru;IZ)V
    .locals 5

    .prologue
    .line 247209
    if-nez p2, :cond_1

    .line 247210
    :cond_0
    return-void

    .line 247211
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/1Ru;->e:LX/0g0;

    invoke-interface {v0}, LX/0g1;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 247212
    iget-object v0, p0, LX/1Ru;->e:LX/0g0;

    invoke-interface {v0, v1}, LX/0g1;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/1Ru;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 247213
    iget-object v0, p0, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RA;

    .line 247214
    iget-object v3, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v3

    .line 247215
    :goto_1
    if-eq v2, v0, :cond_3

    .line 247216
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "While trying to insert into "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " the following feedUnit at position "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is in the ListItemCollection but not the AdaptersCollection: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Last operation was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/1Ru;->o:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Logged exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/1Ru;->p:Ljava/lang/Exception;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 247217
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 247218
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static f(LX/1Ru;I)V
    .locals 3

    .prologue
    .line 247184
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 247185
    :goto_0
    const-string v0, "ObservableAdaptersCollection.destroy"

    const v2, 0x35304f82

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 247186
    :try_start_0
    iget-object v0, p0, LX/1Ru;->n:LX/1OD;

    if-eqz v0, :cond_0

    if-nez v1, :cond_0

    .line 247187
    iget-object v0, p0, LX/1Ru;->n:LX/1OD;

    invoke-virtual {v0}, LX/1OD;->a()V

    .line 247188
    :cond_0
    iget-object v0, p0, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RA;

    .line 247189
    invoke-virtual {v0}, LX/1RA;->c()V

    .line 247190
    if-eqz v1, :cond_1

    .line 247191
    invoke-virtual {v0}, LX/1RA;->d()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 247192
    :catch_0
    move-exception v0

    .line 247193
    :try_start_1
    iput-object v0, p0, LX/1Ru;->p:Ljava/lang/Exception;

    .line 247194
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 247195
    :catchall_0
    move-exception v0

    const v1, -0x5cba9c9f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 247196
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 247197
    :cond_3
    :try_start_2
    iget-object v0, p0, LX/1Ru;->b:LX/1Sx;

    .line 247198
    iget-object v1, v0, LX/1Sx;->a:LX/1Ru;

    iget-object v1, v1, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 247199
    iget-object v1, v0, LX/1Sx;->a:LX/1Ru;

    iget-object v1, v1, LX/1Ru;->j:LX/1Sw;

    .line 247200
    iget-object v0, v1, LX/1Sw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 247201
    invoke-static {v1}, LX/1Sw;->c(LX/1Sw;)V

    .line 247202
    iget-object v0, p0, LX/1Ru;->e:LX/0g0;

    iget-object v1, p0, LX/1Ru;->k:LX/1Sy;

    invoke-interface {v0, v1}, LX/0g0;->b(LX/0qs;)V

    .line 247203
    iget-object v0, p0, LX/1Ru;->m:LX/1R6;

    .line 247204
    iget-object v1, p0, LX/1Ru;->g:LX/1PW;

    instance-of v1, v1, LX/1Px;

    if-eqz v1, :cond_4

    .line 247205
    iget-object v1, p0, LX/1Ru;->g:LX/1PW;

    check-cast v1, LX/1Px;

    invoke-interface {v1, v0}, LX/1Px;->b(LX/1R6;)V

    .line 247206
    :cond_4
    const/4 v0, 0x5

    iput v0, p0, LX/1Ru;->o:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 247207
    const v0, 0x52191a39

    invoke-static {v0}, LX/02m;->a(I)V

    .line 247208
    return-void
.end method

.method public static h(LX/1Ru;I)LX/1RA;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1RA",
            "<TT;TE;>;"
        }
    .end annotation

    .prologue
    .line 247182
    iget-object v0, p0, LX/1Ru;->e:LX/0g0;

    invoke-interface {v0, p1}, LX/0g1;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 247183
    iget-object v1, p0, LX/1Ru;->f:LX/1R0;

    iget-object v2, p0, LX/1Ru;->g:LX/1PW;

    invoke-interface {v1, v0, v2}, LX/1R0;->a(Ljava/lang/Object;LX/1PW;)LX/1RA;

    move-result-object v0

    return-object v0
.end method

.method public static i(LX/1Ru;)V
    .locals 2

    .prologue
    .line 247179
    iget-boolean v0, p0, LX/1Ru;->h:Z

    if-eqz v0, :cond_0

    .line 247180
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This AdaptersCollection has already been destroyed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 247181
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)LX/1Rk;
    .locals 7

    .prologue
    .line 247166
    invoke-static {p0}, LX/1Ru;->i(LX/1Ru;)V

    .line 247167
    invoke-virtual {p0, p1}, LX/1Ru;->b(I)I

    move-result v1

    .line 247168
    invoke-virtual {p0, v1}, LX/1Ru;->c(I)I

    move-result v2

    .line 247169
    sub-int v3, p1, v2

    .line 247170
    :try_start_0
    iget-object v0, p0, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RA;

    invoke-virtual {v0, v3}, LX/1RA;->g(I)LX/1Rk;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 247171
    :goto_0
    return-object v0

    .line 247172
    :catch_0
    move-exception v0

    .line 247173
    iget-object v3, p0, LX/1Ru;->d:LX/03V;

    const-string v4, "ObservableAdaptersCollection.getAt"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "last operation "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, LX/1Ru;->o:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "rowIndex "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " feedEdgeIndex "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " firstRowIndex "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " size "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/1Ru;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " logged exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/1Ru;->p:Ljava/lang/Exception;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 247174
    invoke-virtual {p0, p1}, LX/1Ru;->b(I)I

    move-result v0

    .line 247175
    invoke-virtual {p0, v0}, LX/1Ru;->c(I)I

    move-result v1

    .line 247176
    sub-int v1, p1, v1

    .line 247177
    iget-object v2, p0, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RA;

    invoke-virtual {v0, v1}, LX/1RA;->g(I)LX/1Rk;

    move-result-object v0

    move-object v0, v0

    .line 247178
    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 247157
    invoke-static {p0}, LX/1Ru;->i(LX/1Ru;)V

    .line 247158
    iput-boolean v0, p0, LX/1Ru;->h:Z

    .line 247159
    invoke-static {p0, v0}, LX/1Ru;->f(LX/1Ru;I)V

    .line 247160
    iput-object v1, p0, LX/1Ru;->e:LX/0g0;

    .line 247161
    iget-object v0, p0, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 247162
    iput-object v1, p0, LX/1Ru;->j:LX/1Sw;

    .line 247163
    iput-object v1, p0, LX/1Ru;->b:LX/1Sx;

    .line 247164
    iput-object v1, p0, LX/1Ru;->k:LX/1Sy;

    .line 247165
    return-void
.end method

.method public final a(LX/5Mj;)V
    .locals 6

    .prologue
    .line 247149
    iget-object v0, p1, LX/5Mj;->f:LX/5Mk;

    move-object v1, v0

    .line 247150
    iget-object v0, p1, LX/5Mj;->e:Ljava/io/PrintWriter;

    move-object v2, v0

    .line 247151
    iget-object v0, p0, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 247152
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 247153
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "story "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 247154
    iget-object v4, p0, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4, p1}, LX/5Mk;->a(Ljava/lang/Object;LX/5Mj;)V

    .line 247155
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 247156
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 247146
    invoke-static {p0}, LX/1Ru;->i(LX/1Ru;)V

    .line 247147
    check-cast p1, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object p1, p0, LX/1Ru;->q:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 247148
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 247088
    invoke-static {p0}, LX/1Ru;->i(LX/1Ru;)V

    .line 247089
    sget-object v0, LX/1Ru;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 247090
    sget-object v0, LX/1Ru;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 247091
    :cond_0
    iget-object v0, p0, LX/1Ru;->j:LX/1Sw;

    .line 247092
    iget-object p0, v0, LX/1Sw;->a:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 p0, 0x0

    :goto_0
    move v0, p0

    .line 247093
    return v0

    :cond_1
    iget-object p0, v0, LX/1Sw;->a:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    invoke-virtual {v0, p0}, LX/1Sw;->e(I)I

    move-result p0

    add-int/lit8 p0, p0, 0x1

    goto :goto_0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 247095
    invoke-static {p0}, LX/1Ru;->i(LX/1Ru;)V

    .line 247096
    iget-object v0, p0, LX/1Ru;->j:LX/1Sw;

    invoke-virtual {v0, p1}, LX/1Sw;->a(I)I

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 247097
    invoke-static {p0}, LX/1Ru;->i(LX/1Ru;)V

    .line 247098
    iget-object v0, p0, LX/1Ru;->j:LX/1Sw;

    invoke-virtual {v0, p1}, LX/1Sw;->d(I)I

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 247099
    invoke-static {p0}, LX/1Ru;->i(LX/1Ru;)V

    .line 247100
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/1Ru;->f(LX/1Ru;I)V

    .line 247101
    invoke-virtual {p0}, LX/1Ru;->e()V

    .line 247102
    return-void
.end method

.method public final d(I)I
    .locals 1

    .prologue
    .line 247103
    invoke-static {p0}, LX/1Ru;->i(LX/1Ru;)V

    .line 247104
    iget-object v0, p0, LX/1Ru;->j:LX/1Sw;

    invoke-virtual {v0, p1}, LX/1Sw;->e(I)I

    move-result v0

    return v0
.end method

.method public final d()LX/1R4;
    .locals 1

    .prologue
    .line 247105
    invoke-static {p0}, LX/1Ru;->i(LX/1Ru;)V

    .line 247106
    iget-object v0, p0, LX/1Ru;->l:LX/1R4;

    return-object v0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 247107
    invoke-static {p0}, LX/1Ru;->i(LX/1Ru;)V

    .line 247108
    const-string v0, "ObservableAdaptersCollection.regenerateInternalAdapters"

    const v1, 0x4a137725    # 2416073.2f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 247109
    :try_start_0
    iget-object v0, p0, LX/1Ru;->m:LX/1R6;

    .line 247110
    iget-object v1, p0, LX/1Ru;->g:LX/1PW;

    instance-of v1, v1, LX/1Px;

    if-eqz v1, :cond_0

    .line 247111
    iget-object v1, p0, LX/1Ru;->g:LX/1PW;

    check-cast v1, LX/1Px;

    invoke-interface {v1, v0}, LX/1Px;->a(LX/1R6;)V

    .line 247112
    :cond_0
    iget-object v0, p0, LX/1Ru;->e:LX/0g0;

    iget-object v1, p0, LX/1Ru;->k:LX/1Sy;

    invoke-interface {v0, v1}, LX/0g0;->a(LX/0qs;)V

    .line 247113
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/1Ru;->e:LX/0g0;

    invoke-interface {v1}, LX/0g1;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 247114
    invoke-static {p0, v0}, LX/1Ru;->h(LX/1Ru;I)LX/1RA;

    move-result-object v1

    .line 247115
    iget-object v2, p0, LX/1Ru;->b:LX/1Sx;

    .line 247116
    iget-object v3, v2, LX/1Sx;->a:LX/1Ru;

    iget-object v3, v3, LX/1Ru;->i:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247117
    iget-object v3, v2, LX/1Sx;->a:LX/1Ru;

    iget-object v3, v3, LX/1Ru;->j:LX/1Sw;

    .line 247118
    invoke-static {v3}, LX/1Sw;->c(LX/1Sw;)V

    .line 247119
    iget-object v2, v3, LX/1Sw;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247120
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 247121
    :cond_1
    iget-object v0, p0, LX/1Ru;->n:LX/1OD;

    if-eqz v0, :cond_2

    .line 247122
    iget-object v0, p0, LX/1Ru;->n:LX/1OD;

    invoke-virtual {v0}, LX/1OD;->a()V

    .line 247123
    :cond_2
    const/4 v0, 0x6

    iput v0, p0, LX/1Ru;->o:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 247124
    const v0, 0x3ff576fa

    invoke-static {v0}, LX/02m;->a(I)V

    .line 247125
    return-void

    .line 247126
    :catch_0
    move-exception v0

    .line 247127
    :try_start_1
    iput-object v0, p0, LX/1Ru;->p:Ljava/lang/Exception;

    .line 247128
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 247129
    :catchall_0
    move-exception v0

    const v1, -0x79bf4b53

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final e(I)Z
    .locals 4

    .prologue
    .line 247130
    invoke-static {p0}, LX/1Ru;->i(LX/1Ru;)V

    .line 247131
    iget-object v0, p0, LX/1Ru;->j:LX/1Sw;

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 247132
    if-gez p1, :cond_1

    .line 247133
    :cond_0
    :goto_0
    move v0, v2

    .line 247134
    return v0

    .line 247135
    :cond_1
    invoke-static {v0}, LX/1Sw;->d(LX/1Sw;)I

    move-result v1

    if-gt p1, v1, :cond_2

    move v2, v3

    .line 247136
    goto :goto_0

    :cond_2
    move v1, v2

    .line 247137
    :goto_1
    iget-object p0, v0, LX/1Sw;->a:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    if-ge v1, p0, :cond_0

    .line 247138
    add-int/lit8 p0, v1, 0x1

    invoke-static {v0, p0}, LX/1Sw;->h(LX/1Sw;I)V

    .line 247139
    invoke-static {v0}, LX/1Sw;->d(LX/1Sw;)I

    move-result p0

    if-gt p1, p0, :cond_3

    move v2, v3

    .line 247140
    goto :goto_0

    .line 247141
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 247142
    iget-object v0, p0, LX/1Ru;->e:LX/0g0;

    invoke-interface {v0}, LX/0g1;->size()I

    move-result v0

    return v0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 247143
    invoke-static {p0}, LX/1Ru;->i(LX/1Ru;)V

    .line 247144
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Ru;->q:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 247145
    return-void
.end method
