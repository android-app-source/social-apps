.class public final LX/1ce;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field private final c:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 282377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282378
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/1ce;->a:Ljava/lang/String;

    .line 282379
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/1ce;->b:Ljava/lang/String;

    .line 282380
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, 0x186a3

    mul-int/2addr v0, v1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/1ce;->c:I

    .line 282381
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 282382
    instance-of v1, p1, LX/1ce;

    if-nez v1, :cond_1

    .line 282383
    :cond_0
    :goto_0
    return v0

    .line 282384
    :cond_1
    check-cast p1, LX/1ce;

    .line 282385
    iget-object v1, p0, LX/1ce;->a:Ljava/lang/String;

    iget-object v2, p1, LX/1ce;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1ce;->b:Ljava/lang/String;

    iget-object v2, p1, LX/1ce;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 282386
    iget v0, p0, LX/1ce;->c:I

    return v0
.end method
