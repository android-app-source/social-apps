.class public LX/0pe;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0pe;


# instance fields
.field public final a:LX/0pf;

.field public final b:LX/0pg;

.field public final c:LX/0lB;


# direct methods
.method public constructor <init>(LX/0pf;LX/0pg;LX/0lB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 145012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145013
    iput-object p1, p0, LX/0pe;->a:LX/0pf;

    .line 145014
    iput-object p2, p0, LX/0pe;->b:LX/0pg;

    .line 145015
    iput-object p3, p0, LX/0pe;->c:LX/0lB;

    .line 145016
    return-void
.end method

.method public static a(LX/0QB;)LX/0pe;
    .locals 6

    .prologue
    .line 145017
    sget-object v0, LX/0pe;->d:LX/0pe;

    if-nez v0, :cond_1

    .line 145018
    const-class v1, LX/0pe;

    monitor-enter v1

    .line 145019
    :try_start_0
    sget-object v0, LX/0pe;->d:LX/0pe;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 145020
    if-eqz v2, :cond_0

    .line 145021
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 145022
    new-instance p0, LX/0pe;

    invoke-static {v0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v3

    check-cast v3, LX/0pf;

    .line 145023
    new-instance v5, LX/0pg;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-direct {v5, v4}, LX/0pg;-><init>(LX/0W3;)V

    .line 145024
    move-object v4, v5

    .line 145025
    check-cast v4, LX/0pg;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lB;

    invoke-direct {p0, v3, v4, v5}, LX/0pe;-><init>(LX/0pf;LX/0pg;LX/0lB;)V

    .line 145026
    move-object v0, p0

    .line 145027
    sput-object v0, LX/0pe;->d:LX/0pe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145028
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 145029
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 145030
    :cond_1
    sget-object v0, LX/0pe;->d:LX/0pe;

    return-object v0

    .line 145031
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 145032
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 145033
    const-string v0, "ViewportLoggingHandler.getStrippedTrackingData"

    const v1, -0x57ced264

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 145034
    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 145035
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 145036
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 145037
    iget-object v1, p0, LX/0pe;->a:LX/0pf;

    invoke-virtual {v1, v0}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v0

    .line 145038
    iget-object v1, v0, LX/1g0;->n:LX/162;

    move-object v1, v1

    .line 145039
    if-nez v1, :cond_0

    .line 145040
    invoke-static {p1}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    iget-object v2, p0, LX/0pe;->b:LX/0pg;

    invoke-virtual {v2}, LX/0pg;->a()LX/0Rf;

    move-result-object v2

    iget-object v3, p0, LX/0pe;->c:LX/0lB;

    invoke-static {v1, v2, v3}, LX/1BX;->a(LX/162;LX/0Rf;LX/0lB;)LX/162;

    move-result-object v1

    .line 145041
    iput-object v1, v0, LX/1g0;->n:LX/162;

    .line 145042
    :cond_0
    iget-object v1, v0, LX/1g0;->n:LX/162;

    move-object v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145043
    const v1, 0xf723fdf

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x70df8141

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 145044
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 145045
    iget-object v0, p0, LX/0pe;->a:LX/0pf;

    invoke-virtual {v0, p1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v0

    .line 145046
    iget-boolean p0, v0, LX/1g0;->d:Z

    move v0, p0

    .line 145047
    return v0
.end method
