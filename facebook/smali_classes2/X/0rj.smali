.class public final enum LX/0rj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0rj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0rj;

.field public static final enum COLD_START_RERANKING_COMPLETE:LX/0rj;

.field public static final enum COLD_START_RERANKING_START:LX/0rj;

.field public static final enum DATA_LOADED:LX/0rj;

.field public static final enum FRAGMENT_CREATED:LX/0rj;

.field public static final enum FRAGMENT_DESTROYED:LX/0rj;

.field public static final enum FRAGMENT_RESUMED:LX/0rj;

.field public static final enum GOT_PERSISTED_DATA_LOADER:LX/0rj;

.field public static final enum GOT_TRANSIENT_DATA_LOADER:LX/0rj;

.field public static final enum HEAD_FETCH:LX/0rj;

.field public static final enum HEAD_FETCH_ALREADY_HAPPENING:LX/0rj;

.field public static final enum HEAD_FETCH_CANCELED:LX/0rj;

.field public static final enum HEAD_FETCH_CHUNKED_SUCCEED:LX/0rj;

.field public static final enum HEAD_FETCH_FAILED:LX/0rj;

.field public static final enum HEAD_FETCH_SUCCEED:LX/0rj;

.field public static final enum LOADING_INDICATOR_HIDDEN:LX/0rj;

.field public static final enum LOADING_INDICATOR_SHOWN:LX/0rj;

.field public static final enum NEW_STORIES_BELOW_PILL_FAILED_TO_SHOW:LX/0rj;

.field public static final enum NEW_STORIES_BELOW_PILL_FULLY_LOADED_TEXT_SHOWN:LX/0rj;

.field public static final enum NEW_STORIES_BELOW_PILL_HIDDEN:LX/0rj;

.field public static final enum NEW_STORIES_BELOW_PILL_HIDDEN_IF_NOT_FULLY_LOADED:LX/0rj;

.field public static final enum NEW_STORIES_BELOW_PILL_SPINNER_SHOWN:LX/0rj;

.field public static final enum NEW_STORIES_BELOW_PILL_TAPPED:LX/0rj;

.field public static final enum NEW_STORY_PILL_HIDDEN:LX/0rj;

.field public static final enum NEW_STORY_PILL_SHOWN:LX/0rj;

.field public static final enum NEW_STORY_PILL_TAPPED:LX/0rj;

.field public static final enum NO_NETWORK_DATA_PTR_RERANKING_START:LX/0rj;

.field public static final enum ON_FAILURE_PTR_RERANKING_START:LX/0rj;

.field public static final enum PTR_RERANKING_COMPLETE:LX/0rj;

.field public static final enum PTR_RERANKING_FAILURE:LX/0rj;

.field public static final enum PTR_RERANKING_NO_RESULTS:LX/0rj;

.field public static final enum PTR_RERANKING_SCHEDULED:LX/0rj;

.field public static final enum REACHED_TOP:LX/0rj;

.field public static final enum RELEASED_LOADER:LX/0rj;

.field public static final enum SKIP_TAIL_GAP_FUTURE_CLEARED:LX/0rj;

.field public static final enum STATUS_CHANGED:LX/0rj;

.field public static final enum SWIPE_LAYOUT_NULL:LX/0rj;

.field public static final enum TAIL_FETCH:LX/0rj;

.field public static final enum TAIL_FETCH_BACKGROUND_SUCCEED:LX/0rj;

.field public static final enum TAIL_FETCH_CANCELED:LX/0rj;

.field public static final enum TAIL_FETCH_FAILED:LX/0rj;

.field public static final enum TAIL_FETCH_SKIP_FINISHED_WITH_NO_RESULTS:LX/0rj;

.field public static final enum TAIL_FETCH_SKIP_FINISHED_WITH_RESULTS:LX/0rj;

.field public static final enum TAIL_FETCH_SKIP_GAP_SCHEDULED:LX/0rj;

.field public static final enum TAIL_FETCH_SUCCEED:LX/0rj;

.field public static final enum VIEW_CREATED:LX/0rj;

.field public static final enum VIEW_DESTROYED:LX/0rj;

.field public static final enum WARM_START_RERANKING_COMPLETE:LX/0rj;

.field public static final enum WARM_START_RERANKING_START:LX/0rj;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 150268
    new-instance v0, LX/0rj;

    const-string v1, "FRAGMENT_CREATED"

    invoke-direct {v0, v1, v3}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->FRAGMENT_CREATED:LX/0rj;

    .line 150269
    new-instance v0, LX/0rj;

    const-string v1, "FRAGMENT_RESUMED"

    invoke-direct {v0, v1, v4}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->FRAGMENT_RESUMED:LX/0rj;

    .line 150270
    new-instance v0, LX/0rj;

    const-string v1, "VIEW_CREATED"

    invoke-direct {v0, v1, v5}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->VIEW_CREATED:LX/0rj;

    .line 150271
    new-instance v0, LX/0rj;

    const-string v1, "GOT_PERSISTED_DATA_LOADER"

    invoke-direct {v0, v1, v6}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->GOT_PERSISTED_DATA_LOADER:LX/0rj;

    .line 150272
    new-instance v0, LX/0rj;

    const-string v1, "GOT_TRANSIENT_DATA_LOADER"

    invoke-direct {v0, v1, v7}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->GOT_TRANSIENT_DATA_LOADER:LX/0rj;

    .line 150273
    new-instance v0, LX/0rj;

    const-string v1, "RELEASED_LOADER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->RELEASED_LOADER:LX/0rj;

    .line 150274
    new-instance v0, LX/0rj;

    const-string v1, "FRAGMENT_DESTROYED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->FRAGMENT_DESTROYED:LX/0rj;

    .line 150275
    new-instance v0, LX/0rj;

    const-string v1, "VIEW_DESTROYED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->VIEW_DESTROYED:LX/0rj;

    .line 150276
    new-instance v0, LX/0rj;

    const-string v1, "REACHED_TOP"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->REACHED_TOP:LX/0rj;

    .line 150277
    new-instance v0, LX/0rj;

    const-string v1, "HEAD_FETCH"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->HEAD_FETCH:LX/0rj;

    .line 150278
    new-instance v0, LX/0rj;

    const-string v1, "HEAD_FETCH_CANCELED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->HEAD_FETCH_CANCELED:LX/0rj;

    .line 150279
    new-instance v0, LX/0rj;

    const-string v1, "HEAD_FETCH_FAILED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->HEAD_FETCH_FAILED:LX/0rj;

    .line 150280
    new-instance v0, LX/0rj;

    const-string v1, "HEAD_FETCH_SUCCEED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->HEAD_FETCH_SUCCEED:LX/0rj;

    .line 150281
    new-instance v0, LX/0rj;

    const-string v1, "HEAD_FETCH_CHUNKED_SUCCEED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->HEAD_FETCH_CHUNKED_SUCCEED:LX/0rj;

    .line 150282
    new-instance v0, LX/0rj;

    const-string v1, "HEAD_FETCH_ALREADY_HAPPENING"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->HEAD_FETCH_ALREADY_HAPPENING:LX/0rj;

    .line 150283
    new-instance v0, LX/0rj;

    const-string v1, "TAIL_FETCH"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->TAIL_FETCH:LX/0rj;

    .line 150284
    new-instance v0, LX/0rj;

    const-string v1, "TAIL_FETCH_CANCELED"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->TAIL_FETCH_CANCELED:LX/0rj;

    .line 150285
    new-instance v0, LX/0rj;

    const-string v1, "TAIL_FETCH_FAILED"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->TAIL_FETCH_FAILED:LX/0rj;

    .line 150286
    new-instance v0, LX/0rj;

    const-string v1, "TAIL_FETCH_SUCCEED"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->TAIL_FETCH_SUCCEED:LX/0rj;

    .line 150287
    new-instance v0, LX/0rj;

    const-string v1, "TAIL_FETCH_BACKGROUND_SUCCEED"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->TAIL_FETCH_BACKGROUND_SUCCEED:LX/0rj;

    .line 150288
    new-instance v0, LX/0rj;

    const-string v1, "TAIL_FETCH_SKIP_GAP_SCHEDULED"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->TAIL_FETCH_SKIP_GAP_SCHEDULED:LX/0rj;

    .line 150289
    new-instance v0, LX/0rj;

    const-string v1, "TAIL_FETCH_SKIP_FINISHED_WITH_RESULTS"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->TAIL_FETCH_SKIP_FINISHED_WITH_RESULTS:LX/0rj;

    .line 150290
    new-instance v0, LX/0rj;

    const-string v1, "TAIL_FETCH_SKIP_FINISHED_WITH_NO_RESULTS"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->TAIL_FETCH_SKIP_FINISHED_WITH_NO_RESULTS:LX/0rj;

    .line 150291
    new-instance v0, LX/0rj;

    const-string v1, "DATA_LOADED"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->DATA_LOADED:LX/0rj;

    .line 150292
    new-instance v0, LX/0rj;

    const-string v1, "STATUS_CHANGED"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->STATUS_CHANGED:LX/0rj;

    .line 150293
    new-instance v0, LX/0rj;

    const-string v1, "COLD_START_RERANKING_START"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->COLD_START_RERANKING_START:LX/0rj;

    .line 150294
    new-instance v0, LX/0rj;

    const-string v1, "COLD_START_RERANKING_COMPLETE"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->COLD_START_RERANKING_COMPLETE:LX/0rj;

    .line 150295
    new-instance v0, LX/0rj;

    const-string v1, "PTR_RERANKING_SCHEDULED"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->PTR_RERANKING_SCHEDULED:LX/0rj;

    .line 150296
    new-instance v0, LX/0rj;

    const-string v1, "PTR_RERANKING_COMPLETE"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->PTR_RERANKING_COMPLETE:LX/0rj;

    .line 150297
    new-instance v0, LX/0rj;

    const-string v1, "PTR_RERANKING_NO_RESULTS"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->PTR_RERANKING_NO_RESULTS:LX/0rj;

    .line 150298
    new-instance v0, LX/0rj;

    const-string v1, "PTR_RERANKING_FAILURE"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->PTR_RERANKING_FAILURE:LX/0rj;

    .line 150299
    new-instance v0, LX/0rj;

    const-string v1, "NO_NETWORK_DATA_PTR_RERANKING_START"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->NO_NETWORK_DATA_PTR_RERANKING_START:LX/0rj;

    .line 150300
    new-instance v0, LX/0rj;

    const-string v1, "ON_FAILURE_PTR_RERANKING_START"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->ON_FAILURE_PTR_RERANKING_START:LX/0rj;

    .line 150301
    new-instance v0, LX/0rj;

    const-string v1, "WARM_START_RERANKING_START"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->WARM_START_RERANKING_START:LX/0rj;

    .line 150302
    new-instance v0, LX/0rj;

    const-string v1, "WARM_START_RERANKING_COMPLETE"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->WARM_START_RERANKING_COMPLETE:LX/0rj;

    .line 150303
    new-instance v0, LX/0rj;

    const-string v1, "NEW_STORY_PILL_SHOWN"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->NEW_STORY_PILL_SHOWN:LX/0rj;

    .line 150304
    new-instance v0, LX/0rj;

    const-string v1, "NEW_STORY_PILL_TAPPED"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->NEW_STORY_PILL_TAPPED:LX/0rj;

    .line 150305
    new-instance v0, LX/0rj;

    const-string v1, "NEW_STORY_PILL_HIDDEN"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->NEW_STORY_PILL_HIDDEN:LX/0rj;

    .line 150306
    new-instance v0, LX/0rj;

    const-string v1, "LOADING_INDICATOR_SHOWN"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->LOADING_INDICATOR_SHOWN:LX/0rj;

    .line 150307
    new-instance v0, LX/0rj;

    const-string v1, "LOADING_INDICATOR_HIDDEN"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->LOADING_INDICATOR_HIDDEN:LX/0rj;

    .line 150308
    new-instance v0, LX/0rj;

    const-string v1, "NEW_STORIES_BELOW_PILL_SPINNER_SHOWN"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->NEW_STORIES_BELOW_PILL_SPINNER_SHOWN:LX/0rj;

    .line 150309
    new-instance v0, LX/0rj;

    const-string v1, "NEW_STORIES_BELOW_PILL_FULLY_LOADED_TEXT_SHOWN"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->NEW_STORIES_BELOW_PILL_FULLY_LOADED_TEXT_SHOWN:LX/0rj;

    .line 150310
    new-instance v0, LX/0rj;

    const-string v1, "NEW_STORIES_BELOW_PILL_HIDDEN"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->NEW_STORIES_BELOW_PILL_HIDDEN:LX/0rj;

    .line 150311
    new-instance v0, LX/0rj;

    const-string v1, "NEW_STORIES_BELOW_PILL_HIDDEN_IF_NOT_FULLY_LOADED"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->NEW_STORIES_BELOW_PILL_HIDDEN_IF_NOT_FULLY_LOADED:LX/0rj;

    .line 150312
    new-instance v0, LX/0rj;

    const-string v1, "NEW_STORIES_BELOW_PILL_TAPPED"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->NEW_STORIES_BELOW_PILL_TAPPED:LX/0rj;

    .line 150313
    new-instance v0, LX/0rj;

    const-string v1, "NEW_STORIES_BELOW_PILL_FAILED_TO_SHOW"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->NEW_STORIES_BELOW_PILL_FAILED_TO_SHOW:LX/0rj;

    .line 150314
    new-instance v0, LX/0rj;

    const-string v1, "SKIP_TAIL_GAP_FUTURE_CLEARED"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->SKIP_TAIL_GAP_FUTURE_CLEARED:LX/0rj;

    .line 150315
    new-instance v0, LX/0rj;

    const-string v1, "SWIPE_LAYOUT_NULL"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, LX/0rj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rj;->SWIPE_LAYOUT_NULL:LX/0rj;

    .line 150316
    const/16 v0, 0x30

    new-array v0, v0, [LX/0rj;

    sget-object v1, LX/0rj;->FRAGMENT_CREATED:LX/0rj;

    aput-object v1, v0, v3

    sget-object v1, LX/0rj;->FRAGMENT_RESUMED:LX/0rj;

    aput-object v1, v0, v4

    sget-object v1, LX/0rj;->VIEW_CREATED:LX/0rj;

    aput-object v1, v0, v5

    sget-object v1, LX/0rj;->GOT_PERSISTED_DATA_LOADER:LX/0rj;

    aput-object v1, v0, v6

    sget-object v1, LX/0rj;->GOT_TRANSIENT_DATA_LOADER:LX/0rj;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0rj;->RELEASED_LOADER:LX/0rj;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0rj;->FRAGMENT_DESTROYED:LX/0rj;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0rj;->VIEW_DESTROYED:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0rj;->REACHED_TOP:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0rj;->HEAD_FETCH:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0rj;->HEAD_FETCH_CANCELED:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0rj;->HEAD_FETCH_FAILED:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/0rj;->HEAD_FETCH_SUCCEED:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/0rj;->HEAD_FETCH_CHUNKED_SUCCEED:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/0rj;->HEAD_FETCH_ALREADY_HAPPENING:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/0rj;->TAIL_FETCH:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/0rj;->TAIL_FETCH_CANCELED:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/0rj;->TAIL_FETCH_FAILED:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/0rj;->TAIL_FETCH_SUCCEED:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/0rj;->TAIL_FETCH_BACKGROUND_SUCCEED:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/0rj;->TAIL_FETCH_SKIP_GAP_SCHEDULED:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/0rj;->TAIL_FETCH_SKIP_FINISHED_WITH_RESULTS:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/0rj;->TAIL_FETCH_SKIP_FINISHED_WITH_NO_RESULTS:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/0rj;->DATA_LOADED:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/0rj;->STATUS_CHANGED:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/0rj;->COLD_START_RERANKING_START:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/0rj;->COLD_START_RERANKING_COMPLETE:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/0rj;->PTR_RERANKING_SCHEDULED:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/0rj;->PTR_RERANKING_COMPLETE:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/0rj;->PTR_RERANKING_NO_RESULTS:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/0rj;->PTR_RERANKING_FAILURE:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/0rj;->NO_NETWORK_DATA_PTR_RERANKING_START:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/0rj;->ON_FAILURE_PTR_RERANKING_START:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/0rj;->WARM_START_RERANKING_START:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/0rj;->WARM_START_RERANKING_COMPLETE:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/0rj;->NEW_STORY_PILL_SHOWN:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/0rj;->NEW_STORY_PILL_TAPPED:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/0rj;->NEW_STORY_PILL_HIDDEN:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/0rj;->LOADING_INDICATOR_SHOWN:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/0rj;->LOADING_INDICATOR_HIDDEN:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/0rj;->NEW_STORIES_BELOW_PILL_SPINNER_SHOWN:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/0rj;->NEW_STORIES_BELOW_PILL_FULLY_LOADED_TEXT_SHOWN:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/0rj;->NEW_STORIES_BELOW_PILL_HIDDEN:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/0rj;->NEW_STORIES_BELOW_PILL_HIDDEN_IF_NOT_FULLY_LOADED:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, LX/0rj;->NEW_STORIES_BELOW_PILL_TAPPED:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, LX/0rj;->NEW_STORIES_BELOW_PILL_FAILED_TO_SHOW:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, LX/0rj;->SKIP_TAIL_GAP_FUTURE_CLEARED:LX/0rj;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, LX/0rj;->SWIPE_LAYOUT_NULL:LX/0rj;

    aput-object v2, v0, v1

    sput-object v0, LX/0rj;->$VALUES:[LX/0rj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 150317
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0rj;
    .locals 1

    .prologue
    .line 150318
    const-class v0, LX/0rj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0rj;

    return-object v0
.end method

.method public static values()[LX/0rj;
    .locals 1

    .prologue
    .line 150319
    sget-object v0, LX/0rj;->$VALUES:[LX/0rj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0rj;

    return-object v0
.end method
