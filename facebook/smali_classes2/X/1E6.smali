.class public LX/1E6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public a:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Z

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 219204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219205
    return-void
.end method

.method public static a(LX/0QB;)LX/1E6;
    .locals 4

    .prologue
    .line 219191
    const-class v1, LX/1E6;

    monitor-enter v1

    .line 219192
    :try_start_0
    sget-object v0, LX/1E6;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 219193
    sput-object v2, LX/1E6;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 219194
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219195
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 219196
    new-instance p0, LX/1E6;

    invoke-direct {p0}, LX/1E6;-><init>()V

    .line 219197
    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    .line 219198
    iput-object v3, p0, LX/1E6;->a:LX/0ad;

    .line 219199
    move-object v0, p0

    .line 219200
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 219201
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1E6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 219202
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 219203
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x6

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 219180
    iget-boolean v1, p0, LX/1E6;->b:Z

    if-eqz v1, :cond_0

    .line 219181
    iget-object v0, p0, LX/1E6;->c:Ljava/lang/String;

    .line 219182
    :goto_0
    return-object v0

    .line 219183
    :cond_0
    new-array v3, v6, [C

    sget-char v1, LX/1v6;->B:C

    aput-char v1, v3, v0

    sget-char v1, LX/1v6;->C:C

    aput-char v1, v3, v5

    const/4 v1, 0x2

    sget-char v4, LX/1v6;->D:C

    aput-char v4, v3, v1

    const/4 v1, 0x3

    sget-char v4, LX/1v6;->E:C

    aput-char v4, v3, v1

    const/4 v1, 0x4

    sget-char v4, LX/1v6;->F:C

    aput-char v4, v3, v1

    const/4 v1, 0x5

    sget-char v4, LX/1v6;->G:C

    aput-char v4, v3, v1

    move v1, v0

    move-object v0, v2

    .line 219184
    :goto_1
    if-ge v1, v6, :cond_1

    aget-char v0, v3, v1

    .line 219185
    iget-object v4, p0, LX/1E6;->a:LX/0ad;

    invoke-interface {v4, v0, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 219186
    if-nez v0, :cond_1

    .line 219187
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 219188
    :cond_1
    iput-boolean v5, p0, LX/1E6;->b:Z

    .line 219189
    iput-object v0, p0, LX/1E6;->c:Ljava/lang/String;

    .line 219190
    iget-object v0, p0, LX/1E6;->c:Ljava/lang/String;

    goto :goto_0
.end method
