.class public LX/1qn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qs;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 331473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331474
    return-void
.end method


# virtual methods
.method public final a()LX/0yY;
    .locals 1

    .prologue
    .line 331475
    sget-object v0, LX/0yY;->EXTERNAL_URLS_INTERSTITIAL:LX/0yY;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 331476
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 331477
    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 331478
    invoke-static {p1}, LX/Ehu;->b(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 331479
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 331480
    :cond_0
    :goto_0
    return v0

    .line 331481
    :cond_1
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v1, LX/0ax;->el:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, LX/1H1;->a(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v0

    goto :goto_0
.end method
