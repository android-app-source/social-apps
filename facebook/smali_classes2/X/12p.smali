.class public LX/12p;
.super LX/12q;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/12q",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final delegate:LX/0Py;
    .annotation build Lcom/google/j2objc/annotations/Weak;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Py",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final delegateList:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Py;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Py",
            "<TE;>;",
            "LX/0Px",
            "<+TE;>;)V"
        }
    .end annotation

    .prologue
    .line 175768
    invoke-direct {p0}, LX/12q;-><init>()V

    .line 175769
    iput-object p1, p0, LX/12p;->delegate:LX/0Py;

    .line 175770
    iput-object p2, p0, LX/12p;->delegateList:LX/0Px;

    .line 175771
    return-void
.end method

.method public constructor <init>(LX/0Py;[Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Py",
            "<TE;>;[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 175766
    invoke-static {p2}, LX/0Px;->asImmutableList([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/12p;-><init>(LX/0Py;LX/0Px;)V

    .line 175767
    return-void
.end method


# virtual methods
.method public a()LX/0Py;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Py",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 175765
    iget-object v0, p0, LX/12p;->delegate:LX/0Py;

    return-object v0
.end method

.method public final copyIntoArray([Ljava/lang/Object;I)I
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "not present in emulated superclass"
    .end annotation

    .prologue
    .line 175772
    iget-object v0, p0, LX/12p;->delegateList:LX/0Px;

    invoke-virtual {v0, p1, p2}, LX/0Px;->copyIntoArray([Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 175764
    iget-object v0, p0, LX/12p;->delegateList:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final listIterator(I)LX/0Rb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0Rb",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 175763
    iget-object v0, p0, LX/12p;->delegateList:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->listIterator(I)LX/0Rb;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 175762
    invoke-virtual {p0, p1}, LX/12p;->listIterator(I)LX/0Rb;

    move-result-object v0

    return-object v0
.end method
