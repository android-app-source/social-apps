.class public LX/0YU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Object;


# instance fields
.field public b:Z

.field public c:[I

.field public d:[Ljava/lang/Object;

.field public e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81381
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/0YU;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 81351
    const/16 v0, 0xa

    invoke-direct {p0, v0}, LX/0YU;-><init>(I)V

    .line 81352
    return-void
.end method

.method public constructor <init>(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 81353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81354
    iput-boolean v2, p0, LX/0YU;->b:Z

    .line 81355
    if-nez p1, :cond_0

    .line 81356
    sget-object v0, LX/01M;->a:[I

    iput-object v0, p0, LX/0YU;->c:[I

    .line 81357
    sget-object v0, LX/01M;->c:[Ljava/lang/Object;

    iput-object v0, p0, LX/0YU;->d:[Ljava/lang/Object;

    .line 81358
    :goto_0
    iput v2, p0, LX/0YU;->e:I

    .line 81359
    return-void

    .line 81360
    :cond_0
    invoke-static {p1}, LX/01M;->a(I)I

    move-result v0

    .line 81361
    new-array v1, v0, [I

    iput-object v1, p0, LX/0YU;->c:[I

    .line 81362
    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, LX/0YU;->d:[Ljava/lang/Object;

    goto :goto_0
.end method

.method public static d(LX/0YU;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 81363
    iget v3, p0, LX/0YU;->e:I

    .line 81364
    iget-object v4, p0, LX/0YU;->c:[I

    .line 81365
    iget-object v5, p0, LX/0YU;->d:[Ljava/lang/Object;

    move v1, v2

    move v0, v2

    .line 81366
    :goto_0
    if-ge v1, v3, :cond_2

    .line 81367
    aget-object v6, v5, v1

    .line 81368
    sget-object v7, LX/0YU;->a:Ljava/lang/Object;

    if-eq v6, v7, :cond_1

    .line 81369
    if-eq v1, v0, :cond_0

    .line 81370
    aget v7, v4, v1

    aput v7, v4, v0

    .line 81371
    aput-object v6, v5, v0

    .line 81372
    const/4 v6, 0x0

    aput-object v6, v5, v1

    .line 81373
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 81374
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 81375
    :cond_2
    iput-boolean v2, p0, LX/0YU;->b:Z

    .line 81376
    iput v0, p0, LX/0YU;->e:I

    .line 81377
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 81378
    iget-boolean v0, p0, LX/0YU;->b:Z

    if-eqz v0, :cond_0

    .line 81379
    invoke-static {p0}, LX/0YU;->d(LX/0YU;)V

    .line 81380
    :cond_0
    iget v0, p0, LX/0YU;->e:I

    return v0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 81382
    const/4 v0, 0x0

    .line 81383
    iget-object v1, p0, LX/0YU;->c:[I

    iget v2, p0, LX/0YU;->e:I

    invoke-static {v1, v2, p1}, LX/01M;->a([III)I

    move-result v1

    .line 81384
    if-ltz v1, :cond_0

    iget-object v2, p0, LX/0YU;->d:[Ljava/lang/Object;

    aget-object v2, v2, v1

    sget-object v3, LX/0YU;->a:Ljava/lang/Object;

    if-ne v2, v3, :cond_1

    .line 81385
    :cond_0
    :goto_0
    move-object v0, v0

    .line 81386
    return-object v0

    :cond_1
    iget-object v2, p0, LX/0YU;->d:[Ljava/lang/Object;

    aget-object v0, v2, v1

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 81318
    iget-object v0, p0, LX/0YU;->c:[I

    iget v1, p0, LX/0YU;->e:I

    invoke-static {v0, v1, p1}, LX/01M;->a([III)I

    move-result v0

    .line 81319
    if-ltz v0, :cond_0

    .line 81320
    iget-object v1, p0, LX/0YU;->d:[Ljava/lang/Object;

    aput-object p2, v1, v0

    .line 81321
    :goto_0
    return-void

    .line 81322
    :cond_0
    xor-int/lit8 v0, v0, -0x1

    .line 81323
    iget v1, p0, LX/0YU;->e:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, LX/0YU;->d:[Ljava/lang/Object;

    aget-object v1, v1, v0

    sget-object v2, LX/0YU;->a:Ljava/lang/Object;

    if-ne v1, v2, :cond_1

    .line 81324
    iget-object v1, p0, LX/0YU;->c:[I

    aput p1, v1, v0

    .line 81325
    iget-object v1, p0, LX/0YU;->d:[Ljava/lang/Object;

    aput-object p2, v1, v0

    goto :goto_0

    .line 81326
    :cond_1
    iget-boolean v1, p0, LX/0YU;->b:Z

    if-eqz v1, :cond_2

    iget v1, p0, LX/0YU;->e:I

    iget-object v2, p0, LX/0YU;->c:[I

    array-length v2, v2

    if-lt v1, v2, :cond_2

    .line 81327
    invoke-static {p0}, LX/0YU;->d(LX/0YU;)V

    .line 81328
    iget-object v0, p0, LX/0YU;->c:[I

    iget v1, p0, LX/0YU;->e:I

    invoke-static {v0, v1, p1}, LX/01M;->a([III)I

    move-result v0

    xor-int/lit8 v0, v0, -0x1

    .line 81329
    :cond_2
    iget v1, p0, LX/0YU;->e:I

    iget-object v2, p0, LX/0YU;->c:[I

    array-length v2, v2

    if-lt v1, v2, :cond_3

    .line 81330
    iget v1, p0, LX/0YU;->e:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, LX/01M;->a(I)I

    move-result v1

    .line 81331
    new-array v2, v1, [I

    .line 81332
    new-array v1, v1, [Ljava/lang/Object;

    .line 81333
    iget-object v3, p0, LX/0YU;->c:[I

    iget-object v4, p0, LX/0YU;->c:[I

    array-length v4, v4

    invoke-static {v3, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 81334
    iget-object v3, p0, LX/0YU;->d:[Ljava/lang/Object;

    iget-object v4, p0, LX/0YU;->d:[Ljava/lang/Object;

    array-length v4, v4

    invoke-static {v3, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 81335
    iput-object v2, p0, LX/0YU;->c:[I

    .line 81336
    iput-object v1, p0, LX/0YU;->d:[Ljava/lang/Object;

    .line 81337
    :cond_3
    iget v1, p0, LX/0YU;->e:I

    sub-int/2addr v1, v0

    if-eqz v1, :cond_4

    .line 81338
    iget-object v1, p0, LX/0YU;->c:[I

    iget-object v2, p0, LX/0YU;->c:[I

    add-int/lit8 v3, v0, 0x1

    iget v4, p0, LX/0YU;->e:I

    sub-int/2addr v4, v0

    invoke-static {v1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 81339
    iget-object v1, p0, LX/0YU;->d:[Ljava/lang/Object;

    iget-object v2, p0, LX/0YU;->d:[Ljava/lang/Object;

    add-int/lit8 v3, v0, 0x1

    iget v4, p0, LX/0YU;->e:I

    sub-int/2addr v4, v0

    invoke-static {v1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 81340
    :cond_4
    iget-object v1, p0, LX/0YU;->c:[I

    aput p1, v1, v0

    .line 81341
    iget-object v1, p0, LX/0YU;->d:[Ljava/lang/Object;

    aput-object p2, v1, v0

    .line 81342
    iget v0, p0, LX/0YU;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0YU;->e:I

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 81343
    iget v2, p0, LX/0YU;->e:I

    .line 81344
    iget-object v3, p0, LX/0YU;->d:[Ljava/lang/Object;

    move v0, v1

    .line 81345
    :goto_0
    if-ge v0, v2, :cond_0

    .line 81346
    const/4 v4, 0x0

    aput-object v4, v3, v0

    .line 81347
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81348
    :cond_0
    iput v1, p0, LX/0YU;->e:I

    .line 81349
    iput-boolean v1, p0, LX/0YU;->b:Z

    .line 81350
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 81312
    iget-object v0, p0, LX/0YU;->c:[I

    iget v1, p0, LX/0YU;->e:I

    invoke-static {v0, v1, p1}, LX/01M;->a([III)I

    move-result v0

    .line 81313
    if-ltz v0, :cond_0

    .line 81314
    iget-object v1, p0, LX/0YU;->d:[Ljava/lang/Object;

    aget-object v1, v1, v0

    sget-object v2, LX/0YU;->a:Ljava/lang/Object;

    if-eq v1, v2, :cond_0

    .line 81315
    iget-object v1, p0, LX/0YU;->d:[Ljava/lang/Object;

    sget-object v2, LX/0YU;->a:Ljava/lang/Object;

    aput-object v2, v1, v0

    .line 81316
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0YU;->b:Z

    .line 81317
    :cond_0
    return-void
.end method

.method public final b(ILjava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 81295
    iget v0, p0, LX/0YU;->e:I

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0YU;->c:[I

    iget v1, p0, LX/0YU;->e:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    if-gt p1, v0, :cond_0

    .line 81296
    invoke-virtual {p0, p1, p2}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 81297
    :goto_0
    return-void

    .line 81298
    :cond_0
    iget-boolean v0, p0, LX/0YU;->b:Z

    if-eqz v0, :cond_1

    iget v0, p0, LX/0YU;->e:I

    iget-object v1, p0, LX/0YU;->c:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 81299
    invoke-static {p0}, LX/0YU;->d(LX/0YU;)V

    .line 81300
    :cond_1
    iget v0, p0, LX/0YU;->e:I

    .line 81301
    iget-object v1, p0, LX/0YU;->c:[I

    array-length v1, v1

    if-lt v0, v1, :cond_2

    .line 81302
    add-int/lit8 v1, v0, 0x1

    invoke-static {v1}, LX/01M;->a(I)I

    move-result v1

    .line 81303
    new-array v2, v1, [I

    .line 81304
    new-array v1, v1, [Ljava/lang/Object;

    .line 81305
    iget-object v3, p0, LX/0YU;->c:[I

    iget-object v4, p0, LX/0YU;->c:[I

    array-length v4, v4

    invoke-static {v3, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 81306
    iget-object v3, p0, LX/0YU;->d:[Ljava/lang/Object;

    iget-object v4, p0, LX/0YU;->d:[Ljava/lang/Object;

    array-length v4, v4

    invoke-static {v3, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 81307
    iput-object v2, p0, LX/0YU;->c:[I

    .line 81308
    iput-object v1, p0, LX/0YU;->d:[Ljava/lang/Object;

    .line 81309
    :cond_2
    iget-object v1, p0, LX/0YU;->c:[I

    aput p1, v1, v0

    .line 81310
    iget-object v1, p0, LX/0YU;->d:[Ljava/lang/Object;

    aput-object p2, v1, v0

    .line 81311
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0YU;->e:I

    goto :goto_0
.end method

.method public final clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 81290
    const/4 v1, 0x0

    .line 81291
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0YU;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 81292
    :try_start_1
    iget-object v1, p0, LX/0YU;->c:[I

    invoke-virtual {v1}, [I->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    iput-object v1, v0, LX/0YU;->c:[I

    .line 81293
    iget-object v1, p0, LX/0YU;->d:[Ljava/lang/Object;

    invoke-virtual {v1}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    iput-object v1, v0, LX/0YU;->d:[Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 81294
    :goto_0
    return-object v0

    :catch_0
    move-object v0, v1

    goto :goto_0

    :catch_1
    goto :goto_0
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 81259
    iget-object v0, p0, LX/0YU;->d:[Ljava/lang/Object;

    aget-object v0, v0, p1

    sget-object v1, LX/0YU;->a:Ljava/lang/Object;

    if-eq v0, v1, :cond_0

    .line 81260
    iget-object v0, p0, LX/0YU;->d:[Ljava/lang/Object;

    sget-object v1, LX/0YU;->a:Ljava/lang/Object;

    aput-object v1, v0, p1

    .line 81261
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0YU;->b:Z

    .line 81262
    :cond_0
    return-void
.end method

.method public final e(I)I
    .locals 1

    .prologue
    .line 81287
    iget-boolean v0, p0, LX/0YU;->b:Z

    if-eqz v0, :cond_0

    .line 81288
    invoke-static {p0}, LX/0YU;->d(LX/0YU;)V

    .line 81289
    :cond_0
    iget-object v0, p0, LX/0YU;->c:[I

    aget v0, v0, p1

    return v0
.end method

.method public final f(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 81284
    iget-boolean v0, p0, LX/0YU;->b:Z

    if-eqz v0, :cond_0

    .line 81285
    invoke-static {p0}, LX/0YU;->d(LX/0YU;)V

    .line 81286
    :cond_0
    iget-object v0, p0, LX/0YU;->d:[Ljava/lang/Object;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final g(I)I
    .locals 2

    .prologue
    .line 81281
    iget-boolean v0, p0, LX/0YU;->b:Z

    if-eqz v0, :cond_0

    .line 81282
    invoke-static {p0}, LX/0YU;->d(LX/0YU;)V

    .line 81283
    :cond_0
    iget-object v0, p0, LX/0YU;->c:[I

    iget v1, p0, LX/0YU;->e:I

    invoke-static {v0, v1, p1}, LX/01M;->a([III)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 81263
    invoke-virtual {p0}, LX/0YU;->a()I

    move-result v0

    if-gtz v0, :cond_0

    .line 81264
    const-string v0, "{}"

    .line 81265
    :goto_0
    return-object v0

    .line 81266
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    iget v0, p0, LX/0YU;->e:I

    mul-int/lit8 v0, v0, 0x1c

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 81267
    const/16 v0, 0x7b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 81268
    const/4 v0, 0x0

    :goto_1
    iget v2, p0, LX/0YU;->e:I

    if-ge v0, v2, :cond_3

    .line 81269
    if-lez v0, :cond_1

    .line 81270
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81271
    :cond_1
    invoke-virtual {p0, v0}, LX/0YU;->e(I)I

    move-result v2

    .line 81272
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 81273
    const/16 v2, 0x3d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 81274
    invoke-virtual {p0, v0}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v2

    .line 81275
    if-eq v2, p0, :cond_2

    .line 81276
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 81277
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 81278
    :cond_2
    const-string v2, "(this Map)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 81279
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 81280
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
