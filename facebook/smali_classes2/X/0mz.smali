.class public abstract LX/0mz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 133499
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0lJ;Ljava/lang/Class;)LX/0lJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 133498
    invoke-virtual {p0}, LX/0mz;->a()LX/0m4;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/0m4;->a(LX/0lJ;Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/reflect/Type;)LX/0lJ;
    .locals 1

    .prologue
    .line 133497
    invoke-virtual {p0}, LX/0mz;->c()LX/0li;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0li;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v0

    return-object v0
.end method

.method public abstract a()LX/0m4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0m4",
            "<*>;"
        }
    .end annotation
.end method

.method public final a(LX/0lO;Ljava/lang/Object;)LX/1Xr;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            "Ljava/lang/Object;",
            ")",
            "LX/1Xr",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 133466
    if-nez p2, :cond_0

    move-object p2, v0

    .line 133467
    :goto_0
    return-object p2

    .line 133468
    :cond_0
    instance-of v1, p2, LX/1Xr;

    if-eqz v1, :cond_1

    .line 133469
    check-cast p2, LX/1Xr;

    goto :goto_0

    .line 133470
    :cond_1
    instance-of v1, p2, Ljava/lang/Class;

    if-nez v1, :cond_2

    .line 133471
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AnnotationIntrospector returned Converter definition of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; expected type Converter or Class<Converter> instead"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133472
    :cond_2
    check-cast p2, Ljava/lang/Class;

    .line 133473
    const-class v1, LX/1Xq;

    if-eq p2, v1, :cond_3

    const-class v1, LX/1Xp;

    if-ne p2, v1, :cond_4

    :cond_3
    move-object p2, v0

    .line 133474
    goto :goto_0

    .line 133475
    :cond_4
    const-class v1, LX/1Xr;

    invoke-virtual {v1, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 133476
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AnnotationIntrospector returned Class "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; expected Class<Converter>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133477
    :cond_5
    invoke-virtual {p0}, LX/0mz;->a()LX/0m4;

    move-result-object v1

    .line 133478
    invoke-virtual {v1}, LX/0m4;->l()LX/4py;

    move-result-object v2

    .line 133479
    if-nez v2, :cond_7

    .line 133480
    :goto_1
    if-nez v0, :cond_6

    .line 133481
    invoke-virtual {v1}, LX/0m4;->h()Z

    move-result v0

    invoke-static {p2, v0}, LX/1Xw;->b(Ljava/lang/Class;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Xr;

    :cond_6
    move-object p2, v0

    .line 133482
    goto :goto_0

    .line 133483
    :cond_7
    const/4 v0, 0x0

    move-object v0, v0

    .line 133484
    goto :goto_1
.end method

.method public final a(LX/0lO;LX/4qt;)LX/4pS;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            "LX/4qt;",
            ")",
            "LX/4pS",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 133487
    iget-object v0, p2, LX/4qt;->b:Ljava/lang/Class;

    move-object v1, v0

    .line 133488
    invoke-virtual {p0}, LX/0mz;->a()LX/0m4;

    move-result-object v2

    .line 133489
    invoke-virtual {v2}, LX/0m4;->l()LX/4py;

    move-result-object v0

    .line 133490
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 133491
    :goto_0
    if-nez v0, :cond_0

    .line 133492
    invoke-virtual {v2}, LX/0m4;->h()Z

    move-result v0

    invoke-static {v1, v0}, LX/1Xw;->b(Ljava/lang/Class;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4pS;

    .line 133493
    :cond_0
    iget-object v1, p2, LX/4qt;->c:Ljava/lang/Class;

    move-object v1, v1

    .line 133494
    invoke-virtual {v0, v1}, LX/4pS;->a(Ljava/lang/Class;)LX/4pS;

    move-result-object v0

    return-object v0

    .line 133495
    :cond_1
    const/4 v0, 0x0

    move-object v0, v0

    .line 133496
    goto :goto_0
.end method

.method public final a(LX/0m6;)Z
    .locals 1

    .prologue
    .line 133486
    invoke-virtual {p0}, LX/0mz;->a()LX/0m4;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0m4;->a(LX/0m6;)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 133485
    invoke-virtual {p0}, LX/0mz;->a()LX/0m4;

    move-result-object v0

    invoke-virtual {v0}, LX/0m4;->h()Z

    move-result v0

    return v0
.end method

.method public abstract c()LX/0li;
.end method
