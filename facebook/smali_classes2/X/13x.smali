.class public LX/13x;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/13x;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/140;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/140;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 177838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177839
    iput-object p1, p0, LX/13x;->a:LX/0Ot;

    .line 177840
    return-void
.end method

.method public static a(LX/0QB;)LX/13x;
    .locals 5

    .prologue
    .line 177822
    sget-object v0, LX/13x;->b:LX/13x;

    if-nez v0, :cond_1

    .line 177823
    const-class v1, LX/13x;

    monitor-enter v1

    .line 177824
    :try_start_0
    sget-object v0, LX/13x;->b:LX/13x;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 177825
    if-eqz v2, :cond_0

    .line 177826
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 177827
    new-instance v3, LX/13x;

    .line 177828
    new-instance v4, LX/13y;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-direct {v4, p0}, LX/13y;-><init>(LX/0QB;)V

    move-object v4, v4

    .line 177829
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-static {v4, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v4

    move-object v4, v4

    .line 177830
    invoke-direct {v3, v4}, LX/13x;-><init>(LX/0Ot;)V

    .line 177831
    move-object v0, v3

    .line 177832
    sput-object v0, LX/13x;->b:LX/13x;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177833
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 177834
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 177835
    :cond_1
    sget-object v0, LX/13x;->b:LX/13x;

    return-object v0

    .line 177836
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 177837
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 177797
    iget-object v0, p0, LX/13x;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 177798
    :cond_0
    return-object p1

    .line 177799
    :cond_1
    iget-object v0, p0, LX/13x;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/140;

    .line 177800
    invoke-virtual {v0}, LX/140;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 177801
    invoke-virtual {v0}, LX/140;->a()LX/2o6;

    move-result-object v0

    invoke-virtual {v0}, LX/2o6;->getCode()Ljava/lang/String;

    move-result-object v0

    .line 177802
    iget-object v2, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->k:LX/162;

    if-nez v2, :cond_3

    .line 177803
    new-instance v2, LX/162;

    sget-object p0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, p0}, LX/162;-><init>(LX/0mC;)V

    iput-object v2, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->k:LX/162;

    .line 177804
    :cond_3
    iget-object v2, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->k:LX/162;

    invoke-virtual {v2, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 177805
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 177812
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 177813
    const/4 v1, 0x1

    .line 177814
    iget-object v0, p0, LX/13x;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/140;

    .line 177815
    invoke-virtual {v0}, LX/140;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 177816
    if-nez v1, :cond_1

    .line 177817
    const-string v4, "-"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177818
    :goto_1
    invoke-virtual {v0}, LX/140;->a()LX/2o6;

    move-result-object v0

    invoke-virtual {v0}, LX/2o6;->getCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    move v0, v1

    move v1, v0

    .line 177819
    goto :goto_0

    .line 177820
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 177821
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0zL;)V
    .locals 3

    .prologue
    .line 177806
    iget-object v0, p0, LX/13x;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/140;

    .line 177807
    invoke-virtual {v0}, LX/140;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 177808
    invoke-virtual {v0}, LX/140;->a()LX/2o6;

    move-result-object v0

    invoke-virtual {v0}, LX/2o6;->getCode()Ljava/lang/String;

    move-result-object v0

    .line 177809
    invoke-static {p1, v0}, LX/0zL;->a(LX/0zL;Ljava/lang/Object;)V

    .line 177810
    goto :goto_0

    .line 177811
    :cond_1
    return-void
.end method
