.class public LX/1qq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qs;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 331500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331501
    iput-object p1, p0, LX/1qq;->a:LX/0Or;

    .line 331502
    return-void
.end method


# virtual methods
.method public final a()LX/0yY;
    .locals 1

    .prologue
    .line 331499
    sget-object v0, LX/0yY;->LOCATION_SERVICES_INTERSTITIAL:LX/0yY;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 331503
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 331504
    iget-object v2, p0, LX/1qq;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 331505
    invoke-static {p1}, LX/Ehu;->b(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 331506
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 331507
    :cond_0
    :goto_0
    return v0

    .line 331508
    :cond_1
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v1, LX/0ax;->cc:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, LX/1H1;->a(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v0

    goto :goto_0
.end method
