.class public LX/0hh;
.super LX/0hi;
.source ""

# interfaces
.implements LX/0hj;
.implements LX/0hk;
.implements LX/0hl;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0xI;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0xH;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0xG;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0xF;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0xE;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0xD;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iD;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iG;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0jP;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0x6;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0x5;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hm;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0x4;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0xI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0xH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0xG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0xF;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0xE;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0xD;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0iD;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0iG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0jP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0x6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0x5;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0hm;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0x4;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 118306
    invoke-direct {p0}, LX/0hi;-><init>()V

    .line 118307
    iput-object p1, p0, LX/0hh;->a:LX/0Ot;

    .line 118308
    iput-object p2, p0, LX/0hh;->b:LX/0Ot;

    .line 118309
    iput-object p3, p0, LX/0hh;->c:LX/0Ot;

    .line 118310
    iput-object p4, p0, LX/0hh;->d:LX/0Ot;

    .line 118311
    iput-object p5, p0, LX/0hh;->e:LX/0Ot;

    .line 118312
    iput-object p6, p0, LX/0hh;->f:LX/0Ot;

    .line 118313
    iput-object p7, p0, LX/0hh;->g:LX/0Ot;

    .line 118314
    iput-object p8, p0, LX/0hh;->h:LX/0Ot;

    .line 118315
    iput-object p9, p0, LX/0hh;->i:LX/0Ot;

    .line 118316
    iput-object p10, p0, LX/0hh;->j:LX/0Ot;

    .line 118317
    iput-object p11, p0, LX/0hh;->k:LX/0Ot;

    .line 118318
    iput-object p12, p0, LX/0hh;->l:LX/0Ot;

    .line 118319
    iput-object p13, p0, LX/0hh;->m:LX/0Ot;

    .line 118320
    return-void
.end method

.method public static b(LX/0QB;)LX/0hh;
    .locals 14

    .prologue
    .line 118304
    new-instance v0, LX/0hh;

    const/16 v1, 0xc04

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0xc14

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0xc15

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0xc06

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xc07

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xc0d

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xc0c

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xc0f

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xc08

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xc05

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xc0a

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0xc13

    invoke-static {p0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0xc0e

    invoke-static {p0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v0 .. v13}, LX/0hh;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 118305
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 118289
    iput-object v0, p0, LX/0hh;->a:LX/0Ot;

    .line 118290
    iput-object v0, p0, LX/0hh;->b:LX/0Ot;

    .line 118291
    iput-object v0, p0, LX/0hh;->c:LX/0Ot;

    .line 118292
    iput-object v0, p0, LX/0hh;->d:LX/0Ot;

    .line 118293
    iput-object v0, p0, LX/0hh;->e:LX/0Ot;

    .line 118294
    iput-object v0, p0, LX/0hh;->f:LX/0Ot;

    .line 118295
    iput-object v0, p0, LX/0hh;->g:LX/0Ot;

    .line 118296
    iput-object v0, p0, LX/0hh;->h:LX/0Ot;

    .line 118297
    iput-object v0, p0, LX/0hh;->i:LX/0Ot;

    .line 118298
    iput-object v0, p0, LX/0hh;->j:LX/0Ot;

    .line 118299
    iput-object v0, p0, LX/0hh;->k:LX/0Ot;

    .line 118300
    iput-object v0, p0, LX/0hh;->l:LX/0Ot;

    .line 118301
    iput-object v0, p0, LX/0hh;->m:LX/0Ot;

    .line 118302
    invoke-super {p0}, LX/0hi;->a()V

    .line 118303
    return-void
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 118286
    iget-object v0, p0, LX/0hh;->e:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hh;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hh;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xE;

    invoke-virtual {v0}, LX/0hD;->kw_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118287
    iget-object v0, p0, LX/0hh;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xE;

    invoke-virtual {v0, p1, p2, p3}, LX/0xE;->a(IILandroid/content/Intent;)V

    .line 118288
    :cond_0
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 6
    .annotation build Lcom/facebook/controllercallbacks/api/SetUp;
    .end annotation

    .annotation build Lcom/facebook/controllercallbacks/api/TraceMethod;
    .end annotation

    .prologue
    .line 118150
    :try_start_0
    const-string v0, "FbMainTabFragmentControllerCallbacksDispatcher.onActivityCreated"

    const v1, -0x4309a2ad

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118151
    iget-object v0, p0, LX/0hh;->a:LX/0Ot;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0hh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0hh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xI;

    invoke-virtual {v0}, LX/0xI;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    .line 118152
    :try_start_1
    const-string v0, "FbMainTabFragmentAtWorkController"

    const v1, 0x6ce1ebc7

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118153
    iget-object v0, p0, LX/0hh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xI;

    .line 118154
    iput-object p1, v0, LX/0xI;->f:Landroid/app/Activity;

    .line 118155
    iget-object v1, v0, LX/0xI;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v2, 0x0

    .line 118156
    sget-object v3, LX/Hgv;->c:LX/0Tn;

    invoke-interface {v1, v3, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v1, v2

    .line 118157
    if-nez v1, :cond_10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118158
    :goto_0
    const v0, -0x39eb2764

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118159
    :cond_1
    iget-object v0, p0, LX/0hh;->b:LX/0Ot;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/0hh;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/0hh;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xH;

    invoke-virtual {v0}, LX/0xH;->a()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    if-eqz v0, :cond_3

    .line 118160
    :try_start_3
    const-string v0, "FbMainTabFragmentVideoPlayerServiceController"

    const v1, -0x7123c7f5

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118161
    iget-object v0, p0, LX/0hh;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xH;

    const/4 v3, 0x0

    .line 118162
    iget-object v1, v0, LX/0xH;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    sget-short v2, LX/0ws;->fR:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 118163
    iget-object v1, v0, LX/0xH;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Sg;

    const-string v2, "Pre-init VPS"

    new-instance v3, Lcom/facebook/katana/fragment/maintab/controllercallbacks/FbMainTabFragmentVideoPlayerServiceController$1;

    invoke-direct {v3, v0, p1}, Lcom/facebook/katana/fragment/maintab/controllercallbacks/FbMainTabFragmentVideoPlayerServiceController$1;-><init>(LX/0xH;Landroid/app/Activity;)V

    sget-object v4, LX/0VZ;->APPLICATION_LOADED_UI_IDLE_HIGH_PRIORITY:LX/0VZ;

    sget-object v5, LX/0Vm;->BACKGROUND:LX/0Vm;

    invoke-virtual {v1, v2, v3, v4, v5}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 118164
    :cond_2
    :goto_1
    const v0, 0x346e5dce

    :try_start_4
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118165
    :cond_3
    iget-object v0, p0, LX/0hh;->c:LX/0Ot;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/0hh;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/0hh;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xG;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v0

    if-eqz v0, :cond_4

    .line 118166
    :try_start_5
    const-string v0, "FbMainTabFragmentZeroRatingController"

    const v1, -0x2ff4ffd2    # -9.3281792E9f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118167
    iget-object v0, p0, LX/0hh;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xG;

    .line 118168
    const v1, 0x7f0d1a6b

    invoke-static {p1, v1}, LX/0jc;->a(Landroid/app/Activity;I)LX/0am;

    move-result-object v1

    .line 118169
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v2

    if-nez v2, :cond_12
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 118170
    :goto_2
    const v0, 0x4601f604

    :try_start_6
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118171
    :cond_4
    iget-object v0, p0, LX/0hh;->d:LX/0Ot;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/0hh;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/0hh;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xF;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result v0

    if-eqz v0, :cond_5

    .line 118172
    :try_start_7
    const-string v0, "FbMainTabFragmentBroadcastReceiverController"

    const v1, 0x17c632fe

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118173
    iget-object v0, p0, LX/0hh;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xF;

    .line 118174
    iget-object v1, v0, LX/0xF;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/12Y;

    .line 118175
    iget-object v2, v1, LX/12Y;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/12Z;

    .line 118176
    move-object v2, v2

    .line 118177
    iget-object v1, v0, LX/0xF;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Xl;

    .line 118178
    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v3

    .line 118179
    invoke-virtual {v2, p1, v3}, LX/12Z;->a(Landroid/app/Activity;LX/0YX;)V

    .line 118180
    invoke-interface {v3}, LX/0YX;->a()LX/0Yb;

    move-result-object v3

    iput-object v3, v0, LX/0xF;->d:LX/0Yb;

    .line 118181
    iget-object v3, v0, LX/0xF;->d:LX/0Yb;

    invoke-virtual {v3}, LX/0Yb;->b()V

    .line 118182
    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v3

    .line 118183
    invoke-virtual {v2, p1, v3}, LX/12Z;->b(Landroid/app/Activity;LX/0YX;)V

    .line 118184
    iget-object v1, v0, LX/0xF;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    invoke-interface {v3, v1}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    iput-object v1, v0, LX/0xF;->e:LX/0Yb;

    .line 118185
    iget-object v1, v0, LX/0xF;->e:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 118186
    const v0, -0x12140795

    :try_start_8
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118187
    :cond_5
    iget-object v0, p0, LX/0hh;->e:LX/0Ot;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/0hh;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/0hh;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xE;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v0

    if-eqz v0, :cond_6

    .line 118188
    :try_start_9
    const-string v0, "FbMainTabFragmentCheckpointController"

    const v1, -0x6c04c625

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118189
    iget-object v0, p0, LX/0hh;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xE;

    .line 118190
    iget-object v1, v0, LX/0xE;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Xl;

    .line 118191
    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    .line 118192
    const-string v2, "com.facebook.checkpoint.USER_IN_CHECKPOINT"

    new-instance v3, LX/12h;

    invoke-direct {v3, v0, p1}, LX/12h;-><init>(LX/0xE;Landroid/app/Activity;)V

    invoke-interface {v1, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.checkpoint.USER_IN_CHECKPOINT_RN"

    new-instance v4, LX/12i;

    invoke-direct {v4, v0, p1}, LX/12i;-><init>(LX/0xE;Landroid/app/Activity;)V

    invoke-interface {v2, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    .line 118193
    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    iput-object v1, v0, LX/0xE;->d:LX/0Yb;

    .line 118194
    iget-object v1, v0, LX/0xE;->d:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    .line 118195
    const v0, -0x4ac159be

    :try_start_a
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118196
    :cond_6
    iget-object v0, p0, LX/0hh;->f:LX/0Ot;

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/0hh;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/0hh;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xD;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result v0

    if-eqz v0, :cond_7

    .line 118197
    :try_start_b
    const-string v0, "FbMainTabFragmentNotificationTabImageController"

    const v1, -0x51883b3d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118198
    iget-object v0, p0, LX/0hh;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xD;

    invoke-virtual {v0, p1}, LX/0xD;->a(Landroid/app/Activity;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_6

    .line 118199
    const v0, 0x7f8deb67

    :try_start_c
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118200
    :cond_7
    iget-object v0, p0, LX/0hh;->g:LX/0Ot;

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/0hh;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/0hh;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iD;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    move-result v0

    if-eqz v0, :cond_8

    .line 118201
    :try_start_d
    const-string v0, "FbMainTabFragmentJewelController"

    const v1, -0xa37352c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118202
    iget-object v0, p0, LX/0hh;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iD;

    invoke-virtual {v0, p1}, LX/0iD;->a(Landroid/app/Activity;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_7

    .line 118203
    const v0, -0x298a11b4

    :try_start_e
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118204
    :cond_8
    iget-object v0, p0, LX/0hh;->h:LX/0Ot;

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/0hh;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/0hh;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iG;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    move-result v0

    if-eqz v0, :cond_9

    .line 118205
    :try_start_f
    const-string v0, "FbMainTabFragmentSearchAwarenessController"

    const v1, -0x11ce571

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118206
    iget-object v0, p0, LX/0hh;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iG;

    invoke-virtual {v0, p1}, LX/0iG;->a(Landroid/app/Activity;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_8

    .line 118207
    const v0, 0x67ec8971

    :try_start_10
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118208
    :cond_9
    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    if-eqz v0, :cond_b

    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jP;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    move-result v0

    if-eqz v0, :cond_b

    .line 118209
    :try_start_11
    const-string v0, "FbMainTabFragmentDataSensitivityController"

    const v1, -0x24cc0e9a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118210
    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jP;

    .line 118211
    iget-object v1, v0, LX/0jP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0tK;

    invoke-virtual {v1}, LX/0tK;->m()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 118212
    const v1, 0x7f0d1a6b

    invoke-static {p1, v1}, LX/0jc;->a(Landroid/app/Activity;I)LX/0am;

    move-result-object v1

    .line 118213
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v2

    if-nez v2, :cond_14

    .line 118214
    :cond_a
    :goto_3
    instance-of v1, p1, LX/0f7;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    move-object v1, p1

    .line 118215
    check-cast v1, LX/0f7;

    iput-object v1, v0, LX/0jP;->i:LX/0f7;

    .line 118216
    iput-object p1, v0, LX/0jP;->j:Landroid/app/Activity;

    .line 118217
    const v1, 0x7f0d1a6b

    invoke-static {p1, v1}, LX/0jc;->a(Landroid/app/Activity;I)LX/0am;

    move-result-object v1

    .line 118218
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v2

    if-nez v2, :cond_13
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_9

    .line 118219
    :goto_4
    const v0, 0x5fefceb8

    :try_start_12
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118220
    :cond_b
    iget-object v0, p0, LX/0hh;->j:LX/0Ot;

    if-eqz v0, :cond_c

    iget-object v0, p0, LX/0hh;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v0, p0, LX/0hh;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x6;

    invoke-virtual {v0}, LX/0x6;->a()Z
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    move-result v0

    if-eqz v0, :cond_c

    .line 118221
    :try_start_13
    const-string v0, "FbMainTabFragmentAudioConfiguratorController"

    const v1, -0x743da4eb

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118222
    iget-object v0, p0, LX/0hh;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x6;

    .line 118223
    invoke-static {v0}, LX/0x6;->c(LX/0x6;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_a

    .line 118224
    const v0, -0x41348f69

    :try_start_14
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118225
    :cond_c
    iget-object v0, p0, LX/0hh;->k:LX/0Ot;

    if-eqz v0, :cond_d

    iget-object v0, p0, LX/0hh;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_d

    iget-object v0, p0, LX/0hh;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x5;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    move-result v0

    if-eqz v0, :cond_d

    .line 118226
    :try_start_15
    const-string v0, "FbMainTabFragmentFragmentStackInfoController"

    const v1, 0x75ea7991

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118227
    iget-object v0, p0, LX/0hh;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x5;

    invoke-virtual {v0, p1}, LX/0x5;->a(Landroid/app/Activity;)V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_b

    .line 118228
    const v0, -0x2109dbbe

    :try_start_16
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118229
    :cond_d
    iget-object v0, p0, LX/0hh;->l:LX/0Ot;

    if-eqz v0, :cond_e

    iget-object v0, p0, LX/0hh;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_e

    iget-object v0, p0, LX/0hh;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hm;

    invoke-virtual {v0}, LX/0hm;->a()Z
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    move-result v0

    if-eqz v0, :cond_e

    .line 118230
    :try_start_17
    const-string v0, "FbMainTabFragmentVideoHomeController"

    const v1, 0x5aef9a54

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118231
    iget-object v0, p0, LX/0hh;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hm;

    invoke-virtual {v0, p1}, LX/0hm;->a(Landroid/app/Activity;)V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_c

    .line 118232
    const v0, 0x6ed75727

    :try_start_18
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118233
    :cond_e
    iget-object v0, p0, LX/0hh;->m:LX/0Ot;

    if-eqz v0, :cond_f

    iget-object v0, p0, LX/0hh;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_f

    iget-object v0, p0, LX/0hh;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x4;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    move-result v0

    if-eqz v0, :cond_f

    .line 118234
    :try_start_19
    const-string v0, "FbMainTabFragmentSavedBookmarkNuxController"

    const v1, -0x2e144d28

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118235
    iget-object v0, p0, LX/0hh;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x4;

    .line 118236
    instance-of v1, p1, LX/0f7;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 118237
    check-cast p1, LX/0f7;

    iput-object p1, v0, LX/0x4;->c:LX/0f7;
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_d

    .line 118238
    const v0, 0x56233275

    :try_start_1a
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_1

    .line 118239
    :cond_f
    const v0, 0x36bbc6be

    invoke-static {v0}, LX/02m;->a(I)V

    .line 118240
    return-void

    .line 118241
    :catchall_0
    move-exception v0

    const v1, -0x28e4a9f1

    :try_start_1b
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_1

    .line 118242
    :catchall_1
    move-exception v0

    const v1, 0x267c2f50

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118243
    :catchall_2
    move-exception v0

    const v1, -0x3d27419

    :try_start_1c
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118244
    :catchall_3
    move-exception v0

    const v1, 0x25555428

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118245
    :catchall_4
    move-exception v0

    const v1, -0x9228cc5

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118246
    :catchall_5
    move-exception v0

    const v1, -0x2b882863

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118247
    :catchall_6
    move-exception v0

    const v1, -0x30b8fc94

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118248
    :catchall_7
    move-exception v0

    const v1, -0x15912dc3

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118249
    :catchall_8
    move-exception v0

    const v1, -0x2fc6bbd2

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118250
    :catchall_9
    move-exception v0

    const v1, -0x3c7b8865

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118251
    :catchall_a
    move-exception v0

    const v1, 0x3873b18f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118252
    :catchall_b
    move-exception v0

    const v1, -0x32015ff9

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118253
    :catchall_c
    move-exception v0

    const v1, -0x14c721fe

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118254
    :catchall_d
    move-exception v0

    const v1, 0xcfda56

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1

    .line 118255
    :cond_10
    :try_start_1d
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v1, v0, LX/0xI;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ComponentName;

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    .line 118256
    const/high16 v1, 0x4000000

    invoke-virtual {v2, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 118257
    iget-object v1, v0, LX/0xI;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 118258
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    .line 118259
    :cond_11
    :try_start_1e
    iget-object v1, v0, LX/0xH;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    sget-short v2, LX/0ws;->fS:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_2

    .line 118260
    invoke-static {v0, p1}, LX/0xH;->c(LX/0xH;Landroid/app/Activity;)V

    goto/16 :goto_1
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_2

    .line 118261
    :cond_12
    :try_start_1f
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/CustomViewPager;

    iput-object v1, v0, LX/0xG;->f:Lcom/facebook/widget/CustomViewPager;

    .line 118262
    iget-object v1, v0, LX/0xG;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11u;

    const v2, 0x7f0d16f9

    invoke-static {p1, v2}, LX/0jc;->b(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    .line 118263
    iput-object v2, v1, LX/11u;->e:Landroid/view/ViewStub;

    .line 118264
    const v1, 0x7f0d1a6d

    invoke-static {p1, v1}, LX/0jc;->b(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, LX/0xG;->g:Landroid/view/View;

    .line 118265
    iget-object v1, v0, LX/0xG;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11v;

    iget-object v2, v0, LX/0xG;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/11u;

    .line 118266
    iget-object v3, v2, LX/11u;->a:LX/12P;

    move-object v2, v3

    .line 118267
    iput-object v2, v1, LX/11v;->o:LX/12Q;

    .line 118268
    new-instance v1, LX/12W;

    invoke-direct {v1, v0, p1}, LX/12W;-><init>(LX/0xG;Landroid/app/Activity;)V

    iput-object v1, v0, LX/0xG;->h:LX/12S;

    .line 118269
    iget-object v1, v0, LX/0xG;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/120;

    const v2, 0x7f0d0c8b

    invoke-static {p1, v2}, LX/0jc;->b(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    check-cast v2, Landroid/view/ViewStub;

    invoke-virtual {v1, v2}, LX/120;->a(Landroid/view/ViewStub;)V

    .line 118270
    new-instance v1, LX/12X;

    invoke-direct {v1, v0, p1}, LX/12X;-><init>(LX/0xG;Landroid/app/Activity;)V

    iput-object v1, v0, LX/0xG;->i:LX/12I;

    goto/16 :goto_2
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_3

    .line 118271
    :cond_13
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/CustomViewPager;

    iput-object v1, v0, LX/0jP;->k:Lcom/facebook/widget/CustomViewPager;

    .line 118272
    iget-object v1, v0, LX/0jP;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/13a;

    const v2, 0x7f0d1a70

    invoke-static {p1, v2}, LX/0jc;->b(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    .line 118273
    iput-object v2, v1, LX/13a;->e:Landroid/view/ViewStub;

    .line 118274
    iget-object v1, v0, LX/0jP;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/13a;

    new-instance v2, LX/13c;

    invoke-direct {v2, v0}, LX/13c;-><init>(LX/0jP;)V

    .line 118275
    iput-object v2, v1, LX/13a;->g:LX/13c;

    .line 118276
    goto/16 :goto_4

    .line 118277
    :cond_14
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/CustomViewPager;

    iput-object v1, v0, LX/0jP;->k:Lcom/facebook/widget/CustomViewPager;

    .line 118278
    const v1, 0x7f0d1a72

    invoke-static {p1, v1}, LX/0jc;->b(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    iput-object v1, v0, LX/0jP;->m:Landroid/view/ViewStub;

    .line 118279
    iget-object v1, v0, LX/0jP;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0x8;

    iget-object v2, v0, LX/0jP;->m:Landroid/view/ViewStub;

    invoke-virtual {v1, v2}, LX/0x8;->a(Landroid/view/ViewStub;)V

    .line 118280
    const v1, 0x7f0d1a6d

    invoke-static {p1, v1}, LX/0jc;->b(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, LX/0jP;->l:Landroid/view/View;

    .line 118281
    iget-object v1, v0, LX/0jP;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0x8;

    invoke-virtual {v1, p1}, LX/0x8;->a(Landroid/app/Activity;)I

    move-result v1

    invoke-static {v0, v1}, LX/0jP;->a$redex0(LX/0jP;I)V

    .line 118282
    iget-object v1, v0, LX/0jP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0tK;

    iget-object v2, v0, LX/0jP;->n:LX/0x7;

    invoke-virtual {v1, v2}, LX/0tK;->a(LX/0x7;)V

    .line 118283
    iget-object v1, v0, LX/0jP;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0x8;

    .line 118284
    iput-object p1, v1, LX/0x8;->c:Landroid/app/Activity;

    .line 118285
    goto/16 :goto_3
.end method

.method public final c()V
    .locals 2
    .annotation build Lcom/facebook/controllercallbacks/api/SetUp;
    .end annotation

    .annotation build Lcom/facebook/controllercallbacks/api/TraceMethod;
    .end annotation

    .prologue
    .line 118111
    :try_start_0
    const-string v0, "FbMainTabFragmentControllerCallbacksDispatcher.onResume"

    const v1, -0x63b17c7e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118112
    iget-object v0, p0, LX/0hh;->a:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xI;

    invoke-virtual {v0}, LX/0xI;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_0

    .line 118113
    :try_start_1
    const-string v0, "FbMainTabFragmentAtWorkController"

    const v1, -0x59db1898

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118114
    iget-object v0, p0, LX/0hh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xI;

    invoke-virtual {v0}, LX/0xI;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118115
    const v0, -0x83c1afb

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118116
    :cond_0
    iget-object v0, p0, LX/0hh;->c:LX/0Ot;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0hh;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0hh;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xG;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    .line 118117
    :try_start_3
    const-string v0, "FbMainTabFragmentZeroRatingController"

    const v1, -0x293e8231

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118118
    iget-object v0, p0, LX/0hh;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xG;

    invoke-virtual {v0}, LX/0xG;->c()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 118119
    const v0, -0x6fd97ee1

    :try_start_4
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118120
    :cond_1
    iget-object v0, p0, LX/0hh;->h:LX/0Ot;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0hh;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0hh;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iG;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v0

    if-eqz v0, :cond_2

    .line 118121
    :try_start_5
    const-string v0, "FbMainTabFragmentSearchAwarenessController"

    const v1, 0x39b36d31

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118122
    iget-object v0, p0, LX/0hh;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iG;

    invoke-virtual {v0}, LX/0iG;->c()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 118123
    const v0, -0x7dfac65d

    :try_start_6
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118124
    :cond_2
    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jP;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result v0

    if-eqz v0, :cond_3

    .line 118125
    :try_start_7
    const-string v0, "FbMainTabFragmentDataSensitivityController"

    const v1, 0x5a7bf62

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118126
    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jP;

    invoke-virtual {v0}, LX/0jP;->c()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 118127
    const v0, 0x1f0a7139

    :try_start_8
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118128
    :cond_3
    iget-object v0, p0, LX/0hh;->k:LX/0Ot;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/0hh;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/0hh;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x5;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v0

    if-eqz v0, :cond_4

    .line 118129
    :try_start_9
    const-string v0, "FbMainTabFragmentFragmentStackInfoController"

    const v1, 0x58f60dd4

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118130
    iget-object v0, p0, LX/0hh;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x5;

    invoke-virtual {v0}, LX/0x5;->c()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    .line 118131
    const v0, 0x37f25da9

    :try_start_a
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118132
    :cond_4
    iget-object v0, p0, LX/0hh;->l:LX/0Ot;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/0hh;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/0hh;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hm;

    invoke-virtual {v0}, LX/0hm;->a()Z
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result v0

    if-eqz v0, :cond_5

    .line 118133
    :try_start_b
    const-string v0, "FbMainTabFragmentVideoHomeController"

    const v1, -0x35b63b20    # -3305784.0f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118134
    iget-object v0, p0, LX/0hh;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hm;

    invoke-virtual {v0}, LX/0hm;->c()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_6

    .line 118135
    const v0, 0x4bebe60

    :try_start_c
    invoke-static {v0}, LX/02m;->a(I)V

    .line 118136
    :cond_5
    iget-object v0, p0, LX/0hh;->m:LX/0Ot;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/0hh;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/0hh;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x4;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    move-result v0

    if-eqz v0, :cond_6

    .line 118137
    :try_start_d
    const-string v0, "FbMainTabFragmentSavedBookmarkNuxController"

    const v1, -0x3915aa60    # -29994.812f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118138
    iget-object v0, p0, LX/0hh;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x4;

    invoke-virtual {v0}, LX/0x4;->c()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_7

    .line 118139
    const v0, 0x516d581e

    :try_start_e
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 118140
    :cond_6
    const v0, 0x74647349

    invoke-static {v0}, LX/02m;->a(I)V

    .line 118141
    return-void

    .line 118142
    :catchall_0
    move-exception v0

    const v1, -0xde1246b

    :try_start_f
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 118143
    :catchall_1
    move-exception v0

    const v1, 0x1857a316

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118144
    :catchall_2
    move-exception v0

    const v1, 0x24dd5d9a

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118145
    :catchall_3
    move-exception v0

    const v1, -0x4d973507

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118146
    :catchall_4
    move-exception v0

    const v1, 0x69c82444

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118147
    :catchall_5
    move-exception v0

    const v1, -0x11ec08a0

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118148
    :catchall_6
    move-exception v0

    const v1, -0x3fe04c3e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 118149
    :catchall_7
    move-exception v0

    const v1, -0x64d3a208

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final d()V
    .locals 1
    .annotation build Lcom/facebook/controllercallbacks/api/TearDown;
    .end annotation

    .prologue
    .line 118096
    iget-object v0, p0, LX/0hh;->m:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hh;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hh;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x4;

    invoke-virtual {v0}, LX/0hD;->kw_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118097
    iget-object v0, p0, LX/0hh;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x4;

    invoke-virtual {v0}, LX/0x4;->d()V

    .line 118098
    :cond_0
    iget-object v0, p0, LX/0hh;->l:LX/0Ot;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0hh;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0hh;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hm;

    invoke-virtual {v0}, LX/0hm;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118099
    iget-object v0, p0, LX/0hh;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hm;

    invoke-virtual {v0}, LX/0hm;->d()V

    .line 118100
    :cond_1
    iget-object v0, p0, LX/0hh;->k:LX/0Ot;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0hh;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0hh;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x5;

    invoke-virtual {v0}, LX/0hD;->kw_()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 118101
    iget-object v0, p0, LX/0hh;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x5;

    invoke-virtual {v0}, LX/0x5;->d()V

    .line 118102
    :cond_2
    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jP;

    invoke-virtual {v0}, LX/0hD;->kw_()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 118103
    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jP;

    invoke-virtual {v0}, LX/0jP;->d()V

    .line 118104
    :cond_3
    iget-object v0, p0, LX/0hh;->h:LX/0Ot;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/0hh;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/0hh;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iG;

    invoke-virtual {v0}, LX/0hD;->kw_()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 118105
    iget-object v0, p0, LX/0hh;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iG;

    invoke-virtual {v0}, LX/0iG;->d()V

    .line 118106
    :cond_4
    iget-object v0, p0, LX/0hh;->c:LX/0Ot;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/0hh;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/0hh;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xG;

    invoke-virtual {v0}, LX/0hD;->kw_()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 118107
    iget-object v0, p0, LX/0hh;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xG;

    invoke-virtual {v0}, LX/0xG;->d()V

    .line 118108
    :cond_5
    iget-object v0, p0, LX/0hh;->a:LX/0Ot;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/0hh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/0hh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xI;

    invoke-virtual {v0}, LX/0xI;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 118109
    iget-object v0, p0, LX/0hh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xI;

    invoke-virtual {v0}, LX/0xI;->d()V

    .line 118110
    :cond_6
    return-void
.end method

.method public final e()V
    .locals 2
    .annotation build Lcom/facebook/controllercallbacks/api/SetUp;
    .end annotation

    .annotation build Lcom/facebook/controllercallbacks/api/TraceMethod;
    .end annotation

    .prologue
    .line 118087
    :try_start_0
    const-string v0, "FbMainTabFragmentControllerCallbacksDispatcher.onStart"

    const v1, 0x19feabbf

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118088
    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jP;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_0

    .line 118089
    :try_start_1
    const-string v0, "FbMainTabFragmentDataSensitivityController"

    const v1, 0x4330b581    # 176.709f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 118090
    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jP;

    invoke-virtual {v0}, LX/0jP;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118091
    const v0, -0x38c84f2a

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 118092
    :cond_0
    const v0, -0x28b8aa33

    invoke-static {v0}, LX/02m;->a(I)V

    .line 118093
    return-void

    .line 118094
    :catchall_0
    move-exception v0

    const v1, -0x15b0c3cb

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 118095
    :catchall_1
    move-exception v0

    const v1, -0x4476a423

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final f()V
    .locals 1
    .annotation build Lcom/facebook/controllercallbacks/api/TearDown;
    .end annotation

    .prologue
    .line 118084
    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jP;

    invoke-virtual {v0}, LX/0hD;->kw_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118085
    iget-object v0, p0, LX/0hh;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jP;

    invoke-virtual {v0}, LX/0jP;->f()V

    .line 118086
    :cond_0
    return-void
.end method
