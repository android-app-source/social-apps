.class public LX/0Yl;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final c:LX/0So;

.field public d:Z

.field public e:Z

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82306
    const-class v0, LX/0Yl;

    sput-object v0, LX/0Yl;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;LX/0So;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 82323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82324
    iput-object p1, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 82325
    iput-object p2, p0, LX/0Yl;->c:LX/0So;

    .line 82326
    iput-boolean v0, p0, LX/0Yl;->d:Z

    .line 82327
    iput-boolean v0, p0, LX/0Yl;->e:Z

    .line 82328
    iput-boolean v0, p0, LX/0Yl;->f:Z

    .line 82329
    return-void
.end method

.method public static a(LX/0QB;)LX/0Yl;
    .locals 1

    .prologue
    .line 82330
    invoke-static {p0}, LX/0Yl;->b(LX/0QB;)LX/0Yl;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/0Yl;ILjava/lang/String;Ljava/lang/String;LX/0P1;JZ)LX/0Yl;
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;JZ)",
            "LX/0Yl;"
        }
    .end annotation

    .prologue
    .line 82331
    invoke-direct {p0}, LX/0Yl;->i()Z

    move-result v0

    if-nez v0, :cond_1

    .line 82332
    const/4 v8, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    invoke-direct/range {v1 .. v8}, LX/0Yl;->b(ILjava/lang/String;Ljava/lang/String;LX/0P1;JLjava/lang/Boolean;)V

    .line 82333
    :cond_0
    :goto_0
    return-object p0

    .line 82334
    :cond_1
    if-eqz p7, :cond_0

    .line 82335
    invoke-direct {p0, p1, p2}, LX/0Yl;->n(ILjava/lang/String;)LX/0Yl;

    goto :goto_0
.end method

.method private static a(LX/0Yl;ILjava/lang/String;Ljava/lang/String;LX/0P1;JDZ)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;JDZ)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 82336
    new-instance v0, LX/0Yj;

    invoke-direct {v0, p1, p2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    .line 82337
    iput-object p3, v0, LX/0Yj;->e:Ljava/lang/String;

    .line 82338
    move-object v0, v0

    .line 82339
    iput-wide p5, v0, LX/0Yj;->g:J

    .line 82340
    move-object v0, v0

    .line 82341
    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "native_newsfeed"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v0

    .line 82342
    if-eqz p9, :cond_0

    .line 82343
    new-instance v1, LX/00q;

    invoke-direct {v1}, LX/00q;-><init>()V

    .line 82344
    iput-object v1, v0, LX/0Yj;->s:LX/00q;

    .line 82345
    :cond_0
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v1, p7, v2

    if-eqz v1, :cond_1

    .line 82346
    invoke-virtual {v0, p7, p8}, LX/0Yj;->a(D)LX/0Yj;

    .line 82347
    :cond_1
    if-eqz p4, :cond_2

    .line 82348
    iput-object p4, v0, LX/0Yj;->l:Ljava/util/Map;

    .line 82349
    :cond_2
    iget-object v1, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v1, v0, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->a(LX/0Yj;Z)V

    .line 82350
    return-void
.end method

.method public static b(LX/0QB;)LX/0Yl;
    .locals 3

    .prologue
    .line 82351
    new-instance v2, LX/0Yl;

    invoke-static {p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-direct {v2, v0, v1}, LX/0Yl;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;LX/0So;)V

    .line 82352
    return-object v2
.end method

.method private static b(LX/0Yl;ILjava/lang/String;Ljava/lang/String;LX/0P1;JZ)LX/0Yl;
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;JZ)",
            "LX/0Yl;"
        }
    .end annotation

    .prologue
    .line 82353
    invoke-direct {p0}, LX/0Yl;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 82354
    const/4 v8, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    invoke-direct/range {v1 .. v8}, LX/0Yl;->b(ILjava/lang/String;Ljava/lang/String;LX/0P1;JLjava/lang/Boolean;)V

    .line 82355
    :cond_0
    :goto_0
    return-object p0

    .line 82356
    :cond_1
    if-eqz p7, :cond_0

    .line 82357
    invoke-direct {p0, p1, p2}, LX/0Yl;->m(ILjava/lang/String;)LX/0Yl;

    goto :goto_0
.end method

.method private b(ILjava/lang/String;Ljava/lang/String;LX/0P1;JLjava/lang/Boolean;)V
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 82358
    new-instance v0, LX/0Yj;

    invoke-direct {v0, p1, p2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    .line 82359
    iput-object p3, v0, LX/0Yj;->e:Ljava/lang/String;

    .line 82360
    move-object v1, v0

    .line 82361
    iput-wide p5, v1, LX/0Yj;->h:J

    .line 82362
    move-object v1, v1

    .line 82363
    invoke-virtual {v1, p7}, LX/0Yj;->a(Ljava/lang/Boolean;)LX/0Yj;

    .line 82364
    if-eqz p4, :cond_0

    .line 82365
    iput-object p4, v0, LX/0Yj;->l:Ljava/util/Map;

    .line 82366
    :cond_0
    iget-object v1, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v1, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->b(LX/0Yj;)V

    .line 82367
    return-void
.end method

.method private c(ILjava/lang/String;Ljava/lang/String;LX/0P1;JZ)LX/0Yl;
    .locals 11
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;JZ)",
            "LX/0Yl;"
        }
    .end annotation

    .prologue
    .line 82368
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide/from16 v6, p5

    move/from16 v10, p7

    invoke-static/range {v1 .. v10}, LX/0Yl;->a(LX/0Yl;ILjava/lang/String;Ljava/lang/String;LX/0P1;JDZ)V

    .line 82369
    return-object p0
.end method

.method public static d(LX/0Yl;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0Yj;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 82370
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yj;

    .line 82371
    iget-object v2, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v2, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->f(LX/0Yj;)V

    goto :goto_0

    .line 82372
    :cond_0
    return-void
.end method

.method private static e(LX/0Yl;)V
    .locals 1

    .prologue
    .line 82373
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yl;->e:Z

    .line 82374
    return-void
.end method

.method private f(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;
    .locals 11
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/0Yl;"
        }
    .end annotation

    .prologue
    .line 82375
    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide/from16 v6, p5

    invoke-virtual/range {v1 .. v9}, LX/0Yl;->a(ILjava/lang/String;Ljava/lang/String;LX/0P1;JLX/00q;Ljava/lang/Boolean;)LX/0Yl;

    move-result-object v0

    return-object v0
.end method

.method private static f(LX/0Yl;)V
    .locals 1

    .prologue
    .line 82401
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yl;->f:Z

    .line 82402
    return-void
.end method

.method private g(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;
    .locals 9
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/0Yl;"
        }
    .end annotation

    .prologue
    .line 82376
    const/4 v8, 0x1

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    invoke-static/range {v1 .. v8}, LX/0Yl;->a(LX/0Yl;ILjava/lang/String;Ljava/lang/String;LX/0P1;JZ)LX/0Yl;

    .line 82377
    return-object p0
.end method

.method private h(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;
    .locals 9
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/0Yl;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 82378
    invoke-virtual/range {p0 .. p6}, LX/0Yl;->b(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    .line 82379
    invoke-static/range {v1 .. v8}, LX/0Yl;->b(LX/0Yl;ILjava/lang/String;Ljava/lang/String;LX/0P1;JZ)LX/0Yl;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    .line 82380
    invoke-static/range {v1 .. v8}, LX/0Yl;->a(LX/0Yl;ILjava/lang/String;Ljava/lang/String;LX/0P1;JZ)LX/0Yl;

    .line 82381
    return-object p0
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 82382
    iget-boolean v0, p0, LX/0Yl;->e:Z

    return v0
.end method

.method private i()Z
    .locals 1

    .prologue
    .line 82383
    iget-boolean v0, p0, LX/0Yl;->f:Z

    return v0
.end method

.method private m(ILjava/lang/String;)LX/0Yl;
    .locals 3

    .prologue
    .line 82384
    iget-object v0, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0013

    const-string v2, "NNFWarmStartTTI"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0017

    const-string v2, "NNFWarmStartFromNetwork"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 82385
    if-eqz v0, :cond_1

    .line 82386
    iget-object v0, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 82387
    :cond_1
    return-object p0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n(ILjava/lang/String;)LX/0Yl;
    .locals 3

    .prologue
    .line 82388
    iget-object v0, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa002e

    const-string v2, "NNFHotStartTTI"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa002c

    const-string v2, "NNFHotStartAndFreshRenderTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 82389
    if-eqz v0, :cond_1

    .line 82390
    iget-object v0, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 82391
    :cond_1
    return-object p0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static o(LX/0Yl;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 82392
    invoke-virtual {p0}, LX/0Yl;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82393
    iget-object v0, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 82394
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)LX/0Yl;
    .locals 2

    .prologue
    .line 82395
    iget-object v0, p0, LX/0Yl;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/0Yl;->a(ILjava/lang/String;J)LX/0Yl;

    .line 82396
    return-object p0
.end method

.method public final a(ILjava/lang/String;J)LX/0Yl;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 82397
    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v5, v4

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, LX/0Yl;->f(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    .line 82398
    return-object p0
.end method

.method public final a(ILjava/lang/String;LX/0P1;)LX/0Yl;
    .locals 8
    .param p3    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/0Yl;"
        }
    .end annotation

    .prologue
    .line 82399
    const/4 v4, 0x0

    iget-object v0, p0, LX/0Yl;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v6

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-virtual/range {v1 .. v7}, LX/0Yl;->b(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    .line 82400
    return-object p0
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;
    .locals 11
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/0Yl;"
        }
    .end annotation

    .prologue
    .line 82307
    iget-object v0, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0014

    const-string v2, "NNFFreshContentStart"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82308
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const/4 v10, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide/from16 v6, p5

    invoke-static/range {v1 .. v10}, LX/0Yl;->a(LX/0Yl;ILjava/lang/String;Ljava/lang/String;LX/0P1;JDZ)V

    .line 82309
    :cond_0
    return-object p0
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;LX/0P1;JLX/00q;Ljava/lang/Boolean;)LX/0Yl;
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/00q;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J",
            "LX/00q;",
            "Ljava/lang/Boolean;",
            ")",
            "LX/0Yl;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 82310
    invoke-virtual {p0}, LX/0Yl;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 82311
    new-instance v0, LX/0Yj;

    invoke-direct {v0, p1, p2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    .line 82312
    iput-object p3, v0, LX/0Yj;->e:Ljava/lang/String;

    .line 82313
    move-object v0, v0

    .line 82314
    iput-wide p5, v0, LX/0Yj;->g:J

    .line 82315
    move-object v0, v0

    .line 82316
    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "native_newsfeed"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yj;->b()LX/0Yj;

    move-result-object v0

    invoke-virtual {v0, p8}, LX/0Yj;->a(Ljava/lang/Boolean;)LX/0Yj;

    move-result-object v0

    .line 82317
    if-eqz p7, :cond_0

    .line 82318
    iput-object p7, v0, LX/0Yj;->s:LX/00q;

    .line 82319
    :cond_0
    if-eqz p4, :cond_1

    .line 82320
    iput-object p4, v0, LX/0Yj;->l:Ljava/util/Map;

    .line 82321
    :cond_1
    iget-object v1, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v1, v0, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->a(LX/0Yj;Z)V

    .line 82322
    :cond_2
    return-object p0
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;LX/0P1;JLjava/lang/Boolean;)LX/0Yl;
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/lang/Boolean;",
            ")",
            "LX/0Yl;"
        }
    .end annotation

    .prologue
    .line 82222
    iget-boolean v0, p0, LX/0Yl;->d:Z

    move v0, v0

    .line 82223
    if-nez v0, :cond_0

    .line 82224
    invoke-direct/range {p0 .. p7}, LX/0Yl;->b(ILjava/lang/String;Ljava/lang/String;LX/0P1;JLjava/lang/Boolean;)V

    .line 82225
    :goto_0
    return-object p0

    .line 82226
    :cond_0
    invoke-static {p0, p1, p2}, LX/0Yl;->o(LX/0Yl;ILjava/lang/String;)V

    .line 82227
    goto :goto_0
.end method

.method public final a(ILjava/lang/String;Z)LX/0Yl;
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 82275
    if-eqz p3, :cond_0

    .line 82276
    new-instance v8, LX/00q;

    invoke-direct {v8}, LX/00q;-><init>()V

    .line 82277
    :goto_0
    iget-object v0, p0, LX/0Yl;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v6

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v5, v4

    move-object v9, v4

    invoke-virtual/range {v1 .. v9}, LX/0Yl;->a(ILjava/lang/String;Ljava/lang/String;LX/0P1;JLX/00q;Ljava/lang/Boolean;)LX/0Yl;

    .line 82278
    return-object p0

    :cond_0
    move-object v8, v4

    goto :goto_0
.end method

.method public final a(JLX/00q;ZLjava/util/List;)LX/0Yl;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/00q;",
            "Z",
            "Ljava/util/List",
            "<",
            "LX/0Yj;",
            ">;)",
            "LX/0Yl;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 82264
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82265
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yj;

    .line 82266
    new-instance v2, LX/00q;

    invoke-direct {v2, p3}, LX/00q;-><init>(LX/00q;)V

    .line 82267
    iget-object v3, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v4, LX/0Yj;

    invoke-direct {v4, v0}, LX/0Yj;-><init>(LX/0Yj;)V

    .line 82268
    iput-wide p1, v4, LX/0Yj;->g:J

    .line 82269
    move-object v0, v4

    .line 82270
    new-array v4, v7, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "native_newsfeed"

    aput-object v6, v4, v5

    invoke-virtual {v0, v4}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yj;->b()LX/0Yj;

    move-result-object v0

    .line 82271
    iput-object v2, v0, LX/0Yj;->s:LX/00q;

    .line 82272
    move-object v0, v0

    .line 82273
    invoke-virtual {v0, p4}, LX/0Yj;->c(Z)LX/0Yj;

    move-result-object v0

    invoke-interface {v3, v0, v7}, Lcom/facebook/performancelogger/PerformanceLogger;->a(LX/0Yj;Z)V

    goto :goto_0

    .line 82274
    :cond_0
    return-object p0
.end method

.method public final a(JLjava/util/List;)LX/0Yl;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "LX/0Yj;",
            ">;)",
            "LX/0Yl;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 82255
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82256
    invoke-static {p0}, LX/0Yl;->f(LX/0Yl;)V

    .line 82257
    invoke-static {p0}, LX/0Yl;->e(LX/0Yl;)V

    .line 82258
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yj;

    .line 82259
    iget-object v2, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v3, LX/0Yj;

    invoke-direct {v3, v0}, LX/0Yj;-><init>(LX/0Yj;)V

    .line 82260
    iput-wide p1, v3, LX/0Yj;->g:J

    .line 82261
    move-object v0, v3

    .line 82262
    new-array v3, v6, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "native_newsfeed"

    aput-object v5, v3, v4

    invoke-virtual {v0, v3}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v0

    invoke-interface {v2, v0, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->a(LX/0Yj;Z)V

    goto :goto_0

    .line 82263
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/util/List;JLX/00q;)LX/0Yl;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0Yj;",
            ">;J",
            "LX/00q;",
            ")",
            "LX/0Yl;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 82242
    invoke-static {p0}, LX/0Yl;->f(LX/0Yl;)V

    .line 82243
    invoke-static {p0}, LX/0Yl;->e(LX/0Yl;)V

    .line 82244
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82245
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yj;

    .line 82246
    new-instance v2, LX/00q;

    invoke-direct {v2, p4}, LX/00q;-><init>(LX/00q;)V

    .line 82247
    iget-object v3, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v4, LX/0Yj;

    invoke-direct {v4, v0}, LX/0Yj;-><init>(LX/0Yj;)V

    .line 82248
    iput-wide p2, v4, LX/0Yj;->g:J

    .line 82249
    move-object v0, v4

    .line 82250
    new-array v4, v7, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "native_newsfeed"

    aput-object v6, v4, v5

    invoke-virtual {v0, v4}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v0

    .line 82251
    iput-object v2, v0, LX/0Yj;->s:LX/00q;

    .line 82252
    move-object v0, v0

    .line 82253
    invoke-interface {v3, v0, v7}, Lcom/facebook/performancelogger/PerformanceLogger;->a(LX/0Yj;Z)V

    goto :goto_0

    .line 82254
    :cond_0
    return-object p0
.end method

.method public final b(ILjava/lang/String;)LX/0Yl;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 82240
    iget-object v0, p0, LX/0Yl;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v6

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-virtual/range {v1 .. v7}, LX/0Yl;->a(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    .line 82241
    return-object p0
.end method

.method public final b(ILjava/lang/String;J)LX/0Yl;
    .locals 7

    .prologue
    .line 82235
    iget-boolean v0, p0, LX/0Yl;->d:Z

    move v0, v0

    .line 82236
    if-nez v0, :cond_0

    .line 82237
    iget-object v0, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const/4 v3, 0x0

    move v1, p1

    move-object v2, p2

    move-wide v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;J)V

    .line 82238
    :goto_0
    return-object p0

    .line 82239
    :cond_0
    invoke-static {p0, p1, p2}, LX/0Yl;->o(LX/0Yl;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public final b(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;
    .locals 9
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/0Yl;"
        }
    .end annotation

    .prologue
    .line 82234
    const/4 v8, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    invoke-virtual/range {v1 .. v8}, LX/0Yl;->a(ILjava/lang/String;Ljava/lang/String;LX/0P1;JLjava/lang/Boolean;)LX/0Yl;

    move-result-object v0

    return-object v0
.end method

.method public final b(ILjava/lang/String;Z)LX/0Yl;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 82233
    iget-object v0, p0, LX/0Yl;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v6

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v5, v4

    move v8, p3

    invoke-direct/range {v1 .. v8}, LX/0Yl;->c(ILjava/lang/String;Ljava/lang/String;LX/0P1;JZ)LX/0Yl;

    move-result-object v0

    return-object v0
.end method

.method public final c(ILjava/lang/String;)LX/0Yl;
    .locals 11

    .prologue
    .line 82230
    iget-object v0, p0, LX/0Yl;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    const/4 v6, 0x0

    .line 82231
    move-object v3, p0

    move v4, p1

    move-object v5, p2

    move-object v7, v6

    move-wide v8, v0

    invoke-virtual/range {v3 .. v9}, LX/0Yl;->b(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    .line 82232
    return-object p0
.end method

.method public final c(ILjava/lang/String;J)LX/0Yl;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 82228
    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v5, v4

    move-wide v6, p3

    invoke-virtual/range {v1 .. v7}, LX/0Yl;->c(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    .line 82229
    return-object p0
.end method

.method public final c(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;
    .locals 9
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/0Yl;"
        }
    .end annotation

    .prologue
    .line 82220
    const/4 v8, 0x1

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    invoke-static/range {v1 .. v8}, LX/0Yl;->b(LX/0Yl;ILjava/lang/String;Ljava/lang/String;LX/0P1;JZ)LX/0Yl;

    .line 82221
    return-object p0
.end method

.method public final d(ILjava/lang/String;)LX/0Yl;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 82279
    iget-object v0, p0, LX/0Yl;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v6

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v1 .. v7}, LX/0Yl;->g(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    .line 82280
    return-object p0
.end method

.method public final d(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/0Yl;"
        }
    .end annotation

    .prologue
    .line 82281
    new-instance v0, LX/0Yj;

    invoke-direct {v0, p1, p2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    .line 82282
    iput-object p3, v0, LX/0Yj;->e:Ljava/lang/String;

    .line 82283
    move-object v1, v0

    .line 82284
    iput-wide p5, v1, LX/0Yj;->h:J

    .line 82285
    if-eqz p4, :cond_0

    .line 82286
    iput-object p4, v0, LX/0Yj;->l:Ljava/util/Map;

    .line 82287
    :cond_0
    iget-object v1, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v1, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    .line 82288
    return-object p0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 82289
    iget-object v0, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0016

    const-string v2, "NNFColdStartTTI"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa004d

    const-string v2, "NNFColdStartNetwork"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(ILjava/lang/String;)LX/0Yl;
    .locals 1

    .prologue
    .line 82290
    iget-boolean v0, p0, LX/0Yl;->d:Z

    move v0, v0

    .line 82291
    if-nez v0, :cond_0

    .line 82292
    iget-object v0, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 82293
    :goto_0
    return-object p0

    .line 82294
    :cond_0
    invoke-static {p0, p1, p2}, LX/0Yl;->o(LX/0Yl;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public final e(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;
    .locals 11
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/0Yl;"
        }
    .end annotation

    .prologue
    .line 82295
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const/4 v10, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide/from16 v6, p5

    invoke-static/range {v1 .. v10}, LX/0Yl;->a(LX/0Yl;ILjava/lang/String;Ljava/lang/String;LX/0P1;JDZ)V

    .line 82296
    return-object p0
.end method

.method public final f(ILjava/lang/String;)LX/0Yl;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 82297
    iget-object v0, p0, LX/0Yl;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v6

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-virtual/range {v1 .. v7}, LX/0Yl;->c(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    .line 82298
    return-object p0
.end method

.method public final g(ILjava/lang/String;)LX/0Yl;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 82299
    iget-object v0, p0, LX/0Yl;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v6

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-virtual/range {v1 .. v7}, LX/0Yl;->d(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    move-result-object v0

    return-object v0
.end method

.method public final h(ILjava/lang/String;)LX/0Yl;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 82300
    iget-object v0, p0, LX/0Yl;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v6

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-virtual/range {v1 .. v7}, LX/0Yl;->e(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    move-result-object v0

    return-object v0
.end method

.method public final i(ILjava/lang/String;)LX/0Yl;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 82301
    iget-object v0, p0, LX/0Yl;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v6

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v1 .. v7}, LX/0Yl;->h(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    .line 82302
    return-object p0
.end method

.method public final j(ILjava/lang/String;)LX/0Yl;
    .locals 1

    .prologue
    .line 82303
    iget-object v0, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 82304
    return-object p0
.end method

.method public final k(ILjava/lang/String;)Z
    .locals 1

    .prologue
    .line 82305
    iget-object v0, p0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    return v0
.end method
