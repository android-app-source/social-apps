.class public LX/1EJ;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hj;
.implements LX/0fs;


# annotations
.annotation build Lcom/facebook/controllercallbacks/api/ControllerConfig;
.end annotation


# instance fields
.field private final a:LX/18A;

.field public b:Lcom/facebook/feed/fragment/NewsFeedFragment;

.field private c:LX/1Pq;

.field public d:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "Lcom/facebook/api/feed/data/LegacyFeedUnitUpdater;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/18A;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 219599
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 219600
    iput-object p1, p0, LX/1EJ;->a:LX/18A;

    .line 219601
    return-void
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 219602
    const/16 v0, 0x808

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 219603
    :cond_0
    :goto_0
    return-void

    :cond_1
    move v2, v3

    .line 219604
    :goto_1
    iget-object v0, p0, LX/1EJ;->e:LX/1Iu;

    .line 219605
    iget-object v1, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 219606
    check-cast v0, LX/0g1;

    invoke-interface {v0}, LX/0g1;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 219607
    iget-object v0, p0, LX/1EJ;->e:LX/1Iu;

    .line 219608
    iget-object v1, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 219609
    check-cast v0, LX/0g1;

    invoke-interface {v0, v2}, LX/0g1;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 219610
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;

    if-eqz v1, :cond_2

    .line 219611
    iget-object v1, p0, LX/1EJ;->a:LX/18A;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/18A;->a(Ljava/lang/String;)V

    .line 219612
    iget-object v1, p0, LX/1EJ;->d:LX/1Iu;

    .line 219613
    iget-object v4, v1, LX/1Iu;->a:Ljava/lang/Object;

    move-object v1, v4

    .line 219614
    check-cast v1, LX/0qq;

    invoke-virtual {v1, v0}, LX/0qq;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 219615
    iget-object v1, p0, LX/1EJ;->c:LX/1Pq;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    aput-object v0, v4, v3

    invoke-interface {v1, v4}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 219616
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 219617
    :cond_3
    iget-object v0, p0, LX/1EJ;->b:Lcom/facebook/feed/fragment/NewsFeedFragment;

    sget-object v1, LX/0gf;->PULL_TO_REFRESH:LX/0gf;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/fragment/NewsFeedFragment;->a(LX/0gf;)V

    goto :goto_0
.end method

.method public final a(LX/1Qr;LX/0g8;LX/1Pf;)V
    .locals 0

    .prologue
    .line 219618
    iput-object p3, p0, LX/1EJ;->c:LX/1Pq;

    .line 219619
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 219620
    const/4 v0, 0x0

    iput-object v0, p0, LX/1EJ;->c:LX/1Pq;

    .line 219621
    return-void
.end method
