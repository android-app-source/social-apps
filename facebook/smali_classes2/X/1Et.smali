.class public LX/1Et;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1FZ;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1FZ;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 221403
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1FZ;
    .locals 4

    .prologue
    .line 221404
    sget-object v0, LX/1Et;->a:LX/1FZ;

    if-nez v0, :cond_1

    .line 221405
    const-class v1, LX/1Et;

    monitor-enter v1

    .line 221406
    :try_start_0
    sget-object v0, LX/1Et;->a:LX/1FZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 221407
    if-eqz v2, :cond_0

    .line 221408
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 221409
    invoke-static {v0}, LX/1Eu;->a(LX/0QB;)LX/1FB;

    move-result-object v3

    check-cast v3, LX/1FB;

    invoke-static {v0}, LX/1FC;->a(LX/0QB;)LX/1FG;

    move-result-object p0

    check-cast p0, LX/1FG;

    invoke-static {v3, p0}, LX/1FE;->a(LX/1FB;LX/1FG;)LX/1FZ;

    move-result-object v3

    move-object v0, v3

    .line 221410
    sput-object v0, LX/1Et;->a:LX/1FZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 221411
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 221412
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 221413
    :cond_1
    sget-object v0, LX/1Et;->a:LX/1FZ;

    return-object v0

    .line 221414
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 221415
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 221416
    invoke-static {p0}, LX/1Eu;->a(LX/0QB;)LX/1FB;

    move-result-object v0

    check-cast v0, LX/1FB;

    invoke-static {p0}, LX/1FC;->a(LX/0QB;)LX/1FG;

    move-result-object v1

    check-cast v1, LX/1FG;

    invoke-static {v0, v1}, LX/1FE;->a(LX/1FB;LX/1FG;)LX/1FZ;

    move-result-object v0

    return-object v0
.end method
