.class public LX/1in;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1in;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 299179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 299180
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/1in;->a:Ljava/util/Map;

    .line 299181
    return-void
.end method

.method public static a(LX/0QB;)LX/1in;
    .locals 3

    .prologue
    .line 299182
    sget-object v0, LX/1in;->b:LX/1in;

    if-nez v0, :cond_1

    .line 299183
    const-class v1, LX/1in;

    monitor-enter v1

    .line 299184
    :try_start_0
    sget-object v0, LX/1in;->b:LX/1in;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 299185
    if-eqz v2, :cond_0

    .line 299186
    :try_start_1
    new-instance v0, LX/1in;

    invoke-direct {v0}, LX/1in;-><init>()V

    .line 299187
    move-object v0, v0

    .line 299188
    sput-object v0, LX/1in;->b:LX/1in;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 299189
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 299190
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 299191
    :cond_1
    sget-object v0, LX/1in;->b:LX/1in;

    return-object v0

    .line 299192
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 299193
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 299194
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1in;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pf;

    .line 299195
    if-nez v0, :cond_0

    .line 299196
    new-instance v0, LX/1pf;

    invoke-direct {v0}, LX/1pf;-><init>()V

    .line 299197
    iget-object v1, p0, LX/1in;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299198
    :cond_0
    invoke-virtual {v0, p2, p3}, LX/1pf;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299199
    monitor-exit p0

    return-void

    .line 299200
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
