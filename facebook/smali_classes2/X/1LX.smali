.class public LX/1LX;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hj;
.implements LX/0fs;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BNP;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;>;"
        }
    .end annotation
.end field

.field public c:LX/1Pf;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 233898
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 233899
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 233900
    iput-object v0, p0, LX/1LX;->a:LX/0Ot;

    .line 233901
    return-void
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 233902
    const/16 v0, 0x6df

    invoke-static {v0, p1, p2, p3}, LX/2ck;->a(IIILandroid/content/Intent;)Z

    move-result v0

    .line 233903
    if-nez v0, :cond_0

    .line 233904
    :goto_0
    return-void

    .line 233905
    :cond_0
    iget-object v0, p0, LX/1LX;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BNP;

    const-string v3, "native_newsfeed"

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    iget-object v1, p0, LX/1LX;->c:LX/1Pf;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    move v1, p2

    move-object v2, p3

    invoke-virtual/range {v0 .. v5}, LX/BNP;->a(ILandroid/content/Intent;Ljava/lang/String;LX/0am;LX/0am;)V

    .line 233906
    const-string v0, "publishReviewParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/protocol/PostReviewParams;

    .line 233907
    if-nez v0, :cond_1

    .line 233908
    :goto_1
    goto :goto_0

    .line 233909
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 233910
    iget-object v1, p0, LX/1LX;->b:LX/1Iu;

    .line 233911
    iget-object v2, v1, LX/1Iu;->a:Ljava/lang/Object;

    move-object v1, v2

    .line 233912
    check-cast v1, LX/0g1;

    invoke-interface {v1}, LX/0g1;->size()I

    move-result v5

    .line 233913
    const/4 v1, 0x0

    move v3, v1

    :goto_2
    if-ge v3, v5, :cond_4

    .line 233914
    iget-object v1, p0, LX/1LX;->b:LX/1Iu;

    .line 233915
    iget-object v2, v1, LX/1Iu;->a:Ljava/lang/Object;

    move-object v1, v2

    .line 233916
    check-cast v1, LX/0g1;

    invoke-interface {v1, v3}, LX/0g1;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 233917
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    instance-of v2, v2, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    if-eqz v2, :cond_3

    .line 233918
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    .line 233919
    iget-object v2, p0, LX/1LX;->c:LX/1Pf;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->g()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object p1

    invoke-interface {v2, p1}, LX/1Pr;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/25c;

    .line 233920
    iget p1, v2, LX/25c;->a:I

    move p1, p1

    .line 233921
    add-int/lit8 p2, p1, 0x1

    invoke-static {v1}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;)LX/0Px;

    move-result-object p3

    invoke-virtual {p3}, LX/0Px;->size()I

    move-result p3

    if-ge p2, p3, :cond_2

    .line 233922
    add-int/lit8 p1, p1, 0x1

    .line 233923
    iput p1, v2, LX/25c;->a:I

    .line 233924
    :cond_2
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233925
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 233926
    :cond_4
    iget-object v1, p0, LX/1LX;->c:LX/1Pf;

    invoke-interface {v4}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Pq;->a([Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final a(LX/1Qr;LX/0g8;LX/1Pf;)V
    .locals 0

    .prologue
    .line 233927
    iput-object p3, p0, LX/1LX;->c:LX/1Pf;

    .line 233928
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 233929
    const/4 v0, 0x0

    iput-object v0, p0, LX/1LX;->c:LX/1Pf;

    .line 233930
    return-void
.end method
