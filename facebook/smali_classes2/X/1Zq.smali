.class public final LX/1Zq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/net/nsd/NsdManager$DiscoveryListener;


# instance fields
.field public final synthetic a:LX/1Zm;


# direct methods
.method public constructor <init>(LX/1Zm;)V
    .locals 0

    .prologue
    .line 275446
    iput-object p1, p0, LX/1Zq;->a:LX/1Zm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDiscoveryStarted(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 275447
    iget-object v0, p0, LX/1Zq;->a:LX/1Zm;

    const/4 v1, 0x1

    .line 275448
    iput-boolean v1, v0, LX/1Zm;->n:Z

    .line 275449
    iget-object v0, p0, LX/1Zq;->a:LX/1Zm;

    iget-boolean v0, v0, LX/1Zm;->o:Z

    if-eqz v0, :cond_0

    .line 275450
    iget-object v0, p0, LX/1Zq;->a:LX/1Zm;

    sget-object v1, LX/1Zn;->NSD:LX/1Zn;

    invoke-static {v0, v1}, LX/1Zm;->b(LX/1Zm;LX/1Zn;)V

    .line 275451
    :cond_0
    return-void
.end method

.method public final onDiscoveryStopped(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 275452
    iget-object v0, p0, LX/1Zq;->a:LX/1Zm;

    .line 275453
    iput-boolean v1, v0, LX/1Zm;->n:Z

    .line 275454
    iget-object v0, p0, LX/1Zq;->a:LX/1Zm;

    .line 275455
    iput-boolean v1, v0, LX/1Zm;->o:Z

    .line 275456
    iget-object v0, p0, LX/1Zq;->a:LX/1Zm;

    invoke-static {v0}, LX/1Zm;->e$redex0(LX/1Zm;)V

    .line 275457
    return-void
.end method

.method public final onServiceFound(Landroid/net/nsd/NsdServiceInfo;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 275458
    invoke-virtual {p1}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "_fb._tcp."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Zq;->a:LX/1Zm;

    iget-object v0, v0, LX/1Zm;->g:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/net/nsd/NsdServiceInfo;->getServiceName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 275459
    iget-object v0, p0, LX/1Zq;->a:LX/1Zm;

    invoke-virtual {p1}, Landroid/net/nsd/NsdServiceInfo;->getServiceName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/1Zm;->a$redex0(LX/1Zm;Ljava/lang/String;)V

    .line 275460
    :cond_0
    return-void
.end method

.method public final onServiceLost(Landroid/net/nsd/NsdServiceInfo;)V
    .locals 2

    .prologue
    .line 275461
    iget-object v0, p0, LX/1Zq;->a:LX/1Zm;

    iget-object v0, v0, LX/1Zm;->g:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/net/nsd/NsdServiceInfo;->getServiceName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 275462
    iget-object v0, p0, LX/1Zq;->a:LX/1Zm;

    invoke-static {v0}, LX/1Zm;->g(LX/1Zm;)V

    .line 275463
    return-void
.end method

.method public final onStartDiscoveryFailed(Ljava/lang/String;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 275464
    iget-object v0, p0, LX/1Zq;->a:LX/1Zm;

    .line 275465
    iput-boolean v1, v0, LX/1Zm;->n:Z

    .line 275466
    iget-object v0, p0, LX/1Zq;->a:LX/1Zm;

    .line 275467
    iput-boolean v1, v0, LX/1Zm;->o:Z

    .line 275468
    iget-object v0, p0, LX/1Zq;->a:LX/1Zm;

    invoke-static {v0}, LX/1Zm;->e$redex0(LX/1Zm;)V

    .line 275469
    return-void
.end method

.method public final onStopDiscoveryFailed(Ljava/lang/String;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 275470
    iget-object v0, p0, LX/1Zq;->a:LX/1Zm;

    .line 275471
    iput-boolean v1, v0, LX/1Zm;->n:Z

    .line 275472
    iget-object v0, p0, LX/1Zq;->a:LX/1Zm;

    .line 275473
    iput-boolean v1, v0, LX/1Zm;->o:Z

    .line 275474
    iget-object v0, p0, LX/1Zq;->a:LX/1Zm;

    invoke-static {v0}, LX/1Zm;->e$redex0(LX/1Zm;)V

    .line 275475
    return-void
.end method
