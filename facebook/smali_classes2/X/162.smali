.class public final LX/162;
.super LX/0mA;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0mA",
        "<",
        "LX/162;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0lF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0mC;)V
    .locals 1

    .prologue
    .line 184248
    invoke-direct {p0, p1}, LX/0mA;-><init>(LX/0mC;)V

    .line 184249
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/162;->b:Ljava/util/List;

    .line 184250
    return-void
.end method

.method private N()LX/162;
    .locals 4

    .prologue
    .line 184251
    new-instance v1, LX/162;

    iget-object v0, p0, LX/0mA;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 184252
    iget-object v0, p0, LX/162;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 184253
    iget-object v3, v1, LX/162;->b:Ljava/util/List;

    invoke-virtual {v0}, LX/0lF;->d()LX/0lF;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 184254
    :cond_0
    return-object v1
.end method

.method private b(ILX/0lF;)LX/162;
    .locals 2

    .prologue
    .line 184255
    if-gez p1, :cond_0

    .line 184256
    iget-object v0, p0, LX/162;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 184257
    :goto_0
    return-object p0

    .line 184258
    :cond_0
    iget-object v0, p0, LX/162;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 184259
    iget-object v0, p0, LX/162;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 184260
    :cond_1
    iget-object v0, p0, LX/162;->b:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private b(LX/0lF;)LX/162;
    .locals 1

    .prologue
    .line 184261
    iget-object v0, p0, LX/162;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184262
    return-object p0
.end method


# virtual methods
.method public final G()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "LX/0lF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184263
    iget-object v0, p0, LX/162;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final I()LX/0m9;
    .locals 1

    .prologue
    .line 184264
    invoke-virtual {p0}, LX/0mA;->L()LX/0m9;

    move-result-object v0

    .line 184265
    invoke-direct {p0, v0}, LX/162;->b(LX/0lF;)LX/162;

    .line 184266
    return-object v0
.end method

.method public final J()LX/162;
    .locals 1

    .prologue
    .line 184281
    invoke-static {}, LX/0mC;->a()LX/2FN;

    move-result-object v0

    invoke-direct {p0, v0}, LX/162;->b(LX/0lF;)LX/162;

    .line 184282
    return-object p0
.end method

.method public final a(I)LX/0lF;
    .locals 1

    .prologue
    .line 184267
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/162;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 184268
    iget-object v0, p0, LX/162;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 184269
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)LX/0lF;
    .locals 1

    .prologue
    .line 184270
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()LX/15z;
    .locals 1

    .prologue
    .line 184271
    sget-object v0, LX/15z;->START_ARRAY:LX/15z;

    return-object v0
.end method

.method public final a(ILX/0lF;)LX/162;
    .locals 0

    .prologue
    .line 184272
    if-nez p2, :cond_0

    .line 184273
    invoke-static {}, LX/0mC;->a()LX/2FN;

    move-result-object p2

    .line 184274
    :cond_0
    invoke-direct {p0, p1, p2}, LX/162;->b(ILX/0lF;)LX/162;

    .line 184275
    return-object p0
.end method

.method public final a(LX/0lF;)LX/162;
    .locals 0

    .prologue
    .line 184201
    if-nez p1, :cond_0

    .line 184202
    invoke-static {}, LX/0mC;->a()LX/2FN;

    move-result-object p1

    .line 184203
    :cond_0
    invoke-direct {p0, p1}, LX/162;->b(LX/0lF;)LX/162;

    .line 184204
    return-object p0
.end method

.method public final a(LX/162;)LX/162;
    .locals 2

    .prologue
    .line 184276
    iget-object v0, p0, LX/162;->b:Ljava/util/List;

    iget-object v1, p1, LX/162;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 184277
    return-object p0
.end method

.method public final a(Ljava/lang/Boolean;)LX/162;
    .locals 1

    .prologue
    .line 184278
    if-nez p1, :cond_0

    .line 184279
    invoke-virtual {p0}, LX/162;->J()LX/162;

    move-result-object v0

    .line 184280
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, LX/0mC;->a(Z)LX/1Xb;

    move-result-object v0

    invoke-direct {p0, v0}, LX/162;->b(LX/0lF;)LX/162;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Double;)LX/162;
    .locals 2

    .prologue
    .line 184242
    if-nez p1, :cond_0

    .line 184243
    invoke-virtual {p0}, LX/162;->J()LX/162;

    move-result-object v0

    .line 184244
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, LX/0mC;->a(D)LX/0rP;

    move-result-object v0

    invoke-direct {p0, v0}, LX/162;->b(LX/0lF;)LX/162;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Integer;)LX/162;
    .locals 1

    .prologue
    .line 184245
    if-nez p1, :cond_0

    .line 184246
    invoke-virtual {p0}, LX/162;->J()LX/162;

    move-result-object v0

    .line 184247
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, LX/0mC;->a(I)LX/0rP;

    move-result-object v0

    invoke-direct {p0, v0}, LX/162;->b(LX/0lF;)LX/162;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Long;)LX/162;
    .locals 2

    .prologue
    .line 184195
    if-nez p1, :cond_0

    .line 184196
    invoke-virtual {p0}, LX/162;->J()LX/162;

    move-result-object v0

    .line 184197
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/0mC;->a(J)LX/0rP;

    move-result-object v0

    invoke-direct {p0, v0}, LX/162;->b(LX/0lF;)LX/162;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)LX/0lF;
    .locals 1

    .prologue
    .line 184198
    sget-object v0, LX/2AT;->a:LX/2AT;

    move-object v0, v0

    .line 184199
    return-object v0
.end method

.method public final b(J)LX/162;
    .locals 1

    .prologue
    .line 184200
    invoke-static {p1, p2}, LX/0mC;->a(J)LX/0rP;

    move-result-object v0

    invoke-direct {p0, v0}, LX/162;->b(LX/0lF;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c(I)LX/162;
    .locals 1

    .prologue
    .line 184205
    invoke-static {p1}, LX/0mC;->a(I)LX/0rP;

    move-result-object v0

    invoke-direct {p0, v0}, LX/162;->b(LX/0lF;)LX/162;

    .line 184206
    return-object p0
.end method

.method public final synthetic d()LX/0lF;
    .locals 1

    .prologue
    .line 184207
    invoke-direct {p0}, LX/162;->N()LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 184208
    iget-object v0, p0, LX/162;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final e(Ljava/lang/String;)LX/0lF;
    .locals 2

    .prologue
    .line 184209
    iget-object v0, p0, LX/162;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 184210
    invoke-virtual {v0, p1}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 184211
    if-eqz v0, :cond_0

    .line 184212
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 184213
    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    .line 184214
    :cond_0
    :goto_0
    return v0

    .line 184215
    :cond_1
    if-eqz p1, :cond_0

    .line 184216
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 184217
    iget-object v0, p0, LX/162;->b:Ljava/util/List;

    check-cast p1, LX/162;

    iget-object v1, p1, LX/162;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;)LX/162;
    .locals 1

    .prologue
    .line 184218
    if-nez p1, :cond_0

    .line 184219
    invoke-virtual {p0}, LX/162;->J()LX/162;

    move-result-object v0

    .line 184220
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, LX/0mC;->a(Ljava/lang/String;)LX/0mD;

    move-result-object v0

    invoke-direct {p0, v0}, LX/162;->b(LX/0lF;)LX/162;

    move-result-object v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 184221
    iget-object v0, p0, LX/162;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    return v0
.end method

.method public final k()LX/0nH;
    .locals 1

    .prologue
    .line 184222
    sget-object v0, LX/0nH;->ARRAY:LX/0nH;

    return-object v0
.end method

.method public final serialize(LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 184223
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 184224
    iget-object v0, p0, LX/162;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 184225
    check-cast v0, LX/0mB;

    invoke-virtual {v0, p1, p2}, LX/0mB;->serialize(LX/0nX;LX/0my;)V

    goto :goto_0

    .line 184226
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 184227
    return-void
.end method

.method public final serializeWithType(LX/0nX;LX/0my;LX/4qz;)V
    .locals 2

    .prologue
    .line 184228
    invoke-virtual {p3, p0, p1}, LX/4qz;->c(Ljava/lang/Object;LX/0nX;)V

    .line 184229
    iget-object v0, p0, LX/162;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 184230
    check-cast v0, LX/0mB;

    invoke-virtual {v0, p1, p2}, LX/0mB;->serialize(LX/0nX;LX/0my;)V

    goto :goto_0

    .line 184231
    :cond_0
    invoke-virtual {p3, p0, p1}, LX/4qz;->f(Ljava/lang/Object;LX/0nX;)V

    .line 184232
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 184233
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, LX/0lF;->e()I

    move-result v0

    shl-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x10

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 184234
    const/16 v0, 0x5b

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 184235
    const/4 v0, 0x0

    iget-object v1, p0, LX/162;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 184236
    if-lez v1, :cond_0

    .line 184237
    const/16 v0, 0x2c

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 184238
    :cond_0
    iget-object v0, p0, LX/162;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184239
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 184240
    :cond_1
    const/16 v0, 0x5d

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 184241
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
