.class public LX/1gB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1gC;


# static fields
.field private static final a:Ljava/lang/Object;

.field private static b:LX/1gB;

.field private static c:I


# instance fields
.field public d:LX/1bh;

.field public e:Ljava/lang/String;

.field public f:J

.field public g:J

.field public h:J

.field public i:Ljava/io/IOException;

.field public j:LX/37E;

.field private k:LX/1gB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 293874
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/1gB;->a:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 293872
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 293873
    return-void
.end method

.method public static h()LX/1gB;
    .locals 3

    .prologue
    .line 293861
    sget-object v1, LX/1gB;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 293862
    :try_start_0
    sget-object v0, LX/1gB;->b:LX/1gB;

    if-eqz v0, :cond_0

    .line 293863
    sget-object v0, LX/1gB;->b:LX/1gB;

    .line 293864
    iget-object v2, v0, LX/1gB;->k:LX/1gB;

    sput-object v2, LX/1gB;->b:LX/1gB;

    .line 293865
    const/4 v2, 0x0

    iput-object v2, v0, LX/1gB;->k:LX/1gB;

    .line 293866
    sget v2, LX/1gB;->c:I

    add-int/lit8 v2, v2, -0x1

    sput v2, LX/1gB;->c:I

    .line 293867
    monitor-exit v1

    .line 293868
    :goto_0
    return-object v0

    .line 293869
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 293870
    new-instance v0, LX/1gB;

    invoke-direct {v0}, LX/1gB;-><init>()V

    goto :goto_0

    .line 293871
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()LX/1bh;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293860
    iget-object v0, p0, LX/1gB;->d:LX/1bh;

    return-object v0
.end method

.method public final a(J)LX/1gB;
    .locals 1

    .prologue
    .line 293858
    iput-wide p1, p0, LX/1gB;->f:J

    .line 293859
    return-object p0
.end method

.method public final a(LX/37E;)LX/1gB;
    .locals 0

    .prologue
    .line 293856
    iput-object p1, p0, LX/1gB;->j:LX/37E;

    .line 293857
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/1gB;
    .locals 0

    .prologue
    .line 293854
    iput-object p1, p0, LX/1gB;->e:Ljava/lang/String;

    .line 293855
    return-object p0
.end method

.method public final b(J)LX/1gB;
    .locals 1

    .prologue
    .line 293875
    iput-wide p1, p0, LX/1gB;->h:J

    .line 293876
    return-object p0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293853
    iget-object v0, p0, LX/1gB;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 293852
    iget-wide v0, p0, LX/1gB;->f:J

    return-wide v0
.end method

.method public final c(J)LX/1gB;
    .locals 1

    .prologue
    .line 293850
    iput-wide p1, p0, LX/1gB;->g:J

    .line 293851
    return-object p0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 293849
    iget-wide v0, p0, LX/1gB;->h:J

    return-wide v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 293848
    iget-wide v0, p0, LX/1gB;->g:J

    return-wide v0
.end method

.method public final f()Ljava/io/IOException;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293847
    iget-object v0, p0, LX/1gB;->i:Ljava/io/IOException;

    return-object v0
.end method

.method public final g()LX/37E;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293846
    iget-object v0, p0, LX/1gB;->j:LX/37E;

    return-object v0
.end method

.method public final i()V
    .locals 7

    .prologue
    .line 293831
    sget-object v1, LX/1gB;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 293832
    :try_start_0
    sget v0, LX/1gB;->c:I

    const/4 v2, 0x5

    if-ge v0, v2, :cond_1

    .line 293833
    const-wide/16 v5, 0x0

    const/4 v3, 0x0

    .line 293834
    iput-object v3, p0, LX/1gB;->d:LX/1bh;

    .line 293835
    iput-object v3, p0, LX/1gB;->e:Ljava/lang/String;

    .line 293836
    iput-wide v5, p0, LX/1gB;->f:J

    .line 293837
    iput-wide v5, p0, LX/1gB;->g:J

    .line 293838
    iput-wide v5, p0, LX/1gB;->h:J

    .line 293839
    iput-object v3, p0, LX/1gB;->i:Ljava/io/IOException;

    .line 293840
    iput-object v3, p0, LX/1gB;->j:LX/37E;

    .line 293841
    sget v0, LX/1gB;->c:I

    add-int/lit8 v0, v0, 0x1

    sput v0, LX/1gB;->c:I

    .line 293842
    sget-object v0, LX/1gB;->b:LX/1gB;

    if-eqz v0, :cond_0

    .line 293843
    sget-object v0, LX/1gB;->b:LX/1gB;

    iput-object v0, p0, LX/1gB;->k:LX/1gB;

    .line 293844
    :cond_0
    sput-object p0, LX/1gB;->b:LX/1gB;

    .line 293845
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
