.class public abstract LX/1bp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1aZ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "INFO:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/drawee/components/DeferredReleaser$Releasable;",
        "Lcom/facebook/drawee/gestures/GestureDetector$ClickListener;",
        "LX/1aZ;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/1ab;

.field private final c:LX/1br;

.field private final d:Ljava/util/concurrent/Executor;

.field public e:LX/1fD;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/1fE;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/1Ai;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ai",
            "<TINFO;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/4AR;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/1ag;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;

.field private l:Ljava/lang/Object;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field public q:Z

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:LX/1ca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ca",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 281182
    const-class v0, LX/1bp;

    sput-object v0, LX/1bp;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1br;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 281183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 281184
    invoke-static {}, LX/1ab;->a()LX/1ab;

    move-result-object v0

    iput-object v0, p0, LX/1bp;->b:LX/1ab;

    .line 281185
    iput-object p1, p0, LX/1bp;->c:LX/1br;

    .line 281186
    iput-object p2, p0, LX/1bp;->d:Ljava/util/concurrent/Executor;

    .line 281187
    const/4 v0, 0x1

    invoke-static {p0, p3, p4, v0}, LX/1bp;->a(LX/1bp;Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 281188
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 281189
    iget-boolean v0, p0, LX/1bp;->n:Z

    .line 281190
    iput-boolean v1, p0, LX/1bp;->n:Z

    .line 281191
    iput-boolean v1, p0, LX/1bp;->p:Z

    .line 281192
    iget-object v1, p0, LX/1bp;->s:LX/1ca;

    if-eqz v1, :cond_0

    .line 281193
    iget-object v1, p0, LX/1bp;->s:LX/1ca;

    invoke-interface {v1}, LX/1ca;->g()Z

    .line 281194
    iput-object v3, p0, LX/1bp;->s:LX/1ca;

    .line 281195
    :cond_0
    iget-object v1, p0, LX/1bp;->u:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 281196
    iget-object v1, p0, LX/1bp;->u:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, LX/1bp;->b(Landroid/graphics/drawable/Drawable;)V

    .line 281197
    :cond_1
    iget-object v1, p0, LX/1bp;->r:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 281198
    iput-object v3, p0, LX/1bp;->r:Ljava/lang/String;

    .line 281199
    :cond_2
    iput-object v3, p0, LX/1bp;->u:Landroid/graphics/drawable/Drawable;

    .line 281200
    iget-object v1, p0, LX/1bp;->t:Ljava/lang/Object;

    if-eqz v1, :cond_3

    .line 281201
    const-string v1, "release"

    iget-object v2, p0, LX/1bp;->t:Ljava/lang/Object;

    invoke-direct {p0, v1, v2}, LX/1bp;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 281202
    iget-object v1, p0, LX/1bp;->t:Ljava/lang/Object;

    invoke-virtual {p0, v1}, LX/1bp;->d(Ljava/lang/Object;)V

    .line 281203
    iput-object v3, p0, LX/1bp;->t:Ljava/lang/Object;

    .line 281204
    :cond_3
    if-eqz v0, :cond_4

    .line 281205
    invoke-direct {p0}, LX/1bp;->q()LX/1Ai;

    move-result-object v0

    iget-object v1, p0, LX/1bp;->k:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/1Ai;->a(Ljava/lang/String;)V

    .line 281206
    :cond_4
    return-void
.end method

.method public static a(LX/1bp;Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 281156
    iget-object v0, p0, LX/1bp;->b:LX/1ab;

    sget-object v1, LX/1at;->ON_INIT_CONTROLLER:LX/1at;

    invoke-virtual {v0, v1}, LX/1ab;->a(LX/1at;)V

    .line 281157
    if-nez p3, :cond_0

    iget-object v0, p0, LX/1bp;->c:LX/1br;

    if-eqz v0, :cond_0

    .line 281158
    iget-object v0, p0, LX/1bp;->c:LX/1br;

    invoke-virtual {v0, p0}, LX/1br;->b(LX/1bp;)V

    .line 281159
    :cond_0
    iput-boolean v3, p0, LX/1bp;->m:Z

    .line 281160
    iput-boolean v3, p0, LX/1bp;->o:Z

    .line 281161
    invoke-direct {p0}, LX/1bp;->a()V

    .line 281162
    iput-boolean v3, p0, LX/1bp;->q:Z

    .line 281163
    iget-object v0, p0, LX/1bp;->e:LX/1fD;

    if-eqz v0, :cond_1

    .line 281164
    iget-object v0, p0, LX/1bp;->e:LX/1fD;

    invoke-virtual {v0}, LX/1fD;->b()V

    .line 281165
    :cond_1
    iget-object v0, p0, LX/1bp;->f:LX/1fE;

    if-eqz v0, :cond_2

    .line 281166
    iget-object v0, p0, LX/1bp;->f:LX/1fE;

    invoke-virtual {v0}, LX/1fE;->a()V

    .line 281167
    iget-object v0, p0, LX/1bp;->f:LX/1fE;

    .line 281168
    iput-object p0, v0, LX/1fE;->a:LX/1bp;

    .line 281169
    :cond_2
    iget-object v0, p0, LX/1bp;->g:LX/1Ai;

    instance-of v0, v0, LX/1fe;

    if-eqz v0, :cond_4

    .line 281170
    iget-object v0, p0, LX/1bp;->g:LX/1Ai;

    check-cast v0, LX/1fe;

    invoke-virtual {v0}, LX/1ff;->a()V

    .line 281171
    :goto_0
    iput-object v2, p0, LX/1bp;->h:LX/4AR;

    .line 281172
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    if-eqz v0, :cond_3

    .line 281173
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    invoke-interface {v0}, LX/1ag;->b()V

    .line 281174
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    invoke-interface {v0, v2}, LX/1ag;->a(Landroid/graphics/drawable/Drawable;)V

    .line 281175
    iput-object v2, p0, LX/1bp;->i:LX/1ag;

    .line 281176
    :cond_3
    iput-object v2, p0, LX/1bp;->j:Landroid/graphics/drawable/Drawable;

    .line 281177
    const/4 v0, 0x2

    invoke-static {v0}, LX/03J;->a(I)Z

    .line 281178
    iput-object p1, p0, LX/1bp;->k:Ljava/lang/String;

    .line 281179
    iput-object p2, p0, LX/1bp;->l:Ljava/lang/Object;

    .line 281180
    return-void

    .line 281181
    :cond_4
    iput-object v2, p0, LX/1bp;->g:LX/1Ai;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 281207
    const/4 v0, 0x2

    invoke-static {v0}, LX/03J;->a(I)Z

    .line 281208
    return-void
.end method

.method public static a(LX/1bp;Ljava/lang/String;LX/1ca;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/1ca",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 281209
    if-nez p2, :cond_1

    iget-object v1, p0, LX/1bp;->s:LX/1ca;

    if-nez v1, :cond_1

    .line 281210
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/1bp;->k:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1bp;->s:LX/1ca;

    if-ne p2, v1, :cond_2

    iget-boolean v1, p0, LX/1bp;->n:Z

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/1bp;Ljava/lang/String;LX/1ca;Ljava/lang/Object;FZZ)V
    .locals 6
    .param p2    # LX/1ca;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/1ca",
            "<TT;>;TT;FZZ)V"
        }
    .end annotation

    .prologue
    .line 281211
    invoke-static {p0, p1, p2}, LX/1bp;->a(LX/1bp;Ljava/lang/String;LX/1ca;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 281212
    const-string v0, "ignore_old_datasource @ onNewResult"

    invoke-direct {p0, v0, p3}, LX/1bp;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 281213
    invoke-virtual {p0, p3}, LX/1bp;->d(Ljava/lang/Object;)V

    .line 281214
    invoke-interface {p2}, LX/1ca;->g()Z

    .line 281215
    :cond_0
    :goto_0
    return-void

    .line 281216
    :cond_1
    iget-object v1, p0, LX/1bp;->b:LX/1ab;

    if-eqz p5, :cond_3

    sget-object v0, LX/1at;->ON_DATASOURCE_RESULT:LX/1at;

    :goto_1
    invoke-virtual {v1, v0}, LX/1ab;->a(LX/1at;)V

    .line 281217
    :try_start_0
    invoke-virtual {p0, p3}, LX/1bp;->a(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 281218
    iget-object v2, p0, LX/1bp;->t:Ljava/lang/Object;

    .line 281219
    iget-object v3, p0, LX/1bp;->u:Landroid/graphics/drawable/Drawable;

    .line 281220
    iput-object p3, p0, LX/1bp;->t:Ljava/lang/Object;

    .line 281221
    iput-object v1, p0, LX/1bp;->u:Landroid/graphics/drawable/Drawable;

    .line 281222
    if-eqz p5, :cond_4

    .line 281223
    :try_start_1
    const-string v0, "set_final_result @ onNewResult"

    invoke-direct {p0, v0, p3}, LX/1bp;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 281224
    const/4 v0, 0x0

    iput-object v0, p0, LX/1bp;->s:LX/1ca;

    .line 281225
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v0, v1, v4, p6}, LX/1ag;->a(Landroid/graphics/drawable/Drawable;FZ)V

    .line 281226
    invoke-direct {p0}, LX/1bp;->q()LX/1Ai;

    move-result-object v0

    invoke-virtual {p0, p3}, LX/1bp;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0}, LX/1bp;->f()Landroid/graphics/drawable/Animatable;

    move-result-object v5

    invoke-interface {v0, p1, v4, v5}, LX/1Ai;->a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 281227
    :goto_2
    if-eqz v3, :cond_2

    if-eq v3, v1, :cond_2

    .line 281228
    invoke-virtual {p0, v3}, LX/1bp;->b(Landroid/graphics/drawable/Drawable;)V

    .line 281229
    :cond_2
    if-eqz v2, :cond_0

    if-eq v2, p3, :cond_0

    .line 281230
    const-string v0, "release_previous_result @ onNewResult"

    invoke-direct {p0, v0, v2}, LX/1bp;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 281231
    invoke-virtual {p0, v2}, LX/1bp;->d(Ljava/lang/Object;)V

    goto :goto_0

    .line 281232
    :cond_3
    sget-object v0, LX/1at;->ON_DATASOURCE_RESULT_INT:LX/1at;

    goto :goto_1

    .line 281233
    :catch_0
    move-exception v0

    .line 281234
    const-string v1, "drawable_failed @ onNewResult"

    invoke-direct {p0, v1, p3}, LX/1bp;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 281235
    invoke-virtual {p0, p3}, LX/1bp;->d(Ljava/lang/Object;)V

    .line 281236
    invoke-static {p0, p1, p2, v0, p5}, LX/1bp;->a$redex0(LX/1bp;Ljava/lang/String;LX/1ca;Ljava/lang/Throwable;Z)V

    goto :goto_0

    .line 281237
    :cond_4
    :try_start_2
    const-string v0, "set_intermediate_result @ onNewResult"

    invoke-direct {p0, v0, p3}, LX/1bp;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 281238
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    invoke-interface {v0, v1, p4, p6}, LX/1ag;->a(Landroid/graphics/drawable/Drawable;FZ)V

    .line 281239
    invoke-direct {p0}, LX/1bp;->q()LX/1Ai;

    move-result-object v0

    invoke-virtual {p0, p3}, LX/1bp;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, p1, v4}, LX/1Ai;->b(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 281240
    :catchall_0
    move-exception v0

    if-eqz v3, :cond_5

    if-eq v3, v1, :cond_5

    .line 281241
    invoke-virtual {p0, v3}, LX/1bp;->b(Landroid/graphics/drawable/Drawable;)V

    .line 281242
    :cond_5
    if-eqz v2, :cond_6

    if-eq v2, p3, :cond_6

    .line 281243
    const-string v1, "release_previous_result @ onNewResult"

    invoke-direct {p0, v1, v2}, LX/1bp;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 281244
    invoke-virtual {p0, v2}, LX/1bp;->d(Ljava/lang/Object;)V

    :cond_6
    throw v0
.end method

.method public static a$redex0(LX/1bp;Ljava/lang/String;LX/1ca;Ljava/lang/Throwable;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/1ca",
            "<TT;>;",
            "Ljava/lang/Throwable;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 281245
    invoke-static {p0, p1, p2}, LX/1bp;->a(LX/1bp;Ljava/lang/String;LX/1ca;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 281246
    const-string v0, "ignore_old_datasource @ onFailure"

    invoke-static {v0, p3}, LX/1bp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 281247
    invoke-interface {p2}, LX/1ca;->g()Z

    .line 281248
    :goto_0
    return-void

    .line 281249
    :cond_0
    iget-object v1, p0, LX/1bp;->b:LX/1ab;

    if-eqz p4, :cond_1

    sget-object v0, LX/1at;->ON_DATASOURCE_FAILURE:LX/1at;

    :goto_1
    invoke-virtual {v1, v0}, LX/1ab;->a(LX/1at;)V

    .line 281250
    if-eqz p4, :cond_4

    .line 281251
    const-string v0, "final_failed @ onFailure"

    invoke-static {v0, p3}, LX/1bp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 281252
    const/4 v0, 0x0

    iput-object v0, p0, LX/1bp;->s:LX/1ca;

    .line 281253
    iput-boolean v3, p0, LX/1bp;->p:Z

    .line 281254
    iget-boolean v0, p0, LX/1bp;->q:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1bp;->u:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 281255
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    iget-object v1, p0, LX/1bp;->u:Landroid/graphics/drawable/Drawable;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v0, v1, v2, v3}, LX/1ag;->a(Landroid/graphics/drawable/Drawable;FZ)V

    .line 281256
    :goto_2
    invoke-direct {p0}, LX/1bp;->q()LX/1Ai;

    move-result-object v0

    iget-object v1, p0, LX/1bp;->k:Ljava/lang/String;

    invoke-interface {v0, v1, p3}, LX/1Ai;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 281257
    :cond_1
    sget-object v0, LX/1at;->ON_DATASOURCE_FAILURE_INT:LX/1at;

    goto :goto_1

    .line 281258
    :cond_2
    invoke-direct {p0}, LX/1bp;->r()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 281259
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    invoke-interface {v0, p3}, LX/1ag;->b(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 281260
    :cond_3
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    invoke-interface {v0, p3}, LX/1ag;->a(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 281261
    :cond_4
    const-string v0, "intermediate_failed @ onFailure"

    invoke-static {v0, p3}, LX/1bp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 281262
    invoke-direct {p0}, LX/1bp;->q()LX/1Ai;

    move-result-object v0

    iget-object v1, p0, LX/1bp;->k:Ljava/lang/String;

    invoke-interface {v0, v1, p3}, LX/1Ai;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    .line 281324
    invoke-static {v3}, LX/03J;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281325
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/1bp;->k:Ljava/lang/String;

    aput-object v2, v0, v1

    aput-object p1, v0, v3

    const/4 v1, 0x3

    .line 281326
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object v2, v2

    .line 281327
    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-virtual {p0, p2}, LX/1bp;->c(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 281328
    :cond_0
    return-void

    :cond_1
    const-string v2, "<null>"

    goto :goto_0
.end method

.method private q()LX/1Ai;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Ai",
            "<TINFO;>;"
        }
    .end annotation

    .prologue
    .line 281263
    iget-object v0, p0, LX/1bp;->g:LX/1Ai;

    if-nez v0, :cond_0

    .line 281264
    sget-object v0, LX/1Ah;->a:LX/1Ai;

    move-object v0, v0

    .line 281265
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1bp;->g:LX/1Ai;

    goto :goto_0
.end method

.method private r()Z
    .locals 2

    .prologue
    .line 281321
    iget-boolean v0, p0, LX/1bp;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1bp;->e:LX/1fD;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1bp;->e:LX/1fD;

    .line 281322
    iget-boolean v1, v0, LX/1fD;->a:Z

    if-eqz v1, :cond_1

    iget v1, v0, LX/1fD;->c:I

    iget p0, v0, LX/1fD;->b:I

    if-ge v1, p0, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 281323
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation
.end method

.method public final a(LX/1Ai;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ai",
            "<-TINFO;>;)V"
        }
    .end annotation

    .prologue
    .line 281309
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 281310
    iget-object v0, p0, LX/1bp;->g:LX/1Ai;

    instance-of v0, v0, LX/1fe;

    if-eqz v0, :cond_0

    .line 281311
    iget-object v0, p0, LX/1bp;->g:LX/1Ai;

    check-cast v0, LX/1fe;

    invoke-virtual {v0, p1}, LX/1ff;->a(LX/1Ai;)V

    .line 281312
    :goto_0
    return-void

    .line 281313
    :cond_0
    iget-object v0, p0, LX/1bp;->g:LX/1Ai;

    if-eqz v0, :cond_1

    .line 281314
    iget-object v0, p0, LX/1bp;->g:LX/1Ai;

    .line 281315
    new-instance v1, LX/1fe;

    invoke-direct {v1}, LX/1fe;-><init>()V

    .line 281316
    invoke-virtual {v1, v0}, LX/1ff;->a(LX/1Ai;)V

    .line 281317
    invoke-virtual {v1, p1}, LX/1ff;->a(LX/1Ai;)V

    .line 281318
    move-object v0, v1

    .line 281319
    iput-object v0, p0, LX/1bp;->g:LX/1Ai;

    goto :goto_0

    .line 281320
    :cond_1
    iput-object p1, p0, LX/1bp;->g:LX/1Ai;

    goto :goto_0
.end method

.method public final a(LX/1aY;)V
    .locals 3
    .param p1    # LX/1aY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 281295
    const/4 v0, 0x2

    invoke-static {v0}, LX/03J;->a(I)Z

    .line 281296
    iget-object v1, p0, LX/1bp;->b:LX/1ab;

    if-eqz p1, :cond_3

    sget-object v0, LX/1at;->ON_SET_HIERARCHY:LX/1at;

    :goto_0
    invoke-virtual {v1, v0}, LX/1ab;->a(LX/1at;)V

    .line 281297
    iget-boolean v0, p0, LX/1bp;->n:Z

    if-eqz v0, :cond_0

    .line 281298
    iget-object v0, p0, LX/1bp;->c:LX/1br;

    invoke-virtual {v0, p0}, LX/1br;->b(LX/1bp;)V

    .line 281299
    invoke-virtual {p0}, LX/1bp;->b()V

    .line 281300
    :cond_0
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    if-eqz v0, :cond_1

    .line 281301
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    invoke-interface {v0, v2}, LX/1ag;->a(Landroid/graphics/drawable/Drawable;)V

    .line 281302
    iput-object v2, p0, LX/1bp;->i:LX/1ag;

    .line 281303
    :cond_1
    if-eqz p1, :cond_2

    .line 281304
    instance-of v0, p1, LX/1ag;

    invoke-static {v0}, LX/03g;->a(Z)V

    .line 281305
    check-cast p1, LX/1ag;

    iput-object p1, p0, LX/1bp;->i:LX/1ag;

    .line 281306
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    iget-object v1, p0, LX/1bp;->j:Landroid/graphics/drawable/Drawable;

    invoke-interface {v0, v1}, LX/1ag;->a(Landroid/graphics/drawable/Drawable;)V

    .line 281307
    :cond_2
    return-void

    .line 281308
    :cond_3
    sget-object v0, LX/1at;->ON_CLEAR_HIERARCHY:LX/1at;

    goto :goto_0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 281291
    iput-object p1, p0, LX/1bp;->j:Landroid/graphics/drawable/Drawable;

    .line 281292
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    if-eqz v0, :cond_0

    .line 281293
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    iget-object v1, p0, LX/1bp;->j:Landroid/graphics/drawable/Drawable;

    invoke-interface {v0, v1}, LX/1ag;->a(Landroid/graphics/drawable/Drawable;)V

    .line 281294
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 281266
    const/4 v1, 0x2

    invoke-static {v1}, LX/03J;->a(I)Z

    .line 281267
    iget-object v1, p0, LX/1bp;->f:LX/1fE;

    if-nez v1, :cond_1

    .line 281268
    :cond_0
    :goto_0
    return v0

    .line 281269
    :cond_1
    iget-object v1, p0, LX/1bp;->f:LX/1fE;

    .line 281270
    iget-boolean v2, v1, LX/1fE;->c:Z

    move v1, v2

    .line 281271
    if-nez v1, :cond_2

    invoke-virtual {p0}, LX/1bp;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 281272
    :cond_2
    iget-object v0, p0, LX/1bp;->f:LX/1fE;

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 281273
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 281274
    :cond_3
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 281275
    :pswitch_0
    iput-boolean v7, v0, LX/1fE;->c:Z

    .line 281276
    iput-boolean v7, v0, LX/1fE;->d:Z

    .line 281277
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iput-wide v2, v0, LX/1fE;->e:J

    .line 281278
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, v0, LX/1fE;->f:F

    .line 281279
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, v0, LX/1fE;->g:F

    goto :goto_1

    .line 281280
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v3, v0, LX/1fE;->f:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, v0, LX/1fE;->b:F

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v3, v0, LX/1fE;->g:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, v0, LX/1fE;->b:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    .line 281281
    :cond_4
    iput-boolean v6, v0, LX/1fE;->d:Z

    goto :goto_1

    .line 281282
    :pswitch_2
    iput-boolean v6, v0, LX/1fE;->c:Z

    .line 281283
    :cond_5
    :goto_2
    iput-boolean v6, v0, LX/1fE;->d:Z

    goto :goto_1

    .line 281284
    :pswitch_3
    iput-boolean v6, v0, LX/1fE;->c:Z

    .line 281285
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v3, v0, LX/1fE;->f:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, v0, LX/1fE;->b:F

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v3, v0, LX/1fE;->g:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, v0, LX/1fE;->b:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_7

    .line 281286
    :cond_6
    iput-boolean v6, v0, LX/1fE;->d:Z

    .line 281287
    :cond_7
    iget-boolean v2, v0, LX/1fE;->d:Z

    if-eqz v2, :cond_5

    .line 281288
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iget-wide v4, v0, LX/1fE;->e:J

    sub-long/2addr v2, v4

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_5

    .line 281289
    iget-object v2, v0, LX/1fE;->a:LX/1bp;

    if-eqz v2, :cond_5

    .line 281290
    iget-object v2, v0, LX/1fE;->a:LX/1bp;

    invoke-virtual {v2}, LX/1bp;->onClick()Z

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public abstract b(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TINFO;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 281094
    iget-object v0, p0, LX/1bp;->b:LX/1ab;

    sget-object v1, LX/1at;->ON_RELEASE_CONTROLLER:LX/1at;

    invoke-virtual {v0, v1}, LX/1ab;->a(LX/1at;)V

    .line 281095
    iget-object v0, p0, LX/1bp;->e:LX/1fD;

    if-eqz v0, :cond_0

    .line 281096
    iget-object v0, p0, LX/1bp;->e:LX/1fD;

    invoke-virtual {v0}, LX/1fD;->c()V

    .line 281097
    :cond_0
    iget-object v0, p0, LX/1bp;->f:LX/1fE;

    if-eqz v0, :cond_1

    .line 281098
    iget-object v0, p0, LX/1bp;->f:LX/1fE;

    invoke-virtual {v0}, LX/1fE;->b()V

    .line 281099
    :cond_1
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    if-eqz v0, :cond_2

    .line 281100
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    invoke-interface {v0}, LX/1ag;->b()V

    .line 281101
    :cond_2
    invoke-direct {p0}, LX/1bp;->a()V

    .line 281102
    return-void
.end method

.method public abstract b(Landroid/graphics/drawable/Drawable;)V
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public c(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    .prologue
    .line 281103
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final c()LX/1aY;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 281104
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 281105
    const/4 v0, 0x2

    invoke-static {v0}, LX/03J;->a(I)Z

    .line 281106
    iget-object v0, p0, LX/1bp;->b:LX/1ab;

    sget-object v1, LX/1at;->ON_ATTACH_CONTROLLER:LX/1at;

    invoke-virtual {v0, v1}, LX/1ab;->a(LX/1at;)V

    .line 281107
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 281108
    iget-object v0, p0, LX/1bp;->c:LX/1br;

    invoke-virtual {v0, p0}, LX/1br;->b(LX/1bp;)V

    .line 281109
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1bp;->m:Z

    .line 281110
    iget-boolean v0, p0, LX/1bp;->n:Z

    if-nez v0, :cond_0

    .line 281111
    invoke-virtual {p0}, LX/1bp;->n()V

    .line 281112
    :cond_0
    return-void
.end method

.method public abstract d(Ljava/lang/Object;)V
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public e()V
    .locals 4

    .prologue
    .line 281113
    const/4 v0, 0x2

    invoke-static {v0}, LX/03J;->a(I)Z

    .line 281114
    iget-object v0, p0, LX/1bp;->b:LX/1ab;

    sget-object v1, LX/1at;->ON_DETACH_CONTROLLER:LX/1at;

    invoke-virtual {v0, v1}, LX/1ab;->a(LX/1at;)V

    .line 281115
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1bp;->m:Z

    .line 281116
    iget-object v0, p0, LX/1bp;->c:LX/1br;

    .line 281117
    invoke-static {}, LX/1br;->c()V

    .line 281118
    iget-object v1, v0, LX/1br;->b:Ljava/util/Set;

    invoke-interface {v1, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 281119
    :cond_0
    :goto_0
    return-void

    .line 281120
    :cond_1
    iget-object v1, v0, LX/1br;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 281121
    iget-object v1, v0, LX/1br;->c:Landroid/os/Handler;

    iget-object v2, v0, LX/1br;->d:Ljava/lang/Runnable;

    const v3, -0xd0c990e

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final f()Landroid/graphics/drawable/Animatable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 281122
    iget-object v0, p0, LX/1bp;->u:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/Animatable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1bp;->u:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/Animatable;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 281123
    iget-object v0, p0, LX/1bp;->r:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 281124
    iget-object v0, p0, LX/1bp;->l:Ljava/lang/Object;

    return-object v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 281125
    invoke-direct {p0}, LX/1bp;->r()Z

    move-result v0

    return v0
.end method

.method public final n()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 281126
    invoke-virtual {p0}, LX/1bp;->p()Ljava/lang/Object;

    move-result-object v3

    .line 281127
    if-eqz v3, :cond_0

    .line 281128
    const/4 v0, 0x0

    iput-object v0, p0, LX/1bp;->s:LX/1ca;

    .line 281129
    iput-boolean v5, p0, LX/1bp;->n:Z

    .line 281130
    iput-boolean v4, p0, LX/1bp;->p:Z

    .line 281131
    iget-object v0, p0, LX/1bp;->b:LX/1ab;

    sget-object v1, LX/1at;->ON_SUBMIT_CACHE_HIT:LX/1at;

    invoke-virtual {v0, v1}, LX/1ab;->a(LX/1at;)V

    .line 281132
    invoke-direct {p0}, LX/1bp;->q()LX/1Ai;

    move-result-object v0

    iget-object v1, p0, LX/1bp;->k:Ljava/lang/String;

    iget-object v2, p0, LX/1bp;->l:Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, LX/1Ai;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 281133
    iget-object v1, p0, LX/1bp;->k:Ljava/lang/String;

    iget-object v2, p0, LX/1bp;->s:LX/1ca;

    const/high16 v4, 0x3f800000    # 1.0f

    move-object v0, p0

    move v6, v5

    invoke-static/range {v0 .. v6}, LX/1bp;->a$redex0(LX/1bp;Ljava/lang/String;LX/1ca;Ljava/lang/Object;FZZ)V

    .line 281134
    :goto_0
    return-void

    .line 281135
    :cond_0
    iget-object v0, p0, LX/1bp;->b:LX/1ab;

    sget-object v1, LX/1at;->ON_DATASOURCE_SUBMIT:LX/1at;

    invoke-virtual {v0, v1}, LX/1ab;->a(LX/1at;)V

    .line 281136
    invoke-direct {p0}, LX/1bp;->q()LX/1Ai;

    move-result-object v0

    iget-object v1, p0, LX/1bp;->k:Ljava/lang/String;

    iget-object v2, p0, LX/1bp;->l:Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, LX/1Ai;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 281137
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    const/4 v1, 0x0

    invoke-interface {v0, v1, v5}, LX/1ag;->a(FZ)V

    .line 281138
    iput-boolean v5, p0, LX/1bp;->n:Z

    .line 281139
    iput-boolean v4, p0, LX/1bp;->p:Z

    .line 281140
    invoke-virtual {p0}, LX/1bp;->o()LX/1ca;

    move-result-object v0

    iput-object v0, p0, LX/1bp;->s:LX/1ca;

    .line 281141
    const/4 v0, 0x2

    invoke-static {v0}, LX/03J;->a(I)Z

    .line 281142
    iget-object v0, p0, LX/1bp;->k:Ljava/lang/String;

    .line 281143
    iget-object v1, p0, LX/1bp;->s:LX/1ca;

    invoke-interface {v1}, LX/1ca;->c()Z

    move-result v1

    .line 281144
    new-instance v2, LX/1ch;

    invoke-direct {v2, p0, v0, v1}, LX/1ch;-><init>(LX/1bp;Ljava/lang/String;Z)V

    .line 281145
    iget-object v0, p0, LX/1bp;->s:LX/1ca;

    iget-object v1, p0, LX/1bp;->d:Ljava/util/concurrent/Executor;

    invoke-interface {v0, v2, v1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public abstract o()LX/1ca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1ca",
            "<TT;>;"
        }
    .end annotation
.end method

.method public onClick()Z
    .locals 2

    .prologue
    .line 281146
    const/4 v0, 0x2

    invoke-static {v0}, LX/03J;->a(I)Z

    .line 281147
    invoke-direct {p0}, LX/1bp;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281148
    iget-object v0, p0, LX/1bp;->e:LX/1fD;

    .line 281149
    iget v1, v0, LX/1fD;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/1fD;->c:I

    .line 281150
    iget-object v0, p0, LX/1bp;->i:LX/1ag;

    invoke-interface {v0}, LX/1ag;->b()V

    .line 281151
    invoke-virtual {p0}, LX/1bp;->n()V

    .line 281152
    const/4 v0, 0x1

    .line 281153
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 281154
    const/4 v0, 0x0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 281155
    invoke-static {p0}, LX/15f;->a(Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "isAttached"

    iget-boolean v2, p0, LX/1bp;->m:Z

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Z)LX/15g;

    move-result-object v0

    const-string v1, "isRequestSubmitted"

    iget-boolean v2, p0, LX/1bp;->n:Z

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Z)LX/15g;

    move-result-object v0

    const-string v1, "hasFetchFailed"

    iget-boolean v2, p0, LX/1bp;->p:Z

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Z)LX/15g;

    move-result-object v0

    const-string v1, "fetchedImage"

    iget-object v2, p0, LX/1bp;->t:Ljava/lang/Object;

    invoke-virtual {p0, v2}, LX/1bp;->c(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;I)LX/15g;

    move-result-object v0

    const-string v1, "events"

    iget-object v2, p0, LX/1bp;->b:LX/1ab;

    invoke-virtual {v2}, LX/1ab;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    invoke-virtual {v0}, LX/15g;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
