.class public LX/0yq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 166244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(FFF)F
    .locals 1

    .prologue
    .line 166219
    sub-float v0, p1, p0

    mul-float/2addr v0, p2

    add-float/2addr v0, p0

    return v0
.end method

.method public static a(FFFFF)F
    .locals 2

    .prologue
    .line 166243
    sub-float v0, p0, p1

    sub-float v1, p2, p1

    div-float/2addr v0, v1

    sub-float v1, p4, p3

    mul-float/2addr v0, v1

    add-float/2addr v0, p3

    return v0
.end method

.method public static a(III)I
    .locals 2

    .prologue
    .line 166238
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 166239
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 166240
    invoke-static {v0, p0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 166241
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 166242
    return v0
.end method

.method public static varargs a(Ljava/lang/Comparable;[Ljava/lang/Comparable;)Ljava/lang/Comparable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<TT;>;>(TT;[TT;)TT;"
        }
    .end annotation

    .prologue
    .line 166234
    array-length v3, p1

    const/4 v0, 0x0

    move v2, v0

    move-object v1, p0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v0, p1, v2

    .line 166235
    invoke-interface {v1, v0}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v4

    if-gez v4, :cond_1

    .line 166236
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 166237
    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public static a(J)Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v6, 0x40

    const-wide/16 v4, 0x0

    .line 166226
    cmp-long v0, p0, v4

    if-gez v0, :cond_0

    .line 166227
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid number"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166228
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 166229
    :goto_0
    cmp-long v1, p0, v4

    if-lez v1, :cond_1

    .line 166230
    const-string v1, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"

    rem-long v2, p0, v6

    long-to-int v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 166231
    div-long/2addr p0, v6

    goto :goto_0

    .line 166232
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->reverse()Ljava/lang/StringBuilder;

    .line 166233
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(FFF)F
    .locals 2

    .prologue
    .line 166221
    invoke-static {p1, p2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 166222
    invoke-static {p1, p2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 166223
    invoke-static {v0, p0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 166224
    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 166225
    return v0
.end method

.method public static c(FFF)F
    .locals 2

    .prologue
    .line 166220
    sub-float v0, p0, p1

    sub-float v1, p2, p1

    div-float/2addr v0, v1

    return v0
.end method
