.class public abstract LX/0aw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/0Po",
        "<TT;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:LX/0Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private final f:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 85854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85855
    const/4 v0, 0x0

    iput-object v0, p0, LX/0aw;->a:LX/0Po;

    .line 85856
    iput v1, p0, LX/0aw;->b:I

    .line 85857
    iput v1, p0, LX/0aw;->c:I

    .line 85858
    iput v1, p0, LX/0aw;->d:I

    .line 85859
    iput v1, p0, LX/0aw;->e:I

    .line 85860
    iput p1, p0, LX/0aw;->f:I

    .line 85861
    return-void
.end method


# virtual methods
.method public abstract a()LX/0Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public final declared-synchronized a(LX/0Po;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 85873
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0aw;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0aw;->e:I

    .line 85874
    iget v0, p0, LX/0aw;->c:I

    iget v1, p0, LX/0aw;->f:I

    if-ge v0, v1, :cond_0

    .line 85875
    iget-object v0, p0, LX/0aw;->a:LX/0Po;

    invoke-interface {p1, v0}, LX/0Po;->a(Ljava/lang/Object;)V

    .line 85876
    iput-object p1, p0, LX/0aw;->a:LX/0Po;

    .line 85877
    iget v0, p0, LX/0aw;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0aw;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85878
    :cond_0
    monitor-exit p0

    return-void

    .line 85879
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/0Po;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 85862
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0aw;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0aw;->d:I

    .line 85863
    iget-object v0, p0, LX/0aw;->a:LX/0Po;

    if-nez v0, :cond_0

    .line 85864
    iget v0, p0, LX/0aw;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0aw;->b:I

    .line 85865
    invoke-virtual {p0}, LX/0aw;->a()LX/0Po;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 85866
    :goto_0
    monitor-exit p0

    return-object v0

    .line 85867
    :cond_0
    :try_start_1
    iget-object v1, p0, LX/0aw;->a:LX/0Po;

    .line 85868
    invoke-interface {v1}, LX/0Po;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Po;

    iput-object v0, p0, LX/0aw;->a:LX/0Po;

    .line 85869
    invoke-interface {v1}, LX/0Po;->b()V

    .line 85870
    iget v0, p0, LX/0aw;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0aw;->c:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    .line 85871
    goto :goto_0

    .line 85872
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
