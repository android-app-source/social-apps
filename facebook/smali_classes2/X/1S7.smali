.class public final LX/1S7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0yF;

.field public b:J

.field public c:F

.field public d:J

.field public e:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public g:J

.field public h:J

.field public i:F


# direct methods
.method public constructor <init>(LX/0yF;)V
    .locals 4

    .prologue
    const-wide/32 v2, 0x1d4c0

    .line 247718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247719
    iput-wide v2, p0, LX/1S7;->b:J

    .line 247720
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, LX/1S7;->c:F

    .line 247721
    const-wide/16 v0, 0x1388

    iput-wide v0, p0, LX/1S7;->d:J

    .line 247722
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/1S7;->e:LX/0am;

    .line 247723
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/1S7;->f:LX/0am;

    .line 247724
    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, LX/1S7;->g:J

    .line 247725
    iput-wide v2, p0, LX/1S7;->h:J

    .line 247726
    const v0, 0x3f2aaaab

    iput v0, p0, LX/1S7;->i:F

    .line 247727
    iput-object p1, p0, LX/1S7;->a:LX/0yF;

    .line 247728
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/location/FbLocationOperationParams;
    .locals 2

    .prologue
    .line 247729
    new-instance v0, Lcom/facebook/location/FbLocationOperationParams;

    invoke-direct {v0, p0}, Lcom/facebook/location/FbLocationOperationParams;-><init>(LX/1S7;)V

    return-object v0
.end method

.method public final c(J)LX/1S7;
    .locals 1

    .prologue
    .line 247730
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/1S7;->e:LX/0am;

    .line 247731
    return-object p0
.end method
