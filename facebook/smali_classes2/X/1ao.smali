.class public LX/1ao;
.super LX/1ah;
.source ""


# instance fields
.field public a:LX/1Up;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public c:Ljava/lang/Object;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public d:Landroid/graphics/PointF;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public e:I
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public f:I
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public g:Landroid/graphics/Matrix;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field private h:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;LX/1Up;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 278446
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0}, LX/1ah;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 278447
    const/4 v0, 0x0

    iput-object v0, p0, LX/1ao;->d:Landroid/graphics/PointF;

    .line 278448
    iput v1, p0, LX/1ao;->e:I

    .line 278449
    iput v1, p0, LX/1ao;->f:I

    .line 278450
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/1ao;->h:Landroid/graphics/Matrix;

    .line 278451
    iput-object p2, p0, LX/1ao;->a:LX/1Up;

    .line 278452
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 278453
    iget-object v0, p0, LX/1ao;->a:LX/1Up;

    instance-of v0, v0, LX/1gL;

    if-eqz v0, :cond_6

    .line 278454
    iget-object v0, p0, LX/1ao;->a:LX/1Up;

    check-cast v0, LX/1gL;

    invoke-interface {v0}, LX/1gL;->a()Ljava/lang/Object;

    move-result-object v3

    .line 278455
    if-eqz v3, :cond_0

    iget-object v0, p0, LX/1ao;->c:Ljava/lang/Object;

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_0
    move v0, v2

    .line 278456
    :goto_0
    iput-object v3, p0, LX/1ao;->c:Ljava/lang/Object;

    .line 278457
    :goto_1
    iget v3, p0, LX/1ao;->e:I

    invoke-virtual {p0}, LX/1ao;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    if-ne v3, v4, :cond_1

    iget v3, p0, LX/1ao;->f:I

    invoke-virtual {p0}, LX/1ao;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    if-eq v3, v4, :cond_2

    :cond_1
    move v1, v2

    .line 278458
    :cond_2
    if-nez v1, :cond_3

    if-eqz v0, :cond_4

    .line 278459
    :cond_3
    invoke-direct {p0}, LX/1ao;->d()V

    .line 278460
    :cond_4
    return-void

    :cond_5
    move v0, v1

    .line 278461
    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method private d()V
    .locals 9
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x0

    .line 278468
    invoke-virtual {p0}, LX/1ao;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 278469
    invoke-virtual {p0}, LX/1ao;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 278470
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 278471
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v5

    .line 278472
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    iput v3, p0, LX/1ao;->e:I

    .line 278473
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    iput v4, p0, LX/1ao;->f:I

    .line 278474
    if-lez v3, :cond_0

    if-gtz v4, :cond_1

    .line 278475
    :cond_0
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 278476
    iput-object v7, p0, LX/1ao;->g:Landroid/graphics/Matrix;

    .line 278477
    :goto_0
    return-void

    .line 278478
    :cond_1
    if-ne v3, v1, :cond_2

    if-ne v4, v5, :cond_2

    .line 278479
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 278480
    iput-object v7, p0, LX/1ao;->g:Landroid/graphics/Matrix;

    goto :goto_0

    .line 278481
    :cond_2
    iget-object v1, p0, LX/1ao;->a:LX/1Up;

    sget-object v5, LX/1Up;->a:LX/1Up;

    if-ne v1, v5, :cond_3

    .line 278482
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 278483
    iput-object v7, p0, LX/1ao;->g:Landroid/graphics/Matrix;

    goto :goto_0

    .line 278484
    :cond_3
    invoke-virtual {v0, v8, v8, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 278485
    iget-object v0, p0, LX/1ao;->a:LX/1Up;

    iget-object v1, p0, LX/1ao;->h:Landroid/graphics/Matrix;

    iget-object v5, p0, LX/1ao;->d:Landroid/graphics/PointF;

    if-eqz v5, :cond_5

    iget-object v5, p0, LX/1ao;->d:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    :goto_1
    iget-object v7, p0, LX/1ao;->d:Landroid/graphics/PointF;

    if-eqz v7, :cond_4

    iget-object v6, p0, LX/1ao;->d:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    :cond_4
    invoke-interface/range {v0 .. v6}, LX/1Up;->a(Landroid/graphics/Matrix;Landroid/graphics/Rect;IIFF)Landroid/graphics/Matrix;

    .line 278486
    iget-object v0, p0, LX/1ao;->h:Landroid/graphics/Matrix;

    iput-object v0, p0, LX/1ao;->g:Landroid/graphics/Matrix;

    goto :goto_0

    :cond_5
    move v5, v6

    .line 278487
    goto :goto_1
.end method


# virtual methods
.method public final a(LX/1Up;)V
    .locals 1

    .prologue
    .line 278462
    iget-object v0, p0, LX/1ao;->a:LX/1Up;

    invoke-static {v0, p1}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278463
    :goto_0
    return-void

    .line 278464
    :cond_0
    iput-object p1, p0, LX/1ao;->a:LX/1Up;

    .line 278465
    const/4 v0, 0x0

    iput-object v0, p0, LX/1ao;->c:Ljava/lang/Object;

    .line 278466
    invoke-direct {p0}, LX/1ao;->d()V

    .line 278467
    invoke-virtual {p0}, LX/1ao;->invalidateSelf()V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 1

    .prologue
    .line 278441
    invoke-virtual {p0, p1}, LX/1ah;->b(Landroid/graphics/Matrix;)V

    .line 278442
    invoke-direct {p0}, LX/1ao;->c()V

    .line 278443
    iget-object v0, p0, LX/1ao;->g:Landroid/graphics/Matrix;

    if-eqz v0, :cond_0

    .line 278444
    iget-object v0, p0, LX/1ao;->g:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 278445
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/PointF;)V
    .locals 1

    .prologue
    .line 278420
    iget-object v0, p0, LX/1ao;->d:Landroid/graphics/PointF;

    invoke-static {v0, p1}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278421
    :goto_0
    return-void

    .line 278422
    :cond_0
    iget-object v0, p0, LX/1ao;->d:Landroid/graphics/PointF;

    if-nez v0, :cond_1

    .line 278423
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, LX/1ao;->d:Landroid/graphics/PointF;

    .line 278424
    :cond_1
    iget-object v0, p0, LX/1ao;->d:Landroid/graphics/PointF;

    invoke-virtual {v0, p1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 278425
    invoke-direct {p0}, LX/1ao;->d()V

    .line 278426
    invoke-virtual {p0}, LX/1ao;->invalidateSelf()V

    goto :goto_0
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 278438
    invoke-super {p0, p1}, LX/1ah;->b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 278439
    invoke-direct {p0}, LX/1ao;->d()V

    .line 278440
    return-object v0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 278429
    invoke-direct {p0}, LX/1ao;->c()V

    .line 278430
    iget-object v0, p0, LX/1ao;->g:Landroid/graphics/Matrix;

    if-eqz v0, :cond_0

    .line 278431
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 278432
    invoke-virtual {p0}, LX/1ao;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 278433
    iget-object v1, p0, LX/1ao;->g:Landroid/graphics/Matrix;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 278434
    invoke-super {p0, p1}, LX/1ah;->draw(Landroid/graphics/Canvas;)V

    .line 278435
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 278436
    :goto_0
    return-void

    .line 278437
    :cond_0
    invoke-super {p0, p1}, LX/1ah;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 278427
    invoke-direct {p0}, LX/1ao;->d()V

    .line 278428
    return-void
.end method
