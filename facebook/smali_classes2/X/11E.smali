.class public LX/11E;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/0v6;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/11F;

.field private final b:LX/0sO;

.field private final c:LX/0lp;


# direct methods
.method public constructor <init>(LX/0lp;LX/0sO;LX/11F;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 170977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170978
    iput-object p1, p0, LX/11E;->c:LX/0lp;

    .line 170979
    iput-object p2, p0, LX/11E;->b:LX/0sO;

    .line 170980
    iput-object p3, p0, LX/11E;->a:LX/11F;

    .line 170981
    return-void
.end method

.method public static b(LX/0QB;)LX/11E;
    .locals 4

    .prologue
    .line 170881
    new-instance v3, LX/11E;

    invoke-static {p0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v0

    check-cast v0, LX/0lp;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v1

    check-cast v1, LX/0sO;

    invoke-static {p0}, LX/11F;->b(LX/0QB;)LX/11F;

    move-result-object v2

    check-cast v2, LX/11F;

    invoke-direct {v3, v0, v1, v2}, LX/11E;-><init>(LX/0lp;LX/0sO;LX/11F;)V

    .line 170882
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 170894
    check-cast p1, LX/0v6;

    .line 170895
    const-string v0, "GenericGraphQLBatchMethod.getRequest"

    const v1, 0x796c345f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 170896
    :try_start_0
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 170897
    iget-object v0, p0, LX/11E;->c:LX/0lp;

    invoke-virtual {v0, v1}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v2

    .line 170898
    invoke-virtual {v2}, LX/0nX;->f()V

    .line 170899
    iget-object v0, p1, LX/0v6;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zO;

    .line 170900
    invoke-virtual {p1, v0}, LX/0v6;->d(LX/0zO;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 170901
    invoke-virtual {v0}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v4

    .line 170902
    invoke-virtual {v2, v4}, LX/0nX;->g(Ljava/lang/String;)V

    .line 170903
    iget-object v4, v0, LX/0zO;->m:LX/0gW;

    move-object v4, v4

    .line 170904
    const-string v5, "query_id"

    .line 170905
    iget-object v6, v4, LX/0gW;->h:Ljava/lang/String;

    move-object v4, v6

    .line 170906
    invoke-virtual {v2, v5, v4}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 170907
    iget-object v4, p1, LX/0v6;->j:LX/0vU;

    move-object v4, v4

    .line 170908
    sget-object v5, LX/0vU;->PHASED:LX/0vU;

    if-ne v4, v5, :cond_1

    .line 170909
    const-string v4, "priority"

    .line 170910
    iget v5, v0, LX/0zO;->B:I

    move v5, v5

    .line 170911
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 170912
    :cond_1
    invoke-virtual {v0}, LX/0zO;->d()LX/0w7;

    move-result-object v4

    invoke-static {v4}, LX/0sO;->a(LX/0w7;)Ljava/lang/String;

    move-result-object v4

    .line 170913
    if-eqz v4, :cond_2

    .line 170914
    const-string v5, "query_params"

    invoke-virtual {v2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 170915
    invoke-virtual {v2, v4}, LX/0nX;->d(Ljava/lang/String;)V

    .line 170916
    :cond_2
    iget-object v4, p0, LX/11E;->b:LX/0sO;

    invoke-virtual {v0}, LX/0zO;->d()LX/0w7;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0sO;->b(LX/0w7;)Ljava/lang/String;

    move-result-object v4

    .line 170917
    if-eqz v4, :cond_3

    .line 170918
    const-string v5, "ref_params"

    invoke-virtual {v2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 170919
    invoke-virtual {v2, v4}, LX/0nX;->d(Ljava/lang/String;)V

    .line 170920
    :cond_3
    iget-object v4, p0, LX/11E;->b:LX/0sO;

    invoke-virtual {v0}, LX/0zO;->d()LX/0w7;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0sO;->c(LX/0w7;)Ljava/lang/String;

    move-result-object v0

    .line 170921
    if-eqz v0, :cond_4

    .line 170922
    const-string v4, "rerun_param"

    invoke-virtual {v2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 170923
    invoke-virtual {v2, v0}, LX/0nX;->d(Ljava/lang/String;)V

    .line 170924
    :cond_4
    invoke-virtual {v2}, LX/0nX;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 170925
    :catchall_0
    move-exception v0

    const v1, -0x231417f5

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 170926
    :cond_5
    :try_start_1
    invoke-virtual {v2}, LX/0nX;->g()V

    .line 170927
    invoke-virtual {v2}, LX/0nX;->flush()V

    .line 170928
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 170929
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 170930
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "queries"

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170931
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "method"

    const-string v3, "get"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170932
    iget-object v0, p1, LX/0v6;->j:LX/0vU;

    move-object v0, v0

    .line 170933
    if-eqz v0, :cond_6

    sget-object v2, LX/0vU;->UNSPECIFIED:LX/0vU;

    if-eq v0, v2, :cond_6

    .line 170934
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "scheduler"

    .line 170935
    iget-object v3, p1, LX/0v6;->j:LX/0vU;

    move-object v3, v3

    .line 170936
    iget-object v3, v3, LX/0vU;->schedulerName:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170937
    :cond_6
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "strip_nulls"

    const-string v3, "true"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170938
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "strip_defaults"

    const-string v3, "true"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170939
    iget-boolean v0, p1, LX/0v6;->g:Z

    move v0, v0

    .line 170940
    if-eqz v0, :cond_7

    .line 170941
    sget-object v0, LX/0sO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 170942
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 170943
    :cond_7
    iget-object v0, p1, LX/0v6;->c:Ljava/lang/String;

    move-object v0, v0

    .line 170944
    if-eqz v0, :cond_8

    .line 170945
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "batch_name"

    .line 170946
    iget-object v3, p1, LX/0v6;->c:Ljava/lang/String;

    move-object v3, v3

    .line 170947
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170948
    :cond_8
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    .line 170949
    iget-object v0, p1, LX/0v6;->c:Ljava/lang/String;

    move-object v0, v0

    .line 170950
    if-eqz v0, :cond_9

    .line 170951
    iget-object v0, p1, LX/0v6;->c:Ljava/lang/String;

    move-object v0, v0

    .line 170952
    :goto_2
    iput-object v0, v2, LX/14O;->b:Ljava/lang/String;

    .line 170953
    move-object v0, v2

    .line 170954
    const-string v2, "POST"

    .line 170955
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 170956
    move-object v0, v0

    .line 170957
    const-string v2, "graphqlbatch"

    .line 170958
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 170959
    move-object v0, v0

    .line 170960
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 170961
    move-object v0, v0

    .line 170962
    iget-object v1, p1, LX/0v6;->k:LX/0Px;

    move-object v1, v1

    .line 170963
    invoke-virtual {v0, v1}, LX/14O;->a(LX/0Px;)LX/14O;

    move-result-object v0

    .line 170964
    iget-boolean v1, p1, LX/0v6;->h:Z

    move v1, v1

    .line 170965
    iput-boolean v1, v0, LX/14O;->i:Z

    .line 170966
    move-object v0, v0

    .line 170967
    iget-boolean v1, p1, LX/0v6;->i:Z

    move v1, v1

    .line 170968
    iput-boolean v1, v0, LX/14O;->j:Z

    .line 170969
    move-object v0, v0

    .line 170970
    iget-boolean v1, p1, LX/0v6;->g:Z

    move v1, v1

    .line 170971
    if-eqz v1, :cond_a

    sget-object v1, LX/14S;->FLATBUFFER:LX/14S;

    :goto_3
    move-object v1, v1

    .line 170972
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 170973
    move-object v0, v0

    .line 170974
    sget-object v1, LX/14P;->RETRY_SAFE:LX/14P;

    invoke-virtual {v0, v1}, LX/14O;->a(LX/14P;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 170975
    const v1, -0x518183d8

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 170976
    :cond_9
    :try_start_2
    const-string v0, "GraphQLBatchRequest"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :cond_a
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    goto :goto_3
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 170883
    check-cast p1, LX/0v6;

    .line 170884
    const-string v0, "GenericGraphQLBatchMethod.parseResponse"

    const v1, 0x3fb5eb89

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 170885
    :try_start_0
    iget-object v0, p2, LX/1pN;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 170886
    instance-of v1, v0, Ljava/io/InputStream;

    if-eqz v1, :cond_0

    .line 170887
    iget-object v0, p0, LX/11E;->a:LX/11F;

    invoke-virtual {p2}, LX/1pN;->f()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/11F;->a(Ljava/io/InputStream;LX/0v6;)Ljava/lang/Void;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 170888
    const v1, -0x444d48e1

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_0
    return-object v0

    .line 170889
    :cond_0
    :try_start_1
    instance-of v0, v0, LX/15w;

    if-eqz v0, :cond_1

    .line 170890
    iget-object v0, p0, LX/11E;->a:LX/11F;

    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/11F;->a(LX/15w;LX/0v6;)Ljava/lang/Void;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 170891
    const v1, 0x5f37b6ce

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 170892
    :cond_1
    :try_start_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "responseObject should either be JsonParser or InputStream"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 170893
    :catchall_0
    move-exception v0

    const v1, 0x76b64bac

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
