.class public LX/1Dc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0So;

.field private b:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0So;)V
    .locals 1
    .param p1    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedAwakeTimeSinceBoot;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 217770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217771
    iput-object p1, p0, LX/1Dc;->a:LX/0So;

    .line 217772
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/1Dc;->b:Ljava/util/WeakHashMap;

    .line 217773
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/1Dc;->c:Ljava/util/WeakHashMap;

    .line 217774
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/1Dc;->d:Ljava/util/WeakHashMap;

    .line 217775
    return-void
.end method

.method private static a(Ljava/util/Map;Ljava/lang/Object;)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/Object;",
            ")J"
        }
    .end annotation

    .prologue
    .line 217732
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1Dc;
    .locals 4

    .prologue
    .line 217759
    const-class v1, LX/1Dc;

    monitor-enter v1

    .line 217760
    :try_start_0
    sget-object v0, LX/1Dc;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 217761
    sput-object v2, LX/1Dc;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 217762
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217763
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 217764
    new-instance p0, LX/1Dc;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-direct {p0, v3}, LX/1Dc;-><init>(LX/0So;)V

    .line 217765
    move-object v0, p0

    .line 217766
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 217767
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Dc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 217768
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 217769
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aA;Ljava/util/concurrent/Callable;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aA;",
            "Ljava/util/concurrent/Callable",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 217747
    iget-object v0, p0, LX/1Dc;->a:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 217748
    :try_start_0
    invoke-interface {p2}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 217749
    iget-object v3, p0, LX/1Dc;->a:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    .line 217750
    sub-long v0, v4, v0

    .line 217751
    sget-object v3, LX/1aK;->a:[I

    invoke-virtual {p1}, LX/1aA;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 217752
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 217753
    :catch_0
    move-exception v0

    .line 217754
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 217755
    :pswitch_0
    iget-object v3, p0, LX/1Dc;->b:Ljava/util/WeakHashMap;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217756
    :goto_0
    return-void

    .line 217757
    :pswitch_1
    iget-object v3, p0, LX/1Dc;->c:Ljava/util/WeakHashMap;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 217758
    :pswitch_2
    iget-object v3, p0, LX/1Dc;->d:Ljava/util/WeakHashMap;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Landroid/view/View;)V
    .locals 5

    .prologue
    .line 217733
    instance-of v0, p2, LX/1a8;

    if-nez v0, :cond_2

    .line 217734
    const/4 v0, 0x0

    .line 217735
    :goto_0
    move-object v0, v0

    .line 217736
    if-eqz v0, :cond_1

    .line 217737
    invoke-virtual {v0}, LX/1Rn;->c()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, LX/1Rn;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 217738
    :cond_0
    iget-object v1, p0, LX/1Dc;->d:Ljava/util/WeakHashMap;

    invoke-static {v1, p1}, LX/1Dc;->a(Ljava/util/Map;Ljava/lang/Object;)J

    move-result-wide v2

    .line 217739
    iput-wide v2, v0, LX/1Rn;->r:J

    .line 217740
    iget-object v1, p0, LX/1Dc;->b:Ljava/util/WeakHashMap;

    invoke-static {v1, p1}, LX/1Dc;->a(Ljava/util/Map;Ljava/lang/Object;)J

    move-result-wide v2

    .line 217741
    iput-wide v2, v0, LX/1Rn;->p:J

    .line 217742
    iget-object v1, p0, LX/1Dc;->c:Ljava/util/WeakHashMap;

    invoke-static {v1, p1}, LX/1Dc;->a(Ljava/util/Map;Ljava/lang/Object;)J

    move-result-wide v2

    .line 217743
    iput-wide v2, v0, LX/1Rn;->q:J

    .line 217744
    :cond_1
    iget-object v0, p0, LX/1Dc;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 217745
    iget-object v0, p0, LX/1Dc;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 217746
    return-void

    :cond_2
    check-cast p2, LX/1a8;

    invoke-interface {p2}, LX/1a8;->getViewDiagnostics()LX/1Rn;

    move-result-object v0

    goto :goto_0
.end method
