.class public LX/0kL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0kM;

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0kM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 127058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127059
    iput-object p2, p0, LX/0kL;->a:LX/0kM;

    .line 127060
    iput-object p1, p0, LX/0kL;->b:Landroid/content/Context;

    .line 127061
    return-void
.end method

.method public static a(LX/0QB;)LX/0kL;
    .locals 1

    .prologue
    .line 127057
    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/27k;Z)LX/27l;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 127001
    iget-object v0, p1, LX/27k;->a:Ljava/lang/CharSequence;

    move-object v0, v0

    .line 127002
    if-eqz v0, :cond_2

    .line 127003
    iget-object v0, p1, LX/27k;->a:Ljava/lang/CharSequence;

    move-object v1, v0

    .line 127004
    :goto_0
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/16 v3, 0x3c

    if-le v0, v3, :cond_4

    const/4 v0, 0x1

    .line 127005
    :goto_1
    iget-object v3, p0, LX/0kL;->b:Landroid/content/Context;

    invoke-static {v3, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    .line 127006
    iget v0, p1, LX/27k;->b:I

    move v0, v0

    .line 127007
    if-eqz v0, :cond_0

    .line 127008
    iget v0, p1, LX/27k;->b:I

    move v0, v0

    .line 127009
    invoke-virtual {v6, v0, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 127010
    :cond_0
    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 127011
    if-eqz p2, :cond_1

    .line 127012
    iget-object v0, p0, LX/0kL;->a:LX/0kM;

    .line 127013
    iget-object v2, p1, LX/27k;->f:Ljava/lang/String;

    move-object v2, v2

    .line 127014
    iget-object v3, p1, LX/27k;->e:Ljava/lang/String;

    move-object v3, v3

    .line 127015
    iget-object v4, p1, LX/27k;->g:Ljava/lang/String;

    move-object v4, v4

    .line 127016
    if-nez v4, :cond_5

    invoke-static {}, LX/0kL;->a()Ljava/lang/String;

    move-result-object v4

    .line 127017
    :goto_2
    iget-boolean v5, p1, LX/27k;->h:Z

    move v5, v5

    .line 127018
    invoke-virtual/range {v0 .. v5}, LX/0kM;->a(Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 127019
    :cond_1
    new-instance v0, LX/27l;

    invoke-direct {v0, v6}, LX/27l;-><init>(Landroid/widget/Toast;)V

    return-object v0

    .line 127020
    :cond_2
    iget-object v0, p1, LX/27k;->d:[Ljava/lang/Object;

    move-object v0, v0

    .line 127021
    if-eqz v0, :cond_3

    .line 127022
    iget-object v0, p0, LX/0kL;->b:Landroid/content/Context;

    .line 127023
    iget v1, p1, LX/27k;->c:I

    move v1, v1

    .line 127024
    iget-object v3, p1, LX/27k;->d:[Ljava/lang/Object;

    move-object v3, v3

    .line 127025
    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 127026
    :cond_3
    iget-object v0, p0, LX/0kL;->b:Landroid/content/Context;

    .line 127027
    iget v1, p1, LX/27k;->c:I

    move v1, v1

    .line 127028
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 127029
    goto :goto_1

    .line 127030
    :cond_5
    iget-object v4, p1, LX/27k;->g:Ljava/lang/String;

    move-object v4, v4

    .line 127031
    goto :goto_2
.end method

.method public static a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 127054
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    .line 127055
    invoke-virtual {v0}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    const/4 v1, 0x2

    aget-object v0, v0, v1

    .line 127056
    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 127050
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 127051
    invoke-static {}, LX/0kL;->a()Ljava/lang/String;

    move-result-object v5

    .line 127052
    const/4 v2, 0x0

    move-object v0, p0

    move-object v4, v3

    invoke-static/range {v0 .. v5}, LX/0kL;->a(Landroid/content/Context;Ljava/lang/CharSequence;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127053
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 127047
    invoke-static {}, LX/0kL;->a()Ljava/lang/String;

    move-result-object v5

    .line 127048
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    invoke-static/range {v0 .. v5}, LX/0kL;->a(Landroid/content/Context;Ljava/lang/CharSequence;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127049
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v5, 0x0

    .line 127036
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/16 v2, 0x3c

    if-le v1, v2, :cond_1

    move v1, v0

    .line 127037
    :goto_0
    if-eqz v1, :cond_2

    move v1, v0

    .line 127038
    :goto_1
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/0kM;->a(LX/0QB;)LX/0kM;

    move-result-object v0

    check-cast v0, LX/0kM;

    .line 127039
    invoke-static {p0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 127040
    if-eqz p2, :cond_0

    .line 127041
    invoke-virtual {v1, p2, v5, v5}, Landroid/widget/Toast;->setGravity(III)V

    .line 127042
    :cond_0
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    move-object v1, p1

    move-object v2, p4

    move-object v3, p3

    move-object v4, p5

    .line 127043
    invoke-virtual/range {v0 .. v5}, LX/0kM;->a(Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 127044
    return-void

    :cond_1
    move v1, v5

    .line 127045
    goto :goto_0

    :cond_2
    move v1, v5

    .line 127046
    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/0kL;
    .locals 3

    .prologue
    .line 127034
    new-instance v2, LX/0kL;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0kM;->a(LX/0QB;)LX/0kM;

    move-result-object v1

    check-cast v1, LX/0kM;

    invoke-direct {v2, v0, v1}, LX/0kL;-><init>(Landroid/content/Context;LX/0kM;)V

    .line 127035
    return-object v2
.end method


# virtual methods
.method public final a(LX/27k;)LX/27l;
    .locals 1

    .prologue
    .line 127033
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/0kL;->a(LX/27k;Z)LX/27l;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/27k;)LX/27l;
    .locals 1

    .prologue
    .line 127032
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/0kL;->a(LX/27k;Z)LX/27l;

    move-result-object v0

    return-object v0
.end method
