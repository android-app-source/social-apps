.class public final LX/0kJ;
.super LX/0T0;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0kJ;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 126958
    invoke-direct {p0}, LX/0T0;-><init>()V

    .line 126959
    return-void
.end method

.method public static a(LX/0QB;)LX/0kJ;
    .locals 3

    .prologue
    .line 126960
    sget-object v0, LX/0kJ;->a:LX/0kJ;

    if-nez v0, :cond_1

    .line 126961
    const-class v1, LX/0kJ;

    monitor-enter v1

    .line 126962
    :try_start_0
    sget-object v0, LX/0kJ;->a:LX/0kJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 126963
    if-eqz v2, :cond_0

    .line 126964
    :try_start_1
    new-instance v0, LX/0kJ;

    invoke-direct {v0}, LX/0kJ;-><init>()V

    .line 126965
    move-object v0, v0

    .line 126966
    sput-object v0, LX/0kJ;->a:LX/0kJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 126967
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 126968
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 126969
    :cond_1
    sget-object v0, LX/0kJ;->a:LX/0kJ;

    return-object v0

    .line 126970
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 126971
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 126972
    invoke-super {p0, p1, p2}, LX/0T0;->b(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 126973
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 126974
    const-string v1, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 126975
    if-eqz v0, :cond_0

    .line 126976
    new-instance v1, LX/41s;

    invoke-direct {v1, p1}, LX/41s;-><init>(Landroid/content/Context;)V

    .line 126977
    iget-object p0, v1, LX/41s;->a:LX/0SI;

    move-object v1, p0

    .line 126978
    invoke-interface {v1, v0}, LX/0SI;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 126979
    :cond_0
    return-void
.end method
