.class public final enum LX/1nY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1nY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1nY;

.field public static final enum API_ERROR:LX/1nY;

.field public static final enum CACHE_DISK_ERROR:LX/1nY;

.field public static final enum CANCELLED:LX/1nY;

.field public static final enum CONNECTION_FAILURE:LX/1nY;

.field public static final enum DATE_ERROR:LX/1nY;

.field public static final enum HTTP_400_AUTHENTICATION:LX/1nY;

.field public static final enum HTTP_400_OTHER:LX/1nY;

.field public static final enum HTTP_500_CLASS:LX/1nY;

.field public static final enum MQTT_SEND_FAILURE:LX/1nY;

.field public static final enum NO_ERROR:LX/1nY;

.field public static final enum ORCA_SERVICE_IPC_FAILURE:LX/1nY;

.field public static final enum ORCA_SERVICE_UNKNOWN_OPERATION:LX/1nY;

.field public static final enum OTHER:LX/1nY;

.field public static final enum OUT_OF_MEMORY:LX/1nY;

.field public static final enum SEGMENTED_TRANSCODE_ERROR:LX/1nY;

.field public static final enum WORK_AUTH_COMMUNITY_ID_REQUIRED:LX/1nY;

.field public static final enum WORK_AUTH_FAILED:LX/1nY;


# instance fields
.field private final mAsInt:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 316699
    new-instance v0, LX/1nY;

    const-string v1, "NO_ERROR"

    const v2, 0x186a1

    invoke-direct {v0, v1, v4, v2}, LX/1nY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1nY;->NO_ERROR:LX/1nY;

    .line 316700
    new-instance v0, LX/1nY;

    const-string v1, "API_ERROR"

    const v2, 0x186a2

    invoke-direct {v0, v1, v5, v2}, LX/1nY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1nY;->API_ERROR:LX/1nY;

    .line 316701
    new-instance v0, LX/1nY;

    const-string v1, "HTTP_400_AUTHENTICATION"

    const v2, 0x186a3

    invoke-direct {v0, v1, v6, v2}, LX/1nY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1nY;->HTTP_400_AUTHENTICATION:LX/1nY;

    .line 316702
    new-instance v0, LX/1nY;

    const-string v1, "HTTP_400_OTHER"

    const v2, 0x186a4

    invoke-direct {v0, v1, v7, v2}, LX/1nY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1nY;->HTTP_400_OTHER:LX/1nY;

    .line 316703
    new-instance v0, LX/1nY;

    const-string v1, "HTTP_500_CLASS"

    const v2, 0x186a5

    invoke-direct {v0, v1, v8, v2}, LX/1nY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1nY;->HTTP_500_CLASS:LX/1nY;

    .line 316704
    new-instance v0, LX/1nY;

    const-string v1, "CONNECTION_FAILURE"

    const/4 v2, 0x5

    const v3, 0x186a6

    invoke-direct {v0, v1, v2, v3}, LX/1nY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    .line 316705
    new-instance v0, LX/1nY;

    const-string v1, "ORCA_SERVICE_UNKNOWN_OPERATION"

    const/4 v2, 0x6

    const v3, 0x186a7

    invoke-direct {v0, v1, v2, v3}, LX/1nY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1nY;->ORCA_SERVICE_UNKNOWN_OPERATION:LX/1nY;

    .line 316706
    new-instance v0, LX/1nY;

    const-string v1, "ORCA_SERVICE_IPC_FAILURE"

    const/4 v2, 0x7

    const v3, 0x186a8

    invoke-direct {v0, v1, v2, v3}, LX/1nY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1nY;->ORCA_SERVICE_IPC_FAILURE:LX/1nY;

    .line 316707
    new-instance v0, LX/1nY;

    const-string v1, "OUT_OF_MEMORY"

    const/16 v2, 0x8

    const v3, 0x186a9

    invoke-direct {v0, v1, v2, v3}, LX/1nY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1nY;->OUT_OF_MEMORY:LX/1nY;

    .line 316708
    new-instance v0, LX/1nY;

    const-string v1, "OTHER"

    const/16 v2, 0x9

    const v3, 0x186aa

    invoke-direct {v0, v1, v2, v3}, LX/1nY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1nY;->OTHER:LX/1nY;

    .line 316709
    new-instance v0, LX/1nY;

    const-string v1, "CANCELLED"

    const/16 v2, 0xa

    const v3, 0x186ab

    invoke-direct {v0, v1, v2, v3}, LX/1nY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1nY;->CANCELLED:LX/1nY;

    .line 316710
    new-instance v0, LX/1nY;

    const-string v1, "CACHE_DISK_ERROR"

    const/16 v2, 0xb

    const v3, 0x186ac

    invoke-direct {v0, v1, v2, v3}, LX/1nY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1nY;->CACHE_DISK_ERROR:LX/1nY;

    .line 316711
    new-instance v0, LX/1nY;

    const-string v1, "MQTT_SEND_FAILURE"

    const/16 v2, 0xc

    const v3, 0x186ad

    invoke-direct {v0, v1, v2, v3}, LX/1nY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1nY;->MQTT_SEND_FAILURE:LX/1nY;

    .line 316712
    new-instance v0, LX/1nY;

    const-string v1, "WORK_AUTH_FAILED"

    const/16 v2, 0xd

    const v3, 0x186ae

    invoke-direct {v0, v1, v2, v3}, LX/1nY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1nY;->WORK_AUTH_FAILED:LX/1nY;

    .line 316713
    new-instance v0, LX/1nY;

    const-string v1, "WORK_AUTH_COMMUNITY_ID_REQUIRED"

    const/16 v2, 0xe

    const v3, 0x186af

    invoke-direct {v0, v1, v2, v3}, LX/1nY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1nY;->WORK_AUTH_COMMUNITY_ID_REQUIRED:LX/1nY;

    .line 316714
    new-instance v0, LX/1nY;

    const-string v1, "DATE_ERROR"

    const/16 v2, 0xf

    const v3, 0x186b0

    invoke-direct {v0, v1, v2, v3}, LX/1nY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1nY;->DATE_ERROR:LX/1nY;

    .line 316715
    new-instance v0, LX/1nY;

    const-string v1, "SEGMENTED_TRANSCODE_ERROR"

    const/16 v2, 0x10

    const v3, 0x186b1

    invoke-direct {v0, v1, v2, v3}, LX/1nY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1nY;->SEGMENTED_TRANSCODE_ERROR:LX/1nY;

    .line 316716
    const/16 v0, 0x11

    new-array v0, v0, [LX/1nY;

    sget-object v1, LX/1nY;->NO_ERROR:LX/1nY;

    aput-object v1, v0, v4

    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    aput-object v1, v0, v5

    sget-object v1, LX/1nY;->HTTP_400_AUTHENTICATION:LX/1nY;

    aput-object v1, v0, v6

    sget-object v1, LX/1nY;->HTTP_400_OTHER:LX/1nY;

    aput-object v1, v0, v7

    sget-object v1, LX/1nY;->HTTP_500_CLASS:LX/1nY;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1nY;->ORCA_SERVICE_UNKNOWN_OPERATION:LX/1nY;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/1nY;->ORCA_SERVICE_IPC_FAILURE:LX/1nY;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/1nY;->OUT_OF_MEMORY:LX/1nY;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/1nY;->OTHER:LX/1nY;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/1nY;->CANCELLED:LX/1nY;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/1nY;->CACHE_DISK_ERROR:LX/1nY;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/1nY;->MQTT_SEND_FAILURE:LX/1nY;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/1nY;->WORK_AUTH_FAILED:LX/1nY;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/1nY;->WORK_AUTH_COMMUNITY_ID_REQUIRED:LX/1nY;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/1nY;->DATE_ERROR:LX/1nY;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/1nY;->SEGMENTED_TRANSCODE_ERROR:LX/1nY;

    aput-object v2, v0, v1

    sput-object v0, LX/1nY;->$VALUES:[LX/1nY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 316695
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 316696
    iput p3, p0, LX/1nY;->mAsInt:I

    .line 316697
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1nY;
    .locals 1

    .prologue
    .line 316717
    const-class v0, LX/1nY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1nY;

    return-object v0
.end method

.method public static values()[LX/1nY;
    .locals 1

    .prologue
    .line 316698
    sget-object v0, LX/1nY;->$VALUES:[LX/1nY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1nY;

    return-object v0
.end method


# virtual methods
.method public final getAsInt()I
    .locals 1

    .prologue
    .line 316694
    iget v0, p0, LX/1nY;->mAsInt:I

    return v0
.end method
