.class public LX/1Ap;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 211189
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 211190
    sget-object v0, LX/1Ap;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 211191
    const-class v1, LX/1Ap;

    monitor-enter v1

    .line 211192
    :try_start_0
    sget-object v0, LX/1Ap;->a:Ljava/lang/Boolean;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 211193
    if-eqz v2, :cond_0

    .line 211194
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 211195
    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object p0

    check-cast p0, LX/0W3;

    invoke-static {p0}, LX/1Aq;->a(LX/0W3;)Ljava/lang/Boolean;

    move-result-object p0

    move-object v0, p0

    .line 211196
    sput-object v0, LX/1Ap;->a:Ljava/lang/Boolean;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211197
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 211198
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 211199
    :cond_1
    sget-object v0, LX/1Ap;->a:Ljava/lang/Boolean;

    return-object v0

    .line 211200
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 211201
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 211202
    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v0

    check-cast v0, LX/0W3;

    invoke-static {v0}, LX/1Aq;->a(LX/0W3;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
