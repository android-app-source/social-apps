.class public final LX/17L;
.super LX/0ur;
.source ""


# instance fields
.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Z

.field public e:Z

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 197285
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 197286
    instance-of v0, p0, LX/17L;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 197287
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPageInfo;)LX/17L;
    .locals 2

    .prologue
    .line 197276
    new-instance v0, LX/17L;

    invoke-direct {v0}, LX/17L;-><init>()V

    .line 197277
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 197278
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/17L;->b:Ljava/lang/String;

    .line 197279
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/17L;->c:Ljava/lang/String;

    .line 197280
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v1

    iput-boolean v1, v0, LX/17L;->d:Z

    .line 197281
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->c()Z

    move-result v1

    iput-boolean v1, v0, LX/17L;->e:Z

    .line 197282
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->p_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/17L;->f:Ljava/lang/String;

    .line 197283
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 197284
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/17L;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 197274
    iput-object p1, p0, LX/17L;->c:Ljava/lang/String;

    .line 197275
    return-object p0
.end method

.method public final a(Z)LX/17L;
    .locals 0

    .prologue
    .line 197272
    iput-boolean p1, p0, LX/17L;->d:Z

    .line 197273
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 2

    .prologue
    .line 197270
    new-instance v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;-><init>(LX/17L;)V

    .line 197271
    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/17L;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 197266
    iput-object p1, p0, LX/17L;->f:Ljava/lang/String;

    .line 197267
    return-object p0
.end method

.method public final b(Z)LX/17L;
    .locals 0

    .prologue
    .line 197268
    iput-boolean p1, p0, LX/17L;->e:Z

    .line 197269
    return-object p0
.end method
