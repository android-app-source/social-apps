.class public final LX/1aE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1Pn;

.field public final synthetic b:LX/1EE;

.field public final synthetic c:Landroid/app/Activity;

.field public final synthetic d:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;LX/1Pn;LX/1EE;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 276632
    iput-object p1, p0, LX/1aE;->d:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;

    iput-object p2, p0, LX/1aE;->a:LX/1Pn;

    iput-object p3, p0, LX/1aE;->b:LX/1EE;

    iput-object p4, p0, LX/1aE;->c:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x6dc61fa3

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 276633
    iget-object v0, p0, LX/1aE;->d:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->j:LX/1Np;

    invoke-virtual {v0}, LX/1Np;->b()V

    .line 276634
    iget-object v0, p0, LX/1aE;->d:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;

    iget-object v2, v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->i:LX/1Ns;

    iget-object v0, p0, LX/1aE;->a:LX/1Pn;

    check-cast v0, LX/1Qi;

    .line 276635
    iget-object v3, v0, LX/1Qi;->p:LX/1Qh;

    move-object v3, v3

    .line 276636
    iget-object v0, p0, LX/1aE;->a:LX/1Pn;

    check-cast v0, LX/1Qi;

    .line 276637
    iget-object v4, v0, LX/1Qi;->o:LX/1DQ;

    move-object v0, v4

    .line 276638
    invoke-virtual {v2, v3, v0}, LX/1Ns;->a(LX/1Qh;LX/1DQ;)LX/1aw;

    move-result-object v2

    .line 276639
    iget-object v0, p0, LX/1aE;->b:LX/1EE;

    invoke-virtual {v0}, LX/1EE;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1aE;->d:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->k:LX/1Nq;

    iget-object v3, p0, LX/1aE;->b:LX/1EE;

    .line 276640
    iget-object v4, v3, LX/1EE;->i:Ljava/lang/String;

    move-object v3, v4

    .line 276641
    invoke-static {v3}, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;->a(Ljava/lang/String;)Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v0

    .line 276642
    :goto_0
    const/4 v3, 0x0

    const-string v4, "inlineComposerStatusButton"

    iget-object v5, p0, LX/1aE;->c:Landroid/app/Activity;

    invoke-virtual {v2, v3, v4, v0, v5}, LX/1aw;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;Landroid/app/Activity;)V

    .line 276643
    const v0, 0x45d5f75

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 276644
    :cond_0
    iget-object v0, p0, LX/1aE;->d:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->c(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v0

    goto :goto_0
.end method
