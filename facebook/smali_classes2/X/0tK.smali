.class public LX/0tK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/0tK;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13h;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0x7;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0x7;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private h:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 154424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154425
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 154426
    iput-object v0, p0, LX/0tK;->a:LX/0Ot;

    .line 154427
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 154428
    iput-object v0, p0, LX/0tK;->b:LX/0Ot;

    .line 154429
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 154430
    iput-object v0, p0, LX/0tK;->c:LX/0Ot;

    .line 154431
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 154432
    iput-object v0, p0, LX/0tK;->d:LX/0Ot;

    .line 154433
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 154434
    iput-object v0, p0, LX/0tK;->e:LX/0Ot;

    .line 154435
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 154436
    iput-object v0, p0, LX/0tK;->f:LX/0Ot;

    .line 154437
    return-void
.end method

.method public static a(LX/0QB;)LX/0tK;
    .locals 10

    .prologue
    .line 154407
    sget-object v0, LX/0tK;->i:LX/0tK;

    if-nez v0, :cond_1

    .line 154408
    const-class v1, LX/0tK;

    monitor-enter v1

    .line 154409
    :try_start_0
    sget-object v0, LX/0tK;->i:LX/0tK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 154410
    if-eqz v2, :cond_0

    .line 154411
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 154412
    new-instance v3, LX/0tK;

    invoke-direct {v3}, LX/0tK;-><init>()V

    .line 154413
    const/16 v4, 0xf9a

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xac0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1032

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x494

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2ca

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 154414
    new-instance v9, LX/0tL;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-direct {v9, p0}, LX/0tL;-><init>(LX/0QB;)V

    move-object v9, v9

    .line 154415
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-static {v9, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v9

    move-object v9, v9

    .line 154416
    iput-object v4, v3, LX/0tK;->a:LX/0Ot;

    iput-object v5, v3, LX/0tK;->b:LX/0Ot;

    iput-object v6, v3, LX/0tK;->c:LX/0Ot;

    iput-object v7, v3, LX/0tK;->d:LX/0Ot;

    iput-object v8, v3, LX/0tK;->e:LX/0Ot;

    iput-object v9, v3, LX/0tK;->f:LX/0Ot;

    .line 154417
    move-object v0, v3

    .line 154418
    sput-object v0, LX/0tK;->i:LX/0tK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154419
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 154420
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 154421
    :cond_1
    sget-object v0, LX/0tK;->i:LX/0tK;

    return-object v0

    .line 154422
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 154423
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static s(LX/0tK;)V
    .locals 1

    .prologue
    .line 154403
    invoke-virtual {p0}, LX/0tK;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0tK;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154404
    invoke-virtual {p0}, LX/0tK;->d()V

    .line 154405
    invoke-virtual {p0}, LX/0tK;->e()V

    .line 154406
    :cond_0
    return-void
.end method

.method private declared-synchronized t()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0x7;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154399
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0tK;->g:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 154400
    iget-object v0, p0, LX/0tK;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/0tK;->g:Ljava/util/Set;

    .line 154401
    :cond_0
    iget-object v0, p0, LX/0tK;->g:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 154402
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/0x7;)V
    .locals 1

    .prologue
    .line 154396
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/0tK;->t()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154397
    monitor-exit p0

    return-void

    .line 154398
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Z)Z
    .locals 2

    .prologue
    .line 154395
    invoke-virtual {p0}, LX/0tK;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0tK;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0tK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2nr;->b:LX/0Tn;

    invoke-interface {v0, v1, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/49B;
    .locals 3

    .prologue
    .line 154389
    invoke-virtual {p0}, LX/0tK;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0tK;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget v1, LX/0wm;->z:I

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 154390
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 154391
    sget-object v0, LX/49B;->SMALL:LX/49B;

    :goto_1
    return-object v0

    .line 154392
    :cond_0
    iget-object v0, p0, LX/0tK;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget v1, LX/0wm;->f:I

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    goto :goto_0

    .line 154393
    :pswitch_0
    sget-object v0, LX/49B;->MEDIUM:LX/49B;

    goto :goto_1

    .line 154394
    :pswitch_1
    sget-object v0, LX/49B;->NO_CHANGE:LX/49B;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final declared-synchronized b(LX/0x7;)V
    .locals 1

    .prologue
    .line 154386
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/0tK;->t()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154387
    monitor-exit p0

    return-void

    .line 154388
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 154383
    sget-object v0, LX/2nr;->d:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 154384
    iget-object v1, p0, LX/0tK;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 154385
    return-void
.end method

.method public final b(Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 154380
    invoke-virtual {p0, v0}, LX/0tK;->a(Z)Z

    move-result v1

    .line 154381
    invoke-virtual {p0, p1}, LX/0tK;->c(Z)Z

    move-result v2

    .line 154382
    if-eqz v1, :cond_1

    if-eqz v2, :cond_0

    invoke-virtual {p0}, LX/0tK;->c()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 154378
    iget-object v0, p0, LX/0tK;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kb;

    invoke-virtual {v0}, LX/0kb;->b()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 154379
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Z)Z
    .locals 2

    .prologue
    .line 154438
    invoke-virtual {p0}, LX/0tK;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0tK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2nr;->c:LX/0Tn;

    invoke-interface {v0, v1, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized d()V
    .locals 2

    .prologue
    .line 154313
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/0tK;->t()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x7;

    .line 154314
    invoke-interface {v0}, LX/0x7;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 154315
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 154316
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final d(Z)V
    .locals 3

    .prologue
    .line 154318
    iget-object v0, p0, LX/0tK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2nr;->b:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 154319
    if-ne v0, p1, :cond_0

    .line 154320
    :goto_0
    return-void

    .line 154321
    :cond_0
    invoke-virtual {p0}, LX/0tK;->d()V

    .line 154322
    if-eqz p1, :cond_1

    .line 154323
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/0tK;->g(Z)V

    .line 154324
    :cond_1
    iget-object v0, p0, LX/0tK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 154325
    sget-object v1, LX/2nr;->b:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 154326
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 154327
    if-eqz p1, :cond_2

    .line 154328
    iget-object v0, p0, LX/0tK;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13h;

    invoke-virtual {v0}, LX/13h;->e()V

    .line 154329
    :goto_1
    invoke-virtual {p0}, LX/0tK;->e()V

    goto :goto_0

    .line 154330
    :cond_2
    iget-object v0, p0, LX/0tK;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13h;

    invoke-virtual {v0}, LX/13h;->f()V

    goto :goto_1
.end method

.method public final declared-synchronized e()V
    .locals 2

    .prologue
    .line 154331
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/0tK;->t()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x7;

    .line 154332
    invoke-interface {v0}, LX/0x7;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 154333
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 154334
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final f(Z)V
    .locals 2

    .prologue
    .line 154335
    invoke-virtual {p0}, LX/0tK;->d()V

    .line 154336
    iget-object v0, p0, LX/0tK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 154337
    sget-object v1, LX/2nr;->c:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 154338
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 154339
    if-eqz p1, :cond_0

    .line 154340
    iget-object v0, p0, LX/0tK;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13h;

    invoke-virtual {v0}, LX/13h;->i()V

    .line 154341
    :goto_0
    invoke-virtual {p0}, LX/0tK;->e()V

    .line 154342
    return-void

    .line 154343
    :cond_0
    iget-object v0, p0, LX/0tK;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13h;

    invoke-virtual {v0}, LX/13h;->j()V

    goto :goto_0
.end method

.method public final f()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 154344
    invoke-virtual {p0}, LX/0tK;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0tK;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/0wm;->N:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/0tK;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v3, LX/0wm;->t:S

    invoke-interface {v0, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/0tK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v3, 0x47d

    invoke-virtual {v0, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final g(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 154345
    invoke-virtual {p0}, LX/0tK;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154346
    iget-object v0, p0, LX/0tK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    .line 154347
    sget-object v3, LX/2nr;->e:LX/0Tn;

    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v2, v3, v0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 154348
    if-eqz p1, :cond_2

    .line 154349
    sget-object v0, LX/2nr;->b:LX/0Tn;

    invoke-interface {v2, v0, v1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 154350
    iget-object v0, p0, LX/0tK;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13h;

    .line 154351
    sget-object v1, LX/499;->SETTINGS_DSM_ON:LX/499;

    invoke-static {v0, v1}, LX/13h;->a(LX/13h;LX/499;)V

    .line 154352
    :goto_1
    invoke-interface {v2}, LX/0hN;->commit()V

    .line 154353
    :cond_0
    return-void

    .line 154354
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 154355
    :cond_2
    iget-object v0, p0, LX/0tK;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13h;

    .line 154356
    sget-object v1, LX/499;->SETTINGS_DSM_OFF:LX/499;

    invoke-static {v0, v1}, LX/13h;->a(LX/13h;LX/499;)V

    .line 154357
    goto :goto_1
.end method

.method public final g()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 154317
    invoke-virtual {p0}, LX/0tK;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0tK;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/0wm;->H:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/0tK;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/0wm;->n:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/0tK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v2, 0x480

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final h()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 154358
    invoke-virtual {p0}, LX/0tK;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0tK;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/0wm;->u:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/0tK;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v3, LX/0wm;->a:S

    invoke-interface {v0, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/0tK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v3, 0x47e

    invoke-virtual {v0, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final l()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 154359
    invoke-virtual {p0}, LX/0tK;->m()Z

    move-result v0

    if-nez v0, :cond_1

    .line 154360
    :cond_0
    :goto_0
    return v1

    .line 154361
    :cond_1
    iget-object v0, p0, LX/0tK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2nr;->e:LX/0Tn;

    invoke-interface {v0, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;)LX/03R;

    move-result-object v0

    .line 154362
    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v3

    if-nez v3, :cond_3

    .line 154363
    iget-object v0, p0, LX/0tK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2nr;->b:LX/0Tn;

    invoke-interface {v0, v3, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    .line 154364
    iget-object v0, p0, LX/0tK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    .line 154365
    sget-object v5, LX/2nr;->e:LX/0Tn;

    if-nez v3, :cond_2

    move v0, v1

    :goto_1
    invoke-interface {v4, v5, v0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 154366
    invoke-interface {v4}, LX/0hN;->commit()V

    move v1, v3

    .line 154367
    goto :goto_0

    :cond_2
    move v0, v2

    .line 154368
    goto :goto_1

    .line 154369
    :cond_3
    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public final m()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 154370
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, p0, LX/0tK;->h:Ljava/lang/Boolean;

    invoke-virtual {v0, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 154371
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/0tK;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0tK;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/0wm;->L:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/0tK;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/0wm;->r:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0
.end method

.method public final o()V
    .locals 2

    .prologue
    .line 154372
    iget-object v0, p0, LX/0tK;->h:Ljava/lang/Boolean;

    .line 154373
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, LX/0tK;->h:Ljava/lang/Boolean;

    .line 154374
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0tK;->h:Ljava/lang/Boolean;

    invoke-virtual {v1, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 154375
    :cond_0
    invoke-static {p0}, LX/0tK;->s(LX/0tK;)V

    .line 154376
    :cond_1
    return-void
.end method

.method public final q()Z
    .locals 3

    .prologue
    .line 154377
    iget-object v0, p0, LX/0tK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x393

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
