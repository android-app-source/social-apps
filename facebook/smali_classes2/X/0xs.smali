.class public LX/0xs;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 163510
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, LX/0xs;->a:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 163511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163512
    return-void
.end method

.method private static a(Landroid/content/Context;LX/0xq;LX/0xr;)Landroid/graphics/Typeface;
    .locals 2

    .prologue
    .line 163513
    const/4 v1, 0x0

    .line 163514
    sget-object v0, LX/0xq;->ROBOTO:LX/0xq;

    invoke-virtual {p1, v0}, LX/0xq;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 163515
    sget-object v0, LX/0xr;->REGULAR:LX/0xr;

    if-ne p2, v0, :cond_2

    .line 163516
    const-string v0, "sans-serif"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 163517
    :goto_0
    move-object v0, v0

    .line 163518
    if-nez v0, :cond_1

    .line 163519
    iget-object v0, p1, LX/0xq;->paths:[Ljava/lang/String;

    invoke-virtual {p2}, LX/0xr;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    .line 163520
    sget-object v1, LX/0xs;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Typeface;

    .line 163521
    if-nez v1, :cond_0

    .line 163522
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    .line 163523
    sget-object p1, LX/0xs;->a:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163524
    :cond_0
    move-object v0, v1

    .line 163525
    :cond_1
    return-object v0

    .line 163526
    :cond_2
    sget-object v0, LX/0xr;->BOLD:LX/0xr;

    if-ne p2, v0, :cond_3

    .line 163527
    const-string v0, "sans-serif"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0

    .line 163528
    :cond_3
    sget-object v0, LX/0xr;->LIGHT:LX/0xr;

    if-ne p2, v0, :cond_4

    .line 163529
    const-string v0, "sans-serif-light"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0

    .line 163530
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;
    .locals 1
    .param p3    # Landroid/graphics/Typeface;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 163531
    sget-object v0, LX/0xq;->BUILTIN:LX/0xq;

    if-ne p1, v0, :cond_0

    .line 163532
    :goto_0
    return-object p3

    .line 163533
    :cond_0
    sget-object v0, LX/0xr;->BUILTIN:LX/0xr;

    if-ne p2, v0, :cond_2

    .line 163534
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Landroid/graphics/Typeface;->isBold()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163535
    sget-object v0, LX/0xr;->BOLD:LX/0xr;

    invoke-static {p0, p1, v0}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;)Landroid/graphics/Typeface;

    move-result-object p3

    goto :goto_0

    .line 163536
    :cond_1
    sget-object v0, LX/0xr;->REGULAR:LX/0xr;

    invoke-static {p0, p1, v0}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;)Landroid/graphics/Typeface;

    move-result-object p3

    goto :goto_0

    .line 163537
    :cond_2
    invoke-static {p0, p1, p2}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;)Landroid/graphics/Typeface;

    move-result-object p3

    goto :goto_0
.end method

.method public static a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V
    .locals 1
    .param p3    # Landroid/graphics/Typeface;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 163538
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2, p3}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 163539
    if-ne v0, p3, :cond_0

    .line 163540
    :goto_0
    return-void

    .line 163541
    :cond_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0
.end method
