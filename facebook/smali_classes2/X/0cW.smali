.class public LX/0cW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SharedPreferencesUse"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile w:LX/0cW;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Vt;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8LX;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0WV;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1EZ;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8KY;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0SF;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0en;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0VT;

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0cY;

.field public final o:LX/0cY;

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0d9;",
            ">;"
        }
    .end annotation
.end field

.field public final q:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final r:LX/0Uh;

.field private final s:LX/0cX;

.field private t:Ljava/io/OutputStreamWriter;

.field public u:Z

.field public v:Lcom/facebook/photos/upload/operation/UploadOperation;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88923
    const-class v0, LX/0cW;

    sput-object v0, LX/0cW;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Or;LX/0WV;LX/0Ot;LX/0Ot;LX/0SF;LX/0Ot;LX/0Ot;LX/0VT;LX/0Ot;LX/0Or;LX/0Ot;LX/0Or;LX/0Uh;LX/0cX;)V
    .locals 2
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p10    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p15    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/upload/gatekeeper/MediaUploadAppendRecordsOnRestart;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/0Vt;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8LX;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0WV;",
            "LX/0Ot",
            "<",
            "LX/1EZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8KY;",
            ">;",
            "LX/0SF;",
            "LX/0Ot",
            "<",
            "LX/0en;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Lcom/facebook/common/process/ProcessUtil;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0cY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0d9;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0cX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 88901
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88902
    const/4 v1, 0x0

    iput-object v1, p0, LX/0cW;->t:Ljava/io/OutputStreamWriter;

    .line 88903
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/0cW;->u:Z

    .line 88904
    iput-object p1, p0, LX/0cW;->b:Landroid/content/Context;

    .line 88905
    iput-object p2, p0, LX/0cW;->c:LX/0Ot;

    .line 88906
    iput-object p3, p0, LX/0cW;->d:LX/0Ot;

    .line 88907
    iput-object p4, p0, LX/0cW;->e:LX/0Or;

    .line 88908
    iput-object p5, p0, LX/0cW;->f:LX/0WV;

    .line 88909
    iput-object p6, p0, LX/0cW;->g:LX/0Ot;

    .line 88910
    iput-object p7, p0, LX/0cW;->h:LX/0Ot;

    .line 88911
    iput-object p8, p0, LX/0cW;->i:LX/0SF;

    .line 88912
    iput-object p9, p0, LX/0cW;->j:LX/0Ot;

    .line 88913
    iput-object p10, p0, LX/0cW;->k:LX/0Ot;

    .line 88914
    iput-object p11, p0, LX/0cW;->l:LX/0VT;

    .line 88915
    iput-object p12, p0, LX/0cW;->m:LX/0Ot;

    .line 88916
    invoke-interface {p13}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0cY;

    iput-object v1, p0, LX/0cW;->n:LX/0cY;

    .line 88917
    invoke-interface {p13}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0cY;

    iput-object v1, p0, LX/0cW;->o:LX/0cY;

    .line 88918
    move-object/from16 v0, p14

    iput-object v0, p0, LX/0cW;->p:LX/0Ot;

    .line 88919
    move-object/from16 v0, p15

    iput-object v0, p0, LX/0cW;->q:LX/0Or;

    .line 88920
    move-object/from16 v0, p16

    iput-object v0, p0, LX/0cW;->r:LX/0Uh;

    .line 88921
    move-object/from16 v0, p17

    iput-object v0, p0, LX/0cW;->s:LX/0cX;

    .line 88922
    return-void
.end method

.method public static a(LX/0QB;)LX/0cW;
    .locals 3

    .prologue
    .line 88891
    sget-object v0, LX/0cW;->w:LX/0cW;

    if-nez v0, :cond_1

    .line 88892
    const-class v1, LX/0cW;

    monitor-enter v1

    .line 88893
    :try_start_0
    sget-object v0, LX/0cW;->w:LX/0cW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 88894
    if-eqz v2, :cond_0

    .line 88895
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/0cW;->b(LX/0QB;)LX/0cW;

    move-result-object v0

    sput-object v0, LX/0cW;->w:LX/0cW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 88896
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 88897
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 88898
    :cond_1
    sget-object v0, LX/0cW;->w:LX/0cW;

    return-object v0

    .line 88899
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 88900
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0cW;Lcom/facebook/photos/upload/operation/UploadOperation;ILjava/lang/String;)Ljava/lang/String;
    .locals 10

    .prologue
    .line 88889
    iget-object v0, p0, LX/0cW;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8LX;

    invoke-virtual {v0, p1}, LX/8LX;->d(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/73w;

    move-result-object v1

    const-string v2, "2.0"

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->a()LX/0ck;

    move-result-object v3

    iget-object v0, p0, LX/0cW;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EZ;

    invoke-static {p1}, LX/1EZ;->f(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/742;

    move-result-object v4

    iget-object v0, p0, LX/0cW;->i:LX/0SF;

    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v6

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ak()J

    move-result-wide v8

    sub-long v8, v6, v8

    move-object v5, p1

    move v6, p2

    move-object v7, p3

    invoke-virtual/range {v1 .. v9}, LX/73w;->a(Ljava/lang/String;LX/0ck;LX/742;Lcom/facebook/photos/upload/operation/UploadOperation;ILjava/lang/String;J)V

    .line 88890
    return-object p3
.end method

.method public static a(LX/0cW;Ljava/lang/String;LX/0ck;Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .prologue
    .line 88885
    iget-object v0, p0, LX/0cW;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8LX;

    invoke-virtual {v0, p1}, LX/8LX;->a(Ljava/lang/String;)LX/73w;

    move-result-object v1

    .line 88886
    const-string v2, "2.0"

    iget-object v0, p0, LX/0cW;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    sget-object v0, LX/0ck;->VIDEO:LX/0ck;

    if-ne p2, v0, :cond_0

    sget-object v0, LX/8LS;->VIDEO:LX/8LS;

    :goto_0
    invoke-static {v0}, LX/1EZ;->a(LX/8LS;)LX/742;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, -0x1

    const-wide/16 v8, -0x1

    move-object v3, p2

    move-object v7, p3

    invoke-virtual/range {v1 .. v9}, LX/73w;->a(Ljava/lang/String;LX/0ck;LX/742;Lcom/facebook/photos/upload/operation/UploadOperation;ILjava/lang/String;J)V

    .line 88887
    return-object p3

    .line 88888
    :cond_0
    sget-object v0, LX/8LS;->OWN_TIMELINE:LX/8LS;

    goto :goto_0
.end method

.method public static a(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 88883
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "uploader_user_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "upload_system_version"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "upload_app_build_number"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 88884
    return-void
.end method

.method public static a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/0cY;)V
    .locals 4

    .prologue
    .line 88869
    invoke-virtual {p1}, LX/0cY;->b()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    .line 88870
    :goto_0
    if-nez v0, :cond_2

    .line 88871
    :cond_0
    :goto_1
    return-void

    .line 88872
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 88873
    :cond_2
    invoke-virtual {p1}, LX/0cY;->c()Ljava/util/List;

    move-result-object v0

    .line 88874
    if-eqz v0, :cond_0

    .line 88875
    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 88876
    :cond_3
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88877
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 88878
    iget-object v2, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v2, v2

    .line 88879
    iget-object v3, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v3, v3

    .line 88880
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 88881
    invoke-virtual {p1, v0}, LX/0cY;->b(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 88882
    invoke-virtual {p1, p0}, LX/0cY;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    goto :goto_1
.end method

.method private a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 88673
    iget-object v0, p0, LX/0cW;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8LX;

    invoke-virtual {v0, p1}, LX/8LX;->d(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/73w;

    move-result-object v0

    .line 88674
    const-string v1, "2.1"

    sget-object v2, LX/742;->CHUNKED:LX/742;

    invoke-virtual {v0, v1, v2}, LX/73w;->a(Ljava/lang/String;LX/742;)LX/74b;

    move-result-object v1

    .line 88675
    invoke-virtual {v1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v2

    .line 88676
    invoke-static {v0, v2, p1}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 88677
    const-string v3, "restarted_operation_source"

    invoke-virtual {v2, v3, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88678
    sget-object v3, LX/74R;->MEDIA_UPLOAD_RESTART:LX/74R;

    const/4 p0, 0x0

    invoke-static {v0, v3, v2, p0}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 88679
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 88865
    invoke-direct {p0}, LX/0cW;->d()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {v0, p2}, LX/0cW;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 88866
    const-string v0, "_upload_operation"

    invoke-static {p0, p1, v0}, LX/0cW;->b(LX/0cW;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 88867
    const-string v0, "_partial_uploads"

    invoke-static {p0, p1, v0}, LX/0cW;->b(LX/0cW;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 88868
    return-void
.end method

.method private a(Landroid/content/SharedPreferences;LX/0ck;Ljava/lang/String;)Z
    .locals 6

    .prologue
    .line 88821
    invoke-interface {p1, p3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 88822
    const/4 v0, 0x0

    .line 88823
    :goto_0
    return v0

    .line 88824
    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 88825
    invoke-interface {p1, p3, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 88826
    const-string v0, "uploader_user_id"

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 88827
    const-string v0, "upload_system_version"

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 88828
    const-string v3, "upload_app_build_number"

    const/4 v5, -0x1

    invoke-interface {p1, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 88829
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-gtz v5, :cond_2

    .line 88830
    :cond_1
    :goto_1
    invoke-static {p1, p3}, LX/0cW;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 88831
    const/4 v0, 0x1

    goto :goto_0

    .line 88832
    :cond_2
    if-eqz v0, :cond_3

    sget-object v5, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 88833
    :cond_3
    const-string v0, "Error: System version mismatch"

    invoke-static {p0, v4, p2, v0}, LX/0cW;->a(LX/0cW;Ljava/lang/String;LX/0ck;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 88834
    :cond_4
    iget-object v0, p0, LX/0cW;->f:LX/0WV;

    invoke-virtual {v0}, LX/0WV;->c()I

    move-result v0

    if-eq v3, v0, :cond_5

    .line 88835
    const-string v0, "Error: Build number mismatch"

    invoke-static {p0, v4, p2, v0}, LX/0cW;->a(LX/0cW;Ljava/lang/String;LX/0ck;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 88836
    :cond_5
    iget-object v0, p0, LX/0cW;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 88837
    if-eqz v1, :cond_6

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 88838
    :cond_6
    const-string v0, "Error: User ID mismatch"

    invoke-static {p0, v4, p2, v0}, LX/0cW;->a(LX/0cW;Ljava/lang/String;LX/0ck;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 88839
    :cond_7
    const-string v0, "_upload_operation"

    invoke-static {p0, v4, v0}, LX/0cW;->b(LX/0cW;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 88840
    invoke-static {v0}, Lcom/facebook/photos/upload/operation/UploadOperation;->b(Ljava/io/File;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v5

    .line 88841
    if-nez v5, :cond_8

    .line 88842
    const-string v0, "Error: Couldn\'t read operation"

    invoke-static {p0, v4, p2, v0}, LX/0cW;->a(LX/0cW;Ljava/lang/String;LX/0ck;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 88843
    :cond_8
    invoke-static {p0, v5}, LX/0cW;->j(LX/0cW;Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/photos/upload/operation/UploadRecords;

    move-result-object v3

    .line 88844
    if-eqz v3, :cond_9

    invoke-virtual {v3}, Lcom/facebook/photos/upload/operation/UploadRecords;->a()LX/0P1;

    move-result-object v0

    invoke-virtual {v0}, LX/0P1;->size()I

    move-result v0

    move v1, v0

    .line 88845
    :goto_2
    iget-object v0, v5, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v0

    .line 88846
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 88847
    const-string v0, "Error: Waterfall ID mismatch"

    invoke-static {p0, v5, v1, v0}, LX/0cW;->a(LX/0cW;Lcom/facebook/photos/upload/operation/UploadOperation;ILjava/lang/String;)Ljava/lang/String;

    goto :goto_1

    :cond_9
    move v1, v2

    .line 88848
    goto :goto_2

    .line 88849
    :cond_a
    new-instance v0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    new-instance v4, LX/8Ks;

    invoke-direct {v4, p0}, LX/8Ks;-><init>(LX/0cW;)V

    invoke-static {v4}, LX/0cX;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;-><init>(LX/73z;)V

    invoke-virtual {v5, v0}, Lcom/facebook/photos/upload/operation/UploadOperation;->a(Lcom/facebook/photos/upload/operation/UploadInterruptionCause;)V

    .line 88850
    iget-object v0, p0, LX/0cW;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 88851
    iget-object v0, v5, Lcom/facebook/photos/upload/operation/UploadOperation;->G:Lcom/facebook/photos/upload/operation/UploadRecords;

    move-object v0, v0

    .line 88852
    if-nez v0, :cond_c

    move-object v0, v3

    .line 88853
    :cond_b
    :goto_3
    iput-object v0, v5, Lcom/facebook/photos/upload/operation/UploadOperation;->G:Lcom/facebook/photos/upload/operation/UploadRecords;

    .line 88854
    :goto_4
    iput-object v5, p0, LX/0cW;->v:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 88855
    invoke-virtual {v5}, Lcom/facebook/photos/upload/operation/UploadOperation;->am()V

    .line 88856
    iget-object v0, p0, LX/0cW;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EZ;

    invoke-virtual {v0, v5}, LX/1EZ;->b(Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, "Resume after interruption #%d"

    :goto_5
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/facebook/photos/upload/operation/UploadOperation;->f()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 88857
    invoke-static {p0, v5, v1, v0}, LX/0cW;->a(LX/0cW;Lcom/facebook/photos/upload/operation/UploadOperation;ILjava/lang/String;)Ljava/lang/String;

    goto/16 :goto_1

    .line 88858
    :cond_c
    if-eqz v3, :cond_b

    .line 88859
    invoke-virtual {v3}, Lcom/facebook/photos/upload/operation/UploadRecords;->a()LX/0P1;

    move-result-object v3

    .line 88860
    iget-object v4, v0, Lcom/facebook/photos/upload/operation/UploadRecords;->a:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 88861
    goto :goto_3

    .line 88862
    :cond_d
    iput-object v3, v5, Lcom/facebook/photos/upload/operation/UploadOperation;->G:Lcom/facebook/photos/upload/operation/UploadRecords;

    .line 88863
    goto :goto_4

    .line 88864
    :cond_e
    const-string v0, "Interrupt notification after %d interruptions"

    goto :goto_5
.end method

.method public static a$redex0(LX/0cW;)V
    .locals 7

    .prologue
    .line 88785
    const/4 v0, 0x0

    .line 88786
    iget-object v1, p0, LX/0cW;->r:LX/0Uh;

    const/16 v2, 0x2c9

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v3

    .line 88787
    iget-object v1, p0, LX/0cW;->v:Lcom/facebook/photos/upload/operation/UploadOperation;

    if-eqz v1, :cond_8

    .line 88788
    iget-object v0, p0, LX/0cW;->v:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 88789
    iget-object v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 88790
    if-nez v3, :cond_0

    iget-object v0, p0, LX/0cW;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EZ;

    iget-object v2, p0, LX/0cW;->v:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v0, v2}, LX/1EZ;->b(Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 88791
    :cond_0
    const-string v2, "Resume"

    .line 88792
    iget-object v0, p0, LX/0cW;->v:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-direct {p0, v0, v2}, LX/0cW;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V

    .line 88793
    iget-object v0, p0, LX/0cW;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EZ;

    iget-object v4, p0, LX/0cW;->v:Lcom/facebook/photos/upload/operation/UploadOperation;

    sget-object v5, LX/8Kx;->Resume:LX/8Kx;

    invoke-virtual {v0, v4, v5, v2}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Kx;Ljava/lang/String;)V

    move-object v2, v1

    .line 88794
    :goto_0
    iget-object v0, p0, LX/0cW;->n:LX/0cY;

    invoke-virtual {v0}, LX/0cY;->c()Ljava/util/List;

    move-result-object v0

    .line 88795
    if-eqz v0, :cond_4

    .line 88796
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 88797
    iget-object v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 88798
    invoke-static {v2, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 88799
    iget-boolean v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    move v1, v1

    .line 88800
    if-nez v1, :cond_3

    .line 88801
    const-string v5, "Interrupted re-enqueue"

    .line 88802
    invoke-direct {p0, v0, v5}, LX/0cW;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V

    .line 88803
    iget-object v1, p0, LX/0cW;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1EZ;

    sget-object v6, LX/8Kx;->Restore:LX/8Kx;

    invoke-virtual {v1, v0, v6, v5}, LX/1EZ;->c(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Kx;Ljava/lang/String;)V

    goto :goto_1

    .line 88804
    :cond_2
    const-string v0, "Not Resume"

    .line 88805
    iget-object v2, p0, LX/0cW;->v:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-direct {p0, v2, v0}, LX/0cW;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V

    .line 88806
    iget-object v0, p0, LX/0cW;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EZ;

    iget-object v2, p0, LX/0cW;->v:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v0, v2}, LX/1EZ;->e(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    move-object v2, v1

    goto :goto_0

    .line 88807
    :cond_3
    goto :goto_1

    .line 88808
    :cond_4
    iget-object v0, p0, LX/0cW;->o:LX/0cY;

    invoke-virtual {v0}, LX/0cY;->c()Ljava/util/List;

    move-result-object v0

    .line 88809
    if-eqz v0, :cond_7

    .line 88810
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 88811
    iget-boolean v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    move v1, v1

    .line 88812
    if-nez v1, :cond_6

    .line 88813
    const-string v4, "Recover failed operation"

    .line 88814
    invoke-direct {p0, v0, v4}, LX/0cW;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V

    .line 88815
    if-eqz v3, :cond_5

    .line 88816
    iget-object v1, p0, LX/0cW;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1EZ;

    sget-object v5, LX/8Kx;->Restore:LX/8Kx;

    invoke-virtual {v1, v0, v5, v4}, LX/1EZ;->c(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Kx;Ljava/lang/String;)V

    goto :goto_2

    .line 88817
    :cond_5
    iget-object v1, p0, LX/0cW;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1EZ;

    invoke-virtual {v1, v0}, LX/1EZ;->e(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    goto :goto_2

    .line 88818
    :cond_6
    goto :goto_2

    .line 88819
    :cond_7
    iget-object v0, p0, LX/0cW;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EZ;

    const-string v1, "Crash Monitor recover"

    invoke-virtual {v0, v1}, LX/1EZ;->d(Ljava/lang/String;)V

    .line 88820
    return-void

    :cond_8
    move-object v2, v0

    goto/16 :goto_0
.end method

.method private static b(LX/0QB;)LX/0cW;
    .locals 20

    .prologue
    .line 88783
    new-instance v2, LX/0cW;

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v4, 0x10c5

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2ee5

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x15e7

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static/range {p0 .. p0}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v7

    check-cast v7, LX/0WV;

    const/16 v8, 0xf39

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2ed9

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SF;

    const/16 v11, 0x287

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x1430

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static/range {p0 .. p0}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v13

    check-cast v13, LX/0VT;

    const/16 v14, 0x259

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0xf3a

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    const/16 v16, 0xf3f

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x154f

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v18

    check-cast v18, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/0cX;->a(LX/0QB;)LX/0cX;

    move-result-object v19

    check-cast v19, LX/0cX;

    invoke-direct/range {v2 .. v19}, LX/0cW;-><init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Or;LX/0WV;LX/0Ot;LX/0Ot;LX/0SF;LX/0Ot;LX/0Ot;LX/0VT;LX/0Ot;LX/0Or;LX/0Ot;LX/0Or;LX/0Uh;LX/0cX;)V

    .line 88784
    return-object v2
.end method

.method public static b(LX/0cW;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 4

    .prologue
    const/16 v3, 0x5f

    .line 88781
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-char v1, Ljava/io/File;->pathSeparatorChar:C

    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    sget-char v2, Ljava/io/File;->separatorChar:C

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 88782
    new-instance v1, Ljava/io/File;

    invoke-direct {p0}, LX/0cW;->e()Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method private b(Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 88780
    new-instance v0, Ljava/io/File;

    invoke-direct {p0}, LX/0cW;->e()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 88699
    iget-object v0, p0, LX/0cW;->t:Ljava/io/OutputStreamWriter;

    if-eqz v0, :cond_0

    .line 88700
    :try_start_0
    iget-object v0, p0, LX/0cW;->t:Ljava/io/OutputStreamWriter;

    invoke-virtual {v0}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88701
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/0cW;->t:Ljava/io/OutputStreamWriter;

    .line 88702
    :cond_0
    return-void

    .line 88703
    :catch_0
    move-exception v0

    .line 88704
    sget-object v1, LX/0cW;->a:Ljava/lang/Class;

    const-string v2, "Failed to close partial records"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static b(LX/0cW;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 88680
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v0

    .line 88681
    const-string v1, "_upload_operation"

    invoke-static {p0, v0, v1}, LX/0cW;->b(LX/0cW;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 88682
    invoke-virtual {p1, v0}, Lcom/facebook/photos/upload/operation/UploadOperation;->a(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88683
    invoke-direct {p0}, LX/0cW;->d()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 88684
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 88685
    invoke-interface {v0, p2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "uploader_user_id"

    iget-object v0, p0, LX/0cW;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "upload_app_build_number"

    iget-object v2, p0, LX/0cW;->f:LX/0WV;

    invoke-virtual {v2}, LX/0WV;->c()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "upload_system_version"

    sget-object v2, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 88686
    :cond_0
    return-void
.end method

.method public static c(LX/0cW;)V
    .locals 1

    .prologue
    .line 88687
    iget-object v0, p0, LX/0cW;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-direct {p0}, LX/0cW;->e()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, LX/2W9;->a(Ljava/io/File;)Z

    .line 88688
    return-void
.end method

.method private d()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 88689
    iget-object v0, p0, LX/0cW;->b:Landroid/content/Context;

    const-string v1, "upload_crash_monitor"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private e()Ljava/io/File;
    .locals 3

    .prologue
    .line 88690
    iget-object v0, p0, LX/0cW;->b:Landroid/content/Context;

    const-string v1, "upload_crash_monitor_temp"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static i(LX/0cW;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 4

    .prologue
    .line 88691
    :try_start_0
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v0

    .line 88692
    const-string v1, "_partial_uploads"

    invoke-static {p0, v0, v1}, LX/0cW;->b(LX/0cW;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 88693
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 88694
    new-instance v0, Ljava/io/OutputStreamWriter;

    const-string v2, "UTF-8"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    iput-object v0, p0, LX/0cW;->t:Ljava/io/OutputStreamWriter;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88695
    :goto_0
    return-void

    .line 88696
    :catch_0
    move-exception v0

    .line 88697
    const/4 v1, 0x0

    iput-object v1, p0, LX/0cW;->t:Ljava/io/OutputStreamWriter;

    .line 88698
    sget-object v1, LX/0cW;->a:Ljava/lang/Class;

    const-string v2, "Failed to create partial upload file"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static j(LX/0cW;Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/photos/upload/operation/UploadRecords;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 88705
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 88706
    :try_start_0
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 88707
    const-string v3, "_partial_uploads"

    invoke-static {p0, v1, v3}, LX/0cW;->b(LX/0cW;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 88708
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v3, "UTF-8"

    invoke-direct {v4, v5, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88709
    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->ready()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 88710
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 88711
    invoke-static {v4}, Lcom/facebook/photos/upload/operation/UploadRecord;->a(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadRecord;

    move-result-object v4

    .line 88712
    if-eqz v4, :cond_0

    .line 88713
    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 88714
    :catch_0
    :cond_1
    :goto_1
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 88715
    :goto_2
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v1

    if-nez v1, :cond_4

    .line 88716
    :cond_2
    :goto_3
    return-object v0

    .line 88717
    :catch_1
    move-object v1, v0

    :goto_4
    if-eqz v1, :cond_2

    .line 88718
    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    :catch_2
    goto :goto_3

    .line 88719
    :catch_3
    move-object v1, v0

    :goto_5
    if-eqz v1, :cond_2

    .line 88720
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_3

    :catch_4
    goto :goto_3

    .line 88721
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_6
    if-eqz v1, :cond_3

    .line 88722
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    .line 88723
    :cond_3
    :goto_7
    throw v0

    .line 88724
    :cond_4
    invoke-interface {v2}, Ljava/util/Map;->size()I

    .line 88725
    new-instance v0, Lcom/facebook/photos/upload/operation/UploadRecords;

    invoke-direct {v0, v2}, Lcom/facebook/photos/upload/operation/UploadRecords;-><init>(Ljava/util/Map;)V

    goto :goto_3

    .line 88726
    :catch_5
    goto :goto_2

    :catch_6
    goto :goto_7

    .line 88727
    :catchall_1
    move-exception v0

    goto :goto_6

    :catch_7
    goto :goto_5

    :catch_8
    goto :goto_4

    :catch_9
    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 88728
    const-string v0, "video_upload_in_progress_waterfallid"

    invoke-direct {p0, p1, v0}, LX/0cW;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 88729
    invoke-direct {p0}, LX/0cW;->b()V

    .line 88730
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadRecord;)Z
    .locals 3

    .prologue
    .line 88731
    iget-object v0, p0, LX/0cW;->t:Ljava/io/OutputStreamWriter;

    if-eqz v0, :cond_0

    .line 88732
    :try_start_0
    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadRecord;->a()Ljava/lang/String;

    move-result-object v0

    .line 88733
    iget-object v1, p0, LX/0cW;->t:Ljava/io/OutputStreamWriter;

    invoke-virtual {v1, p1}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 88734
    iget-object v1, p0, LX/0cW;->t:Ljava/io/OutputStreamWriter;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/io/OutputStreamWriter;->write(I)V

    .line 88735
    iget-object v1, p0, LX/0cW;->t:Ljava/io/OutputStreamWriter;

    invoke-virtual {v1, v0}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 88736
    iget-object v0, p0, LX/0cW;->t:Ljava/io/OutputStreamWriter;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/io/OutputStreamWriter;->write(I)V

    .line 88737
    iget-object v0, p0, LX/0cW;->t:Ljava/io/OutputStreamWriter;

    invoke-virtual {v0}, Ljava/io/OutputStreamWriter;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88738
    const/4 v0, 0x1

    .line 88739
    :goto_0
    return v0

    .line 88740
    :catch_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/0cW;->t:Ljava/io/OutputStreamWriter;

    .line 88741
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 2

    .prologue
    .line 88742
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v0

    .line 88743
    const-string v1, "multimedia_upload_in_progress_waterfallid"

    invoke-direct {p0, v0, v1}, LX/0cW;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 88744
    invoke-direct {p0}, LX/0cW;->b()V

    .line 88745
    return-void
.end method

.method public final d(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 2

    .prologue
    .line 88746
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v0

    .line 88747
    const-string v1, "photo_upload_in_progress_waterfallid"

    invoke-direct {p0, v0, v1}, LX/0cW;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 88748
    invoke-direct {p0}, LX/0cW;->b()V

    .line 88749
    return-void
.end method

.method public final f(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 1

    .prologue
    .line 88750
    iget-boolean v0, p0, LX/0cW;->u:Z

    if-eqz v0, :cond_0

    .line 88751
    iget-object v0, p0, LX/0cW;->n:LX/0cY;

    invoke-virtual {v0, p1}, LX/0cY;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 88752
    :cond_0
    return-void
.end method

.method public final init()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 88753
    iget-object v2, p0, LX/0cW;->l:LX/0VT;

    invoke-virtual {v2}, LX/0VT;->a()LX/00G;

    move-result-object v2

    invoke-virtual {v2}, LX/00G;->e()Z

    move-result v2

    if-nez v2, :cond_0

    .line 88754
    :goto_0
    return-void

    .line 88755
    :cond_0
    iget-boolean v2, p0, LX/0cW;->u:Z

    if-eqz v2, :cond_1

    .line 88756
    iget-object v0, p0, LX/0cW;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/0cW;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "double init"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 88757
    :cond_1
    iget-object v2, p0, LX/0cW;->n:LX/0cY;

    const-string v3, "upload_queue"

    invoke-direct {p0, v3}, LX/0cW;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    iget-object v4, p0, LX/0cW;->f:LX/0WV;

    invoke-virtual {v4}, LX/0WV;->c()I

    move-result v4

    sget-object v5, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, LX/0cY;->a(Ljava/io/File;ILjava/lang/String;)V

    .line 88758
    iget-object v2, p0, LX/0cW;->o:LX/0cY;

    const-string v3, "failed_upload"

    invoke-direct {p0, v3}, LX/0cW;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    iget-object v4, p0, LX/0cW;->f:LX/0WV;

    invoke-virtual {v4}, LX/0WV;->c()I

    move-result v4

    sget-object v5, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, LX/0cY;->a(Ljava/io/File;ILjava/lang/String;)V

    .line 88759
    iput-boolean v0, p0, LX/0cW;->u:Z

    .line 88760
    invoke-direct {p0}, LX/0cW;->d()Landroid/content/SharedPreferences;

    move-result-object v4

    .line 88761
    :try_start_0
    sget-object v2, LX/0ck;->PHOTO:LX/0ck;

    const-string v3, "photo_upload_in_progress_waterfallid"

    invoke-direct {p0, v4, v2, v3}, LX/0cW;->a(Landroid/content/SharedPreferences;LX/0ck;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, LX/0ck;->VIDEO:LX/0ck;

    const-string v3, "video_upload_in_progress_waterfallid"

    invoke-direct {p0, v4, v2, v3}, LX/0cW;->a(Landroid/content/SharedPreferences;LX/0ck;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, LX/0ck;->MULTIMEDIA:LX/0ck;

    const-string v3, "multimedia_upload_in_progress_waterfallid"

    invoke-direct {p0, v4, v2, v3}, LX/0cW;->a(Landroid/content/SharedPreferences;LX/0ck;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_2
    move v3, v0

    .line 88762
    :goto_1
    iget-object v2, p0, LX/0cW;->n:LX/0cY;

    invoke-virtual {v2}, LX/0cY;->b()I

    move-result v2

    if-lez v2, :cond_5

    move v2, v0

    .line 88763
    :goto_2
    iget-object v5, p0, LX/0cW;->o:LX/0cY;

    invoke-virtual {v5}, LX/0cY;->b()I

    move-result v5

    if-lez v5, :cond_6

    .line 88764
    :goto_3
    if-nez v2, :cond_3

    if-nez v0, :cond_3

    if-eqz v3, :cond_7

    .line 88765
    :cond_3
    invoke-static {p0}, LX/0cW;->c(LX/0cW;)V

    .line 88766
    iget-object v0, p0, LX/0cW;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8KY;

    .line 88767
    iget-object v1, v0, LX/8KY;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "uploads"

    invoke-static {v0, v1, v2}, LX/8KY;->b(LX/8KY;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    .line 88768
    iget-object v1, v0, LX/8KY;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "uploads"

    invoke-static {v0, v1, v2}, LX/8KY;->b(LX/8KY;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    .line 88769
    iget-object v0, p0, LX/0cW;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Vt;

    invoke-virtual {v0}, LX/0Vt;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/8Kr;

    invoke-direct {v2, p0}, LX/8Kr;-><init>(LX/0cW;)V

    iget-object v0, p0, LX/0cW;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 88770
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 88771
    iget-object v0, p0, LX/0cW;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, LX/0cW;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 88772
    const-string v0, "photo_upload_in_progress_waterfallid"

    invoke-static {v4, v0}, LX/0cW;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 88773
    const-string v0, "video_upload_in_progress_waterfallid"

    invoke-static {v4, v0}, LX/0cW;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 88774
    invoke-static {p0}, LX/0cW;->c(LX/0cW;)V

    .line 88775
    goto/16 :goto_0

    :cond_4
    move v3, v1

    .line 88776
    goto :goto_1

    :cond_5
    move v2, v1

    .line 88777
    goto :goto_2

    :cond_6
    move v0, v1

    .line 88778
    goto :goto_3

    .line 88779
    :cond_7
    :try_start_1
    iget-object v0, p0, LX/0cW;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0d9;

    invoke-virtual {v0}, LX/0d9;->b()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
