.class public LX/0lY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0lZ;
.implements LX/0la;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0lZ;",
        "LX/0la",
        "<",
        "LX/0lY;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field public static final a:LX/0lb;

.field private static final serialVersionUID:J = -0x4c80a4585cebda9dL


# instance fields
.field public _arrayIndenter:LX/0lf;

.field public _objectIndenter:LX/0lf;

.field public final _rootSeparator:LX/0lc;

.field public _spacesInObjectEntries:Z

.field public transient b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 130040
    new-instance v0, LX/0lb;

    const-string v1, " "

    invoke-direct {v0, v1}, LX/0lb;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/0lY;->a:LX/0lb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 130038
    sget-object v0, LX/0lY;->a:LX/0lb;

    invoke-direct {p0, v0}, LX/0lY;-><init>(LX/0lc;)V

    .line 130039
    return-void
.end method

.method private constructor <init>(LX/0lY;)V
    .locals 1

    .prologue
    .line 130036
    iget-object v0, p1, LX/0lY;->_rootSeparator:LX/0lc;

    invoke-direct {p0, p1, v0}, LX/0lY;-><init>(LX/0lY;LX/0lc;)V

    .line 130037
    return-void
.end method

.method private constructor <init>(LX/0lY;LX/0lc;)V
    .locals 1

    .prologue
    .line 130025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130026
    sget-object v0, LX/0ld;->a:LX/0ld;

    iput-object v0, p0, LX/0lY;->_arrayIndenter:LX/0lf;

    .line 130027
    sget-object v0, LX/0lg;->a:LX/0lg;

    iput-object v0, p0, LX/0lY;->_objectIndenter:LX/0lf;

    .line 130028
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0lY;->_spacesInObjectEntries:Z

    .line 130029
    const/4 v0, 0x0

    iput v0, p0, LX/0lY;->b:I

    .line 130030
    iget-object v0, p1, LX/0lY;->_arrayIndenter:LX/0lf;

    iput-object v0, p0, LX/0lY;->_arrayIndenter:LX/0lf;

    .line 130031
    iget-object v0, p1, LX/0lY;->_objectIndenter:LX/0lf;

    iput-object v0, p0, LX/0lY;->_objectIndenter:LX/0lf;

    .line 130032
    iget-boolean v0, p1, LX/0lY;->_spacesInObjectEntries:Z

    iput-boolean v0, p0, LX/0lY;->_spacesInObjectEntries:Z

    .line 130033
    iget v0, p1, LX/0lY;->b:I

    iput v0, p0, LX/0lY;->b:I

    .line 130034
    iput-object p2, p0, LX/0lY;->_rootSeparator:LX/0lc;

    .line 130035
    return-void
.end method

.method private constructor <init>(LX/0lc;)V
    .locals 1

    .prologue
    .line 130018
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130019
    sget-object v0, LX/0ld;->a:LX/0ld;

    iput-object v0, p0, LX/0lY;->_arrayIndenter:LX/0lf;

    .line 130020
    sget-object v0, LX/0lg;->a:LX/0lg;

    iput-object v0, p0, LX/0lY;->_objectIndenter:LX/0lf;

    .line 130021
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0lY;->_spacesInObjectEntries:Z

    .line 130022
    const/4 v0, 0x0

    iput v0, p0, LX/0lY;->b:I

    .line 130023
    iput-object p1, p0, LX/0lY;->_rootSeparator:LX/0lc;

    .line 130024
    return-void
.end method

.method private b()LX/0lY;
    .locals 1

    .prologue
    .line 130017
    new-instance v0, LX/0lY;

    invoke-direct {v0, p0}, LX/0lY;-><init>(LX/0lY;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 130016
    invoke-direct {p0}, LX/0lY;->b()LX/0lY;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0nX;)V
    .locals 1

    .prologue
    .line 130013
    iget-object v0, p0, LX/0lY;->_rootSeparator:LX/0lc;

    if-eqz v0, :cond_0

    .line 130014
    iget-object v0, p0, LX/0lY;->_rootSeparator:LX/0lc;

    invoke-virtual {p1, v0}, LX/0nX;->d(LX/0lc;)V

    .line 130015
    :cond_0
    return-void
.end method

.method public final a(LX/0nX;I)V
    .locals 2

    .prologue
    .line 129977
    iget-object v0, p0, LX/0lY;->_objectIndenter:LX/0lf;

    invoke-interface {v0}, LX/0lf;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 129978
    iget v0, p0, LX/0lY;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0lY;->b:I

    .line 129979
    :cond_0
    if-lez p2, :cond_1

    .line 129980
    iget-object v0, p0, LX/0lY;->_objectIndenter:LX/0lf;

    iget v1, p0, LX/0lY;->b:I

    invoke-interface {v0, p1, v1}, LX/0lf;->a(LX/0nX;I)V

    .line 129981
    :goto_0
    const/16 v0, 0x7d

    invoke-virtual {p1, v0}, LX/0nX;->a(C)V

    .line 129982
    return-void

    .line 129983
    :cond_1
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, LX/0nX;->a(C)V

    goto :goto_0
.end method

.method public final b(LX/0nX;)V
    .locals 1

    .prologue
    .line 130009
    const/16 v0, 0x7b

    invoke-virtual {p1, v0}, LX/0nX;->a(C)V

    .line 130010
    iget-object v0, p0, LX/0lY;->_objectIndenter:LX/0lf;

    invoke-interface {v0}, LX/0lf;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130011
    iget v0, p0, LX/0lY;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0lY;->b:I

    .line 130012
    :cond_0
    return-void
.end method

.method public final b(LX/0nX;I)V
    .locals 2

    .prologue
    .line 130002
    iget-object v0, p0, LX/0lY;->_arrayIndenter:LX/0lf;

    invoke-interface {v0}, LX/0lf;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130003
    iget v0, p0, LX/0lY;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0lY;->b:I

    .line 130004
    :cond_0
    if-lez p2, :cond_1

    .line 130005
    iget-object v0, p0, LX/0lY;->_arrayIndenter:LX/0lf;

    iget v1, p0, LX/0lY;->b:I

    invoke-interface {v0, p1, v1}, LX/0lf;->a(LX/0nX;I)V

    .line 130006
    :goto_0
    const/16 v0, 0x5d

    invoke-virtual {p1, v0}, LX/0nX;->a(C)V

    .line 130007
    return-void

    .line 130008
    :cond_1
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, LX/0nX;->a(C)V

    goto :goto_0
.end method

.method public final c(LX/0nX;)V
    .locals 2

    .prologue
    .line 129999
    const/16 v0, 0x2c

    invoke-virtual {p1, v0}, LX/0nX;->a(C)V

    .line 130000
    iget-object v0, p0, LX/0lY;->_objectIndenter:LX/0lf;

    iget v1, p0, LX/0lY;->b:I

    invoke-interface {v0, p1, v1}, LX/0lf;->a(LX/0nX;I)V

    .line 130001
    return-void
.end method

.method public final d(LX/0nX;)V
    .locals 1

    .prologue
    .line 129995
    iget-boolean v0, p0, LX/0lY;->_spacesInObjectEntries:Z

    if-eqz v0, :cond_0

    .line 129996
    const-string v0, " : "

    invoke-virtual {p1, v0}, LX/0nX;->c(Ljava/lang/String;)V

    .line 129997
    :goto_0
    return-void

    .line 129998
    :cond_0
    const/16 v0, 0x3a

    invoke-virtual {p1, v0}, LX/0nX;->a(C)V

    goto :goto_0
.end method

.method public final e(LX/0nX;)V
    .locals 1

    .prologue
    .line 129991
    iget-object v0, p0, LX/0lY;->_arrayIndenter:LX/0lf;

    invoke-interface {v0}, LX/0lf;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 129992
    iget v0, p0, LX/0lY;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0lY;->b:I

    .line 129993
    :cond_0
    const/16 v0, 0x5b

    invoke-virtual {p1, v0}, LX/0nX;->a(C)V

    .line 129994
    return-void
.end method

.method public final f(LX/0nX;)V
    .locals 2

    .prologue
    .line 129988
    const/16 v0, 0x2c

    invoke-virtual {p1, v0}, LX/0nX;->a(C)V

    .line 129989
    iget-object v0, p0, LX/0lY;->_arrayIndenter:LX/0lf;

    iget v1, p0, LX/0lY;->b:I

    invoke-interface {v0, p1, v1}, LX/0lf;->a(LX/0nX;I)V

    .line 129990
    return-void
.end method

.method public final g(LX/0nX;)V
    .locals 2

    .prologue
    .line 129986
    iget-object v0, p0, LX/0lY;->_arrayIndenter:LX/0lf;

    iget v1, p0, LX/0lY;->b:I

    invoke-interface {v0, p1, v1}, LX/0lf;->a(LX/0nX;I)V

    .line 129987
    return-void
.end method

.method public final h(LX/0nX;)V
    .locals 2

    .prologue
    .line 129984
    iget-object v0, p0, LX/0lY;->_objectIndenter:LX/0lf;

    iget v1, p0, LX/0lY;->b:I

    invoke-interface {v0, p1, v1}, LX/0lf;->a(LX/0nX;I)V

    .line 129985
    return-void
.end method
