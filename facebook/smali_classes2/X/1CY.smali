.class public LX/1CY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0SG;

.field private final c:LX/0jU;

.field private final d:LX/03V;

.field public e:LX/0fz;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0g4;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0SG;LX/0jU;LX/03V;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;",
            "LX/0SG;",
            "LX/0jU;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 216461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216462
    iput-object v0, p0, LX/1CY;->e:LX/0fz;

    .line 216463
    iput-object v0, p0, LX/1CY;->f:LX/0g4;

    .line 216464
    iput-object p1, p0, LX/1CY;->a:LX/0Ot;

    .line 216465
    iput-object p2, p0, LX/1CY;->b:LX/0SG;

    .line 216466
    iput-object p3, p0, LX/1CY;->c:LX/0jU;

    .line 216467
    iput-object p4, p0, LX/1CY;->d:LX/03V;

    .line 216468
    return-void
.end method

.method public static a(LX/0QB;)LX/1CY;
    .locals 5

    .prologue
    .line 216458
    new-instance v3, LX/1CY;

    const/16 v0, 0xafc

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/0jU;->a(LX/0QB;)LX/0jU;

    move-result-object v1

    check-cast v1, LX/0jU;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-direct {v3, v4, v0, v1, v2}, LX/1CY;-><init>(LX/0Ot;LX/0SG;LX/0jU;LX/03V;)V

    .line 216459
    move-object v0, v3

    .line 216460
    return-object v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 216441
    const-string v2, "FeedUnitSubscriber.subscribeToUnit"

    const v3, 0x4cf78e62    # 1.29790736E8f

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 216442
    :try_start_0
    invoke-static {p1}, LX/1CY;->c(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v12

    .line 216443
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v9

    .line 216444
    invoke-static {p1}, LX/1CY;->c(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v0

    .line 216445
    if-eqz v0, :cond_2

    .line 216446
    :goto_0
    move-object v11, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216447
    if-nez v11, :cond_0

    .line 216448
    const v2, -0x1523d153

    invoke-static {v2}, LX/02m;->a(I)V

    move-object v2, v10

    :goto_1
    return-object v2

    .line 216449
    :cond_0
    :try_start_1
    iget-object v2, p0, LX/1CY;->c:LX/0jU;

    invoke-virtual {v2, v9}, LX/0jU;->e(Lcom/facebook/graphql/model/FeedUnit;)Ljava/util/Set;

    move-result-object v8

    .line 216450
    iget-object v2, p0, LX/1CY;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1My;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v4

    sget-object v5, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v6, 0x0

    invoke-virtual/range {v3 .. v8}, LX/1My;->a(LX/0jT;LX/0ta;JLjava/util/Set;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v4

    .line 216451
    iget-object v2, p0, LX/1CY;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1My;

    new-instance v5, LX/1NF;

    instance-of v3, v9, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v3, :cond_1

    move-object v0, v9

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v3, v0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v10

    :cond_1
    invoke-direct {v5, p0, v12, v10, v9}, LX/1NF;-><init>(LX/1CY;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v2, v5, v11, v4}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216452
    const v2, 0x318f4d15

    invoke-static {v2}, LX/02m;->a(I)V

    move-object v2, v11

    goto :goto_1

    :catchall_0
    move-exception v2

    const v3, 0x3658d694

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    .line 216453
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 216454
    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    .line 216455
    if-eqz v1, :cond_3

    move-object v0, v1

    .line 216456
    goto :goto_0

    .line 216457
    :cond_3
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_4

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/Iterable;Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 216435
    if-eqz p1, :cond_1

    .line 216436
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 216437
    invoke-direct {p0, v0}, LX/1CY;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v0

    .line 216438
    if-eqz v0, :cond_0

    .line 216439
    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 216440
    :cond_1
    return-void
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 216391
    invoke-static {p0}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v0

    .line 216392
    sget-object v1, LX/0ql;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 216393
    const/4 v0, 0x0

    .line 216394
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 216432
    iput-object v0, p0, LX/1CY;->e:LX/0fz;

    .line 216433
    iput-object v0, p0, LX/1CY;->f:LX/0g4;

    .line 216434
    return-void
.end method

.method public final a(LX/0fz;LX/0g4;)V
    .locals 0

    .prologue
    .line 216427
    invoke-virtual {p0}, LX/1CY;->a()V

    .line 216428
    iput-object p1, p0, LX/1CY;->e:LX/0fz;

    .line 216429
    iput-object p2, p0, LX/1CY;->f:LX/0g4;

    .line 216430
    invoke-virtual {p0, p1}, LX/1CY;->a(Ljava/lang/Iterable;)V

    .line 216431
    return-void
.end method

.method public final a(Ljava/lang/Iterable;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 216415
    const-string v0, "FeedUnitSubscriber.addEdges"

    const v2, -0x3db105e4

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 216416
    :try_start_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 216417
    invoke-direct {p0, v0}, LX/1CY;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 216418
    :catchall_0
    move-exception v0

    const v1, 0x71dae98d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 216419
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1CY;->e:LX/0fz;

    if-nez v0, :cond_2

    .line 216420
    :goto_1
    iget-object v0, p0, LX/1CY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    invoke-virtual {v0}, LX/1My;->c()I

    move-result v0

    int-to-double v2, v0

    int-to-double v4, v1

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v4, v6

    cmpl-double v0, v2, v4

    if-lez v0, :cond_1

    .line 216421
    iget-object v0, p0, LX/1CY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    invoke-virtual {v0}, LX/1My;->c()I

    move-result v0

    .line 216422
    invoke-virtual {p0}, LX/1CY;->b()V

    .line 216423
    iget-object v2, p0, LX/1CY;->d:LX/03V;

    const-string v3, "FeedUnitSubscriberManualGC"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Started at: %d Ended at: %d Feed Size: %d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v7

    const/4 v7, 0x1

    iget-object v0, p0, LX/1CY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    invoke-virtual {v0}, LX/1My;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v7

    const/4 v0, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2710

    invoke-virtual {v2, v3, v0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216424
    :cond_1
    const v0, -0x7a3b91b2

    invoke-static {v0}, LX/02m;->a(I)V

    .line 216425
    return-void

    .line 216426
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/1CY;->e:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->k()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    move v1, v0

    goto :goto_1
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 216401
    const-string v0, "FeedUnitSubscriber.reInitialize"

    const v1, -0x13ac76da

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 216402
    :try_start_0
    iget-object v1, p0, LX/1CY;->e:LX/0fz;

    .line 216403
    iget-object v0, p0, LX/1CY;->e:LX/0fz;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1CY;->e:LX/0fz;

    .line 216404
    iget-object v2, v0, LX/0fz;->i:LX/0qv;

    move-object v0, v2

    .line 216405
    :goto_0
    if-nez v1, :cond_1

    if-nez v0, :cond_1

    .line 216406
    iget-object v0, p0, LX/1CY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    invoke-virtual {v0}, LX/1My;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216407
    :goto_1
    const v0, -0x796a068d

    invoke-static {v0}, LX/02m;->a(I)V

    .line 216408
    return-void

    .line 216409
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 216410
    :cond_1
    :try_start_1
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v2

    .line 216411
    invoke-direct {p0, v1, v2}, LX/1CY;->a(Ljava/lang/Iterable;Ljava/util/Set;)V

    .line 216412
    invoke-direct {p0, v0, v2}, LX/1CY;->a(Ljava/lang/Iterable;Ljava/util/Set;)V

    .line 216413
    iget-object v0, p0, LX/1CY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    invoke-virtual {v0, v2}, LX/1My;->a(Ljava/util/Set;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 216414
    :catchall_0
    move-exception v0

    const v1, 0x6a638ac1

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 216399
    iget-object v0, p0, LX/1CY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    invoke-virtual {v0}, LX/1My;->d()V

    .line 216400
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 216397
    iget-object v0, p0, LX/1CY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    invoke-virtual {v0}, LX/1My;->e()V

    .line 216398
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 216395
    iget-object v0, p0, LX/1CY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    invoke-virtual {v0}, LX/1My;->a()V

    .line 216396
    return-void
.end method
