.class public LX/1Nq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Nr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/88f;",
        ">",
        "Ljava/lang/Object;",
        "LX/1Nr",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0lC;


# direct methods
.method public constructor <init>(LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 237791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237792
    iput-object p1, p0, LX/1Nq;->a:LX/0lC;

    .line 237793
    return-void
.end method

.method public static a(LX/0QB;)LX/1Nq;
    .locals 1

    .prologue
    .line 237790
    invoke-static {p0}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1Nq;
    .locals 2

    .prologue
    .line 237788
    new-instance v1, LX/1Nq;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-direct {v1, v0}, LX/1Nq;-><init>(LX/0lC;)V

    .line 237789
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;Ljava/lang/Class;)LX/88f;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 237781
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 237782
    :try_start_0
    iget-object v0, p0, LX/1Nq;->a:LX/0lC;

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/88f;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 237783
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, LX/88e;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 237784
    invoke-interface {v0}, LX/88f;->a()V

    .line 237785
    return-object v0

    .line 237786
    :catch_0
    move-exception v0

    .line 237787
    new-instance v1, LX/89E;

    invoke-direct {v1, v0}, LX/89E;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;"
        }
    .end annotation

    .prologue
    .line 237778
    :try_start_0
    iget-object v0, p0, LX/1Nq;->a:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->a(LX/88e;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 237779
    :catch_0
    move-exception v0

    .line 237780
    new-instance v1, LX/89E;

    invoke-direct {v1, v0}, LX/89E;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
