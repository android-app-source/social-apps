.class public LX/1hs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile k:LX/1hs;


# instance fields
.field public b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/util/regex/Pattern;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;

.field private final e:LX/1ht;

.field private final f:LX/0Uh;

.field private final g:LX/03V;

.field private final h:LX/0yN;

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0yh;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2gy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 297166
    const-class v0, LX/1hs;

    sput-object v0, LX/1hs;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0yN;LX/1ht;LX/0Or;LX/0Ot;LX/0Uh;LX/03V;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/common/annotations/CurrentlyActiveTokenType;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0yN;",
            "LX/1ht;",
            "LX/0Or",
            "<",
            "LX/0yh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2gy;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 297167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297168
    iput-object v0, p0, LX/1hs;->b:LX/0P1;

    .line 297169
    iput-object v0, p0, LX/1hs;->c:LX/0P1;

    .line 297170
    const-string v0, ""

    iput-object v0, p0, LX/1hs;->d:Ljava/lang/String;

    .line 297171
    iput-object p2, p0, LX/1hs;->e:LX/1ht;

    .line 297172
    iput-object p5, p0, LX/1hs;->f:LX/0Uh;

    .line 297173
    iput-object p6, p0, LX/1hs;->g:LX/03V;

    .line 297174
    iput-object p1, p0, LX/1hs;->h:LX/0yN;

    .line 297175
    iput-object p3, p0, LX/1hs;->i:LX/0Or;

    .line 297176
    iput-object p4, p0, LX/1hs;->j:LX/0Ot;

    .line 297177
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 297178
    iput-object v0, p0, LX/1hs;->c:LX/0P1;

    .line 297179
    return-void
.end method

.method public static a(LX/0QB;)LX/1hs;
    .locals 10

    .prologue
    .line 297180
    sget-object v0, LX/1hs;->k:LX/1hs;

    if-nez v0, :cond_1

    .line 297181
    const-class v1, LX/1hs;

    monitor-enter v1

    .line 297182
    :try_start_0
    sget-object v0, LX/1hs;->k:LX/1hs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 297183
    if-eqz v2, :cond_0

    .line 297184
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 297185
    new-instance v3, LX/1hs;

    invoke-static {v0}, LX/0yM;->a(LX/0QB;)LX/0yM;

    move-result-object v4

    check-cast v4, LX/0yN;

    .line 297186
    new-instance v8, LX/1ht;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, LX/0TD;

    invoke-direct {v8, v5, v6, v7}, LX/1ht;-><init>(LX/0tX;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0TD;)V

    .line 297187
    move-object v5, v8

    .line 297188
    check-cast v5, LX/1ht;

    const/16 v6, 0x13c1

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x13ee

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-direct/range {v3 .. v9}, LX/1hs;-><init>(LX/0yN;LX/1ht;LX/0Or;LX/0Ot;LX/0Uh;LX/03V;)V

    .line 297189
    move-object v0, v3

    .line 297190
    sput-object v0, LX/1hs;->k:LX/1hs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297191
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 297192
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 297193
    :cond_1
    sget-object v0, LX/1hs;->k:LX/1hs;

    return-object v0

    .line 297194
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 297195
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private c()V
    .locals 8

    .prologue
    .line 297196
    new-instance v0, LX/63m;

    invoke-direct {v0, p0}, LX/63m;-><init>(LX/1hs;)V

    .line 297197
    iget-object v1, p0, LX/1hs;->e:LX/1ht;

    .line 297198
    sget-boolean v2, LX/1ht;->d:Z

    if-eqz v2, :cond_0

    .line 297199
    :goto_0
    return-void

    .line 297200
    :cond_0
    new-instance v2, LX/63r;

    invoke-direct {v2}, LX/63r;-><init>()V

    move-object v2, v2

    .line 297201
    iget-object v3, v1, LX/1ht;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0df;->w:LX/0Tn;

    const-wide/32 v6, 0x93a80

    invoke-interface {v3, v4, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 297202
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    const-string v3, "ZeroBalanceRequest"

    invoke-static {v3}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v3

    .line 297203
    iput-object v3, v2, LX/0zO;->d:Ljava/util/Set;

    .line 297204
    move-object v2, v2

    .line 297205
    sget-object v3, LX/0zS;->a:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, LX/0zO;->a(J)LX/0zO;

    move-result-object v2

    .line 297206
    iget-object v3, v1, LX/1ht;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 297207
    const/4 v3, 0x1

    sput-boolean v3, LX/1ht;->d:Z

    .line 297208
    if-nez v2, :cond_2

    .line 297209
    :cond_1
    :goto_1
    goto :goto_0

    .line 297210
    :cond_2
    new-instance v3, LX/63y;

    invoke-direct {v3, v1}, LX/63y;-><init>(LX/1ht;)V

    iget-object v4, v1, LX/1ht;->b:LX/0TD;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 297211
    if-eqz v0, :cond_1

    .line 297212
    iget-object v4, v1, LX/1ht;->b:LX/0TD;

    invoke-static {v3, v0, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1
.end method

.method private d()Z
    .locals 3

    .prologue
    .line 297213
    iget-object v0, p0, LX/1hs;->f:LX/0Uh;

    const/16 v1, 0x336

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method private static declared-synchronized e(LX/1hs;)V
    .locals 6

    .prologue
    .line 297214
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/1hs;->h:LX/0yN;

    iget-object v0, p0, LX/1hs;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yh;

    invoke-virtual {v0}, LX/0yh;->getPoolPricingMapKey()LX/0Tn;

    move-result-object v0

    invoke-virtual {v0}, LX/0Tn;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-interface {v1, v0, v2}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 297215
    iget-object v0, p0, LX/1hs;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 297216
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 297217
    :cond_1
    :try_start_1
    iput-object v1, p0, LX/1hs;->d:Ljava/lang/String;

    .line 297218
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 297219
    iget-object v0, p0, LX/1hs;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gy;

    .line 297220
    iget-object v2, v0, LX/2gy;->b:LX/0lp;

    invoke-virtual {v2, v1}, LX/0lp;->b(Ljava/lang/String;)LX/15w;

    move-result-object v2

    .line 297221
    iget-object v3, v0, LX/2gy;->a:LX/0lC;

    invoke-virtual {v3, v2}, LX/0lD;->a(LX/15w;)LX/0lG;

    move-result-object v2

    check-cast v2, LX/0lF;

    .line 297222
    invoke-static {v2}, LX/2gy;->a(LX/0lF;)LX/0P1;

    move-result-object v2

    move-object v0, v2

    .line 297223
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    .line 297224
    iput-object v0, p0, LX/1hs;->c:LX/0P1;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297225
    goto :goto_0

    .line 297226
    :catch_0
    move-exception v0

    .line 297227
    :try_start_2
    sget-object v1, LX/1hs;->a:Ljava/lang/Class;

    const-string v2, "Error deserializing pool pricing map: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 297228
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized f()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 297229
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/1hs;->e(LX/1hs;)V

    .line 297230
    iget-object v0, p0, LX/1hs;->c:LX/0P1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 297231
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Z
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 297232
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/1H1;->f(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v3

    .line 297233
    :goto_0
    return v0

    .line 297234
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v5

    .line 297235
    if-nez v5, :cond_2

    move v0, v3

    .line 297236
    goto :goto_0

    .line 297237
    :cond_2
    const-class v1, LX/1hs;

    monitor-enter v1

    .line 297238
    :try_start_0
    iget-object v0, p0, LX/1hs;->b:LX/0P1;

    if-nez v0, :cond_3

    .line 297239
    const/16 v0, 0x3f

    new-array v0, v0, [Ljava/util/regex/Pattern;

    const/4 v2, 0x0

    const-string v4, "^i\\.org$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/4 v2, 0x1

    const-string v4, "^.*\\.i\\.org$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/4 v2, 0x2

    const-string v4, "^internet\\.org$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/4 v2, 0x3

    const-string v4, "^.*\\.internet\\.org$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/4 v2, 0x4

    const-string v4, "^.*\\.freeb6\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/4 v2, 0x5

    const-string v4, "^.*\\.freeb\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/4 v2, 0x6

    const-string v4, "^fb\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/4 v2, 0x7

    const-string v4, "^www\\.fb\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x8

    const-string v4, "^freebasic\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x9

    const-string v4, "^.*\\.freebasic\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0xa

    const-string v4, "^.*\\.freebasik\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0xb

    const-string v4, "^.*\\.frebasik\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0xc

    const-string v4, "^facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0xd

    const-string v4, "^z-m-.*\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0xe

    const-string v4, "^0\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0xf

    const-string v4, "^static-0\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x10

    const-string v4, "^static\\.0\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x11

    const-string v4, "^z-upload\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x12

    const-string v4, "^sd\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x13

    const-string v4, "^free\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x14

    const-string v4, "^people\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x15

    const-string v4, "^lite\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x16

    const-string v4, "^.*\\.lite\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x17

    const-string v4, "^h\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x18

    const-string v4, "^b-graph\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x19

    const-string v4, "^b-api\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x1a

    const-string v4, "^feedback\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x1b

    const-string v4, "^pixel\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x1c

    const-string v4, "^m\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x1d

    const-string v4, "^b-m\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x1e

    const-string v4, "^lm\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x1f

    const-string v4, "^n\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x20

    const-string v4, "^o\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x21

    const-string v4, "^z-graph-video\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x22

    const-string v4, "^zero\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x23

    const-string v4, "^ads\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x24

    const-string v4, "^hs\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x25

    const-string v4, "^connect\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x26

    const-string v4, "^light\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x27

    const-string v4, "^about\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x28

    const-string v4, "^new\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x29

    const-string v4, "^www\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x2a

    const-string v4, "^b-www\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x2b

    const-string v4, "^ec-www\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x2c

    const-string v4, "^x\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x2d

    const-string v4, "^pay\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x2e

    const-string v4, "^z\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x2f

    const-string v4, "^.*\\.z\\.facebook\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x30

    const-string v4, "^.*\\.freebs\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x31

    const-string v4, "^freebasics\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x32

    const-string v4, "^.*\\.freebasics\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x33

    const-string v4, "^.*\\.frebasics\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x34

    const-string v4, "^.*\\.fbasics\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x35

    const-string v4, "^.*\\.freebasicservices\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x36

    const-string v4, "^.*\\.freebasiks\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x37

    const-string v4, "^.*\\.frebasiks\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x38

    const-string v4, "^.*\\.freebasixs\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x39

    const-string v4, "^z-m-.*\\.fbsbx\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x3a

    const-string v4, "^.*\\.freebasix\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x3b

    const-string v4, "^.*\\.frebasix\\.com$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x3c

    const-string v4, "^z-m-.*\\.fbcdn\\.net$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x3d

    const-string v4, "^0\\.static\\.ak\\.fbcdn\\.net$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    const/16 v2, 0x3e

    const-string v4, "^z-m\\.ak\\.fbcdn\\.net$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    aput-object v4, v0, v2

    .line 297240
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 297241
    const-string v4, "z_free"

    invoke-virtual {v2, v4, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 297242
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    move-object v0, v0

    .line 297243
    iput-object v0, p0, LX/1hs;->b:LX/0P1;

    .line 297244
    invoke-direct {p0}, LX/1hs;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 297245
    invoke-direct {p0}, LX/1hs;->c()V

    .line 297246
    :cond_3
    iget-object v0, p0, LX/1hs;->c:LX/0P1;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/1hs;->c:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 297247
    :cond_4
    invoke-direct {p0}, LX/1hs;->f()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/1hs;->c:LX/0P1;

    .line 297248
    invoke-direct {p0}, LX/1hs;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/1hs;->c:LX/0P1;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/1hs;->c:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 297249
    :cond_5
    iget-object v0, p0, LX/1hs;->c:LX/0P1;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/1hs;->c:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 297250
    :cond_6
    iget-object v0, p0, LX/1hs;->g:LX/03V;

    const-string v2, "ZeroUriUtil"

    const-string v4, "Campaign API pool pricing map field missing."

    invoke-virtual {v0, v2, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 297251
    :cond_7
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 297252
    const-string v2, "z_free"

    const-string v4, "free"

    invoke-virtual {v0, v2, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 297253
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    move-object v0, v0

    .line 297254
    iput-object v0, p0, LX/1hs;->c:LX/0P1;

    .line 297255
    :cond_8
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 297256
    const/4 v2, 0x0

    .line 297257
    iget-object v0, p0, LX/1hs;->b:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 297258
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 297259
    iget-object v4, p0, LX/1hs;->c:LX/0P1;

    invoke-virtual {v4, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 297260
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 297261
    const-string v4, "free"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 297262
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/util/regex/Pattern;

    array-length v7, v0

    move-object v1, v2

    move v2, v3

    :goto_2
    if-ge v2, v7, :cond_c

    aget-object v8, v0, v2

    .line 297263
    if-nez v1, :cond_9

    .line 297264
    invoke-virtual {v8, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 297265
    :goto_3
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v8

    if-eqz v8, :cond_a

    move v0, v4

    .line 297266
    goto/16 :goto_0

    .line 297267
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 297268
    :cond_9
    invoke-virtual {v1, v8}, Ljava/util/regex/Matcher;->usePattern(Ljava/util/regex/Pattern;)Ljava/util/regex/Matcher;

    .line 297269
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->reset()Ljava/util/regex/Matcher;

    goto :goto_3

    .line 297270
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_b
    move-object v1, v2

    :cond_c
    move-object v2, v1

    .line 297271
    goto :goto_1

    :cond_d
    move v0, v3

    .line 297272
    goto/16 :goto_0
.end method
