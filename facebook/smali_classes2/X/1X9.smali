.class public final enum LX/1X9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1X9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1X9;

.field public static final enum BOTTOM:LX/1X9;

.field public static final enum BOX:LX/1X9;

.field public static final enum DIVIDER_BOTTOM:LX/1X9;

.field public static final enum DIVIDER_BOTTOM_NON_TOP:LX/1X9;

.field public static final enum DIVIDER_TOP:LX/1X9;

.field public static final enum FOLLOW_UP:LX/1X9;

.field public static final enum MIDDLE:LX/1X9;

.field public static final enum TOP:LX/1X9;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 270583
    new-instance v0, LX/1X9;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v3}, LX/1X9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1X9;->TOP:LX/1X9;

    .line 270584
    new-instance v0, LX/1X9;

    const-string v1, "MIDDLE"

    invoke-direct {v0, v1, v4}, LX/1X9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1X9;->MIDDLE:LX/1X9;

    .line 270585
    new-instance v0, LX/1X9;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v5}, LX/1X9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1X9;->BOTTOM:LX/1X9;

    .line 270586
    new-instance v0, LX/1X9;

    const-string v1, "BOX"

    invoke-direct {v0, v1, v6}, LX/1X9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1X9;->BOX:LX/1X9;

    .line 270587
    new-instance v0, LX/1X9;

    const-string v1, "DIVIDER_TOP"

    invoke-direct {v0, v1, v7}, LX/1X9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1X9;->DIVIDER_TOP:LX/1X9;

    .line 270588
    new-instance v0, LX/1X9;

    const-string v1, "DIVIDER_BOTTOM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/1X9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1X9;->DIVIDER_BOTTOM:LX/1X9;

    .line 270589
    new-instance v0, LX/1X9;

    const-string v1, "DIVIDER_BOTTOM_NON_TOP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/1X9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1X9;->DIVIDER_BOTTOM_NON_TOP:LX/1X9;

    .line 270590
    new-instance v0, LX/1X9;

    const-string v1, "FOLLOW_UP"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/1X9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1X9;->FOLLOW_UP:LX/1X9;

    .line 270591
    const/16 v0, 0x8

    new-array v0, v0, [LX/1X9;

    sget-object v1, LX/1X9;->TOP:LX/1X9;

    aput-object v1, v0, v3

    sget-object v1, LX/1X9;->MIDDLE:LX/1X9;

    aput-object v1, v0, v4

    sget-object v1, LX/1X9;->BOTTOM:LX/1X9;

    aput-object v1, v0, v5

    sget-object v1, LX/1X9;->BOX:LX/1X9;

    aput-object v1, v0, v6

    sget-object v1, LX/1X9;->DIVIDER_TOP:LX/1X9;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/1X9;->DIVIDER_BOTTOM:LX/1X9;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1X9;->DIVIDER_BOTTOM_NON_TOP:LX/1X9;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/1X9;->FOLLOW_UP:LX/1X9;

    aput-object v2, v0, v1

    sput-object v0, LX/1X9;->$VALUES:[LX/1X9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 270580
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1X9;
    .locals 1

    .prologue
    .line 270581
    const-class v0, LX/1X9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1X9;

    return-object v0
.end method

.method public static values()[LX/1X9;
    .locals 1

    .prologue
    .line 270582
    sget-object v0, LX/1X9;->$VALUES:[LX/1X9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1X9;

    return-object v0
.end method
