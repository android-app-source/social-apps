.class public LX/0pC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/analytics2/logger/Uploader;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/flexiblesampling/SamplingPolicyConfig;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/analytics2/logger/HandlerThreadFactory;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0pD;

.field public final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;LX/0pD;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/analytics2/logger/Uploader;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/flexiblesampling/SamplingPolicyConfig;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/analytics2/logger/HandlerThreadFactory;",
            ">;",
            "LX/0pD;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 143980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143981
    iput-object p1, p0, LX/0pC;->a:Ljava/lang/Class;

    .line 143982
    iput-object p2, p0, LX/0pC;->b:Ljava/lang/Class;

    .line 143983
    iput-object p3, p0, LX/0pC;->c:Ljava/lang/Class;

    .line 143984
    iput-object p4, p0, LX/0pC;->d:LX/0pD;

    .line 143985
    iput-object p5, p0, LX/0pC;->e:Ljava/lang/String;

    .line 143986
    return-void
.end method
