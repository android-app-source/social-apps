.class public LX/1AM;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/19K;",
        "LX/1ZI;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/1AM;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 210351
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 210352
    return-void
.end method

.method public static a(LX/0QB;)LX/1AM;
    .locals 3

    .prologue
    .line 210353
    sget-object v0, LX/1AM;->a:LX/1AM;

    if-nez v0, :cond_1

    .line 210354
    const-class v1, LX/1AM;

    monitor-enter v1

    .line 210355
    :try_start_0
    sget-object v0, LX/1AM;->a:LX/1AM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 210356
    if-eqz v2, :cond_0

    .line 210357
    :try_start_1
    new-instance v0, LX/1AM;

    invoke-direct {v0}, LX/1AM;-><init>()V

    .line 210358
    move-object v0, v0

    .line 210359
    sput-object v0, LX/1AM;->a:LX/1AM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210360
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 210361
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 210362
    :cond_1
    sget-object v0, LX/1AM;->a:LX/1AM;

    return-object v0

    .line 210363
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 210364
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
