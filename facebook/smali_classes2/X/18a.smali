.class public LX/18a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/18Z;


# instance fields
.field private final a:LX/18Y;


# direct methods
.method public constructor <init>(LX/18Y;)V
    .locals 0

    .prologue
    .line 206678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206679
    iput-object p1, p0, LX/18a;->a:LX/18Y;

    .line 206680
    return-void
.end method


# virtual methods
.method public final a(LX/2Vj;LX/15w;LX/11M;)LX/2WM;
    .locals 2

    .prologue
    .line 206681
    iget-object v0, p1, LX/2Vj;->c:Ljava/lang/String;

    move-object v0, v0

    .line 206682
    const-string v1, "All operations in a streaming batch must be named"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206683
    iget-object v0, p0, LX/18a;->a:LX/18Y;

    invoke-virtual {v0, p1, p2, p3}, LX/18Y;->a(LX/2Vj;LX/15w;LX/11M;)LX/2WM;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0n9;)V
    .locals 2

    .prologue
    .line 206684
    const-string v0, "flush"

    const-string v1, "1"

    .line 206685
    invoke-static {p1, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 206686
    return-void
.end method
