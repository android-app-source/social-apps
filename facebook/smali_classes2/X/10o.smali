.class public final LX/10o;
.super LX/10p;
.source ""


# instance fields
.field public final synthetic a:LX/0fK;


# direct methods
.method public constructor <init>(LX/0fK;LX/10b;)V
    .locals 0

    .prologue
    .line 169419
    iput-object p1, p0, LX/10o;->a:LX/0fK;

    invoke-direct {p0, p2}, LX/10p;-><init>(LX/10b;)V

    return-void
.end method


# virtual methods
.method public final a(LX/A7U;)V
    .locals 2

    .prologue
    .line 169420
    iget-object v0, p0, LX/10o;->a:LX/0fK;

    iget-object v0, v0, LX/0fK;->m:LX/10s;

    if-eqz v0, :cond_0

    .line 169421
    iget-object v0, p0, LX/10o;->a:LX/0fK;

    iget-object v0, v0, LX/0fK;->m:LX/10s;

    invoke-static {p1}, LX/0fK;->b(LX/A7U;)LX/0gs;

    move-result-object v1

    invoke-interface {v0, v1}, LX/10s;->a(LX/0gs;)V

    .line 169422
    :cond_0
    iget-object v0, p0, LX/10o;->a:LX/0fK;

    iget-object v0, v0, LX/0fK;->l:LX/2t7;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/10o;->a:LX/0fK;

    iget-object v0, v0, LX/0fK;->l:LX/2t7;

    iget-object v0, v0, LX/2t7;->a:LX/0fT;

    if-eqz v0, :cond_1

    .line 169423
    iget-object v0, p0, LX/10o;->a:LX/0fK;

    iget-object v0, v0, LX/0fK;->l:LX/2t7;

    iget-object v0, v0, LX/2t7;->a:LX/0fT;

    sget-object v1, LX/0gs;->ANIMATING:LX/0gs;

    invoke-interface {v0, v1}, LX/0fT;->a(LX/0gs;)V

    .line 169424
    :cond_1
    iget-object v0, p0, LX/10o;->a:LX/0fK;

    iget-object v0, v0, LX/0fK;->p:LX/10i;

    sget-object v1, LX/0gs;->ANIMATING:LX/0gs;

    invoke-virtual {v0, v1}, LX/10i;->a(LX/0gs;)V

    .line 169425
    return-void
.end method

.method public final b(LX/A7U;)V
    .locals 3

    .prologue
    .line 169426
    iget-object v0, p0, LX/10o;->a:LX/0fK;

    iget-object v1, v0, LX/0fK;->p:LX/10i;

    sget-object v0, LX/A7U;->OPENED:LX/A7U;

    if-ne p1, v0, :cond_2

    const/4 v0, 0x1

    .line 169427
    :goto_0
    invoke-static {v1}, LX/10i;->c(LX/10i;)LX/10k;

    move-result-object v2

    invoke-interface {v2, v0}, LX/10k;->a(Z)V

    .line 169428
    invoke-static {p1}, LX/0fK;->b(LX/A7U;)LX/0gs;

    move-result-object v0

    .line 169429
    iget-object v1, p0, LX/10o;->a:LX/0fK;

    iget-object v1, v1, LX/0fK;->m:LX/10s;

    if-eqz v1, :cond_0

    .line 169430
    iget-object v1, p0, LX/10o;->a:LX/0fK;

    iget-object v1, v1, LX/0fK;->m:LX/10s;

    invoke-interface {v1, v0}, LX/10s;->b(LX/0gs;)V

    .line 169431
    :cond_0
    iget-object v1, p0, LX/10o;->a:LX/0fK;

    iget-object v1, v1, LX/0fK;->l:LX/2t7;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/10o;->a:LX/0fK;

    iget-object v1, v1, LX/0fK;->l:LX/2t7;

    iget-object v1, v1, LX/2t7;->a:LX/0fT;

    if-eqz v1, :cond_1

    .line 169432
    iget-object v1, p0, LX/10o;->a:LX/0fK;

    iget-object v1, v1, LX/0fK;->l:LX/2t7;

    iget-object v1, v1, LX/2t7;->a:LX/0fT;

    invoke-static {p1}, LX/0fK;->b(LX/A7U;)LX/0gs;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0fT;->a(LX/0gs;)V

    .line 169433
    :cond_1
    iget-object v1, p0, LX/10o;->a:LX/0fK;

    iget-object v1, v1, LX/0fK;->p:LX/10i;

    invoke-virtual {v1, v0}, LX/10i;->a(LX/0gs;)V

    .line 169434
    return-void

    .line 169435
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(LX/A7U;)V
    .locals 2

    .prologue
    .line 169436
    iget-object v0, p0, LX/10o;->a:LX/0fK;

    iget-object v0, v0, LX/0fK;->p:LX/10i;

    invoke-static {p1}, LX/0fK;->b(LX/A7U;)LX/0gs;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/10i;->a(LX/0gs;)V

    .line 169437
    return-void
.end method
