.class public LX/0g5;
.super LX/0g6;
.source ""


# instance fields
.field private b:LX/1UQ;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V
    .locals 0

    .prologue
    .line 110486
    invoke-direct {p0, p1}, LX/0g6;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    .line 110487
    return-void
.end method

.method private E()Lcom/facebook/feed/ui/NewsFeedRecyclerView;
    .locals 1

    .prologue
    .line 110488
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 110489
    check-cast v0, Lcom/facebook/feed/ui/NewsFeedRecyclerView;

    return-object v0
.end method


# virtual methods
.method public final D()I
    .locals 3

    .prologue
    .line 110490
    iget-object v0, p0, LX/0g5;->b:LX/1UQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0g5;->b:LX/1UQ;

    invoke-interface {v0}, LX/1Qr;->d()I

    move-result v0

    if-nez v0, :cond_1

    .line 110491
    :cond_0
    const/4 v0, 0x0

    .line 110492
    :goto_0
    return v0

    .line 110493
    :cond_1
    invoke-direct {p0}, LX/0g5;->E()Lcom/facebook/feed/ui/NewsFeedRecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feed/ui/NewsFeedRecyclerView;->o()I

    move-result v0

    .line 110494
    iget-object v1, p0, LX/0g5;->b:LX/1UQ;

    invoke-virtual {v1}, LX/1UQ;->b()I

    move-result v1

    iget-object v2, p0, LX/0g5;->b:LX/1UQ;

    invoke-virtual {v2}, LX/1UQ;->c()I

    move-result v2

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 110495
    iget-object v1, p0, LX/0g5;->b:LX/1UQ;

    invoke-interface {v1, v0}, LX/1Qr;->h_(I)I

    move-result v0

    goto :goto_0
.end method

.method public final a(LX/1UQ;)V
    .locals 2

    .prologue
    .line 110496
    instance-of v0, p1, LX/1Cw;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 110497
    check-cast v0, LX/1Cw;

    invoke-virtual {p0, v0}, LX/0g7;->a(Landroid/widget/ListAdapter;)V

    .line 110498
    :goto_0
    iput-object p1, p0, LX/0g5;->b:LX/1UQ;

    .line 110499
    return-void

    .line 110500
    :cond_0
    instance-of v0, p1, LX/1OO;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 110501
    check-cast v0, LX/1OO;

    invoke-virtual {p0, v0}, LX/0g7;->a(LX/1OO;)V

    goto :goto_0

    .line 110502
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown adapter type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
