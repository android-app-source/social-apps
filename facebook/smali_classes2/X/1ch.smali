.class public final LX/1ch;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Z

.field public final synthetic c:LX/1bp;


# direct methods
.method public constructor <init>(LX/1bp;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 282503
    iput-object p1, p0, LX/1ch;->c:LX/1bp;

    iput-object p2, p0, LX/1ch;->a:Ljava/lang/String;

    iput-boolean p3, p0, LX/1ch;->b:Z

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final d(LX/1ca;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 282483
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    .line 282484
    invoke-interface {p1}, LX/1ca;->f()F

    move-result v1

    .line 282485
    iget-object v2, p0, LX/1ch;->c:LX/1bp;

    iget-object v3, p0, LX/1ch;->a:Ljava/lang/String;

    .line 282486
    invoke-static {v2, v3, p1}, LX/1bp;->a(LX/1bp;Ljava/lang/String;LX/1ca;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 282487
    const-string v4, "ignore_old_datasource @ onProgress"

    const/4 p0, 0x0

    invoke-static {v4, p0}, LX/1bp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 282488
    invoke-interface {p1}, LX/1ca;->g()Z

    .line 282489
    :cond_0
    :goto_0
    return-void

    .line 282490
    :cond_1
    if-nez v0, :cond_0

    .line 282491
    iget-object v4, v2, LX/1bp;->i:LX/1ag;

    const/4 p0, 0x0

    invoke-interface {v4, v1, p0}, LX/1ag;->a(FZ)V

    goto :goto_0
.end method

.method public final e(LX/1ca;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 282494
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v5

    .line 282495
    invoke-interface {p1}, LX/1ca;->f()F

    move-result v4

    .line 282496
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v3

    .line 282497
    if-eqz v3, :cond_1

    .line 282498
    iget-object v0, p0, LX/1ch;->c:LX/1bp;

    iget-object v1, p0, LX/1ch;->a:Ljava/lang/String;

    iget-boolean v6, p0, LX/1ch;->b:Z

    move-object v2, p1

    .line 282499
    invoke-static/range {v0 .. v6}, LX/1bp;->a$redex0(LX/1bp;Ljava/lang/String;LX/1ca;Ljava/lang/Object;FZZ)V

    .line 282500
    :cond_0
    :goto_0
    return-void

    .line 282501
    :cond_1
    if-eqz v5, :cond_0

    .line 282502
    iget-object v0, p0, LX/1ch;->c:LX/1bp;

    iget-object v1, p0, LX/1ch;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    const/4 v3, 0x1

    invoke-static {v0, v1, p1, v2, v3}, LX/1bp;->a$redex0(LX/1bp;Ljava/lang/String;LX/1ca;Ljava/lang/Throwable;Z)V

    goto :goto_0
.end method

.method public final f(LX/1ca;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 282492
    iget-object v0, p0, LX/1ch;->c:LX/1bp;

    iget-object v1, p0, LX/1ch;->a:Ljava/lang/String;

    invoke-interface {p1}, LX/1ca;->e()Ljava/lang/Throwable;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0, v1, p1, v2, v3}, LX/1bp;->a$redex0(LX/1bp;Ljava/lang/String;LX/1ca;Ljava/lang/Throwable;Z)V

    .line 282493
    return-void
.end method
