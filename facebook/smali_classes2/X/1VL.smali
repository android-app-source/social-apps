.class public LX/1VL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0sd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 258896
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 258897
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 258898
    iput-object v0, p0, LX/1VL;->a:LX/0Ot;

    .line 258899
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 258891
    invoke-virtual {p0, p1}, LX/1VL;->h(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Rc;

    move-result-object v2

    move v0, v1

    .line 258892
    :goto_0
    invoke-virtual {v2}, LX/0Rc;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    if-ge v0, p2, :cond_0

    .line 258893
    add-int/lit8 v0, v0, 0x1

    .line 258894
    invoke-virtual {v2}, LX/0Rc;->next()Ljava/lang/Object;

    goto :goto_0

    .line 258895
    :cond_0
    if-lt v0, p2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I
    .locals 1

    .prologue
    .line 258889
    invoke-static {p0}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v0

    .line 258890
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->x()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/1VL;
    .locals 2

    .prologue
    .line 258885
    new-instance v0, LX/1VL;

    invoke-direct {v0}, LX/1VL;-><init>()V

    .line 258886
    const/16 v1, 0x1224

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 258887
    iput-object v1, v0, LX/1VL;->a:LX/0Ot;

    .line 258888
    return-object v0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I
    .locals 1

    .prologue
    .line 258883
    invoke-static {p0}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v0

    .line 258884
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->w()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I
    .locals 1

    .prologue
    .line 258900
    invoke-static {p0}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v0

    .line 258901
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->v()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I
    .locals 4

    .prologue
    .line 258876
    invoke-virtual {p0, p1}, LX/1VL;->h(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Rc;

    move-result-object v2

    .line 258877
    const/4 v0, -0x1

    move v1, v0

    .line 258878
    :goto_0
    invoke-virtual {v2}, LX/0Rc;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258879
    invoke-virtual {v2}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 258880
    invoke-static {v0}, LX/1VL;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v3

    invoke-static {v0}, LX/1VL;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v0

    add-int/2addr v0, v3

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    .line 258881
    goto :goto_0

    .line 258882
    :cond_0
    if-lez v1, :cond_1

    :goto_1
    return v1

    :cond_1
    const/4 v1, 0x6

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I
    .locals 3

    .prologue
    .line 258871
    iget-object v0, p0, LX/1VL;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sd;

    .line 258872
    iget v1, v0, LX/0sd;->b:I

    move v0, v1

    .line 258873
    int-to-float v0, v0

    .line 258874
    invoke-static {p2}, LX/1VL;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0, p1}, LX/1VL;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 258875
    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    const/16 v1, 0x280

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 258865
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 258866
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 258867
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {p0, v0}, LX/1VL;->h(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Rc;

    move-result-object v2

    .line 258868
    :goto_0
    invoke-virtual {v2}, LX/0Rc;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258869
    invoke-virtual {v2}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {p1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 258870
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 258864
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, LX/1VL;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Z

    move-result v0

    return v0
.end method

.method public final g(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 258863
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/1VL;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Z

    move-result v0

    return v0
.end method

.method public final h(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Rc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ")",
            "LX/0Rc",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 258862
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    new-instance v1, LX/23x;

    invoke-direct {v1, p0}, LX/23x;-><init>(LX/1VL;)V

    invoke-static {v0, v1}, LX/0RZ;->b(Ljava/util/Iterator;LX/0Rl;)LX/0Rc;

    move-result-object v0

    return-object v0
.end method
