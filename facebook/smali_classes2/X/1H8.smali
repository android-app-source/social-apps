.class public final LX/1H8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1GA;

.field public b:Landroid/graphics/Bitmap$Config;

.field public c:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "LX/1HR;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/1Ao;

.field public final e:Landroid/content/Context;

.field public f:Z

.field public g:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "LX/1HR;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/1Ft;

.field public i:LX/1GF;

.field public j:LX/1GL;

.field public k:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/1Gf;

.field public m:LX/0rb;

.field public n:LX/1Gj;

.field public o:LX/1FZ;

.field public p:LX/1FB;

.field public q:LX/1Gv;

.field public r:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1BU;",
            ">;"
        }
    .end annotation
.end field

.field public s:Z

.field public t:LX/1Gf;

.field public u:LX/1HC;

.field public v:LX/4e5;

.field public final w:LX/1H9;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 226476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226477
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1H8;->f:Z

    .line 226478
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1H8;->s:Z

    .line 226479
    new-instance v0, LX/1H9;

    invoke-direct {v0, p0}, LX/1H9;-><init>(LX/1H8;)V

    iput-object v0, p0, LX/1H8;->w:LX/1H9;

    .line 226480
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LX/1H8;->e:Landroid/content/Context;

    .line 226481
    return-void
.end method


# virtual methods
.method public final a(LX/0rb;)LX/1H8;
    .locals 0

    .prologue
    .line 226470
    iput-object p1, p0, LX/1H8;->m:LX/0rb;

    .line 226471
    return-object p0
.end method

.method public final a(LX/1Ao;)LX/1H8;
    .locals 0

    .prologue
    .line 226472
    iput-object p1, p0, LX/1H8;->d:LX/1Ao;

    .line 226473
    return-object p0
.end method

.method public final a(LX/1FB;)LX/1H8;
    .locals 0

    .prologue
    .line 226474
    iput-object p1, p0, LX/1H8;->p:LX/1FB;

    .line 226475
    return-object p0
.end method

.method public final a(LX/1FZ;)LX/1H8;
    .locals 0

    .prologue
    .line 226482
    iput-object p1, p0, LX/1H8;->o:LX/1FZ;

    .line 226483
    return-object p0
.end method

.method public final a(LX/1Ft;)LX/1H8;
    .locals 0

    .prologue
    .line 226484
    iput-object p1, p0, LX/1H8;->h:LX/1Ft;

    .line 226485
    return-object p0
.end method

.method public final a(LX/1GA;)LX/1H8;
    .locals 0

    .prologue
    .line 226486
    iput-object p1, p0, LX/1H8;->a:LX/1GA;

    .line 226487
    return-object p0
.end method

.method public final a(LX/1GF;)LX/1H8;
    .locals 0

    .prologue
    .line 226488
    iput-object p1, p0, LX/1H8;->i:LX/1GF;

    .line 226489
    return-object p0
.end method

.method public final a(LX/1GL;)LX/1H8;
    .locals 0

    .prologue
    .line 226490
    iput-object p1, p0, LX/1H8;->j:LX/1GL;

    .line 226491
    return-object p0
.end method

.method public final a(LX/1Gd;)LX/1H8;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Gd",
            "<",
            "LX/1HR;",
            ">;)",
            "LX/1H8;"
        }
    .end annotation

    .prologue
    .line 226466
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Gd;

    iput-object v0, p0, LX/1H8;->c:LX/1Gd;

    .line 226467
    return-object p0
.end method

.method public final a(LX/1Gf;)LX/1H8;
    .locals 0

    .prologue
    .line 226468
    iput-object p1, p0, LX/1H8;->l:LX/1Gf;

    .line 226469
    return-object p0
.end method

.method public final a(LX/1Gj;)LX/1H8;
    .locals 0

    .prologue
    .line 226448
    iput-object p1, p0, LX/1H8;->n:LX/1Gj;

    .line 226449
    return-object p0
.end method

.method public final a(LX/1Gv;)LX/1H8;
    .locals 0

    .prologue
    .line 226450
    iput-object p1, p0, LX/1H8;->q:LX/1Gv;

    .line 226451
    return-object p0
.end method

.method public final a(LX/1HC;)LX/1H8;
    .locals 0

    .prologue
    .line 226452
    iput-object p1, p0, LX/1H8;->u:LX/1HC;

    .line 226453
    return-object p0
.end method

.method public final a(Ljava/util/Set;)LX/1H8;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/1BU;",
            ">;)",
            "LX/1H8;"
        }
    .end annotation

    .prologue
    .line 226454
    iput-object p1, p0, LX/1H8;->r:Ljava/util/Set;

    .line 226455
    return-object p0
.end method

.method public final a(Z)LX/1H8;
    .locals 0

    .prologue
    .line 226456
    iput-boolean p1, p0, LX/1H8;->f:Z

    .line 226457
    return-object p0
.end method

.method public final b(LX/1Gd;)LX/1H8;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Gd",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "LX/1H8;"
        }
    .end annotation

    .prologue
    .line 226458
    iput-object p1, p0, LX/1H8;->k:LX/1Gd;

    .line 226459
    return-object p0
.end method

.method public final b(LX/1Gf;)LX/1H8;
    .locals 0

    .prologue
    .line 226464
    iput-object p1, p0, LX/1H8;->t:LX/1Gf;

    .line 226465
    return-object p0
.end method

.method public final b(Z)LX/1H8;
    .locals 0

    .prologue
    .line 226460
    iput-boolean p1, p0, LX/1H8;->s:Z

    .line 226461
    return-object p0
.end method

.method public final b()LX/1H9;
    .locals 1

    .prologue
    .line 226462
    iget-object v0, p0, LX/1H8;->w:LX/1H9;

    return-object v0
.end method

.method public final c()LX/1H6;
    .locals 2

    .prologue
    .line 226463
    new-instance v0, LX/1H6;

    invoke-direct {v0, p0}, LX/1H6;-><init>(LX/1H8;)V

    return-object v0
.end method
