.class public LX/0pW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/0pW;


# instance fields
.field public final a:LX/0Yi;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0ks;

.field public volatile d:Z

.field public volatile e:Z


# direct methods
.method public constructor <init>(LX/0Yi;LX/0Ot;LX/0ks;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Yi;",
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;",
            "LX/0ks;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 144719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144720
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0pW;->e:Z

    .line 144721
    iput-object p1, p0, LX/0pW;->a:LX/0Yi;

    .line 144722
    iput-object p2, p0, LX/0pW;->b:LX/0Ot;

    .line 144723
    iput-object p3, p0, LX/0pW;->c:LX/0ks;

    .line 144724
    return-void
.end method

.method public static a(LX/0QB;)LX/0pW;
    .locals 6

    .prologue
    .line 144706
    sget-object v0, LX/0pW;->f:LX/0pW;

    if-nez v0, :cond_1

    .line 144707
    const-class v1, LX/0pW;

    monitor-enter v1

    .line 144708
    :try_start_0
    sget-object v0, LX/0pW;->f:LX/0pW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 144709
    if-eqz v2, :cond_0

    .line 144710
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 144711
    new-instance v5, LX/0pW;

    invoke-static {v0}, LX/0Yi;->a(LX/0QB;)LX/0Yi;

    move-result-object v3

    check-cast v3, LX/0Yi;

    const/16 v4, 0x245

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0ks;->a(LX/0QB;)LX/0ks;

    move-result-object v4

    check-cast v4, LX/0ks;

    invoke-direct {v5, v3, p0, v4}, LX/0pW;-><init>(LX/0Yi;LX/0Ot;LX/0ks;)V

    .line 144712
    move-object v0, v5

    .line 144713
    sput-object v0, LX/0pW;->f:LX/0pW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144714
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 144715
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 144716
    :cond_1
    sget-object v0, LX/0pW;->f:LX/0pW;

    return-object v0

    .line 144717
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 144718
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static k(LX/0pW;)V
    .locals 1

    .prologue
    .line 144703
    iget-object v0, p0, LX/0pW;->c:LX/0ks;

    .line 144704
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/0ks;->a:Z

    .line 144705
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedResult;Z)V
    .locals 13

    .prologue
    .line 144600
    iget-object v0, p0, LX/0pW;->a:LX/0Yi;

    const v12, 0xa0048

    const v11, 0xa0038

    const/4 v10, 0x0

    const/4 v5, 0x0

    const/4 v9, 0x1

    .line 144601
    iget-boolean v1, v0, LX/0Yi;->q:Z

    if-eqz v1, :cond_0

    .line 144602
    iput-boolean v10, v0, LX/0Yi;->q:Z

    .line 144603
    iget-object v1, v0, LX/0Yi;->k:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v7

    .line 144604
    iget-object v2, v0, LX/0Yi;->j:LX/0Yl;

    const v3, 0xa0047

    const-string v4, "NNFWarm_FragmentCreateToDataFetched"

    move-object v6, v5

    invoke-virtual/range {v2 .. v8}, LX/0Yl;->c(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    move-result-object v2

    const v3, 0xa0046

    const-string v4, "NNFWarm_DataFetchedToFirstRender"

    move-object v6, v5

    invoke-virtual/range {v2 .. v8}, LX/0Yl;->a(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    .line 144605
    :cond_0
    iget-boolean v1, v0, LX/0Yi;->t:Z

    if-nez v1, :cond_2

    invoke-static {v0}, LX/0Yi;->H(LX/0Yi;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v0}, LX/0Yi;->x(LX/0Yi;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 144606
    iget-boolean v1, v0, LX/0Yi;->u:Z

    if-eqz v1, :cond_1

    .line 144607
    iget-object v1, v0, LX/0Yi;->j:LX/0Yl;

    const v2, 0xa0053

    const-string v3, "NNFCold_FragmentCreateToDataFetched"

    invoke-virtual {v1, v2, v3}, LX/0Yl;->c(ILjava/lang/String;)LX/0Yl;

    .line 144608
    iget-object v1, v0, LX/0Yi;->j:LX/0Yl;

    const-string v2, "NNFCold_DataFetchedToFirstRender"

    invoke-virtual {v1, v12, v2}, LX/0Yl;->a(ILjava/lang/String;)LX/0Yl;

    .line 144609
    :cond_1
    iput-boolean v9, v0, LX/0Yi;->t:Z

    .line 144610
    :cond_2
    if-eqz p2, :cond_3

    invoke-static {v0}, LX/0Yi;->E(LX/0Yi;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 144611
    if-eqz p1, :cond_a

    .line 144612
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v1, v1

    .line 144613
    if-eqz v1, :cond_a

    .line 144614
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v1, v1

    .line 144615
    iget-object v2, v1, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v1, v2

    .line 144616
    if-eqz v1, :cond_a

    .line 144617
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v1, v1

    .line 144618
    iget-object v2, v1, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v1, v2

    .line 144619
    invoke-virtual {v1}, LX/0gf;->isManual()Z

    move-result v1

    if-eqz v1, :cond_a

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 144620
    if-eqz v1, :cond_3

    .line 144621
    iget-object v1, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0040

    const-string v3, "NNFPullToRefreshNetworkTime"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 144622
    const v1, 0xa0055

    const-string v2, "NNFPullToRefreshBeforeExecuteTime"

    invoke-static {v0, v1, v2}, LX/0Yi;->a(LX/0Yi;ILjava/lang/String;)V

    .line 144623
    :cond_3
    iget-boolean v1, v0, LX/0Yi;->r:Z

    if-eqz v1, :cond_7

    if-eqz p2, :cond_7

    .line 144624
    iget-object v1, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v1, v1

    .line 144625
    iget-boolean v2, v0, LX/0Yi;->s:Z

    if-nez v2, :cond_7

    sget-object v2, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    if-eq v1, v2, :cond_4

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v1, v2, :cond_7

    .line 144626
    :cond_4
    iput-boolean v9, v0, LX/0Yi;->s:Z

    .line 144627
    invoke-static {v0}, LX/0Yi;->P(LX/0Yi;)V

    .line 144628
    iget-object v1, v0, LX/0Yi;->j:LX/0Yl;

    const v2, 0xa0013

    const-string v3, "NNFWarmStartTTI"

    invoke-virtual {v1, v2, v3}, LX/0Yl;->f(ILjava/lang/String;)LX/0Yl;

    .line 144629
    iget-object v1, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0014

    const-string v3, "NNFFreshContentStart"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 144630
    iget-object v1, v0, LX/0Yi;->j:LX/0Yl;

    const v2, 0xa000f

    const-string v3, "NNFFreshFetch"

    invoke-virtual {v1, v2, v3}, LX/0Yl;->e(ILjava/lang/String;)LX/0Yl;

    .line 144631
    invoke-static {v0}, LX/0Yi;->H(LX/0Yi;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 144632
    iget-boolean v1, v0, LX/0Yi;->t:Z

    if-nez v1, :cond_6

    invoke-static {v0}, LX/0Yi;->x(LX/0Yi;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 144633
    iget-boolean v1, v0, LX/0Yi;->u:Z

    if-eqz v1, :cond_5

    .line 144634
    iget-object v1, v0, LX/0Yi;->j:LX/0Yl;

    const v2, 0xa0053

    const-string v3, "NNFCold_FragmentCreateToDataFetched"

    invoke-virtual {v1, v2, v3}, LX/0Yl;->c(ILjava/lang/String;)LX/0Yl;

    .line 144635
    iget-object v1, v0, LX/0Yi;->j:LX/0Yl;

    const-string v2, "NNFCold_DataFetchedToFirstRender"

    invoke-virtual {v1, v12, v2}, LX/0Yl;->a(ILjava/lang/String;)LX/0Yl;

    .line 144636
    :cond_5
    iput-boolean v9, v0, LX/0Yi;->t:Z

    .line 144637
    :cond_6
    const v1, 0xa004d

    const-string v2, "NNFColdStartNetwork"

    invoke-static {v0, v1, v2}, LX/0Yi;->c(LX/0Yi;ILjava/lang/String;)V

    .line 144638
    :cond_7
    iget-object v1, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa003e

    const-string v3, "NNFTailFetchTime"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 144639
    const v1, 0xa003f

    const-string v2, "NNFTailFetchRenderTime"

    invoke-static {v0, v1, v2, v10}, LX/0Yi;->a(LX/0Yi;ILjava/lang/String;Z)V

    .line 144640
    iget-object v1, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0042

    const-string v3, "NNFTailFetchNetworkCallTime"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 144641
    iget-object v1, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0043

    const-string v3, "NNFTailFetchNotConnectedCallTime"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 144642
    :cond_8
    iget-boolean v1, v0, LX/0Yi;->v:Z

    if-nez v1, :cond_9

    iget-object v1, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v2, "NNFHotStartTTI"

    invoke-interface {v1, v11, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    if-eqz p2, :cond_9

    .line 144643
    iput-boolean v9, v0, LX/0Yi;->v:Z

    .line 144644
    iget-object v1, v0, LX/0Yi;->j:LX/0Yl;

    const-string v2, "NNFHotStartTTI"

    invoke-virtual {v1, v11, v2}, LX/0Yl;->d(ILjava/lang/String;)LX/0Yl;

    .line 144645
    :cond_9
    return-void

    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;ZLcom/facebook/api/feedtype/FeedType;)V
    .locals 11

    .prologue
    .line 144662
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0pW;->d:Z

    .line 144663
    iget-object v0, p0, LX/0pW;->a:LX/0Yi;

    const/4 p0, 0x4

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x3

    const/4 v1, 0x0

    .line 144664
    if-nez p1, :cond_0

    .line 144665
    const-string p1, "UnknownError"

    .line 144666
    :cond_0
    const-string v2, "exception_name"

    invoke-static {v2, p1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v3

    .line 144667
    iget-boolean v2, v0, LX/0Yi;->q:Z

    if-eqz v2, :cond_1

    if-nez p2, :cond_1

    .line 144668
    iput-boolean v1, v0, LX/0Yi;->q:Z

    .line 144669
    sget-object v4, LX/0Yi;->b:[LX/0Yj;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 144670
    iget-object v7, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-virtual {v6, v3}, LX/0Yj;->a(Ljava/util/Map;)LX/0Yj;

    move-result-object v6

    invoke-interface {v7, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    .line 144671
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 144672
    :cond_1
    iget-boolean v2, v0, LX/0Yi;->r:Z

    if-eqz v2, :cond_3

    .line 144673
    iput-boolean v1, v0, LX/0Yi;->r:Z

    .line 144674
    sget-object v4, LX/0Yi;->d:[LX/0Yj;

    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    .line 144675
    iget-object v7, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-virtual {v6, v3}, LX/0Yj;->a(Ljava/util/Map;)LX/0Yj;

    move-result-object v6

    invoke-interface {v7, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    .line 144676
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 144677
    :cond_2
    iget-boolean v2, v0, LX/0Yi;->y:Z

    if-nez v2, :cond_3

    .line 144678
    iget-object v2, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v4, LX/0Yj;

    const v5, 0xa0016

    const-string v6, "NNFColdStartTTI"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    invoke-virtual {v4, v3}, LX/0Yj;->a(Ljava/util/Map;)LX/0Yj;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    .line 144679
    :cond_3
    iget-object v2, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v4, 0xa0038

    const-string v5, "NNFHotStartTTI"

    invoke-interface {v2, v4, v5}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 144680
    sget-object v4, LX/0Yi;->g:[LX/0Yj;

    array-length v5, v4

    move v2, v1

    :goto_2
    if-ge v2, v5, :cond_4

    aget-object v6, v4, v2

    .line 144681
    iget-object v7, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-virtual {v6, v3}, LX/0Yj;->a(Ljava/util/Map;)LX/0Yj;

    move-result-object v6

    invoke-interface {v7, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    .line 144682
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 144683
    :cond_4
    iget-object v2, v0, LX/0Yi;->j:LX/0Yl;

    const/4 v3, 0x6

    new-array v3, v3, [LX/0Yj;

    new-instance v4, LX/0Yj;

    const v5, 0xa0001

    const-string v6, "NNFColdStart"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v1

    new-instance v4, LX/0Yj;

    const v5, 0xa003c

    const-string v6, "NNFColdStartChromeLoadTime"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v9

    new-instance v4, LX/0Yj;

    const v5, 0xa0016

    const-string v6, "NNFColdStartTTI"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v10

    new-instance v4, LX/0Yj;

    const v5, 0xa004d

    const-string v6, "NNFColdStartNetwork"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v8

    new-instance v4, LX/0Yj;

    const v5, 0xa0020

    const-string v6, "NNFColdStartAndRenderTime"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, p0

    const/4 v4, 0x5

    new-instance v5, LX/0Yj;

    const v6, 0xa003a

    const-string v7, "NNFFirstRunColdStart"

    invoke-direct {v5, v6, v7}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v5, v3, v4

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 144684
    invoke-static {v2, v3}, LX/0Yl;->d(LX/0Yl;Ljava/util/List;)V

    .line 144685
    iget-object v2, v0, LX/0Yi;->j:LX/0Yl;

    const/4 v3, 0x6

    new-array v3, v3, [LX/0Yj;

    new-instance v4, LX/0Yj;

    const v5, 0xa0013

    const-string v6, "NNFWarmStartTTI"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v1

    new-instance v4, LX/0Yj;

    const v5, 0xa0023

    const-string v6, "NNFWarmStartAndRenderTime"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v9

    new-instance v4, LX/0Yj;

    const v5, 0xa0024

    const-string v6, "NNFWarmStartAndFreshRenderTime"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v10

    new-instance v4, LX/0Yj;

    const v5, 0xa0025

    const-string v6, "NNFWarmStartAndCachedRenderTime"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v8

    new-instance v4, LX/0Yj;

    const v5, 0xa0004

    const-string v6, "NNFWarmStart"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, p0

    const/4 v4, 0x5

    new-instance v5, LX/0Yj;

    const v6, 0xa0014

    const-string v7, "NNFFreshContentStart"

    invoke-direct {v5, v6, v7}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v5, v3, v4

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 144686
    invoke-static {v2, v3}, LX/0Yl;->d(LX/0Yl;Ljava/util/List;)V

    .line 144687
    iget-object v2, v0, LX/0Yi;->j:LX/0Yl;

    new-array v3, p0, [LX/0Yj;

    new-instance v4, LX/0Yj;

    const v5, 0xa0038

    const-string v6, "NNFHotStartTTI"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v1

    new-instance v4, LX/0Yj;

    const v5, 0xa002b

    const-string v6, "NNFHotStartAndRenderTime"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v9

    new-instance v4, LX/0Yj;

    const v5, 0xa002c

    const-string v6, "NNFHotStartAndFreshRenderTime"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v10

    new-instance v4, LX/0Yj;

    const v5, 0xa004a

    const-string v6, "NNFHotStartAndFreshRenderTimeNotVisible"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v8

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 144688
    invoke-static {v2, v3}, LX/0Yl;->d(LX/0Yl;Ljava/util/List;)V

    .line 144689
    sget-object v3, LX/0Yi;->e:[LX/0Yj;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_5

    aget-object v5, v3, v2

    .line 144690
    iget-object v6, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v6, v5}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    .line 144691
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 144692
    :cond_5
    sget-object v2, LX/0Yi;->f:[I

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_6

    aget v4, v2, v1

    .line 144693
    iget-object v5, v0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v5, v4, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 144694
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 144695
    :cond_6
    iget-object v1, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0040

    const-string v3, "NNFPullToRefreshNetworkTime"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 144696
    iget-object v1, v0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0xa0041

    invoke-interface {v1, v2, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 144697
    const v1, 0xa0055

    const-string v2, "NNFPullToRefreshBeforeExecuteTime"

    invoke-static {v0, v1, v2}, LX/0Yi;->a(LX/0Yi;ILjava/lang/String;)V

    .line 144698
    if-nez p2, :cond_7

    .line 144699
    iget-object v1, v0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x230013

    invoke-interface {v1, v2, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 144700
    :cond_7
    iget-object v1, v0, LX/0Yi;->j:LX/0Yl;

    iget-object v2, v0, LX/0Yi;->o:LX/0Ym;

    invoke-virtual {v2}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Yi;->f(Lcom/facebook/api/feedtype/FeedType;)I

    move-result v2

    const-string v3, "FragmentResumeToRender"

    invoke-virtual {v0, v3, p3}, LX/0Yi;->a(Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0Yl;->g(ILjava/lang/String;)LX/0Yl;

    .line 144701
    invoke-static {v0}, LX/0Yi;->N(LX/0Yi;)V

    .line 144702
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 144659
    iget-object v0, p0, LX/0pW;->a:LX/0Yi;

    .line 144660
    iput-boolean p1, v0, LX/0Yi;->B:Z

    .line 144661
    return-void
.end method

.method public final b()V
    .locals 11

    .prologue
    .line 144646
    iget-object v0, p0, LX/0pW;->a:LX/0Yi;

    const-wide/16 v9, -0x1

    const v5, 0x230016

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 144647
    iget-wide v1, v0, LX/0Yi;->w:J

    cmp-long v1, v1, v9

    if-lez v1, :cond_1

    .line 144648
    iget-object v1, v0, LX/0Yi;->m:LX/0Uo;

    invoke-virtual {v1}, LX/0Uo;->k()LX/03R;

    move-result-object v1

    sget-object v2, LX/03R;->YES:LX/03R;

    if-eq v1, v2, :cond_0

    .line 144649
    iget-object v1, v0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-wide v3, v0, LX/0Yi;->w:J

    invoke-interface {v1, v5, v6, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerStart(IIJ)V

    .line 144650
    iget-object v1, v0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v5, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 144651
    :cond_0
    iput-wide v9, v0, LX/0Yi;->w:J

    .line 144652
    :cond_1
    invoke-static {v0}, LX/0Yi;->C(LX/0Yi;)V

    .line 144653
    iget-object v1, v0, LX/0Yi;->j:LX/0Yl;

    const/4 v2, 0x7

    new-array v2, v2, [LX/0Yj;

    new-instance v3, LX/0Yj;

    const v4, 0xa0013

    const-string v5, "NNFWarmStartTTI"

    invoke-direct {v3, v4, v5}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v3, v2, v6

    const/4 v3, 0x1

    new-instance v4, LX/0Yj;

    const v5, 0xa0023

    const-string v6, "NNFWarmStartAndRenderTime"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v2, v3

    new-instance v3, LX/0Yj;

    const v4, 0xa0024

    const-string v5, "NNFWarmStartAndFreshRenderTime"

    invoke-direct {v3, v4, v5}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v3, v2, v7

    const/4 v3, 0x3

    new-instance v4, LX/0Yj;

    const v5, 0xa0025

    const-string v6, "NNFWarmStartAndCachedRenderTime"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x4

    new-instance v4, LX/0Yj;

    const v5, 0xa0004

    const-string v6, "NNFWarmStart"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x5

    new-instance v4, LX/0Yj;

    const v5, 0xa0014

    const-string v6, "NNFFreshContentStart"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x6

    new-instance v4, LX/0Yj;

    const v5, 0xa00a7

    const-string v6, "NNFInspirationCameraWarmTTI"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 144654
    invoke-static {v1, v2}, LX/0Yl;->d(LX/0Yl;Ljava/util/List;)V

    .line 144655
    iget-object v1, v0, LX/0Yi;->j:LX/0Yl;

    iget-object v2, v0, LX/0Yi;->o:LX/0Ym;

    invoke-virtual {v2}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Yi;->f(Lcom/facebook/api/feedtype/FeedType;)I

    move-result v2

    const-string v3, "FragmentResumeToRender"

    iget-object v4, v0, LX/0Yi;->o:LX/0Ym;

    invoke-virtual {v4}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0Yi;->a(Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0Yl;->g(ILjava/lang/String;)LX/0Yl;

    .line 144656
    invoke-static {v0}, LX/0Yi;->N(LX/0Yi;)V

    .line 144657
    invoke-static {p0}, LX/0pW;->k(LX/0pW;)V

    .line 144658
    return-void
.end method
