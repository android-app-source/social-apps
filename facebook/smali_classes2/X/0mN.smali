.class public final LX/0mN;
.super Landroid/database/Observable;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/database/Observable",
        "<",
        "Lcom/facebook/analytics2/logger/SessionManagerCallback;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 132443
    invoke-direct {p0}, Landroid/database/Observable;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 132415
    iget-object v2, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    monitor-enter v2

    .line 132416
    :try_start_0
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 132417
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 132418
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0q8;

    .line 132419
    invoke-static {v0}, LX/0q8;->e(LX/0q8;)V

    .line 132420
    invoke-static {v0}, LX/0q8;->g(LX/0q8;)V

    .line 132421
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 132422
    :cond_0
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 132444
    iget-object v2, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    monitor-enter v2

    .line 132445
    :try_start_0
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 132446
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 132447
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0q8;

    .line 132448
    invoke-static {v0}, LX/0q8;->e(LX/0q8;)V

    .line 132449
    iput-object p1, v0, LX/0q8;->c:Ljava/lang/String;

    .line 132450
    invoke-static {v0}, LX/0q8;->g(LX/0q8;)V

    .line 132451
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 132452
    :cond_0
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 132435
    iget-object v2, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    monitor-enter v2

    .line 132436
    :try_start_0
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 132437
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 132438
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0q8;

    .line 132439
    invoke-static {v0}, LX/0q8;->e(LX/0q8;)V

    .line 132440
    invoke-static {v0}, LX/0q8;->g(LX/0q8;)V

    .line 132441
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 132442
    :cond_0
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 132423
    iget-object v2, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    monitor-enter v2

    .line 132424
    :try_start_0
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 132425
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 132426
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0q8;

    .line 132427
    invoke-static {v0}, LX/0q8;->e(LX/0q8;)V

    .line 132428
    iget-object v4, v0, LX/0q8;->c:Ljava/lang/String;

    .line 132429
    const/4 v5, 0x0

    iput-object v5, v0, LX/0q8;->c:Ljava/lang/String;

    .line 132430
    iget-object v5, v0, LX/0q8;->b:LX/0mk;

    invoke-virtual {v5}, LX/0mk;->d()LX/0pE;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/0pE;->b(Ljava/lang/String;)V

    .line 132431
    iget-object v5, v0, LX/0q8;->b:LX/0mk;

    invoke-virtual {v5}, LX/0mk;->b()LX/0pE;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/0pE;->b(Ljava/lang/String;)V

    .line 132432
    invoke-static {v0}, LX/0q8;->g(LX/0q8;)V

    .line 132433
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 132434
    :cond_0
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
