.class public LX/15q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/15q;


# instance fields
.field private final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 181591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181592
    iput-object p1, p0, LX/15q;->a:LX/0ad;

    .line 181593
    return-void
.end method

.method public static a(LX/0QB;)LX/15q;
    .locals 4

    .prologue
    .line 181594
    sget-object v0, LX/15q;->b:LX/15q;

    if-nez v0, :cond_1

    .line 181595
    const-class v1, LX/15q;

    monitor-enter v1

    .line 181596
    :try_start_0
    sget-object v0, LX/15q;->b:LX/15q;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 181597
    if-eqz v2, :cond_0

    .line 181598
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 181599
    new-instance p0, LX/15q;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/15q;-><init>(LX/0ad;)V

    .line 181600
    move-object v0, p0

    .line 181601
    sput-object v0, LX/15q;->b:LX/15q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 181602
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 181603
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 181604
    :cond_1
    sget-object v0, LX/15q;->b:LX/15q;

    return-object v0

    .line 181605
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 181606
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 181607
    iget-object v0, p0, LX/15q;->a:LX/0ad;

    sget-short v1, LX/15r;->B:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
