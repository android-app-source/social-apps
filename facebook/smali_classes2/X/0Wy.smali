.class public LX/0Wy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Wx;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76886
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/0eT;
    .locals 1

    .prologue
    .line 76891
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;
    .locals 1

    .prologue
    .line 76890
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;
    .locals 1

    .prologue
    .line 76889
    const/4 v0, 0x0

    return-object v0
.end method

.method public final clearCurrentUserData()V
    .locals 0

    .prologue
    .line 76888
    return-void
.end method

.method public final clearOverrides()V
    .locals 0

    .prologue
    .line 76887
    return-void
.end method

.method public final deleteOldUserData(I)V
    .locals 0

    .prologue
    .line 76885
    return-void
.end method

.method public final getFrameworkStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76884
    const-string v0, "UNINITIALIZED"

    return-object v0
.end method

.method public final getQEInfoFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76883
    const-string v0, ""

    return-object v0
.end method

.method public final isQEInfoAvailable()Z
    .locals 1

    .prologue
    .line 76892
    const/4 v0, 0x0

    return v0
.end method

.method public final isTigonServiceSet()Z
    .locals 1

    .prologue
    .line 76882
    const/4 v0, 0x0

    return v0
.end method

.method public final isValid()Z
    .locals 1

    .prologue
    .line 76881
    const/4 v0, 0x0

    return v0
.end method

.method public final logExposure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 76880
    return-void
.end method

.method public final logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 76879
    return-void
.end method

.method public final refreshConfigInfos(I)Z
    .locals 1

    .prologue
    .line 76873
    const/4 v0, 0x0

    return v0
.end method

.method public final registerConfigChangeListener(Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;)Z
    .locals 1

    .prologue
    .line 76878
    const/4 v0, 0x0

    return v0
.end method

.method public final setTigonService(Lcom/facebook/tigon/iface/TigonServiceHolder;Z)V
    .locals 0

    .prologue
    .line 76877
    return-void
.end method

.method public final tryUpdateConfigsSynchronously(I)Z
    .locals 1

    .prologue
    .line 76876
    const/4 v0, 0x0

    return v0
.end method

.method public final updateConfigs()Z
    .locals 1

    .prologue
    .line 76875
    const/4 v0, 0x0

    return v0
.end method

.method public final updateConfigsSynchronously(I)Z
    .locals 1

    .prologue
    .line 76874
    const/4 v0, 0x0

    return v0
.end method
