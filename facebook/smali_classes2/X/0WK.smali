.class public LX/0WK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0WK;


# instance fields
.field public final a:LX/0WS;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0WP;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0WP;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 76068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76069
    const-string v0, "authentication"

    invoke-virtual {p1, v0}, LX/0WP;->a(Ljava/lang/String;)LX/0WS;

    move-result-object v0

    iput-object v0, p0, LX/0WK;->a:LX/0WS;

    .line 76070
    iput-object p2, p0, LX/0WK;->b:LX/0Ot;

    .line 76071
    return-void
.end method

.method public static a(LX/0QB;)LX/0WK;
    .locals 5

    .prologue
    .line 76072
    sget-object v0, LX/0WK;->c:LX/0WK;

    if-nez v0, :cond_1

    .line 76073
    const-class v1, LX/0WK;

    monitor-enter v1

    .line 76074
    :try_start_0
    sget-object v0, LX/0WK;->c:LX/0WK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 76075
    if-eqz v2, :cond_0

    .line 76076
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 76077
    new-instance v4, LX/0WK;

    invoke-static {v0}, LX/0WL;->a(LX/0QB;)LX/0WP;

    move-result-object v3

    check-cast v3, LX/0WP;

    const/16 p0, 0xf9a

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/0WK;-><init>(LX/0WP;LX/0Ot;)V

    .line 76078
    move-object v0, v4

    .line 76079
    sput-object v0, LX/0WK;->c:LX/0WK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76080
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 76081
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 76082
    :cond_1
    sget-object v0, LX/0WK;->c:LX/0WK;

    return-object v0

    .line 76083
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 76084
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1gW;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 76085
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "No user ID in credentials"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 76086
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "No token in credentials"

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 76087
    invoke-interface {p6}, LX/1gW;->a()LX/1gW;

    .line 76088
    const-string v0, "uid"

    invoke-interface {p6, v0, p0}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;

    .line 76089
    const-string v0, "access_token"

    invoke-interface {p6, v0, p1}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;

    .line 76090
    const-string v0, "session_cookies_string"

    invoke-interface {p6, v0, p2}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;

    .line 76091
    const-string v0, "secret"

    invoke-interface {p6, v0, p3}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;

    .line 76092
    const-string v0, "session_key"

    invoke-interface {p6, v0, p4}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;

    .line 76093
    const-string v0, "username"

    invoke-interface {p6, v0, p5}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;

    .line 76094
    return-void

    :cond_0
    move v0, v2

    .line 76095
    goto :goto_0

    :cond_1
    move v1, v2

    .line 76096
    goto :goto_1
.end method

.method public static f(LX/0WK;)V
    .locals 2

    .prologue
    .line 76097
    iget-object v0, p0, LX/0WK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/26p;->c:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/26p;->d:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/26p;->e:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/26p;->g:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/26p;->h:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/26p;->i:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 76098
    return-void
.end method
