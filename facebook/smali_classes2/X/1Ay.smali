.class public LX/1Ay;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/1Ay;


# instance fields
.field private volatile a:Ljava/util/Date;

.field private volatile b:LX/1B0;

.field private volatile c:LX/1B0;

.field private volatile d:LX/0lF;

.field private volatile e:Ljava/lang/String;

.field private volatile f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation
.end field

.field public final g:LX/0Zb;

.field private final h:LX/17Q;

.field private final i:Ljava/util/concurrent/ExecutorService;

.field private volatile j:LX/1Az;


# direct methods
.method public constructor <init>(LX/0Zb;LX/17Q;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 211538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211539
    sget-object v0, LX/1Az;->RESUME:LX/1Az;

    iput-object v0, p0, LX/1Ay;->j:LX/1Az;

    .line 211540
    iput-object p1, p0, LX/1Ay;->g:LX/0Zb;

    .line 211541
    iput-object p2, p0, LX/1Ay;->h:LX/17Q;

    .line 211542
    iput-object p3, p0, LX/1Ay;->i:Ljava/util/concurrent/ExecutorService;

    .line 211543
    new-instance v0, LX/1B0;

    invoke-direct {v0}, LX/1B0;-><init>()V

    iput-object v0, p0, LX/1Ay;->b:LX/1B0;

    .line 211544
    new-instance v0, LX/1B0;

    invoke-direct {v0}, LX/1B0;-><init>()V

    iput-object v0, p0, LX/1Ay;->c:LX/1B0;

    .line 211545
    return-void
.end method

.method public static a(LX/0QB;)LX/1Ay;
    .locals 6

    .prologue
    .line 211525
    sget-object v0, LX/1Ay;->k:LX/1Ay;

    if-nez v0, :cond_1

    .line 211526
    const-class v1, LX/1Ay;

    monitor-enter v1

    .line 211527
    :try_start_0
    sget-object v0, LX/1Ay;->k:LX/1Ay;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 211528
    if-eqz v2, :cond_0

    .line 211529
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 211530
    new-instance p0, LX/1Ay;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v4

    check-cast v4, LX/17Q;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v3, v4, v5}, LX/1Ay;-><init>(LX/0Zb;LX/17Q;Ljava/util/concurrent/ExecutorService;)V

    .line 211531
    move-object v0, p0

    .line 211532
    sput-object v0, LX/1Ay;->k:LX/1Ay;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211533
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 211534
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 211535
    :cond_1
    sget-object v0, LX/1Ay;->k:LX/1Ay;

    return-object v0

    .line 211536
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 211537
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    .line 211515
    iget-object v0, p0, LX/1Ay;->j:LX/1Az;

    sget-object v1, LX/1Az;->RESUME:LX/1Az;

    if-ne v0, v1, :cond_0

    .line 211516
    :goto_0
    return-void

    .line 211517
    :cond_0
    iget-object v3, p0, LX/1Ay;->a:Ljava/util/Date;

    .line 211518
    iget-object v0, p0, LX/1Ay;->b:LX/1B0;

    invoke-virtual {v0}, LX/1B0;->doubleValue()D

    move-result-wide v6

    .line 211519
    iget-object v0, p0, LX/1Ay;->c:LX/1B0;

    invoke-virtual {v0}, LX/1B0;->doubleValue()D

    move-result-wide v4

    .line 211520
    iget-object v8, p0, LX/1Ay;->e:Ljava/lang/String;

    .line 211521
    iget-object v9, p0, LX/1Ay;->d:LX/0lF;

    .line 211522
    iget-object v10, p0, LX/1Ay;->f:Ljava/util/Map;

    .line 211523
    iget-object v0, p0, LX/1Ay;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/feed/analytics/LongClickTracker$1;

    move-object v2, p0

    invoke-direct/range {v1 .. v10}, Lcom/facebook/feed/analytics/LongClickTracker$1;-><init>(LX/1Ay;Ljava/util/Date;DDLjava/lang/String;LX/0lF;Ljava/util/Map;)V

    const v2, -0x1067a618

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 211524
    sget-object v0, LX/1Az;->RESUME:LX/1Az;

    iput-object v0, p0, LX/1Ay;->j:LX/1Az;

    goto :goto_0
.end method

.method public final a(LX/0lF;Ljava/lang/String;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 211507
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, LX/1Ay;->a:Ljava/util/Date;

    .line 211508
    iput-object p1, p0, LX/1Ay;->d:LX/0lF;

    .line 211509
    iput-object p2, p0, LX/1Ay;->e:Ljava/lang/String;

    .line 211510
    iget-object v0, p0, LX/1Ay;->b:LX/1B0;

    invoke-virtual {v0, v2, v3}, LX/1B0;->a(D)V

    .line 211511
    iget-object v0, p0, LX/1Ay;->c:LX/1B0;

    invoke-virtual {v0, v2, v3}, LX/1B0;->a(D)V

    .line 211512
    sget-object v0, LX/1Az;->LINK:LX/1Az;

    iput-object v0, p0, LX/1Ay;->j:LX/1Az;

    .line 211513
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Ay;->f:Ljava/util/Map;

    .line 211514
    return-void
.end method

.method public final a(Ljava/lang/String;DDLjava/util/Map;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "DD",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 211500
    iput-object p1, p0, LX/1Ay;->e:Ljava/lang/String;

    .line 211501
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Ay;->a:Ljava/util/Date;

    .line 211502
    iget-object v0, p0, LX/1Ay;->b:LX/1B0;

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double v2, p2, v2

    invoke-virtual {v0, v2, v3}, LX/1B0;->a(D)V

    .line 211503
    iget-object v0, p0, LX/1Ay;->c:LX/1B0;

    invoke-virtual {v0, p4, p5}, LX/1B0;->a(D)V

    .line 211504
    sget-object v0, LX/1Az;->LINK:LX/1Az;

    iput-object v0, p0, LX/1Ay;->j:LX/1Az;

    .line 211505
    iput-object p6, p0, LX/1Ay;->f:Ljava/util/Map;

    .line 211506
    return-void
.end method
