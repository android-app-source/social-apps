.class public LX/13R;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/13S;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/13S;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 176832
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/13S;
    .locals 7

    .prologue
    .line 176820
    sget-object v0, LX/13R;->a:LX/13S;

    if-nez v0, :cond_1

    .line 176821
    const-class v1, LX/13R;

    monitor-enter v1

    .line 176822
    :try_start_0
    sget-object v0, LX/13R;->a:LX/13S;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 176823
    if-eqz v2, :cond_0

    .line 176824
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 176825
    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {v0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v6

    check-cast v6, LX/0So;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object p0

    check-cast p0, Landroid/os/Handler;

    invoke-static {v3, v4, v5, v6, p0}, Lcom/facebook/analytics/AnalyticsClientModule;->a(LX/0Xl;LX/0Zb;LX/0So;LX/0So;Landroid/os/Handler;)LX/13S;

    move-result-object v3

    move-object v0, v3

    .line 176826
    sput-object v0, LX/13R;->a:LX/13S;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 176827
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 176828
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 176829
    :cond_1
    sget-object v0, LX/13R;->a:LX/13S;

    return-object v0

    .line 176830
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 176831
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 176819
    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v0

    check-cast v0, LX/0Xl;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v2

    check-cast v2, LX/0So;

    invoke-static {p0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v4

    check-cast v4, Landroid/os/Handler;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/analytics/AnalyticsClientModule;->a(LX/0Xl;LX/0Zb;LX/0So;LX/0So;Landroid/os/Handler;)LX/13S;

    move-result-object v0

    return-object v0
.end method
