.class public LX/0XX;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/quicklog/QuickPerformanceLogger;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/quicklog/QuickPerformanceLogger;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79016
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;
    .locals 3

    .prologue
    .line 79017
    sget-object v0, LX/0XX;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-nez v0, :cond_1

    .line 79018
    const-class v1, LX/0XX;

    monitor-enter v1

    .line 79019
    :try_start_0
    sget-object v0, LX/0XX;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 79020
    if-eqz v2, :cond_0

    .line 79021
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/0XX;->b(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v0

    sput-object v0, LX/0XX;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79022
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 79023
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 79024
    :cond_1
    sget-object v0, LX/0XX;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    return-object v0

    .line 79025
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 79026
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;
    .locals 14

    .prologue
    .line 79027
    const/16 v0, 0x103b

    invoke-static {p0, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0}, LX/0XY;->a(LX/0QB;)LX/0Xe;

    move-result-object v1

    check-cast v1, LX/0Xe;

    const/16 v2, 0x3034

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v5

    check-cast v5, LX/0Uo;

    invoke-static {p0}, LX/0Xf;->a(LX/0QB;)Ljava/util/concurrent/ExecutorService;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/03e;->a(LX/0QB;)LX/03e;

    move-result-object v7

    check-cast v7, LX/03e;

    invoke-static {p0}, LX/0Xh;->a(LX/0QB;)LX/0Xh;

    move-result-object v8

    check-cast v8, LX/0Xh;

    invoke-static {p0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v10

    check-cast v10, LX/0Xl;

    invoke-static {p0}, LX/0Xy;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v11

    invoke-static {p0}, LX/0Xz;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v12

    invoke-static {p0}, LX/0Y0;->a(LX/0QB;)LX/0Y0;

    move-result-object v13

    check-cast v13, LX/0Y0;

    invoke-static/range {v0 .. v13}, LX/0Xd;->a(LX/0Ot;LX/0Xe;LX/0Ot;LX/0So;LX/0SG;LX/0Uo;Ljava/util/concurrent/ExecutorService;LX/03e;LX/0Xh;Ljava/util/concurrent/ScheduledExecutorService;LX/0Xl;Ljava/util/Set;Ljava/util/Set;LX/0Y0;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 14

    .prologue
    .line 79028
    const/16 v0, 0x103b

    invoke-static {p0, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0}, LX/0XY;->a(LX/0QB;)LX/0Xe;

    move-result-object v1

    check-cast v1, LX/0Xe;

    const/16 v2, 0x3034

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v5

    check-cast v5, LX/0Uo;

    invoke-static {p0}, LX/0Xf;->a(LX/0QB;)Ljava/util/concurrent/ExecutorService;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/03e;->a(LX/0QB;)LX/03e;

    move-result-object v7

    check-cast v7, LX/03e;

    invoke-static {p0}, LX/0Xh;->a(LX/0QB;)LX/0Xh;

    move-result-object v8

    check-cast v8, LX/0Xh;

    invoke-static {p0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v10

    check-cast v10, LX/0Xl;

    invoke-static {p0}, LX/0Xy;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v11

    invoke-static {p0}, LX/0Xz;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v12

    invoke-static {p0}, LX/0Y0;->a(LX/0QB;)LX/0Y0;

    move-result-object v13

    check-cast v13, LX/0Y0;

    invoke-static/range {v0 .. v13}, LX/0Xd;->a(LX/0Ot;LX/0Xe;LX/0Ot;LX/0So;LX/0SG;LX/0Uo;Ljava/util/concurrent/ExecutorService;LX/03e;LX/0Xh;Ljava/util/concurrent/ScheduledExecutorService;LX/0Xl;Ljava/util/Set;Ljava/util/Set;LX/0Y0;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v0

    return-object v0
.end method
