.class public LX/0u7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Landroid/content/IntentFilter;

.field private static volatile g:LX/0u7;


# instance fields
.field private final b:Ljava/lang/Object;

.field private final c:Landroid/content/Context;

.field private final d:LX/0pw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0pw",
            "<",
            "LX/2Zz;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field public f:Landroid/content/Intent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 155975
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/0u7;->a:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 156035
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156036
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/0u7;->b:Ljava/lang/Object;

    .line 156037
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0u7;->e:Z

    .line 156038
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LX/0u7;->c:Landroid/content/Context;

    .line 156039
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 156040
    const-string v1, "android.intent.action.BATTERY_LOW"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 156041
    const-string v1, "android.intent.action.BATTERY_OKAY"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 156042
    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 156043
    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 156044
    new-instance v1, LX/0pv;

    iget-object v2, p0, LX/0u7;->c:Landroid/content/Context;

    new-instance v3, LX/0u8;

    invoke-direct {v3, p0}, LX/0u8;-><init>(LX/0u7;)V

    invoke-direct {v1, v2, v3, v0}, LX/0pv;-><init>(Landroid/content/Context;LX/0py;Landroid/content/IntentFilter;)V

    iput-object v1, p0, LX/0u7;->d:LX/0pw;

    .line 156045
    return-void
.end method

.method public static a(LX/0QB;)LX/0u7;
    .locals 4

    .prologue
    .line 156049
    sget-object v0, LX/0u7;->g:LX/0u7;

    if-nez v0, :cond_1

    .line 156050
    const-class v1, LX/0u7;

    monitor-enter v1

    .line 156051
    :try_start_0
    sget-object v0, LX/0u7;->g:LX/0u7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 156052
    if-eqz v2, :cond_0

    .line 156053
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 156054
    new-instance p0, LX/0u7;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/0u7;-><init>(Landroid/content/Context;)V

    .line 156055
    move-object v0, p0

    .line 156056
    sput-object v0, LX/0u7;->g:LX/0u7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 156057
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 156058
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 156059
    :cond_1
    sget-object v0, LX/0u7;->g:LX/0u7;

    return-object v0

    .line 156060
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 156061
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/2Zz;Landroid/os/Handler;)V
    .locals 2
    .param p2    # Landroid/os/Handler;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 156046
    iget-object v1, p0, LX/0u7;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 156047
    :try_start_0
    iget-object v0, p0, LX/0u7;->d:LX/0pw;

    invoke-virtual {v0, p1, p2}, LX/0pw;->a(Ljava/lang/Object;Landroid/os/Handler;)V

    .line 156048
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static e(LX/0u7;)Landroid/content/Intent;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 156017
    iget-boolean v1, p0, LX/0u7;->e:Z

    if-eqz v1, :cond_1

    .line 156018
    iget-object v0, p0, LX/0u7;->f:Landroid/content/Intent;

    .line 156019
    :cond_0
    :goto_0
    return-object v0

    .line 156020
    :cond_1
    :try_start_0
    iget-object v1, p0, LX/0u7;->c:Landroid/content/Context;

    new-instance v2, LX/0xu;

    invoke-direct {v2, p0}, LX/0xu;-><init>(LX/0u7;)V

    sget-object v3, LX/0u7;->a:Landroid/content/IntentFilter;

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    iput-object v1, p0, LX/0u7;->f:Landroid/content/Intent;

    .line 156021
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/0u7;->e:Z

    .line 156022
    iget-object v0, p0, LX/0u7;->f:Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 156023
    :catch_0
    move-exception v1

    .line 156024
    invoke-virtual {v1}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 156025
    if-eqz v2, :cond_2

    const-string v3, "Unable to find app for caller"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 156026
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 156027
    :catch_1
    goto :goto_0
.end method


# virtual methods
.method public final a()F
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/high16 v0, -0x40800000    # -1.0f

    .line 156028
    invoke-static {p0}, LX/0u7;->e(LX/0u7;)Landroid/content/Intent;

    move-result-object v1

    .line 156029
    if-nez v1, :cond_1

    .line 156030
    :cond_0
    :goto_0
    return v0

    .line 156031
    :cond_1
    const-string v2, "level"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 156032
    const-string v3, "scale"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 156033
    if-ltz v2, :cond_0

    if-ltz v1, :cond_0

    .line 156034
    int-to-float v0, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method public final a(LX/2Zz;)V
    .locals 1

    .prologue
    .line 156015
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/0u7;->a(LX/2Zz;Landroid/os/Handler;)V

    .line 156016
    return-void
.end method

.method public final a(I)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, -0x1

    const/4 v0, 0x0

    .line 156005
    invoke-static {p0}, LX/0u7;->e(LX/0u7;)Landroid/content/Intent;

    move-result-object v2

    .line 156006
    if-nez v2, :cond_1

    .line 156007
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 156008
    :cond_1
    const-string v3, "status"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 156009
    packed-switch v3, :pswitch_data_0

    .line 156010
    :pswitch_1
    const-string v3, "level"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 156011
    const-string v4, "scale"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 156012
    if-ltz v3, :cond_0

    if-ltz v2, :cond_0

    .line 156013
    int-to-float v3, v3

    int-to-float v2, v2

    div-float v2, v3, v2

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    .line 156014
    int-to-float v3, p1

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b()LX/0y1;
    .locals 3

    .prologue
    .line 155989
    invoke-static {p0}, LX/0u7;->e(LX/0u7;)Landroid/content/Intent;

    move-result-object v0

    .line 155990
    if-nez v0, :cond_0

    .line 155991
    sget-object v0, LX/0y1;->UNKNOWN:LX/0y1;

    .line 155992
    :goto_0
    return-object v0

    .line 155993
    :cond_0
    const-string v1, "status"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 155994
    packed-switch v1, :pswitch_data_0

    .line 155995
    sget-object v0, LX/0y1;->UNKNOWN:LX/0y1;

    goto :goto_0

    .line 155996
    :pswitch_0
    sget-object v0, LX/0y1;->FULL:LX/0y1;

    goto :goto_0

    .line 155997
    :pswitch_1
    const-string v1, "plugged"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 155998
    packed-switch v0, :pswitch_data_1

    .line 155999
    :pswitch_2
    sget-object v0, LX/0y1;->UNKNOWN:LX/0y1;

    goto :goto_0

    .line 156000
    :pswitch_3
    sget-object v0, LX/0y1;->CHARGING_AC:LX/0y1;

    goto :goto_0

    .line 156001
    :pswitch_4
    sget-object v0, LX/0y1;->CHARGING_USB:LX/0y1;

    goto :goto_0

    .line 156002
    :pswitch_5
    sget-object v0, LX/0y1;->CHARGING_WIRELESS:LX/0y1;

    goto :goto_0

    .line 156003
    :pswitch_6
    sget-object v0, LX/0y1;->DISCHARGING:LX/0y1;

    goto :goto_0

    .line 156004
    :pswitch_7
    sget-object v0, LX/0y1;->NOT_CHARGING:LX/0y1;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_6
        :pswitch_7
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method public final b(LX/2Zz;)V
    .locals 2

    .prologue
    .line 155986
    iget-object v1, p0, LX/0u7;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 155987
    :try_start_0
    iget-object v0, p0, LX/0u7;->d:LX/0pw;

    invoke-virtual {v0, p1}, LX/0pw;->a(Ljava/lang/Object;)V

    .line 155988
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d()LX/454;
    .locals 3

    .prologue
    .line 155976
    invoke-static {p0}, LX/0u7;->e(LX/0u7;)Landroid/content/Intent;

    move-result-object v0

    .line 155977
    if-nez v0, :cond_0

    .line 155978
    sget-object v0, LX/454;->UNKNOWN:LX/454;

    .line 155979
    :goto_0
    return-object v0

    .line 155980
    :cond_0
    const-string v1, "plugged"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 155981
    packed-switch v0, :pswitch_data_0

    .line 155982
    :pswitch_0
    sget-object v0, LX/454;->NOT_PLUGGED:LX/454;

    goto :goto_0

    .line 155983
    :pswitch_1
    sget-object v0, LX/454;->PLUGGED_AC:LX/454;

    goto :goto_0

    .line 155984
    :pswitch_2
    sget-object v0, LX/454;->PLUGGED_USB:LX/454;

    goto :goto_0

    .line 155985
    :pswitch_3
    sget-object v0, LX/454;->PLUGGED_WIRELESS:LX/454;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
