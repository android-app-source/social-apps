.class public final enum LX/1hN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1hN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1hN;

.field public static final enum CELLULAR:LX/1hN;

.field public static final enum NOCONN:LX/1hN;

.field public static final enum OTHER:LX/1hN;

.field public static final enum WIFI:LX/1hN;


# instance fields
.field public value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 296032
    new-instance v0, LX/1hN;

    const-string v1, "NOCONN"

    invoke-direct {v0, v1, v2, v2}, LX/1hN;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1hN;->NOCONN:LX/1hN;

    new-instance v0, LX/1hN;

    const-string v1, "WIFI"

    invoke-direct {v0, v1, v3, v3}, LX/1hN;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1hN;->WIFI:LX/1hN;

    new-instance v0, LX/1hN;

    const-string v1, "CELLULAR"

    invoke-direct {v0, v1, v4, v4}, LX/1hN;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1hN;->CELLULAR:LX/1hN;

    new-instance v0, LX/1hN;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v5, v5}, LX/1hN;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1hN;->OTHER:LX/1hN;

    .line 296033
    const/4 v0, 0x4

    new-array v0, v0, [LX/1hN;

    sget-object v1, LX/1hN;->NOCONN:LX/1hN;

    aput-object v1, v0, v2

    sget-object v1, LX/1hN;->WIFI:LX/1hN;

    aput-object v1, v0, v3

    sget-object v1, LX/1hN;->CELLULAR:LX/1hN;

    aput-object v1, v0, v4

    sget-object v1, LX/1hN;->OTHER:LX/1hN;

    aput-object v1, v0, v5

    sput-object v0, LX/1hN;->$VALUES:[LX/1hN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 296036
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 296037
    iput p3, p0, LX/1hN;->value:I

    .line 296038
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1hN;
    .locals 1

    .prologue
    .line 296035
    const-class v0, LX/1hN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1hN;

    return-object v0
.end method

.method public static values()[LX/1hN;
    .locals 1

    .prologue
    .line 296034
    sget-object v0, LX/1hN;->$VALUES:[LX/1hN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1hN;

    return-object v0
.end method
