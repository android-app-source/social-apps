.class public LX/1gv;
.super LX/0jZ;
.source ""


# static fields
.field public static final b:Ljava/lang/String;

.field private static final c:[I


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1gx;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/0ad;

.field public final f:LX/1gi;

.field public final g:LX/0jY;

.field private h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0qz;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pm;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/187;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oy;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pV;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0qb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0qf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ys;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0r3;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0rW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private u:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 295252
    const-class v0, LX/1gv;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1gv;->b:Ljava/lang/String;

    .line 295253
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/1gv;->c:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x3
    .end array-data
.end method

.method public constructor <init>(Landroid/os/Looper;LX/1gi;LX/0jY;Ljava/util/concurrent/ExecutorService;LX/0ad;)V
    .locals 2
    .param p1    # Landroid/os/Looper;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1gi;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0jY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/FeedFetchExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 295101
    sget-object v0, LX/1gv;->c:[I

    invoke-direct {p0, p1, v0}, LX/0jZ;-><init>(Landroid/os/Looper;[I)V

    .line 295102
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 295103
    iput-object v0, p0, LX/1gv;->h:LX/0Ot;

    .line 295104
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 295105
    iput-object v0, p0, LX/1gv;->i:LX/0Ot;

    .line 295106
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 295107
    iput-object v0, p0, LX/1gv;->j:LX/0Ot;

    .line 295108
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 295109
    iput-object v0, p0, LX/1gv;->k:LX/0Ot;

    .line 295110
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 295111
    iput-object v0, p0, LX/1gv;->l:LX/0Ot;

    .line 295112
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 295113
    iput-object v0, p0, LX/1gv;->m:LX/0Ot;

    .line 295114
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 295115
    iput-object v0, p0, LX/1gv;->n:LX/0Ot;

    .line 295116
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 295117
    iput-object v0, p0, LX/1gv;->o:LX/0Ot;

    .line 295118
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 295119
    iput-object v0, p0, LX/1gv;->p:LX/0Ot;

    .line 295120
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 295121
    iput-object v0, p0, LX/1gv;->q:LX/0Ot;

    .line 295122
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 295123
    iput-object v0, p0, LX/1gv;->r:LX/0Ot;

    .line 295124
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 295125
    iput-object v0, p0, LX/1gv;->s:LX/0Ot;

    .line 295126
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 295127
    iput-object v0, p0, LX/1gv;->t:LX/0Ot;

    .line 295128
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 295129
    iput-object v0, p0, LX/1gv;->u:LX/0Ot;

    .line 295130
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/1gv;->a:Ljava/util/ArrayList;

    .line 295131
    iput-object p4, p0, LX/1gv;->d:Ljava/util/concurrent/ExecutorService;

    .line 295132
    iput-object p2, p0, LX/1gv;->f:LX/1gi;

    .line 295133
    iput-object p3, p0, LX/1gv;->g:LX/0jY;

    .line 295134
    iput-object p5, p0, LX/1gv;->e:LX/0ad;

    .line 295135
    return-void
.end method

.method public static a(LX/1gv;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1gv;",
            "LX/0Ot",
            "<",
            "LX/0qz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0pm;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/187;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0oy;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0pV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0pW;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0qb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0qf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0ys;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0r3;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0rW;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 295251
    iput-object p1, p0, LX/1gv;->h:LX/0Ot;

    iput-object p2, p0, LX/1gv;->i:LX/0Ot;

    iput-object p3, p0, LX/1gv;->j:LX/0Ot;

    iput-object p4, p0, LX/1gv;->k:LX/0Ot;

    iput-object p5, p0, LX/1gv;->l:LX/0Ot;

    iput-object p6, p0, LX/1gv;->m:LX/0Ot;

    iput-object p7, p0, LX/1gv;->n:LX/0Ot;

    iput-object p8, p0, LX/1gv;->o:LX/0Ot;

    iput-object p9, p0, LX/1gv;->p:LX/0Ot;

    iput-object p10, p0, LX/1gv;->q:LX/0Ot;

    iput-object p11, p0, LX/1gv;->r:LX/0Ot;

    iput-object p12, p0, LX/1gv;->s:LX/0Ot;

    iput-object p13, p0, LX/1gv;->t:LX/0Ot;

    iput-object p14, p0, LX/1gv;->u:LX/0Ot;

    return-void
.end method

.method private static a(LX/1gv;Ljava/lang/String;LX/0gf;)V
    .locals 3

    .prologue
    .line 295245
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295246
    iget-object v0, p0, LX/1gv;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const-string v1, "android_fresh_feed_empty_cursors"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 295247
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 295248
    const-string v1, "fetch_cause"

    invoke-virtual {p2}, LX/0gf;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 295249
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 295250
    :cond_0
    return-void
.end method

.method private a(LX/3rL;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3rL",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;",
            "Lcom/facebook/api/feedtype/FeedType;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 295198
    const-string v2, "FreshFeedNetworkHandler.doHandlePushedStories"

    const v3, 0x7eb8903e

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 295199
    :try_start_0
    iget-object v2, p1, LX/3rL;->a:Ljava/lang/Object;

    move-object v0, v2

    check-cast v0, LX/0Px;

    move-object v9, v0

    .line 295200
    iget-object v2, p1, LX/3rL;->b:Ljava/lang/Object;

    check-cast v2, Lcom/facebook/api/feedtype/FeedType;

    .line 295201
    new-instance v3, LX/0rT;

    invoke-direct {v3}, LX/0rT;-><init>()V

    .line 295202
    iput-object v2, v3, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 295203
    move-object v3, v3

    .line 295204
    const/4 v2, 0x0

    invoke-virtual {v9, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v2

    .line 295205
    iput-object v2, v3, LX/0rT;->g:Ljava/lang/String;

    .line 295206
    move-object v3, v3

    .line 295207
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v9, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v2

    .line 295208
    iput-object v2, v3, LX/0rT;->f:Ljava/lang/String;

    .line 295209
    move-object v2, v3

    .line 295210
    sget-object v3, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 295211
    iput-object v3, v2, LX/0rT;->a:LX/0rS;

    .line 295212
    move-object v2, v2

    .line 295213
    sget-object v3, Lcom/facebook/api/feed/FeedFetchContext;->a:Lcom/facebook/api/feed/FeedFetchContext;

    .line 295214
    iput-object v3, v2, LX/0rT;->l:Lcom/facebook/api/feed/FeedFetchContext;

    .line 295215
    move-object v2, v2

    .line 295216
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v3

    .line 295217
    iput v3, v2, LX/0rT;->c:I

    .line 295218
    move-object v2, v2

    .line 295219
    invoke-virtual {v2}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v3

    .line 295220
    new-instance v2, LX/0uq;

    invoke-direct {v2}, LX/0uq;-><init>()V

    .line 295221
    iput-object v9, v2, LX/0uq;->d:LX/0Px;

    .line 295222
    move-object v4, v2

    .line 295223
    new-instance v5, LX/17L;

    invoke-direct {v5}, LX/17L;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v9, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v2

    .line 295224
    iput-object v2, v5, LX/17L;->f:Ljava/lang/String;

    .line 295225
    move-object v5, v5

    .line 295226
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v9, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v2

    .line 295227
    iput-object v2, v5, LX/17L;->c:Ljava/lang/String;

    .line 295228
    move-object v2, v5

    .line 295229
    invoke-virtual {v2}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    .line 295230
    iput-object v2, v4, LX/0uq;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 295231
    move-object v2, v4

    .line 295232
    invoke-virtual {v2}, LX/0uq;->a()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v4

    .line 295233
    new-instance v2, Lcom/facebook/api/feed/FetchFeedResult;

    sget-object v5, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v6, p0, LX/1gv;->u:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/facebook/api/feed/FetchFeedResult;-><init>(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;JZ)V

    .line 295234
    iget-object v3, p0, LX/1gv;->j:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0pm;

    invoke-virtual {v3, v2}, LX/0pm;->b(Lcom/facebook/api/feed/FetchFeedResult;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v3

    .line 295235
    iget-object v2, p0, LX/1gv;->j:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0pm;

    invoke-static {v3}, LX/0pn;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 295236
    iget-object v2, p0, LX/1gv;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/187;

    invoke-virtual {v2, v3}, LX/187;->a(Lcom/facebook/api/feed/FetchFeedResult;)LX/0Px;

    .line 295237
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 295238
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v5

    move v3, v10

    :goto_0
    if-ge v3, v5, :cond_0

    invoke-virtual {v9, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 295239
    invoke-static {v2}, LX/0x0;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Lcom/facebook/feed/model/ClientFeedUnitEdge;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 295240
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 295241
    :cond_0
    iget-object v2, p0, LX/1gv;->g:LX/0jY;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    const/16 v4, 0xb

    invoke-virtual {v2, v3, v4}, LX/0jY;->a(LX/0Px;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295242
    const v2, 0x11f4e182

    invoke-static {v2}, LX/02m;->a(I)V

    .line 295243
    return-void

    .line 295244
    :catchall_0
    move-exception v2

    const v3, -0x2f93a758

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2
.end method

.method private b(LX/1gx;)V
    .locals 2

    .prologue
    .line 295183
    const-string v0, "FreshFeedNetworkHandler.doFreeNetworkCallback"

    const v1, -0x5a8fbcf2

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 295184
    :try_start_0
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 295185
    iput-object v0, p1, LX/1gx;->c:Lcom/facebook/api/feed/FetchFeedParams;

    .line 295186
    iput-object v0, p1, LX/1gx;->c:Lcom/facebook/api/feed/FetchFeedParams;

    .line 295187
    iput-boolean v1, p1, LX/1gx;->d:Z

    .line 295188
    iput-boolean v1, p1, LX/1gx;->e:Z

    .line 295189
    iput v1, p1, LX/1gx;->f:I

    .line 295190
    iput-object v0, p1, LX/1gx;->g:Ljava/lang/String;

    .line 295191
    iput-object v0, p1, LX/1gx;->h:Ljava/lang/String;

    .line 295192
    iput-object v0, p1, LX/1gx;->j:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 295193
    iput-object v0, p1, LX/1gx;->i:Ljava/lang/String;

    .line 295194
    iget-object v0, p0, LX/1gv;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295195
    const v0, 0x44f261d8

    invoke-static {v0}, LX/02m;->a(I)V

    .line 295196
    return-void

    .line 295197
    :catchall_0
    move-exception v0

    const v1, 0x53554b8f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private c(LX/1J0;)V
    .locals 7

    .prologue
    .line 295161
    const-string v0, "FreshFeedNetworkHandler.doFetchNewStoriesFromNetwork"

    const v1, -0x2a6bd1cc

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 295162
    :try_start_0
    iget-object v0, p0, LX/1gv;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ys;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ys;->a(Z)V

    .line 295163
    sget-object v0, LX/00a;->a:LX/00a;

    move-object v0, v0

    .line 295164
    invoke-virtual {v0}, LX/00a;->i()Ljava/lang/String;

    move-result-object v2

    .line 295165
    iget-object v0, p0, LX/1gv;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qb;

    iget-object v1, p0, LX/1gv;->p:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0qg;

    sget-object v3, LX/0rH;->NEWS_FEED:LX/0rH;

    invoke-virtual {v0, v1, v3}, LX/0qb;->a(LX/0qg;LX/0rH;)Z

    move-result v4

    .line 295166
    iget-object v0, p0, LX/1gv;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qz;

    iget-object v1, p1, LX/1J0;->b:Lcom/facebook/api/feedtype/FeedType;

    iget-object v3, p0, LX/1gv;->l:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0oy;

    invoke-virtual {v3}, LX/0oy;->n()I

    move-result v3

    sget-object v5, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    iget-object v6, p1, LX/1J0;->h:LX/0gf;

    invoke-virtual/range {v0 .. v6}, LX/0qz;->a(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;IZLX/0rS;LX/0gf;)Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v3

    .line 295167
    iget-object v0, p1, LX/1J0;->h:LX/0gf;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1J0;->h:LX/0gf;

    invoke-virtual {v0}, LX/0gf;->isManual()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295168
    iget-object v0, p0, LX/1gv;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0r3;

    invoke-virtual {v0}, LX/0r3;->a()V

    .line 295169
    :cond_0
    iget-object v0, p1, LX/1J0;->c:LX/0rB;

    invoke-virtual {v0}, LX/0rB;->b()LX/0rn;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;)V

    .line 295170
    iget-object v0, p0, LX/1gv;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 295171
    iget-object v0, p0, LX/1gv;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1gx;

    move-object v1, v0

    .line 295172
    :goto_0
    iget-object v0, p0, LX/1gv;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pV;

    sget-object v4, LX/1gv;->b:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Starting New Stories Fetch. Cursor: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 295173
    iget-object v0, p1, LX/1J0;->c:LX/0rB;

    invoke-virtual {v0}, LX/0rB;->b()LX/0rn;

    move-result-object v0

    iget-object v4, p1, LX/1J0;->d:Ljava/lang/String;

    invoke-virtual {v0, v3, v4, v1}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;LX/0tW;)LX/0v6;

    move-result-object v4

    .line 295174
    iget-boolean v0, v4, LX/0v6;->h:Z

    move v0, v0

    .line 295175
    invoke-virtual {v1, p1, v3, v0}, LX/1gx;->a(LX/1J0;Lcom/facebook/api/feed/FetchFeedParams;Z)V

    .line 295176
    iget-object v0, p1, LX/1J0;->h:LX/0gf;

    invoke-static {p0, v2, v0}, LX/1gv;->a(LX/1gv;Ljava/lang/String;LX/0gf;)V

    .line 295177
    iget-object v0, p0, LX/1gv;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    iget-object v1, p0, LX/1gv;->d:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v0, v4, v1}, LX/0tX;->b(LX/0v6;Ljava/util/concurrent/ExecutorService;)V

    .line 295178
    iget-object v0, p0, LX/1gv;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0r3;

    iget-object v1, p1, LX/1J0;->h:LX/0gf;

    invoke-virtual {v1}, LX/0gf;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0r3;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295179
    const v0, -0x56e161

    invoke-static {v0}, LX/02m;->a(I)V

    .line 295180
    return-void

    .line 295181
    :cond_1
    :try_start_1
    new-instance v0, LX/1gx;

    invoke-direct {v0, p0}, LX/1gx;-><init>(LX/1gv;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, v0

    goto :goto_0

    .line 295182
    :catchall_0
    move-exception v0

    const v1, 0x4ca03ec0    # 8.4014592E7f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private d(LX/1J0;)V
    .locals 7

    .prologue
    .line 295145
    const-string v0, "FreshFeedNetworkHandler.doFetchMoreStoriesFromNetwork"

    const v1, 0x24dc3512

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 295146
    :try_start_0
    iget-object v0, p0, LX/1gv;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qz;

    iget-object v1, p1, LX/1J0;->b:Lcom/facebook/api/feedtype/FeedType;

    iget-object v2, p1, LX/1J0;->f:Ljava/lang/String;

    iget-object v3, p1, LX/1J0;->g:Ljava/lang/String;

    iget-object v4, p0, LX/1gv;->l:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0oy;

    invoke-virtual {v4}, LX/0oy;->n()I

    move-result v4

    sget-object v5, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    iget-object v6, p1, LX/1J0;->h:LX/0gf;

    invoke-virtual/range {v0 .. v6}, LX/0qz;->a(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;Ljava/lang/String;ILX/0rS;LX/0gf;)Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v2

    .line 295147
    iget-object v0, p1, LX/1J0;->c:LX/0rB;

    invoke-virtual {v0}, LX/0rB;->b()LX/0rn;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;)V

    .line 295148
    iget-object v0, p0, LX/1gv;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 295149
    iget-object v0, p0, LX/1gv;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1gx;

    move-object v1, v0

    .line 295150
    :goto_0
    iget-object v0, p0, LX/1gv;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pV;

    sget-object v3, LX/1gv;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Starting More Stories Fetch. After Cursor: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, LX/1J0;->f:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Before Cursor: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, LX/1J0;->g:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 295151
    iget-object v0, p1, LX/1J0;->c:LX/0rB;

    invoke-virtual {v0}, LX/0rB;->b()LX/0rn;

    move-result-object v0

    iget-object v3, p1, LX/1J0;->d:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;LX/0tW;)LX/0v6;

    move-result-object v3

    .line 295152
    iget-boolean v0, v3, LX/0v6;->h:Z

    move v0, v0

    .line 295153
    invoke-virtual {v1, p1, v2, v0}, LX/1gx;->a(LX/1J0;Lcom/facebook/api/feed/FetchFeedParams;Z)V

    .line 295154
    iget-object v0, p1, LX/1J0;->f:Ljava/lang/String;

    iget-object v1, p1, LX/1J0;->h:LX/0gf;

    invoke-static {p0, v0, v1}, LX/1gv;->a(LX/1gv;Ljava/lang/String;LX/0gf;)V

    .line 295155
    iget-object v0, p0, LX/1gv;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    iget-object v1, p0, LX/1gv;->d:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v0, v3, v1}, LX/0tX;->b(LX/0v6;Ljava/util/concurrent/ExecutorService;)V

    .line 295156
    iget-object v0, p0, LX/1gv;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0r3;

    iget-object v1, p1, LX/1J0;->h:LX/0gf;

    invoke-virtual {v1}, LX/0gf;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0r3;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295157
    const v0, -0x40d585ea

    invoke-static {v0}, LX/02m;->a(I)V

    .line 295158
    return-void

    .line 295159
    :cond_0
    :try_start_1
    new-instance v0, LX/1gx;

    invoke-direct {v0, p0}, LX/1gx;-><init>(LX/1gv;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, v0

    goto :goto_0

    .line 295160
    :catchall_0
    move-exception v0

    const v1, -0x55f92b2

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final a(LX/1gx;)V
    .locals 1

    .prologue
    .line 295143
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, LX/1gv;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1gv;->sendMessage(Landroid/os/Message;)Z

    .line 295144
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 295136
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 295137
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown what="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 295138
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/1J0;

    invoke-direct {p0, v0}, LX/1gv;->c(LX/1J0;)V

    .line 295139
    :goto_0
    return-void

    .line 295140
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/1J0;

    invoke-direct {p0, v0}, LX/1gv;->d(LX/1J0;)V

    goto :goto_0

    .line 295141
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/1gx;

    invoke-direct {p0, v0}, LX/1gv;->b(LX/1gx;)V

    goto :goto_0

    .line 295142
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/3rL;

    invoke-direct {p0, v0}, LX/1gv;->a(LX/3rL;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
