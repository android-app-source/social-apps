.class public final enum LX/1Ed;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1Ed;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1Ed;

.field public static final enum CONNECTED:LX/1Ed;

.field public static final enum NO_INTERNET:LX/1Ed;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 220685
    new-instance v0, LX/1Ed;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v2}, LX/1Ed;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Ed;->CONNECTED:LX/1Ed;

    .line 220686
    new-instance v0, LX/1Ed;

    const-string v1, "NO_INTERNET"

    invoke-direct {v0, v1, v3}, LX/1Ed;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Ed;->NO_INTERNET:LX/1Ed;

    .line 220687
    const/4 v0, 0x2

    new-array v0, v0, [LX/1Ed;

    sget-object v1, LX/1Ed;->CONNECTED:LX/1Ed;

    aput-object v1, v0, v2

    sget-object v1, LX/1Ed;->NO_INTERNET:LX/1Ed;

    aput-object v1, v0, v3

    sput-object v0, LX/1Ed;->$VALUES:[LX/1Ed;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 220690
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1Ed;
    .locals 1

    .prologue
    .line 220689
    const-class v0, LX/1Ed;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1Ed;

    return-object v0
.end method

.method public static values()[LX/1Ed;
    .locals 1

    .prologue
    .line 220688
    sget-object v0, LX/1Ed;->$VALUES:[LX/1Ed;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1Ed;

    return-object v0
.end method
