.class public interface abstract LX/1Di;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract A(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
.end method

.method public abstract a(F)LX/1Di;
.end method

.method public abstract a(I)LX/1Di;
.end method

.method public abstract a(II)LX/1Di;
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
.end method

.method public abstract a(LX/1dQ;)LX/1Di;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;)",
            "LX/1Di;"
        }
    .end annotation
.end method

.method public abstract a(LX/1dc;)LX/1Di;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1Di;"
        }
    .end annotation
.end method

.method public abstract a(LX/1n6;)LX/1Di;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1n6",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1Di;"
        }
    .end annotation
.end method

.method public abstract a(Landroid/util/SparseArray;)LX/1Di;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "LX/1Di;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/CharSequence;)LX/1Di;
.end method

.method public abstract a(Z)LX/1Di;
.end method

.method public abstract b(F)LX/1Di;
.end method

.method public abstract b(I)LX/1Di;
.end method

.method public abstract b(II)LX/1Di;
    .param p2    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
.end method

.method public abstract b(LX/1dQ;)LX/1Di;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dQ",
            "<",
            "LX/48C;",
            ">;)",
            "LX/1Di;"
        }
    .end annotation
.end method

.method public abstract b(LX/1n6;)LX/1Di;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1n6",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1Di;"
        }
    .end annotation
.end method

.method public abstract b(Z)LX/1Di;
.end method

.method public abstract c(F)LX/1Di;
.end method

.method public abstract c(I)LX/1Di;
.end method

.method public abstract c(II)LX/1Di;
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
.end method

.method public abstract c(LX/1dQ;)LX/1Di;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dQ",
            "<",
            "LX/48J;",
            ">;)",
            "LX/1Di;"
        }
    .end annotation
.end method

.method public abstract d(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
.end method

.method public abstract d(II)LX/1Di;
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
.end method

.method public abstract e(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
.end method

.method public abstract e(II)LX/1Di;
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
.end method

.method public abstract f(I)LX/1Di;
.end method

.method public abstract f(II)LX/1Di;
    .param p2    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
.end method

.method public abstract g(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
.end method

.method public abstract g(II)LX/1Di;
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
.end method

.method public abstract h(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
.end method

.method public abstract h(II)LX/1Di;
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
.end method

.method public abstract i(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
.end method

.method public abstract i(II)LX/1Di;
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
.end method

.method public abstract j()LX/1Di;
.end method

.method public abstract j(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
.end method

.method public abstract j(II)LX/1Di;
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
.end method

.method public abstract k()LX/1Dg;
.end method

.method public abstract k(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
.end method

.method public abstract k(II)LX/1Di;
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
.end method

.method public abstract l(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
.end method

.method public abstract l(II)LX/1Di;
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
.end method

.method public abstract m(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
.end method

.method public abstract m(II)LX/1Di;
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
.end method

.method public abstract n(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
.end method

.method public abstract n(II)LX/1Di;
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
.end method

.method public abstract o(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
.end method

.method public abstract p(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
.end method

.method public abstract q(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
.end method

.method public abstract r(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
.end method

.method public abstract s(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
.end method

.method public abstract t(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
.end method

.method public abstract u(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
.end method

.method public abstract v(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
.end method

.method public abstract w(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
.end method

.method public abstract x(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
.end method

.method public abstract y(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
.end method

.method public abstract z(I)LX/1Di;
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
.end method
