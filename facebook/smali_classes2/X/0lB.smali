.class public LX/0lB;
.super LX/0lC;
.source ""


# static fields
.field private static f:LX/0lB;


# instance fields
.field private final mJsonLogger:LX/0ly;


# direct methods
.method public static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v4, 0x0

    .line 128400
    sput-object v4, LX/0lB;->f:LX/0lB;

    .line 128401
    new-instance v1, LX/0lo;

    invoke-direct {v1}, LX/0lo;-><init>()V

    .line 128402
    new-instance v0, LX/0lh;

    sget-object v2, LX/0lC;->b:LX/0lU;

    sget-object v3, LX/0lC;->c:LX/0lW;

    .line 128403
    sget-object v5, LX/0li;->a:LX/0li;

    move-object v5, v5

    .line 128404
    sget-object v7, LX/0ll;->f:LX/0ll;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v9

    const-string v6, "GMT"

    invoke-static {v6}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v10

    .line 128405
    sget-object v6, LX/0lm;->b:LX/0ln;

    move-object v11, v6

    .line 128406
    move-object v6, v4

    move-object v8, v4

    invoke-direct/range {v0 .. v11}, LX/0lh;-><init>(LX/0lM;LX/0lU;LX/0lW;LX/4pt;LX/0li;LX/4qy;Ljava/text/DateFormat;LX/4py;Ljava/util/Locale;Ljava/util/TimeZone;LX/0ln;)V

    .line 128407
    :try_start_0
    const-class v2, LX/0lC;

    const-string v3, "a"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 128408
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 128409
    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 128410
    const-class v1, LX/0lC;

    const-string v2, "e"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 128411
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 128412
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128413
    :goto_0
    return-void

    :catch_0
    goto :goto_0

    .line 128414
    :catch_1
    goto :goto_0
.end method

.method public constructor <init>(LX/0lp;)V
    .locals 1
    .param p1    # LX/0lp;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 128368
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/0lB;-><init>(LX/0lp;LX/0ly;)V

    .line 128369
    return-void
.end method

.method private constructor <init>(LX/0lp;LX/0ly;)V
    .locals 2
    .param p1    # LX/0lp;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0ly;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-<init>"
        }
    .end annotation

    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 128415
    invoke-direct {p0, p1}, LX/0lC;-><init>(LX/0lp;)V

    .line 128416
    iput-object p2, p0, LX/0lB;->mJsonLogger:LX/0ly;

    .line 128417
    new-instance v0, LX/0nb;

    invoke-direct {v0}, LX/0nb;-><init>()V

    invoke-virtual {p0, v0}, LX/0lC;->a(LX/0nd;)LX/0lC;

    .line 128418
    sget-object v0, LX/0np;->ALL:LX/0np;

    sget-object v1, LX/0lX;->NONE:LX/0lX;

    invoke-virtual {p0, v0, v1}, LX/0lC;->a(LX/0np;LX/0lX;)LX/0lC;

    .line 128419
    sget-object v0, LX/0mv;->FAIL_ON_UNKNOWN_PROPERTIES:LX/0mv;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0lC;->a(LX/0mv;Z)LX/0lC;

    .line 128420
    sget-object v0, LX/0nr;->NON_NULL:LX/0nr;

    invoke-virtual {p0, v0}, LX/0lC;->a(LX/0nr;)LX/0lC;

    .line 128421
    return-void
.end method

.method private static a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 128422
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 128423
    const-class v1, Ljava/util/List;

    if-eq v0, v1, :cond_0

    const-class v1, Ljava/util/ArrayList;

    if-ne v0, v1, :cond_1

    .line 128424
    :cond_0
    new-instance v0, Lcom/facebook/common/json/ArrayListDeserializer;

    invoke-direct {v0, p0}, Lcom/facebook/common/json/ArrayListDeserializer;-><init>(LX/0lJ;)V

    .line 128425
    :goto_0
    return-object v0

    .line 128426
    :cond_1
    const-class v1, LX/0Px;

    if-ne v0, v1, :cond_2

    .line 128427
    new-instance v0, Lcom/facebook/common/json/ImmutableListDeserializer;

    invoke-direct {v0, p0}, Lcom/facebook/common/json/ImmutableListDeserializer;-><init>(LX/0lJ;)V

    goto :goto_0

    .line 128428
    :cond_2
    const-class v1, Ljava/util/Map;

    if-eq v0, v1, :cond_3

    const-class v1, Ljava/util/LinkedHashMap;

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-virtual {p0, v2}, LX/0lJ;->a(I)LX/0lJ;

    move-result-object v1

    .line 128429
    iget-object v3, v1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v3

    .line 128430
    invoke-static {v1}, LX/0lB;->a(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 128431
    new-instance v0, Lcom/facebook/common/json/LinkedHashMapDeserializer;

    invoke-direct {v0, p0}, Lcom/facebook/common/json/LinkedHashMapDeserializer;-><init>(LX/0lJ;)V

    goto :goto_0

    .line 128432
    :cond_4
    const-class v1, LX/0P1;

    if-ne v0, v1, :cond_5

    invoke-virtual {p0, v2}, LX/0lJ;->a(I)LX/0lJ;

    move-result-object v0

    .line 128433
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 128434
    invoke-static {v0}, LX/0lB;->a(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 128435
    new-instance v0, Lcom/facebook/common/json/ImmutableMapDeserializer;

    invoke-direct {v0, p0}, Lcom/facebook/common/json/ImmutableMapDeserializer;-><init>(LX/0lJ;)V

    goto :goto_0

    .line 128436
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;)Z
    .locals 1

    .prologue
    .line 128437
    const-class v0, Ljava/lang/String;

    if-eq p0, v0, :cond_0

    const-class v0, Ljava/lang/Enum;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized i()LX/0lB;
    .locals 4

    .prologue
    .line 128395
    const-class v1, LX/0lB;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/0lB;->f:LX/0lB;

    if-nez v0, :cond_0

    .line 128396
    new-instance v0, LX/0lB;

    new-instance v2, LX/0lp;

    invoke-direct {v2}, LX/0lp;-><init>()V

    new-instance v3, LX/0ly;

    invoke-direct {v3}, LX/0ly;-><init>()V

    invoke-direct {v0, v2, v3}, LX/0lB;-><init>(LX/0lp;LX/0ly;)V

    sput-object v0, LX/0lB;->f:LX/0lB;

    .line 128397
    :cond_0
    sget-object v0, LX/0lB;->f:LX/0lB;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 128398
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/0m2;)LX/0mx;
    .locals 4

    .prologue
    .line 128399
    new-instance v0, LX/2Ah;

    iget-object v1, p0, LX/0lC;->_serializerProvider:LX/0mx;

    iget-object v2, p0, LX/0lC;->_serializerFactory:LX/0nS;

    iget-object v3, p0, LX/0lB;->mJsonLogger:LX/0ly;

    invoke-direct {v0, v1, p1, v2, v3}, LX/2Ah;-><init>(LX/0my;LX/0m2;LX/0nS;LX/0ly;)V

    return-object v0
.end method

.method public final a(LX/0n3;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128394
    invoke-virtual {p0, p1, p2}, LX/0lB;->b(LX/0n3;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0n3;Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0n3;",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 128387
    invoke-static {p2}, LX/11G;->a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 128388
    if-nez v0, :cond_0

    .line 128389
    iget-object v0, p0, LX/0lC;->_typeFactory:LX/0li;

    invoke-virtual {v0, p2}, LX/0li;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v0

    .line 128390
    invoke-super {p0, p1, v0}, LX/0lC;->a(LX/0n3;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 128391
    iget-object v1, p0, LX/0lB;->mJsonLogger:LX/0ly;

    if-eqz v1, :cond_0

    .line 128392
    invoke-virtual {p2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    .line 128393
    :cond_0
    return-object v0
.end method

.method public final a(LX/0n3;Ljava/lang/reflect/Type;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0n3;",
            "Ljava/lang/reflect/Type;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 128384
    instance-of v0, p2, Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 128385
    check-cast p2, Ljava/lang/Class;

    invoke-virtual {p0, p1, p2}, LX/0lB;->a(LX/0n3;Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 128386
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0lC;->_typeFactory:LX/0li;

    invoke-virtual {v0, p2}, LX/0li;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/0lB;->b(LX/0n3;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0mu;LX/15w;LX/0lJ;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 128381
    invoke-virtual {p2}, LX/15w;->a()LX/0lD;

    move-result-object v0

    if-nez v0, :cond_0

    .line 128382
    invoke-virtual {p2, p0}, LX/15w;->a(LX/0lD;)V

    .line 128383
    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/0lC;->a(LX/0mu;LX/15w;LX/0lJ;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/0n3;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0n3;",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 128370
    const/4 v0, 0x0

    .line 128371
    invoke-virtual {p2}, LX/0lJ;->p()Z

    move-result v1

    if-nez v1, :cond_0

    .line 128372
    iget-object v0, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 128373
    invoke-static {v0}, LX/11G;->a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 128374
    :cond_0
    if-nez v0, :cond_1

    .line 128375
    invoke-static {p2}, LX/0lB;->a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 128376
    :cond_1
    if-nez v0, :cond_2

    .line 128377
    invoke-super {p0, p1, p2}, LX/0lC;->a(LX/0n3;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 128378
    iget-object v1, p0, LX/0lB;->mJsonLogger:LX/0ly;

    if-eqz v1, :cond_2

    .line 128379
    invoke-virtual {p2}, LX/0lJ;->toString()Ljava/lang/String;

    .line 128380
    :cond_2
    return-object v0
.end method

.method public final b(LX/15w;LX/0lJ;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 128365
    invoke-virtual {p1}, LX/15w;->a()LX/0lD;

    move-result-object v0

    if-nez v0, :cond_0

    .line 128366
    invoke-virtual {p1, p0}, LX/15w;->a(LX/0lD;)V

    .line 128367
    :cond_0
    invoke-super {p0, p1, p2}, LX/0lC;->b(LX/15w;LX/0lJ;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
