.class public final LX/1H9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1H8;

.field public b:I

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/4ec;

.field public h:Z

.field public i:Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;

.field public j:Z

.field public k:Z


# direct methods
.method public constructor <init>(LX/1H8;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 226512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226513
    iput v1, p0, LX/1H9;->b:I

    .line 226514
    iput-boolean v1, p0, LX/1H9;->c:Z

    .line 226515
    iput-boolean v1, p0, LX/1H9;->d:Z

    .line 226516
    iput-boolean v1, p0, LX/1H9;->e:Z

    .line 226517
    const/4 v0, 0x0

    iput-object v0, p0, LX/1H9;->f:LX/1Gd;

    .line 226518
    iput-boolean v1, p0, LX/1H9;->h:Z

    .line 226519
    iput-boolean v1, p0, LX/1H9;->j:Z

    .line 226520
    iput-boolean v1, p0, LX/1H9;->k:Z

    .line 226521
    iput-object p1, p0, LX/1H9;->a:LX/1H8;

    .line 226522
    return-void
.end method


# virtual methods
.method public final a(I)LX/1H8;
    .locals 1

    .prologue
    .line 226510
    iput p1, p0, LX/1H9;->b:I

    .line 226511
    iget-object v0, p0, LX/1H9;->a:LX/1H8;

    return-object v0
.end method

.method public final a(LX/1Gd;)LX/1H8;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Gd",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "LX/1H8;"
        }
    .end annotation

    .prologue
    .line 226508
    iput-object p1, p0, LX/1H9;->f:LX/1Gd;

    .line 226509
    iget-object v0, p0, LX/1H9;->a:LX/1H8;

    return-object v0
.end method

.method public final a(LX/4ec;)LX/1H8;
    .locals 1

    .prologue
    .line 226506
    iput-object p1, p0, LX/1H9;->g:LX/4ec;

    .line 226507
    iget-object v0, p0, LX/1H9;->a:LX/1H8;

    return-object v0
.end method

.method public final a(Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;)LX/1H8;
    .locals 1

    .prologue
    .line 226504
    iput-object p1, p0, LX/1H9;->i:Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;

    .line 226505
    iget-object v0, p0, LX/1H9;->a:LX/1H8;

    return-object v0
.end method

.method public final a(Z)LX/1H8;
    .locals 1

    .prologue
    .line 226492
    iput-boolean p1, p0, LX/1H9;->d:Z

    .line 226493
    iget-object v0, p0, LX/1H9;->a:LX/1H8;

    return-object v0
.end method

.method public final b(Z)LX/1H8;
    .locals 1

    .prologue
    .line 226502
    iput-boolean p1, p0, LX/1H9;->e:Z

    .line 226503
    iget-object v0, p0, LX/1H9;->a:LX/1H8;

    return-object v0
.end method

.method public final c(Z)LX/1H8;
    .locals 1

    .prologue
    .line 226500
    iput-boolean p1, p0, LX/1H9;->c:Z

    .line 226501
    iget-object v0, p0, LX/1H9;->a:LX/1H8;

    return-object v0
.end method

.method public final d(Z)LX/1H8;
    .locals 1

    .prologue
    .line 226498
    iput-boolean p1, p0, LX/1H9;->k:Z

    .line 226499
    iget-object v0, p0, LX/1H9;->a:LX/1H8;

    return-object v0
.end method

.method public final e(Z)LX/1H8;
    .locals 1

    .prologue
    .line 226496
    iput-boolean p1, p0, LX/1H9;->h:Z

    .line 226497
    iget-object v0, p0, LX/1H9;->a:LX/1H8;

    return-object v0
.end method

.method public final f(Z)LX/1H8;
    .locals 1

    .prologue
    .line 226494
    iput-boolean p1, p0, LX/1H9;->j:Z

    .line 226495
    iget-object v0, p0, LX/1H9;->a:LX/1H8;

    return-object v0
.end method
