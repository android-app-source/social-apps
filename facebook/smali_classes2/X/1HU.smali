.class public LX/1HU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Fh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1Fh",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final a:LX/1Fh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fh",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final b:LX/1HT;


# direct methods
.method public constructor <init>(LX/1Fh;LX/1HT;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Fh",
            "<TK;TV;>;",
            "LX/1HT;",
            ")V"
        }
    .end annotation

    .prologue
    .line 227090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227091
    iput-object p1, p0, LX/1HU;->a:LX/1Fh;

    .line 227092
    iput-object p2, p0, LX/1HU;->b:LX/1HT;

    .line 227093
    return-void
.end method


# virtual methods
.method public final a(Lcom/android/internal/util/Predicate;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/util/Predicate",
            "<TK;>;)I"
        }
    .end annotation

    .prologue
    .line 227094
    iget-object v0, p0, LX/1HU;->a:LX/1Fh;

    invoke-interface {v0, p1}, LX/1Fh;->a(Lcom/android/internal/util/Predicate;)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)LX/1FJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LX/1FJ",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 227095
    iget-object v0, p0, LX/1HU;->a:LX/1Fh;

    invoke-interface {v0, p1}, LX/1Fh;->a(Ljava/lang/Object;)LX/1FJ;

    move-result-object v0

    .line 227096
    if-nez v0, :cond_0

    .line 227097
    iget-object v1, p0, LX/1HU;->b:LX/1HT;

    invoke-interface {v1}, LX/1HT;->a()V

    .line 227098
    :goto_0
    return-object v0

    .line 227099
    :cond_0
    iget-object v1, p0, LX/1HU;->b:LX/1HT;

    invoke-interface {v1, p1}, LX/1HT;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1FJ;)LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "LX/1FJ",
            "<TV;>;)",
            "LX/1FJ",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 227100
    iget-object v0, p0, LX/1HU;->b:LX/1HT;

    invoke-interface {v0}, LX/1HT;->b()V

    .line 227101
    iget-object v0, p0, LX/1HU;->a:LX/1Fh;

    invoke-interface {v0, p1, p2}, LX/1Fh;->a(Ljava/lang/Object;LX/1FJ;)LX/1FJ;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/android/internal/util/Predicate;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/util/Predicate",
            "<TK;>;)Z"
        }
    .end annotation

    .prologue
    .line 227102
    iget-object v0, p0, LX/1HU;->a:LX/1Fh;

    invoke-interface {v0, p1}, LX/1Fh;->b(Lcom/android/internal/util/Predicate;)Z

    move-result v0

    return v0
.end method
