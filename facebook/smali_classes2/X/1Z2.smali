.class public LX/1Z2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1RW;

.field public final b:LX/1RX;

.field public final c:LX/1Ns;

.field public final d:Landroid/support/v4/app/FragmentActivity;

.field public final e:LX/1Np;

.field public final f:LX/1EE;

.field private final g:LX/1Nq;

.field public final h:LX/0W9;

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1E8;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0ad;

.field public final k:Landroid/content/res/Resources;

.field public final l:Lcom/facebook/content/SecureContextHelper;

.field public final m:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(LX/1RW;LX/1RX;Landroid/content/Context;LX/1Ns;Landroid/support/v4/app/FragmentActivity;LX/1Np;LX/1EE;LX/1Nq;LX/0W9;LX/0Or;LX/0ad;Ljava/lang/Boolean;Lcom/facebook/content/SecureContextHelper;)V
    .locals 1
    .param p12    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1RW;",
            "LX/1RX;",
            "Landroid/content/Context;",
            "LX/1Ns;",
            "Landroid/support/v4/app/FragmentActivity;",
            "LX/1Np;",
            "LX/1EE;",
            "LX/1Nq;",
            "LX/0W9;",
            "LX/0Or",
            "<",
            "LX/1E8;",
            ">;",
            "LX/0ad;",
            "Ljava/lang/Boolean;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 274247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 274248
    iput-object p5, p0, LX/1Z2;->d:Landroid/support/v4/app/FragmentActivity;

    .line 274249
    iput-object p7, p0, LX/1Z2;->f:LX/1EE;

    .line 274250
    iput-object p1, p0, LX/1Z2;->a:LX/1RW;

    .line 274251
    iput-object p2, p0, LX/1Z2;->b:LX/1RX;

    .line 274252
    iput-object p4, p0, LX/1Z2;->c:LX/1Ns;

    .line 274253
    iput-object p6, p0, LX/1Z2;->e:LX/1Np;

    .line 274254
    iput-object p10, p0, LX/1Z2;->i:LX/0Or;

    .line 274255
    iput-object p8, p0, LX/1Z2;->g:LX/1Nq;

    .line 274256
    iput-object p9, p0, LX/1Z2;->h:LX/0W9;

    .line 274257
    iput-object p11, p0, LX/1Z2;->j:LX/0ad;

    .line 274258
    iput-object p12, p0, LX/1Z2;->m:Ljava/lang/Boolean;

    .line 274259
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/1Z2;->k:Landroid/content/res/Resources;

    .line 274260
    iput-object p13, p0, LX/1Z2;->l:Lcom/facebook/content/SecureContextHelper;

    .line 274261
    return-void
.end method

.method public static b(LX/0QB;)LX/1Z2;
    .locals 14

    .prologue
    .line 274244
    new-instance v0, LX/1Z2;

    invoke-static {p0}, LX/1RW;->b(LX/0QB;)LX/1RW;

    move-result-object v1

    check-cast v1, LX/1RW;

    invoke-static {p0}, LX/1RX;->a(LX/0QB;)LX/1RX;

    move-result-object v2

    check-cast v2, LX/1RX;

    const-class v3, Landroid/content/Context;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const-class v4, LX/1Ns;

    invoke-interface {p0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1Ns;

    invoke-static {p0}, LX/1No;->b(LX/0QB;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    check-cast v5, Landroid/support/v4/app/FragmentActivity;

    invoke-static {p0}, LX/1Np;->a(LX/0QB;)LX/1Np;

    move-result-object v6

    check-cast v6, LX/1Np;

    invoke-static {p0}, LX/1E4;->b(LX/0QB;)LX/1EE;

    move-result-object v7

    check-cast v7, LX/1EE;

    invoke-static {p0}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v8

    check-cast v8, LX/1Nq;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v9

    check-cast v9, LX/0W9;

    const/16 v10, 0x64b

    invoke-static {p0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v12

    check-cast v12, Ljava/lang/Boolean;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v13

    check-cast v13, Lcom/facebook/content/SecureContextHelper;

    invoke-direct/range {v0 .. v13}, LX/1Z2;-><init>(LX/1RW;LX/1RX;Landroid/content/Context;LX/1Ns;Landroid/support/v4/app/FragmentActivity;LX/1Np;LX/1EE;LX/1Nq;LX/0W9;LX/0Or;LX/0ad;Ljava/lang/Boolean;Lcom/facebook/content/SecureContextHelper;)V

    .line 274245
    return-object v0
.end method

.method public static g(LX/1Z2;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 274246
    iget-object v0, p0, LX/1Z2;->j:LX/0ad;

    sget-short v1, LX/1EB;->y:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Z2;->g:LX/1Nq;

    invoke-static {}, Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfig;->c()Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
