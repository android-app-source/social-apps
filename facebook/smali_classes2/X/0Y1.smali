.class public LX/0Y1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/quicklog/QuickPerformanceLogger;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:LX/0So;

.field private c:LX/0SG;

.field private d:LX/0YV;

.field private e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/quicklog/HoneyClientLogger;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0Xe;

.field private g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/quicklog/StatsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/facebook/quicklog/PerformanceLoggingEvent;

.field private final i:LX/0YT;

.field private final j:Landroid/util/SparseIntArray;

.field private final k:LX/0Xh;

.field private l:LX/0Y2;

.field private m:LX/0Y3;

.field private n:LX/03R;

.field private final o:[LX/0Y8;

.field private final p:Ljava/util/Random;

.field private final q:LX/03f;

.field private final r:LX/0Y0;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80199
    const-class v0, LX/0Y1;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0Y1;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Xe;LX/0Or;LX/0So;LX/0SG;LX/0Xh;LX/0Y2;LX/0Y3;[LX/0Y4;[LX/0Y8;LX/03f;LX/0Y0;)V
    .locals 2
    .param p9    # [LX/0Y4;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # [LX/0Y8;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # LX/03f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # LX/0Y0;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/quicklog/HoneyClientLogger;",
            ">;",
            "Lcom/facebook/quicklog/HoneySamplingPolicy;",
            "LX/0Or",
            "<",
            "Lcom/facebook/quicklog/StatsLogger;",
            ">;",
            "LX/0So;",
            "LX/0SG;",
            "Lcom/facebook/quicklog/DebugAndTestConfig;",
            "Lcom/facebook/quicklog/AppStates;",
            "Lcom/facebook/quicklog/BackgroundExecution;",
            "[",
            "LX/0Y4;",
            "[",
            "LX/0Y8;",
            "Lcom/facebook/quicklog/GCInfo;",
            "Lcom/facebook/quicklog/NetworkConditionProvider;",
            ")V"
        }
    .end annotation

    .prologue
    .line 80181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80182
    new-instance v0, LX/0YT;

    invoke-direct {v0}, LX/0YT;-><init>()V

    iput-object v0, p0, LX/0Y1;->i:LX/0YT;

    .line 80183
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, LX/0Y1;->j:Landroid/util/SparseIntArray;

    .line 80184
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/0Y1;->n:LX/03R;

    .line 80185
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/0Y1;->p:Ljava/util/Random;

    .line 80186
    iput-object p1, p0, LX/0Y1;->e:LX/0Or;

    .line 80187
    iput-object p2, p0, LX/0Y1;->f:LX/0Xe;

    .line 80188
    iput-object p3, p0, LX/0Y1;->g:LX/0Or;

    .line 80189
    iput-object p4, p0, LX/0Y1;->b:LX/0So;

    .line 80190
    iput-object p5, p0, LX/0Y1;->c:LX/0SG;

    .line 80191
    iput-object p6, p0, LX/0Y1;->k:LX/0Xh;

    .line 80192
    iput-object p7, p0, LX/0Y1;->l:LX/0Y2;

    .line 80193
    iput-object p8, p0, LX/0Y1;->m:LX/0Y3;

    .line 80194
    iput-object p10, p0, LX/0Y1;->o:[LX/0Y8;

    .line 80195
    iput-object p11, p0, LX/0Y1;->q:LX/03f;

    .line 80196
    iput-object p12, p0, LX/0Y1;->r:LX/0Y0;

    .line 80197
    new-instance v0, LX/0YV;

    iget-object v1, p0, LX/0Y1;->g:LX/0Or;

    invoke-direct {v0, v1, p9}, LX/0YV;-><init>(LX/0Or;[LX/0Y4;)V

    iput-object v0, p0, LX/0Y1;->d:LX/0YV;

    .line 80198
    return-void
.end method

.method private a(ILX/0ar;Z)I
    .locals 2

    .prologue
    .line 80176
    if-eqz p3, :cond_1

    .line 80177
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p0, p1, v0}, LX/0Y1;->a(LX/0Y1;IZ)I

    move-result v0

    .line 80178
    :goto_1
    return v0

    .line 80179
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 80180
    :cond_1
    iget-object v0, p0, LX/0Y1;->f:LX/0Xe;

    invoke-virtual {p2, p1}, LX/0ar;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/0Xe;->a(I)I

    move-result v0

    goto :goto_1
.end method

.method private static a(LX/0Y1;IZ)I
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/high16 v4, -0x80000000

    .line 80166
    iget-object v1, p0, LX/0Y1;->k:LX/0Xh;

    invoke-virtual {v1}, LX/0Xh;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/0Y1;->k:LX/0Xh;

    invoke-virtual {v1}, LX/0Xh;->d()Z

    move-result v1

    if-nez v1, :cond_1

    .line 80167
    :cond_0
    :goto_0
    return v0

    .line 80168
    :cond_1
    iget-object v1, p0, LX/0Y1;->j:Landroid/util/SparseIntArray;

    monitor-enter v1

    .line 80169
    :try_start_0
    iget-object v2, p0, LX/0Y1;->j:Landroid/util/SparseIntArray;

    const/high16 v3, -0x80000000

    invoke-virtual {v2, p1, v3}, Landroid/util/SparseIntArray;->get(II)I

    move-result v2

    .line 80170
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80171
    if-eq v2, v4, :cond_2

    .line 80172
    iget-object v0, p0, LX/0Y1;->f:LX/0Xe;

    invoke-virtual {v0, v2}, LX/0Xe;->a(I)I

    move-result v0

    goto :goto_0

    .line 80173
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 80174
    :cond_2
    if-eqz p2, :cond_0

    .line 80175
    iget-object v0, p0, LX/0Y1;->f:LX/0Xe;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, LX/0Xe;->a(I)I

    move-result v0

    goto :goto_0
.end method

.method private static a(LX/0Y1;J)J
    .locals 3

    .prologue
    .line 80163
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 80164
    iget-object v0, p0, LX/0Y1;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide p1

    .line 80165
    :cond_0
    return-wide p1
.end method

.method private static a(LX/0Y1;Z)J
    .locals 6

    .prologue
    .line 79981
    iget-object v0, p0, LX/0Y1;->q:LX/03f;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 79982
    iget-object v0, p0, LX/0Y1;->q:LX/03f;

    .line 79983
    iget-object v2, v0, LX/03f;->a:LX/03e;

    .line 79984
    iget-boolean v4, v2, LX/03e;->c:Z

    if-nez v4, :cond_0

    .line 79985
    invoke-static {v2}, LX/03e;->d(LX/03e;)V

    .line 79986
    :cond_0
    iget-boolean v4, v2, LX/03e;->b:Z

    move v4, v4

    .line 79987
    if-eqz v4, :cond_2

    .line 79988
    invoke-static {}, Lcom/facebook/common/dextricks/DalvikInternals;->getElapsedGcMillis()J

    move-result-wide v4

    .line 79989
    :goto_0
    move-wide v2, v4

    .line 79990
    move-wide v0, v2

    .line 79991
    :goto_1
    return-wide v0

    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_1

    :cond_2
    const-wide/16 v4, -0x1

    goto :goto_0
.end method

.method private static a(LX/00q;)LX/00q;
    .locals 1

    .prologue
    .line 80157
    if-nez p0, :cond_1

    .line 80158
    const/4 p0, 0x0

    .line 80159
    :cond_0
    :goto_0
    return-object p0

    .line 80160
    :cond_1
    iget-boolean v0, p0, LX/00q;->b:Z

    move v0, v0

    .line 80161
    if-nez v0, :cond_0

    .line 80162
    invoke-virtual {p0}, LX/00q;->n()V

    goto :goto_0
.end method

.method private a(IIIJZZZ)LX/0Pn;
    .locals 18

    .prologue
    .line 80145
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0Y1;->d:LX/0YV;

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v2, v0, v1}, LX/0YV;->f(II)J

    move-result-wide v2

    .line 80146
    const-wide/16 v4, -0x1

    and-long/2addr v4, v2

    long-to-int v5, v4

    .line 80147
    const v4, 0x7fffffff

    if-eq v5, v4, :cond_3

    .line 80148
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Y1;->f:LX/0Xe;

    invoke-virtual {v4}, LX/0Xe;->a()LX/0ar;

    move-result-object v4

    .line 80149
    if-nez v4, :cond_1

    const/4 v7, 0x1

    .line 80150
    :goto_0
    if-nez v7, :cond_0

    invoke-virtual {v4}, LX/0ar;->a()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual/range {p0 .. p0}, LX/0Y1;->b()Z

    move-result v4

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0Y1;->k:LX/0Xh;

    invoke-virtual {v4}, LX/0Xh;->b()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    const/4 v6, 0x1

    .line 80151
    :goto_1
    const/16 v4, 0x20

    shr-long/2addr v2, v4

    long-to-int v4, v2

    .line 80152
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0Y1;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v12

    move-object/from16 v0, p0

    move/from16 v1, p6

    invoke-static {v0, v1}, LX/0Y1;->a(LX/0Y1;Z)J

    move-result-wide v14

    move/from16 v2, p1

    move/from16 v3, p3

    move/from16 v8, p8

    move-wide/from16 v9, p4

    move/from16 v11, p6

    move/from16 v16, p7

    invoke-static/range {v2 .. v16}, LX/0Pn;->a(IIIIZZZJZJJZ)LX/0Pn;

    move-result-object v2

    .line 80153
    :goto_2
    return-object v2

    .line 80154
    :cond_1
    const/4 v7, 0x0

    goto :goto_0

    .line 80155
    :cond_2
    const/4 v6, 0x0

    goto :goto_1

    .line 80156
    :cond_3
    const/4 v2, 0x0

    goto :goto_2
.end method

.method private a(IIJZZZ)LX/0Pn;
    .locals 17

    .prologue
    .line 80134
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0Y1;->f:LX/0Xe;

    invoke-virtual {v2}, LX/0Xe;->a()LX/0ar;

    move-result-object v2

    .line 80135
    if-nez v2, :cond_1

    const/4 v7, 0x1

    .line 80136
    :goto_0
    if-nez v7, :cond_0

    invoke-virtual {v2}, LX/0ar;->a()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual/range {p0 .. p0}, LX/0Y1;->b()Z

    move-result v3

    if-nez v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0Y1;->k:LX/0Xh;

    invoke-virtual {v3}, LX/0Xh;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    const/4 v6, 0x1

    .line 80137
    :goto_1
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v1, v2, v6}, LX/0Y1;->a(ILX/0ar;Z)I

    move-result v5

    .line 80138
    const v2, 0x7fffffff

    if-eq v5, v2, :cond_3

    .line 80139
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0Y1;->p:Ljava/util/Random;

    const v3, 0x7fffffff

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    .line 80140
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0Y1;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v12

    move-object/from16 v0, p0

    move/from16 v1, p5

    invoke-static {v0, v1}, LX/0Y1;->a(LX/0Y1;Z)J

    move-result-wide v14

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v8, p7

    move-wide/from16 v9, p3

    move/from16 v11, p5

    move/from16 v16, p6

    invoke-static/range {v2 .. v16}, LX/0Pn;->a(IIIIZZZJZJJZ)LX/0Pn;

    move-result-object v2

    .line 80141
    :goto_2
    return-object v2

    .line 80142
    :cond_1
    const/4 v7, 0x0

    goto :goto_0

    .line 80143
    :cond_2
    const/4 v6, 0x0

    goto :goto_1

    .line 80144
    :cond_3
    const/4 v2, 0x0

    goto :goto_2
.end method

.method private a(III)V
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 80034
    const-wide/16 v4, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v8, v7

    move-object v9, v6

    move-object v10, v6

    invoke-static/range {v0 .. v10}, LX/0Y1;->a(LX/0Y1;IIIJLjava/lang/String;ZZLX/00q;LX/03R;)V

    .line 80035
    return-void
.end method

.method private a(IIJLjava/lang/String;ZZLX/00q;LX/03R;)V
    .locals 23
    .param p8    # LX/00q;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # LX/03R;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 80127
    invoke-static/range {p3 .. p4}, LX/0Y1;->c(J)Z

    move-result v10

    .line 80128
    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    invoke-static {v0, v1, v2}, LX/0Y1;->a(LX/0Y1;J)J

    move-result-wide v8

    move-object/from16 v5, p0

    move/from16 v6, p1

    move/from16 v7, p2

    move/from16 v11, p7

    move-object/from16 v12, p8

    move-object/from16 v13, p9

    .line 80129
    invoke-direct/range {v5 .. v13}, LX/0Y1;->a(IIJZZLX/00q;LX/03R;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 80130
    :goto_0
    return-void

    :cond_0
    move-object/from16 v5, p0

    move/from16 v6, p1

    move/from16 v7, p2

    move/from16 v11, p6

    move/from16 v12, p7

    .line 80131
    invoke-direct/range {v5 .. v12}, LX/0Y1;->a(IIJZZZ)LX/0Pn;

    move-result-object v12

    move-object/from16 v11, p0

    move/from16 v13, p1

    move/from16 v14, p2

    move-object/from16 v15, p5

    move-object/from16 v16, p8

    move-object/from16 v17, p9

    move-wide/from16 v18, v8

    move/from16 v20, v10

    move/from16 v21, p7

    .line 80132
    invoke-direct/range {v11 .. v21}, LX/0Y1;->a(LX/0Pn;IILjava/lang/String;LX/00q;LX/03R;JZZ)V

    goto :goto_0
.end method

.method private a(IILjava/lang/String;JZ)V
    .locals 8

    .prologue
    .line 80120
    invoke-static {p4, p5}, LX/0Y1;->c(J)Z

    move-result v6

    .line 80121
    invoke-static {p0, p4, p5}, LX/0Y1;->a(LX/0Y1;J)J

    move-result-wide v4

    .line 80122
    invoke-virtual {p0}, LX/0Y1;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80123
    :goto_0
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v7, p6

    invoke-virtual/range {v0 .. v7}, LX/0YV;->a(IILjava/lang/String;JZZ)V

    .line 80124
    const-wide/16 v0, 0x4

    invoke-static {v0, v1}, LX/00k;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80125
    const-wide/16 v0, 0x4

    invoke-static {p1}, LX/0Y1;->g(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, p2}, LX/0YV;->i(II)I

    move-result v3

    invoke-static {v4, v5}, LX/0Y1;->b(J)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, LX/018;->a(JLjava/lang/String;IJ)V

    .line 80126
    :cond_0
    return-void

    :cond_1
    goto :goto_0
.end method

.method private a(IISJ)V
    .locals 14

    .prologue
    .line 80113
    invoke-static/range {p4 .. p5}, LX/0Y1;->c(J)Z

    move-result v8

    .line 80114
    move-wide/from16 v0, p4

    invoke-static {p0, v0, v1}, LX/0Y1;->a(LX/0Y1;J)J

    move-result-wide v6

    .line 80115
    iget-object v2, p0, LX/0Y1;->d:LX/0YV;

    invoke-static {p0, v8}, LX/0Y1;->a(LX/0Y1;Z)J

    move-result-wide v9

    const/4 v11, 0x0

    const/4 v12, 0x0

    move v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    invoke-virtual/range {v2 .. v12}, LX/0YV;->a(IISJZJLjava/lang/String;Ljava/lang/String;)Lcom/facebook/quicklog/PerformanceLoggingEvent;

    move-result-object v2

    .line 80116
    if-eqz v2, :cond_0

    .line 80117
    const-string v3, "markerNote"

    invoke-direct {p0, v3, p1}, LX/0Y1;->a(Ljava/lang/String;I)V

    .line 80118
    invoke-direct {p0, v2}, LX/0Y1;->a(Lcom/facebook/quicklog/PerformanceLoggingEvent;)V

    .line 80119
    :cond_0
    return-void
.end method

.method private a(IISLjava/lang/String;Ljava/lang/String;J)V
    .locals 14

    .prologue
    .line 80106
    invoke-static/range {p6 .. p7}, LX/0Y1;->c(J)Z

    move-result v8

    .line 80107
    move-wide/from16 v0, p6

    invoke-static {p0, v0, v1}, LX/0Y1;->a(LX/0Y1;J)J

    move-result-wide v6

    .line 80108
    iget-object v2, p0, LX/0Y1;->d:LX/0YV;

    invoke-static {p0, v8}, LX/0Y1;->a(LX/0Y1;Z)J

    move-result-wide v9

    move v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v11, p4

    move-object/from16 v12, p5

    invoke-virtual/range {v2 .. v12}, LX/0YV;->a(IISJZJLjava/lang/String;Ljava/lang/String;)Lcom/facebook/quicklog/PerformanceLoggingEvent;

    move-result-object v2

    .line 80109
    if-eqz v2, :cond_0

    .line 80110
    const-string v3, "markerNoteWithAnnotation"

    invoke-direct {p0, v3, p1}, LX/0Y1;->a(Ljava/lang/String;I)V

    .line 80111
    invoke-direct {p0, v2}, LX/0Y1;->a(Lcom/facebook/quicklog/PerformanceLoggingEvent;)V

    .line 80112
    :cond_0
    return-void
.end method

.method private a(ISLjava/lang/String;IZLjava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IS",
            "Ljava/lang/String;",
            "IZ",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 80079
    const-wide/16 v4, -0x1

    move-object v1, p0

    move v2, p1

    move v7, v3

    move v8, v3

    invoke-direct/range {v1 .. v8}, LX/0Y1;->a(IIJZZZ)LX/0Pn;

    move-result-object v2

    .line 80080
    if-eqz p5, :cond_0

    .line 80081
    iget-object v0, p0, LX/0Y1;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4l4;

    int-to-long v4, p4

    invoke-virtual {v0, p1, p2, v4, v5}, LX/4l4;->a(ISJ)V

    .line 80082
    :cond_0
    if-eqz v2, :cond_2

    .line 80083
    if-eqz p6, :cond_1

    .line 80084
    invoke-interface {p6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 80085
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, LX/0Pn;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 80086
    :cond_1
    sget-object v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->c:LX/0aw;

    invoke-virtual {v0}, LX/0aw;->b()LX/0Po;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;

    .line 80087
    iput p4, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->i:I

    .line 80088
    const/4 v1, -0x1

    iput v1, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->j:I

    .line 80089
    iget-object v1, v2, LX/0Pn;->r:Ljava/util/ArrayList;

    move-object v1, v1

    .line 80090
    invoke-virtual {v0, v1}, Lcom/facebook/quicklog/PerformanceLoggingEvent;->a(Ljava/util/List;)V

    .line 80091
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/quicklog/PerformanceLoggingEvent;->a(I)V

    .line 80092
    iput p1, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->k:I

    .line 80093
    iput-short p2, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->o:S

    .line 80094
    iget-object v1, p0, LX/0Y1;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->g:J

    .line 80095
    iget-object v1, p0, LX/0Y1;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->h:J

    .line 80096
    iget v1, v2, LX/0Pn;->b:I

    iput v1, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->f:I

    .line 80097
    iget v1, v2, LX/0Pn;->h:I

    iput v1, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->l:I

    .line 80098
    iget-boolean v1, v2, LX/0Pn;->i:Z

    iput-boolean v1, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->m:Z

    .line 80099
    iget-boolean v1, v2, LX/0Pn;->j:Z

    iput-boolean v1, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->n:Z

    .line 80100
    iput-short v6, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->p:S

    .line 80101
    iput p4, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->q:I

    .line 80102
    iput-object p3, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->y:Ljava/lang/String;

    .line 80103
    sget-object v1, LX/0Pn;->a:LX/0aw;

    invoke-virtual {v1, v2}, LX/0aw;->a(LX/0Po;)V

    .line 80104
    invoke-direct {p0, v0}, LX/0Y1;->a(Lcom/facebook/quicklog/PerformanceLoggingEvent;)V

    .line 80105
    :cond_2
    return-void
.end method

.method private a(LX/0Pn;IILjava/lang/String;LX/00q;LX/03R;JZZ)V
    .locals 9

    .prologue
    .line 80069
    if-eqz p1, :cond_1

    .line 80070
    const-string v0, "onMarkerStart"

    invoke-direct {p0, v0, p2}, LX/0Y1;->a(Ljava/lang/String;I)V

    .line 80071
    iput-object p4, p1, LX/0Pn;->k:Ljava/lang/String;

    .line 80072
    invoke-static {p5}, LX/0Y1;->a(LX/00q;)LX/00q;

    move-result-object v0

    iput-object v0, p1, LX/0Pn;->o:LX/00q;

    .line 80073
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    invoke-virtual {v0, p1, p6}, LX/0YV;->a(LX/0Pn;LX/03R;)V

    .line 80074
    :goto_0
    const-wide/16 v0, 0x4

    invoke-static {v0, v1}, LX/00k;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80075
    const-wide/16 v0, 0x4

    invoke-static {p2}, LX/0Y1;->g(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, p3}, LX/0YV;->i(II)I

    move-result v3

    invoke-static/range {p7 .. p8}, LX/0Y1;->b(J)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, LX/018;->a(JLjava/lang/String;IJ)V

    .line 80076
    :cond_0
    return-void

    .line 80077
    :cond_1
    const-string v0, "markerNotStarted"

    invoke-direct {p0, v0, p2}, LX/0Y1;->a(Ljava/lang/String;I)V

    .line 80078
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    move v1, p2

    move v2, p3

    move-object v3, p4

    move-wide/from16 v4, p7

    move/from16 v6, p9

    move/from16 v7, p10

    invoke-virtual/range {v0 .. v7}, LX/0YV;->a(IILjava/lang/String;JZZ)V

    goto :goto_0
.end method

.method private static a(LX/0Y1;IIIJLjava/lang/String;ZZLX/00q;LX/03R;)V
    .locals 22
    .param p8    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # LX/00q;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 80063
    invoke-static/range {p4 .. p5}, LX/0Y1;->c(J)Z

    move-result v10

    .line 80064
    move-object/from16 v0, p0

    move-wide/from16 v1, p4

    invoke-static {v0, v1, v2}, LX/0Y1;->a(LX/0Y1;J)J

    move-result-wide v8

    move-object/from16 v5, p0

    move/from16 v6, p1

    move/from16 v7, p3

    move/from16 v11, p8

    move-object/from16 v12, p9

    move-object/from16 v13, p10

    .line 80065
    invoke-direct/range {v5 .. v13}, LX/0Y1;->a(IIJZZLX/00q;LX/03R;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 80066
    :goto_0
    return-void

    :cond_0
    move-object/from16 v4, p0

    move/from16 v5, p1

    move/from16 v6, p2

    move/from16 v7, p3

    move/from16 v11, p7

    move/from16 v12, p8

    .line 80067
    invoke-direct/range {v4 .. v12}, LX/0Y1;->a(IIIJZZZ)LX/0Pn;

    move-result-object v12

    move-object/from16 v11, p0

    move/from16 v13, p1

    move/from16 v14, p3

    move-object/from16 v15, p6

    move-object/from16 v16, p9

    move-object/from16 v17, p10

    move-wide/from16 v18, v8

    move/from16 v20, v10

    move/from16 v21, p8

    .line 80068
    invoke-direct/range {v11 .. v21}, LX/0Y1;->a(LX/0Pn;IILjava/lang/String;LX/00q;LX/03R;JZZ)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/quicklog/PerformanceLoggingEvent;)V
    .locals 5

    .prologue
    .line 80045
    iget-object v0, p0, LX/0Y1;->o:[LX/0Y8;

    if-eqz v0, :cond_0

    .line 80046
    iget-object v1, p0, LX/0Y1;->o:[LX/0Y8;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 80047
    invoke-interface {v3, p1}, LX/0Y8;->a(Lcom/facebook/quicklog/PerformanceLoggingEvent;)V

    .line 80048
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80049
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 80050
    iget-object v1, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->x:Ljava/util/ArrayList;

    .line 80051
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 80052
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 80053
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80054
    const/4 v2, 0x1

    if-le v3, v2, :cond_1

    .line 80055
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80056
    :cond_1
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    .line 80057
    goto :goto_1

    .line 80058
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 80059
    iget-object v1, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->w:Ljava/util/ArrayList;

    const-string v2, "trace_tags"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80060
    iget-object v1, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->w:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80061
    :cond_3
    invoke-static {p0, p1}, LX/0Y1;->c(LX/0Y1;Lcom/facebook/quicklog/PerformanceLoggingEvent;)V

    .line 80062
    return-void
.end method

.method private a(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 80043
    invoke-virtual {p0}, LX/0Y1;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x3

    invoke-static {p1}, LX/01m;->b(I)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 80044
    :goto_0
    return-void

    :cond_0
    goto :goto_0
.end method

.method private a(IIJZZLX/00q;LX/03R;)Z
    .locals 15
    .param p7    # LX/00q;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/03R;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 80036
    invoke-virtual/range {p0 .. p2}, LX/0Y1;->j(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 80037
    iget-object v2, p0, LX/0Y1;->d:LX/0YV;

    iget-object v3, p0, LX/0Y1;->c:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v8

    move/from16 v0, p5

    invoke-static {p0, v0}, LX/0Y1;->a(LX/0Y1;Z)J

    move-result-wide v10

    invoke-static/range {p7 .. p7}, LX/0Y1;->a(LX/00q;)LX/00q;

    move-result-object v12

    move/from16 v3, p1

    move/from16 v4, p2

    move-wide/from16 v5, p3

    move/from16 v7, p5

    move-object/from16 v13, p8

    move/from16 v14, p6

    invoke-virtual/range {v2 .. v14}, LX/0YV;->a(IIJZJJLX/00q;LX/03R;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 80038
    const-wide/16 v2, 0x4

    invoke-static {v2, v3}, LX/00k;->a(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 80039
    const-wide/16 v2, 0x4

    invoke-static/range {p1 .. p1}, LX/0Y1;->g(I)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {p1 .. p2}, LX/0Y1;->k(II)I

    move-result v5

    invoke-static {v2, v3, v4, v5}, LX/018;->f(JLjava/lang/String;I)V

    .line 80040
    const-wide/16 v2, 0x4

    invoke-static/range {p1 .. p1}, LX/0Y1;->g(I)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {p1 .. p2}, LX/0Y1;->k(II)I

    move-result v5

    invoke-static/range {p3 .. p4}, LX/0Y1;->b(J)J

    move-result-wide v6

    invoke-static/range {v2 .. v7}, LX/018;->a(JLjava/lang/String;IJ)V

    .line 80041
    :cond_0
    const/4 v2, 0x1

    .line 80042
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static b(J)J
    .locals 2

    .prologue
    .line 80133
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p0, p1}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private b(Lcom/facebook/quicklog/PerformanceLoggingEvent;)V
    .locals 7

    .prologue
    .line 80322
    iget-object v0, p0, LX/0Y1;->m:LX/0Y3;

    new-instance v1, Lcom/facebook/quicklog/QuickPerformanceLoggerImpl$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/quicklog/QuickPerformanceLoggerImpl$1;-><init>(LX/0Y1;Lcom/facebook/quicklog/PerformanceLoggingEvent;)V

    const-wide/16 v2, 0x1388

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 80323
    iget-object v5, v0, LX/0Y3;->b:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v5, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 80324
    goto :goto_0

    .line 80325
    :goto_0
    return-void
.end method

.method public static c(LX/0Y1;Lcom/facebook/quicklog/PerformanceLoggingEvent;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 80270
    iget-boolean v1, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->d:Z

    if-eqz v1, :cond_1

    .line 80271
    iget-object v1, p0, LX/0Y1;->l:LX/0Y2;

    invoke-virtual {v1}, LX/0Y2;->a()LX/03R;

    move-result-object v1

    .line 80272
    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v1, v2, :cond_0

    .line 80273
    sget-object v1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->c:LX/0aw;

    invoke-virtual {v1, p1}, LX/0aw;->a(LX/0Po;)V

    .line 80274
    iput-object v0, p0, LX/0Y1;->h:Lcom/facebook/quicklog/PerformanceLoggingEvent;

    .line 80275
    :goto_0
    return-void

    .line 80276
    :cond_0
    sget-object v2, LX/03R;->UNSET:LX/03R;

    if-ne v1, v2, :cond_1

    .line 80277
    invoke-direct {p0, p1}, LX/0Y1;->b(Lcom/facebook/quicklog/PerformanceLoggingEvent;)V

    goto :goto_0

    .line 80278
    :cond_1
    iget-object v1, p0, LX/0Y1;->i:LX/0YT;

    .line 80279
    iget v2, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->k:I

    move v2, v2

    .line 80280
    invoke-virtual {v1, v2}, LX/0YT;->b(I)Z

    move-result v4

    .line 80281
    invoke-virtual {p0}, LX/0Y1;->b()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 80282
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 80283
    if-eqz v4, :cond_2

    .line 80284
    const-string v1, " [SUPPRESSED]"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80285
    :cond_2
    iget-object v1, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->y:Ljava/lang/String;

    move-object v1, v1

    .line 80286
    if-eqz v1, :cond_3

    .line 80287
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " ID="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/quicklog/PerformanceLoggingEvent;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80288
    :cond_3
    iget-object v1, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->w:Ljava/util/ArrayList;

    move-object v1, v1

    .line 80289
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_5

    .line 80290
    iget-object v1, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->w:Ljava/util/ArrayList;

    move-object v1, v1

    .line 80291
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v1, v0

    move v2, v3

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 80292
    add-int/lit8 v2, v2, 0x1

    .line 80293
    rem-int/lit8 v7, v2, 0x2

    if-nez v7, :cond_4

    .line 80294
    const-string v7, ", "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80295
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80296
    const-string v7, "="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80297
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    move-object v1, v0

    .line 80298
    goto :goto_1

    .line 80299
    :cond_5
    sget-object v1, LX/0Y1;->a:Ljava/lang/String;

    const-string v2, "QPLSent - %s %s %d[ms] %s (1:%d) %s"

    const/4 v0, 0x6

    new-array v6, v0, [Ljava/lang/Object;

    .line 80300
    iget-object v0, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->y:Ljava/lang/String;

    move-object v0, v0

    .line 80301
    if-eqz v0, :cond_7

    .line 80302
    iget-object v0, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->y:Ljava/lang/String;

    move-object v0, v0

    .line 80303
    :goto_2
    aput-object v0, v6, v3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/facebook/quicklog/PerformanceLoggingEvent;->g()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v0

    const/4 v0, 0x2

    .line 80304
    iget v3, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->i:I

    move v3, v3

    .line 80305
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v0

    const/4 v0, 0x3

    .line 80306
    iget-boolean v3, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->n:Z

    move v3, v3

    .line 80307
    iget-boolean v7, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->m:Z

    move v7, v7

    .line 80308
    invoke-static {v3, v7}, LX/17y;->a(ZZ)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v0

    const/4 v0, 0x4

    .line 80309
    iget v3, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->l:I

    move v3, v3

    .line 80310
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v0

    const/4 v0, 0x5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v0

    invoke-static {v1, v2, v6}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 80311
    :cond_6
    if-nez v4, :cond_8

    .line 80312
    sget-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a:Z

    move v1, v1

    .line 80313
    move v0, v1

    .line 80314
    if-nez v0, :cond_8

    .line 80315
    iget-object v0, p0, LX/0Y1;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17K;

    iput-object v0, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->r:LX/17K;

    .line 80316
    iget-object v0, p0, LX/0Y1;->r:LX/0Y0;

    iput-object v0, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->s:LX/0Y0;

    .line 80317
    iget-object v0, p0, LX/0Y1;->m:LX/0Y3;

    .line 80318
    iget-object v1, v0, LX/0Y3;->a:Ljava/util/concurrent/ExecutorService;

    const v2, -0x2e839915

    invoke-static {v1, p1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 80319
    :goto_3
    iput-object p1, p0, LX/0Y1;->h:Lcom/facebook/quicklog/PerformanceLoggingEvent;

    goto/16 :goto_0

    .line 80320
    :cond_7
    invoke-virtual {p1}, Lcom/facebook/quicklog/PerformanceLoggingEvent;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 80321
    :cond_8
    sget-object v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->c:LX/0aw;

    invoke-virtual {v0, p1}, LX/0aw;->a(LX/0Po;)V

    goto :goto_3
.end method

.method private static c(J)Z
    .locals 2

    .prologue
    .line 80269
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static g(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 80268
    invoke-static {p0}, LX/2BW;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static k(II)I
    .locals 1

    .prologue
    .line 80267
    invoke-static {p0, p1}, LX/0YV;->i(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 12

    .prologue
    const-wide/16 v10, 0x4

    .line 80259
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    invoke-virtual {v0}, LX/0YV;->a()Ljava/util/List;

    move-result-object v1

    .line 80260
    invoke-static {v10, v11}, LX/00k;->a(J)Z

    move-result v2

    .line 80261
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 80262
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/16 v6, 0x20

    shr-long/2addr v4, v6

    long-to-int v4, v4

    .line 80263
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, -0x1

    and-long/2addr v6, v8

    long-to-int v0, v6

    .line 80264
    if-eqz v2, :cond_0

    .line 80265
    invoke-static {v0}, LX/0Y1;->g(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v4}, LX/0YV;->i(II)I

    move-result v0

    invoke-static {v10, v11, v5, v0}, LX/018;->f(JLjava/lang/String;I)V

    goto :goto_0

    .line 80266
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 80257
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    invoke-virtual {v0, p1, p2}, LX/0YV;->h(II)V

    .line 80258
    return-void
.end method

.method public final a(IIILjava/lang/String;JZZLX/00q;LX/03R;)V
    .locals 11
    .param p9    # LX/00q;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # LX/03R;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 80244
    if-nez p8, :cond_3

    if-nez p2, :cond_3

    .line 80245
    invoke-virtual {p0, p1, p3}, LX/0Y1;->j(II)Z

    move-result v0

    if-nez v0, :cond_3

    .line 80246
    iget-object v0, p0, LX/0Y1;->f:LX/0Xe;

    invoke-virtual {v0}, LX/0Xe;->a()LX/0ar;

    move-result-object v1

    .line 80247
    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 80248
    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {v1}, LX/0ar;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 80249
    :goto_1
    if-eqz v0, :cond_3

    iget-object v0, p0, LX/0Y1;->k:LX/0Xh;

    invoke-virtual {v0}, LX/0Xh;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 80250
    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p3

    move-object v3, p4

    move-wide/from16 v4, p5

    invoke-direct/range {v0 .. v6}, LX/0Y1;->a(IILjava/lang/String;JZ)V

    .line 80251
    :goto_2
    return-void

    .line 80252
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 80253
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 80254
    :cond_3
    if-nez p2, :cond_4

    .line 80255
    const/4 v8, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p3

    move-wide/from16 v4, p5

    move-object v6, p4

    move/from16 v7, p7

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v1 .. v10}, LX/0Y1;->a(IIJLjava/lang/String;ZZLX/00q;LX/03R;)V

    goto :goto_2

    .line 80256
    :cond_4
    const/4 v8, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-wide/from16 v4, p5

    move-object v6, p4

    move/from16 v7, p7

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-static/range {v0 .. v10}, LX/0Y1;->a(LX/0Y1;IIIJLjava/lang/String;ZZLX/00q;LX/03R;)V

    goto :goto_2
.end method

.method public final a(IIILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 80241
    invoke-direct {p0, p1, p2, p3}, LX/0Y1;->a(III)V

    .line 80242
    invoke-virtual {p0, p1, p3, p4, p5}, LX/0Y1;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 80243
    return-void
.end method

.method public final a(IIJZ)V
    .locals 11

    .prologue
    .line 80202
    const/4 v6, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-wide v4, p3

    move/from16 v7, p5

    invoke-direct/range {v1 .. v10}, LX/0Y1;->a(IIJLjava/lang/String;ZZLX/00q;LX/03R;)V

    .line 80203
    return-void
.end method

.method public final a(IILjava/lang/String;)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    const/4 v7, 0x0

    .line 80239
    const-wide/16 v4, -0x1

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v6, p3

    move v8, v7

    move-object v10, v9

    invoke-direct/range {v1 .. v10}, LX/0Y1;->a(IIJLjava/lang/String;ZZLX/00q;LX/03R;)V

    .line 80240
    return-void
.end method

.method public final a(IILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 80236
    invoke-virtual {p0, p1, p2}, LX/0Y1;->e(II)V

    .line 80237
    invoke-virtual {p0, p1, p2, p3, p4}, LX/0Y1;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 80238
    return-void
.end method

.method public final a(IILjava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Collection",
            "<+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 80234
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    invoke-virtual {v0, p1, p2, p3}, LX/0YV;->a(IILjava/util/Collection;)V

    .line 80235
    return-void
.end method

.method public final a(IILjava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 80232
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    invoke-virtual {v0, p1, p2, p3}, LX/0YV;->a(IILjava/util/Map;)V

    .line 80233
    return-void
.end method

.method public final a(IIS)V
    .locals 6

    .prologue
    .line 80230
    const-wide/16 v4, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, LX/0Y1;->a(IISJ)V

    .line 80231
    return-void
.end method

.method public final a(IISJLX/03R;)V
    .locals 14

    .prologue
    .line 80214
    invoke-static/range {p4 .. p5}, LX/0Y1;->c(J)Z

    move-result v8

    .line 80215
    move-wide/from16 v0, p4

    invoke-static {p0, v0, v1}, LX/0Y1;->a(LX/0Y1;J)J

    move-result-wide v6

    .line 80216
    invoke-virtual/range {p0 .. p2}, LX/0Y1;->c(II)Z

    move-result v13

    .line 80217
    iget-object v2, p0, LX/0Y1;->d:LX/0YV;

    move/from16 v0, p2

    invoke-virtual {v2, p1, v0}, LX/0YV;->b(II)Ljava/lang/String;

    move-result-object v12

    .line 80218
    iget-object v2, p0, LX/0Y1;->d:LX/0YV;

    invoke-static {p0, v8}, LX/0Y1;->a(LX/0Y1;Z)J

    move-result-wide v9

    move v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v11, p6

    invoke-virtual/range {v2 .. v11}, LX/0YV;->a(IISJZJLX/03R;)Lcom/facebook/quicklog/PerformanceLoggingEvent;

    move-result-object v2

    .line 80219
    if-eqz v2, :cond_0

    .line 80220
    iget-object v3, p0, LX/0Y1;->l:LX/0Y2;

    invoke-virtual {v3}, LX/0Y2;->a()LX/03R;

    move-result-object v3

    iput-object v3, v2, Lcom/facebook/quicklog/PerformanceLoggingEvent;->u:LX/03R;

    .line 80221
    const-string v3, "markerEnd"

    invoke-direct {p0, v3, p1}, LX/0Y1;->a(Ljava/lang/String;I)V

    .line 80222
    invoke-direct {p0, v2}, LX/0Y1;->a(Lcom/facebook/quicklog/PerformanceLoggingEvent;)V

    .line 80223
    :cond_0
    const-wide/16 v2, 0x4

    invoke-static {v2, v3}, LX/00k;->a(J)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v13, :cond_1

    .line 80224
    invoke-static {p1}, LX/0Y1;->g(I)Ljava/lang/String;

    move-result-object v4

    .line 80225
    if-eqz v12, :cond_2

    move-object v8, v12

    .line 80226
    :goto_0
    const-wide/16 v2, 0x4

    invoke-static/range {p1 .. p2}, LX/0Y1;->k(II)I

    move-result v5

    invoke-static {v6, v7}, LX/0Y1;->b(J)J

    move-result-wide v6

    invoke-static/range {v2 .. v7}, LX/018;->b(JLjava/lang/String;IJ)V

    .line 80227
    const-wide/16 v2, 0x4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static/range {p3 .. p3}, LX/2BY;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {p1 .. p2}, LX/0Y1;->k(II)I

    move-result v6

    invoke-static {v2, v3, v4, v5, v6}, LX/018;->b(JLjava/lang/String;Ljava/lang/String;I)V

    .line 80228
    :cond_1
    return-void

    :cond_2
    move-object v8, v4

    .line 80229
    goto :goto_0
.end method

.method public final a(IISLjava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 80212
    const-wide/16 v6, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, LX/0Y1;->a(IISLjava/lang/String;Ljava/lang/String;J)V

    .line 80213
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 80210
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, LX/0YV;->a(IILjava/lang/String;)V

    .line 80211
    return-void
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 80207
    invoke-virtual {p0, p1}, LX/0Y1;->b(I)V

    .line 80208
    invoke-virtual {p0, p1, p2, p3}, LX/0Y1;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 80209
    return-void
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 80204
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p4, p5}, LX/0Y1;->markerStart(IIJ)V

    .line 80205
    invoke-virtual {p0, p1, p2, p3}, LX/0Y1;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 80206
    return-void
.end method

.method public final a(ILjava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 80200
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, LX/0YV;->a(IILjava/util/Collection;)V

    .line 80201
    return-void
.end method

.method public final a(IS)V
    .locals 6

    .prologue
    .line 79966
    const/4 v2, 0x0

    const-wide/16 v4, -0x1

    move-object v0, p0

    move v1, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, LX/0Y1;->a(IISJ)V

    .line 79967
    return-void
.end method

.method public final a(ISI)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 79964
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, p3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, LX/0Y1;->a(ISLjava/lang/String;IZLjava/util/Map;)V

    .line 79965
    return-void
.end method

.method public final a(ISILjava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ISI",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 79962
    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/0Y1;->a(ISLjava/lang/String;IZLjava/util/Map;)V

    .line 79963
    return-void
.end method

.method public final a(ISJ)V
    .locals 7

    .prologue
    .line 79960
    const/4 v2, 0x0

    move-object v0, p0

    move v1, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, LX/0Y1;->a(IISJ)V

    .line 79961
    return-void
.end method

.method public final a(ISLjava/lang/String;I)V
    .locals 7

    .prologue
    .line 79958
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v6}, LX/0Y1;->a(ISLjava/lang/String;IZLjava/util/Map;)V

    .line 79959
    return-void
.end method

.method public final a(ISLjava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 79956
    const/4 v2, 0x0

    const-wide/16 v6, -0x1

    move-object v0, p0

    move v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v7}, LX/0Y1;->a(IISLjava/lang/String;Ljava/lang/String;J)V

    .line 79957
    return-void
.end method

.method public final a(ISLjava/lang/String;Ljava/lang/String;J)V
    .locals 9

    .prologue
    .line 79954
    const/4 v2, 0x0

    move-object v0, p0

    move v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    invoke-direct/range {v0 .. v7}, LX/0Y1;->a(IISLjava/lang/String;Ljava/lang/String;J)V

    .line 79955
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x4

    .line 79946
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    invoke-virtual {v0, p1}, LX/0YV;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 79947
    invoke-static {v8, v9}, LX/00k;->a(J)Z

    move-result v1

    .line 79948
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 79949
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/16 v3, 0x20

    shr-long/2addr v4, v3

    long-to-int v3, v4

    .line 79950
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    and-long/2addr v4, v6

    long-to-int v0, v4

    .line 79951
    if-eqz v1, :cond_0

    .line 79952
    invoke-static {v0}, LX/0Y1;->g(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3}, LX/0YV;->i(II)I

    move-result v0

    invoke-static {v8, v9, v4, v0}, LX/018;->f(JLjava/lang/String;I)V

    goto :goto_0

    .line 79953
    :cond_1
    return-void
.end method

.method public final a(S)V
    .locals 1

    .prologue
    .line 79944
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    invoke-virtual {v0, p1}, LX/0YV;->a(S)V

    .line 79945
    return-void
.end method

.method public final a(SLjava/lang/String;)V
    .locals 1

    .prologue
    .line 79942
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    invoke-virtual {v0, p1, p2}, LX/0YV;->a(SLjava/lang/String;)V

    .line 79943
    return-void
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    .line 79941
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/0YV;->a(II)Z

    move-result v0

    return v0
.end method

.method public final b(I)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 79939
    const-wide/16 v4, -0x1

    move-object v1, p0

    move v2, p1

    move v7, v3

    move v8, v3

    move-object v9, v6

    move-object v10, v6

    invoke-direct/range {v1 .. v10}, LX/0Y1;->a(IIJLjava/lang/String;ZZLX/00q;LX/03R;)V

    .line 79940
    return-void
.end method

.method public final b(II)V
    .locals 2

    .prologue
    .line 79934
    const/high16 v0, -0x80000000

    if-ne p2, v0, :cond_0

    .line 79935
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 79936
    :cond_0
    iget-object v1, p0, LX/0Y1;->j:Landroid/util/SparseIntArray;

    monitor-enter v1

    .line 79937
    :try_start_0
    iget-object v0, p0, LX/0Y1;->j:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 79938
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(IILjava/lang/String;)V
    .locals 1

    .prologue
    .line 79932
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    invoke-virtual {v0, p1, p2, p3}, LX/0YV;->a(IILjava/lang/String;)V

    .line 79933
    return-void
.end method

.method public final b(IILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 79930
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/0YV;->a(IILjava/lang/String;Ljava/lang/String;)V

    .line 79931
    return-void
.end method

.method public final b(IIS)V
    .locals 7

    .prologue
    .line 79928
    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v6}, LX/0Y1;->a(IISJLX/03R;)V

    .line 79929
    return-void
.end method

.method public final b(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 79926
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2, p3}, LX/0Y1;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 79927
    return-void
.end method

.method public final b(IS)V
    .locals 7

    .prologue
    .line 79924
    const/4 v2, 0x0

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v3, p2

    invoke-virtual/range {v0 .. v6}, LX/0Y1;->a(IISJLX/03R;)V

    .line 79925
    return-void
.end method

.method public final b(ISI)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 79922
    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, p3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, LX/0Y1;->a(ISLjava/lang/String;IZLjava/util/Map;)V

    .line 79923
    return-void
.end method

.method public final b(ISJ)V
    .locals 7

    .prologue
    .line 79920
    const/4 v2, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v3, p2

    move-wide v4, p3

    invoke-virtual/range {v0 .. v6}, LX/0Y1;->a(IISJLX/03R;)V

    .line 79921
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 80005
    iget-object v0, p0, LX/0Y1;->n:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-ne v0, v1, :cond_0

    .line 80006
    iget-object v0, p0, LX/0Y1;->k:LX/0Xh;

    invoke-virtual {v0}, LX/0Xh;->a()LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/0Y1;->n:LX/03R;

    .line 80007
    :cond_0
    iget-object v0, p0, LX/0Y1;->n:LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method

.method public final c(I)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 80032
    const-wide/16 v4, -0x1

    const/4 v8, 0x1

    move-object v1, p0

    move v2, p1

    move v7, v3

    move-object v9, v6

    move-object v10, v6

    invoke-direct/range {v1 .. v10}, LX/0Y1;->a(IIJLjava/lang/String;ZZLX/00q;LX/03R;)V

    .line 80033
    return-void
.end method

.method public final c(IS)V
    .locals 4

    .prologue
    .line 80028
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, p1, v2, v3}, LX/0YV;->a(IJ)Ljava/util/List;

    move-result-object v0

    .line 80029
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 80030
    invoke-virtual {p0, v0, p2}, LX/0Y1;->b(IS)V

    goto :goto_0

    .line 80031
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 80026
    iget-object v0, p0, LX/0Y1;->f:LX/0Xe;

    invoke-virtual {v0}, LX/0Xe;->a()LX/0ar;

    move-result-object v0

    .line 80027
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0ar;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(II)Z
    .locals 1

    .prologue
    .line 80025
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    invoke-virtual {v0, p1, p2}, LX/0YV;->a(II)Z

    move-result v0

    return v0
.end method

.method public final currentMonotonicTimestamp()J
    .locals 2

    .prologue
    .line 80024
    iget-object v0, p0, LX/0Y1;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    return-wide v0
.end method

.method public final d(II)J
    .locals 4

    .prologue
    .line 80018
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    .line 80019
    invoke-static {p1, p2}, LX/0YV;->i(II)I

    move-result v2

    invoke-static {v0, v2}, LX/0YV;->a(LX/0YV;I)LX/0Pn;

    move-result-object v2

    .line 80020
    if-eqz v2, :cond_0

    .line 80021
    iget-wide v2, v2, LX/0Pn;->d:J

    .line 80022
    :goto_0
    move-wide v0, v2

    .line 80023
    return-wide v0

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public final d(I)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x4

    const/4 v1, 0x0

    .line 80014
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    invoke-virtual {v0, p1, v1}, LX/0YV;->g(II)V

    .line 80015
    invoke-static {v2, v3}, LX/00k;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80016
    invoke-static {p1}, LX/0Y1;->g(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v1}, LX/0YV;->i(II)I

    move-result v1

    invoke-static {v2, v3, v0, v1}, LX/018;->f(JLjava/lang/String;I)V

    .line 80017
    :cond_0
    return-void
.end method

.method public final e(I)V
    .locals 4

    .prologue
    .line 80010
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, p1, v2, v3}, LX/0YV;->a(IJ)Ljava/util/List;

    move-result-object v0

    .line 80011
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 80012
    invoke-virtual {p0, v0}, LX/0Y1;->d(I)V

    goto :goto_0

    .line 80013
    :cond_0
    return-void
.end method

.method public final e(II)V
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 80008
    const-wide/16 v4, -0x1

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v8, v7

    move-object v9, v6

    move-object v10, v6

    invoke-direct/range {v1 .. v10}, LX/0Y1;->a(IIJLjava/lang/String;ZZLX/00q;LX/03R;)V

    .line 80009
    return-void
.end method

.method public final f(II)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 79968
    const-wide/16 v4, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v7, v3

    move v8, v3

    move-object v9, v6

    move-object v10, v6

    invoke-static/range {v0 .. v10}, LX/0Y1;->a(LX/0Y1;IIIJLjava/lang/String;ZZLX/00q;LX/03R;)V

    .line 79969
    return-void
.end method

.method public final f(I)Z
    .locals 2

    .prologue
    .line 80004
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/0YV;->d(II)Z

    move-result v0

    return v0
.end method

.method public final g(II)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 80002
    const-wide/16 v4, -0x1

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v9, v6

    move-object v10, v6

    invoke-direct/range {v1 .. v10}, LX/0Y1;->a(IIJLjava/lang/String;ZZLX/00q;LX/03R;)V

    .line 80003
    return-void
.end method

.method public final h(II)Z
    .locals 1

    .prologue
    .line 79996
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    .line 79997
    invoke-static {p1, p2}, LX/0YV;->i(II)I

    move-result p0

    invoke-static {v0, p0}, LX/0YV;->a(LX/0YV;I)LX/0Pn;

    move-result-object p0

    .line 79998
    if-eqz p0, :cond_0

    .line 79999
    iget-boolean p0, p0, LX/0Pn;->l:Z

    .line 80000
    :goto_0
    move v0, p0

    .line 80001
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final i(II)V
    .locals 6

    .prologue
    .line 79992
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    iget-object v1, p0, LX/0Y1;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    int-to-long v4, p2

    sub-long/2addr v2, v4

    invoke-virtual {v0, p1, v2, v3}, LX/0YV;->a(IJ)Ljava/util/List;

    move-result-object v0

    .line 79993
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 79994
    invoke-virtual {p0, v0}, LX/0Y1;->d(I)V

    goto :goto_0

    .line 79995
    :cond_0
    return-void
.end method

.method public final j(II)Z
    .locals 1

    .prologue
    .line 79980
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    invoke-virtual {v0, p1, p2}, LX/0YV;->d(II)Z

    move-result v0

    return v0
.end method

.method public final markerCancel(II)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x4

    .line 79976
    iget-object v0, p0, LX/0Y1;->d:LX/0YV;

    invoke-virtual {v0, p1, p2}, LX/0YV;->g(II)V

    .line 79977
    invoke-static {v2, v3}, LX/00k;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79978
    invoke-static {p1}, LX/0Y1;->g(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2}, LX/0YV;->i(II)I

    move-result v1

    invoke-static {v2, v3, v0, v1}, LX/018;->f(JLjava/lang/String;I)V

    .line 79979
    :cond_0
    return-void
.end method

.method public final markerEnd(IISJ)V
    .locals 8

    .prologue
    .line 79974
    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v6}, LX/0Y1;->a(IISJLX/03R;)V

    .line 79975
    return-void
.end method

.method public final markerNote(IISJ)V
    .locals 0

    .prologue
    .line 79972
    invoke-direct/range {p0 .. p5}, LX/0Y1;->a(IISJ)V

    .line 79973
    return-void
.end method

.method public final markerStart(IIJ)V
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 79970
    move-object v1, p0

    move v2, p1

    move v3, p2

    move-wide v4, p3

    move v8, v7

    move-object v9, v6

    move-object v10, v6

    invoke-direct/range {v1 .. v10}, LX/0Y1;->a(IIJLjava/lang/String;ZZLX/00q;LX/03R;)V

    .line 79971
    return-void
.end method
