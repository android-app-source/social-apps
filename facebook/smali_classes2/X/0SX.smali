.class public final LX/0SX;
.super LX/0SY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0SY",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public volatile a:J

.field public b:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public c:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILX/0R1;)V
    .locals 2
    .param p4    # LX/0R1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 61419
    invoke-direct {p0, p1, p2, p3, p4}, LX/0SY;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILX/0R1;)V

    .line 61420
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LX/0SX;->a:J

    .line 61421
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    move-object v0, v0

    .line 61422
    iput-object v0, p0, LX/0SX;->b:LX/0R1;

    .line 61423
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    move-object v0, v0

    .line 61424
    iput-object v0, p0, LX/0SX;->c:LX/0R1;

    .line 61425
    return-void
.end method


# virtual methods
.method public final getAccessTime()J
    .locals 2

    .prologue
    .line 61418
    iget-wide v0, p0, LX/0SX;->a:J

    return-wide v0
.end method

.method public final getNextInAccessQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 61417
    iget-object v0, p0, LX/0SX;->b:LX/0R1;

    return-object v0
.end method

.method public final getPreviousInAccessQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 61410
    iget-object v0, p0, LX/0SX;->c:LX/0R1;

    return-object v0
.end method

.method public final setAccessTime(J)V
    .locals 1

    .prologue
    .line 61415
    iput-wide p1, p0, LX/0SX;->a:J

    .line 61416
    return-void
.end method

.method public final setNextInAccessQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 61413
    iput-object p1, p0, LX/0SX;->b:LX/0R1;

    .line 61414
    return-void
.end method

.method public final setPreviousInAccessQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 61411
    iput-object p1, p0, LX/0SX;->c:LX/0R1;

    .line 61412
    return-void
.end method
