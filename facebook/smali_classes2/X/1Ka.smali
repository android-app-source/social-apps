.class public LX/1Ka;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hj;
.implements LX/1DD;
.implements LX/0hk;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gd;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2iz;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BeH;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/1Kb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/1Kf;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "Lcom/facebook/api/feed/data/LegacyFeedUnitUpdater;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 232880
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 232881
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 232882
    iput-object v0, p0, LX/1Ka;->a:LX/0Ot;

    .line 232883
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 232884
    iput-object v0, p0, LX/1Ka;->c:LX/0Ot;

    .line 232885
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 232886
    iput-object v0, p0, LX/1Ka;->d:LX/0Ot;

    .line 232887
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 232888
    iput-object v0, p0, LX/1Ka;->e:LX/0Ot;

    .line 232889
    return-void
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 12

    .prologue
    const/16 v1, 0x6dc

    const/4 v5, 0x0

    .line 232839
    if-eq p1, v1, :cond_1

    const/16 v0, 0x6de

    if-eq p1, v0, :cond_1

    const/16 v0, 0x3d

    if-eq p1, v0, :cond_1

    const/16 v0, 0x3c

    if-eq p1, v0, :cond_1

    .line 232840
    :cond_0
    return-void

    .line 232841
    :cond_1
    if-ne p1, v1, :cond_2

    .line 232842
    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 232843
    const/4 v8, 0x0

    .line 232844
    if-eqz p3, :cond_9

    const-string v6, "publishPostParams"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 232845
    const-string v6, "publishPostParams"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 232846
    if-eqz v6, :cond_8

    .line 232847
    iget-object v8, v6, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    move v10, v9

    .line 232848
    :goto_0
    iget-object v6, p0, LX/1Ka;->a:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0gd;

    sget-object v7, LX/0ge;->COMPOSER_ACTIVITY_RESULT:LX/0ge;

    move v11, p2

    invoke-virtual/range {v6 .. v11}, LX/0gd;->a(LX/0ge;Ljava/lang/String;ZZI)V

    .line 232849
    :cond_2
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    .line 232850
    iget-object v0, p0, LX/1Ka;->b:LX/0Uh;

    sget v1, LX/4Bm;->a:I

    invoke-virtual {v0, v1, v5}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 232851
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 232852
    const-string v1, "publishPostParams"

    new-instance v2, LX/5M9;

    invoke-direct {v2, v0}, LX/5M9;-><init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V

    const/4 v0, 0x1

    .line 232853
    iput-boolean v0, v2, LX/5M9;->ad:Z

    .line 232854
    move-object v0, v2

    .line 232855
    invoke-virtual {v0}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v0

    invoke-virtual {p3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 232856
    :cond_3
    const-string v0, "publishEditPostParamsKey"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    .line 232857
    const-string v1, "publishPostParams"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    .line 232858
    const-string v2, "extra_composer_has_published"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    if-nez v0, :cond_4

    if-eqz v1, :cond_5

    .line 232859
    :cond_4
    iget-object v0, p0, LX/1Ka;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 232860
    :cond_5
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 232861
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 232862
    iget-object v1, p0, LX/1Ka;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2iz;

    iget-object v3, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    iget-object v2, p0, LX/1Ka;->i:LX/1Iu;

    .line 232863
    iget-object v4, v2, LX/1Iu;->a:Ljava/lang/Object;

    move-object v2, v4

    .line 232864
    check-cast v2, Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v1, v3, v2}, LX/2iz;->a(Ljava/lang/String;Lcom/facebook/base/fragment/FbFragment;)Z

    .line 232865
    iget-object v1, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->placeTag:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-boolean v1, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->isExplicitLocation:Z

    if-eqz v1, :cond_6

    .line 232866
    iget-object v1, p0, LX/1Ka;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BeH;

    new-instance v2, LX/BeI;

    iget-object v3, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->placeTag:Ljava/lang/String;

    const-string v4, "android_feather_post_compose"

    invoke-direct {v2, v3, v4}, LX/BeI;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "checked_in"

    iget-object v4, p0, LX/1Ka;->h:Landroid/content/Context;

    invoke-virtual {v1, v2, v3, v4}, LX/BeH;->a(LX/BeI;Ljava/lang/String;Landroid/content/Context;)V

    .line 232867
    :cond_6
    iget-object v1, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->privacy:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->privacy:Ljava/lang/String;

    const-string v2, "{\"value\":\"SELF\"}"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v0, :cond_7

    .line 232868
    iget-object v0, p0, LX/1Ka;->f:LX/1Kb;

    invoke-virtual {v0}, LX/1Kb;->c()V

    .line 232869
    :cond_7
    const-string v0, "publishEditPostParamsKey"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232870
    const-string v0, "publishEditPostParamsKey"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/EditPostParams;

    .line 232871
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getCacheIds()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getCacheIds()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 232872
    iget-object v1, p0, LX/1Ka;->j:LX/1Iu;

    .line 232873
    iget-object v2, v1, LX/1Iu;->a:Ljava/lang/Object;

    move-object v1, v2

    .line 232874
    check-cast v1, LX/0qq;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getCacheIds()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/0qq;->d(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 232875
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 232876
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 232877
    iget-object v1, p0, LX/1Ka;->j:LX/1Iu;

    .line 232878
    iget-object v3, v1, LX/1Iu;->a:Ljava/lang/Object;

    move-object v1, v3

    .line 232879
    check-cast v1, LX/0qq;

    invoke-virtual {v1, v0}, LX/0qq;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    goto :goto_1

    :cond_8
    move v10, v9

    move v9, v7

    goto/16 :goto_0

    :cond_9
    move v9, v7

    move v10, v7

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 232832
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 232836
    iget-object v0, p0, LX/1Ka;->f:LX/1Kb;

    .line 232837
    iget-object v1, v0, LX/1Kb;->a:LX/0bH;

    iget-object p0, v0, LX/1Kb;->c:LX/1Kc;

    invoke-virtual {v1, p0}, LX/0b4;->a(LX/0b2;)Z

    .line 232838
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 232833
    iget-object v0, p0, LX/1Ka;->f:LX/1Kb;

    .line 232834
    iget-object v1, v0, LX/1Kb;->a:LX/0bH;

    iget-object p0, v0, LX/1Kb;->c:LX/1Kc;

    invoke-virtual {v1, p0}, LX/0b4;->b(LX/0b2;)Z

    .line 232835
    return-void
.end method
