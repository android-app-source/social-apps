.class public LX/1q5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1q2;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 329994
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 329995
    iput-object p1, p0, LX/1q5;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 329996
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1q5;->b:Z

    .line 329997
    return-void
.end method


# virtual methods
.method public final a(JZ)V
    .locals 1

    .prologue
    .line 329998
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1q5;->b:Z

    .line 329999
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 330000
    iget-boolean v0, p0, LX/1q5;->b:Z

    return v0
.end method

.method public final a(J)Z
    .locals 7

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x1

    .line 330001
    iget-object v1, p0, LX/1q5;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/1kp;->g:LX/0Tn;

    invoke-interface {v1, v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 330002
    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 330003
    sub-long v2, p1, v2

    .line 330004
    const-wide/32 v4, 0x493e0

    cmp-long v1, v4, v2

    if-gez v1, :cond_1

    .line 330005
    :cond_0
    :goto_0
    return v0

    .line 330006
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(J)V
    .locals 3

    .prologue
    .line 330007
    iget-object v0, p0, LX/1q5;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1kp;->g:LX/0Tn;

    invoke-interface {v0, v1, p1, p2}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 330008
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1q5;->b:Z

    .line 330009
    return-void
.end method
