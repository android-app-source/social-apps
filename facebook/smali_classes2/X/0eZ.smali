.class public LX/0eZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0eZ;


# instance fields
.field private final a:LX/0WC;

.field public final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0WC;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 92192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92193
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/0eZ;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 92194
    iput-object p1, p0, LX/0eZ;->a:LX/0WC;

    .line 92195
    return-void
.end method

.method public static a(LX/0QB;)LX/0eZ;
    .locals 4

    .prologue
    .line 92176
    sget-object v0, LX/0eZ;->c:LX/0eZ;

    if-nez v0, :cond_1

    .line 92177
    const-class v1, LX/0eZ;

    monitor-enter v1

    .line 92178
    :try_start_0
    sget-object v0, LX/0eZ;->c:LX/0eZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 92179
    if-eqz v2, :cond_0

    .line 92180
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 92181
    new-instance p0, LX/0eZ;

    invoke-static {v0}, LX/0WA;->a(LX/0QB;)LX/0WA;

    move-result-object v3

    check-cast v3, LX/0WC;

    invoke-direct {p0, v3}, LX/0eZ;-><init>(LX/0WC;)V

    .line 92182
    move-object v0, p0

    .line 92183
    sput-object v0, LX/0eZ;->c:LX/0eZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 92184
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 92185
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 92186
    :cond_1
    sget-object v0, LX/0eZ;->c:LX/0eZ;

    return-object v0

    .line 92187
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 92188
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/0ea;Ljava/util/Locale;)Z
    .locals 2

    .prologue
    .line 92191
    iget-object v0, p0, LX/0eZ;->a:LX/0WC;

    invoke-virtual {v0, p1}, LX/0WC;->a(LX/0ea;)LX/0Rf;

    move-result-object v0

    invoke-virtual {p2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/util/Locale;)Z
    .locals 1

    .prologue
    .line 92190
    sget-object v0, LX/0ea;->ASSET:LX/0ea;

    invoke-direct {p0, v0, p1}, LX/0eZ;->a(LX/0ea;Ljava/util/Locale;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/util/Locale;)Z
    .locals 1

    .prologue
    .line 92189
    sget-object v0, LX/0ea;->DOWNLOAD:LX/0ea;

    invoke-direct {p0, v0, p1}, LX/0eZ;->a(LX/0ea;Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0eZ;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
