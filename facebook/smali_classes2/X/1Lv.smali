.class public LX/1Lv;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:J

.field public static c:J


# instance fields
.field public final A:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;"
        }
    .end annotation
.end field

.field private final B:LX/0ad;

.field public C:Z

.field public final D:LX/0tQ;

.field private E:LX/36v;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final F:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "LX/375;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final G:LX/1M1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1M1",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private H:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mReentrantLock"
    .end annotation
.end field

.field public final I:Ljava/lang/Runnable;

.field public final J:Ljava/lang/Runnable;

.field public final d:LX/1Ln;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ln",
            "<",
            "LX/37C;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/1AA;

.field public final f:LX/1Lg;

.field public final g:LX/0TD;

.field public final h:LX/03V;

.field private final i:LX/0ka;

.field public final j:LX/1Lr;

.field public final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0yH;

.field public final m:Landroid/os/Handler;

.field public final n:LX/1Lt;

.field public final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0A1;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/1Lw;

.field public final q:LX/1FQ;

.field public final r:LX/1Lu;

.field public final s:Ljava/util/concurrent/locks/ReentrantLock;

.field public final t:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final u:LX/16V;

.field public final v:LX/0So;

.field private final w:LX/11i;

.field public final x:LX/0WJ;

.field public final y:LX/0lC;

.field public final z:LX/19s;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 234561
    const-class v0, LX/1Lv;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1Lv;->a:Ljava/lang/String;

    .line 234562
    sput-wide v2, LX/1Lv;->b:J

    .line 234563
    sput-wide v2, LX/1Lv;->c:J

    return-void
.end method

.method public constructor <init>(LX/1Ln;LX/1AA;LX/1Lg;LX/0TD;LX/03V;LX/0ka;LX/1Lr;LX/0Or;LX/0yH;Landroid/os/Handler;LX/1Lt;LX/0Ot;LX/1Lw;LX/0So;LX/11i;LX/0WJ;LX/0lC;LX/19s;LX/1Lu;LX/0Ot;LX/0ad;LX/0tQ;)V
    .locals 7
    .param p10    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ln",
            "<",
            "LX/37C;",
            ">;",
            "LX/1AA;",
            "LX/1Lg;",
            "LX/0TD;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0ka;",
            "LX/1Lr;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0yH;",
            "Landroid/os/Handler;",
            "Lcom/facebook/video/server/VideoKeyCreator;",
            "LX/0Ot",
            "<",
            "LX/0A1;",
            ">;",
            "Lcom/facebook/video/server/NetworkProcessor;",
            "LX/0So;",
            "LX/11i;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/0lC;",
            "LX/19s;",
            "LX/1Lu;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;",
            "LX/0ad;",
            "LX/0tQ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 234525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 234526
    new-instance v1, Ljava/util/concurrent/locks/ReentrantLock;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/locks/ReentrantLock;-><init>(Z)V

    iput-object v1, p0, LX/1Lv;->s:Ljava/util/concurrent/locks/ReentrantLock;

    .line 234527
    new-instance v1, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$1;

    invoke-direct {v1, p0}, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$1;-><init>(LX/1Lv;)V

    iput-object v1, p0, LX/1Lv;->I:Ljava/lang/Runnable;

    .line 234528
    new-instance v1, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;

    invoke-direct {v1, p0}, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;-><init>(LX/1Lv;)V

    iput-object v1, p0, LX/1Lv;->J:Ljava/lang/Runnable;

    .line 234529
    iput-object p1, p0, LX/1Lv;->d:LX/1Ln;

    .line 234530
    iput-object p2, p0, LX/1Lv;->e:LX/1AA;

    .line 234531
    iput-object p3, p0, LX/1Lv;->f:LX/1Lg;

    .line 234532
    iput-object p4, p0, LX/1Lv;->g:LX/0TD;

    .line 234533
    iput-object p5, p0, LX/1Lv;->h:LX/03V;

    .line 234534
    iput-object p6, p0, LX/1Lv;->i:LX/0ka;

    .line 234535
    iput-object p7, p0, LX/1Lv;->j:LX/1Lr;

    .line 234536
    iput-object p8, p0, LX/1Lv;->k:LX/0Or;

    .line 234537
    move-object/from16 v0, p9

    iput-object v0, p0, LX/1Lv;->l:LX/0yH;

    .line 234538
    move-object/from16 v0, p10

    iput-object v0, p0, LX/1Lv;->m:Landroid/os/Handler;

    .line 234539
    move-object/from16 v0, p11

    iput-object v0, p0, LX/1Lv;->n:LX/1Lt;

    .line 234540
    iget-object v1, p0, LX/1Lv;->f:LX/1Lg;

    new-instance v2, LX/1Lx;

    invoke-direct {v2, p0}, LX/1Lx;-><init>(LX/1Lv;)V

    invoke-virtual {v1, v2}, LX/1Lg;->a(LX/1Lx;)V

    .line 234541
    iget v1, p7, LX/1Lr;->b:I

    iput v1, p0, LX/1Lv;->H:I

    .line 234542
    move-object/from16 v0, p12

    iput-object v0, p0, LX/1Lv;->o:LX/0Ot;

    .line 234543
    move-object/from16 v0, p13

    iput-object v0, p0, LX/1Lv;->p:LX/1Lw;

    .line 234544
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1Lv;->w:LX/11i;

    .line 234545
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1Lv;->x:LX/0WJ;

    .line 234546
    move-object/from16 v0, p17

    iput-object v0, p0, LX/1Lv;->y:LX/0lC;

    .line 234547
    new-instance v1, LX/16V;

    invoke-direct {v1}, LX/16V;-><init>()V

    iput-object v1, p0, LX/1Lv;->u:LX/16V;

    .line 234548
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, LX/1Lv;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 234549
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1Lv;->v:LX/0So;

    .line 234550
    invoke-static {}, LX/1Ly;->g()LX/1Ly;

    move-result-object v1

    iput-object v1, p0, LX/1Lv;->G:LX/1M1;

    .line 234551
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, LX/1Lv;->F:Ljava/util/Map;

    .line 234552
    move-object/from16 v0, p19

    iput-object v0, p0, LX/1Lv;->r:LX/1Lu;

    .line 234553
    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    .line 234554
    const v2, 0x32000

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 234555
    new-instance v2, LX/1FQ;

    invoke-static {}, LX/1IX;->a()LX/1IX;

    move-result-object v3

    new-instance v4, LX/1F7;

    const v5, 0x32000

    const v6, 0x7fffffff

    invoke-direct {v4, v5, v6, v1}, LX/1F7;-><init>(IILandroid/util/SparseIntArray;)V

    invoke-static {}, LX/1FT;->a()LX/1FT;

    move-result-object v1

    invoke-direct {v2, v3, v4, v1}, LX/1FQ;-><init>(LX/0rb;LX/1F7;LX/1F0;)V

    iput-object v2, p0, LX/1Lv;->q:LX/1FQ;

    .line 234556
    move-object/from16 v0, p18

    iput-object v0, p0, LX/1Lv;->z:LX/19s;

    .line 234557
    move-object/from16 v0, p20

    iput-object v0, p0, LX/1Lv;->A:LX/0Ot;

    .line 234558
    move-object/from16 v0, p21

    iput-object v0, p0, LX/1Lv;->B:LX/0ad;

    .line 234559
    move-object/from16 v0, p22

    iput-object v0, p0, LX/1Lv;->D:LX/0tQ;

    .line 234560
    return-void
.end method

.method public static a$redex0(LX/1Lv;LX/374;)I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 234512
    iget-object v0, p1, LX/374;->b:LX/36s;

    move-object v3, v0

    .line 234513
    iget-object v0, v3, LX/36s;->h:LX/36t;

    move-object v0, v0

    .line 234514
    sget-object v4, LX/36t;->DASH:LX/36t;

    if-ne v0, v4, :cond_0

    move v0, v1

    .line 234515
    :goto_0
    iget-object v4, p0, LX/1Lv;->i:LX/0ka;

    invoke-virtual {v4}, LX/0ka;->b()Z

    move-result v4

    .line 234516
    iget-object v5, p0, LX/1Lv;->j:LX/1Lr;

    invoke-virtual {v5, v4, v0}, LX/1Lr;->a(ZZ)LX/377;

    move-result-object v0

    .line 234517
    iget-boolean v4, v0, LX/377;->e:Z

    if-nez v4, :cond_1

    iget-boolean v4, p1, LX/374;->a:Z

    if-eqz v4, :cond_1

    .line 234518
    :goto_1
    return v2

    :cond_0
    move v0, v2

    .line 234519
    goto :goto_0

    .line 234520
    :cond_1
    iget-boolean v4, v0, LX/377;->c:Z

    if-eqz v4, :cond_2

    iget v4, v0, LX/377;->d:I

    if-lez v4, :cond_2

    iget-boolean v4, p1, LX/374;->a:Z

    if-nez v4, :cond_2

    .line 234521
    :goto_2
    if-eqz v1, :cond_3

    .line 234522
    iget v1, v0, LX/377;->d:I

    int-to-long v4, v1

    iget v1, v0, LX/377;->a:I

    iget v0, v0, LX/377;->b:I

    invoke-virtual {v3, v4, v5, v1, v0}, LX/36s;->a(JII)I

    move-result v2

    goto :goto_1

    :cond_2
    move v1, v2

    .line 234523
    goto :goto_2

    .line 234524
    :cond_3
    iget v2, v0, LX/377;->b:I

    goto :goto_1
.end method

.method public static declared-synchronized a$redex0(LX/1Lv;LX/375;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 234506
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/1Lv;->F:Ljava/util/Map;

    iget-object v2, p1, LX/375;->b:Landroid/net/Uri;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 234507
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 234508
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/1Lv;->G:LX/1M1;

    iget-object v2, p1, LX/375;->b:Landroid/net/Uri;

    invoke-interface {v1, v2}, LX/1M1;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 234509
    iget-object v0, p0, LX/1Lv;->F:Ljava/util/Map;

    iget-object v1, p1, LX/375;->b:Landroid/net/Uri;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 234510
    const/4 v0, 0x1

    goto :goto_0

    .line 234511
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 234505
    sget-object v0, LX/1Lv;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static declared-synchronized b$redex0(LX/1Lv;LX/375;)V
    .locals 2

    .prologue
    .line 234502
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Lv;->F:Ljava/util/Map;

    iget-object v1, p1, LX/375;->b:Landroid/net/Uri;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234503
    monitor-exit p0

    return-void

    .line 234504
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized e(LX/1Lv;)V
    .locals 4

    .prologue
    .line 234457
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Lv;->E:LX/36v;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Lv;->w:LX/11i;

    if-eqz v0, :cond_0

    .line 234458
    new-instance v0, LX/36v;

    iget-object v1, p0, LX/1Lv;->w:LX/11i;

    invoke-direct {v0, v1}, LX/36v;-><init>(LX/11i;)V

    iput-object v0, p0, LX/1Lv;->E:LX/36v;

    .line 234459
    iget-object v0, p0, LX/1Lv;->E:LX/36v;

    iget-object v1, p0, LX/1Lv;->u:LX/16V;

    .line 234460
    const-class v2, LX/36x;

    iget-object v3, v0, LX/36v;->b:LX/36w;

    invoke-virtual {v1, v2, v3}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 234461
    const-class v2, LX/36z;

    iget-object v3, v0, LX/36v;->b:LX/36w;

    invoke-virtual {v1, v2, v3}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 234462
    const-class v2, LX/370;

    iget-object v3, v0, LX/36v;->b:LX/36w;

    invoke-virtual {v1, v2, v3}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 234463
    const-class v2, LX/371;

    iget-object v3, v0, LX/36v;->b:LX/36w;

    invoke-virtual {v1, v2, v3}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 234464
    const-class v2, LX/372;

    iget-object v3, v0, LX/36v;->b:LX/36w;

    invoke-virtual {v1, v2, v3}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 234465
    const-class v2, LX/373;

    iget-object v3, v0, LX/36v;->b:LX/36w;

    invoke-virtual {v1, v2, v3}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234466
    :cond_0
    monitor-exit p0

    return-void

    .line 234467
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static g(LX/1Lv;)Z
    .locals 3

    .prologue
    .line 234495
    iget-object v0, p0, LX/1Lv;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 234496
    :try_start_0
    iget v0, p0, LX/1Lv;->H:I

    if-gtz v0, :cond_0

    .line 234497
    new-instance v0, Ljava/io/InterruptedIOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Prefetch interrupted in tryHoldThread(), idleThreads = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/1Lv;->H:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234498
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1Lv;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 234499
    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    .line 234500
    iget v0, p0, LX/1Lv;->H:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1Lv;->H:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 234501
    iget-object v0, p0, LX/1Lv;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    const/4 v0, 0x1

    return v0
.end method

.method public static h(LX/1Lv;)V
    .locals 2

    .prologue
    .line 234489
    iget-object v0, p0, LX/1Lv;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 234490
    :try_start_0
    iget v0, p0, LX/1Lv;->H:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1Lv;->H:I

    .line 234491
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234492
    iget-object v0, p0, LX/1Lv;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 234493
    return-void

    .line 234494
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1Lv;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public static j(LX/1Lv;)Z
    .locals 2

    .prologue
    .line 234484
    iget-object v0, p0, LX/1Lv;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 234485
    :try_start_0
    iget v0, p0, LX/1Lv;->H:I

    if-lez v0, :cond_0

    iget-object v0, p0, LX/1Lv;->f:LX/1Lg;

    invoke-virtual {v0}, LX/1Lg;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 234486
    :goto_0
    iget-object v1, p0, LX/1Lv;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v0

    .line 234487
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 234488
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1Lv;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/net/Uri;Z)LX/7Ou;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 234472
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Lv;->G:LX/1M1;

    invoke-interface {v0, p1}, LX/1M1;->add(Ljava/lang/Object;)Z

    .line 234473
    iget-object v0, p0, LX/1Lv;->G:LX/1M1;

    invoke-interface {v0, p1}, LX/1M1;->a(Ljava/lang/Object;)I

    .line 234474
    iget-object v0, p0, LX/1Lv;->j:LX/1Lr;

    iget-boolean v0, v0, LX/1Lr;->f:Z

    if-eqz v0, :cond_0

    .line 234475
    iget-object v0, p0, LX/1Lv;->f:LX/1Lg;

    invoke-virtual {v0, p1}, LX/1Lg;->a(Landroid/net/Uri;)V

    .line 234476
    :cond_0
    iget-object v0, p0, LX/1Lv;->F:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/375;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234477
    if-nez v0, :cond_1

    move-object v0, v1

    .line 234478
    :goto_0
    monitor-exit p0

    return-object v0

    .line 234479
    :cond_1
    :try_start_1
    invoke-virtual {v0, p2}, LX/375;->a(Z)LX/7Ou;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 234480
    :catch_0
    move-exception v0

    .line 234481
    :try_start_2
    sget-object v2, LX/1Lv;->a:Ljava/lang/String;

    const-string v3, "Error getting the intercepted reader"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, v1

    .line 234482
    goto :goto_0

    .line 234483
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 234468
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Lv;->G:LX/1M1;

    invoke-interface {v0, p1}, LX/1M1;->remove(Ljava/lang/Object;)Z

    .line 234469
    iget-object v0, p0, LX/1Lv;->G:LX/1M1;

    invoke-interface {v0, p1}, LX/1M1;->a(Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234470
    monitor-exit p0

    return-void

    .line 234471
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
