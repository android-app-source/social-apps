.class public LX/0km;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 127846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127847
    return-void
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127829
    packed-switch p0, :pswitch_data_0

    .line 127830
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 127831
    :pswitch_0
    const-string v0, "1xRTT"

    goto :goto_0

    .line 127832
    :pswitch_1
    const-string v0, "CDMA"

    goto :goto_0

    .line 127833
    :pswitch_2
    const-string v0, "EDGE"

    goto :goto_0

    .line 127834
    :pswitch_3
    const-string v0, "EHRPD"

    goto :goto_0

    .line 127835
    :pswitch_4
    const-string v0, "EVDO_0"

    goto :goto_0

    .line 127836
    :pswitch_5
    const-string v0, "EVDO_A"

    goto :goto_0

    .line 127837
    :pswitch_6
    const-string v0, "EVDO_B"

    goto :goto_0

    .line 127838
    :pswitch_7
    const-string v0, "GPRS"

    goto :goto_0

    .line 127839
    :pswitch_8
    const-string v0, "HSDPA"

    goto :goto_0

    .line 127840
    :pswitch_9
    const-string v0, "HSPA"

    goto :goto_0

    .line 127841
    :pswitch_a
    const-string v0, "HSPAP"

    goto :goto_0

    .line 127842
    :pswitch_b
    const-string v0, "HSUPA"

    goto :goto_0

    .line 127843
    :pswitch_c
    const-string v0, "IDEN"

    goto :goto_0

    .line 127844
    :pswitch_d
    const-string v0, "LTE"

    goto :goto_0

    .line 127845
    :pswitch_e
    const-string v0, "UMTS"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_2
        :pswitch_e
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_8
        :pswitch_b
        :pswitch_9
        :pswitch_c
        :pswitch_6
        :pswitch_d
        :pswitch_3
        :pswitch_a
    .end packed-switch
.end method

.method public static a(Landroid/telephony/TelephonyManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127826
    :try_start_0
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getPhoneType()I
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 127827
    :goto_0
    move v0, v0

    .line 127828
    invoke-static {v0}, LX/0km;->b(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(II)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 127848
    if-ne p0, v1, :cond_1

    .line 127849
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 127850
    :cond_1
    if-nez p0, :cond_0

    .line 127851
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    move v0, v1

    .line 127852
    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 127853
    goto :goto_0

    :pswitch_3
    move v0, v1

    .line 127854
    goto :goto_0

    :pswitch_4
    move v0, v1

    .line 127855
    goto :goto_0

    :pswitch_5
    move v0, v1

    .line 127856
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127807
    packed-switch p0, :pswitch_data_0

    .line 127808
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 127809
    :pswitch_0
    const-string v0, "CDMA"

    goto :goto_0

    .line 127810
    :pswitch_1
    const-string v0, "GSM"

    goto :goto_0

    .line 127811
    :pswitch_2
    const-string v0, "SIP"

    goto :goto_0

    .line 127812
    :pswitch_3
    const-string v0, "NONE"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127813
    packed-switch p0, :pswitch_data_0

    .line 127814
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 127815
    :pswitch_0
    const-string v0, "ABSENT"

    goto :goto_0

    .line 127816
    :pswitch_1
    const-string v0, "PIN_REQUIRED"

    goto :goto_0

    .line 127817
    :pswitch_2
    const-string v0, "PUK_REQUIRED"

    goto :goto_0

    .line 127818
    :pswitch_3
    const-string v0, "NETWORK_LOCKED"

    goto :goto_0

    .line 127819
    :pswitch_4
    const-string v0, "READY"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static d(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127820
    packed-switch p0, :pswitch_data_0

    .line 127821
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 127822
    :pswitch_0
    const-string v0, "DATA_DISCONNECTED"

    goto :goto_0

    .line 127823
    :pswitch_1
    const-string v0, "DATA_CONNECTING"

    goto :goto_0

    .line 127824
    :pswitch_2
    const-string v0, "DATA_CONNECTED"

    goto :goto_0

    .line 127825
    :pswitch_3
    const-string v0, "DATA_SUSPENDED"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
