.class public final LX/1ee;
.super LX/1eP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1eP",
        "<",
        "LX/1FL;",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/1Fh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1bh;


# direct methods
.method public constructor <init>(LX/1cd;LX/1Fh;LX/1bh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;",
            "LX/1bh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 288682
    invoke-direct {p0, p1}, LX/1eP;-><init>(LX/1cd;)V

    .line 288683
    iput-object p2, p0, LX/1ee;->a:LX/1Fh;

    .line 288684
    iput-object p3, p0, LX/1ee;->b:LX/1bh;

    .line 288685
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Z)V
    .locals 4

    .prologue
    .line 288686
    check-cast p1, LX/1FL;

    const/4 v3, 0x1

    .line 288687
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 288688
    :cond_0
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 288689
    invoke-virtual {v0, p1, p2}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 288690
    :goto_0
    return-void

    .line 288691
    :cond_1
    invoke-virtual {p1}, LX/1FL;->a()LX/1FJ;

    move-result-object v1

    .line 288692
    if-eqz v1, :cond_3

    .line 288693
    :try_start_0
    iget-object v0, p1, LX/1FL;->i:LX/1bh;

    move-object v0, v0

    .line 288694
    if-eqz v0, :cond_2

    .line 288695
    iget-object v0, p1, LX/1FL;->i:LX/1bh;

    move-object v0, v0

    .line 288696
    :goto_1
    iget-object v2, p0, LX/1ee;->a:LX/1Fh;

    invoke-interface {v2, v0, v1}, LX/1Fh;->a(Ljava/lang/Object;LX/1FJ;)LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 288697
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    .line 288698
    if-eqz v2, :cond_3

    .line 288699
    :try_start_1
    new-instance v1, LX/1FL;

    invoke-direct {v1, v2}, LX/1FL;-><init>(LX/1FJ;)V

    .line 288700
    invoke-virtual {v1, p1}, LX/1FL;->b(LX/1FL;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 288701
    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V

    .line 288702
    :try_start_2
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 288703
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, LX/1cd;->b(F)V

    .line 288704
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 288705
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/1cd;->b(Ljava/lang/Object;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 288706
    invoke-static {v1}, LX/1FL;->d(LX/1FL;)V

    goto :goto_0

    .line 288707
    :cond_2
    :try_start_3
    iget-object v0, p0, LX/1ee;->b:LX/1bh;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 288708
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0

    .line 288709
    :catchall_1
    move-exception v0

    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V

    throw v0

    .line 288710
    :catchall_2
    move-exception v0

    invoke-static {v1}, LX/1FL;->d(LX/1FL;)V

    throw v0

    .line 288711
    :cond_3
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 288712
    invoke-virtual {v0, p1, v3}, LX/1cd;->b(Ljava/lang/Object;Z)V

    goto :goto_0
.end method
