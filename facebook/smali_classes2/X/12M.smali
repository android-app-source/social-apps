.class public LX/12M;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/12O;",
        "LX/4g2;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/12M;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 175351
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 175352
    return-void
.end method

.method public static a(LX/0QB;)LX/12M;
    .locals 3

    .prologue
    .line 175353
    sget-object v0, LX/12M;->a:LX/12M;

    if-nez v0, :cond_1

    .line 175354
    const-class v1, LX/12M;

    monitor-enter v1

    .line 175355
    :try_start_0
    sget-object v0, LX/12M;->a:LX/12M;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 175356
    if-eqz v2, :cond_0

    .line 175357
    :try_start_1
    new-instance v0, LX/12M;

    invoke-direct {v0}, LX/12M;-><init>()V

    .line 175358
    move-object v0, v0

    .line 175359
    sput-object v0, LX/12M;->a:LX/12M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175360
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 175361
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 175362
    :cond_1
    sget-object v0, LX/12M;->a:LX/12M;

    return-object v0

    .line 175363
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 175364
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
