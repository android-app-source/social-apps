.class public final LX/1jn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 300870
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1jm;Ljava/lang/String;LX/15w;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 300871
    const-string v2, "name"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 300872
    invoke-virtual {p2}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v2, v3, :cond_0

    :goto_0
    iput-object v0, p0, LX/1jm;->a:Ljava/lang/String;

    move v0, v1

    .line 300873
    :goto_1
    return v0

    .line 300874
    :cond_0
    invoke-virtual {p2}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 300875
    :cond_1
    const-string v2, "cctype"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 300876
    invoke-virtual {p2}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v2, v3, :cond_2

    :goto_2
    iput-object v0, p0, LX/1jm;->b:Ljava/lang/String;

    move v0, v1

    .line 300877
    goto :goto_1

    .line 300878
    :cond_2
    invoke-virtual {p2}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 300879
    :cond_3
    const-string v2, "version"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 300880
    invoke-virtual {p2}, LX/15w;->E()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/1jm;->c:Ljava/lang/Integer;

    move v0, v1

    .line 300881
    goto :goto_1

    .line 300882
    :cond_4
    const-string v2, "sample_rate"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 300883
    invoke-virtual {p2}, LX/15w;->E()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/1jm;->d:Ljava/lang/Integer;

    move v0, v1

    .line 300884
    goto :goto_1

    .line 300885
    :cond_5
    const-string v2, "policy_id"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 300886
    invoke-virtual {p2}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v2, v3, :cond_6

    :goto_3
    iput-object v0, p0, LX/1jm;->e:Ljava/lang/String;

    move v0, v1

    .line 300887
    goto :goto_1

    .line 300888
    :cond_6
    invoke-virtual {p2}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 300889
    :cond_7
    const/4 v0, 0x0

    goto :goto_1
.end method
