.class public final enum LX/103;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/103;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/103;

.field public static final enum COMMERCE:LX/103;

.field public static final enum EVENT:LX/103;

.field public static final enum GROUP:LX/103;

.field public static final enum MARKETPLACE:LX/103;

.field public static final enum PAGE:LX/103;

.field public static final enum URL:LX/103;

.field public static final enum USER:LX/103;

.field public static final enum VIDEO:LX/103;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 168330
    new-instance v0, LX/103;

    const-string v1, "URL"

    const-string v2, "url"

    invoke-direct {v0, v1, v4, v2}, LX/103;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/103;->URL:LX/103;

    .line 168331
    new-instance v0, LX/103;

    const-string v1, "USER"

    const-string v2, "user"

    invoke-direct {v0, v1, v5, v2}, LX/103;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/103;->USER:LX/103;

    .line 168332
    new-instance v0, LX/103;

    const-string v1, "GROUP"

    const-string v2, "group"

    invoke-direct {v0, v1, v6, v2}, LX/103;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/103;->GROUP:LX/103;

    .line 168333
    new-instance v0, LX/103;

    const-string v1, "PAGE"

    const-string v2, "page"

    invoke-direct {v0, v1, v7, v2}, LX/103;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/103;->PAGE:LX/103;

    .line 168334
    new-instance v0, LX/103;

    const-string v1, "EVENT"

    const-string v2, "event"

    invoke-direct {v0, v1, v8, v2}, LX/103;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/103;->EVENT:LX/103;

    .line 168335
    new-instance v0, LX/103;

    const-string v1, "VIDEO"

    const/4 v2, 0x5

    const-string v3, "video"

    invoke-direct {v0, v1, v2, v3}, LX/103;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/103;->VIDEO:LX/103;

    .line 168336
    new-instance v0, LX/103;

    const-string v1, "MARKETPLACE"

    const/4 v2, 0x6

    const-string v3, "marketplace"

    invoke-direct {v0, v1, v2, v3}, LX/103;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/103;->MARKETPLACE:LX/103;

    .line 168337
    new-instance v0, LX/103;

    const-string v1, "COMMERCE"

    const/4 v2, 0x7

    const-string v3, "commerce"

    invoke-direct {v0, v1, v2, v3}, LX/103;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/103;->COMMERCE:LX/103;

    .line 168338
    const/16 v0, 0x8

    new-array v0, v0, [LX/103;

    sget-object v1, LX/103;->URL:LX/103;

    aput-object v1, v0, v4

    sget-object v1, LX/103;->USER:LX/103;

    aput-object v1, v0, v5

    sget-object v1, LX/103;->GROUP:LX/103;

    aput-object v1, v0, v6

    sget-object v1, LX/103;->PAGE:LX/103;

    aput-object v1, v0, v7

    sget-object v1, LX/103;->EVENT:LX/103;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/103;->VIDEO:LX/103;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/103;->MARKETPLACE:LX/103;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/103;->COMMERCE:LX/103;

    aput-object v2, v0, v1

    sput-object v0, LX/103;->$VALUES:[LX/103;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 168324
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 168325
    iput-object p3, p0, LX/103;->name:Ljava/lang/String;

    .line 168326
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/103;
    .locals 1

    .prologue
    .line 168327
    const-class v0, LX/103;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/103;

    return-object v0
.end method

.method public static values()[LX/103;
    .locals 1

    .prologue
    .line 168328
    sget-object v0, LX/103;->$VALUES:[LX/103;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/103;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168329
    iget-object v0, p0, LX/103;->name:Ljava/lang/String;

    return-object v0
.end method
