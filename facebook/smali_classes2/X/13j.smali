.class public LX/13j;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile l:LX/13j;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0yc;

.field public f:Landroid/app/Activity;

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/dialtone/activitylistener/DialtoneActivityListener$ActivityChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private k:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0yc;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;)V
    .locals 2
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/gk/IsDialtoneModeSelectionAvailableGK;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/IsDialtoneManualSwitcherFeatureAvailable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0yc;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 177170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177171
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/13j;->g:Z

    .line 177172
    iput-boolean v1, p0, LX/13j;->h:Z

    .line 177173
    iput-boolean v1, p0, LX/13j;->i:Z

    .line 177174
    iput v1, p0, LX/13j;->k:I

    .line 177175
    invoke-virtual {p1}, LX/0yc;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/13j;->h:Z

    .line 177176
    iput-object p2, p0, LX/13j;->c:LX/0Ot;

    .line 177177
    iput-object p3, p0, LX/13j;->d:LX/0Ot;

    .line 177178
    iput-object p4, p0, LX/13j;->a:LX/0Or;

    .line 177179
    iput-object p5, p0, LX/13j;->b:LX/0Or;

    .line 177180
    iput-object p1, p0, LX/13j;->e:LX/0yc;

    .line 177181
    iget-object v0, p0, LX/13j;->e:LX/0yc;

    invoke-virtual {v0, p0}, LX/0yc;->a(LX/13j;)V

    .line 177182
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/13j;->j:Ljava/util/Set;

    .line 177183
    return-void
.end method

.method public static a(LX/0QB;)LX/13j;
    .locals 9

    .prologue
    .line 177157
    sget-object v0, LX/13j;->l:LX/13j;

    if-nez v0, :cond_1

    .line 177158
    const-class v1, LX/13j;

    monitor-enter v1

    .line 177159
    :try_start_0
    sget-object v0, LX/13j;->l:LX/13j;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 177160
    if-eqz v2, :cond_0

    .line 177161
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 177162
    new-instance v3, LX/13j;

    invoke-static {v0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v4

    check-cast v4, LX/0yc;

    const/16 v5, 0x455

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1ce

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x148b

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x1486

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/13j;-><init>(LX/0yc;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;)V

    .line 177163
    move-object v0, v3

    .line 177164
    sput-object v0, LX/13j;->l:LX/13j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177165
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 177166
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 177167
    :cond_1
    sget-object v0, LX/13j;->l:LX/13j;

    return-object v0

    .line 177168
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 177169
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static g(LX/13j;)Z
    .locals 1

    .prologue
    .line 177156
    iget-object v0, p0, LX/13j;->e:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/13j;->g:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, LX/13j;->h:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/13j;->i:Z

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, LX/13j;->j()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized j()Z
    .locals 1

    .prologue
    .line 177155
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/13j;->k:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 177142
    monitor-enter p0

    .line 177143
    :try_start_0
    iget-object v0, p0, LX/13j;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, LX/13j;->g(LX/13j;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 177144
    if-eqz v0, :cond_1

    .line 177145
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;

    invoke-direct {v1, p1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 177146
    const/high16 v0, 0x4000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 177147
    iget-object v0, p0, LX/13j;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 177148
    :cond_0
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/13j;->i:Z

    .line 177149
    iget v0, p0, LX/13j;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/13j;->k:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177150
    monitor-exit p0

    return-void

    .line 177151
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/13j;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p0}, LX/13j;->g(LX/13j;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, LX/13j;->h:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 177152
    if-eqz v0, :cond_0

    .line 177153
    iget-object v0, p0, LX/13j;->e:LX/0yc;

    invoke-virtual {v0, p1}, LX/0yc;->a(Landroid/content/Context;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 177154
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    const/4 v0, 0x0

    goto :goto_0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 177141
    iget-object v0, p0, LX/13j;->e:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/13j;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/13j;->f:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/13j;->f:Landroid/app/Activity;

    instance-of v0, v0, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/13j;->f:Landroid/app/Activity;

    instance-of v0, v0, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 177128
    iput-object p1, p0, LX/13j;->f:Landroid/app/Activity;

    .line 177129
    iget-object v0, p0, LX/13j;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Mx;

    .line 177130
    iget-object p0, v0, LX/5Mx;->a:LX/0yb;

    iget-object p0, p0, LX/0yb;->x:LX/0am;

    invoke-virtual {p0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/13j;

    .line 177131
    iget-object p1, p0, LX/13j;->j:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 177132
    iget-object p0, v0, LX/5Mx;->a:LX/0yb;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/0yc;->a(Z)Z

    .line 177133
    goto :goto_0

    .line 177134
    :cond_0
    return-void
.end method

.method public final declared-synchronized f()V
    .locals 1

    .prologue
    .line 177135
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/13j;->g:Z

    .line 177136
    iget v0, p0, LX/13j;->k:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/13j;->k:I

    .line 177137
    iget v0, p0, LX/13j;->k:I

    if-gtz v0, :cond_0

    .line 177138
    iget-object v0, p0, LX/13j;->e:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/13j;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177139
    :cond_0
    monitor-exit p0

    return-void

    .line 177140
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
