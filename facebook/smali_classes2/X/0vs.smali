.class public final LX/0vs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0vt;


# instance fields
.field public final synthetic a:LX/0vn;

.field public final synthetic b:LX/0vo;


# direct methods
.method public constructor <init>(LX/0vo;LX/0vn;)V
    .locals 0

    .prologue
    .line 158200
    iput-object p1, p0, LX/0vs;->b:LX/0vo;

    iput-object p2, p0, LX/0vs;->a:LX/0vn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 158194
    iget-object v0, p0, LX/0vs;->a:LX/0vn;

    invoke-virtual {v0, p1}, LX/0vn;->a(Landroid/view/View;)LX/3t3;

    move-result-object v0

    .line 158195
    if-eqz v0, :cond_0

    .line 158196
    iget-object p0, v0, LX/3t3;->b:Ljava/lang/Object;

    move-object v0, p0

    .line 158197
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 158198
    iget-object v0, p0, LX/0vs;->a:LX/0vn;

    invoke-virtual {v0, p1, p2}, LX/0vn;->a(Landroid/view/View;I)V

    .line 158199
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 158191
    iget-object v0, p0, LX/0vs;->a:LX/0vn;

    new-instance v1, LX/3sp;

    invoke-direct {v1, p2}, LX/3sp;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v1}, LX/0vn;->a(Landroid/view/View;LX/3sp;)V

    .line 158192
    return-void
.end method

.method public final a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 158193
    iget-object v0, p0, LX/0vs;->a:LX/0vn;

    invoke-virtual {v0, p1, p2, p3}, LX/0vn;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 158190
    iget-object v0, p0, LX/0vs;->a:LX/0vn;

    invoke-virtual {v0, p1, p2}, LX/0vn;->b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 158189
    iget-object v0, p0, LX/0vs;->a:LX/0vn;

    invoke-virtual {v0, p1, p2, p3}, LX/0vn;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 158183
    iget-object v0, p0, LX/0vs;->a:LX/0vn;

    invoke-virtual {v0, p1, p2}, LX/0vn;->d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 158184
    return-void
.end method

.method public final c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 158187
    iget-object v0, p0, LX/0vs;->a:LX/0vn;

    invoke-virtual {v0, p1, p2}, LX/0vn;->c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 158188
    return-void
.end method

.method public final d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 158185
    iget-object v0, p0, LX/0vs;->a:LX/0vn;

    invoke-virtual {v0, p1, p2}, LX/0vn;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 158186
    return-void
.end method
