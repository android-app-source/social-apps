.class public LX/1hi;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static volatile e:LX/03V;


# instance fields
.field private A:Lcom/facebook/tigon/tigonapi/TigonRequestToken;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/http/interfaces/RequestPriority;

.field private f:B

.field private g:B

.field private h:I

.field private i:I

.field public final j:Lorg/apache/http/client/ResponseHandler;

.field private final k:Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;

.field private final l:Lcom/google/common/util/concurrent/SettableFuture;

.field private final m:LX/1AI;

.field private final n:LX/15F;

.field public final o:LX/1iN;

.field private final p:Z

.field private final q:Z

.field private final r:I

.field private final s:Ljava/lang/String;

.field private final t:I

.field private u:Ljava/lang/Object;

.field private v:Ljava/lang/Throwable;

.field private w:LX/1pK;

.field private x:I

.field private y:I

.field public z:LX/1iM;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILorg/apache/http/client/ResponseHandler;Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;LX/15F;Lcom/google/common/util/concurrent/SettableFuture;LX/1AI;ILjava/lang/String;ILjava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;ZZ)V
    .locals 3

    .prologue
    .line 296676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296677
    const/4 v1, 0x0

    iput v1, p0, LX/1hi;->h:I

    .line 296678
    iput-object p1, p0, LX/1hi;->a:Ljava/lang/String;

    .line 296679
    iput p2, p0, LX/1hi;->b:I

    .line 296680
    iput-object p3, p0, LX/1hi;->j:Lorg/apache/http/client/ResponseHandler;

    .line 296681
    iput-object p4, p0, LX/1hi;->k:Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;

    .line 296682
    iput-object p5, p0, LX/1hi;->n:LX/15F;

    .line 296683
    iput-object p6, p0, LX/1hi;->l:Lcom/google/common/util/concurrent/SettableFuture;

    .line 296684
    iput-object p7, p0, LX/1hi;->m:LX/1AI;

    .line 296685
    move/from16 v0, p13

    iput-boolean v0, p0, LX/1hi;->p:Z

    .line 296686
    move/from16 v0, p14

    iput-boolean v0, p0, LX/1hi;->q:Z

    .line 296687
    iput p8, p0, LX/1hi;->r:I

    .line 296688
    iput-object p9, p0, LX/1hi;->s:Ljava/lang/String;

    .line 296689
    iput p10, p0, LX/1hi;->t:I

    .line 296690
    iput-object p11, p0, LX/1hi;->c:Ljava/lang/String;

    .line 296691
    iput-object p12, p0, LX/1hi;->d:Lcom/facebook/http/interfaces/RequestPriority;

    .line 296692
    const/4 v1, 0x0

    iput-byte v1, p0, LX/1hi;->f:B

    .line 296693
    const/16 v1, 0xa

    iput-byte v1, p0, LX/1hi;->g:B

    .line 296694
    const/4 v1, 0x0

    iput v1, p0, LX/1hi;->i:I

    .line 296695
    new-instance v1, LX/1iM;

    iget v2, p0, LX/1hi;->b:I

    invoke-direct {v1, p7, v2}, LX/1iM;-><init>(LX/1AI;I)V

    iput-object v1, p0, LX/1hi;->z:LX/1iM;

    .line 296696
    new-instance v1, LX/1iN;

    invoke-direct {v1, p0}, LX/1iN;-><init>(LX/1hi;)V

    iput-object v1, p0, LX/1hi;->o:LX/1iN;

    .line 296697
    return-void
.end method

.method private a(LX/16j;)V
    .locals 3

    .prologue
    .line 296647
    iget-object v0, p0, LX/1hi;->l:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0}, LX/0SQ;->isDone()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 296648
    iget v0, p0, LX/1hi;->b:I

    invoke-virtual {p1, v0}, LX/16j;->b(I)V

    .line 296649
    iget-object v0, p0, LX/1hi;->o:LX/1iN;

    invoke-virtual {v0}, LX/1iN;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 296650
    const-string v0, "Not completed successfully"

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {p0, v0, v1, v2}, LX/1hi;->a(Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 296651
    :cond_0
    return-void
.end method

.method private static a(LX/1hi;ILX/16j;Ljava/lang/Object;)V
    .locals 3
    .param p2    # LX/16j;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 296652
    invoke-direct {p0}, LX/1hi;->n()V

    .line 296653
    iget-object v0, p0, LX/1hi;->l:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, 0x1e13cc78

    invoke-static {v0, p3, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 296654
    invoke-static {p0, p1}, LX/1hi;->b(LX/1hi;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296655
    iget-object v0, p0, LX/1hi;->k:Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;

    iget-object v1, p0, LX/1hi;->w:LX/1pK;

    .line 296656
    iget-object v2, v0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->i:Lorg/apache/http/HttpResponse;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 296657
    iget-object v2, v0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->h:Ljava/util/Set;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 296658
    const-string v2, "done"

    invoke-static {v0, v1, v2}, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a(Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;LX/1pK;Ljava/lang/String;)V

    .line 296659
    iget-object v2, v0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->h:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1iZ;

    .line 296660
    iget-object p3, v0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->i:Lorg/apache/http/HttpResponse;

    invoke-interface {v2, p3, v0}, LX/1iZ;->b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V

    goto :goto_0

    .line 296661
    :cond_0
    invoke-direct {p0, p2}, LX/1hi;->a(LX/16j;)V

    .line 296662
    return-void
.end method

.method private static a(LX/1hi;ILX/16j;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 296663
    invoke-direct {p0}, LX/1hi;->n()V

    .line 296664
    iget-boolean v0, p0, LX/1hi;->q:Z

    if-eqz v0, :cond_1

    invoke-static {p3}, LX/1hi;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 296665
    iget-object v0, p0, LX/1hi;->l:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0SQ;->cancel(Z)Z

    .line 296666
    :goto_0
    invoke-static {p0, p1}, LX/1hi;->b(LX/1hi;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296667
    instance-of v0, p3, Ljava/io/IOException;

    if-eqz v0, :cond_2

    check-cast p3, Ljava/io/IOException;

    .line 296668
    :goto_1
    iget-object v0, p0, LX/1hi;->k:Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;

    iget-object v1, p0, LX/1hi;->w:LX/1pK;

    .line 296669
    iget-object p1, v0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->h:Ljava/util/Set;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 296670
    invoke-static {p3}, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a(Ljava/io/IOException;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a(Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;LX/1pK;Ljava/lang/String;)V

    .line 296671
    invoke-static {v0, p3}, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->b(Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;Ljava/io/IOException;)V

    .line 296672
    :cond_0
    invoke-direct {p0, p2}, LX/1hi;->a(LX/16j;)V

    .line 296673
    return-void

    .line 296674
    :cond_1
    iget-object v0, p0, LX/1hi;->l:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p3}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0

    .line 296675
    :cond_2
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, p3}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    move-object p3, v0

    goto :goto_1
.end method

.method private static a(LX/1hi;LX/16j;ILjava/lang/Object;Ljava/lang/Exception;)V
    .locals 2
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 296614
    invoke-static {p0, p2}, LX/1hi;->b(LX/1hi;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 296615
    :goto_0
    return-void

    .line 296616
    :cond_0
    if-eqz p4, :cond_1

    const/4 v0, 0x3

    :goto_1
    iput-byte v0, p0, LX/1hi;->f:B

    .line 296617
    iget-byte v0, p0, LX/1hi;->g:B

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    iget-byte v0, p0, LX/1hi;->g:B

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    .line 296618
    iput-object p3, p0, LX/1hi;->u:Ljava/lang/Object;

    .line 296619
    iput-object p4, p0, LX/1hi;->v:Ljava/lang/Throwable;

    goto :goto_0

    .line 296620
    :cond_1
    const/4 v0, 0x2

    goto :goto_1

    .line 296621
    :cond_2
    if-nez p4, :cond_3

    .line 296622
    invoke-static {p0, p2, p1, p3}, LX/1hi;->a(LX/1hi;ILX/16j;Ljava/lang/Object;)V

    goto :goto_0

    .line 296623
    :cond_3
    invoke-static {p0, p2, p1, p4}, LX/1hi;->a(LX/1hi;ILX/16j;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p1    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 296698
    sget-object v0, LX/1hi;->e:LX/03V;

    if-eqz v0, :cond_0

    .line 296699
    sget-object v0, LX/1hi;->e:LX/03V;

    const-string v1, "tigon"

    invoke-virtual {v0, v1, p0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 296700
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Throwable;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 296701
    instance-of v2, p0, LX/2BH;

    if-eqz v2, :cond_1

    .line 296702
    check-cast p0, LX/2BH;

    .line 296703
    iget-object v2, p0, LX/2BH;->tigonError:Lcom/facebook/tigon/tigonapi/TigonError;

    iget v2, v2, Lcom/facebook/tigon/tigonapi/TigonError;->a:I

    if-ne v2, v0, :cond_0

    .line 296704
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 296705
    goto :goto_0

    :cond_1
    move v0, v1

    .line 296706
    goto :goto_0
.end method

.method private static b(LX/1hi;I)Z
    .locals 1

    .prologue
    .line 296707
    iget v0, p0, LX/1hi;->i:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()V
    .locals 4

    .prologue
    .line 296708
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 296709
    const-string v0, "tigon_handlers"

    iget v2, p0, LX/1hi;->x:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296710
    const-string v0, "tigon_running_handlers"

    iget v2, p0, LX/1hi;->y:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296711
    const-string v0, "tigon_request_states"

    iget v2, p0, LX/1hi;->r:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296712
    const-string v0, "tigon_request_provider_type"

    iget-object v2, p0, LX/1hi;->s:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296713
    const-string v0, "tigon_request_size"

    iget v2, p0, LX/1hi;->t:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296714
    iget-object v0, p0, LX/1hi;->w:LX/1pK;

    if-eqz v0, :cond_1

    .line 296715
    iget-object v0, p0, LX/1hi;->w:LX/1pK;

    sget-object v2, LX/1pY;->a:LX/1pZ;

    invoke-virtual {v0, v2}, LX/1pK;->a(LX/1pZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pH;

    .line 296716
    if-eqz v0, :cond_0

    .line 296717
    const-string v2, "tigon_redirects"

    .line 296718
    iget v3, v0, LX/1pH;->i:I

    move v0, v3

    .line 296719
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296720
    :cond_0
    iget-object v0, p0, LX/1hi;->w:LX/1pK;

    sget-object v2, LX/1pY;->e:LX/1pZ;

    invoke-virtual {v0, v2}, LX/1pK;->a(LX/1pZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pJ;

    .line 296721
    if-eqz v0, :cond_1

    .line 296722
    const-string v2, "tigon_queue_initial_concurrency"

    .line 296723
    iget v3, v0, LX/1pJ;->a:I

    move v3, v3

    .line 296724
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296725
    const-string v2, "tigon_queue_final_concurrency"

    .line 296726
    iget v3, v0, LX/1pJ;->b:I

    move v3, v3

    .line 296727
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296728
    const-string v2, "tigon_queue_initial_queue_size"

    .line 296729
    iget v3, v0, LX/1pJ;->c:I

    move v3, v3

    .line 296730
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296731
    const-string v2, "tigon_queue_final_queue_size"

    .line 296732
    iget v3, v0, LX/1pJ;->d:I

    move v0, v3

    .line 296733
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296734
    :cond_1
    iget-object v0, p0, LX/1hi;->z:LX/1iM;

    if-eqz v0, :cond_2

    .line 296735
    const-string v0, "tigon_body_buffer_total_size"

    iget-object v2, p0, LX/1hi;->z:LX/1iM;

    invoke-virtual {v2}, LX/1iM;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296736
    const-string v0, "tigon_body_buffer_max_cached_size"

    iget-object v2, p0, LX/1hi;->z:LX/1iM;

    invoke-virtual {v2}, LX/1iM;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296737
    :cond_2
    iget-object v0, p0, LX/1hi;->w:LX/1pK;

    sget-object v2, LX/1pY;->c:LX/1pZ;

    invoke-virtual {v0, v2}, LX/1pK;->a(LX/1pZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pI;

    .line 296738
    if-eqz v0, :cond_3

    .line 296739
    const-string v2, "tigon_response_cmp_size"

    .line 296740
    iget v3, v0, LX/1pI;->i:I

    move v3, v3

    .line 296741
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296742
    const-string v2, "tigon_response_size"

    .line 296743
    iget v3, v0, LX/1pI;->h:I

    move v0, v3

    .line 296744
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296745
    :cond_3
    iget-object v0, p0, LX/1hi;->n:LX/15F;

    .line 296746
    iput-object v1, v0, LX/15F;->f:Ljava/util/Map;

    .line 296747
    return-void
.end method

.method private o()Z
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 296748
    iget-object v2, p0, LX/1hi;->o:LX/1iN;

    invoke-virtual {v2}, LX/1iN;->c()B

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 296749
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 296750
    :pswitch_0
    iget-byte v2, p0, LX/1hi;->f:B

    if-nez v2, :cond_1

    .line 296751
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 296752
    goto :goto_0

    .line 296753
    :pswitch_1
    iget-byte v2, p0, LX/1hi;->f:B

    if-eq v2, v0, :cond_0

    move v0, v1

    goto :goto_0

    .line 296754
    :pswitch_2
    iget-byte v2, p0, LX/1hi;->f:B

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 296755
    :pswitch_3
    iget-byte v2, p0, LX/1hi;->f:B

    if-eq v2, v4, :cond_0

    move v0, v1

    goto :goto_0

    .line 296756
    :pswitch_4
    iget-byte v2, p0, LX/1hi;->f:B

    if-eqz v2, :cond_0

    iget-byte v2, p0, LX/1hi;->f:B

    if-eq v2, v3, :cond_0

    iget-byte v2, p0, LX/1hi;->f:B

    if-eq v2, v4, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_5
    move v0, v1

    .line 296757
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private p()Z
    .locals 5

    .prologue
    const/16 v4, 0xc

    const/16 v3, 0xb

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 296758
    iget-object v2, p0, LX/1hi;->o:LX/1iN;

    invoke-virtual {v2}, LX/1iN;->e()B

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 296759
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 296760
    :pswitch_0
    iget-byte v2, p0, LX/1hi;->g:B

    const/16 v3, 0xa

    if-ne v2, v3, :cond_1

    .line 296761
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 296762
    goto :goto_0

    .line 296763
    :pswitch_1
    iget-byte v2, p0, LX/1hi;->g:B

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 296764
    :pswitch_2
    iget-byte v2, p0, LX/1hi;->g:B

    if-eq v2, v4, :cond_0

    move v0, v1

    goto :goto_0

    .line 296765
    :pswitch_3
    iget-byte v2, p0, LX/1hi;->g:B

    if-eq v2, v3, :cond_0

    iget-byte v2, p0, LX/1hi;->g:B

    if-eq v2, v4, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_4
    move v0, v1

    .line 296766
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static q(LX/1hi;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 296767
    iget v0, p0, LX/1hi;->h:I

    if-nez v0, :cond_0

    invoke-direct {p0}, LX/1hi;->o()Z

    move-result v0

    if-nez v0, :cond_0

    .line 296768
    const-string v0, "Handler state mismatch (%d, %s)"

    iget-byte v1, p0, LX/1hi;->f:B

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    iget-object v2, p0, LX/1hi;->o:LX/1iN;

    invoke-virtual {v2}, LX/1iN;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 296769
    invoke-virtual {p0, v0, v4, v3}, LX/1hi;->a(Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 296770
    :cond_0
    iget v0, p0, LX/1hi;->h:I

    if-nez v0, :cond_1

    invoke-direct {p0}, LX/1hi;->p()Z

    move-result v0

    if-nez v0, :cond_1

    .line 296771
    const-string v0, "Request state mismatch (%d,%s)"

    iget-byte v1, p0, LX/1hi;->g:B

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    iget-object v2, p0, LX/1hi;->o:LX/1iN;

    invoke-virtual {v2}, LX/1iN;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 296772
    invoke-virtual {p0, v0, v4, v3}, LX/1hi;->a(Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 296773
    :cond_1
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/1pB;)Lorg/apache/http/HttpResponse;
    .locals 10

    .prologue
    .line 296774
    monitor-enter p0

    .line 296775
    :try_start_0
    iget v1, p1, LX/1pB;->a:I

    .line 296776
    sget-object v2, LX/1pC;->a:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 296777
    if-eqz v2, :cond_7

    .line 296778
    :goto_0
    move-object v1, v2

    .line 296779
    new-instance v2, Lorg/apache/http/message/BasicHttpResponse;

    sget-object v3, Lorg/apache/http/HttpVersion;->HTTP_1_1:Lorg/apache/http/HttpVersion;

    iget v4, p1, LX/1pB;->a:I

    invoke-direct {v2, v3, v4, v1}, Lorg/apache/http/message/BasicHttpResponse;-><init>(Lorg/apache/http/ProtocolVersion;ILjava/lang/String;)V

    .line 296780
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p1, LX/1pB;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296781
    iget-object v1, p1, LX/1pB;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 296782
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Lorg/apache/http/message/BasicHttpResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 296783
    goto :goto_1

    .line 296784
    :cond_0
    move-object v0, v2

    .line 296785
    iget-object v1, p0, LX/1hi;->k:Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;

    .line 296786
    iget-object v2, v1, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->h:Ljava/util/Set;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 296787
    iput-object v0, v1, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->i:Lorg/apache/http/HttpResponse;

    .line 296788
    iget-object v2, v1, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->h:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1iZ;

    .line 296789
    iget-object v4, v1, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->i:Lorg/apache/http/HttpResponse;

    invoke-interface {v2, v4, v1}, LX/1iZ;->a(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V

    goto :goto_2

    .line 296790
    :cond_1
    iget-object v2, p0, LX/1hi;->z:LX/1iM;

    move-object v2, v2

    .line 296791
    iget-object v3, v1, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->i:Lorg/apache/http/HttpResponse;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 296792
    iget-object v3, v1, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->h:Ljava/util/Set;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 296793
    iget-object v3, v1, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->h:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1iZ;

    .line 296794
    instance-of v5, v3, LX/1pD;

    if-eqz v5, :cond_2

    .line 296795
    check-cast v3, LX/1pD;

    .line 296796
    invoke-interface {v3}, LX/1pD;->a()Ljava/io/InputStream;

    move-result-object v2

    goto :goto_3

    .line 296797
    :cond_3
    move-object v1, v2

    .line 296798
    if-eqz v1, :cond_6

    .line 296799
    iget-object v6, p1, LX/1pB;->b:Ljava/util/Map;

    const-string v7, "Content-Length"

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 296800
    const-wide/16 v8, -0x1

    .line 296801
    if-eqz v6, :cond_8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296802
    :try_start_1
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    move-result-wide v6

    .line 296803
    :goto_4
    move-wide v3, v6

    .line 296804
    new-instance v5, Lorg/apache/http/entity/InputStreamEntity;

    invoke-direct {v5, v1, v3, v4}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .line 296805
    iget-object v3, p1, LX/1pB;->b:Ljava/util/Map;

    const-string v4, "Content-Encoding"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 296806
    if-eqz v3, :cond_4

    .line 296807
    invoke-virtual {v5, v3}, Lorg/apache/http/entity/InputStreamEntity;->setContentEncoding(Ljava/lang/String;)V

    .line 296808
    :cond_4
    iget-object v3, p1, LX/1pB;->b:Ljava/util/Map;

    const-string v4, "Content-Type"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 296809
    if-eqz v3, :cond_5

    .line 296810
    invoke-virtual {v5, v3}, Lorg/apache/http/entity/InputStreamEntity;->setContentType(Ljava/lang/String;)V

    .line 296811
    :cond_5
    move-object v1, v5

    .line 296812
    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 296813
    :cond_6
    monitor-exit p0

    return-object v0

    .line 296814
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_7
    :try_start_3
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_0
    :cond_8
    move-wide v6, v8

    goto :goto_4
.end method

.method public final declared-synchronized a(I)V
    .locals 1

    .prologue
    .line 296815
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/1hi;->b(LX/1hi;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296816
    iget-object v0, p0, LX/1hi;->o:LX/1iN;

    .line 296817
    const/16 p1, 0x8

    invoke-static {v0, p1}, LX/1iN;->a(LX/1iN;B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296818
    :cond_0
    monitor-exit p0

    return-void

    .line 296819
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(ILjava/lang/Exception;LX/16j;)V
    .locals 2

    .prologue
    .line 296820
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/1hi;->b(LX/1hi;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 296821
    :goto_0
    monitor-exit p0

    return-void

    .line 296822
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1hi;->o:LX/1iN;

    .line 296823
    const/4 v1, 0x2

    invoke-static {v0, v1}, LX/1iN;->b(LX/1iN;B)V

    .line 296824
    const/4 v0, 0x0

    invoke-static {p0, p3, p1, v0, p2}, LX/1hi;->a(LX/1hi;LX/16j;ILjava/lang/Object;Ljava/lang/Exception;)V

    .line 296825
    invoke-static {p0}, LX/1hi;->q(LX/1hi;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 296826
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(ILjava/lang/Object;LX/16j;)V
    .locals 2

    .prologue
    .line 296624
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/1hi;->b(LX/1hi;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 296625
    :goto_0
    monitor-exit p0

    return-void

    .line 296626
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1hi;->o:LX/1iN;

    .line 296627
    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/1iN;->b(LX/1iN;B)V

    .line 296628
    const/4 v0, 0x0

    invoke-static {p0, p3, p1, p2, v0}, LX/1hi;->a(LX/1hi;LX/16j;ILjava/lang/Object;Ljava/lang/Exception;)V

    .line 296629
    invoke-static {p0}, LX/1hi;->q(LX/1hi;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 296630
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/16j;LX/1pK;)V
    .locals 2

    .prologue
    .line 296631
    monitor-enter p0

    :try_start_0
    iget-byte v0, p0, LX/1hi;->g:B

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 296632
    const/16 v0, 0xb

    iput-byte v0, p0, LX/1hi;->g:B

    .line 296633
    iput-object p2, p0, LX/1hi;->w:LX/1pK;

    .line 296634
    iget-object v0, p0, LX/1hi;->o:LX/1iN;

    .line 296635
    const/4 v1, 0x4

    invoke-static {v0, v1}, LX/1iN;->b(LX/1iN;B)V

    .line 296636
    iget-byte v0, p0, LX/1hi;->f:B

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 296637
    iget-object v0, p0, LX/1hi;->z:LX/1iM;

    invoke-virtual {v0}, LX/1iM;->a()V

    .line 296638
    iget v0, p0, LX/1hi;->i:I

    iget-object v1, p0, LX/1hi;->u:Ljava/lang/Object;

    invoke-static {p0, v0, p1, v1}, LX/1hi;->a(LX/1hi;ILX/16j;Ljava/lang/Object;)V

    .line 296639
    :goto_1
    invoke-static {p0}, LX/1hi;->q(LX/1hi;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296640
    monitor-exit p0

    return-void

    .line 296641
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 296642
    :cond_1
    :try_start_1
    iget-byte v0, p0, LX/1hi;->f:B

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 296643
    iget-object v0, p0, LX/1hi;->z:LX/1iM;

    iget-object v1, p0, LX/1hi;->v:Ljava/lang/Throwable;

    invoke-virtual {v0, v1}, LX/1iM;->a(Ljava/lang/Throwable;)V

    .line 296644
    iget v0, p0, LX/1hi;->i:I

    iget-object v1, p0, LX/1hi;->v:Ljava/lang/Throwable;

    invoke-static {p0, v0, p1, v1}, LX/1hi;->a(LX/1hi;ILX/16j;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 296645
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 296646
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/1hi;->z:LX/1iM;

    invoke-virtual {v0}, LX/1iM;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized a(LX/16j;Lcom/facebook/tigon/tigonapi/TigonError;LX/1pK;)V
    .locals 2

    .prologue
    .line 296585
    monitor-enter p0

    :try_start_0
    iget-byte v0, p0, LX/1hi;->g:B

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 296586
    iget-object v0, p0, LX/1hi;->o:LX/1iN;

    .line 296587
    const/4 v1, 0x5

    invoke-static {v0, v1}, LX/1iN;->b(LX/1iN;B)V

    .line 296588
    const/16 v0, 0xc

    iput-byte v0, p0, LX/1hi;->g:B

    .line 296589
    iput-object p3, p0, LX/1hi;->w:LX/1pK;

    .line 296590
    new-instance v0, LX/2BH;

    invoke-direct {v0, p2}, LX/2BH;-><init>(Lcom/facebook/tigon/tigonapi/TigonError;)V

    .line 296591
    iget-byte v1, p0, LX/1hi;->f:B

    packed-switch v1, :pswitch_data_0

    .line 296592
    iget-object v1, p0, LX/1hi;->v:Ljava/lang/Throwable;

    if-eqz v1, :cond_0

    iget-object v0, p0, LX/1hi;->v:Ljava/lang/Throwable;

    .line 296593
    :cond_0
    iget-object v1, p0, LX/1hi;->z:LX/1iM;

    invoke-virtual {v1, v0}, LX/1iM;->a(Ljava/lang/Throwable;)V

    .line 296594
    iget v1, p0, LX/1hi;->i:I

    invoke-static {p0, v1, p1, v0}, LX/1hi;->a(LX/1hi;ILX/16j;Ljava/lang/Throwable;)V

    .line 296595
    :goto_1
    invoke-static {p0}, LX/1hi;->q(LX/1hi;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296596
    monitor-exit p0

    return-void

    .line 296597
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 296598
    :pswitch_0
    :try_start_1
    iget-object v1, p0, LX/1hi;->z:LX/1iM;

    invoke-virtual {v1, v0}, LX/1iM;->a(Ljava/lang/Throwable;)V

    .line 296599
    iput-object v0, p0, LX/1hi;->v:Ljava/lang/Throwable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 296600
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 296601
    :pswitch_1
    :try_start_2
    iget-object v0, p0, LX/1hi;->z:LX/1iM;

    invoke-virtual {v0}, LX/1iM;->a()V

    .line 296602
    iget v0, p0, LX/1hi;->i:I

    iget-object v1, p0, LX/1hi;->u:Ljava/lang/Object;

    invoke-static {p0, v0, p1, v1}, LX/1hi;->a(LX/1hi;ILX/16j;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final declared-synchronized a(Lcom/facebook/tigon/iface/TigonRequest;)V
    .locals 8

    .prologue
    .line 296573
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1hi;->o:LX/1iN;

    .line 296574
    const/4 p1, 0x7

    invoke-static {v0, p1}, LX/1iN;->a(LX/1iN;B)V

    .line 296575
    iget-object v0, p0, LX/1hi;->k:Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;

    .line 296576
    iget-object v1, v0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 296577
    iget-object v1, v0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->h:Ljava/util/Set;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 296578
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->j:Z

    .line 296579
    iget-object v1, v0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    iget-object v2, v0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-static {v2}, LX/1jS;->a(Lorg/apache/http/HttpRequest;)J

    move-result-wide v5

    sub-long/2addr v3, v5

    .line 296580
    iput-wide v3, v1, LX/1iW;->a:J

    .line 296581
    iget-object v1, v0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->h:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1iZ;

    .line 296582
    iget-object v3, v0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v1, v3, v0}, LX/1iZ;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)V

    goto :goto_0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296583
    :cond_0
    monitor-exit p0

    return-void

    .line 296584
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/tigon/tigonapi/TigonError;LX/1pK;)V
    .locals 9

    .prologue
    .line 296546
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1hi;->k:Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;

    iget v1, p0, LX/1hi;->i:I

    const/4 v4, 0x0

    const/4 v3, 0x1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296547
    :try_start_1
    iget v2, v0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->e:I

    if-ne v1, v2, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 296548
    invoke-static {p1}, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a(Lcom/facebook/tigon/tigonapi/TigonError;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, p2, v2}, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a(Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;LX/1pK;Ljava/lang/String;)V

    .line 296549
    new-instance v2, LX/2BH;

    invoke-direct {v2, p1}, LX/2BH;-><init>(Lcom/facebook/tigon/tigonapi/TigonError;)V

    invoke-static {v0, v2}, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->b(Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;Ljava/io/IOException;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 296550
    :goto_1
    :try_start_2
    iget v2, v0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->e:I

    .line 296551
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->i:Lorg/apache/http/HttpResponse;

    .line 296552
    invoke-static {v0}, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->b(Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;)V

    .line 296553
    iget-object v0, p0, LX/1hi;->o:LX/1iN;

    .line 296554
    const/4 v1, 0x3

    invoke-static {v0, v1}, LX/1iN;->b(LX/1iN;B)V

    .line 296555
    const/4 v0, 0x0

    iput-byte v0, p0, LX/1hi;->f:B

    .line 296556
    const/16 v0, 0xa

    iput-byte v0, p0, LX/1hi;->g:B

    .line 296557
    const/4 v0, 0x0

    iput-object v0, p0, LX/1hi;->u:Ljava/lang/Object;

    .line 296558
    const/4 v0, 0x0

    iput-object v0, p0, LX/1hi;->v:Ljava/lang/Throwable;

    .line 296559
    iget v0, p0, LX/1hi;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1hi;->i:I

    .line 296560
    iget-object v0, p0, LX/1hi;->z:LX/1iM;

    invoke-virtual {v0}, LX/1iM;->a()V

    .line 296561
    new-instance v0, LX/1iM;

    iget-object v1, p0, LX/1hi;->m:LX/1AI;

    iget v2, p0, LX/1hi;->b:I

    invoke-direct {v0, v1, v2}, LX/1iM;-><init>(LX/1AI;I)V

    iput-object v0, p0, LX/1hi;->z:LX/1iM;

    .line 296562
    const/4 v0, 0x0

    iput v0, p0, LX/1hi;->x:I

    .line 296563
    const/4 v0, 0x0

    iput v0, p0, LX/1hi;->y:I

    .line 296564
    invoke-static {p0}, LX/1hi;->q(LX/1hi;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 296565
    monitor-exit p0

    return-void

    .line 296566
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move v2, v4

    .line 296567
    goto :goto_0

    .line 296568
    :catch_0
    move-exception v2

    .line 296569
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "att:%d/%d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    iget v4, v0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v3

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 296570
    iget-object v5, v0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a:LX/1hd;

    .line 296571
    iget-object v6, v5, LX/1hd;->i:LX/03V;

    move-object v5, v6

    .line 296572
    const-string v6, "Tigon retry state"

    invoke-virtual {v5, v6, v4, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    goto :goto_1
.end method

.method public final declared-synchronized a(Lcom/facebook/tigon/tigonapi/TigonRequestToken;)V
    .locals 1
    .param p1    # Lcom/facebook/tigon/tigonapi/TigonRequestToken;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 296543
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/1hi;->A:Lcom/facebook/tigon/tigonapi/TigonRequestToken;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296544
    monitor-exit p0

    return-void

    .line 296545
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Throwable;I)V
    .locals 4
    .param p2    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 296508
    iget v0, p0, LX/1hi;->h:I

    if-lt v0, p3, :cond_0

    .line 296509
    :goto_0
    return-void

    .line 296510
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " seq="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/1hi;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1hi;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " handler="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1hi;->j:Lorg/apache/http/client/ResponseHandler;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " buffer=("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1hi;->z:LX/1iM;

    const/16 p3, 0x20

    .line 296511
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 296512
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object p1

    iget-boolean v2, v1, LX/1iM;->k:Z

    if-eqz v2, :cond_2

    const-string v2, "waiting"

    :goto_1
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296513
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object p1

    iget-boolean v2, v1, LX/1iM;->h:Z

    if-eqz v2, :cond_3

    const-string v2, "outputOpen"

    :goto_2
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296514
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object p1

    iget-boolean v2, v1, LX/1iM;->l:Z

    if-eqz v2, :cond_4

    const-string v2, "clientOpen"

    :goto_3
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296515
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 296516
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ) state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1hi;->o:LX/1iN;

    invoke-virtual {v1}, LX/1iN;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 296517
    const-string v1, "TigonHttpClientAdapter"

    invoke-static {v1, v0, p2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 296518
    invoke-static {v0, p2}, LX/1hi;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 296519
    iget-boolean v1, p0, LX/1hi;->p:Z

    if-eqz v1, :cond_1

    .line 296520
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 296521
    :cond_1
    iget v0, p0, LX/1hi;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1hi;->h:I

    goto/16 :goto_0

    .line 296522
    :cond_2
    const-string v2, "notWaiting"

    goto :goto_1

    .line 296523
    :cond_3
    const-string v2, "outputCLosed"

    goto :goto_2

    .line 296524
    :cond_4
    const-string v2, "clientClosed"

    goto :goto_3
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 296507
    monitor-enter p0

    :try_start_0
    iget-byte v0, p0, LX/1hi;->f:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(III)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 296526
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/1hi;->b(LX/1hi;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v2

    if-nez v2, :cond_0

    .line 296527
    :goto_0
    monitor-exit p0

    return v0

    .line 296528
    :cond_0
    :try_start_1
    iput p2, p0, LX/1hi;->x:I

    .line 296529
    iput p3, p0, LX/1hi;->y:I

    .line 296530
    iget-object v2, p0, LX/1hi;->o:LX/1iN;

    .line 296531
    const/4 v3, 0x0

    invoke-static {v2, v3}, LX/1iN;->b(LX/1iN;B)V

    .line 296532
    iget-byte v2, p0, LX/1hi;->g:B

    const/16 v3, 0xc

    if-ne v2, v3, :cond_1

    .line 296533
    const/4 v2, 0x3

    iput-byte v2, p0, LX/1hi;->f:B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 296534
    :cond_1
    :try_start_2
    iget-byte v2, p0, LX/1hi;->f:B

    packed-switch v2, :pswitch_data_0

    .line 296535
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 296536
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-static {p0}, LX/1hi;->q(LX/1hi;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 296537
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 296538
    :pswitch_1
    const/4 v0, 0x1

    :try_start_4
    iput-byte v0, p0, LX/1hi;->f:B
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 296539
    :try_start_5
    invoke-static {p0}, LX/1hi;->q(LX/1hi;)V

    move v0, v1

    goto :goto_0

    :pswitch_2
    invoke-static {p0}, LX/1hi;->q(LX/1hi;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final declared-synchronized b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 296525
    monitor-enter p0

    :try_start_0
    iget-byte v1, p0, LX/1hi;->f:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v0, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 2

    .prologue
    .line 296603
    monitor-enter p0

    :try_start_0
    iget-byte v0, p0, LX/1hi;->f:B

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-byte v0, p0, LX/1hi;->f:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()I
    .locals 1

    .prologue
    .line 296604
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/1hi;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final f()Lcom/facebook/tigon/tigonapi/TigonRequestToken;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 296605
    iget-object v0, p0, LX/1hi;->A:Lcom/facebook/tigon/tigonapi/TigonRequestToken;

    return-object v0
.end method

.method public final g()Lcom/facebook/tigon/tigonapi/TigonRequestToken;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 296606
    iget-object v0, p0, LX/1hi;->A:Lcom/facebook/tigon/tigonapi/TigonRequestToken;

    if-nez v0, :cond_0

    .line 296607
    const-string v0, "Tokenless state"

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/1hi;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 296608
    :cond_0
    iget-object v0, p0, LX/1hi;->A:Lcom/facebook/tigon/tigonapi/TigonRequestToken;

    return-object v0
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 296609
    iget-object v0, p0, LX/1hi;->o:LX/1iN;

    invoke-virtual {v0}, LX/1iN;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public final declared-synchronized i()LX/1iM;
    .locals 1

    .prologue
    .line 296610
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1hi;->z:LX/1iM;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 296611
    iget-object v0, p0, LX/1hi;->o:LX/1iN;

    .line 296612
    const/16 p0, 0x9

    invoke-static {v0, p0}, LX/1iN;->a(LX/1iN;B)V

    .line 296613
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v2, 0x27

    .line 296540
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TigonRequestState{mHandlerState="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-byte v1, p0, LX/1hi;->f:B

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mRequestState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v1, p0, LX/1hi;->g:B

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mReportedCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/1hi;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTicket="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/1hi;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1hi;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSequence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/1hi;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mResponseHandler="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1hi;->j:Lorg/apache/http/client/ResponseHandler;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mFuture="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1hi;->l:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mThrowOnStateFailures="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/1hi;->p:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1hi;->u:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mException="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1hi;->v:Ljava/lang/Throwable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTerminalSummary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1hi;->w:LX/1pK;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mBodyBuffer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1hi;->z:LX/1iM;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", requestToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1hi;->A:Lcom/facebook/tigon/tigonapi/TigonRequestToken;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 296541
    iget-object v1, p0, LX/1hi;->a:Ljava/lang/String;

    move-object v1, v1

    .line 296542
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", creationTimeNanos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/1hi;->h()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
