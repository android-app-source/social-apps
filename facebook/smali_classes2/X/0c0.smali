.class public final enum LX/0c0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0c0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0c0;

.field public static final enum Cached:LX/0c0;

.field public static final enum Live:LX/0c0;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 87567
    new-instance v0, LX/0c0;

    const-string v1, "Live"

    invoke-direct {v0, v1, v2}, LX/0c0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0c0;->Live:LX/0c0;

    .line 87568
    new-instance v0, LX/0c0;

    const-string v1, "Cached"

    invoke-direct {v0, v1, v3}, LX/0c0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0c0;->Cached:LX/0c0;

    .line 87569
    const/4 v0, 0x2

    new-array v0, v0, [LX/0c0;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    aput-object v1, v0, v2

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    aput-object v1, v0, v3

    sput-object v0, LX/0c0;->$VALUES:[LX/0c0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 87570
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0c0;
    .locals 1

    .prologue
    .line 87571
    const-class v0, LX/0c0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0c0;

    return-object v0
.end method

.method public static values()[LX/0c0;
    .locals 1

    .prologue
    .line 87572
    sget-object v0, LX/0c0;->$VALUES:[LX/0c0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0c0;

    return-object v0
.end method
