.class public LX/11D;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0Zb;

.field public final c:Landroid/content/Context;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field private e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 170821
    const-class v0, LX/11D;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/11D;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Zb;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 170822
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170823
    const/4 v0, -0x1

    iput v0, p0, LX/11D;->e:I

    .line 170824
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/11D;->c:Landroid/content/Context;

    .line 170825
    iput-object p2, p0, LX/11D;->b:LX/0Zb;

    .line 170826
    iput-object p3, p0, LX/11D;->d:Ljava/util/concurrent/ExecutorService;

    .line 170827
    return-void
.end method

.method private a(LX/1Zs;)V
    .locals 3

    .prologue
    .line 170828
    iget-object v0, p0, LX/11D;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/funnellogger/beacon/BeaconLogger$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/funnellogger/beacon/BeaconLogger$1;-><init>(LX/11D;LX/1Zs;)V

    const v2, 0x69a54dbe

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 170829
    return-void
.end method


# virtual methods
.method public final a(LX/1Zs;LX/1Zr;)V
    .locals 2

    .prologue
    .line 170830
    iget-boolean v0, p2, LX/1Zr;->e:Z

    move v0, v0

    .line 170831
    if-eqz v0, :cond_1

    .line 170832
    :cond_0
    :goto_0
    return-void

    .line 170833
    :cond_1
    sget-object v0, LX/1Zs;->FUNNEL_STARTED:LX/1Zs;

    if-ne p1, v0, :cond_3

    .line 170834
    iget v0, p0, LX/11D;->e:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_2

    const/4 v0, 0x0

    :goto_1
    iput v0, p0, LX/11D;->e:I

    .line 170835
    iget v0, p0, LX/11D;->e:I

    rem-int/lit8 v0, v0, 0xb

    if-nez v0, :cond_0

    .line 170836
    invoke-static {p2}, LX/1Zr;->p(LX/1Zr;)V

    .line 170837
    const/4 v0, 0x1

    iput-boolean v0, p2, LX/1Zr;->i:Z

    .line 170838
    invoke-direct {p0, p1}, LX/11D;->a(LX/1Zs;)V

    goto :goto_0

    .line 170839
    :cond_2
    iget v0, p0, LX/11D;->e:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 170840
    :cond_3
    invoke-virtual {p2}, LX/1Zr;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170841
    invoke-direct {p0, p1}, LX/11D;->a(LX/1Zs;)V

    goto :goto_0
.end method
