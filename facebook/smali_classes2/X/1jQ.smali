.class public LX/1jQ;
.super LX/0eW;
.source ""


# static fields
.field public static final c:Ljava/nio/charset/Charset;

.field public static final d:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 300071
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, LX/1jQ;->c:Ljava/nio/charset/Charset;

    .line 300072
    const/16 v0, 0x40

    new-array v0, v0, [B

    sput-object v0, LX/1jQ;->d:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 300073
    invoke-direct {p0}, LX/0eW;-><init>()V

    return-void
.end method

.method public static a(Ljava/nio/ByteBuffer;I)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 300074
    invoke-virtual {p0, p1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    add-int v1, p1, v0

    .line 300075
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    .line 300076
    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v3

    .line 300077
    sget-object v0, LX/1jQ;->d:[B

    array-length v0, v0

    if-gt v3, v0, :cond_0

    .line 300078
    sget-object v0, LX/1jQ;->d:[B

    .line 300079
    :goto_0
    add-int/lit8 v1, v1, 0x4

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 300080
    invoke-virtual {p0, v0, v4, v3}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 300081
    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 300082
    new-instance v1, Ljava/lang/String;

    sget-object v2, LX/1jQ;->c:Ljava/nio/charset/Charset;

    invoke-direct {v1, v0, v4, v3, v2}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    return-object v1

    .line 300083
    :cond_0
    new-array v0, v3, [B

    goto :goto_0
.end method
