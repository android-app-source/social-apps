.class public LX/0YH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile j:LX/0YH;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/content/Context;

.field public final e:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
    .end annotation
.end field

.field public final f:LX/0YI;

.field public final g:LX/0YC;

.field public final h:LX/0SG;

.field public i:LX/0Yb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80871
    const-class v0, LX/0YH;

    sput-object v0, LX/0YH;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;Landroid/os/Handler;LX/0YI;LX/0YC;LX/0SG;)V
    .locals 0
    .param p2    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p4    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;",
            "Landroid/os/Handler;",
            "LX/0YI;",
            "LX/0YC;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 80833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80834
    iput-object p1, p0, LX/0YH;->d:Landroid/content/Context;

    .line 80835
    iput-object p2, p0, LX/0YH;->b:LX/0Ot;

    .line 80836
    iput-object p3, p0, LX/0YH;->c:LX/0Ot;

    .line 80837
    iput-object p4, p0, LX/0YH;->e:Landroid/os/Handler;

    .line 80838
    iput-object p5, p0, LX/0YH;->f:LX/0YI;

    .line 80839
    iput-object p6, p0, LX/0YH;->g:LX/0YC;

    .line 80840
    iput-object p7, p0, LX/0YH;->h:LX/0SG;

    .line 80841
    return-void
.end method

.method public static a(LX/0QB;)LX/0YH;
    .locals 11

    .prologue
    .line 80842
    sget-object v0, LX/0YH;->j:LX/0YH;

    if-nez v0, :cond_1

    .line 80843
    const-class v1, LX/0YH;

    monitor-enter v1

    .line 80844
    :try_start_0
    sget-object v0, LX/0YH;->j:LX/0YH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 80845
    if-eqz v2, :cond_0

    .line 80846
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 80847
    new-instance v3, LX/0YH;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x1ce

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2ca

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0Zw;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v7

    check-cast v7, Landroid/os/Handler;

    invoke-static {v0}, LX/0YI;->a(LX/0QB;)LX/0YI;

    move-result-object v8

    check-cast v8, LX/0YI;

    invoke-static {v0}, LX/0YC;->a(LX/0QB;)LX/0YC;

    move-result-object v9

    check-cast v9, LX/0YC;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-direct/range {v3 .. v10}, LX/0YH;-><init>(Landroid/content/Context;LX/0Ot;LX/0Ot;Landroid/os/Handler;LX/0YI;LX/0YC;LX/0SG;)V

    .line 80848
    move-object v0, v3

    .line 80849
    sput-object v0, LX/0YH;->j:LX/0YH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80850
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 80851
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 80852
    :cond_1
    sget-object v0, LX/0YH;->j:LX/0YH;

    return-object v0

    .line 80853
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 80854
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0YH;[Ljava/io/File;)Z
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 80855
    if-nez p1, :cond_1

    .line 80856
    :cond_0
    return v0

    .line 80857
    :cond_1
    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    .line 80858
    const/4 v5, 0x1

    .line 80859
    iget-object v6, p0, LX/0YH;->h:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v7

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v9

    sub-long/2addr v7, v9

    .line 80860
    const-wide/16 v9, 0x0

    cmp-long v6, v7, v9

    if-gez v6, :cond_5

    .line 80861
    :cond_2
    :goto_1
    move v4, v5

    .line 80862
    if-eqz v4, :cond_4

    .line 80863
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 80864
    iget-object v4, p0, LX/0YH;->f:LX/0YI;

    .line 80865
    invoke-virtual {v4, v3}, LX/0YI;->c(Ljava/io/File;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 80866
    iget-object v5, v4, LX/0YI;->a:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 80867
    :cond_3
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 80868
    :cond_4
    const/4 v0, 0x1

    goto :goto_2

    :cond_5
    const-wide/32 v9, 0x240c8400

    cmp-long v6, v7, v9

    if-gtz v6, :cond_2

    iget-object v6, p0, LX/0YH;->f:LX/0YI;

    invoke-virtual {v6, v3}, LX/0YI;->c(Ljava/io/File;)Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v5, 0x0

    goto :goto_1

    .line 80869
    :cond_6
    iget-object v4, p0, LX/0YH;->f:LX/0YI;

    invoke-virtual {v4, v3}, LX/0YI;->a(Ljava/io/File;)V

    .line 80870
    sget-object v4, LX/0YH;->a:Ljava/lang/Class;

    const-string v5, "Error: failed to delete traceFile %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public static d(LX/0YH;)V
    .locals 3

    .prologue
    .line 80806
    iget-object v0, p0, LX/0YH;->f:LX/0YI;

    invoke-virtual {v0}, LX/0YI;->b()V

    .line 80807
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/0YH;->d:Landroid/content/Context;

    const-class v2, Lcom/facebook/trace/DebugTraceUploadService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 80808
    iget-object v1, p0, LX/0YH;->d:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 80809
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Z
    .locals 11

    .prologue
    .line 80810
    monitor-enter p0

    :try_start_0
    const/4 v1, 0x1

    .line 80811
    iget-object v2, p0, LX/0YH;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v3

    iget-object v2, p0, LX/0YH;->f:LX/0YI;

    .line 80812
    iget-object v7, v2, LX/0YI;->a:Landroid/content/SharedPreferences;

    const-string v8, "LastRunTime"

    const-wide/16 v9, 0x0

    invoke-interface {v7, v8, v9, v10}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v7

    move-wide v5, v7

    .line 80813
    sub-long/2addr v3, v5

    .line 80814
    const-wide/16 v5, 0x0

    cmp-long v2, v3, v5

    if-gez v2, :cond_5

    .line 80815
    iget-object v2, p0, LX/0YH;->f:LX/0YI;

    invoke-virtual {v2}, LX/0YI;->b()V

    .line 80816
    :cond_0
    :goto_0
    move v0, v1

    .line 80817
    if-nez v0, :cond_2

    .line 80818
    iget-object v0, p0, LX/0YH;->g:LX/0YC;

    invoke-virtual {v0}, LX/0YC;->a()[Ljava/io/File;

    move-result-object v0

    .line 80819
    iget-object v1, p0, LX/0YH;->g:LX/0YC;

    invoke-virtual {v1}, LX/0YC;->b()[Ljava/io/File;

    move-result-object v1

    .line 80820
    invoke-static {p0, v0}, LX/0YH;->a(LX/0YH;[Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0, v1}, LX/0YH;->a(LX/0YH;[Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_1
    const/4 v0, 0x1

    :goto_1
    move v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80821
    if-nez v0, :cond_3

    .line 80822
    :cond_2
    const/4 v0, 0x0

    .line 80823
    :goto_2
    monitor-exit p0

    return v0

    .line 80824
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/0YH;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kb;

    invoke-virtual {v0}, LX/0kb;->v()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 80825
    invoke-static {p0}, LX/0YH;->d(LX/0YH;)V

    .line 80826
    :goto_3
    const/4 v0, 0x1

    goto :goto_2

    .line 80827
    :cond_4
    iget-object v0, p0, LX/0YH;->i:LX/0Yb;

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/0YH;->i:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->a()Z

    move-result v0

    if-eqz v0, :cond_7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80828
    :goto_4
    goto :goto_3

    .line 80829
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    :try_start_2
    const-wide/32 v5, 0xdbba0

    cmp-long v2, v3, v5

    if-ltz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_6
    :try_start_3
    const/4 v0, 0x0

    goto :goto_1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 80830
    :cond_7
    new-instance v1, LX/4mH;

    invoke-direct {v1, p0}, LX/4mH;-><init>(LX/0YH;)V

    .line 80831
    iget-object v0, p0, LX/0YH;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v2, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-interface {v0, v2, v1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    iget-object v1, p0, LX/0YH;->e:Landroid/os/Handler;

    invoke-interface {v0, v1}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/0YH;->i:LX/0Yb;

    .line 80832
    iget-object v0, p0, LX/0YH;->i:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    goto :goto_4
.end method
