.class public LX/0oh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public a:Ljava/lang/Boolean;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final b:LX/0Uh;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1At;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "LX/1Av;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uh;LX/0Ot;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/1At;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 141883
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141884
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/0oh;->d:LX/01J;

    .line 141885
    iput-object p1, p0, LX/0oh;->b:LX/0Uh;

    .line 141886
    iput-object p2, p0, LX/0oh;->c:LX/0Ot;

    .line 141887
    return-void
.end method

.method public static a(LX/0QB;)LX/0oh;
    .locals 5

    .prologue
    .line 141840
    const-class v1, LX/0oh;

    monitor-enter v1

    .line 141841
    :try_start_0
    sget-object v0, LX/0oh;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 141842
    sput-object v2, LX/0oh;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 141843
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141844
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 141845
    new-instance v4, LX/0oh;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    const/16 p0, 0x2aa

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/0oh;-><init>(LX/0Uh;LX/0Ot;)V

    .line 141846
    move-object v0, v4

    .line 141847
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 141848
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/0oh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 141849
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 141850
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a()Z
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 141867
    iget-object v0, p0, LX/0oh;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 141868
    iget-object v0, p0, LX/0oh;->b:LX/0Uh;

    const/16 v1, 0x337

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/0oh;->a:Ljava/lang/Boolean;

    .line 141869
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 141870
    :cond_0
    iget-object v0, p0, LX/0oh;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private c(Ljava/lang/Class;)LX/1Av;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/1Av;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 141871
    monitor-enter p0

    .line 141872
    :try_start_0
    iget-object v0, p0, LX/0oh;->d:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141873
    iget-object v0, p0, LX/0oh;->d:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Av;

    monitor-exit p0

    .line 141874
    :goto_0
    return-object v0

    .line 141875
    :cond_0
    iget-object v0, p0, LX/0oh;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1At;

    invoke-interface {v0, p1}, LX/1At;->a(Ljava/lang/Class;)LX/1Av;

    move-result-object v0

    .line 141876
    if-nez v0, :cond_1

    .line 141877
    const/4 v0, 0x0

    monitor-exit p0

    goto :goto_0

    .line 141878
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 141879
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/0oh;->d:LX/01J;

    invoke-virtual {v1, p1, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141880
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 141881
    invoke-virtual {v0, p0}, LX/1Av;->a(LX/0oh;)V

    .line 141882
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)LX/1Av;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/1Av;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 141864
    invoke-direct {p0}, LX/0oh;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 141865
    const/4 v0, 0x0

    .line 141866
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, LX/0oh;->c(Ljava/lang/Class;)LX/1Av;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Class;)LX/1Av;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TARGET:",
            "Ljava/lang/Object;",
            "TASK:",
            "LX/1Ax;",
            ">(",
            "Ljava/lang/Class",
            "<TTARGET;>;)",
            "LX/1Av",
            "<TTARGET;TTASK;>;"
        }
    .end annotation

    .prologue
    .line 141851
    monitor-enter p0

    .line 141852
    :try_start_0
    iget-object v0, p0, LX/0oh;->d:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Av;

    .line 141853
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141854
    if-eqz v0, :cond_0

    .line 141855
    invoke-virtual {v0, p0}, LX/1Av;->c(LX/0oh;)V

    .line 141856
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 141857
    :goto_0
    return-object v0

    .line 141858
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 141859
    :cond_0
    iget-object v0, p0, LX/0oh;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1At;

    invoke-interface {v0, p1}, LX/1At;->a(Ljava/lang/Class;)LX/1Av;

    move-result-object v0

    .line 141860
    if-nez v0, :cond_1

    .line 141861
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "no Initializer for %s: it must be annotated with @GenerateInitializer"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141862
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 141863
    invoke-virtual {v0, p0}, LX/1Av;->b(LX/0oh;)V

    goto :goto_0
.end method
