.class public LX/1hE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1hF;


# static fields
.field private static final TAG:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final mErrorReporter:LX/03V;

.field private mGLogWrapper:Lcom/facebook/proxygen/utils/GLogWrapper;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 295666
    const-class v0, LX/1hE;

    sput-object v0, LX/1hE;->TAG:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 0

    .prologue
    .line 295667
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 295668
    iput-object p1, p0, LX/1hE;->mErrorReporter:LX/03V;

    .line 295669
    return-void
.end method


# virtual methods
.method public init()V
    .locals 1

    .prologue
    .line 295670
    new-instance v0, Lcom/facebook/proxygen/utils/GLogWrapper;

    invoke-direct {v0, p0}, Lcom/facebook/proxygen/utils/GLogWrapper;-><init>(LX/1hF;)V

    iput-object v0, p0, LX/1hE;->mGLogWrapper:Lcom/facebook/proxygen/utils/GLogWrapper;

    .line 295671
    return-void
.end method

.method public log(LX/1hG;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 295672
    sget-object v0, LX/1hG;->FATAL:LX/1hG;

    if-ne p1, v0, :cond_0

    .line 295673
    iget-object v0, p0, LX/1hE;->mErrorReporter:LX/03V;

    sget-object v1, LX/1hE;->TAG:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 295674
    :cond_0
    return-void
.end method
