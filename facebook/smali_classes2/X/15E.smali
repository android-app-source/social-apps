.class public final LX/15E;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public b:Lorg/apache/http/client/methods/HttpUriRequest;

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/common/callercontext/CallerContext;

.field public e:Ljava/lang/String;

.field public f:LX/14Q;

.field public g:Lorg/apache/http/client/ResponseHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;"
        }
    .end annotation
.end field

.field public h:Lorg/apache/http/client/RedirectHandler;

.field public i:LX/15F;

.field public j:LX/14P;

.field public k:Lcom/facebook/http/interfaces/RequestPriority;

.field public l:Ljava/lang/String;

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1iW;",
            ">;"
        }
    .end annotation
.end field

.field public n:I

.field public o:J

.field public p:Z

.field public q:LX/2BD;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 180351
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, LX/15E;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 180348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180349
    sget-object v0, LX/14Q;->FALLBACK_NOT_REQUIRED:LX/14Q;

    iput-object v0, p0, LX/15E;->f:LX/14Q;

    .line 180350
    sget-object v0, LX/14P;->CONSERVATIVE:LX/14P;

    iput-object v0, p0, LX/15E;->j:LX/14P;

    return-void
.end method


# virtual methods
.method public final a()LX/15D;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/15D",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 180320
    move-object/from16 v0, p0

    iget-object v9, v0, LX/15E;->h:Lorg/apache/http/client/RedirectHandler;

    .line 180321
    if-nez v9, :cond_0

    .line 180322
    new-instance v9, Lorg/apache/http/impl/client/DefaultRedirectHandler;

    invoke-direct {v9}, Lorg/apache/http/impl/client/DefaultRedirectHandler;-><init>()V

    .line 180323
    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, LX/15E;->n:I

    if-nez v2, :cond_1

    .line 180324
    sget-object v2, LX/15E;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/15E;->n:I

    .line 180325
    :cond_1
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/15E;->o:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 180326
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/15E;->o:J

    .line 180327
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/15E;->i:LX/15F;

    if-nez v2, :cond_3

    .line 180328
    new-instance v2, LX/15F;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/15E;->c:Ljava/lang/String;

    invoke-direct {v2, v3}, LX/15F;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/15E;->i:LX/15F;

    .line 180329
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    if-eqz v2, :cond_4

    .line 180330
    move-object/from16 v0, p0

    iget-object v2, v0, LX/15E;->i:LX/15F;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v2, v3}, LX/15F;->a(Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 180331
    :cond_4
    new-instance v2, LX/15D;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/15E;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/15E;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/15E;->f:LX/14Q;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/15E;->i:LX/15F;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/15E;->j:LX/14P;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/15E;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v13, v0, LX/15E;->n:I

    move-object/from16 v0, p0

    iget-wide v14, v0, LX/15E;->o:J

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/15E;->p:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/15E;->m:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, LX/15E;->q:LX/2BD;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-direct/range {v2 .. v19}, LX/15D;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;LX/14Q;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/client/RedirectHandler;LX/15F;LX/14P;Ljava/lang/String;IJZLX/0am;LX/2BD;B)V

    return-object v2
.end method

.method public final a(LX/14P;)LX/15E;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/14P;",
            ")",
            "LX/15E",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 180346
    iput-object p1, p0, LX/15E;->j:LX/14P;

    .line 180347
    return-object p0
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;)LX/15E;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/15E",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 180344
    iput-object p1, p0, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 180345
    return-object p0
.end method

.method public final a(Lcom/facebook/http/interfaces/RequestPriority;)LX/15E;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            ")",
            "LX/15E",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 180342
    iput-object p1, p0, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 180343
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/15E;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/15E",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 180340
    iput-object p1, p0, LX/15E;->c:Ljava/lang/String;

    .line 180341
    return-object p0
.end method

.method public final a(Ljava/util/List;)LX/15E;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/1iW;",
            ">;)",
            "LX/15E;"
        }
    .end annotation

    .prologue
    .line 180338
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, LX/15E;->m:Ljava/util/List;

    .line 180339
    return-object p0
.end method

.method public final a(Lorg/apache/http/client/RedirectHandler;)LX/15E;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/RedirectHandler;",
            ")",
            "LX/15E",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 180336
    iput-object p1, p0, LX/15E;->h:Lorg/apache/http/client/RedirectHandler;

    .line 180337
    return-object p0
.end method

.method public final a(Lorg/apache/http/client/ResponseHandler;)LX/15E;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;)",
            "LX/15E",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 180334
    iput-object p1, p0, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 180335
    return-object p0
.end method

.method public final a(Lorg/apache/http/client/methods/HttpUriRequest;)LX/15E;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ")",
            "LX/15E",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 180332
    iput-object p1, p0, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 180333
    return-object p0
.end method

.method public final a(Z)LX/15E;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/15E",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 180318
    iput-boolean p1, p0, LX/15E;->p:Z

    .line 180319
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/15E;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/15E",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 180316
    iput-object p1, p0, LX/15E;->e:Ljava/lang/String;

    .line 180317
    return-object p0
.end method
