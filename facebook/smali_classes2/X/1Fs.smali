.class public LX/1Fs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ft;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/1Fu;

.field public static final b:LX/1Fu;

.field private static volatile i:LX/1Fs;


# instance fields
.field private final c:LX/0ad;

.field private final d:Ljava/util/concurrent/Executor;

.field private final e:Ljava/util/concurrent/Executor;

.field private final f:Ljava/util/concurrent/Executor;

.field private final g:LX/0TD;

.field private final h:LX/0TD;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 224718
    sget-object v0, LX/1Fu;->NORMAL:LX/1Fu;

    sput-object v0, LX/1Fs;->a:LX/1Fu;

    .line 224719
    sget-object v0, LX/1Fu;->HIGH:LX/1Fu;

    sput-object v0, LX/1Fs;->b:LX/1Fu;

    return-void
.end method

.method public constructor <init>(LX/1Fx;LX/0TD;LX/0TD;LX/0TD;LX/0ad;)V
    .locals 1
    .param p1    # LX/1Fx;
        .annotation runtime Lcom/facebook/common/executors/ImageCacheRequestExecutor;
        .end annotation
    .end param
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ImageDecodeExecutorService;
        .end annotation
    .end param
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ImageTransformExecutorService;
        .end annotation
    .end param
    .param p4    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ImageOffUiThreadExecutor;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 224710
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224711
    sget-object v0, LX/1Fs;->b:LX/1Fu;

    invoke-interface {p1, v0}, LX/1Fx;->a(LX/1Fu;)Ljava/util/concurrent/Executor;

    move-result-object v0

    iput-object v0, p0, LX/1Fs;->d:Ljava/util/concurrent/Executor;

    .line 224712
    sget-object v0, LX/1Fs;->a:LX/1Fu;

    invoke-interface {p1, v0}, LX/1Fx;->a(LX/1Fu;)Ljava/util/concurrent/Executor;

    move-result-object v0

    iput-object v0, p0, LX/1Fs;->e:Ljava/util/concurrent/Executor;

    .line 224713
    iput-object p2, p0, LX/1Fs;->f:Ljava/util/concurrent/Executor;

    .line 224714
    iput-object p3, p0, LX/1Fs;->g:LX/0TD;

    .line 224715
    iput-object p4, p0, LX/1Fs;->h:LX/0TD;

    .line 224716
    iput-object p5, p0, LX/1Fs;->c:LX/0ad;

    .line 224717
    return-void
.end method

.method public static a(LX/0QB;)LX/1Fs;
    .locals 9

    .prologue
    .line 224697
    sget-object v0, LX/1Fs;->i:LX/1Fs;

    if-nez v0, :cond_1

    .line 224698
    const-class v1, LX/1Fs;

    monitor-enter v1

    .line 224699
    :try_start_0
    sget-object v0, LX/1Fs;->i:LX/1Fs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 224700
    if-eqz v2, :cond_0

    .line 224701
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 224702
    new-instance v3, LX/1Fs;

    invoke-static {v0}, LX/1Fv;->a(LX/0QB;)LX/1Fx;

    move-result-object v4

    check-cast v4, LX/1Fx;

    invoke-static {v0}, LX/1G4;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, LX/0TD;

    invoke-static {v0}, LX/1G5;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, LX/0TD;

    invoke-static {v0}, LX/1G6;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, LX/0TD;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/1Fs;-><init>(LX/1Fx;LX/0TD;LX/0TD;LX/0TD;LX/0ad;)V

    .line 224703
    move-object v0, v3

    .line 224704
    sput-object v0, LX/1Fs;->i:LX/1Fs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 224705
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 224706
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 224707
    :cond_1
    sget-object v0, LX/1Fs;->i:LX/1Fs;

    return-object v0

    .line 224708
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 224709
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/util/concurrent/Executor;I)Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 224695
    const/high16 v0, -0x80000000

    if-ne p1, v0, :cond_0

    .line 224696
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/4eJ;

    invoke-direct {v0, p0, p1}, LX/4eJ;-><init>(Ljava/util/concurrent/Executor;I)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/concurrent/Executor;
    .locals 3

    .prologue
    .line 224685
    iget-object v0, p0, LX/1Fs;->c:LX/0ad;

    sget v1, LX/1FD;->h:I

    const/high16 v2, -0x80000000

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 224686
    iget-object v1, p0, LX/1Fs;->d:Ljava/util/concurrent/Executor;

    invoke-static {v1, v0}, LX/1Fs;->a(Ljava/util/concurrent/Executor;I)Ljava/util/concurrent/Executor;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/concurrent/Executor;
    .locals 3

    .prologue
    .line 224693
    iget-object v0, p0, LX/1Fs;->c:LX/0ad;

    sget v1, LX/1FD;->i:I

    const/high16 v2, -0x80000000

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 224694
    iget-object v1, p0, LX/1Fs;->e:Ljava/util/concurrent/Executor;

    invoke-static {v1, v0}, LX/1Fs;->a(Ljava/util/concurrent/Executor;I)Ljava/util/concurrent/Executor;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/util/concurrent/Executor;
    .locals 3

    .prologue
    .line 224691
    iget-object v0, p0, LX/1Fs;->c:LX/0ad;

    sget v1, LX/1FD;->f:I

    const/high16 v2, -0x80000000

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 224692
    iget-object v1, p0, LX/1Fs;->f:Ljava/util/concurrent/Executor;

    invoke-static {v1, v0}, LX/1Fs;->a(Ljava/util/concurrent/Executor;I)Ljava/util/concurrent/Executor;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/util/concurrent/Executor;
    .locals 3

    .prologue
    .line 224689
    iget-object v0, p0, LX/1Fs;->c:LX/0ad;

    sget v1, LX/1FD;->e:I

    const/high16 v2, -0x80000000

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 224690
    iget-object v1, p0, LX/1Fs;->g:LX/0TD;

    invoke-static {v1, v0}, LX/1Fs;->a(Ljava/util/concurrent/Executor;I)Ljava/util/concurrent/Executor;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/util/concurrent/Executor;
    .locals 3

    .prologue
    .line 224687
    iget-object v0, p0, LX/1Fs;->c:LX/0ad;

    sget v1, LX/1FD;->g:I

    const/high16 v2, -0x80000000

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 224688
    iget-object v1, p0, LX/1Fs;->h:LX/0TD;

    invoke-static {v1, v0}, LX/1Fs;->a(Ljava/util/concurrent/Executor;I)Ljava/util/concurrent/Executor;

    move-result-object v0

    return-object v0
.end method
