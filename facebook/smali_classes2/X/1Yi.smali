.class public final enum LX/1Yi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1Yi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1Yi;

.field public static final enum CLOSEST_FIRST:LX/1Yi;

.field public static final enum FURTHEST_FIRST:LX/1Yi;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 273948
    new-instance v0, LX/1Yi;

    const-string v1, "FURTHEST_FIRST"

    invoke-direct {v0, v1, v2}, LX/1Yi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Yi;->FURTHEST_FIRST:LX/1Yi;

    .line 273949
    new-instance v0, LX/1Yi;

    const-string v1, "CLOSEST_FIRST"

    invoke-direct {v0, v1, v3}, LX/1Yi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Yi;->CLOSEST_FIRST:LX/1Yi;

    .line 273950
    const/4 v0, 0x2

    new-array v0, v0, [LX/1Yi;

    sget-object v1, LX/1Yi;->FURTHEST_FIRST:LX/1Yi;

    aput-object v1, v0, v2

    sget-object v1, LX/1Yi;->CLOSEST_FIRST:LX/1Yi;

    aput-object v1, v0, v3

    sput-object v0, LX/1Yi;->$VALUES:[LX/1Yi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 273951
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1Yi;
    .locals 1

    .prologue
    .line 273952
    const-class v0, LX/1Yi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1Yi;

    return-object v0
.end method

.method public static values()[LX/1Yi;
    .locals 1

    .prologue
    .line 273953
    sget-object v0, LX/1Yi;->$VALUES:[LX/1Yi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1Yi;

    return-object v0
.end method
