.class public LX/0wA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0wA;


# instance fields
.field public final a:LX/0pn;

.field public final b:LX/0ad;


# direct methods
.method public constructor <init>(LX/0pn;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 159455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159456
    iput-object p1, p0, LX/0wA;->a:LX/0pn;

    .line 159457
    iput-object p2, p0, LX/0wA;->b:LX/0ad;

    .line 159458
    return-void
.end method

.method public static a(LX/0QB;)LX/0wA;
    .locals 5

    .prologue
    .line 159459
    sget-object v0, LX/0wA;->c:LX/0wA;

    if-nez v0, :cond_1

    .line 159460
    const-class v1, LX/0wA;

    monitor-enter v1

    .line 159461
    :try_start_0
    sget-object v0, LX/0wA;->c:LX/0wA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 159462
    if-eqz v2, :cond_0

    .line 159463
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 159464
    new-instance p0, LX/0wA;

    invoke-static {v0}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v3

    check-cast v3, LX/0pn;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/0wA;-><init>(LX/0pn;LX/0ad;)V

    .line 159465
    move-object v0, p0

    .line 159466
    sput-object v0, LX/0wA;->c:LX/0wA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159467
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 159468
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 159469
    :cond_1
    sget-object v0, LX/0wA;->c:LX/0wA;

    return-object v0

    .line 159470
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 159471
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
