.class public final LX/1LW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1B3;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1B3;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 233895
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233896
    iput-object p1, p0, LX/1LW;->a:LX/0QB;

    .line 233897
    return-void
.end method

.method public static a(LX/0QB;)LX/0Ot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1B3;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 233893
    new-instance v0, LX/1LW;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1LW;-><init>(LX/0QB;)V

    move-object v0, v0

    .line 233894
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 233892
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/1LW;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 233887
    packed-switch p2, :pswitch_data_0

    .line 233888
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233889
    :pswitch_0
    invoke-static {p1}, LX/1B2;->a(LX/0QB;)LX/1B2;

    move-result-object v0

    .line 233890
    :goto_0
    return-object v0

    :pswitch_1
    invoke-static {p1}, LX/1BE;->a(LX/0QB;)LX/1BE;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 233891
    const/4 v0, 0x2

    return v0
.end method
