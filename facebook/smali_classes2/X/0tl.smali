.class public final LX/0tl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/3HG;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/3HG;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 155568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155569
    iput-object p1, p0, LX/0tl;->a:LX/0QB;

    .line 155570
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 155571
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0tl;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 155572
    packed-switch p2, :pswitch_data_0

    .line 155573
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155574
    :pswitch_0
    new-instance p2, LX/3PE;

    const-class v0, LX/3PF;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3PF;

    const-class v1, LX/3PG;

    invoke-interface {p1, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/3PG;

    invoke-static {p1}, LX/3PH;->b(LX/0QB;)LX/3PH;

    move-result-object v2

    check-cast v2, LX/3PH;

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    invoke-direct {p2, v0, v1, v2, p0}, LX/3PE;-><init>(LX/3PF;LX/3PG;LX/3PH;LX/0Uh;)V

    .line 155575
    move-object v0, p2

    .line 155576
    :goto_0
    return-object v0

    .line 155577
    :pswitch_1
    invoke-static {p1}, LX/3HF;->b(LX/0QB;)LX/3HF;

    move-result-object v0

    goto :goto_0

    .line 155578
    :pswitch_2
    new-instance p2, LX/3PI;

    invoke-direct {p2}, LX/3PI;-><init>()V

    .line 155579
    const-class v0, LX/3GS;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3GS;

    const-class v1, LX/3PJ;

    invoke-interface {p1, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/3PJ;

    invoke-static {p1}, LX/3PH;->b(LX/0QB;)LX/3PH;

    move-result-object v2

    check-cast v2, LX/3PH;

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    .line 155580
    iput-object v0, p2, LX/3PI;->a:LX/3GS;

    iput-object v1, p2, LX/3PI;->b:LX/3PJ;

    iput-object v2, p2, LX/3PI;->c:LX/3PH;

    iput-object p0, p2, LX/3PI;->d:LX/0Uh;

    .line 155581
    move-object v0, p2

    .line 155582
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 155583
    const/4 v0, 0x3

    return v0
.end method
