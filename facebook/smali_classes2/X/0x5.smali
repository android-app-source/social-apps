.class public LX/0x5;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hk;


# instance fields
.field public volatile a:LX/0Or;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0f7;

.field public d:LX/0Yb;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 161883
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 161884
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 161885
    iput-object v0, p0, LX/0x5;->b:LX/0Ot;

    .line 161886
    return-void
.end method

.method public static g(LX/0x5;)V
    .locals 3

    .prologue
    .line 161887
    iget-object v0, p0, LX/0x5;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "harrison_fragment_stacks"

    iget-object v2, p0, LX/0x5;->c:LX/0f7;

    invoke-interface {v2}, LX/0f7;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 161888
    iget-object v0, p0, LX/0x5;->c:LX/0f7;

    invoke-interface {v0}, LX/0f7;->s()Ljava/lang/String;

    move-result-object v0

    .line 161889
    if-nez v0, :cond_0

    .line 161890
    const-string v0, "None"

    move-object v1, v0

    .line 161891
    :goto_0
    iget-object v0, p0, LX/0x5;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "harrison_current_tab"

    invoke-virtual {v0, v2, v1}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 161892
    return-void

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 161893
    instance-of v0, p1, LX/0f7;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 161894
    check-cast p1, LX/0f7;

    iput-object p1, p0, LX/0x5;->c:LX/0f7;

    .line 161895
    iget-object v0, p0, LX/0x5;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    .line 161896
    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    .line 161897
    const-string v1, "com.facebook.feed.util.NEWS_FEED_NEW_STORIES"

    new-instance p1, LX/13e;

    invoke-direct {p1, p0}, LX/13e;-><init>(LX/0x5;)V

    invoke-interface {v0, v1, p1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    .line 161898
    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/0x5;->d:LX/0Yb;

    .line 161899
    iget-object v0, p0, LX/0x5;->d:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 161900
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 161901
    invoke-static {p0}, LX/0x5;->g(LX/0x5;)V

    .line 161902
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 161903
    return-void
.end method
