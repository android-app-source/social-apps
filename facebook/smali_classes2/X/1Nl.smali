.class public final LX/1Nl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1CH;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/fragment/NewsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/fragment/NewsFeedFragment;)V
    .locals 0

    .prologue
    .line 237612
    iput-object p1, p0, LX/1Nl;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 237613
    iget-object v0, p0, LX/1Nl;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->I:LX/1KS;

    .line 237614
    iget-object v1, v0, LX/1KS;->m:LX/0Ot;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/1KS;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/1KS;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1PI;

    invoke-virtual {v1}, LX/0hD;->kw_()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 237615
    iget-object v1, v0, LX/1KS;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1PI;

    .line 237616
    iget-object v2, v1, LX/1PI;->a:LX/1PJ;

    sget-object v0, LX/1PL;->IS_POSTING:LX/1PL;

    invoke-virtual {v2, v0}, LX/1PJ;->b(LX/1PL;)V

    .line 237617
    :cond_0
    iget-object v0, p0, LX/1Nl;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    .line 237618
    iget-boolean v1, v0, LX/0g2;->ak:Z

    move v0, v1

    .line 237619
    if-eqz v0, :cond_1

    .line 237620
    iget-object v0, p0, LX/1Nl;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->mJ_()V

    .line 237621
    :cond_1
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 237622
    iget-object v0, p0, LX/1Nl;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->A()Z

    move-result v0

    if-nez v0, :cond_1

    .line 237623
    :cond_0
    :goto_0
    return-void

    .line 237624
    :cond_1
    iget-object v0, p0, LX/1Nl;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->I:LX/1KS;

    invoke-virtual {v0, p1}, LX/1KS;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 237625
    iget-object v0, p0, LX/1Nl;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->u()LX/0qq;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0qq;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 237626
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-nez v0, :cond_0

    .line 237627
    iget-object v0, p0, LX/1Nl;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->f()V

    goto :goto_0
.end method

.method public final a(JLcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3

    .prologue
    .line 237628
    iget-object v0, p0, LX/1Nl;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->s:LX/0pd;

    iget-object v1, p0, LX/1Nl;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v1, v1, Lcom/facebook/feed/fragment/NewsFeedFragment;->o:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0, v1, p1, p2, p3}, LX/0pd;->a(Lcom/facebook/api/feedtype/FeedType;JLcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 237629
    iget-object v0, p0, LX/1Nl;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->mJ_()V

    .line 237630
    iget-object v0, p0, LX/1Nl;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->I:LX/1KS;

    .line 237631
    iget-object v1, v0, LX/1KS;->m:LX/0Ot;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/1KS;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/1KS;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1PI;

    invoke-virtual {v1}, LX/0hD;->kw_()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 237632
    iget-object v1, v0, LX/1KS;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1PI;

    .line 237633
    iget-object p0, v1, LX/1PI;->a:LX/1PJ;

    sget-object v0, LX/1PL;->IS_POSTING:LX/1PL;

    invoke-virtual {p0, v0}, LX/1PJ;->b(LX/1PL;)V

    .line 237634
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 7

    .prologue
    .line 237635
    iget-object v0, p0, LX/1Nl;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->I:LX/1KS;

    invoke-virtual {v0, p1}, LX/1KS;->b(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 237636
    iget-object v0, p0, LX/1Nl;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, LX/16t;->a(Lcom/facebook/graphql/model/GraphQLStory;J)V

    .line 237637
    iget-object v0, p0, LX/1Nl;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->u()LX/0qq;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0qq;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 237638
    iget-object v0, p0, LX/1Nl;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->C:LX/1CY;

    iget-object v1, p0, LX/1Nl;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v1}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v1

    .line 237639
    iget-object v2, v1, LX/0fz;->d:LX/0qm;

    move-object v1, v2

    .line 237640
    invoke-virtual {v1}, LX/0qm;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1CY;->a(Ljava/lang/Iterable;)V

    .line 237641
    iget-object v0, p0, LX/1Nl;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    const/4 v3, 0x0

    .line 237642
    iget-object v1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->G:LX/1Ar;

    invoke-virtual {v1}, LX/1Ar;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 237643
    invoke-static {p1}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 237644
    if-nez v1, :cond_1

    .line 237645
    :cond_0
    :goto_0
    return-void

    .line 237646
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v4

    .line 237647
    if-eqz v4, :cond_0

    .line 237648
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v3

    :goto_1
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    .line 237649
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    .line 237650
    if-eqz v1, :cond_2

    .line 237651
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    .line 237652
    if-eqz v6, :cond_2

    .line 237653
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v1

    .line 237654
    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v6

    const p0, -0x36bd2417

    if-ne v6, p0, :cond_2

    if-eqz v1, :cond_2

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->NEW_YEARS:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    invoke-virtual {v1, v6}, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 237655
    iget-object v1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->A:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zb;

    const-string v2, "factory_delights_nye_2017_post_submitted"

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 237656
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 237657
    iget-object v1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->F:Lcom/facebook/delights/floating/DelightsFireworks;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/delights/floating/DelightsFireworks;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 237658
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method
