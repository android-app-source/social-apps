.class public LX/19e;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/19e;


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 208199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208200
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/19e;->a:Landroid/content/Context;

    .line 208201
    return-void
.end method

.method public static b(LX/0QB;)LX/19e;
    .locals 4

    .prologue
    .line 208186
    sget-object v0, LX/19e;->b:LX/19e;

    if-nez v0, :cond_1

    .line 208187
    const-class v1, LX/19e;

    monitor-enter v1

    .line 208188
    :try_start_0
    sget-object v0, LX/19e;->b:LX/19e;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 208189
    if-eqz v2, :cond_0

    .line 208190
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 208191
    new-instance p0, LX/19e;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/19e;-><init>(Landroid/content/Context;)V

    .line 208192
    move-object v0, p0

    .line 208193
    sput-object v0, LX/19e;->b:LX/19e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208194
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 208195
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 208196
    :cond_1
    sget-object v0, LX/19e;->b:LX/19e;

    return-object v0

    .line 208197
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 208198
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public a()Landroid/view/TextureView;
    .locals 2

    .prologue
    .line 208185
    new-instance v0, Landroid/view/TextureView;

    iget-object v1, p0, LX/19e;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method
