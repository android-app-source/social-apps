.class public LX/10u;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/10u;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Uh;

.field public final c:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Uh;LX/0Uh;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p3    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 169476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169477
    iput-object p1, p0, LX/10u;->a:LX/0Or;

    .line 169478
    iput-object p2, p0, LX/10u;->b:LX/0Uh;

    .line 169479
    iput-object p3, p0, LX/10u;->c:LX/0Uh;

    .line 169480
    return-void
.end method

.method public static a(LX/0QB;)LX/10u;
    .locals 6

    .prologue
    .line 169481
    sget-object v0, LX/10u;->d:LX/10u;

    if-nez v0, :cond_1

    .line 169482
    const-class v1, LX/10u;

    monitor-enter v1

    .line 169483
    :try_start_0
    sget-object v0, LX/10u;->d:LX/10u;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 169484
    if-eqz v2, :cond_0

    .line 169485
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 169486
    new-instance v5, LX/10u;

    const/16 v3, 0x12cb

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {v5, p0, v3, v4}, LX/10u;-><init>(LX/0Or;LX/0Uh;LX/0Uh;)V

    .line 169487
    move-object v0, v5

    .line 169488
    sput-object v0, LX/10u;->d:LX/10u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169489
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 169490
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 169491
    :cond_1
    sget-object v0, LX/10u;->d:LX/10u;

    return-object v0

    .line 169492
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 169493
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
