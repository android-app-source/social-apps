.class public final LX/0rQ;
.super LX/0rP;
.source ""


# static fields
.field private static final b:[LX/0rQ;


# instance fields
.field public final a:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xc

    .line 149571
    new-array v0, v4, [LX/0rQ;

    sput-object v0, LX/0rQ;->b:[LX/0rQ;

    .line 149572
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    .line 149573
    sget-object v1, LX/0rQ;->b:[LX/0rQ;

    new-instance v2, LX/0rQ;

    add-int/lit8 v3, v0, -0x1

    invoke-direct {v2, v3}, LX/0rQ;-><init>(I)V

    aput-object v2, v1, v0

    .line 149574
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 149575
    :cond_0
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 149570
    invoke-direct {p0}, LX/0rP;-><init>()V

    iput p1, p0, LX/0rQ;->a:I

    return-void
.end method

.method public static c(I)LX/0rQ;
    .locals 2

    .prologue
    .line 149561
    const/16 v0, 0xa

    if-gt p0, v0, :cond_0

    const/4 v0, -0x1

    if-ge p0, v0, :cond_1

    :cond_0
    new-instance v0, LX/0rQ;

    invoke-direct {v0, p0}, LX/0rQ;-><init>(I)V

    .line 149562
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/0rQ;->b:[LX/0rQ;

    add-int/lit8 v1, p0, 0x1

    aget-object v0, v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final A()Ljava/math/BigInteger;
    .locals 2

    .prologue
    .line 149576
    iget v0, p0, LX/0rQ;->a:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149579
    iget v0, p0, LX/0rQ;->a:I

    invoke-static {v0}, LX/13I;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/15z;
    .locals 1

    .prologue
    .line 149577
    sget-object v0, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    return-object v0
.end method

.method public final a(Z)Z
    .locals 1

    .prologue
    .line 149578
    iget v0, p0, LX/0rQ;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/16L;
    .locals 1

    .prologue
    .line 149563
    sget-object v0, LX/16L;->INT:LX/16L;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 149564
    if-ne p1, p0, :cond_1

    .line 149565
    :cond_0
    :goto_0
    return v0

    .line 149566
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 149567
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 149568
    goto :goto_0

    .line 149569
    :cond_3
    check-cast p1, LX/0rQ;

    iget v2, p1, LX/0rQ;->a:I

    iget v3, p0, LX/0rQ;->a:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 149556
    iget v0, p0, LX/0rQ;->a:I

    return v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 149555
    const/4 v0, 0x1

    return v0
.end method

.method public final serialize(LX/0nX;LX/0my;)V
    .locals 1

    .prologue
    .line 149553
    iget v0, p0, LX/0rQ;->a:I

    invoke-virtual {p1, v0}, LX/0nX;->b(I)V

    .line 149554
    return-void
.end method

.method public final v()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 149552
    iget v0, p0, LX/0rQ;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final w()I
    .locals 1

    .prologue
    .line 149557
    iget v0, p0, LX/0rQ;->a:I

    return v0
.end method

.method public final x()J
    .locals 2

    .prologue
    .line 149558
    iget v0, p0, LX/0rQ;->a:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public final y()D
    .locals 2

    .prologue
    .line 149559
    iget v0, p0, LX/0rQ;->a:I

    int-to-double v0, v0

    return-wide v0
.end method

.method public final z()Ljava/math/BigDecimal;
    .locals 2

    .prologue
    .line 149560
    iget v0, p0, LX/0rQ;->a:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method
