.class public LX/0Pa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Pb;


# instance fields
.field private final a:LX/1vN;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 56823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56824
    invoke-static {p1}, LX/0Pa;->b(Landroid/content/Context;)LX/1vN;

    move-result-object v0

    iput-object v0, p0, LX/0Pa;->a:LX/1vN;

    .line 56825
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 56826
    const-string v0, ""

    .line 56827
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "LoomColdStartConfig"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 56828
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "LoomInitFileConfig"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 56829
    invoke-static {v1}, LX/0Pa;->a(Ljava/io/File;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 56830
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 56831
    :cond_0
    invoke-static {v2}, LX/0Pa;->a(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 56832
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 56833
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 56834
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not delete Loom config file(s):"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 56835
    :cond_2
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/loom/config/SystemControlConfiguration;JLjava/util/ArrayList;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/loom/config/SystemControlConfiguration;",
            "J",
            "Ljava/util/ArrayList",
            "<",
            "LX/1vM;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56836
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 56837
    :cond_0
    return-void

    .line 56838
    :cond_1
    new-instance v5, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "LoomInitFileConfig"

    invoke-direct {v5, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 56839
    const-string v2, "LoomInitFileConfig"

    const-string v3, ".tmp"

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-static {v2, v3, v4}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v6

    .line 56840
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/4 v3, 0x0

    .line 56841
    const/16 v2, 0x100

    :try_start_0
    new-array v8, v2, [B

    .line 56842
    invoke-static {v8}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 56843
    const/4 v2, 0x5

    invoke-virtual {v9, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    move-wide/from16 v0, p2

    invoke-virtual {v2, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/loom/config/SystemControlConfiguration;->a()J

    move-result-wide v10

    invoke-virtual {v2, v10, v11}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/loom/config/SystemControlConfiguration;->b()J

    move-result-wide v10

    invoke-virtual {v2, v10, v11}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/loom/config/SystemControlConfiguration;->c()J

    move-result-wide v10

    invoke-virtual {v2, v10, v11}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 56844
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v10

    const/4 v2, 0x0

    move v4, v2

    :goto_0
    if-ge v4, v10, :cond_2

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1vM;

    .line 56845
    iget v11, v2, LX/1vM;->a:I

    invoke-virtual {v9, v11}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v11

    iget v12, v2, LX/1vM;->b:I

    invoke-virtual {v11, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v11

    iget v12, v2, LX/1vM;->c:I

    invoke-virtual {v11, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v11

    iget v12, v2, LX/1vM;->d:I

    invoke-virtual {v11, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v11

    iget v12, v2, LX/1vM;->e:I

    invoke-virtual {v11, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v11

    iget v12, v2, LX/1vM;->f:I

    invoke-virtual {v11, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v11

    iget v2, v2, LX/1vM;->g:I

    invoke-virtual {v11, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 56846
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 56847
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    invoke-virtual {v7, v8, v2, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 56848
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    .line 56849
    invoke-virtual {v6, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 56850
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Could not rename config temp file to final location"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 56851
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    move-result v3

    if-nez v3, :cond_4

    .line 56852
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not remove config temp file "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 56853
    :catch_0
    move-exception v2

    :try_start_1
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 56854
    :catchall_0
    move-exception v3

    move-object v13, v3

    move-object v3, v2

    move-object v2, v13

    :goto_1
    if-eqz v3, :cond_3

    :try_start_2
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    throw v2

    :catch_1
    move-exception v4

    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    goto :goto_2

    .line 56855
    :cond_4
    throw v2

    .line 56856
    :catchall_1
    move-exception v2

    goto :goto_1
.end method

.method private static a(Ljava/io/File;)Z
    .locals 1

    .prologue
    .line 56857
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 56858
    invoke-virtual {p0}, Ljava/io/File;->deleteOnExit()V

    .line 56859
    const/4 v0, 0x0

    .line 56860
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;)LX/1vN;
    .locals 28
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 56861
    new-instance v14, Ljava/io/File;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v4

    const-string v5, "LoomInitFileConfig"

    invoke-direct {v14, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 56862
    :try_start_0
    new-instance v15, Ljava/io/FileInputStream;

    invoke-direct {v15, v14}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v12, 0x0

    .line 56863
    const/16 v4, 0x100

    :try_start_1
    new-array v4, v4, [B

    .line 56864
    const/4 v5, 0x0

    const/16 v6, 0x100

    invoke-virtual {v15, v4, v5, v6}, Ljava/io/FileInputStream;->read([BII)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 56865
    if-nez v5, :cond_0

    .line 56866
    :try_start_2
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 56867
    invoke-static {v14}, LX/0Pa;->a(Ljava/io/File;)Z

    const/4 v4, 0x0

    :goto_0
    return-object v4

    .line 56868
    :cond_0
    :try_start_3
    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v16

    .line 56869
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->getInt()I
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result v4

    .line 56870
    const/4 v6, 0x5

    if-eq v4, v6, :cond_1

    .line 56871
    :try_start_4
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 56872
    invoke-static {v14}, LX/0Pa;->a(Ljava/io/File;)Z

    const/4 v4, 0x0

    goto :goto_0

    .line 56873
    :cond_1
    :try_start_5
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v18

    .line 56874
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v20

    .line 56875
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v22

    .line 56876
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v24

    .line 56877
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v17

    .line 56878
    move/from16 v0, v17

    mul-int/lit16 v4, v0, 0xe0

    add-int/lit16 v4, v4, 0x140

    div-int/lit8 v4, v4, 0x8
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 56879
    if-eq v4, v5, :cond_2

    .line 56880
    :try_start_6
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 56881
    invoke-static {v14}, LX/0Pa;->a(Ljava/io/File;)Z

    const/4 v4, 0x0

    goto :goto_0

    .line 56882
    :cond_2
    :try_start_7
    move/from16 v0, v17

    new-array v0, v0, [LX/1vM;

    move-object/from16 v26, v0

    .line 56883
    const/4 v4, 0x0

    move v13, v4

    :goto_1
    move/from16 v0, v17

    if-ge v13, v0, :cond_3

    .line 56884
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    .line 56885
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    .line 56886
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    .line 56887
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v8

    .line 56888
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v9

    .line 56889
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    .line 56890
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v11

    .line 56891
    new-instance v4, LX/1vM;

    invoke-direct/range {v4 .. v11}, LX/1vM;-><init>(IIIIIII)V

    aput-object v4, v26, v13

    .line 56892
    add-int/lit8 v4, v13, 0x1

    move v13, v4

    goto :goto_1

    .line 56893
    :cond_3
    new-instance v5, Lcom/facebook/loom/config/SystemControlConfiguration;

    move-wide/from16 v6, v20

    move-wide/from16 v8, v22

    move-wide/from16 v10, v24

    invoke-direct/range {v5 .. v11}, Lcom/facebook/loom/config/SystemControlConfiguration;-><init>(JJJ)V

    .line 56894
    new-instance v4, LX/1vN;

    move-wide/from16 v0, v18

    move-object/from16 v2, v26

    invoke-direct {v4, v0, v1, v5, v2}, LX/1vN;-><init>(JLcom/facebook/loom/config/SystemControlConfiguration;[LX/1vM;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 56895
    :try_start_8
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 56896
    invoke-static {v14}, LX/0Pa;->a(Ljava/io/File;)Z

    goto/16 :goto_0

    .line 56897
    :catch_0
    move-exception v4

    :try_start_9
    throw v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 56898
    :catchall_0
    move-exception v5

    move-object/from16 v27, v5

    move-object v5, v4

    move-object/from16 v4, v27

    :goto_2
    if-eqz v5, :cond_4

    :try_start_a
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :goto_3
    :try_start_b
    throw v4
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 56899
    :catch_1
    invoke-static {v14}, LX/0Pa;->a(Ljava/io/File;)Z

    const/4 v4, 0x0

    goto/16 :goto_0

    .line 56900
    :catch_2
    move-exception v6

    :try_start_c
    invoke-static {v5, v6}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto :goto_3

    .line 56901
    :catchall_1
    move-exception v4

    invoke-static {v14}, LX/0Pa;->a(Ljava/io/File;)Z

    throw v4

    .line 56902
    :cond_4
    :try_start_d
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto :goto_3

    :catchall_2
    move-exception v4

    move-object v5, v12

    goto :goto_2
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 56903
    iget-object v0, p0, LX/0Pa;->a:LX/1vN;

    if-eqz v0, :cond_0

    .line 56904
    iget-object v0, p0, LX/0Pa;->a:LX/1vN;

    .line 56905
    iget-object v1, v0, LX/1vN;->b:LX/1vO;

    const/16 p0, 0x8

    invoke-virtual {v1, p0}, LX/1vO;->a(I)LX/0Pk;

    move-result-object v1

    check-cast v1, LX/0Pp;

    .line 56906
    if-eqz v1, :cond_1

    .line 56907
    iget v1, v1, LX/0Pp;->c:I

    .line 56908
    :goto_0
    move v0, v1

    .line 56909
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(LX/0Pc;)V
    .locals 0
    .param p1    # LX/0Pc;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 56910
    return-void
.end method

.method public final b()LX/0Pd;
    .locals 1

    .prologue
    .line 56911
    iget-object v0, p0, LX/0Pa;->a:LX/1vN;

    if-eqz v0, :cond_0

    .line 56912
    iget-object v0, p0, LX/0Pa;->a:LX/1vN;

    .line 56913
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/0Pf;->a:LX/0Pd;

    goto :goto_0
.end method
