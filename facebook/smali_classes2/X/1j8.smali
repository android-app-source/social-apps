.class public final LX/1j8;
.super LX/0eW;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadArgumentPlacement",
        "BadClosingBracePlacement",
        "YodaConditions"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 300013
    invoke-direct {p0}, LX/0eW;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1j9;)LX/1j9;
    .locals 2

    .prologue
    .line 300010
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, LX/0eW;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->b(I)I

    move-result v0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 300011
    iput v0, p1, LX/1j9;->a:I

    iput-object v1, p1, LX/1j9;->b:Ljava/nio/ByteBuffer;

    move-object v0, p1

    .line 300012
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1jA;)LX/1jA;
    .locals 2

    .prologue
    .line 300007
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, LX/0eW;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->b(I)I

    move-result v0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 300008
    iput v0, p1, LX/1jA;->a:I

    iput-object v1, p1, LX/1jA;->b:Ljava/nio/ByteBuffer;

    move-object v0, p1

    .line 300009
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1jB;)LX/1jB;
    .locals 2

    .prologue
    .line 300004
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, LX/0eW;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->b(I)I

    move-result v0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 300005
    iput v0, p1, LX/1jB;->a:I

    iput-object v1, p1, LX/1jB;->b:Ljava/nio/ByteBuffer;

    move-object v0, p1

    .line 300006
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1jC;)LX/1jC;
    .locals 2

    .prologue
    .line 299995
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, LX/0eW;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->b(I)I

    move-result v0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 299996
    iput v0, p1, LX/1jC;->a:I

    iput-object v1, p1, LX/1jC;->b:Ljava/nio/ByteBuffer;

    move-object v0, p1

    .line 299997
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1jD;)LX/1jD;
    .locals 2

    .prologue
    .line 300001
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, LX/0eW;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->b(I)I

    move-result v0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 300002
    iput v0, p1, LX/1jD;->a:I

    iput-object v1, p1, LX/1jD;->b:Ljava/nio/ByteBuffer;

    move-object v0, p1

    .line 300003
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1jM;)LX/1jM;
    .locals 2

    .prologue
    .line 299998
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, LX/0eW;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->b(I)I

    move-result v0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 299999
    iput v0, p1, LX/1jM;->a:I

    iput-object v1, p1, LX/1jM;->b:Ljava/nio/ByteBuffer;

    move-object v0, p1

    .line 300000
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
