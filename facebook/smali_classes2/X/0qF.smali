.class public interface abstract LX/0qF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract getExpirationTime()J
.end method

.method public abstract getHash()I
.end method

.method public abstract getKey()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation
.end method

.method public abstract getNext()LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract getNextEvictable()LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract getNextExpirable()LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract getPreviousEvictable()LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract getPreviousExpirable()LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract getValueReference()LX/0cp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0cp",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract setExpirationTime(J)V
.end method

.method public abstract setNextEvictable(LX/0qF;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation
.end method

.method public abstract setNextExpirable(LX/0qF;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation
.end method

.method public abstract setPreviousEvictable(LX/0qF;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation
.end method

.method public abstract setPreviousExpirable(LX/0qF;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation
.end method

.method public abstract setValueReference(LX/0cp;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0cp",
            "<TK;TV;>;)V"
        }
    .end annotation
.end method
