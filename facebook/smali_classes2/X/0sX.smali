.class public LX/0sX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/view/accessibility/AccessibilityManager;

.field public final b:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/view/accessibility/AccessibilityManager;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 152128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152129
    iput-object p1, p0, LX/0sX;->a:Landroid/view/accessibility/AccessibilityManager;

    .line 152130
    iput-object p2, p0, LX/0sX;->b:LX/0Uh;

    .line 152131
    return-void
.end method

.method public static a(LX/0QB;)LX/0sX;
    .locals 1

    .prologue
    .line 152132
    invoke-static {p0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/0sX;
    .locals 3

    .prologue
    .line 152133
    new-instance v2, LX/0sX;

    invoke-static {p0}, LX/0sY;->b(LX/0QB;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-direct {v2, v0, v1}, LX/0sX;-><init>(Landroid/view/accessibility/AccessibilityManager;LX/0Uh;)V

    .line 152134
    return-object v2
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 152135
    iget-object v1, p0, LX/0sX;->a:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0sX;->a:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0sX;->b:LX/0Uh;

    const/16 v2, 0x15

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
