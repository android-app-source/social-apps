.class public final LX/0bo;
.super LX/0bi;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0bi",
        "<",
        "LX/2K9;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0bo;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2K9;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 87290
    const/16 v0, 0x230

    invoke-direct {p0, p1, v0}, LX/0bi;-><init>(LX/0Ot;I)V

    .line 87291
    return-void
.end method

.method public static a(LX/0QB;)LX/0bo;
    .locals 4

    .prologue
    .line 87292
    sget-object v0, LX/0bo;->b:LX/0bo;

    if-nez v0, :cond_1

    .line 87293
    const-class v1, LX/0bo;

    monitor-enter v1

    .line 87294
    :try_start_0
    sget-object v0, LX/0bo;->b:LX/0bo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 87295
    if-eqz v2, :cond_0

    .line 87296
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 87297
    new-instance v3, LX/0bo;

    const/16 p0, 0x4ad

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/0bo;-><init>(LX/0Ot;)V

    .line 87298
    move-object v0, v3

    .line 87299
    sput-object v0, LX/0bo;->b:LX/0bo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87300
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 87301
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 87302
    :cond_1
    sget-object v0, LX/0bo;->b:LX/0bo;

    return-object v0

    .line 87303
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 87304
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Uh;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 87305
    check-cast p3, LX/2K9;

    .line 87306
    invoke-static {p3}, LX/2K9;->a$redex0(LX/2K9;)V

    .line 87307
    return-void
.end method
