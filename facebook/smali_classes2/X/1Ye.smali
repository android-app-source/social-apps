.class public LX/1Ye;
.super LX/1Yf;
.source ""


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hwp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0Ot;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/Hwp;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 273807
    invoke-direct {p0}, LX/1Yf;-><init>()V

    .line 273808
    iput-object p2, p0, LX/1Ye;->b:LX/0Ot;

    .line 273809
    iput-object p1, p0, LX/1Ye;->a:Ljava/util/concurrent/ExecutorService;

    .line 273810
    return-void
.end method


# virtual methods
.method public final e_(Z)V
    .locals 3

    .prologue
    .line 273804
    if-eqz p1, :cond_0

    .line 273805
    iget-object v0, p0, LX/1Ye;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/dialtone/cachemanager/DialtoneCacheManagerStateChangedListener$1;

    invoke-direct {v1, p0}, Lcom/facebook/dialtone/cachemanager/DialtoneCacheManagerStateChangedListener$1;-><init>(LX/1Ye;)V

    const v2, -0x2aeb44d2

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 273806
    :cond_0
    return-void
.end method
