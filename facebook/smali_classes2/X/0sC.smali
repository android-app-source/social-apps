.class public abstract LX/0sC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0s9;


# instance fields
.field private final a:LX/0s9;


# direct methods
.method public constructor <init>(LX/0s9;)V
    .locals 0

    .prologue
    .line 151283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151284
    iput-object p1, p0, LX/0sC;->a:LX/0s9;

    .line 151285
    return-void
.end method


# virtual methods
.method public final a()Landroid/net/Uri$Builder;
    .locals 1

    .prologue
    .line 151282
    iget-object v0, p0, LX/0sC;->a:LX/0s9;

    invoke-interface {v0}, LX/0s9;->a()Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final b()Landroid/net/Uri$Builder;
    .locals 1

    .prologue
    .line 151281
    iget-object v0, p0, LX/0sC;->a:LX/0s9;

    invoke-interface {v0}, LX/0s9;->b()Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Landroid/net/Uri$Builder;
    .locals 1

    .prologue
    .line 151280
    iget-object v0, p0, LX/0sC;->a:LX/0s9;

    invoke-interface {v0}, LX/0s9;->c()Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Landroid/net/Uri$Builder;
    .locals 1

    .prologue
    .line 151286
    iget-object v0, p0, LX/0sC;->a:LX/0s9;

    invoke-interface {v0}, LX/0s9;->d()Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final e()Landroid/net/Uri$Builder;
    .locals 1

    .prologue
    .line 151279
    iget-object v0, p0, LX/0sC;->a:LX/0s9;

    invoke-interface {v0}, LX/0s9;->e()Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final f()Landroid/net/Uri$Builder;
    .locals 1

    .prologue
    .line 151275
    iget-object v0, p0, LX/0sC;->a:LX/0s9;

    invoke-interface {v0}, LX/0s9;->f()Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final g()Landroid/net/Uri$Builder;
    .locals 1

    .prologue
    .line 151278
    iget-object v0, p0, LX/0sC;->a:LX/0s9;

    invoke-interface {v0}, LX/0s9;->g()Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151277
    iget-object v0, p0, LX/0sC;->a:LX/0s9;

    invoke-interface {v0}, LX/0s9;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151276
    iget-object v0, p0, LX/0sC;->a:LX/0s9;

    invoke-interface {v0}, LX/0s9;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
