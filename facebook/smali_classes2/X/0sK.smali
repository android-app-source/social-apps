.class public final LX/0sK;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static c:LX/0sK;


# instance fields
.field private a:LX/0WS;

.field private b:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 151617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151618
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/0sK;->b:Landroid/content/Context;

    .line 151619
    const/4 v0, 0x0

    iput-object v0, p0, LX/0sK;->a:LX/0WS;

    .line 151620
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)LX/0sK;
    .locals 2

    .prologue
    .line 151621
    const-class v1, LX/0sK;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/0sK;->c:LX/0sK;

    if-nez v0, :cond_0

    .line 151622
    new-instance v0, LX/0sK;

    invoke-direct {v0, p0}, LX/0sK;-><init>(Landroid/content/Context;)V

    sput-object v0, LX/0sK;->c:LX/0sK;

    .line 151623
    :cond_0
    sget-object v0, LX/0sK;->c:LX/0sK;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 151624
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized i(LX/0sK;)LX/0WS;
    .locals 2

    .prologue
    .line 151625
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0sK;->a:LX/0WS;

    if-nez v0, :cond_0

    .line 151626
    new-instance v0, LX/0WO;

    iget-object v1, p0, LX/0sK;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/0WO;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, LX/0WO;->a()LX/0WP;

    move-result-object v0

    .line 151627
    const-string v1, "overtheair_prefs"

    invoke-virtual {v0, v1}, LX/0WP;->a(Ljava/lang/String;)LX/0WS;

    move-result-object v0

    iput-object v0, p0, LX/0sK;->a:LX/0WS;

    .line 151628
    :cond_0
    iget-object v0, p0, LX/0sK;->a:LX/0WS;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 151629
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final b()I
    .locals 3

    .prologue
    .line 151630
    invoke-static {p0}, LX/0sK;->i(LX/0sK;)LX/0WS;

    move-result-object v0

    const-string v1, "next"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0WS;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 151631
    invoke-static {p0}, LX/0sK;->i(LX/0sK;)LX/0WS;

    move-result-object v0

    const-string v1, "activated"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0WS;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 151632
    invoke-static {p0}, LX/0sK;->i(LX/0sK;)LX/0WS;

    move-result-object v0

    invoke-virtual {v0}, LX/0WS;->b()LX/1gW;

    move-result-object v0

    const-string v1, "download_in_progress_ota_version"

    invoke-interface {v0, v1}, LX/1gW;->a(Ljava/lang/String;)LX/1gW;

    move-result-object v0

    invoke-interface {v0}, LX/1gW;->c()V

    .line 151633
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 151634
    invoke-static {p0}, LX/0sK;->i(LX/0sK;)LX/0WS;

    move-result-object v0

    invoke-virtual {v0}, LX/0WS;->b()LX/1gW;

    move-result-object v0

    const-string v1, "next"

    invoke-interface {v0, v1}, LX/1gW;->a(Ljava/lang/String;)LX/1gW;

    move-result-object v0

    const-string v1, "download_end"

    invoke-interface {v0, v1}, LX/1gW;->a(Ljava/lang/String;)LX/1gW;

    move-result-object v0

    invoke-interface {v0}, LX/1gW;->c()V

    .line 151635
    return-void
.end method
