.class public LX/0oo;
.super LX/0i8;
.source ""


# instance fields
.field private final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 143195
    invoke-direct {p0}, LX/0i8;-><init>()V

    .line 143196
    iput-object p1, p0, LX/0oo;->a:LX/0ad;

    .line 143197
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 143194
    iget-object v0, p0, LX/0oo;->a:LX/0ad;

    sget-short v1, LX/0on;->y:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 143193
    iget-object v0, p0, LX/0oo;->a:LX/0ad;

    sget-short v1, LX/0on;->F:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 143192
    invoke-virtual {p0}, LX/0oo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0oo;->g()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 143179
    iget-object v0, p0, LX/0oo;->a:LX/0ad;

    sget-short v1, LX/0on;->E:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 3

    .prologue
    .line 143191
    iget-object v0, p0, LX/0oo;->a:LX/0ad;

    sget v1, LX/0on;->K:I

    const/16 v2, 0xf

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 3

    .prologue
    .line 143190
    iget-object v0, p0, LX/0oo;->a:LX/0ad;

    sget-short v1, LX/0on;->I:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 3

    .prologue
    .line 143189
    iget-object v0, p0, LX/0oo;->a:LX/0ad;

    sget-short v1, LX/0on;->L:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 3

    .prologue
    .line 143188
    iget-object v0, p0, LX/0oo;->a:LX/0ad;

    sget-short v1, LX/0on;->S:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final n()Z
    .locals 3

    .prologue
    .line 143187
    iget-object v0, p0, LX/0oo;->a:LX/0ad;

    sget-short v1, LX/0on;->R:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final o()Z
    .locals 3

    .prologue
    .line 143186
    iget-object v0, p0, LX/0oo;->a:LX/0ad;

    sget-short v1, LX/0on;->A:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final p()Z
    .locals 3

    .prologue
    .line 143185
    iget-object v0, p0, LX/0oo;->a:LX/0ad;

    sget-short v1, LX/0on;->M:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final q()Z
    .locals 3

    .prologue
    .line 143184
    iget-object v0, p0, LX/0oo;->a:LX/0ad;

    sget-short v1, LX/0on;->B:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final r()I
    .locals 3

    .prologue
    .line 143183
    iget-object v0, p0, LX/0oo;->a:LX/0ad;

    sget v1, LX/0on;->J:I

    const/16 v2, 0xa

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    return v0
.end method

.method public final s()Z
    .locals 3

    .prologue
    .line 143182
    iget-object v0, p0, LX/0oo;->a:LX/0ad;

    sget-short v1, LX/0on;->P:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final u()Z
    .locals 3

    .prologue
    .line 143181
    iget-object v0, p0, LX/0oo;->a:LX/0ad;

    sget-short v1, LX/0on;->U:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final v()Z
    .locals 3

    .prologue
    .line 143180
    iget-object v0, p0, LX/0oo;->a:LX/0ad;

    sget-short v1, LX/0on;->z:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
