.class public LX/0PV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public a:[J

.field public b:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 56608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56609
    const/4 v0, -0x1

    iput v0, p0, LX/0PV;->b:I

    .line 56610
    new-array v0, p1, [J

    iput-object v0, p0, LX/0PV;->a:[J

    .line 56611
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 56586
    iget v0, p0, LX/0PV;->b:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final a(J)V
    .locals 4

    .prologue
    .line 56601
    iget-object v0, p0, LX/0PV;->a:[J

    array-length v0, v0

    iget v1, p0, LX/0PV;->b:I

    add-int/lit8 v1, v1, 0x1

    if-ne v0, v1, :cond_0

    .line 56602
    const/4 v3, 0x0

    .line 56603
    iget-object v0, p0, LX/0PV;->a:[J

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [J

    .line 56604
    iget-object v1, p0, LX/0PV;->a:[J

    iget-object v2, p0, LX/0PV;->a:[J

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 56605
    iput-object v0, p0, LX/0PV;->a:[J

    .line 56606
    :cond_0
    iget-object v0, p0, LX/0PV;->a:[J

    iget v1, p0, LX/0PV;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0PV;->b:I

    aput-wide p1, v0, v1

    .line 56607
    return-void
.end method

.method public final b()J
    .locals 3

    .prologue
    .line 56600
    iget-object v0, p0, LX/0PV;->a:[J

    iget v1, p0, LX/0PV;->b:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, LX/0PV;->b:I

    aget-wide v0, v0, v1

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 56587
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 56588
    const-string v0, "<LongStack vector:["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56589
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LX/0PV;->a:[J

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 56590
    if-eqz v0, :cond_0

    .line 56591
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56592
    :cond_0
    iget v2, p0, LX/0PV;->b:I

    if-ne v0, v2, :cond_1

    .line 56593
    const-string v2, ">>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56594
    :cond_1
    iget-object v2, p0, LX/0PV;->a:[J

    aget-wide v2, v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 56595
    iget v2, p0, LX/0PV;->b:I

    if-ne v0, v2, :cond_2

    .line 56596
    const-string v2, "<<"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56597
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 56598
    :cond_3
    const-string v0, "]>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56599
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
