.class public LX/0WH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 75667
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;II)Landroid/content/Context;
    .locals 2

    .prologue
    .line 75668
    invoke-static {p0, p1, p2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v0

    .line 75669
    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-direct {v1, p0, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 75670
    move-object v1, p0

    :goto_0
    invoke-virtual {p1, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 75671
    instance-of v0, v1, Landroid/content/ContextWrapper;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 75672
    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object p0

    .line 75673
    if-ne v1, p0, :cond_1

    move-object v1, v2

    .line 75674
    :cond_0
    :goto_1
    return-object v1

    :cond_1
    move-object v1, p0

    .line 75675
    goto :goto_0

    :cond_2
    move-object v1, v2

    .line 75676
    goto :goto_1
.end method

.method public static b(Landroid/content/Context;II)I
    .locals 2

    .prologue
    .line 75677
    invoke-static {p0, p1}, LX/0WH;->c(Landroid/content/Context;I)LX/0am;

    move-result-object v0

    .line 75678
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75679
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    .line 75680
    :cond_0
    return p2
.end method

.method public static b(Landroid/content/Context;I)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "LX/0am",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75681
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 75682
    invoke-static {p0, p1}, LX/0WH;->c(Landroid/content/Context;I)LX/0am;

    move-result-object v0

    .line 75683
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 75684
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 75685
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Landroid/content/Context;
    .locals 2

    .prologue
    .line 75686
    instance-of v0, p0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 75687
    check-cast v0, Landroid/app/Activity;

    .line 75688
    invoke-virtual {v0}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 75689
    invoke-virtual {v0}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object p0

    .line 75690
    :cond_0
    return-object p0
.end method

.method public static c(Landroid/content/Context;II)I
    .locals 2

    .prologue
    .line 75691
    invoke-static {p0, p1}, LX/0WH;->d(Landroid/content/Context;I)LX/0am;

    move-result-object v0

    .line 75692
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75693
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    .line 75694
    :cond_0
    return p2
.end method

.method private static c(Landroid/content/Context;I)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75695
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 75696
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75697
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 75698
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;II)I
    .locals 3

    .prologue
    .line 75699
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 75700
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75701
    iget v0, v0, Landroid/util/TypedValue;->data:I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result p2

    .line 75702
    :cond_0
    return p2
.end method

.method public static d(Landroid/content/Context;I)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75703
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 75704
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75705
    iget v0, v0, Landroid/util/TypedValue;->data:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 75706
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public static e(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 75707
    invoke-static {p0, p1}, LX/0WH;->b(Landroid/content/Context;I)LX/0am;

    move-result-object v0

    .line 75708
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75709
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 75710
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method
