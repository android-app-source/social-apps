.class public final enum LX/183;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/183;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/183;

.field public static final enum FAILED:LX/183;

.field public static final enum LOGGED:LX/183;

.field public static final enum LOGGING:LX/183;

.field public static final enum NOT_LOGGED:LX/183;

.field public static final enum PENDING:LX/183;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 203875
    new-instance v0, LX/183;

    const-string v1, "NOT_LOGGED"

    invoke-direct {v0, v1, v2}, LX/183;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/183;->NOT_LOGGED:LX/183;

    .line 203876
    new-instance v0, LX/183;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v3}, LX/183;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/183;->PENDING:LX/183;

    .line 203877
    new-instance v0, LX/183;

    const-string v1, "LOGGING"

    invoke-direct {v0, v1, v4}, LX/183;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/183;->LOGGING:LX/183;

    .line 203878
    new-instance v0, LX/183;

    const-string v1, "LOGGED"

    invoke-direct {v0, v1, v5}, LX/183;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/183;->LOGGED:LX/183;

    .line 203879
    new-instance v0, LX/183;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v6}, LX/183;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/183;->FAILED:LX/183;

    .line 203880
    const/4 v0, 0x5

    new-array v0, v0, [LX/183;

    sget-object v1, LX/183;->NOT_LOGGED:LX/183;

    aput-object v1, v0, v2

    sget-object v1, LX/183;->PENDING:LX/183;

    aput-object v1, v0, v3

    sget-object v1, LX/183;->LOGGING:LX/183;

    aput-object v1, v0, v4

    sget-object v1, LX/183;->LOGGED:LX/183;

    aput-object v1, v0, v5

    sget-object v1, LX/183;->FAILED:LX/183;

    aput-object v1, v0, v6

    sput-object v0, LX/183;->$VALUES:[LX/183;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 203883
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/183;
    .locals 1

    .prologue
    .line 203882
    const-class v0, LX/183;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/183;

    return-object v0
.end method

.method public static values()[LX/183;
    .locals 1

    .prologue
    .line 203881
    sget-object v0, LX/183;->$VALUES:[LX/183;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/183;

    return-object v0
.end method
