.class public LX/1cI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cF",
        "<",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/1cF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/1Ib;


# direct methods
.method public constructor <init>(LX/1cF;LX/1Ib;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;",
            "LX/1Ib;",
            ")V"
        }
    .end annotation

    .prologue
    .line 281788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 281789
    iput-object p1, p0, LX/1cI;->a:LX/1cF;

    .line 281790
    iput-object p2, p0, LX/1cI;->b:LX/1Ib;

    .line 281791
    return-void
.end method


# virtual methods
.method public final a(LX/1cd;LX/1cW;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 281792
    iget-object v0, p2, LX/1cW;->e:LX/1bY;

    move-object v0, v0

    .line 281793
    invoke-virtual {v0}, LX/1bY;->getValue()I

    move-result v0

    sget-object v1, LX/1bY;->DISK_CACHE:LX/1bY;

    invoke-virtual {v1}, LX/1bY;->getValue()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 281794
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 281795
    :goto_0
    return-void

    .line 281796
    :cond_0
    iget-object v0, p2, LX/1cW;->a:LX/1bf;

    move-object v0, v0

    .line 281797
    iget-boolean v1, v0, LX/1bf;->l:Z

    move v0, v1

    .line 281798
    if-eqz v0, :cond_1

    .line 281799
    new-instance v0, LX/1uq;

    iget-object v1, p0, LX/1cI;->b:LX/1Ib;

    invoke-direct {v0, p1, p2, v1}, LX/1uq;-><init>(LX/1cd;LX/1cW;LX/1Ib;)V

    move-object p1, v0

    .line 281800
    :cond_1
    iget-object v0, p0, LX/1cI;->a:LX/1cF;

    invoke-interface {v0, p1, p2}, LX/1cF;->a(LX/1cd;LX/1cW;)V

    goto :goto_0
.end method
