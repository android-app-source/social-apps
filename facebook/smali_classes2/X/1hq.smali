.class public LX/1hq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1hr;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1hq;


# instance fields
.field public a:Z

.field private final b:LX/1hs;


# direct methods
.method public constructor <init>(LX/1hs;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 297144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297145
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1hq;->a:Z

    .line 297146
    iput-object p1, p0, LX/1hq;->b:LX/1hs;

    .line 297147
    return-void
.end method

.method public static a(LX/0QB;)LX/1hq;
    .locals 4

    .prologue
    .line 297148
    sget-object v0, LX/1hq;->c:LX/1hq;

    if-nez v0, :cond_1

    .line 297149
    const-class v1, LX/1hq;

    monitor-enter v1

    .line 297150
    :try_start_0
    sget-object v0, LX/1hq;->c:LX/1hq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 297151
    if-eqz v2, :cond_0

    .line 297152
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 297153
    new-instance p0, LX/1hq;

    invoke-static {v0}, LX/1hs;->a(LX/0QB;)LX/1hs;

    move-result-object v3

    check-cast v3, LX/1hs;

    invoke-direct {p0, v3}, LX/1hq;-><init>(LX/1hs;)V

    .line 297154
    move-object v0, p0

    .line 297155
    sput-object v0, LX/1hq;->c:LX/1hq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297156
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 297157
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 297158
    :cond_1
    sget-object v0, LX/1hq;->c:LX/1hq;

    return-object v0

    .line 297159
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 297160
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 297161
    const v0, 0x7fffffff

    return v0
.end method

.method public final a(Lorg/apache/http/impl/client/RequestWrapper;)V
    .locals 2

    .prologue
    .line 297162
    iget-boolean v0, p0, LX/1hq;->a:Z

    if-eqz v0, :cond_0

    .line 297163
    iget-object v0, p0, LX/1hq;->b:LX/1hs;

    invoke-virtual {p1}, Lorg/apache/http/impl/client/RequestWrapper;->getURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1hs;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 297164
    const-string v0, "https://broken.facebook.com"

    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/RequestWrapper;->setURI(Ljava/net/URI;)V

    .line 297165
    :cond_0
    return-void
.end method
