.class public final LX/0wP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field public b:Landroid/view/View;

.field public c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field private final d:I

.field private final e:I

.field public f:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field public g:LX/63W;

.field public h:LX/107;

.field private final i:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/facebook/ui/titlebar/Fb4aTitleBar;II)V
    .locals 1

    .prologue
    .line 159890
    iput-object p1, p0, LX/0wP;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159891
    iput p2, p0, LX/0wP;->d:I

    .line 159892
    iput p3, p0, LX/0wP;->e:I

    .line 159893
    new-instance v0, LX/0wQ;

    invoke-direct {v0, p0, p1}, LX/0wQ;-><init>(LX/0wP;Lcom/facebook/ui/titlebar/Fb4aTitleBar;)V

    iput-object v0, p0, LX/0wP;->i:Landroid/view/View$OnClickListener;

    .line 159894
    return-void
.end method

.method public static a$redex0(LX/0wP;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;Z)V
    .locals 6
    .param p0    # LX/0wP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 159895
    iget-object v0, p0, LX/0wP;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iget-object v0, v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestLayout()V

    .line 159896
    if-eqz p1, :cond_0

    sget-object v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    if-ne p1, v0, :cond_2

    .line 159897
    :cond_0
    iput-object v2, p0, LX/0wP;->f:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 159898
    invoke-direct {p0}, LX/0wP;->d()V

    .line 159899
    :cond_1
    :goto_0
    return-void

    .line 159900
    :cond_2
    if-eqz p2, :cond_d

    .line 159901
    iget-object v0, p0, LX/0wP;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iget-object v0, v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->h:Landroid/widget/LinearLayout;

    .line 159902
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 159903
    iget-object v1, p0, LX/0wP;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->e()V

    move-object v1, v0

    .line 159904
    :goto_1
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 159905
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 159906
    iput-object v2, p0, LX/0wP;->b:Landroid/view/View;

    .line 159907
    :cond_3
    iget-object v0, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    if-eqz v0, :cond_4

    .line 159908
    iget-object v0, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 159909
    iput-object v2, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 159910
    :cond_4
    iput-object p1, p0, LX/0wP;->f:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 159911
    iget-object v0, p0, LX/0wP;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f01026c

    invoke-static {v0, v2, v5}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v2

    .line 159912
    iget-boolean v0, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->q:Z

    move v0, v0

    .line 159913
    if-eqz v0, :cond_f

    .line 159914
    iget-object v0, p0, LX/0wP;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v3, p0, LX/0wP;->e:I

    invoke-virtual {v0, v3, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 159915
    iget v0, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->h:I

    move v0, v0

    .line 159916
    if-eq v0, v4, :cond_5

    .line 159917
    iget-object v0, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 159918
    iget v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->h:I

    move v3, v3

    .line 159919
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 159920
    iget-object v0, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(I)V

    .line 159921
    iget-object v0, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 159922
    :cond_5
    iget-object v0, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->i:Ljava/lang/String;

    move-object v0, v0

    .line 159923
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 159924
    iget-object v2, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 159925
    iget-boolean v0, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->p:Z

    move v0, v0

    .line 159926
    if-eqz v0, :cond_e

    iget-object v0, p0, LX/0wP;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iget-object v0, v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->u:LX/0wJ;

    .line 159927
    iget-object v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->i:Ljava/lang/String;

    move-object v3, v3

    .line 159928
    iget-object v4, p0, LX/0wP;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v0, v3, v4}, LX/0wJ;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_2
    invoke-virtual {v2, v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 159929
    iget-object v0, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 159930
    :cond_6
    iget-object v0, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 159931
    :cond_7
    :goto_3
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    if-eqz v0, :cond_8

    .line 159932
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    .line 159933
    iget-boolean v1, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->t:Z

    move v1, v1

    .line 159934
    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 159935
    :cond_8
    iget-object v0, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    if-eqz v0, :cond_9

    .line 159936
    iget-object v0, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 159937
    iget-boolean v1, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    move v1, v1

    .line 159938
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setEnabled(Z)V

    .line 159939
    :cond_9
    iget-object v0, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->k:Ljava/lang/String;

    move-object v0, v0

    .line 159940
    if-eqz v0, :cond_b

    .line 159941
    iget-object v1, p0, LX/0wP;->b:Landroid/view/View;

    if-eqz v1, :cond_a

    .line 159942
    iget-object v1, p0, LX/0wP;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 159943
    :cond_a
    iget-object v1, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    if-eqz v1, :cond_b

    .line 159944
    iget-object v1, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 159945
    :cond_b
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    if-eqz v0, :cond_c

    .line 159946
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    iget-object v1, p0, LX/0wP;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159947
    :cond_c
    iget-object v0, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    if-eqz v0, :cond_1

    .line 159948
    iget-object v0, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iget-object v1, p0, LX/0wP;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 159949
    :cond_d
    iget-object v0, p0, LX/0wP;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iget-object v0, v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->g:Landroid/widget/LinearLayout;

    move-object v1, v0

    goto/16 :goto_1

    .line 159950
    :cond_e
    iget-object v0, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->i:Ljava/lang/String;

    move-object v0, v0

    .line 159951
    goto :goto_2

    .line 159952
    :cond_f
    iget v0, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->h:I

    move v0, v0

    .line 159953
    if-eq v0, v4, :cond_11

    .line 159954
    iget-object v0, p0, LX/0wP;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v3, p0, LX/0wP;->d:I

    invoke-virtual {v0, v3, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/0wP;->b:Landroid/view/View;

    .line 159955
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    instance-of v0, v0, Lcom/facebook/apptab/glyph/BadgableGlyphView;

    if-eqz v0, :cond_10

    .line 159956
    iget-object v0, p0, LX/0wP;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 159957
    iget v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->h:I

    move v3, v3

    .line 159958
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 159959
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    check-cast v0, Lcom/facebook/apptab/glyph/BadgableGlyphView;

    invoke-virtual {v0, v3, v2}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 159960
    :goto_4
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 159961
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_3

    .line 159962
    :cond_10
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageButton;

    .line 159963
    iget v2, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->h:I

    move v2, v2

    .line 159964
    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_4

    .line 159965
    :cond_11
    iget-object v0, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->d:Landroid/graphics/drawable/Drawable;

    move-object v0, v0

    .line 159966
    if-eqz v0, :cond_15

    .line 159967
    iget-object v0, p0, LX/0wP;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v3, p0, LX/0wP;->d:I

    invoke-virtual {v0, v3, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/0wP;->b:Landroid/view/View;

    .line 159968
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    instance-of v0, v0, Lcom/facebook/apptab/glyph/BadgableGlyphView;

    if-eqz v0, :cond_14

    .line 159969
    iget-object v0, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->d:Landroid/graphics/drawable/Drawable;

    move-object v3, v0

    .line 159970
    iget-boolean v0, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->o:Z

    move v0, v0

    .line 159971
    if-eqz v0, :cond_13

    .line 159972
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    check-cast v0, Lcom/facebook/apptab/glyph/BadgableGlyphView;

    invoke-virtual {v0, v3, v2}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 159973
    :cond_12
    :goto_5
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 159974
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_3

    .line 159975
    :cond_13
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    check-cast v0, Lcom/facebook/apptab/glyph/BadgableGlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setGlyphImage(Landroid/graphics/drawable/Drawable;)V

    goto :goto_5

    .line 159976
    :cond_14
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/ImageButton;

    if-eqz v0, :cond_12

    .line 159977
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageButton;

    .line 159978
    iget-object v2, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->d:Landroid/graphics/drawable/Drawable;

    move-object v2, v2

    .line 159979
    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_5

    .line 159980
    :cond_15
    iget-object v0, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->i:Ljava/lang/String;

    move-object v0, v0

    .line 159981
    if-eqz v0, :cond_7

    .line 159982
    iget-object v0, p0, LX/0wP;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v2, p0, LX/0wP;->e:I

    invoke-virtual {v0, v2, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 159983
    iget-object v2, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 159984
    iget-boolean v0, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->p:Z

    move v0, v0

    .line 159985
    if-eqz v0, :cond_16

    iget-object v0, p0, LX/0wP;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iget-object v0, v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->u:LX/0wJ;

    .line 159986
    iget-object v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->i:Ljava/lang/String;

    move-object v3, v3

    .line 159987
    iget-object v4, p0, LX/0wP;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v0, v3, v4}, LX/0wJ;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_6
    invoke-virtual {v2, v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 159988
    iget-object v0, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 159989
    iget-object v0, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_3

    .line 159990
    :cond_16
    iget-object v0, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->i:Ljava/lang/String;

    move-object v0, v0

    .line 159991
    goto :goto_6
.end method

.method private d()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 159992
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 159993
    iget-object v0, p0, LX/0wP;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 159994
    :cond_0
    iget-object v0, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    if-eqz v0, :cond_1

    .line 159995
    iget-object v0, p0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 159996
    :cond_1
    return-void
.end method
