.class public final LX/1kg;
.super LX/1kh;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "LX/1kh",
        "<TV;",
        "Ljava/util/List",
        "<TV;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0Py;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Py",
            "<+",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;>;Z)V"
        }
    .end annotation

    .prologue
    .line 310132
    invoke-direct {p0}, LX/1kh;-><init>()V

    .line 310133
    new-instance v0, Lcom/google/common/util/concurrent/Futures$ListFuture$ListFutureRunningState;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/common/util/concurrent/Futures$ListFuture$ListFutureRunningState;-><init>(LX/1kg;LX/0Py;Z)V

    .line 310134
    iput-object v0, p0, LX/1ki;->b:Lcom/google/common/util/concurrent/AggregateFuture$RunningState;

    .line 310135
    iget-object v1, v0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->b:LX/0Py;

    invoke-virtual {v1}, LX/0Py;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 310136
    invoke-virtual {v0}, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->b()V

    .line 310137
    :cond_0
    return-void

    .line 310138
    :cond_1
    iget-boolean v1, v0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->c:Z

    if-eqz v1, :cond_2

    .line 310139
    const/4 v1, 0x0

    .line 310140
    iget-object v2, v0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->b:LX/0Py;

    invoke-virtual {v2}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object p2

    move v2, v1

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 310141
    add-int/lit8 p1, v2, 0x1

    .line 310142
    new-instance p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState$1;

    invoke-direct {p0, v0, v2, v1}, Lcom/google/common/util/concurrent/AggregateFuture$RunningState$1;-><init>(Lcom/google/common/util/concurrent/AggregateFuture$RunningState;ILcom/google/common/util/concurrent/ListenableFuture;)V

    .line 310143
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 310144
    invoke-interface {v1, p0, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    move v2, p1

    .line 310145
    goto :goto_0

    .line 310146
    :cond_2
    iget-object v1, v0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->b:LX/0Py;

    invoke-virtual {v1}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 310147
    sget-object p1, LX/131;->INSTANCE:LX/131;

    move-object p1, p1

    .line 310148
    invoke-interface {v1, v0, p1}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    goto :goto_1
.end method
