.class public final enum LX/1jR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1jR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1jR;

.field public static final enum CELL_2G:LX/1jR;

.field public static final enum CELL_3G:LX/1jR;

.field public static final enum CELL_4G:LX/1jR;

.field public static final enum CELL_UNKNOWN:LX/1jR;

.field public static final enum DISCONNECTED:LX/1jR;

.field public static final enum UNKNOWN:LX/1jR;

.field public static final enum WIFI:LX/1jR;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 300090
    new-instance v0, LX/1jR;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/1jR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1jR;->UNKNOWN:LX/1jR;

    .line 300091
    new-instance v0, LX/1jR;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v4}, LX/1jR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1jR;->DISCONNECTED:LX/1jR;

    .line 300092
    new-instance v0, LX/1jR;

    const-string v1, "CELL_UNKNOWN"

    invoke-direct {v0, v1, v5}, LX/1jR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1jR;->CELL_UNKNOWN:LX/1jR;

    .line 300093
    new-instance v0, LX/1jR;

    const-string v1, "CELL_2G"

    invoke-direct {v0, v1, v6}, LX/1jR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1jR;->CELL_2G:LX/1jR;

    .line 300094
    new-instance v0, LX/1jR;

    const-string v1, "CELL_3G"

    invoke-direct {v0, v1, v7}, LX/1jR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1jR;->CELL_3G:LX/1jR;

    .line 300095
    new-instance v0, LX/1jR;

    const-string v1, "CELL_4G"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/1jR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1jR;->CELL_4G:LX/1jR;

    .line 300096
    new-instance v0, LX/1jR;

    const-string v1, "WIFI"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/1jR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1jR;->WIFI:LX/1jR;

    .line 300097
    const/4 v0, 0x7

    new-array v0, v0, [LX/1jR;

    sget-object v1, LX/1jR;->UNKNOWN:LX/1jR;

    aput-object v1, v0, v3

    sget-object v1, LX/1jR;->DISCONNECTED:LX/1jR;

    aput-object v1, v0, v4

    sget-object v1, LX/1jR;->CELL_UNKNOWN:LX/1jR;

    aput-object v1, v0, v5

    sget-object v1, LX/1jR;->CELL_2G:LX/1jR;

    aput-object v1, v0, v6

    sget-object v1, LX/1jR;->CELL_3G:LX/1jR;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/1jR;->CELL_4G:LX/1jR;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1jR;->WIFI:LX/1jR;

    aput-object v2, v0, v1

    sput-object v0, LX/1jR;->$VALUES:[LX/1jR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 300098
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getNetworkType(I)LX/1jR;
    .locals 1

    .prologue
    .line 300099
    packed-switch p0, :pswitch_data_0

    .line 300100
    sget-object v0, LX/1jR;->CELL_UNKNOWN:LX/1jR;

    :goto_0
    return-object v0

    .line 300101
    :pswitch_0
    sget-object v0, LX/1jR;->CELL_2G:LX/1jR;

    goto :goto_0

    .line 300102
    :pswitch_1
    sget-object v0, LX/1jR;->CELL_3G:LX/1jR;

    goto :goto_0

    .line 300103
    :pswitch_2
    sget-object v0, LX/1jR;->CELL_4G:LX/1jR;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static getNetworkType(Landroid/net/NetworkInfo;)LX/1jR;
    .locals 2

    .prologue
    .line 300104
    if-nez p0, :cond_0

    .line 300105
    sget-object v0, LX/1jR;->UNKNOWN:LX/1jR;

    .line 300106
    :goto_0
    return-object v0

    .line 300107
    :cond_0
    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    .line 300108
    sget-object v1, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    if-eq v0, v1, :cond_1

    sget-object v1, Landroid/net/NetworkInfo$State;->DISCONNECTING:Landroid/net/NetworkInfo$State;

    if-ne v0, v1, :cond_2

    .line 300109
    :cond_1
    sget-object v0, LX/1jR;->DISCONNECTED:LX/1jR;

    goto :goto_0

    .line 300110
    :cond_2
    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    .line 300111
    packed-switch v0, :pswitch_data_0

    .line 300112
    sget-object v0, LX/1jR;->UNKNOWN:LX/1jR;

    goto :goto_0

    .line 300113
    :pswitch_0
    sget-object v0, LX/1jR;->WIFI:LX/1jR;

    goto :goto_0

    .line 300114
    :pswitch_1
    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v0

    invoke-static {v0}, LX/1jR;->getNetworkType(I)LX/1jR;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/1jR;
    .locals 1

    .prologue
    .line 300115
    const-class v0, LX/1jR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1jR;

    return-object v0
.end method

.method public static values()[LX/1jR;
    .locals 1

    .prologue
    .line 300116
    sget-object v0, LX/1jR;->$VALUES:[LX/1jR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1jR;

    return-object v0
.end method
