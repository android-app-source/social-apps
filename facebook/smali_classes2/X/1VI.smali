.class public LX/1VI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/0W3;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(LX/0W3;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 258718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 258719
    iput-object p1, p0, LX/1VI;->a:LX/0W3;

    .line 258720
    return-void
.end method

.method public static a(LX/0QB;)LX/1VI;
    .locals 4

    .prologue
    .line 258721
    const-class v1, LX/1VI;

    monitor-enter v1

    .line 258722
    :try_start_0
    sget-object v0, LX/1VI;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 258723
    sput-object v2, LX/1VI;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 258724
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258725
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 258726
    new-instance p0, LX/1VI;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-direct {p0, v3}, LX/1VI;-><init>(LX/0W3;)V

    .line 258727
    move-object v0, p0

    .line 258728
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 258729
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1VI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 258730
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 258731
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c(LX/1VI;)Z
    .locals 4

    .prologue
    .line 258732
    iget-object v0, p0, LX/1VI;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 258733
    iget-object v0, p0, LX/1VI;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jM:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1VI;->b:Ljava/lang/Boolean;

    .line 258734
    :cond_0
    iget-object v0, p0, LX/1VI;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 258735
    invoke-static {p0}, LX/1VI;->c(LX/1VI;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 258736
    const/4 v0, 0x0

    .line 258737
    :goto_0
    return v0

    .line 258738
    :cond_0
    iget-object v0, p0, LX/1VI;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 258739
    iget-object v0, p0, LX/1VI;->a:LX/0W3;

    sget-wide v2, LX/0X5;->jL:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1VI;->c:Ljava/lang/Boolean;

    .line 258740
    :cond_1
    iget-object v0, p0, LX/1VI;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method
