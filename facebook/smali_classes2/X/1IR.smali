.class public final LX/1IR;
.super LX/1IC;
.source ""


# static fields
.field public static final INSTANCE:LX/1IR;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 228677
    new-instance v0, LX/1IR;

    invoke-direct {v0}, LX/1IR;-><init>()V

    sput-object v0, LX/1IR;->INSTANCE:LX/1IR;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 228675
    const-string v0, "CharMatcher.none()"

    invoke-direct {p0, v0}, LX/1IC;-><init>(Ljava/lang/String;)V

    .line 228676
    return-void
.end method


# virtual methods
.method public final indexIn(Ljava/lang/CharSequence;)I
    .locals 1

    .prologue
    .line 228673
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228674
    const/4 v0, -0x1

    return v0
.end method

.method public final indexIn(Ljava/lang/CharSequence;I)I
    .locals 1

    .prologue
    .line 228670
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 228671
    invoke-static {p2, v0}, LX/0PB;->checkPositionIndex(II)I

    .line 228672
    const/4 v0, -0x1

    return v0
.end method

.method public final matches(C)Z
    .locals 1

    .prologue
    .line 228669
    const/4 v0, 0x0

    return v0
.end method

.method public final matchesAllOf(Ljava/lang/CharSequence;)Z
    .locals 1

    .prologue
    .line 228668
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final or(LX/1IA;)LX/1IA;
    .locals 1

    .prologue
    .line 228667
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1IA;

    return-object v0
.end method

.method public final removeFrom(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 228663
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final trimFrom(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 228666
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final trimLeadingFrom(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 228665
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final trimTrailingFrom(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 228664
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
