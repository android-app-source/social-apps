.class public LX/18V;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/18V;


# instance fields
.field private final a:LX/11H;

.field private final b:LX/18W;


# direct methods
.method public constructor <init>(LX/11H;LX/18W;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 206570
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206571
    iput-object p1, p0, LX/18V;->a:LX/11H;

    .line 206572
    iput-object p2, p0, LX/18V;->b:LX/18W;

    .line 206573
    return-void
.end method

.method public static a(LX/0QB;)LX/18V;
    .locals 5

    .prologue
    .line 206578
    sget-object v0, LX/18V;->c:LX/18V;

    if-nez v0, :cond_1

    .line 206579
    const-class v1, LX/18V;

    monitor-enter v1

    .line 206580
    :try_start_0
    sget-object v0, LX/18V;->c:LX/18V;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 206581
    if-eqz v2, :cond_0

    .line 206582
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 206583
    new-instance p0, LX/18V;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v3

    check-cast v3, LX/11H;

    invoke-static {v0}, LX/18W;->a(LX/0QB;)LX/18W;

    move-result-object v4

    check-cast v4, LX/18W;

    invoke-direct {p0, v3, v4}, LX/18V;-><init>(LX/11H;LX/18W;)V

    .line 206584
    move-object v0, p0

    .line 206585
    sput-object v0, LX/18V;->c:LX/18V;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 206586
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 206587
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 206588
    :cond_1
    sget-object v0, LX/18V;->c:LX/18V;

    return-object v0

    .line 206589
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 206590
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/2VK;
    .locals 1

    .prologue
    .line 206577
    iget-object v0, p0, LX/18V;->b:LX/18W;

    invoke-virtual {v0}, LX/18W;->a()LX/2VK;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;TPARAMS;)TRESU",
            "LT;"
        }
    .end annotation

    .prologue
    .line 206576
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/14U;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;TPARAMS;",
            "LX/14U;",
            ")TRESU",
            "LT;"
        }
    .end annotation

    .prologue
    .line 206575
    iget-object v0, p0, LX/18V;->a:LX/11H;

    invoke-virtual {v0, p1, p2, p3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/14U;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;TPARAMS;",
            "LX/14U;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")TRESU",
            "LT;"
        }
    .end annotation

    .prologue
    .line 206574
    iget-object v0, p0, LX/18V;->a:LX/11H;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
