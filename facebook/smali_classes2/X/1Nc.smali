.class public final LX/1Nc;
.super LX/0bJ;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field public final h:Z


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 237573
    invoke-direct {p0}, LX/0bJ;-><init>()V

    .line 237574
    iput-object p1, p0, LX/1Nc;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 237575
    iput-object p2, p0, LX/1Nc;->b:Ljava/lang/String;

    .line 237576
    iput-object p3, p0, LX/1Nc;->c:Ljava/lang/String;

    .line 237577
    iput-object p4, p0, LX/1Nc;->d:Ljava/lang/String;

    .line 237578
    iput-object p5, p0, LX/1Nc;->e:Ljava/lang/String;

    .line 237579
    iput-object p6, p0, LX/1Nc;->f:Ljava/lang/String;

    .line 237580
    iput-object p7, p0, LX/1Nc;->g:Ljava/lang/String;

    .line 237581
    iput-boolean p8, p0, LX/1Nc;->h:Z

    .line 237582
    return-void
.end method
