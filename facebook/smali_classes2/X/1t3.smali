.class public final LX/1t3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/Beta;
.end annotation


# static fields
.field private static final a:LX/1vG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1vG",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 335828
    new-instance v0, LX/1vF;

    invoke-direct {v0}, LX/1vF;-><init>()V

    sput-object v0, LX/1t3;->a:LX/1vG;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 335827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/io/File;)LX/1vI;
    .locals 2

    .prologue
    .line 335826
    new-instance v0, LX/1vH;

    invoke-direct {v0, p0}, LX/1vH;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method public static varargs a(Ljava/io/File;[LX/3AS;)LX/3AU;
    .locals 2

    .prologue
    .line 335825
    new-instance v0, LX/3AT;

    invoke-direct {v0, p0, p1}, LX/3AT;-><init>(Ljava/io/File;[LX/3AS;)V

    return-object v0
.end method

.method public static a(Ljava/io/File;LX/51l;)LX/51o;
    .locals 2

    .prologue
    .line 335819
    invoke-static {p0}, LX/1t3;->a(Ljava/io/File;)LX/1vI;

    move-result-object v0

    .line 335820
    invoke-virtual {p1}, LX/51l;->a()LX/51h;

    move-result-object v1

    .line 335821
    new-instance p0, LX/51m;

    invoke-direct {p0, v1}, LX/51m;-><init>(LX/51g;)V

    move-object p0, p0

    .line 335822
    invoke-virtual {v0, p0}, LX/1vI;->a(Ljava/io/OutputStream;)J

    .line 335823
    invoke-interface {v1}, LX/51h;->a()LX/51o;

    move-result-object v1

    move-object v0, v1

    .line 335824
    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 335815
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 335816
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 335817
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 335818
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/io/File;Ljava/io/File;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 335811
    invoke-virtual {p0, p1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Source %s and destination %s must be different"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v2

    aput-object p1, v4, v1

    invoke-static {v0, v3, v4}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 335812
    invoke-static {p0}, LX/1t3;->a(Ljava/io/File;)LX/1vI;

    move-result-object v0

    new-array v1, v2, [LX/3AS;

    invoke-static {p1, v1}, LX/1t3;->a(Ljava/io/File;[LX/3AS;)LX/3AU;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1vI;->a(LX/3AU;)J

    .line 335813
    return-void

    :cond_0
    move v0, v2

    .line 335814
    goto :goto_0
.end method

.method public static a(Ljava/io/File;Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 335752
    invoke-static {p0}, LX/1t3;->a(Ljava/io/File;)LX/1vI;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1vI;->a(Ljava/io/OutputStream;)J

    .line 335753
    return-void
.end method

.method public static a([BLjava/io/File;)V
    .locals 1

    .prologue
    .line 335809
    const/4 v0, 0x0

    new-array v0, v0, [LX/3AS;

    invoke-static {p1, v0}, LX/1t3;->a(Ljava/io/File;[LX/3AS;)LX/3AU;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/3AU;->a([B)V

    .line 335810
    return-void
.end method

.method public static a(Ljava/io/InputStream;J)[B
    .locals 5

    .prologue
    .line 335787
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 335788
    new-instance v0, Ljava/lang/OutOfMemoryError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "file is too large to fit in a byte array: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 335789
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    invoke-static {p0}, LX/0hW;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    long-to-int v0, p1

    const/4 p2, -0x1

    const/4 p1, 0x0

    .line 335790
    new-array v1, v0, [B

    move v2, v0

    .line 335791
    :goto_1
    if-lez v2, :cond_4

    .line 335792
    sub-int v3, v0, v2

    .line 335793
    invoke-virtual {p0, v1, v3, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    .line 335794
    if-ne v4, p2, :cond_3

    .line 335795
    invoke-static {v1, v3}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    .line 335796
    :cond_2
    :goto_2
    move-object v0, v1

    .line 335797
    goto :goto_0

    .line 335798
    :cond_3
    sub-int/2addr v2, v4

    .line 335799
    goto :goto_1

    .line 335800
    :cond_4
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v2

    .line 335801
    if-eq v2, p2, :cond_2

    .line 335802
    new-instance v3, LX/523;

    invoke-direct {v3}, LX/523;-><init>()V

    .line 335803
    invoke-virtual {v3, v2}, LX/523;->write(I)V

    .line 335804
    invoke-static {p0, v3}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 335805
    invoke-virtual {v3}, LX/523;->size()I

    move-result v2

    add-int/2addr v2, v0

    new-array v2, v2, [B

    .line 335806
    invoke-static {v1, p1, v2, p1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 335807
    invoke-virtual {v3, v2, v0}, LX/523;->a([BI)V

    move-object v1, v2

    .line 335808
    goto :goto_2
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 335783
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 335784
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 335785
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 335786
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Ljava/io/File;Ljava/io/File;)Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 335773
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 335774
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 335775
    if-eq p0, p1, :cond_0

    invoke-virtual {p0, p1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 335776
    :cond_0
    const/4 v0, 0x1

    .line 335777
    :goto_0
    return v0

    .line 335778
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 335779
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 335780
    cmp-long v4, v0, v6

    if-eqz v4, :cond_2

    cmp-long v4, v2, v6

    if-eqz v4, :cond_2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 335781
    const/4 v0, 0x0

    goto :goto_0

    .line 335782
    :cond_2
    invoke-static {p0}, LX/1t3;->a(Ljava/io/File;)LX/1vI;

    move-result-object v0

    invoke-static {p1}, LX/1t3;->a(Ljava/io/File;)LX/1vI;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1vI;->a(LX/1vI;)Z

    move-result v0

    goto :goto_0
.end method

.method public static b(Ljava/io/File;)[B
    .locals 1

    .prologue
    .line 335772
    invoke-static {p0}, LX/1t3;->a(Ljava/io/File;)LX/1vI;

    move-result-object v0

    invoke-virtual {v0}, LX/1vI;->b()[B

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/io/File;)V
    .locals 3

    .prologue
    .line 335765
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 335766
    invoke-virtual {p0}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 335767
    if-nez v0, :cond_1

    .line 335768
    :cond_0
    return-void

    .line 335769
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 335770
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_0

    .line 335771
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to create parent directories of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static c(Ljava/io/File;Ljava/io/File;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 335754
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 335755
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 335756
    invoke-virtual {p0, p1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Source %s and destination %s must be different"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v2

    aput-object p1, v4, v1

    invoke-static {v0, v3, v4}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 335757
    invoke-virtual {p0, p1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 335758
    invoke-static {p0, p1}, LX/1t3;->a(Ljava/io/File;Ljava/io/File;)V

    .line 335759
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_2

    .line 335760
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_1

    .line 335761
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to delete "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 335762
    goto :goto_0

    .line 335763
    :cond_1
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to delete "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 335764
    :cond_2
    return-void
.end method
