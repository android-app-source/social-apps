.class public LX/1Aq;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 211236
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 211237
    return-void
.end method

.method public static a(LX/0Zm;LX/0SG;)LX/1Ex;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211238
    new-instance v0, LX/1Ex;

    invoke-direct {v0, p0, p1}, LX/1Ex;-><init>(LX/0Zm;LX/0SG;)V

    return-object v0
.end method

.method public static a(LX/2WA;LX/0SG;LX/1Ex;)LX/1Ez;
    .locals 1
    .annotation runtime Lcom/facebook/imagepipeline/module/BitmapPoolStatsTracker;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211239
    const-string v0, "bitmap_pool_stats_counters"

    invoke-static {p0, p1, p2, v0}, LX/38I;->a(LX/2WA;LX/0SG;LX/1Ex;Ljava/lang/String;)LX/1Ez;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/WindowManager;Ljava/lang/Integer;)LX/1F7;
    .locals 8
    .param p1    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/common/executors/ImageDecodeExecutorService;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/imagepipeline/module/FlexByteArrayPoolParams;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211240
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/high16 v2, 0x400000

    .line 211241
    const/high16 v1, 0x10000

    .line 211242
    invoke-interface {p0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    .line 211243
    if-nez v3, :cond_1

    .line 211244
    const-string v3, "FbFlexByteArrayPoolParams"

    const-string v4, "Window manager passed down to Fresco has no display attached! Object of class %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 211245
    :cond_0
    :goto_0
    move v5, v1

    .line 211246
    invoke-static {v5, v2, v0}, LX/1F6;->a(III)Landroid/util/SparseIntArray;

    move-result-object v4

    .line 211247
    new-instance v1, LX/1F7;

    mul-int v3, v0, v2

    move v6, v2

    move v7, v0

    invoke-direct/range {v1 .. v7}, LX/1F7;-><init>(IILandroid/util/SparseIntArray;III)V

    move-object v0, v1

    .line 211248
    return-object v0

    .line 211249
    :cond_1
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 211250
    invoke-virtual {v3, v4}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 211251
    iget v3, v4, Landroid/graphics/Point;->x:I

    iget v4, v4, Landroid/graphics/Point;->y:I

    mul-int/2addr v3, v4

    .line 211252
    const v4, 0x7e900

    if-lt v3, v4, :cond_0

    .line 211253
    const v1, 0xfa000

    if-ge v3, v1, :cond_2

    .line 211254
    const/high16 v1, 0x20000

    goto :goto_0

    .line 211255
    :cond_2
    const/high16 v1, 0x40000

    goto :goto_0
.end method

.method public static a(LX/1F0;LX/1F0;LX/0rb;LX/1F0;LX/1F7;)LX/1FB;
    .locals 2
    .param p0    # LX/1F0;
        .annotation runtime Lcom/facebook/imagepipeline/module/BitmapPoolStatsTracker;
        .end annotation
    .end param
    .param p1    # LX/1F0;
        .annotation runtime Lcom/facebook/imagepipeline/module/SmallByteArrayPoolStatsTracker;
        .end annotation
    .end param
    .param p3    # LX/1F0;
        .annotation runtime Lcom/facebook/imagepipeline/module/NativeMemoryChunkPoolStatsTracker;
        .end annotation
    .end param
    .param p4    # LX/1F7;
        .annotation runtime Lcom/facebook/imagepipeline/module/FlexByteArrayPoolParams;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211256
    invoke-static {}, LX/1F8;->newBuilder()LX/1F9;

    move-result-object v0

    .line 211257
    invoke-static {p0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1F0;

    iput-object v1, v0, LX/1F9;->b:LX/1F0;

    .line 211258
    move-object v0, v0

    .line 211259
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1F0;

    iput-object v1, v0, LX/1F9;->h:LX/1F0;

    .line 211260
    move-object v0, v0

    .line 211261
    iput-object p2, v0, LX/1F9;->d:LX/0rb;

    .line 211262
    move-object v0, v0

    .line 211263
    invoke-static {p3}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1F0;

    iput-object v1, v0, LX/1F9;->f:LX/1F0;

    .line 211264
    move-object v0, v0

    .line 211265
    iput-object p4, v0, LX/1F9;->c:LX/1F7;

    .line 211266
    move-object v0, v0

    .line 211267
    invoke-virtual {v0}, LX/1F9;->a()LX/1F8;

    move-result-object v0

    .line 211268
    new-instance v1, LX/1FB;

    invoke-direct {v1, v0}, LX/1FB;-><init>(LX/1F8;)V

    return-object v1
.end method

.method public static a(LX/1GA;LX/1Ao;Landroid/content/Context;LX/0Uh;LX/1Ft;LX/1GF;LX/1GL;LX/0Or;LX/1Gf;Ljava/lang/Integer;LX/0Or;Ljava/lang/Boolean;LX/0rb;Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;LX/1FZ;LX/1FB;LX/1Gf;LX/1Gv;LX/0ad;Ljava/util/Set;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/03V;LX/0tK;LX/1Gx;LX/0Or;LX/1Gy;)LX/1FE;
    .locals 13
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/IsDialtonePhotoFeatureEnabled;
        .end annotation
    .end param
    .param p8    # LX/1Gf;
        .annotation build Lcom/facebook/imagepipeline/module/MainImageFileCache;
        .end annotation
    .end param
    .param p9    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/imagepipeline/module/MaxSmallImageBytes;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/imagepipeline/module/IsResizeAndRotateForNetworkImagesEnabled;
        .end annotation
    .end param
    .param p11    # Ljava/lang/Boolean;
        .annotation build Lcom/facebook/imagepipeline/module/IsMediaVariationsIndexEnabled;
        .end annotation
    .end param
    .param p16    # LX/1Gf;
        .annotation build Lcom/facebook/imagepipeline/module/ProfileThumbnailImageFileCache;
        .end annotation
    .end param
    .param p20    # LX/0Ot;
        .annotation build Lcom/facebook/crypto/module/SharedPrefsKey;
        .end annotation
    .end param
    .param p21    # LX/0Ot;
        .annotation build Lcom/facebook/crypto/module/FixedKey;
        .end annotation
    .end param
    .param p22    # LX/0Ot;
        .annotation build Lcom/facebook/crypto/module/FixedKey256;
        .end annotation
    .end param
    .param p28    # LX/0Or;
        .annotation runtime Lcom/facebook/imagepipeline/abtest/IsExternalBitmapCreationLoggingEnabled;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/animated/factory/AnimatedImageFactory;",
            "Lcom/facebook/imagepipeline/cache/CacheKeyFactory;",
            "Landroid/content/Context;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/1Ft;",
            "LX/1GF;",
            "LX/1GL;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1Gf;",
            "Ljava/lang/Integer;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/Boolean;",
            "LX/0rb;",
            "Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;",
            "LX/1FZ;",
            "LX/1FB;",
            "LX/1Gf;",
            "Lcom/facebook/imagepipeline/decoder/ProgressiveJpegConfig;",
            "LX/0ad;",
            "Ljava/util/Set",
            "<",
            "LX/1BU;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Hz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Hz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Hz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1IW;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2ux;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0tK;",
            "LX/1Gx;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1Gy;",
            ")",
            "LX/1FE;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211269
    const/16 v2, 0x34

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v2

    invoke-static {v2}, LX/1FJ;->a(Z)V

    .line 211270
    const/16 v2, 0xe

    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v2

    invoke-static {v2}, LX/1H1;->a(Z)V

    .line 211271
    new-instance v2, LX/1H2;

    move-object/from16 v0, p7

    move-object/from16 v1, p26

    invoke-direct {v2, v0, v1}, LX/1H2;-><init>(LX/0Or;LX/0tK;)V

    .line 211272
    new-instance v3, LX/1H3;

    new-instance v4, LX/1H4;

    invoke-direct {v4}, LX/1H4;-><init>()V

    invoke-direct {v3, v4}, LX/1H3;-><init>(LX/0Or;)V

    .line 211273
    new-instance v10, LX/1H5;

    move-object/from16 v0, p11

    invoke-direct {v10, v0}, LX/1H5;-><init>(Ljava/lang/Boolean;)V

    .line 211274
    sget-short v4, LX/1FD;->c:S

    const/4 v5, 0x0

    move-object/from16 v0, p18

    invoke-interface {v0, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v4

    .line 211275
    sget-short v5, LX/1FD;->B:S

    const/4 v6, 0x0

    move-object/from16 v0, p18

    invoke-interface {v0, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v11

    .line 211276
    invoke-static {p2}, LX/1H6;->a(Landroid/content/Context;)LX/1H8;

    move-result-object v5

    invoke-virtual {v5, p0}, LX/1H8;->a(LX/1GA;)LX/1H8;

    move-result-object v5

    invoke-virtual {v5, p1}, LX/1H8;->a(LX/1Ao;)LX/1H8;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1H8;->a(Z)LX/1H8;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, LX/1H8;->a(LX/1Ft;)LX/1H8;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, LX/1H8;->a(LX/1GF;)LX/1H8;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-virtual {v4, v0}, LX/1H8;->a(LX/1GL;)LX/1H8;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1H8;->b(LX/1Gd;)LX/1H8;

    move-result-object v2

    move-object/from16 v0, p8

    invoke-virtual {v2, v0}, LX/1H8;->a(LX/1Gf;)LX/1H8;

    move-result-object v2

    move-object/from16 v0, p12

    invoke-virtual {v2, v0}, LX/1H8;->a(LX/0rb;)LX/1H8;

    move-result-object v2

    move-object/from16 v0, p13

    invoke-virtual {v2, v0}, LX/1H8;->a(LX/1Gj;)LX/1H8;

    move-result-object v2

    move-object/from16 v0, p14

    invoke-virtual {v2, v0}, LX/1H8;->a(LX/1FZ;)LX/1H8;

    move-result-object v2

    move-object/from16 v0, p15

    invoke-virtual {v2, v0}, LX/1H8;->a(LX/1FB;)LX/1H8;

    move-result-object v2

    move-object/from16 v0, p17

    invoke-virtual {v2, v0}, LX/1H8;->a(LX/1Gv;)LX/1H8;

    move-result-object v2

    move-object/from16 v0, p19

    invoke-virtual {v2, v0}, LX/1H8;->a(Ljava/util/Set;)LX/1H8;

    move-result-object v4

    invoke-interface/range {p10 .. p10}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v4, v2}, LX/1H8;->b(Z)LX/1H8;

    move-result-object v2

    move-object/from16 v0, p16

    invoke-virtual {v2, v0}, LX/1H8;->b(LX/1Gf;)LX/1H8;

    move-result-object v2

    move-object/from16 v0, p29

    invoke-virtual {v2, v0}, LX/1H8;->a(LX/1Gd;)LX/1H8;

    move-result-object v12

    new-instance v2, LX/1HB;

    move-object/from16 v0, p27

    invoke-virtual {v0, v3}, LX/1Gx;->a(LX/0Or;)LX/1HD;

    move-result-object v3

    move-object/from16 v4, p20

    move-object/from16 v5, p21

    move-object/from16 v6, p22

    move-object/from16 v7, p23

    move-object/from16 v8, p24

    move-object/from16 v9, p25

    invoke-direct/range {v2 .. v9}, LX/1HB;-><init>(LX/1HC;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/03V;)V

    invoke-virtual {v12, v2}, LX/1H8;->a(LX/1HC;)LX/1H8;

    move-result-object v2

    invoke-virtual {v2}, LX/1H8;->b()LX/1H9;

    move-result-object v2

    sget-short v3, LX/1FD;->a:S

    const/4 v4, 0x0

    move-object/from16 v0, p18

    invoke-interface {v0, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-virtual {v2, v3}, LX/1H9;->a(Z)LX/1H8;

    move-result-object v2

    invoke-virtual {v2}, LX/1H8;->b()LX/1H9;

    move-result-object v2

    invoke-virtual/range {p9 .. p9}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, LX/1H9;->a(I)LX/1H8;

    move-result-object v2

    invoke-virtual {v2}, LX/1H8;->b()LX/1H9;

    move-result-object v3

    invoke-interface/range {p28 .. p28}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v3, v2}, LX/1H9;->b(Z)LX/1H8;

    move-result-object v2

    invoke-virtual {v2}, LX/1H8;->b()LX/1H9;

    move-result-object v2

    invoke-virtual {v2, v10}, LX/1H9;->a(LX/1Gd;)LX/1H8;

    move-result-object v2

    invoke-virtual {v2}, LX/1H8;->b()LX/1H9;

    move-result-object v2

    sget-short v3, LX/1FD;->d:S

    const/4 v4, 0x0

    move-object/from16 v0, p18

    invoke-interface {v0, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-virtual {v2, v3}, LX/1H9;->d(Z)LX/1H8;

    move-result-object v2

    invoke-virtual {v2}, LX/1H8;->b()LX/1H9;

    move-result-object v2

    sget-short v3, LX/1FD;->t:S

    const/4 v4, 0x0

    move-object/from16 v0, p18

    invoke-interface {v0, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-virtual {v2, v3}, LX/1H9;->f(Z)LX/1H8;

    move-result-object v2

    .line 211277
    invoke-virtual {v2}, LX/1H8;->b()LX/1H9;

    move-result-object v3

    invoke-virtual {v3, v11}, LX/1H9;->c(Z)LX/1H8;

    .line 211278
    if-eqz v11, :cond_0

    sget-boolean v3, LX/1cG;->a:Z

    if-eqz v3, :cond_0

    .line 211279
    new-instance v3, Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;

    invoke-direct {v3}, Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;-><init>()V

    .line 211280
    invoke-virtual {v2}, LX/1H8;->b()LX/1H9;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1H9;->a(Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;)LX/1H8;

    .line 211281
    invoke-virtual {v2}, LX/1H8;->b()LX/1H9;

    move-result-object v3

    new-instance v4, LX/4ec;

    move-object/from16 v0, p25

    invoke-direct {v4, v0}, LX/4ec;-><init>(LX/03V;)V

    invoke-virtual {v3, v4}, LX/1H9;->a(LX/4ec;)LX/1H8;

    .line 211282
    :cond_0
    const/16 v3, 0x25

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v3

    .line 211283
    invoke-virtual {v2}, LX/1H8;->b()LX/1H9;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1H9;->e(Z)LX/1H8;

    .line 211284
    invoke-virtual {v2}, LX/1H8;->c()LX/1H6;

    move-result-object v2

    invoke-static {v2}, LX/1FE;->a(LX/1H6;)V

    .line 211285
    invoke-static {}, LX/1FE;->a()LX/1FE;

    move-result-object v2

    return-object v2
.end method

.method public static a(LX/1FB;LX/0ad;Landroid/content/Context;)LX/1FG;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211286
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    .line 211287
    invoke-virtual {p0}, LX/1FB;->c()I

    move-result v0

    .line 211288
    new-instance v1, LX/4eE;

    invoke-virtual {p0}, LX/1FB;->a()LX/4eM;

    move-result-object p1

    new-instance p2, LX/0Zi;

    invoke-direct {p2, v0}, LX/0Zi;-><init>(I)V

    invoke-direct {v1, p1, v0, p2}, LX/4eE;-><init>(LX/4eM;ILX/0Zi;)V

    move-object v0, v1

    .line 211289
    :goto_0
    return-object v0

    .line 211290
    :cond_0
    sget-short v0, LX/1FD;->B:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 211291
    invoke-static {p0, v0}, LX/1FE;->a(LX/1FB;Z)LX/1FG;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/1bw;)LX/1G9;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211292
    new-instance v0, LX/4ed;

    invoke-direct {v0, p0}, LX/4ed;-><init>(LX/1bw;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;)LX/1GA;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211293
    invoke-virtual {p0}, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->a()LX/1GA;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0pi;LX/0Ot;LX/0Ot;Ljava/lang/String;LX/0Uh;Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;LX/0Ot;LX/0ad;)LX/1GE;
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/imagepipeline/module/CacheTrackerName;
        .end annotation
    .end param
    .annotation build Lcom/facebook/imagepipeline/module/PrimaryDiskCacheEventListener;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0pi;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;",
            "LX/0Ot",
            "<",
            "LX/1GD;",
            ">;",
            "LX/0ad;",
            ")",
            "LX/1GE;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 211294
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 211295
    const/16 v1, 0xe4

    invoke-virtual {p4, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 211296
    sget-short v1, LX/1FD;->b:S

    invoke-interface {p7, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 211297
    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211298
    :cond_0
    :goto_0
    sget-short v1, LX/1FD;->n:S

    invoke-interface {p7, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 211299
    invoke-interface {p6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211300
    :cond_1
    invoke-virtual {p0, p3}, LX/0pi;->a(Ljava/lang/String;)LX/1GE;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211301
    invoke-interface {v0, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211302
    new-instance v1, LX/1GY;

    invoke-direct {v1, v0}, LX/1GY;-><init>(Ljava/util/List;)V

    return-object v1

    .line 211303
    :cond_2
    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;LX/0pi;LX/1GQ;LX/0pq;)LX/1Gf;
    .locals 7
    .annotation build Lcom/facebook/imagepipeline/module/ProfileThumbnailImageFileCache;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    const-wide/32 v4, 0x400000

    .line 211384
    invoke-static {p0}, LX/1Gf;->a(Landroid/content/Context;)LX/1Gg;

    move-result-object v0

    const/4 v1, 0x1

    .line 211385
    iput v1, v0, LX/1Gg;->a:I

    .line 211386
    move-object v0, v0

    .line 211387
    const-string v1, "image"

    .line 211388
    iput-object v1, v0, LX/1Gg;->b:Ljava/lang/String;

    .line 211389
    move-object v0, v0

    .line 211390
    new-instance v1, LX/1Gr;

    invoke-direct {v1, p0}, LX/1Gr;-><init>(Landroid/content/Context;)V

    .line 211391
    iput-object v1, v0, LX/1Gg;->c:LX/1Gd;

    .line 211392
    move-object v0, v0

    .line 211393
    const-wide/32 v2, 0x40000

    .line 211394
    iput-wide v2, v0, LX/1Gg;->f:J

    .line 211395
    move-object v0, v0

    .line 211396
    iput-wide v4, v0, LX/1Gg;->e:J

    .line 211397
    move-object v0, v0

    .line 211398
    iput-wide v4, v0, LX/1Gg;->d:J

    .line 211399
    move-object v0, v0

    .line 211400
    iput-object p2, v0, LX/1Gg;->h:LX/1GQ;

    .line 211401
    move-object v0, v0

    .line 211402
    const-string v1, "profile_thumbnail_image_file"

    invoke-virtual {p1, v1}, LX/0pi;->a(Ljava/lang/String;)LX/1GE;

    move-result-object v1

    .line 211403
    iput-object v1, v0, LX/1Gg;->i:LX/1GE;

    .line 211404
    move-object v0, v0

    .line 211405
    iput-object p3, v0, LX/1Gg;->j:LX/0pr;

    .line 211406
    move-object v0, v0

    .line 211407
    invoke-virtual {v0}, LX/1Gg;->a()LX/1Gf;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LX/1GQ;LX/0pq;LX/1GT;LX/1GE;LX/0ad;LX/0Uh;LX/1GZ;LX/1Gb;)LX/1Gf;
    .locals 7
    .param p4    # LX/1GE;
        .annotation build Lcom/facebook/imagepipeline/module/PrimaryDiskCacheEventListener;
        .end annotation
    .end param
    .annotation build Lcom/facebook/imagepipeline/module/MainImageFileCache;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 211304
    sget-short v0, LX/1FD;->p:S

    invoke-interface {p5, v0, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 211305
    if-eqz v0, :cond_1

    sget-object v0, LX/1Ga;->FILES:LX/1Ga;

    .line 211306
    :goto_0
    :try_start_0
    iget-object v1, p7, LX/1GZ;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/1GZ;->b:LX/0Tn;

    sget-object v3, LX/1GZ;->c:LX/1Ga;

    invoke-virtual {v3}, LX/1Ga;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1Ga;->valueOf(Ljava/lang/String;)LX/1Ga;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 211307
    :goto_1
    move-object v1, v1

    .line 211308
    if-ne v1, v0, :cond_2

    .line 211309
    :cond_0
    :goto_2
    move-object v0, v0

    .line 211310
    invoke-static {p0}, LX/1Gf;->a(Landroid/content/Context;)LX/1Gg;

    move-result-object v1

    const/4 v2, 0x1

    .line 211311
    iput v2, v1, LX/1Gg;->a:I

    .line 211312
    move-object v1, v1

    .line 211313
    invoke-virtual {p8, v0}, LX/1Gb;->b(LX/1Ga;)Ljava/lang/String;

    move-result-object v2

    .line 211314
    iput-object v2, v1, LX/1Gg;->b:Ljava/lang/String;

    .line 211315
    move-object v1, v1

    .line 211316
    invoke-virtual {p8, v0}, LX/1Gb;->a(LX/1Ga;)LX/1Gd;

    move-result-object v0

    .line 211317
    iput-object v0, v1, LX/1Gg;->c:LX/1Gd;

    .line 211318
    move-object v0, v1

    .line 211319
    invoke-static {p5}, LX/1Aq;->d(LX/0ad;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 211320
    iput-wide v2, v0, LX/1Gg;->f:J

    .line 211321
    move-object v0, v0

    .line 211322
    invoke-static {p5}, LX/1Aq;->c(LX/0ad;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 211323
    iput-wide v2, v0, LX/1Gg;->e:J

    .line 211324
    move-object v0, v0

    .line 211325
    invoke-static {p5}, LX/1Aq;->b(LX/0ad;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 211326
    iput-wide v2, v0, LX/1Gg;->d:J

    .line 211327
    move-object v0, v0

    .line 211328
    iput-object p1, v0, LX/1Gg;->h:LX/1GQ;

    .line 211329
    move-object v0, v0

    .line 211330
    iput-object p4, v0, LX/1Gg;->i:LX/1GE;

    .line 211331
    move-object v0, v0

    .line 211332
    iput-object p2, v0, LX/1Gg;->j:LX/0pr;

    .line 211333
    move-object v0, v0

    .line 211334
    iput-object p3, v0, LX/1Gg;->g:LX/1GU;

    .line 211335
    move-object v0, v0

    .line 211336
    const/16 v1, 0x5d

    invoke-virtual {p6, v1, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 211337
    iput-boolean v1, v0, LX/1Gg;->k:Z

    .line 211338
    move-object v0, v0

    .line 211339
    invoke-virtual {v0}, LX/1Gg;->a()LX/1Gf;

    move-result-object v0

    return-object v0

    .line 211340
    :cond_1
    sget-object v0, LX/1Ga;->CACHE:LX/1Ga;

    goto :goto_0

    .line 211341
    :cond_2
    new-instance v3, Ljava/io/File;

    iget-object v2, p7, LX/1GZ;->e:LX/1Gb;

    invoke-virtual {v2, v1}, LX/1Gb;->a(LX/1Ga;)LX/1Gd;

    move-result-object v2

    invoke-interface {v2}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    iget-object v5, p7, LX/1GZ;->e:LX/1Gb;

    invoke-virtual {v5, v1}, LX/1Gb;->b(LX/1Ga;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v2, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 211342
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_4

    .line 211343
    const/4 v2, 0x1

    .line 211344
    :goto_3
    move v2, v2

    .line 211345
    if-eqz v2, :cond_3

    .line 211346
    iget-object v3, p7, LX/1GZ;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v5, LX/1GZ;->b:LX/0Tn;

    invoke-virtual {v0}, LX/1Ga;->name()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v5, v6}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 211347
    :cond_3
    if-nez v2, :cond_0

    move-object v0, v1

    goto/16 :goto_2

    .line 211348
    :catch_0
    iget-object v1, p7, LX/1GZ;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/1GZ;->b:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 211349
    sget-object v1, LX/1GZ;->c:LX/1Ga;

    goto/16 :goto_1

    .line 211350
    :cond_4
    new-instance v5, Ljava/io/File;

    iget-object v2, p7, LX/1GZ;->e:LX/1Gb;

    invoke-virtual {v2, v0}, LX/1Gb;->a(LX/1Ga;)LX/1Gd;

    move-result-object v2

    invoke-interface {v2}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    iget-object v6, p7, LX/1GZ;->e:LX/1Gb;

    invoke-virtual {v6, v0}, LX/1Gb;->b(LX/1Ga;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v2, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 211351
    invoke-virtual {v3, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    goto :goto_3
.end method

.method public static a(LX/1FE;)LX/1Ha;
    .locals 1
    .annotation build Lcom/facebook/imagepipeline/module/MainImageFileCache;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211352
    invoke-virtual {p0}, LX/1FE;->g()LX/1Ha;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;)LX/1c3;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211232
    invoke-virtual {p1, p0}, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->a(Landroid/content/Context;)LX/1c3;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0ad;)LX/2ux;
    .locals 4
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 211353
    sget-short v0, LX/1FD;->v:S

    invoke-interface {p0, v0, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 211354
    sget-short v1, LX/1FD;->u:S

    invoke-interface {p0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 211355
    sget-short v2, LX/1FD;->w:S

    invoke-interface {p0, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 211356
    if-nez v0, :cond_0

    .line 211357
    sget-object v0, LX/2ux;->NONE:LX/2ux;

    .line 211358
    :goto_0
    return-object v0

    .line 211359
    :cond_0
    if-eqz v1, :cond_2

    .line 211360
    if-eqz v2, :cond_1

    .line 211361
    sget-object v0, LX/2ux;->FIXED_KEY_128:LX/2ux;

    goto :goto_0

    .line 211362
    :cond_1
    sget-object v0, LX/2ux;->FIXED_KEY:LX/2ux;

    goto :goto_0

    .line 211363
    :cond_2
    sget-object v0, LX/2ux;->ENCRYPTED:LX/2ux;

    goto :goto_0
.end method

.method public static a(LX/1FB;)LX/4eM;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211364
    invoke-virtual {p0}, LX/1FB;->a()LX/4eM;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1FZ;LX/1Ft;)Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211365
    new-instance v0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;

    invoke-direct {v0, p0, p1}, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;-><init>(LX/1FZ;LX/1Ft;)V

    return-object v0
.end method

.method public static a(LX/0Uh;)Ljava/lang/Boolean;
    .locals 2
    .annotation runtime Lcom/facebook/imagepipeline/abtest/IsFrescoBitmapCacheLoggingEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 211366
    const/16 v0, 0x1a

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0W3;)Ljava/lang/Boolean;
    .locals 2
    .annotation build Lcom/facebook/imagepipeline/module/IsMediaVariationsIndexEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211367
    sget-wide v0, LX/0X5;->fj:J

    invoke-interface {p0, v0, v1}, LX/0W4;->a(J)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0ad;Landroid/view/WindowManager;)Ljava/lang/Integer;
    .locals 6
    .annotation runtime Lcom/facebook/imagepipeline/module/MaxSmallImageBytes;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 211368
    sget-short v0, LX/1FD;->A:S

    invoke-interface {p0, v0, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 211369
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 211370
    :goto_0
    return-object v0

    .line 211371
    :cond_0
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 211372
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 211373
    if-nez v1, :cond_1

    .line 211374
    const-string v1, "ImagePipelineModule"

    const-string v2, "Window manager passed down to Fresco has no display attached! Object of class %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 211375
    const/16 v1, 0x1e0

    const/16 v2, 0x320

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 211376
    :goto_1
    sget v1, LX/1FD;->y:F

    const v2, 0x3b2e7ba9    # 0.0026624f

    invoke-interface {p0, v1, v2}, LX/0ad;->a(FF)F

    move-result v1

    .line 211377
    sget v2, LX/1FD;->z:I

    const/16 v3, 0x726

    invoke-interface {p0, v2, v3}, LX/0ad;->a(II)I

    move-result v2

    .line 211378
    iget v3, v0, Landroid/graphics/Point;->x:I

    iget v4, v0, Landroid/graphics/Point;->y:I

    mul-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v1

    int-to-float v4, v2

    add-float/2addr v3, v4

    float-to-int v3, v3

    .line 211379
    move v0, v3

    .line 211380
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 211381
    :cond_1
    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    goto :goto_1
.end method

.method public static b(LX/2WA;LX/0SG;LX/1Ex;)LX/1Ez;
    .locals 1
    .annotation runtime Lcom/facebook/imagepipeline/module/SmallByteArrayPoolStatsTracker;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211382
    const-string v0, "common_byte_array_pool_stats_counters"

    invoke-static {p0, p1, p2, v0}, LX/38I;->a(LX/2WA;LX/0SG;LX/1Ex;Ljava/lang/String;)LX/1Ez;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/1FB;)LX/1FQ;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211383
    invoke-virtual {p0}, LX/1FB;->h()LX/1FQ;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0W3;)LX/1Gv;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211233
    new-instance v0, LX/1Gt;

    invoke-direct {v0, p0}, LX/1Gt;-><init>(LX/0W3;)V

    .line 211234
    new-instance v1, LX/1Gv;

    invoke-direct {v1, v0}, LX/1Gv;-><init>(LX/1Gu;)V

    return-object v1
.end method

.method public static b(LX/1FE;)LX/1Ha;
    .locals 1
    .annotation build Lcom/facebook/imagepipeline/module/ProfileThumbnailImageFileCache;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211235
    invoke-virtual {p0}, LX/1FE;->i()LX/1Ha;

    move-result-object v0

    return-object v0
.end method

.method public static b()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lcom/facebook/imagepipeline/module/IsResizeAndRotateForNetworkImagesEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211213
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0Uh;)Ljava/lang/Boolean;
    .locals 2
    .annotation runtime Lcom/facebook/imagepipeline/abtest/IsExternalBitmapCreationLoggingEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 211212
    const/16 v0, 0x6b

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/0ad;)Ljava/lang/Long;
    .locals 4

    .prologue
    .line 211211
    sget-wide v0, LX/1FD;->k:J

    const-wide/32 v2, 0x3c00000

    invoke-interface {p0, v0, v1, v2, v3}, LX/0ad;->a(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/2WA;LX/0SG;LX/1Ex;)LX/1Ez;
    .locals 1
    .annotation runtime Lcom/facebook/imagepipeline/module/NativeMemoryChunkPoolStatsTracker;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211210
    const-string v0, "native_memory_chunk_pool_stats_counters"

    invoke-static {p0, p1, p2, v0}, LX/38I;->a(LX/2WA;LX/0SG;LX/1Ex;Ljava/lang/String;)LX/1Ez;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/1FE;)LX/1Fg;
    .locals 1
    .annotation runtime Lcom/facebook/imagepipeline/module/SimpleImageMemoryCache;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211209
    invoke-virtual {p0}, LX/1FE;->e()LX/1Fg;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/1FB;)LX/1Fl;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211208
    invoke-virtual {p0}, LX/1FB;->f()LX/1Fl;

    move-result-object v0

    return-object v0
.end method

.method public static c()LX/1bw;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211207
    new-instance v0, LX/1bw;

    invoke-direct {v0}, LX/1bw;-><init>()V

    return-object v0
.end method

.method public static c(LX/0Uh;)Ljava/lang/Boolean;
    .locals 2
    .annotation runtime Lcom/facebook/imagepipeline/abtest/IsCloseableReferenceStatsLoggingEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 211206
    const/16 v0, 0x35

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method private static c(LX/0ad;)Ljava/lang/Long;
    .locals 4

    .prologue
    .line 211205
    sget-wide v0, LX/1FD;->l:J

    const-wide/32 v2, 0xf00000

    invoke-interface {p0, v0, v1, v2, v3}, LX/0ad;->a(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/1FE;)LX/1Fg;
    .locals 1
    .annotation runtime Lcom/facebook/imagepipeline/module/BitmapMemoryCache;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211204
    invoke-virtual {p0}, LX/1FE;->c()LX/1Fg;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/1FB;)LX/1Fk;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211203
    invoke-virtual {p0}, LX/1FB;->d()LX/1Fk;

    move-result-object v0

    return-object v0
.end method

.method public static d()LX/1Ih;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211214
    new-instance v0, LX/1Ih;

    invoke-direct {v0}, LX/1Ih;-><init>()V

    return-object v0
.end method

.method public static d(LX/0Uh;)Ljava/lang/Boolean;
    .locals 2
    .annotation runtime Lcom/facebook/imagepipeline/abtest/IsExperimentalZoomableDraweeTouchHandlingEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 211215
    const/16 v0, 0x6a

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method private static d(LX/0ad;)Ljava/lang/Long;
    .locals 4

    .prologue
    .line 211216
    sget-wide v0, LX/1FD;->m:J

    const-wide/32 v2, 0x200000

    invoke-interface {p0, v0, v1, v2, v3}, LX/0ad;->a(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public static e()LX/1BS;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211217
    new-instance v0, LX/1BS;

    invoke-direct {v0}, LX/1BS;-><init>()V

    return-object v0
.end method

.method public static e(LX/1FE;)LX/1Fh;
    .locals 1
    .annotation runtime Lcom/facebook/imagepipeline/module/SimpleImageMemoryCache;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211218
    invoke-virtual {p0}, LX/1FE;->f()LX/1Fh;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/1FB;)LX/1Fj;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211219
    invoke-virtual {p0}, LX/1FB;->e()LX/1Fj;

    move-result-object v0

    return-object v0
.end method

.method public static f(LX/1FB;)LX/1FO;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211220
    invoke-virtual {p0}, LX/1FB;->b()LX/1FO;

    move-result-object v0

    return-object v0
.end method

.method public static f(LX/1FE;)LX/1Fh;
    .locals 1
    .annotation runtime Lcom/facebook/imagepipeline/module/BitmapMemoryCache;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211221
    invoke-virtual {p0}, LX/1FE;->d()LX/1Fh;

    move-result-object v0

    return-object v0
.end method

.method public static g(LX/1FE;)LX/1HI;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211222
    invoke-virtual {p0}, LX/1FE;->h()LX/1HI;

    move-result-object v0

    return-object v0
.end method

.method public static g(LX/1FB;)LX/4eS;
    .locals 4
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211223
    iget-object v0, p0, LX/1FB;->g:LX/4eS;

    if-nez v0, :cond_0

    .line 211224
    new-instance v0, LX/4eS;

    iget-object v1, p0, LX/1FB;->a:LX/1F8;

    .line 211225
    iget-object v2, v1, LX/1F8;->d:LX/0rb;

    move-object v1, v2

    .line 211226
    iget-object v2, p0, LX/1FB;->a:LX/1F8;

    .line 211227
    iget-object v3, v2, LX/1F8;->c:LX/1F7;

    move-object v2, v3

    .line 211228
    invoke-direct {v0, v1, v2}, LX/4eS;-><init>(LX/0rb;LX/1F7;)V

    iput-object v0, p0, LX/1FB;->g:LX/4eS;

    .line 211229
    :cond_0
    iget-object v0, p0, LX/1FB;->g:LX/4eS;

    move-object v0, v0

    .line 211230
    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 211231
    return-void
.end method
