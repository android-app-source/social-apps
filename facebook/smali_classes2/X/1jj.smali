.class public LX/1jj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final b:Ljava/lang/Object;


# instance fields
.field public final a:LX/0uf;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 300816
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/1jj;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0uf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 300846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 300847
    iput-object p1, p0, LX/1jj;->a:LX/0uf;

    .line 300848
    return-void
.end method

.method public static a(LX/0QB;)LX/1jj;
    .locals 7

    .prologue
    .line 300817
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 300818
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 300819
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 300820
    if-nez v1, :cond_0

    .line 300821
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300822
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 300823
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 300824
    sget-object v1, LX/1jj;->b:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 300825
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 300826
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 300827
    :cond_1
    if-nez v1, :cond_4

    .line 300828
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 300829
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 300830
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 300831
    new-instance p0, LX/1jj;

    invoke-static {v0}, LX/0ts;->a(LX/0QB;)LX/0uf;

    move-result-object v1

    check-cast v1, LX/0uf;

    invoke-direct {p0, v1}, LX/1jj;-><init>(LX/0uf;)V

    .line 300832
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 300833
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 300834
    if-nez v1, :cond_2

    .line 300835
    sget-object v0, LX/1jj;->b:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1jj;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 300836
    :goto_1
    if-eqz v0, :cond_3

    .line 300837
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 300838
    :goto_3
    check-cast v0, LX/1jj;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 300839
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 300840
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 300841
    :catchall_1
    move-exception v0

    .line 300842
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 300843
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 300844
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 300845
    :cond_2
    :try_start_8
    sget-object v0, LX/1jj;->b:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1jj;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method
