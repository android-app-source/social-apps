.class public LX/1AQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/1AQ;


# instance fields
.field public final a:LX/0wY;

.field public final b:LX/0wa;

.field public c:J

.field public d:Z


# direct methods
.method public constructor <init>(LX/0wY;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 210469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210470
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/1AQ;->c:J

    .line 210471
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1AQ;->d:Z

    .line 210472
    iput-object p1, p0, LX/1AQ;->a:LX/0wY;

    .line 210473
    new-instance v0, LX/1AR;

    invoke-direct {v0, p0}, LX/1AR;-><init>(LX/1AQ;)V

    move-object v0, v0

    .line 210474
    iput-object v0, p0, LX/1AQ;->b:LX/0wa;

    .line 210475
    return-void
.end method

.method public static a(LX/0QB;)LX/1AQ;
    .locals 4

    .prologue
    .line 210476
    sget-object v0, LX/1AQ;->e:LX/1AQ;

    if-nez v0, :cond_1

    .line 210477
    const-class v1, LX/1AQ;

    monitor-enter v1

    .line 210478
    :try_start_0
    sget-object v0, LX/1AQ;->e:LX/1AQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 210479
    if-eqz v2, :cond_0

    .line 210480
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 210481
    new-instance p0, LX/1AQ;

    invoke-static {v0}, LX/0wX;->a(LX/0QB;)LX/0wX;

    move-result-object v3

    check-cast v3, LX/0wY;

    invoke-direct {p0, v3}, LX/1AQ;-><init>(LX/0wY;)V

    .line 210482
    move-object v0, p0

    .line 210483
    sput-object v0, LX/1AQ;->e:LX/1AQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210484
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 210485
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 210486
    :cond_1
    sget-object v0, LX/1AQ;->e:LX/1AQ;

    return-object v0

    .line 210487
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 210488
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
