.class public LX/0a7;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 84344
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 84345
    return-void
.end method

.method public static a(LX/0Uh;Ljava/util/concurrent/Executor;)LX/0a8;
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 84341
    new-instance v0, LX/0a8;

    invoke-direct {v0, p1}, LX/0a8;-><init>(Ljava/util/concurrent/Executor;)V

    .line 84342
    invoke-virtual {p0, v0}, LX/0Uh;->a(LX/0a8;)V

    .line 84343
    return-object v0
.end method

.method public static b(LX/0Uh;Ljava/util/concurrent/Executor;)LX/0a8;
    .locals 1
    .param p0    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 84338
    new-instance v0, LX/0a8;

    invoke-direct {v0, p1}, LX/0a8;-><init>(Ljava/util/concurrent/Executor;)V

    .line 84339
    invoke-virtual {p0, v0}, LX/0Uh;->a(LX/0a8;)V

    .line 84340
    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 84337
    return-void
.end method
