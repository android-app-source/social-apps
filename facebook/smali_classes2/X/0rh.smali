.class public LX/0rh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 150055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;LX/0SG;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 150056
    invoke-interface {p1}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 150057
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    .line 150058
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Z
    .locals 2

    .prologue
    .line 150059
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    instance-of v0, v0, LX/16n;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    instance-of v0, v0, LX/16n;

    if-nez v0, :cond_1

    .line 150060
    :cond_0
    const/4 v0, 0x0

    .line 150061
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 150062
    instance-of p0, v0, LX/16n;

    if-eqz p0, :cond_2

    instance-of p0, v1, LX/16n;

    if-eqz p0, :cond_2

    move-object p0, v0

    check-cast p0, LX/16n;

    invoke-interface {p0}, LX/16n;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p0

    if-eqz p0, :cond_2

    move-object p0, v1

    check-cast p0, LX/16n;

    invoke-interface {p0}, LX/16n;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p0

    if-nez p0, :cond_3

    .line 150063
    :cond_2
    const/4 p0, 0x0

    .line 150064
    :goto_1
    move v0, p0

    .line 150065
    goto :goto_0

    .line 150066
    :cond_3
    check-cast v0, LX/16n;

    invoke-interface {v0}, LX/16n;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/4Zt;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 150067
    check-cast v1, LX/16n;

    invoke-interface {v1}, LX/16n;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/4Zt;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 150068
    invoke-static {p0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    goto :goto_1
.end method
