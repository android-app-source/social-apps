.class public LX/1VJ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3AB;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1VJ",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/3AB;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 258854
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 258855
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1VJ;->b:LX/0Zi;

    .line 258856
    iput-object p1, p0, LX/1VJ;->a:LX/0Ot;

    .line 258857
    return-void
.end method

.method public static a(LX/0QB;)LX/1VJ;
    .locals 4

    .prologue
    .line 258843
    const-class v1, LX/1VJ;

    monitor-enter v1

    .line 258844
    :try_start_0
    sget-object v0, LX/1VJ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 258845
    sput-object v2, LX/1VJ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 258846
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258847
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 258848
    new-instance v3, LX/1VJ;

    const/16 p0, 0x92a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1VJ;-><init>(LX/0Ot;)V

    .line 258849
    move-object v0, v3

    .line 258850
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 258851
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1VJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 258852
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 258853
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(Landroid/view/View;LX/1X1;)V
    .locals 9

    .prologue
    .line 258833
    check-cast p2, LX/3A9;

    .line 258834
    iget-object v0, p0, LX/1VJ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AB;

    iget-object v1, p2, LX/3A9;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/3A9;->c:LX/3AH;

    .line 258835
    new-instance v3, LX/8pR;

    .line 258836
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 258837
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/8pK;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;

    move-result-object v4

    iget-object v5, v0, LX/3AB;->g:Landroid/content/Context;

    iget-object v6, v0, LX/3AB;->d:LX/3AE;

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iget-object v8, v0, LX/3AB;->f:LX/17W;

    invoke-direct/range {v3 .. v8}, LX/8pR;-><init>(Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;Landroid/content/Context;LX/3AE;Ljava/lang/Boolean;LX/17W;)V

    .line 258838
    iget-object v4, v3, LX/8pR;->b:LX/8pV;

    move-object v4, v4

    .line 258839
    new-instance v5, LX/C7R;

    invoke-direct {v5, v0, v1, v2}, LX/C7R;-><init>(LX/3AB;Lcom/facebook/feed/rows/core/props/FeedProps;LX/3AH;)V

    .line 258840
    iput-object v5, v4, LX/8pV;->e:LX/8pU;

    .line 258841
    invoke-virtual {v3, p1}, LX/8pR;->a(Landroid/view/View;)V

    .line 258842
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 258826
    check-cast p2, LX/3A9;

    .line 258827
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 258828
    iget-object v0, p0, LX/1VJ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AB;

    iget-object v2, p2, LX/3A9;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/3A9;->b:LX/1Ps;

    invoke-virtual {v0, p1, v2, v3, v1}, LX/3AB;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;LX/1np;)LX/1Dg;

    move-result-object v2

    .line 258829
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 258830
    check-cast v0, LX/3AH;

    iput-object v0, p2, LX/3A9;->c:LX/3AH;

    .line 258831
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 258832
    return-object v2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 258795
    invoke-static {}, LX/1dS;->b()V

    .line 258796
    iget v0, p1, LX/1dQ;->b:I

    .line 258797
    sparse-switch v0, :sswitch_data_0

    .line 258798
    :goto_0
    return-object v2

    .line 258799
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 258800
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 258801
    check-cast v1, LX/3A9;

    .line 258802
    iget-object v3, p0, LX/1VJ;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3AB;

    iget-object v4, v1, LX/3A9;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v1, LX/3A9;->b:LX/1Ps;

    iget-object p2, v1, LX/3A9;->c:LX/3AH;

    const/4 v0, 0x1

    .line 258803
    iput-boolean v0, p2, LX/3AH;->b:Z

    .line 258804
    move-object p0, p1

    .line 258805
    check-cast p0, LX/1Pq;

    new-array v0, v0, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    aput-object v4, v0, v1

    invoke-interface {p0, v0}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 258806
    iget-object p0, v3, LX/3AB;->b:LX/3AC;

    new-instance v0, LX/C7Q;

    invoke-direct {v0, v3, p1, v4}, LX/C7Q;-><init>(LX/3AB;LX/1Ps;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 258807
    iput-object v0, p0, LX/3AC;->d:LX/C7Q;

    .line 258808
    iget-object v0, v3, LX/3AB;->b:LX/3AC;

    .line 258809
    iget-object p0, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 258810
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 258811
    iget-object v3, v0, LX/3AC;->b:LX/0tX;

    .line 258812
    new-instance v1, LX/81N;

    invoke-direct {v1}, LX/81N;-><init>()V

    move-object v1, v1

    .line 258813
    const-string v4, "story_id"

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v4, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    check-cast v1, LX/81N;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 258814
    iget-object v3, v0, LX/3AC;->c:LX/0Sh;

    new-instance v4, LX/C7S;

    invoke-direct {v4, v0, p2}, LX/C7S;-><init>(LX/3AC;LX/3AH;)V

    invoke-virtual {v3, v1, v4}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 258815
    goto :goto_0

    .line 258816
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 258817
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/1VJ;->b(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x44c5ee4 -> :sswitch_1
        0x5a8b92f -> :sswitch_0
    .end sparse-switch
.end method

.method public final c(LX/1De;)LX/3AA;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1VJ",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 258818
    new-instance v1, LX/3A9;

    invoke-direct {v1, p0}, LX/3A9;-><init>(LX/1VJ;)V

    .line 258819
    iget-object v2, p0, LX/1VJ;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3AA;

    .line 258820
    if-nez v2, :cond_0

    .line 258821
    new-instance v2, LX/3AA;

    invoke-direct {v2, p0}, LX/3AA;-><init>(LX/1VJ;)V

    .line 258822
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/3AA;->a$redex0(LX/3AA;LX/1De;IILX/3A9;)V

    .line 258823
    move-object v1, v2

    .line 258824
    move-object v0, v1

    .line 258825
    return-object v0
.end method
