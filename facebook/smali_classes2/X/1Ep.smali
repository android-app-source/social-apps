.class public LX/1Ep;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0Zb;

.field private final c:LX/0Zm;

.field public final d:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/util/Random;

.field public final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1Eq;",
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "LX/0m9;",
            ">;>;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final j:Landroid/os/Handler;

.field private final k:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 221253
    const-class v0, LX/1Ep;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1Ep;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0Zm;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 221256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221257
    new-instance v1, Lcom/facebook/analytics/PrefetchAnalytics$1;

    invoke-direct {v1, p0}, Lcom/facebook/analytics/PrefetchAnalytics$1;-><init>(LX/1Ep;)V

    iput-object v1, p0, LX/1Ep;->k:Ljava/lang/Runnable;

    .line 221258
    iput-object p1, p0, LX/1Ep;->b:LX/0Zb;

    .line 221259
    iput-object p2, p0, LX/1Ep;->c:LX/0Zm;

    .line 221260
    iput-object p3, p0, LX/1Ep;->j:Landroid/os/Handler;

    .line 221261
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    iput-object v1, p0, LX/1Ep;->g:Ljava/util/Random;

    .line 221262
    iput-object p4, p0, LX/1Ep;->e:Ljava/lang/String;

    .line 221263
    iput-object p5, p0, LX/1Ep;->f:Ljava/lang/String;

    .line 221264
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, LX/1Ep;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 221265
    const-class v1, LX/1Eq;

    invoke-static {v1}, LX/0PM;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v1

    iput-object v1, p0, LX/1Ep;->h:Ljava/util/Map;

    .line 221266
    invoke-static {}, LX/1Eq;->values()[LX/1Eq;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 221267
    iget-object v4, p0, LX/1Ep;->h:Ljava/util/Map;

    new-instance v5, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v5}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221268
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 221269
    :cond_0
    new-instance v0, LX/0S8;

    invoke-direct {v0}, LX/0S8;-><init>()V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/0S8;->c(I)LX/0S8;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/0S8;->a(I)LX/0S8;

    move-result-object v0

    invoke-virtual {v0}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/1Ep;->d:Ljava/util/concurrent/ConcurrentMap;

    .line 221270
    return-void
.end method

.method public static a(LX/1Ep;LX/1Eq;LX/0m9;)V
    .locals 5

    .prologue
    .line 221206
    iget-object v0, p0, LX/1Ep;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 221207
    iget-object v0, p0, LX/1Ep;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221208
    iget-object v0, p0, LX/1Ep;->j:Landroid/os/Handler;

    iget-object v1, p0, LX/1Ep;->k:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    const v4, -0x7e057cd9

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 221209
    :cond_0
    return-void
.end method

.method public static a(LX/1Ep;LX/1Eq;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 221254
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-static/range {v0 .. v6}, LX/1Ep;->a(LX/1Ep;LX/1Eq;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 221255
    return-void
.end method

.method public static a(LX/1Ep;LX/1Eq;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 221238
    if-eqz p2, :cond_0

    invoke-static {p0}, LX/1Ep;->c(LX/1Ep;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 221239
    :cond_0
    :goto_0
    return-void

    .line 221240
    :cond_1
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 221241
    const-string v1, "cache_key"

    invoke-virtual {v0, v1, p2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 221242
    if-eqz p3, :cond_2

    .line 221243
    const-string v1, "prefetch_duration_ms"

    invoke-virtual {v0, v1, p3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Long;)LX/0m9;

    .line 221244
    :cond_2
    if-eqz p4, :cond_3

    .line 221245
    const-string v1, "prefetch_bytes"

    invoke-virtual {v0, v1, p4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Long;)LX/0m9;

    .line 221246
    :cond_3
    if-eqz p5, :cond_4

    .line 221247
    const-string v1, "prefetch_batch_id"

    invoke-virtual {v0, v1, p5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Integer;)LX/0m9;

    .line 221248
    :cond_4
    if-eqz p6, :cond_5

    .line 221249
    const-string v1, "prefetch_source"

    invoke-virtual {v0, v1, p6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 221250
    :cond_5
    const-string v1, "action_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 221251
    invoke-static {p0, p1, v0}, LX/1Ep;->a(LX/1Ep;LX/1Eq;LX/0m9;)V

    goto :goto_0
.end method

.method public static c(LX/1Ep;)Z
    .locals 2

    .prologue
    .line 221252
    iget-object v0, p0, LX/1Ep;->c:LX/0Zm;

    const-string v1, "prefetch_cache_efficiency"

    invoke-virtual {v0, v1}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static g(LX/1Ep;Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 221233
    iget-object v0, p0, LX/1Ep;->d:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 221234
    const-wide/16 v2, 0x0

    .line 221235
    if-eqz v0, :cond_0

    .line 221236
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    .line 221237
    :goto_0
    return-wide v0

    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 221220
    if-nez p1, :cond_0

    .line 221221
    :goto_0
    return-void

    .line 221222
    :cond_0
    invoke-static {p0, p1}, LX/1Ep;->g(LX/1Ep;Ljava/lang/String;)J

    move-result-wide v2

    .line 221223
    sget-object v1, LX/1Ep;->a:Ljava/lang/String;

    const-string v4, "failed prefetching %s after %d ms for reason: %s"

    const/4 v0, 0x3

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p1, v5, v0

    const/4 v0, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v6, 0x2

    if-nez p2, :cond_2

    const-string v0, ""

    :goto_1
    aput-object v0, v5, v6

    invoke-static {v1, v4, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 221224
    if-eqz p1, :cond_1

    invoke-static {p0}, LX/1Ep;->c(LX/1Ep;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 221225
    :cond_1
    :goto_2
    goto :goto_0

    :cond_2
    move-object v0, p2

    .line 221226
    goto :goto_1

    .line 221227
    :cond_3
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 221228
    const-string v1, "cache_key"

    invoke-virtual {v0, v1, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 221229
    const-string v1, "prefetch_duration_ms"

    invoke-virtual {v0, v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 221230
    if-eqz p2, :cond_4

    .line 221231
    const-string v1, "failure_reason"

    invoke-virtual {v0, v1, p2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 221232
    :cond_4
    sget-object v1, LX/1Eq;->PREFETCH_FAILED:LX/1Eq;

    invoke-static {p0, v1, v0}, LX/1Ep;->a(LX/1Ep;LX/1Eq;LX/0m9;)V

    goto :goto_2
.end method

.method public final b(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 221215
    const/4 v5, 0x0

    .line 221216
    invoke-virtual {p0, p1}, LX/1Ep;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v2, LX/1Eq;->CACHE_MISS_PREFETCH_IN_FLIGHT:LX/1Eq;

    :goto_0
    move-object v1, p0

    move-object v3, p1

    move-object v4, v0

    move-object v6, v5

    move-object v7, v0

    .line 221217
    invoke-static/range {v1 .. v7}, LX/1Ep;->a(LX/1Ep;LX/1Eq;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 221218
    return-void

    .line 221219
    :cond_0
    sget-object v2, LX/1Eq;->CACHE_MISS:LX/1Eq;

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 221211
    const/4 v0, 0x0

    .line 221212
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 221213
    sget-object v3, LX/1Eq;->CACHE_STALE_HIT:LX/1Eq;

    move-object v2, p0

    move-object v4, p1

    move-object v5, v1

    move-object v7, v6

    move-object v8, v0

    invoke-static/range {v2 .. v8}, LX/1Ep;->a(LX/1Ep;LX/1Eq;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 221214
    return-void
.end method

.method public final d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 221210
    iget-object v0, p0, LX/1Ep;->d:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
