.class public LX/0xa;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 163293
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 163294
    return-void
.end method

.method public static a(LX/0Or;)Ljava/lang/Boolean;
    .locals 2
    .param p0    # LX/0Or;
        .annotation runtime Lcom/facebook/fbreact/annotations/IsFb4aReactNativeMasterGkEnabled;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/fbreact/annotations/IsFb4aReactNativeEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .prologue
    .line 163296
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-gt v0, v1, :cond_0

    .line 163297
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 163298
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 163295
    return-void
.end method
