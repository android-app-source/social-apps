.class public final enum LX/12j;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/12j;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/12j;

.field public static final enum BACKSTAGE:LX/12j;

.field public static final enum FEED:LX/12j;

.field public static final enum FRIEND_REQUESTS:LX/12j;

.field public static final enum INBOX:LX/12j;

.field public static final enum MARKETPLACE:LX/12j;

.field public static final enum NOTIFICATIONS:LX/12j;

.field private static final PREF_PREFIX:LX/0Tn;

.field public static final enum VIDEO_HOME:LX/12j;


# instance fields
.field public final graphName:Ljava/lang/String;

.field private final mPrefKey:Ljava/lang/String;

.field private final mStyleIndex:I


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 175688
    new-instance v0, LX/12j;

    const-string v1, "INBOX"

    const-string v2, "inbox"

    invoke-direct {v0, v1, v5, v2, v5}, LX/12j;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/12j;->INBOX:LX/12j;

    .line 175689
    new-instance v0, LX/12j;

    const-string v1, "FRIEND_REQUESTS"

    const-string v2, "friendrequests"

    invoke-direct {v0, v1, v6, v2, v6}, LX/12j;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/12j;->FRIEND_REQUESTS:LX/12j;

    .line 175690
    new-instance v0, LX/12j;

    const-string v1, "NOTIFICATIONS"

    const-string v2, "notifications"

    invoke-direct {v0, v1, v7, v2, v7}, LX/12j;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/12j;->NOTIFICATIONS:LX/12j;

    .line 175691
    new-instance v0, LX/12j;

    const-string v1, "FEED"

    const-string v2, "feed"

    invoke-direct {v0, v1, v9, v2, v8}, LX/12j;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/12j;->FEED:LX/12j;

    .line 175692
    new-instance v0, LX/12j;

    const-string v1, "BACKSTAGE"

    const-string v2, "backstage"

    const/4 v3, 0x7

    invoke-direct {v0, v1, v8, v2, v3}, LX/12j;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/12j;->BACKSTAGE:LX/12j;

    .line 175693
    new-instance v0, LX/12j;

    const-string v1, "VIDEO_HOME"

    const/4 v2, 0x5

    const-string v3, "videohome"

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, LX/12j;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/12j;->VIDEO_HOME:LX/12j;

    .line 175694
    new-instance v0, LX/12j;

    const-string v1, "MARKETPLACE"

    const/4 v2, 0x6

    const-string v3, "marketplace"

    const/16 v4, 0xa

    invoke-direct {v0, v1, v2, v3, v4}, LX/12j;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/12j;->MARKETPLACE:LX/12j;

    .line 175695
    const/4 v0, 0x7

    new-array v0, v0, [LX/12j;

    sget-object v1, LX/12j;->INBOX:LX/12j;

    aput-object v1, v0, v5

    sget-object v1, LX/12j;->FRIEND_REQUESTS:LX/12j;

    aput-object v1, v0, v6

    sget-object v1, LX/12j;->NOTIFICATIONS:LX/12j;

    aput-object v1, v0, v7

    sget-object v1, LX/12j;->FEED:LX/12j;

    aput-object v1, v0, v9

    sget-object v1, LX/12j;->BACKSTAGE:LX/12j;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/12j;->VIDEO_HOME:LX/12j;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/12j;->MARKETPLACE:LX/12j;

    aput-object v2, v0, v1

    sput-object v0, LX/12j;->$VALUES:[LX/12j;

    .line 175696
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "jewels/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/12j;->PREF_PREFIX:LX/0Tn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 175715
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 175716
    iput-object p3, p0, LX/12j;->graphName:Ljava/lang/String;

    .line 175717
    iput-object p3, p0, LX/12j;->mPrefKey:Ljava/lang/String;

    .line 175718
    iput p4, p0, LX/12j;->mStyleIndex:I

    .line 175719
    return-void
.end method

.method public static forCountPrefKey(LX/0Tn;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Tn;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/12j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175709
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 175710
    invoke-static {}, LX/12j;->values()[LX/12j;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 175711
    invoke-static {v4}, LX/12j;->getCountPrefKey(LX/12j;)LX/0Tn;

    move-result-object v5

    invoke-virtual {v5, p0}, LX/0Tn;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 175712
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175713
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 175714
    :cond_1
    return-object v1
.end method

.method public static forIndex(I)LX/12j;
    .locals 5

    .prologue
    .line 175704
    invoke-static {}, LX/12j;->values()[LX/12j;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 175705
    iget v4, v0, LX/12j;->mStyleIndex:I

    if-ne v4, p0, :cond_0

    .line 175706
    :goto_1
    return-object v0

    .line 175707
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 175708
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static getCountPrefKey(LX/12j;)LX/0Tn;
    .locals 2

    .prologue
    .line 175720
    sget-object v0, LX/12j;->PREF_PREFIX:LX/0Tn;

    iget-object v1, p0, LX/12j;->mPrefKey:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "/count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static getCountPrefKeys()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175699
    invoke-static {}, LX/12j;->values()[LX/12j;

    move-result-object v0

    array-length v0, v0

    invoke-static {v0}, LX/0RA;->a(I)Ljava/util/HashSet;

    move-result-object v1

    .line 175700
    invoke-static {}, LX/12j;->values()[LX/12j;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 175701
    invoke-static {v4}, LX/12j;->getCountPrefKey(LX/12j;)LX/0Tn;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 175702
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 175703
    :cond_0
    return-object v1
.end method

.method public static valueOf(Ljava/lang/String;)LX/12j;
    .locals 1

    .prologue
    .line 175698
    const-class v0, LX/12j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/12j;

    return-object v0
.end method

.method public static values()[LX/12j;
    .locals 1

    .prologue
    .line 175697
    sget-object v0, LX/12j;->$VALUES:[LX/12j;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/12j;

    return-object v0
.end method
