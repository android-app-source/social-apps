.class public LX/1iI;
.super Ljava/io/OutputStream;
.source ""


# instance fields
.field public final a:Z

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 297694
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 297695
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1iI;->b:Ljava/util/ArrayList;

    .line 297696
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1iI;->a:Z

    .line 297697
    iget-object v0, p0, LX/1iI;->b:Ljava/util/ArrayList;

    const/16 v1, 0x400

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 297698
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 297699
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 297700
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1iI;->b:Ljava/util/ArrayList;

    .line 297701
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1iI;->a:Z

    .line 297702
    iget-object v0, p0, LX/1iI;->b:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 297703
    return-void
.end method

.method private c()Ljava/nio/ByteBuffer;
    .locals 2

    .prologue
    .line 297704
    iget-object v0, p0, LX/1iI;->b:Ljava/util/ArrayList;

    iget-object v1, p0, LX/1iI;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method private d()Ljava/nio/ByteBuffer;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 297705
    iget-boolean v0, p0, LX/1iI;->a:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 297706
    invoke-direct {p0}, LX/1iI;->c()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 297707
    const/16 v0, 0x400

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 297708
    iget-object v1, p0, LX/1iI;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 297709
    return-object v0

    :cond_0
    move v0, v2

    .line 297710
    goto :goto_0

    :cond_1
    move v1, v2

    .line 297711
    goto :goto_1
.end method


# virtual methods
.method public final a()[Ljava/nio/ByteBuffer;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 297712
    iget-boolean v0, p0, LX/1iI;->a:Z

    if-eqz v0, :cond_0

    .line 297713
    iget-object v0, p0, LX/1iI;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 297714
    iget-object v0, p0, LX/1iI;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 297715
    :cond_0
    iget-object v0, p0, LX/1iI;->b:Ljava/util/ArrayList;

    iget-object v1, p0, LX/1iI;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/nio/ByteBuffer;

    return-object v0

    :cond_1
    move v0, v2

    .line 297716
    goto :goto_0

    :cond_2
    move v1, v2

    .line 297717
    goto :goto_1
.end method

.method public final b()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 297718
    move v1, v0

    move v2, v0

    .line 297719
    :goto_0
    iget-object v0, p0, LX/1iI;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 297720
    iget-object v0, p0, LX/1iI;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    add-int/2addr v2, v0

    .line 297721
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 297722
    :cond_0
    return v2
.end method

.method public final write(I)V
    .locals 2

    .prologue
    .line 297723
    invoke-direct {p0}, LX/1iI;->c()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 297724
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    if-nez v1, :cond_0

    .line 297725
    invoke-direct {p0}, LX/1iI;->d()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 297726
    :cond_0
    int-to-byte v1, p1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 297727
    return-void
.end method

.method public final write([BII)V
    .locals 2

    .prologue
    .line 297728
    :goto_0
    if-lez p3, :cond_1

    .line 297729
    invoke-direct {p0}, LX/1iI;->c()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 297730
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    if-nez v1, :cond_0

    .line 297731
    invoke-direct {p0}, LX/1iI;->d()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 297732
    :cond_0
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    invoke-static {v1, p3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 297733
    invoke-virtual {v0, p1, p2, v1}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 297734
    add-int/2addr p2, v1

    .line 297735
    sub-int/2addr p3, v1

    .line 297736
    goto :goto_0

    .line 297737
    :cond_1
    return-void
.end method
