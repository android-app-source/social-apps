.class public LX/0gd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0gd;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/0kb;

.field private final c:LX/0oz;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0kb;LX/0oz;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 112700
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112701
    iput-object p1, p0, LX/0gd;->a:LX/0Zb;

    .line 112702
    iput-object p2, p0, LX/0gd;->b:LX/0kb;

    .line 112703
    iput-object p3, p0, LX/0gd;->c:LX/0oz;

    .line 112704
    return-void
.end method

.method public static a(LX/0QB;)LX/0gd;
    .locals 6

    .prologue
    .line 112713
    sget-object v0, LX/0gd;->d:LX/0gd;

    if-nez v0, :cond_1

    .line 112714
    const-class v1, LX/0gd;

    monitor-enter v1

    .line 112715
    :try_start_0
    sget-object v0, LX/0gd;->d:LX/0gd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 112716
    if-eqz v2, :cond_0

    .line 112717
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 112718
    new-instance p0, LX/0gd;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v5

    check-cast v5, LX/0oz;

    invoke-direct {p0, v3, v4, v5}, LX/0gd;-><init>(LX/0Zb;LX/0kb;LX/0oz;)V

    .line 112719
    move-object v0, p0

    .line 112720
    sput-object v0, LX/0gd;->d:LX/0gd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 112721
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 112722
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 112723
    :cond_1
    sget-object v0, LX/0gd;->d:LX/0gd;

    return-object v0

    .line 112724
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 112725
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0gd;LX/324;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V
    .locals 4

    .prologue
    .line 112705
    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/324;->a(LX/2rt;)LX/324;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/324;->a(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)LX/324;

    move-result-object v0

    iget-object v1, p0, LX/0gd;->c:LX/0oz;

    invoke-virtual {v1}, LX/0oz;->c()LX/0p3;

    move-result-object v1

    invoke-virtual {v1}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v1

    .line 112706
    iget-object v2, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "connection_class"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 112707
    move-object v0, v0

    .line 112708
    iget-object v1, p0, LX/0gd;->b:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/324;->g(Ljava/lang/String;)LX/324;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v0, v1}, LX/324;->a(LX/2rw;)LX/324;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-wide v2, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-virtual {v0, v2, v3}, LX/324;->a(J)LX/324;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/324;->l(Z)LX/324;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLegacyApiStoryId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/324;->s(Ljava/lang/String;)LX/324;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 112709
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v2

    .line 112710
    :goto_0
    iget-object v2, v1, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "posting_as_page_id"

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 112711
    return-void

    .line 112712
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0gd;LX/324;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V
    .locals 2

    .prologue
    .line 112698
    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/324;->a(LX/2rt;)LX/324;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/324;->a(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)LX/324;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/324;->l(Z)LX/324;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLegacyApiStoryId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/324;->s(Ljava/lang/String;)LX/324;

    move-result-object v0

    iget-object v1, p0, LX/0gd;->b:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/324;->g(Ljava/lang/String;)LX/324;

    .line 112699
    return-void
.end method


# virtual methods
.method public final a(LX/0ge;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 112691
    iget-object v0, p0, LX/0gd;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v2, p1, LX/0ge;->name:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "composer"

    .line 112692
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 112693
    move-object v1, v1

    .line 112694
    iput-object p2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 112695
    move-object v1, v1

    .line 112696
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 112697
    return-void
.end method

.method public final a(LX/0ge;Ljava/lang/String;JLX/2rt;)V
    .locals 3

    .prologue
    .line 112687
    invoke-static {p1, p2}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, LX/324;->a(J)LX/324;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/324;->a(LX/2rt;)LX/324;

    move-result-object v0

    .line 112688
    iget-object v1, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, v1

    .line 112689
    iget-object v1, p0, LX/0gd;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 112690
    return-void
.end method

.method public final a(LX/0ge;Ljava/lang/String;ZZI)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 112677
    invoke-static {p2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v0

    .line 112678
    iget-object v1, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "has_post_params_extra"

    invoke-virtual {v1, p1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 112679
    move-object v0, v0

    .line 112680
    iget-object v1, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "has_post_params"

    invoke-virtual {v1, p1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 112681
    move-object v0, v0

    .line 112682
    iget-object v1, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "result_code"

    invoke-virtual {v1, p1, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 112683
    move-object v0, v0

    .line 112684
    iget-object v1, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, v1

    .line 112685
    iget-object v1, p0, LX/0gd;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 112686
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0Px;LX/0Px;ZZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 112663
    sget-object v0, LX/0ge;->COMPOSER_MEDIA_PICKER_SELECTION_CHANGE:LX/0ge;

    invoke-static {v0, p1}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v0

    .line 112664
    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-virtual {v1}, LX/0mC;->b()LX/162;

    move-result-object v3

    .line 112665
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result p1

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, p1, :cond_0

    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 112666
    invoke-virtual {v3, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 112667
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 112668
    :cond_0
    if-eqz p4, :cond_1

    const-string v1, "video_items_added"

    .line 112669
    :goto_1
    iget-object v2, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {v2, v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 112670
    move-object v0, v0

    .line 112671
    invoke-virtual {v0, p3, p5}, LX/324;->b(LX/0Px;Z)LX/324;

    move-result-object v0

    .line 112672
    iget-object v1, p0, LX/0gd;->a:LX/0Zb;

    .line 112673
    iget-object v2, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, v2

    .line 112674
    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 112675
    return-void

    .line 112676
    :cond_1
    const-string v1, "photo_items_added"

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;LX/2rt;DF)V
    .locals 7

    .prologue
    .line 112655
    sget-object v0, LX/0ge;->COMPOSER_PAUSE:LX/0ge;

    invoke-static {v0, p1}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v0

    iget-object v1, p0, LX/0gd;->b:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/324;->g(Ljava/lang/String;)LX/324;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/324;->a(LX/2rt;)LX/324;

    move-result-object v0

    .line 112656
    iget-object v1, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "scroll_percent"

    invoke-virtual {v1, v2, p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 112657
    move-object v0, v0

    .line 112658
    iget-object v3, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "min_aspect_ratio"

    float-to-double v5, p5

    invoke-virtual {v3, v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 112659
    move-object v0, v0

    .line 112660
    iget-object v1, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, v1

    .line 112661
    iget-object v1, p0, LX/0gd;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 112662
    return-void
.end method

.method public final a(Ljava/lang/String;LX/2rt;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 112726
    sget-object v0, LX/0ge;->COMPOSER_POST_SUCCESS:LX/0ge;

    invoke-static {v0, p1}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v0

    iget-object v1, p0, LX/0gd;->b:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/324;->g(Ljava/lang/String;)LX/324;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/324;->b(Ljava/lang/String;)LX/324;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/324;->a(LX/2rt;)LX/324;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/324;->c(Ljava/lang/String;)LX/324;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/324;->a(I)LX/324;

    move-result-object v0

    .line 112727
    iget-object v1, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, v1

    .line 112728
    iget-object v1, p0, LX/0gd;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 112729
    sget-object v0, LX/0ge;->COMPOSER_POST_COMPLETED:LX/0ge;

    invoke-virtual {p0, v0, p1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 112730
    return-void
.end method

.method public final a(Ljava/lang/String;LX/2rt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/fbservice/service/ServiceException;I)V
    .locals 4

    .prologue
    .line 112638
    sget-object v0, LX/0ge;->COMPOSER_POST_FAILURE:LX/0ge;

    invoke-static {v0, p1}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v0

    iget-object v1, p0, LX/0gd;->b:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/324;->g(Ljava/lang/String;)LX/324;

    move-result-object v0

    .line 112639
    iget-object v1, p6, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v1, v1

    .line 112640
    iget-object v2, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "error_code"

    invoke-virtual {v1}, LX/1nY;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 112641
    move-object v0, v0

    .line 112642
    iget-object v1, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "error_message"

    invoke-virtual {v1, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 112643
    move-object v0, v0

    .line 112644
    invoke-virtual {v0, p4}, LX/324;->b(Ljava/lang/String;)LX/324;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/324;->a(LX/2rt;)LX/324;

    move-result-object v0

    invoke-virtual {v0, p7}, LX/324;->a(I)LX/324;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/324;->c(Ljava/lang/String;)LX/324;

    move-result-object v1

    .line 112645
    iget-object v0, p6, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 112646
    iget-object v2, v0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    move-object v2, v2

    .line 112647
    if-eqz v2, :cond_0

    .line 112648
    const-string v0, "originalExceptionMessage"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 112649
    iget-object v3, v1, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "exception_message"

    invoke-virtual {v3, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 112650
    const-string v0, "originalExceptionStack"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 112651
    iget-object v2, v1, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "exception_stack"

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 112652
    :cond_0
    iget-object v0, v1, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, v0

    .line 112653
    iget-object v1, p0, LX/0gd;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 112654
    return-void
.end method

.method public final a(Ljava/lang/String;LX/5Ro;)V
    .locals 2

    .prologue
    .line 112632
    sget-object v0, LX/0ge;->COMPOSER_POST_RETRY:LX/0ge;

    invoke-static {v0, p1}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v0

    iget-object v1, p0, LX/0gd;->b:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/324;->g(Ljava/lang/String;)LX/324;

    move-result-object v0

    .line 112633
    iget-object v1, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "retry_source"

    invoke-virtual {v1, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 112634
    move-object v0, v0

    .line 112635
    iget-object v1, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, v1

    .line 112636
    iget-object v1, p0, LX/0gd;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 112637
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V
    .locals 2

    .prologue
    .line 112626
    sget-object v0, LX/0ge;->COMPOSER_ENTRY:LX/0ge;

    invoke-static {v0, p1}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v0

    .line 112627
    invoke-static {p0, v0, p2}, LX/0gd;->a(LX/0gd;LX/324;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V

    .line 112628
    iget-object v1, p0, LX/0gd;->a:LX/0Zb;

    .line 112629
    iget-object p0, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, p0

    .line 112630
    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 112631
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;IJJLX/1M1;ZIZZZZZLX/0P1;Ljava/lang/String;IZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration;",
            "IJJ",
            "LX/1M1",
            "<",
            "LX/7mH;",
            ">;ZIZZZZZ",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            "IZ)V"
        }
    .end annotation

    .prologue
    .line 112621
    sget-object v1, LX/0ge;->COMPOSER_CANCELLED:LX/0ge;

    invoke-static {v1, p1}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v1

    .line 112622
    invoke-static {p0, v1, p2}, LX/0gd;->b(LX/0gd;LX/324;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V

    .line 112623
    invoke-virtual {v1, p3}, LX/324;->b(I)LX/324;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, LX/324;->a(J)LX/324;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, LX/324;->b(J)LX/324;

    move-result-object v2

    invoke-virtual {v2, p6, p7}, LX/324;->c(J)LX/324;

    move-result-object v2

    invoke-virtual {v2, p8}, LX/324;->a(LX/1M1;)LX/324;

    move-result-object v2

    invoke-virtual {v2, p9}, LX/324;->a(Z)LX/324;

    move-result-object v2

    invoke-virtual {v2, p10}, LX/324;->c(I)LX/324;

    move-result-object v2

    invoke-virtual {v2, p11}, LX/324;->b(Z)LX/324;

    move-result-object v2

    move/from16 v0, p12

    invoke-virtual {v2, v0}, LX/324;->c(Z)LX/324;

    move-result-object v2

    move/from16 v0, p13

    invoke-virtual {v2, v0}, LX/324;->d(Z)LX/324;

    move-result-object v2

    move/from16 v0, p14

    invoke-virtual {v2, v0}, LX/324;->e(Z)LX/324;

    move-result-object v2

    move/from16 v0, p15

    invoke-virtual {v2, v0}, LX/324;->f(Z)LX/324;

    move-result-object v2

    move-object/from16 v0, p16

    invoke-virtual {v2, v0}, LX/324;->a(LX/0P1;)LX/324;

    move-result-object v2

    move-object/from16 v0, p17

    invoke-virtual {v2, v0}, LX/324;->j(Ljava/lang/String;)LX/324;

    move-result-object v2

    move/from16 v0, p18

    invoke-virtual {v2, v0}, LX/324;->d(I)LX/324;

    move-result-object v2

    move/from16 v0, p19

    invoke-virtual {v2, v0}, LX/324;->m(Z)LX/324;

    .line 112624
    iget-object v2, p0, LX/0gd;->a:LX/0Zb;

    invoke-virtual {v1}, LX/324;->a()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 112625
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;LX/2rw;JLX/1M1;LX/0P1;Ljava/lang/String;IZLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;)V
    .locals 6
    .param p13    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p14    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p15    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p16    # Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration;",
            "LX/2rw;",
            "J",
            "LX/1M1",
            "<",
            "LX/7mH;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            "IZ",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 112614
    sget-object v2, LX/0ge;->COMPOSER_POST:LX/0ge;

    invoke-static {v2, p1}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v2

    iget-object v3, p0, LX/0gd;->b:LX/0kb;

    invoke-virtual {v3}, LX/0kb;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/324;->g(Ljava/lang/String;)LX/324;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/324;->a(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)LX/324;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/324;->a(LX/2rt;)LX/324;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/324;->l(Z)LX/324;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLegacyApiStoryId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/324;->s(Ljava/lang/String;)LX/324;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/324;->a(LX/2rw;)LX/324;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, LX/324;->a(J)LX/324;

    move-result-object v2

    invoke-virtual {v2, p6}, LX/324;->a(LX/1M1;)LX/324;

    move-result-object v2

    invoke-virtual {v2, p7}, LX/324;->a(LX/0P1;)LX/324;

    move-result-object v2

    invoke-virtual {v2, p8}, LX/324;->j(Ljava/lang/String;)LX/324;

    move-result-object v2

    invoke-virtual {v2, p9}, LX/324;->d(I)LX/324;

    move-result-object v2

    move/from16 v0, p10

    invoke-virtual {v2, v0}, LX/324;->i(Z)LX/324;

    move-result-object v2

    move-object/from16 v0, p11

    invoke-virtual {v2, v0}, LX/324;->l(Ljava/lang/String;)LX/324;

    move-result-object v2

    move/from16 v0, p12

    invoke-virtual {v2, v0}, LX/324;->e(I)LX/324;

    move-result-object v2

    move-object/from16 v0, p13

    invoke-virtual {v2, v0}, LX/324;->n(Ljava/lang/String;)LX/324;

    move-result-object v2

    move-object/from16 v0, p14

    invoke-virtual {v2, v0}, LX/324;->o(Ljava/lang/String;)LX/324;

    move-result-object v2

    move-object/from16 v0, p15

    invoke-virtual {v2, v0}, LX/324;->r(Ljava/lang/String;)LX/324;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldUsePublishExperiment()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/324;->n(Z)LX/324;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getSouvenirUniqueId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/324;->u(Ljava/lang/String;)LX/324;

    move-result-object v2

    .line 112615
    if-eqz p16, :cond_0

    .line 112616
    invoke-virtual/range {p16 .. p16}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->hasFaceboxes()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/324;->g(Z)LX/324;

    .line 112617
    invoke-virtual/range {p16 .. p16}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->hasFaceDetectionFinished()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/324;->h(Z)LX/324;

    .line 112618
    invoke-virtual/range {p16 .. p16}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->getTimeToFindFirstFaceMs()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, LX/324;->d(J)LX/324;

    .line 112619
    :cond_0
    iget-object v3, p0, LX/0gd;->a:LX/0Zb;

    invoke-virtual {v2}, LX/324;->a()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 112620
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "logPublishFlow"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation

    .prologue
    .line 112608
    sget-object v0, LX/0ge;->COMPOSER_PUBLISH_FLOW:LX/0ge;

    invoke-static {v0, p1}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v0

    .line 112609
    iget-object v1, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "publish_flow"

    invoke-static {p2}, LX/8vK;->a(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 112610
    move-object v0, v0

    .line 112611
    iget-object v1, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, v1

    .line 112612
    iget-object v1, p0, LX/0gd;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 112613
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 112601
    sget-object v0, LX/0ge;->TEXT_ONLY_PLACE_POSTED:LX/0ge;

    invoke-static {v0, p1}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v0

    .line 112602
    iget-object v1, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "text_only_name"

    invoke-virtual {v1, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 112603
    move-object v0, v0

    .line 112604
    iget-object v1, p0, LX/0gd;->a:LX/0Zb;

    .line 112605
    iget-object p0, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, p0

    .line 112606
    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 112607
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 112583
    if-eqz p3, :cond_0

    sget-object v0, LX/0ge;->COMPOSER_REMOVE_VIDEO:LX/0ge;

    .line 112584
    :goto_0
    invoke-static {v0, p1}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v0

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, LX/324;->b(LX/0Px;Z)LX/324;

    move-result-object v0

    .line 112585
    iget-object v1, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, v1

    .line 112586
    iget-object v1, p0, LX/0gd;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 112587
    return-void

    .line 112588
    :cond_0
    sget-object v0, LX/0ge;->COMPOSER_REMOVE_PHOTO:LX/0ge;

    goto :goto_0
.end method

.method public final b(LX/0ge;Ljava/lang/String;JLX/2rt;)V
    .locals 3

    .prologue
    .line 112595
    sget-object v0, LX/0ge;->COMPOSER_DISCARD_DIALOG_DISPLAYED:LX/0ge;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/0ge;->COMPOSER_DISCARD_DIALOG_DISMISSED:LX/0ge;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, LX/0ge;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a discard dialog event"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 112596
    invoke-static {p1, p2}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, LX/324;->a(J)LX/324;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/324;->a(LX/2rt;)LX/324;

    move-result-object v0

    .line 112597
    iget-object v1, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, v1

    .line 112598
    iget-object v1, p0, LX/0gd;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 112599
    return-void

    .line 112600
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V
    .locals 2

    .prologue
    .line 112589
    sget-object v0, LX/0ge;->COMPOSER_CANCELLED:LX/0ge;

    invoke-static {v0, p1}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v0

    .line 112590
    invoke-static {p0, v0, p2}, LX/0gd;->b(LX/0gd;LX/324;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V

    .line 112591
    iget-object v1, p0, LX/0gd;->a:LX/0Zb;

    .line 112592
    iget-object p0, v0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, p0

    .line 112593
    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 112594
    return-void
.end method
