.class public LX/10N;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/0Uh;

.field private final d:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0Uh;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/katana/annotations/DeviceBasedLoginKillSwitch;
        .end annotation
    .end param
    .param p3    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 168911
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168912
    iput-object p1, p0, LX/10N;->a:LX/0Or;

    .line 168913
    iput-object p2, p0, LX/10N;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 168914
    iput-object p3, p0, LX/10N;->c:LX/0Uh;

    .line 168915
    iput-object p4, p0, LX/10N;->d:LX/0Uh;

    .line 168916
    return-void
.end method

.method public static a(LX/0QB;)LX/10N;
    .locals 1

    .prologue
    .line 168910
    invoke-static {p0}, LX/10N;->b(LX/0QB;)LX/10N;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/10N;
    .locals 5

    .prologue
    .line 168908
    new-instance v3, LX/10N;

    const/16 v0, 0x324

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-direct {v3, v4, v0, v1, v2}, LX/10N;-><init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0Uh;)V

    .line 168909
    return-object v3
.end method

.method private s()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 168907
    iget-object v0, p0, LX/10N;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->z:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 168906
    iget-object v0, p0, LX/10N;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 168905
    iget-object v0, p0, LX/10N;->c:LX/0Uh;

    const/16 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 168904
    invoke-virtual {p0}, LX/10N;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/10N;->c:LX/0Uh;

    const/16 v2, 0x13

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 168917
    invoke-virtual {p0}, LX/10N;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168918
    iget-object v0, p0, LX/10N;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->k:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    move-object v0, v0

    .line 168919
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 168903
    invoke-virtual {p0}, LX/10N;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/10N;->s()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 3

    .prologue
    .line 168902
    iget-object v0, p0, LX/10N;->d:LX/0Uh;

    const/16 v1, 0x576

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 3

    .prologue
    .line 168901
    iget-object v0, p0, LX/10N;->c:LX/0Uh;

    const/16 v1, 0x23

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 168900
    iget-object v1, p0, LX/10N;->c:LX/0Uh;

    const/16 v2, 0x28

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/10N;->c:LX/0Uh;

    const/16 v2, 0x29

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/10N;->c:LX/0Uh;

    const/16 v2, 0x22

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final j()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 168899
    iget-object v1, p0, LX/10N;->c:LX/0Uh;

    const/16 v2, 0x29

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/10N;->c:LX/0Uh;

    const/16 v2, 0x22

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 168898
    invoke-virtual {p0}, LX/10N;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/10N;->s()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 3

    .prologue
    .line 168897
    iget-object v0, p0, LX/10N;->c:LX/0Uh;

    const/16 v1, 0x22

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final o()Z
    .locals 3

    .prologue
    .line 168896
    iget-object v0, p0, LX/10N;->c:LX/0Uh;

    const/16 v1, 0x2f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
