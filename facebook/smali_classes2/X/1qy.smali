.class public LX/1qy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qx;


# instance fields
.field private final a:LX/0yc;

.field private final b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(LX/0yc;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 331582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331583
    iput-object p1, p0, LX/1qy;->a:LX/0yc;

    .line 331584
    iput-object p2, p0, LX/1qy;->b:Lcom/facebook/content/SecureContextHelper;

    .line 331585
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;ILandroid/app/Activity;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 331586
    iget-object v1, p0, LX/1qy;->a:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1qy;->a:LX/0yc;

    invoke-static {p1, v1}, LX/5N1;->a(Landroid/content/Intent;LX/0yc;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 331587
    iget-object v1, p0, LX/1qy;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-static {p3, p1, v0, v0}, LX/5N1;->a(Landroid/content/Context;Landroid/content/Intent;IZ)Landroid/content/Intent;

    move-result-object v0

    invoke-interface {v1, v0, p3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 331588
    const/4 v0, 0x1

    .line 331589
    :cond_0
    return v0
.end method

.method public final a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 331590
    iget-object v1, p0, LX/1qy;->a:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1qy;->a:LX/0yc;

    invoke-static {p1, v1}, LX/5N1;->a(Landroid/content/Intent;LX/0yc;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 331591
    iget-object v1, p0, LX/1qy;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1, v0, v0}, LX/5N1;->a(Landroid/content/Context;Landroid/content/Intent;IZ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 331592
    const/4 v0, 0x1

    .line 331593
    :cond_0
    return v0
.end method

.method public final a(Landroid/content/Intent;Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 331594
    iget-object v1, p0, LX/1qy;->a:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1qy;->a:LX/0yc;

    invoke-static {p1, v1}, LX/5N1;->a(Landroid/content/Intent;LX/0yc;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 331595
    iget-object v1, p0, LX/1qy;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-static {p2, p1, v0, v0}, LX/5N1;->a(Landroid/content/Context;Landroid/content/Intent;IZ)Landroid/content/Intent;

    move-result-object v0

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 331596
    const/4 v0, 0x1

    .line 331597
    :cond_0
    return v0
.end method

.method public final b(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 331598
    const/4 v0, 0x0

    return-object v0
.end method
