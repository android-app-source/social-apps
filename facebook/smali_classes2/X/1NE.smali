.class public final enum LX/1NE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1NE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1NE;

.field public static final enum DB_CACHE:LX/1NE;

.field public static final enum MEMORY_CACHE:LX/1NE;

.field public static final enum NETWORK:LX/1NE;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 237066
    new-instance v0, LX/1NE;

    const-string v1, "MEMORY_CACHE"

    invoke-direct {v0, v1, v2}, LX/1NE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1NE;->MEMORY_CACHE:LX/1NE;

    .line 237067
    new-instance v0, LX/1NE;

    const-string v1, "DB_CACHE"

    invoke-direct {v0, v1, v3}, LX/1NE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1NE;->DB_CACHE:LX/1NE;

    .line 237068
    new-instance v0, LX/1NE;

    const-string v1, "NETWORK"

    invoke-direct {v0, v1, v4}, LX/1NE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1NE;->NETWORK:LX/1NE;

    .line 237069
    const/4 v0, 0x3

    new-array v0, v0, [LX/1NE;

    sget-object v1, LX/1NE;->MEMORY_CACHE:LX/1NE;

    aput-object v1, v0, v2

    sget-object v1, LX/1NE;->DB_CACHE:LX/1NE;

    aput-object v1, v0, v3

    sget-object v1, LX/1NE;->NETWORK:LX/1NE;

    aput-object v1, v0, v4

    sput-object v0, LX/1NE;->$VALUES:[LX/1NE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 237070
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1NE;
    .locals 1

    .prologue
    .line 237071
    const-class v0, LX/1NE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1NE;

    return-object v0
.end method

.method public static values()[LX/1NE;
    .locals 1

    .prologue
    .line 237072
    sget-object v0, LX/1NE;->$VALUES:[LX/1NE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1NE;

    return-object v0
.end method
