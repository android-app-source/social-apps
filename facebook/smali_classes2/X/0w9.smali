.class public LX/0w9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/0w9;


# instance fields
.field private final a:LX/0sa;

.field private final b:LX/0rq;

.field private final c:LX/0se;

.field private final d:LX/0wA;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0y2;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yD;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0ad;


# direct methods
.method public constructor <init>(LX/0sa;LX/0rq;LX/0se;LX/0wA;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0sa;",
            "LX/0rq;",
            "LX/0se;",
            "LX/0wA;",
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0y2;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0yD;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 159445
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159446
    iput-object p1, p0, LX/0w9;->a:LX/0sa;

    .line 159447
    iput-object p2, p0, LX/0w9;->b:LX/0rq;

    .line 159448
    iput-object p3, p0, LX/0w9;->c:LX/0se;

    .line 159449
    iput-object p4, p0, LX/0w9;->d:LX/0wA;

    .line 159450
    iput-object p6, p0, LX/0w9;->f:LX/0Ot;

    .line 159451
    iput-object p5, p0, LX/0w9;->e:LX/0Ot;

    .line 159452
    iput-object p7, p0, LX/0w9;->g:LX/0Ot;

    .line 159453
    iput-object p8, p0, LX/0w9;->h:LX/0ad;

    .line 159454
    return-void
.end method

.method public static final a(LX/0gW;)LX/0gW;
    .locals 2

    .prologue
    .line 159442
    const-string v0, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 159443
    const-string v0, "action_location"

    sget-object v1, LX/0wD;->NEWSFEED:LX/0wD;

    invoke-virtual {v1}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 159444
    return-object p0
.end method

.method public static a(LX/0gW;LX/0dC;)LX/0gW;
    .locals 2

    .prologue
    .line 159440
    const-string v0, "device_id"

    invoke-virtual {p1}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 159441
    return-object p0
.end method

.method public static a(LX/0gW;LX/0oz;)LX/0gW;
    .locals 2

    .prologue
    .line 159436
    invoke-virtual {p1}, LX/0oz;->c()LX/0p3;

    move-result-object v0

    .line 159437
    sget-object v1, LX/0p3;->UNKNOWN:LX/0p3;

    invoke-virtual {v1, v0}, LX/0p3;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 159438
    const-string v1, "connection_class"

    invoke-virtual {v0}, LX/0p3;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 159439
    :cond_0
    return-object p0
.end method

.method public static a(LX/0gW;LX/0u7;)LX/0gW;
    .locals 7

    .prologue
    const/high16 v6, -0x40800000    # -1.0f

    .line 159416
    invoke-virtual {p1}, LX/0u7;->b()LX/0y1;

    move-result-object v1

    .line 159417
    invoke-virtual {p1}, LX/0u7;->a()F

    move-result v2

    .line 159418
    sget-object v0, LX/0y1;->UNKNOWN:LX/0y1;

    if-ne v1, v0, :cond_0

    cmpl-float v0, v2, v6

    if-nez v0, :cond_0

    .line 159419
    :goto_0
    return-object p0

    .line 159420
    :cond_0
    new-instance v3, Ljava/io/StringWriter;

    const/16 v0, 0x28

    invoke-direct {v3, v0}, Ljava/io/StringWriter;-><init>(I)V

    .line 159421
    const/16 v0, 0x7b

    invoke-virtual {v3, v0}, Ljava/io/StringWriter;->write(I)V

    .line 159422
    sget-object v0, LX/0y1;->UNKNOWN:LX/0y1;

    if-eq v1, v0, :cond_1

    .line 159423
    const/4 v0, 0x0

    .line 159424
    sget-object v4, LX/0yC;->a:[I

    invoke-virtual {v1}, LX/0y1;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 159425
    :goto_1
    const-string v4, "is_charging:"

    invoke-virtual {v3, v4}, Ljava/io/StringWriter;->write(Ljava/lang/String;)V

    .line 159426
    if-eqz v0, :cond_4

    const-string v0, "\"true\""

    :goto_2
    invoke-virtual {v3, v0}, Ljava/io/StringWriter;->write(Ljava/lang/String;)V

    .line 159427
    :cond_1
    cmpl-float v0, v2, v6

    if-eqz v0, :cond_3

    .line 159428
    sget-object v0, LX/0y1;->UNKNOWN:LX/0y1;

    if-eq v1, v0, :cond_2

    .line 159429
    const/16 v0, 0x2c

    invoke-virtual {v3, v0}, Ljava/io/StringWriter;->write(I)V

    .line 159430
    :cond_2
    const-string v0, "battery_level:"

    invoke-virtual {v3, v0}, Ljava/io/StringWriter;->write(Ljava/lang/String;)V

    .line 159431
    const/high16 v0, 0x42c80000    # 100.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/StringWriter;->write(Ljava/lang/String;)V

    .line 159432
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {v3, v0}, Ljava/io/StringWriter;->append(C)Ljava/io/StringWriter;

    .line 159433
    const-string v0, "battery_context"

    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    goto :goto_0

    .line 159434
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_1

    .line 159435
    :cond_4
    const-string v0, "\"false\""

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;Ljava/lang/String;)LX/0gW;
    .locals 1
    .param p1    # Lcom/facebook/api/feed/FetchFeedParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 159407
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 159408
    if-eqz v0, :cond_0

    .line 159409
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 159410
    invoke-virtual {p0, p2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 159411
    :cond_0
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 159412
    if-eqz v0, :cond_1

    .line 159413
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 159414
    invoke-virtual {p0, p3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 159415
    :cond_1
    return-object p0
.end method

.method public static a(LX/0QB;)LX/0w9;
    .locals 12

    .prologue
    .line 159354
    sget-object v0, LX/0w9;->i:LX/0w9;

    if-nez v0, :cond_1

    .line 159355
    const-class v1, LX/0w9;

    monitor-enter v1

    .line 159356
    :try_start_0
    sget-object v0, LX/0w9;->i:LX/0w9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 159357
    if-eqz v2, :cond_0

    .line 159358
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 159359
    new-instance v3, LX/0w9;

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v4

    check-cast v4, LX/0sa;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v5

    check-cast v5, LX/0rq;

    invoke-static {v0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v6

    check-cast v6, LX/0se;

    invoke-static {v0}, LX/0wA;->a(LX/0QB;)LX/0wA;

    move-result-object v7

    check-cast v7, LX/0wA;

    const/16 v8, 0x245

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xc7e

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xc89

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-direct/range {v3 .. v11}, LX/0w9;-><init>(LX/0sa;LX/0rq;LX/0se;LX/0wA;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;)V

    .line 159360
    move-object v0, v3

    .line 159361
    sput-object v0, LX/0w9;->i:LX/0w9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159362
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 159363
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 159364
    :cond_1
    sget-object v0, LX/0w9;->i:LX/0w9;

    return-object v0

    .line 159365
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 159366
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static d(LX/0gW;)LX/0gW;
    .locals 2

    .prologue
    .line 159404
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v0

    .line 159405
    const-string v1, "default_image_scale"

    if-nez v0, :cond_0

    sget-object v0, LX/0wB;->a:LX/0wC;

    :cond_0
    invoke-virtual {p0, v1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 159406
    return-object p0
.end method


# virtual methods
.method public final a(LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;)LX/0gW;
    .locals 5
    .param p2    # Lcom/facebook/api/feed/FetchFeedParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 159382
    if-eqz p2, :cond_1

    .line 159383
    iget-object v0, p2, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 159384
    if-eqz v0, :cond_1

    .line 159385
    iget-object v0, p2, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 159386
    if-nez v0, :cond_1

    .line 159387
    iget-object v0, p0, LX/0w9;->d:LX/0wA;

    .line 159388
    iget-object v1, p2, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 159389
    iget-object v2, p2, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v2, v2

    .line 159390
    const/4 v3, 0x0

    .line 159391
    iget-object v4, v0, LX/0wA;->b:LX/0ad;

    sget-short p0, LX/0fe;->aQ:S

    const/4 p2, 0x0

    invoke-interface {v4, p0, p2}, LX/0ad;->a(SZ)Z

    move-result v4

    if-nez v4, :cond_2

    .line 159392
    :cond_0
    :goto_0
    move-object v0, v3

    .line 159393
    invoke-virtual {p1, p3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 159394
    :cond_1
    return-object p1

    .line 159395
    :cond_2
    iget-object v4, v0, LX/0wA;->a:LX/0pn;

    invoke-virtual {v4, v1, v2}, LX/0pn;->a(Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 159396
    iget-object p0, v0, LX/0wA;->a:LX/0pn;

    invoke-virtual {p0, v2, v4}, LX/0pn;->c(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 159397
    :goto_1
    if-nez v4, :cond_3

    .line 159398
    iget-object v4, v0, LX/0wA;->a:LX/0pn;

    invoke-virtual {v4, v2}, LX/0pn;->g(Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v4

    .line 159399
    :cond_3
    if-eqz v4, :cond_0

    .line 159400
    new-instance v3, LX/0m9;

    sget-object p0, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, p0}, LX/0m9;-><init>(LX/0mC;)V

    .line 159401
    const-string p0, "start_cursor"

    invoke-virtual {v3, p0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 159402
    const-string p0, "end_cursor"

    invoke-virtual {v3, p0, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 159403
    invoke-virtual {v3}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_4
    move-object v4, v3

    goto :goto_1
.end method

.method public final b(LX/0gW;)LX/0gW;
    .locals 2

    .prologue
    .line 159378
    iget-object v0, p0, LX/0w9;->c:LX/0se;

    iget-object v1, p0, LX/0w9;->b:LX/0rq;

    invoke-virtual {v1}, LX/0rq;->c()LX/0wF;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 159379
    const-string v0, "image_large_aspect_height"

    iget-object v1, p0, LX/0w9;->a:LX/0sa;

    invoke-virtual {v1}, LX/0sa;->A()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 159380
    const-string v0, "image_large_aspect_width"

    iget-object v1, p0, LX/0w9;->a:LX/0sa;

    invoke-virtual {v1}, LX/0sa;->z()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 159381
    return-object p1
.end method

.method public final c(LX/0gW;)LX/0gW;
    .locals 3

    .prologue
    .line 159369
    const-string v0, "angora_attachment_cover_image_size"

    iget-object v1, p0, LX/0w9;->a:LX/0sa;

    invoke-virtual {v1}, LX/0sa;->s()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 159370
    const-string v0, "angora_attachment_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 159371
    const-string v0, "reading_attachment_profile_image_width"

    iget-object v1, p0, LX/0w9;->a:LX/0sa;

    invoke-virtual {v1}, LX/0sa;->M()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "reading_attachment_profile_image_height"

    iget-object v2, p0, LX/0w9;->a:LX/0sa;

    invoke-virtual {v2}, LX/0sa;->N()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 159372
    const-string v0, "question_poll_count"

    .line 159373
    sget-object v1, LX/0sa;->c:Ljava/lang/Integer;

    move-object v1, v1

    .line 159374
    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "poll_voters_count"

    .line 159375
    sget-object v2, LX/0sa;->d:Ljava/lang/Integer;

    move-object v2, v2

    .line 159376
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "poll_facepile_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 159377
    return-object p1
.end method

.method public final e(LX/0gW;)LX/0gW;
    .locals 4

    .prologue
    .line 159367
    const-string v0, "with_actor_profile_video_playback"

    iget-object v1, p0, LX/0w9;->h:LX/0ad;

    sget-short v2, LX/0wf;->aJ:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 159368
    return-object p1
.end method
