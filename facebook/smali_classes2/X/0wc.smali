.class public LX/0wc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0wc;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 160249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160250
    return-void
.end method

.method public static a(LX/0QB;)LX/0wc;
    .locals 3

    .prologue
    .line 160251
    sget-object v0, LX/0wc;->a:LX/0wc;

    if-nez v0, :cond_1

    .line 160252
    const-class v1, LX/0wc;

    monitor-enter v1

    .line 160253
    :try_start_0
    sget-object v0, LX/0wc;->a:LX/0wc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 160254
    if-eqz v2, :cond_0

    .line 160255
    :try_start_1
    new-instance v0, LX/0wc;

    invoke-direct {v0}, LX/0wc;-><init>()V

    .line 160256
    move-object v0, v0

    .line 160257
    sput-object v0, LX/0wc;->a:LX/0wc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 160258
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 160259
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 160260
    :cond_1
    sget-object v0, LX/0wc;->a:LX/0wc;

    return-object v0

    .line 160261
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 160262
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static final a(Landroid/view/View;)V
    .locals 1
    .param p0    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 160263
    const/4 v0, 0x2

    invoke-static {p0, v0}, LX/0wc;->a(Landroid/view/View;I)V

    .line 160264
    return-void
.end method

.method private static a(Landroid/view/View;I)V
    .locals 3
    .param p0    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 160265
    if-eqz p0, :cond_0

    invoke-static {p0}, LX/0vv;->g(Landroid/view/View;)I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 160266
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ne v0, v1, :cond_1

    .line 160267
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0, p1, v0}, LX/0vv;->a(Landroid/view/View;ILandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160268
    :cond_0
    :goto_0
    return-void

    .line 160269
    :cond_1
    invoke-static {p0, p1, v2}, LX/0vv;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    goto :goto_0

    .line 160270
    :catch_0
    goto :goto_0
.end method

.method public static final b(Landroid/view/View;)V
    .locals 1
    .param p0    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 160271
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/0wc;->a(Landroid/view/View;I)V

    .line 160272
    return-void
.end method
