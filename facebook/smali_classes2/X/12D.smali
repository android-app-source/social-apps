.class public final enum LX/12D;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/12D;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/12D;

.field public static final enum CONCAT_BUFFER:LX/12D;

.field public static final enum NAME_COPY_BUFFER:LX/12D;

.field public static final enum TEXT_BUFFER:LX/12D;

.field public static final enum TOKEN_BUFFER:LX/12D;


# instance fields
.field public final size:I


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0xc8

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 174181
    new-instance v0, LX/12D;

    const-string v1, "TOKEN_BUFFER"

    const/16 v2, 0x7d0

    invoke-direct {v0, v1, v3, v2}, LX/12D;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/12D;->TOKEN_BUFFER:LX/12D;

    .line 174182
    new-instance v0, LX/12D;

    const-string v1, "CONCAT_BUFFER"

    const/16 v2, 0x7d0

    invoke-direct {v0, v1, v4, v2}, LX/12D;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/12D;->CONCAT_BUFFER:LX/12D;

    .line 174183
    new-instance v0, LX/12D;

    const-string v1, "TEXT_BUFFER"

    invoke-direct {v0, v1, v5, v7}, LX/12D;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/12D;->TEXT_BUFFER:LX/12D;

    .line 174184
    new-instance v0, LX/12D;

    const-string v1, "NAME_COPY_BUFFER"

    invoke-direct {v0, v1, v6, v7}, LX/12D;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/12D;->NAME_COPY_BUFFER:LX/12D;

    .line 174185
    const/4 v0, 0x4

    new-array v0, v0, [LX/12D;

    sget-object v1, LX/12D;->TOKEN_BUFFER:LX/12D;

    aput-object v1, v0, v3

    sget-object v1, LX/12D;->CONCAT_BUFFER:LX/12D;

    aput-object v1, v0, v4

    sget-object v1, LX/12D;->TEXT_BUFFER:LX/12D;

    aput-object v1, v0, v5

    sget-object v1, LX/12D;->NAME_COPY_BUFFER:LX/12D;

    aput-object v1, v0, v6

    sput-object v0, LX/12D;->$VALUES:[LX/12D;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 174186
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, LX/12D;->size:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/12D;
    .locals 1

    .prologue
    .line 174187
    const-class v0, LX/12D;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/12D;

    return-object v0
.end method

.method public static values()[LX/12D;
    .locals 1

    .prologue
    .line 174188
    sget-object v0, LX/12D;->$VALUES:[LX/12D;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/12D;

    return-object v0
.end method
