.class public LX/1cH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cF",
        "<",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:LX/1Fj;

.field private final c:LX/1cF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/1Fj;LX/1cF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBufferFactory;",
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 281783
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 281784
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, LX/1cH;->a:Ljava/util/concurrent/Executor;

    .line 281785
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Fj;

    iput-object v0, p0, LX/1cH;->b:LX/1Fj;

    .line 281786
    invoke-static {p3}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cF;

    iput-object v0, p0, LX/1cH;->c:LX/1cF;

    .line 281787
    return-void
.end method

.method public static b(LX/1FL;LX/1gO;)V
    .locals 3

    .prologue
    .line 281771
    invoke-virtual {p0}, LX/1FL;->b()Ljava/io/InputStream;

    move-result-object v0

    .line 281772
    invoke-static {v0}, LX/1la;->b(Ljava/io/InputStream;)LX/1lW;

    move-result-object v1

    .line 281773
    sget-object v2, LX/1ld;->e:LX/1lW;

    if-eq v1, v2, :cond_0

    sget-object v2, LX/1ld;->g:LX/1lW;

    if-ne v1, v2, :cond_1

    .line 281774
    :cond_0
    sget-object v1, LX/4ek;->b:Lcom/facebook/imagepipeline/nativecode/WebpTranscoderImpl;

    move-object v1, v1

    .line 281775
    const/16 v2, 0x50

    invoke-virtual {v1, v0, p1, v2}, Lcom/facebook/imagepipeline/nativecode/WebpTranscoderImpl;->a(Ljava/io/InputStream;Ljava/io/OutputStream;I)V

    .line 281776
    :goto_0
    return-void

    .line 281777
    :cond_1
    sget-object v2, LX/1ld;->f:LX/1lW;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/1ld;->h:LX/1lW;

    if-ne v1, v2, :cond_3

    .line 281778
    :cond_2
    sget-object v1, LX/4ek;->b:Lcom/facebook/imagepipeline/nativecode/WebpTranscoderImpl;

    move-object v1, v1

    .line 281779
    invoke-virtual {v1, v0, p1}, Lcom/facebook/imagepipeline/nativecode/WebpTranscoderImpl;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    goto :goto_0

    .line 281780
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong image format"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(LX/1cd;LX/1cW;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 281781
    iget-object v0, p0, LX/1cH;->c:LX/1cF;

    new-instance v1, LX/2co;

    invoke-direct {v1, p0, p1, p2}, LX/2co;-><init>(LX/1cH;LX/1cd;LX/1cW;)V

    invoke-interface {v0, v1, p2}, LX/1cF;->a(LX/1cd;LX/1cW;)V

    .line 281782
    return-void
.end method
