.class public LX/0b4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "LX/0b2;",
        "E::",
        "LX/0b7;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<TE;>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<TT;>;>;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 86617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86618
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0b4;->a:Ljava/util/Map;

    .line 86619
    invoke-static {}, LX/0UK;->a()Ljava/util/ArrayDeque;

    move-result-object v0

    iput-object v0, p0, LX/0b4;->b:Ljava/util/Queue;

    .line 86620
    return-void
.end method

.method public static a(Ljava/util/List;)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 86557
    if-eqz p0, :cond_2

    move v1, v0

    move v2, v0

    .line 86558
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 86559
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 86560
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 86561
    add-int/lit8 v3, v2, 0x1

    invoke-interface {p0, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v2, v3

    .line 86562
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 86563
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-lt v0, v2, :cond_2

    .line 86564
    invoke-interface {p0, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 86565
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 86566
    :cond_2
    return-void
.end method


# virtual methods
.method public declared-synchronized a(LX/0b7;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 86567
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0b4;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86568
    iget-object v0, p0, LX/0b4;->b:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 86569
    :goto_0
    iget-object v0, p0, LX/0b4;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 86570
    iget-object v0, p0, LX/0b4;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0b7;

    .line 86571
    iget-object v1, p0, LX/0b4;->a:Ljava/util/Map;

    if-nez v1, :cond_3

    .line 86572
    :cond_0
    iget-object v0, p0, LX/0b4;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 86573
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 86574
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/0b4;->b:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 86575
    :cond_2
    monitor-exit p0

    return-void

    .line 86576
    :cond_3
    iget-object v1, p0, LX/0b4;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 86577
    if-eqz v1, :cond_0

    .line 86578
    invoke-static {v1}, LX/0b4;->a(Ljava/util/List;)V

    .line 86579
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 86580
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 86581
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 86582
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0b2;

    .line 86583
    if-eqz v1, :cond_4

    invoke-virtual {v1, v0}, LX/0b2;->a(LX/0b7;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 86584
    invoke-virtual {v1, v0}, LX/0b2;->b(LX/0b7;)V

    goto :goto_1
.end method

.method public final declared-synchronized a(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+TE;>;)V"
        }
    .end annotation

    .prologue
    .line 86585
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0b4;->a:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 86586
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 86587
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/0b4;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 86588
    if-eqz v0, :cond_0

    .line 86589
    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 86590
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0b2;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 86591
    monitor-enter p0

    if-nez p1, :cond_0

    move v0, v2

    .line 86592
    :goto_0
    monitor-exit p0

    return v0

    .line 86593
    :cond_0
    :try_start_0
    invoke-virtual {p1}, LX/0b2;->a()Ljava/lang/Class;

    move-result-object v0

    .line 86594
    iget-object v1, p0, LX/0b4;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 86595
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 86596
    iget-object v3, p0, LX/0b4;->a:Ljava/util/Map;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86597
    :cond_1
    iget-object v1, p0, LX/0b4;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 86598
    invoke-static {v0}, LX/0b4;->a(Ljava/util/List;)V

    .line 86599
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_3

    .line 86600
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_2

    move v0, v2

    .line 86601
    goto :goto_0

    .line 86602
    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 86603
    :cond_3
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    goto :goto_0

    .line 86604
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/0b2;)Z
    .locals 5
    .param p1    # LX/0b2;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 86605
    monitor-enter p0

    if-nez p1, :cond_0

    move v0, v2

    .line 86606
    :goto_0
    monitor-exit p0

    return v0

    .line 86607
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/0b4;->a:Ljava/util/Map;

    invoke-virtual {p1}, LX/0b2;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 86608
    if-nez v0, :cond_1

    move v0, v2

    .line 86609
    goto :goto_0

    .line 86610
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_3

    .line 86611
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_2

    .line 86612
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86613
    const/4 v0, 0x1

    goto :goto_0

    .line 86614
    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_3
    move v0, v2

    .line 86615
    goto :goto_0

    .line 86616
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
