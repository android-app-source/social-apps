.class public LX/0s2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0WV;

.field private final c:Landroid/telephony/TelephonyManager;

.field public final d:Ljava/lang/String;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/48Q;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:LX/0s5;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0WV;Landroid/telephony/TelephonyManager;Ljava/lang/String;LX/0Ot;LX/0s5;)V
    .locals 0
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/config/server/AppNameInUserAgent;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0WV;",
            "Landroid/telephony/TelephonyManager;",
            "Ljava/lang/String;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/48Q;",
            ">;>;",
            "LX/0s5;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 151138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151139
    iput-object p1, p0, LX/0s2;->a:Landroid/content/Context;

    .line 151140
    iput-object p2, p0, LX/0s2;->b:LX/0WV;

    .line 151141
    iput-object p3, p0, LX/0s2;->c:Landroid/telephony/TelephonyManager;

    .line 151142
    iput-object p4, p0, LX/0s2;->d:Ljava/lang/String;

    .line 151143
    iput-object p5, p0, LX/0s2;->e:LX/0Ot;

    .line 151144
    iput-object p6, p0, LX/0s2;->f:LX/0s5;

    .line 151145
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 151146
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151147
    const-string v0, "null"

    .line 151148
    :goto_0
    return-object v0

    .line 151149
    :cond_0
    invoke-static {p0}, LX/0YN;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 151150
    const-string v1, "/"

    const-string v2, "-"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ";"

    const-string v2, "-"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/0s2;
    .locals 7

    .prologue
    .line 151151
    new-instance v0, LX/0s2;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v2

    check-cast v2, LX/0WV;

    invoke-static {p0}, LX/0e7;->b(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    invoke-static {p0}, LX/0s3;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 151152
    new-instance v5, LX/0s4;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v6

    invoke-direct {v5, v6}, LX/0s4;-><init>(LX/0QB;)V

    move-object v5, v5

    .line 151153
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v6

    invoke-static {v5, v6}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v5

    move-object v5, v5

    .line 151154
    invoke-static {p0}, LX/0s5;->a(LX/0QB;)LX/0s5;

    move-result-object v6

    check-cast v6, LX/0s5;

    invoke-direct/range {v0 .. v6}, LX/0s2;-><init>(Landroid/content/Context;LX/0WV;Landroid/telephony/TelephonyManager;Ljava/lang/String;LX/0Ot;LX/0s5;)V

    .line 151155
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 151156
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 151157
    const-string v0, "http.agent"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151158
    const-string v0, " ["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151159
    const-string v0, "%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;%s/%s:%s;%s/%s;FB_FW/1;"

    const/16 v2, 0x19

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "FBAN"

    aput-object v3, v2, v5

    iget-object v3, p0, LX/0s2;->d:Ljava/lang/String;

    aput-object v3, v2, v6

    const-string v3, "FBAV"

    aput-object v3, v2, v7

    const/4 v3, 0x3

    iget-object v4, p0, LX/0s2;->b:LX/0WV;

    invoke-virtual {v4}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0s2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "FBPN"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-object v4, p0, LX/0s2;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "FBLC"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0s2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "FBBV"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    iget-object v4, p0, LX/0s2;->b:LX/0WV;

    invoke-virtual {v4}, LX/0WV;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "FBCR"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    iget-object v4, p0, LX/0s2;->c:Landroid/telephony/TelephonyManager;

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0s2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "FBMF"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    sget-object v4, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-static {v4}, LX/0s2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, "FBBD"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    sget-object v4, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-static {v4}, LX/0s2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, "FBDV"

    aput-object v4, v2, v3

    const/16 v3, 0x11

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v4}, LX/0s2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x12

    const-string v4, "FBSV"

    aput-object v4, v2, v3

    const/16 v3, 0x13

    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-static {v4}, LX/0s2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x14

    const-string v4, "FBCA"

    aput-object v4, v2, v3

    const/16 v3, 0x15

    sget-object v4, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-static {v4}, LX/0s2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x16

    sget-object v4, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-static {v4}, LX/0s2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x17

    const-string v4, "FBDM"

    aput-object v4, v2, v3

    const/16 v3, 0x18

    .line 151160
    iget-object v4, p0, LX/0s2;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    .line 151161
    new-instance v9, Landroid/graphics/Point;

    iget v4, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v10, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-direct {v9, v4, v10}, Landroid/graphics/Point;-><init>(II)V

    .line 151162
    iget-object v4, p0, LX/0s2;->a:Landroid/content/Context;

    const-string v10, "window"

    invoke-virtual {v4, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    .line 151163
    if-eqz v4, :cond_0

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 151164
    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 151165
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v10, "{density="

    invoke-direct {v4, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, ",width="

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v8, v9, Landroid/graphics/Point;->x:I

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, ",height="

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v8, v9, Landroid/graphics/Point;->y:I

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "}"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 151166
    invoke-static {v4}, LX/0s2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151167
    iget-object v0, p0, LX/0s2;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48Q;

    .line 151168
    invoke-interface {v0}, LX/48Q;->a()Ljava/lang/String;

    move-result-object v3

    .line 151169
    invoke-interface {v0}, LX/48Q;->b()Ljava/lang/String;

    move-result-object v0

    .line 151170
    if-eqz v3, :cond_1

    .line 151171
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, LX/0s2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ";"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 151172
    :cond_2
    iget-object v0, p0, LX/0s2;->f:LX/0s5;

    invoke-virtual {v0}, LX/0s5;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 151173
    const-string v2, "%s/%s;"

    new-array v3, v7, [Ljava/lang/Object;

    const-string v0, "FBAT"

    aput-object v0, v3, v5

    iget-object v0, p0, LX/0s2;->f:LX/0s5;

    invoke-virtual {v0}, LX/0s5;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "2"

    :goto_1
    aput-object v0, v3, v6

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151174
    :cond_3
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151175
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 151176
    :cond_4
    const-string v0, "1"

    goto :goto_1
.end method
