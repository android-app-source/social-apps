.class public LX/1hj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1hk;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/1hj;


# instance fields
.field private final b:LX/1hl;

.field private final c:LX/16j;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/4bO;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/14J;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 296897
    const-class v0, LX/1hj;

    sput-object v0, LX/1hj;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1hl;LX/16j;LX/0Ot;LX/14J;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1hl;",
            "Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapter;",
            "LX/0Ot",
            "<",
            "LX/4bO;",
            ">;",
            "Lcom/facebook/tigon/httpclientadapter/TigonExperiment;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 296828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296829
    iput-object p1, p0, LX/1hj;->b:LX/1hl;

    .line 296830
    iput-object p2, p0, LX/1hj;->c:LX/16j;

    .line 296831
    iput-object p3, p0, LX/1hj;->d:LX/0Ot;

    .line 296832
    iput-object p4, p0, LX/1hj;->e:LX/14J;

    .line 296833
    return-void
.end method

.method public static a(LX/0QB;)LX/1hj;
    .locals 7

    .prologue
    .line 296884
    sget-object v0, LX/1hj;->f:LX/1hj;

    if-nez v0, :cond_1

    .line 296885
    const-class v1, LX/1hj;

    monitor-enter v1

    .line 296886
    :try_start_0
    sget-object v0, LX/1hj;->f:LX/1hj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 296887
    if-eqz v2, :cond_0

    .line 296888
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 296889
    new-instance v6, LX/1hj;

    invoke-static {v0}, LX/1hl;->a(LX/0QB;)LX/1hl;

    move-result-object v3

    check-cast v3, LX/1hl;

    invoke-static {v0}, LX/16j;->a(LX/0QB;)LX/16j;

    move-result-object v4

    check-cast v4, LX/16j;

    const/16 v5, 0x24f2

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/14I;->a(LX/0QB;)LX/14J;

    move-result-object v5

    check-cast v5, LX/14J;

    invoke-direct {v6, v3, v4, p0, v5}, LX/1hj;-><init>(LX/1hl;LX/16j;LX/0Ot;LX/14J;)V

    .line 296890
    move-object v0, v6

    .line 296891
    sput-object v0, LX/1hj;->f:LX/1hj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 296892
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 296893
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 296894
    :cond_1
    sget-object v0, LX/1hj;->f:LX/1hj;

    return-object v0

    .line 296895
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 296896
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/15D;Lcom/google/common/util/concurrent/SettableFuture;)Lcom/google/common/util/concurrent/SettableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/15D",
            "<TT;>;",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 296877
    iget-object v0, p1, LX/15D;->f:LX/14Q;

    move-object v0, v0

    .line 296878
    sget-object v1, LX/14Q;->FALLBACK_REQUIRED:LX/14Q;

    if-ne v0, v1, :cond_0

    .line 296879
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 296880
    new-instance v1, LX/2K8;

    invoke-direct {v1, p0, p2, p1}, LX/2K8;-><init>(LX/1hj;Lcom/google/common/util/concurrent/SettableFuture;LX/15D;)V

    .line 296881
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 296882
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    move-object p2, v0

    .line 296883
    :cond_0
    return-object p2
.end method

.method private c(LX/15D;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/15D",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 296872
    iget-object v0, p1, LX/15D;->c:Ljava/lang/String;

    move-object v0, v0

    .line 296873
    const-string v1, "hprofUpload"

    if-ne v0, v1, :cond_0

    .line 296874
    iget-object v0, p0, LX/1hj;->e:LX/14J;

    .line 296875
    iget-object v1, v0, LX/14J;->a:LX/0ad;

    sget-short p0, LX/14p;->b:S

    const/4 p1, 0x1

    invoke-interface {v1, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 296876
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/15D;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/15D",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 296864
    invoke-direct/range {p0 .. p1}, LX/1hj;->c(LX/15D;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 296865
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1hj;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/4bO;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, LX/4bO;->a(LX/15D;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 296866
    :goto_0
    return-object v3

    .line 296867
    :cond_0
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v17

    .line 296868
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, LX/1hj;->a(LX/15D;Lcom/google/common/util/concurrent/SettableFuture;)Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v16

    .line 296869
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1hj;->b:LX/1hl;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, LX/1hl;->b(LX/15D;)Lorg/apache/http/impl/client/RequestWrapper;

    move-result-object v4

    .line 296870
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1hj;->c:LX/16j;

    invoke-virtual/range {p1 .. p1}, LX/15D;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, LX/15D;->c()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, LX/15D;->m()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, LX/15D;->f()Lorg/apache/http/client/ResponseHandler;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, LX/15D;->h()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, LX/15D;->n()Z

    move-result v10

    invoke-virtual/range {p1 .. p1}, LX/15D;->o()LX/2BD;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, LX/15D;->j()LX/14P;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, LX/15D;->i()LX/15F;

    move-result-object v13

    invoke-static/range {p1 .. p1}, Lcom/facebook/http/common/FbHttpUtils;->a(LX/15D;)Ljava/lang/String;

    move-result-object v14

    invoke-static/range {p1 .. p1}, Lcom/facebook/http/common/FbHttpUtils;->b(LX/15D;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {v3 .. v16}, LX/16j;->a(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;ILorg/apache/http/client/ResponseHandler;Lcom/facebook/http/interfaces/RequestPriority;ZLX/2BD;LX/14P;LX/15F;Ljava/lang/String;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V

    move-object/from16 v3, v17

    .line 296871
    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 296861
    iget-object v0, p0, LX/1hj;->c:LX/16j;

    .line 296862
    iget-object p0, v0, LX/16j;->a:Lcom/facebook/tigon/tigonliger/TigonLigerService;

    invoke-virtual {p0}, Lcom/facebook/tigon/tigonliger/TigonLigerService;->cancelAllRequests()V

    .line 296863
    return-void
.end method

.method public final a(LX/15D;Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15D",
            "<*>;",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 296855
    invoke-direct {p0, p1}, LX/1hj;->c(LX/15D;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296856
    iget-object v0, p0, LX/1hj;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4bO;

    invoke-virtual {v0, p1, p2}, LX/4bO;->a(LX/15D;Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 296857
    :goto_0
    return-void

    .line 296858
    :cond_0
    iget-object v0, p0, LX/1hj;->c:LX/16j;

    .line 296859
    iget v1, p1, LX/15D;->m:I

    move v1, v1

    .line 296860
    invoke-virtual {v0, v1, p2}, LX/16j;->a(ILcom/facebook/http/interfaces/RequestPriority;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 296850
    iget-object v0, p0, LX/1hj;->c:LX/16j;

    .line 296851
    iget-object v1, v0, LX/16j;->d:LX/1hh;

    invoke-virtual {v1, p1, p2}, LX/1hh;->a(Ljava/lang/String;Ljava/lang/String;)LX/0P1;

    move-result-object v1

    .line 296852
    invoke-virtual {v1}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v1

    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 296853
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v2, v1}, LX/16j;->a(ILcom/facebook/http/interfaces/RequestPriority;)V

    goto :goto_0

    .line 296854
    :cond_0
    return-void
.end method

.method public final b()LX/2Ee;
    .locals 3

    .prologue
    .line 296849
    new-instance v0, LX/2Ee;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v1, v2}, LX/2Ee;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-object v0
.end method

.method public final b(LX/15D;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15D",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 296838
    invoke-direct {p0, p1}, LX/1hj;->c(LX/15D;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296839
    iget-object v0, p0, LX/1hj;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4bO;

    invoke-virtual {v0, p1}, LX/4bO;->b(LX/15D;)Z

    move-result v0

    .line 296840
    :goto_0
    return v0

    .line 296841
    :cond_0
    iget-object v0, p0, LX/1hj;->c:LX/16j;

    .line 296842
    iget v1, p1, LX/15D;->m:I

    move v1, v1

    .line 296843
    iget-object p0, v0, LX/16j;->d:LX/1hh;

    invoke-virtual {p0, v1}, LX/1hh;->d(I)LX/1hi;

    move-result-object p0

    .line 296844
    if-nez p0, :cond_2

    .line 296845
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 296846
    :cond_2
    invoke-virtual {p0}, LX/1hi;->g()Lcom/facebook/tigon/tigonapi/TigonRequestToken;

    move-result-object p0

    .line 296847
    if-eqz p0, :cond_1

    .line 296848
    invoke-interface {p0}, Lcom/facebook/tigon/tigonapi/TigonRequestToken;->cancel()V

    goto :goto_1
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 296835
    iget-object v0, p0, LX/1hj;->c:LX/16j;

    .line 296836
    iget-object p0, v0, LX/16j;->d:LX/1hh;

    invoke-virtual {p0}, LX/1hh;->a()Ljava/lang/String;

    move-result-object p0

    move-object v0, p0

    .line 296837
    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 296834
    sget-object v0, LX/1hj;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 296827
    const-string v0, "Tigon"

    return-object v0
.end method
