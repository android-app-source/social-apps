.class public final enum LX/0cw;
.super LX/0cs;
.source ""


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 89477
    invoke-direct {p0, p1, p2}, LX/0cs;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final copyEntry(LX/0d3;LX/0qF;LX/0qF;)LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0d3",
            "<TK;TV;>;",
            "LX/0qF",
            "<TK;TV;>;",
            "LX/0qF",
            "<TK;TV;>;)",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 89472
    invoke-super {p0, p1, p2, p3}, LX/0cs;->copyEntry(LX/0d3;LX/0qF;LX/0qF;)LX/0qF;

    move-result-object v0

    .line 89473
    invoke-virtual {p0, p2, v0}, LX/0cs;->copyExpirableEntry(LX/0qF;LX/0qF;)V

    .line 89474
    invoke-virtual {p0, p2, v0}, LX/0cs;->copyEvictableEntry(LX/0qF;LX/0qF;)V

    .line 89475
    return-object v0
.end method

.method public final newEntry(LX/0d3;Ljava/lang/Object;ILX/0qF;)LX/0qF;
    .locals 1
    .param p4    # LX/0qF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0d3",
            "<TK;TV;>;TK;I",
            "LX/0qF",
            "<TK;TV;>;)",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 89476
    new-instance v0, LX/4zd;

    invoke-direct {v0, p2, p3, p4}, LX/4zd;-><init>(Ljava/lang/Object;ILX/0qF;)V

    return-object v0
.end method
