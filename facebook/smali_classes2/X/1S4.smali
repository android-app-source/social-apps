.class public final LX/1S4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/csslayout/YogaMeasureFunction;


# instance fields
.field public final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1no;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 247537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247538
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1S4;->a:LX/0Zi;

    return-void
.end method

.method private a(LX/1no;)V
    .locals 1

    .prologue
    .line 247539
    iget-object v0, p0, LX/1S4;->a:LX/0Zi;

    invoke-virtual {v0, p1}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 247540
    return-void
.end method


# virtual methods
.method public final measure(LX/1mn;FLcom/facebook/csslayout/YogaMeasureMode;FLcom/facebook/csslayout/YogaMeasureMode;)J
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    .line 247541
    invoke-interface {p1}, LX/1mn;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Dg;

    .line 247542
    iget-boolean v0, v2, LX/1Dg;->aj:Z

    move v0, v0

    .line 247543
    if-eqz v0, :cond_0

    .line 247544
    iget-object v0, v2, LX/1Dg;->ai:LX/1ml;

    move-object v0, v0

    .line 247545
    :goto_0
    iget-object v1, v2, LX/1Dg;->l:LX/1X1;

    move-object v6, v1

    .line 247546
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "measure:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, LX/1X1;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1mm;->a(Ljava/lang/String;)V

    .line 247547
    invoke-static {p2, p3}, LX/1mh;->a(FLcom/facebook/csslayout/YogaMeasureMode;)I

    move-result v3

    .line 247548
    invoke-static {p4, p5}, LX/1mh;->a(FLcom/facebook/csslayout/YogaMeasureMode;)I

    move-result v4

    .line 247549
    invoke-virtual {v2, v3}, LX/1Dg;->Z(I)V

    .line 247550
    invoke-virtual {v2, v4}, LX/1Dg;->aa(I)V

    .line 247551
    invoke-static {v6}, LX/1X1;->f(LX/1X1;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 247552
    invoke-static {v2, v3, v4}, LX/1dN;->a(LX/1Dg;II)LX/1Dg;

    move-result-object v0

    .line 247553
    invoke-virtual {v0}, LX/1Dg;->c()I

    move-result v1

    .line 247554
    invoke-virtual {v0}, LX/1Dg;->d()I

    move-result v0

    .line 247555
    :goto_1
    int-to-float v3, v1

    invoke-virtual {v2, v3}, LX/1Dg;->g(F)V

    .line 247556
    int-to-float v3, v0

    invoke-virtual {v2, v3}, LX/1Dg;->h(F)V

    .line 247557
    invoke-static {}, LX/1mm;->a()V

    .line 247558
    invoke-static {v1, v0}, LX/1Dr;->a(II)J

    move-result-wide v0

    return-wide v0

    .line 247559
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 247560
    :cond_1
    if-eqz v0, :cond_2

    .line 247561
    iget v1, v0, LX/1ml;->i:I

    move v1, v1

    .line 247562
    if-ne v1, v3, :cond_2

    .line 247563
    iget v1, v0, LX/1ml;->j:I

    move v1, v1

    .line 247564
    if-ne v1, v4, :cond_2

    .line 247565
    iget v1, v0, LX/1ml;->g:F

    move v1, v1

    .line 247566
    float-to-int v1, v1

    .line 247567
    iget v3, v0, LX/1ml;->h:F

    move v0, v3

    .line 247568
    float-to-int v0, v0

    goto :goto_1

    .line 247569
    :cond_2
    const/high16 v0, -0x80000000

    .line 247570
    iget-object v1, p0, LX/1S4;->a:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1no;

    .line 247571
    if-nez v1, :cond_3

    .line 247572
    new-instance v1, LX/1no;

    invoke-direct {v1}, LX/1no;-><init>()V

    .line 247573
    :cond_3
    iput v0, v1, LX/1no;->a:I

    .line 247574
    iput v0, v1, LX/1no;->b:I

    .line 247575
    move-object v5, v1

    .line 247576
    :try_start_0
    iget-object v0, v6, LX/1X1;->e:LX/1S3;

    move-object v0, v0

    .line 247577
    iget-object v1, v2, LX/1Dg;->j:LX/1De;

    move-object v1, v1

    .line 247578
    invoke-virtual/range {v0 .. v6}, LX/1S3;->a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V

    .line 247579
    iget v0, v5, LX/1no;->a:I

    if-ltz v0, :cond_4

    iget v0, v5, LX/1no;->b:I

    if-gez v0, :cond_5

    .line 247580
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MeasureOutput not set, ComponentLifecycle is: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 247581
    iget-object v2, v6, LX/1X1;->e:LX/1S3;

    move-object v2, v2

    .line 247582
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 247583
    :catchall_0
    move-exception v0

    invoke-direct {p0, v5}, LX/1S4;->a(LX/1no;)V

    throw v0

    .line 247584
    :cond_5
    :try_start_1
    iget v1, v5, LX/1no;->a:I

    .line 247585
    iget v0, v5, LX/1no;->b:I

    .line 247586
    iget-object v6, v2, LX/1Dg;->ai:LX/1ml;

    move-object v6, v6

    .line 247587
    if-eqz v6, :cond_6

    .line 247588
    iget-object v6, v2, LX/1Dg;->ai:LX/1ml;

    move-object v6, v6

    .line 247589
    iput v3, v6, LX/1ml;->i:I

    .line 247590
    iget-object v3, v2, LX/1Dg;->ai:LX/1ml;

    move-object v3, v3

    .line 247591
    iput v4, v3, LX/1ml;->j:I

    .line 247592
    iget-object v3, v2, LX/1Dg;->ai:LX/1ml;

    move-object v3, v3

    .line 247593
    int-to-float v4, v1

    .line 247594
    iput v4, v3, LX/1ml;->g:F

    .line 247595
    iget-object v3, v2, LX/1Dg;->ai:LX/1ml;

    move-object v3, v3

    .line 247596
    int-to-float v4, v0

    .line 247597
    iput v4, v3, LX/1ml;->h:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 247598
    :cond_6
    invoke-direct {p0, v5}, LX/1S4;->a(LX/1no;)V

    goto/16 :goto_1
.end method
