.class public LX/1qt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1qt;


# instance fields
.field public final a:Landroid/content/pm/PackageManager;

.field public final b:Landroid/content/pm/ApplicationInfo;


# direct methods
.method public constructor <init>(Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 331510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331511
    iput-object p1, p0, LX/1qt;->a:Landroid/content/pm/PackageManager;

    .line 331512
    iput-object p2, p0, LX/1qt;->b:Landroid/content/pm/ApplicationInfo;

    .line 331513
    return-void
.end method

.method public static a(LX/0QB;)LX/1qt;
    .locals 5

    .prologue
    .line 331514
    sget-object v0, LX/1qt;->c:LX/1qt;

    if-nez v0, :cond_1

    .line 331515
    const-class v1, LX/1qt;

    monitor-enter v1

    .line 331516
    :try_start_0
    sget-object v0, LX/1qt;->c:LX/1qt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 331517
    if-eqz v2, :cond_0

    .line 331518
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 331519
    new-instance p0, LX/1qt;

    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageManager;

    invoke-static {v0}, LX/0s7;->b(LX/0QB;)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ApplicationInfo;

    invoke-direct {p0, v3, v4}, LX/1qt;-><init>(Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;)V

    .line 331520
    move-object v0, p0

    .line 331521
    sput-object v0, LX/1qt;->c:LX/1qt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331522
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 331523
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 331524
    :cond_1
    sget-object v0, LX/1qt;->c:LX/1qt;

    return-object v0

    .line 331525
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 331526
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Intent;Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Landroid/content/pm/PackageManager;",
            "Landroid/content/pm/ApplicationInfo;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ActivityInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 331527
    const/16 v0, 0x40

    invoke-virtual {p1, p0, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 331528
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 331529
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 331530
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 331531
    invoke-static {v0, p1, p2}, LX/1qt;->a(Landroid/content/pm/ComponentInfo;Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 331532
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 331533
    :cond_1
    invoke-static {v1}, LX/1c9;->a(Ljava/util/List;)LX/1c9;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/pm/ComponentInfo;Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;)Z
    .locals 2

    .prologue
    .line 331534
    iget v0, p2, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 331535
    iget-object v1, p0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 331536
    if-eq v0, v1, :cond_0

    invoke-virtual {p1, v0, v1}, Landroid/content/pm/PackageManager;->checkSignatures(II)I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
