.class public final LX/1lw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/Object;

.field public d:I


# direct methods
.method public constructor <init>(IIILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 312671
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 312672
    iput p1, p0, LX/1lw;->a:I

    .line 312673
    iput p2, p0, LX/1lw;->b:I

    .line 312674
    iput p3, p0, LX/1lw;->d:I

    .line 312675
    iput-object p4, p0, LX/1lw;->c:Ljava/lang/Object;

    .line 312676
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 312677
    if-ne p0, p1, :cond_1

    .line 312678
    :cond_0
    :goto_0
    return v0

    .line 312679
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 312680
    goto :goto_0

    .line 312681
    :cond_3
    check-cast p1, LX/1lw;

    .line 312682
    iget v2, p0, LX/1lw;->a:I

    iget v3, p1, LX/1lw;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 312683
    goto :goto_0

    .line 312684
    :cond_4
    iget v2, p0, LX/1lw;->a:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_5

    iget v2, p0, LX/1lw;->d:I

    iget v3, p0, LX/1lw;->b:I

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-ne v2, v0, :cond_5

    .line 312685
    iget v2, p0, LX/1lw;->d:I

    iget v3, p1, LX/1lw;->b:I

    if-ne v2, v3, :cond_5

    iget v2, p0, LX/1lw;->b:I

    iget v3, p1, LX/1lw;->d:I

    if-eq v2, v3, :cond_0

    .line 312686
    :cond_5
    iget v2, p0, LX/1lw;->d:I

    iget v3, p1, LX/1lw;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 312687
    goto :goto_0

    .line 312688
    :cond_6
    iget v2, p0, LX/1lw;->b:I

    iget v3, p1, LX/1lw;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 312689
    goto :goto_0

    .line 312690
    :cond_7
    iget-object v2, p0, LX/1lw;->c:Ljava/lang/Object;

    if-eqz v2, :cond_8

    .line 312691
    iget-object v2, p0, LX/1lw;->c:Ljava/lang/Object;

    iget-object v3, p1, LX/1lw;->c:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 312692
    goto :goto_0

    .line 312693
    :cond_8
    iget-object v2, p1, LX/1lw;->c:Ljava/lang/Object;

    if-eqz v2, :cond_0

    move v0, v1

    .line 312694
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 312695
    iget v0, p0, LX/1lw;->a:I

    .line 312696
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/1lw;->b:I

    add-int/2addr v0, v1

    .line 312697
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/1lw;->d:I

    add-int/2addr v0, v1

    .line 312698
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 312699
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 312700
    iget v1, p0, LX/1lw;->a:I

    packed-switch v1, :pswitch_data_0

    .line 312701
    const-string v1, "??"

    :goto_0
    move-object v1, v1

    .line 312702
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",s:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/1lw;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "c:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/1lw;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",p:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1lw;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 312703
    :pswitch_0
    const-string v1, "add"

    goto :goto_0

    .line 312704
    :pswitch_1
    const-string v1, "rm"

    goto :goto_0

    .line 312705
    :pswitch_2
    const-string v1, "up"

    goto :goto_0

    .line 312706
    :pswitch_3
    const-string v1, "mv"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
