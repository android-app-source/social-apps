.class public LX/0r3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/0r3;


# instance fields
.field private final a:Z

.field public final b:D

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0So;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field public final f:Landroid/os/Handler;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/hardware/BatteryStateManager;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/net/ConnectivityManager;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0So;Ljava/util/concurrent/ExecutorService;Landroid/os/Handler;LX/0Ot;LX/0Ot;LX/0Zb;LX/0ad;)V
    .locals 4
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0So;",
            "Ljava/util/concurrent/ExecutorService;",
            "Landroid/os/Handler;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/hardware/BatteryStateManager;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/net/ConnectivityManager;",
            ">;",
            "LX/0Zb;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 148963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148964
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0r3;->c:Ljava/util/Map;

    .line 148965
    iput-object p1, p0, LX/0r3;->d:LX/0So;

    .line 148966
    iput-object p2, p0, LX/0r3;->e:Ljava/util/concurrent/ExecutorService;

    .line 148967
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, LX/0r3;->f:Landroid/os/Handler;

    .line 148968
    iput-object p4, p0, LX/0r3;->g:LX/0Ot;

    .line 148969
    iput-object p5, p0, LX/0r3;->h:LX/0Ot;

    .line 148970
    iput-object p6, p0, LX/0r3;->i:LX/0Zb;

    .line 148971
    sget-short v0, LX/0fe;->m:S

    invoke-interface {p7, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0r3;->a:Z

    .line 148972
    sget v0, LX/0fe;->l:I

    invoke-interface {p7, v0, v2}, LX/0ad;->a(II)I

    move-result v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    div-double/2addr v0, v2

    iput-wide v0, p0, LX/0r3;->b:D

    .line 148973
    return-void
.end method

.method public static a(LX/0QB;)LX/0r3;
    .locals 11

    .prologue
    .line 148977
    sget-object v0, LX/0r3;->j:LX/0r3;

    if-nez v0, :cond_1

    .line 148978
    const-class v1, LX/0r3;

    monitor-enter v1

    .line 148979
    :try_start_0
    sget-object v0, LX/0r3;->j:LX/0r3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 148980
    if-eqz v2, :cond_0

    .line 148981
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 148982
    new-instance v3, LX/0r3;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v6

    check-cast v6, Landroid/os/Handler;

    const/16 v7, 0x292

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x23

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v9

    check-cast v9, LX/0Zb;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-direct/range {v3 .. v10}, LX/0r3;-><init>(LX/0So;Ljava/util/concurrent/ExecutorService;Landroid/os/Handler;LX/0Ot;LX/0Ot;LX/0Zb;LX/0ad;)V

    .line 148983
    move-object v0, v3

    .line 148984
    sput-object v0, LX/0r3;->j:LX/0r3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 148985
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 148986
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 148987
    :cond_1
    sget-object v0, LX/0r3;->j:LX/0r3;

    return-object v0

    .line 148988
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 148989
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0r3;LX/0oG;)Z
    .locals 1

    .prologue
    .line 148976
    iget-boolean v0, p0, LX/0r3;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0r3;LX/0oG;)V
    .locals 3

    .prologue
    .line 148974
    iget-object v0, p0, LX/0r3;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/feed/logging/feednotloading/FeedNotLoadingLogger$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/feed/logging/feednotloading/FeedNotLoadingLogger$1;-><init>(LX/0r3;LX/0oG;)V

    const v2, 0x55cb26b2

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 148975
    return-void
.end method

.method public static d(LX/0r3;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 148912
    iget-object v0, p0, LX/0r3;->f:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 148913
    iget-object v0, p0, LX/0r3;->f:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 148914
    return-void
.end method

.method public static f(LX/0r3;)LX/0oG;
    .locals 3

    .prologue
    .line 148962
    iget-object v0, p0, LX/0r3;->i:LX/0Zb;

    const-string v1, "fb4a_feed_not_loading"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    return-object v0
.end method

.method public static g(LX/0r3;)Z
    .locals 4

    .prologue
    .line 148961
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    iget-wide v2, p0, LX/0r3;->b:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 148990
    invoke-static {p0}, LX/0r3;->f(LX/0r3;)LX/0oG;

    move-result-object v0

    .line 148991
    invoke-static {p0, v0}, LX/0r3;->a(LX/0r3;LX/0oG;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/0r3;->g(LX/0r3;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 148992
    :cond_0
    :goto_0
    return-void

    .line 148993
    :cond_1
    const-string v1, "state"

    const-string v2, "pull_to_refresh"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 148994
    invoke-static {p0, v0}, LX/0r3;->b(LX/0r3;LX/0oG;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 148956
    invoke-static {p0}, LX/0r3;->f(LX/0r3;)LX/0oG;

    move-result-object v0

    .line 148957
    invoke-static {p0, v0}, LX/0r3;->a(LX/0r3;LX/0oG;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/0r3;->g(LX/0r3;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 148958
    :cond_0
    :goto_0
    return-void

    .line 148959
    :cond_1
    const-string v1, "state"

    const-string v2, "service_exception"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "message"

    invoke-virtual {v1, v2, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 148960
    invoke-static {p0, v0}, LX/0r3;->b(LX/0r3;LX/0oG;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 148947
    invoke-static {p0, p1}, LX/0r3;->d(LX/0r3;Ljava/lang/String;)V

    .line 148948
    iget-object v0, p0, LX/0r3;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 148949
    if-eqz v0, :cond_0

    .line 148950
    iget-object v1, p0, LX/0r3;->d:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    long-to-double v0, v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget-wide v4, p0, LX/0r3;->b:D

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    double-to-long v0, v0

    .line 148951
    invoke-static {p0}, LX/0r3;->f(LX/0r3;)LX/0oG;

    move-result-object v2

    .line 148952
    invoke-static {p0, v2}, LX/0r3;->a(LX/0r3;LX/0oG;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 148953
    :cond_0
    :goto_0
    return-void

    .line 148954
    :cond_1
    const-string v3, "state"

    const-string v4, "fetch_duration"

    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "fetch_feed_cause"

    invoke-virtual {v3, v4, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "freshness_request"

    invoke-virtual {v3, v4, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "freshness_response"

    invoke-virtual {v3, v4, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "num_new_stories"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "request_response_duration"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 148955
    invoke-static {p0, v2}, LX/0r3;->b(LX/0r3;LX/0oG;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 148942
    invoke-static {p0}, LX/0r3;->f(LX/0r3;)LX/0oG;

    move-result-object v0

    .line 148943
    invoke-static {p0, v0}, LX/0r3;->a(LX/0r3;LX/0oG;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/0r3;->g(LX/0r3;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 148944
    :cond_0
    :goto_0
    return-void

    .line 148945
    :cond_1
    const-string v1, "state"

    const-string v2, "zero_stories_received"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 148946
    invoke-static {p0, v0}, LX/0r3;->b(LX/0r3;LX/0oG;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 148934
    iget-object v0, p0, LX/0r3;->c:Ljava/util/Map;

    iget-object v1, p0, LX/0r3;->d:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148935
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 148936
    invoke-static {p0, p1}, LX/0r3;->d(LX/0r3;Ljava/lang/String;)V

    .line 148937
    iget-object v4, p0, LX/0r3;->f:Landroid/os/Handler;

    const/4 v5, 0x1

    invoke-virtual {v4, v5, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    .line 148938
    iget-object v5, p0, LX/0r3;->f:Landroid/os/Handler;

    const-wide v6, 0x40b3880000000000L    # 5000.0

    iget-wide v8, p0, LX/0r3;->b:D

    add-double/2addr v8, v10

    mul-double/2addr v6, v8

    double-to-long v6, v6

    invoke-virtual {v5, v4, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 148939
    iget-object v4, p0, LX/0r3;->f:Landroid/os/Handler;

    const/4 v5, 0x2

    invoke-virtual {v4, v5, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    .line 148940
    iget-object v5, p0, LX/0r3;->f:Landroid/os/Handler;

    const-wide v6, 0x40c3880000000000L    # 10000.0

    iget-wide v8, p0, LX/0r3;->b:D

    add-double/2addr v8, v10

    mul-double/2addr v6, v8

    double-to-long v6, v6

    invoke-virtual {v5, v4, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 148941
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 148929
    invoke-static {p0}, LX/0r3;->f(LX/0r3;)LX/0oG;

    move-result-object v0

    .line 148930
    invoke-static {p0, v0}, LX/0r3;->a(LX/0r3;LX/0oG;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/0r3;->g(LX/0r3;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 148931
    :cond_0
    :goto_0
    return-void

    .line 148932
    :cond_1
    const-string v1, "state"

    const-string v2, "request_canceled"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 148933
    invoke-static {p0, v0}, LX/0r3;->b(LX/0r3;LX/0oG;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 148924
    invoke-static {p0}, LX/0r3;->f(LX/0r3;)LX/0oG;

    move-result-object v0

    .line 148925
    invoke-static {p0, v0}, LX/0r3;->a(LX/0r3;LX/0oG;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/0r3;->g(LX/0r3;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 148926
    :cond_0
    :goto_0
    return-void

    .line 148927
    :cond_1
    const-string v1, "state"

    const-string v2, "end_of_feed_dot_shown"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 148928
    invoke-static {p0, v0}, LX/0r3;->b(LX/0r3;LX/0oG;)V

    goto :goto_0
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 148915
    invoke-static {p0}, LX/0r3;->f(LX/0r3;)LX/0oG;

    move-result-object v0

    .line 148916
    invoke-static {p0, v0}, LX/0r3;->a(LX/0r3;LX/0oG;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 148917
    :goto_0
    return v4

    .line 148918
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 148919
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown message ID - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148920
    :pswitch_0
    const-string v1, "fetch_feed_cause"

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v1

    const-string v2, "state"

    const-string v3, "fetch_incomplete_5s"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 148921
    invoke-static {p0, v0}, LX/0r3;->b(LX/0r3;LX/0oG;)V

    goto :goto_0

    .line 148922
    :pswitch_1
    const-string v1, "fetch_feed_cause"

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v1

    const-string v2, "state"

    const-string v3, "fetch_incomplete_10s"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 148923
    invoke-static {p0, v0}, LX/0r3;->b(LX/0r3;LX/0oG;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
