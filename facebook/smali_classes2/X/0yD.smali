.class public LX/0yD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0yD;


# instance fields
.field public final a:LX/0SG;

.field public final b:LX/0So;


# direct methods
.method public constructor <init>(LX/0SG;LX/0So;)V
    .locals 0
    .param p2    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 164194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164195
    iput-object p1, p0, LX/0yD;->a:LX/0SG;

    .line 164196
    iput-object p2, p0, LX/0yD;->b:LX/0So;

    .line 164197
    return-void
.end method

.method public static a(Lcom/facebook/location/ImmutableLocation;J)J
    .locals 3

    .prologue
    .line 164192
    invoke-virtual {p0}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v0

    .line 164193
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, p1, v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0
.end method

.method public static a(Lcom/facebook/location/ImmutableLocation;JJ)J
    .locals 5

    .prologue
    .line 164171
    invoke-virtual {p0}, Lcom/facebook/location/ImmutableLocation;->h()LX/0am;

    move-result-object v0

    .line 164172
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164173
    const-wide/32 v2, 0xf4240

    mul-long/2addr v2, p3

    .line 164174
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    invoke-static {v0, v1}, LX/1lQ;->q(J)J

    move-result-wide v0

    .line 164175
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, p1, v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/wifiscan/WifiScanResult;J)J
    .locals 3

    .prologue
    .line 164189
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_0

    .line 164190
    const-wide/high16 v0, -0x8000000000000000L

    .line 164191
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/facebook/wifiscan/WifiScanResult;->a:J

    sub-long v0, p1, v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/0yD;
    .locals 5

    .prologue
    .line 164198
    sget-object v0, LX/0yD;->c:LX/0yD;

    if-nez v0, :cond_1

    .line 164199
    const-class v1, LX/0yD;

    monitor-enter v1

    .line 164200
    :try_start_0
    sget-object v0, LX/0yD;->c:LX/0yD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 164201
    if-eqz v2, :cond_0

    .line 164202
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 164203
    new-instance p0, LX/0yD;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-direct {p0, v3, v4}, LX/0yD;-><init>(LX/0SG;LX/0So;)V

    .line 164204
    move-object v0, p0

    .line 164205
    sput-object v0, LX/0yD;->c:LX/0yD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164206
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 164207
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 164208
    :cond_1
    sget-object v0, LX/0yD;->c:LX/0yD;

    return-object v0

    .line 164209
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 164210
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/location/ImmutableLocation;J)J
    .locals 7

    .prologue
    const-wide/high16 v0, -0x8000000000000000L

    .line 164183
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-ge v2, v3, :cond_1

    .line 164184
    :cond_0
    :goto_0
    return-wide v0

    .line 164185
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/location/ImmutableLocation;->h()LX/0am;

    move-result-object v2

    .line 164186
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 164187
    const-wide/32 v0, 0xf4240

    mul-long v4, v0, p1

    .line 164188
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v4, v0

    invoke-static {v0, v1}, LX/1lQ;->q(J)J

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/location/Location;)J
    .locals 6

    .prologue
    .line 164177
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 164178
    invoke-static {p1}, LX/1v5;->a(Landroid/location/Location;)LX/0am;

    move-result-object v0

    .line 164179
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164180
    const-wide/32 v2, 0xf4240

    iget-object v1, p0, LX/0yD;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    mul-long/2addr v2, v4

    .line 164181
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    invoke-static {v0, v1}, LX/1lQ;->q(J)J

    move-result-wide v0

    .line 164182
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/0yD;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public final a(Lcom/facebook/location/ImmutableLocation;)J
    .locals 4

    .prologue
    .line 164176
    iget-object v0, p0, LX/0yD;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-object v2, p0, LX/0yD;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-static {p1, v0, v1, v2, v3}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;JJ)J

    move-result-wide v0

    return-wide v0
.end method
