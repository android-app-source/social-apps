.class public LX/0qu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/facebook/graphql/model/FeedEdge;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0qp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qp",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Sh;

.field public c:Z

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z


# direct methods
.method public constructor <init>(LX/0Sh;)V
    .locals 2

    .prologue
    .line 148663
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148664
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0qu;->f:Z

    .line 148665
    new-instance v0, LX/0qp;

    sget-object v1, LX/0ql;->a:Ljava/util/Comparator;

    invoke-direct {v0, v1}, LX/0qp;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, LX/0qu;->a:LX/0qp;

    .line 148666
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0qu;->d:Ljava/util/List;

    .line 148667
    iput-object p1, p0, LX/0qu;->b:LX/0Sh;

    .line 148668
    return-void
.end method

.method public static i(LX/0qu;)V
    .locals 1

    .prologue
    .line 148661
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0qu;->f:Z

    .line 148662
    return-void
.end method

.method public static j(LX/0qu;)V
    .locals 3

    .prologue
    .line 148645
    iget-object v0, p0, LX/0qu;->b:LX/0Sh;

    const-string v1, "CallOnUiThreadOnly"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 148646
    iget-boolean v0, p0, LX/0qu;->f:Z

    if-eqz v0, :cond_2

    .line 148647
    iget-object v0, p0, LX/0qu;->e:Ljava/util/List;

    if-nez v0, :cond_0

    .line 148648
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/0qu;->e:Ljava/util/List;

    .line 148649
    :cond_0
    const/4 v0, 0x0

    .line 148650
    iget-object v1, p0, LX/0qu;->b:LX/0Sh;

    const-string v2, "CallOnUiThreadOnly"

    invoke-virtual {v1, v2}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 148651
    iget-object v1, p0, LX/0qu;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 148652
    iput-boolean v0, p0, LX/0qu;->f:Z

    move v1, v0

    .line 148653
    :goto_0
    iget-object v0, p0, LX/0qu;->a:LX/0qp;

    invoke-virtual {v0}, LX/0qp;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 148654
    iget-object v0, p0, LX/0qu;->a:LX/0qp;

    .line 148655
    iget-object v2, v0, LX/0qp;->e:Ljava/util/List;

    move-object v0, v2

    .line 148656
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 148657
    instance-of v0, v0, Lcom/facebook/feed/model/GapFeedEdge;

    if-eqz v0, :cond_1

    .line 148658
    iget-object v0, p0, LX/0qu;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148659
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 148660
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V
    .locals 2

    .prologue
    .line 148639
    iget-object v0, p0, LX/0qu;->b:LX/0Sh;

    const-string v1, "CallOnUiThreadOnly"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 148640
    iget-boolean v0, p0, LX/0qu;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 148641
    iget-object v0, p0, LX/0qu;->d:Ljava/util/List;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148642
    :cond_0
    iget-object v0, p0, LX/0qu;->a:LX/0qp;

    invoke-static {p1}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0qp;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148643
    invoke-static {p0}, LX/0qu;->i(LX/0qu;)V

    .line 148644
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 148638
    iget-object v0, p0, LX/0qu;->a:LX/0qp;

    invoke-virtual {v0, p1}, LX/0qp;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 148637
    iget-object v0, p0, LX/0qu;->a:LX/0qp;

    invoke-virtual {v0}, LX/0qp;->size()I

    move-result v0

    return v0
.end method

.method public final b(I)I
    .locals 2

    .prologue
    .line 148630
    invoke-static {p0}, LX/0qu;->j(LX/0qu;)V

    .line 148631
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/0qu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 148632
    iget-object v0, p0, LX/0qu;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 148633
    if-le v0, p1, :cond_0

    .line 148634
    :goto_1
    return v0

    .line 148635
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 148636
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
    .locals 1

    .prologue
    .line 148629
    iget-object v0, p0, LX/0qu;->a:LX/0qp;

    invoke-virtual {v0, p1}, LX/0qp;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    return-object v0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V
    .locals 2

    .prologue
    .line 148617
    iget-object v0, p0, LX/0qu;->b:LX/0Sh;

    const-string v1, "CallOnUiThreadOnly"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 148618
    iget-object v0, p0, LX/0qu;->a:LX/0qp;

    invoke-static {p1}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/0qp;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148619
    invoke-static {p0}, LX/0qu;->i(LX/0qu;)V

    .line 148620
    return-void
.end method

.method public final d(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
    .locals 1

    .prologue
    .line 148626
    iget-object v0, p0, LX/0qu;->a:LX/0qp;

    .line 148627
    iget-object p0, v0, LX/0qp;->e:Ljava/util/List;

    move-object v0, p0

    .line 148628
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 148624
    invoke-static {p0}, LX/0qu;->j(LX/0qu;)V

    .line 148625
    iget-object v0, p0, LX/0qu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148621
    iget-object v0, p0, LX/0qu;->a:LX/0qp;

    .line 148622
    iget-object p0, v0, LX/0qp;->e:Ljava/util/List;

    move-object v0, p0

    .line 148623
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
