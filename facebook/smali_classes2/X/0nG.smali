.class public LX/0nG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 134970
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0m9;LX/0n9;)V
    .locals 5

    .prologue
    .line 134971
    invoke-virtual {p0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v2

    .line 134972
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 134973
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 134974
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 134975
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 134976
    invoke-virtual {v0}, LX/0lF;->q()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 134977
    const/4 v0, 0x0

    .line 134978
    invoke-static {p1, v1, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 134979
    goto :goto_0

    .line 134980
    :cond_0
    invoke-virtual {v0}, LX/0lF;->o()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 134981
    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    .line 134982
    invoke-static {p1, v1, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 134983
    goto :goto_0

    .line 134984
    :cond_1
    invoke-virtual {v0}, LX/0lF;->m()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 134985
    invoke-virtual {v0}, LX/0lF;->v()Ljava/lang/Number;

    move-result-object v0

    .line 134986
    invoke-static {p1, v1, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 134987
    goto :goto_0

    .line 134988
    :cond_2
    invoke-virtual {v0}, LX/0lF;->p()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 134989
    invoke-virtual {v0}, LX/0lF;->u()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 134990
    invoke-static {p1, v1, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 134991
    goto :goto_0

    .line 134992
    :cond_3
    invoke-virtual {v0}, LX/0lF;->i()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 134993
    check-cast v0, LX/0m9;

    invoke-virtual {p1, v1}, LX/0n9;->b(Ljava/lang/String;)LX/0n9;

    move-result-object v1

    invoke-static {v0, v1}, LX/0nG;->a(LX/0m9;LX/0n9;)V

    goto :goto_0

    .line 134994
    :cond_4
    invoke-virtual {v0}, LX/0lF;->h()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 134995
    check-cast v0, LX/162;

    invoke-virtual {p1, v1}, LX/0n9;->c(Ljava/lang/String;)LX/0zL;

    move-result-object v3

    invoke-static {v0, v1, v3}, LX/0nG;->a(LX/162;Ljava/lang/String;LX/0zL;)V

    goto :goto_0

    .line 134996
    :cond_5
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unsupported JSON type for \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\': "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, LX/0lF;->k()LX/0nH;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 134997
    :cond_6
    return-void
.end method

.method private static a(LX/162;Ljava/lang/String;LX/0zL;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 134998
    invoke-virtual {p0}, LX/0lF;->G()Ljava/util/Iterator;

    move-result-object v1

    .line 134999
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 135000
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 135001
    invoke-virtual {v0}, LX/0lF;->q()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 135002
    const/4 v0, 0x0

    .line 135003
    invoke-static {p2, v0}, LX/0zL;->a(LX/0zL;Ljava/lang/Object;)V

    .line 135004
    goto :goto_0

    .line 135005
    :cond_0
    invoke-virtual {v0}, LX/0lF;->o()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 135006
    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    .line 135007
    invoke-static {p2, v0}, LX/0zL;->a(LX/0zL;Ljava/lang/Object;)V

    .line 135008
    goto :goto_0

    .line 135009
    :cond_1
    invoke-virtual {v0}, LX/0lF;->m()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 135010
    invoke-virtual {v0}, LX/0lF;->v()Ljava/lang/Number;

    move-result-object v0

    .line 135011
    invoke-static {p2, v0}, LX/0zL;->a(LX/0zL;Ljava/lang/Object;)V

    .line 135012
    goto :goto_0

    .line 135013
    :cond_2
    invoke-virtual {v0}, LX/0lF;->p()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 135014
    invoke-virtual {v0}, LX/0lF;->u()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 135015
    invoke-static {p2, v0}, LX/0zL;->a(LX/0zL;Ljava/lang/Object;)V

    .line 135016
    goto :goto_0

    .line 135017
    :cond_3
    invoke-virtual {v0}, LX/0lF;->i()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 135018
    check-cast v0, LX/0m9;

    invoke-virtual {p2}, LX/0zL;->k()LX/0n9;

    move-result-object v2

    invoke-static {v0, v2}, LX/0nG;->a(LX/0m9;LX/0n9;)V

    goto :goto_0

    .line 135019
    :cond_4
    invoke-virtual {v0}, LX/0lF;->h()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 135020
    check-cast v0, LX/162;

    invoke-virtual {p2}, LX/0zL;->l()LX/0zL;

    move-result-object v2

    invoke-static {v0, p1, v2}, LX/0nG;->a(LX/162;Ljava/lang/String;LX/0zL;)V

    goto :goto_0

    .line 135021
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported JSON type for \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\': "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, LX/0lF;->k()LX/0nH;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 135022
    :cond_6
    return-void
.end method

.method public static a(Ljava/lang/String;LX/0lF;LX/0n9;)V
    .locals 3
    .param p1    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 135023
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0lF;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135024
    :cond_0
    const/4 v0, 0x0

    .line 135025
    invoke-static {p2, p0, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 135026
    :goto_0
    return-void

    .line 135027
    :cond_1
    invoke-virtual {p1}, LX/0lF;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 135028
    invoke-virtual {p1}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    .line 135029
    invoke-static {p2, p0, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 135030
    goto :goto_0

    .line 135031
    :cond_2
    invoke-virtual {p1}, LX/0lF;->m()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 135032
    invoke-virtual {p1}, LX/0lF;->v()Ljava/lang/Number;

    move-result-object v0

    .line 135033
    invoke-static {p2, p0, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 135034
    goto :goto_0

    .line 135035
    :cond_3
    invoke-virtual {p1}, LX/0lF;->p()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 135036
    invoke-virtual {p1}, LX/0lF;->u()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 135037
    invoke-static {p2, p0, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 135038
    goto :goto_0

    .line 135039
    :cond_4
    invoke-virtual {p1}, LX/0lF;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 135040
    check-cast p1, LX/0m9;

    invoke-virtual {p2, p0}, LX/0n9;->b(Ljava/lang/String;)LX/0n9;

    move-result-object v0

    invoke-static {p1, v0}, LX/0nG;->a(LX/0m9;LX/0n9;)V

    goto :goto_0

    .line 135041
    :cond_5
    invoke-virtual {p1}, LX/0lF;->h()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 135042
    check-cast p1, LX/162;

    invoke-virtual {p2, p0}, LX/0n9;->c(Ljava/lang/String;)LX/0zL;

    move-result-object v0

    invoke-static {p1, p0, v0}, LX/0nG;->a(LX/162;Ljava/lang/String;LX/0zL;)V

    goto :goto_0

    .line 135043
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported JSON type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/0lF;->k()LX/0nH;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
