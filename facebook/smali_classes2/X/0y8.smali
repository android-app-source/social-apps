.class public LX/0y8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private mConnectionQuality:LX/0p3;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "connection_quality"
    .end annotation
.end field

.field private mIsConnected:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_connected"
    .end annotation
.end field

.field private mIsPhotoTakenInLastNMinutes:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_photo_taken_in_last_n_min"
    .end annotation
.end field

.field private mIsWaitingForNewStories:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_waiting_for_new_stories"
    .end annotation
.end field

.field private mReactionCount:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "reaction_count"
    .end annotation
.end field

.field private mUIHConfigVersion:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uih_ver"
    .end annotation
.end field

.field private mVideoPlayCount:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "video_play_count"
    .end annotation
.end field

.field private mVideoPlaySecs:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "video_play_secs"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 164146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164147
    sget-object v0, LX/0p3;->UNKNOWN:LX/0p3;

    iput-object v0, p0, LX/0y8;->mConnectionQuality:LX/0p3;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()LX/0p3;
    .locals 1

    .prologue
    .line 164151
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0y8;->mConnectionQuality:LX/0p3;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)V
    .locals 1

    .prologue
    .line 164148
    monitor-enter p0

    :try_start_0
    iput p1, p0, LX/0y8;->mReactionCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164149
    monitor-exit p0

    return-void

    .line 164150
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0p3;)V
    .locals 1

    .prologue
    .line 164143
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/0y8;->mConnectionQuality:LX/0p3;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164144
    monitor-exit p0

    return-void

    .line 164145
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 164140
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/0y8;->mUIHConfigVersion:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164141
    monitor-exit p0

    return-void

    .line 164142
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 164137
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, LX/0y8;->mIsConnected:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164138
    monitor-exit p0

    return-void

    .line 164139
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(I)V
    .locals 1

    .prologue
    .line 164134
    monitor-enter p0

    :try_start_0
    iput p1, p0, LX/0y8;->mVideoPlayCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164135
    monitor-exit p0

    return-void

    .line 164136
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Z)V
    .locals 1

    .prologue
    .line 164152
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, LX/0y8;->mIsPhotoTakenInLastNMinutes:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164153
    monitor-exit p0

    return-void

    .line 164154
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 164133
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0y8;->mIsConnected:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(I)V
    .locals 1

    .prologue
    .line 164130
    monitor-enter p0

    :try_start_0
    iput p1, p0, LX/0y8;->mVideoPlaySecs:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164131
    monitor-exit p0

    return-void

    .line 164132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Z)V
    .locals 1

    .prologue
    .line 164127
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, LX/0y8;->mIsWaitingForNewStories:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164128
    monitor-exit p0

    return-void

    .line 164129
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 164126
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0y8;->mIsPhotoTakenInLastNMinutes:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 1

    .prologue
    .line 164125
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0y8;->mIsWaitingForNewStories:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()I
    .locals 1

    .prologue
    .line 164122
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0y8;->mReactionCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()I
    .locals 1

    .prologue
    .line 164124
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0y8;->mVideoPlayCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()I
    .locals 1

    .prologue
    .line 164123
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0y8;->mVideoPlaySecs:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
