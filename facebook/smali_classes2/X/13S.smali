.class public LX/13S;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0Zb;

.field private final c:LX/0So;

.field private final d:LX/0So;

.field public final e:Landroid/os/Handler;

.field private final f:Ljava/lang/Object;

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/13W;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mFlushSync"
    .end annotation
.end field

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/13W;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mFlushSync"
    .end annotation
.end field

.field private i:I

.field private j:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mFlushSync"
    .end annotation
.end field

.field private k:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mFlushSync"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 176833
    const-class v0, LX/13S;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/13S;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Xl;LX/0Zb;LX/0So;LX/0So;Landroid/os/Handler;)V
    .locals 2
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p3    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedAwakeTimeSinceBoot;
        .end annotation
    .end param
    .param p4    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .param p5    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param

    .prologue
    .line 176834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176835
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/13S;->f:Ljava/lang/Object;

    .line 176836
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/13S;->g:Ljava/util/Map;

    .line 176837
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/13S;->h:Ljava/util/Map;

    .line 176838
    const/4 v0, 0x0

    iput v0, p0, LX/13S;->i:I

    .line 176839
    iput-object p2, p0, LX/13S;->b:LX/0Zb;

    .line 176840
    iput-object p3, p0, LX/13S;->c:LX/0So;

    .line 176841
    iput-object p4, p0, LX/13S;->d:LX/0So;

    .line 176842
    iput-object p5, p0, LX/13S;->e:Landroid/os/Handler;

    .line 176843
    iget-object v0, p0, LX/13S;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/13S;->j:J

    .line 176844
    iget-object v0, p0, LX/13S;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/13S;->k:J

    .line 176845
    invoke-interface {p1}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance p2, LX/13T;

    invoke-direct {p2, p0}, LX/13T;-><init>(LX/13S;)V

    invoke-interface {v0, v1, p2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    new-instance p2, LX/13U;

    invoke-direct {p2, p0}, LX/13U;-><init>(LX/13S;)V

    invoke-interface {v0, v1, p2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 176846
    return-void
.end method

.method private static a(LX/13S;LX/1w0;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 176847
    iget-object v1, p0, LX/13S;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 176848
    :try_start_0
    iget-object v0, p0, LX/13S;->g:Ljava/util/Map;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v2}, LX/13S;->a(LX/1w0;Ljava/util/Map;Z)V

    .line 176849
    iget-object v0, p0, LX/13S;->h:Ljava/util/Map;

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v2}, LX/13S;->a(LX/1w0;Ljava/util/Map;Z)V

    .line 176850
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(LX/1w0;Ljava/util/Map;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1w0;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/13W;",
            ">;>;Z)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 176851
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 176852
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 176853
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 176854
    iget-object v2, p0, LX/13S;->b:LX/0Zb;

    invoke-interface {v2, v1, p3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 176855
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v3

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 176856
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 176857
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/13W;

    iget v8, p0, LX/13S;->i:I

    invoke-interface {v1, v4, v8}, LX/13W;->a(ZI)LX/0lF;

    move-result-object v1

    .line 176858
    if-eqz v1, :cond_4

    .line 176859
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 176860
    invoke-virtual {v6, v0, v1}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    move v0, v4

    :goto_2
    move v2, v0

    .line 176861
    goto :goto_1

    .line 176862
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13W;

    iget v1, p0, LX/13S;->i:I

    invoke-interface {v0, v3, v1}, LX/13W;->a(ZI)LX/0lF;

    goto :goto_1

    .line 176863
    :cond_2
    if-eqz v2, :cond_0

    .line 176864
    const-string v0, "period_start"

    iget-wide v8, p1, LX/1w0;->c:J

    invoke-virtual {v6, v0, v8, v9}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 176865
    const-string v0, "period_end"

    iget-wide v8, p1, LX/1w0;->d:J

    invoke-virtual {v6, v0, v8, v9}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 176866
    const-string v0, "real_start"

    iget-wide v8, p1, LX/1w0;->a:J

    invoke-virtual {v6, v0, v8, v9}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 176867
    const-string v0, "real_end"

    iget-wide v8, p1, LX/1w0;->b:J

    invoke-virtual {v6, v0, v8, v9}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 176868
    const-string v0, "is_background"

    iget-boolean v1, p1, LX/1w0;->e:Z

    invoke-virtual {v6, v0, v1}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 176869
    const-string v0, "session_count"

    iget v1, p1, LX/1w0;->f:I

    invoke-virtual {v6, v0, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 176870
    const-string v0, "sample_rate"

    .line 176871
    iget v1, v6, LX/0oG;->c:I

    move v1, v1

    .line 176872
    invoke-virtual {v6, v0, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 176873
    invoke-virtual {v6}, LX/0oG;->d()V

    goto/16 :goto_0

    .line 176874
    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method public static b$redex0(LX/13S;Z)V
    .locals 2

    .prologue
    .line 176875
    iget-object v1, p0, LX/13S;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 176876
    :try_start_0
    invoke-static {p0, p1}, LX/13S;->c(LX/13S;Z)LX/1w0;

    move-result-object v0

    invoke-static {p0, v0}, LX/13S;->a(LX/13S;LX/1w0;)V

    .line 176877
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static c(LX/13S;Z)LX/1w0;
    .locals 13

    .prologue
    .line 176878
    iget-object v12, p0, LX/13S;->f:Ljava/lang/Object;

    monitor-enter v12

    .line 176879
    :try_start_0
    iget-object v0, p0, LX/13S;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    .line 176880
    iget-wide v2, p0, LX/13S;->j:J

    .line 176881
    iput-wide v4, p0, LX/13S;->j:J

    .line 176882
    iget-object v0, p0, LX/13S;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v8

    .line 176883
    iget-wide v6, p0, LX/13S;->k:J

    .line 176884
    iput-wide v8, p0, LX/13S;->k:J

    .line 176885
    new-instance v1, LX/1w0;

    iget v11, p0, LX/13S;->i:I

    add-int/lit8 v0, v11, 0x1

    iput v0, p0, LX/13S;->i:I

    move v10, p1

    invoke-direct/range {v1 .. v11}, LX/1w0;-><init>(JJJJZI)V

    monitor-exit v12

    return-object v1

    .line 176886
    :catchall_0
    move-exception v0

    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/13W;)LX/13S;
    .locals 4

    .prologue
    .line 176887
    iget-object v1, p0, LX/13S;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 176888
    :try_start_0
    iget-object v0, p0, LX/13S;->g:Ljava/util/Map;

    .line 176889
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 176890
    if-nez v2, :cond_1

    .line 176891
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 176892
    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v3, v2

    .line 176893
    :goto_0
    invoke-interface {v3, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/13W;

    .line 176894
    if-eqz v2, :cond_0

    if-eq v2, p3, :cond_0

    .line 176895
    sget-object v2, LX/13S;->a:Ljava/lang/String;

    const-string v3, "Duplicate Logger Registration"

    invoke-static {v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 176896
    :goto_1
    monitor-exit v1

    return-object p0

    .line 176897
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 176898
    :cond_0
    invoke-interface {v3, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    move-object v3, v2

    goto :goto_0
.end method
