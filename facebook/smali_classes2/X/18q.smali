.class public LX/18q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/0Xl;


# direct methods
.method public constructor <init>(LX/0Xl;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 207017
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207018
    iput-object p1, p0, LX/18q;->a:LX/0Xl;

    .line 207019
    return-void
.end method

.method public static a(LX/0QB;)LX/18q;
    .locals 4

    .prologue
    .line 207020
    const-class v1, LX/18q;

    monitor-enter v1

    .line 207021
    :try_start_0
    sget-object v0, LX/18q;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 207022
    sput-object v2, LX/18q;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 207023
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207024
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 207025
    new-instance p0, LX/18q;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-direct {p0, v3}, LX/18q;-><init>(LX/0Xl;)V

    .line 207026
    move-object v0, p0

    .line 207027
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 207028
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/18q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207029
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 207030
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 207031
    const-string v0, "tab_bar_tap"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
