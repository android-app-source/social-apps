.class public final enum LX/0ea;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0ea;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0ea;

.field public static final enum ASSET:LX/0ea;

.field public static final enum DOWNLOAD:LX/0ea;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 92196
    new-instance v0, LX/0ea;

    const-string v1, "ASSET"

    invoke-direct {v0, v1, v2}, LX/0ea;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ea;->ASSET:LX/0ea;

    .line 92197
    new-instance v0, LX/0ea;

    const-string v1, "DOWNLOAD"

    invoke-direct {v0, v1, v3}, LX/0ea;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ea;->DOWNLOAD:LX/0ea;

    .line 92198
    const/4 v0, 0x2

    new-array v0, v0, [LX/0ea;

    sget-object v1, LX/0ea;->ASSET:LX/0ea;

    aput-object v1, v0, v2

    sget-object v1, LX/0ea;->DOWNLOAD:LX/0ea;

    aput-object v1, v0, v3

    sput-object v0, LX/0ea;->$VALUES:[LX/0ea;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 92199
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0ea;
    .locals 1

    .prologue
    .line 92200
    const-class v0, LX/0ea;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0ea;

    return-object v0
.end method

.method public static values()[LX/0ea;
    .locals 1

    .prologue
    .line 92201
    sget-object v0, LX/0ea;->$VALUES:[LX/0ea;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0ea;

    return-object v0
.end method
