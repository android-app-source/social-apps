.class public LX/0go;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/0go;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile f:LX/0go;


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/0lC;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/apptab/state/NavigationConfig;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/facebook/apptab/state/NavigationImmersiveConfig;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 113419
    const-class v0, LX/0go;

    sput-object v0, LX/0go;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0lC;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/apptab/state/NavigationConfig;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 113462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113463
    iput-object p1, p0, LX/0go;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 113464
    iput-object p2, p0, LX/0go;->c:LX/0lC;

    .line 113465
    iput-object p3, p0, LX/0go;->d:LX/0Ot;

    .line 113466
    return-void
.end method

.method public static a(LX/0QB;)LX/0go;
    .locals 6

    .prologue
    .line 113449
    sget-object v0, LX/0go;->f:LX/0go;

    if-nez v0, :cond_1

    .line 113450
    const-class v1, LX/0go;

    monitor-enter v1

    .line 113451
    :try_start_0
    sget-object v0, LX/0go;->f:LX/0go;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 113452
    if-eqz v2, :cond_0

    .line 113453
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 113454
    new-instance v5, LX/0go;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    const/16 p0, 0x135

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/0go;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;LX/0Ot;)V

    .line 113455
    move-object v0, v5

    .line 113456
    sput-object v0, LX/0go;->f:LX/0go;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 113457
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 113458
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 113459
    :cond_1
    sget-object v0, LX/0go;->f:LX/0go;

    return-object v0

    .line 113460
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 113461
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/facebook/apptab/state/NavigationConfig;
    .locals 1

    .prologue
    .line 113448
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0go;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/NavigationConfig;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/apptab/state/TabTag;
    .locals 3

    .prologue
    .line 113467
    invoke-virtual {p0, p1}, LX/0go;->b(Ljava/lang/String;)Lcom/facebook/apptab/state/TabTag;

    move-result-object v0

    .line 113468
    if-nez v0, :cond_0

    .line 113469
    new-instance v0, LX/0iF;

    invoke-virtual {p0}, LX/0go;->a()Lcom/facebook/apptab/state/NavigationConfig;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, LX/0iF;-><init>(Ljava/lang/String;Lcom/facebook/apptab/state/NavigationConfig;Z)V

    throw v0

    .line 113470
    :cond_0
    return-object v0
.end method

.method public final declared-synchronized b()Lcom/facebook/apptab/state/NavigationImmersiveConfig;
    .locals 6

    .prologue
    .line 113432
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0go;->e:Lcom/facebook/apptab/state/NavigationImmersiveConfig;

    if-nez v0, :cond_1

    .line 113433
    sget-object v0, LX/10R;->a:LX/0Tn;

    const-class v1, Lcom/facebook/apptab/state/NavigationImmersiveConfig;

    const/4 v2, 0x0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113434
    :try_start_1
    iget-object v3, p0, LX/0go;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->c()V

    .line 113435
    iget-object v3, p0, LX/0go;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 113436
    if-eqz v3, :cond_0

    .line 113437
    iget-object v4, p0, LX/0go;->c:LX/0lC;

    invoke-virtual {v4, v3, v1}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    move-result-object v2

    .line 113438
    :cond_0
    :goto_0
    move-object v0, v2

    .line 113439
    check-cast v0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;

    iput-object v0, p0, LX/0go;->e:Lcom/facebook/apptab/state/NavigationImmersiveConfig;

    .line 113440
    iget-object v0, p0, LX/0go;->e:Lcom/facebook/apptab/state/NavigationImmersiveConfig;

    if-nez v0, :cond_1

    .line 113441
    invoke-static {}, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->a()Lcom/facebook/apptab/state/NavigationImmersiveConfig;

    move-result-object v0

    iput-object v0, p0, LX/0go;->e:Lcom/facebook/apptab/state/NavigationImmersiveConfig;

    .line 113442
    :cond_1
    iget-object v0, p0, LX/0go;->e:Lcom/facebook/apptab/state/NavigationImmersiveConfig;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    .line 113443
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 113444
    :catch_0
    move-exception v3

    .line 113445
    sget-object v4, LX/0go;->a:Ljava/lang/Class;

    const-string v5, "Parse error reading navigation config from shared pref"

    invoke-static {v4, v5, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 113446
    :catch_1
    move-exception v3

    .line 113447
    sget-object v4, LX/0go;->a:Ljava/lang/Class;

    const-string v5, "Shared prefs initialization wait was interrupted"

    invoke-static {v4, v5, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/apptab/state/TabTag;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 113427
    invoke-virtual {p0}, LX/0go;->a()Lcom/facebook/apptab/state/NavigationConfig;

    move-result-object v0

    iget-object v2, v0, Lcom/facebook/apptab/state/NavigationConfig;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/TabTag;

    .line 113428
    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 113429
    :goto_1
    return-object v0

    .line 113430
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 113431
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final c(Ljava/lang/String;)Lcom/facebook/apptab/state/TabTag;
    .locals 5

    .prologue
    .line 113422
    invoke-virtual {p0}, LX/0go;->a()Lcom/facebook/apptab/state/NavigationConfig;

    move-result-object v0

    iget-object v2, v0, Lcom/facebook/apptab/state/NavigationConfig;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/TabTag;

    .line 113423
    iget-object v4, v0, Lcom/facebook/apptab/state/TabTag;->a:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 113424
    return-object v0

    .line 113425
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 113426
    :cond_1
    new-instance v0, LX/0iF;

    invoke-virtual {p0}, LX/0go;->a()Lcom/facebook/apptab/state/NavigationConfig;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, p1, v1, v2}, LX/0iF;-><init>(Ljava/lang/String;Lcom/facebook/apptab/state/NavigationConfig;Z)V

    throw v0
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 113420
    const/4 v0, 0x0

    iput-object v0, p0, LX/0go;->e:Lcom/facebook/apptab/state/NavigationImmersiveConfig;

    .line 113421
    return-void
.end method
