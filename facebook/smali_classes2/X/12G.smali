.class public abstract LX/12G;
.super LX/0nX;
.source ""


# instance fields
.field public b:LX/0lD;

.field public c:I

.field public d:Z

.field public e:LX/12U;

.field public f:Z


# direct methods
.method public constructor <init>(ILX/0lD;)V
    .locals 1

    .prologue
    .line 175043
    invoke-direct {p0}, LX/0nX;-><init>()V

    .line 175044
    iput p1, p0, LX/12G;->c:I

    .line 175045
    invoke-static {}, LX/12U;->i()LX/12U;

    move-result-object v0

    iput-object v0, p0, LX/12G;->e:LX/12U;

    .line 175046
    iput-object p2, p0, LX/12G;->b:LX/0lD;

    .line 175047
    sget-object v0, LX/0ls;->WRITE_NUMBERS_AS_STRINGS:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    iput-boolean v0, p0, LX/12G;->d:Z

    .line 175048
    return-void
.end method

.method private b(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 175009
    if-nez p1, :cond_0

    .line 175010
    invoke-virtual {p0}, LX/0nX;->h()V

    .line 175011
    :goto_0
    return-void

    .line 175012
    :cond_0
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 175013
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, LX/0nX;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 175014
    :cond_1
    instance-of v0, p1, Ljava/lang/Number;

    if-eqz v0, :cond_b

    move-object v0, p1

    .line 175015
    check-cast v0, Ljava/lang/Number;

    .line 175016
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 175017
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, LX/0nX;->b(I)V

    goto :goto_0

    .line 175018
    :cond_2
    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 175019
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LX/0nX;->a(J)V

    goto :goto_0

    .line 175020
    :cond_3
    instance-of v1, v0, Ljava/lang/Double;

    if-eqz v1, :cond_4

    .line 175021
    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LX/0nX;->a(D)V

    goto :goto_0

    .line 175022
    :cond_4
    instance-of v1, v0, Ljava/lang/Float;

    if-eqz v1, :cond_5

    .line 175023
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    invoke-virtual {p0, v0}, LX/0nX;->a(F)V

    goto :goto_0

    .line 175024
    :cond_5
    instance-of v1, v0, Ljava/lang/Short;

    if-eqz v1, :cond_6

    .line 175025
    invoke-virtual {v0}, Ljava/lang/Number;->shortValue()S

    move-result v0

    invoke-virtual {p0, v0}, LX/0nX;->a(S)V

    goto :goto_0

    .line 175026
    :cond_6
    instance-of v1, v0, Ljava/lang/Byte;

    if-eqz v1, :cond_7

    .line 175027
    invoke-virtual {v0}, Ljava/lang/Number;->byteValue()B

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p0, v0}, LX/0nX;->a(S)V

    goto :goto_0

    .line 175028
    :cond_7
    instance-of v1, v0, Ljava/math/BigInteger;

    if-eqz v1, :cond_8

    .line 175029
    check-cast v0, Ljava/math/BigInteger;

    invoke-virtual {p0, v0}, LX/0nX;->a(Ljava/math/BigInteger;)V

    goto :goto_0

    .line 175030
    :cond_8
    instance-of v1, v0, Ljava/math/BigDecimal;

    if-eqz v1, :cond_9

    .line 175031
    check-cast v0, Ljava/math/BigDecimal;

    invoke-virtual {p0, v0}, LX/0nX;->a(Ljava/math/BigDecimal;)V

    goto :goto_0

    .line 175032
    :cond_9
    instance-of v1, v0, Ljava/util/concurrent/atomic/AtomicInteger;

    if-eqz v1, :cond_a

    .line 175033
    check-cast v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    invoke-virtual {p0, v0}, LX/0nX;->b(I)V

    goto :goto_0

    .line 175034
    :cond_a
    instance-of v1, v0, Ljava/util/concurrent/atomic/AtomicLong;

    if-eqz v1, :cond_e

    .line 175035
    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LX/0nX;->a(J)V

    goto/16 :goto_0

    .line 175036
    :cond_b
    instance-of v0, p1, [B

    if-eqz v0, :cond_c

    .line 175037
    check-cast p1, [B

    check-cast p1, [B

    invoke-virtual {p0, p1}, LX/0nX;->a([B)V

    goto/16 :goto_0

    .line 175038
    :cond_c
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    .line 175039
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, LX/0nX;->a(Z)V

    goto/16 :goto_0

    .line 175040
    :cond_d
    instance-of v0, p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    if-eqz v0, :cond_e

    .line 175041
    check-cast p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    invoke-virtual {p0, v0}, LX/0nX;->a(Z)V

    goto/16 :goto_0

    .line 175042
    :cond_e
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No ObjectCodec defined for the generator, can only serialize simple wrapper types (type passed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static i(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 175008
    new-instance v0, LX/4pY;

    invoke-direct {v0, p0}, LX/4pY;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()LX/0lD;
    .locals 1

    .prologue
    .line 175007
    iget-object v0, p0, LX/12G;->b:LX/0lD;

    return-object v0
.end method

.method public final a(LX/0lD;)LX/0nX;
    .locals 0

    .prologue
    .line 175005
    iput-object p1, p0, LX/12G;->b:LX/0lD;

    .line 175006
    return-object p0
.end method

.method public final a(LX/0lG;)V
    .locals 2

    .prologue
    .line 174999
    if-nez p1, :cond_0

    .line 175000
    invoke-virtual {p0}, LX/0nX;->h()V

    .line 175001
    :goto_0
    return-void

    .line 175002
    :cond_0
    iget-object v0, p0, LX/12G;->b:LX/0lD;

    if-nez v0, :cond_1

    .line 175003
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No ObjectCodec defined"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175004
    :cond_1
    iget-object v0, p0, LX/12G;->b:LX/0lD;

    invoke-virtual {v0, p0, p1}, LX/0lD;->a(LX/0nX;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 174993
    if-nez p1, :cond_0

    .line 174994
    invoke-virtual {p0}, LX/0nX;->h()V

    .line 174995
    :goto_0
    return-void

    .line 174996
    :cond_0
    iget-object v0, p0, LX/12G;->b:LX/0lD;

    if-eqz v0, :cond_1

    .line 174997
    iget-object v0, p0, LX/12G;->b:LX/0lD;

    invoke-virtual {v0, p0, p1}, LX/0lD;->a(LX/0nX;Ljava/lang/Object;)V

    goto :goto_0

    .line 174998
    :cond_1
    invoke-direct {p0, p1}, LX/12G;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(LX/0ls;)Z
    .locals 2

    .prologue
    .line 174979
    iget v0, p0, LX/12G;->c:I

    invoke-virtual {p1}, LX/0ls;->getMask()I

    move-result v1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(LX/0lc;)V
    .locals 1

    .prologue
    .line 174991
    invoke-interface {p1}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 174992
    return-void
.end method

.method public c()LX/0nX;
    .locals 1

    .prologue
    .line 174988
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    move-object v0, v0

    .line 174989
    if-eqz v0, :cond_0

    .line 174990
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/0lY;

    invoke-direct {v0}, LX/0lY;-><init>()V

    invoke-virtual {p0, v0}, LX/0nX;->a(LX/0lZ;)LX/0nX;

    move-result-object p0

    goto :goto_0
.end method

.method public c(LX/0lc;)V
    .locals 1

    .prologue
    .line 174986
    invoke-interface {p1}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 174987
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 174984
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/12G;->f:Z

    .line 174985
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 174981
    const-string v0, "write raw value"

    invoke-virtual {p0, v0}, LX/12G;->h(Ljava/lang/String;)V

    .line 174982
    invoke-virtual {p0, p1}, LX/0nX;->c(Ljava/lang/String;)V

    .line 174983
    return-void
.end method

.method public abstract h(Ljava/lang/String;)V
.end method

.method public version()LX/0ne;
    .locals 1

    .prologue
    .line 174980
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, LX/4pl;->a(Ljava/lang/Class;)LX/0ne;

    move-result-object v0

    return-object v0
.end method
