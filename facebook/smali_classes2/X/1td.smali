.class public final LX/1td;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/05O;


# instance fields
.field public final synthetic a:Lcom/facebook/mqttlite/MqttService;


# direct methods
.method public constructor <init>(Lcom/facebook/mqttlite/MqttService;)V
    .locals 0

    .prologue
    .line 336641
    iput-object p1, p0, LX/1td;->a:Lcom/facebook/mqttlite/MqttService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/06d;)V
    .locals 4

    .prologue
    .line 336642
    iget-object v0, p0, LX/1td;->a:Lcom/facebook/mqttlite/MqttService;

    iget-object v0, v0, Lcom/facebook/mqttlite/MqttService;->T:LX/0mh;

    .line 336643
    iget-object v1, p1, LX/06d;->d:Ljava/lang/String;

    move-object v1, v1

    .line 336644
    iget-object v2, p1, LX/06d;->b:Ljava/lang/String;

    move-object v2, v2

    .line 336645
    const/4 p0, 0x0

    .line 336646
    sget-object v3, LX/0mq;->CLIENT_EVENT:LX/0mq;

    invoke-static {v1, v2, p0, v3, p0}, LX/1tk;->a(Ljava/lang/String;Ljava/lang/String;ZLX/0mq;Z)LX/1tk;

    move-result-object v3

    move-object v1, v3

    .line 336647
    invoke-virtual {v0, v1}, LX/0mh;->a(LX/1tk;)LX/0mj;

    move-result-object v2

    .line 336648
    invoke-virtual {v2}, LX/0mj;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 336649
    iget-object v0, p1, LX/06d;->e:Ljava/util/Map;

    move-object v0, v0

    .line 336650
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 336651
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, LX/0mj;->d(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    goto :goto_0

    .line 336652
    :cond_0
    invoke-virtual {v2}, LX/0mj;->e()V

    .line 336653
    :cond_1
    return-void
.end method
