.class public LX/0bC;
.super LX/0b1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b1",
        "<",
        "LX/0bE;",
        "LX/1ZD;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0bD;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0bD;",
            "LX/0Ot",
            "<",
            "LX/1ZD;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 86757
    invoke-direct {p0, p1, p2}, LX/0b1;-><init>(LX/0b4;LX/0Ot;)V

    .line 86758
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/0bE;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86759
    const-class v0, LX/0bE;

    return-object v0
.end method

.method public final a(LX/0b7;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 86760
    check-cast p2, LX/1ZD;

    .line 86761
    iget-object v0, p2, LX/1ZD;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86762
    iget-object v0, p2, LX/1ZD;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1ZE;->a:LX/0Tn;

    iget-object v2, p2, LX/1ZD;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 86763
    :goto_0
    return-void

    .line 86764
    :cond_0
    iget-object v0, p2, LX/1ZD;->c:LX/03V;

    const-string v1, "FeedFirstLaunchManager"

    const-string v2, "Shared preferences were not initialized onAuthFinished"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
