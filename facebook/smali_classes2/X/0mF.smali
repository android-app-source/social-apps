.class public LX/0mF;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0mh;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0mh;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 132328
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method private a()LX/0mh;
    .locals 17

    .prologue
    .line 132329
    const-class v1, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0mG;->a(LX/0QB;)LX/0mI;

    move-result-object v2

    check-cast v2, LX/0mI;

    invoke-static/range {p0 .. p0}, LX/0mJ;->a(LX/0QB;)LX/0mI;

    move-result-object v3

    check-cast v3, LX/0mI;

    const/16 v4, 0x1444

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x4cd

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {p0 .. p0}, LX/0mL;->a(LX/0QB;)LX/0mL;

    move-result-object v6

    check-cast v6, LX/0mM;

    invoke-static/range {p0 .. p0}, LX/0mO;->a(LX/0QB;)LX/0mO;

    move-result-object v7

    check-cast v7, LX/0mO;

    invoke-static/range {p0 .. p0}, LX/0mP;->a(LX/0QB;)LX/0mP;

    move-result-object v8

    check-cast v8, LX/0mP;

    invoke-static/range {p0 .. p0}, LX/0mQ;->a(LX/0QB;)Ljava/lang/Class;

    move-result-object v9

    check-cast v9, Ljava/lang/Class;

    const/16 v10, 0x1032

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static/range {p0 .. p0}, LX/0mR;->a(LX/0QB;)LX/0mR;

    move-result-object v11

    check-cast v11, LX/0mR;

    const/16 v12, 0x2ca

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static/range {p0 .. p0}, LX/0mS;->a(LX/0QB;)Ljava/lang/Class;

    move-result-object v13

    check-cast v13, Ljava/lang/Class;

    const/16 v14, 0x29f

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/0mT;->a(LX/0QB;)LX/0mU;

    move-result-object v15

    check-cast v15, LX/0mU;

    invoke-static/range {p0 .. p0}, LX/0mV;->a(LX/0QB;)LX/40D;

    move-result-object v16

    check-cast v16, LX/40D;

    invoke-static/range {v1 .. v16}, LX/0mW;->a(Landroid/content/Context;LX/0mI;LX/0mI;LX/0Or;LX/0Ot;LX/0mM;LX/0mO;LX/0mP;Ljava/lang/Class;LX/0Ot;LX/0mR;LX/0Ot;Ljava/lang/Class;LX/0Ot;LX/0mU;LX/40D;)LX/0mh;

    move-result-object v1

    return-object v1
.end method

.method public static a(LX/0QB;)LX/0mh;
    .locals 3

    .prologue
    .line 132330
    sget-object v0, LX/0mF;->a:LX/0mh;

    if-nez v0, :cond_1

    .line 132331
    const-class v1, LX/0mF;

    monitor-enter v1

    .line 132332
    :try_start_0
    sget-object v0, LX/0mF;->a:LX/0mh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 132333
    if-eqz v2, :cond_0

    .line 132334
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/0mF;->b(LX/0QB;)LX/0mh;

    move-result-object v0

    sput-object v0, LX/0mF;->a:LX/0mh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 132335
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 132336
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 132337
    :cond_1
    sget-object v0, LX/0mF;->a:LX/0mh;

    return-object v0

    .line 132338
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 132339
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/0mh;
    .locals 17

    .prologue
    .line 132340
    const-class v1, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0mG;->a(LX/0QB;)LX/0mI;

    move-result-object v2

    check-cast v2, LX/0mI;

    invoke-static/range {p0 .. p0}, LX/0mJ;->a(LX/0QB;)LX/0mI;

    move-result-object v3

    check-cast v3, LX/0mI;

    const/16 v4, 0x1444

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x4cd

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {p0 .. p0}, LX/0mL;->a(LX/0QB;)LX/0mL;

    move-result-object v6

    check-cast v6, LX/0mM;

    invoke-static/range {p0 .. p0}, LX/0mO;->a(LX/0QB;)LX/0mO;

    move-result-object v7

    check-cast v7, LX/0mO;

    invoke-static/range {p0 .. p0}, LX/0mP;->a(LX/0QB;)LX/0mP;

    move-result-object v8

    check-cast v8, LX/0mP;

    invoke-static/range {p0 .. p0}, LX/0mQ;->a(LX/0QB;)Ljava/lang/Class;

    move-result-object v9

    check-cast v9, Ljava/lang/Class;

    const/16 v10, 0x1032

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static/range {p0 .. p0}, LX/0mR;->a(LX/0QB;)LX/0mR;

    move-result-object v11

    check-cast v11, LX/0mR;

    const/16 v12, 0x2ca

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static/range {p0 .. p0}, LX/0mS;->a(LX/0QB;)Ljava/lang/Class;

    move-result-object v13

    check-cast v13, Ljava/lang/Class;

    const/16 v14, 0x29f

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/0mT;->a(LX/0QB;)LX/0mU;

    move-result-object v15

    check-cast v15, LX/0mU;

    invoke-static/range {p0 .. p0}, LX/0mV;->a(LX/0QB;)LX/40D;

    move-result-object v16

    check-cast v16, LX/40D;

    invoke-static/range {v1 .. v16}, LX/0mW;->a(Landroid/content/Context;LX/0mI;LX/0mI;LX/0Or;LX/0Ot;LX/0mM;LX/0mO;LX/0mP;Ljava/lang/Class;LX/0Ot;LX/0mR;LX/0Ot;Ljava/lang/Class;LX/0Ot;LX/0mU;LX/40D;)LX/0mh;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 132341
    invoke-direct {p0}, LX/0mF;->a()LX/0mh;

    move-result-object v0

    return-object v0
.end method
