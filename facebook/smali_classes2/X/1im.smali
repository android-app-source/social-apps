.class public LX/1im;
.super LX/1iY;
.source ""


# instance fields
.field private final a:Lcom/facebook/http/debug/NetworkStats;

.field private final b:LX/1in;

.field private final c:LX/0So;

.field private d:Lorg/apache/http/HttpHost;

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J


# direct methods
.method public constructor <init>(Lcom/facebook/http/debug/NetworkStats;LX/1in;LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 299071
    invoke-direct {p0}, LX/1iY;-><init>()V

    .line 299072
    iput-object p1, p0, LX/1im;->a:Lcom/facebook/http/debug/NetworkStats;

    .line 299073
    iput-object p2, p0, LX/1im;->b:LX/1in;

    .line 299074
    iput-object p3, p0, LX/1im;->c:LX/0So;

    .line 299075
    return-void
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 299076
    if-nez p0, :cond_0

    const-string v0, "null"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "nonnull"

    goto :goto_0
.end method

.method private a(Lorg/apache/http/HttpResponse;)V
    .locals 13

    .prologue
    .line 299077
    iget-object v0, p0, LX/1iY;->d:LX/1iW;

    move-object v10, v0

    .line 299078
    iget-object v0, v10, LX/1iW;->d:LX/03R;

    move-object v0, v0

    .line 299079
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299080
    iget-object v0, p0, LX/1im;->a:Lcom/facebook/http/debug/NetworkStats;

    invoke-direct {p0}, LX/1im;->e()Lorg/apache/http/HttpHost;

    move-result-object v1

    .line 299081
    iget-object v2, p0, LX/1im;->e:Ljava/lang/String;

    move-object v2, v2

    .line 299082
    invoke-virtual {v0, v1, v2}, Lcom/facebook/http/debug/NetworkStats;->a(Lorg/apache/http/HttpHost;Ljava/lang/String;)V

    .line 299083
    :cond_0
    :try_start_0
    iget-object v1, p0, LX/1im;->a:Lcom/facebook/http/debug/NetworkStats;

    invoke-direct {p0}, LX/1im;->e()Lorg/apache/http/HttpHost;

    move-result-object v2

    .line 299084
    iget-object v0, p0, LX/1im;->e:Ljava/lang/String;

    move-object v3, v0

    .line 299085
    iget-object v0, v10, LX/1iW;->requestHeaderBytes:LX/1iX;

    .line 299086
    iget-wide v11, v0, LX/1iX;->a:J

    move-wide v4, v11

    .line 299087
    iget-object v0, v10, LX/1iW;->requestBodyBytes:LX/1iX;

    .line 299088
    iget-wide v11, v0, LX/1iX;->a:J

    move-wide v6, v11

    .line 299089
    iget-object v0, v10, LX/1iW;->responseHeaderBytes:LX/1iX;

    .line 299090
    iget-wide v11, v0, LX/1iX;->a:J

    move-wide v8, v11

    .line 299091
    invoke-virtual/range {v1 .. v9}, Lcom/facebook/http/debug/NetworkStats;->a(Lorg/apache/http/HttpHost;Ljava/lang/String;JJJ)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 299092
    iget-object v0, p0, LX/1im;->a:Lcom/facebook/http/debug/NetworkStats;

    invoke-direct {p0}, LX/1im;->e()Lorg/apache/http/HttpHost;

    move-result-object v1

    .line 299093
    iget-object v2, p0, LX/1im;->e:Ljava/lang/String;

    move-object v2, v2

    .line 299094
    iget-object v3, p0, LX/1iY;->d:LX/1iW;

    move-object v3, v3

    .line 299095
    iget-object v3, v3, LX/1iW;->responseBodyBytes:LX/1iX;

    .line 299096
    iget-wide v11, v3, LX/1iX;->a:J

    move-wide v4, v11

    .line 299097
    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/facebook/http/debug/NetworkStats;->a(Lorg/apache/http/HttpHost;Ljava/lang/String;J)V

    .line 299098
    iget-object v0, p0, LX/1im;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/1im;->f:J

    sub-long/2addr v0, v2

    .line 299099
    iget-object v2, p0, LX/1im;->b:LX/1in;

    .line 299100
    iget-object v3, p0, LX/1im;->e:Ljava/lang/String;

    move-object v3, v3

    .line 299101
    invoke-virtual {v2, v3, v0, v1}, LX/1in;->a(Ljava/lang/String;J)V

    .line 299102
    return-void

    .line 299103
    :catch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "t12439004 mNetworkStats="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/1im;->a:Lcom/facebook/http/debug/NetworkStats;

    invoke-static {v1}, LX/1im;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mHost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1im;->d:Lorg/apache/http/HttpHost;

    invoke-static {v1}, LX/1im;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " flowStats="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v10}, LX/1im;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 299104
    if-eqz v10, :cond_1

    .line 299105
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " flowStats.requestHedaderBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v10, LX/1iW;->requestHeaderBytes:LX/1iX;

    invoke-static {v1}, LX/1im;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " flowStats.requestBodyBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v10, LX/1iW;->requestBodyBytes:LX/1iX;

    invoke-static {v1}, LX/1im;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " flowStats.responseHeaderBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v10, LX/1iW;->responseHeaderBytes:LX/1iX;

    invoke-static {v1}, LX/1im;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 299106
    :cond_1
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private e()Lorg/apache/http/HttpHost;
    .locals 2

    .prologue
    .line 299107
    iget-object v0, p0, LX/1im;->d:Lorg/apache/http/HttpHost;

    const-string v1, "mHost"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/HttpHost;

    return-object v0
.end method


# virtual methods
.method public final a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V
    .locals 0
    .param p3    # Lorg/apache/http/HttpResponse;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299108
    invoke-super/range {p0 .. p5}, LX/1iY;->a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V

    .line 299109
    invoke-direct {p0, p3}, LX/1im;->a(Lorg/apache/http/HttpResponse;)V

    .line 299110
    return-void
.end method

.method public final a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)V
    .locals 4

    .prologue
    .line 299111
    invoke-super {p0, p1, p2}, LX/1iY;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)V

    .line 299112
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 299113
    new-instance v1, Lorg/apache/http/HttpHost;

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/Uri;->getPort()I

    move-result v3

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    move-object v0, v1

    .line 299114
    iput-object v0, p0, LX/1im;->d:Lorg/apache/http/HttpHost;

    .line 299115
    invoke-static {p2}, LX/1iV;->a(Lorg/apache/http/protocol/HttpContext;)LX/1iV;

    move-result-object v0

    .line 299116
    iget-object v1, v0, LX/1iV;->a:Ljava/lang/String;

    move-object v0, v1

    .line 299117
    iput-object v0, p0, LX/1im;->e:Ljava/lang/String;

    .line 299118
    iget-object v0, p0, LX/1im;->a:Lcom/facebook/http/debug/NetworkStats;

    invoke-direct {p0}, LX/1im;->e()Lorg/apache/http/HttpHost;

    move-result-object v1

    .line 299119
    iget-object v2, p0, LX/1im;->e:Ljava/lang/String;

    move-object v2, v2

    .line 299120
    invoke-virtual {v0, v1, v2, p1}, Lcom/facebook/http/debug/NetworkStats;->a(Lorg/apache/http/HttpHost;Ljava/lang/String;Lorg/apache/http/HttpRequest;)V

    .line 299121
    iget-object v0, p0, LX/1im;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/1im;->f:J

    .line 299122
    return-void
.end method

.method public final b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 0

    .prologue
    .line 299123
    invoke-super {p0, p1, p2}, LX/1iY;->b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V

    .line 299124
    invoke-direct {p0, p1}, LX/1im;->a(Lorg/apache/http/HttpResponse;)V

    .line 299125
    return-void
.end method
