.class public LX/0kf;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0tV;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 127726
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 127727
    new-instance v0, LX/0tU;

    invoke-direct {v0}, LX/0tU;-><init>()V

    sput-object v0, LX/0kf;->a:LX/0tV;

    .line 127728
    :goto_0
    return-void

    .line 127729
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-lt v0, v1, :cond_1

    .line 127730
    new-instance v0, LX/39B;

    invoke-direct {v0}, LX/39B;-><init>()V

    sput-object v0, LX/0kf;->a:LX/0tV;

    goto :goto_0

    .line 127731
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_2

    .line 127732
    new-instance v0, LX/3r6;

    invoke-direct {v0}, LX/3r6;-><init>()V

    sput-object v0, LX/0kf;->a:LX/0tV;

    goto :goto_0

    .line 127733
    :cond_2
    new-instance v0, LX/3r5;

    invoke-direct {v0}, LX/3r5;-><init>()V

    sput-object v0, LX/0kf;->a:LX/0tV;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 127724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127725
    return-void
.end method

.method public static a(Landroid/net/ConnectivityManager;)Z
    .locals 1

    .prologue
    .line 127723
    sget-object v0, LX/0kf;->a:LX/0tV;

    invoke-interface {v0, p0}, LX/0tV;->a(Landroid/net/ConnectivityManager;)Z

    move-result v0

    return v0
.end method
