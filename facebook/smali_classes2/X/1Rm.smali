.class public abstract LX/1Rm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/concurrent/ExecutorService;

.field public b:Z

.field public final c:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 1

    .prologue
    .line 246783
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246784
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Rm;->b:Z

    .line 246785
    new-instance v0, Lcom/facebook/common/executors/IncrementalRunnable$1;

    invoke-direct {v0, p0}, Lcom/facebook/common/executors/IncrementalRunnable$1;-><init>(LX/1Rm;)V

    iput-object v0, p0, LX/1Rm;->c:Ljava/lang/Runnable;

    .line 246786
    iput-object p1, p0, LX/1Rm;->a:Ljava/util/concurrent/ExecutorService;

    .line 246787
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 246779
    iget-boolean v0, p0, LX/1Rm;->b:Z

    if-eqz v0, :cond_0

    .line 246780
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot schedule once stop is called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 246781
    :cond_0
    iget-object v0, p0, LX/1Rm;->a:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, LX/1Rm;->c:Ljava/lang/Runnable;

    const v2, 0xd3a77ca

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 246782
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 246788
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Rm;->b:Z

    .line 246789
    return-void
.end method

.method public abstract c()V
.end method

.method public abstract d()Z
.end method
