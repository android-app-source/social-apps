.class public final enum LX/1gs;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/1gt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1gs;",
        ">;",
        "LX/1gt;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1gs;

.field public static final enum EMPTY_COLLECTION:LX/1gs;

.field public static final enum END_OF_FEED:LX/1gs;

.field public static final enum FETCH_FRESH_STORIES:LX/1gs;

.field public static final enum INITIALIZED:LX/1gs;

.field public static final enum LOAD_DATA_FROM_CACHE:LX/1gs;

.field public static final enum LOAD_MORE_DATA_FROM_NETWORK:LX/1gs;

.field public static final enum LOAD_MORE_DATA_FROM_NETWORK_ABORT:LX/1gs;

.field public static final enum LOAD_NEW_DATA_FROM_NETWORK:LX/1gs;

.field public static final enum LOAD_OLDER_DATA_FROM_NETWORK:LX/1gs;

.field public static final enum NETWORK_ERROR:LX/1gs;

.field public static final enum NETWORK_NEXT:LX/1gs;

.field public static final enum NETWORK_NEXT_IS_NULL:LX/1gs;

.field public static final enum NETWORK_NEXT_NOT_IN_USE:LX/1gs;

.field public static final enum NETWORK_SUCCESS:LX/1gs;

.field public static final enum NEW_STORY_INSERTED_AT_TOP:LX/1gs;

.field public static final enum NEW_STORY_SKIPPED_INSERTION_AT_TOP:LX/1gs;

.field public static final enum NOT_INITIALIZED:LX/1gs;

.field public static final enum START_NEW_SESSION:LX/1gs;

.field public static final enum STORIES_FOR_FEED:LX/1gs;

.field public static final enum STORIES_FOR_UI:LX/1gs;

.field public static final enum STORIES_READ_FROM_DB:LX/1gs;

.field public static final enum SYNC_SEEN_STATE_FROM_DB:LX/1gs;


# instance fields
.field private mShouldLogClientEvent:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 294996
    new-instance v0, LX/1gs;

    const-string v1, "INITIALIZED"

    invoke-direct {v0, v1, v4}, LX/1gs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1gs;->INITIALIZED:LX/1gs;

    .line 294997
    new-instance v0, LX/1gs;

    const-string v1, "NOT_INITIALIZED"

    invoke-direct {v0, v1, v3}, LX/1gs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1gs;->NOT_INITIALIZED:LX/1gs;

    .line 294998
    new-instance v0, LX/1gs;

    const-string v1, "LOAD_NEW_DATA_FROM_NETWORK"

    invoke-direct {v0, v1, v5, v3}, LX/1gs;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1gs;->LOAD_NEW_DATA_FROM_NETWORK:LX/1gs;

    .line 294999
    new-instance v0, LX/1gs;

    const-string v1, "LOAD_MORE_DATA_FROM_NETWORK"

    invoke-direct {v0, v1, v6, v3}, LX/1gs;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1gs;->LOAD_MORE_DATA_FROM_NETWORK:LX/1gs;

    .line 295000
    new-instance v0, LX/1gs;

    const-string v1, "LOAD_MORE_DATA_FROM_NETWORK_ABORT"

    invoke-direct {v0, v1, v7, v3}, LX/1gs;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1gs;->LOAD_MORE_DATA_FROM_NETWORK_ABORT:LX/1gs;

    .line 295001
    new-instance v0, LX/1gs;

    const-string v1, "LOAD_DATA_FROM_CACHE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/1gs;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1gs;->LOAD_DATA_FROM_CACHE:LX/1gs;

    .line 295002
    new-instance v0, LX/1gs;

    const-string v1, "END_OF_FEED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v3}, LX/1gs;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1gs;->END_OF_FEED:LX/1gs;

    .line 295003
    new-instance v0, LX/1gs;

    const-string v1, "STORIES_FOR_FEED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/1gs;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1gs;->STORIES_FOR_FEED:LX/1gs;

    .line 295004
    new-instance v0, LX/1gs;

    const-string v1, "STORIES_FOR_UI"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v3}, LX/1gs;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1gs;->STORIES_FOR_UI:LX/1gs;

    .line 295005
    new-instance v0, LX/1gs;

    const-string v1, "STORIES_READ_FROM_DB"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v3}, LX/1gs;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1gs;->STORIES_READ_FROM_DB:LX/1gs;

    .line 295006
    new-instance v0, LX/1gs;

    const-string v1, "NETWORK_ERROR"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/1gs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1gs;->NETWORK_ERROR:LX/1gs;

    .line 295007
    new-instance v0, LX/1gs;

    const-string v1, "NETWORK_SUCCESS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/1gs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1gs;->NETWORK_SUCCESS:LX/1gs;

    .line 295008
    new-instance v0, LX/1gs;

    const-string v1, "NETWORK_NEXT"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/1gs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1gs;->NETWORK_NEXT:LX/1gs;

    .line 295009
    new-instance v0, LX/1gs;

    const-string v1, "NETWORK_NEXT_NOT_IN_USE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2, v3}, LX/1gs;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1gs;->NETWORK_NEXT_NOT_IN_USE:LX/1gs;

    .line 295010
    new-instance v0, LX/1gs;

    const-string v1, "NETWORK_NEXT_IS_NULL"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2, v3}, LX/1gs;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1gs;->NETWORK_NEXT_IS_NULL:LX/1gs;

    .line 295011
    new-instance v0, LX/1gs;

    const-string v1, "START_NEW_SESSION"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v3}, LX/1gs;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1gs;->START_NEW_SESSION:LX/1gs;

    .line 295012
    new-instance v0, LX/1gs;

    const-string v1, "FETCH_FRESH_STORIES"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2, v3}, LX/1gs;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1gs;->FETCH_FRESH_STORIES:LX/1gs;

    .line 295013
    new-instance v0, LX/1gs;

    const-string v1, "EMPTY_COLLECTION"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2, v3}, LX/1gs;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1gs;->EMPTY_COLLECTION:LX/1gs;

    .line 295014
    new-instance v0, LX/1gs;

    const-string v1, "NEW_STORY_INSERTED_AT_TOP"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2, v3}, LX/1gs;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1gs;->NEW_STORY_INSERTED_AT_TOP:LX/1gs;

    .line 295015
    new-instance v0, LX/1gs;

    const-string v1, "NEW_STORY_SKIPPED_INSERTION_AT_TOP"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2, v3}, LX/1gs;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1gs;->NEW_STORY_SKIPPED_INSERTION_AT_TOP:LX/1gs;

    .line 295016
    new-instance v0, LX/1gs;

    const-string v1, "SYNC_SEEN_STATE_FROM_DB"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2, v3}, LX/1gs;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1gs;->SYNC_SEEN_STATE_FROM_DB:LX/1gs;

    .line 295017
    new-instance v0, LX/1gs;

    const-string v1, "LOAD_OLDER_DATA_FROM_NETWORK"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2, v3}, LX/1gs;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1gs;->LOAD_OLDER_DATA_FROM_NETWORK:LX/1gs;

    .line 295018
    const/16 v0, 0x16

    new-array v0, v0, [LX/1gs;

    sget-object v1, LX/1gs;->INITIALIZED:LX/1gs;

    aput-object v1, v0, v4

    sget-object v1, LX/1gs;->NOT_INITIALIZED:LX/1gs;

    aput-object v1, v0, v3

    sget-object v1, LX/1gs;->LOAD_NEW_DATA_FROM_NETWORK:LX/1gs;

    aput-object v1, v0, v5

    sget-object v1, LX/1gs;->LOAD_MORE_DATA_FROM_NETWORK:LX/1gs;

    aput-object v1, v0, v6

    sget-object v1, LX/1gs;->LOAD_MORE_DATA_FROM_NETWORK_ABORT:LX/1gs;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/1gs;->LOAD_DATA_FROM_CACHE:LX/1gs;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1gs;->END_OF_FEED:LX/1gs;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/1gs;->STORIES_FOR_FEED:LX/1gs;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/1gs;->STORIES_FOR_UI:LX/1gs;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/1gs;->STORIES_READ_FROM_DB:LX/1gs;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/1gs;->NETWORK_ERROR:LX/1gs;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/1gs;->NETWORK_SUCCESS:LX/1gs;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/1gs;->NETWORK_NEXT:LX/1gs;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/1gs;->NETWORK_NEXT_NOT_IN_USE:LX/1gs;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/1gs;->NETWORK_NEXT_IS_NULL:LX/1gs;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/1gs;->START_NEW_SESSION:LX/1gs;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/1gs;->FETCH_FRESH_STORIES:LX/1gs;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/1gs;->EMPTY_COLLECTION:LX/1gs;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/1gs;->NEW_STORY_INSERTED_AT_TOP:LX/1gs;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/1gs;->NEW_STORY_SKIPPED_INSERTION_AT_TOP:LX/1gs;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/1gs;->SYNC_SEEN_STATE_FROM_DB:LX/1gs;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/1gs;->LOAD_OLDER_DATA_FROM_NETWORK:LX/1gs;

    aput-object v2, v0, v1

    sput-object v0, LX/1gs;->$VALUES:[LX/1gs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 294994
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/1gs;-><init>(Ljava/lang/String;IZ)V

    .line 294995
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 295019
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 295020
    iput-boolean p3, p0, LX/1gs;->mShouldLogClientEvent:Z

    .line 295021
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1gs;
    .locals 1

    .prologue
    .line 294993
    const-class v0, LX/1gs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1gs;

    return-object v0
.end method

.method public static values()[LX/1gs;
    .locals 1

    .prologue
    .line 294992
    sget-object v0, LX/1gs;->$VALUES:[LX/1gs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1gs;

    return-object v0
.end method


# virtual methods
.method public final getClientEventName()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 294989
    iget-boolean v0, p0, LX/1gs;->mShouldLogClientEvent:Z

    if-eqz v0, :cond_0

    .line 294990
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ff_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/1gs;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 294991
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 294988
    invoke-virtual {p0}, LX/1gs;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
