.class public LX/0v9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final b:LX/0vE;


# instance fields
.field public final a:LX/0vL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vL",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 157698
    sget-object v0, LX/0vB;->b:LX/0vB;

    move-object v0, v0

    .line 157699
    const/4 v3, 0x0

    .line 157700
    iget-object v1, v0, LX/0vB;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 157701
    const-class v1, LX/0vE;

    invoke-static {v1}, LX/0vB;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    .line 157702
    if-nez v1, :cond_1

    .line 157703
    iget-object v1, v0, LX/0vB;->d:Ljava/util/concurrent/atomic/AtomicReference;

    .line 157704
    sget-object v2, LX/0vF;->a:LX/0vF;

    move-object v2, v2

    .line 157705
    invoke-virtual {v1, v3, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 157706
    :cond_0
    :goto_0
    iget-object v1, v0, LX/0vB;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0vE;

    move-object v0, v1

    .line 157707
    sput-object v0, LX/0v9;->b:LX/0vE;

    return-void

    .line 157708
    :cond_1
    iget-object v2, v0, LX/0vB;->d:Ljava/util/concurrent/atomic/AtomicReference;

    check-cast v1, LX/0vE;

    invoke-virtual {v2, v3, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public constructor <init>(LX/0vL;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0vL",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 157695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157696
    iput-object p1, p0, LX/0v9;->a:LX/0vL;

    .line 157697
    return-void
.end method

.method public static final a(LX/0v9;LX/0v9;)LX/0v9;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0v9",
            "<+TT;>;",
            "LX/0v9",
            "<+TT;>;)",
            "LX/0v9",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 157694
    const/4 v0, 0x2

    new-array v0, v0, [LX/0v9;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0v9;->b(Ljava/lang/Iterable;)LX/0v9;

    move-result-object v0

    invoke-static {v0}, LX/0v9;->e(LX/0v9;)LX/0v9;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0v9;LX/52o;)LX/0v9;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "LX/52o",
            "<+TR;-TT;>;)",
            "LX/0v9",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 157709
    new-instance v0, LX/0v9;

    new-instance v1, LX/52n;

    invoke-direct {v1, p0, p1}, LX/52n;-><init>(LX/0v9;LX/52o;)V

    invoke-direct {v0, v1}, LX/0v9;-><init>(LX/0vL;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Iterable;)LX/0v9;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+TT;>;)",
            "LX/0v9",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 157689
    new-instance v0, LX/535;

    invoke-direct {v0, p0}, LX/535;-><init>(Ljava/lang/Iterable;)V

    .line 157690
    new-instance v1, LX/0v9;

    .line 157691
    move-object p0, v0

    .line 157692
    invoke-direct {v1, p0}, LX/0v9;-><init>(LX/0vL;)V

    move-object v0, v1

    .line 157693
    return-object v0
.end method

.method public static e(LX/0v9;)LX/0v9;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0v9",
            "<+",
            "LX/0v9",
            "<+TT;>;>;)",
            "LX/0v9",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 157688
    new-instance v0, LX/53K;

    invoke-direct {v0}, LX/53K;-><init>()V

    invoke-static {p0, v0}, LX/0v9;->a(LX/0v9;LX/52o;)LX/0v9;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/0zZ;)LX/0za;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zZ",
            "<-TT;>;)",
            "LX/0za;"
        }
    .end annotation

    .prologue
    .line 157671
    :try_start_0
    invoke-virtual {p1}, LX/0zZ;->d()V

    .line 157672
    iget-object v0, p0, LX/0v9;->a:LX/0vL;

    .line 157673
    move-object v0, v0

    .line 157674
    invoke-interface {v0, p1}, LX/0vM;->a(Ljava/lang/Object;)V

    .line 157675
    move-object v0, p1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 157676
    :goto_0
    return-object v0

    .line 157677
    :catch_0
    move-exception v0

    .line 157678
    invoke-static {v0}, LX/52y;->b(Ljava/lang/Throwable;)V

    .line 157679
    :try_start_1
    move-object v1, v0

    .line 157680
    invoke-virtual {p1, v1}, LX/0zZ;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catch LX/531; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    .line 157681
    sget-object v0, LX/0ze;->a:LX/0zf;

    move-object v0, v0

    .line 157682
    goto :goto_0

    .line 157683
    :catch_1
    move-exception v0

    .line 157684
    throw v0

    .line 157685
    :catch_2
    move-exception v1

    .line 157686
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error occurred attempting to subscribe ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "] and then again while trying to pass to onError."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 157687
    throw v2
.end method

.method public final b(LX/0zZ;)LX/0za;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zZ",
            "<-TT;>;)",
            "LX/0za;"
        }
    .end annotation

    .prologue
    .line 157648
    if-nez p1, :cond_0

    .line 157649
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "observer can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157650
    :cond_0
    iget-object v0, p0, LX/0v9;->a:LX/0vL;

    if-nez v0, :cond_1

    .line 157651
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onSubscribe function can not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157652
    :cond_1
    invoke-virtual {p1}, LX/0zZ;->d()V

    .line 157653
    instance-of v0, p1, LX/0zc;

    if-nez v0, :cond_2

    .line 157654
    new-instance v0, LX/0zc;

    invoke-direct {v0, p1}, LX/0zc;-><init>(LX/0zZ;)V

    move-object p1, v0

    .line 157655
    :cond_2
    :try_start_0
    iget-object v0, p0, LX/0v9;->a:LX/0vL;

    .line 157656
    move-object v0, v0

    .line 157657
    invoke-interface {v0, p1}, LX/0vM;->a(Ljava/lang/Object;)V

    .line 157658
    move-object v0, p1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 157659
    :goto_0
    return-object v0

    .line 157660
    :catch_0
    move-exception v0

    .line 157661
    invoke-static {v0}, LX/52y;->b(Ljava/lang/Throwable;)V

    .line 157662
    :try_start_1
    move-object v1, v0

    .line 157663
    invoke-virtual {p1, v1}, LX/0zZ;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catch LX/531; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    .line 157664
    sget-object v0, LX/0ze;->a:LX/0zf;

    move-object v0, v0

    .line 157665
    goto :goto_0

    .line 157666
    :catch_1
    move-exception v0

    .line 157667
    throw v0

    .line 157668
    :catch_2
    move-exception v1

    .line 157669
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error occurred attempting to subscribe ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "] and then again while trying to pass to onError."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 157670
    throw v2
.end method
