.class public LX/0vx;
.super LX/0vy;
.source ""


# static fields
.field public static b:Ljava/lang/reflect/Field;

.field public static c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 158379
    const/4 v0, 0x0

    sput-boolean v0, LX/0vx;->c:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 158378
    invoke-direct {p0}, LX/0vy;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/0vn;)V
    .locals 1
    .param p2    # LX/0vn;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 158373
    if-nez p2, :cond_0

    const/4 v0, 0x0

    .line 158374
    :goto_0
    check-cast v0, Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {p1, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 158375
    return-void

    .line 158376
    :cond_0
    iget-object v0, p2, LX/0vn;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 158377
    goto :goto_0
.end method

.method public final a(Landroid/view/View;LX/3sp;)V
    .locals 1

    .prologue
    .line 158370
    iget-object v0, p2, LX/3sp;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 158371
    check-cast v0, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, v0}, Landroid/view/View;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 158372
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 0

    .prologue
    .line 158368
    invoke-virtual {p1, p2}, Landroid/view/View;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 158369
    return-void
.end method

.method public final a(Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 158366
    invoke-virtual {p1, p2}, Landroid/view/View;->setFitsSystemWindows(Z)V

    .line 158367
    return-void
.end method

.method public final a(Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 158364
    invoke-virtual {p1, p2}, Landroid/view/View;->canScrollHorizontally(I)Z

    move-result v0

    move v0, v0

    .line 158365
    return v0
.end method

.method public final b(Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 158347
    sget-boolean v2, LX/0vx;->c:Z

    if-eqz v2, :cond_1

    .line 158348
    :cond_0
    :goto_0
    return v0

    .line 158349
    :cond_1
    sget-object v2, LX/0vx;->b:Ljava/lang/reflect/Field;

    if-nez v2, :cond_2

    .line 158350
    :try_start_0
    const-class v2, Landroid/view/View;

    const-string v3, "mAccessibilityDelegate"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 158351
    sput-object v2, LX/0vx;->b:Ljava/lang/reflect/Field;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 158352
    :cond_2
    :try_start_1
    sget-object v2, LX/0vx;->b:Ljava/lang/reflect/Field;

    invoke-virtual {v2, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 158353
    :catch_0
    sput-boolean v1, LX/0vx;->c:Z

    goto :goto_0

    .line 158354
    :catch_1
    sput-boolean v1, LX/0vx;->c:Z

    goto :goto_0
.end method

.method public final b(Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 158362
    invoke-virtual {p1, p2}, Landroid/view/View;->canScrollVertically(I)Z

    move-result v0

    move v0, v0

    .line 158363
    return v0
.end method

.method public final w(Landroid/view/View;)LX/3sU;
    .locals 2

    .prologue
    .line 158355
    iget-object v0, p0, LX/0w1;->a:Ljava/util/WeakHashMap;

    if-nez v0, :cond_0

    .line 158356
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/0vx;->a:Ljava/util/WeakHashMap;

    .line 158357
    :cond_0
    iget-object v0, p0, LX/0w1;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3sU;

    .line 158358
    if-nez v0, :cond_1

    .line 158359
    new-instance v0, LX/3sU;

    invoke-direct {v0, p1}, LX/3sU;-><init>(Landroid/view/View;)V

    .line 158360
    iget-object v1, p0, LX/0w1;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158361
    :cond_1
    return-object v0
.end method
