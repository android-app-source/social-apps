.class public LX/1Ml;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 236297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236298
    iput-object p1, p0, LX/1Ml;->a:Landroid/content/Context;

    .line 236299
    iput-object p2, p0, LX/1Ml;->b:Lcom/facebook/content/SecureContextHelper;

    .line 236300
    return-void
.end method

.method public static a(LX/0QB;)LX/1Ml;
    .locals 1

    .prologue
    .line 236296
    invoke-static {p0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1Ml;
    .locals 3

    .prologue
    .line 236294
    new-instance v2, LX/1Ml;

    const-class v0, Landroid/content/Context;

    const-class v1, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v2, v0, v1}, LX/1Ml;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V

    .line 236295
    return-object v2
.end method

.method public static g(LX/1Ml;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 236291
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 236292
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "package:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/1Ml;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 236293
    return-object v0
.end method


# virtual methods
.method public final a(Z)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 236301
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.action.MANAGE_OVERLAY_PERMISSION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 236302
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "package:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/1Ml;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 236303
    if-eqz p1, :cond_0

    .line 236304
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 236305
    :cond_0
    return-object v0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 236288
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_0

    .line 236289
    const-string v0, "android.permission.SYSTEM_ALERT_WINDOW"

    invoke-virtual {p0, v0}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    .line 236290
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1Ml;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/provider/Settings;->canDrawOverlays(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 236282
    :try_start_0
    const-string v2, "android.permission.SYSTEM_ALERT_WINDOW"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v2, v3, :cond_1

    .line 236283
    invoke-virtual {p0}, LX/1Ml;->a()Z

    move-result v0

    .line 236284
    :cond_0
    :goto_0
    return v0

    .line 236285
    :cond_1
    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_0

    .line 236286
    :cond_2
    iget-object v2, p0, LX/1Ml;->a:Landroid/content/Context;

    invoke-virtual {v2, p1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 236287
    :catch_0
    move v0, v1

    goto :goto_0
.end method

.method public final a([Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 236277
    move v0, v1

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 236278
    aget-object v2, p1, v0

    invoke-virtual {p0, v2}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 236279
    :goto_1
    return v1

    .line 236280
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 236281
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 236273
    invoke-static {p0}, LX/1Ml;->g(LX/1Ml;)Landroid/content/Intent;

    move-result-object v0

    .line 236274
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 236275
    iget-object v1, p0, LX/1Ml;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/1Ml;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 236276
    return-void
.end method
