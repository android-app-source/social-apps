.class public LX/1Fl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:I

.field private final b:LX/1FQ;


# direct methods
.method public constructor <init>(LX/1FQ;)V
    .locals 1

    .prologue
    .line 224574
    const/16 v0, 0x4000

    invoke-direct {p0, p1, v0}, LX/1Fl;-><init>(LX/1FQ;I)V

    .line 224575
    return-void
.end method

.method private constructor <init>(LX/1FQ;I)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 224585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224586
    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 224587
    iput p2, p0, LX/1Fl;->a:I

    .line 224588
    iput-object p1, p0, LX/1Fl;->b:LX/1FQ;

    .line 224589
    return-void

    .line 224590
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    .locals 6

    .prologue
    .line 224576
    const-wide/16 v2, 0x0

    .line 224577
    iget-object v0, p0, LX/1Fl;->b:LX/1FQ;

    iget v1, p0, LX/1Fl;->a:I

    invoke-interface {v0, v1}, LX/1FS;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 224578
    :goto_0
    const/4 v1, 0x0

    :try_start_0
    iget v4, p0, LX/1Fl;->a:I

    invoke-virtual {p1, v0, v1, v4}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 224579
    const/4 v4, -0x1

    if-ne v1, v4, :cond_0

    .line 224580
    iget-object v1, p0, LX/1Fl;->b:LX/1FQ;

    invoke-interface {v1, v0}, LX/1FS;->a(Ljava/lang/Object;)V

    return-wide v2

    .line 224581
    :cond_0
    const/4 v4, 0x0

    :try_start_1
    invoke-virtual {p2, v0, v4, v1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 224582
    int-to-long v4, v1

    add-long/2addr v2, v4

    .line 224583
    goto :goto_0

    .line 224584
    :catchall_0
    move-exception v1

    iget-object v2, p0, LX/1Fl;->b:LX/1FQ;

    invoke-interface {v2, v0}, LX/1FS;->a(Ljava/lang/Object;)V

    throw v1
.end method
