.class public LX/1ju;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0uc;


# instance fields
.field private a:LX/1jk;

.field private b:LX/1jr;

.field private c:Ljava/lang/String;

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 300985
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/1jk;LX/1jr;I)V
    .locals 3

    .prologue
    .line 300986
    const/4 v0, 0x3

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 300987
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 300988
    invoke-virtual {p1}, LX/1jr;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300989
    const-string v1, " (policy: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300990
    invoke-virtual {p1}, LX/1jr;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300991
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300992
    const-string v1, " - RESULT - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300993
    invoke-virtual {p0}, LX/1jk;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300994
    const-string v1, " , Contexts: "

    invoke-virtual {p1, v0, v1}, LX/1jr;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 300995
    const-string v1, " , Contexts: none"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300996
    :cond_0
    const-string v1, " , Buckets: "

    invoke-virtual {p1, v0, v1}, LX/1jr;->c(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 300997
    const-string v1, " , Buckets: none"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300998
    :cond_1
    const-string v1, " , Values: "

    invoke-virtual {p1, v0, v1}, LX/1jr;->b(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 300999
    const-string v1, " , Values: none"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301000
    :cond_2
    const-string v1, " , Monitors: "

    invoke-virtual {p1, v0, v1}, LX/1jr;->d(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 301001
    const-string v1, " , Monitors: none"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301002
    :cond_3
    const-string v1, " , MonitorValues: "

    invoke-virtual {p1, v0, v1}, LX/1jr;->e(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 301003
    const-string v1, " , MonitorValues: none"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301004
    :cond_4
    const-string v1, " , Result: "

    invoke-virtual {p1, v0, v1}, LX/1jr;->f(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 301005
    const-string v1, " , Result: INVALID"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301006
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " , json: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 301007
    iget-object v2, p0, LX/1jk;->a:Ljava/lang/String;

    move-object v2, v2

    .line 301008
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301009
    const-string v1, " , sample_rate: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301010
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 301011
    :cond_6
    return-void
.end method

.method public static b(LX/1jk;Ljava/lang/String;I)V
    .locals 3
    .param p0    # LX/1jk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 301012
    const/4 v0, 0x3

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301013
    if-eqz p0, :cond_0

    .line 301014
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 301015
    const-string v1, "ERROR - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301016
    invoke-virtual {p0}, LX/1jk;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301017
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " - exception: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301018
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " , json: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 301019
    iget-object v2, p0, LX/1jk;->a:Ljava/lang/String;

    move-object v2, v2

    .line 301020
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301021
    const-string v1, " , sample_rate: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301022
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 301023
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/1jk;LX/1jr;I)V
    .locals 1

    .prologue
    .line 301024
    invoke-static {p1, p2, p3}, LX/1ju;->b(LX/1jk;LX/1jr;I)V

    .line 301025
    iput-object p1, p0, LX/1ju;->a:LX/1jk;

    .line 301026
    iput-object p2, p0, LX/1ju;->b:LX/1jr;

    .line 301027
    const/4 v0, 0x0

    iput-object v0, p0, LX/1ju;->c:Ljava/lang/String;

    .line 301028
    iput p3, p0, LX/1ju;->d:I

    .line 301029
    return-void
.end method

.method public final a(LX/1jk;Ljava/lang/String;I)V
    .locals 1
    .param p1    # LX/1jk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 301030
    invoke-static {p1, p2, p3}, LX/1ju;->b(LX/1jk;Ljava/lang/String;I)V

    .line 301031
    iput-object p1, p0, LX/1ju;->a:LX/1jk;

    .line 301032
    iput-object p2, p0, LX/1ju;->c:Ljava/lang/String;

    .line 301033
    const/4 v0, 0x0

    iput-object v0, p0, LX/1ju;->b:LX/1jr;

    .line 301034
    iput p3, p0, LX/1ju;->d:I

    .line 301035
    return-void
.end method
