.class public LX/1En;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile e:LX/1En;


# instance fields
.field public final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/1Er;

.field public final d:LX/1Eo;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 221171
    const-class v0, LX/1En;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1En;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Eo;Ljava/util/concurrent/ExecutorService;LX/1Er;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 221150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221151
    iput-object p2, p0, LX/1En;->b:Ljava/util/concurrent/ExecutorService;

    .line 221152
    iput-object p3, p0, LX/1En;->c:LX/1Er;

    .line 221153
    iput-object p1, p0, LX/1En;->d:LX/1Eo;

    .line 221154
    return-void
.end method

.method public static a(LX/0QB;)LX/1En;
    .locals 6

    .prologue
    .line 221158
    sget-object v0, LX/1En;->e:LX/1En;

    if-nez v0, :cond_1

    .line 221159
    const-class v1, LX/1En;

    monitor-enter v1

    .line 221160
    :try_start_0
    sget-object v0, LX/1En;->e:LX/1En;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 221161
    if-eqz v2, :cond_0

    .line 221162
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 221163
    new-instance p0, LX/1En;

    invoke-static {v0}, LX/1Eo;->a(LX/0QB;)LX/1Eo;

    move-result-object v3

    check-cast v3, LX/1Eo;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v5

    check-cast v5, LX/1Er;

    invoke-direct {p0, v3, v4, v5}, LX/1En;-><init>(LX/1Eo;Ljava/util/concurrent/ExecutorService;LX/1Er;)V

    .line 221164
    move-object v0, p0

    .line 221165
    sput-object v0, LX/1En;->e:LX/1En;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 221166
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 221167
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 221168
    :cond_1
    sget-object v0, LX/1En;->e:LX/1En;

    return-object v0

    .line 221169
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 221170
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/7gj;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7gj;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221155
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 221156
    iget-object v1, p0, LX/1En;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/audience/util/MediaProcessor$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/facebook/audience/util/MediaProcessor$1;-><init>(LX/1En;LX/7gj;Lcom/google/common/util/concurrent/SettableFuture;)V

    const v3, -0x4ee4499a

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 221157
    return-object v0
.end method
