.class public LX/0bu;
.super LX/0bi;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0bi",
        "<",
        "Lcom/facebook/mobileconfig/init/MobileConfigInit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0bu;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/init/MobileConfigInit;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 87411
    const/16 v0, 0xd

    invoke-direct {p0, p1, v0}, LX/0bi;-><init>(LX/0Ot;I)V

    .line 87412
    return-void
.end method

.method public static a(LX/0QB;)LX/0bu;
    .locals 4

    .prologue
    .line 87413
    sget-object v0, LX/0bu;->b:LX/0bu;

    if-nez v0, :cond_1

    .line 87414
    const-class v1, LX/0bu;

    monitor-enter v1

    .line 87415
    :try_start_0
    sget-object v0, LX/0bu;->b:LX/0bu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 87416
    if-eqz v2, :cond_0

    .line 87417
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 87418
    new-instance v3, LX/0bu;

    const/16 p0, 0xdf8

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/0bu;-><init>(LX/0Ot;)V

    .line 87419
    move-object v0, v3

    .line 87420
    sput-object v0, LX/0bu;->b:LX/0bu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87421
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 87422
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 87423
    :cond_1
    sget-object v0, LX/0bu;->b:LX/0bu;

    return-object v0

    .line 87424
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 87425
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Uh;ILjava/lang/Object;)V
    .locals 1
    .param p1    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param

    .prologue
    .line 87426
    check-cast p3, Lcom/facebook/mobileconfig/init/MobileConfigInit;

    .line 87427
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 87428
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 87429
    if-eqz v0, :cond_0

    .line 87430
    invoke-virtual {p3}, Lcom/facebook/mobileconfig/init/MobileConfigInit;->init()V

    .line 87431
    :cond_0
    return-void
.end method
