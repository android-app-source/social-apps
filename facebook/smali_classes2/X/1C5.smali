.class public LX/1C5;
.super LX/1C6;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 215382
    const-class v0, LX/1C4;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1C5;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0So;LX/03V;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 215383
    const-wide/32 v0, 0x1b7740

    const/16 v2, 0x1e

    invoke-direct {p0, p1, v0, v1, v2}, LX/1C6;-><init>(LX/0So;JI)V

    .line 215384
    iput-object p2, p0, LX/1C5;->b:LX/03V;

    .line 215385
    return-void
.end method


# virtual methods
.method public a(LX/7KB;)V
    .locals 4

    .prologue
    .line 215379
    iget-object v0, p0, LX/1C5;->b:LX/03V;

    sget-object v1, LX/1C5;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid video-session:\n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/7KB;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 215380
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 215381
    const/4 v0, 0x0

    return v0
.end method
