.class public LX/1Wp;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 269707
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;LX/1VF;LX/14w;)LX/1Wi;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pr;",
            ":",
            "LX/1Pg;",
            ":",
            "LX/1Ps;",
            ":",
            "LX/1Po;",
            ">(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;",
            "LX/1VF;",
            "LX/14w;",
            ")",
            "LX/1Wi;"
        }
    .end annotation

    .prologue
    .line 269673
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 269674
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, p1

    .line 269675
    check-cast v1, LX/1Po;

    .line 269676
    instance-of v2, v1, LX/1Pf;

    if-eqz v2, :cond_a

    move-object v2, v1

    check-cast v2, LX/1Pf;

    invoke-interface {v2}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    if-eqz v2, :cond_a

    sget-object v2, LX/1Qt;->GROUPS:LX/1Qt;

    check-cast v1, LX/1Pf;

    invoke-interface {v1}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    invoke-interface {v3}, LX/1PT;->a()LX/1Qt;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-static {p0}, LX/14w;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 269677
    if-eqz v1, :cond_0

    .line 269678
    sget-object v0, LX/1Wi;->COMPACT_STORIES:LX/1Wi;

    .line 269679
    :goto_1
    return-object v0

    .line 269680
    :cond_0
    invoke-virtual {p2, v0}, LX/1VF;->c(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    move v1, v1

    .line 269681
    if-nez v1, :cond_2

    .line 269682
    invoke-virtual {p2, p0}, LX/1VF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p2, p0}, LX/1VF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_b

    :cond_1
    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 269683
    if-eqz v1, :cond_3

    .line 269684
    :cond_2
    sget-object v0, LX/1Wi;->HAS_INLINE_COMMENTS:LX/1Wi;

    goto :goto_1

    .line 269685
    :cond_3
    const/4 v2, 0x0

    .line 269686
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->B()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_c

    :cond_4
    move v1, v2

    .line 269687
    :goto_3
    move v0, v1

    .line 269688
    if-eqz v0, :cond_5

    .line 269689
    sget-object v0, LX/1Wi;->HAS_INLINE_SURVEY:LX/1Wi;

    goto :goto_1

    .line 269690
    :cond_5
    invoke-static {p0}, LX/14w;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_6

    check-cast p1, LX/1Ps;

    const/4 v1, 0x0

    .line 269691
    if-nez p1, :cond_f

    move v0, v1

    .line 269692
    :goto_4
    move v0, v0

    .line 269693
    if-eqz v0, :cond_7

    .line 269694
    :cond_6
    sget-object v0, LX/1Wi;->HAS_FOLLOWUP_SECTION:LX/1Wi;

    goto :goto_1

    .line 269695
    :cond_7
    invoke-static {p0}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {p0}, LX/14w;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_8
    const/4 v0, 0x1

    :goto_5
    move v0, v0

    .line 269696
    if-eqz v0, :cond_9

    .line 269697
    sget-object v0, LX/1Wi;->TOP:LX/1Wi;

    goto :goto_1

    .line 269698
    :cond_9
    const/4 v0, 0x0

    goto :goto_1

    :cond_a
    const/4 v2, 0x0

    goto :goto_0

    :cond_b
    const/4 v1, 0x0

    goto :goto_2

    .line 269699
    :cond_c
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->B()LX/0Px;

    move-result-object v4

    .line 269700
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_6
    if-ge v3, v5, :cond_e

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 269701
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p2

    if-eqz p2, :cond_d

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const p2, 0x650f0e32

    if-ne v1, p2, :cond_d

    .line 269702
    const/4 v1, 0x1

    goto :goto_3

    .line 269703
    :cond_d
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_6

    :cond_e
    move v1, v2

    .line 269704
    goto :goto_3

    .line 269705
    :cond_f
    invoke-interface {p1}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    .line 269706
    instance-of v2, v0, LX/1Vh;

    if-eqz v2, :cond_10

    check-cast v0, LX/1Vh;

    invoke-interface {v0}, LX/1Vh;->b()LX/1X8;

    move-result-object v0

    sget-object v2, LX/1X8;->FOLLOWUP_FEEDUNIT:LX/1X8;

    if-ne v0, v2, :cond_10

    const/4 v0, 0x1

    goto :goto_4

    :cond_10
    move v0, v1

    goto :goto_4

    :cond_11
    const/4 v0, 0x0

    goto :goto_5
.end method
