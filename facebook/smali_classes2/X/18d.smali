.class public final LX/18d;
.super LX/18e;
.source ""

# interfaces
.implements LX/0Xu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/18e;",
        "LX/0Xu",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public transient a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field

.field public transient b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field

.field public transient c:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field public transient d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field public transient e:LX/1M1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1M1",
            "<TK;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Xu;Ljava/lang/Object;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xu",
            "<TK;TV;>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 206867
    invoke-direct {p0, p1, p2}, LX/18e;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 206868
    return-void
.end method

.method private a()LX/0Xu;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Xu",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 206866
    invoke-super {p0}, LX/18e;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xu;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/lang/Iterable",
            "<+TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 206863
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 206864
    :try_start_0
    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 206865
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)Z"
        }
    .end annotation

    .prologue
    .line 206860
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 206861
    :try_start_0
    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 206862
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 206855
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 206856
    :try_start_0
    iget-object v0, p0, LX/18d;->d:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 206857
    new-instance v0, LX/50z;

    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v2

    invoke-interface {v2}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, LX/18e;->mutex:Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, LX/50z;-><init>(Ljava/util/Map;Ljava/lang/Object;)V

    iput-object v0, p0, LX/18d;->d:Ljava/util/Map;

    .line 206858
    :cond_0
    iget-object v0, p0, LX/18d;->d:Ljava/util/Map;

    monitor-exit v1

    return-object v0

    .line 206859
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 206852
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 206853
    :try_start_0
    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/0Xu;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 206854
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 206851
    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 206848
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 206849
    :try_start_0
    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, LX/18e;->mutex:Ljava/lang/Object;

    invoke-static {v0, v2}, LX/2sR;->d(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 206850
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 206845
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 206846
    :try_start_0
    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/0Xu;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 206847
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 206842
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 206843
    :try_start_0
    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0Xu;->d(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 206844
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 206869
    if-ne p1, p0, :cond_0

    .line 206870
    const/4 v0, 0x1

    .line 206871
    :goto_0
    return v0

    .line 206872
    :cond_0
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 206873
    :try_start_0
    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0Xu;->equals(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 206874
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 206801
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 206802
    :try_start_0
    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0}, LX/0Xu;->f()I

    move-result v0

    monitor-exit v1

    return v0

    .line 206803
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final f(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 206804
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 206805
    :try_start_0
    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0Xu;->f(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 206806
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 206807
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 206808
    :try_start_0
    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0}, LX/0Xu;->g()V

    .line 206809
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final g(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 206810
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 206811
    :try_start_0
    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0Xu;->g(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 206812
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 206813
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 206814
    :try_start_0
    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0}, LX/0Xu;->hashCode()I

    move-result v0

    monitor-exit v1

    return v0

    .line 206815
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final i()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 206816
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 206817
    :try_start_0
    iget-object v0, p0, LX/18d;->b:Ljava/util/Collection;

    if-nez v0, :cond_0

    .line 206818
    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0}, LX/0Xu;->i()Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, LX/18e;->mutex:Ljava/lang/Object;

    invoke-static {v0, v2}, LX/2sR;->c(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, LX/18d;->b:Ljava/util/Collection;

    .line 206819
    :cond_0
    iget-object v0, p0, LX/18d;->b:Ljava/util/Collection;

    monitor-exit v1

    return-object v0

    .line 206820
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final k()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 206821
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 206822
    :try_start_0
    iget-object v0, p0, LX/18d;->c:Ljava/util/Collection;

    if-nez v0, :cond_0

    .line 206823
    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0}, LX/0Xu;->k()Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, LX/18e;->mutex:Ljava/lang/Object;

    invoke-static {v0, v2}, LX/2sR;->d(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, LX/18d;->c:Ljava/util/Collection;

    .line 206824
    :cond_0
    iget-object v0, p0, LX/18d;->c:Ljava/util/Collection;

    monitor-exit v1

    return-object v0

    .line 206825
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 206826
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 206827
    :try_start_0
    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0}, LX/0Xu;->n()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 206828
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final p()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 206829
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 206830
    :try_start_0
    iget-object v0, p0, LX/18d;->a:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 206831
    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, LX/18e;->mutex:Ljava/lang/Object;

    invoke-static {v0, v2}, LX/2sR;->c(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/18d;->a:Ljava/util/Set;

    .line 206832
    :cond_0
    iget-object v0, p0, LX/18d;->a:Ljava/util/Set;

    monitor-exit v1

    return-object v0

    .line 206833
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final q()LX/1M1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1M1",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 206834
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 206835
    :try_start_0
    iget-object v0, p0, LX/18d;->e:LX/1M1;

    if-nez v0, :cond_1

    .line 206836
    invoke-direct {p0}, LX/18d;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0}, LX/0Xu;->q()LX/1M1;

    move-result-object v0

    iget-object v2, p0, LX/18e;->mutex:Ljava/lang/Object;

    .line 206837
    instance-of v3, v0, LX/518;

    if-nez v3, :cond_0

    instance-of v3, v0, LX/4yO;

    if-eqz v3, :cond_2

    .line 206838
    :cond_0
    :goto_0
    move-object v0, v0

    .line 206839
    iput-object v0, p0, LX/18d;->e:LX/1M1;

    .line 206840
    :cond_1
    iget-object v0, p0, LX/18d;->e:LX/1M1;

    monitor-exit v1

    return-object v0

    .line 206841
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    new-instance v3, LX/518;

    invoke-direct {v3, v0, v2}, LX/518;-><init>(LX/1M1;Ljava/lang/Object;)V

    move-object v0, v3

    goto :goto_0
.end method
