.class public final enum LX/1eW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1eW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1eW;

.field public static final enum IDLE:LX/1eW;

.field public static final enum QUEUED:LX/1eW;

.field public static final enum RUNNING:LX/1eW;

.field public static final enum RUNNING_AND_PENDING:LX/1eW;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 288531
    new-instance v0, LX/1eW;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, LX/1eW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1eW;->IDLE:LX/1eW;

    new-instance v0, LX/1eW;

    const-string v1, "QUEUED"

    invoke-direct {v0, v1, v3}, LX/1eW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1eW;->QUEUED:LX/1eW;

    new-instance v0, LX/1eW;

    const-string v1, "RUNNING"

    invoke-direct {v0, v1, v4}, LX/1eW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1eW;->RUNNING:LX/1eW;

    new-instance v0, LX/1eW;

    const-string v1, "RUNNING_AND_PENDING"

    invoke-direct {v0, v1, v5}, LX/1eW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1eW;->RUNNING_AND_PENDING:LX/1eW;

    .line 288532
    const/4 v0, 0x4

    new-array v0, v0, [LX/1eW;

    sget-object v1, LX/1eW;->IDLE:LX/1eW;

    aput-object v1, v0, v2

    sget-object v1, LX/1eW;->QUEUED:LX/1eW;

    aput-object v1, v0, v3

    sget-object v1, LX/1eW;->RUNNING:LX/1eW;

    aput-object v1, v0, v4

    sget-object v1, LX/1eW;->RUNNING_AND_PENDING:LX/1eW;

    aput-object v1, v0, v5

    sput-object v0, LX/1eW;->$VALUES:[LX/1eW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 288533
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1eW;
    .locals 1

    .prologue
    .line 288534
    const-class v0, LX/1eW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1eW;

    return-object v0
.end method

.method public static values()[LX/1eW;
    .locals 1

    .prologue
    .line 288535
    sget-object v0, LX/1eW;->$VALUES:[LX/1eW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1eW;

    return-object v0
.end method
