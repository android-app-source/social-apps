.class public final LX/1XI;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1WX;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLStory;

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:LX/1dQ;

.field public f:I

.field public g:I

.field public h:I

.field public i:F

.field public j:F

.field public k:F

.field public final synthetic l:LX/1WX;


# direct methods
.method public constructor <init>(LX/1WX;)V
    .locals 1

    .prologue
    .line 270892
    iput-object p1, p0, LX/1XI;->l:LX/1WX;

    .line 270893
    move-object v0, p1

    .line 270894
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 270895
    const v0, -0x6e685d

    iput v0, p0, LX/1XI;->f:I

    .line 270896
    const/4 v0, 0x0

    iput v0, p0, LX/1XI;->g:I

    .line 270897
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 270891
    const-string v0, "UFIFeedbackSummaryComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 270859
    if-ne p0, p1, :cond_1

    .line 270860
    :cond_0
    :goto_0
    return v0

    .line 270861
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 270862
    goto :goto_0

    .line 270863
    :cond_3
    check-cast p1, LX/1XI;

    .line 270864
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 270865
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 270866
    if-eq v2, v3, :cond_0

    .line 270867
    iget-object v2, p0, LX/1XI;->a:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/1XI;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v3, p1, LX/1XI;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 270868
    goto :goto_0

    .line 270869
    :cond_5
    iget-object v2, p1, LX/1XI;->a:Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v2, :cond_4

    .line 270870
    :cond_6
    iget-boolean v2, p0, LX/1XI;->b:Z

    iget-boolean v3, p1, LX/1XI;->b:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 270871
    goto :goto_0

    .line 270872
    :cond_7
    iget-boolean v2, p0, LX/1XI;->c:Z

    iget-boolean v3, p1, LX/1XI;->c:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 270873
    goto :goto_0

    .line 270874
    :cond_8
    iget-boolean v2, p0, LX/1XI;->d:Z

    iget-boolean v3, p1, LX/1XI;->d:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 270875
    goto :goto_0

    .line 270876
    :cond_9
    iget-object v2, p0, LX/1XI;->e:LX/1dQ;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/1XI;->e:LX/1dQ;

    iget-object v3, p1, LX/1XI;->e:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 270877
    goto :goto_0

    .line 270878
    :cond_b
    iget-object v2, p1, LX/1XI;->e:LX/1dQ;

    if-nez v2, :cond_a

    .line 270879
    :cond_c
    iget v2, p0, LX/1XI;->f:I

    iget v3, p1, LX/1XI;->f:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 270880
    goto :goto_0

    .line 270881
    :cond_d
    iget v2, p0, LX/1XI;->g:I

    iget v3, p1, LX/1XI;->g:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 270882
    goto :goto_0

    .line 270883
    :cond_e
    iget v2, p0, LX/1XI;->h:I

    iget v3, p1, LX/1XI;->h:I

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 270884
    goto :goto_0

    .line 270885
    :cond_f
    iget v2, p0, LX/1XI;->i:F

    iget v3, p1, LX/1XI;->i:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_10

    move v0, v1

    .line 270886
    goto :goto_0

    .line 270887
    :cond_10
    iget v2, p0, LX/1XI;->j:F

    iget v3, p1, LX/1XI;->j:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_11

    move v0, v1

    .line 270888
    goto/16 :goto_0

    .line 270889
    :cond_11
    iget v2, p0, LX/1XI;->k:F

    iget v3, p1, LX/1XI;->k:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 270890
    goto/16 :goto_0
.end method
