.class public LX/1Sk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/1Sk;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:LX/79k;

.field private c:J

.field private d:LX/0W3;

.field private final e:LX/0TD;

.field private final f:LX/0tX;

.field private final g:LX/0So;


# direct methods
.method public constructor <init>(LX/0W3;LX/0TD;LX/0tX;LX/0So;)V
    .locals 1
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 249188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 249189
    iput-object p1, p0, LX/1Sk;->d:LX/0W3;

    .line 249190
    iput-object p2, p0, LX/1Sk;->e:LX/0TD;

    .line 249191
    iput-object p3, p0, LX/1Sk;->f:LX/0tX;

    .line 249192
    iput-object p4, p0, LX/1Sk;->g:LX/0So;

    .line 249193
    iput-object v0, p0, LX/1Sk;->a:Ljava/lang/Integer;

    .line 249194
    iput-object v0, p0, LX/1Sk;->b:LX/79k;

    .line 249195
    return-void
.end method

.method public static a(LX/0QB;)LX/1Sk;
    .locals 7

    .prologue
    .line 249212
    sget-object v0, LX/1Sk;->h:LX/1Sk;

    if-nez v0, :cond_1

    .line 249213
    const-class v1, LX/1Sk;

    monitor-enter v1

    .line 249214
    :try_start_0
    sget-object v0, LX/1Sk;->h:LX/1Sk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 249215
    if-eqz v2, :cond_0

    .line 249216
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 249217
    new-instance p0, LX/1Sk;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v6

    check-cast v6, LX/0So;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1Sk;-><init>(LX/0W3;LX/0TD;LX/0tX;LX/0So;)V

    .line 249218
    move-object v0, p0

    .line 249219
    sput-object v0, LX/1Sk;->h:LX/1Sk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 249220
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 249221
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 249222
    :cond_1
    sget-object v0, LX/1Sk;->h:LX/1Sk;

    return-object v0

    .line 249223
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 249224
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/1Sk;ILX/79k;)V
    .locals 2

    .prologue
    .line 249207
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/1Sk;->a:Ljava/lang/Integer;

    .line 249208
    iput-object p2, p0, LX/1Sk;->b:LX/79k;

    .line 249209
    iget-object v0, p0, LX/1Sk;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/1Sk;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249210
    monitor-exit p0

    return-void

    .line 249211
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static d(LX/1Sk;)V
    .locals 5

    .prologue
    .line 249203
    iget-object v0, p0, LX/1Sk;->d:LX/0W3;

    sget-wide v2, LX/0X5;->hk:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    .line 249204
    iget-object v1, p0, LX/1Sk;->d:LX/0W3;

    sget-wide v2, LX/0X5;->hl:J

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/79k;->convertString(Ljava/lang/String;)LX/79k;

    move-result-object v1

    .line 249205
    invoke-static {p0, v0, v1}, LX/1Sk;->a$redex0(LX/1Sk;ILX/79k;)V

    .line 249206
    return-void
.end method

.method public static e(LX/1Sk;)Z
    .locals 4

    .prologue
    .line 249202
    iget-object v0, p0, LX/1Sk;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/1Sk;->c:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x1499700

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final c()V
    .locals 3

    .prologue
    .line 249196
    new-instance v0, LX/79b;

    invoke-direct {v0}, LX/79b;-><init>()V

    move-object v0, v0

    .line 249197
    const-string v1, "query_type"

    const-string v2, "ALL_SAVES"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 249198
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 249199
    iget-object v1, p0, LX/1Sk;->f:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 249200
    new-instance v1, LX/79j;

    invoke-direct {v1, p0}, LX/79j;-><init>(LX/1Sk;)V

    iget-object v2, p0, LX/1Sk;->e:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 249201
    return-void
.end method
