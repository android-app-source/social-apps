.class public LX/0YC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/regex/Pattern;

.field private static final c:Ljava/util/regex/Pattern;

.field private static volatile h:LX/0YC;


# instance fields
.field public final d:LX/0VP;

.field private final e:LX/0YE;

.field public final f:LX/0SG;

.field public final g:LX/0YI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80743
    const-class v0, LX/0YC;

    sput-object v0, LX/0YC;->a:Ljava/lang/Class;

    .line 80744
    const-string v0, "\\d+_.+\\.trace"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0YC;->b:Ljava/util/regex/Pattern;

    .line 80745
    const-string v0, "(\\d+)_(.+)\\.trace\\.gz"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0YC;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(LX/0VP;LX/0YE;LX/0SG;LX/0YI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 80737
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80738
    iput-object p1, p0, LX/0YC;->d:LX/0VP;

    .line 80739
    iput-object p2, p0, LX/0YC;->e:LX/0YE;

    .line 80740
    iput-object p3, p0, LX/0YC;->f:LX/0SG;

    .line 80741
    iput-object p4, p0, LX/0YC;->g:LX/0YI;

    .line 80742
    return-void
.end method

.method public static a(LX/0QB;)LX/0YC;
    .locals 7

    .prologue
    .line 80714
    sget-object v0, LX/0YC;->h:LX/0YC;

    if-nez v0, :cond_1

    .line 80715
    const-class v1, LX/0YC;

    monitor-enter v1

    .line 80716
    :try_start_0
    sget-object v0, LX/0YC;->h:LX/0YC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 80717
    if-eqz v2, :cond_0

    .line 80718
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 80719
    new-instance p0, LX/0YC;

    invoke-static {v0}, LX/0VP;->a(LX/0QB;)LX/0VP;

    move-result-object v3

    check-cast v3, LX/0VP;

    invoke-static {v0}, LX/0YE;->a(LX/0QB;)LX/0YE;

    move-result-object v4

    check-cast v4, LX/0YE;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v0}, LX/0YI;->a(LX/0QB;)LX/0YI;

    move-result-object v6

    check-cast v6, LX/0YI;

    invoke-direct {p0, v3, v4, v5, v6}, LX/0YC;-><init>(LX/0VP;LX/0YE;LX/0SG;LX/0YI;)V

    .line 80720
    move-object v0, p0

    .line 80721
    sput-object v0, LX/0YC;->h:LX/0YC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80722
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 80723
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 80724
    :cond_1
    sget-object v0, LX/0YC;->h:LX/0YC;

    return-object v0

    .line 80725
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 80726
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(Ljava/lang/String;)Ljava/util/regex/Matcher;
    .locals 2

    .prologue
    .line 80733
    sget-object v0, LX/0YC;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 80734
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-nez v1, :cond_0

    .line 80735
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Trace file format invariant <timestamp>_perfname.trace.gz failed to hold true"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80736
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()[Ljava/io/File;
    .locals 3

    .prologue
    .line 80728
    iget-object v0, p0, LX/0YC;->e:LX/0YE;

    invoke-virtual {v0}, LX/0YE;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 80729
    const/4 v0, 0x0

    .line 80730
    :goto_0
    return-object v0

    .line 80731
    :cond_0
    iget-object v0, p0, LX/0YC;->d:LX/0VP;

    iget-object v1, p0, LX/0YC;->d:LX/0VP;

    invoke-virtual {v1}, LX/0VP;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/0YC;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v1, v2}, LX/0VP;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)[Ljava/io/File;

    move-result-object v0

    .line 80732
    iget-object v1, p0, LX/0YC;->e:LX/0YE;

    invoke-virtual {v1}, LX/0YE;->b()V

    goto :goto_0
.end method

.method public final b()[Ljava/io/File;
    .locals 3

    .prologue
    .line 80727
    iget-object v0, p0, LX/0YC;->d:LX/0VP;

    iget-object v1, p0, LX/0YC;->d:LX/0VP;

    invoke-virtual {v1}, LX/0VP;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/0YC;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v1, v2}, LX/0VP;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method
