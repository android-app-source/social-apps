.class public LX/0mT;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/analytics2/logger/BeginWritingBlock;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0mU;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 132546
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0mU;
    .locals 3

    .prologue
    .line 132547
    sget-object v0, LX/0mT;->a:LX/0mU;

    if-nez v0, :cond_1

    .line 132548
    const-class v1, LX/0mT;

    monitor-enter v1

    .line 132549
    :try_start_0
    sget-object v0, LX/0mT;->a:LX/0mU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 132550
    if-eqz v2, :cond_0

    .line 132551
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 132552
    invoke-static {v0}, LX/0mU;->a(LX/0QB;)LX/0mU;

    move-result-object p0

    check-cast p0, LX/0mU;

    .line 132553
    move-object p0, p0

    .line 132554
    move-object v0, p0

    .line 132555
    sput-object v0, LX/0mT;->a:LX/0mU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 132556
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 132557
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 132558
    :cond_1
    sget-object v0, LX/0mT;->a:LX/0mU;

    return-object v0

    .line 132559
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 132560
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 132561
    invoke-static {p0}, LX/0mU;->a(LX/0QB;)LX/0mU;

    move-result-object v0

    check-cast v0, LX/0mU;

    .line 132562
    move-object v0, v0

    .line 132563
    return-object v0
.end method
