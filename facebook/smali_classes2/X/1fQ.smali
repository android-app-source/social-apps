.class public LX/1fQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/1fQ;


# instance fields
.field public final a:LX/0gX;

.field private final b:LX/14w;

.field public final c:LX/1BF;

.field public d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0gM",
            "<",
            "Lcom/facebook/api/feedcache/db/subscriptions/LiveVideoStatusSubscriptionsModels$LiveVideoStatusUpdateSubscriptionModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0gX;LX/14w;LX/1BF;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 291582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291583
    iput-object p1, p0, LX/1fQ;->a:LX/0gX;

    .line 291584
    iput-object p2, p0, LX/1fQ;->b:LX/14w;

    .line 291585
    iput-object p3, p0, LX/1fQ;->c:LX/1BF;

    .line 291586
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1fQ;->d:Ljava/util/Map;

    .line 291587
    return-void
.end method

.method public static a(LX/0QB;)LX/1fQ;
    .locals 6

    .prologue
    .line 291588
    sget-object v0, LX/1fQ;->e:LX/1fQ;

    if-nez v0, :cond_1

    .line 291589
    const-class v1, LX/1fQ;

    monitor-enter v1

    .line 291590
    :try_start_0
    sget-object v0, LX/1fQ;->e:LX/1fQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 291591
    if-eqz v2, :cond_0

    .line 291592
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 291593
    new-instance p0, LX/1fQ;

    invoke-static {v0}, LX/0gX;->a(LX/0QB;)LX/0gX;

    move-result-object v3

    check-cast v3, LX/0gX;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v4

    check-cast v4, LX/14w;

    invoke-static {v0}, LX/1BF;->b(LX/0QB;)LX/1BF;

    move-result-object v5

    check-cast v5, LX/1BF;

    invoke-direct {p0, v3, v4, v5}, LX/1fQ;-><init>(LX/0gX;LX/14w;LX/1BF;)V

    .line 291594
    move-object v0, p0

    .line 291595
    sput-object v0, LX/1fQ;->e:LX/1fQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291596
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 291597
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 291598
    :cond_1
    sget-object v0, LX/1fQ;->e:LX/1fQ;

    return-object v0

    .line 291599
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 291600
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
