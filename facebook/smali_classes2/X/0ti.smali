.class public LX/0ti;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0si;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0jV;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Sh;

.field private final d:LX/0tc;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field private final f:LX/0tA;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1kv;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Ot;Ljava/util/Set;LX/0Sh;LX/0tc;Ljava/util/concurrent/ExecutorService;LX/0tA;LX/0Ot;LX/0Uh;)V
    .locals 0
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0si;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/0jV;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0tc;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0tA;",
            "LX/0Ot",
            "<",
            "LX/1kv;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 155459
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155460
    iput-object p1, p0, LX/0ti;->a:LX/0Ot;

    .line 155461
    iput-object p2, p0, LX/0ti;->b:Ljava/util/Set;

    .line 155462
    iput-object p3, p0, LX/0ti;->c:LX/0Sh;

    .line 155463
    iput-object p4, p0, LX/0ti;->d:LX/0tc;

    .line 155464
    iput-object p5, p0, LX/0ti;->e:Ljava/util/concurrent/ExecutorService;

    .line 155465
    iput-object p6, p0, LX/0ti;->f:LX/0tA;

    .line 155466
    iput-object p7, p0, LX/0ti;->g:LX/0Ot;

    .line 155467
    iput-object p8, p0, LX/0ti;->h:LX/0Uh;

    .line 155468
    return-void
.end method

.method public static a(LX/0QB;)LX/0ti;
    .locals 1

    .prologue
    .line 155458
    invoke-static {p0}, LX/0ti;->b(LX/0QB;)LX/0ti;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/0ti;
    .locals 9

    .prologue
    .line 155424
    new-instance v0, LX/0ti;

    const/16 v1, 0xb0f

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0}, LX/0tj;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v2

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {p0}, LX/0tc;->a(LX/0QB;)LX/0tc;

    move-result-object v4

    check-cast v4, LX/0tc;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0tA;->a(LX/0QB;)LX/0tA;

    move-result-object v6

    check-cast v6, LX/0tA;

    const/16 v7, 0xaf6

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-direct/range {v0 .. v8}, LX/0ti;-><init>(LX/0Ot;Ljava/util/Set;LX/0Sh;LX/0tc;Ljava/util/concurrent/ExecutorService;LX/0tA;LX/0Ot;LX/0Uh;)V

    .line 155425
    return-object v0
.end method

.method public static c(LX/0ti;LX/3Bq;)V
    .locals 2

    .prologue
    .line 155450
    iget-object v0, p0, LX/0ti;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 155451
    iget-object v0, p0, LX/0ti;->d:LX/0tc;

    invoke-virtual {v0, p1}, LX/0tc;->a(LX/3Bq;)LX/37X;

    move-result-object v0

    .line 155452
    sget-object v1, LX/1NE;->NETWORK:LX/1NE;

    invoke-virtual {v0, v1}, LX/1NB;->a(LX/1NE;)LX/1NB;

    .line 155453
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/37X;->a(Z)V

    .line 155454
    :try_start_0
    invoke-virtual {p0, v0}, LX/0ti;->a(LX/37X;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155455
    invoke-virtual {v0}, LX/1NB;->e()V

    .line 155456
    return-void

    .line 155457
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, LX/1NB;->e()V

    throw v1
.end method

.method private d(LX/3Bq;)V
    .locals 2

    .prologue
    .line 155447
    iget-object v0, p0, LX/0ti;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jV;

    .line 155448
    invoke-interface {v0, p1}, LX/0jV;->a(LX/3Bq;)V

    goto :goto_0

    .line 155449
    :cond_0
    return-void
.end method

.method private e(LX/3Bq;)V
    .locals 1

    .prologue
    .line 155445
    iget-object v0, p0, LX/0ti;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0si;

    invoke-interface {v0, p1}, LX/0si;->a(LX/3Bq;)V

    .line 155446
    return-void
.end method


# virtual methods
.method public final a(LX/3Bq;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Bq;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 155469
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 155470
    iget-object v1, p0, LX/0ti;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/graphql/executor/GraphQLCacheAggregator$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/facebook/graphql/executor/GraphQLCacheAggregator$1;-><init>(LX/0ti;LX/3Bq;Lcom/google/common/util/concurrent/SettableFuture;)V

    const v3, -0x23605d2e

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 155471
    return-object v0
.end method

.method public final a(LX/4VT;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4VT",
            "<**>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155444
    new-instance v0, LX/4VL;

    iget-object v1, p0, LX/0ti;->h:LX/0Uh;

    const/4 v2, 0x1

    new-array v2, v2, [LX/4VT;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {v0, v1, v2}, LX/4VL;-><init>(LX/0Uh;[LX/4VT;)V

    invoke-virtual {p0, v0}, LX/0ti;->a(LX/3Bq;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0jT;)V
    .locals 1

    .prologue
    .line 155436
    iget-object v0, p0, LX/0ti;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 155437
    if-nez p1, :cond_0

    .line 155438
    :goto_0
    return-void

    .line 155439
    :cond_0
    instance-of v0, p1, LX/0jS;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 155440
    instance-of v0, p1, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 155441
    iget-object v0, p0, LX/0ti;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1kv;

    check-cast p1, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-virtual {v0, p1}, LX/1kv;->a(Lcom/facebook/flatbuffers/MutableFlattenable;)LX/2lk;

    move-result-object v0

    .line 155442
    invoke-virtual {p0, v0}, LX/0ti;->a(LX/2lk;)V

    goto :goto_0

    .line 155443
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(LX/2lk;)V
    .locals 3

    .prologue
    .line 155426
    iget-object v0, p0, LX/0ti;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 155427
    invoke-interface {p1}, LX/2lk;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155428
    :goto_0
    return-void

    .line 155429
    :cond_0
    iget-object v0, p0, LX/0ti;->f:LX/0tA;

    invoke-static {}, LX/0tA;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/2lk;->d()LX/3Bq;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0tA;->b(Ljava/lang/String;LX/3Bq;)V

    .line 155430
    invoke-interface {p1}, LX/2lk;->d()LX/3Bq;

    move-result-object v0

    .line 155431
    iget-object v1, p0, LX/0ti;->d:LX/0tc;

    invoke-virtual {v1, v0}, LX/0tc;->a(LX/3Bq;)LX/37X;

    move-result-object v0

    .line 155432
    invoke-virtual {v0}, LX/37X;->g()I

    .line 155433
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/37X;->a(Z)V

    .line 155434
    :try_start_0
    invoke-interface {p1}, LX/2lk;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155435
    invoke-virtual {v0}, LX/1NB;->e()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, LX/1NB;->e()V

    throw v1
.end method

.method public final a(LX/37X;)V
    .locals 1

    .prologue
    .line 155419
    invoke-virtual {p1}, LX/1NB;->c()V

    .line 155420
    invoke-virtual {p1}, LX/37X;->h()LX/3Bq;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0ti;->d(LX/3Bq;)V

    .line 155421
    invoke-virtual {p1}, LX/1NB;->d()V

    .line 155422
    invoke-virtual {p1}, LX/37X;->h()LX/3Bq;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0ti;->e(LX/3Bq;)V

    .line 155423
    return-void
.end method

.method public final b(LX/3Bq;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 155415
    iget-object v0, p0, LX/0ti;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 155416
    invoke-direct {p0, p1}, LX/0ti;->d(LX/3Bq;)V

    .line 155417
    invoke-direct {p0, p1}, LX/0ti;->e(LX/3Bq;)V

    .line 155418
    return-void
.end method
