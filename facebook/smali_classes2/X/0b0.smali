.class public final LX/0b0;
.super LX/0b1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b1",
        "<",
        "Lcom/facebook/photos/upload/event/MediaUploadEnqueuedEvent;",
        "LX/BaK;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0b3;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0b3;",
            "LX/0Ot",
            "<",
            "LX/BaK;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 86516
    invoke-direct {p0, p1, p2}, LX/0b1;-><init>(LX/0b4;LX/0Ot;)V

    .line 86517
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/upload/event/MediaUploadEnqueuedEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86518
    const-class v0, Lcom/facebook/photos/upload/event/MediaUploadEnqueuedEvent;

    return-object v0
.end method

.method public final a(LX/0b7;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 86519
    check-cast p1, Lcom/facebook/photos/upload/event/MediaUploadEnqueuedEvent;

    check-cast p2, LX/BaK;

    .line 86520
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 86521
    iget-object v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->am:Lcom/facebook/audience/model/UploadShot;

    move-object v0, v1

    .line 86522
    if-nez v0, :cond_1

    .line 86523
    :cond_0
    :goto_0
    return-void

    .line 86524
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/audience/model/UploadShot;->getAudience()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 86525
    iget-object v1, p2, LX/BaK;->a:LX/1EY;

    .line 86526
    iget-object v2, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v2, v2

    .line 86527
    iget-object p0, v2, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v2, p0

    .line 86528
    invoke-virtual {v1, v2}, LX/1EY;->a(Ljava/lang/String;)V

    .line 86529
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 86530
    invoke-virtual {v1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ac()Z

    move-result v1

    if-nez v1, :cond_0

    .line 86531
    iget-object v1, p2, LX/BaK;->b:LX/1EW;

    invoke-virtual {v0}, Lcom/facebook/audience/model/UploadShot;->getAudience()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 86532
    iget v2, v1, LX/1EW;->e:I

    add-int/2addr v2, v0

    iput v2, v1, LX/1EW;->e:I

    .line 86533
    iget-object v2, v1, LX/1EW;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0130

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, p0, p1

    invoke-virtual {v2, v3, v0, p0}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/1EW;->a(LX/1EW;Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v1, LX/1EW;->f:Z

    .line 86534
    goto :goto_0
.end method
