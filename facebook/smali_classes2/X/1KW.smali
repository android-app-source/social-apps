.class public LX/1KW;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hk;
.implements LX/0fs;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/1KX;

.field public final d:LX/0r7;

.field public final e:LX/1KY;

.field public final f:LX/0hI;

.field public g:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/Timer;

.field public i:LX/DBm;

.field public j:LX/0g8;

.field public k:LX/1UQ;

.field private l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 232723
    const-class v0, LX/1KW;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1KW;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0hI;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 232712
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 232713
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1KW;->l:Z

    .line 232714
    iput-object p1, p0, LX/1KW;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 232715
    iput-object p2, p0, LX/1KW;->f:LX/0hI;

    .line 232716
    sget-object v0, LX/1KX;->a:LX/1KX;

    move-object v0, v0

    .line 232717
    iput-object v0, p0, LX/1KW;->c:LX/1KX;

    .line 232718
    sget-object v0, LX/0r7;->a:LX/0r7;

    move-object v0, v0

    .line 232719
    iput-object v0, p0, LX/1KW;->d:LX/0r7;

    .line 232720
    sget-object v0, LX/1KY;->a:LX/1KY;

    move-object v0, v0

    .line 232721
    iput-object v0, p0, LX/1KW;->e:LX/1KY;

    .line 232722
    return-void
.end method

.method public static synthetic a(LX/1KW;IIIIILjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZZLjava/util/Set;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 0

    .prologue
    .line 232711
    invoke-static/range {p0 .. p17}, LX/1KW;->a$redex0(LX/1KW;IIIIILjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZZLjava/util/Set;Lcom/facebook/graphql/model/FeedUnit;)V

    return-void
.end method

.method private static a$redex0(LX/1KW;IIIIILjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZZLjava/util/Set;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIII",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;ZZZ",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ")V"
        }
    .end annotation

    .prologue
    .line 232705
    move-object/from16 v0, p0

    iget-object v0, v0, LX/1KW;->i:LX/DBm;

    move-object/from16 v20, v0

    new-instance v1, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$2;

    move-object/from16 v2, p0

    move/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move/from16 v15, p13

    move/from16 v16, p14

    move/from16 v17, p15

    move-object/from16 v18, p16

    move-object/from16 v19, p17

    invoke-direct/range {v1 .. v19}, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$2;-><init>(LX/1KW;IIIIILjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZZLjava/util/Set;Lcom/facebook/graphql/model/FeedUnit;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, LX/DBm;->post(Ljava/lang/Runnable;)Z

    .line 232706
    return-void
.end method

.method public static synthetic e(LX/1KW;)LX/1KX;
    .locals 1

    .prologue
    .line 232710
    iget-object v0, p0, LX/1KW;->c:LX/1KX;

    return-object v0
.end method

.method public static e(LX/1KW;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232724
    iget-object v0, p0, LX/1KW;->c:LX/1KX;

    .line 232725
    iget-object v1, v0, LX/1KX;->i:Ljava/util/List;

    invoke-static {v1}, LX/1c9;->a(Ljava/util/List;)LX/1c9;

    move-result-object v1

    move-object v0, v1

    .line 232726
    iget-object v1, p0, LX/1KW;->c:LX/1KX;

    .line 232727
    iget p0, v1, LX/1KX;->j:I

    move v1, p0

    .line 232728
    invoke-static {v0, v1}, LX/AjT;->a(Ljava/util/List;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1Qr;LX/0g8;LX/1Pf;)V
    .locals 0

    .prologue
    .line 232707
    iput-object p2, p0, LX/1KW;->j:LX/0g8;

    .line 232708
    check-cast p1, LX/1UQ;

    iput-object p1, p0, LX/1KW;->k:LX/1UQ;

    .line 232709
    return-void
.end method

.method public final c()V
    .locals 7

    .prologue
    .line 232683
    iget-object v0, p0, LX/1KW;->g:LX/1Iu;

    .line 232684
    iget-object v1, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 232685
    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const/4 v3, -0x2

    const/4 v4, 0x0

    .line 232686
    iget-object v1, p0, LX/1KW;->i:LX/DBm;

    if-nez v1, :cond_3

    const/4 v1, 0x0

    .line 232687
    iget-object v2, p0, LX/1KW;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/0eJ;->j:LX/0Tn;

    invoke-interface {v2, v5, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/1KW;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/0eJ;->k:LX/0Tn;

    invoke-interface {v2, v5, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/1KW;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/0eJ;->l:LX/0Tn;

    invoke-interface {v2, v5, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    move v1, v1

    .line 232688
    if-eqz v1, :cond_3

    .line 232689
    new-instance v1, LX/DBm;

    iget-object v2, p0, LX/1KW;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v1, v0, v2}, LX/DBm;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    iput-object v1, p0, LX/1KW;->i:LX/DBm;

    .line 232690
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, 0x5

    invoke-direct {v1, v3, v3, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 232691
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    .line 232692
    and-int/lit16 v2, v2, 0x500

    const/16 v3, 0x500

    if-ne v2, v3, :cond_2

    .line 232693
    iget-object v2, p0, LX/1KW;->f:LX/0hI;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0hI;->a(Landroid/view/Window;)I

    move-result v2

    invoke-virtual {v1, v4, v2, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 232694
    :cond_2
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    iget-object v3, p0, LX/1KW;->i:LX/DBm;

    invoke-virtual {v2, v3, v1}, Landroid/view/Window;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 232695
    :cond_3
    iget-boolean v0, p0, LX/1KW;->l:Z

    if-nez v0, :cond_4

    iget-object v0, p0, LX/1KW;->i:LX/DBm;

    if-eqz v0, :cond_4

    .line 232696
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1KW;->l:Z

    .line 232697
    const/4 v2, 0x1

    .line 232698
    iget-object v1, p0, LX/1KW;->c:LX/1KX;

    .line 232699
    iput-boolean v2, v1, LX/1KX;->e:Z

    .line 232700
    iget-object v1, p0, LX/1KW;->e:LX/1KY;

    .line 232701
    iput-boolean v2, v1, LX/1KY;->b:Z

    .line 232702
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, LX/1KW;->h:Ljava/util/Timer;

    .line 232703
    iget-object v1, p0, LX/1KW;->h:Ljava/util/Timer;

    new-instance v2, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;

    invoke-direct {v2, p0}, Lcom/facebook/feed/fragment/debug/FreshFeedDebugViewController$1;-><init>(LX/1KW;)V

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x3e8

    invoke-virtual/range {v1 .. v6}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 232704
    :cond_4
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 232674
    iget-boolean v0, p0, LX/1KW;->l:Z

    if-eqz v0, :cond_0

    .line 232675
    iput-boolean v1, p0, LX/1KW;->l:Z

    .line 232676
    iget-object v0, p0, LX/1KW;->h:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 232677
    const/4 v0, 0x0

    iput-object v0, p0, LX/1KW;->h:Ljava/util/Timer;

    .line 232678
    iget-object v0, p0, LX/1KW;->c:LX/1KX;

    .line 232679
    iput-boolean v1, v0, LX/1KX;->e:Z

    .line 232680
    iget-object v0, p0, LX/1KW;->e:LX/1KY;

    .line 232681
    iput-boolean v1, v0, LX/1KY;->b:Z

    .line 232682
    :cond_0
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 232671
    iput-object v0, p0, LX/1KW;->j:LX/0g8;

    .line 232672
    iput-object v0, p0, LX/1KW;->k:LX/1UQ;

    .line 232673
    return-void
.end method
