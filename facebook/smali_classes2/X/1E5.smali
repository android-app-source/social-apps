.class public LX/1E5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 219163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219164
    iput-object p1, p0, LX/1E5;->a:LX/0Uh;

    .line 219165
    return-void
.end method

.method public static b(LX/0QB;)LX/1E5;
    .locals 2

    .prologue
    .line 219166
    new-instance v1, LX/1E5;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-direct {v1, v0}, LX/1E5;-><init>(LX/0Uh;)V

    .line 219167
    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 219168
    iget-object v0, p0, LX/1E5;->a:LX/0Uh;

    const/16 v1, 0x34e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final b(Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 219169
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-gt v0, v1, :cond_0

    .line 219170
    const-string v0, "Unsupported api level"

    .line 219171
    :goto_0
    return-object v0

    .line 219172
    :cond_0
    if-eqz p1, :cond_1

    .line 219173
    invoke-virtual {p0}, LX/1E5;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 219174
    const-string v0, "Does not pass facecast page gk"

    goto :goto_0

    .line 219175
    :cond_1
    invoke-virtual {p0}, LX/1E5;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 219176
    const-string v0, "Does not pass facecast gk"

    goto :goto_0

    .line 219177
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 219178
    iget-object v0, p0, LX/1E5;->a:LX/0Uh;

    const/16 v1, 0x5aa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 219179
    invoke-virtual {p0}, LX/1E5;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1E5;->a:LX/0Uh;

    const/16 v2, 0x358

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
