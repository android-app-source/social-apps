.class public LX/1eQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field public d:I

.field public e:I

.field public f:I

.field private final g:LX/1FQ;


# direct methods
.method public constructor <init>(LX/1FQ;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 288199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 288200
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FQ;

    iput-object v0, p0, LX/1eQ;->g:LX/1FQ;

    .line 288201
    iput v1, p0, LX/1eQ;->c:I

    .line 288202
    iput v1, p0, LX/1eQ;->b:I

    .line 288203
    iput v1, p0, LX/1eQ;->d:I

    .line 288204
    iput v1, p0, LX/1eQ;->f:I

    .line 288205
    iput v1, p0, LX/1eQ;->e:I

    .line 288206
    iput v1, p0, LX/1eQ;->a:I

    .line 288207
    return-void
.end method

.method private static a(LX/1eQ;Ljava/io/InputStream;)Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/16 v9, 0xff

    const/4 v8, 0x6

    .line 288208
    iget v3, p0, LX/1eQ;->e:I

    .line 288209
    :goto_0
    :try_start_0
    iget v2, p0, LX/1eQ;->a:I

    if-eq v2, v8, :cond_1

    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_1

    .line 288210
    iget v4, p0, LX/1eQ;->c:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/1eQ;->c:I

    .line 288211
    iget v4, p0, LX/1eQ;->a:I

    packed-switch v4, :pswitch_data_0

    .line 288212
    const/4 v4, 0x0

    invoke-static {v4}, LX/03g;->b(Z)V

    .line 288213
    :cond_0
    :goto_1
    iput v2, p0, LX/1eQ;->b:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 288214
    :catch_0
    move-exception v2

    .line 288215
    invoke-static {v2}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    .line 288216
    :cond_1
    iget v2, p0, LX/1eQ;->a:I

    if-eq v2, v8, :cond_b

    iget v2, p0, LX/1eQ;->e:I

    if-eq v2, v3, :cond_b

    :goto_2
    return v0

    .line 288217
    :pswitch_0
    if-ne v2, v9, :cond_2

    .line 288218
    const/4 v4, 0x1

    :try_start_1
    iput v4, p0, LX/1eQ;->a:I

    goto :goto_1

    .line 288219
    :cond_2
    const/4 v4, 0x6

    iput v4, p0, LX/1eQ;->a:I

    goto :goto_1

    .line 288220
    :pswitch_1
    const/16 v4, 0xd8

    if-ne v2, v4, :cond_3

    .line 288221
    const/4 v4, 0x2

    iput v4, p0, LX/1eQ;->a:I

    goto :goto_1

    .line 288222
    :cond_3
    const/4 v4, 0x6

    iput v4, p0, LX/1eQ;->a:I

    goto :goto_1

    .line 288223
    :pswitch_2
    if-ne v2, v9, :cond_0

    .line 288224
    const/4 v4, 0x3

    iput v4, p0, LX/1eQ;->a:I

    goto :goto_1

    .line 288225
    :pswitch_3
    if-ne v2, v9, :cond_4

    .line 288226
    const/4 v4, 0x3

    iput v4, p0, LX/1eQ;->a:I

    goto :goto_1

    .line 288227
    :cond_4
    if-nez v2, :cond_5

    .line 288228
    const/4 v4, 0x2

    iput v4, p0, LX/1eQ;->a:I

    goto :goto_1

    .line 288229
    :cond_5
    const/16 v4, 0xda

    if-eq v2, v4, :cond_6

    const/16 v4, 0xd9

    if-ne v2, v4, :cond_8

    .line 288230
    :cond_6
    iget v4, p0, LX/1eQ;->c:I

    add-int/lit8 v4, v4, -0x2

    .line 288231
    iget v5, p0, LX/1eQ;->d:I

    if-lez v5, :cond_7

    .line 288232
    iput v4, p0, LX/1eQ;->f:I

    .line 288233
    :cond_7
    iget v5, p0, LX/1eQ;->d:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/1eQ;->d:I

    iput v5, p0, LX/1eQ;->e:I

    .line 288234
    :cond_8
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 288235
    if-ne v2, v5, :cond_c

    .line 288236
    :cond_9
    :goto_3
    move v4, v4

    .line 288237
    if-eqz v4, :cond_a

    .line 288238
    const/4 v4, 0x4

    iput v4, p0, LX/1eQ;->a:I

    goto :goto_1

    .line 288239
    :cond_a
    const/4 v4, 0x2

    iput v4, p0, LX/1eQ;->a:I

    goto :goto_1

    .line 288240
    :pswitch_4
    const/4 v4, 0x5

    iput v4, p0, LX/1eQ;->a:I

    goto :goto_1

    .line 288241
    :pswitch_5
    iget v4, p0, LX/1eQ;->b:I

    shl-int/lit8 v4, v4, 0x8

    add-int/2addr v4, v2

    .line 288242
    add-int/lit8 v4, v4, -0x2

    .line 288243
    int-to-long v6, v4

    invoke-static {p1, v6, v7}, LX/47v;->a(Ljava/io/InputStream;J)J

    .line 288244
    iget v5, p0, LX/1eQ;->c:I

    add-int/2addr v4, v5

    iput v4, p0, LX/1eQ;->c:I

    .line 288245
    const/4 v4, 0x2

    iput v4, p0, LX/1eQ;->a:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :cond_b
    move v0, v1

    .line 288246
    goto :goto_2

    .line 288247
    :cond_c
    const/16 v6, 0xd0

    if-lt v2, v6, :cond_d

    const/16 v6, 0xd7

    if-le v2, v6, :cond_9

    .line 288248
    :cond_d
    const/16 v6, 0xd9

    if-eq v2, v6, :cond_9

    const/16 v6, 0xd8

    if-eq v2, v6, :cond_9

    move v4, v5

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/1FL;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 288249
    iget v0, p0, LX/1eQ;->a:I

    const/4 v2, 0x6

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 288250
    :goto_0
    return v0

    .line 288251
    :cond_0
    invoke-virtual {p1}, LX/1FL;->i()I

    move-result v0

    .line 288252
    iget v2, p0, LX/1eQ;->c:I

    if-gt v0, v2, :cond_1

    move v0, v1

    .line 288253
    goto :goto_0

    .line 288254
    :cond_1
    new-instance v2, LX/4eP;

    invoke-virtual {p1}, LX/1FL;->b()Ljava/io/InputStream;

    move-result-object v3

    iget-object v0, p0, LX/1eQ;->g:LX/1FQ;

    const/16 v4, 0x4000

    invoke-interface {v0, v4}, LX/1FS;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iget-object v4, p0, LX/1eQ;->g:LX/1FQ;

    invoke-direct {v2, v3, v0, v4}, LX/4eP;-><init>(Ljava/io/InputStream;[BLX/1FN;)V

    .line 288255
    :try_start_0
    iget v0, p0, LX/1eQ;->c:I

    int-to-long v4, v0

    invoke-static {v2, v4, v5}, LX/47v;->a(Ljava/io/InputStream;J)J

    .line 288256
    invoke-static {p0, v2}, LX/1eQ;->a(LX/1eQ;Ljava/io/InputStream;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 288257
    invoke-static {v2}, LX/1vz;->a(Ljava/io/InputStream;)V

    goto :goto_0

    .line 288258
    :catch_0
    move-exception v0

    .line 288259
    :try_start_1
    invoke-static {v0}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 288260
    invoke-static {v2}, LX/1vz;->a(Ljava/io/InputStream;)V

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2}, LX/1vz;->a(Ljava/io/InputStream;)V

    throw v0
.end method
