.class public LX/0uf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ug;


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Character;",
            "LX/1jp;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LX/1jp;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0tw;

.field private d:LX/0ud;

.field private e:LX/0uc;

.field private final f:LX/0So;


# direct methods
.method public constructor <init>(LX/0tw;LX/0ud;LX/0uc;LX/0So;)V
    .locals 1

    .prologue
    .line 156812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156813
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0uf;->a:Ljava/util/Map;

    .line 156814
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0uf;->b:Ljava/util/Map;

    .line 156815
    iput-object p1, p0, LX/0uf;->c:LX/0tw;

    .line 156816
    iput-object p2, p0, LX/0uf;->d:LX/0ud;

    .line 156817
    iput-object p3, p0, LX/0uf;->e:LX/0uc;

    .line 156818
    iput-object p4, p0, LX/0uf;->f:LX/0So;

    .line 156819
    return-void
.end method

.method private a(LX/1jk;)LX/1jp;
    .locals 6
    .param p1    # LX/1jk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 156820
    if-nez p1, :cond_0

    .line 156821
    :try_start_0
    new-instance v0, LX/5MH;

    const-string v1, "Config not found"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch LX/5MH; {:try_start_0 .. :try_end_0} :catch_0

    .line 156822
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 156823
    const-string v2, "ContextualResolverImpl"

    const-string v3, "Error analyzing configuration: %s"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, LX/1jk;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 156824
    new-instance v0, LX/5MI;

    invoke-virtual {v1}, LX/5MH;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/0uf;->f:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-direct {v0, p1, v1, v2, v3}, LX/5MI;-><init>(LX/1jk;Ljava/lang/String;J)V

    .line 156825
    :goto_1
    return-object v0

    .line 156826
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0uf;->d:LX/0ud;

    invoke-virtual {v0, p1}, LX/0ud;->a(LX/1jk;)LX/1jp;

    move-result-object v0

    invoke-static {v0}, LX/0uf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1jp;
    :try_end_1
    .catch LX/5MH; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 156827
    :cond_1
    const-string v0, "CONFIG_NOT_FOUND!"

    goto :goto_0
.end method

.method public static a(LX/0uf;LX/1jp;LX/8K8;)LX/1jr;
    .locals 4

    .prologue
    .line 156758
    :try_start_0
    invoke-interface {p1, p2}, LX/1jp;->a(LX/8K8;)LX/1jr;

    move-result-object v0

    invoke-static {v0}, LX/0uf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1jr;

    .line 156759
    iget-object v1, p0, LX/0uf;->e:LX/0uc;

    invoke-interface {p1}, LX/1jp;->a()LX/1jk;

    move-result-object v2

    invoke-interface {p1}, LX/1jp;->b()I

    move-result v3

    invoke-interface {v1, v2, v0, v3}, LX/0uc;->a(LX/1jk;LX/1jr;I)V
    :try_end_0
    .catch LX/5MH; {:try_start_0 .. :try_end_0} :catch_0

    .line 156760
    :goto_0
    return-object v0

    .line 156761
    :catch_0
    move-exception v0

    .line 156762
    iget-object v1, p0, LX/0uf;->e:LX/0uc;

    invoke-interface {p1}, LX/1jp;->a()LX/1jk;

    move-result-object v2

    invoke-virtual {v0}, LX/5MH;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, LX/1jp;->b()I

    move-result v3

    invoke-interface {v1, v2, v0, v3}, LX/0uc;->a(LX/1jk;Ljava/lang/String;I)V

    .line 156763
    new-instance v0, LX/1jr;

    const/4 v1, 0x0

    iget-object v2, p0, LX/0uf;->e:LX/0uc;

    invoke-direct {v0, p1, v1, v2}, LX/1jr;-><init>(LX/1jp;[LX/0uE;LX/0uc;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 156809
    if-nez p0, :cond_0

    .line 156810
    new-instance v0, LX/5MH;

    const-string v1, "Illegal null value"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156811
    :cond_0
    return-object p0
.end method

.method public static b(LX/0uf;J)LX/1jp;
    .locals 12

    .prologue
    .line 156785
    iget-object v1, p0, LX/0uf;->b:Ljava/util/Map;

    monitor-enter v1

    .line 156786
    :try_start_0
    iget-object v0, p0, LX/0uf;->b:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1jp;

    .line 156787
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156788
    if-nez v0, :cond_3

    .line 156789
    iget-object v0, p0, LX/0uf;->c:LX/0tw;

    const/4 v5, 0x0

    .line 156790
    invoke-static {p1, p2}, LX/0X6;->a(J)I

    move-result v7

    .line 156791
    iget-object v6, v0, LX/0tw;->b:LX/0W3;

    invoke-virtual {v6}, LX/0W3;->a()LX/0W4;

    move-result-object v6

    invoke-interface {v6, p1, p2, v5}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 156792
    if-eqz v6, :cond_1

    .line 156793
    iget-object v5, v0, LX/0tw;->c:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v9

    .line 156794
    new-instance v5, LX/1jk;

    invoke-direct {v5, v6, v9, v10}, LX/1jk;-><init>(Ljava/lang/String;J)V

    move-object v6, v5

    .line 156795
    const-string v5, "conduit"

    const-string v8, "mobile_config"

    invoke-virtual {v6, v5, v8}, LX/1jk;->a(Ljava/lang/String;Ljava/lang/String;)LX/1jk;

    .line 156796
    invoke-static {}, LX/0X8;->a()Ljava/util/Map;

    move-result-object v5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 156797
    if-eqz v5, :cond_0

    .line 156798
    const-string v7, "conduit_info"

    invoke-virtual {v6, v7, v5}, LX/1jk;->a(Ljava/lang/String;Ljava/lang/String;)LX/1jk;

    :cond_0
    move-object v5, v6

    .line 156799
    :cond_1
    move-object v1, v5

    .line 156800
    invoke-direct {p0, v1}, LX/0uf;->a(LX/1jk;)LX/1jp;

    move-result-object v0

    .line 156801
    const/4 v2, 0x3

    invoke-static {v2}, LX/01m;->b(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 156802
    if-eqz v1, :cond_2

    invoke-virtual {v1}, LX/1jk;->a()Ljava/lang/String;

    .line 156803
    :cond_2
    iget-object v1, p0, LX/0uf;->b:Ljava/util/Map;

    monitor-enter v1

    .line 156804
    :try_start_1
    iget-object v2, p0, LX/0uf;->b:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156805
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 156806
    :cond_3
    return-object v0

    .line 156807
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 156808
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 156828
    const/4 v0, -0x1

    return v0
.end method

.method public final a(JJ)J
    .locals 3

    .prologue
    .line 156784
    invoke-static {p0, p1, p2}, LX/0uf;->b(LX/0uf;J)LX/1jp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/0uf;->a(LX/0uf;LX/1jp;LX/8K8;)LX/1jr;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p3, p4}, LX/1jr;->a(IJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(J)LX/1jr;
    .locals 3

    .prologue
    .line 156783
    invoke-static {p0, p1, p2}, LX/0uf;->b(LX/0uf;J)LX/1jp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/0uf;->a(LX/0uf;LX/1jp;LX/8K8;)LX/1jr;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 8

    .prologue
    .line 156773
    iget-object v2, p0, LX/0uf;->b:Ljava/util/Map;

    monitor-enter v2

    .line 156774
    :try_start_0
    invoke-static {}, LX/0X8;->a()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 156775
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 156776
    iget-object v0, p0, LX/0uf;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 156777
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, LX/0X6;->a(J)I

    move-result v1

    if-ne v1, p1, :cond_0

    .line 156778
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 156779
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 156780
    :cond_1
    :try_start_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 156781
    iget-object v3, p0, LX/0uf;->b:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 156782
    :cond_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(JZ)Z
    .locals 3

    .prologue
    .line 156772
    invoke-static {p0, p1, p2}, LX/0uf;->b(LX/0uf;J)LX/1jp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/0uf;->a(LX/0uf;LX/1jp;LX/8K8;)LX/1jr;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p3}, LX/1jr;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 156764
    iget-object v1, p0, LX/0uf;->b:Ljava/util/Map;

    monitor-enter v1

    .line 156765
    :try_start_0
    iget-object v0, p0, LX/0uf;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 156766
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156767
    iget-object v1, p0, LX/0uf;->a:Ljava/util/Map;

    monitor-enter v1

    .line 156768
    :try_start_1
    iget-object v0, p0, LX/0uf;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 156769
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 156770
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 156771
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
