.class public LX/0qV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 147690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147691
    return-void
.end method

.method public static a(LX/0QB;)LX/0qV;
    .locals 1

    .prologue
    .line 147692
    new-instance v0, LX/0qV;

    invoke-direct {v0}, LX/0qV;-><init>()V

    .line 147693
    move-object v0, v0

    .line 147694
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/api/feed/FeedFetchContext;)V
    .locals 4
    .param p1    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 147695
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    .line 147696
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1}, Lcom/facebook/graphql/model/OrganicImpression;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/OrganicImpression;

    move-result-object v0

    .line 147697
    if-eqz v0, :cond_0

    .line 147698
    invoke-virtual {v0}, Lcom/facebook/graphql/model/OrganicImpression;->l()V

    .line 147699
    :cond_0
    return-void

    .line 147700
    :cond_1
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v0, :cond_0

    .line 147701
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 147702
    invoke-static {p1}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 147703
    invoke-virtual {p0, v0, p2}, LX/0qV;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/api/feed/FeedFetchContext;)V

    .line 147704
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
