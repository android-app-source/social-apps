.class public LX/0Yj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field private c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:D

.field public g:J

.field public h:J

.field public i:Z

.field public j:Z

.field public k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcom/facebook/common/perftest/PerfTestConfig;

.field public n:Z

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:J

.field private r:J

.field public s:LX/00q;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:D

.field public u:LX/03R;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 82200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82201
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0Yj;->f:D

    .line 82202
    iput-wide v2, p0, LX/0Yj;->g:J

    .line 82203
    iput-wide v2, p0, LX/0Yj;->h:J

    .line 82204
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Yj;->i:Z

    .line 82205
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yj;->n:Z

    .line 82206
    iput-wide v2, p0, LX/0Yj;->q:J

    .line 82207
    iput-wide v2, p0, LX/0Yj;->r:J

    .line 82208
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/0Yj;->u:LX/03R;

    .line 82209
    iput-object p2, p0, LX/0Yj;->d:Ljava/lang/String;

    .line 82210
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0Yj;->c:Ljava/lang/String;

    .line 82211
    iput p1, p0, LX/0Yj;->a:I

    .line 82212
    return-void
.end method

.method public constructor <init>(LX/0Yj;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 82171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82172
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0Yj;->f:D

    .line 82173
    iput-wide v2, p0, LX/0Yj;->g:J

    .line 82174
    iput-wide v2, p0, LX/0Yj;->h:J

    .line 82175
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Yj;->i:Z

    .line 82176
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yj;->n:Z

    .line 82177
    iput-wide v2, p0, LX/0Yj;->q:J

    .line 82178
    iput-wide v2, p0, LX/0Yj;->r:J

    .line 82179
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/0Yj;->u:LX/03R;

    .line 82180
    iget v0, p1, LX/0Yj;->a:I

    iput v0, p0, LX/0Yj;->a:I

    .line 82181
    iget v0, p1, LX/0Yj;->b:I

    iput v0, p0, LX/0Yj;->b:I

    .line 82182
    iget-object v0, p1, LX/0Yj;->c:Ljava/lang/String;

    iput-object v0, p0, LX/0Yj;->c:Ljava/lang/String;

    .line 82183
    iget-object v0, p1, LX/0Yj;->d:Ljava/lang/String;

    iput-object v0, p0, LX/0Yj;->d:Ljava/lang/String;

    .line 82184
    iget-object v0, p1, LX/0Yj;->e:Ljava/lang/String;

    iput-object v0, p0, LX/0Yj;->e:Ljava/lang/String;

    .line 82185
    iget-wide v0, p1, LX/0Yj;->f:D

    iput-wide v0, p0, LX/0Yj;->f:D

    .line 82186
    iget-wide v0, p1, LX/0Yj;->g:J

    iput-wide v0, p0, LX/0Yj;->g:J

    .line 82187
    iget-wide v0, p1, LX/0Yj;->h:J

    iput-wide v0, p0, LX/0Yj;->h:J

    .line 82188
    iget-boolean v0, p1, LX/0Yj;->i:Z

    iput-boolean v0, p0, LX/0Yj;->i:Z

    .line 82189
    iget-boolean v0, p1, LX/0Yj;->j:Z

    iput-boolean v0, p0, LX/0Yj;->j:Z

    .line 82190
    iget-object v0, p1, LX/0Yj;->k:Ljava/util/Set;

    iput-object v0, p0, LX/0Yj;->k:Ljava/util/Set;

    .line 82191
    iget-boolean v0, p1, LX/0Yj;->n:Z

    iput-boolean v0, p0, LX/0Yj;->n:Z

    .line 82192
    iget-object v0, p1, LX/0Yj;->l:Ljava/util/Map;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p1, LX/0Yj;->l:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    :goto_0
    iput-object v0, p0, LX/0Yj;->l:Ljava/util/Map;

    .line 82193
    iget-object v0, p1, LX/0Yj;->o:Ljava/lang/String;

    iput-object v0, p0, LX/0Yj;->o:Ljava/lang/String;

    .line 82194
    iget-object v0, p1, LX/0Yj;->p:Ljava/lang/String;

    iput-object v0, p0, LX/0Yj;->p:Ljava/lang/String;

    .line 82195
    iget-object v0, p1, LX/0Yj;->s:LX/00q;

    iput-object v0, p0, LX/0Yj;->s:LX/00q;

    .line 82196
    iget-wide v0, p1, LX/0Yj;->t:D

    iput-wide v0, p0, LX/0Yj;->t:D

    .line 82197
    iget-object v0, p1, LX/0Yj;->u:LX/03R;

    iput-object v0, p0, LX/0Yj;->u:LX/03R;

    .line 82198
    return-void

    .line 82199
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/03R;)LX/0Yj;
    .locals 2

    .prologue
    .line 82167
    iget-object v0, p0, LX/0Yj;->u:LX/03R;

    sget-object v1, LX/03R;->YES:LX/03R;

    if-eq v0, v1, :cond_0

    .line 82168
    if-eqz p1, :cond_1

    :goto_0
    iput-object p1, p0, LX/0Yj;->u:LX/03R;

    .line 82169
    :cond_0
    return-object p0

    .line 82170
    :cond_1
    sget-object p1, LX/03R;->UNSET:LX/03R;

    goto :goto_0
.end method


# virtual methods
.method public final a(D)LX/0Yj;
    .locals 3

    .prologue
    .line 82161
    iput-wide p1, p0, LX/0Yj;->t:D

    .line 82162
    iget-object v0, p0, LX/0Yj;->m:Lcom/facebook/common/perftest/PerfTestConfig;

    if-nez v0, :cond_0

    .line 82163
    new-instance v0, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-direct {v0}, Lcom/facebook/common/perftest/PerfTestConfig;-><init>()V

    iput-object v0, p0, LX/0Yj;->m:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 82164
    :cond_0
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    cmpg-double v0, v0, p1

    if-gez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/0Yj;->i:Z

    .line 82165
    return-object p0

    .line 82166
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Boolean;)LX/0Yj;
    .locals 1

    .prologue
    .line 82160
    invoke-static {p1}, LX/03R;->valueOf(Ljava/lang/Boolean;)LX/03R;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0Yj;->a(LX/03R;)LX/0Yj;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/0Yj;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 82213
    iput-object p1, p0, LX/0Yj;->o:Ljava/lang/String;

    .line 82214
    iput-object p2, p0, LX/0Yj;->p:Ljava/lang/String;

    .line 82215
    return-object p0
.end method

.method public final a(Ljava/util/Map;)LX/0Yj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/0Yj;"
        }
    .end annotation

    .prologue
    .line 82152
    if-nez p1, :cond_0

    .line 82153
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Yj;->l:Ljava/util/Map;

    .line 82154
    :goto_0
    return-object p0

    .line 82155
    :cond_0
    instance-of v0, p1, LX/0P1;

    if-eqz v0, :cond_1

    .line 82156
    check-cast p1, LX/0P1;

    .line 82157
    iput-object p1, p0, LX/0Yj;->l:Ljava/util/Map;

    .line 82158
    goto :goto_0

    .line 82159
    :cond_1
    invoke-static {p1}, LX/0PM;->a(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0Yj;->l:Ljava/util/Map;

    goto :goto_0
.end method

.method public final varargs a([Ljava/lang/String;)LX/0Yj;
    .locals 4

    .prologue
    .line 82147
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/0Yj;->k:Ljava/util/Set;

    .line 82148
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 82149
    iget-object v3, p0, LX/0Yj;->k:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 82150
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 82151
    :cond_0
    return-object p0
.end method

.method public final b()LX/0Yj;
    .locals 1

    .prologue
    .line 82145
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Yj;->j:Z

    .line 82146
    return-object p0
.end method

.method public final c(Z)LX/0Yj;
    .locals 1

    .prologue
    .line 82144
    invoke-static {p1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0Yj;->a(LX/03R;)LX/0Yj;

    move-result-object v0

    return-object v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 82143
    iget-wide v0, p0, LX/0Yj;->g:J

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 82142
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, LX/0Yj;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "name"

    iget-object v2, p0, LX/0Yj;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "namespace"

    iget-object v2, p0, LX/0Yj;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
