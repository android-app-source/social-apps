.class public LX/0sU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0sU;


# instance fields
.field private final a:LX/0sV;


# direct methods
.method public constructor <init>(LX/0sV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 152035
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152036
    iput-object p1, p0, LX/0sU;->a:LX/0sV;

    .line 152037
    return-void
.end method

.method public static a(LX/0QB;)LX/0sU;
    .locals 4

    .prologue
    .line 152038
    sget-object v0, LX/0sU;->b:LX/0sU;

    if-nez v0, :cond_1

    .line 152039
    const-class v1, LX/0sU;

    monitor-enter v1

    .line 152040
    :try_start_0
    sget-object v0, LX/0sU;->b:LX/0sU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 152041
    if-eqz v2, :cond_0

    .line 152042
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 152043
    new-instance p0, LX/0sU;

    invoke-static {v0}, LX/0sV;->a(LX/0QB;)LX/0sV;

    move-result-object v3

    check-cast v3, LX/0sV;

    invoke-direct {p0, v3}, LX/0sU;-><init>(LX/0sV;)V

    .line 152044
    move-object v0, p0

    .line 152045
    sput-object v0, LX/0sU;->b:LX/0sU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152046
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 152047
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 152048
    :cond_1
    sget-object v0, LX/0sU;->b:LX/0sU;

    return-object v0

    .line 152049
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 152050
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0gW;)V
    .locals 1

    .prologue
    .line 152051
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, p1, v0}, LX/0sU;->a(LX/0gW;Ljava/lang/String;)V

    .line 152052
    return-void
.end method

.method public final a(LX/0gW;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/VideoChannelEntryPoint;
        .end annotation
    .end param

    .prologue
    .line 152053
    iget-object v0, p0, LX/0sU;->a:LX/0sV;

    iget-boolean v0, v0, LX/0sV;->j:Z

    .line 152054
    const-string v1, "in_channel_eligibility_experiment"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 152055
    if-eqz v0, :cond_0

    .line 152056
    const-string v0, "video_channel_entry_point"

    invoke-virtual {p1, v0, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 152057
    :cond_0
    return-void
.end method
