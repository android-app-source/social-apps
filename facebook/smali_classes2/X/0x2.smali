.class public LX/0x2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field public A:Lcom/facebook/graphql/model/SponsoredImpression;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:LX/162;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/SponsoredImpression;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/162;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:I

.field public p:Z

.field public q:Z

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/OrganicImpression;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Z

.field public u:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Z

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:I

.field public y:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 161858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161859
    return-void
.end method


# virtual methods
.method public final clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 161860
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x2;

    .line 161861
    const/4 v1, 0x0

    iput-object v1, v0, LX/0x2;->b:Lcom/facebook/graphql/model/SponsoredImpression;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 161862
    return-object v0

    .line 161863
    :catch_0
    move-exception v0

    .line 161864
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Not able to clone PropertyBag"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
