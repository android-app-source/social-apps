.class public LX/1hl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/util/logging/Logger;

.field private static volatile r:LX/1hl;


# instance fields
.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1MQ;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/1Go;

.field private final f:LX/1hm;

.field private final g:LX/0yQ;

.field private final h:LX/1hn;

.field public final i:LX/18b;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lorg/apache/http/client/HttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/1ho;

.field private final l:LX/1hz;

.field private final m:LX/0dx;

.field private final n:LX/1iB;

.field private final o:LX/1iC;

.field private final p:LX/1iD;

.field private final q:LX/0So;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 297046
    const-class v0, LX/1hl;

    .line 297047
    sput-object v0, LX/1hl;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1hl;->b:Ljava/lang/String;

    .line 297048
    sget-object v0, LX/1hl;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, LX/1hl;->c:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/1Go;LX/1hm;LX/0yQ;LX/1hn;LX/18b;LX/0Ot;LX/1ho;LX/1hz;LX/0dx;LX/1iB;LX/1iC;LX/1iD;LX/0So;)V
    .locals 0
    .param p1    # LX/0Ot;
        .annotation runtime Lcom/facebook/http/executors/qebased/QeBasedHttpExecutor;
        .end annotation
    .end param
    .param p7    # LX/0Ot;
        .annotation runtime Lcom/facebook/http/annotations/FallbackHttpClient;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1MQ;",
            ">;",
            "LX/1Go;",
            "LX/1hm;",
            "LX/0yQ;",
            "LX/1hn;",
            "LX/18b;",
            "LX/0Ot",
            "<",
            "Lorg/apache/http/client/HttpClient;",
            ">;",
            "LX/1ho;",
            "LX/1hz;",
            "LX/0dx;",
            "LX/1iB;",
            "LX/1iC;",
            "LX/1iD;",
            "LX/0So;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 296898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296899
    iput-object p1, p0, LX/1hl;->d:LX/0Ot;

    .line 296900
    iput-object p2, p0, LX/1hl;->e:LX/1Go;

    .line 296901
    iput-object p3, p0, LX/1hl;->f:LX/1hm;

    .line 296902
    iput-object p5, p0, LX/1hl;->h:LX/1hn;

    .line 296903
    iput-object p6, p0, LX/1hl;->i:LX/18b;

    .line 296904
    iput-object p7, p0, LX/1hl;->j:LX/0Ot;

    .line 296905
    iput-object p8, p0, LX/1hl;->k:LX/1ho;

    .line 296906
    iput-object p9, p0, LX/1hl;->l:LX/1hz;

    .line 296907
    iput-object p10, p0, LX/1hl;->m:LX/0dx;

    .line 296908
    iput-object p11, p0, LX/1hl;->n:LX/1iB;

    .line 296909
    iput-object p4, p0, LX/1hl;->g:LX/0yQ;

    .line 296910
    iput-object p12, p0, LX/1hl;->o:LX/1iC;

    .line 296911
    iput-object p13, p0, LX/1hl;->p:LX/1iD;

    .line 296912
    iput-object p14, p0, LX/1hl;->q:LX/0So;

    .line 296913
    return-void
.end method

.method public static a(LX/0QB;)LX/1hl;
    .locals 3

    .prologue
    .line 297036
    sget-object v0, LX/1hl;->r:LX/1hl;

    if-nez v0, :cond_1

    .line 297037
    const-class v1, LX/1hl;

    monitor-enter v1

    .line 297038
    :try_start_0
    sget-object v0, LX/1hl;->r:LX/1hl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 297039
    if-eqz v2, :cond_0

    .line 297040
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1hl;->b(LX/0QB;)LX/1hl;

    move-result-object v0

    sput-object v0, LX/1hl;->r:LX/1hl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297041
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 297042
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 297043
    :cond_1
    sget-object v0, LX/1hl;->r:LX/1hl;

    return-object v0

    .line 297044
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 297045
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/1hl;Ljava/io/IOException;)Ljava/io/IOException;
    .locals 1

    .prologue
    .line 297033
    invoke-static {p1}, LX/3d7;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297034
    iget-object v0, p0, LX/1hl;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1MQ;

    invoke-interface {v0}, LX/1MQ;->d()V

    .line 297035
    :cond_0
    throw p1
.end method

.method public static a(LX/1hl;LX/15D;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/15D",
            "<TT;>;",
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;",
            "Lorg/apache/http/HttpResponse;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 297016
    iget-object v0, p0, LX/1hl;->o:LX/1iC;

    .line 297017
    new-instance v2, LX/4bH;

    const-class v1, LX/4bG;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/4bG;

    invoke-direct {v2, v1, p3}, LX/4bH;-><init>(LX/4bG;Lorg/apache/http/HttpResponse;)V

    .line 297018
    move-object v1, v2

    .line 297019
    iget-object v0, p0, LX/1hl;->q:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 297020
    :try_start_0
    invoke-interface {p2, v1}, Lorg/apache/http/client/ResponseHandler;->handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v6

    .line 297021
    const/4 v0, 0x0

    invoke-static {p3, v0}, LX/1hl;->a(Lorg/apache/http/HttpResponse;Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 297022
    iget-object v0, p0, LX/1hl;->q:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 297023
    iget-object v0, p0, LX/1hl;->p:LX/1iD;

    invoke-virtual {v1}, LX/4bH;->a()J

    move-result-wide v4

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/1iD;->a(LX/15D;JJ)V

    .line 297024
    return-object v6

    .line 297025
    :catch_0
    move-exception v0

    .line 297026
    :try_start_1
    invoke-static {p3, v0}, LX/1hl;->a(Lorg/apache/http/HttpResponse;Ljava/lang/Throwable;)V

    .line 297027
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 297028
    const-class v4, Ljava/io/IOException;

    invoke-static {v0, v4}, LX/1Bz;->propagateIfInstanceOf(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 297029
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297030
    :catchall_0
    move-exception v0

    move-object v6, v0

    iget-object v0, p0, LX/1hl;->q:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 297031
    iget-object v0, p0, LX/1hl;->p:LX/1iD;

    invoke-virtual {v1}, LX/4bH;->a()J

    move-result-wide v4

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/1iD;->a(LX/15D;JJ)V

    .line 297032
    throw v6
.end method

.method private static a(LX/1hl;LX/15D;Ljava/lang/Throwable;)Lorg/apache/http/HttpResponse;
    .locals 5

    .prologue
    .line 297005
    iget-object v0, p1, LX/15D;->f:LX/14Q;

    move-object v0, v0

    .line 297006
    sget-object v1, LX/14Q;->FALLBACK_REQUIRED:LX/14Q;

    if-ne v0, v1, :cond_1

    .line 297007
    sget-object v1, LX/1hl;->a:Ljava/lang/Class;

    const-string v2, "Got %s while executing %s, retrying on a safe network stack"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v3, v4

    const/4 v0, 0x1

    .line 297008
    iget-object v4, p1, LX/15D;->c:Ljava/lang/String;

    move-object v4, v4

    .line 297009
    aput-object v4, v3, v0

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 297010
    iget-object v0, p0, LX/1hl;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    .line 297011
    iget-object v1, p1, LX/15D;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    move-object v1, v1

    .line 297012
    invoke-interface {v0, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0

    .line 297013
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 297014
    :cond_1
    const-class v0, Ljava/io/IOException;

    invoke-static {p2, v0}, LX/1Bz;->propagateIfInstanceOf(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 297015
    invoke-static {p2}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public static a(LX/1hl;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 297001
    iget-object v0, p0, LX/1hl;->m:LX/0dx;

    invoke-interface {v0}, LX/0dy;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Tor integrity checker"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297002
    :cond_0
    :goto_0
    return-void

    .line 297003
    :cond_1
    iget-object v0, p0, LX/1hl;->m:LX/0dx;

    invoke-interface {v0}, LX/0dy;->b()Z

    move-result v0

    if-eq p2, v0, :cond_0

    .line 297004
    iget-object v0, p0, LX/1hl;->m:LX/0dx;

    invoke-interface {v0}, LX/0dy;->d()V

    goto :goto_0
.end method

.method private static a(LX/1hl;Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;ZJLX/14P;)V
    .locals 2

    .prologue
    .line 296980
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 296981
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const-string v1, "fb_user_request_identifier"

    invoke-interface {v0, v1, p2}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 296982
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 296983
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const-string v1, "fb_user_request_is_sampled"

    invoke-interface {v0, v1, p3}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    .line 296984
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 296985
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const-string v1, "fb_request_creation_time"

    invoke-interface {v0, v1, p4, p5}, Lorg/apache/http/params/HttpParams;->setLongParameter(Ljava/lang/String;J)Lorg/apache/http/params/HttpParams;

    .line 296986
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 296987
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v1

    const-string p2, "replay_safe"

    sget-object v0, LX/14P;->RETRY_SAFE:LX/14P;

    if-ne p6, v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, p2, v0}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    .line 296988
    iget-object v0, p0, LX/1hl;->e:LX/1Go;

    invoke-interface {v0, p1}, LX/1Go;->a(Lorg/apache/http/HttpRequest;)V

    .line 296989
    iget-object v0, p0, LX/1hl;->f:LX/1hm;

    .line 296990
    iget-object v1, v0, LX/1hm;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/1hm;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/1hm;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 296991
    :cond_0
    const-string v1, "X-ZERO-CATEGORY"

    const-string p2, "dialtone"

    invoke-interface {p1, v1, p2}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 296992
    :cond_1
    const-string v0, "x-fb-net-hni"

    iget-object v1, p0, LX/1hl;->i:LX/18b;

    invoke-virtual {v1}, LX/18b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 296993
    const-string v0, "x-fb-sim-hni"

    iget-object v1, p0, LX/1hl;->i:LX/18b;

    invoke-virtual {v1}, LX/18b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 296994
    const-string v0, "x-fb-net-sid"

    iget-object v1, p0, LX/1hl;->i:LX/18b;

    invoke-virtual {v1}, LX/18b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 296995
    iget-object v0, p0, LX/1hl;->h:LX/1hn;

    .line 296996
    iget-object v1, v0, LX/1hn;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "X-FB-Background-State"

    invoke-interface {p1, v1}, Lorg/apache/http/HttpRequest;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v1

    array-length v1, v1

    if-nez v1, :cond_2

    .line 296997
    const-string v1, "X-FB-Background-State"

    const-string p2, "1"

    invoke-interface {p1, v1, p2}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 296998
    :cond_2
    iget-object v0, p0, LX/1hl;->g:LX/0yQ;

    invoke-virtual {v0, p1}, LX/0yQ;->a(Lorg/apache/http/HttpRequest;)V

    .line 296999
    return-void

    .line 297000
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private static a(Lorg/apache/http/HttpResponse;Ljava/lang/Throwable;)V
    .locals 4
    .param p1    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 296972
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 296973
    if-eqz v0, :cond_0

    .line 296974
    :try_start_0
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 296975
    :cond_0
    :goto_0
    return-void

    .line 296976
    :catch_0
    move-exception v0

    .line 296977
    if-eqz p1, :cond_1

    .line 296978
    sget-object v1, LX/1hl;->c:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Error consuming content after an exception."

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 296979
    :cond_1
    sget-object v1, LX/1hl;->c:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Error consuming content after response handler executed"

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static a(Lorg/apache/http/protocol/HttpContext;LX/15D;)V
    .locals 9

    .prologue
    .line 296963
    iget-object v0, p1, LX/15D;->e:Ljava/lang/String;

    move-object v0, v0

    .line 296964
    iget-object v1, p1, LX/15D;->c:Ljava/lang/String;

    move-object v2, v1

    .line 296965
    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 296966
    :goto_0
    new-instance v1, LX/1iV;

    invoke-virtual {p1}, LX/15D;->h()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/http/interfaces/RequestPriority;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LX/15D;->q()J

    move-result-wide v4

    .line 296967
    iget v0, p1, LX/15D;->m:I

    move v6, v0

    .line 296968
    iget-object v0, p1, LX/15D;->d:Lcom/facebook/common/callercontext/CallerContext;

    move-object v7, v0

    .line 296969
    invoke-direct/range {v1 .. v8}, LX/1iV;-><init>(Ljava/lang/String;Ljava/lang/String;JILcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;)V

    invoke-virtual {v1, p0}, LX/1iV;->b(Lorg/apache/http/protocol/HttpContext;)V

    .line 296970
    return-void

    .line 296971
    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/1hl;
    .locals 15

    .prologue
    .line 296961
    new-instance v0, LX/1hl;

    const/16 v1, 0x2500

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0}, LX/1Gn;->a(LX/0QB;)LX/1Gn;

    move-result-object v2

    check-cast v2, LX/1Go;

    invoke-static {p0}, LX/1hm;->a(LX/0QB;)LX/1hm;

    move-result-object v3

    check-cast v3, LX/1hm;

    invoke-static {p0}, LX/0yQ;->a(LX/0QB;)LX/0yQ;

    move-result-object v4

    check-cast v4, LX/0yQ;

    invoke-static {p0}, LX/1hn;->a(LX/0QB;)LX/1hn;

    move-result-object v5

    check-cast v5, LX/1hn;

    invoke-static {p0}, LX/18b;->a(LX/0QB;)LX/18b;

    move-result-object v6

    check-cast v6, LX/18b;

    const/16 v7, 0x1655

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/1ho;->a(LX/0QB;)LX/1ho;

    move-result-object v8

    check-cast v8, LX/1ho;

    invoke-static {p0}, LX/1hz;->a(LX/0QB;)LX/1hz;

    move-result-object v9

    check-cast v9, LX/1hz;

    invoke-static {p0}, LX/0dt;->b(LX/0QB;)LX/0dx;

    move-result-object v10

    check-cast v10, LX/0dx;

    invoke-static {p0}, LX/1iB;->a(LX/0QB;)LX/1iB;

    move-result-object v11

    check-cast v11, LX/1iB;

    const-class v12, LX/1iC;

    invoke-interface {p0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/1iC;

    invoke-static {p0}, LX/1iD;->a(LX/0QB;)LX/1iD;

    move-result-object v13

    check-cast v13, LX/1iD;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v14

    check-cast v14, LX/0So;

    invoke-direct/range {v0 .. v14}, LX/1hl;-><init>(LX/0Ot;LX/1Go;LX/1hm;LX/0yQ;LX/1hn;LX/18b;LX/0Ot;LX/1ho;LX/1hz;LX/0dx;LX/1iB;LX/1iC;LX/1iD;LX/0So;)V

    .line 296962
    return-object v0
.end method

.method public static b(LX/1hl;LX/15D;Ljava/lang/String;Z)Lorg/apache/http/HttpResponse;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/15D",
            "<TT;>;",
            "Ljava/lang/String;",
            "Z)",
            "Lorg/apache/http/HttpResponse;"
        }
    .end annotation

    .prologue
    .line 296955
    :try_start_0
    invoke-static {p0, p1, p2, p3}, LX/1hl;->c(LX/1hl;LX/15D;Ljava/lang/String;Z)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 296956
    :catch_0
    move-exception v0

    .line 296957
    sget-object v1, Lcom/facebook/http/interfaces/RequestStage;->FAILED:Lcom/facebook/http/interfaces/RequestStage;

    invoke-virtual {p1, v1}, LX/15D;->a(Lcom/facebook/http/interfaces/RequestStage;)V

    .line 296958
    iget-object v1, p1, LX/15D;->c:Ljava/lang/String;

    move-object v1, v1

    .line 296959
    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, LX/1hl;->a(LX/1hl;Ljava/lang/String;Z)V

    .line 296960
    invoke-static {p0, v0}, LX/1hl;->a(LX/1hl;Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method private static c(LX/1hl;LX/15D;Ljava/lang/String;Z)Lorg/apache/http/HttpResponse;
    .locals 10

    .prologue
    .line 296935
    :try_start_0
    iget-object v0, p1, LX/15D;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    move-object v1, v0

    .line 296936
    iget-wide v8, p1, LX/15D;->n:J

    move-wide v4, v8

    .line 296937
    iget-object v0, p1, LX/15D;->j:LX/14P;

    move-object v6, v0

    .line 296938
    move-object v0, p0

    move-object v2, p2

    move v3, p3

    invoke-static/range {v0 .. v6}, LX/1hl;->a(LX/1hl;Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;ZJLX/14P;)V

    .line 296939
    new-instance v5, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v5}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 296940
    invoke-static {v5, p1}, LX/1hl;->a(Lorg/apache/http/protocol/HttpContext;LX/15D;)V

    .line 296941
    iget-object v0, p0, LX/1hl;->k:LX/1ho;

    invoke-virtual {v0, p1}, LX/1ho;->a(LX/15D;)Lorg/apache/http/impl/client/RequestWrapper;

    move-result-object v1

    .line 296942
    const-string v0, "request_method"

    .line 296943
    iget-object v2, p1, LX/15D;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    move-object v2, v2

    .line 296944
    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->getMethod()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v0, v2}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296945
    iget-object v0, p0, LX/1hl;->l:LX/1hz;

    .line 296946
    iget-object v2, p1, LX/15D;->i:LX/15F;

    move-object v2, v2

    .line 296947
    iget-object v3, p0, LX/1hl;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1MQ;

    .line 296948
    iget-object v4, p1, LX/15D;->h:Lorg/apache/http/client/RedirectHandler;

    move-object v4, v4

    .line 296949
    iget-object v6, p1, LX/15D;->k:LX/0am;

    move-object v6, v6

    .line 296950
    iget-object v7, p1, LX/15D;->p:LX/2BD;

    move-object v7, v7

    .line 296951
    invoke-virtual/range {v0 .. v7}, LX/1hz;->a(Lorg/apache/http/client/methods/HttpUriRequest;LX/15F;LX/1MQ;Lorg/apache/http/client/RedirectHandler;Lorg/apache/http/protocol/HttpContext;LX/0am;LX/2BD;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 296952
    :goto_0
    return-object v0

    .line 296953
    :catch_0
    move-exception v0

    .line 296954
    invoke-static {p0, p1, v0}, LX/1hl;->a(LX/1hl;LX/15D;Ljava/lang/Throwable;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/15D;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/15D",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 296921
    iget-object v0, p1, LX/15D;->c:Ljava/lang/String;

    move-object v0, v0

    .line 296922
    const-string v1, "%s[%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, LX/1hl;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const v0, -0x207ba939

    invoke-static {v1, v2, v0}, LX/02m;->a(Ljava/lang/String;[Ljava/lang/Object;I)V

    .line 296923
    :try_start_0
    sget-object v0, Lcom/facebook/http/interfaces/RequestStage;->WAITING_RESPONSE:Lcom/facebook/http/interfaces/RequestStage;

    invoke-virtual {p1, v0}, LX/15D;->a(Lcom/facebook/http/interfaces/RequestStage;)V

    .line 296924
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 296925
    iget-object v1, p0, LX/1hl;->n:LX/1iB;

    invoke-virtual {v1}, LX/1iB;->a()Z

    move-result v1

    .line 296926
    iget-object v2, p1, LX/15D;->g:Lorg/apache/http/client/ResponseHandler;

    move-object v2, v2

    .line 296927
    invoke-static {p0, p1, v0, v1}, LX/1hl;->b(LX/1hl;LX/15D;Ljava/lang/String;Z)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 296928
    sget-object v4, Lcom/facebook/http/interfaces/RequestStage;->DOWNLOADING_RESPONSE:Lcom/facebook/http/interfaces/RequestStage;

    invoke-virtual {p1, v4}, LX/15D;->a(Lcom/facebook/http/interfaces/RequestStage;)V

    .line 296929
    invoke-static {p0, p1, v2, v3}, LX/1hl;->a(LX/1hl;LX/15D;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v2

    .line 296930
    sget-object v3, Lcom/facebook/http/interfaces/RequestStage;->FINISHED:Lcom/facebook/http/interfaces/RequestStage;

    invoke-virtual {p1, v3}, LX/15D;->a(Lcom/facebook/http/interfaces/RequestStage;)V

    .line 296931
    iget-object v3, p1, LX/15D;->c:Ljava/lang/String;

    move-object v3, v3

    .line 296932
    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, LX/1hl;->a(LX/1hl;Ljava/lang/String;Z)V

    .line 296933
    move-object v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296934
    const v1, -0x35e41b0c    # -2554173.0f

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x4014c822

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 296920
    iget-object v0, p0, LX/1hl;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1MQ;

    invoke-interface {v0}, LX/1MQ;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15D;)Lorg/apache/http/impl/client/RequestWrapper;
    .locals 9

    .prologue
    .line 296914
    iget-object v0, p1, LX/15D;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    move-object v1, v0

    .line 296915
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, LX/1hl;->n:LX/1iB;

    invoke-virtual {v0}, LX/1iB;->a()Z

    move-result v3

    .line 296916
    iget-wide v7, p1, LX/15D;->n:J

    move-wide v4, v7

    .line 296917
    iget-object v0, p1, LX/15D;->j:LX/14P;

    move-object v6, v0

    .line 296918
    move-object v0, p0

    invoke-static/range {v0 .. v6}, LX/1hl;->a(LX/1hl;Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;ZJLX/14P;)V

    .line 296919
    iget-object v0, p0, LX/1hl;->k:LX/1ho;

    invoke-virtual {v0, p1}, LX/1ho;->a(LX/15D;)Lorg/apache/http/impl/client/RequestWrapper;

    move-result-object v0

    return-object v0
.end method
