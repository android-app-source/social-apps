.class public final synthetic LX/1mF;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 313458
    invoke-static {}, LX/0y1;->values()[LX/0y1;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/1mF;->b:[I

    :try_start_0
    sget-object v0, LX/1mF;->b:[I

    sget-object v1, LX/0y1;->CHARGING_AC:LX/0y1;

    invoke-virtual {v1}, LX/0y1;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_0
    :try_start_1
    sget-object v0, LX/1mF;->b:[I

    sget-object v1, LX/0y1;->CHARGING_USB:LX/0y1;

    invoke-virtual {v1}, LX/0y1;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_1
    :try_start_2
    sget-object v0, LX/1mF;->b:[I

    sget-object v1, LX/0y1;->CHARGING_WIRELESS:LX/0y1;

    invoke-virtual {v1}, LX/0y1;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_2
    :try_start_3
    sget-object v0, LX/1mF;->b:[I

    sget-object v1, LX/0y1;->FULL:LX/0y1;

    invoke-virtual {v1}, LX/0y1;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    .line 313459
    :goto_3
    invoke-static {}, LX/1mE;->values()[LX/1mE;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/1mF;->a:[I

    :try_start_4
    sget-object v0, LX/1mF;->a:[I

    sget-object v1, LX/1mE;->YES:LX/1mE;

    invoke-virtual {v1}, LX/1mE;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_4
    :try_start_5
    sget-object v0, LX/1mF;->a:[I

    sget-object v1, LX/1mE;->NO:LX/1mE;

    invoke-virtual {v1}, LX/1mE;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_5
    :try_start_6
    sget-object v0, LX/1mF;->a:[I

    sget-object v1, LX/1mE;->UNKNOWN:LX/1mE;

    invoke-virtual {v1}, LX/1mE;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_6
    return-void

    :catch_0
    goto :goto_6

    :catch_1
    goto :goto_5

    :catch_2
    goto :goto_4

    :catch_3
    goto :goto_3

    :catch_4
    goto :goto_2

    :catch_5
    goto :goto_1

    :catch_6
    goto :goto_0
.end method
