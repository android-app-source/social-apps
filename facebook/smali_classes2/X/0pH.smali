.class public LX/0pH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0SI;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0WJ;

.field private final c:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private final d:Ljava/lang/String;

.field private e:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 144155
    const-class v0, LX/0pH;

    sput-object v0, LX/0pH;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0WJ;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 1

    .prologue
    .line 144142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144143
    new-instance v0, LX/0pI;

    invoke-direct {v0, p0}, LX/0pI;-><init>(LX/0pH;)V

    iput-object v0, p0, LX/0pH;->e:Ljava/lang/ThreadLocal;

    .line 144144
    iput-object p1, p0, LX/0pH;->b:LX/0WJ;

    .line 144145
    if-eqz p2, :cond_0

    :goto_0
    iput-object p2, p0, LX/0pH;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 144146
    invoke-virtual {p1}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 144147
    invoke-virtual {p1}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 144148
    iget-object p1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, p1

    .line 144149
    iput-object v0, p0, LX/0pH;->d:Ljava/lang/String;

    .line 144150
    :goto_1
    return-void

    .line 144151
    :cond_0
    sget-object p2, LX/0SH;->a:Lcom/facebook/auth/viewercontext/ViewerContext;

    goto :goto_0

    .line 144152
    :cond_1
    iget-object v0, p0, LX/0pH;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 144153
    iget-object p1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, p1

    .line 144154
    iput-object v0, p0, LX/0pH;->d:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public final a()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 1

    .prologue
    .line 144141
    iget-object v0, p0, LX/0pH;->b:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 1

    .prologue
    .line 144140
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;
    .locals 1

    .prologue
    .line 144156
    if-nez p1, :cond_0

    .line 144157
    sget-object v0, LX/1mW;->a:LX/1mW;

    .line 144158
    :goto_0
    return-object v0

    .line 144159
    :cond_0
    iget-object v0, p0, LX/0pH;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144160
    new-instance v0, LX/42c;

    invoke-direct {v0, p0, p1}, LX/42c;-><init>(LX/0pH;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    goto :goto_0
.end method

.method public final b()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 144135
    iget-object v0, p0, LX/0pH;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 144136
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 144137
    iget-object v1, p0, LX/0pH;->d:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144138
    const/4 v0, 0x0

    .line 144139
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0pH;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    goto :goto_0
.end method

.method public final c()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 1

    .prologue
    .line 144134
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 3

    .prologue
    .line 144118
    iget-object v0, p0, LX/0pH;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 144119
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 144120
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 144121
    :cond_0
    :goto_0
    return-object v0

    .line 144122
    :cond_1
    iget-object v0, p0, LX/0pH;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 144123
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 144124
    iget-object v1, p0, LX/0pH;->d:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 144125
    iget-object v0, p0, LX/0pH;->b:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 144126
    if-nez v0, :cond_2

    .line 144127
    iget-object v0, p0, LX/0pH;->b:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 144128
    iget-object v0, p0, LX/0pH;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    goto :goto_0

    .line 144129
    :cond_2
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v1

    .line 144130
    iget-object v2, p0, LX/0pH;->d:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 144131
    iget-object v0, p0, LX/0pH;->b:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 144132
    iget-object v0, p0, LX/0pH;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    goto :goto_0

    .line 144133
    :cond_3
    iget-object v0, p0, LX/0pH;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    goto :goto_0
.end method

.method public final e()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 1

    .prologue
    .line 144117
    invoke-virtual {p0}, LX/0pH;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    return-object v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 144112
    iget-object v0, p0, LX/0pH;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 144113
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 144114
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 144115
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 144116
    return-void
.end method
