.class public final enum LX/1J8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1J8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1J8;

.field public static final enum DOWN_ENOUGH:LX/1J8;

.field public static final enum FEED_LOAD_FAILURE:LX/1J8;

.field public static final enum FEED_LOAD_START:LX/1J8;

.field public static final enum FEED_LOAD_SUCCESS:LX/1J8;

.field public static final enum HEAD_FETCH_TRIGGERED:LX/1J8;

.field public static final enum TIMER_STARTED:LX/1J8;

.field public static final enum TOP_OF_FEED:LX/1J8;

.field public static final enum USER_LEFT_FEED:LX/1J8;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 229804
    new-instance v0, LX/1J8;

    const-string v1, "TOP_OF_FEED"

    invoke-direct {v0, v1, v3}, LX/1J8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1J8;->TOP_OF_FEED:LX/1J8;

    .line 229805
    new-instance v0, LX/1J8;

    const-string v1, "DOWN_ENOUGH"

    invoke-direct {v0, v1, v4}, LX/1J8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1J8;->DOWN_ENOUGH:LX/1J8;

    .line 229806
    new-instance v0, LX/1J8;

    const-string v1, "TIMER_STARTED"

    invoke-direct {v0, v1, v5}, LX/1J8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1J8;->TIMER_STARTED:LX/1J8;

    .line 229807
    new-instance v0, LX/1J8;

    const-string v1, "HEAD_FETCH_TRIGGERED"

    invoke-direct {v0, v1, v6}, LX/1J8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1J8;->HEAD_FETCH_TRIGGERED:LX/1J8;

    .line 229808
    new-instance v0, LX/1J8;

    const-string v1, "FEED_LOAD_START"

    invoke-direct {v0, v1, v7}, LX/1J8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1J8;->FEED_LOAD_START:LX/1J8;

    .line 229809
    new-instance v0, LX/1J8;

    const-string v1, "FEED_LOAD_SUCCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/1J8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1J8;->FEED_LOAD_SUCCESS:LX/1J8;

    .line 229810
    new-instance v0, LX/1J8;

    const-string v1, "FEED_LOAD_FAILURE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/1J8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1J8;->FEED_LOAD_FAILURE:LX/1J8;

    .line 229811
    new-instance v0, LX/1J8;

    const-string v1, "USER_LEFT_FEED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/1J8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1J8;->USER_LEFT_FEED:LX/1J8;

    .line 229812
    const/16 v0, 0x8

    new-array v0, v0, [LX/1J8;

    sget-object v1, LX/1J8;->TOP_OF_FEED:LX/1J8;

    aput-object v1, v0, v3

    sget-object v1, LX/1J8;->DOWN_ENOUGH:LX/1J8;

    aput-object v1, v0, v4

    sget-object v1, LX/1J8;->TIMER_STARTED:LX/1J8;

    aput-object v1, v0, v5

    sget-object v1, LX/1J8;->HEAD_FETCH_TRIGGERED:LX/1J8;

    aput-object v1, v0, v6

    sget-object v1, LX/1J8;->FEED_LOAD_START:LX/1J8;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/1J8;->FEED_LOAD_SUCCESS:LX/1J8;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1J8;->FEED_LOAD_FAILURE:LX/1J8;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/1J8;->USER_LEFT_FEED:LX/1J8;

    aput-object v2, v0, v1

    sput-object v0, LX/1J8;->$VALUES:[LX/1J8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 229814
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1J8;
    .locals 1

    .prologue
    .line 229815
    const-class v0, LX/1J8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1J8;

    return-object v0
.end method

.method public static values()[LX/1J8;
    .locals 1

    .prologue
    .line 229813
    sget-object v0, LX/1J8;->$VALUES:[LX/1J8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1J8;

    return-object v0
.end method
