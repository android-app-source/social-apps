.class public final enum LX/1Zs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1Zs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1Zs;

.field public static final enum FUNNEL_CANCELLED:LX/1Zs;

.field public static final enum FUNNEL_ENDED:LX/1Zs;

.field public static final enum FUNNEL_STARTED:LX/1Zs;


# instance fields
.field public beaconIdGenerator:LX/1Zu;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 275567
    new-instance v0, LX/1Zs;

    const-string v1, "FUNNEL_STARTED"

    const-string v2, "funnel_started"

    invoke-direct {v0, v1, v3, v2}, LX/1Zs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Zs;->FUNNEL_STARTED:LX/1Zs;

    .line 275568
    new-instance v0, LX/1Zs;

    const-string v1, "FUNNEL_ENDED"

    const-string v2, "funnel_ended"

    invoke-direct {v0, v1, v4, v2}, LX/1Zs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Zs;->FUNNEL_ENDED:LX/1Zs;

    .line 275569
    new-instance v0, LX/1Zs;

    const-string v1, "FUNNEL_CANCELLED"

    const-string v2, "funnel_cancelled"

    invoke-direct {v0, v1, v5, v2}, LX/1Zs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Zs;->FUNNEL_CANCELLED:LX/1Zs;

    .line 275570
    const/4 v0, 0x3

    new-array v0, v0, [LX/1Zs;

    sget-object v1, LX/1Zs;->FUNNEL_STARTED:LX/1Zs;

    aput-object v1, v0, v3

    sget-object v1, LX/1Zs;->FUNNEL_ENDED:LX/1Zs;

    aput-object v1, v0, v4

    sget-object v1, LX/1Zs;->FUNNEL_CANCELLED:LX/1Zs;

    aput-object v1, v0, v5

    sput-object v0, LX/1Zs;->$VALUES:[LX/1Zs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 275563
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 275564
    iput-object p3, p0, LX/1Zs;->name:Ljava/lang/String;

    .line 275565
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Zs;->beaconIdGenerator:LX/1Zu;

    .line 275566
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1Zs;
    .locals 1

    .prologue
    .line 275561
    const-class v0, LX/1Zs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1Zs;

    return-object v0
.end method

.method public static values()[LX/1Zs;
    .locals 1

    .prologue
    .line 275562
    sget-object v0, LX/1Zs;->$VALUES:[LX/1Zs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1Zs;

    return-object v0
.end method
