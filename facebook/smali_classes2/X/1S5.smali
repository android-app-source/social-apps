.class public LX/1S5;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final e:Ljava/lang/String;


# instance fields
.field public a:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 247599
    const-class v0, LX/1S5;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1S5;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 247600
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247601
    return-void
.end method

.method public static a$redex0(LX/1S5;LX/2tX;LX/2ta;)V
    .locals 4
    .param p1    # LX/2tX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 247602
    if-eqz p2, :cond_0

    iget-object v0, p2, LX/2ta;->a:LX/3Aj;

    if-nez v0, :cond_1

    .line 247603
    :cond_0
    :goto_0
    return-void

    .line 247604
    :cond_1
    new-instance v0, LX/34f;

    invoke-direct {v0}, LX/34f;-><init>()V

    move-object v0, v0

    .line 247605
    new-instance v1, LX/34g;

    invoke-direct {v1}, LX/34g;-><init>()V

    .line 247606
    iget-object v2, p2, LX/2ta;->a:LX/3Aj;

    .line 247607
    const-string v3, "viewer_coordinates"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 247608
    iget-object v2, p2, LX/2ta;->b:LX/3Ak;

    if-eqz v2, :cond_2

    .line 247609
    iget-object v2, p2, LX/2ta;->b:LX/3Ak;

    .line 247610
    const-string v3, "wifi_data"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 247611
    :cond_2
    sget-object v2, LX/34h;->a:[I

    .line 247612
    iget-object v3, p1, LX/2tX;->d:LX/328;

    move-object v3, v3

    .line 247613
    invoke-virtual {v3}, LX/328;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 247614
    :goto_1
    iget-object v2, p1, LX/2tX;->c:Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;

    move-object v2, v2

    .line 247615
    invoke-virtual {v2}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;->a()Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 247616
    const-string v3, "question_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 247617
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 247618
    iget-object v1, p0, LX/1S5;->d:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 247619
    :pswitch_0
    const-string v2, "agree"

    invoke-virtual {v1, v2}, LX/34g;->b(Ljava/lang/String;)LX/34g;

    goto :goto_1

    .line 247620
    :pswitch_1
    const-string v2, "disagree"

    invoke-virtual {v1, v2}, LX/34g;->b(Ljava/lang/String;)LX/34g;

    goto :goto_1

    .line 247621
    :pswitch_2
    const-string v2, "dismiss"

    invoke-virtual {v1, v2}, LX/34g;->b(Ljava/lang/String;)LX/34g;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/1S5;
    .locals 5

    .prologue
    .line 247622
    new-instance v4, LX/1S5;

    invoke-direct {v4}, LX/1S5;-><init>()V

    .line 247623
    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;->b(LX/0QB;)Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    .line 247624
    iput-object v0, v4, LX/1S5;->a:LX/0SG;

    iput-object v1, v4, LX/1S5;->b:Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;

    iput-object v2, v4, LX/1S5;->c:LX/03V;

    iput-object v3, v4, LX/1S5;->d:LX/0tX;

    .line 247625
    return-object v4
.end method


# virtual methods
.method public final b(LX/2tX;)V
    .locals 6

    .prologue
    .line 247626
    new-instance v0, LX/34i;

    invoke-direct {v0}, LX/34i;-><init>()V

    move-object v0, v0

    .line 247627
    new-instance v1, LX/34j;

    invoke-direct {v1}, LX/34j;-><init>()V

    .line 247628
    iget-object v2, p1, LX/2tX;->c:Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;

    move-object v2, v2

    .line 247629
    invoke-virtual {v2}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;->a()Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 247630
    const-string v3, "question_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 247631
    const-string v2, "QUESTION_RENDERED"

    .line 247632
    const-string v3, "event_name"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 247633
    iget-object v2, p0, LX/1S5;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 247634
    const-string v3, "event_time"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 247635
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 247636
    iget-object v1, p0, LX/1S5;->d:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 247637
    return-void
.end method
