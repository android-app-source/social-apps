.class public LX/0oG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

.field private final b:LX/0aE;

.field public final c:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ZILX/0aE;)V
    .locals 1

    .prologue
    .line 140088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140089
    new-instance v0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    invoke-direct {v0, p1, p2}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 140090
    iput-object p4, p0, LX/0oG;->b:LX/0aE;

    .line 140091
    iput p3, p0, LX/0oG;->c:I

    .line 140092
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0oG;
    .locals 1

    .prologue
    .line 140084
    iget-object v0, p0, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 140085
    invoke-static {v0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 140086
    iput-object p1, v0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->d:Ljava/lang/String;

    .line 140087
    return-object p0
.end method

.method public final a(Ljava/lang/String;D)LX/0oG;
    .locals 3

    .prologue
    .line 140078
    iget-object v0, p0, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 140079
    invoke-static {v0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 140080
    invoke-static {v0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->u(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 140081
    iget-object v1, v0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->j:LX/0n9;

    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    .line 140082
    invoke-static {v1, p1, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 140083
    return-object p0
.end method

.method public final a(Ljava/lang/String;I)LX/0oG;
    .locals 1

    .prologue
    .line 140076
    iget-object v0, p0, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->a(Ljava/lang/String;I)Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 140077
    return-object p0
.end method

.method public final a(Ljava/lang/String;J)LX/0oG;
    .locals 3

    .prologue
    .line 140070
    iget-object v0, p0, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 140071
    invoke-static {v0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 140072
    invoke-static {v0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->u(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 140073
    iget-object v1, v0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->j:LX/0n9;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 140074
    invoke-static {v1, p1, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 140075
    return-object p0
.end method

.method public final a(Ljava/lang/String;LX/0lF;)LX/0oG;
    .locals 1
    .param p2    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 140068
    iget-object v0, p0, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 140069
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 140066
    iget-object v0, p0, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 140067
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 140064
    iget-object v0, p0, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 140065
    return-object p0
.end method

.method public final a(Ljava/lang/String;Z)LX/0oG;
    .locals 1

    .prologue
    .line 140093
    iget-object v0, p0, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 140094
    return-object p0
.end method

.method public final a(Ljava/util/Map;)LX/0oG;
    .locals 5
    .param p1    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)",
            "LX/0oG;"
        }
    .end annotation

    .prologue
    .line 140039
    iget-object v0, p0, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 140040
    invoke-static {v0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 140041
    if-nez p1, :cond_1

    .line 140042
    :cond_0
    return-object p0

    .line 140043
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 140044
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    .line 140045
    instance-of v4, v2, LX/0lF;

    if-eqz v4, :cond_2

    .line 140046
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    check-cast v2, LX/0lF;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    goto :goto_0

    .line 140047
    :cond_2
    instance-of v4, v2, Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 140048
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    goto :goto_0

    .line 140049
    :cond_3
    instance-of v4, v2, Ljava/lang/Number;

    if-eqz v4, :cond_4

    .line 140050
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    check-cast v2, Ljava/lang/Number;

    .line 140051
    invoke-static {v0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 140052
    invoke-static {v0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->u(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 140053
    iget-object v4, v0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->j:LX/0n9;

    .line 140054
    invoke-static {v4, v1, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 140055
    goto :goto_0

    .line 140056
    :cond_4
    instance-of v4, v2, Ljava/lang/Boolean;

    if-eqz v4, :cond_5

    .line 140057
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    check-cast v2, Ljava/lang/Boolean;

    .line 140058
    invoke-static {v0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 140059
    invoke-static {v0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->u(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 140060
    iget-object v4, v0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->j:LX/0n9;

    .line 140061
    invoke-static {v4, v1, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 140062
    goto :goto_0

    .line 140063
    :cond_5
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 140038
    iget-object v0, p0, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    invoke-virtual {v0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->j()Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)LX/0oG;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 140034
    iget-object v0, p0, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 140035
    invoke-static {v0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 140036
    iput-object p1, v0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->e:Ljava/lang/String;

    .line 140037
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/0oG;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 140030
    iget-object v0, p0, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 140031
    invoke-static {v0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 140032
    iput-object p1, v0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->f:Ljava/lang/String;

    .line 140033
    return-object p0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140015
    iget-object v0, p0, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 140016
    iget-object p0, v0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v0, p0

    .line 140017
    return-object v0
.end method

.method public final d(Ljava/lang/String;)LX/0oG;
    .locals 2

    .prologue
    .line 140025
    iget-object v0, p0, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 140026
    invoke-static {v0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 140027
    const-string v1, "fbobj"

    iput-object v1, v0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->e:Ljava/lang/String;

    .line 140028
    iput-object p1, v0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->f:Ljava/lang/String;

    .line 140029
    return-object p0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 140023
    iget-object v0, p0, LX/0oG;->b:LX/0aE;

    iget-object v1, p0, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    invoke-virtual {v0, v1}, LX/0aE;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 140024
    return-void
.end method

.method public final e(Ljava/lang/String;)LX/0oG;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 140019
    iget-object v0, p0, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 140020
    invoke-static {v0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 140021
    iput-object p1, v0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->g:Ljava/lang/String;

    .line 140022
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140018
    iget-object v0, p0, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    invoke-virtual {v0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
