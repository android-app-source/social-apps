.class public LX/1P0;
.super LX/1P1;
.source ""

# interfaces
.implements LX/1OS;


# instance fields
.field private a:LX/1P4;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;

.field public g:Z

.field private h:Ljava/lang/reflect/Field;

.field private i:Ljava/lang/reflect/Field;

.field private t:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 242871
    invoke-direct {p0, p1}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 242872
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1P0;->g:Z

    .line 242873
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 1

    .prologue
    .line 242874
    invoke-direct {p0, p2, p3}, LX/1P1;-><init>(IZ)V

    .line 242875
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1P0;->g:Z

    .line 242876
    return-void
.end method

.method public static c(LX/1P0;)LX/1P4;
    .locals 1

    .prologue
    .line 242877
    iget-object v0, p0, LX/1P0;->a:LX/1P4;

    if-nez v0, :cond_0

    .line 242878
    new-instance v0, LX/1P4;

    invoke-direct {v0, p0}, LX/1P4;-><init>(LX/1P1;)V

    iput-object v0, p0, LX/1P0;->a:LX/1P4;

    .line 242879
    :cond_0
    iget-object v0, p0, LX/1P0;->a:LX/1P4;

    return-object v0
.end method

.method private d(Z)V
    .locals 2

    .prologue
    .line 242880
    iget-object v0, p0, LX/1P0;->h:Ljava/lang/reflect/Field;

    if-nez v0, :cond_0

    .line 242881
    :try_start_0
    const-class v0, LX/1P1;

    const-string v1, "b"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    iput-object v0, p0, LX/1P0;->h:Ljava/lang/reflect/Field;

    .line 242882
    iget-object v0, p0, LX/1P0;->h:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 242883
    const-class v0, LX/1P1;

    const-string v1, "d"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    iput-object v0, p0, LX/1P0;->i:Ljava/lang/reflect/Field;

    .line 242884
    iget-object v0, p0, LX/1P0;->i:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242885
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1P0;->i:Ljava/lang/reflect/Field;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 242886
    iget-object v0, p0, LX/1P0;->h:Ljava/lang/reflect/Field;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    .line 242887
    return-void

    .line 242888
    :catch_0
    move-exception v0

    .line 242889
    invoke-static {v0}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 242890
    :catch_1
    move-exception v0

    .line 242891
    invoke-static {v0}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public final I()I
    .locals 1

    .prologue
    .line 242892
    iget-boolean v0, p0, LX/1P0;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1P0;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 242893
    :cond_0
    invoke-static {p0}, LX/1P0;->c(LX/1P0;)LX/1P4;

    move-result-object v0

    invoke-virtual {v0}, LX/1P4;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/1P0;->b:Ljava/lang/Integer;

    .line 242894
    :cond_1
    iget-object v0, p0, LX/1P0;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final J()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 242853
    iput-object v0, p0, LX/1P0;->f:Ljava/lang/Integer;

    .line 242854
    iput-object v0, p0, LX/1P0;->e:Ljava/lang/Integer;

    .line 242855
    iput-object v0, p0, LX/1P0;->d:Ljava/lang/Integer;

    .line 242856
    iput-object v0, p0, LX/1P0;->c:Ljava/lang/Integer;

    .line 242857
    iput-object v0, p0, LX/1P0;->b:Ljava/lang/Integer;

    .line 242858
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;LX/1Ok;I)V
    .locals 1

    .prologue
    .line 242895
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 242896
    :goto_0
    return-void

    .line 242897
    :cond_0
    invoke-virtual {p0}, LX/1P0;->J()V

    .line 242898
    invoke-super {p0, p1, p2, p3}, LX/1P1;->a(Landroid/support/v7/widget/RecyclerView;LX/1Ok;I)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;II)V
    .locals 2

    .prologue
    .line 242899
    const-string v0, "BetterLinearLayoutManager.measureChildWithMargins"

    const v1, 0xe538b6d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 242900
    :try_start_0
    invoke-super {p0, p1, p2, p3}, LX/1P1;->a(Landroid/view/View;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242901
    const v0, 0x72ea7b69

    invoke-static {v0}, LX/02m;->a(I)V

    .line 242902
    return-void

    .line 242903
    :catchall_0
    move-exception v0

    const v1, 0x55079418

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Landroid/view/View;IIII)V
    .locals 2

    .prologue
    .line 242904
    const-string v0, "BetterLinearLayoutManager.layoutDecorated"

    const v1, -0x2ddda124

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 242905
    :try_start_0
    invoke-super/range {p0 .. p5}, LX/1P1;->a(Landroid/view/View;IIII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242906
    const v0, -0x49406d86

    invoke-static {v0}, LX/02m;->a(I)V

    .line 242907
    return-void

    .line 242908
    :catchall_0
    move-exception v0

    const v1, -0x6fe6d516

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public a(Landroid/view/View;LX/1Od;)V
    .locals 2

    .prologue
    .line 242859
    const-string v0, "BetterLinearLayoutManager.removeAndRecycleView"

    const v1, 0x13af0ee9

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 242860
    :try_start_0
    invoke-super {p0, p1, p2}, LX/1P1;->a(Landroid/view/View;LX/1Od;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242861
    const v0, -0x1e3b5287

    invoke-static {v0}, LX/02m;->a(I)V

    .line 242862
    return-void

    .line 242863
    :catchall_0
    move-exception v0

    const v1, 0x41ab55ee

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b(ILX/1Od;LX/1Ok;)I
    .locals 4

    .prologue
    .line 242864
    const-string v0, "BetterLinearLayoutManager.scrollVerticallyBy"

    const v1, -0x445bc159

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 242865
    :try_start_0
    invoke-virtual {p0}, LX/1P0;->J()V

    .line 242866
    invoke-super {p0, p1, p2, p3}, LX/1P1;->b(ILX/1Od;LX/1Ok;)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 242867
    const v1, 0x68f06c7c

    invoke-static {v1}, LX/02m;->a(I)V

    return v0

    .line 242868
    :catch_0
    move-exception v0

    .line 242869
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Adapter count: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/1OR;->D()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Scroll amount: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 242870
    :catchall_0
    move-exception v0

    const v1, -0x79483fd2

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b()LX/1a3;
    .locals 3

    .prologue
    .line 242837
    new-instance v0, LX/1a3;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, LX/1a3;-><init>(II)V

    return-object v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 242820
    invoke-super {p0, p1}, LX/1P1;->b(I)V

    .line 242821
    invoke-static {p0}, LX/1P0;->c(LX/1P0;)LX/1P4;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1P4;->a(I)V

    .line 242822
    return-void
.end method

.method public b(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 242823
    const-string v0, "BetterLinearLayoutManager.addView"

    const v1, 0xf7412d2

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 242824
    :try_start_0
    invoke-super {p0, p1, p2}, LX/1P1;->b(Landroid/view/View;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242825
    const v0, 0xc112d3e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 242826
    return-void

    .line 242827
    :catchall_0
    move-exception v0

    const v1, -0x277a7fa9

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public c(LX/1Od;LX/1Ok;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 242828
    invoke-virtual {p0}, LX/1P0;->J()V

    .line 242829
    invoke-super {p0, p1, p2}, LX/1P1;->c(LX/1Od;LX/1Ok;)V

    .line 242830
    iget-boolean v0, p0, LX/1P0;->t:Z

    if-eqz v0, :cond_0

    .line 242831
    iput-boolean v1, p0, LX/1P0;->t:Z

    .line 242832
    invoke-direct {p0, v1}, LX/1P0;->d(Z)V

    .line 242833
    :cond_0
    return-void
.end method

.method public final d(II)V
    .locals 0

    .prologue
    .line 242834
    invoke-virtual {p0}, LX/1P0;->J()V

    .line 242835
    invoke-super {p0, p1, p2}, LX/1P1;->d(II)V

    .line 242836
    return-void
.end method

.method public final e(I)V
    .locals 0

    .prologue
    .line 242838
    invoke-virtual {p0}, LX/1P0;->J()V

    .line 242839
    invoke-super {p0, p1}, LX/1P1;->e(I)V

    .line 242840
    return-void
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 242841
    iget-boolean v0, p0, LX/1P0;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1P0;->c:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 242842
    :cond_0
    invoke-super {p0}, LX/1P1;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/1P0;->c:Ljava/lang/Integer;

    .line 242843
    :cond_1
    iget-object v0, p0, LX/1P0;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 242844
    iget-boolean v0, p0, LX/1P0;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1P0;->d:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 242845
    :cond_0
    invoke-super {p0}, LX/1P1;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/1P0;->d:Ljava/lang/Integer;

    .line 242846
    :cond_1
    iget-object v0, p0, LX/1P0;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 242847
    iget-boolean v0, p0, LX/1P0;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1P0;->e:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 242848
    :cond_0
    invoke-super {p0}, LX/1P1;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/1P0;->e:Ljava/lang/Integer;

    .line 242849
    :cond_1
    iget-object v0, p0, LX/1P0;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final o()I
    .locals 1

    .prologue
    .line 242850
    iget-boolean v0, p0, LX/1P0;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1P0;->f:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 242851
    :cond_0
    invoke-super {p0}, LX/1P1;->o()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/1P0;->f:Ljava/lang/Integer;

    .line 242852
    :cond_1
    iget-object v0, p0, LX/1P0;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
