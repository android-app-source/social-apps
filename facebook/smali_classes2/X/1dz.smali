.class public LX/1dz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1dz;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0tX;

.field public final c:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/concurrent/ExecutorService;LX/0tX;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 287212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 287213
    iput-object p1, p0, LX/1dz;->a:Ljava/lang/String;

    .line 287214
    iput-object p2, p0, LX/1dz;->c:Ljava/util/concurrent/ExecutorService;

    .line 287215
    iput-object p3, p0, LX/1dz;->b:LX/0tX;

    .line 287216
    return-void
.end method

.method public static a(LX/0QB;)LX/1dz;
    .locals 6

    .prologue
    .line 287217
    sget-object v0, LX/1dz;->d:LX/1dz;

    if-nez v0, :cond_1

    .line 287218
    const-class v1, LX/1dz;

    monitor-enter v1

    .line 287219
    :try_start_0
    sget-object v0, LX/1dz;->d:LX/1dz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 287220
    if-eqz v2, :cond_0

    .line 287221
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 287222
    new-instance p0, LX/1dz;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-direct {p0, v3, v4, v5}, LX/1dz;-><init>(Ljava/lang/String;Ljava/util/concurrent/ExecutorService;LX/0tX;)V

    .line 287223
    move-object v0, p0

    .line 287224
    sput-object v0, LX/1dz;->d:LX/1dz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 287225
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 287226
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 287227
    :cond_1
    sget-object v0, LX/1dz;->d:LX/1dz;

    return-object v0

    .line 287228
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 287229
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 3

    .prologue
    .line 287230
    new-instance v0, LX/0ju;

    invoke-direct {v0, p1}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081052

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f08105f

    new-instance v2, LX/B0q;

    invoke-direct {v2, p0, p1, p2}, LX/B0q;-><init>(LX/1dz;Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0810d9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 287231
    return-void
.end method
