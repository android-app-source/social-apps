.class public LX/0w8;
.super LX/0gS;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 159328
    invoke-direct {p0}, LX/0gS;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 159329
    if-nez p2, :cond_0

    .line 159330
    :goto_0
    return-void

    .line 159331
    :cond_0
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 159332
    check-cast p2, Ljava/lang/String;

    .line 159333
    invoke-virtual {p0, p1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 159334
    goto :goto_0

    .line 159335
    :cond_1
    instance-of v0, p2, Ljava/lang/Number;

    if-eqz v0, :cond_2

    .line 159336
    check-cast p2, Ljava/lang/Number;

    .line 159337
    invoke-virtual {p0}, LX/0gS;->a()LX/0n9;

    move-result-object v0

    .line 159338
    invoke-static {v0, p1, p2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 159339
    goto :goto_0

    .line 159340
    :cond_2
    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 159341
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2}, LX/0w8;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 159342
    :cond_3
    instance-of v0, p2, Ljava/lang/Enum;

    if-eqz v0, :cond_4

    .line 159343
    check-cast p2, Ljava/lang/Enum;

    .line 159344
    invoke-virtual {p0}, LX/0gS;->a()LX/0n9;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v1

    .line 159345
    invoke-static {v0, p1, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 159346
    goto :goto_0

    .line 159347
    :cond_4
    instance-of v0, p2, Ljava/util/List;

    if-eqz v0, :cond_5

    .line 159348
    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    .line 159349
    :cond_5
    instance-of v0, p2, LX/0gS;

    if-eqz v0, :cond_6

    .line 159350
    check-cast p2, LX/0gS;

    invoke-virtual {p0, p1, p2}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    goto :goto_0

    .line 159351
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Value is not type that can be added. Actual value type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 159352
    invoke-virtual {p2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 159353
    return-void
.end method
