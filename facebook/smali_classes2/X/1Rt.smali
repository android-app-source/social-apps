.class public LX/1Rt;
.super LX/1OD;
.source ""


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1OD;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 247007
    invoke-direct {p0}, LX/1OD;-><init>()V

    .line 247008
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Rt;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 247009
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Rt;->b:Z

    move v1, v2

    .line 247010
    :goto_0
    iget-object v0, p0, LX/1Rt;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 247011
    iget-object v0, p0, LX/1Rt;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OD;

    invoke-virtual {v0}, LX/1OD;->a()V

    .line 247012
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 247013
    :cond_0
    iput-boolean v2, p0, LX/1Rt;->b:Z

    .line 247014
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 247015
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Rt;->b:Z

    move v1, v2

    .line 247016
    :goto_0
    iget-object v0, p0, LX/1Rt;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 247017
    iget-object v0, p0, LX/1Rt;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OD;

    invoke-virtual {v0, p1, p2}, LX/1OD;->a(II)V

    .line 247018
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 247019
    :cond_0
    iput-boolean v2, p0, LX/1Rt;->b:Z

    .line 247020
    return-void
.end method

.method public final a(III)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 247021
    iput-boolean v3, p0, LX/1Rt;->b:Z

    move v1, v2

    .line 247022
    :goto_0
    iget-object v0, p0, LX/1Rt;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 247023
    iget-object v0, p0, LX/1Rt;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OD;

    invoke-virtual {v0, p1, p2, v3}, LX/1OD;->a(III)V

    .line 247024
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 247025
    :cond_0
    iput-boolean v2, p0, LX/1Rt;->b:Z

    .line 247026
    return-void
.end method

.method public final a(LX/1OD;)V
    .locals 2

    .prologue
    .line 247027
    iget-boolean v0, p0, LX/1Rt;->b:Z

    if-eqz v0, :cond_0

    .line 247028
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t register observer during notify*()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 247029
    :cond_0
    iget-object v0, p0, LX/1Rt;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247030
    return-void
.end method

.method public final b(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 247031
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Rt;->b:Z

    move v1, v2

    .line 247032
    :goto_0
    iget-object v0, p0, LX/1Rt;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 247033
    iget-object v0, p0, LX/1Rt;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OD;

    invoke-virtual {v0, p1, p2}, LX/1OD;->b(II)V

    .line 247034
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 247035
    :cond_0
    iput-boolean v2, p0, LX/1Rt;->b:Z

    .line 247036
    return-void
.end method

.method public final b(LX/1OD;)V
    .locals 2

    .prologue
    .line 247037
    iget-boolean v0, p0, LX/1Rt;->b:Z

    if-eqz v0, :cond_0

    .line 247038
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t unregister observer during notify*()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 247039
    :cond_0
    iget-object v0, p0, LX/1Rt;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 247040
    return-void
.end method

.method public final c(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 247041
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Rt;->b:Z

    move v1, v2

    .line 247042
    :goto_0
    iget-object v0, p0, LX/1Rt;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 247043
    iget-object v0, p0, LX/1Rt;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OD;

    invoke-virtual {v0, p1, p2}, LX/1OD;->c(II)V

    .line 247044
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 247045
    :cond_0
    iput-boolean v2, p0, LX/1Rt;->b:Z

    .line 247046
    return-void
.end method
