.class public final LX/0ap;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0Xb;

.field private b:LX/3zj;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/03R;

.field public e:LX/03R;


# direct methods
.method public constructor <init>(LX/0Xb;)V
    .locals 2

    .prologue
    .line 85693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85694
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/0ap;->d:LX/03R;

    .line 85695
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/0ap;->e:LX/03R;

    .line 85696
    iput-object p1, p0, LX/0ap;->a:LX/0Xb;

    .line 85697
    new-instance v0, LX/0aq;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/0ap;->c:LX/0aq;

    .line 85698
    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 85699
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/0ap;->c(LX/0ap;)LX/0Xc;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, LX/0Xc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a(LX/0ap;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 85700
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/0ap;->c(LX/0ap;)LX/0Xc;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Xc;->b(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/0ap;LX/3zj;)V
    .locals 1

    .prologue
    .line 85675
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/0ap;->b:LX/3zj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85676
    monitor-exit p0

    return-void

    .line 85677
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 85690
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/0ap;->c(LX/0ap;)LX/0Xc;

    move-result-object v0

    .line 85691
    invoke-virtual {v0, p1}, LX/0Xc;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p1}, LX/0Xc;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    move v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85692
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static c(LX/0ap;)LX/0Xc;
    .locals 1

    .prologue
    .line 85689
    iget-object v0, p0, LX/0ap;->b:LX/3zj;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0ap;->a:LX/0Xb;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0ap;->b:LX/3zj;

    goto :goto_0
.end method


# virtual methods
.method public final a(SS)I
    .locals 4

    .prologue
    .line 85681
    shl-int/lit8 v0, p1, 0x10

    or-int/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 85682
    iget-object v0, p0, LX/0ap;->c:LX/0aq;

    invoke-virtual {v0, v1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 85683
    if-eqz v0, :cond_0

    .line 85684
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 85685
    :goto_0
    return v0

    .line 85686
    :cond_0
    const-string v0, "perf"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v2, v3}, LX/0ap;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 85687
    iget-object v2, p0, LX/0ap;->c:LX/0aq;

    invoke-virtual {v2, v1, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85688
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 85678
    iget-object v0, p0, LX/0ap;->d:LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 85679
    const-string v0, "perf"

    invoke-direct {p0, v0}, LX/0ap;->b(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/0ap;->d:LX/03R;

    .line 85680
    :cond_0
    iget-object v0, p0, LX/0ap;->d:LX/03R;

    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    return v0
.end method
