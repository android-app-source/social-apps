.class public final enum LX/0p3;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0p3;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0p3;

.field public static final enum DEGRADED:LX/0p3;

.field public static final enum EXCELLENT:LX/0p3;

.field public static final enum GOOD:LX/0p3;

.field public static final enum MODERATE:LX/0p3;

.field public static final enum POOR:LX/0p3;

.field public static final enum UNKNOWN:LX/0p3;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 143586
    new-instance v0, LX/0p3;

    const-string v1, "DEGRADED"

    invoke-direct {v0, v1, v3}, LX/0p3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0p3;->DEGRADED:LX/0p3;

    new-instance v0, LX/0p3;

    const-string v1, "POOR"

    invoke-direct {v0, v1, v4}, LX/0p3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0p3;->POOR:LX/0p3;

    new-instance v0, LX/0p3;

    const-string v1, "MODERATE"

    invoke-direct {v0, v1, v5}, LX/0p3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0p3;->MODERATE:LX/0p3;

    new-instance v0, LX/0p3;

    const-string v1, "GOOD"

    invoke-direct {v0, v1, v6}, LX/0p3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0p3;->GOOD:LX/0p3;

    new-instance v0, LX/0p3;

    const-string v1, "EXCELLENT"

    invoke-direct {v0, v1, v7}, LX/0p3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0p3;->EXCELLENT:LX/0p3;

    new-instance v0, LX/0p3;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0p3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0p3;->UNKNOWN:LX/0p3;

    .line 143587
    const/4 v0, 0x6

    new-array v0, v0, [LX/0p3;

    sget-object v1, LX/0p3;->DEGRADED:LX/0p3;

    aput-object v1, v0, v3

    sget-object v1, LX/0p3;->POOR:LX/0p3;

    aput-object v1, v0, v4

    sget-object v1, LX/0p3;->MODERATE:LX/0p3;

    aput-object v1, v0, v5

    sget-object v1, LX/0p3;->GOOD:LX/0p3;

    aput-object v1, v0, v6

    sget-object v1, LX/0p3;->EXCELLENT:LX/0p3;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0p3;->UNKNOWN:LX/0p3;

    aput-object v2, v0, v1

    sput-object v0, LX/0p3;->$VALUES:[LX/0p3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 143588
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0p3;
    .locals 1

    .prologue
    .line 143589
    const-class v0, LX/0p3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0p3;

    return-object v0
.end method

.method public static values()[LX/0p3;
    .locals 1

    .prologue
    .line 143590
    sget-object v0, LX/0p3;->$VALUES:[LX/0p3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0p3;

    return-object v0
.end method
