.class public LX/1RZ;
.super LX/1Ra;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1PW;",
        "V:",
        "Landroid/view/View;",
        ">",
        "LX/1Ra",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/api/SinglePartDefinitionWithViewTypeAndIsNeeded",
            "<TP;TS;TE;TV;>;"
        }
    .end annotation
.end field

.field public final b:LX/1RE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1RE",
            "<***>;"
        }
    .end annotation
.end field

.field public final c:LX/1Rb;

.field public final d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private final e:LX/1Qx;

.field private final f:LX/1Ra;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ra",
            "<TV;>;"
        }
    .end annotation
.end field

.field public g:Z

.field private h:LX/1aJ;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;LX/1RE;Ljava/lang/Object;LX/1Qx;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/SinglePartDefinitionWithViewTypeAndIsNeeded",
            "<TP;TS;TE;TV;>;",
            "LX/1RE",
            "<***>;TP;",
            "LX/1Qx;",
            ")V"
        }
    .end annotation

    .prologue
    .line 246453
    invoke-direct {p0}, LX/1Ra;-><init>()V

    .line 246454
    new-instance v0, LX/1Rb;

    invoke-direct {v0}, LX/1Rb;-><init>()V

    iput-object v0, p0, LX/1RZ;->c:LX/1Rb;

    .line 246455
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1RZ;->g:Z

    .line 246456
    iput-object p1, p0, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    .line 246457
    iput-object p2, p0, LX/1RZ;->b:LX/1RE;

    .line 246458
    iput-object p3, p0, LX/1RZ;->d:Ljava/lang/Object;

    .line 246459
    iput-object p4, p0, LX/1RZ;->e:LX/1Qx;

    .line 246460
    new-instance v0, LX/1Rc;

    iget-object v1, p0, LX/1RZ;->d:Ljava/lang/Object;

    iget-object v2, p0, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-direct {v0, v1, v2}, LX/1Rc;-><init>(Ljava/lang/Object;LX/1Nt;)V

    iput-object v0, p0, LX/1RZ;->f:LX/1Ra;

    .line 246461
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 246448
    iget-object v0, p0, LX/1RZ;->h:LX/1aJ;

    if-nez v0, :cond_0

    .line 246449
    new-instance v0, LX/1aJ;

    invoke-direct {v0, p0}, LX/1aJ;-><init>(LX/1RZ;)V

    move-object v0, v0

    .line 246450
    iput-object v0, p0, LX/1RZ;->h:LX/1aJ;

    .line 246451
    :cond_0
    iget-object v0, p0, LX/1RZ;->e:LX/1Qx;

    iget-object v1, p0, LX/1RZ;->h:LX/1aJ;

    invoke-interface {v0, p1, v1}, LX/1Qx;->a(ILjava/util/concurrent/Callable;)V

    .line 246452
    return-void
.end method


# virtual methods
.method public final a(LX/1PW;)V
    .locals 3

    .prologue
    .line 246436
    const-string v0, "SinglePartHolder.prepare"

    const v1, -0x4fe04ee0

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 246437
    :try_start_0
    iget-object v0, p0, LX/1RZ;->e:LX/1Qx;

    iget-object v1, p0, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/1Qx;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;I)V

    .line 246438
    iget-object v0, p0, LX/1RZ;->e:LX/1Qx;

    const/4 v1, 0x1

    invoke-interface {v0, v1, p1}, LX/1Qx;->a(ILX/1PW;)V

    .line 246439
    iget-object v0, p0, LX/1RZ;->f:LX/1Ra;

    invoke-virtual {v0, p1}, LX/1Ra;->a(LX/1PW;)V

    .line 246440
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/1RZ;->a(I)V

    .line 246441
    iget-object v0, p0, LX/1RZ;->e:LX/1Qx;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Qx;->a(I)V

    .line 246442
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1RZ;->g:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246443
    const v0, 0x1f7e641e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 246444
    :goto_0
    return-void

    .line 246445
    :catch_0
    move-exception v0

    .line 246446
    :try_start_1
    const-string v1, "preparing"

    invoke-static {p0, v0, v1}, LX/6Vp;->a(LX/1RZ;Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 246447
    const v0, 0x5f000bec

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x720f168d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 246462
    const-string v0, "SinglePartHolder.bind"

    const v1, -0x73ffce84

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 246463
    :try_start_0
    iget-object v0, p0, LX/1RZ;->b:LX/1RE;

    invoke-virtual {p0}, LX/1RZ;->b()LX/1Cz;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/1RE;->a(Landroid/view/View;LX/1Cz;)V

    .line 246464
    iget-object v0, p0, LX/1RZ;->e:LX/1Qx;

    iget-object v1, p0, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, LX/1Qx;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;I)V

    .line 246465
    iget-object v0, p0, LX/1RZ;->f:LX/1Ra;

    invoke-virtual {v0, p1}, LX/1Ra;->a(Landroid/view/View;)V

    .line 246466
    const/4 v0, 0x2

    invoke-direct {p0, v0}, LX/1RZ;->a(I)V

    .line 246467
    iget-object v0, p0, LX/1RZ;->e:LX/1Qx;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/1Qx;->a(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246468
    const v0, -0x535da7ae

    invoke-static {v0}, LX/02m;->a(I)V

    .line 246469
    :goto_0
    return-void

    .line 246470
    :catch_0
    move-exception v0

    .line 246471
    :try_start_1
    const-string v1, "binding"

    invoke-static {p0, v0, v1}, LX/6Vp;->a(LX/1RZ;Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 246472
    const v0, 0x45ebdcf2

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x6a5bb65c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 246435
    iget-object v0, p0, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-interface {v0}, LX/1RC;->a()LX/1Cz;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/1PW;)V
    .locals 2

    .prologue
    .line 246430
    :try_start_0
    iget-object v0, p0, LX/1RZ;->f:LX/1Ra;

    invoke-virtual {v0, p1}, LX/1Ra;->b(LX/1PW;)V

    .line 246431
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1RZ;->g:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 246432
    :goto_0
    return-void

    .line 246433
    :catch_0
    move-exception v0

    .line 246434
    const-string v1, "releasing"

    invoke-static {p0, v0, v1}, LX/6Vp;->a(LX/1RZ;Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 246419
    const-string v0, "SinglePartHolder.unbind"

    const v1, 0x59c8dc9d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 246420
    :try_start_0
    iget-object v0, p0, LX/1RZ;->e:LX/1Qx;

    iget-object v1, p0, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, LX/1Qx;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;I)V

    .line 246421
    iget-object v0, p0, LX/1RZ;->f:LX/1Ra;

    invoke-virtual {v0, p1}, LX/1Ra;->b(Landroid/view/View;)V

    .line 246422
    const/4 v0, 0x3

    invoke-direct {p0, v0}, LX/1RZ;->a(I)V

    .line 246423
    iget-object v0, p0, LX/1RZ;->e:LX/1Qx;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, LX/1Qx;->a(I)V

    .line 246424
    iget-object v0, p0, LX/1RZ;->b:LX/1RE;

    invoke-virtual {p0}, LX/1RZ;->b()LX/1Cz;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/1RE;->b(Landroid/view/View;LX/1Cz;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246425
    const v0, 0x6947c765

    invoke-static {v0}, LX/02m;->a(I)V

    .line 246426
    :goto_0
    return-void

    .line 246427
    :catch_0
    move-exception v0

    .line 246428
    :try_start_1
    const-string v1, "unbinding"

    invoke-static {p0, v0, v1}, LX/6Vp;->a(LX/1RZ;Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 246429
    const v0, 0xe9c3e3d

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x1810c335

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
