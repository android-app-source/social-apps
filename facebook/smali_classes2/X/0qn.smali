.class public LX/0qn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0qn;


# instance fields
.field private final a:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 148478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148479
    new-instance v0, LX/0aq;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/0qn;->a:LX/0aq;

    .line 148480
    return-void
.end method

.method public static a(LX/0QB;)LX/0qn;
    .locals 3

    .prologue
    .line 148466
    sget-object v0, LX/0qn;->b:LX/0qn;

    if-nez v0, :cond_1

    .line 148467
    const-class v1, LX/0qn;

    monitor-enter v1

    .line 148468
    :try_start_0
    sget-object v0, LX/0qn;->b:LX/0qn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 148469
    if-eqz v2, :cond_0

    .line 148470
    :try_start_1
    new-instance v0, LX/0qn;

    invoke-direct {v0}, LX/0qn;-><init>()V

    .line 148471
    move-object v0, v0

    .line 148472
    sput-object v0, LX/0qn;->b:LX/0qn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 148473
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 148474
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 148475
    :cond_1
    sget-object v0, LX/0qn;->b:LX/0qn;

    return-object v0

    .line 148476
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 148477
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static c(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 148463
    if-nez p0, :cond_0

    .line 148464
    const/4 v0, 0x0

    .line 148465
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;
    .locals 2

    .prologue
    .line 148453
    invoke-static {p1}, LX/0qn;->c(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 148454
    if-nez v0, :cond_1

    .line 148455
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    .line 148456
    :cond_0
    :goto_0
    return-object v0

    .line 148457
    :cond_1
    iget-object v1, p0, LX/0qn;->a:LX/0aq;

    invoke-virtual {v1, v0}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    .line 148458
    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V
    .locals 2

    .prologue
    .line 148461
    iget-object v0, p0, LX/0qn;->a:LX/0aq;

    invoke-static {p1}, LX/0qn;->c(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148462
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2

    .prologue
    .line 148459
    invoke-virtual {p0, p1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    .line 148460
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
