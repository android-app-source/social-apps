.class public LX/0ih;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/0ih;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:S

.field public c:I

.field public d:Z

.field public e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 122042
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/0ih;->f:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 122055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122056
    const/16 v0, 0x258

    iput v0, p0, LX/0ih;->c:I

    .line 122057
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0ih;->d:Z

    .line 122058
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0ih;->e:Z

    .line 122059
    iput-object p1, p0, LX/0ih;->a:Ljava/lang/String;

    .line 122060
    iget-object v0, p0, LX/0ih;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, LX/0ih;->b:S

    .line 122061
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0ih;
    .locals 2

    .prologue
    .line 122051
    new-instance v0, LX/0ih;

    invoke-direct {v0, p0}, LX/0ih;-><init>(Ljava/lang/String;)V

    .line 122052
    sget-object v1, LX/0ih;->f:Ljava/util/HashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122053
    return-object v0
.end method

.method public static b(Ljava/lang/String;)LX/0ih;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122054
    sget-object v0, LX/0ih;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ih;

    return-object v0
.end method


# virtual methods
.method public final e()LX/0ih;
    .locals 1

    .prologue
    .line 122049
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0ih;->e:Z

    .line 122050
    return-object p0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 122044
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/0ih;

    if-eqz v0, :cond_0

    .line 122045
    check-cast p1, LX/0ih;

    .line 122046
    iget-object v0, p1, LX/0ih;->a:Ljava/lang/String;

    move-object v0, v0

    .line 122047
    iget-object v1, p0, LX/0ih;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 122048
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 122043
    iget-object v0, p0, LX/0ih;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
