.class public LX/1qm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/05H;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1qm;


# instance fields
.field private final a:LX/1p6;

.field private final b:LX/0ad;

.field private final c:LX/01T;


# direct methods
.method public constructor <init>(LX/01T;LX/0ad;LX/1p6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 331468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331469
    iput-object p1, p0, LX/1qm;->c:LX/01T;

    .line 331470
    iput-object p2, p0, LX/1qm;->b:LX/0ad;

    .line 331471
    iput-object p3, p0, LX/1qm;->a:LX/1p6;

    .line 331472
    return-void
.end method

.method public static a(LX/0QB;)LX/1qm;
    .locals 6

    .prologue
    .line 331455
    sget-object v0, LX/1qm;->d:LX/1qm;

    if-nez v0, :cond_1

    .line 331456
    const-class v1, LX/1qm;

    monitor-enter v1

    .line 331457
    :try_start_0
    sget-object v0, LX/1qm;->d:LX/1qm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 331458
    if-eqz v2, :cond_0

    .line 331459
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 331460
    new-instance p0, LX/1qm;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v3

    check-cast v3, LX/01T;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/1p6;->a(LX/0QB;)LX/1p6;

    move-result-object v5

    check-cast v5, LX/1p6;

    invoke-direct {p0, v3, v4, v5}, LX/1qm;-><init>(LX/01T;LX/0ad;LX/1p6;)V

    .line 331461
    move-object v0, p0

    .line 331462
    sput-object v0, LX/1qm;->d:LX/1qm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331463
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 331464
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 331465
    :cond_1
    sget-object v0, LX/1qm;->d:LX/1qm;

    return-object v0

    .line 331466
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 331467
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 331451
    iget-object v0, p0, LX/1qm;->a:LX/1p6;

    invoke-virtual {v0}, LX/04p;->b()LX/04q;

    move-result-object v0

    iget v0, v0, LX/04q;->p:I

    return v0
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 331452
    iget-object v0, p0, LX/1qm;->c:LX/01T;

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_0

    .line 331453
    const/16 v0, 0x12c

    .line 331454
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1qm;->b:LX/0ad;

    sget v1, LX/6ms;->a:I

    iget-object v2, p0, LX/1qm;->a:LX/1p6;

    invoke-virtual {v2}, LX/04p;->b()LX/04q;

    move-result-object v2

    iget v2, v2, LX/04q;->o:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    goto :goto_0
.end method
