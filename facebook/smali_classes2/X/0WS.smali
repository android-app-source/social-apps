.class public LX/0WS;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Object;

.field private static final b:Landroid/os/Handler;


# instance fields
.field public final c:Ljava/lang/Object;

.field public final d:Ljava/util/concurrent/Executor;

.field public final e:Ljava/util/concurrent/CountDownLatch;

.field public final f:LX/0WR;

.field public final g:I

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "LX/48Z;",
            "Landroid/os/Handler;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mCacheLock"
    .end annotation
.end field

.field private final j:Ljava/lang/Thread;

.field public final k:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public volatile l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 76385
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/0WS;->a:Ljava/lang/Object;

    .line 76386
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, LX/0WS;->b:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/util/concurrent/Executor;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 76372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76373
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/0WS;->c:Ljava/lang/Object;

    .line 76374
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/0WS;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 76375
    iput-boolean v1, p0, LX/0WS;->l:Z

    .line 76376
    new-instance v1, LX/0WR;

    invoke-static {p1}, LX/0WS;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-direct {v1, v0}, LX/0WR;-><init>(Ljava/io/File;)V

    iput-object v1, p0, LX/0WS;->f:LX/0WR;

    .line 76377
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0WS;->i:Ljava/util/Map;

    .line 76378
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0WS;->h:Ljava/util/Map;

    .line 76379
    invoke-static {p2}, LX/0WS;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, LX/0WS;->d:Ljava/util/concurrent/Executor;

    .line 76380
    iput p3, p0, LX/0WS;->g:I

    .line 76381
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LX/0WS;->e:Ljava/util/concurrent/CountDownLatch;

    .line 76382
    new-instance v0, Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$1;

    invoke-direct {v0, p0}, Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$1;-><init>(LX/0WS;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LSP-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v2, 0x48697c6d

    invoke-static {v0, v1, v2}, LX/00l;->a(Ljava/lang/Runnable;Ljava/lang/String;I)Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, LX/0WS;->j:Ljava/lang/Thread;

    .line 76383
    iget-object v0, p0, LX/0WS;->j:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 76384
    return-void
.end method

.method private static a(LX/0WS;Ljava/lang/Exception;Ljava/lang/String;)Ljava/lang/RuntimeException;
    .locals 5

    .prologue
    .line 76367
    const/4 v0, 0x0

    .line 76368
    :try_start_0
    iget-object v1, p0, LX/0WS;->f:LX/0WR;

    invoke-virtual {v1}, LX/0WR;->a()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 76369
    :goto_0
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "LightSharedPreferences threw an exception for Key: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; Raw file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v1

    .line 76370
    :catch_0
    move-exception v1

    .line 76371
    const-string v2, "LightSharedPreferencesImpl"

    const-string v3, "Failed to load the file for soft report!"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static declared-synchronized a$redex0(LX/0WS;Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 76358
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 76359
    iget-object v1, p0, LX/0WS;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 76360
    if-eqz v1, :cond_0

    .line 76361
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 76362
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/48Z;

    .line 76363
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    .line 76364
    new-instance v5, Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$2;

    invoke-direct {v5, p0, v2, v0}, Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$2;-><init>(LX/0WS;LX/48Z;Ljava/lang/String;)V

    const v2, -0x59901a25

    invoke-static {v1, v5, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 76365
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 76366
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public static b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 76355
    if-nez p0, :cond_0

    .line 76356
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 76357
    :cond_0
    return-object p0
.end method

.method private static d(LX/0WS;)V
    .locals 1

    .prologue
    .line 76350
    :goto_0
    iget-boolean v0, p0, LX/0WS;->l:Z

    if-nez v0, :cond_0

    .line 76351
    invoke-direct {p0}, LX/0WS;->e()V

    .line 76352
    :try_start_0
    iget-object v0, p0, LX/0WS;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76353
    :cond_0
    return-void

    .line 76354
    :catch_0
    goto :goto_0
.end method

.method private declared-synchronized e()V
    .locals 2

    .prologue
    .line 76291
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0WS;->j:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v1, Ljava/lang/Thread$State;->TERMINATED:Ljava/lang/Thread$State;

    if-eq v0, v1, :cond_0

    .line 76292
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 76293
    invoke-virtual {v0}, Ljava/lang/Thread;->getPriority()I

    move-result v0

    .line 76294
    iget-object v1, p0, LX/0WS;->j:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->getPriority()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 76295
    iget-object v1, p0, LX/0WS;->j:Ljava/lang/Thread;

    invoke-virtual {v1, v0}, Ljava/lang/Thread;->setPriority(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76296
    :cond_0
    monitor-exit p0

    return-void

    .line 76297
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;F)F
    .locals 2

    .prologue
    .line 76343
    invoke-static {p0}, LX/0WS;->d(LX/0WS;)V

    .line 76344
    iget-object v1, p0, LX/0WS;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 76345
    :try_start_0
    iget-object v0, p0, LX/0WS;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 76346
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result p2

    :cond_0
    :try_start_1
    monitor-exit v1

    return p2

    .line 76347
    :catch_0
    move-exception v0

    .line 76348
    invoke-static {p0, v0, p1}, LX/0WS;->a(LX/0WS;Ljava/lang/Exception;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 76349
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 76336
    invoke-static {p0}, LX/0WS;->d(LX/0WS;)V

    .line 76337
    iget-object v1, p0, LX/0WS;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 76338
    :try_start_0
    iget-object v0, p0, LX/0WS;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 76339
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result p2

    :cond_0
    :try_start_1
    monitor-exit v1

    return p2

    .line 76340
    :catch_0
    move-exception v0

    .line 76341
    invoke-static {p0, v0, p1}, LX/0WS;->a(LX/0WS;Ljava/lang/Exception;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 76342
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 76329
    invoke-static {p0}, LX/0WS;->d(LX/0WS;)V

    .line 76330
    iget-object v1, p0, LX/0WS;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 76331
    :try_start_0
    iget-object v0, p0, LX/0WS;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 76332
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide p2

    :cond_0
    :try_start_1
    monitor-exit v1

    return-wide p2

    .line 76333
    :catch_0
    move-exception v0

    .line 76334
    invoke-static {p0, v0, p1}, LX/0WS;->a(LX/0WS;Ljava/lang/Exception;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 76335
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 76322
    invoke-static {p0}, LX/0WS;->d(LX/0WS;)V

    .line 76323
    iget-object v1, p0, LX/0WS;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 76324
    :try_start_0
    iget-object v0, p0, LX/0WS;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76325
    if-eqz v0, :cond_0

    :goto_0
    :try_start_1
    monitor-exit v1

    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0

    .line 76326
    :catch_0
    move-exception v0

    .line 76327
    invoke-static {p0, v0, p1}, LX/0WS;->a(LX/0WS;Ljava/lang/Exception;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 76328
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation

    .prologue
    .line 76318
    invoke-static {p0}, LX/0WS;->d(LX/0WS;)V

    .line 76319
    iget-object v1, p0, LX/0WS;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 76320
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    iget-object v2, p0, LX/0WS;->i:Ljava/util/Map;

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    monitor-exit v1

    return-object v0

    .line 76321
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76311
    invoke-static {p0}, LX/0WS;->d(LX/0WS;)V

    .line 76312
    iget-object v1, p0, LX/0WS;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 76313
    :try_start_0
    iget-object v0, p0, LX/0WS;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76314
    if-eqz v0, :cond_0

    :goto_0
    :try_start_1
    monitor-exit v1

    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0

    .line 76315
    :catch_0
    move-exception v0

    .line 76316
    invoke-static {p0, v0, p1}, LX/0WS;->a(LX/0WS;Ljava/lang/Exception;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 76317
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 76307
    invoke-static {p0}, LX/0WS;->d(LX/0WS;)V

    .line 76308
    iget-object v1, p0, LX/0WS;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 76309
    :try_start_0
    iget-object v0, p0, LX/0WS;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 76310
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 2

    .prologue
    .line 76300
    invoke-static {p0}, LX/0WS;->d(LX/0WS;)V

    .line 76301
    iget-object v1, p0, LX/0WS;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 76302
    :try_start_0
    iget-object v0, p0, LX/0WS;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 76303
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result p2

    :cond_0
    :try_start_1
    monitor-exit v1

    return p2

    .line 76304
    :catch_0
    move-exception v0

    .line 76305
    invoke-static {p0, v0, p1}, LX/0WS;->a(LX/0WS;Ljava/lang/Exception;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 76306
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()LX/1gW;
    .locals 1

    .prologue
    .line 76298
    invoke-static {p0}, LX/0WS;->d(LX/0WS;)V

    .line 76299
    new-instance v0, LX/1gV;

    invoke-direct {v0, p0}, LX/1gV;-><init>(LX/0WS;)V

    return-object v0
.end method
