.class public LX/15L;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/13m;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/1Aa;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/13m;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 180507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180508
    iput-object p1, p0, LX/15L;->a:LX/13m;

    .line 180509
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/15L;->b:Ljava/util/List;

    .line 180510
    return-void
.end method

.method public static a(LX/0QB;)LX/15L;
    .locals 4

    .prologue
    .line 180511
    const-class v1, LX/15L;

    monitor-enter v1

    .line 180512
    :try_start_0
    sget-object v0, LX/15L;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 180513
    sput-object v2, LX/15L;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 180514
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180515
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 180516
    new-instance p0, LX/15L;

    invoke-static {v0}, LX/13m;->a(LX/0QB;)LX/13m;

    move-result-object v3

    check-cast v3, LX/13m;

    invoke-direct {p0, v3}, LX/15L;-><init>(LX/13m;)V

    .line 180517
    move-object v0, p0

    .line 180518
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 180519
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/15L;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 180520
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 180521
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
