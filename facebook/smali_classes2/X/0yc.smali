.class public abstract LX/0yc;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 165675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165676
    return-void
.end method


# virtual methods
.method public a(FFLX/396;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 165661
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(LX/0yL;)V
    .locals 0

    .prologue
    .line 165662
    return-void
.end method

.method public a(LX/13j;)V
    .locals 0

    .prologue
    .line 165663
    return-void
.end method

.method public a(LX/15i;I)V
    .locals 0
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "updateQuota"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 165664
    return-void
.end method

.method public a(LX/32P;)V
    .locals 0

    .prologue
    .line 165665
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 165659
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 165666
    const/4 v0, 0x0

    return v0
.end method

.method public a(LX/0yL;Landroid/net/Uri;Z)Z
    .locals 1

    .prologue
    .line 165667
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 165668
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Z
    .locals 1

    .prologue
    .line 165669
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 165672
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/0yc;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)Z
    .locals 1

    .prologue
    .line 165680
    const/4 v0, 0x0

    return v0
.end method

.method public a(Ljava/lang/String;Z)Z
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 165679
    const/4 v0, 0x0

    return v0
.end method

.method public a(Z)Z
    .locals 1

    .prologue
    .line 165678
    const/4 v0, 0x0

    return v0
.end method

.method public b(LX/0yL;)V
    .locals 0

    .prologue
    .line 165677
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 165674
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Z
    .locals 1

    .prologue
    .line 165673
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 165670
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/0yc;->b(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;Z)Z
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 165671
    const/4 v0, 0x0

    return v0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 165660
    const/4 v0, 0x0

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 165638
    const/4 v0, 0x0

    return v0
.end method

.method public d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 165639
    const/4 v0, 0x0

    return v0
.end method

.method public e()Landroid/app/Activity;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 165640
    const/4 v0, 0x0

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 165641
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 165642
    const/4 v0, 0x0

    return v0
.end method

.method public g()V
    .locals 0

    .prologue
    .line 165658
    return-void
.end method

.method public h()V
    .locals 0

    .prologue
    .line 165643
    return-void
.end method

.method public i()V
    .locals 0

    .prologue
    .line 165644
    return-void
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 165645
    const/4 v0, 0x0

    return v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 165646
    const/4 v0, 0x0

    return v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 165647
    const/4 v0, 0x0

    return v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 165648
    const/4 v0, 0x0

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 165649
    const/4 v0, 0x0

    return v0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 165650
    const/4 v0, 0x0

    return v0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 165651
    const/4 v0, 0x0

    return v0
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 165652
    const/4 v0, 0x0

    return v0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 165653
    const/4 v0, 0x0

    return v0
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 165654
    const/4 v0, 0x0

    return v0
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 165655
    const/4 v0, 0x0

    return v0
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 165656
    const/4 v0, 0x0

    return v0
.end method

.method public v()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165657
    const/4 v0, 0x0

    return-object v0
.end method
