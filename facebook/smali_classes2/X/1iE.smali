.class public LX/1iE;
.super Lorg/apache/http/impl/client/EntityEnclosingRequestWrapper;
.source ""

# interfaces
.implements Lorg/apache/http/client/methods/AbortableHttpRequest;


# instance fields
.field private final a:Lorg/apache/http/client/methods/AbortableHttpRequest;

.field private b:Lorg/apache/http/RequestLine;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/http/client/methods/AbortableHttpRequest;Lorg/apache/http/HttpEntityEnclosingRequest;)V
    .locals 1

    .prologue
    .line 297608
    invoke-direct {p0, p2}, Lorg/apache/http/impl/client/EntityEnclosingRequestWrapper;-><init>(Lorg/apache/http/HttpEntityEnclosingRequest;)V

    .line 297609
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/methods/AbortableHttpRequest;

    iput-object v0, p0, LX/1iE;->a:Lorg/apache/http/client/methods/AbortableHttpRequest;

    .line 297610
    return-void
.end method


# virtual methods
.method public final getRequestLine()Lorg/apache/http/RequestLine;
    .locals 1

    .prologue
    .line 297611
    iget-object v0, p0, LX/1iE;->b:Lorg/apache/http/RequestLine;

    .line 297612
    if-eqz v0, :cond_0

    .line 297613
    :goto_0
    return-object v0

    .line 297614
    :cond_0
    invoke-super {p0}, Lorg/apache/http/impl/client/EntityEnclosingRequestWrapper;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    iput-object v0, p0, LX/1iE;->b:Lorg/apache/http/RequestLine;

    .line 297615
    iget-object v0, p0, LX/1iE;->b:Lorg/apache/http/RequestLine;

    goto :goto_0
.end method

.method public final setConnectionRequest(Lorg/apache/http/conn/ClientConnectionRequest;)V
    .locals 1

    .prologue
    .line 297616
    iget-object v0, p0, LX/1iE;->a:Lorg/apache/http/client/methods/AbortableHttpRequest;

    invoke-interface {v0, p1}, Lorg/apache/http/client/methods/AbortableHttpRequest;->setConnectionRequest(Lorg/apache/http/conn/ClientConnectionRequest;)V

    .line 297617
    return-void
.end method

.method public final setMethod(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 297618
    const/4 v0, 0x0

    iput-object v0, p0, LX/1iE;->b:Lorg/apache/http/RequestLine;

    .line 297619
    invoke-super {p0, p1}, Lorg/apache/http/impl/client/EntityEnclosingRequestWrapper;->setMethod(Ljava/lang/String;)V

    .line 297620
    return-void
.end method

.method public final setProtocolVersion(Lorg/apache/http/ProtocolVersion;)V
    .locals 1

    .prologue
    .line 297621
    const/4 v0, 0x0

    iput-object v0, p0, LX/1iE;->b:Lorg/apache/http/RequestLine;

    .line 297622
    invoke-super {p0, p1}, Lorg/apache/http/impl/client/EntityEnclosingRequestWrapper;->setProtocolVersion(Lorg/apache/http/ProtocolVersion;)V

    .line 297623
    return-void
.end method

.method public final setReleaseTrigger(Lorg/apache/http/conn/ConnectionReleaseTrigger;)V
    .locals 1

    .prologue
    .line 297624
    iget-object v0, p0, LX/1iE;->a:Lorg/apache/http/client/methods/AbortableHttpRequest;

    invoke-interface {v0, p1}, Lorg/apache/http/client/methods/AbortableHttpRequest;->setReleaseTrigger(Lorg/apache/http/conn/ConnectionReleaseTrigger;)V

    .line 297625
    return-void
.end method

.method public final setURI(Ljava/net/URI;)V
    .locals 1

    .prologue
    .line 297626
    const/4 v0, 0x0

    iput-object v0, p0, LX/1iE;->b:Lorg/apache/http/RequestLine;

    .line 297627
    invoke-super {p0, p1}, Lorg/apache/http/impl/client/EntityEnclosingRequestWrapper;->setURI(Ljava/net/URI;)V

    .line 297628
    return-void
.end method
