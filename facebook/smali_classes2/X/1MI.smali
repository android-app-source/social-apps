.class public final enum LX/1MI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1MI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1MI;

.field public static final enum COMPLETED:LX/1MI;

.field public static final enum INIT:LX/1MI;

.field public static final enum OPERATION_QUEUED:LX/1MI;

.field public static final enum READY_TO_QUEUE:LX/1MI;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 235143
    new-instance v0, LX/1MI;

    const-string v1, "INIT"

    invoke-direct {v0, v1, v2}, LX/1MI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1MI;->INIT:LX/1MI;

    .line 235144
    new-instance v0, LX/1MI;

    const-string v1, "READY_TO_QUEUE"

    invoke-direct {v0, v1, v3}, LX/1MI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1MI;->READY_TO_QUEUE:LX/1MI;

    .line 235145
    new-instance v0, LX/1MI;

    const-string v1, "OPERATION_QUEUED"

    invoke-direct {v0, v1, v4}, LX/1MI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1MI;->OPERATION_QUEUED:LX/1MI;

    .line 235146
    new-instance v0, LX/1MI;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v5}, LX/1MI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1MI;->COMPLETED:LX/1MI;

    .line 235147
    const/4 v0, 0x4

    new-array v0, v0, [LX/1MI;

    sget-object v1, LX/1MI;->INIT:LX/1MI;

    aput-object v1, v0, v2

    sget-object v1, LX/1MI;->READY_TO_QUEUE:LX/1MI;

    aput-object v1, v0, v3

    sget-object v1, LX/1MI;->OPERATION_QUEUED:LX/1MI;

    aput-object v1, v0, v4

    sget-object v1, LX/1MI;->COMPLETED:LX/1MI;

    aput-object v1, v0, v5

    sput-object v0, LX/1MI;->$VALUES:[LX/1MI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 235148
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1MI;
    .locals 1

    .prologue
    .line 235142
    const-class v0, LX/1MI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1MI;

    return-object v0
.end method

.method public static values()[LX/1MI;
    .locals 1

    .prologue
    .line 235141
    sget-object v0, LX/1MI;->$VALUES:[LX/1MI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1MI;

    return-object v0
.end method
