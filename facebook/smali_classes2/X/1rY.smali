.class public LX/1rY;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 332476
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 332477
    return-void
.end method

.method public static a(Landroid/os/Handler;)LX/0Tf;
    .locals 1
    .param p0    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/location/threading/ForLocationNonUiThread;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.Thread.start"
        }
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/location/threading/ForLocationNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 332482
    new-instance v0, LX/0Td;

    invoke-direct {v0, p0}, LX/0Td;-><init>(Landroid/os/Handler;)V

    return-object v0
.end method

.method public static a(LX/0Zr;)Landroid/os/Handler;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.Thread.start"
        }
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/location/threading/ForLocationNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 332479
    const-string v0, "fglNonUiHandler"

    sget-object v1, LX/0TP;->BACKGROUND:LX/0TP;

    invoke-virtual {p0, v0, v1}, LX/0Zr;->a(Ljava/lang/String;LX/0TP;)Landroid/os/HandlerThread;

    move-result-object v0

    .line 332480
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 332481
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-object v1
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 332478
    return-void
.end method
