.class public abstract LX/1cr;
.super LX/0vn;
.source ""


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Landroid/graphics/Rect;

.field private final d:Landroid/graphics/Rect;

.field public final e:Landroid/graphics/Rect;

.field private final f:[I

.field public final g:Landroid/view/accessibility/AccessibilityManager;

.field public final h:Landroid/view/View;

.field private i:LX/3ta;

.field public j:I

.field private k:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 283535
    const-class v0, Landroid/view/View;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1cr;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 283505
    invoke-direct {p0}, LX/0vn;-><init>()V

    .line 283506
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1cr;->c:Landroid/graphics/Rect;

    .line 283507
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1cr;->d:Landroid/graphics/Rect;

    .line 283508
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1cr;->e:Landroid/graphics/Rect;

    .line 283509
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, LX/1cr;->f:[I

    .line 283510
    iput v1, p0, LX/1cr;->j:I

    .line 283511
    iput v1, p0, LX/1cr;->k:I

    .line 283512
    if-nez p1, :cond_0

    .line 283513
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "View may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283514
    :cond_0
    iput-object p1, p0, LX/1cr;->h:Landroid/view/View;

    .line 283515
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 283516
    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, LX/1cr;->g:Landroid/view/accessibility/AccessibilityManager;

    .line 283517
    return-void
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 283518
    iget v0, p0, LX/1cr;->k:I

    if-ne v0, p1, :cond_0

    .line 283519
    :goto_0
    return-void

    .line 283520
    :cond_0
    iget v0, p0, LX/1cr;->k:I

    .line 283521
    iput p1, p0, LX/1cr;->k:I

    .line 283522
    const/16 v1, 0x80

    invoke-virtual {p0, p1, v1}, LX/1cr;->a(II)Z

    .line 283523
    const/16 v1, 0x100

    invoke-virtual {p0, v0, v1}, LX/1cr;->a(II)Z

    goto :goto_0
.end method

.method public static d(LX/1cr;II)Landroid/view/accessibility/AccessibilityEvent;
    .locals 3

    .prologue
    .line 283524
    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 283525
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 283526
    sget-object v1, LX/1cr;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 283527
    invoke-virtual {p0, p1, v0}, LX/1cr;->a(ILandroid/view/accessibility/AccessibilityEvent;)V

    .line 283528
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_0

    .line 283529
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must add text or a content description in populateEventForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283530
    :cond_0
    iget-object v1, p0, LX/1cr;->h:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 283531
    invoke-static {v0}, LX/2fV;->a(Landroid/view/accessibility/AccessibilityEvent;)LX/2fO;

    move-result-object v1

    .line 283532
    iget-object v2, p0, LX/1cr;->h:Landroid/view/View;

    invoke-virtual {v1, v2, p1}, LX/2fO;->a(Landroid/view/View;I)V

    .line 283533
    return-object v0
.end method

.method public static e(LX/1cr;I)LX/3sp;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 283451
    invoke-static {}, LX/3sp;->b()LX/3sp;

    move-result-object v0

    .line 283452
    invoke-virtual {v0, v3}, LX/3sp;->h(Z)V

    .line 283453
    sget-object v1, LX/1cr;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3sp;->b(Ljava/lang/CharSequence;)V

    .line 283454
    invoke-virtual {p0, p1, v0}, LX/1cr;->a(ILX/3sp;)V

    .line 283455
    invoke-virtual {v0}, LX/3sp;->n()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, LX/3sp;->o()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_0

    .line 283456
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must add text or a content description in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283457
    :cond_0
    iget-object v1, p0, LX/1cr;->d:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, LX/3sp;->a(Landroid/graphics/Rect;)V

    .line 283458
    iget-object v1, p0, LX/1cr;->d:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 283459
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must set parent bounds in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283460
    :cond_1
    invoke-virtual {v0}, LX/3sp;->c()I

    move-result v1

    .line 283461
    and-int/lit8 v2, v1, 0x40

    if-eqz v2, :cond_2

    .line 283462
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283463
    :cond_2
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_3

    .line 283464
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283465
    :cond_3
    iget-object v1, p0, LX/1cr;->h:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3sp;->a(Ljava/lang/CharSequence;)V

    .line 283466
    iget-object v1, p0, LX/1cr;->h:Landroid/view/View;

    .line 283467
    sget-object v2, LX/3sp;->a:LX/3sg;

    iget-object v5, v0, LX/3sp;->b:Ljava/lang/Object;

    invoke-interface {v2, v5, v1, p1}, LX/3sg;->a(Ljava/lang/Object;Landroid/view/View;I)V

    .line 283468
    iget-object v1, p0, LX/1cr;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/3sp;->d(Landroid/view/View;)V

    .line 283469
    iget v1, p0, LX/1cr;->j:I

    if-ne v1, p1, :cond_6

    .line 283470
    invoke-virtual {v0, v3}, LX/3sp;->d(Z)V

    .line 283471
    const/16 v1, 0x80

    invoke-virtual {v0, v1}, LX/3sp;->a(I)V

    .line 283472
    :goto_0
    iget-object v1, p0, LX/1cr;->d:Landroid/graphics/Rect;

    const/4 v5, 0x0

    .line 283473
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_4
    move v2, v5

    .line 283474
    :goto_1
    move v1, v2

    .line 283475
    if-eqz v1, :cond_5

    .line 283476
    invoke-virtual {v0, v3}, LX/3sp;->c(Z)V

    .line 283477
    iget-object v1, p0, LX/1cr;->d:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, LX/3sp;->b(Landroid/graphics/Rect;)V

    .line 283478
    :cond_5
    iget-object v1, p0, LX/1cr;->h:Landroid/view/View;

    iget-object v2, p0, LX/1cr;->f:[I

    invoke-virtual {v1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 283479
    iget-object v1, p0, LX/1cr;->f:[I

    aget v1, v1, v4

    .line 283480
    iget-object v2, p0, LX/1cr;->f:[I

    aget v2, v2, v3

    .line 283481
    iget-object v3, p0, LX/1cr;->c:Landroid/graphics/Rect;

    iget-object v4, p0, LX/1cr;->d:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 283482
    iget-object v3, p0, LX/1cr;->c:Landroid/graphics/Rect;

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 283483
    iget-object v1, p0, LX/1cr;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, LX/3sp;->d(Landroid/graphics/Rect;)V

    .line 283484
    return-object v0

    .line 283485
    :cond_6
    invoke-virtual {v0, v4}, LX/3sp;->d(Z)V

    .line 283486
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, LX/3sp;->a(I)V

    goto :goto_0

    .line 283487
    :cond_7
    iget-object v2, p0, LX/1cr;->h:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWindowVisibility()I

    move-result v2

    if-eqz v2, :cond_8

    move v2, v5

    .line 283488
    goto :goto_1

    .line 283489
    :cond_8
    iget-object v2, p0, LX/1cr;->h:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 283490
    :goto_2
    instance-of v6, v2, Landroid/view/View;

    if-eqz v6, :cond_b

    .line 283491
    check-cast v2, Landroid/view/View;

    .line 283492
    invoke-static {v2}, LX/0vv;->f(Landroid/view/View;)F

    move-result v6

    const/4 p1, 0x0

    cmpg-float v6, v6, p1

    if-lez v6, :cond_9

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-eqz v6, :cond_a

    :cond_9
    move v2, v5

    .line 283493
    goto :goto_1

    .line 283494
    :cond_a
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    goto :goto_2

    .line 283495
    :cond_b
    if-nez v2, :cond_c

    move v2, v5

    .line 283496
    goto :goto_1

    .line 283497
    :cond_c
    iget-object v2, p0, LX/1cr;->h:Landroid/view/View;

    iget-object v6, p0, LX/1cr;->e:Landroid/graphics/Rect;

    invoke-virtual {v2, v6}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_d

    move v2, v5

    .line 283498
    goto :goto_1

    .line 283499
    :cond_d
    iget-object v2, p0, LX/1cr;->e:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v2

    goto :goto_1
.end method

.method public static f(LX/1cr;I)Z
    .locals 1

    .prologue
    .line 283534
    iget v0, p0, LX/1cr;->j:I

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(FF)I
.end method

.method public a(Landroid/view/View;)LX/3t3;
    .locals 2

    .prologue
    .line 283500
    iget-object v0, p0, LX/1cr;->i:LX/3ta;

    if-nez v0, :cond_0

    .line 283501
    new-instance v0, LX/3ta;

    invoke-direct {v0, p0}, LX/3ta;-><init>(LX/1cr;)V

    iput-object v0, p0, LX/1cr;->i:LX/3ta;

    .line 283502
    :cond_0
    iget-object v0, p0, LX/1cr;->i:LX/3ta;

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 283503
    const/16 v0, 0x800

    invoke-virtual {p0, p1, v0}, LX/1cr;->a(II)Z

    .line 283504
    return-void
.end method

.method public abstract a(ILX/3sp;)V
.end method

.method public abstract a(ILandroid/view/accessibility/AccessibilityEvent;)V
.end method

.method public abstract a(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public a(II)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 283426
    const/high16 v1, -0x80000000

    if-eq p1, v1, :cond_0

    iget-object v1, p0, LX/1cr;->g:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 283427
    :cond_0
    :goto_0
    return v0

    .line 283428
    :cond_1
    iget-object v1, p0, LX/1cr;->h:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 283429
    if-eqz v1, :cond_0

    .line 283430
    packed-switch p1, :pswitch_data_0

    .line 283431
    invoke-static {p0, p1, p2}, LX/1cr;->d(LX/1cr;II)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    :goto_1
    move-object v0, v0

    .line 283432
    iget-object v2, p0, LX/1cr;->h:Landroid/view/View;

    .line 283433
    sget-object p0, LX/3B6;->a:LX/3B9;

    invoke-interface {p0, v1, v2, v0}, LX/3B9;->a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result p0

    move v0, p0

    .line 283434
    goto :goto_0

    .line 283435
    :pswitch_0
    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 283436
    iget-object p1, p0, LX/1cr;->h:Landroid/view/View;

    invoke-static {p1, v0}, LX/0vv;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 283437
    move-object v0, v0

    .line 283438
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/high16 v4, -0x80000000

    const/4 v1, 0x0

    .line 283439
    iget-object v2, p0, LX/1cr;->g:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/1cr;->g:Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v2}, LX/1d8;->b(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 283440
    :cond_1
    :goto_0
    return v0

    .line 283441
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    move v0, v1

    .line 283442
    goto :goto_0

    .line 283443
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p0, v2, v3}, LX/1cr;->a(FF)I

    move-result v2

    .line 283444
    invoke-direct {p0, v2}, LX/1cr;->b(I)V

    .line 283445
    if-ne v2, v4, :cond_1

    move v0, v1

    goto :goto_0

    .line 283446
    :pswitch_2
    iget v2, p0, LX/1cr;->j:I

    if-eq v2, v4, :cond_3

    .line 283447
    invoke-direct {p0, v4}, LX/1cr;->b(I)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 283448
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b()V
    .locals 1

    .prologue
    .line 283449
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, LX/1cr;->a(I)V

    .line 283450
    return-void
.end method

.method public abstract b(II)Z
.end method
