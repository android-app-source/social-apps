.class public LX/0ZR;
.super LX/0ZK;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ZK",
        "<",
        "Lcom/facebook/push/mqtt/service/response/MqttPushServiceClientFlightRecorderEvent;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0ZR;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 83295
    const/16 v0, 0x14

    invoke-direct {p0, v0}, LX/0ZK;-><init>(I)V

    .line 83296
    return-void
.end method

.method public static a(LX/0QB;)LX/0ZR;
    .locals 3

    .prologue
    .line 83297
    sget-object v0, LX/0ZR;->a:LX/0ZR;

    if-nez v0, :cond_1

    .line 83298
    const-class v1, LX/0ZR;

    monitor-enter v1

    .line 83299
    :try_start_0
    sget-object v0, LX/0ZR;->a:LX/0ZR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 83300
    if-eqz v2, :cond_0

    .line 83301
    :try_start_1
    new-instance v0, LX/0ZR;

    invoke-direct {v0}, LX/0ZR;-><init>()V

    .line 83302
    move-object v0, v0

    .line 83303
    sput-object v0, LX/0ZR;->a:LX/0ZR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83304
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 83305
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 83306
    :cond_1
    sget-object v0, LX/0ZR;->a:LX/0ZR;

    return-object v0

    .line 83307
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 83308
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
