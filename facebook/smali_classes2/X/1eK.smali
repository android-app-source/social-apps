.class public final enum LX/1eK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1eK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1eK;

.field public static final enum FEEDBACK:LX/1eK;

.field public static final enum MESSAGE:LX/1eK;

.field public static final enum SUBTITLE:LX/1eK;

.field public static final enum SUFFIX:LX/1eK;

.field public static final enum TITLE:LX/1eK;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 287965
    new-instance v0, LX/1eK;

    const-string v1, "TITLE"

    invoke-direct {v0, v1, v2}, LX/1eK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1eK;->TITLE:LX/1eK;

    .line 287966
    new-instance v0, LX/1eK;

    const-string v1, "MESSAGE"

    invoke-direct {v0, v1, v3}, LX/1eK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1eK;->MESSAGE:LX/1eK;

    .line 287967
    new-instance v0, LX/1eK;

    const-string v1, "FEEDBACK"

    invoke-direct {v0, v1, v4}, LX/1eK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1eK;->FEEDBACK:LX/1eK;

    .line 287968
    new-instance v0, LX/1eK;

    const-string v1, "SUFFIX"

    invoke-direct {v0, v1, v5}, LX/1eK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1eK;->SUFFIX:LX/1eK;

    .line 287969
    new-instance v0, LX/1eK;

    const-string v1, "SUBTITLE"

    invoke-direct {v0, v1, v6}, LX/1eK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1eK;->SUBTITLE:LX/1eK;

    .line 287970
    const/4 v0, 0x5

    new-array v0, v0, [LX/1eK;

    sget-object v1, LX/1eK;->TITLE:LX/1eK;

    aput-object v1, v0, v2

    sget-object v1, LX/1eK;->MESSAGE:LX/1eK;

    aput-object v1, v0, v3

    sget-object v1, LX/1eK;->FEEDBACK:LX/1eK;

    aput-object v1, v0, v4

    sget-object v1, LX/1eK;->SUFFIX:LX/1eK;

    aput-object v1, v0, v5

    sget-object v1, LX/1eK;->SUBTITLE:LX/1eK;

    aput-object v1, v0, v6

    sput-object v0, LX/1eK;->$VALUES:[LX/1eK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 287964
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1eK;
    .locals 1

    .prologue
    .line 287963
    const-class v0, LX/1eK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1eK;

    return-object v0
.end method

.method public static values()[LX/1eK;
    .locals 1

    .prologue
    .line 287962
    sget-object v0, LX/1eK;->$VALUES:[LX/1eK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1eK;

    return-object v0
.end method
