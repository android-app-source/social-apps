.class public LX/1Wg;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1Wh;


# instance fields
.field public final b:LX/1V7;

.field public final c:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 269352
    new-instance v0, LX/1Wh;

    invoke-direct {v0, v1, v1, v1}, LX/1Wh;-><init>(FFF)V

    sput-object v0, LX/1Wg;->a:LX/1Wh;

    return-void
.end method

.method public constructor <init>(LX/1V7;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 269348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 269349
    iput-object p1, p0, LX/1Wg;->b:LX/1V7;

    .line 269350
    iput-object p2, p0, LX/1Wg;->c:Landroid/content/res/Resources;

    .line 269351
    return-void
.end method

.method public static e(LX/1Wg;)F
    .locals 1

    .prologue
    .line 269347
    iget-object v0, p0, LX/1Wg;->b:LX/1V7;

    invoke-virtual {v0}, LX/1V7;->d()F

    move-result v0

    return v0
.end method

.method public static f(LX/1Wg;)F
    .locals 1

    .prologue
    .line 269346
    iget-object v0, p0, LX/1Wg;->b:LX/1V7;

    invoke-virtual {v0}, LX/1V7;->e()F

    move-result v0

    return v0
.end method

.method public static h(LX/1Wg;)F
    .locals 3

    .prologue
    .line 269345
    iget-object v0, p0, LX/1Wg;->c:Landroid/content/res/Resources;

    iget-object v1, p0, LX/1Wg;->c:Landroid/content/res/Resources;

    const v2, 0x7f0b00d8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-static {v0, v1}, LX/0tP;->b(Landroid/content/res/Resources;F)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public static k(LX/1Wg;)LX/1Wh;
    .locals 4

    .prologue
    .line 269293
    new-instance v0, LX/1Wh;

    .line 269294
    const/4 v1, 0x0

    move v1, v1

    .line 269295
    invoke-static {p0}, LX/1Wg;->e(LX/1Wg;)F

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LX/1Wh;-><init>(FFF)V

    return-object v0
.end method

.method public static l(LX/1Wg;)LX/1Wh;
    .locals 4

    .prologue
    .line 269340
    new-instance v0, LX/1Wh;

    .line 269341
    const/4 v1, 0x0

    move v1, v1

    .line 269342
    invoke-static {p0}, LX/1Wg;->e(LX/1Wg;)F

    move-result v2

    iget-object v3, p0, LX/1Wg;->b:LX/1V7;

    .line 269343
    invoke-virtual {v3}, LX/1V7;->c()F

    move-result p0

    move v3, p0

    .line 269344
    invoke-direct {v0, v1, v2, v3}, LX/1Wh;-><init>(FFF)V

    return-object v0
.end method

.method public static m(LX/1Wg;)LX/1Wh;
    .locals 4

    .prologue
    .line 269333
    new-instance v0, LX/1Wh;

    .line 269334
    const/high16 v1, 0x41400000    # 12.0f

    move v1, v1

    .line 269335
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {p0}, LX/1Wg;->e(LX/1Wg;)F

    move-result v3

    add-float/2addr v2, v3

    .line 269336
    const/high16 v3, 0x41200000    # 10.0f

    move p0, v3

    .line 269337
    move v3, p0

    .line 269338
    move v3, v3

    .line 269339
    invoke-direct {v0, v1, v2, v3}, LX/1Wh;-><init>(FFF)V

    return-object v0
.end method


# virtual methods
.method public c()LX/1Wh;
    .locals 4

    .prologue
    .line 269330
    new-instance v0, LX/1Wh;

    .line 269331
    const/4 v1, 0x0

    move v1, v1

    .line 269332
    invoke-static {p0}, LX/1Wg;->e(LX/1Wg;)F

    move-result v2

    invoke-static {p0}, LX/1Wg;->h(LX/1Wg;)F

    move-result v3

    invoke-direct {v0, v1, v2, v3}, LX/1Wh;-><init>(FFF)V

    return-object v0
.end method

.method public d()Ljava/util/EnumMap;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumMap",
            "<",
            "LX/1Wi;",
            "LX/1Wj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 269296
    iget-object v1, p0, LX/1Wg;->b:LX/1V7;

    .line 269297
    iget-object v2, v1, LX/1V7;->k:LX/0fO;

    invoke-virtual {v2}, LX/0fO;->b()Z

    move-result v2

    move v2, v2

    .line 269298
    new-instance v10, Ljava/util/EnumMap;

    const-class v1, LX/1Wi;

    invoke-direct {v10, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 269299
    sget-object v11, LX/1Wi;->TOP:LX/1Wi;

    new-instance v1, LX/1Wj;

    if-eqz v2, :cond_0

    const v2, 0x7f020a65

    :goto_0
    const v3, 0x7f0219e5

    invoke-virtual {p0}, LX/1Wg;->c()LX/1Wh;

    move-result-object v4

    const/4 v5, 0x0

    const v6, 0x7f0219e7

    sget-object v7, LX/1Wk;->NEWSFEED_SHADOW:LX/1Wk;

    sget-object v8, LX/1Wl;->VISIBLE:LX/1Wl;

    sget-object v9, LX/1Wl;->HIDDEN:LX/1Wl;

    invoke-direct/range {v1 .. v9}, LX/1Wj;-><init>(IILX/1Wh;IILX/1Wk;LX/1Wl;LX/1Wl;)V

    invoke-virtual {v10, v11, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269300
    sget-object v11, LX/1Wi;->SUB_STORY:LX/1Wi;

    new-instance v1, LX/1Wj;

    const v2, 0x7f020a70

    const v3, 0x7f0219e7

    .line 269301
    new-instance v4, LX/1Wh;

    .line 269302
    const/high16 v5, 0x41400000    # 12.0f

    move v5, v5

    .line 269303
    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {p0}, LX/1Wg;->e(LX/1Wg;)F

    move-result v7

    add-float/2addr v6, v7

    invoke-static {p0}, LX/1Wg;->f(LX/1Wg;)F

    move-result v7

    invoke-direct {v4, v5, v6, v7}, LX/1Wh;-><init>(FFF)V

    move-object v4, v4

    .line 269304
    const v5, 0x7f020a76

    const/4 v6, -0x1

    sget-object v7, LX/1Wk;->SUBSTORY_SHADOW:LX/1Wk;

    sget-object v8, LX/1Wl;->HIDDEN:LX/1Wl;

    sget-object v9, LX/1Wl;->HIDDEN:LX/1Wl;

    invoke-direct/range {v1 .. v9}, LX/1Wj;-><init>(IILX/1Wh;IILX/1Wk;LX/1Wl;LX/1Wl;)V

    invoke-virtual {v10, v11, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269305
    sget-object v11, LX/1Wi;->LAST_SUB_STORY:LX/1Wi;

    new-instance v1, LX/1Wj;

    const v2, 0x7f020a70

    const v3, 0x7f0219e7

    invoke-static {p0}, LX/1Wg;->m(LX/1Wg;)LX/1Wh;

    move-result-object v4

    const v5, 0x7f020a76

    const/4 v6, -0x1

    sget-object v7, LX/1Wk;->SUBSTORY_SHADOW:LX/1Wk;

    sget-object v8, LX/1Wl;->HIDDEN:LX/1Wl;

    sget-object v9, LX/1Wl;->HIDDEN:LX/1Wl;

    invoke-direct/range {v1 .. v9}, LX/1Wj;-><init>(IILX/1Wh;IILX/1Wk;LX/1Wl;LX/1Wl;)V

    invoke-virtual {v10, v11, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269306
    sget-object v11, LX/1Wi;->PERMALINK:LX/1Wi;

    new-instance v1, LX/1Wj;

    const v2, 0x7f020a70

    const v3, 0x7f0219e7

    .line 269307
    new-instance v4, LX/1Wh;

    .line 269308
    const/4 v5, 0x0

    move v5, v5

    .line 269309
    invoke-static {p0}, LX/1Wg;->e(LX/1Wg;)F

    move-result v6

    invoke-static {p0}, LX/1Wg;->f(LX/1Wg;)F

    move-result v7

    invoke-direct {v4, v5, v6, v7}, LX/1Wh;-><init>(FFF)V

    move-object v4, v4

    .line 269310
    const/4 v5, 0x0

    const v6, 0x7f0219e7

    sget-object v7, LX/1Wk;->NEWSFEED_SHADOW:LX/1Wk;

    sget-object v8, LX/1Wl;->VISIBLE:LX/1Wl;

    sget-object v9, LX/1Wl;->VISIBLE:LX/1Wl;

    invoke-direct/range {v1 .. v9}, LX/1Wj;-><init>(IILX/1Wh;IILX/1Wk;LX/1Wl;LX/1Wl;)V

    invoke-virtual {v10, v11, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269311
    sget-object v11, LX/1Wi;->HAS_INLINE_COMMENTS:LX/1Wi;

    new-instance v1, LX/1Wj;

    const v2, 0x7f020a70

    const v3, 0x7f0219e7

    invoke-static {p0}, LX/1Wg;->k(LX/1Wg;)LX/1Wh;

    move-result-object v4

    const/4 v5, 0x0

    const v6, 0x7f0219e7

    sget-object v7, LX/1Wk;->NEWSFEED_SHADOW:LX/1Wk;

    sget-object v8, LX/1Wl;->VISIBLE:LX/1Wl;

    sget-object v9, LX/1Wl;->VISIBLE:LX/1Wl;

    invoke-direct/range {v1 .. v9}, LX/1Wj;-><init>(IILX/1Wh;IILX/1Wk;LX/1Wl;LX/1Wl;)V

    invoke-virtual {v10, v11, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269312
    sget-object v11, LX/1Wi;->HAS_INLINE_SURVEY:LX/1Wi;

    new-instance v1, LX/1Wj;

    const v2, 0x7f020a70

    const v3, 0x7f0219e7

    invoke-static {p0}, LX/1Wg;->k(LX/1Wg;)LX/1Wh;

    move-result-object v4

    const/4 v5, 0x0

    const v6, 0x7f0219e7

    sget-object v7, LX/1Wk;->NEWSFEED_SHADOW:LX/1Wk;

    sget-object v8, LX/1Wl;->HIDDEN:LX/1Wl;

    sget-object v9, LX/1Wl;->HIDDEN:LX/1Wl;

    invoke-direct/range {v1 .. v9}, LX/1Wj;-><init>(IILX/1Wh;IILX/1Wk;LX/1Wl;LX/1Wl;)V

    invoke-virtual {v10, v11, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269313
    sget-object v11, LX/1Wi;->HAS_FOLLOWUP_SECTION:LX/1Wi;

    new-instance v1, LX/1Wj;

    const v2, 0x7f020a70

    const v3, 0x7f0219e7

    invoke-static {p0}, LX/1Wg;->l(LX/1Wg;)LX/1Wh;

    move-result-object v4

    const/4 v5, 0x0

    const v6, 0x7f0219e7

    sget-object v7, LX/1Wk;->NEWSFEED_SHADOW:LX/1Wk;

    sget-object v8, LX/1Wl;->VISIBLE:LX/1Wl;

    sget-object v9, LX/1Wl;->HIDDEN:LX/1Wl;

    invoke-direct/range {v1 .. v9}, LX/1Wj;-><init>(IILX/1Wh;IILX/1Wk;LX/1Wl;LX/1Wl;)V

    invoke-virtual {v10, v11, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269314
    sget-object v11, LX/1Wi;->PAGE:LX/1Wi;

    new-instance v1, LX/1Wj;

    const/4 v2, 0x0

    const/4 v3, -0x1

    sget-object v4, LX/1Wg;->a:LX/1Wh;

    const v5, 0x7f020a70

    const/4 v6, -0x1

    sget-object v7, LX/1Wk;->NEWSFEED_SHADOW:LX/1Wk;

    sget-object v8, LX/1Wl;->VISIBLE:LX/1Wl;

    sget-object v9, LX/1Wl;->HIDDEN:LX/1Wl;

    invoke-direct/range {v1 .. v9}, LX/1Wj;-><init>(IILX/1Wh;IILX/1Wk;LX/1Wl;LX/1Wl;)V

    invoke-virtual {v10, v11, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269315
    sget-object v11, LX/1Wi;->PHOTOS_FEED:LX/1Wi;

    new-instance v1, LX/1Wj;

    const v2, 0x7f020a64

    const v3, 0x7f0219e5

    .line 269316
    new-instance v4, LX/1Wh;

    .line 269317
    const/4 v5, 0x0

    move v5, v5

    .line 269318
    const/4 v6, 0x0

    invoke-static {p0}, LX/1Wg;->h(LX/1Wg;)F

    move-result v7

    invoke-direct {v4, v5, v6, v7}, LX/1Wh;-><init>(FFF)V

    move-object v4, v4

    .line 269319
    const/4 v5, 0x0

    const v6, 0x7f0219e7

    sget-object v7, LX/1Wk;->NEWSFEED_SHADOW:LX/1Wk;

    sget-object v8, LX/1Wl;->VISIBLE:LX/1Wl;

    sget-object v9, LX/1Wl;->HIDDEN:LX/1Wl;

    invoke-direct/range {v1 .. v9}, LX/1Wj;-><init>(IILX/1Wh;IILX/1Wk;LX/1Wl;LX/1Wl;)V

    invoke-virtual {v10, v11, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269320
    sget-object v11, LX/1Wi;->SUB_STORY_BOX_WITH_COMMENTS:LX/1Wi;

    new-instance v1, LX/1Wj;

    const v2, 0x7f020a68

    const v3, 0x7f0219e7

    const/high16 v7, 0x3f800000    # 1.0f

    .line 269321
    new-instance v4, LX/1Wh;

    .line 269322
    const/high16 v5, 0x41400000    # 12.0f

    move v5, v5

    .line 269323
    invoke-static {p0}, LX/1Wg;->e(LX/1Wg;)F

    move-result v6

    add-float/2addr v6, v7

    invoke-direct {v4, v5, v6, v7}, LX/1Wh;-><init>(FFF)V

    move-object v4, v4

    .line 269324
    const/4 v5, 0x0

    const/4 v6, -0x1

    sget-object v7, LX/1Wk;->SUBSTORY_SHADOW:LX/1Wk;

    sget-object v8, LX/1Wl;->VISIBLE:LX/1Wl;

    sget-object v9, LX/1Wl;->HIDDEN:LX/1Wl;

    invoke-direct/range {v1 .. v9}, LX/1Wj;-><init>(IILX/1Wh;IILX/1Wk;LX/1Wl;LX/1Wl;)V

    invoke-virtual {v10, v11, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269325
    sget-object v11, LX/1Wi;->SUB_STORY_BOX_WITHOUT_COMMENTS:LX/1Wi;

    new-instance v1, LX/1Wj;

    const v2, 0x7f020a69

    const v3, 0x7f0219e7

    invoke-static {p0}, LX/1Wg;->m(LX/1Wg;)LX/1Wh;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, -0x1

    sget-object v7, LX/1Wk;->SUBSTORY_SHADOW:LX/1Wk;

    sget-object v8, LX/1Wl;->VISIBLE:LX/1Wl;

    sget-object v9, LX/1Wl;->HIDDEN:LX/1Wl;

    invoke-direct/range {v1 .. v9}, LX/1Wj;-><init>(IILX/1Wh;IILX/1Wk;LX/1Wl;LX/1Wl;)V

    invoke-virtual {v10, v11, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269326
    sget-object v11, LX/1Wi;->COMPACT_STORIES:LX/1Wi;

    new-instance v1, LX/1Wj;

    const v2, 0x7f020a70

    const v3, 0x7f0219e7

    invoke-static {p0}, LX/1Wg;->k(LX/1Wg;)LX/1Wh;

    move-result-object v4

    const/4 v5, 0x0

    const v6, 0x7f0219e7

    sget-object v7, LX/1Wk;->NEWSFEED_SHADOW:LX/1Wk;

    sget-object v8, LX/1Wl;->VISIBLE:LX/1Wl;

    sget-object v9, LX/1Wl;->HIDDEN:LX/1Wl;

    invoke-direct/range {v1 .. v9}, LX/1Wj;-><init>(IILX/1Wh;IILX/1Wk;LX/1Wl;LX/1Wl;)V

    invoke-virtual {v10, v11, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269327
    move-object v0, v10

    .line 269328
    return-object v0

    .line 269329
    :cond_0
    const v2, 0x7f020a64

    goto/16 :goto_0
.end method
