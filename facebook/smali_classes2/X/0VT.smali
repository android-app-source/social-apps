.class public LX/0VT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/0VT;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:Landroid/app/ActivityManager;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67812
    const-class v0, LX/0VT;

    sput-object v0, LX/0VT;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/ActivityManager;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/common/process/MyProcessId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/app/ActivityManager;",
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 67834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67835
    iput-object p1, p0, LX/0VT;->b:Landroid/content/Context;

    .line 67836
    iput-object p2, p0, LX/0VT;->c:Landroid/app/ActivityManager;

    .line 67837
    iput-object p3, p0, LX/0VT;->d:LX/0Or;

    .line 67838
    return-void
.end method

.method public static a(LX/0QB;)LX/0VT;
    .locals 6

    .prologue
    .line 67821
    sget-object v0, LX/0VT;->e:LX/0VT;

    if-nez v0, :cond_1

    .line 67822
    const-class v1, LX/0VT;

    monitor-enter v1

    .line 67823
    :try_start_0
    sget-object v0, LX/0VT;->e:LX/0VT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 67824
    if-eqz v2, :cond_0

    .line 67825
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 67826
    new-instance v5, LX/0VT;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0VU;->b(LX/0QB;)Landroid/app/ActivityManager;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager;

    const/16 p0, 0x15ce

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/0VT;-><init>(Landroid/content/Context;Landroid/app/ActivityManager;LX/0Or;)V

    .line 67827
    move-object v0, v5

    .line 67828
    sput-object v0, LX/0VT;->e:LX/0VT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67829
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 67830
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 67831
    :cond_1
    sget-object v0, LX/0VT;->e:LX/0VT;

    return-object v0

    .line 67832
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 67833
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/00G;
    .locals 1

    .prologue
    .line 67820
    invoke-static {}, LX/00G;->g()LX/00G;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 67813
    invoke-virtual {p0}, LX/0VT;->a()LX/00G;

    move-result-object v0

    .line 67814
    if-nez v0, :cond_0

    .line 67815
    sget-object v0, LX/0VT;->a:Ljava/lang/Class;

    const-string v1, "Couldn\'t find own process name"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 67816
    const/4 v0, 0x0

    .line 67817
    :goto_0
    return v0

    .line 67818
    :cond_0
    iget-object v1, v0, LX/00G;->b:Ljava/lang/String;

    move-object v0, v1

    .line 67819
    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
