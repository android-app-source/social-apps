.class public LX/1MH;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 235129
    const-class v0, LX/1MH;

    sput-object v0, LX/1MH;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 235130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235131
    invoke-static {p1}, LX/0WH;->b(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/1MH;->b:Landroid/content/Context;

    .line 235132
    return-void
.end method

.method public static b(LX/0QB;)LX/1MH;
    .locals 2

    .prologue
    .line 235133
    new-instance v1, LX/1MH;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/1MH;-><init>(Landroid/content/Context;)V

    .line 235134
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/content/ServiceConnection;)V
    .locals 2

    .prologue
    .line 235135
    iget-object v0, p0, LX/1MH;->b:Landroid/content/Context;

    const v1, -0x305298e4

    invoke-static {v0, p1, v1}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V

    .line 235136
    return-void
.end method

.method public final a(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    .locals 3

    .prologue
    .line 235137
    iget-object v0, p0, LX/1MH;->b:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 235138
    const-string v0, "BindService(%s)"

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    const v2, -0x2ec4d64d

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 235139
    :try_start_0
    iget-object v0, p0, LX/1MH;->b:Landroid/content/Context;

    const v1, -0x172334b0

    invoke-static {v0, p1, p2, p3, v1}, LX/04O;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 235140
    const v1, 0x3d0c53c0

    invoke-static {v1}, LX/02m;->a(I)V

    return v0

    :catchall_0
    move-exception v0

    const v1, -0x1a5a2f1d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
