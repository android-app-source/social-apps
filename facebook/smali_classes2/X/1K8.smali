.class public LX/1K8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Ljava/lang/Object;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 231008
    const-class v0, LX/1K8;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1K8;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 231002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231003
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/1K8;->b:Ljava/lang/Object;

    .line 231004
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1K8;->c:Ljava/util/Map;

    .line 231005
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1K8;->d:Ljava/util/List;

    .line 231006
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1K8;->e:Ljava/util/List;

    .line 231007
    return-void
.end method

.method public static a(LX/0QB;)LX/1K8;
    .locals 3

    .prologue
    .line 230991
    const-class v1, LX/1K8;

    monitor-enter v1

    .line 230992
    :try_start_0
    sget-object v0, LX/1K8;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 230993
    sput-object v2, LX/1K8;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 230994
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230995
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 230996
    new-instance v0, LX/1K8;

    invoke-direct {v0}, LX/1K8;-><init>()V

    .line 230997
    move-object v0, v0

    .line 230998
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 230999
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1K8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 231000
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 231001
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/1K8;LX/1KL;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 230988
    iget-object v1, p0, LX/1K8;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 230989
    :try_start_0
    iget-object v0, p0, LX/1K8;->c:Ljava/util/Map;

    invoke-interface {p1}, LX/1KL;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 230990
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static b(LX/1K8;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation

    .prologue
    .line 230978
    iget-object v0, p0, LX/1K8;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 230979
    iget-object v0, p0, LX/1K8;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 230980
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 230981
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 230982
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 230983
    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230984
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 230985
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 230986
    iget-object v0, p0, LX/1K8;->c:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 230987
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(LX/1KL;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 230969
    invoke-static {p0, p1}, LX/1K8;->b(LX/1K8;LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    .line 230970
    if-eqz v0, :cond_0

    .line 230971
    :goto_0
    return-object v0

    .line 230972
    :cond_0
    invoke-interface {p1}, LX/1KL;->a()Ljava/lang/Object;

    move-result-object v0

    .line 230973
    iget-object v2, p0, LX/1K8;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 230974
    :try_start_0
    invoke-static {p0, p1}, LX/1K8;->b(LX/1K8;LX/1KL;)Ljava/lang/Object;

    move-result-object v1

    .line 230975
    if-nez v1, :cond_1

    .line 230976
    iget-object v1, p0, LX/1K8;->c:Ljava/util/Map;

    invoke-interface {p1}, LX/1KL;->b()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230977
    :goto_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(LX/1KL;LX/0jW;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;",
            "LX/0jW;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 230958
    invoke-static {p0, p1}, LX/1K8;->b(LX/1K8;LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    .line 230959
    if-eqz v0, :cond_0

    .line 230960
    :goto_0
    return-object v0

    .line 230961
    :cond_0
    invoke-interface {p1}, LX/1KL;->a()Ljava/lang/Object;

    move-result-object v0

    .line 230962
    iget-object v2, p0, LX/1K8;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 230963
    :try_start_0
    invoke-static {p0, p1}, LX/1K8;->b(LX/1K8;LX/1KL;)Ljava/lang/Object;

    move-result-object v1

    .line 230964
    if-nez v1, :cond_1

    .line 230965
    iget-object v1, p0, LX/1K8;->c:Ljava/util/Map;

    invoke-interface {p1}, LX/1KL;->b()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230966
    iget-object v1, p0, LX/1K8;->d:Ljava/util/List;

    invoke-interface {p2}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230967
    iget-object v1, p0, LX/1K8;->e:Ljava/util/List;

    invoke-interface {p1}, LX/1KL;->b()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230968
    :goto_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 230935
    iget-object v1, p0, LX/1K8;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 230936
    :try_start_0
    iget-object v0, p0, LX/1K8;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 230937
    iget-object v3, p0, LX/1K8;->c:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 230938
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 230939
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1K8;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 230940
    iget-object v0, p0, LX/1K8;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 230941
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 230953
    if-nez p1, :cond_0

    .line 230954
    :goto_0
    return-void

    .line 230955
    :cond_0
    iget-object v1, p0, LX/1K8;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 230956
    :try_start_0
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, LX/1K8;->b(LX/1K8;Ljava/util/List;)V

    .line 230957
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 230950
    iget-object v1, p0, LX/1K8;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 230951
    :try_start_0
    invoke-static {p0, p1}, LX/1K8;->b(LX/1K8;Ljava/util/List;)V

    .line 230952
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/1KL;Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;TT;)Z"
        }
    .end annotation

    .prologue
    .line 230942
    iget-object v1, p0, LX/1K8;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 230943
    :try_start_0
    invoke-static {p0, p1}, LX/1K8;->b(LX/1K8;LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    .line 230944
    if-nez v0, :cond_0

    .line 230945
    const/4 v0, 0x0

    monitor-exit v1

    .line 230946
    :goto_0
    return v0

    .line 230947
    :cond_0
    iget-object v0, p0, LX/1K8;->c:Ljava/util/Map;

    invoke-interface {p1}, LX/1KL;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230948
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0

    .line 230949
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
