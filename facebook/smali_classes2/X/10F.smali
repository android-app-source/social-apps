.class public final LX/10F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0nD;


# static fields
.field private static a:LX/10F;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 168562
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a()LX/10F;
    .locals 2

    .prologue
    .line 168563
    const-class v1, LX/10F;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/10F;->a:LX/10F;

    if-nez v0, :cond_0

    .line 168564
    new-instance v0, LX/10F;

    invoke-direct {v0}, LX/10F;-><init>()V

    sput-object v0, LX/10F;->a:LX/10F;

    .line 168565
    :cond_0
    sget-object v0, LX/10F;->a:LX/10F;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 168566
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/io/Writer;LX/0nA;)V
    .locals 5

    .prologue
    .line 168543
    new-instance v2, LX/10G;

    invoke-direct {v2, p1}, LX/10G;-><init>(Ljava/io/Writer;)V

    .line 168544
    check-cast p2, LX/0n9;

    .line 168545
    iget v0, p2, LX/0n9;->c:I

    move v3, v0

    .line 168546
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_5

    .line 168547
    if-lez v1, :cond_0

    .line 168548
    const/16 v0, 0x26

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(I)V

    .line 168549
    :cond_0
    invoke-virtual {p2, v1}, LX/0n9;->b(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 168550
    invoke-virtual {p2, v1}, LX/0n9;->c(I)Ljava/lang/Object;

    move-result-object v0

    .line 168551
    if-eqz v0, :cond_1

    .line 168552
    const/16 v4, 0x3d

    invoke-virtual {p1, v4}, Ljava/io/Writer;->write(I)V

    .line 168553
    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 168554
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 168555
    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 168556
    :cond_2
    instance-of v4, v0, Ljava/lang/Number;

    if-eqz v4, :cond_3

    .line 168557
    invoke-static {}, LX/10I;->a()LX/10I;

    move-result-object v4

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v4, v2, v0}, LX/10I;->a(Ljava/io/Writer;Ljava/lang/Number;)V

    goto :goto_1

    .line 168558
    :cond_3
    instance-of v4, v0, LX/0nA;

    if-eqz v4, :cond_4

    .line 168559
    check-cast v0, LX/0nA;

    invoke-virtual {v0, v2, p0}, LX/0nA;->a(Ljava/io/Writer;LX/0nD;)V

    goto :goto_1

    .line 168560
    :cond_4
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "The type of \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v1}, LX/0n9;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is not allowed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 168561
    :cond_5
    return-void
.end method
