.class public final LX/0Ur;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0Up;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0Up;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 67090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67091
    iput-object p1, p0, LX/0Ur;->a:LX/0QB;

    .line 67092
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 67093
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0Ur;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 67094
    packed-switch p2, :pswitch_data_0

    .line 67095
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67096
    :pswitch_0
    new-instance p0, LX/Hm7;

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p1}, LX/Hma;->a(LX/0QB;)LX/Hma;

    move-result-object v1

    check-cast v1, LX/Hma;

    invoke-direct {p0, v0, v1}, LX/Hm7;-><init>(LX/0Uh;LX/Hma;)V

    .line 67097
    const/16 v0, 0x17fb

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    .line 67098
    iput-object v0, p0, LX/Hm7;->c:LX/0Ot;

    .line 67099
    move-object v0, p0

    .line 67100
    :goto_0
    return-object v0

    .line 67101
    :pswitch_1
    new-instance v1, LX/HnO;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const/16 p0, 0xbc

    invoke-static {p1, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    const/16 p2, 0x190c

    invoke-static {p1, p2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p2

    invoke-direct {v1, v0, p0, p2}, LX/HnO;-><init>(Landroid/content/Context;LX/0Or;LX/0Ot;)V

    .line 67102
    move-object v0, v1

    .line 67103
    goto :goto_0

    .line 67104
    :pswitch_2
    invoke-static {p1}, LX/28o;->a(LX/0QB;)LX/28o;

    move-result-object v0

    goto :goto_0

    .line 67105
    :pswitch_3
    invoke-static {p1}, LX/2Ih;->a(LX/0QB;)LX/2Ih;

    move-result-object v0

    goto :goto_0

    .line 67106
    :pswitch_4
    invoke-static {p1}, LX/Gaw;->b(LX/0QB;)LX/Gaw;

    move-result-object v0

    goto :goto_0

    .line 67107
    :pswitch_5
    invoke-static {p1}, LX/2bl;->a(LX/0QB;)LX/2bl;

    move-result-object v0

    goto :goto_0

    .line 67108
    :pswitch_6
    invoke-static {p1}, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;->a(LX/0QB;)Lcom/facebook/gk/internal/OnUpgradeGkRefresher;

    move-result-object v0

    goto :goto_0

    .line 67109
    :pswitch_7
    new-instance v0, LX/6Y0;

    const/16 v1, 0xbd2

    invoke-static {p1, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6Y0;-><init>(LX/0Ot;)V

    .line 67110
    move-object v0, v0

    .line 67111
    goto :goto_0

    .line 67112
    :pswitch_8
    invoke-static {p1}, LX/28Q;->b(LX/0QB;)LX/28Q;

    move-result-object v0

    goto :goto_0

    .line 67113
    :pswitch_9
    invoke-static {p1}, LX/2bk;->a(LX/0QB;)LX/2bk;

    move-result-object v0

    goto :goto_0

    .line 67114
    :pswitch_a
    new-instance v0, LX/GzJ;

    invoke-direct {v0}, LX/GzJ;-><init>()V

    .line 67115
    const-class v1, Landroid/content/Context;

    invoke-interface {p1, v1}, LX/0QC;->getProvider(Ljava/lang/Class;)LX/0Or;

    move-result-object v1

    .line 67116
    iput-object v1, v0, LX/GzJ;->a:LX/0Or;

    .line 67117
    move-object v0, v0

    .line 67118
    goto :goto_0

    .line 67119
    :pswitch_b
    new-instance v2, LX/6dD;

    invoke-static {p1}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v3

    check-cast v3, LX/01T;

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v5, 0x274b

    invoke-static {p1, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p1}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v6

    check-cast v6, LX/0WJ;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-direct/range {v2 .. v7}, LX/6dD;-><init>(LX/01T;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0WJ;LX/03V;)V

    .line 67120
    move-object v0, v2

    .line 67121
    goto :goto_0

    .line 67122
    :pswitch_c
    new-instance v2, LX/Hby;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v2, v0, v1}, LX/Hby;-><init>(LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 67123
    move-object v0, v2

    .line 67124
    goto/16 :goto_0

    .line 67125
    :pswitch_d
    new-instance v1, LX/G5r;

    invoke-static {p1}, LX/0oT;->a(LX/0QB;)LX/00u;

    move-result-object v0

    check-cast v0, LX/00u;

    invoke-direct {v1, v0}, LX/G5r;-><init>(LX/00u;)V

    .line 67126
    move-object v0, v1

    .line 67127
    goto/16 :goto_0

    .line 67128
    :pswitch_e
    new-instance v2, LX/KA9;

    invoke-static {p1}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageManager;

    invoke-static {p1}, LX/0dF;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {p1}, LX/KA5;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v5

    invoke-static {p1}, LX/KA6;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v6

    invoke-static {p1}, LX/KA7;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, LX/KA9;-><init>(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    .line 67129
    move-object v0, v2

    .line 67130
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 67131
    const/16 v0, 0xf

    return v0
.end method
