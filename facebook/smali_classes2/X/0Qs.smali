.class public final enum LX/0Qs;
.super LX/0Qo;
.source ""


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 58529
    invoke-direct {p0, p1, p2}, LX/0Qo;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final copyEntry(LX/0Qx;LX/0R1;LX/0R1;)LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Qx",
            "<TK;TV;>;",
            "LX/0R1",
            "<TK;TV;>;",
            "LX/0R1",
            "<TK;TV;>;)",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 58530
    invoke-super {p0, p1, p2, p3}, LX/0Qo;->copyEntry(LX/0Qx;LX/0R1;LX/0R1;)LX/0R1;

    move-result-object v0

    .line 58531
    invoke-virtual {p0, p2, v0}, LX/0Qo;->copyAccessEntry(LX/0R1;LX/0R1;)V

    .line 58532
    invoke-virtual {p0, p2, v0}, LX/0Qo;->copyWriteEntry(LX/0R1;LX/0R1;)V

    .line 58533
    return-object v0
.end method

.method public final newEntry(LX/0Qx;Ljava/lang/Object;ILX/0R1;)LX/0R1;
    .locals 1
    .param p4    # LX/0R1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Qx",
            "<TK;TV;>;TK;I",
            "LX/0R1",
            "<TK;TV;>;)",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 58534
    new-instance v0, LX/4wP;

    invoke-direct {v0, p2, p3, p4}, LX/4wP;-><init>(Ljava/lang/Object;ILX/0R1;)V

    return-object v0
.end method
