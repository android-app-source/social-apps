.class public LX/1tP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1tP;


# instance fields
.field private final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 336474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336475
    iput-object p1, p0, LX/1tP;->a:LX/0Uh;

    .line 336476
    return-void
.end method

.method public static a(LX/0QB;)LX/1tP;
    .locals 4

    .prologue
    .line 336460
    sget-object v0, LX/1tP;->b:LX/1tP;

    if-nez v0, :cond_1

    .line 336461
    const-class v1, LX/1tP;

    monitor-enter v1

    .line 336462
    :try_start_0
    sget-object v0, LX/1tP;->b:LX/1tP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 336463
    if-eqz v2, :cond_0

    .line 336464
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 336465
    new-instance p0, LX/1tP;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/1tP;-><init>(LX/0Uh;)V

    .line 336466
    move-object v0, p0

    .line 336467
    sput-object v0, LX/1tP;->b:LX/1tP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 336468
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 336469
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 336470
    :cond_1
    sget-object v0, LX/1tP;->b:LX/1tP;

    return-object v0

    .line 336471
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 336472
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 336473
    iget-object v0, p0, LX/1tP;->a:LX/0Uh;

    const/16 v1, 0x191

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
