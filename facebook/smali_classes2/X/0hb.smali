.class public final LX/0hb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hc;


# instance fields
.field public a:I

.field public b:Z

.field public c:Z

.field public d:I

.field public final synthetic e:Landroid/app/Activity;

.field public final synthetic f:LX/0gg;


# direct methods
.method public constructor <init>(LX/0gg;Landroid/app/Activity;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 117854
    iput-object p1, p0, LX/0hb;->f:LX/0gg;

    iput-object p2, p0, LX/0hb;->e:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117855
    const/4 v0, 0x0

    iput v0, p0, LX/0hb;->a:I

    .line 117856
    iput-boolean v1, p0, LX/0hb;->b:Z

    .line 117857
    iput-boolean v1, p0, LX/0hb;->c:Z

    .line 117858
    iget-object v0, p0, LX/0hb;->f:LX/0gg;

    iget-object v0, v0, LX/0gg;->p:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iput v0, p0, LX/0hb;->d:I

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 117879
    iget-object v0, p0, LX/0hb;->f:LX/0gg;

    iget-object v0, v0, LX/0gg;->o:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->f()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/TabTag;

    .line 117880
    iput-boolean v3, p0, LX/0hb;->b:Z

    .line 117881
    iput p1, p0, LX/0hb;->a:I

    .line 117882
    iget v1, p0, LX/0hb;->a:I

    iget v4, p0, LX/0hb;->d:I

    if-le v1, v4, :cond_a

    move v1, v2

    :goto_0
    iput-boolean v1, p0, LX/0hb;->c:Z

    .line 117883
    iget-object v1, v0, Lcom/facebook/apptab/state/TabTag;->h:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 117884
    iget-object v1, p0, LX/0hb;->f:LX/0gg;

    iget-object v1, v1, LX/0gg;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v4, LX/0Yj;

    iget v5, v0, Lcom/facebook/apptab/state/TabTag;->f:I

    iget-object v6, v0, Lcom/facebook/apptab/state/TabTag;->h:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v5, v2, [Ljava/lang/String;

    iget-object v6, v0, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    aput-object v6, v5, v3

    invoke-virtual {v4, v5}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v4

    .line 117885
    iput-boolean v2, v4, LX/0Yj;->n:Z

    .line 117886
    move-object v4, v4

    .line 117887
    invoke-interface {v1, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 117888
    :cond_0
    iget-object v1, p0, LX/0hb;->f:LX/0gg;

    iget-object v1, v1, LX/0gg;->o:LX/0fd;

    sget-object v4, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 117889
    iget-object v5, v1, LX/0fd;->b:LX/0fG;

    invoke-interface {v5, v4}, LX/0fG;->c(Z)V

    .line 117890
    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v1

    .line 117891
    instance-of v4, v0, Lcom/facebook/notifications/tab/NotificationsTab;

    if-eqz v4, :cond_1

    .line 117892
    iget-object v4, p0, LX/0hb;->f:LX/0gg;

    const/4 v8, 0x0

    .line 117893
    iget-object v5, v4, LX/0gg;->d:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 117894
    iget-object v6, v4, LX/0gg;->l:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0Uh;

    const/16 v7, 0x496

    invoke-virtual {v6, v7, v8}, LX/0Uh;->a(IZ)Z

    move-result v6

    if-eqz v6, :cond_1

    sget-object v6, LX/B6A;->b:LX/0Tn;

    invoke-interface {v5, v6, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v6

    if-nez v6, :cond_1

    .line 117895
    iget-object v6, v4, LX/0gg;->c:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0gt;

    .line 117896
    const-string v7, "407346402789425"

    .line 117897
    iput-object v7, v6, LX/0gt;->a:Ljava/lang/String;

    .line 117898
    move-object v6, v6

    .line 117899
    iget-object v7, v4, LX/0gg;->f:Landroid/content/Context;

    invoke-virtual {v6, v7}, LX/0gt;->a(Landroid/content/Context;)V

    .line 117900
    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    sget-object v6, LX/B6A;->b:LX/0Tn;

    const/4 v7, 0x1

    invoke-interface {v5, v6, v7}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v5

    invoke-interface {v5}, LX/0hN;->commit()V

    .line 117901
    :cond_1
    iget-object v4, p0, LX/0hb;->f:LX/0gg;

    iget-object v4, v4, LX/0gg;->o:LX/0fd;

    invoke-virtual {v4, v1}, LX/0fd;->c(Ljava/lang/String;)LX/0hO;

    move-result-object v4

    .line 117902
    if-eqz v4, :cond_2

    .line 117903
    iget-object v1, p0, LX/0hb;->f:LX/0gg;

    iget-object v1, v1, LX/0gg;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wL;

    .line 117904
    const/4 v5, 0x0

    invoke-static {v1, v4, v5}, LX/0wL;->b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 117905
    :cond_2
    iget-object v1, p0, LX/0hb;->f:LX/0gg;

    iget-object v1, v1, LX/0gg;->o:LX/0fd;

    invoke-virtual {v1}, LX/0fd;->f()LX/0Px;

    move-result-object v1

    iget v4, p0, LX/0hb;->d:I

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/apptab/state/TabTag;

    .line 117906
    iget-object v4, p0, LX/0hb;->f:LX/0gg;

    invoke-virtual {v1}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0gg;->a(Ljava/lang/String;)Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v1

    .line 117907
    if-eqz v1, :cond_3

    .line 117908
    invoke-virtual {v1, v3}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 117909
    :cond_3
    iget-object v1, p0, LX/0hb;->f:LX/0gg;

    invoke-virtual {v1}, LX/0gg;->b()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v4

    .line 117910
    if-eqz v4, :cond_4

    .line 117911
    invoke-virtual {v4, v2}, Lcom/facebook/katana/fragment/FbChromeFragment;->a(Z)V

    .line 117912
    invoke-virtual {v4}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 117913
    instance-of v2, v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;

    if-eqz v2, :cond_b

    .line 117914
    check-cast v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;

    .line 117915
    iget-object v2, p0, LX/0hb;->f:LX/0gg;

    iget-object v2, v2, LX/0gg;->o:LX/0fd;

    .line 117916
    iget-object v5, v2, LX/0fd;->a:LX/0fF;

    invoke-interface {v5, v1}, LX/0fF;->a(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    .line 117917
    :cond_4
    :goto_1
    iget-object v1, p0, LX/0hb;->f:LX/0gg;

    iget-object v2, p0, LX/0hb;->e:Landroid/app/Activity;

    invoke-static {v1, v2, p1}, LX/0gg;->a$redex0(LX/0gg;Landroid/app/Activity;I)V

    .line 117918
    iput p1, p0, LX/0hb;->d:I

    .line 117919
    iget-object v1, p0, LX/0hb;->f:LX/0gg;

    iget-object v1, v1, LX/0gg;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0fU;

    sget-object v2, LX/0ax;->cL:Ljava/lang/String;

    .line 117920
    iput-object v2, v1, LX/0fU;->n:Ljava/lang/String;

    .line 117921
    iget-object v1, p0, LX/0hb;->f:LX/0gg;

    iget v1, v1, LX/0gg;->v:I

    if-eqz v1, :cond_5

    iget-object v1, p0, LX/0hb;->f:LX/0gg;

    iget v1, v1, LX/0gg;->v:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    iget-object v1, p0, LX/0hb;->f:LX/0gg;

    iget-object v1, v1, LX/0gg;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const/16 v2, 0x4b3

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 117922
    :cond_5
    iget-object v1, p0, LX/0hb;->f:LX/0gg;

    iget-object v1, v1, LX/0gg;->o:LX/0fd;

    invoke-virtual {v1}, LX/0fd;->d()V

    .line 117923
    :cond_6
    iget-object v1, p0, LX/0hb;->f:LX/0gg;

    iget-object v1, v1, LX/0gg;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0i0;

    .line 117924
    iget-object v2, v1, LX/0i0;->b:LX/0iU;

    .line 117925
    sget-object v3, LX/7gA;->a:[I

    iget-object v1, v2, LX/0iU;->a:LX/10Q;

    invoke-virtual {v1}, LX/10Q;->ordinal()I

    move-result v1

    aget v3, v3, v1

    packed-switch v3, :pswitch_data_0

    .line 117926
    :cond_7
    :goto_2
    iget-object v1, p0, LX/0hb;->f:LX/0gg;

    iget-object v1, v1, LX/0gg;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2r2;

    iget-object v2, p0, LX/0hb;->f:LX/0gg;

    iget-object v2, v2, LX/0gg;->o:LX/0fd;

    invoke-virtual {v2}, LX/0fd;->f()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/apptab/state/TabTag;

    invoke-virtual {v1, v2, v4}, LX/2r2;->a(Lcom/facebook/apptab/state/TabTag;Lcom/facebook/katana/fragment/FbChromeFragment;)V

    .line 117927
    iget-object v1, p0, LX/0hb;->e:Landroid/app/Activity;

    invoke-static {v1}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 117928
    instance-of v1, v0, Lcom/facebook/bookmark/tab/BookmarkTab;

    if-eqz v1, :cond_8

    .line 117929
    iget-object v1, p0, LX/0hb;->f:LX/0gg;

    iget-object v1, v1, LX/0gg;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/15W;

    iget-object v2, p0, LX/0hb;->f:LX/0gg;

    iget-object v2, v2, LX/0gg;->f:Landroid/content/Context;

    new-instance v3, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v4, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->BOOKMARK_TAB_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v3, v4}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    .line 117930
    const-class v4, LX/0i1;

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 117931
    :cond_8
    sget-object v1, Lcom/facebook/video/videohome/tab/VideoHomeTab;->l:Lcom/facebook/video/videohome/tab/VideoHomeTab;

    if-ne v0, v1, :cond_9

    .line 117932
    iget-object v0, p0, LX/0hb;->f:LX/0gg;

    iget-object v0, v0, LX/0gg;->o:LX/0fd;

    .line 117933
    iget-object v1, v0, LX/0fd;->b:LX/0fG;

    invoke-interface {v1}, LX/0fG;->E()V

    .line 117934
    :cond_9
    return-void

    :cond_a
    move v1, v3

    .line 117935
    goto/16 :goto_0

    .line 117936
    :cond_b
    iget-object v1, p0, LX/0hb;->f:LX/0gg;

    iget-object v1, v1, LX/0gg;->o:LX/0fd;

    .line 117937
    iget-object v2, v1, LX/0fd;->a:LX/0fF;

    invoke-interface {v2}, LX/0fF;->y()V

    .line 117938
    goto/16 :goto_1

    .line 117939
    :pswitch_0
    sget-object v3, LX/10Q;->SHOWN_MORE:LX/10Q;

    invoke-static {v2, v3}, LX/0iU;->a$redex0(LX/0iU;LX/10Q;)V

    goto :goto_2

    .line 117940
    :pswitch_1
    sget-object v3, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    .line 117941
    iget-object v1, v2, LX/0iV;->b:Ljava/util/List;

    if-nez v1, :cond_c

    const/4 v1, -0x1

    :goto_3
    move v3, v1

    .line 117942
    if-ne p1, v3, :cond_7

    .line 117943
    sget-object v3, LX/10Q;->HIDDEN:LX/10Q;

    invoke-static {v2, v3}, LX/0iU;->a$redex0(LX/0iU;LX/10Q;)V

    goto :goto_2

    :cond_c
    iget-object v1, v2, LX/0iV;->b:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(IFI)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 117944
    iget-object v0, p0, LX/0hb;->f:LX/0gg;

    iget-object v0, v0, LX/0gg;->o:LX/0fd;

    .line 117945
    iget-object v1, v0, LX/0fd;->d:LX/0gx;

    invoke-virtual {v1, p1, p2}, LX/0gx;->a(IF)V

    .line 117946
    iget-boolean v0, p0, LX/0hb;->b:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, LX/0hb;->c:Z

    if-nez v0, :cond_0

    iget v0, p0, LX/0hb;->a:I

    if-eq p1, v0, :cond_1

    :cond_0
    iget-boolean v0, p0, LX/0hb;->c:Z

    if-eqz v0, :cond_3

    iget v0, p0, LX/0hb;->a:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_3

    .line 117947
    :cond_1
    iget-object v0, p0, LX/0hb;->f:LX/0gg;

    iget-object v0, v0, LX/0gg;->o:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->f()LX/0Px;

    move-result-object v0

    iget v1, p0, LX/0hb;->a:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/TabTag;

    .line 117948
    iget-object v1, v0, Lcom/facebook/apptab/state/TabTag;->i:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 117949
    iget-object v1, p0, LX/0hb;->f:LX/0gg;

    iget-object v1, v1, LX/0gg;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v2, LX/0Yj;

    iget v3, v0, Lcom/facebook/apptab/state/TabTag;->g:I

    iget-object v4, v0, Lcom/facebook/apptab/state/TabTag;->i:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v3, v5, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v0, v0, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v0

    .line 117950
    iput-boolean v5, v0, LX/0Yj;->n:Z

    .line 117951
    move-object v0, v0

    .line 117952
    invoke-interface {v1, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 117953
    :cond_2
    iput-boolean v5, p0, LX/0hb;->b:Z

    .line 117954
    :cond_3
    return-void
.end method

.method public final b(I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 117859
    iget-object v0, p0, LX/0hb;->f:LX/0gg;

    iget-object v0, v0, LX/0gg;->o:LX/0fd;

    invoke-virtual {v0, p1}, LX/0fd;->b(I)V

    .line 117860
    iget-object v0, p0, LX/0hb;->f:LX/0gg;

    iget v0, v0, LX/0gg;->v:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 117861
    iget-object v0, p0, LX/0hb;->f:LX/0gg;

    .line 117862
    iput-boolean v1, v0, LX/0gg;->s:Z

    .line 117863
    :cond_0
    iget-object v0, p0, LX/0hb;->f:LX/0gg;

    .line 117864
    iput p1, v0, LX/0gg;->v:I

    .line 117865
    if-nez p1, :cond_2

    .line 117866
    iget-object v0, p0, LX/0hb;->f:LX/0gg;

    .line 117867
    invoke-virtual {v0}, LX/0gg;->b()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, LX/0gg;->b()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/katana/fragment/FbChromeFragment;->c()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 117868
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 117869
    invoke-virtual {v0}, LX/0gg;->b()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/katana/fragment/FbChromeFragment;->c()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 117870
    iget-object v2, v0, LX/0gg;->p:Lcom/facebook/widget/CustomViewPager;

    iget v3, v1, Landroid/graphics/Rect;->left:I

    const/4 p1, 0x0

    invoke-virtual {v2, v3, p1}, Lcom/facebook/widget/CustomViewPager;->scrollBy(II)V

    .line 117871
    iget v2, v1, Landroid/graphics/Rect;->left:I

    if-eqz v2, :cond_1

    .line 117872
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "harrison_scroll_correction"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 117873
    const-string v3, "scrolled_by"

    iget v1, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 117874
    iget-object v1, v0, LX/0gg;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zb;

    .line 117875
    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 117876
    :cond_1
    iget-object v0, p0, LX/0hb;->f:LX/0gg;

    iget-object v0, v0, LX/0gg;->o:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->d()V

    .line 117877
    iget-object v0, p0, LX/0hb;->f:LX/0gg;

    iget-object v0, v0, LX/0gg;->o:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->u()V

    .line 117878
    :cond_2
    return-void
.end method
