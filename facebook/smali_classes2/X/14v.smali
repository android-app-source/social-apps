.class public LX/14v;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/14v;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Qb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/7Qb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 179392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179393
    iput-object p1, p0, LX/14v;->a:LX/0Ot;

    .line 179394
    return-void
.end method

.method public static a(LX/0QB;)LX/14v;
    .locals 4

    .prologue
    .line 179379
    sget-object v0, LX/14v;->b:LX/14v;

    if-nez v0, :cond_1

    .line 179380
    const-class v1, LX/14v;

    monitor-enter v1

    .line 179381
    :try_start_0
    sget-object v0, LX/14v;->b:LX/14v;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 179382
    if-eqz v2, :cond_0

    .line 179383
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 179384
    new-instance v3, LX/14v;

    const/16 p0, 0x381c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/14v;-><init>(LX/0Ot;)V

    .line 179385
    move-object v0, v3

    .line 179386
    sput-object v0, LX/14v;->b:LX/14v;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 179387
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 179388
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 179389
    :cond_1
    sget-object v0, LX/14v;->b:LX/14v;

    return-object v0

    .line 179390
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 179391
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/157;)V
    .locals 4

    .prologue
    .line 179395
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179396
    iget-object v0, p0, LX/14v;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Qb;

    .line 179397
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179398
    invoke-static {p1}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 179399
    if-nez v2, :cond_1

    .line 179400
    :cond_0
    :goto_0
    return-void

    .line 179401
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v1

    invoke-static {v1}, LX/7Qb;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 179402
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v3

    .line 179403
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object p0

    .line 179404
    if-eqz v3, :cond_0

    if-eqz p0, :cond_0

    .line 179405
    iget-object v1, v0, LX/7Qb;->g:Ljava/util/Map;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Qa;

    .line 179406
    if-nez v1, :cond_2

    .line 179407
    new-instance v1, LX/7Qa;

    invoke-direct {v1, v0, v3, p0}, LX/7Qa;-><init>(LX/7Qb;Ljava/lang/String;Ljava/lang/String;)V

    .line 179408
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v2

    .line 179409
    iput-object v2, v1, LX/7Qa;->f:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 179410
    iget-object v2, v0, LX/7Qb;->g:Ljava/util/Map;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179411
    iget-object v2, v0, LX/7Qb;->h:LX/7QZ;

    invoke-virtual {v2, v1}, LX/7QZ;->a(LX/7Qa;)V

    .line 179412
    invoke-static {v0}, LX/7Qb;->h(LX/7Qb;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 179413
    invoke-virtual {v1}, LX/7Qa;->b()V

    .line 179414
    iget-object v2, v0, LX/7Qb;->h:LX/7QZ;

    invoke-virtual {v2}, LX/7QZ;->b()V

    .line 179415
    :cond_2
    invoke-virtual {v1, p2}, LX/7Qa;->a(LX/157;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/157;)V
    .locals 2

    .prologue
    .line 179369
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179370
    iget-object v0, p0, LX/14v;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Qb;

    .line 179371
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179372
    iget-object v1, v0, LX/7Qb;->g:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Qa;

    .line 179373
    if-eqz v1, :cond_0

    .line 179374
    invoke-virtual {v1, p2}, LX/7Qa;->b(LX/157;)V

    .line 179375
    iget-object p0, v1, LX/7Qa;->d:Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 179376
    invoke-virtual {v1}, LX/7Qa;->a()V

    .line 179377
    iget-object v1, v0, LX/7Qb;->g:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179378
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLMedia;)Z
    .locals 2

    .prologue
    .line 179363
    iget-object v0, p0, LX/14v;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Qb;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    .line 179364
    iget-object p0, v0, LX/7Qb;->g:Ljava/util/Map;

    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/7Qa;

    .line 179365
    if-nez p0, :cond_2

    const/4 p0, 0x0

    :goto_0
    move-object v0, p0

    .line 179366
    if-nez v0, :cond_0

    .line 179367
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    .line 179368
    :cond_0
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    iget-object p0, p0, LX/7Qa;->f:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    goto :goto_0
.end method
