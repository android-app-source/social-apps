.class public LX/1Y7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final a:LX/1Y7;

.field public static final b:LX/1Y7;


# instance fields
.field public final c:Z

.field public final d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/2fy;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 273281
    new-instance v0, LX/1Y8;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/1Y8;-><init>(Z)V

    invoke-virtual {v0}, LX/1Y8;->a()LX/1Y7;

    move-result-object v0

    sput-object v0, LX/1Y7;->a:LX/1Y7;

    .line 273282
    new-instance v0, LX/1Y8;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/1Y8;-><init>(Z)V

    invoke-virtual {v0}, LX/1Y8;->a()LX/1Y7;

    move-result-object v0

    sput-object v0, LX/1Y7;->b:LX/1Y7;

    return-void
.end method

.method public constructor <init>(LX/1Y8;)V
    .locals 1

    .prologue
    .line 273274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273275
    iget-boolean v0, p1, LX/1Y8;->a:Z

    iput-boolean v0, p0, LX/1Y7;->c:Z

    .line 273276
    iget-object v0, p1, LX/1Y8;->b:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/1Y7;->d:LX/0am;

    .line 273277
    iget-object v0, p1, LX/1Y8;->c:LX/2fy;

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/1Y7;->e:LX/0am;

    .line 273278
    iget-object v0, p1, LX/1Y8;->d:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/1Y7;->f:LX/0am;

    .line 273279
    iget-object v0, p1, LX/1Y8;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/1Y7;->g:LX/0am;

    .line 273280
    return-void
.end method
