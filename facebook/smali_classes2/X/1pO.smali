.class public final LX/1pO;
.super LX/0lx;
.source ""


# instance fields
.field public final c:[I

.field public final d:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I[II)V
    .locals 2

    .prologue
    .line 329347
    invoke-direct {p0, p1, p2}, LX/0lx;-><init>(Ljava/lang/String;I)V

    .line 329348
    const/4 v0, 0x3

    if-ge p4, v0, :cond_0

    .line 329349
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Qlen must >= 3"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 329350
    :cond_0
    iput-object p3, p0, LX/1pO;->c:[I

    .line 329351
    iput p4, p0, LX/1pO;->d:I

    .line 329352
    return-void
.end method


# virtual methods
.method public final a(I)Z
    .locals 1

    .prologue
    .line 329353
    const/4 v0, 0x0

    return v0
.end method

.method public final a(II)Z
    .locals 1

    .prologue
    .line 329354
    const/4 v0, 0x0

    return v0
.end method

.method public final a([II)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 329355
    iget v1, p0, LX/1pO;->d:I

    if-eq p2, v1, :cond_1

    .line 329356
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v1, v0

    .line 329357
    :goto_1
    if-ge v1, p2, :cond_2

    .line 329358
    aget v2, p1, v1

    iget-object v3, p0, LX/1pO;->c:[I

    aget v3, v3, v1

    if-ne v2, v3, :cond_0

    .line 329359
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 329360
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
