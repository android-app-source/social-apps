.class public final LX/1A6;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 210075
    new-instance v0, LX/0U1;

    const-string v1, "video_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1A6;->a:LX/0U1;

    .line 210076
    new-instance v0, LX/0U1;

    const-string v1, "download_attempts"

    const-string v2, "INT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1A6;->b:LX/0U1;

    .line 210077
    new-instance v0, LX/0U1;

    const-string v1, "download_start_time"

    const-string v2, "BIGINT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1A6;->c:LX/0U1;

    .line 210078
    new-instance v0, LX/0U1;

    const-string v1, "download_end_time"

    const-string v2, "BIGINT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1A6;->d:LX/0U1;

    .line 210079
    new-instance v0, LX/0U1;

    const-string v1, "view_count"

    const-string v2, "INT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1A6;->e:LX/0U1;

    .line 210080
    new-instance v0, LX/0U1;

    const-string v1, "download_origin"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1A6;->f:LX/0U1;

    return-void
.end method
