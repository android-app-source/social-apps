.class public final enum LX/0rN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0rN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0rN;

.field public static final enum AVAILABLE:LX/0rN;

.field public static final enum PROVISIONAL:LX/0rN;

.field public static final enum TAKEN:LX/0rN;


# instance fields
.field private final mStateName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 149518
    new-instance v0, LX/0rN;

    const-string v1, "AVAILABLE"

    const-string v2, "available"

    invoke-direct {v0, v1, v3, v2}, LX/0rN;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0rN;->AVAILABLE:LX/0rN;

    .line 149519
    new-instance v0, LX/0rN;

    const-string v1, "PROVISIONAL"

    const-string v2, "provisional"

    invoke-direct {v0, v1, v4, v2}, LX/0rN;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0rN;->PROVISIONAL:LX/0rN;

    .line 149520
    new-instance v0, LX/0rN;

    const-string v1, "TAKEN"

    const-string v2, "taken"

    invoke-direct {v0, v1, v5, v2}, LX/0rN;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0rN;->TAKEN:LX/0rN;

    .line 149521
    const/4 v0, 0x3

    new-array v0, v0, [LX/0rN;

    sget-object v1, LX/0rN;->AVAILABLE:LX/0rN;

    aput-object v1, v0, v3

    sget-object v1, LX/0rN;->PROVISIONAL:LX/0rN;

    aput-object v1, v0, v4

    sget-object v1, LX/0rN;->TAKEN:LX/0rN;

    aput-object v1, v0, v5

    sput-object v0, LX/0rN;->$VALUES:[LX/0rN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 149515
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 149516
    iput-object p3, p0, LX/0rN;->mStateName:Ljava/lang/String;

    .line 149517
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0rN;
    .locals 1

    .prologue
    .line 149514
    const-class v0, LX/0rN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0rN;

    return-object v0
.end method

.method public static values()[LX/0rN;
    .locals 1

    .prologue
    .line 149512
    sget-object v0, LX/0rN;->$VALUES:[LX/0rN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0rN;

    return-object v0
.end method


# virtual methods
.method public final getSlotName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149513
    iget-object v0, p0, LX/0rN;->mStateName:Ljava/lang/String;

    return-object v0
.end method
