.class public LX/1mD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile h:LX/1mD;


# instance fields
.field public b:LX/1mQ;

.field public c:LX/03V;

.field private d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/1mC;

.field public f:LX/1mC;

.field private g:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 313450
    const-class v0, LX/1mD;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1mD;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1mQ;LX/0Or;LX/1mC;Landroid/content/res/Resources;LX/03V;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/video/settings/IsAutoplaySettingsMigrationEnabled;
        .end annotation
    .end param
    .param p3    # LX/1mC;
        .annotation runtime Lcom/facebook/video/settings/DefaultAutoPlaySettingsFromServer;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1mQ;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/1mC;",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 313362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313363
    sget-object v0, LX/1mC;->OFF:LX/1mC;

    iput-object v0, p0, LX/1mD;->e:LX/1mC;

    .line 313364
    iput-object p1, p0, LX/1mD;->b:LX/1mQ;

    .line 313365
    iput-object p5, p0, LX/1mD;->c:LX/03V;

    .line 313366
    iput-object p2, p0, LX/1mD;->d:LX/0Or;

    .line 313367
    iput-object p3, p0, LX/1mD;->f:LX/1mC;

    .line 313368
    iput-object p4, p0, LX/1mD;->g:Landroid/content/res/Resources;

    .line 313369
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLAutoplaySettingEffective;)LX/1mC;
    .locals 2

    .prologue
    .line 313370
    sget-object v0, LX/7QC;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLAutoplaySettingEffective;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 313371
    sget-object v0, LX/1mC;->OFF:LX/1mC;

    :goto_0
    return-object v0

    .line 313372
    :pswitch_0
    sget-object v0, LX/1mC;->ON:LX/1mC;

    goto :goto_0

    .line 313373
    :pswitch_1
    sget-object v0, LX/1mC;->WIFI_ONLY:LX/1mC;

    goto :goto_0

    .line 313374
    :pswitch_2
    sget-object v0, LX/1mC;->OFF:LX/1mC;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/1mD;
    .locals 9

    .prologue
    .line 313375
    sget-object v0, LX/1mD;->h:LX/1mD;

    if-nez v0, :cond_1

    .line 313376
    const-class v1, LX/1mD;

    monitor-enter v1

    .line 313377
    :try_start_0
    sget-object v0, LX/1mD;->h:LX/1mD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 313378
    if-eqz v2, :cond_0

    .line 313379
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 313380
    new-instance v3, LX/1mD;

    .line 313381
    new-instance v7, LX/1mQ;

    invoke-static {v0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v4

    check-cast v4, LX/0dC;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/1mR;->a(LX/0QB;)LX/1mR;

    move-result-object v6

    check-cast v6, LX/1mR;

    invoke-direct {v7, v4, v5, v6}, LX/1mQ;-><init>(LX/0dC;LX/0tX;LX/1mR;)V

    .line 313382
    move-object v4, v7

    .line 313383
    check-cast v4, LX/1mQ;

    const/16 v5, 0x386

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/1mG;->a(LX/0QB;)LX/1mC;

    move-result-object v6

    check-cast v6, LX/1mC;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-direct/range {v3 .. v8}, LX/1mD;-><init>(LX/1mQ;LX/0Or;LX/1mC;Landroid/content/res/Resources;LX/03V;)V

    .line 313384
    move-object v0, v3

    .line 313385
    sput-object v0, LX/1mD;->h:LX/1mD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 313386
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 313387
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 313388
    :cond_1
    sget-object v0, LX/1mD;->h:LX/1mD;

    return-object v0

    .line 313389
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 313390
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1mC;Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/1mC;
    .locals 9

    .prologue
    .line 313391
    const/4 v0, 0x1

    const/4 v5, 0x0

    .line 313392
    invoke-static {p1, p2}, LX/1mS;->a(LX/1mC;Lcom/facebook/prefs/shared/FbSharedPreferences;)Ljava/lang/String;

    move-result-object v1

    .line 313393
    sget-object v2, LX/1mC;->DEFAULT:LX/1mC;

    invoke-virtual {v2}, LX/1mC;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 313394
    invoke-interface {p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/1mI;->i:LX/0Tn;

    invoke-interface {v1, v2, v5}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 313395
    invoke-interface {p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/1mI;->g:LX/0Tn;

    invoke-virtual {p1}, LX/1mC;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 313396
    :goto_0
    move-object v0, p1

    .line 313397
    iput-object v0, p0, LX/1mD;->e:LX/1mC;

    .line 313398
    iget-object v0, p0, LX/1mD;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1mD;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_1

    .line 313399
    :cond_0
    iget-object v2, p0, LX/1mD;->b:LX/1mQ;

    .line 313400
    new-instance v5, LX/7QD;

    invoke-direct {v5}, LX/7QD;-><init>()V

    move-object v5, v5

    .line 313401
    const-string v6, "input"

    iget-object v7, v2, LX/1mQ;->b:LX/0dC;

    invoke-virtual {v7}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 313402
    iget-object v6, v2, LX/1mQ;->a:LX/0tX;

    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    sget-object v7, LX/0zS;->a:LX/0zS;

    invoke-virtual {v5, v7}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v5

    const-wide/32 v7, 0x15180

    invoke-virtual {v5, v7, v8}, LX/0zO;->a(J)LX/0zO;

    move-result-object v5

    const-string v7, "AUTOPLAY_SETTING_READ_QUERY"

    invoke-static {v7}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v7

    .line 313403
    iput-object v7, v5, LX/0zO;->d:Ljava/util/Set;

    .line 313404
    move-object v5, v5

    .line 313405
    invoke-virtual {v6, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    .line 313406
    new-instance v6, LX/7QN;

    invoke-direct {v6, v2}, LX/7QN;-><init>(LX/1mQ;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v7

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 313407
    move-object v2, v5

    .line 313408
    new-instance v3, LX/7QA;

    invoke-direct {v3, p0, p2}, LX/7QA;-><init>(LX/1mD;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 313409
    :cond_1
    iget-object v0, p0, LX/1mD;->e:LX/1mC;

    return-object v0

    .line 313410
    :cond_2
    invoke-static {v1}, LX/1mC;->valueOf(Ljava/lang/String;)LX/1mC;

    move-result-object v1

    .line 313411
    sget-object v2, LX/1mI;->i:LX/0Tn;

    invoke-interface {p2, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;)LX/03R;

    move-result-object v2

    invoke-virtual {v2}, LX/03R;->isSet()Z

    move-result v2

    if-nez v2, :cond_3

    .line 313412
    if-ne v1, p1, :cond_4

    .line 313413
    invoke-interface {p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/1mI;->i:LX/0Tn;

    invoke-interface {v2, v3, v5}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 313414
    :cond_3
    :goto_1
    sget-object v2, LX/1mI;->i:LX/0Tn;

    invoke-interface {p2, v2, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    .line 313415
    if-eqz v0, :cond_5

    if-nez v2, :cond_5

    .line 313416
    if-eq v1, p1, :cond_5

    .line 313417
    invoke-static {p2, v1}, LX/1mS;->b(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1mC;)V

    goto/16 :goto_0

    .line 313418
    :cond_4
    invoke-interface {p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/1mI;->i:LX/0Tn;

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    goto :goto_1

    :cond_5
    move-object p1, v1

    goto/16 :goto_0
.end method

.method public final a(LX/1mC;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 313419
    sget-object v0, LX/7QC;->b:[I

    invoke-virtual {p1}, LX/1mC;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 313420
    iget-object v0, p0, LX/1mD;->g:Landroid/content/res/Resources;

    const v1, 0x7f081ae7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 313421
    :pswitch_0
    iget-object v0, p0, LX/1mD;->g:Landroid/content/res/Resources;

    const v1, 0x7f081ae9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 313422
    :pswitch_1
    iget-object v0, p0, LX/1mD;->g:Landroid/content/res/Resources;

    const v1, 0x7f081ae8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1mC;Ljava/lang/String;)V
    .locals 5
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/UpdateTypeValues;
        .end annotation
    .end param

    .prologue
    .line 313423
    iget-object v0, p0, LX/1mD;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 313424
    invoke-static {p1}, LX/1mS;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/03R;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    .line 313425
    if-nez v0, :cond_1

    .line 313426
    const-string v1, "DEFAULT_AUTOPLAY"

    .line 313427
    :goto_0
    move-object v0, v1

    .line 313428
    iget-object v1, p0, LX/1mD;->b:LX/1mQ;

    .line 313429
    new-instance v2, LX/4E2;

    invoke-direct {v2}, LX/4E2;-><init>()V

    iget-object v3, v1, LX/1mQ;->b:LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    .line 313430
    const-string v4, "device_identifier"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 313431
    move-object v2, v2

    .line 313432
    const-string v3, "update_type"

    invoke-virtual {v2, v3, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 313433
    move-object v2, v2

    .line 313434
    const-string v3, "autoplay_setting"

    invoke-virtual {v2, v3, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 313435
    move-object v2, v2

    .line 313436
    new-instance v3, LX/7QI;

    invoke-direct {v3}, LX/7QI;-><init>()V

    move-object v3, v3

    .line 313437
    const-string v4, "input"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 313438
    iget-object v2, v1, LX/1mQ;->a:LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 313439
    new-instance v3, LX/7QM;

    invoke-direct {v3, v1}, LX/7QM;-><init>(LX/1mQ;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 313440
    move-object v0, v2

    .line 313441
    new-instance v1, LX/7QB;

    invoke-direct {v1, p0, p2, p1}, LX/7QB;-><init>(LX/1mD;LX/1mC;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 313442
    :cond_0
    return-void

    .line 313443
    :cond_1
    sget-object v1, LX/7QC;->b:[I

    invoke-virtual {p2}, LX/1mC;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 313444
    const-string v1, "OFF"

    goto :goto_0

    .line 313445
    :pswitch_0
    const-string v1, "ON"

    goto :goto_0

    .line 313446
    :pswitch_1
    const-string v1, "WIFI_ONLY"

    goto :goto_0

    .line 313447
    :pswitch_2
    const-string v1, "OFF"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 313448
    iget-object v0, p0, LX/1mD;->c:LX/03V;

    sget-object v1, LX/1mD;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 313449
    return-void
.end method
