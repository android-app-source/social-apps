.class public LX/1e8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1e8;


# instance fields
.field public final a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 287352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 287353
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1e8;->a:Ljava/util/HashSet;

    .line 287354
    return-void
.end method

.method public static a(LX/0QB;)LX/1e8;
    .locals 3

    .prologue
    .line 287359
    sget-object v0, LX/1e8;->b:LX/1e8;

    if-nez v0, :cond_1

    .line 287360
    const-class v1, LX/1e8;

    monitor-enter v1

    .line 287361
    :try_start_0
    sget-object v0, LX/1e8;->b:LX/1e8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 287362
    if-eqz v2, :cond_0

    .line 287363
    :try_start_1
    new-instance v0, LX/1e8;

    invoke-direct {v0}, LX/1e8;-><init>()V

    .line 287364
    move-object v0, v0

    .line 287365
    sput-object v0, LX/1e8;->b:LX/1e8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 287366
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 287367
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 287368
    :cond_1
    sget-object v0, LX/1e8;->b:LX/1e8;

    return-object v0

    .line 287369
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 287370
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;Z)V
    .locals 2

    .prologue
    .line 287355
    if-eqz p2, :cond_0

    .line 287356
    iget-object v0, p0, LX/1e8;->a:Ljava/util/HashSet;

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 287357
    :goto_0
    return-void

    .line 287358
    :cond_0
    iget-object v0, p0, LX/1e8;->a:Ljava/util/HashSet;

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
