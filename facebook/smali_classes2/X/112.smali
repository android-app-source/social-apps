.class public LX/112;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile e:LX/112;


# instance fields
.field public final b:LX/113;

.field public final c:LX/0Uh;

.field public final d:LX/114;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 169715
    const-class v0, LX/112;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/112;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Uh;LX/113;LX/114;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 169716
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169717
    iput-object p2, p0, LX/112;->b:LX/113;

    .line 169718
    iput-object p1, p0, LX/112;->c:LX/0Uh;

    .line 169719
    iput-object p3, p0, LX/112;->d:LX/114;

    .line 169720
    return-void
.end method

.method public static a(LX/0QB;)LX/112;
    .locals 6

    .prologue
    .line 169721
    sget-object v0, LX/112;->e:LX/112;

    if-nez v0, :cond_1

    .line 169722
    const-class v1, LX/112;

    monitor-enter v1

    .line 169723
    :try_start_0
    sget-object v0, LX/112;->e:LX/112;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 169724
    if-eqz v2, :cond_0

    .line 169725
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 169726
    new-instance p0, LX/112;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/113;->a(LX/0QB;)LX/113;

    move-result-object v4

    check-cast v4, LX/113;

    invoke-static {v0}, LX/114;->a(LX/0QB;)LX/114;

    move-result-object v5

    check-cast v5, LX/114;

    invoke-direct {p0, v3, v4, v5}, LX/112;-><init>(LX/0Uh;LX/113;LX/114;)V

    .line 169727
    move-object v0, p0

    .line 169728
    sput-object v0, LX/112;->e:LX/112;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169729
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 169730
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 169731
    :cond_1
    sget-object v0, LX/112;->e:LX/112;

    return-object v0

    .line 169732
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 169733
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
