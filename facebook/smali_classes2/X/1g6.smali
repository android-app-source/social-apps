.class public LX/1g6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1g7;


# direct methods
.method public constructor <init>(LX/1g7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 293301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 293302
    iput-object p1, p0, LX/1g6;->a:LX/1g7;

    .line 293303
    return-void
.end method

.method public static a(LX/0QB;)LX/1g6;
    .locals 4

    .prologue
    .line 293304
    const-class v1, LX/1g6;

    monitor-enter v1

    .line 293305
    :try_start_0
    sget-object v0, LX/1g6;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 293306
    sput-object v2, LX/1g6;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 293307
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293308
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 293309
    new-instance p0, LX/1g6;

    const-class v3, LX/1g7;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1g7;

    invoke-direct {p0, v3}, LX/1g6;-><init>(LX/1g7;)V

    .line 293310
    move-object v0, p0

    .line 293311
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 293312
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1g6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 293313
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 293314
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/7iQ;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 293315
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 293316
    sget-object v1, LX/7iL;->COMPONENT:LX/7iL;

    sget-object v2, LX/7iO;->PDFY_FEED_UNIT:LX/7iO;

    iget-object v2, v2, LX/7iO;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293317
    if-eqz p2, :cond_0

    .line 293318
    sget-object v1, LX/7iL;->PRODUCT_ID:LX/7iL;

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293319
    :cond_0
    iget-object v1, p0, LX/1g6;->a:LX/1g7;

    sget-object v2, LX/7iM;->PDFY_ACTIVITY:LX/7iM;

    sget-object v3, LX/7iN;->COMMERCE_NEWS_FEED:LX/7iN;

    sget-object v4, LX/7iP;->PDFY:LX/7iP;

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, LX/1g7;->a(LX/7iM;LX/7iN;LX/7iP;Ljava/lang/Long;)LX/7iT;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, LX/7iT;->a(LX/7iQ;Ljava/util/Map;)V

    .line 293320
    return-void
.end method
