.class public LX/1Dg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Dh;


# static fields
.field private static final b:Z

.field private static final c:[I

.field private static final d:[I

.field private static final e:[I

.field private static final f:[I

.field private static final g:[I

.field private static final h:[I


# instance fields
.field public A:LX/1dQ;

.field public B:LX/1dQ;

.field public C:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/32D;",
            ">;"
        }
    .end annotation
.end field

.field public D:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/32A;",
            ">;"
        }
    .end annotation
.end field

.field public E:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/48D;",
            ">;"
        }
    .end annotation
.end field

.field public F:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/3CI;",
            ">;"
        }
    .end annotation
.end field

.field public G:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/2uO;",
            ">;"
        }
    .end annotation
.end field

.field public H:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/48E;",
            ">;"
        }
    .end annotation
.end field

.field public I:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/48F;",
            ">;"
        }
    .end annotation
.end field

.field public J:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/48G;",
            ">;"
        }
    .end annotation
.end field

.field public K:Ljava/lang/CharSequence;

.field public L:Ljava/lang/Object;

.field public M:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public N:Ljava/lang/String;

.field public O:LX/1mz;

.field private P:LX/1mz;

.field private Q:LX/1mz;

.field private R:F

.field private S:F

.field private T:F

.field private U:F

.field private V:F

.field private W:F

.field public X:F

.field public Y:F

.field private Z:F

.field public a:LX/1mn;

.field private aa:F

.field private ab:F

.field private ac:F

.field private ad:I

.field private ae:I

.field private af:I

.field public ag:F

.field public ah:F

.field public ai:LX/1ml;

.field public aj:Z

.field private final i:LX/1Dp;

.field public j:LX/1De;

.field private k:Landroid/content/res/Resources;

.field public l:LX/1X1;

.field public m:I

.field private n:Z

.field public o:Z

.field public p:LX/1Dg;

.field public q:J

.field private r:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private s:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public t:Z

.field private u:Ljava/lang/String;

.field public v:LX/1dQ;

.field public w:LX/1dQ;

.field public x:LX/1dQ;

.field public y:LX/1dQ;

.field public z:LX/1dQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 218438
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/1Dg;->b:Z

    .line 218439
    sget-object v0, LX/1Dj;->b:[I

    move-object v0, v0

    .line 218440
    sput-object v0, LX/1Dg;->c:[I

    .line 218441
    sget-object v0, LX/1Dk;->b:[I

    move-object v0, v0

    .line 218442
    sput-object v0, LX/1Dg;->d:[I

    .line 218443
    sget-object v0, LX/1Dl;->b:[I

    move-object v0, v0

    .line 218444
    sput-object v0, LX/1Dg;->e:[I

    .line 218445
    sget-object v0, LX/1Dm;->b:[I

    move-object v0, v0

    .line 218446
    sput-object v0, LX/1Dg;->f:[I

    .line 218447
    sget-object v0, LX/1Dn;->b:[I

    move-object v0, v0

    .line 218448
    sput-object v0, LX/1Dg;->g:[I

    .line 218449
    sget-object v0, LX/1Do;->b:[I

    move-object v0, v0

    .line 218450
    sput-object v0, LX/1Dg;->h:[I

    return-void

    .line 218451
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/high16 v3, -0x40800000    # -1.0f

    const/4 v2, -0x1

    const/high16 v1, 0x7fc00000    # NaNf

    .line 218452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218453
    new-instance v0, LX/1Dp;

    invoke-direct {v0}, LX/1Dp;-><init>()V

    iput-object v0, p0, LX/1Dg;->i:LX/1Dp;

    .line 218454
    const/4 v0, 0x0

    iput v0, p0, LX/1Dg;->m:I

    .line 218455
    iput v1, p0, LX/1Dg;->R:F

    .line 218456
    iput v1, p0, LX/1Dg;->S:F

    .line 218457
    iput v1, p0, LX/1Dg;->T:F

    .line 218458
    iput v1, p0, LX/1Dg;->U:F

    .line 218459
    iput v1, p0, LX/1Dg;->V:F

    .line 218460
    iput v1, p0, LX/1Dg;->W:F

    .line 218461
    iput v1, p0, LX/1Dg;->X:F

    .line 218462
    iput v1, p0, LX/1Dg;->Y:F

    .line 218463
    iput v1, p0, LX/1Dg;->Z:F

    .line 218464
    iput v1, p0, LX/1Dg;->aa:F

    .line 218465
    iput v1, p0, LX/1Dg;->ab:F

    .line 218466
    iput v1, p0, LX/1Dg;->ac:F

    .line 218467
    iput v2, p0, LX/1Dg;->ad:I

    .line 218468
    iput v2, p0, LX/1Dg;->ae:I

    .line 218469
    iput v2, p0, LX/1Dg;->af:I

    .line 218470
    iput v3, p0, LX/1Dg;->ag:F

    .line 218471
    iput v3, p0, LX/1Dg;->ah:F

    return-void
.end method

.method private Q(II)LX/1Dg;
    .locals 4
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218472
    iget-object v0, p0, LX/1Dg;->O:LX/1mz;

    if-nez v0, :cond_0

    .line 218473
    invoke-static {}, LX/1cy;->o()LX/1mz;

    move-result-object v0

    iput-object v0, p0, LX/1Dg;->O:LX/1mz;

    .line 218474
    :cond_0
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide v2, 0x80000000L

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218475
    iget-object v0, p0, LX/1Dg;->O:LX/1mz;

    int-to-float v1, p2

    invoke-virtual {v0, p1, v1}, LX/1mz;->a(IF)Z

    .line 218476
    return-object p0
.end method

.method public static a(LX/1Dg;LX/1mz;I)F
    .locals 4

    .prologue
    const/4 v0, 0x5

    const/4 v1, 0x4

    .line 218477
    iget-object v2, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v2}, LX/1mn;->o()Lcom/facebook/csslayout/YogaDirection;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v3}, LX/1Dn;->a(I)Lcom/facebook/csslayout/YogaDirection;

    move-result-object v3

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    .line 218478
    :goto_0
    packed-switch p2, :pswitch_data_0

    .line 218479
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not an horizontal padding index: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218480
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 218481
    :pswitch_1
    if-eqz v2, :cond_2

    .line 218482
    :goto_1
    invoke-virtual {p1, v0}, LX/1mz;->b(I)F

    move-result v0

    .line 218483
    invoke-static {v0}, LX/1mo;->a(F)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 218484
    invoke-virtual {p1, p2}, LX/1mz;->a(I)F

    move-result v0

    .line 218485
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 218486
    goto :goto_1

    .line 218487
    :pswitch_2
    if-eqz v2, :cond_3

    :goto_2
    move v0, v1

    .line 218488
    goto :goto_1

    :cond_3
    move v1, v0

    .line 218489
    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;
    .locals 0
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TA;>;TA;)",
            "Ljava/util/List",
            "<TA;>;"
        }
    .end annotation

    .prologue
    .line 218490
    if-nez p0, :cond_0

    .line 218491
    new-instance p0, Ljava/util/LinkedList;

    invoke-direct {p0}, Ljava/util/LinkedList;-><init>()V

    .line 218492
    :cond_0
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218493
    return-object p0
.end method

.method public static a(LX/1Dg;LX/1Dg;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 218494
    iget-wide v4, p1, LX/1Dg;->q:J

    const-wide/16 v6, 0x1

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    move v0, v1

    .line 218495
    :goto_0
    invoke-virtual {p1}, LX/1Dg;->i()I

    move-result v3

    invoke-virtual {p0}, LX/1Dg;->i()I

    move-result v4

    if-ne v3, v4, :cond_3

    move v3, v1

    .line 218496
    :goto_1
    if-nez v0, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 218497
    goto :goto_0

    :cond_3
    move v3, v2

    .line 218498
    goto :goto_1
.end method

.method public static ao(LX/1Dg;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 218499
    iget-object v0, p0, LX/1Dg;->v:LX/1dQ;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Dg;->w:LX/1dQ;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Dg;->x:LX/1dQ;

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 218500
    :goto_0
    iget-object v3, p0, LX/1Dg;->O:LX/1mz;

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    :goto_1
    return v2

    :cond_1
    move v0, v1

    .line 218501
    goto :goto_0

    :cond_2
    move v2, v1

    .line 218502
    goto :goto_1
.end method

.method public static c(LX/1Dg;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const-wide/16 v6, 0x0

    .line 218503
    iget-wide v2, p0, LX/1Dg;->q:J

    const-wide/16 v4, 0x2

    and-long/2addr v2, v4

    cmp-long v1, v2, v6

    if-eqz v1, :cond_0

    .line 218504
    const-string v1, "alignSelf"

    invoke-static {v0, v1}, LX/1Dg;->a(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 218505
    :cond_0
    iget-wide v2, p0, LX/1Dg;->q:J

    const-wide/16 v4, 0x4

    and-long/2addr v2, v4

    cmp-long v1, v2, v6

    if-eqz v1, :cond_1

    .line 218506
    const-string v1, "positionType"

    invoke-static {v0, v1}, LX/1Dg;->a(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 218507
    :cond_1
    iget-wide v2, p0, LX/1Dg;->q:J

    const-wide/16 v4, 0x8

    and-long/2addr v2, v4

    cmp-long v1, v2, v6

    if-eqz v1, :cond_2

    .line 218508
    const-string v1, "flex"

    invoke-static {v0, v1}, LX/1Dg;->a(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 218509
    :cond_2
    iget-wide v2, p0, LX/1Dg;->q:J

    const-wide/16 v4, 0x10

    and-long/2addr v2, v4

    cmp-long v1, v2, v6

    if-eqz v1, :cond_3

    .line 218510
    const-string v1, "flexGrow"

    invoke-static {v0, v1}, LX/1Dg;->a(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 218511
    :cond_3
    iget-wide v2, p0, LX/1Dg;->q:J

    const-wide/16 v4, 0x20

    and-long/2addr v2, v4

    cmp-long v1, v2, v6

    if-eqz v1, :cond_4

    .line 218512
    const-string v1, "flexShrink"

    invoke-static {v0, v1}, LX/1Dg;->a(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 218513
    :cond_4
    iget-wide v2, p0, LX/1Dg;->q:J

    const-wide/16 v4, 0x200

    and-long/2addr v2, v4

    cmp-long v1, v2, v6

    if-eqz v1, :cond_5

    .line 218514
    const-string v1, "margin"

    invoke-static {v0, v1}, LX/1Dg;->a(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 218515
    :cond_5
    if-eqz v0, :cond_6

    .line 218516
    const-string v1, ", "

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 218517
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "You should not set "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " to a root layout in "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 218518
    iget-object v2, p0, LX/1Dg;->l:LX/1X1;

    move-object v2, v2

    .line 218519
    iget-object v3, v2, LX/1X1;->e:LX/1S3;

    move-object v2, v3

    .line 218520
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 218521
    :cond_6
    return-void
.end method

.method private d(LX/1dc;)LX/1Dg;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1Dg;"
        }
    .end annotation

    .prologue
    .line 218522
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x80000

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218523
    iput-object p1, p0, LX/1Dg;->s:LX/1dc;

    .line 218524
    return-object p0
.end method

.method private i(LX/1dQ;)LX/1Dg;
    .locals 4

    .prologue
    .line 218525
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x200000

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218526
    iput-object p1, p0, LX/1Dg;->w:LX/1dQ;

    .line 218527
    return-object p0
.end method


# virtual methods
.method public A(II)LX/1Dg;
    .locals 4
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218528
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/16 v2, 0x200

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218529
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-static {p1}, Lcom/facebook/csslayout/YogaEdge;->fromInt(I)Lcom/facebook/csslayout/YogaEdge;

    move-result-object v1

    int-to-float v2, p2

    invoke-interface {v0, v1, v2}, LX/1mn;->a(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218530
    return-object p0
.end method

.method public synthetic A(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 218531
    invoke-virtual {p0, p1}, LX/1Dg;->aI(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public B(II)LX/1Dg;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param

    .prologue
    .line 218532
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/1Dg;->a(III)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic B(I)LX/1Dh;
    .locals 1

    .prologue
    .line 218533
    invoke-virtual {p0, p1}, LX/1Dg;->ah(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public C()LX/1Dg;
    .locals 1

    .prologue
    .line 218436
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Dg;->t:Z

    .line 218437
    return-object p0
.end method

.method public C(II)LX/1Dg;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218535
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p2}, LX/1Dp;->f(I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, LX/1Dg;->A(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic C(I)LX/1Dh;
    .locals 1

    .prologue
    .line 218536
    invoke-virtual {p0, p1}, LX/1Dg;->ai(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public D(II)LX/1Dg;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218537
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p2}, LX/1Dp;->a(I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, LX/1Dg;->A(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic D(I)LX/1Dh;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218538
    invoke-virtual {p0, p1}, LX/1Dg;->ak(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public E(II)LX/1Dg;
    .locals 4
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218539
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/16 v2, 0x400

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218540
    iget-boolean v0, p0, LX/1Dg;->o:Z

    if-eqz v0, :cond_1

    .line 218541
    iget-object v0, p0, LX/1Dg;->P:LX/1mz;

    if-nez v0, :cond_0

    .line 218542
    invoke-static {}, LX/1cy;->o()LX/1mz;

    move-result-object v0

    iput-object v0, p0, LX/1Dg;->P:LX/1mz;

    .line 218543
    :cond_0
    iget-object v0, p0, LX/1Dg;->P:LX/1mz;

    int-to-float v1, p2

    invoke-virtual {v0, p1, v1}, LX/1mz;->a(IF)Z

    .line 218544
    :goto_0
    return-object p0

    .line 218545
    :cond_1
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-static {p1}, Lcom/facebook/csslayout/YogaEdge;->fromInt(I)Lcom/facebook/csslayout/YogaEdge;

    move-result-object v1

    int-to-float v2, p2

    invoke-interface {v0, v1, v2}, LX/1mn;->b(Lcom/facebook/csslayout/YogaEdge;F)V

    goto :goto_0
.end method

.method public synthetic E(I)LX/1Dh;
    .locals 1

    .prologue
    .line 218546
    invoke-virtual {p0, p1}, LX/1Dg;->al(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final E()LX/1dQ;
    .locals 1

    .prologue
    .line 218547
    iget-object v0, p0, LX/1Dg;->v:LX/1dQ;

    return-object v0
.end method

.method public F(II)LX/1Dg;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param

    .prologue
    .line 218548
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/1Dg;->b(III)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic F(I)LX/1Dh;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218549
    invoke-virtual {p0, p1}, LX/1Dg;->am(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final F()LX/1dQ;
    .locals 1

    .prologue
    .line 218550
    iget-object v0, p0, LX/1Dg;->w:LX/1dQ;

    return-object v0
.end method

.method public final G()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 218551
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x2000000

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 218552
    const/16 v0, 0x100

    .line 218553
    :goto_0
    return v0

    .line 218554
    :cond_0
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x4000000

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 218555
    const/16 v0, 0x200

    goto :goto_0

    .line 218556
    :cond_1
    const/16 v0, 0x80

    goto :goto_0
.end method

.method public G(II)LX/1Dg;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218557
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p2}, LX/1Dp;->f(I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, LX/1Dg;->E(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic G(I)LX/1Dh;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218558
    invoke-virtual {p0, p1}, LX/1Dg;->an(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public H(II)LX/1Dg;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218559
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p2}, LX/1Dp;->a(I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, LX/1Dg;->E(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic H(I)LX/1Dh;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218421
    invoke-virtual {p0, p1}, LX/1Dg;->ap(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final H()LX/1dQ;
    .locals 1

    .prologue
    .line 218398
    iget-object v0, p0, LX/1Dg;->x:LX/1dQ;

    return-object v0
.end method

.method public I(II)LX/1Dg;
    .locals 4
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218399
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide v2, 0x200000000L

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218400
    iget-boolean v0, p0, LX/1Dg;->o:Z

    if-eqz v0, :cond_1

    .line 218401
    iget-object v0, p0, LX/1Dg;->Q:LX/1mz;

    if-nez v0, :cond_0

    .line 218402
    invoke-static {}, LX/1cy;->o()LX/1mz;

    move-result-object v0

    iput-object v0, p0, LX/1Dg;->Q:LX/1mz;

    .line 218403
    :cond_0
    iget-object v0, p0, LX/1Dg;->Q:LX/1mz;

    int-to-float v1, p2

    invoke-virtual {v0, p1, v1}, LX/1mz;->a(IF)Z

    .line 218404
    :goto_0
    return-object p0

    .line 218405
    :cond_1
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-static {p1}, Lcom/facebook/csslayout/YogaEdge;->fromInt(I)Lcom/facebook/csslayout/YogaEdge;

    move-result-object v1

    int-to-float v2, p2

    invoke-interface {v0, v1, v2}, LX/1mn;->c(Lcom/facebook/csslayout/YogaEdge;F)V

    goto :goto_0
.end method

.method public synthetic I(I)LX/1Dh;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218406
    invoke-virtual {p0, p1}, LX/1Dg;->au(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public J(II)LX/1Dg;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218407
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p2}, LX/1Dp;->a(I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, LX/1Dg;->I(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic J(I)LX/1Dh;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218408
    invoke-virtual {p0, p1}, LX/1Dg;->av(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public K(II)LX/1Dg;
    .locals 4
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218409
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/16 v2, 0x800

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218410
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-static {p1}, Lcom/facebook/csslayout/YogaEdge;->fromInt(I)Lcom/facebook/csslayout/YogaEdge;

    move-result-object v1

    int-to-float v2, p2

    invoke-interface {v0, v1, v2}, LX/1mn;->d(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218411
    return-object p0
.end method

.method public synthetic K(I)LX/1Dh;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218412
    invoke-virtual {p0, p1}, LX/1Dg;->ax(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public L(II)LX/1Dg;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218413
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p2}, LX/1Dp;->f(I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, LX/1Dg;->K(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic L(I)LX/1Dh;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218414
    invoke-virtual {p0, p1}, LX/1Dg;->ay(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public M(II)LX/1Dg;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218415
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p2}, LX/1Dp;->a(I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, LX/1Dg;->K(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic M(I)LX/1Dh;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218416
    invoke-virtual {p0, p1}, LX/1Dg;->az(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final M()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 218417
    iget-object v0, p0, LX/1Dg;->K:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public N(II)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218418
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p1, p2}, LX/1Dp;->b(II)I

    move-result v0

    invoke-virtual {p0, v0}, LX/1Dg;->am(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic N(I)LX/1Dh;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218419
    invoke-virtual {p0, p1}, LX/1Dg;->aA(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final N()LX/1dQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dQ",
            "<",
            "LX/32D;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218420
    iget-object v0, p0, LX/1Dg;->C:LX/1dQ;

    return-object v0
.end method

.method public O(II)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218397
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p1, p2}, LX/1Dp;->b(II)I

    move-result v0

    invoke-virtual {p0, v0}, LX/1Dg;->au(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic O(I)LX/1Dh;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218422
    invoke-virtual {p0, p1}, LX/1Dg;->aC(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final O()LX/1dQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dQ",
            "<",
            "LX/32A;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218423
    iget-object v0, p0, LX/1Dg;->D:LX/1dQ;

    return-object v0
.end method

.method public P(II)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 218424
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p1, p2}, LX/1Dp;->e(II)I

    move-result v0

    invoke-virtual {p0, v0}, LX/1Dg;->aE(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic P(I)LX/1Dh;
    .locals 1

    .prologue
    .line 218425
    invoke-virtual {p0, p1}, LX/1Dg;->ad(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final P()LX/1dQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dQ",
            "<",
            "LX/3CI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218426
    iget-object v0, p0, LX/1Dg;->F:LX/1dQ;

    return-object v0
.end method

.method public synthetic Q(I)LX/1Dh;
    .locals 1

    .prologue
    .line 218427
    invoke-virtual {p0, p1}, LX/1Dg;->ac(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final Q()LX/1dQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dQ",
            "<",
            "LX/48D;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218428
    iget-object v0, p0, LX/1Dg;->E:LX/1dQ;

    return-object v0
.end method

.method public synthetic R(I)LX/1Dh;
    .locals 1

    .prologue
    .line 218429
    invoke-virtual {p0, p1}, LX/1Dg;->ae(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final R()LX/1dQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dQ",
            "<",
            "LX/2uO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218430
    iget-object v0, p0, LX/1Dg;->G:LX/1dQ;

    return-object v0
.end method

.method public synthetic S(I)LX/1Dh;
    .locals 1

    .prologue
    .line 218431
    invoke-virtual {p0, p1}, LX/1Dg;->af(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final S()LX/1dQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dQ",
            "<",
            "LX/48E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218432
    iget-object v0, p0, LX/1Dg;->H:LX/1dQ;

    return-object v0
.end method

.method public synthetic T(I)LX/1Dh;
    .locals 1

    .prologue
    .line 218433
    invoke-virtual {p0, p1}, LX/1Dg;->ag(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final T()LX/1dQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dQ",
            "<",
            "LX/48F;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218434
    iget-object v0, p0, LX/1Dg;->I:LX/1dQ;

    return-object v0
.end method

.method public synthetic U(I)LX/1Dh;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param

    .prologue
    .line 218435
    invoke-virtual {p0, p1}, LX/1Dg;->aD(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final U()LX/1dQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dQ",
            "<",
            "LX/48G;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218714
    iget-object v0, p0, LX/1Dg;->J:LX/1dQ;

    return-object v0
.end method

.method public synthetic V(I)LX/1Dh;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 218598
    invoke-virtual {p0, p1}, LX/1Dg;->aE(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final V()Ljava/lang/String;
    .locals 1

    .prologue
    .line 218715
    iget-object v0, p0, LX/1Dg;->u:Ljava/lang/String;

    return-object v0
.end method

.method public synthetic W(I)LX/1Dh;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 218716
    invoke-virtual {p0, p1}, LX/1Dg;->aF(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final W()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 218717
    iget-object v0, p0, LX/1Dg;->L:Ljava/lang/Object;

    return-object v0
.end method

.method public synthetic X(I)LX/1Dh;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 218718
    invoke-virtual {p0, p1}, LX/1Dg;->aG(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final X()Landroid/util/SparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218719
    iget-object v0, p0, LX/1Dg;->M:Landroid/util/SparseArray;

    return-object v0
.end method

.method public synthetic Y(I)LX/1Dh;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 218720
    invoke-virtual {p0, p1}, LX/1Dg;->aI(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final Y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 218721
    iget-object v0, p0, LX/1Dg;->N:Ljava/lang/String;

    return-object v0
.end method

.method public Z(I)V
    .locals 0

    .prologue
    .line 218722
    iput p1, p0, LX/1Dg;->ae:I

    .line 218723
    return-void
.end method

.method public final Z()Z
    .locals 1

    .prologue
    .line 218724
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v0}, LX/1mn;->d()Z

    move-result v0

    return v0
.end method

.method public a()I
    .locals 1
    .annotation build Landroid/support/annotation/Px;
    .end annotation

    .prologue
    .line 218725
    iget v0, p0, LX/1Dg;->Z:F

    invoke-static {v0}, LX/1mo;->a(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218726
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v0}, LX/1mn;->k()F

    move-result v0

    iput v0, p0, LX/1Dg;->Z:F

    .line 218727
    :cond_0
    iget v0, p0, LX/1Dg;->Z:F

    float-to-int v0, v0

    return v0
.end method

.method public a(III)LX/1Dg;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218728
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p2, p3}, LX/1Dp;->c(II)I

    move-result v0

    invoke-virtual {p0, p1, v0}, LX/1Dg;->A(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(LX/1Dg;)LX/1Dh;
    .locals 1

    .prologue
    .line 218729
    invoke-virtual {p0, p1}, LX/1Dg;->b(LX/1Dg;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(LX/1Di;)LX/1Dh;
    .locals 1

    .prologue
    .line 218730
    invoke-virtual {p0, p1}, LX/1Dg;->b(LX/1Di;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(LX/1X1;)LX/1Dh;
    .locals 1

    .prologue
    .line 218747
    invoke-virtual {p0, p1}, LX/1Dg;->b(LX/1X1;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(LX/1X5;)LX/1Dh;
    .locals 1

    .prologue
    .line 218731
    invoke-virtual {p0, p1}, LX/1Dg;->b(LX/1X5;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(F)LX/1Di;
    .locals 1

    .prologue
    .line 218754
    invoke-virtual {p0, p1}, LX/1Dg;->i(F)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(I)LX/1Di;
    .locals 1

    .prologue
    .line 218753
    invoke-virtual {p0, p1}, LX/1Dg;->ab(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(II)LX/1Di;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218752
    invoke-virtual {p0, p1, p2}, LX/1Dg;->A(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(LX/1dQ;)LX/1Di;
    .locals 1

    .prologue
    .line 218751
    invoke-virtual {p0, p1}, LX/1Dg;->g(LX/1dQ;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(LX/1dc;)LX/1Di;
    .locals 1

    .prologue
    .line 218750
    invoke-virtual {p0, p1}, LX/1Dg;->c(LX/1dc;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(LX/1n6;)LX/1Di;
    .locals 1

    .prologue
    .line 218755
    invoke-virtual {p0, p1}, LX/1Dg;->d(LX/1n6;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Landroid/util/SparseArray;)LX/1Di;
    .locals 1

    .prologue
    .line 218749
    invoke-virtual {p0, p1}, LX/1Dg;->c(Landroid/util/SparseArray;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/CharSequence;)LX/1Di;
    .locals 1

    .prologue
    .line 218748
    invoke-virtual {p0, p1}, LX/1Dg;->c(Ljava/lang/CharSequence;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Z)LX/1Di;
    .locals 1

    .prologue
    .line 218185
    invoke-virtual {p0, p1}, LX/1Dg;->f(Z)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public a(LX/1Dg;)V
    .locals 0

    .prologue
    .line 218745
    iput-object p1, p0, LX/1Dg;->p:LX/1Dg;

    .line 218746
    return-void
.end method

.method public a(LX/1ml;)V
    .locals 0

    .prologue
    .line 218743
    iput-object p1, p0, LX/1Dg;->ai:LX/1ml;

    .line 218744
    return-void
.end method

.method public a(LX/1mn;LX/1De;Landroid/content/res/Resources;)V
    .locals 3

    .prologue
    .line 218733
    invoke-interface {p1, p0}, LX/1mn;->a(Ljava/lang/Object;)V

    .line 218734
    sget-object v0, Lcom/facebook/csslayout/YogaOverflow;->HIDDEN:Lcom/facebook/csslayout/YogaOverflow;

    invoke-interface {p1, v0}, LX/1mn;->a(Lcom/facebook/csslayout/YogaOverflow;)V

    .line 218735
    const/4 v0, 0x0

    invoke-interface {p1, v0}, LX/1mn;->a(Lcom/facebook/csslayout/YogaMeasureFunction;)V

    .line 218736
    iput-object p1, p0, LX/1Dg;->a:LX/1mn;

    .line 218737
    iput-object p2, p0, LX/1Dg;->j:LX/1De;

    .line 218738
    iput-object p3, p0, LX/1Dg;->k:Landroid/content/res/Resources;

    .line 218739
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    iget-object v1, p0, LX/1Dg;->j:LX/1De;

    .line 218740
    iget-object v2, p2, LX/1De;->h:LX/1Ds;

    move-object v2, v2

    .line 218741
    invoke-virtual {v0, v1, v2}, LX/1Dp;->a(LX/1De;LX/1Ds;)V

    .line 218742
    return-void
.end method

.method public a(Landroid/content/res/TypedArray;)V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v1, 0x0

    .line 218633
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_23

    .line 218634
    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v3

    .line 218635
    const/16 v4, 0x7

    if-ne v3, v4, :cond_1

    .line 218636
    invoke-virtual {p1, v3, v6}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v3

    .line 218637
    if-ltz v3, :cond_0

    .line 218638
    invoke-virtual {p0, v3}, LX/1Dg;->am(I)LX/1Dg;

    .line 218639
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 218640
    :cond_1
    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    .line 218641
    invoke-virtual {p1, v3, v6}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v3

    .line 218642
    if-ltz v3, :cond_0

    .line 218643
    invoke-virtual {p0, v3}, LX/1Dg;->au(I)LX/1Dg;

    goto :goto_1

    .line 218644
    :cond_2
    const/16 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 218645
    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    invoke-virtual {p0, v1, v3}, LX/1Dg;->E(II)LX/1Dg;

    goto :goto_1

    .line 218646
    :cond_3
    const/16 v4, 0x3

    if-ne v3, v4, :cond_4

    .line 218647
    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    invoke-virtual {p0, v7, v3}, LX/1Dg;->E(II)LX/1Dg;

    goto :goto_1

    .line 218648
    :cond_4
    const/16 v4, 0x4

    if-ne v3, v4, :cond_5

    .line 218649
    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    invoke-virtual {p0, v8, v3}, LX/1Dg;->E(II)LX/1Dg;

    goto :goto_1

    .line 218650
    :cond_5
    const/16 v4, 0x5

    if-ne v3, v4, :cond_6

    .line 218651
    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    invoke-virtual {p0, v9, v3}, LX/1Dg;->E(II)LX/1Dg;

    goto :goto_1

    .line 218652
    :cond_6
    const/16 v4, 0x11

    if-ne v3, v4, :cond_7

    sget-boolean v4, LX/1Dg;->b:Z

    if-eqz v4, :cond_7

    .line 218653
    const/4 v4, 0x4

    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    invoke-virtual {p0, v4, v3}, LX/1Dg;->E(II)LX/1Dg;

    goto :goto_1

    .line 218654
    :cond_7
    const/16 v4, 0x12

    if-ne v3, v4, :cond_8

    sget-boolean v4, LX/1Dg;->b:Z

    if-eqz v4, :cond_8

    .line 218655
    const/4 v4, 0x5

    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    invoke-virtual {p0, v4, v3}, LX/1Dg;->E(II)LX/1Dg;

    goto :goto_1

    .line 218656
    :cond_8
    const/16 v4, 0x1

    if-ne v3, v4, :cond_9

    .line 218657
    const/16 v4, 0x8

    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    invoke-virtual {p0, v4, v3}, LX/1Dg;->E(II)LX/1Dg;

    goto :goto_1

    .line 218658
    :cond_9
    const/16 v4, 0xa

    if-ne v3, v4, :cond_a

    .line 218659
    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    invoke-virtual {p0, v1, v3}, LX/1Dg;->A(II)LX/1Dg;

    goto :goto_1

    .line 218660
    :cond_a
    const/16 v4, 0xb

    if-ne v3, v4, :cond_b

    .line 218661
    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    invoke-virtual {p0, v7, v3}, LX/1Dg;->A(II)LX/1Dg;

    goto/16 :goto_1

    .line 218662
    :cond_b
    const/16 v4, 0xc

    if-ne v3, v4, :cond_c

    .line 218663
    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    invoke-virtual {p0, v8, v3}, LX/1Dg;->A(II)LX/1Dg;

    goto/16 :goto_1

    .line 218664
    :cond_c
    const/16 v4, 0xd

    if-ne v3, v4, :cond_d

    .line 218665
    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    invoke-virtual {p0, v9, v3}, LX/1Dg;->A(II)LX/1Dg;

    goto/16 :goto_1

    .line 218666
    :cond_d
    const/16 v4, 0x13

    if-ne v3, v4, :cond_e

    sget-boolean v4, LX/1Dg;->b:Z

    if-eqz v4, :cond_e

    .line 218667
    const/4 v4, 0x4

    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    invoke-virtual {p0, v4, v3}, LX/1Dg;->A(II)LX/1Dg;

    goto/16 :goto_1

    .line 218668
    :cond_e
    const/16 v4, 0x14

    if-ne v3, v4, :cond_f

    sget-boolean v4, LX/1Dg;->b:Z

    if-eqz v4, :cond_f

    .line 218669
    const/4 v4, 0x5

    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    invoke-virtual {p0, v4, v3}, LX/1Dg;->A(II)LX/1Dg;

    goto/16 :goto_1

    .line 218670
    :cond_f
    const/16 v4, 0x9

    if-ne v3, v4, :cond_10

    .line 218671
    const/16 v4, 0x8

    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    invoke-virtual {p0, v4, v3}, LX/1Dg;->A(II)LX/1Dg;

    goto/16 :goto_1

    .line 218672
    :cond_10
    const/16 v4, 0x10

    if-ne v3, v4, :cond_11

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v4, v5, :cond_11

    .line 218673
    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    invoke-virtual {p0, v3}, LX/1Dg;->al(I)LX/1Dg;

    goto/16 :goto_1

    .line 218674
    :cond_11
    const/16 v4, 0x6

    if-ne v3, v4, :cond_12

    .line 218675
    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    invoke-virtual {p0, v3}, LX/1Dg;->f(Z)LX/1Dg;

    goto/16 :goto_1

    .line 218676
    :cond_12
    const/16 v4, 0x0

    if-ne v3, v4, :cond_14

    .line 218677
    const/16 v4, 0x0

    invoke-static {p1, v4}, LX/48M;->a(Landroid/content/res/TypedArray;I)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 218678
    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {p0, v3}, LX/1Dg;->aF(I)LX/1Dg;

    goto/16 :goto_1

    .line 218679
    :cond_13
    invoke-virtual {p1, v3, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    invoke-virtual {p0, v3}, LX/1Dg;->aE(I)LX/1Dg;

    goto/16 :goto_1

    .line 218680
    :cond_14
    const/16 v4, 0xe

    if-ne v3, v4, :cond_16

    .line 218681
    const/16 v4, 0xe

    invoke-static {p1, v4}, LX/48M;->a(Landroid/content/res/TypedArray;I)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 218682
    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {p0, v3}, LX/1Dg;->aH(I)LX/1Dg;

    goto/16 :goto_1

    .line 218683
    :cond_15
    invoke-virtual {p1, v3, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    invoke-virtual {p0, v3}, LX/1Dg;->aG(I)LX/1Dg;

    goto/16 :goto_1

    .line 218684
    :cond_16
    const/16 v4, 0xf

    if-ne v3, v4, :cond_17

    .line 218685
    invoke-virtual {p1, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/1Dg;->c(Ljava/lang/CharSequence;)LX/1Dg;

    goto/16 :goto_1

    .line 218686
    :cond_17
    const/16 v4, 0x15

    if-ne v3, v4, :cond_18

    .line 218687
    sget-object v4, LX/1Dg;->e:[I

    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    aget v3, v4, v3

    invoke-virtual {p0, v3}, LX/1Dg;->ac(I)LX/1Dg;

    goto/16 :goto_1

    .line 218688
    :cond_18
    const/16 v4, 0x1b

    if-ne v3, v4, :cond_19

    .line 218689
    sget-object v4, LX/1Dg;->h:[I

    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    aget v3, v4, v3

    invoke-virtual {p0, v3}, LX/1Dg;->ad(I)LX/1Dg;

    goto/16 :goto_1

    .line 218690
    :cond_19
    const/16 v4, 0x17

    if-ne v3, v4, :cond_1a

    .line 218691
    sget-object v4, LX/1Dg;->d:[I

    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    aget v3, v4, v3

    invoke-virtual {p0, v3}, LX/1Dg;->ae(I)LX/1Dg;

    goto/16 :goto_1

    .line 218692
    :cond_1a
    const/16 v4, 0x1a

    if-ne v3, v4, :cond_1b

    .line 218693
    sget-object v4, LX/1Dg;->c:[I

    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    aget v3, v4, v3

    invoke-virtual {p0, v3}, LX/1Dg;->af(I)LX/1Dg;

    goto/16 :goto_1

    .line 218694
    :cond_1b
    const/16 v4, 0x19

    if-ne v3, v4, :cond_1c

    .line 218695
    sget-object v4, LX/1Dg;->c:[I

    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    aget v3, v4, v3

    invoke-virtual {p0, v3}, LX/1Dg;->ah(I)LX/1Dg;

    goto/16 :goto_1

    .line 218696
    :cond_1c
    const/16 v4, 0x18

    if-ne v3, v4, :cond_1d

    .line 218697
    sget-object v4, LX/1Dg;->f:[I

    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    aget v3, v4, v3

    invoke-virtual {p0, v3}, LX/1Dg;->ai(I)LX/1Dg;

    goto/16 :goto_1

    .line 218698
    :cond_1d
    const/16 v4, 0x20

    if-ne v3, v4, :cond_1e

    .line 218699
    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {p1, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    .line 218700
    const/4 v4, 0x0

    cmpl-float v4, v3, v4

    if-ltz v4, :cond_0

    .line 218701
    invoke-virtual {p0, v3}, LX/1Dg;->i(F)LX/1Dg;

    goto/16 :goto_1

    .line 218702
    :cond_1e
    const/16 v4, 0x1c

    if-ne v3, v4, :cond_1f

    .line 218703
    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    invoke-virtual {p0, v1, v3}, LX/1Dg;->K(II)LX/1Dg;

    goto/16 :goto_1

    .line 218704
    :cond_1f
    const/16 v4, 0x1d

    if-ne v3, v4, :cond_20

    .line 218705
    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    invoke-virtual {p0, v7, v3}, LX/1Dg;->K(II)LX/1Dg;

    goto/16 :goto_1

    .line 218706
    :cond_20
    const/16 v4, 0x1e

    if-ne v3, v4, :cond_21

    .line 218707
    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    invoke-virtual {p0, v8, v3}, LX/1Dg;->K(II)LX/1Dg;

    goto/16 :goto_1

    .line 218708
    :cond_21
    const/16 v4, 0x1f

    if-ne v3, v4, :cond_22

    .line 218709
    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    invoke-virtual {p0, v9, v3}, LX/1Dg;->K(II)LX/1Dg;

    goto/16 :goto_1

    .line 218710
    :cond_22
    const/16 v4, 0x16

    if-ne v3, v4, :cond_0

    .line 218711
    invoke-virtual {p1, v3, v6}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    .line 218712
    sget-object v4, LX/1Dg;->g:[I

    aget v3, v4, v3

    invoke-virtual {p0, v3}, LX/1Dg;->ab(I)LX/1Dg;

    goto/16 :goto_1

    .line 218713
    :cond_23
    return-void
.end method

.method public aA(I)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218732
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p1}, LX/1Dp;->a(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/1Dg;->ay(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public aB(I)LX/1Dg;
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218564
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x20000

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218565
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    int-to-float v1, p1

    invoke-interface {v0, v1}, LX/1mn;->j(F)V

    .line 218566
    return-object p0
.end method

.method public aC(I)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218567
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p1}, LX/1Dp;->a(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/1Dg;->aB(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public aD(I)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param

    .prologue
    .line 218568
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/1Dg;->P(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public aE(I)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 218569
    if-nez p1, :cond_0

    .line 218570
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/1Dg;->c(LX/1dc;)LX/1Dg;

    move-result-object v0

    .line 218571
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1Dg;->j:LX/1De;

    invoke-static {v0}, LX/1ni;->a(LX/1De;)LX/1nm;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1nm;->h(I)LX/1nm;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1Dg;->c(LX/1dc;)LX/1Dg;

    move-result-object v0

    goto :goto_0
.end method

.method public aF(I)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 218572
    iget-object v0, p0, LX/1Dg;->j:LX/1De;

    invoke-static {v0}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1nh;->h(I)LX/1nh;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1Dg;->c(LX/1dc;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public aG(I)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 218573
    if-nez p1, :cond_0

    .line 218574
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/1Dg;->d(LX/1dc;)LX/1Dg;

    move-result-object v0

    .line 218575
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1Dg;->j:LX/1De;

    invoke-static {v0}, LX/1ni;->a(LX/1De;)LX/1nm;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1nm;->h(I)LX/1nm;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1Dg;->d(LX/1dc;)LX/1Dg;

    move-result-object v0

    goto :goto_0
.end method

.method public aH(I)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 218576
    iget-object v0, p0, LX/1Dg;->j:LX/1De;

    invoke-static {v0}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1nh;->h(I)LX/1nh;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1Dg;->d(LX/1dc;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public aI(I)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 218577
    iget-object v0, p0, LX/1Dg;->k:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1Dg;->c(Ljava/lang/CharSequence;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final aJ(I)LX/1Dg;
    .locals 1

    .prologue
    .line 218578
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v0, p1}, LX/1mn;->c(I)LX/1mn;

    move-result-object v0

    if-nez v0, :cond_0

    .line 218579
    const/4 v0, 0x0

    .line 218580
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v0, p1}, LX/1mn;->c(I)LX/1mn;

    move-result-object v0

    invoke-interface {v0}, LX/1mn;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Dg;

    goto :goto_0
.end method

.method public aL(I)V
    .locals 2

    .prologue
    .line 218581
    invoke-static {p1}, LX/1mh;->a(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 218582
    :goto_0
    return-void

    .line 218583
    :sswitch_0
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    const/high16 v1, 0x7fc00000    # NaNf

    invoke-interface {v0, v1}, LX/1mn;->e(F)V

    goto :goto_0

    .line 218584
    :sswitch_1
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-static {p1}, LX/1mh;->b(I)I

    move-result v1

    int-to-float v1, v1

    invoke-interface {v0, v1}, LX/1mn;->i(F)V

    goto :goto_0

    .line 218585
    :sswitch_2
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-static {p1}, LX/1mh;->b(I)I

    move-result v1

    int-to-float v1, v1

    invoke-interface {v0, v1}, LX/1mn;->e(F)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method public aM(I)V
    .locals 2

    .prologue
    .line 218586
    invoke-static {p1}, LX/1mh;->a(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 218587
    :goto_0
    return-void

    .line 218588
    :sswitch_0
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    const/high16 v1, 0x7fc00000    # NaNf

    invoke-interface {v0, v1}, LX/1mn;->f(F)V

    goto :goto_0

    .line 218589
    :sswitch_1
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-static {p1}, LX/1mh;->b(I)I

    move-result v1

    int-to-float v1, v1

    invoke-interface {v0, v1}, LX/1mn;->j(F)V

    goto :goto_0

    .line 218590
    :sswitch_2
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-static {p1}, LX/1mh;->b(I)I

    move-result v1

    int-to-float v1, v1

    invoke-interface {v0, v1}, LX/1mn;->f(F)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method public final aa()V
    .locals 1

    .prologue
    .line 218591
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v0}, LX/1mn;->g()V

    .line 218592
    return-void
.end method

.method public aa(I)V
    .locals 0

    .prologue
    .line 218593
    iput p1, p0, LX/1Dg;->af:I

    .line 218594
    return-void
.end method

.method public ab(I)LX/1Dg;
    .locals 4

    .prologue
    .line 218595
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/16 v2, 0x1

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218596
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-static {p1}, LX/1Dn;->a(I)Lcom/facebook/csslayout/YogaDirection;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1mn;->a(Lcom/facebook/csslayout/YogaDirection;)V

    .line 218597
    return-object p0
.end method

.method public ac(I)LX/1Dg;
    .locals 2

    .prologue
    .line 218560
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    .line 218561
    sget-object v1, LX/1Dl;->a:[Lcom/facebook/csslayout/YogaFlexDirection;

    aget-object v1, v1, p1

    move-object v1, v1

    .line 218562
    invoke-interface {v0, v1}, LX/1mn;->a(Lcom/facebook/csslayout/YogaFlexDirection;)V

    .line 218563
    return-object p0
.end method

.method public ad(I)LX/1Dg;
    .locals 2

    .prologue
    .line 218599
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    .line 218600
    sget-object v1, LX/1Do;->a:[Lcom/facebook/csslayout/YogaWrap;

    aget-object v1, v1, p1

    move-object v1, v1

    .line 218601
    invoke-interface {v0, v1}, LX/1mn;->a(Lcom/facebook/csslayout/YogaWrap;)V

    .line 218602
    return-object p0
.end method

.method public final ae()I
    .locals 1

    .prologue
    .line 218603
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v0}, LX/1mn;->b()I

    move-result v0

    return v0
.end method

.method public ae(I)LX/1Dg;
    .locals 2

    .prologue
    .line 218604
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    .line 218605
    sget-object v1, LX/1Dk;->a:[Lcom/facebook/csslayout/YogaJustify;

    aget-object v1, v1, p1

    move-object v1, v1

    .line 218606
    invoke-interface {v0, v1}, LX/1mn;->a(Lcom/facebook/csslayout/YogaJustify;)V

    .line 218607
    return-object p0
.end method

.method public af(I)LX/1Dg;
    .locals 2

    .prologue
    .line 218608
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-static {p1}, LX/1Dj;->a(I)Lcom/facebook/csslayout/YogaAlign;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1mn;->a(Lcom/facebook/csslayout/YogaAlign;)V

    .line 218609
    return-object p0
.end method

.method public ag(I)LX/1Dg;
    .locals 2

    .prologue
    .line 218610
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-static {p1}, LX/1Dj;->a(I)Lcom/facebook/csslayout/YogaAlign;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1mn;->c(Lcom/facebook/csslayout/YogaAlign;)V

    .line 218611
    return-object p0
.end method

.method public ah(I)LX/1Dg;
    .locals 4

    .prologue
    .line 218612
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/16 v2, 0x2

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218613
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-static {p1}, LX/1Dj;->a(I)Lcom/facebook/csslayout/YogaAlign;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1mn;->b(Lcom/facebook/csslayout/YogaAlign;)V

    .line 218614
    return-object p0
.end method

.method public ai(I)LX/1Dg;
    .locals 4

    .prologue
    .line 218615
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/16 v2, 0x4

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218616
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    .line 218617
    sget-object v1, LX/1Dm;->a:[Lcom/facebook/csslayout/YogaPositionType;

    aget-object v1, v1, p1

    move-object v1, v1

    .line 218618
    invoke-interface {v0, v1}, LX/1mn;->a(Lcom/facebook/csslayout/YogaPositionType;)V

    .line 218619
    return-object p0
.end method

.method public final ai()LX/1X1;
    .locals 1

    .prologue
    .line 218620
    iget-object v0, p0, LX/1Dg;->l:LX/1X1;

    return-object v0
.end method

.method public aj(I)LX/1Dg;
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218621
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/16 v2, 0x40

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218622
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    int-to-float v1, p1

    invoke-interface {v0, v1}, LX/1mn;->d(F)V

    .line 218623
    return-object p0
.end method

.method public ak(I)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218624
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p1}, LX/1Dp;->a(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/1Dg;->aj(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final al()I
    .locals 1

    .prologue
    .line 218625
    iget v0, p0, LX/1Dg;->m:I

    return v0
.end method

.method public al(I)LX/1Dg;
    .locals 4

    .prologue
    .line 218626
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/16 v2, 0x80

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218627
    iput p1, p0, LX/1Dg;->m:I

    .line 218628
    return-object p0
.end method

.method public am(I)LX/1Dg;
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218629
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/16 v2, 0x1000

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218630
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    int-to-float v1, p1

    invoke-interface {v0, v1}, LX/1mn;->e(F)V

    .line 218631
    return-object p0
.end method

.method public final am()Z
    .locals 1

    .prologue
    .line 218534
    iget-boolean v0, p0, LX/1Dg;->n:Z

    return v0
.end method

.method public an(I)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218632
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p1}, LX/1Dp;->e(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/1Dg;->am(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final an()V
    .locals 7

    .prologue
    const/high16 v5, -0x40800000    # -1.0f

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/high16 v1, 0x7fc00000    # NaNf

    const/4 v2, 0x0

    .line 218118
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v0}, LX/1mn;->r()LX/1mn;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v0}, LX/1mn;->b()I

    move-result v0

    if-lez v0, :cond_1

    .line 218119
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You should not free an attached Internalnode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218120
    :cond_1
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    .line 218121
    invoke-interface {v0}, LX/1mn;->a()V

    .line 218122
    sget-object v6, LX/1cy;->f:LX/0Zi;

    invoke-virtual {v6, v0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 218123
    iput-object v2, p0, LX/1Dg;->a:LX/1mn;

    .line 218124
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0}, LX/1Dp;->e()V

    .line 218125
    iput v1, p0, LX/1Dg;->R:F

    .line 218126
    iput v1, p0, LX/1Dg;->S:F

    .line 218127
    iput v1, p0, LX/1Dg;->T:F

    .line 218128
    iput v1, p0, LX/1Dg;->U:F

    .line 218129
    iput v1, p0, LX/1Dg;->V:F

    .line 218130
    iput v1, p0, LX/1Dg;->W:F

    .line 218131
    iput v1, p0, LX/1Dg;->X:F

    .line 218132
    iput v1, p0, LX/1Dg;->Y:F

    .line 218133
    iput v1, p0, LX/1Dg;->Z:F

    .line 218134
    iput v1, p0, LX/1Dg;->aa:F

    .line 218135
    iput v1, p0, LX/1Dg;->ab:F

    .line 218136
    iput v1, p0, LX/1Dg;->ac:F

    .line 218137
    iput v4, p0, LX/1Dg;->ad:I

    .line 218138
    iput-object v2, p0, LX/1Dg;->j:LX/1De;

    .line 218139
    iput-object v2, p0, LX/1Dg;->k:Landroid/content/res/Resources;

    .line 218140
    iput-object v2, p0, LX/1Dg;->l:LX/1X1;

    .line 218141
    iput-object v2, p0, LX/1Dg;->p:LX/1Dg;

    .line 218142
    iput v3, p0, LX/1Dg;->m:I

    .line 218143
    iput-boolean v3, p0, LX/1Dg;->n:Z

    .line 218144
    iput-object v2, p0, LX/1Dg;->r:LX/1dc;

    .line 218145
    iput-object v2, p0, LX/1Dg;->s:LX/1dc;

    .line 218146
    iput-boolean v3, p0, LX/1Dg;->t:Z

    .line 218147
    iput-object v2, p0, LX/1Dg;->v:LX/1dQ;

    .line 218148
    iput-object v2, p0, LX/1Dg;->w:LX/1dQ;

    .line 218149
    iput-object v2, p0, LX/1Dg;->x:LX/1dQ;

    .line 218150
    iput-object v2, p0, LX/1Dg;->y:LX/1dQ;

    .line 218151
    iput-object v2, p0, LX/1Dg;->z:LX/1dQ;

    .line 218152
    iput-object v2, p0, LX/1Dg;->A:LX/1dQ;

    .line 218153
    iput-object v2, p0, LX/1Dg;->B:LX/1dQ;

    .line 218154
    iput-object v2, p0, LX/1Dg;->C:LX/1dQ;

    .line 218155
    iput-object v2, p0, LX/1Dg;->D:LX/1dQ;

    .line 218156
    iput-object v2, p0, LX/1Dg;->E:LX/1dQ;

    .line 218157
    iput-object v2, p0, LX/1Dg;->F:LX/1dQ;

    .line 218158
    iput-object v2, p0, LX/1Dg;->G:LX/1dQ;

    .line 218159
    iput-object v2, p0, LX/1Dg;->H:LX/1dQ;

    .line 218160
    iput-object v2, p0, LX/1Dg;->I:LX/1dQ;

    .line 218161
    iput-object v2, p0, LX/1Dg;->J:LX/1dQ;

    .line 218162
    iput-object v2, p0, LX/1Dg;->K:Ljava/lang/CharSequence;

    .line 218163
    iput-object v2, p0, LX/1Dg;->L:Ljava/lang/Object;

    .line 218164
    iput-object v2, p0, LX/1Dg;->M:Landroid/util/SparseArray;

    .line 218165
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218166
    iput-object v2, p0, LX/1Dg;->u:Ljava/lang/String;

    .line 218167
    iget-object v0, p0, LX/1Dg;->O:LX/1mz;

    if-eqz v0, :cond_2

    .line 218168
    iget-object v0, p0, LX/1Dg;->O:LX/1mz;

    invoke-static {v0}, LX/1cy;->a(LX/1mz;)V

    .line 218169
    iput-object v2, p0, LX/1Dg;->O:LX/1mz;

    .line 218170
    :cond_2
    iget-object v0, p0, LX/1Dg;->P:LX/1mz;

    if-eqz v0, :cond_3

    .line 218171
    iget-object v0, p0, LX/1Dg;->P:LX/1mz;

    invoke-static {v0}, LX/1cy;->a(LX/1mz;)V

    .line 218172
    iput-object v2, p0, LX/1Dg;->P:LX/1mz;

    .line 218173
    :cond_3
    iget-object v0, p0, LX/1Dg;->Q:LX/1mz;

    if-eqz v0, :cond_4

    .line 218174
    iget-object v0, p0, LX/1Dg;->Q:LX/1mz;

    invoke-static {v0}, LX/1cy;->a(LX/1mz;)V

    .line 218175
    iput-object v2, p0, LX/1Dg;->Q:LX/1mz;

    .line 218176
    :cond_4
    iput v4, p0, LX/1Dg;->ae:I

    .line 218177
    iput v4, p0, LX/1Dg;->af:I

    .line 218178
    iput v5, p0, LX/1Dg;->ah:F

    .line 218179
    iput v5, p0, LX/1Dg;->ag:F

    .line 218180
    iput-object v2, p0, LX/1Dg;->ai:LX/1ml;

    .line 218181
    iput-boolean v3, p0, LX/1Dg;->aj:Z

    .line 218182
    iput-boolean v3, p0, LX/1Dg;->o:Z

    .line 218183
    iput-object v2, p0, LX/1Dg;->N:Ljava/lang/String;

    .line 218184
    return-void
.end method

.method public ao(I)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param

    .prologue
    .line 218186
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/1Dg;->N(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public ap(I)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218187
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p1}, LX/1Dp;->a(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/1Dg;->am(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public aq(I)LX/1Dg;
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218188
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/16 v2, 0x2000

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218189
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    int-to-float v1, p1

    invoke-interface {v0, v1}, LX/1mn;->g(F)V

    .line 218190
    return-object p0
.end method

.method public ar(I)LX/1Dg;
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218191
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/16 v2, 0x4000

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218192
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    int-to-float v1, p1

    invoke-interface {v0, v1}, LX/1mn;->i(F)V

    .line 218193
    return-object p0
.end method

.method public as(I)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218194
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p1}, LX/1Dp;->e(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/1Dg;->ar(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public at(I)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218195
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p1}, LX/1Dp;->a(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/1Dg;->ar(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public au(I)LX/1Dg;
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218196
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x8000

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218197
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    int-to-float v1, p1

    invoke-interface {v0, v1}, LX/1mn;->f(F)V

    .line 218198
    return-object p0
.end method

.method public av(I)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218199
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p1}, LX/1Dp;->e(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/1Dg;->au(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public aw(I)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param

    .prologue
    .line 218200
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/1Dg;->O(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public ax(I)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218201
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p1}, LX/1Dp;->a(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/1Dg;->au(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public ay(I)LX/1Dg;
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218202
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x10000

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218203
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    int-to-float v1, p1

    invoke-interface {v0, v1}, LX/1mn;->h(F)V

    .line 218204
    return-object p0
.end method

.method public az(I)LX/1Dg;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218205
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p1}, LX/1Dp;->e(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/1Dg;->ay(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 1
    .annotation build Landroid/support/annotation/Px;
    .end annotation

    .prologue
    .line 218115
    iget v0, p0, LX/1Dg;->aa:F

    invoke-static {v0}, LX/1mo;->a(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218116
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v0}, LX/1mn;->l()F

    move-result v0

    iput v0, p0, LX/1Dg;->aa:F

    .line 218117
    :cond_0
    iget v0, p0, LX/1Dg;->aa:F

    float-to-int v0, v0

    return v0
.end method

.method public b(III)LX/1Dg;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218291
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p2, p3}, LX/1Dp;->c(II)I

    move-result v0

    invoke-virtual {p0, p1, v0}, LX/1Dg;->E(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public b(LX/1Dg;)LX/1Dg;
    .locals 3

    .prologue
    .line 218292
    if-eqz p1, :cond_0

    sget-object v0, LX/1De;->a:LX/1Dg;

    if-eq p1, v0, :cond_0

    .line 218293
    check-cast p1, LX/1Dg;

    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v0}, LX/1mn;->b()I

    move-result v0

    .line 218294
    iget-object v1, p0, LX/1Dg;->a:LX/1mn;

    iget-object v2, p1, LX/1Dg;->a:LX/1mn;

    invoke-interface {v1, v2, v0}, LX/1mn;->a(LX/1mn;I)V

    .line 218295
    :cond_0
    return-object p0
.end method

.method public b(LX/1Di;)LX/1Dg;
    .locals 1

    .prologue
    .line 218296
    if-eqz p1, :cond_0

    sget-object v0, LX/1De;->a:LX/1Dg;

    if-eq p1, v0, :cond_0

    .line 218297
    invoke-interface {p1}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1Dg;->b(LX/1Dg;)LX/1Dg;

    .line 218298
    :cond_0
    return-object p0
.end method

.method public b(LX/1X1;)LX/1Dg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;)",
            "LX/1Dg;"
        }
    .end annotation

    .prologue
    .line 218299
    if-eqz p1, :cond_0

    .line 218300
    iget-object v0, p0, LX/1Dg;->j:LX/1De;

    invoke-static {v0, p1}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1Dg;->b(LX/1Di;)LX/1Dg;

    .line 218301
    :cond_0
    return-object p0
.end method

.method public b(LX/1X5;)LX/1Dg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X5",
            "<*>;)",
            "LX/1Dg;"
        }
    .end annotation

    .prologue
    .line 218302
    if-eqz p1, :cond_0

    .line 218303
    invoke-virtual {p1}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1Dg;->b(LX/1X1;)LX/1Dg;

    .line 218304
    :cond_0
    return-object p0
.end method

.method public synthetic b(LX/1dc;)LX/1Dh;
    .locals 1

    .prologue
    .line 218305
    invoke-virtual {p0, p1}, LX/1Dg;->c(LX/1dc;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Landroid/util/SparseArray;)LX/1Dh;
    .locals 1

    .prologue
    .line 218306
    invoke-virtual {p0, p1}, LX/1Dg;->c(Landroid/util/SparseArray;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/CharSequence;)LX/1Dh;
    .locals 1

    .prologue
    .line 218307
    invoke-virtual {p0, p1}, LX/1Dg;->c(Ljava/lang/CharSequence;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(F)LX/1Di;
    .locals 1

    .prologue
    .line 218308
    invoke-virtual {p0, p1}, LX/1Dg;->j(F)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(I)LX/1Di;
    .locals 1

    .prologue
    .line 218309
    invoke-virtual {p0, p1}, LX/1Dg;->ah(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(II)LX/1Di;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param

    .prologue
    .line 218310
    invoke-virtual {p0, p1, p2}, LX/1Dg;->B(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(LX/1dQ;)LX/1Di;
    .locals 1

    .prologue
    .line 218311
    invoke-direct {p0, p1}, LX/1Dg;->i(LX/1dQ;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(LX/1n6;)LX/1Di;
    .locals 1

    .prologue
    .line 218312
    invoke-virtual {p0, p1}, LX/1Dg;->e(LX/1n6;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Z)LX/1Di;
    .locals 1

    .prologue
    .line 218313
    invoke-virtual {p0, p1}, LX/1Dg;->d(Z)LX/1Dh;

    move-result-object v0

    return-object v0
.end method

.method public b(LX/1Dg;)V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    const-wide/16 v6, 0x0

    .line 218206
    iget-wide v0, p1, LX/1Dg;->q:J

    const-wide/16 v2, 0x1

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/1Dg;->i()I

    move-result v0

    if-nez v0, :cond_1

    .line 218207
    :cond_0
    invoke-virtual {p0}, LX/1Dg;->i()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1Dg;->ab(I)LX/1Dg;

    .line 218208
    :cond_1
    iget-wide v0, p1, LX/1Dg;->q:J

    const-wide/16 v2, 0x80

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_2

    iget v0, p1, LX/1Dg;->m:I

    if-nez v0, :cond_3

    .line 218209
    :cond_2
    iget v0, p0, LX/1Dg;->m:I

    iput v0, p1, LX/1Dg;->m:I

    .line 218210
    :cond_3
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/16 v2, 0x100

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_4

    .line 218211
    iget-boolean v0, p0, LX/1Dg;->n:Z

    iput-boolean v0, p1, LX/1Dg;->n:Z

    .line 218212
    :cond_4
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x40000

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_5

    .line 218213
    iget-object v0, p0, LX/1Dg;->r:LX/1dc;

    iput-object v0, p1, LX/1Dg;->r:LX/1dc;

    .line 218214
    :cond_5
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x80000

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_6

    .line 218215
    iget-object v0, p0, LX/1Dg;->s:LX/1dc;

    iput-object v0, p1, LX/1Dg;->s:LX/1dc;

    .line 218216
    :cond_6
    iget-boolean v0, p0, LX/1Dg;->t:Z

    if-eqz v0, :cond_7

    .line 218217
    iput-boolean v8, p1, LX/1Dg;->t:Z

    .line 218218
    :cond_7
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x100000

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_8

    .line 218219
    iget-object v0, p0, LX/1Dg;->v:LX/1dQ;

    iput-object v0, p1, LX/1Dg;->v:LX/1dQ;

    .line 218220
    :cond_8
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x200000

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_9

    .line 218221
    iget-object v0, p0, LX/1Dg;->w:LX/1dQ;

    iput-object v0, p1, LX/1Dg;->w:LX/1dQ;

    .line 218222
    :cond_9
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x8000000

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_a

    .line 218223
    iget-object v0, p0, LX/1Dg;->y:LX/1dQ;

    iput-object v0, p1, LX/1Dg;->y:LX/1dQ;

    .line 218224
    :cond_a
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x10000000

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_b

    .line 218225
    iget-object v0, p0, LX/1Dg;->z:LX/1dQ;

    iput-object v0, p1, LX/1Dg;->z:LX/1dQ;

    .line 218226
    :cond_b
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x20000000

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_c

    .line 218227
    iget-object v0, p0, LX/1Dg;->A:LX/1dQ;

    iput-object v0, p1, LX/1Dg;->A:LX/1dQ;

    .line 218228
    :cond_c
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x40000000

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_d

    .line 218229
    iget-object v0, p0, LX/1Dg;->B:LX/1dQ;

    iput-object v0, p1, LX/1Dg;->B:LX/1dQ;

    .line 218230
    :cond_d
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x400000

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_e

    .line 218231
    iget-object v0, p0, LX/1Dg;->K:Ljava/lang/CharSequence;

    iput-object v0, p1, LX/1Dg;->K:Ljava/lang/CharSequence;

    .line 218232
    :cond_e
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x2000000

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_f

    .line 218233
    invoke-virtual {p1, v8}, LX/1Dg;->d(Z)LX/1Dh;

    .line 218234
    :cond_f
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x4000000

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_10

    .line 218235
    invoke-virtual {p1, v9}, LX/1Dg;->d(Z)LX/1Dh;

    .line 218236
    :cond_10
    iget-object v0, p0, LX/1Dg;->L:Ljava/lang/Object;

    if-eqz v0, :cond_11

    .line 218237
    iget-object v0, p0, LX/1Dg;->L:Ljava/lang/Object;

    iput-object v0, p1, LX/1Dg;->L:Ljava/lang/Object;

    .line 218238
    :cond_11
    iget-object v0, p0, LX/1Dg;->M:Landroid/util/SparseArray;

    if-eqz v0, :cond_12

    .line 218239
    iget-object v0, p0, LX/1Dg;->M:Landroid/util/SparseArray;

    iput-object v0, p1, LX/1Dg;->M:Landroid/util/SparseArray;

    .line 218240
    :cond_12
    iget-object v0, p0, LX/1Dg;->N:Ljava/lang/String;

    if-eqz v0, :cond_13

    .line 218241
    iget-object v0, p0, LX/1Dg;->N:Ljava/lang/String;

    iput-object v0, p1, LX/1Dg;->N:Ljava/lang/String;

    .line 218242
    :cond_13
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x1000000

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_14

    .line 218243
    iget-object v0, p0, LX/1Dg;->x:LX/1dQ;

    iput-object v0, p1, LX/1Dg;->x:LX/1dQ;

    .line 218244
    :cond_14
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide v2, 0x400000000L

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_15

    .line 218245
    iget-object v0, p0, LX/1Dg;->C:LX/1dQ;

    iput-object v0, p1, LX/1Dg;->C:LX/1dQ;

    .line 218246
    :cond_15
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide v2, 0x800000000L

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_16

    .line 218247
    iget-object v0, p0, LX/1Dg;->D:LX/1dQ;

    iput-object v0, p1, LX/1Dg;->D:LX/1dQ;

    .line 218248
    :cond_16
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide v2, 0x1000000000L

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_17

    .line 218249
    iget-object v0, p0, LX/1Dg;->F:LX/1dQ;

    iput-object v0, p1, LX/1Dg;->F:LX/1dQ;

    .line 218250
    :cond_17
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide v2, 0x2000000000L

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_18

    .line 218251
    iget-object v0, p0, LX/1Dg;->E:LX/1dQ;

    iput-object v0, p1, LX/1Dg;->E:LX/1dQ;

    .line 218252
    :cond_18
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide v2, 0x4000000000L

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_19

    .line 218253
    iget-object v0, p0, LX/1Dg;->G:LX/1dQ;

    iput-object v0, p1, LX/1Dg;->G:LX/1dQ;

    .line 218254
    :cond_19
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide v2, 0x8000000000L

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1a

    .line 218255
    iget-object v0, p0, LX/1Dg;->H:LX/1dQ;

    iput-object v0, p1, LX/1Dg;->H:LX/1dQ;

    .line 218256
    :cond_1a
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide v2, 0x10000000000L

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1b

    .line 218257
    iget-object v0, p0, LX/1Dg;->I:LX/1dQ;

    iput-object v0, p1, LX/1Dg;->I:LX/1dQ;

    .line 218258
    :cond_1b
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide v2, 0x20000000000L

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1c

    .line 218259
    iget-object v0, p0, LX/1Dg;->J:LX/1dQ;

    iput-object v0, p1, LX/1Dg;->J:LX/1dQ;

    .line 218260
    :cond_1c
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/16 v2, 0x400

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1e

    .line 218261
    iget-object v0, p0, LX/1Dg;->P:LX/1mz;

    if-nez v0, :cond_1d

    .line 218262
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "copyInto() must be used when resolving a nestedTree. If padding was set on the holder node, we must have a mNestedTreePadding instance"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218263
    :cond_1d
    iget-object v0, p1, LX/1Dg;->a:LX/1mn;

    .line 218264
    iget-wide v2, p1, LX/1Dg;->q:J

    const-wide/16 v4, 0x400

    or-long/2addr v2, v4

    iput-wide v2, p1, LX/1Dg;->q:J

    .line 218265
    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->LEFT:Lcom/facebook/csslayout/YogaEdge;

    iget-object v2, p0, LX/1Dg;->P:LX/1mz;

    invoke-virtual {v2, v9}, LX/1mz;->b(I)F

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1mn;->b(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218266
    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->TOP:Lcom/facebook/csslayout/YogaEdge;

    iget-object v2, p0, LX/1Dg;->P:LX/1mz;

    invoke-virtual {v2, v8}, LX/1mz;->b(I)F

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1mn;->b(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218267
    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->RIGHT:Lcom/facebook/csslayout/YogaEdge;

    iget-object v2, p0, LX/1Dg;->P:LX/1mz;

    invoke-virtual {v2, v10}, LX/1mz;->b(I)F

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1mn;->b(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218268
    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->BOTTOM:Lcom/facebook/csslayout/YogaEdge;

    iget-object v2, p0, LX/1Dg;->P:LX/1mz;

    invoke-virtual {v2, v11}, LX/1mz;->b(I)F

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1mn;->b(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218269
    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->VERTICAL:Lcom/facebook/csslayout/YogaEdge;

    iget-object v2, p0, LX/1Dg;->P:LX/1mz;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, LX/1mz;->b(I)F

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1mn;->b(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218270
    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->HORIZONTAL:Lcom/facebook/csslayout/YogaEdge;

    iget-object v2, p0, LX/1Dg;->P:LX/1mz;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, LX/1mz;->b(I)F

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1mn;->b(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218271
    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->START:Lcom/facebook/csslayout/YogaEdge;

    iget-object v2, p0, LX/1Dg;->P:LX/1mz;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, LX/1mz;->b(I)F

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1mn;->b(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218272
    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->END:Lcom/facebook/csslayout/YogaEdge;

    iget-object v2, p0, LX/1Dg;->P:LX/1mz;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, LX/1mz;->b(I)F

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1mn;->b(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218273
    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->ALL:Lcom/facebook/csslayout/YogaEdge;

    iget-object v2, p0, LX/1Dg;->P:LX/1mz;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, LX/1mz;->b(I)F

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1mn;->b(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218274
    :cond_1e
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide v2, 0x200000000L

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_20

    .line 218275
    iget-object v0, p0, LX/1Dg;->Q:LX/1mz;

    if-nez v0, :cond_1f

    .line 218276
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "copyInto() must be used when resolving a nestedTree. If border width was set on the holder node, we must have a mNestedTreeBorderWidth instance"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218277
    :cond_1f
    iget-object v0, p1, LX/1Dg;->a:LX/1mn;

    .line 218278
    iget-wide v2, p1, LX/1Dg;->q:J

    const-wide v4, 0x200000000L

    or-long/2addr v2, v4

    iput-wide v2, p1, LX/1Dg;->q:J

    .line 218279
    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->LEFT:Lcom/facebook/csslayout/YogaEdge;

    iget-object v2, p0, LX/1Dg;->Q:LX/1mz;

    invoke-virtual {v2, v9}, LX/1mz;->b(I)F

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1mn;->c(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218280
    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->TOP:Lcom/facebook/csslayout/YogaEdge;

    iget-object v2, p0, LX/1Dg;->Q:LX/1mz;

    invoke-virtual {v2, v8}, LX/1mz;->b(I)F

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1mn;->c(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218281
    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->RIGHT:Lcom/facebook/csslayout/YogaEdge;

    iget-object v2, p0, LX/1Dg;->Q:LX/1mz;

    invoke-virtual {v2, v10}, LX/1mz;->b(I)F

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1mn;->c(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218282
    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->BOTTOM:Lcom/facebook/csslayout/YogaEdge;

    iget-object v2, p0, LX/1Dg;->Q:LX/1mz;

    invoke-virtual {v2, v11}, LX/1mz;->b(I)F

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1mn;->c(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218283
    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->VERTICAL:Lcom/facebook/csslayout/YogaEdge;

    iget-object v2, p0, LX/1Dg;->Q:LX/1mz;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, LX/1mz;->b(I)F

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1mn;->c(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218284
    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->HORIZONTAL:Lcom/facebook/csslayout/YogaEdge;

    iget-object v2, p0, LX/1Dg;->Q:LX/1mz;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, LX/1mz;->b(I)F

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1mn;->c(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218285
    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->START:Lcom/facebook/csslayout/YogaEdge;

    iget-object v2, p0, LX/1Dg;->Q:LX/1mz;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, LX/1mz;->b(I)F

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1mn;->c(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218286
    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->END:Lcom/facebook/csslayout/YogaEdge;

    iget-object v2, p0, LX/1Dg;->Q:LX/1mz;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, LX/1mz;->b(I)F

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1mn;->c(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218287
    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->ALL:Lcom/facebook/csslayout/YogaEdge;

    iget-object v2, p0, LX/1Dg;->Q:LX/1mz;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, LX/1mz;->b(I)F

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1mn;->c(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 218288
    :cond_20
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide v2, 0x80000000000L

    and-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-eqz v0, :cond_21

    .line 218289
    iget-object v0, p0, LX/1Dg;->u:Ljava/lang/String;

    iput-object v0, p1, LX/1Dg;->u:Ljava/lang/String;

    .line 218290
    :cond_21
    return-void
.end method

.method public c()I
    .locals 1
    .annotation build Landroid/support/annotation/Px;
    .end annotation

    .prologue
    .line 218087
    iget v0, p0, LX/1Dg;->ab:F

    invoke-static {v0}, LX/1mo;->a(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218088
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v0}, LX/1mn;->m()F

    move-result v0

    iput v0, p0, LX/1Dg;->ab:F

    .line 218089
    :cond_0
    iget v0, p0, LX/1Dg;->ab:F

    float-to-int v0, v0

    return v0
.end method

.method public c(LX/1dc;)LX/1Dg;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1Dg;"
        }
    .end annotation

    .prologue
    .line 218084
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x40000

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218085
    iput-object p1, p0, LX/1Dg;->r:LX/1dc;

    .line 218086
    return-object p0
.end method

.method public c(Landroid/util/SparseArray;)LX/1Dg;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "LX/1Dg;"
        }
    .end annotation

    .prologue
    .line 218081
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x800000

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218082
    iput-object p1, p0, LX/1Dg;->M:Landroid/util/SparseArray;

    .line 218083
    return-object p0
.end method

.method public c(Ljava/lang/CharSequence;)LX/1Dg;
    .locals 4

    .prologue
    .line 218078
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x400000

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218079
    iput-object p1, p0, LX/1Dg;->K:Ljava/lang/CharSequence;

    .line 218080
    return-object p0
.end method

.method public synthetic c(LX/1n6;)LX/1Dh;
    .locals 1

    .prologue
    .line 218077
    invoke-virtual {p0, p1}, LX/1Dg;->d(LX/1n6;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Z)LX/1Dh;
    .locals 1

    .prologue
    .line 218076
    invoke-virtual {p0, p1}, LX/1Dg;->f(Z)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(F)LX/1Di;
    .locals 1

    .prologue
    .line 218075
    invoke-virtual {p0, p1}, LX/1Dg;->k(F)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(I)LX/1Di;
    .locals 1

    .prologue
    .line 218074
    invoke-virtual {p0, p1}, LX/1Dg;->ai(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(II)LX/1Di;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218073
    invoke-virtual {p0, p1, p2}, LX/1Dg;->C(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(LX/1dQ;)LX/1Di;
    .locals 1

    .prologue
    .line 218072
    invoke-virtual {p0, p1}, LX/1Dg;->h(LX/1dQ;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .locals 1
    .annotation build Landroid/support/annotation/Px;
    .end annotation

    .prologue
    .line 218069
    iget v0, p0, LX/1Dg;->ac:F

    invoke-static {v0}, LX/1mo;->a(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218070
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v0}, LX/1mn;->n()F

    move-result v0

    iput v0, p0, LX/1Dg;->ac:F

    .line 218071
    :cond_0
    iget v0, p0, LX/1Dg;->ac:F

    float-to-int v0, v0

    return v0
.end method

.method public d(LX/1n6;)LX/1Dg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1n6",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1Dg;"
        }
    .end annotation

    .prologue
    .line 218068
    invoke-virtual {p1}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1Dg;->c(LX/1dc;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d(F)LX/1Dh;
    .locals 1

    .prologue
    .line 218067
    invoke-virtual {p0, p1}, LX/1Dg;->i(F)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d(LX/1dQ;)LX/1Dh;
    .locals 1

    .prologue
    .line 218066
    invoke-virtual {p0, p1}, LX/1Dg;->g(LX/1dQ;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final d(Z)LX/1Dh;
    .locals 4

    .prologue
    .line 218062
    if-eqz p1, :cond_0

    .line 218063
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x2000000

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218064
    :goto_0
    return-object p0

    .line 218065
    :cond_0
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x4000000

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    goto :goto_0
.end method

.method public synthetic d(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218090
    invoke-virtual {p0, p1}, LX/1Dg;->aj(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d(II)LX/1Di;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218091
    invoke-virtual {p0, p1, p2}, LX/1Dg;->D(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public e()I
    .locals 2
    .annotation build Landroid/support/annotation/Px;
    .end annotation

    .prologue
    .line 218092
    iget v0, p0, LX/1Dg;->T:F

    invoke-static {v0}, LX/1mo;->a(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218093
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->TOP:Lcom/facebook/csslayout/YogaEdge;

    invoke-interface {v0, v1}, LX/1mn;->a(Lcom/facebook/csslayout/YogaEdge;)F

    move-result v0

    iput v0, p0, LX/1Dg;->T:F

    .line 218094
    :cond_0
    iget v0, p0, LX/1Dg;->T:F

    invoke-static {v0}, LX/1n0;->a(F)I

    move-result v0

    return v0
.end method

.method public e(LX/1n6;)LX/1Dg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1n6",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1Dg;"
        }
    .end annotation

    .prologue
    .line 218095
    invoke-virtual {p1}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1Dg;->d(LX/1dc;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e(F)LX/1Dh;
    .locals 1

    .prologue
    .line 218096
    invoke-virtual {p0, p1}, LX/1Dg;->j(F)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e(LX/1dQ;)LX/1Dh;
    .locals 1

    .prologue
    .line 218097
    invoke-direct {p0, p1}, LX/1Dg;->i(LX/1dQ;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218098
    invoke-virtual {p0, p1}, LX/1Dg;->ak(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e(II)LX/1Di;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218099
    invoke-virtual {p0, p1, p2}, LX/1Dg;->E(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public e(Z)V
    .locals 0

    .prologue
    .line 218100
    iput-boolean p1, p0, LX/1Dg;->aj:Z

    .line 218101
    return-void
.end method

.method public f()I
    .locals 2
    .annotation build Landroid/support/annotation/Px;
    .end annotation

    .prologue
    .line 218102
    iget v0, p0, LX/1Dg;->S:F

    invoke-static {v0}, LX/1mo;->a(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218103
    invoke-virtual {p0}, LX/1Dg;->i()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->END:Lcom/facebook/csslayout/YogaEdge;

    invoke-interface {v0, v1}, LX/1mn;->a(Lcom/facebook/csslayout/YogaEdge;)F

    move-result v0

    :goto_0
    iput v0, p0, LX/1Dg;->S:F

    .line 218104
    iget v0, p0, LX/1Dg;->S:F

    invoke-static {v0}, LX/1mo;->a(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218105
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->RIGHT:Lcom/facebook/csslayout/YogaEdge;

    invoke-interface {v0, v1}, LX/1mn;->a(Lcom/facebook/csslayout/YogaEdge;)F

    move-result v0

    iput v0, p0, LX/1Dg;->S:F

    .line 218106
    :cond_0
    iget v0, p0, LX/1Dg;->S:F

    invoke-static {v0}, LX/1n0;->a(F)I

    move-result v0

    return v0

    .line 218107
    :cond_1
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->START:Lcom/facebook/csslayout/YogaEdge;

    invoke-interface {v0, v1}, LX/1mn;->a(Lcom/facebook/csslayout/YogaEdge;)F

    move-result v0

    goto :goto_0
.end method

.method public f(Z)LX/1Dg;
    .locals 4

    .prologue
    .line 218108
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/16 v2, 0x100

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218109
    iput-boolean p1, p0, LX/1Dg;->n:Z

    .line 218110
    return-object p0
.end method

.method public synthetic f(F)LX/1Dh;
    .locals 1

    .prologue
    .line 218111
    invoke-virtual {p0, p1}, LX/1Dg;->k(F)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f(LX/1dQ;)LX/1Dh;
    .locals 1

    .prologue
    .line 218112
    invoke-virtual {p0, p1}, LX/1Dg;->h(LX/1dQ;)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f(I)LX/1Di;
    .locals 1

    .prologue
    .line 218113
    invoke-virtual {p0, p1}, LX/1Dg;->al(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f(II)LX/1Di;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param

    .prologue
    .line 218114
    invoke-virtual {p0, p1, p2}, LX/1Dg;->F(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public g()I
    .locals 2
    .annotation build Landroid/support/annotation/Px;
    .end annotation

    .prologue
    .line 218378
    iget v0, p0, LX/1Dg;->U:F

    invoke-static {v0}, LX/1mo;->a(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218379
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->BOTTOM:Lcom/facebook/csslayout/YogaEdge;

    invoke-interface {v0, v1}, LX/1mn;->a(Lcom/facebook/csslayout/YogaEdge;)F

    move-result v0

    iput v0, p0, LX/1Dg;->U:F

    .line 218380
    :cond_0
    iget v0, p0, LX/1Dg;->U:F

    invoke-static {v0}, LX/1n0;->a(F)I

    move-result v0

    return v0
.end method

.method public g(LX/1dQ;)LX/1Dg;
    .locals 4

    .prologue
    .line 218345
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x100000

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218346
    iput-object p1, p0, LX/1Dg;->v:LX/1dQ;

    .line 218347
    return-object p0
.end method

.method public synthetic g(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218348
    invoke-virtual {p0, p1}, LX/1Dg;->am(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic g(II)LX/1Di;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218349
    invoke-virtual {p0, p1, p2}, LX/1Dg;->G(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public g(F)V
    .locals 0

    .prologue
    .line 218350
    iput p1, p0, LX/1Dg;->ag:F

    .line 218351
    return-void
.end method

.method public h()I
    .locals 2
    .annotation build Landroid/support/annotation/Px;
    .end annotation

    .prologue
    .line 218352
    iget v0, p0, LX/1Dg;->R:F

    invoke-static {v0}, LX/1mo;->a(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218353
    invoke-virtual {p0}, LX/1Dg;->i()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->START:Lcom/facebook/csslayout/YogaEdge;

    invoke-interface {v0, v1}, LX/1mn;->a(Lcom/facebook/csslayout/YogaEdge;)F

    move-result v0

    :goto_0
    iput v0, p0, LX/1Dg;->R:F

    .line 218354
    iget v0, p0, LX/1Dg;->R:F

    invoke-static {v0}, LX/1mo;->a(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218355
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->LEFT:Lcom/facebook/csslayout/YogaEdge;

    invoke-interface {v0, v1}, LX/1mn;->a(Lcom/facebook/csslayout/YogaEdge;)F

    move-result v0

    iput v0, p0, LX/1Dg;->R:F

    .line 218356
    :cond_0
    iget v0, p0, LX/1Dg;->R:F

    invoke-static {v0}, LX/1n0;->a(F)I

    move-result v0

    return v0

    .line 218357
    :cond_1
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->END:Lcom/facebook/csslayout/YogaEdge;

    invoke-interface {v0, v1}, LX/1mn;->a(Lcom/facebook/csslayout/YogaEdge;)F

    move-result v0

    goto :goto_0
.end method

.method public h(LX/1dQ;)LX/1Dg;
    .locals 4

    .prologue
    .line 218358
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/32 v2, 0x1000000

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218359
    iput-object p1, p0, LX/1Dg;->x:LX/1dQ;

    .line 218360
    return-object p0
.end method

.method public synthetic h(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param

    .prologue
    .line 218361
    invoke-virtual {p0, p1}, LX/1Dg;->ao(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h(II)LX/1Di;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218362
    invoke-virtual {p0, p1, p2}, LX/1Dg;->H(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public h(F)V
    .locals 0

    .prologue
    .line 218363
    iput p1, p0, LX/1Dg;->ah:F

    .line 218364
    return-void
.end method

.method public final i()I
    .locals 2

    .prologue
    .line 218365
    iget v0, p0, LX/1Dg;->ad:I

    if-gez v0, :cond_0

    .line 218366
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v0}, LX/1mn;->o()Lcom/facebook/csslayout/YogaDirection;

    move-result-object v0

    .line 218367
    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaDirection;->ordinal()I

    move-result v1

    move v0, v1

    .line 218368
    iput v0, p0, LX/1Dg;->ad:I

    .line 218369
    :cond_0
    iget v0, p0, LX/1Dg;->ad:I

    return v0
.end method

.method public i(F)LX/1Dg;
    .locals 4

    .prologue
    .line 218370
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/16 v2, 0x8

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218371
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v0, p1}, LX/1mn;->a(F)V

    .line 218372
    return-object p0
.end method

.method public synthetic i(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218373
    invoke-virtual {p0, p1}, LX/1Dg;->an(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i(II)LX/1Di;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218374
    invoke-virtual {p0, p1, p2}, LX/1Dg;->I(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public j(F)LX/1Dg;
    .locals 4

    .prologue
    .line 218375
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/16 v2, 0x10

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218376
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v0, p1}, LX/1mn;->b(F)V

    .line 218377
    return-object p0
.end method

.method public synthetic j()LX/1Di;
    .locals 1

    .prologue
    .line 218344
    invoke-virtual {p0}, LX/1Dg;->C()LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic j(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218381
    invoke-virtual {p0, p1}, LX/1Dg;->ap(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic j(II)LX/1Di;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218382
    invoke-virtual {p0, p1, p2}, LX/1Dg;->J(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/1Dg;
    .locals 0

    .prologue
    .line 218383
    return-object p0
.end method

.method public k(F)LX/1Dg;
    .locals 4

    .prologue
    .line 218384
    iget-wide v0, p0, LX/1Dg;->q:J

    const-wide/16 v2, 0x20

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Dg;->q:J

    .line 218385
    iget-object v0, p0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v0, p1}, LX/1mn;->c(F)V

    .line 218386
    return-object p0
.end method

.method public synthetic k(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218387
    invoke-virtual {p0, p1}, LX/1Dg;->aq(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k(II)LX/1Di;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218388
    invoke-virtual {p0, p1, p2}, LX/1Dg;->K(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic l(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218389
    invoke-virtual {p0, p1}, LX/1Dg;->ar(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic l(II)LX/1Di;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218390
    invoke-virtual {p0, p1, p2}, LX/1Dg;->L(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/1dc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dc",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218391
    iget-object v0, p0, LX/1Dg;->r:LX/1dc;

    return-object v0
.end method

.method public synthetic m(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218392
    invoke-virtual {p0, p1}, LX/1Dg;->as(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic m(II)LX/1Di;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218393
    invoke-virtual {p0, p1, p2}, LX/1Dg;->M(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final m()LX/1dc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dc",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218394
    iget-object v0, p0, LX/1Dg;->s:LX/1dc;

    return-object v0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 218395
    iget v0, p0, LX/1Dg;->ae:I

    return v0
.end method

.method public synthetic n(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218396
    invoke-virtual {p0, p1}, LX/1Dg;->at(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final n(II)LX/1Di;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218329
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p2}, LX/1Dp;->a(I)I

    move-result v0

    invoke-direct {p0, p1, v0}, LX/1Dg;->Q(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 218315
    iget v0, p0, LX/1Dg;->af:I

    return v0
.end method

.method public synthetic o(II)LX/1Dh;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218316
    invoke-virtual {p0, p1, p2}, LX/1Dg;->A(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic o(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218317
    invoke-virtual {p0, p1}, LX/1Dg;->au(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic p(II)LX/1Dh;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param

    .prologue
    .line 218318
    invoke-virtual {p0, p1, p2}, LX/1Dg;->B(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic p(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param

    .prologue
    .line 218319
    invoke-virtual {p0, p1}, LX/1Dg;->aw(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 218320
    iget-object v0, p0, LX/1Dg;->y:LX/1dQ;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Dg;->z:LX/1dQ;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Dg;->A:LX/1dQ;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Dg;->B:LX/1dQ;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic q(II)LX/1Dh;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218321
    invoke-virtual {p0, p1, p2}, LX/1Dg;->C(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic q(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218322
    invoke-virtual {p0, p1}, LX/1Dg;->av(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic r(II)LX/1Dh;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218323
    invoke-virtual {p0, p1, p2}, LX/1Dg;->D(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic r(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218324
    invoke-virtual {p0, p1}, LX/1Dg;->ax(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic s(II)LX/1Dh;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218325
    invoke-virtual {p0, p1, p2}, LX/1Dg;->E(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic s(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218326
    invoke-virtual {p0, p1}, LX/1Dg;->ay(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final s()LX/1ml;
    .locals 1

    .prologue
    .line 218327
    iget-object v0, p0, LX/1Dg;->ai:LX/1ml;

    return-object v0
.end method

.method public synthetic t(II)LX/1Dh;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param

    .prologue
    .line 218328
    invoke-virtual {p0, p1, p2}, LX/1Dg;->F(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic t(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218314
    invoke-virtual {p0, p1}, LX/1Dg;->az(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 218330
    iget-boolean v0, p0, LX/1Dg;->aj:Z

    return v0
.end method

.method public synthetic u(II)LX/1Dh;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218331
    invoke-virtual {p0, p1, p2}, LX/1Dg;->G(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic u(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218332
    invoke-virtual {p0, p1}, LX/1Dg;->aB(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v(II)LX/1Dh;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218333
    invoke-virtual {p0, p1, p2}, LX/1Dg;->H(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218334
    invoke-virtual {p0, p1}, LX/1Dg;->aC(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 218335
    iget-boolean v0, p0, LX/1Dg;->o:Z

    return v0
.end method

.method public synthetic w(II)LX/1Dh;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 218336
    invoke-virtual {p0, p1, p2}, LX/1Dg;->K(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic w(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param

    .prologue
    .line 218337
    invoke-virtual {p0, p1}, LX/1Dg;->aD(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x(II)LX/1Dh;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218338
    invoke-virtual {p0, p1, p2}, LX/1Dg;->L(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 218339
    invoke-virtual {p0, p1}, LX/1Dg;->aE(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic y(II)LX/1Dh;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 218340
    invoke-virtual {p0, p1, p2}, LX/1Dg;->M(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic y(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 218341
    invoke-virtual {p0, p1}, LX/1Dg;->aF(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final z(II)LX/1Dh;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 218342
    iget-object v0, p0, LX/1Dg;->i:LX/1Dp;

    invoke-virtual {v0, p2}, LX/1Dp;->f(I)I

    move-result v0

    invoke-direct {p0, p1, v0}, LX/1Dg;->Q(II)LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic z(I)LX/1Di;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 218343
    invoke-virtual {p0, p1}, LX/1Dg;->aG(I)LX/1Dg;

    move-result-object v0

    return-object v0
.end method
