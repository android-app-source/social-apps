.class public LX/1GV;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1GE;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1GE;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 225534
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1GE;
    .locals 11

    .prologue
    .line 225535
    sget-object v0, LX/1GV;->a:LX/1GE;

    if-nez v0, :cond_1

    .line 225536
    const-class v1, LX/1GV;

    monitor-enter v1

    .line 225537
    :try_start_0
    sget-object v0, LX/1GV;->a:LX/1GE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 225538
    if-eqz v2, :cond_0

    .line 225539
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 225540
    invoke-static {v0}, LX/0pi;->a(LX/0QB;)LX/0pi;

    move-result-object v3

    check-cast v3, LX/0pi;

    const/16 v4, 0x251a

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xba8

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/1GW;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {v0}, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->a(LX/0QB;)Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;

    move-result-object v8

    check-cast v8, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;

    const/16 v9, 0xb9f

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static/range {v3 .. v10}, LX/1Aq;->a(LX/0pi;LX/0Ot;LX/0Ot;Ljava/lang/String;LX/0Uh;Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;LX/0Ot;LX/0ad;)LX/1GE;

    move-result-object v3

    move-object v0, v3

    .line 225541
    sput-object v0, LX/1GV;->a:LX/1GE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225542
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 225543
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 225544
    :cond_1
    sget-object v0, LX/1GV;->a:LX/1GE;

    return-object v0

    .line 225545
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 225546
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 225547
    invoke-static {p0}, LX/0pi;->a(LX/0QB;)LX/0pi;

    move-result-object v0

    check-cast v0, LX/0pi;

    const/16 v1, 0x251a

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0xba8

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/1GW;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {p0}, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->a(LX/0QB;)Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;

    move-result-object v5

    check-cast v5, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;

    const/16 v6, 0xb9f

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static/range {v0 .. v7}, LX/1Aq;->a(LX/0pi;LX/0Ot;LX/0Ot;Ljava/lang/String;LX/0Uh;Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;LX/0Ot;LX/0ad;)LX/1GE;

    move-result-object v0

    return-object v0
.end method
