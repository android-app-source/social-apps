.class public LX/1Gk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1Gk;


# instance fields
.field private final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final b:LX/0So;


# direct methods
.method public constructor <init>(LX/0So;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 225971
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225972
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1Gk;->a:Ljava/util/HashMap;

    .line 225973
    iput-object p1, p0, LX/1Gk;->b:LX/0So;

    .line 225974
    return-void
.end method

.method public static a(LX/0QB;)LX/1Gk;
    .locals 4

    .prologue
    .line 225958
    sget-object v0, LX/1Gk;->c:LX/1Gk;

    if-nez v0, :cond_1

    .line 225959
    const-class v1, LX/1Gk;

    monitor-enter v1

    .line 225960
    :try_start_0
    sget-object v0, LX/1Gk;->c:LX/1Gk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 225961
    if-eqz v2, :cond_0

    .line 225962
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 225963
    new-instance p0, LX/1Gk;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-direct {p0, v3}, LX/1Gk;-><init>(LX/0So;)V

    .line 225964
    move-object v0, p0

    .line 225965
    sput-object v0, LX/1Gk;->c:LX/1Gk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225966
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 225967
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 225968
    :cond_1
    sget-object v0, LX/1Gk;->c:LX/1Gk;

    return-object v0

    .line 225969
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 225970
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;J)Z
    .locals 4

    .prologue
    .line 225951
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Gk;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 225952
    iget-object v0, p0, LX/1Gk;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 225953
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    cmp-long v0, v0, p2

    if-lez v0, :cond_1

    .line 225954
    :cond_0
    iget-object v0, p0, LX/1Gk;->a:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225955
    const/4 v0, 0x0

    .line 225956
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 225957
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
