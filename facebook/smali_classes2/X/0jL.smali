.class public abstract LX/0jL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0cA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0cA",
            "<",
            "LX/5L2;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/composer/system/model/ComposerModelImpl;

.field public c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/0Sh;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/system/model/ComposerModelImpl;LX/0Sh;)V
    .locals 1

    .prologue
    .line 122786
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122787
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->a:LX/0cA;

    .line 122788
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    iput-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 122789
    iput-object p2, p0, LX/0jL;->d:LX/0Sh;

    .line 122790
    return-void
.end method


# virtual methods
.method public final a(LX/5Rn;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122791
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122792
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getPublishMode()LX/5Rn;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122793
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122794
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122795
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setPublishMode(LX/5Rn;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122796
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122797
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122798
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122799
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->u()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122800
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122801
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122802
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122803
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122804
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122805
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122806
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->v()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122807
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122808
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122809
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122810
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_INLINE_SPROUTS_STATE_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122811
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122812
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122813
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->i()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122814
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122815
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122816
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122817
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122818
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122819
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122820
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122821
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122822
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122823
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setMinutiaeObject(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122824
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122825
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122826
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122827
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122828
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122829
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122830
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122831
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122832
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122833
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122834
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getComposerSessionLoggingData()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122835
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122836
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122837
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setComposerSessionLoggingData(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122838
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122839
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122889
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122890
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->t()Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122891
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122892
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122893
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122894
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122895
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122840
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122841
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122842
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122843
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122844
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122845
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122846
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122847
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122848
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->k()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122849
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122850
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122851
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122852
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122853
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122854
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122855
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122856
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122857
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122858
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122859
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122860
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122861
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122862
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->m()Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122863
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122864
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122865
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122866
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122867
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122868
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122869
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122870
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122871
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122872
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122873
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122874
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122875
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122876
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122877
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122878
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122879
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122880
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122881
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122882
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122883
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->p()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122884
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122885
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122886
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122887
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122888
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122666
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122667
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getPrivacyOverride()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122668
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122669
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122670
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setPrivacyOverride(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122671
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122672
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122772
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122773
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122774
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122775
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122776
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setTextWithEntities(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122777
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_STATUS_TEXT_CHANGED:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122778
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122779
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122780
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122781
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122782
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122783
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122784
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122785
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122659
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122660
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122661
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122662
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122663
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122664
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122665
    :cond_1
    return-object p0
.end method

.method public final synthetic a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 122673
    invoke-virtual {p0, p1}, LX/0jL;->b(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)LX/0jL;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122674
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122675
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122676
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122677
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122678
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setLocationInfo(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122679
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122680
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/ipc/composer/model/ComposerSlideshowData;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122681
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122682
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122683
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122684
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122685
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setSlideshowData(Lcom/facebook/ipc/composer/model/ComposerSlideshowData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122686
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122687
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/ipc/composer/model/ComposerStickerData;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122688
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122689
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122690
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122691
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122692
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setReferencedStickerData(Lcom/facebook/ipc/composer/model/ComposerStickerData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122693
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122694
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/facebook/ipc/composer/model/ComposerStorylineData;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122695
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122696
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getStorylineData()Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122697
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122698
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122699
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setStorylineData(Lcom/facebook/ipc/composer/model/ComposerStorylineData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122700
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122701
    :cond_1
    return-object p0
.end method

.method public final a(Ljava/lang/Long;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122702
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122703
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getPublishScheduleTime()Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122704
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122705
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122706
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setPublishScheduleTime(Ljava/lang/Long;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122707
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122708
    :cond_1
    return-object p0
.end method

.method public abstract a()V
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLAlbum;)LX/0jL;
    .locals 2

    .prologue
    .line 122709
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122710
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122711
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122712
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122713
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setTargetAlbum(Lcom/facebook/graphql/model/GraphQLAlbum;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122714
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122715
    :cond_1
    return-object p0
.end method

.method public final b(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)LX/0jL;
    .locals 2

    .prologue
    .line 122716
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122717
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122718
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122719
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122720
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122721
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122722
    :cond_1
    return-object p0
.end method

.method public final b(LX/0Px;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122723
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122724
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getTaggedUsers()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122725
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122726
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122727
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setTaggedUsers(LX/0Px;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122728
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122729
    :cond_1
    return-object p0
.end method

.method public final c(LX/0Px;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122730
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122731
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122732
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122733
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122734
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setAttachments(LX/0Px;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122735
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122736
    :cond_1
    return-object p0
.end method

.method public final d(Z)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122737
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122738
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->e()Z

    move-result v0

    if-eq v0, p1, :cond_1

    .line 122739
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122740
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122741
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->e(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122742
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_KEYBOARD_STATE_CHANGED:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122743
    :cond_1
    return-object p0
.end method

.method public final f(Z)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122744
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122745
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->f()Z

    move-result v0

    if-eq v0, p1, :cond_1

    .line 122746
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122747
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122748
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->f(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122749
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122750
    :cond_1
    return-object p0
.end method

.method public final g(Z)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122751
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122752
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->hasPrivacyChanged()Z

    move-result v0

    if-eq v0, p1, :cond_1

    .line 122753
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122754
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122755
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setHasPrivacyChanged(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122756
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122757
    :cond_1
    return-object p0
.end method

.method public final i(Z)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122758
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122759
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->d()Z

    move-result v0

    if-eq v0, p1, :cond_1

    .line 122760
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122761
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122762
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->c(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122763
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122764
    :cond_1
    return-object p0
.end method

.method public final j(Z)LX/0jL;
    .locals 2

    .prologue
    .line 122765
    iget-object v0, p0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122766
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->c()Z

    move-result v0

    if-eq v0, p1, :cond_1

    .line 122767
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 122768
    iget-object v0, p0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122769
    :cond_0
    iget-object v0, p0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->d(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 122770
    iget-object v0, p0, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 122771
    :cond_1
    return-object p0
.end method
