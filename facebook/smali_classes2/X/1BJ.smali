.class public LX/1BJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1BJ;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 212922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 212923
    iput-object p1, p0, LX/1BJ;->a:LX/0Zb;

    .line 212924
    return-void
.end method

.method public static a(LX/0QB;)LX/1BJ;
    .locals 4

    .prologue
    .line 212925
    sget-object v0, LX/1BJ;->b:LX/1BJ;

    if-nez v0, :cond_1

    .line 212926
    const-class v1, LX/1BJ;

    monitor-enter v1

    .line 212927
    :try_start_0
    sget-object v0, LX/1BJ;->b:LX/1BJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 212928
    if-eqz v2, :cond_0

    .line 212929
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 212930
    new-instance p0, LX/1BJ;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/1BJ;-><init>(LX/0Zb;)V

    .line 212931
    move-object v0, p0

    .line 212932
    sput-object v0, LX/1BJ;->b:LX/1BJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212933
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 212934
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 212935
    :cond_1
    sget-object v0, LX/1BJ;->b:LX/1BJ;

    return-object v0

    .line 212936
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 212937
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
