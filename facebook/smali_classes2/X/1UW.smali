.class public LX/1UW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vf;


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1OO;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1UX;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field public final d:Z

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field private j:Z


# direct methods
.method public constructor <init>(Ljava/util/List;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/1OO;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 256164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256165
    iput-boolean p2, p0, LX/1UW;->d:Z

    .line 256166
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/1UW;->a:LX/0Px;

    .line 256167
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, LX/1UW;->c:I

    .line 256168
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 256169
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v0, p0, LX/1UW;->c:I

    if-ge v1, v0, :cond_0

    .line 256170
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OO;

    .line 256171
    new-instance v3, LX/1UX;

    invoke-direct {v3, p0, v0, v1}, LX/1UX;-><init>(LX/1UW;LX/1OO;I)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 256172
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 256173
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/1UW;->b:LX/0Px;

    .line 256174
    invoke-virtual {p0}, LX/1UW;->a()V

    .line 256175
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 256147
    move v1, v2

    move v3, v2

    move v4, v2

    .line 256148
    :goto_0
    iget v0, p0, LX/1UW;->c:I

    if-ge v1, v0, :cond_0

    .line 256149
    iget-object v0, p0, LX/1UW;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1UX;

    .line 256150
    iget-object v5, v0, LX/1UX;->b:LX/1OO;

    invoke-interface {v5}, LX/1OP;->ij_()I

    move-result v5

    iput v5, v0, LX/1UX;->c:I

    .line 256151
    iget-object v5, v0, LX/1UX;->g:LX/1UW;

    iget-boolean v5, v5, LX/1UW;->d:Z

    if-eqz v5, :cond_1

    iget-object v5, v0, LX/1UX;->b:LX/1OO;

    invoke-interface {v5}, LX/1OO;->ii_()I

    move-result v5

    :goto_1
    iput v5, v0, LX/1UX;->d:I

    .line 256152
    iput v3, v0, LX/1UX;->e:I

    .line 256153
    iput v4, v0, LX/1UX;->f:I

    .line 256154
    iget v5, v0, LX/1UX;->c:I

    add-int/2addr v3, v5

    .line 256155
    iget v0, v0, LX/1UX;->d:I

    add-int/2addr v4, v0

    .line 256156
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 256157
    :cond_0
    iput v3, p0, LX/1UW;->h:I

    .line 256158
    iput v4, p0, LX/1UW;->i:I

    .line 256159
    iput v2, p0, LX/1UW;->e:I

    .line 256160
    iput v2, p0, LX/1UW;->f:I

    .line 256161
    iput v2, p0, LX/1UW;->g:I

    .line 256162
    return-void

    .line 256163
    :cond_1
    const/16 v5, 0x7d0

    goto :goto_1
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 256137
    if-ltz p1, :cond_0

    iget v0, p0, LX/1UW;->h:I

    if-lt p1, v0, :cond_1

    .line 256138
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Position: %d Count: %d"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p0, LX/1UW;->h:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 256139
    :cond_1
    iput p1, p0, LX/1UW;->f:I

    .line 256140
    :goto_0
    iget-object v0, p0, LX/1UW;->b:LX/0Px;

    iget v1, p0, LX/1UW;->e:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1UX;

    .line 256141
    iget v1, v0, LX/1UX;->e:I

    if-ge p1, v1, :cond_2

    .line 256142
    iget v0, p0, LX/1UW;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1UW;->e:I

    goto :goto_0

    .line 256143
    :cond_2
    iget v1, v0, LX/1UX;->e:I

    iget v2, v0, LX/1UX;->c:I

    add-int/2addr v1, v2

    if-lt p1, v1, :cond_3

    .line 256144
    iget v0, p0, LX/1UW;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1UW;->e:I

    goto :goto_0

    .line 256145
    :cond_3
    iget v0, v0, LX/1UX;->e:I

    sub-int v0, p1, v0

    iput v0, p0, LX/1UW;->g:I

    .line 256146
    return-void
.end method

.method public final a(LX/1OO;)V
    .locals 3

    .prologue
    .line 256132
    iget-object v0, p0, LX/1UW;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 256133
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 256134
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown adapter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 256135
    :cond_0
    invoke-virtual {p0, v0}, LX/1UW;->b(I)V

    .line 256136
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 256131
    iget-object v0, p0, LX/1UW;->b:LX/0Px;

    iget v1, p0, LX/1UW;->e:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1UX;

    iget v0, v0, LX/1UX;->f:I

    return v0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 256125
    if-ltz p1, :cond_0

    iget v0, p0, LX/1UW;->c:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 256126
    iput p1, p0, LX/1UW;->e:I

    .line 256127
    iput v1, p0, LX/1UW;->g:I

    .line 256128
    iget-object v0, p0, LX/1UW;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1UX;

    iget v0, v0, LX/1UX;->e:I

    iput v0, p0, LX/1UW;->f:I

    .line 256129
    return-void

    :cond_0
    move v0, v1

    .line 256130
    goto :goto_0
.end method

.method public final c(I)V
    .locals 4

    .prologue
    .line 256105
    if-ltz p1, :cond_0

    iget v0, p0, LX/1UW;->i:I

    if-lt p1, v0, :cond_1

    .line 256106
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ViewType: %d TotalViewTypeCount: %d"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p0, LX/1UW;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 256107
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, LX/1UW;->g:I

    .line 256108
    :goto_0
    iget-object v0, p0, LX/1UW;->b:LX/0Px;

    iget v1, p0, LX/1UW;->e:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1UX;

    .line 256109
    iget v1, v0, LX/1UX;->f:I

    if-ge p1, v1, :cond_2

    .line 256110
    iget v0, p0, LX/1UW;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1UW;->e:I

    goto :goto_0

    .line 256111
    :cond_2
    iget v1, v0, LX/1UX;->f:I

    iget v2, v0, LX/1UX;->d:I

    add-int/2addr v1, v2

    if-lt p1, v1, :cond_3

    .line 256112
    iget v0, p0, LX/1UW;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1UW;->e:I

    goto :goto_0

    .line 256113
    :cond_3
    iget v0, v0, LX/1UX;->e:I

    iput v0, p0, LX/1UW;->f:I

    .line 256114
    return-void
.end method

.method public final dispose()V
    .locals 3

    .prologue
    .line 256118
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v0, p0, LX/1UW;->c:I

    if-ge v1, v0, :cond_1

    .line 256119
    iget-object v0, p0, LX/1UW;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OO;

    .line 256120
    instance-of v2, v0, LX/0Vf;

    if-eqz v2, :cond_0

    .line 256121
    check-cast v0, LX/0Vf;

    invoke-interface {v0}, LX/0Vf;->dispose()V

    .line 256122
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 256123
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1UW;->j:Z

    .line 256124
    return-void
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 256117
    iget-object v0, p0, LX/1UW;->b:LX/0Px;

    iget v1, p0, LX/1UW;->e:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1UX;

    iget v0, v0, LX/1UX;->e:I

    return v0
.end method

.method public final h()LX/1OO;
    .locals 2

    .prologue
    .line 256116
    iget-object v0, p0, LX/1UW;->b:LX/0Px;

    iget v1, p0, LX/1UW;->e:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1UX;

    iget-object v0, v0, LX/1UX;->b:LX/1OO;

    return-object v0
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 256115
    iget-boolean v0, p0, LX/1UW;->j:Z

    return v0
.end method
