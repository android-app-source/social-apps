.class public abstract LX/15v;
.super LX/15w;
.source ""


# instance fields
.field public K:LX/15z;

.field public L:LX/15z;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 182812
    invoke-direct {p0}, LX/15w;-><init>()V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 182813
    invoke-direct {p0, p1}, LX/15w;-><init>(I)V

    .line 182814
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/Throwable;)LX/2aQ;
    .locals 2

    .prologue
    .line 182815
    new-instance v0, LX/2aQ;

    invoke-virtual {p0}, LX/15w;->l()LX/28G;

    move-result-object v1

    invoke-direct {v0, p1, v1, p2}, LX/2aQ;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static final e(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 182816
    int-to-char v0, p0

    .line 182817
    invoke-static {v0}, Ljava/lang/Character;->isISOControl(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 182818
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "(CTRL-CHAR, code "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182819
    :goto_0
    return-object v0

    .line 182820
    :cond_0
    const/16 v1, 0xff

    if-le p0, v1, :cond_1

    .line 182821
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' (code "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 182822
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' (code "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public abstract P()V
.end method

.method public final S()V
    .locals 2

    .prologue
    .line 182823
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " in "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->d(Ljava/lang/String;)V

    .line 182824
    return-void
.end method

.method public final T()V
    .locals 1

    .prologue
    .line 182856
    const-string v0, " in a value"

    invoke-virtual {p0, v0}, LX/15v;->d(Ljava/lang/String;)V

    .line 182857
    return-void
.end method

.method public final a(C)C
    .locals 2

    .prologue
    .line 182825
    sget-object v0, LX/0lr;->ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER:LX/0lr;

    invoke-virtual {p0, v0}, LX/15w;->a(LX/0lr;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182826
    :cond_0
    :goto_0
    return p1

    .line 182827
    :cond_1
    const/16 v0, 0x27

    if-ne p1, v0, :cond_2

    sget-object v0, LX/0lr;->ALLOW_SINGLE_QUOTES:LX/0lr;

    invoke-virtual {p0, v0}, LX/15w;->a(LX/0lr;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 182828
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unrecognized character escape "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, LX/15v;->e(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(D)D
    .locals 3

    .prologue
    .line 182829
    iget-object v0, p0, LX/15v;->K:LX/15z;

    if-eqz v0, :cond_0

    .line 182830
    sget-object v0, LX/1Xm;->a:[I

    iget-object v1, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v1}, LX/15z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 182831
    :cond_0
    :goto_0
    return-wide p1

    .line 182832
    :pswitch_0
    invoke-virtual {p0}, LX/15w;->B()D

    move-result-wide p1

    goto :goto_0

    .line 182833
    :pswitch_1
    const-wide/high16 p1, 0x3ff0000000000000L    # 1.0

    goto :goto_0

    .line 182834
    :pswitch_2
    const-wide/16 p1, 0x0

    goto :goto_0

    .line 182835
    :pswitch_3
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, LX/16K;->a(Ljava/lang/String;D)D

    move-result-wide p1

    goto :goto_0

    .line 182836
    :pswitch_4
    invoke-virtual {p0}, LX/15w;->D()Ljava/lang/Object;

    move-result-object v0

    .line 182837
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 182838
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide p1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public final a(J)J
    .locals 3

    .prologue
    .line 182839
    iget-object v0, p0, LX/15v;->K:LX/15z;

    if-eqz v0, :cond_0

    .line 182840
    sget-object v0, LX/1Xm;->a:[I

    iget-object v1, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v1}, LX/15z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 182841
    :cond_0
    :goto_0
    return-wide p1

    .line 182842
    :pswitch_0
    invoke-virtual {p0}, LX/15w;->y()J

    move-result-wide p1

    goto :goto_0

    .line 182843
    :pswitch_1
    const-wide/16 p1, 0x1

    goto :goto_0

    .line 182844
    :pswitch_2
    const-wide/16 p1, 0x0

    goto :goto_0

    .line 182845
    :pswitch_3
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, LX/16K;->a(Ljava/lang/String;J)J

    move-result-wide p1

    goto :goto_0

    .line 182846
    :pswitch_4
    invoke-virtual {p0}, LX/15w;->D()Ljava/lang/Object;

    move-result-object v0

    .line 182847
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 182848
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide p1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 182849
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-eq v0, v1, :cond_1

    .line 182850
    iget-object v0, p0, LX/15v;->K:LX/15z;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0}, LX/15z;->isScalarValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 182851
    :cond_0
    :goto_0
    return-object p1

    :cond_1
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/2SG;LX/0ln;)V
    .locals 1

    .prologue
    .line 182852
    :try_start_0
    invoke-virtual {p3, p1, p2}, LX/0ln;->a(Ljava/lang/String;LX/2SG;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182853
    :goto_0
    return-void

    .line 182854
    :catch_0
    move-exception v0

    .line 182855
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 182799
    invoke-direct {p0, p1, p2}, LX/15v;->b(Ljava/lang/String;Ljava/lang/Throwable;)LX/2aQ;

    move-result-object v0

    throw v0
.end method

.method public final a(Z)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 182800
    iget-object v2, p0, LX/15v;->K:LX/15z;

    if-eqz v2, :cond_0

    .line 182801
    sget-object v2, LX/1Xm;->a:[I

    iget-object v3, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v3}, LX/15z;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 182802
    :cond_0
    :goto_0
    return p1

    .line 182803
    :pswitch_0
    invoke-virtual {p0}, LX/15w;->x()I

    move-result v2

    if-eqz v2, :cond_1

    move p1, v1

    goto :goto_0

    :cond_1
    move p1, v0

    goto :goto_0

    :pswitch_1
    move p1, v1

    .line 182804
    goto :goto_0

    :pswitch_2
    move p1, v0

    .line 182805
    goto :goto_0

    .line 182806
    :pswitch_3
    invoke-virtual {p0}, LX/15w;->D()Ljava/lang/Object;

    move-result-object v0

    .line 182807
    instance-of v2, v0, Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 182808
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    goto :goto_0

    .line 182809
    :cond_2
    :pswitch_4
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 182810
    const-string v2, "true"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move p1, v1

    .line 182811
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b(I)I
    .locals 2

    .prologue
    .line 182751
    iget-object v0, p0, LX/15v;->K:LX/15z;

    if-eqz v0, :cond_0

    .line 182752
    sget-object v0, LX/1Xm;->a:[I

    iget-object v1, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v1}, LX/15z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 182753
    :cond_0
    :goto_0
    return p1

    .line 182754
    :pswitch_0
    invoke-virtual {p0}, LX/15w;->x()I

    move-result p1

    goto :goto_0

    .line 182755
    :pswitch_1
    const/4 p1, 0x1

    goto :goto_0

    .line 182756
    :pswitch_2
    const/4 p1, 0x0

    goto :goto_0

    .line 182757
    :pswitch_3
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, LX/16K;->a(Ljava/lang/String;I)I

    move-result p1

    goto :goto_0

    .line 182758
    :pswitch_4
    invoke-virtual {p0}, LX/15w;->D()Ljava/lang/Object;

    move-result-object v0

    .line 182759
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 182760
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result p1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public final b(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 182761
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected character ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, LX/15v;->e(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182762
    if-eqz p2, :cond_0

    .line 182763
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182764
    :cond_0
    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 182765
    return-void
.end method

.method public abstract c()LX/15z;
.end method

.method public final c(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 182766
    sget-object v0, LX/0lr;->ALLOW_UNQUOTED_CONTROL_CHARS:LX/0lr;

    invoke-virtual {p0, v0}, LX/15w;->a(LX/0lr;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x20

    if-lt p1, v0, :cond_1

    .line 182767
    :cond_0
    int-to-char v0, p1

    .line 182768
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal unquoted character ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/15v;->e(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): has to be escaped using backslash to be included in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182769
    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 182770
    :cond_1
    return-void
.end method

.method public final d()LX/15z;
    .locals 2

    .prologue
    .line 182771
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 182772
    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v1, :cond_0

    .line 182773
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 182774
    :cond_0
    return-object v0
.end method

.method public final d(I)V
    .locals 3

    .prologue
    .line 182775
    int-to-char v0, p1

    .line 182776
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal character ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/15v;->e(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): only regular white space (\\r, \\n, \\t) is allowed between tokens"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182777
    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 182778
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 182779
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected end-of-input"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 182780
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 182781
    invoke-virtual {p0, p1}, LX/15w;->b(Ljava/lang/String;)LX/2aQ;

    move-result-object v0

    throw v0
.end method

.method public f()LX/15w;
    .locals 3

    .prologue
    .line 182782
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-eq v0, v1, :cond_0

    .line 182783
    :goto_0
    return-object p0

    .line 182784
    :cond_0
    const/4 v0, 0x1

    .line 182785
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    .line 182786
    if-nez v1, :cond_2

    .line 182787
    invoke-virtual {p0}, LX/15v;->P()V

    goto :goto_0

    .line 182788
    :cond_2
    sget-object v2, LX/1Xm;->a:[I

    invoke-virtual {v1}, LX/15z;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_1

    .line 182789
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    .line 182790
    goto :goto_1

    .line 182791
    :pswitch_1
    add-int/lit8 v0, v0, -0x1

    if-nez v0, :cond_1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final g()LX/15z;
    .locals 1

    .prologue
    .line 182792
    iget-object v0, p0, LX/15v;->K:LX/15z;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 182793
    iget-object v0, p0, LX/15v;->K:LX/15z;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 182794
    iget-object v0, p0, LX/15v;->K:LX/15z;

    if-eqz v0, :cond_0

    .line 182795
    iget-object v0, p0, LX/15v;->K:LX/15z;

    iput-object v0, p0, LX/15v;->L:LX/15z;

    .line 182796
    const/4 v0, 0x0

    iput-object v0, p0, LX/15v;->K:LX/15z;

    .line 182797
    :cond_0
    return-void
.end method

.method public abstract o()Ljava/lang/String;
.end method

.method public version()LX/0ne;
    .locals 1

    .prologue
    .line 182798
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, LX/4pl;->a(Ljava/lang/Class;)LX/0ne;

    move-result-object v0

    return-object v0
.end method
