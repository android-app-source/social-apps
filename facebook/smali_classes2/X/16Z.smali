.class public LX/16Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16a;


# static fields
.field public static final a:LX/16Z;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 185399
    new-instance v0, LX/16Z;

    invoke-direct {v0}, LX/16Z;-><init>()V

    sput-object v0, LX/16Z;->a:LX/16Z;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 185400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(S)Lcom/facebook/flatbuffers/Flattenable;
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 185401
    if-gtz p1, :cond_0

    move-object v0, v1

    .line 185402
    :goto_0
    return-object v0

    .line 185403
    :cond_0
    add-int/lit8 v0, p1, -0x1

    int-to-short v0, v0

    .line 185404
    sget-object v2, LX/16c;->a:[Ljava/lang/String;

    array-length v2, v2

    if-lt v0, v2, :cond_1

    move-object v0, v1

    .line 185405
    goto :goto_0

    .line 185406
    :cond_1
    sget-object v2, LX/16c;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 185407
    if-nez v2, :cond_2

    move-object v0, v1

    .line 185408
    goto :goto_0

    .line 185409
    :cond_2
    :try_start_0
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 185410
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 185411
    :catch_0
    move-exception v0

    .line 185412
    const-string v3, "FlatBuffer"

    const-string v4, "Could not create instance for GraphQL type: %s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-static {v3, v0, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    move-object v0, v1

    .line 185413
    goto :goto_0

    .line 185414
    :catch_1
    move-exception v0

    .line 185415
    const-string v3, "FlatBuffer"

    const-string v4, "IllegalAccess when creating instance for GraphQL type: %s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-static {v3, v0, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 185416
    :catch_2
    move-exception v0

    .line 185417
    const-string v3, "FlatBuffer"

    const-string v4, "ClassNotFoundException when creating instance for GraphQL type: %s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-static {v3, v0, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final a(Lcom/facebook/flatbuffers/Flattenable;)S
    .locals 1

    .prologue
    .line 185418
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 185419
    instance-of v0, p1, LX/0jT;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 185420
    check-cast p1, LX/0jT;

    .line 185421
    invoke-interface {p1}, LX/0jT;->f()I

    move-result v0

    invoke-static {v0}, LX/38I;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/23C;->a(Ljava/lang/String;)S

    move-result v0

    return v0
.end method
