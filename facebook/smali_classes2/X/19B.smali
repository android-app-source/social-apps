.class public LX/19B;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/19B;


# instance fields
.field public final a:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/view/WindowManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 207428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207429
    iput-object p1, p0, LX/19B;->a:Landroid/view/WindowManager;

    .line 207430
    return-void
.end method

.method public static a(LX/0QB;)LX/19B;
    .locals 4

    .prologue
    .line 207431
    sget-object v0, LX/19B;->b:LX/19B;

    if-nez v0, :cond_1

    .line 207432
    const-class v1, LX/19B;

    monitor-enter v1

    .line 207433
    :try_start_0
    sget-object v0, LX/19B;->b:LX/19B;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 207434
    if-eqz v2, :cond_0

    .line 207435
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 207436
    new-instance p0, LX/19B;

    invoke-static {v0}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    invoke-direct {p0, v3}, LX/19B;-><init>(Landroid/view/WindowManager;)V

    .line 207437
    move-object v0, p0

    .line 207438
    sput-object v0, LX/19B;->b:LX/19B;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207439
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 207440
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 207441
    :cond_1
    sget-object v0, LX/19B;->b:LX/19B;

    return-object v0

    .line 207442
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 207443
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Landroid/graphics/Point;
    .locals 2

    .prologue
    .line 207444
    iget-object v0, p0, LX/19B;->a:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 207445
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 207446
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 207447
    return-object v1
.end method
