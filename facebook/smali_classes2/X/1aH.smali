.class public LX/1aH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public final e:I
    .annotation build Landroid/support/annotation/Dimension;
    .end annotation
.end field

.field public final f:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p4    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 276663
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 276664
    iput-object p1, p0, LX/1aH;->a:Ljava/lang/String;

    .line 276665
    iput-object p2, p0, LX/1aH;->b:Ljava/lang/String;

    .line 276666
    iput-object p3, p0, LX/1aH;->c:Ljava/lang/String;

    .line 276667
    iput p4, p0, LX/1aH;->d:I

    .line 276668
    iput p5, p0, LX/1aH;->e:I

    .line 276669
    iput-object p6, p0, LX/1aH;->f:Landroid/graphics/drawable/Drawable;

    .line 276670
    return-void
.end method

.method public static a(Landroid/content/res/Resources;LX/1EE;LX/0W9;Z)LX/1aH;
    .locals 7

    .prologue
    .line 276671
    const/4 v6, 0x0

    .line 276672
    iget v0, p1, LX/1EE;->l:I

    move v0, v0

    .line 276673
    if-eqz v0, :cond_0

    .line 276674
    iget v0, p1, LX/1EE;->l:I

    move v0, v0

    .line 276675
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 276676
    :cond_0
    new-instance v0, LX/1aH;

    if-eqz p3, :cond_1

    .line 276677
    iget-object v1, p1, LX/1EE;->g:Ljava/lang/String;

    move-object v1, v1

    .line 276678
    invoke-virtual {p2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    if-eqz p3, :cond_2

    .line 276679
    iget-object v2, p1, LX/1EE;->f:Ljava/lang/String;

    move-object v2, v2

    .line 276680
    invoke-virtual {p2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    if-eqz p3, :cond_3

    .line 276681
    iget-object v3, p1, LX/1EE;->h:Ljava/lang/String;

    move-object v3, v3

    .line 276682
    invoke-virtual {p2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 276683
    :goto_2
    iget v4, p1, LX/1EE;->j:I

    move v4, v4

    .line 276684
    iget v5, p1, LX/1EE;->p:I

    move v5, v5

    .line 276685
    invoke-direct/range {v0 .. v6}, LX/1aH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;)V

    return-object v0

    .line 276686
    :cond_1
    iget-object v1, p1, LX/1EE;->g:Ljava/lang/String;

    move-object v1, v1

    .line 276687
    goto :goto_0

    .line 276688
    :cond_2
    iget-object v2, p1, LX/1EE;->f:Ljava/lang/String;

    move-object v2, v2

    .line 276689
    goto :goto_1

    .line 276690
    :cond_3
    iget-object v3, p1, LX/1EE;->h:Ljava/lang/String;

    move-object v3, v3

    .line 276691
    goto :goto_2
.end method
