.class public LX/1VX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 264651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264652
    if-nez p0, :cond_0

    .line 264653
    const/4 v0, 0x0

    .line 264654
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->B()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1VX;->a(Ljava/util/List;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 3
    .param p0    # Lcom/facebook/graphql/model/GraphQLStoryAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 264655
    if-nez p0, :cond_1

    .line 264656
    :cond_0
    :goto_0
    return-object v0

    .line 264657
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v1

    .line 264658
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264659
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1VX;->a(Ljava/util/List;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;[I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264660
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1VX;->a(Ljava/util/List;[I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 4
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;I)",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 264661
    if-nez p0, :cond_0

    move-object v0, v1

    .line 264662
    :goto_0
    return-object v0

    .line 264663
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_2

    .line 264664
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 264665
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-ne v0, p1, :cond_1

    .line 264666
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    goto :goto_0

    .line 264667
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 264668
    goto :goto_0
.end method

.method public static a(Ljava/util/List;[I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 9
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;[I)",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 264669
    if-eqz p0, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 264670
    :goto_0
    return-object v0

    .line 264671
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_4

    .line 264672
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    .line 264673
    if-eqz v5, :cond_3

    .line 264674
    array-length v6, p1

    move v0, v2

    :goto_2
    if-ge v0, v6, :cond_3

    .line 264675
    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v7

    aget v8, p1, v0

    if-ne v7, v8, :cond_2

    .line 264676
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    goto :goto_0

    .line 264677
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 264678
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 264679
    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Z
    .locals 1

    .prologue
    .line 264680
    invoke-static {p0, p1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
