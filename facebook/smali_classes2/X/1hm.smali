.class public LX/1hm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/1hm;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0Or;)V
    .locals 3
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/IsDialtoneEnabled;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/IsDialtonePhotoFeatureEnabled;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/SendDialtoneHeaderWhenNoToken;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 297049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297050
    iput-object p1, p0, LX/1hm;->a:LX/0Or;

    .line 297051
    iput-object p2, p0, LX/1hm;->b:LX/0Or;

    .line 297052
    iput-object p3, p0, LX/1hm;->c:LX/0Or;

    .line 297053
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/1hm;->d:Ljava/util/Map;

    .line 297054
    iget-object v0, p0, LX/1hm;->d:Ljava/util/Map;

    const-string v1, "X-ZERO-CATEGORY"

    const-string v2, "dialtone"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297055
    return-void
.end method

.method public static a(LX/0QB;)LX/1hm;
    .locals 6

    .prologue
    .line 297056
    sget-object v0, LX/1hm;->e:LX/1hm;

    if-nez v0, :cond_1

    .line 297057
    const-class v1, LX/1hm;

    monitor-enter v1

    .line 297058
    :try_start_0
    sget-object v0, LX/1hm;->e:LX/1hm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 297059
    if-eqz v2, :cond_0

    .line 297060
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 297061
    new-instance v3, LX/1hm;

    const/16 v4, 0x1483

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x1488

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 p0, 0x148a

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, LX/1hm;-><init>(LX/0Or;LX/0Or;LX/0Or;)V

    .line 297062
    move-object v0, v3

    .line 297063
    sput-object v0, LX/1hm;->e:LX/1hm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297064
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 297065
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 297066
    :cond_1
    sget-object v0, LX/1hm;->e:LX/1hm;

    return-object v0

    .line 297067
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 297068
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
