.class public LX/0ac;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ad;
.implements LX/0ae;


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/0ac;",
            ">;"
        }
    .end annotation
.end field

.field private static u:J

.field private static v:J


# instance fields
.field public final b:LX/0aa;

.field public final c:LX/0aN;

.field private final d:LX/0ag;

.field private final e:LX/0af;

.field public final f:LX/0aa;

.field public final g:LX/0aN;

.field private final h:LX/0ag;

.field private final i:LX/0af;

.field private final j:LX/0ZW;

.field public final k:LX/0aM;

.field public final l:LX/0ZT;

.field public final m:LX/0ZU;

.field public final n:Z

.field private final o:Z

.field private volatile p:LX/0eg;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public volatile q:LX/0eg;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final r:LX/0ai;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:LX/5og;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:LX/33I;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85137
    const-class v0, LX/0ac;

    sput-object v0, LX/0ac;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0aa;LX/0aa;LX/0aN;LX/0aN;LX/0af;LX/0af;LX/0ZW;LX/0ZT;LX/0aM;LX/0ZU;ZZLX/03V;LX/5og;LX/33I;)V
    .locals 2
    .param p13    # LX/03V;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p14    # LX/5og;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p15    # LX/33I;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 85138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85139
    iput-object p1, p0, LX/0ac;->b:LX/0aa;

    .line 85140
    iput-object p3, p0, LX/0ac;->c:LX/0aN;

    .line 85141
    iget-object v1, p0, LX/0ac;->b:LX/0aa;

    invoke-static {v1}, LX/0ag;->a(LX/0aa;)LX/0ag;

    move-result-object v1

    iput-object v1, p0, LX/0ac;->d:LX/0ag;

    .line 85142
    iput-object p5, p0, LX/0ac;->e:LX/0af;

    .line 85143
    iput-object p2, p0, LX/0ac;->f:LX/0aa;

    .line 85144
    iput-object p4, p0, LX/0ac;->g:LX/0aN;

    .line 85145
    iget-object v1, p0, LX/0ac;->f:LX/0aa;

    invoke-static {v1}, LX/0ag;->a(LX/0aa;)LX/0ag;

    move-result-object v1

    iput-object v1, p0, LX/0ac;->h:LX/0ag;

    .line 85146
    iput-object p6, p0, LX/0ac;->i:LX/0af;

    .line 85147
    iput-object p7, p0, LX/0ac;->j:LX/0ZW;

    .line 85148
    iput-object p9, p0, LX/0ac;->k:LX/0aM;

    .line 85149
    iput-object p8, p0, LX/0ac;->l:LX/0ZT;

    .line 85150
    iput-object p10, p0, LX/0ac;->m:LX/0ZU;

    .line 85151
    iput-boolean p11, p0, LX/0ac;->n:Z

    .line 85152
    iput-boolean p12, p0, LX/0ac;->o:Z

    .line 85153
    if-nez p13, :cond_0

    const/4 v1, 0x0

    :goto_0
    iput-object v1, p0, LX/0ac;->r:LX/0ai;

    .line 85154
    move-object/from16 v0, p14

    iput-object v0, p0, LX/0ac;->s:LX/5og;

    .line 85155
    move-object/from16 v0, p15

    iput-object v0, p0, LX/0ac;->t:LX/33I;

    .line 85156
    return-void

    .line 85157
    :cond_0
    new-instance v1, LX/0ai;

    invoke-direct {v1, p0, p13}, LX/0ai;-><init>(LX/0ac;LX/03V;)V

    goto :goto_0
.end method

.method private a(LX/0c0;LX/0c1;DLjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(",
            "LX/0c0;",
            "LX/0c1;",
            "D",
            "Ljava/lang/Class",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 85158
    invoke-static {p3, p4}, LX/0c2;->a(D)Z

    move-result v1

    .line 85159
    invoke-static {p3, p4}, LX/0c2;->b(D)I

    move-result v3

    .line 85160
    if-eqz v1, :cond_0

    iget-object v0, p0, LX/0ac;->p:LX/0eg;

    .line 85161
    :goto_0
    invoke-static {v0, v1}, LX/0ac;->a(LX/0eg;Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 85162
    :goto_1
    return-object p6

    .line 85163
    :cond_0
    iget-object v0, p0, LX/0ac;->q:LX/0eg;

    goto :goto_0

    .line 85164
    :cond_1
    sget-object v2, LX/0c1;->On:LX/0c1;

    if-ne p2, v2, :cond_2

    .line 85165
    invoke-static {p0, p1, v1, v3}, LX/0ac;->a(LX/0ac;LX/0c0;ZI)V

    .line 85166
    :cond_2
    iget-object v2, p0, LX/0ac;->s:LX/5og;

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/0ac;->s:LX/5og;

    invoke-interface {v2}, LX/5og;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 85167
    iget-object v2, p0, LX/0ac;->s:LX/5og;

    invoke-static {p0, v2, p1, v1, v3}, LX/0ac;->a(LX/0ac;LX/5og;LX/0c0;ZI)V

    .line 85168
    :cond_3
    sget-object v2, LX/0c0;->Live:LX/0c0;

    if-ne p1, v2, :cond_5

    .line 85169
    iget-object v2, v0, LX/0eg;->j:LX/0ej;

    move-object v0, v2

    .line 85170
    :goto_2
    sget-object v2, LX/0oc;->EFFECTIVE:LX/0oc;

    invoke-virtual {v0, v2, v3, p5, p6}, LX/0ej;->a(LX/0oc;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v7

    .line 85171
    if-nez v1, :cond_4

    iget-object v0, p0, LX/0ac;->t:LX/33I;

    if-eqz v0, :cond_4

    .line 85172
    iget-object v1, p0, LX/0ac;->t:LX/33I;

    move-object v2, p1

    move-wide v4, p3

    move-object v6, p5

    move-object v8, p6

    invoke-virtual/range {v1 .. v8}, LX/33I;->a(LX/0c0;IDLjava/lang/Class;Ljava/lang/Enum;Ljava/lang/Enum;)V

    :cond_4
    move-object p6, v7

    .line 85173
    goto :goto_1

    .line 85174
    :cond_5
    iget-object v2, v0, LX/0eg;->i:LX/0ej;

    move-object v0, v2

    .line 85175
    goto :goto_2
.end method

.method private static a(LX/0ac;LX/0c0;ZI)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 85176
    if-eqz p2, :cond_1

    iget-object v0, p0, LX/0ac;->p:LX/0eg;

    .line 85177
    :goto_0
    if-nez v0, :cond_3

    .line 85178
    sget-object v1, LX/0ac;->a:Ljava/lang/Class;

    const-string v2, "Exposure logged while session%s store was null"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    if-eqz p2, :cond_2

    const-string v0, "less"

    :goto_1
    aput-object v0, v3, v6

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85179
    :cond_0
    :goto_2
    return-void

    .line 85180
    :cond_1
    iget-object v0, p0, LX/0ac;->q:LX/0eg;

    goto :goto_0

    .line 85181
    :cond_2
    const-string v0, "ed"

    goto :goto_1

    .line 85182
    :cond_3
    sget-object v1, LX/0c0;->Cached:LX/0c0;

    if-ne p1, v1, :cond_4

    .line 85183
    iget-object v1, v0, LX/0eg;->i:LX/0ej;

    move-object v0, v1

    .line 85184
    move-object v2, v0

    .line 85185
    :goto_3
    if-eqz p2, :cond_5

    iget-object v0, p0, LX/0ac;->h:LX/0ag;

    move-object v1, v0

    .line 85186
    :goto_4
    if-eqz p2, :cond_6

    iget-object v0, p0, LX/0ac;->i:LX/0af;

    .line 85187
    :goto_5
    invoke-virtual {v1, p3}, LX/0ag;->a(I)I

    move-result v3

    .line 85188
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 85189
    iget-object p1, v0, LX/0af;->a:Ljava/util/concurrent/atomic/AtomicIntegerArray;

    invoke-virtual {p1, v3, v5, v4}, Ljava/util/concurrent/atomic/AtomicIntegerArray;->compareAndSet(III)Z

    move-result p1

    if-nez p1, :cond_7

    :goto_6
    move v0, v4

    .line 85190
    if-nez v0, :cond_0

    .line 85191
    invoke-virtual {v1, v3}, LX/0ag;->b(I)Ljava/lang/String;

    move-result-object v0

    .line 85192
    invoke-virtual {v1, v3}, LX/0ag;->c(I)I

    move-result v1

    .line 85193
    sget-object v3, LX/0oc;->EFFECTIVE:LX/0oc;

    add-int/lit8 v4, v1, 0x2

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/0ej;->a(LX/0oc;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 85194
    sget-object v4, LX/0oc;->EFFECTIVE:LX/0oc;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v2, v4, v1, v6}, LX/0ej;->a(LX/0oc;IZ)Z

    move-result v1

    .line 85195
    iget-object v2, p0, LX/0ac;->j:LX/0ZW;

    invoke-virtual {v2, v1, v0, v3}, LX/0ZW;->a(ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 85196
    :cond_4
    iget-object v1, v0, LX/0eg;->j:LX/0ej;

    move-object v0, v1

    .line 85197
    move-object v2, v0

    goto :goto_3

    .line 85198
    :cond_5
    iget-object v0, p0, LX/0ac;->d:LX/0ag;

    move-object v1, v0

    goto :goto_4

    .line 85199
    :cond_6
    iget-object v0, p0, LX/0ac;->e:LX/0af;

    goto :goto_5

    :cond_7
    move v4, v5

    goto :goto_6
.end method

.method private static a(LX/0ac;LX/5og;LX/0c0;ZI)V
    .locals 6

    .prologue
    .line 85200
    if-eqz p3, :cond_1

    iget-object v0, p0, LX/0ac;->p:LX/0eg;

    .line 85201
    :goto_0
    if-nez v0, :cond_2

    .line 85202
    :cond_0
    :goto_1
    return-void

    .line 85203
    :cond_1
    iget-object v0, p0, LX/0ac;->q:LX/0eg;

    goto :goto_0

    .line 85204
    :cond_2
    sget-object v1, LX/0c0;->Cached:LX/0c0;

    if-ne p2, v1, :cond_3

    .line 85205
    iget-object v1, v0, LX/0eg;->i:LX/0ej;

    move-object v0, v1

    .line 85206
    move-object v1, v0

    .line 85207
    :goto_2
    if-eqz p3, :cond_4

    iget-object v0, p0, LX/0ac;->h:LX/0ag;

    .line 85208
    :goto_3
    invoke-virtual {v0, p4}, LX/0ag;->a(I)I

    move-result v2

    .line 85209
    invoke-virtual {v0, v2}, LX/0ag;->b(I)Ljava/lang/String;

    move-result-object v3

    .line 85210
    invoke-virtual {v0, v2}, LX/0ag;->c(I)I

    move-result v0

    .line 85211
    sget-object v2, LX/0oc;->EFFECTIVE:LX/0oc;

    add-int/lit8 v4, v0, 0x2

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v4, v5}, LX/0ej;->a(LX/0oc;ILjava/lang/String;)Ljava/lang/String;

    .line 85212
    sget-object v2, LX/0oc;->EFFECTIVE:LX/0oc;

    add-int/lit8 v0, v0, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v4}, LX/0ej;->a(LX/0oc;IZ)Z

    move-result v0

    .line 85213
    if-eqz v0, :cond_0

    .line 85214
    sget-object v0, LX/0oc;->EFFECTIVE:LX/0oc;

    invoke-virtual {p0, v0, v3}, LX/0ac;->d(LX/0oc;Ljava/lang/String;)Ljava/util/Map;

    goto :goto_1

    .line 85215
    :cond_3
    iget-object v1, v0, LX/0eg;->j:LX/0ej;

    move-object v0, v1

    .line 85216
    move-object v1, v0

    goto :goto_2

    .line 85217
    :cond_4
    iget-object v0, p0, LX/0ac;->d:LX/0ag;

    goto :goto_3
.end method

.method private static a(LX/0eg;LX/0ag;LX/26w;LX/26y;)V
    .locals 1

    .prologue
    .line 85218
    invoke-virtual {p1, p3}, LX/0ag;->a(LX/26y;)V

    .line 85219
    invoke-virtual {p2}, LX/26w;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0eg;->a(Ljava/nio/ByteBuffer;)V

    .line 85220
    return-void
.end method

.method private static a(LX/0eg;Z)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x1388

    .line 85221
    if-nez p0, :cond_2

    .line 85222
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 85223
    if-eqz p1, :cond_1

    .line 85224
    sget-wide v2, LX/0ac;->v:J

    sub-long v2, v0, v2

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 85225
    sget-object v2, LX/0ac;->a:Ljava/lang/Class;

    const-string v3, "The sessionless store is not available."

    invoke-static {v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 85226
    sput-wide v0, LX/0ac;->v:J

    .line 85227
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 85228
    :goto_1
    return v0

    .line 85229
    :cond_1
    sget-wide v2, LX/0ac;->u:J

    sub-long v2, v0, v2

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 85230
    sget-object v2, LX/0ac;->a:Ljava/lang/Class;

    const-string v3, "The sessioned store is not available. Are you fetching sessioned quick experiment data while the user is logged out?"

    invoke-static {v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 85231
    sput-wide v0, LX/0ac;->u:J

    goto :goto_0

    .line 85232
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private f(Ljava/lang/String;)LX/0eg;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 85233
    iget-object v0, p0, LX/0ac;->h:LX/0ag;

    invoke-virtual {v0, p1}, LX/0ag;->b(Ljava/lang/String;)Z

    move-result v0

    .line 85234
    if-nez v0, :cond_0

    iget-object v1, p0, LX/0ac;->d:LX/0ag;

    invoke-virtual {v1, p1}, LX/0ag;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 85235
    const/4 v0, 0x0

    .line 85236
    :goto_0
    return-object v0

    :cond_0
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0ac;->p:LX/0eg;

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/0ac;->q:LX/0eg;

    goto :goto_0
.end method

.method private g(Ljava/lang/String;)LX/0ag;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 85237
    iget-object v0, p0, LX/0ac;->h:LX/0ag;

    invoke-virtual {v0, p1}, LX/0ag;->b(Ljava/lang/String;)Z

    move-result v0

    .line 85238
    if-nez v0, :cond_0

    iget-object v1, p0, LX/0ac;->d:LX/0ag;

    invoke-virtual {v1, p1}, LX/0ag;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 85239
    const/4 v0, 0x0

    .line 85240
    :goto_0
    return-object v0

    :cond_0
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0ac;->h:LX/0ag;

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/0ac;->d:LX/0ag;

    goto :goto_0
.end method


# virtual methods
.method public final a(FF)F
    .locals 2

    .prologue
    .line 85025
    sget-object v0, LX/0c0;->Cached:LX/0c0;

    sget-object v1, LX/0c1;->On:LX/0c1;

    invoke-virtual {p0, v0, v1, p1, p2}, LX/0ac;->a(LX/0c0;LX/0c1;FF)F

    move-result v0

    return v0
.end method

.method public final a(LX/0c0;LX/0c1;FF)F
    .locals 6

    .prologue
    .line 85241
    invoke-static {p3}, LX/0c2;->a(F)Z

    move-result v1

    .line 85242
    invoke-static {p3}, LX/0c2;->b(F)I

    move-result v2

    .line 85243
    if-eqz v1, :cond_0

    iget-object v0, p0, LX/0ac;->p:LX/0eg;

    .line 85244
    :goto_0
    invoke-static {v0, v1}, LX/0ac;->a(LX/0eg;Z)Z

    move-result v3

    if-nez v3, :cond_1

    .line 85245
    :goto_1
    return p4

    .line 85246
    :cond_0
    iget-object v0, p0, LX/0ac;->q:LX/0eg;

    goto :goto_0

    .line 85247
    :cond_1
    sget-object v3, LX/0c1;->On:LX/0c1;

    if-ne p2, v3, :cond_2

    .line 85248
    invoke-static {p0, p1, v1, v2}, LX/0ac;->a(LX/0ac;LX/0c0;ZI)V

    .line 85249
    :cond_2
    iget-object v3, p0, LX/0ac;->s:LX/5og;

    if-eqz v3, :cond_3

    iget-object v3, p0, LX/0ac;->s:LX/5og;

    invoke-interface {v3}, LX/5og;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 85250
    iget-object v3, p0, LX/0ac;->s:LX/5og;

    invoke-static {p0, v3, p1, v1, v2}, LX/0ac;->a(LX/0ac;LX/5og;LX/0c0;ZI)V

    .line 85251
    :cond_3
    sget-object v3, LX/0c0;->Live:LX/0c0;

    if-ne p1, v3, :cond_5

    .line 85252
    iget-object v3, v0, LX/0eg;->j:LX/0ej;

    move-object v0, v3

    .line 85253
    :goto_2
    sget-object v3, LX/0oc;->EFFECTIVE:LX/0oc;

    invoke-virtual {v0, v3, v2, p4}, LX/0ej;->a(LX/0oc;IF)F

    move-result v4

    .line 85254
    if-nez v1, :cond_4

    iget-object v0, p0, LX/0ac;->t:LX/33I;

    if-eqz v0, :cond_4

    .line 85255
    iget-object v0, p0, LX/0ac;->t:LX/33I;

    move-object v1, p1

    move v3, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, LX/33I;->a(LX/0c0;IFFF)V

    :cond_4
    move p4, v4

    .line 85256
    goto :goto_1

    .line 85257
    :cond_5
    iget-object v3, v0, LX/0eg;->i:LX/0ej;

    move-object v0, v3

    .line 85258
    goto :goto_2
.end method

.method public final a(II)I
    .locals 2

    .prologue
    .line 85259
    sget-object v0, LX/0c0;->Cached:LX/0c0;

    sget-object v1, LX/0c1;->On:LX/0c1;

    invoke-virtual {p0, v0, v1, p1, p2}, LX/0ac;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    return v0
.end method

.method public final a(LX/0c0;II)I
    .locals 1

    .prologue
    .line 85314
    sget-object v0, LX/0c1;->On:LX/0c1;

    invoke-virtual {p0, p1, v0, p2, p3}, LX/0ac;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    return v0
.end method

.method public final a(LX/0c0;LX/0c1;II)I
    .locals 6

    .prologue
    .line 85260
    invoke-static {p3}, LX/0c2;->a(I)Z

    move-result v1

    .line 85261
    shr-int/lit8 v0, p3, 0x1

    move v2, v0

    .line 85262
    if-eqz v1, :cond_0

    iget-object v0, p0, LX/0ac;->p:LX/0eg;

    .line 85263
    :goto_0
    invoke-static {v0, v1}, LX/0ac;->a(LX/0eg;Z)Z

    move-result v3

    if-nez v3, :cond_1

    .line 85264
    :goto_1
    return p4

    .line 85265
    :cond_0
    iget-object v0, p0, LX/0ac;->q:LX/0eg;

    goto :goto_0

    .line 85266
    :cond_1
    sget-object v3, LX/0c1;->On:LX/0c1;

    if-ne p2, v3, :cond_2

    .line 85267
    invoke-static {p0, p1, v1, v2}, LX/0ac;->a(LX/0ac;LX/0c0;ZI)V

    .line 85268
    :cond_2
    iget-object v3, p0, LX/0ac;->s:LX/5og;

    if-eqz v3, :cond_3

    iget-object v3, p0, LX/0ac;->s:LX/5og;

    invoke-interface {v3}, LX/5og;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 85269
    iget-object v3, p0, LX/0ac;->s:LX/5og;

    invoke-static {p0, v3, p1, v1, v2}, LX/0ac;->a(LX/0ac;LX/5og;LX/0c0;ZI)V

    .line 85270
    :cond_3
    sget-object v3, LX/0c0;->Live:LX/0c0;

    if-ne p1, v3, :cond_5

    .line 85271
    iget-object v3, v0, LX/0eg;->j:LX/0ej;

    move-object v0, v3

    .line 85272
    :goto_2
    sget-object v3, LX/0oc;->EFFECTIVE:LX/0oc;

    invoke-virtual {v0, v3, v2, p4}, LX/0ej;->a(LX/0oc;II)I

    move-result v4

    .line 85273
    if-nez v1, :cond_4

    iget-object v0, p0, LX/0ac;->t:LX/33I;

    if-eqz v0, :cond_4

    .line 85274
    iget-object v0, p0, LX/0ac;->t:LX/33I;

    move-object v1, p1

    move v3, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, LX/33I;->a(LX/0c0;IIII)V

    :cond_4
    move p4, v4

    .line 85275
    goto :goto_1

    .line 85276
    :cond_5
    iget-object v3, v0, LX/0eg;->i:LX/0ej;

    move-object v0, v3

    .line 85277
    goto :goto_2
.end method

.method public final a(JJ)J
    .locals 9

    .prologue
    .line 85313
    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->On:LX/0c1;

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-virtual/range {v1 .. v7}, LX/0ac;->a(LX/0c0;LX/0c1;JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(LX/0c0;JJ)J
    .locals 8

    .prologue
    .line 85312
    sget-object v3, LX/0c1;->On:LX/0c1;

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-virtual/range {v1 .. v7}, LX/0ac;->a(LX/0c0;LX/0c1;JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(LX/0c0;LX/0c1;JJ)J
    .locals 13

    .prologue
    .line 85296
    invoke-static/range {p3 .. p4}, LX/0c2;->a(J)Z

    move-result v3

    .line 85297
    invoke-static/range {p3 .. p4}, LX/0c2;->b(J)I

    move-result v5

    .line 85298
    if-eqz v3, :cond_0

    iget-object v2, p0, LX/0ac;->p:LX/0eg;

    .line 85299
    :goto_0
    invoke-static {v2, v3}, LX/0ac;->a(LX/0eg;Z)Z

    move-result v4

    if-nez v4, :cond_1

    .line 85300
    :goto_1
    return-wide p5

    .line 85301
    :cond_0
    iget-object v2, p0, LX/0ac;->q:LX/0eg;

    goto :goto_0

    .line 85302
    :cond_1
    sget-object v4, LX/0c1;->On:LX/0c1;

    if-ne p2, v4, :cond_2

    .line 85303
    invoke-static {p0, p1, v3, v5}, LX/0ac;->a(LX/0ac;LX/0c0;ZI)V

    .line 85304
    :cond_2
    iget-object v4, p0, LX/0ac;->s:LX/5og;

    if-eqz v4, :cond_3

    iget-object v4, p0, LX/0ac;->s:LX/5og;

    invoke-interface {v4}, LX/5og;->a()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 85305
    iget-object v4, p0, LX/0ac;->s:LX/5og;

    invoke-static {p0, v4, p1, v3, v5}, LX/0ac;->a(LX/0ac;LX/5og;LX/0c0;ZI)V

    .line 85306
    :cond_3
    sget-object v4, LX/0c0;->Live:LX/0c0;

    if-ne p1, v4, :cond_5

    invoke-virtual {v2}, LX/0eg;->d()LX/0ej;

    move-result-object v2

    .line 85307
    :goto_2
    sget-object v4, LX/0oc;->EFFECTIVE:LX/0oc;

    move-wide/from16 v0, p5

    invoke-virtual {v2, v4, v5, v0, v1}, LX/0ej;->a(LX/0oc;IJ)J

    move-result-wide v8

    .line 85308
    if-nez v3, :cond_4

    iget-object v2, p0, LX/0ac;->t:LX/33I;

    if-eqz v2, :cond_4

    .line 85309
    iget-object v3, p0, LX/0ac;->t:LX/33I;

    move-object v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v10, p5

    invoke-virtual/range {v3 .. v11}, LX/33I;->a(LX/0c0;IJJJ)V

    :cond_4
    move-wide/from16 p5, v8

    .line 85310
    goto :goto_1

    .line 85311
    :cond_5
    invoke-virtual {v2}, LX/0eg;->c()LX/0ej;

    move-result-object v2

    goto :goto_2
.end method

.method public final a(DLjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(D",
            "Ljava/lang/Class",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 85295
    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->On:LX/0c1;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v7}, LX/0ac;->a(LX/0c0;LX/0c1;DLjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    return-object v0
.end method

.method public final a(CILandroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85293
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/0ac;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 85294
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p3, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(CLjava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 85292
    sget-object v0, LX/0c0;->Cached:LX/0c0;

    sget-object v1, LX/0c1;->On:LX/0c1;

    invoke-virtual {p0, v0, v1, p1, p2}, LX/0ac;->a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0c0;CILandroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85290
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/0ac;->a(LX/0c0;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 85291
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p4, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0c0;CLjava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85289
    sget-object v0, LX/0c1;->On:LX/0c1;

    invoke-virtual {p0, p1, v0, p2, p3}, LX/0ac;->a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 85119
    invoke-static {p3}, LX/0c2;->a(C)Z

    move-result v1

    .line 85120
    shr-int/lit8 v0, p3, 0x1

    move v2, v0

    .line 85121
    if-eqz v1, :cond_0

    iget-object v0, p0, LX/0ac;->p:LX/0eg;

    .line 85122
    :goto_0
    invoke-static {v0, v1}, LX/0ac;->a(LX/0eg;Z)Z

    move-result v3

    if-nez v3, :cond_1

    .line 85123
    :goto_1
    return-object p4

    .line 85124
    :cond_0
    iget-object v0, p0, LX/0ac;->q:LX/0eg;

    goto :goto_0

    .line 85125
    :cond_1
    sget-object v3, LX/0c1;->On:LX/0c1;

    if-ne p2, v3, :cond_2

    .line 85126
    invoke-static {p0, p1, v1, v2}, LX/0ac;->a(LX/0ac;LX/0c0;ZI)V

    .line 85127
    :cond_2
    iget-object v3, p0, LX/0ac;->s:LX/5og;

    if-eqz v3, :cond_3

    iget-object v3, p0, LX/0ac;->s:LX/5og;

    invoke-interface {v3}, LX/5og;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 85128
    iget-object v3, p0, LX/0ac;->s:LX/5og;

    invoke-static {p0, v3, p1, v1, v2}, LX/0ac;->a(LX/0ac;LX/5og;LX/0c0;ZI)V

    .line 85129
    :cond_3
    sget-object v3, LX/0c0;->Live:LX/0c0;

    if-ne p1, v3, :cond_5

    .line 85130
    iget-object v3, v0, LX/0eg;->j:LX/0ej;

    move-object v0, v3

    .line 85131
    :goto_2
    sget-object v3, LX/0oc;->EFFECTIVE:LX/0oc;

    invoke-virtual {v0, v3, v2, p4}, LX/0ej;->a(LX/0oc;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 85132
    if-nez v1, :cond_4

    iget-object v0, p0, LX/0ac;->t:LX/33I;

    if-eqz v0, :cond_4

    .line 85133
    iget-object v0, p0, LX/0ac;->t:LX/33I;

    move-object v1, p1

    move v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, LX/33I;->a(LX/0c0;ICLjava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object p4, v4

    .line 85134
    goto :goto_1

    .line 85135
    :cond_5
    iget-object v3, v0, LX/0eg;->i:LX/0ej;

    move-object v0, v3

    .line 85136
    goto :goto_2
.end method

.method public final a()V
    .locals 15

    .prologue
    .line 85278
    iget-object v0, p0, LX/0ac;->k:LX/0aM;

    invoke-virtual {v0}, LX/0aM;->a()Ljava/lang/String;

    move-result-object v3

    .line 85279
    iget-object v0, p0, LX/0ac;->g:LX/0aN;

    iget-object v1, p0, LX/0ac;->f:LX/0aa;

    iget-object v2, p0, LX/0ac;->h:LX/0ag;

    iget-boolean v4, p0, LX/0ac;->n:Z

    iget-boolean v5, p0, LX/0ac;->o:Z

    iget-object v6, p0, LX/0ac;->r:LX/0ai;

    .line 85280
    new-instance v7, LX/0eg;

    move-object v8, v0

    move-object v9, v1

    move-object v10, v2

    move-object v11, v3

    move v12, v4

    move v13, v5

    move-object v14, v6

    invoke-direct/range {v7 .. v14}, LX/0eg;-><init>(LX/0aN;LX/0aa;LX/0ag;Ljava/lang/String;ZZLX/0ai;)V

    move-object v0, v7

    .line 85281
    invoke-virtual {v0}, LX/0eg;->a()V

    .line 85282
    iput-object v0, p0, LX/0ac;->p:LX/0eg;

    .line 85283
    iget-object v0, p0, LX/0ac;->l:LX/0ZT;

    invoke-virtual {v0}, LX/0ZT;->a()Ljava/lang/String;

    move-result-object v3

    .line 85284
    if-eqz v3, :cond_0

    .line 85285
    iget-object v0, p0, LX/0ac;->c:LX/0aN;

    iget-object v1, p0, LX/0ac;->b:LX/0aa;

    iget-object v2, p0, LX/0ac;->d:LX/0ag;

    iget-boolean v4, p0, LX/0ac;->n:Z

    iget-boolean v5, p0, LX/0ac;->o:Z

    iget-object v6, p0, LX/0ac;->r:LX/0ai;

    invoke-static/range {v0 .. v6}, LX/0eg;->b(LX/0aN;LX/0aa;LX/0ag;Ljava/lang/String;ZZLX/0ai;)LX/0eg;

    move-result-object v0

    .line 85286
    invoke-virtual {v0}, LX/0eg;->a()V

    .line 85287
    iput-object v0, p0, LX/0ac;->q:LX/0eg;

    .line 85288
    :cond_0
    return-void
.end method

.method public final a(LX/0c0;C)V
    .locals 2

    .prologue
    .line 84944
    invoke-static {p2}, LX/0c2;->a(C)Z

    move-result v0

    .line 84945
    shr-int/lit8 v1, p2, 0x1

    move v1, v1

    .line 84946
    invoke-static {p0, p1, v0, v1}, LX/0ac;->a(LX/0ac;LX/0c0;ZI)V

    .line 84947
    return-void
.end method

.method public final a(LX/0c0;I)V
    .locals 2

    .prologue
    .line 85021
    invoke-static {p2}, LX/0c2;->a(I)Z

    move-result v0

    .line 85022
    shr-int/lit8 v1, p2, 0x1

    move v1, v1

    .line 85023
    invoke-static {p0, p1, v0, v1}, LX/0ac;->a(LX/0ac;LX/0c0;ZI)V

    .line 85024
    return-void
.end method

.method public final a(LX/0c0;S)V
    .locals 2

    .prologue
    .line 85017
    invoke-static {p2}, LX/0c2;->a(S)Z

    move-result v0

    .line 85018
    invoke-static {p2}, LX/0c2;->b(S)I

    move-result v1

    .line 85019
    invoke-static {p0, p1, v0, v1}, LX/0ac;->a(LX/0ac;LX/0c0;ZI)V

    .line 85020
    return-void
.end method

.method public final a(Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;)V
    .locals 7

    .prologue
    const/4 v0, -0x1

    .line 85003
    invoke-virtual {p1}, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->a()Ljava/lang/String;

    move-result-object v2

    .line 85004
    invoke-direct {p0, v2}, LX/0ac;->f(Ljava/lang/String;)LX/0eg;

    move-result-object v6

    .line 85005
    invoke-direct {p0, v2}, LX/0ac;->g(Ljava/lang/String;)LX/0ag;

    move-result-object v1

    .line 85006
    if-nez v1, :cond_1

    move v4, v0

    .line 85007
    :goto_0
    if-eqz v6, :cond_0

    if-ne v4, v0, :cond_2

    .line 85008
    :cond_0
    sget-object v0, LX/0ac;->a:Ljava/lang/Class;

    const-string v1, "Unrecognized experiment: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v0, v1, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85009
    :goto_1
    return-void

    .line 85010
    :cond_1
    invoke-virtual {v1, v2}, LX/0ag;->a(Ljava/lang/String;)I

    move-result v4

    goto :goto_0

    .line 85011
    :cond_2
    new-instance v3, LX/26w;

    .line 85012
    iget-object v0, v6, LX/0eg;->c:LX/0aa;

    move-object v0, v0

    .line 85013
    invoke-interface {v0}, LX/0aa;->a()I

    move-result v0

    invoke-direct {v3, v0}, LX/26w;-><init>(I)V

    .line 85014
    new-instance v0, LX/5oj;

    .line 85015
    iget-object v2, v6, LX/0eg;->j:LX/0ej;

    move-object v2, v2

    .line 85016
    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/5oj;-><init>(LX/0ag;LX/0ej;LX/26w;ILcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;)V

    invoke-static {v6, v1, v3, v0}, LX/0ac;->a(LX/0eg;LX/0ag;LX/26w;LX/26y;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 84996
    iget-object v0, p0, LX/0ac;->q:LX/0eg;

    if-eqz v0, :cond_0

    .line 84997
    sget-object v0, LX/0ac;->a:Ljava/lang/Class;

    const-string v1, "User was already logged in when handleUserLogin was called"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 84998
    :cond_0
    iget-object v0, p0, LX/0ac;->e:LX/0af;

    invoke-virtual {v0}, LX/0af;->a()V

    .line 84999
    iget-object v0, p0, LX/0ac;->c:LX/0aN;

    iget-object v1, p0, LX/0ac;->b:LX/0aa;

    iget-object v2, p0, LX/0ac;->d:LX/0ag;

    iget-boolean v4, p0, LX/0ac;->n:Z

    iget-boolean v5, p0, LX/0ac;->o:Z

    iget-object v6, p0, LX/0ac;->r:LX/0ai;

    move-object v3, p1

    invoke-static/range {v0 .. v6}, LX/0eg;->b(LX/0aN;LX/0aa;LX/0ag;Ljava/lang/String;ZZLX/0ai;)LX/0eg;

    move-result-object v0

    .line 85000
    invoke-virtual {v0}, LX/0eg;->a()V

    .line 85001
    iput-object v0, p0, LX/0ac;->q:LX/0eg;

    .line 85002
    return-void
.end method

.method public final a(Ljava/util/Map;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/qe/api/manager/SyncedExperimentData;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 84983
    if-eqz p2, :cond_1

    iget-object v0, p0, LX/0ac;->p:LX/0eg;

    .line 84984
    :goto_0
    if-eqz v0, :cond_0

    .line 84985
    new-instance v1, LX/26w;

    .line 84986
    iget-object v2, v0, LX/0eg;->c:LX/0aa;

    move-object v2, v2

    .line 84987
    invoke-interface {v2}, LX/0aa;->a()I

    move-result v2

    invoke-direct {v1, v2}, LX/26w;-><init>(I)V

    .line 84988
    iget-object v2, v0, LX/0eg;->j:LX/0ej;

    move-object v2, v2

    .line 84989
    iget-object p0, v0, LX/0eg;->c:LX/0aa;

    move-object p0, p0

    .line 84990
    invoke-static {p0}, LX/0ag;->a(LX/0aa;)LX/0ag;

    move-result-object p0

    .line 84991
    new-instance p2, LX/2WU;

    invoke-direct {p2, p1, v2, v1}, LX/2WU;-><init>(Ljava/util/Map;LX/0ej;LX/26w;)V

    invoke-virtual {p0, p2}, LX/0ag;->a(LX/26y;)V

    .line 84992
    invoke-virtual {v1}, LX/26w;->a()Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 84993
    invoke-virtual {v0, v2}, LX/0eg;->a(Ljava/nio/ByteBuffer;)V

    .line 84994
    :cond_0
    return-void

    .line 84995
    :cond_1
    iget-object v0, p0, LX/0ac;->q:LX/0eg;

    goto :goto_0
.end method

.method public final a(LX/0c0;LX/0c1;SZ)Z
    .locals 6

    .prologue
    .line 84965
    invoke-static {p3}, LX/0c2;->a(S)Z

    move-result v1

    .line 84966
    invoke-static {p3}, LX/0c2;->b(S)I

    move-result v2

    .line 84967
    if-eqz v1, :cond_0

    iget-object v0, p0, LX/0ac;->p:LX/0eg;

    .line 84968
    :goto_0
    invoke-static {v0, v1}, LX/0ac;->a(LX/0eg;Z)Z

    move-result v3

    if-nez v3, :cond_1

    .line 84969
    :goto_1
    return p4

    .line 84970
    :cond_0
    iget-object v0, p0, LX/0ac;->q:LX/0eg;

    goto :goto_0

    .line 84971
    :cond_1
    sget-object v3, LX/0c1;->On:LX/0c1;

    if-ne p2, v3, :cond_2

    .line 84972
    invoke-static {p0, p1, v1, v2}, LX/0ac;->a(LX/0ac;LX/0c0;ZI)V

    .line 84973
    :cond_2
    iget-object v3, p0, LX/0ac;->s:LX/5og;

    if-eqz v3, :cond_3

    iget-object v3, p0, LX/0ac;->s:LX/5og;

    invoke-interface {v3}, LX/5og;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 84974
    iget-object v3, p0, LX/0ac;->s:LX/5og;

    invoke-static {p0, v3, p1, v1, v2}, LX/0ac;->a(LX/0ac;LX/5og;LX/0c0;ZI)V

    .line 84975
    :cond_3
    sget-object v3, LX/0c0;->Live:LX/0c0;

    if-ne p1, v3, :cond_5

    .line 84976
    iget-object v3, v0, LX/0eg;->j:LX/0ej;

    move-object v0, v3

    .line 84977
    :goto_2
    sget-object v3, LX/0oc;->EFFECTIVE:LX/0oc;

    invoke-virtual {v0, v3, v2, p4}, LX/0ej;->a(LX/0oc;IZ)Z

    move-result v4

    .line 84978
    if-nez v1, :cond_4

    iget-object v0, p0, LX/0ac;->t:LX/33I;

    if-eqz v0, :cond_4

    .line 84979
    iget-object v0, p0, LX/0ac;->t:LX/33I;

    move-object v1, p1

    move v3, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, LX/33I;->a(LX/0c0;ISZZ)V

    :cond_4
    move p4, v4

    .line 84980
    goto :goto_1

    .line 84981
    :cond_5
    iget-object v3, v0, LX/0eg;->i:LX/0ej;

    move-object v0, v3

    .line 84982
    goto :goto_2
.end method

.method public final a(LX/0c0;SZ)Z
    .locals 1

    .prologue
    .line 84964
    sget-object v0, LX/0c1;->On:LX/0c1;

    invoke-virtual {p0, p1, v0, p2, p3}, LX/0ac;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    return v0
.end method

.method public final a(LX/0oc;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 84949
    iget-object v0, p0, LX/0ac;->d:LX/0ag;

    invoke-virtual {v0, p2}, LX/0ag;->b(Ljava/lang/String;)Z

    move-result v0

    .line 84950
    if-nez v0, :cond_5

    .line 84951
    iget-object v0, p0, LX/0ac;->h:LX/0ag;

    invoke-virtual {v0, p2}, LX/0ag;->b(Ljava/lang/String;)Z

    move-result v0

    .line 84952
    if-nez v0, :cond_1

    .line 84953
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v3, v1

    .line 84954
    :goto_1
    if-eqz v3, :cond_2

    iget-object v0, p0, LX/0ac;->q:LX/0eg;

    move-object v2, v0

    .line 84955
    :goto_2
    if-nez v3, :cond_3

    const/4 v0, 0x1

    :goto_3
    invoke-static {v2, v0}, LX/0ac;->a(LX/0eg;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84956
    if-eqz v3, :cond_4

    iget-object v0, p0, LX/0ac;->d:LX/0ag;

    .line 84957
    :goto_4
    invoke-virtual {v0, p2}, LX/0ag;->a(Ljava/lang/String;)I

    move-result v3

    .line 84958
    invoke-virtual {v0, v3}, LX/0ag;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 84959
    iget-object v3, v2, LX/0eg;->j:LX/0ej;

    move-object v2, v3

    .line 84960
    invoke-virtual {v2, p1, v0, v1}, LX/0ej;->a(LX/0oc;IZ)Z

    move-result v1

    goto :goto_0

    .line 84961
    :cond_2
    iget-object v0, p0, LX/0ac;->p:LX/0eg;

    move-object v2, v0

    goto :goto_2

    :cond_3
    move v0, v1

    .line 84962
    goto :goto_3

    .line 84963
    :cond_4
    iget-object v0, p0, LX/0ac;->h:LX/0ag;

    goto :goto_4

    :cond_5
    move v3, v0

    goto :goto_1
.end method

.method public final a(SZ)Z
    .locals 2

    .prologue
    .line 84948
    sget-object v0, LX/0c0;->Cached:LX/0c0;

    sget-object v1, LX/0c1;->On:LX/0c1;

    invoke-virtual {p0, v0, v1, p1, p2}, LX/0ac;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    return v0
.end method

.method public final b(LX/0oc;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 84933
    invoke-direct {p0, p2}, LX/0ac;->f(Ljava/lang/String;)LX/0eg;

    move-result-object v3

    .line 84934
    invoke-direct {p0, p2}, LX/0ac;->g(Ljava/lang/String;)LX/0ag;

    move-result-object v4

    .line 84935
    if-nez v4, :cond_2

    move v1, v2

    .line 84936
    :goto_0
    if-eqz v3, :cond_0

    if-ne v1, v2, :cond_3

    .line 84937
    :cond_0
    sget-object v1, LX/0ac;->a:Ljava/lang/Class;

    const-string v2, "Unrecognized experiment: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84938
    :cond_1
    :goto_1
    return-object v0

    .line 84939
    :cond_2
    invoke-virtual {v4, p2}, LX/0ag;->a(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 84940
    :cond_3
    invoke-virtual {v4, v1}, LX/0ag;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    .line 84941
    iget-object v2, v3, LX/0eg;->j:LX/0ej;

    move-object v2, v2

    .line 84942
    invoke-virtual {v2, p1, v1}, LX/0ej;->f(LX/0oc;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 84943
    invoke-virtual {v2, p1, v1}, LX/0ej;->a(LX/0oc;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 85026
    iget-object v0, p0, LX/0ac;->q:LX/0eg;

    if-nez v0, :cond_0

    .line 85027
    sget-object v0, LX/0ac;->a:Ljava/lang/Class;

    const-string v1, "User was already logged out when handleUserLogout was called"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 85028
    :cond_0
    iget-boolean v0, p0, LX/0ac;->n:Z

    if-eqz v0, :cond_1

    .line 85029
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/0ac;->q:LX/0eg;

    .line 85030
    return-void

    .line 85031
    :cond_1
    iget-object v0, p0, LX/0ac;->k:LX/0aM;

    invoke-virtual {v0}, LX/0aM;->a()Ljava/lang/String;

    move-result-object v0

    .line 85032
    new-instance v1, Ljava/util/HashSet;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 85033
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 85034
    iget-object v2, p0, LX/0ac;->g:LX/0aN;

    invoke-virtual {v2, v1}, LX/0aN;->a(Ljava/util/Set;)V

    .line 85035
    iget-object v1, p0, LX/0ac;->g:LX/0aN;

    iget-object v2, p0, LX/0ac;->f:LX/0aa;

    invoke-interface {v2}, LX/0aa;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0aN;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 85036
    iget-object v0, p0, LX/0ac;->q:LX/0eg;

    if-nez v0, :cond_2

    .line 85037
    sget-object v0, LX/0ac;->a:Ljava/lang/Class;

    const-string v1, "Attempted to call cleanupOnUserLogout when we didn\'t have a sessioned store."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 85038
    :cond_2
    iget-object v0, p0, LX/0ac;->l:LX/0ZT;

    invoke-virtual {v0}, LX/0ZT;->a()Ljava/lang/String;

    move-result-object v0

    .line 85039
    iget-object v1, p0, LX/0ac;->m:LX/0ZU;

    .line 85040
    iget-object v2, v1, LX/0ZU;->a:LX/0ZV;

    invoke-virtual {v2}, LX/0ZV;->a()Ljava/util/List;

    move-result-object v2

    move-object v1, v2

    .line 85041
    new-instance v2, Ljava/util/HashSet;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(I)V

    .line 85042
    invoke-virtual {v2, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 85043
    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 85044
    iget-object v1, p0, LX/0ac;->c:LX/0aN;

    invoke-virtual {v1, v2}, LX/0aN;->a(Ljava/util/Set;)V

    .line 85045
    iget-object v1, p0, LX/0ac;->c:LX/0aN;

    iget-object v2, p0, LX/0ac;->b:LX/0aa;

    invoke-interface {v2}, LX/0aa;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0aN;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 85046
    iget-object v0, p0, LX/0ac;->d:LX/0ag;

    invoke-virtual {v0, p1}, LX/0ag;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0ac;->h:LX/0ag;

    invoke-virtual {v0, p1}, LX/0ag;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/Iterable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85047
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Iterable;

    const/4 v1, 0x0

    invoke-virtual {p0}, LX/0ac;->d()Ljava/lang/Iterable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, LX/0ac;->e()Ljava/lang/Iterable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 85048
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0Tp;->b(Z)V

    .line 85049
    new-instance v1, LX/5JL;

    invoke-direct {v1, v0}, LX/5JL;-><init>([Ljava/lang/Iterable;)V

    move-object v0, v1

    .line 85050
    return-object v0

    .line 85051
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final c(LX/0oc;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 85052
    invoke-direct {p0, p2}, LX/0ac;->f(Ljava/lang/String;)LX/0eg;

    move-result-object v3

    .line 85053
    invoke-direct {p0, p2}, LX/0ac;->g(Ljava/lang/String;)LX/0ag;

    move-result-object v4

    .line 85054
    if-nez v4, :cond_2

    move v1, v2

    .line 85055
    :goto_0
    if-eqz v3, :cond_0

    if-ne v1, v2, :cond_3

    .line 85056
    :cond_0
    sget-object v1, LX/0ac;->a:Ljava/lang/Class;

    const-string v2, "Unrecognized experiment: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85057
    :cond_1
    :goto_1
    return-object v0

    .line 85058
    :cond_2
    invoke-virtual {v4, p2}, LX/0ag;->a(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 85059
    :cond_3
    invoke-virtual {v4, v1}, LX/0ag;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x3

    .line 85060
    iget-object v2, v3, LX/0eg;->j:LX/0ej;

    move-object v2, v2

    .line 85061
    invoke-virtual {v2, p1, v1}, LX/0ej;->f(LX/0oc;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 85062
    invoke-virtual {v2, p1, v1}, LX/0ej;->a(LX/0oc;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v3, -0x1

    const/4 v0, 0x0

    .line 85063
    invoke-direct {p0, p1}, LX/0ac;->f(Ljava/lang/String;)LX/0eg;

    move-result-object v4

    .line 85064
    invoke-direct {p0, p1}, LX/0ac;->g(Ljava/lang/String;)LX/0ag;

    move-result-object v5

    .line 85065
    if-nez v5, :cond_2

    move v2, v3

    .line 85066
    :goto_0
    if-eqz v4, :cond_0

    if-ne v2, v3, :cond_3

    .line 85067
    :cond_0
    sget-object v2, LX/0ac;->a:Ljava/lang/Class;

    const-string v3, "Unrecognized experiment: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v0

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85068
    :cond_1
    :goto_1
    return v0

    .line 85069
    :cond_2
    invoke-virtual {v5, p1}, LX/0ag;->a(Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 85070
    :cond_3
    invoke-virtual {v5, v2}, LX/0ag;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x3

    .line 85071
    iget-object v3, v4, LX/0eg;->j:LX/0ej;

    move-object v3, v3

    .line 85072
    sget-object v4, LX/0oc;->OVERRIDE:LX/0oc;

    invoke-virtual {v3, v4, v2}, LX/0ej;->f(LX/0oc;I)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v3, v2}, LX/0ej;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final d()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85073
    iget-object v0, p0, LX/0ac;->d:LX/0ag;

    invoke-virtual {v0}, LX/0ag;->b()Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public final d(LX/0oc;Ljava/lang/String;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0oc;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 85074
    invoke-direct {p0, p2}, LX/0ac;->f(Ljava/lang/String;)LX/0eg;

    move-result-object v2

    .line 85075
    invoke-direct {p0, p2}, LX/0ac;->g(Ljava/lang/String;)LX/0ag;

    move-result-object v3

    .line 85076
    if-nez v3, :cond_1

    move v0, v1

    .line 85077
    :goto_0
    if-eqz v2, :cond_0

    if-ne v0, v1, :cond_2

    .line 85078
    :cond_0
    sget-object v0, LX/0ac;->a:Ljava/lang/Class;

    const-string v1, "Unrecognized experiment: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85079
    const/4 v0, 0x0

    .line 85080
    :goto_1
    return-object v0

    .line 85081
    :cond_1
    invoke-virtual {v3, p2}, LX/0ag;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 85082
    :cond_2
    iget-object v0, v2, LX/0eg;->j:LX/0ej;

    move-object v1, v0

    .line 85083
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 85084
    new-instance v2, LX/5oh;

    invoke-direct {v2, p1, v1, p2, v0}, LX/5oh;-><init>(LX/0oc;LX/0ej;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {v3, v2}, LX/0ag;->a(LX/26y;)V

    goto :goto_1
.end method

.method public final d(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 85085
    invoke-direct {p0, p1}, LX/0ac;->f(Ljava/lang/String;)LX/0eg;

    move-result-object v2

    .line 85086
    invoke-direct {p0, p1}, LX/0ac;->g(Ljava/lang/String;)LX/0ag;

    move-result-object v3

    .line 85087
    if-nez v3, :cond_1

    move v0, v1

    .line 85088
    :goto_0
    if-eqz v2, :cond_0

    if-ne v0, v1, :cond_2

    .line 85089
    :cond_0
    sget-object v0, LX/0ac;->a:Ljava/lang/Class;

    const-string v1, "Unrecognized experiment: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85090
    :goto_1
    return-void

    .line 85091
    :cond_1
    invoke-virtual {v3, p1}, LX/0ag;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 85092
    :cond_2
    new-instance v1, LX/26w;

    .line 85093
    iget-object v4, v2, LX/0eg;->c:LX/0aa;

    move-object v4, v4

    .line 85094
    invoke-interface {v4}, LX/0aa;->a()I

    move-result v4

    invoke-direct {v1, v4}, LX/26w;-><init>(I)V

    .line 85095
    new-instance v4, LX/5oi;

    .line 85096
    iget-object v5, v2, LX/0eg;->j:LX/0ej;

    move-object v5, v5

    .line 85097
    invoke-direct {v4, v3, v5, v1, v0}, LX/5oi;-><init>(LX/0ag;LX/0ej;LX/26w;I)V

    invoke-static {v2, v3, v1, v4}, LX/0ac;->a(LX/0eg;LX/0ag;LX/26w;LX/26y;)V

    goto :goto_1
.end method

.method public final e()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85098
    iget-object v0, p0, LX/0ac;->h:LX/0ag;

    invoke-virtual {v0}, LX/0ag;->b()Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 85099
    invoke-direct {p0, p1}, LX/0ac;->f(Ljava/lang/String;)LX/0eg;

    move-result-object v2

    .line 85100
    invoke-direct {p0, p1}, LX/0ac;->g(Ljava/lang/String;)LX/0ag;

    move-result-object v3

    .line 85101
    if-nez v3, :cond_1

    move v0, v1

    .line 85102
    :goto_0
    if-eqz v2, :cond_0

    if-ne v0, v1, :cond_2

    .line 85103
    :cond_0
    sget-object v0, LX/0ac;->a:Ljava/lang/Class;

    const-string v1, "Unrecognized experiment: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85104
    :goto_1
    return-void

    .line 85105
    :cond_1
    invoke-virtual {v3, p1}, LX/0ag;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 85106
    :cond_2
    new-instance v1, LX/26w;

    .line 85107
    iget-object v4, v2, LX/0eg;->c:LX/0aa;

    move-object v4, v4

    .line 85108
    invoke-interface {v4}, LX/0aa;->a()I

    move-result v4

    invoke-direct {v1, v4}, LX/26w;-><init>(I)V

    .line 85109
    new-instance v4, LX/5ok;

    .line 85110
    iget-object v5, v2, LX/0eg;->j:LX/0ej;

    move-object v5, v5

    .line 85111
    invoke-direct {v4, v3, v5, v1, v0}, LX/5ok;-><init>(LX/0ag;LX/0ej;LX/26w;I)V

    invoke-static {v2, v3, v1, v4}, LX/0ac;->a(LX/0eg;LX/0ag;LX/26w;LX/26y;)V

    goto :goto_1
.end method

.method public final f()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 85112
    iget-object v1, p0, LX/0ac;->q:LX/0eg;

    .line 85113
    if-nez v1, :cond_1

    .line 85114
    :cond_0
    :goto_0
    return v0

    .line 85115
    :cond_1
    iget-object p0, v1, LX/0eg;->j:LX/0ej;

    move-object v1, p0

    .line 85116
    if-eqz v1, :cond_0

    .line 85117
    iget-boolean v0, v1, LX/0ej;->c:Z

    move v0, v0

    .line 85118
    goto :goto_0
.end method
