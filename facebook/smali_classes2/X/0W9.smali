.class public LX/0W9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/util/Locale;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static final b:Ljava/util/Locale;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static final c:Ljava/util/Locale;

.field private static volatile j:LX/0W9;


# instance fields
.field private final d:LX/0WC;

.field private final e:LX/0Wj;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/common/locale/Locales$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/util/Locale;",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 75546
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    sput-object v0, LX/0W9;->a:Ljava/util/Locale;

    .line 75547
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    sput-object v0, LX/0W9;->b:Ljava/util/Locale;

    .line 75548
    new-instance v0, Ljava/util/Locale;

    const-string v1, "fb"

    const-string v2, "HA"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0W9;->c:Ljava/util/Locale;

    return-void
.end method

.method public constructor <init>(LX/0WC;LX/0Or;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0WC;",
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x5

    .line 75538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75539
    new-instance v0, LX/0Wj;

    invoke-direct {v0, p0}, LX/0Wj;-><init>(LX/0W9;)V

    iput-object v0, p0, LX/0W9;->e:LX/0Wj;

    .line 75540
    iput-object p1, p0, LX/0W9;->d:LX/0WC;

    .line 75541
    iput-object p2, p0, LX/0W9;->f:LX/0Or;

    .line 75542
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, LX/0W9;->g:Ljava/util/Set;

    .line 75543
    new-instance v0, Landroid/util/LruCache;

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, LX/0W9;->h:Landroid/util/LruCache;

    .line 75544
    new-instance v0, Landroid/util/LruCache;

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, LX/0W9;->i:Landroid/util/LruCache;

    .line 75545
    return-void
.end method

.method public static a(LX/0QB;)LX/0W9;
    .locals 5

    .prologue
    .line 75525
    sget-object v0, LX/0W9;->j:LX/0W9;

    if-nez v0, :cond_1

    .line 75526
    const-class v1, LX/0W9;

    monitor-enter v1

    .line 75527
    :try_start_0
    sget-object v0, LX/0W9;->j:LX/0W9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 75528
    if-eqz v2, :cond_0

    .line 75529
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 75530
    new-instance v4, LX/0W9;

    invoke-static {v0}, LX/0WA;->a(LX/0QB;)LX/0WA;

    move-result-object v3

    check-cast v3, LX/0WC;

    const/16 p0, 0x1617

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/0W9;-><init>(LX/0WC;LX/0Or;)V

    .line 75531
    move-object v0, v4

    .line 75532
    sput-object v0, LX/0W9;->j:LX/0W9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75533
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 75534
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 75535
    :cond_1
    sget-object v0, LX/0W9;->j:LX/0W9;

    return-object v0

    .line 75536
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 75537
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(Ljava/util/Locale;)Ljava/util/Locale;
    .locals 3

    .prologue
    .line 75520
    iget-object v0, p0, LX/0W9;->h:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    .line 75521
    if-eqz v0, :cond_0

    .line 75522
    :goto_0
    return-object v0

    .line 75523
    :cond_0
    new-instance v0, Ljava/util/Locale;

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 75524
    iget-object v1, p0, LX/0W9;->h:Landroid/util/LruCache;

    invoke-virtual {v1, p1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static e()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 75519
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method private h()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 75518
    iget-object v0, p0, LX/0W9;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/Locale;
    .locals 3

    .prologue
    .line 75511
    invoke-direct {p0}, LX/0W9;->h()Ljava/util/Locale;

    move-result-object v0

    .line 75512
    iget-object v1, p0, LX/0W9;->d:LX/0WC;

    invoke-virtual {v1}, LX/0WC;->a()LX/0Rf;

    move-result-object v1

    .line 75513
    invoke-virtual {v1}, LX/0Rf;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 75514
    :cond_0
    :goto_0
    return-object v0

    .line 75515
    :cond_1
    invoke-direct {p0, v0}, LX/0W9;->b(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v2

    .line 75516
    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/0W9;->c:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 75517
    sget-object v0, LX/0W9;->a:Ljava/util/Locale;

    goto :goto_0
.end method

.method public final a(Ljava/util/Locale;)Ljava/util/Locale;
    .locals 3

    .prologue
    .line 75490
    iget-object v0, p0, LX/0W9;->d:LX/0WC;

    invoke-virtual {v0}, LX/0WC;->a()LX/0Rf;

    move-result-object v1

    .line 75491
    invoke-virtual {v1}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75492
    :goto_0
    return-object p1

    .line 75493
    :cond_0
    invoke-direct {p0, p1}, LX/0W9;->b(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v0

    .line 75494
    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object p1, v0

    .line 75495
    goto :goto_0

    .line 75496
    :cond_1
    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 75497
    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 75498
    iget-object v0, p0, LX/0W9;->i:Landroid/util/LruCache;

    invoke-virtual {v0, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    .line 75499
    if-nez v0, :cond_2

    .line 75500
    new-instance v0, Ljava/util/Locale;

    invoke-direct {v0, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 75501
    iget-object v1, p0, LX/0W9;->i:Landroid/util/LruCache;

    invoke-virtual {v1, v2, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    move-object p1, v0

    .line 75502
    goto :goto_0

    .line 75503
    :cond_3
    sget-object p1, LX/0W9;->b:Ljava/util/Locale;

    goto :goto_0
.end method

.method public final declared-synchronized a(LX/0Wo;)V
    .locals 1

    .prologue
    .line 75504
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0W9;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75505
    monitor-exit p0

    return-void

    .line 75506
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 75507
    invoke-virtual {p0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0W9;->a(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75508
    invoke-virtual {p0}, LX/0W9;->b()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, LX/0sI;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75509
    invoke-direct {p0}, LX/0W9;->h()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, LX/0sI;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75510
    iget-object v0, p0, LX/0W9;->d:LX/0WC;

    invoke-virtual {v0}, LX/0WC;->a()LX/0Rf;

    move-result-object v0

    return-object v0
.end method
