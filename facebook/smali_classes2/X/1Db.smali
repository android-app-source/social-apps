.class public LX/1Db;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Landroid/view/View;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/1Db;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 217711
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217712
    return-void
.end method

.method public static a(LX/0QB;)LX/1Db;
    .locals 3

    .prologue
    .line 217713
    sget-object v0, LX/1Db;->a:LX/1Db;

    if-nez v0, :cond_1

    .line 217714
    const-class v1, LX/1Db;

    monitor-enter v1

    .line 217715
    :try_start_0
    sget-object v0, LX/1Db;->a:LX/1Db;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 217716
    if-eqz v2, :cond_0

    .line 217717
    :try_start_1
    new-instance v0, LX/1Db;

    invoke-direct {v0}, LX/1Db;-><init>()V

    .line 217718
    move-object v0, v0

    .line 217719
    sput-object v0, LX/1Db;->a:LX/1Db;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 217720
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 217721
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 217722
    :cond_1
    sget-object v0, LX/1Db;->a:LX/1Db;

    return-object v0

    .line 217723
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 217724
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/view/View;)Ljava/lang/Void;
    .locals 1
    .param p0    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 217725
    instance-of v0, p0, LX/1a5;

    if-eqz v0, :cond_0

    check-cast p0, LX/1a5;

    invoke-virtual {p0}, LX/1a5;->getWrappedView()Landroid/view/View;

    move-result-object p0

    .line 217726
    :cond_0
    invoke-static {p0}, LX/1aL;->a(Landroid/view/View;)LX/1Ra;

    move-result-object v0

    .line 217727
    if-eqz v0, :cond_1

    .line 217728
    invoke-virtual {v0, p0}, LX/1Ra;->b(Landroid/view/View;)V

    .line 217729
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public final a()LX/1St;
    .locals 1

    .prologue
    .line 217730
    new-instance v0, LX/1Ss;

    invoke-direct {v0, p0}, LX/1Ss;-><init>(LX/1Db;)V

    return-object v0
.end method

.method public final synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 217731
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, LX/1Db;->a(Landroid/view/View;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
