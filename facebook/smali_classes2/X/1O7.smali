.class public final LX/1O7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:LX/1O5;

.field public final synthetic b:LX/1O1;


# direct methods
.method public constructor <init>(LX/1O1;LX/1O5;)V
    .locals 0

    .prologue
    .line 238750
    iput-object p1, p0, LX/1O7;->b:LX/1O1;

    iput-object p2, p0, LX/1O7;->a:LX/1O5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 238732
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 238736
    iget-object v0, p0, LX/1O7;->a:LX/1O5;

    invoke-virtual {v0}, LX/1O5;->j()V

    .line 238737
    iget-object v0, p0, LX/1O7;->a:LX/1O5;

    .line 238738
    iget v1, v0, LX/1O5;->k:I

    add-int/lit8 v1, v1, 0x1

    iget-object v3, v0, LX/1O5;->j:[I

    array-length v3, v3

    rem-int/2addr v1, v3

    iput v1, v0, LX/1O5;->k:I

    .line 238739
    iget-object v0, p0, LX/1O7;->a:LX/1O5;

    iget-object v1, p0, LX/1O7;->a:LX/1O5;

    .line 238740
    iget v3, v1, LX/1O5;->f:F

    move v1, v3

    .line 238741
    invoke-virtual {v0, v1}, LX/1O5;->b(F)V

    .line 238742
    iget-object v0, p0, LX/1O7;->b:LX/1O1;

    iget-boolean v0, v0, LX/1O1;->a:Z

    if-eqz v0, :cond_0

    .line 238743
    iget-object v0, p0, LX/1O7;->b:LX/1O1;

    iput-boolean v2, v0, LX/1O1;->a:Z

    .line 238744
    const-wide/16 v0, 0x535

    invoke-virtual {p1, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 238745
    iget-object v0, p0, LX/1O7;->a:LX/1O5;

    invoke-virtual {v0, v2}, LX/1O5;->a(Z)V

    .line 238746
    :goto_0
    return-void

    .line 238747
    :cond_0
    iget-object v0, p0, LX/1O7;->b:LX/1O1;

    iget-object v1, p0, LX/1O7;->b:LX/1O1;

    iget v1, v1, LX/1O1;->m:F

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    const/high16 v2, 0x40a00000    # 5.0f

    rem-float/2addr v1, v2

    .line 238748
    iput v1, v0, LX/1O1;->m:F

    .line 238749
    goto :goto_0
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 238733
    iget-object v0, p0, LX/1O7;->b:LX/1O1;

    const/4 v1, 0x0

    .line 238734
    iput v1, v0, LX/1O1;->m:F

    .line 238735
    return-void
.end method
