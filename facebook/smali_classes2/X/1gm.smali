.class public final LX/1gm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V
    .locals 0

    .prologue
    .line 294888
    iput-object p1, p0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 294889
    const-string v0, "FreshFeedDataLoader.ItemsCallback.onEdgesAdded"

    const v1, -0x2ff67f65

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 294890
    :try_start_0
    iget-object v0, p0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 294891
    iget-object v0, p0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    const-wide/16 v8, 0x0

    .line 294892
    iget-object v2, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->Y:LX/1EM;

    if-eqz v2, :cond_1

    .line 294893
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v6, v2

    move-wide v4, v8

    :goto_0
    if-ge v6, v7, :cond_0

    invoke-virtual {p1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 294894
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    instance-of v3, v3, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v3, :cond_4

    .line 294895
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aM()J

    move-result-wide v2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 294896
    :goto_1
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move-wide v4, v2

    goto :goto_0

    .line 294897
    :cond_0
    cmp-long v2, v4, v8

    if-lez v2, :cond_1

    .line 294898
    iget-object v2, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->Y:LX/1EM;

    invoke-virtual {v2, v4, v5}, LX/1EM;->a(J)V

    .line 294899
    :cond_1
    iget-object v0, p0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    .line 294900
    iget-object v2, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ab:Ljava/util/List;

    invoke-static {v2}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 294901
    iget-object v2, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ab:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1JH;

    .line 294902
    iget-object v4, v2, LX/1JH;->d:LX/1JR;

    invoke-virtual {v4}, LX/1JR;->a()Z

    move-result v4

    if-nez v4, :cond_5

    .line 294903
    :cond_2
    :goto_3
    goto :goto_2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294904
    :cond_3
    const v0, 0x22bfecb3

    invoke-static {v0}, LX/02m;->a(I)V

    .line 294905
    return-void

    .line 294906
    :catchall_0
    move-exception v0

    const v1, 0x1fffec29

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_4
    :try_start_1
    move-wide v2, v4

    goto :goto_1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 294907
    :cond_5
    iget-object v4, v2, LX/1JH;->d:LX/1JR;

    invoke-virtual {v4}, LX/1JR;->b()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 294908
    invoke-static {v2}, LX/1JH;->i(LX/1JH;)V

    goto :goto_3

    .line 294909
    :cond_6
    iget-object v4, v2, LX/1JH;->d:LX/1JR;

    .line 294910
    iget-object v7, v4, LX/1JR;->g:Ljava/lang/Boolean;

    if-nez v7, :cond_7

    .line 294911
    iget-object v7, v4, LX/1JR;->i:LX/0W3;

    sget-wide v9, LX/0X5;->eH:J

    invoke-interface {v7, v9, v10}, LX/0W4;->a(J)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iput-object v7, v4, LX/1JR;->g:Ljava/lang/Boolean;

    .line 294912
    :cond_7
    iget-object v7, v4, LX/1JR;->g:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    move v4, v7

    .line 294913
    if-nez v4, :cond_2

    .line 294914
    iget-object v4, v2, LX/1JH;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsSubscriber$2;

    invoke-direct {v5, v2, p1}, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsSubscriber$2;-><init>(LX/1JH;Ljava/util/List;)V

    const v6, -0x58cbec5b

    invoke-static {v4, v5, v6}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_3
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 294915
    const-string v0, "FreshFeedDataLoader.ItemsCallback.onClear"

    const v1, -0x68f83081

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 294916
    :try_start_0
    iget-object v0, p0, LX/1gm;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    const-string v2, "ItemsCallback.onClear"

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294917
    const v0, 0xb51dae7

    invoke-static {v0}, LX/02m;->a(I)V

    .line 294918
    return-void

    .line 294919
    :catchall_0
    move-exception v0

    const v1, -0x1b4edd82

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
