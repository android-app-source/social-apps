.class public LX/1ij;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1iZ;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/1il;

.field private final c:LX/1ik;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/common/perftest/PerfTestConfig;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 298945
    const-class v0, LX/1ij;

    sput-object v0, LX/1ij;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1Gl;LX/0So;LX/1ik;LX/0Ot;Lcom/facebook/common/perftest/PerfTestConfig;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Gl;",
            "LX/0So;",
            "LX/1ik;",
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 298939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298940
    new-instance v0, LX/1il;

    invoke-direct {v0, p2, p1}, LX/1il;-><init>(LX/0So;LX/1Gl;)V

    iput-object v0, p0, LX/1ij;->b:LX/1il;

    .line 298941
    iput-object p3, p0, LX/1ij;->c:LX/1ik;

    .line 298942
    iput-object p4, p0, LX/1ij;->d:LX/0Ot;

    .line 298943
    iput-object p5, p0, LX/1ij;->e:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 298944
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 298884
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298885
    :try_start_0
    sget-object v1, LX/1ij;->a:Ljava/lang/Class;

    iget-object v0, p0, LX/1ij;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lC;

    iget-object v2, p0, LX/1ij;->b:LX/1il;

    invoke-virtual {v0, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 298886
    :cond_0
    :goto_0
    return-void

    .line 298887
    :catch_0
    move-exception v0

    .line 298888
    sget-object v1, LX/1ij;->a:Ljava/lang/Class;

    const-string v2, "failed "

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V
    .locals 4
    .param p3    # Lorg/apache/http/HttpResponse;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 298929
    iget-object v0, p0, LX/1ij;->b:LX/1il;

    .line 298930
    invoke-static {v0}, LX/1il;->c(LX/1il;)V

    .line 298931
    const-string v2, "error"

    iput-object v2, v0, LX/1il;->e:Ljava/lang/String;

    .line 298932
    iget-object v2, v0, LX/1il;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, v0, LX/1il;->q:J

    .line 298933
    invoke-static {p5}, LX/1il;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/1il;->k:Ljava/lang/String;

    .line 298934
    if-eqz p3, :cond_0

    .line 298935
    invoke-interface {p3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    iput v2, v0, LX/1il;->n:I

    .line 298936
    :cond_0
    iget-object v0, p0, LX/1ij;->c:LX/1ik;

    iget-object v1, p0, LX/1ij;->b:LX/1il;

    invoke-virtual {v0, v1}, LX/1ik;->b(LX/1il;)V

    .line 298937
    invoke-direct {p0}, LX/1ij;->a()V

    .line 298938
    return-void
.end method

.method public final a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)V
    .locals 0

    .prologue
    .line 298928
    return-void
.end method

.method public final a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;LX/1iW;)V
    .locals 8

    .prologue
    .line 298906
    iget-object v0, p0, LX/1ij;->b:LX/1il;

    .line 298907
    iget-object v2, v0, LX/1il;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, v0, LX/1il;->d:J

    .line 298908
    const-string v2, "inFlight"

    iput-object v2, v0, LX/1il;->e:Ljava/lang/String;

    .line 298909
    invoke-static {v0}, LX/1il;->c(LX/1il;)V

    .line 298910
    invoke-static {p2}, LX/1iV;->a(Lorg/apache/http/protocol/HttpContext;)LX/1iV;

    move-result-object v2

    .line 298911
    iget-object v3, v2, LX/1iV;->a:Ljava/lang/String;

    move-object v2, v3

    .line 298912
    iput-object v2, v0, LX/1il;->f:Ljava/lang/String;

    .line 298913
    invoke-static {p2}, LX/1iV;->a(Lorg/apache/http/protocol/HttpContext;)LX/1iV;

    move-result-object v2

    .line 298914
    iget-object v3, v2, LX/1iV;->b:Ljava/lang/String;

    move-object v2, v3

    .line 298915
    iput-object v2, v0, LX/1il;->g:Ljava/lang/String;

    .line 298916
    invoke-static {p2}, LX/1iV;->a(Lorg/apache/http/protocol/HttpContext;)LX/1iV;

    move-result-object v2

    .line 298917
    iget-wide v6, v2, LX/1iV;->c:J

    move-wide v2, v6

    .line 298918
    iput-wide v2, v0, LX/1il;->r:J

    .line 298919
    instance-of v2, p1, Lorg/apache/http/client/methods/HttpUriRequest;

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v2

    .line 298920
    :goto_0
    new-instance v3, Lorg/apache/http/HttpHost;

    invoke-virtual {v2}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Ljava/net/URI;->getPort()I

    move-result v5

    invoke-virtual {v2}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v5, v2}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 298921
    invoke-virtual {v3}, Lorg/apache/http/HttpHost;->toURI()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/1il;->h:Ljava/lang/String;

    .line 298922
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/03l;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/1il;->j:Ljava/lang/String;

    .line 298923
    iput-object p3, v0, LX/1il;->p:LX/1iW;

    .line 298924
    iget-object v0, p0, LX/1ij;->c:LX/1ik;

    iget-object v1, p0, LX/1ij;->b:LX/1il;

    .line 298925
    iget-object v2, v0, LX/1ik;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, v1, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298926
    return-void

    .line 298927
    :cond_0
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 3

    .prologue
    .line 298895
    iget-object v0, p0, LX/1ij;->b:LX/1il;

    .line 298896
    invoke-static {v0}, LX/1il;->c(LX/1il;)V

    .line 298897
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    iput v1, v0, LX/1il;->n:I

    .line 298898
    invoke-static {p1}, LX/1Gl;->c(Lorg/apache/http/HttpResponse;)J

    move-result-wide v1

    iput-wide v1, v0, LX/1il;->o:J

    .line 298899
    const-string v1, "X-FB-Connection-Quality"

    invoke-interface {p1, v1}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 298900
    const-string v1, "X-FB-Connection-Quality"

    invoke-interface {p1, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/1il;->m:Ljava/lang/String;

    .line 298901
    :cond_0
    iget-object v1, v0, LX/1il;->p:LX/1iW;

    .line 298902
    iget-object v2, v1, LX/1iW;->b:LX/03R;

    move-object v1, v2

    .line 298903
    invoke-virtual {v1}, LX/03R;->isSet()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 298904
    invoke-virtual {v1}, LX/03R;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/1il;->l:Ljava/lang/String;

    .line 298905
    :cond_1
    return-void
.end method

.method public final b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 4

    .prologue
    .line 298889
    iget-object v0, p0, LX/1ij;->b:LX/1il;

    .line 298890
    const-string v2, "success"

    iput-object v2, v0, LX/1il;->e:Ljava/lang/String;

    .line 298891
    iget-object v2, v0, LX/1il;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, v0, LX/1il;->q:J

    .line 298892
    iget-object v0, p0, LX/1ij;->c:LX/1ik;

    iget-object v1, p0, LX/1ij;->b:LX/1il;

    invoke-virtual {v0, v1}, LX/1ik;->b(LX/1il;)V

    .line 298893
    invoke-direct {p0}, LX/1ij;->a()V

    .line 298894
    return-void
.end method
