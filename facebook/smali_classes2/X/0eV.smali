.class public final LX/0eV;
.super LX/0eW;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91985
    invoke-direct {p0}, LX/0eW;-><init>()V

    return-void
.end method

.method public static a(Ljava/nio/ByteBuffer;)LX/0eV;
    .locals 3

    .prologue
    .line 91986
    new-instance v0, LX/0eV;

    invoke-direct {v0}, LX/0eV;-><init>()V

    .line 91987
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    add-int/2addr v1, v2

    .line 91988
    iput v1, v0, LX/0eV;->a:I

    iput-object p0, v0, LX/0eV;->b:Ljava/nio/ByteBuffer;

    move-object v1, v0

    .line 91989
    move-object v0, v1

    .line 91990
    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 91991
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, LX/0eW;->d(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(I)LX/0oC;
    .locals 3

    .prologue
    .line 91992
    new-instance v0, LX/0oC;

    invoke-direct {v0}, LX/0oC;-><init>()V

    .line 91993
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, LX/0eW;->e(I)I

    move-result v1

    mul-int/lit8 v2, p1, 0x4

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, LX/0eW;->b(I)I

    move-result v1

    iget-object v2, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 91994
    iput v1, v0, LX/0oC;->a:I

    iput-object v2, v0, LX/0oC;->b:Ljava/nio/ByteBuffer;

    move-object v1, v0

    .line 91995
    :goto_0
    move-object v0, v1

    .line 91996
    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
