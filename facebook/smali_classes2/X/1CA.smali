.class public LX/1CA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final A:LX/0Tn;

.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;

.field public static final o:LX/0Tn;

.field public static final p:LX/0Tn;

.field public static final q:LX/0Tn;

.field public static final r:LX/0Tn;

.field public static final s:LX/0Tn;

.field public static final t:LX/0Tn;

.field public static final u:LX/0Tn;

.field public static final v:LX/0Tn;

.field public static final w:LX/0Tn;

.field public static final x:LX/0Tn;

.field public static final y:LX/0Tn;

.field public static final z:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 215525
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "fb_android/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 215526
    sput-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "kvm/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->b:LX/0Tn;

    .line 215527
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "uvm/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->c:LX/0Tn;

    .line 215528
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "events/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->d:LX/0Tn;

    .line 215529
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "client_time_offset_via_login_approvals/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->e:LX/0Tn;

    .line 215530
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "login_approvals_secret/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->f:LX/0Tn;

    .line 215531
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "video_spec_display"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->g:LX/0Tn;

    .line 215532
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "photo_360_spec_display"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->h:LX/0Tn;

    .line 215533
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "video_inline_unmute"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->i:LX/0Tn;

    .line 215534
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "video_logging_level"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->j:LX/0Tn;

    .line 215535
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "video_home_force_prefetch"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->k:LX/0Tn;

    .line 215536
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "video_home_data_fetch_toast"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->l:LX/0Tn;

    .line 215537
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "video_home_debug_overlay"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->m:LX/0Tn;

    .line 215538
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "in_progress_login_timestamp"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->n:LX/0Tn;

    .line 215539
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "last_login_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->o:LX/0Tn;

    .line 215540
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "last_username"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->p:LX/0Tn;

    .line 215541
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "hashed_uid"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->q:LX/0Tn;

    .line 215542
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "last_logout_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->r:LX/0Tn;

    .line 215543
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "intern_settings_history"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->s:LX/0Tn;

    .line 215544
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "jewel_footer_promo_times_shown_since_last_reset"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->t:LX/0Tn;

    .line 215545
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "jewel_footer_promo_times_shown_total"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->u:LX/0Tn;

    .line 215546
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "jewel_footer_promo_last_shown_secs"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->v:LX/0Tn;

    .line 215547
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "in_app_browser_profiling"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->w:LX/0Tn;

    .line 215548
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "in_app_browser_debug_overlay"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->x:LX/0Tn;

    .line 215549
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "login_broadcasted_cross_app"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->y:LX/0Tn;

    .line 215550
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "facecast_debug_overlay"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->z:LX/0Tn;

    .line 215551
    sget-object v0, LX/1CA;->a:LX/0Tn;

    const-string v1, "first_boot_page_seen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1CA;->A:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 215552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 215553
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 215554
    sget-object v0, LX/0pP;->q:LX/0Tn;

    sget-object v1, LX/1CA;->y:LX/0Tn;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
