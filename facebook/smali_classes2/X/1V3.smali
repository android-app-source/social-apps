.class public LX/1V3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zr;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/components/ComponentsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1DR;

.field public final d:LX/1V4;

.field public final e:Ljava/util/Random;

.field public final f:LX/0ad;

.field public final g:Z

.field public final h:Z

.field private i:Ljava/lang/Boolean;

.field public j:Ljava/lang/Boolean;

.field private k:Landroid/os/Looper;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/1DR;LX/1V4;Ljava/util/Random;LX/0ad;Lcom/facebook/common/perftest/PerfTestConfig;)V
    .locals 2
    .param p5    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0Zr;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/components/ComponentsLogger;",
            ">;",
            "LX/1DR;",
            "LX/1V4;",
            "Ljava/util/Random;",
            "LX/0ad;",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 257130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 257131
    iput-object p1, p0, LX/1V3;->a:LX/0Ot;

    .line 257132
    iput-object p2, p0, LX/1V3;->b:LX/0Ot;

    .line 257133
    iput-object p3, p0, LX/1V3;->c:LX/1DR;

    .line 257134
    iput-object p4, p0, LX/1V3;->d:LX/1V4;

    .line 257135
    iput-object p6, p0, LX/1V3;->f:LX/0ad;

    .line 257136
    iput-object p5, p0, LX/1V3;->e:Ljava/util/Random;

    .line 257137
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    iput-boolean v0, p0, LX/1V3;->g:Z

    .line 257138
    sget-boolean v0, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->p:Z

    move v0, v0

    .line 257139
    iput-boolean v0, p0, LX/1V3;->h:Z

    .line 257140
    sget-short v0, LX/1Dd;->u:S

    const/4 v1, 0x0

    invoke-interface {p6, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    sput-boolean v0, LX/1V5;->j:Z

    .line 257141
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "LX/0jW;",
            ">;)",
            "LX/0jW;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 257142
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->b()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 257143
    instance-of v1, v0, LX/0jW;

    if-eqz v1, :cond_1

    .line 257144
    check-cast v0, LX/0jW;

    .line 257145
    :goto_0
    invoke-interface {v0}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 257146
    const/4 v0, 0x0

    .line 257147
    :cond_0
    return-object v0

    .line 257148
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257149
    check-cast v0, LX/0jW;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1V3;
    .locals 11

    .prologue
    .line 257150
    const-class v1, LX/1V3;

    monitor-enter v1

    .line 257151
    :try_start_0
    sget-object v0, LX/1V3;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 257152
    sput-object v2, LX/1V3;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 257153
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257154
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 257155
    new-instance v3, LX/1V3;

    const/16 v4, 0x274

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x3a5

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v6

    check-cast v6, LX/1DR;

    invoke-static {v0}, LX/1V4;->a(LX/0QB;)LX/1V4;

    move-result-object v7

    check-cast v7, LX/1V4;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v8

    check-cast v8, Ljava/util/Random;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {v0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v10

    check-cast v10, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-direct/range {v3 .. v10}, LX/1V3;-><init>(LX/0Ot;LX/0Ot;LX/1DR;LX/1V4;Ljava/util/Random;LX/0ad;Lcom/facebook/common/perftest/PerfTestConfig;)V

    .line 257156
    move-object v0, v3

    .line 257157
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 257158
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1V3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 257159
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 257160
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/Object;LX/0jW;LX/1Rj;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "LX/0jW;",
            "LX/1Rj;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 257161
    instance-of v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 257162
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 257163
    instance-of v0, v0, LX/0jW;

    if-eqz v0, :cond_1

    .line 257164
    check-cast p0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 257165
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->b()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 257166
    instance-of v1, v0, LX/0jW;

    if-eqz v1, :cond_0

    check-cast v0, LX/0jW;

    invoke-interface {v0}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v0

    .line 257167
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 257168
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257169
    check-cast v0, LX/0jW;

    invoke-interface {v0}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 257170
    :goto_1
    return-object v0

    .line 257171
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 257172
    :cond_1
    invoke-interface {p1}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(LX/1X1;ZZLX/1cp;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 257173
    if-eqz p3, :cond_0

    .line 257174
    const-string v0, "log_tag"

    invoke-virtual {p3, v2, p0, v0, p4}, LX/1cp;->a(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 257175
    const-string v0, "is_async_prepare"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v2, p0, v0, v1}, LX/1cp;->b(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 257176
    const-string v0, "is_async_layout"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v2, p0, v0, v1}, LX/1cp;->b(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 257177
    :cond_0
    return-void
.end method

.method private static a(LX/1Rj;LX/1Pn;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            "E::",
            "LX/1Pn;",
            ":",
            "LX/1Pv;",
            ":",
            "LX/1Pr;",
            ">(",
            "LX/1Rj",
            "<TP;TE;>;TE;)Z"
        }
    .end annotation

    .prologue
    .line 257178
    instance-of v0, p1, LX/1Po;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, LX/1Po;

    invoke-interface {v0}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast p1, LX/1Po;

    invoke-interface {p1}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    sget-object v1, LX/1Qt;->FEED:LX/1Qt;

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, LX/1Rj;->iV_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/1V3;)Z
    .locals 3

    .prologue
    .line 257179
    iget-object v0, p0, LX/1V3;->i:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 257180
    iget-object v0, p0, LX/1V3;->f:LX/0ad;

    sget-short v1, LX/1Dd;->v:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1V3;->i:Ljava/lang/Boolean;

    .line 257181
    :cond_0
    iget-object v0, p0, LX/1V3;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private declared-synchronized c()Landroid/os/Looper;
    .locals 3

    .prologue
    .line 257182
    monitor-enter p0

    .line 257183
    :try_start_0
    iget-object v0, p0, LX/1V3;->j:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 257184
    iget-object v0, p0, LX/1V3;->f:LX/0ad;

    sget-short v1, LX/1Dd;->m:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1V3;->j:Ljava/lang/Boolean;

    .line 257185
    :cond_0
    iget-object v0, p0, LX/1V3;->j:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 257186
    if-nez v0, :cond_1

    .line 257187
    const/4 v0, 0x0

    .line 257188
    :goto_0
    monitor-exit p0

    return-object v0

    .line 257189
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/1V3;->k:Landroid/os/Looper;

    if-eqz v0, :cond_2

    .line 257190
    iget-object v0, p0, LX/1V3;->k:Landroid/os/Looper;

    goto :goto_0

    .line 257191
    :cond_2
    iget-object v0, p0, LX/1V3;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zr;

    const-string v1, "Feed_ComponentLayoutThread"

    sget-object v2, LX/0TP;->NORMAL:LX/0TP;

    invoke-virtual {v0, v1, v2}, LX/0Zr;->a(Ljava/lang/String;LX/0TP;)Landroid/os/HandlerThread;

    move-result-object v0

    .line 257192
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 257193
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, LX/1V3;->k:Landroid/os/Looper;

    .line 257194
    iget-object v0, p0, LX/1V3;->k:Landroid/os/Looper;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 257195
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1Pn;Ljava/lang/String;LX/1Rj;Z)LX/1dV;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;TP;TE;",
            "Ljava/lang/String;",
            "LX/1Rj",
            "<TP;TE;>;Z)",
            "LX/1dV;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, 0x0

    .line 257196
    const/4 v0, 0x1

    .line 257197
    invoke-interface {p5}, LX/1Rj;->c()Z

    move-result v1

    if-nez v1, :cond_7

    .line 257198
    :cond_0
    :goto_0
    move v0, v0

    .line 257199
    if-eqz v0, :cond_5

    iget-object v0, p0, LX/1V3;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cp;

    move-object v1, v0

    .line 257200
    :goto_1
    new-instance v3, LX/1De;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0, p4, v1}, LX/1De;-><init>(Landroid/content/Context;Ljava/lang/String;LX/1cp;)V

    .line 257201
    invoke-interface {p5, v3, p2, p3}, LX/1Rj;->a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;

    move-result-object v4

    .line 257202
    invoke-static {p0}, LX/1V3;->b(LX/1V3;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 257203
    invoke-interface {p5, p2}, LX/1Rj;->b(Ljava/lang/Object;)LX/0jW;

    move-result-object v5

    .line 257204
    if-eqz v5, :cond_1

    move-object v0, p3

    .line 257205
    check-cast v0, LX/1Pr;

    new-instance v2, LX/1z8;

    invoke-static {p2, v5, p5}, LX/1V3;->a(Ljava/lang/Object;LX/0jW;LX/1Rj;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, LX/1z8;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v2, v5}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1mg;

    move-object v2, v0

    .line 257206
    :cond_1
    invoke-static {v3, v4}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object v0

    .line 257207
    iput-object v2, v0, LX/1me;->g:LX/1mg;

    .line 257208
    move-object v0, v0

    .line 257209
    invoke-static {p5, p3}, LX/1V3;->a(LX/1Rj;LX/1Pn;)Z

    move-result v2

    .line 257210
    iput-boolean v2, v0, LX/1me;->c:Z

    .line 257211
    move-object v0, v0

    .line 257212
    iput-boolean v7, v0, LX/1me;->h:Z

    .line 257213
    move-object v0, v0

    .line 257214
    invoke-direct {p0}, LX/1V3;->c()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1me;->a(Landroid/os/Looper;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v2

    move-object v0, p3

    .line 257215
    check-cast v0, LX/1Pv;

    invoke-interface {v0}, LX/1Pv;->iO_()Z

    move-result v5

    .line 257216
    invoke-interface {p5, p3}, LX/1Rj;->a(LX/1PW;)Z

    move-result v6

    .line 257217
    invoke-static {v4, v5, v6, v1, p4}, LX/1V3;->a(LX/1X1;ZZLX/1cp;Ljava/lang/String;)V

    .line 257218
    invoke-interface {p5, v3}, LX/1Rj;->a(Landroid/content/Context;)I

    move-result v0

    .line 257219
    if-gez v0, :cond_2

    .line 257220
    iget-object v0, p0, LX/1V3;->c:LX/1DR;

    invoke-virtual {v0}, LX/1DR;->a()I

    move-result v0

    .line 257221
    :cond_2
    if-lez v0, :cond_3

    .line 257222
    if-eqz v5, :cond_6

    if-eqz v6, :cond_6

    .line 257223
    invoke-static {v0, v8}, LX/1mh;->a(II)I

    move-result v0

    invoke-static {v7, v7}, LX/1mh;->a(II)I

    move-result v3

    invoke-virtual {v2, v4, v0, v3}, LX/1dV;->a(LX/1X1;II)V

    .line 257224
    :cond_3
    :goto_2
    invoke-interface {p5, p1, p2}, LX/1Rj;->a(LX/1aD;Ljava/lang/Object;)V

    .line 257225
    if-eqz v1, :cond_4

    .line 257226
    const/4 v0, 0x4

    const/16 v3, 0x10

    invoke-virtual {v1, v0, v4, v3}, LX/1cp;->a(ILjava/lang/Object;I)V

    .line 257227
    :cond_4
    return-object v2

    :cond_5
    move-object v1, v2

    .line 257228
    goto/16 :goto_1

    .line 257229
    :cond_6
    invoke-static {v0, v8}, LX/1mh;->a(II)I

    move-result v0

    invoke-static {v7, v7}, LX/1mh;->a(II)I

    move-result v3

    invoke-virtual {v2, v4, v0, v3}, LX/1dV;->b(LX/1X1;II)V

    .line 257230
    if-eqz p6, :cond_3

    check-cast p3, LX/1Pv;

    invoke-interface {p3}, LX/1Pv;->iO_()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 257231
    invoke-virtual {v2}, LX/1dV;->h()V

    goto :goto_2

    .line 257232
    :cond_7
    iget-boolean v1, p0, LX/1V3;->g:Z

    if-eqz v1, :cond_8

    .line 257233
    iget-boolean v0, p0, LX/1V3;->h:Z

    goto/16 :goto_0

    .line 257234
    :cond_8
    iget-object v1, p0, LX/1V3;->e:Ljava/util/Random;

    const/16 v3, 0x1f4

    invoke-virtual {v1, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto/16 :goto_0
.end method
