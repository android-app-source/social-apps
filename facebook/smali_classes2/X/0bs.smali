.class public final LX/0bs;
.super LX/0bi;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0bi",
        "<",
        "LX/30l;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0bs;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/30l;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 87366
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x1d6

    aput v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x631

    aput v2, v0, v1

    invoke-direct {p0, p1, v0}, LX/0bi;-><init>(LX/0Ot;[I)V

    .line 87367
    return-void
.end method

.method public static a(LX/0QB;)LX/0bs;
    .locals 4

    .prologue
    .line 87368
    sget-object v0, LX/0bs;->b:LX/0bs;

    if-nez v0, :cond_1

    .line 87369
    const-class v1, LX/0bs;

    monitor-enter v1

    .line 87370
    :try_start_0
    sget-object v0, LX/0bs;->b:LX/0bs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 87371
    if-eqz v2, :cond_0

    .line 87372
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 87373
    new-instance v3, LX/0bs;

    const/16 p0, 0xd9b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/0bs;-><init>(LX/0Ot;)V

    .line 87374
    move-object v0, v3

    .line 87375
    sput-object v0, LX/0bs;->b:LX/0bs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87376
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 87377
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 87378
    :cond_1
    sget-object v0, LX/0bs;->b:LX/0bs;

    return-object v0

    .line 87379
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 87380
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Uh;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 87381
    check-cast p3, LX/30l;

    .line 87382
    iget-object v0, p3, LX/30l;->f:LX/2Or;

    invoke-virtual {v0}, LX/2Or;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87383
    iget-object v0, p3, LX/30l;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object p0, LX/3Kr;->c:LX/0Tn;

    invoke-interface {v0, p0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 87384
    :cond_0
    iget-object v0, p3, LX/30l;->f:LX/2Or;

    .line 87385
    iget-object p0, v0, LX/2Or;->a:LX/0Uh;

    const/16 p1, 0x1d6

    const/4 p2, 0x0

    invoke-virtual {p0, p1, p2}, LX/0Uh;->a(IZ)Z

    move-result p0

    move v0, p0

    .line 87386
    if-eqz v0, :cond_2

    .line 87387
    iget-object v0, p3, LX/30l;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 87388
    iget-object v0, p3, LX/30l;->a:Landroid/content/Context;

    invoke-static {v0}, LX/2uq;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87389
    iget-object v0, p3, LX/30l;->d:LX/2uq;

    sget-object p0, LX/FMK;->KILL_SWITCH:LX/FMK;

    iget-object p1, p3, LX/30l;->a:Landroid/content/Context;

    const/4 p2, 0x0

    invoke-virtual {v0, p0, p1, p2}, LX/2uq;->a(Ljava/lang/Object;Landroid/content/Context;Z)Z

    .line 87390
    :cond_1
    iget-object v0, p3, LX/30l;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object p0, LX/3Kr;->a:LX/0Tn;

    invoke-interface {v0, p0}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 87391
    :cond_2
    return-void
.end method
