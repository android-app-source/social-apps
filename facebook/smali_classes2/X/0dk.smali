.class public LX/0dk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0dk;


# instance fields
.field private final a:LX/0dl;

.field private b:Lcom/facebook/java2js/JSContext;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0dl;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 90763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90764
    iput-object p1, p0, LX/0dk;->a:LX/0dl;

    .line 90765
    return-void
.end method

.method public static a(LX/0QB;)LX/0dk;
    .locals 8

    .prologue
    .line 90766
    sget-object v0, LX/0dk;->c:LX/0dk;

    if-nez v0, :cond_1

    .line 90767
    const-class v1, LX/0dk;

    monitor-enter v1

    .line 90768
    :try_start_0
    sget-object v0, LX/0dk;->c:LX/0dk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 90769
    if-eqz v2, :cond_0

    .line 90770
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 90771
    new-instance v4, LX/0dk;

    .line 90772
    new-instance p0, LX/0dl;

    .line 90773
    new-instance v6, LX/0dm;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-direct {v6, v3, v5}, LX/0dm;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0So;)V

    .line 90774
    move-object v3, v6

    .line 90775
    check-cast v3, LX/0dm;

    invoke-static {v0}, LX/0dn;->a(LX/0QB;)LX/0dn;

    move-result-object v5

    check-cast v5, LX/0dn;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-direct {p0, v3, v5, v6, v7}, LX/0dl;-><init>(LX/0dm;LX/0dn;Landroid/content/Context;LX/03V;)V

    .line 90776
    move-object v3, p0

    .line 90777
    check-cast v3, LX/0dl;

    invoke-direct {v4, v3}, LX/0dk;-><init>(LX/0dl;)V

    .line 90778
    move-object v0, v4

    .line 90779
    sput-object v0, LX/0dk;->c:LX/0dk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90780
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 90781
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 90782
    :cond_1
    sget-object v0, LX/0dk;->c:LX/0dk;

    return-object v0

    .line 90783
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 90784
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/facebook/java2js/JSContext;
    .locals 11

    .prologue
    .line 90785
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0dk;->b:Lcom/facebook/java2js/JSContext;

    if-nez v0, :cond_0

    .line 90786
    iget-object v0, p0, LX/0dk;->a:LX/0dl;

    const/4 v8, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 90787
    iget-object v1, v0, LX/0dl;->b:LX/0dm;

    invoke-virtual {v1, v3}, LX/0dm;->a(I)V

    .line 90788
    iget-object v1, v0, LX/0dl;->b:LX/0dm;

    invoke-virtual {v1, v2}, LX/0dm;->a(I)V

    .line 90789
    new-instance v4, Lcom/facebook/java2js/JSContext;

    const-string v1, "ComponentScriptBundle"

    new-instance v5, LX/5L0;

    invoke-direct {v5, v0}, LX/5L0;-><init>(LX/0dl;)V

    invoke-direct {v4, v1, v5}, Lcom/facebook/java2js/JSContext;-><init>(Ljava/lang/String;LX/5L0;)V

    .line 90790
    iget-object v1, v0, LX/0dl;->b:LX/0dm;

    invoke-virtual {v1, v2}, LX/0dm;->b(I)V

    .line 90791
    iget-object v1, v0, LX/0dl;->b:LX/0dm;

    invoke-virtual {v1, v8}, LX/0dm;->a(I)V

    .line 90792
    const/16 v1, 0x1

    move v1, v1

    .line 90793
    if-eqz v1, :cond_1

    iget-object v1, v0, LX/0dl;->a:LX/0dn;

    .line 90794
    sget-wide v9, LX/0X5;->jg:J

    invoke-static {v1, v9, v10}, LX/0dn;->a(LX/0dn;J)Z

    move-result v9

    if-eqz v9, :cond_3

    sget-wide v9, LX/0X5;->jh:J

    invoke-static {v1, v9, v10}, LX/0dn;->a(LX/0dn;J)Z

    move-result v9

    if-eqz v9, :cond_3

    const/4 v9, 0x1

    :goto_0
    move v1, v9

    .line 90795
    if-eqz v1, :cond_1

    move v1, v2

    .line 90796
    :goto_1
    iget-object v5, v0, LX/0dl;->b:LX/0dm;

    const-string v6, "used_bytecode"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v7

    .line 90797
    iget-object v9, v5, LX/0dm;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v3}, LX/0dm;->d(I)I

    move-result v10

    invoke-interface {v9, v10, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 90798
    if-eqz v1, :cond_2

    .line 90799
    invoke-static {v0}, LX/0dl;->b(LX/0dl;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/facebook/java2js/JSContext;->evaluateSourceCode(Ljava/io/File;)V

    .line 90800
    :goto_2
    iget-object v1, v0, LX/0dl;->b:LX/0dm;

    invoke-virtual {v1, v8}, LX/0dm;->b(I)V

    .line 90801
    const-string v1, "InternationalizationConfig"

    const-string v5, "registerOverride"

    new-array v2, v2, [Ljava/lang/Object;

    new-instance v6, LX/5L1;

    invoke-direct {v6, v0, v4}, LX/5L1;-><init>(LX/0dl;Lcom/facebook/java2js/JSContext;)V

    invoke-static {v4, v6}, Lcom/facebook/java2js/JSValue;->makeFunction(Lcom/facebook/java2js/JSContext;Lcom/facebook/java2js/Invokable;)Lcom/facebook/java2js/JSValue;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-virtual {v4, v1, v5, v2}, Lcom/facebook/java2js/JSContext;->callModuleMethod(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    .line 90802
    iget-object v1, v0, LX/0dl;->b:LX/0dm;

    invoke-virtual {v1, v3}, LX/0dm;->b(I)V

    .line 90803
    move-object v0, v4

    .line 90804
    iput-object v0, p0, LX/0dk;->b:Lcom/facebook/java2js/JSContext;

    .line 90805
    :cond_0
    iget-object v0, p0, LX/0dk;->b:Lcom/facebook/java2js/JSContext;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 90806
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move v1, v3

    .line 90807
    goto :goto_1

    .line 90808
    :cond_2
    iget-object v1, v0, LX/0dl;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string v5, "ComponentScriptBundle.js"

    invoke-virtual {v4, v1, v5}, Lcom/facebook/java2js/JSContext;->evaluateSourceCode(Landroid/content/res/AssetManager;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    const/4 v9, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90809
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0dk;->b:Lcom/facebook/java2js/JSContext;

    if-nez v0, :cond_0

    const-string v0, "cold_start"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    const-string v0, "warm_start"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Lcom/facebook/java2js/JscMemoryMetrics;
    .locals 1

    .prologue
    .line 90810
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0dk;->b:Lcom/facebook/java2js/JSContext;

    if-eqz v0, :cond_0

    .line 90811
    invoke-static {}, Lcom/facebook/java2js/JSContext;->getMemoryMetrics()Lcom/facebook/java2js/JscMemoryMetrics;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 90812
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 90813
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
