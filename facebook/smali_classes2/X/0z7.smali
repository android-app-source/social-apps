.class public final LX/0z7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/location/Location;

.field public b:J

.field public c:J

.field public d:F


# direct methods
.method public constructor <init>(DD)V
    .locals 3

    .prologue
    .line 166683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166684
    new-instance v0, Landroid/location/Location;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0z7;->a:Landroid/location/Location;

    .line 166685
    iget-object v0, p0, LX/0z7;->a:Landroid/location/Location;

    invoke-virtual {v0, p1, p2}, Landroid/location/Location;->setLatitude(D)V

    .line 166686
    iget-object v0, p0, LX/0z7;->a:Landroid/location/Location;

    invoke-virtual {v0, p3, p4}, Landroid/location/Location;->setLongitude(D)V

    .line 166687
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/location/ImmutableLocation;
    .locals 8

    .prologue
    .line 166688
    new-instance v0, Lcom/facebook/location/ImmutableLocation;

    iget-object v1, p0, LX/0z7;->a:Landroid/location/Location;

    iget-wide v2, p0, LX/0z7;->b:J

    iget-wide v4, p0, LX/0z7;->c:J

    iget v6, p0, LX/0z7;->d:F

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/facebook/location/ImmutableLocation;-><init>(Landroid/location/Location;JJFB)V

    return-object v0
.end method

.method public final b(F)LX/0z7;
    .locals 1

    .prologue
    .line 166689
    iget-object v0, p0, LX/0z7;->a:Landroid/location/Location;

    invoke-virtual {v0, p1}, Landroid/location/Location;->setAccuracy(F)V

    .line 166690
    return-object p0
.end method

.method public final c(J)LX/0z7;
    .locals 3

    .prologue
    .line 166691
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 166692
    iget-object v0, p0, LX/0z7;->a:Landroid/location/Location;

    invoke-virtual {v0, p1, p2}, Landroid/location/Location;->setTime(J)V

    .line 166693
    return-object p0

    .line 166694
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
