.class public LX/1Cx;
.super LX/0qg;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static final c:Ljava/lang/Object;


# instance fields
.field public b:LX/0iA;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 217209
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NEWSFEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/1Cx;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 217210
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/1Cx;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0iA;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 217211
    invoke-direct {p0}, LX/0qg;-><init>()V

    .line 217212
    iput-object p1, p0, LX/1Cx;->b:LX/0iA;

    .line 217213
    return-void
.end method

.method public static a(LX/0QB;)LX/1Cx;
    .locals 7

    .prologue
    .line 217214
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 217215
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 217216
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 217217
    if-nez v1, :cond_0

    .line 217218
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 217219
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 217220
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 217221
    sget-object v1, LX/1Cx;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 217222
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 217223
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 217224
    :cond_1
    if-nez v1, :cond_4

    .line 217225
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 217226
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 217227
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 217228
    new-instance p0, LX/1Cx;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v1

    check-cast v1, LX/0iA;

    invoke-direct {p0, v1}, LX/1Cx;-><init>(LX/0iA;)V

    .line 217229
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 217230
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 217231
    if-nez v1, :cond_2

    .line 217232
    sget-object v0, LX/1Cx;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cx;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 217233
    :goto_1
    if-eqz v0, :cond_3

    .line 217234
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 217235
    :goto_3
    check-cast v0, LX/1Cx;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 217236
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 217237
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 217238
    :catchall_1
    move-exception v0

    .line 217239
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 217240
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 217241
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 217242
    :cond_2
    :try_start_8
    sget-object v0, LX/1Cx;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cx;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static c(LX/0rH;)Lcom/facebook/interstitial/manager/InterstitialTrigger;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 217243
    sget-object v0, LX/1XY;->a:[I

    invoke-virtual {p0}, LX/0rH;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 217244
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 217245
    :pswitch_0
    sget-object v0, LX/1Cx;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/0rH;)LX/0qi;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 217246
    invoke-static {p1}, LX/1Cx;->c(LX/0rH;)Lcom/facebook/interstitial/manager/InterstitialTrigger;

    move-result-object v1

    .line 217247
    if-nez v1, :cond_0

    .line 217248
    invoke-static {v4}, LX/0qi;->a(Z)LX/0qi;

    move-result-object v0

    .line 217249
    :goto_0
    return-object v0

    .line 217250
    :cond_0
    iget-object v0, p0, LX/1Cx;->b:LX/0iA;

    sget-object v2, LX/1XZ;->a:Ljava/lang/String;

    const-class v3, LX/13D;

    invoke-virtual {v0, v2, v3, v1}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/13D;

    .line 217251
    if-nez v0, :cond_1

    .line 217252
    invoke-static {v4}, LX/0qi;->a(Z)LX/0qi;

    move-result-object v0

    goto :goto_0

    .line 217253
    :cond_1
    iget-object v2, v0, LX/13D;->a:LX/13K;

    .line 217254
    invoke-static {v2, v1}, LX/13K;->d(LX/13K;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    move-result-object v0

    iput-object v0, v2, LX/13K;->q:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 217255
    iget-object v0, v2, LX/13K;->q:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    if-eqz v0, :cond_3

    .line 217256
    iput-object v1, v2, LX/13K;->r:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 217257
    iget-object v0, v2, LX/13K;->q:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    .line 217258
    :goto_1
    move-object v2, v0

    .line 217259
    move-object v0, v2

    .line 217260
    if-nez v0, :cond_2

    .line 217261
    invoke-static {v4}, LX/0qi;->a(Z)LX/0qi;

    move-result-object v0

    goto :goto_0

    .line 217262
    :cond_2
    invoke-static {v0}, LX/0qi;->a(Ljava/lang/String;)LX/0qi;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217263
    const-string v0, "quick_promotion_megaphone"

    return-object v0
.end method

.method public final b()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0rH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217264
    const/4 v0, 0x1

    new-array v0, v0, [LX/0rH;

    const/4 v1, 0x0

    sget-object v2, LX/0rH;->NEWS_FEED:LX/0rH;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    return-object v0
.end method
