.class public LX/190;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 207131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/view/View;I)Landroid/view/TouchDelegate;
    .locals 1

    .prologue
    .line 207166
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-static {p0, v0, p1}, LX/190;->a(Landroid/view/View;Landroid/view/ViewParent;I)Landroid/view/TouchDelegate;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/View;Landroid/view/ViewParent;I)Landroid/view/TouchDelegate;
    .locals 6

    .prologue
    .line 207165
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p2

    move v4, p2

    move v5, p2

    invoke-static/range {v0 .. v5}, LX/190;->a(Landroid/view/View;Landroid/view/ViewParent;IIII)Landroid/view/TouchDelegate;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/View;Landroid/view/ViewParent;IIII)Landroid/view/TouchDelegate;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, -0x1

    .line 207132
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 207133
    invoke-virtual {p0, v5}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 207134
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 207135
    :goto_0
    if-eq v0, p1, :cond_0

    if-eqz v0, :cond_0

    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_0

    .line 207136
    check-cast v0, Landroid/view/View;

    .line 207137
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v5, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    .line 207138
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    .line 207139
    :cond_0
    instance-of v6, p1, Landroid/view/View;

    .line 207140
    if-ne p2, v7, :cond_2

    .line 207141
    if-eqz v6, :cond_1

    .line 207142
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v0

    move v4, v0

    .line 207143
    :goto_1
    if-ne p3, v7, :cond_4

    .line 207144
    if-eqz v6, :cond_3

    .line 207145
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v0

    move v3, v0

    .line 207146
    :goto_2
    if-ne p4, v7, :cond_6

    .line 207147
    if-eqz v6, :cond_5

    move-object v0, p1

    .line 207148
    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v2

    sub-int/2addr v0, v2

    move v2, v0

    .line 207149
    :goto_3
    if-ne p5, v7, :cond_8

    .line 207150
    if-eqz v6, :cond_7

    .line 207151
    check-cast p1, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v1

    sub-int/2addr v0, v1

    .line 207152
    :goto_4
    iget v1, v5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v4

    iput v1, v5, Landroid/graphics/Rect;->left:I

    .line 207153
    iget v1, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v3

    iput v1, v5, Landroid/graphics/Rect;->top:I

    .line 207154
    iget v1, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    iput v1, v5, Landroid/graphics/Rect;->right:I

    .line 207155
    iget v1, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    iput v0, v5, Landroid/graphics/Rect;->bottom:I

    .line 207156
    new-instance v0, Landroid/view/TouchDelegate;

    invoke-direct {v0, v5, p0}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    return-object v0

    :cond_1
    move v4, v1

    .line 207157
    goto :goto_1

    .line 207158
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    int-to-float v2, p2

    invoke-static {v0, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    move v4, v0

    goto :goto_1

    :cond_3
    move v3, v1

    .line 207159
    goto :goto_2

    .line 207160
    :cond_4
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    int-to-float v2, p3

    invoke-static {v0, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    move v3, v0

    goto :goto_2

    :cond_5
    move v2, v1

    .line 207161
    goto :goto_3

    .line 207162
    :cond_6
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    int-to-float v2, p4

    invoke-static {v0, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    move v2, v0

    goto :goto_3

    :cond_7
    move v0, v1

    .line 207163
    goto :goto_4

    .line 207164
    :cond_8
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    int-to-float v1, p5

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    goto :goto_4
.end method
