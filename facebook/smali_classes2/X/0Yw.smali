.class public LX/0Yw;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:I

.field public d:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/0rk;",
            ">;"
        }
    .end annotation
.end field

.field public e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82812
    const-class v0, LX/0Yw;

    sput-object v0, LX/0Yw;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 4

    .prologue
    const/16 v3, 0x1e

    .line 82761
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82762
    if-lez p1, :cond_0

    if-ge p1, v3, :cond_0

    .line 82763
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "trace size limit must be at least "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " chars long"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82764
    :cond_0
    iput p1, p0, LX/0Yw;->a:I

    .line 82765
    iput p2, p0, LX/0Yw;->b:I

    .line 82766
    invoke-virtual {p0}, LX/0Yw;->c()V

    .line 82767
    return-void
.end method

.method private d()LX/0rk;
    .locals 3

    .prologue
    .line 82809
    iget-object v0, p0, LX/0Yw;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rk;

    .line 82810
    iget v1, p0, LX/0Yw;->e:I

    iget-object v2, v0, LX/0rk;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, LX/0Yw;->e:I

    .line 82811
    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 82804
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, LX/0Yw;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 82805
    iget-object v0, p0, LX/0Yw;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rk;

    .line 82806
    new-instance v3, Landroid/util/Pair;

    iget-wide v4, v0, LX/0rk;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v0, v0, LX/0rk;->a:Ljava/lang/String;

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 82807
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 82808
    :cond_0
    monitor-exit p0

    return-object v1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 82791
    monitor-enter p0

    .line 82792
    :try_start_0
    iget v0, p0, LX/0Yw;->b:I

    if-lez v0, :cond_0

    .line 82793
    :goto_0
    iget-object v0, p0, LX/0Yw;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, LX/0Yw;->b:I

    if-le v0, v1, :cond_0

    .line 82794
    invoke-direct {p0}, LX/0Yw;->d()LX/0rk;

    goto :goto_0

    .line 82795
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 82796
    :cond_0
    :try_start_1
    iget v0, p0, LX/0Yw;->a:I

    if-lez v0, :cond_2

    .line 82797
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, LX/0Yw;->a:I

    if-le v0, v1, :cond_1

    .line 82798
    const-string p1, "overly large log entry skipped"

    .line 82799
    :cond_1
    :goto_1
    iget v0, p0, LX/0Yw;->e:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, LX/0Yw;->a:I

    if-le v0, v1, :cond_2

    .line 82800
    invoke-direct {p0}, LX/0Yw;->d()LX/0rk;

    goto :goto_1

    .line 82801
    :cond_2
    iget-object v2, p0, LX/0Yw;->d:Ljava/util/Queue;

    new-instance v3, LX/0rk;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    invoke-direct {v3, p1, v4, v5}, LX/0rk;-><init>(Ljava/lang/String;J)V

    invoke-interface {v2, v3}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 82802
    iget v2, p0, LX/0Yw;->e:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, LX/0Yw;->e:I

    .line 82803
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 82789
    invoke-static {p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Yw;->a(Ljava/lang/String;)V

    .line 82790
    return-void
.end method

.method public final declared-synchronized b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 82781
    monitor-enter p0

    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82782
    :try_start_1
    iget-object v0, p0, LX/0Yw;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rk;

    .line 82783
    new-instance v3, Lorg/json/JSONObject;

    iget-object v0, v0, LX/0rk;->a:Ljava/lang/String;

    invoke-direct {v3, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 82784
    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 82785
    :catch_0
    move-exception v0

    .line 82786
    :try_start_2
    sget-object v2, LX/0Yw;->c:Ljava/lang/Class;

    const-string v3, "Exception caused by invoking toJSONArray on traces that are not in json format."

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 82787
    :cond_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 82788
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 82777
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/0Yw;->d:Ljava/util/Queue;

    .line 82778
    const/4 v0, 0x0

    iput v0, p0, LX/0Yw;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82779
    monitor-exit p0

    return-void

    .line 82780
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 82768
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    iget v0, p0, LX/0Yw;->e:I

    iget-object v1, p0, LX/0Yw;->d:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1e

    add-int/2addr v0, v1

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 82769
    const/4 v1, 0x1

    .line 82770
    iget-object v0, p0, LX/0Yw;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rk;

    .line 82771
    if-eqz v1, :cond_0

    .line 82772
    const/4 v1, 0x0

    .line 82773
    :goto_1
    const-string v4, "[%d] %s"

    iget-wide v6, v0, LX/0rk;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iget-object v0, v0, LX/0rk;->a:Ljava/lang/String;

    invoke-static {v4, v5, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 82774
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 82775
    :cond_0
    const/16 v4, 0xa

    :try_start_1
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 82776
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method
