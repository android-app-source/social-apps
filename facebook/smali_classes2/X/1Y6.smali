.class public final LX/1Y6;
.super LX/0Rf;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Enum",
        "<TE;>;>",
        "LX/0Rf",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final transient a:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<TE;>;"
        }
    .end annotation
.end field

.field private transient b:I


# direct methods
.method public constructor <init>(Ljava/util/EnumSet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 273270
    invoke-direct {p0}, LX/0Rf;-><init>()V

    .line 273271
    iput-object p1, p0, LX/1Y6;->a:Ljava/util/EnumSet;

    .line 273272
    return-void
.end method

.method public static a(Ljava/util/EnumSet;)LX/0Rf;
    .locals 1

    .prologue
    .line 273252
    invoke-virtual {p0}, Ljava/util/EnumSet;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 273253
    new-instance v0, LX/1Y6;

    invoke-direct {v0, p0}, LX/1Y6;-><init>(Ljava/util/EnumSet;)V

    :goto_0
    return-object v0

    .line 273254
    :pswitch_0
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 273255
    goto :goto_0

    .line 273256
    :pswitch_1
    invoke-static {p0}, LX/0Ph;->d(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 273269
    iget-object v0, p0, LX/1Y6;->a:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 273266
    instance-of v0, p1, LX/1Y6;

    if-eqz v0, :cond_0

    .line 273267
    check-cast p1, LX/1Y6;

    iget-object p1, p1, LX/1Y6;->a:Ljava/util/EnumSet;

    .line 273268
    :cond_0
    iget-object v0, p0, LX/1Y6;->a:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 273260
    if-ne p1, p0, :cond_0

    .line 273261
    const/4 v0, 0x1

    .line 273262
    :goto_0
    return v0

    .line 273263
    :cond_0
    instance-of v0, p1, LX/1Y6;

    if-eqz v0, :cond_1

    .line 273264
    check-cast p1, LX/1Y6;

    iget-object p1, p1, LX/1Y6;->a:Ljava/util/EnumSet;

    .line 273265
    :cond_1
    iget-object v0, p0, LX/1Y6;->a:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 273258
    iget v0, p0, LX/1Y6;->b:I

    .line 273259
    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Y6;->a:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->hashCode()I

    move-result v0

    iput v0, p0, LX/1Y6;->b:I

    :cond_0
    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 273257
    iget-object v0, p0, LX/1Y6;->a:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final isHashCodeFast()Z
    .locals 1

    .prologue
    .line 273273
    const/4 v0, 0x1

    return v0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 273246
    const/4 v0, 0x0

    return v0
.end method

.method public final iterator()LX/0Rc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 273247
    iget-object v0, p0, LX/1Y6;->a:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0RZ;->a(Ljava/util/Iterator;)LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 273248
    invoke-virtual {p0}, LX/1Y6;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 273249
    iget-object v0, p0, LX/1Y6;->a:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->size()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273250
    iget-object v0, p0, LX/1Y6;->a:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 273251
    new-instance v0, LX/4y7;

    iget-object v1, p0, LX/1Y6;->a:Ljava/util/EnumSet;

    invoke-direct {v0, v1}, LX/4y7;-><init>(Ljava/util/EnumSet;)V

    return-object v0
.end method
