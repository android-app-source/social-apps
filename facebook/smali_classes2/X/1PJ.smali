.class public LX/1PJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static r:LX/0Xm;


# instance fields
.field public a:I

.field public b:I

.field public c:Lcom/facebook/nux/ui/NuxBubbleView;

.field public d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/content/Context;

.field private final f:Landroid/view/LayoutInflater;

.field private final g:LX/0hB;

.field public final h:LX/0Sy;

.field private final i:LX/03V;

.field private final j:LX/0Sh;

.field public k:LX/0Vq;

.field public l:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public m:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/view/View;",
            "LX/8D5;",
            ">;"
        }
    .end annotation
.end field

.field public n:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/view/ViewGroup;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public o:LX/8D5;

.field public final p:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "LX/1PL;",
            ">;"
        }
    .end annotation
.end field

.field public q:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Sy;Landroid/view/LayoutInflater;LX/0hB;LX/03V;LX/0Sh;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 243904
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243905
    iput-object v1, p0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    .line 243906
    iput-object v1, p0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    .line 243907
    iput v0, p0, LX/1PJ;->a:I

    .line 243908
    iput v0, p0, LX/1PJ;->b:I

    .line 243909
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/1PJ;->m:Ljava/util/WeakHashMap;

    .line 243910
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/1PJ;->n:Ljava/util/WeakHashMap;

    .line 243911
    const-class v0, LX/1PL;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, LX/1PJ;->p:Ljava/util/EnumSet;

    .line 243912
    iput-object p2, p0, LX/1PJ;->h:LX/0Sy;

    .line 243913
    iput-object p3, p0, LX/1PJ;->f:Landroid/view/LayoutInflater;

    .line 243914
    iput-object p4, p0, LX/1PJ;->g:LX/0hB;

    .line 243915
    iput-object p1, p0, LX/1PJ;->e:Landroid/content/Context;

    .line 243916
    iput-object p5, p0, LX/1PJ;->i:LX/03V;

    .line 243917
    iput-object p6, p0, LX/1PJ;->j:LX/0Sh;

    .line 243918
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->UNKNOWN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    iput-object v0, p0, LX/1PJ;->l:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 243919
    return-void
.end method

.method public static a(LX/0QB;)LX/1PJ;
    .locals 10

    .prologue
    .line 243893
    const-class v1, LX/1PJ;

    monitor-enter v1

    .line 243894
    :try_start_0
    sget-object v0, LX/1PJ;->r:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 243895
    sput-object v2, LX/1PJ;->r:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 243896
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243897
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 243898
    new-instance v3, LX/1PJ;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v5

    check-cast v5, LX/0Sy;

    invoke-static {v0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v7

    check-cast v7, LX/0hB;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v9

    check-cast v9, LX/0Sh;

    invoke-direct/range {v3 .. v9}, LX/1PJ;-><init>(Landroid/content/Context;LX/0Sy;Landroid/view/LayoutInflater;LX/0hB;LX/03V;LX/0Sh;)V

    .line 243899
    move-object v0, v3

    .line 243900
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 243901
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1PJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 243902
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 243903
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 243890
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 243891
    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 243892
    const/4 v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public static b(LX/1PJ;LX/8D5;)Z
    .locals 9

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 243865
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/8D5;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/8D5;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 243866
    :goto_0
    return v0

    .line 243867
    :cond_1
    iget-object v0, p1, LX/8D5;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 243868
    iget-object v1, p0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 243869
    new-array v4, v5, [I

    .line 243870
    new-array v5, v5, [I

    .line 243871
    :try_start_0
    invoke-virtual {v0, v4}, Landroid/view/View;->getLocationInWindow([I)V

    .line 243872
    invoke-virtual {v1, v5}, Landroid/view/View;->getLocationInWindow([I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 243873
    iget-object v1, p0, LX/1PJ;->e:Landroid/content/Context;

    const/high16 v6, 0x42820000    # 65.0f

    invoke-static {v1, v6}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    .line 243874
    iget-object v6, p0, LX/1PJ;->g:LX/0hB;

    invoke-virtual {v6}, LX/0hB;->d()I

    move-result v6

    .line 243875
    iget-boolean v7, p1, LX/8D5;->e:Z

    if-eqz v7, :cond_3

    .line 243876
    aget v7, v4, v3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x3

    add-int/2addr v0, v7

    iget v7, p0, LX/1PJ;->b:I

    sub-int/2addr v6, v7

    if-le v0, v6, :cond_2

    move v0, v2

    .line 243877
    goto :goto_0

    .line 243878
    :catch_0
    move-exception v0

    .line 243879
    iget-object v1, p0, LX/1PJ;->i:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "NPE happens inside the getLocationInWindow()"

    invoke-virtual {v1, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v2

    .line 243880
    goto :goto_0

    .line 243881
    :cond_2
    aget v0, v4, v3

    iget v6, p0, LX/1PJ;->a:I

    sub-int/2addr v0, v6

    aget v5, v5, v3

    sub-int/2addr v0, v5

    if-ge v0, v1, :cond_5

    move v0, v2

    .line 243882
    goto :goto_0

    .line 243883
    :cond_3
    aget v7, v4, v3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v8

    div-int/lit8 v8, v8, 0x3

    add-int/2addr v7, v8

    aget v5, v5, v3

    iget v8, p0, LX/1PJ;->a:I

    add-int/2addr v5, v8

    if-ge v7, v5, :cond_4

    move v0, v2

    .line 243884
    goto :goto_0

    .line 243885
    :cond_4
    aget v5, v4, v3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    iget v1, p0, LX/1PJ;->b:I

    sub-int v1, v6, v1

    if-le v0, v1, :cond_5

    move v0, v2

    .line 243886
    goto :goto_0

    .line 243887
    :cond_5
    aget v0, v4, v2

    iget-object v1, p0, LX/1PJ;->g:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    if-gt v0, v1, :cond_6

    aget v0, v4, v2

    if-gez v0, :cond_7

    :cond_6
    move v0, v2

    .line 243888
    goto/16 :goto_0

    :cond_7
    move v0, v3

    .line 243889
    goto/16 :goto_0
.end method

.method public static c(LX/1PJ;LX/8D5;)Lcom/facebook/nux/ui/NuxBubbleView;
    .locals 4
    .param p0    # LX/1PJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 243853
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    .line 243854
    :cond_0
    :goto_0
    return-object v1

    .line 243855
    :cond_1
    iget-object v0, p0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 243856
    if-eqz v0, :cond_0

    .line 243857
    iget-object v1, p0, LX/1PJ;->f:Landroid/view/LayoutInflater;

    iget v2, p1, LX/8D5;->a:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/nux/ui/NuxBubbleView;

    .line 243858
    iget-object v2, p1, LX/8D5;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/nux/ui/NuxBubbleView;->setBubbleBody(Ljava/lang/CharSequence;)V

    .line 243859
    iget-object v2, p1, LX/8D5;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/nux/ui/NuxBubbleView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 243860
    new-instance v2, LX/7zr;

    invoke-direct {v2, p0, p1}, LX/7zr;-><init>(LX/1PJ;LX/8D5;)V

    invoke-virtual {v1, v2}, Lcom/facebook/nux/ui/NuxBubbleView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 243861
    new-instance v2, LX/7zt;

    invoke-direct {v2, p0, p1}, LX/7zt;-><init>(LX/1PJ;LX/8D5;)V

    .line 243862
    iput-object v2, v1, Lcom/facebook/nux/ui/NuxBubbleView;->p:LX/7zs;

    .line 243863
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 243864
    goto :goto_0
.end method

.method public static c(LX/1PJ;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 243841
    iget-object v0, p0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    if-nez v0, :cond_0

    .line 243842
    :goto_0
    return-void

    .line 243843
    :cond_0
    iget-object v0, p0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    .line 243844
    iput-object v2, v0, Lcom/facebook/nux/ui/NuxBubbleView;->p:LX/7zs;

    .line 243845
    iget-object v0, p0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    invoke-virtual {v0, v2}, Lcom/facebook/nux/ui/NuxBubbleView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 243846
    iget-object v0, p0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    .line 243847
    iget-object v0, p0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 243848
    if-eqz v0, :cond_1

    .line 243849
    iget-object v1, p0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    .line 243850
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 243851
    :cond_1
    iput-object v2, p0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    .line 243852
    iput-object v2, p0, LX/1PJ;->o:LX/8D5;

    goto :goto_0
.end method

.method public static c(LX/1PJ;Z)V
    .locals 2

    .prologue
    .line 243835
    const/4 v0, 0x0

    iput-object v0, p0, LX/1PJ;->q:Ljava/lang/Runnable;

    .line 243836
    iget-object v0, p0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    if-nez v0, :cond_0

    .line 243837
    :goto_0
    return-void

    .line 243838
    :cond_0
    if-nez p1, :cond_1

    .line 243839
    iget-object v0, p0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/nux/ui/NuxBubbleView;->setVisibility(I)V

    .line 243840
    :cond_1
    iget-object v0, p0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    invoke-virtual {v0}, Lcom/facebook/nux/ui/NuxBubbleView;->e()V

    goto :goto_0
.end method

.method public static d(LX/1PJ;LX/8D5;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 243817
    iget-object v0, p1, LX/8D5;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 243818
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_1

    .line 243819
    :cond_0
    :goto_0
    return-void

    .line 243820
    :cond_1
    iget-object v1, p0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 243821
    if-eqz v1, :cond_0

    .line 243822
    new-array v2, v3, [I

    .line 243823
    new-array v3, v3, [I

    .line 243824
    invoke-virtual {v0, v2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 243825
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    .line 243826
    aget v1, v2, v4

    aget v4, v3, v4

    sub-int/2addr v1, v4

    .line 243827
    iget-boolean v4, p1, LX/8D5;->e:Z

    if-eqz v4, :cond_2

    .line 243828
    iget-object v4, p0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    invoke-virtual {v4}, Lcom/facebook/nux/ui/NuxBubbleView;->getHeight()I

    move-result v4

    sub-int/2addr v1, v4

    .line 243829
    :cond_2
    iget-object v4, p0, LX/1PJ;->e:Landroid/content/Context;

    iget v5, p1, LX/8D5;->f:I

    int-to-float v5, v5

    invoke-static {v4, v5}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v4

    add-int/2addr v1, v4

    .line 243830
    iget-object v4, p0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    int-to-float v1, v1

    invoke-virtual {v4, v1}, Lcom/facebook/nux/ui/NuxBubbleView;->setTranslationY(F)V

    .line 243831
    aget v1, v2, v6

    aget v2, v3, v6

    sub-int/2addr v1, v2

    iget-object v2, p0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    invoke-virtual {v2}, Lcom/facebook/nux/ui/NuxBubbleView;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    .line 243832
    iget-object v2, p0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    invoke-virtual {v2}, Lcom/facebook/nux/ui/NuxBubbleView;->getNubRightMargin()I

    move-result v2

    iget-object v3, p0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    invoke-virtual {v3}, Lcom/facebook/nux/ui/NuxBubbleView;->getPointerWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 243833
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    .line 243834
    iget-object v1, p0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Lcom/facebook/nux/ui/NuxBubbleView;->setTranslationX(F)V

    goto :goto_0
.end method

.method public static f$redex0(LX/1PJ;)V
    .locals 4

    .prologue
    .line 243814
    new-instance v0, Lcom/facebook/feed/nux/FeedNuxBubbleManager$2;

    invoke-direct {v0, p0}, Lcom/facebook/feed/nux/FeedNuxBubbleManager$2;-><init>(LX/1PJ;)V

    iput-object v0, p0, LX/1PJ;->q:Ljava/lang/Runnable;

    .line 243815
    iget-object v0, p0, LX/1PJ;->j:LX/0Sh;

    iget-object v1, p0, LX/1PJ;->q:Ljava/lang/Runnable;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, LX/0Sh;->a(Ljava/lang/Runnable;J)V

    .line 243816
    return-void
.end method

.method public static g(LX/1PJ;)Z
    .locals 1

    .prologue
    .line 243920
    iget-object v0, p0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    invoke-virtual {v0}, Lcom/facebook/nux/ui/NuxBubbleView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    invoke-virtual {v0}, Lcom/facebook/nux/ui/NuxBubbleView;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 243810
    invoke-static {p0}, LX/1PJ;->g(LX/1PJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243811
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/1PJ;->c(LX/1PJ;Z)V

    .line 243812
    iget-object v0, p0, LX/1PJ;->o:LX/8D5;

    iget-object v0, v0, LX/8D5;->d:LX/8D6;

    invoke-virtual {v0}, LX/8D6;->b()V

    .line 243813
    :cond_0
    return-void
.end method

.method public final a(LX/1PL;)V
    .locals 1

    .prologue
    .line 243807
    iget-object v0, p0, LX/1PJ;->p:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 243808
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1PJ;->a(Z)V

    .line 243809
    return-void
.end method

.method public final a(LX/8D5;)V
    .locals 4
    .param p1    # LX/8D5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 243793
    if-nez p1, :cond_1

    .line 243794
    :cond_0
    :goto_0
    return-void

    .line 243795
    :cond_1
    iget-object v0, p0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 243796
    iget-object v0, p1, LX/8D5;->d:LX/8D6;

    iget-object v1, p0, LX/1PJ;->l:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-virtual {v0, v1}, LX/8D6;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243797
    iget-object v0, p1, LX/8D5;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 243798
    if-eqz v0, :cond_0

    .line 243799
    iget-object v1, p0, LX/1PJ;->m:Ljava/util/WeakHashMap;

    invoke-virtual {v1, v0, p1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243800
    iget-object v1, p0, LX/1PJ;->n:Ljava/util/WeakHashMap;

    iget-object v2, p0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 243801
    if-nez v1, :cond_2

    .line 243802
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 243803
    iget-object v2, p0, LX/1PJ;->n:Ljava/util/WeakHashMap;

    iget-object v3, p0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243804
    :cond_2
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 243805
    invoke-static {p0}, LX/1PJ;->g(LX/1PJ;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 243806
    invoke-static {p0}, LX/1PJ;->f$redex0(LX/1PJ;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 243782
    iget-object v0, p0, LX/1PJ;->o:LX/8D5;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1PJ;->o:LX/8D5;

    iget-object v0, v0, LX/8D5;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    invoke-static {p0}, LX/1PJ;->g(LX/1PJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243783
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/1PJ;->c(LX/1PJ;Z)V

    .line 243784
    invoke-static {p0}, LX/1PJ;->c(LX/1PJ;)V

    .line 243785
    :cond_0
    iget-object v0, p0, LX/1PJ;->m:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 243786
    iget-object v0, p0, LX/1PJ;->n:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 243787
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 243788
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 243789
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 243790
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    if-ne v4, p1, :cond_2

    .line 243791
    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 243792
    :cond_3
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 4
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 243768
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->UNKNOWN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    iput-object v0, p0, LX/1PJ;->l:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 243769
    if-nez p1, :cond_1

    .line 243770
    :cond_0
    :goto_0
    return-void

    .line 243771
    :cond_1
    iget-object v0, p0, LX/1PJ;->n:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 243772
    if-eqz v0, :cond_0

    .line 243773
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 243774
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 243775
    if-eqz v0, :cond_2

    .line 243776
    iget-object v1, p0, LX/1PJ;->m:Ljava/util/WeakHashMap;

    invoke-virtual {v1, v0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8D5;

    .line 243777
    iget-object v3, p0, LX/1PJ;->m:Ljava/util/WeakHashMap;

    invoke-virtual {v3, v0}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 243778
    iget-object v0, p0, LX/1PJ;->o:LX/8D5;

    if-ne v0, v1, :cond_2

    .line 243779
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/1PJ;->c(LX/1PJ;Z)V

    .line 243780
    invoke-static {p0}, LX/1PJ;->c(LX/1PJ;)V

    goto :goto_1

    .line 243781
    :cond_3
    iget-object v0, p0, LX/1PJ;->n:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 243760
    iput-object p2, p0, LX/1PJ;->l:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 243761
    iget-object v0, p0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 243762
    invoke-static {p0}, LX/1PJ;->c(LX/1PJ;)V

    .line 243763
    :cond_0
    iget-object v0, p0, LX/1PJ;->k:LX/0Vq;

    if-eqz v0, :cond_1

    .line 243764
    :goto_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    .line 243765
    return-void

    .line 243766
    :cond_1
    new-instance v0, LX/1PM;

    invoke-direct {v0, p0}, LX/1PM;-><init>(LX/1PJ;)V

    iput-object v0, p0, LX/1PJ;->k:LX/0Vq;

    .line 243767
    iget-object v0, p0, LX/1PJ;->h:LX/0Sy;

    iget-object p2, p0, LX/1PJ;->k:LX/0Vq;

    invoke-virtual {v0, p2}, LX/0Sy;->a(LX/0Vq;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 243752
    invoke-static {p0}, LX/1PJ;->g(LX/1PJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243753
    invoke-static {p0, p1}, LX/1PJ;->c(LX/1PJ;Z)V

    .line 243754
    iget-object v0, p0, LX/1PJ;->o:LX/8D5;

    iget-object v0, v0, LX/8D5;->d:LX/8D6;

    .line 243755
    invoke-static {v0}, LX/8D6;->f(LX/8D6;)Lcom/facebook/nux/NuxHistory;

    move-result-object v1

    .line 243756
    iget-object p0, v0, LX/8D6;->e:LX/0SG;

    invoke-virtual {v1, p0}, Lcom/facebook/nux/NuxHistory;->a(LX/0SG;)Lcom/facebook/nux/NuxHistory;

    .line 243757
    iget-object p0, v0, LX/8D6;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p0

    invoke-static {v0}, LX/8D6;->d(LX/8D6;)LX/8D7;

    move-result-object p1

    iget-object p1, p1, LX/8D7;->prefKey:LX/0Tn;

    invoke-static {v0, v1}, LX/8D6;->a(LX/8D6;Lcom/facebook/nux/NuxHistory;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, p1, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 243758
    iget-object v1, v0, LX/8D6;->d:LX/0iA;

    invoke-static {v0}, LX/8D6;->e(LX/8D6;)LX/0i1;

    move-result-object p0

    invoke-virtual {v1, p0}, LX/0iA;->a(LX/0i1;)V

    .line 243759
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 243747
    invoke-static {p0}, LX/1PJ;->g(LX/1PJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243748
    iget-object v0, p0, LX/1PJ;->o:LX/8D5;

    invoke-static {p0, v0}, LX/1PJ;->b(LX/1PJ;LX/8D5;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243749
    iget-object v0, p0, LX/1PJ;->o:LX/8D5;

    invoke-static {p0, v0}, LX/1PJ;->d(LX/1PJ;LX/8D5;)V

    .line 243750
    :cond_0
    :goto_0
    return-void

    .line 243751
    :cond_1
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/1PJ;->c(LX/1PJ;Z)V

    goto :goto_0
.end method

.method public final b(LX/1PL;)V
    .locals 1

    .prologue
    .line 243745
    iget-object v0, p0, LX/1PJ;->p:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    .line 243746
    return-void
.end method
