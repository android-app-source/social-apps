.class public LX/11g;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/11g;


# instance fields
.field private final b:LX/11i;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    .line 172865
    const-string v0, "com.facebook.katana.activity.FbFragmentChromeActivity"

    const-string v1, "com.facebook.katana.activity.FbMainTabActivity"

    const-string v2, "com.facebook.katana.FacebookLoginActivity"

    const-string v3, "com.facebook.katana.activity.ImmersiveActivity"

    const-string v4, "com.facebook.katana.fragment.FbChromeFragment"

    const-string v5, "com.facebook.katana.ui.bookmark.BookmarkMenuFragment"

    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "com.facebook.photos.consumptiongallery.snowflake.ConsumptionSnowflakeFragment"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "com.facebook.virtuallifecycle.LifecycleReporterFragment"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "com.facebook.ui.drawers.BackStackFragment"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "com.facebook.katana.IntentUriHandler"

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string v8, "com.facebook.timeline.TimelineFragment"

    aput-object v8, v6, v7

    const/4 v7, 0x5

    const-string v8, "UserAction"

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/11g;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/11i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 172866
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172867
    iput-object p1, p0, LX/11g;->b:LX/11i;

    .line 172868
    return-void
.end method

.method public static a(LX/0QB;)LX/11g;
    .locals 4

    .prologue
    .line 172849
    sget-object v0, LX/11g;->c:LX/11g;

    if-nez v0, :cond_1

    .line 172850
    const-class v1, LX/11g;

    monitor-enter v1

    .line 172851
    :try_start_0
    sget-object v0, LX/11g;->c:LX/11g;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 172852
    if-eqz v2, :cond_0

    .line 172853
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 172854
    new-instance p0, LX/11g;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-direct {p0, v3}, LX/11g;-><init>(LX/11i;)V

    .line 172855
    move-object v0, p0

    .line 172856
    sput-object v0, LX/11g;->c:LX/11g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 172857
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 172858
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 172859
    :cond_1
    sget-object v0, LX/11g;->c:LX/11g;

    return-object v0

    .line 172860
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 172861
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/11g;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 172862
    sget-object v0, LX/11g;->a:LX/0Rf;

    invoke-virtual {v0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 172863
    iget-object v0, p0, LX/11g;->b:LX/11i;

    invoke-interface {v0, p1}, LX/11i;->a(Ljava/lang/String;)V

    .line 172864
    :cond_0
    return-void
.end method
