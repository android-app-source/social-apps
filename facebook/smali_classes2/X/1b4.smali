.class public LX/1b4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0ad;

.field public final b:LX/0Uh;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Ri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0ad;LX/0Uh;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/6Ri;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 279143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279144
    iput-object p1, p0, LX/1b4;->a:LX/0ad;

    .line 279145
    iput-object p2, p0, LX/1b4;->b:LX/0Uh;

    .line 279146
    iput-object p3, p0, LX/1b4;->c:LX/0Ot;

    .line 279147
    return-void
.end method

.method public static a(LX/0QB;)LX/1b4;
    .locals 1

    .prologue
    .line 279142
    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v0

    return-object v0
.end method

.method public static ag(LX/1b4;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 279141
    iget-object v0, p0, LX/1b4;->a:LX/0ad;

    sget-char v1, LX/1v6;->b:C

    const-string v2, "disabled"

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1b4;
    .locals 4

    .prologue
    .line 279139
    new-instance v2, LX/1b4;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const/16 v3, 0x1bc8

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, LX/1b4;-><init>(LX/0ad;LX/0Uh;LX/0Ot;)V

    .line 279140
    return-object v2
.end method


# virtual methods
.method public final C()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 279138
    iget-object v1, p0, LX/1b4;->b:LX/0Uh;

    const/16 v2, 0x33d

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, LX/1b4;->D()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final D()Z
    .locals 3

    .prologue
    .line 279137
    iget-object v0, p0, LX/1b4;->b:LX/0Uh;

    const/16 v1, 0x33c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final F()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 279134
    invoke-static {p0}, LX/1b4;->ag(LX/1b4;)Ljava/lang/String;

    move-result-object v1

    .line 279135
    if-nez v1, :cond_1

    .line 279136
    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v2, "opt_in"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "auto_enable"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final H()LX/6Ri;
    .locals 1

    .prologue
    .line 279133
    iget-object v0, p0, LX/1b4;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Ri;

    return-object v0
.end method

.method public final I()Z
    .locals 3

    .prologue
    .line 279132
    iget-object v0, p0, LX/1b4;->b:LX/0Uh;

    const/16 v1, 0x669

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final W()Z
    .locals 3

    .prologue
    .line 279131
    iget-object v0, p0, LX/1b4;->b:LX/0Uh;

    const/16 v1, 0x317

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final X()Z
    .locals 3

    .prologue
    .line 279130
    iget-object v0, p0, LX/1b4;->b:LX/0Uh;

    const/16 v1, 0x340

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 279129
    iget-object v0, p0, LX/1b4;->b:LX/0Uh;

    const/16 v1, 0x342

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLMedia;)Z
    .locals 1

    .prologue
    .line 279148
    invoke-virtual {p0}, LX/1b4;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bQ()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Z)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 279114
    if-eqz p1, :cond_0

    .line 279115
    iget-object v0, p0, LX/1b4;->b:LX/0Uh;

    const/16 v1, 0x5ab

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 279116
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1b4;->a:LX/0ad;

    sget-short v1, LX/1v6;->h:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0
.end method

.method public final aa()Z
    .locals 3

    .prologue
    .line 279117
    iget-object v0, p0, LX/1b4;->a:LX/0ad;

    sget-short v1, LX/1v6;->y:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final ab()Z
    .locals 3

    .prologue
    .line 279118
    iget-object v0, p0, LX/1b4;->b:LX/0Uh;

    const/16 v1, 0x33e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final ae()Z
    .locals 3

    .prologue
    .line 279119
    iget-object v0, p0, LX/1b4;->b:LX/0Uh;

    const/16 v1, 0x349

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 279120
    iget-object v1, p0, LX/1b4;->b:LX/0Uh;

    const/16 v2, 0x34c

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LX/1b4;->F()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 279121
    iget-object v0, p0, LX/1b4;->a:LX/0ad;

    sget-short v1, LX/1v6;->M:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 3

    .prologue
    .line 279122
    iget-object v0, p0, LX/1b4;->a:LX/0ad;

    sget-short v1, LX/1v6;->J:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 3

    .prologue
    .line 279123
    iget-object v0, p0, LX/1b4;->b:LX/0Uh;

    const/16 v1, 0x35c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final n()Z
    .locals 3

    .prologue
    .line 279124
    iget-object v0, p0, LX/1b4;->a:LX/0ad;

    sget-short v1, LX/1v6;->S:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final o()Z
    .locals 3

    .prologue
    .line 279125
    iget-object v0, p0, LX/1b4;->a:LX/0ad;

    sget-short v1, LX/1v6;->V:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 3

    .prologue
    .line 279126
    iget-object v0, p0, LX/1b4;->a:LX/0ad;

    sget-short v1, LX/1v6;->U:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final s()Z
    .locals 3

    .prologue
    .line 279127
    iget-object v0, p0, LX/1b4;->a:LX/0ad;

    sget-short v1, LX/1v6;->m:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final w()Z
    .locals 3

    .prologue
    .line 279128
    iget-object v0, p0, LX/1b4;->a:LX/0ad;

    sget-short v1, LX/1v6;->i:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
