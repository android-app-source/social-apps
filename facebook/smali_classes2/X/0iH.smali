.class public LX/0iH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0fK;

.field private b:I

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field public g:Z


# direct methods
.method public constructor <init>(IFLX/0fK;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 120633
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120634
    iput v0, p0, LX/0iH;->c:F

    .line 120635
    iput v0, p0, LX/0iH;->d:F

    .line 120636
    iput v0, p0, LX/0iH;->e:F

    .line 120637
    iput p1, p0, LX/0iH;->b:I

    .line 120638
    iput p2, p0, LX/0iH;->f:F

    .line 120639
    iput-object p3, p0, LX/0iH;->a:LX/0fK;

    .line 120640
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0iH;->g:Z

    .line 120641
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 120642
    iget-boolean v0, p0, LX/0iH;->g:Z

    if-nez v0, :cond_0

    .line 120643
    :goto_0
    return v2

    .line 120644
    :cond_0
    iget-object v0, p0, LX/0iH;->a:LX/0fK;

    invoke-virtual {v0, p1, p1}, LX/0fK;->a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V

    .line 120645
    iput v1, p0, LX/0iH;->c:F

    .line 120646
    iput v1, p0, LX/0iH;->d:F

    .line 120647
    iput v1, p0, LX/0iH;->e:F

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 120648
    iget-boolean v0, p0, LX/0iH;->g:Z

    if-nez v0, :cond_0

    .line 120649
    :goto_0
    return v2

    .line 120650
    :cond_0
    iget v0, p0, LX/0iH;->d:F

    add-float/2addr v0, p3

    iput v0, p0, LX/0iH;->d:F

    .line 120651
    iget v0, p0, LX/0iH;->e:F

    add-float/2addr v0, p4

    iput v0, p0, LX/0iH;->e:F

    .line 120652
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    iget v0, p0, LX/0iH;->c:F

    add-float/2addr v0, p3

    iput v0, p0, LX/0iH;->c:F

    iget v1, p0, LX/0iH;->b:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget v0, p0, LX/0iH;->d:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, LX/0iH;->e:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 120653
    iget v0, p0, LX/0iH;->f:F

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 120654
    :cond_1
    iget-object v0, p0, LX/0iH;->a:LX/0fK;

    invoke-virtual {v0, p1, p2}, LX/0fK;->a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V

    goto :goto_0
.end method
