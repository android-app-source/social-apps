.class public abstract enum LX/1i5;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/0Rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1i5;",
        ">;",
        "LX/0Rl",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1i5;

.field public static final enum ALWAYS_FALSE:LX/1i5;

.field public static final enum ALWAYS_TRUE:LX/1i5;

.field public static final enum IS_NULL:LX/1i5;

.field public static final enum NOT_NULL:LX/1i5;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 297512
    new-instance v0, LX/1i6;

    const-string v1, "ALWAYS_TRUE"

    invoke-direct {v0, v1, v2}, LX/1i6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1i5;->ALWAYS_TRUE:LX/1i5;

    .line 297513
    new-instance v0, LX/1i7;

    const-string v1, "ALWAYS_FALSE"

    invoke-direct {v0, v1, v3}, LX/1i7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1i5;->ALWAYS_FALSE:LX/1i5;

    .line 297514
    new-instance v0, LX/1i8;

    const-string v1, "IS_NULL"

    invoke-direct {v0, v1, v4}, LX/1i8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1i5;->IS_NULL:LX/1i5;

    .line 297515
    new-instance v0, LX/1i9;

    const-string v1, "NOT_NULL"

    invoke-direct {v0, v1, v5}, LX/1i9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1i5;->NOT_NULL:LX/1i5;

    .line 297516
    const/4 v0, 0x4

    new-array v0, v0, [LX/1i5;

    sget-object v1, LX/1i5;->ALWAYS_TRUE:LX/1i5;

    aput-object v1, v0, v2

    sget-object v1, LX/1i5;->ALWAYS_FALSE:LX/1i5;

    aput-object v1, v0, v3

    sget-object v1, LX/1i5;->IS_NULL:LX/1i5;

    aput-object v1, v0, v4

    sget-object v1, LX/1i5;->NOT_NULL:LX/1i5;

    aput-object v1, v0, v5

    sput-object v0, LX/1i5;->$VALUES:[LX/1i5;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 297517
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1i5;
    .locals 1

    .prologue
    .line 297518
    const-class v0, LX/1i5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1i5;

    return-object v0
.end method

.method public static values()[LX/1i5;
    .locals 1

    .prologue
    .line 297519
    sget-object v0, LX/1i5;->$VALUES:[LX/1i5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1i5;

    return-object v0
.end method


# virtual methods
.method public withNarrowedType()LX/0Rl;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "LX/0Rl",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 297520
    return-object p0
.end method
