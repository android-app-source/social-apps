.class public LX/1kT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final k:Ljava/lang/Object;


# instance fields
.field public final b:Landroid/content/ClipboardManager;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:LX/1E1;

.field private final g:LX/03V;

.field private final h:LX/1bQ;

.field public final i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final j:LX/1kD;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 309782
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->CLIPBOARD:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1kT;->a:Ljava/lang/String;

    .line 309783
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/1kT;->k:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1E1;LX/03V;LX/1bQ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1kD;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 309769
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309770
    iput-object p2, p0, LX/1kT;->f:LX/1E1;

    .line 309771
    iput-object p3, p0, LX/1kT;->g:LX/03V;

    .line 309772
    const-string v0, "clipboard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    iput-object v0, p0, LX/1kT;->b:Landroid/content/ClipboardManager;

    .line 309773
    iput-object p4, p0, LX/1kT;->h:LX/1bQ;

    .line 309774
    iput-object p5, p0, LX/1kT;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 309775
    iput-object p6, p0, LX/1kT;->j:LX/1kD;

    .line 309776
    const v0, 0x7f020268

    invoke-static {v0}, LX/1bQ;->a(I)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1kT;->c:Ljava/lang/String;

    .line 309777
    const v0, 0x7f082727

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1kT;->d:Ljava/lang/String;

    .line 309778
    const v0, 0x7f082728

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1kT;->e:Ljava/lang/String;

    .line 309779
    iget-object v0, p0, LX/1kT;->f:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1kT;->f:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 309780
    :cond_0
    iget-object v0, p0, LX/1kT;->b:Landroid/content/ClipboardManager;

    new-instance p1, LX/1kU;

    invoke-direct {p1, p0}, LX/1kU;-><init>(LX/1kT;)V

    invoke-virtual {v0, p1}, Landroid/content/ClipboardManager;->addPrimaryClipChangedListener(Landroid/content/ClipboardManager$OnPrimaryClipChangedListener;)V

    .line 309781
    :cond_1
    return-void
.end method

.method public static a(LX/0QB;)LX/1kT;
    .locals 14

    .prologue
    .line 309740
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 309741
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 309742
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 309743
    if-nez v1, :cond_0

    .line 309744
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 309745
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 309746
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 309747
    sget-object v1, LX/1kT;->k:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 309748
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 309749
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 309750
    :cond_1
    if-nez v1, :cond_4

    .line 309751
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 309752
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 309753
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 309754
    new-instance v7, LX/1kT;

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v0}, LX/1E1;->b(LX/0QB;)LX/1E1;

    move-result-object v9

    check-cast v9, LX/1E1;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {v0}, LX/1bQ;->b(LX/0QB;)LX/1bQ;

    move-result-object v11

    check-cast v11, LX/1bQ;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v12

    check-cast v12, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/1kD;->a(LX/0QB;)LX/1kD;

    move-result-object v13

    check-cast v13, LX/1kD;

    invoke-direct/range {v7 .. v13}, LX/1kT;-><init>(Landroid/content/Context;LX/1E1;LX/03V;LX/1bQ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1kD;)V

    .line 309755
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 309756
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 309757
    if-nez v1, :cond_2

    .line 309758
    sget-object v0, LX/1kT;->k:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1kT;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 309759
    :goto_1
    if-eqz v0, :cond_3

    .line 309760
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 309761
    :goto_3
    check-cast v0, LX/1kT;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 309762
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 309763
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 309764
    :catchall_1
    move-exception v0

    .line 309765
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 309766
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 309767
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 309768
    :cond_2
    :try_start_8
    sget-object v0, LX/1kT;->k:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1kT;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static e(LX/1kT;)V
    .locals 6

    .prologue
    .line 309734
    invoke-static {p0}, LX/1kT;->g(LX/1kT;)Ljava/lang/String;

    move-result-object v0

    .line 309735
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/1kT;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/1kp;->q:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 309736
    :cond_0
    :goto_0
    return-void

    .line 309737
    :cond_1
    iget-object v1, p0, LX/1kT;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/1kp;->q:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/1kp;->r:LX/0Tn;

    .line 309738
    sget-object v2, LX/0SF;->a:LX/0SF;

    move-object v2, v2

    .line 309739
    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    const-wide/32 v4, 0x1b7740

    add-long/2addr v2, v4

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method public static f(LX/1kT;)Z
    .locals 1

    .prologue
    .line 309784
    invoke-static {p0}, LX/1kT;->g(LX/1kT;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static g(LX/1kT;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 309724
    iget-object v1, p0, LX/1kT;->b:Landroid/content/ClipboardManager;

    invoke-virtual {v1}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    move-result v1

    if-nez v1, :cond_1

    .line 309725
    :cond_0
    :goto_0
    return-object v0

    .line 309726
    :cond_1
    iget-object v1, p0, LX/1kT;->b:Landroid/content/ClipboardManager;

    invoke-virtual {v1}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v1

    .line 309727
    iget-object v2, p0, LX/1kT;->b:Landroid/content/ClipboardManager;

    invoke-virtual {v2}, Landroid/content/ClipboardManager;->getPrimaryClipDescription()Landroid/content/ClipDescription;

    move-result-object v2

    .line 309728
    invoke-virtual {v2}, Landroid/content/ClipDescription;->getMimeTypeCount()I

    move-result v3

    .line 309729
    if-lez v3, :cond_2

    invoke-virtual {v2, v4}, Landroid/content/ClipDescription;->getMimeType(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "text/plain"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 309730
    :cond_2
    invoke-virtual {v1, v4}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 309731
    if-nez v2, :cond_3

    .line 309732
    iget-object v2, p0, LX/1kT;->g:LX/03V;

    const-class v3, LX/1kT;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Converting clipboard data into text but is null. Current clip data is "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 309733
    :cond_3
    sget-object v1, Landroid/util/Patterns;->WEB_URL:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 309697
    iget-object v0, p0, LX/1kT;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/1kp;->q:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 309698
    iget-object v0, p0, LX/1kT;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/1kp;->r:LX/0Tn;

    const-wide/16 v6, 0x0

    invoke-interface {v0, v3, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 309699
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/32 v8, 0x1b7740

    sub-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 309700
    invoke-static {p0}, LX/1kT;->g(LX/1kT;)Ljava/lang/String;

    move-result-object v6

    .line 309701
    if-eqz v5, :cond_2

    .line 309702
    sget-object v4, LX/0SF;->a:LX/0SF;

    move-object v4, v4

    .line 309703
    invoke-virtual {v4}, LX/0SF;->a()J

    move-result-wide v8

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v0, v8, v10

    if-gtz v0, :cond_2

    move v4, v1

    .line 309704
    :goto_0
    if-eqz v5, :cond_3

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/32 v10, 0x493e0

    add-long/2addr v8, v10

    .line 309705
    sget-object v0, LX/0SF;->a:LX/0SF;

    move-object v0, v0

    .line 309706
    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v10

    cmp-long v0, v8, v10

    if-gez v0, :cond_3

    move v3, v1

    .line 309707
    :goto_1
    if-eqz v5, :cond_4

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 309708
    :goto_2
    invoke-static {p0}, LX/1kT;->f(LX/1kT;)Z

    move-result v6

    .line 309709
    const/4 v7, 0x0

    .line 309710
    if-eqz v3, :cond_6

    .line 309711
    const-string v7, "This link has been prompted before"

    .line 309712
    :cond_0
    :goto_3
    iget-object v8, p0, LX/1kT;->j:LX/1kD;

    sget-object v9, LX/1kT;->a:Ljava/lang/String;

    invoke-virtual {v8, v9, v7}, LX/1kD;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 309713
    if-eqz v3, :cond_5

    if-eqz v0, :cond_5

    if-nez v4, :cond_1

    if-eqz v6, :cond_5

    :cond_1
    :goto_4
    return v1

    :cond_2
    move v4, v2

    .line 309714
    goto :goto_0

    :cond_3
    move v3, v2

    .line 309715
    goto :goto_1

    :cond_4
    move v0, v2

    .line 309716
    goto :goto_2

    :cond_5
    move v1, v2

    .line 309717
    goto :goto_4

    .line 309718
    :cond_6
    if-nez v0, :cond_7

    .line 309719
    const-string v7, "Link in clipboard is replaced by something else"

    goto :goto_3

    .line 309720
    :cond_7
    if-nez v5, :cond_8

    .line 309721
    const-string v7, "Text in clipboard is not a valid URI"

    goto :goto_3

    .line 309722
    :cond_8
    if-nez v4, :cond_0

    .line 309723
    const-string v7, "Link expired. Needs to be copied in the last 1800000 ms"

    goto :goto_3
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 309693
    iget-object v0, p0, LX/1kT;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1kp;->r:LX/0Tn;

    .line 309694
    sget-object v2, LX/0SF;->a:LX/0SF;

    move-object v2, v2

    .line 309695
    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 309696
    return-void
.end method
