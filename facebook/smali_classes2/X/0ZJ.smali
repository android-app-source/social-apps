.class public LX/0ZJ;
.super LX/0ZK;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ZK",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile b:LX/0ZJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83166
    const-class v0, LX/0ZJ;

    sput-object v0, LX/0ZJ;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 83179
    const/16 v0, 0x14

    invoke-direct {p0, v0}, LX/0ZK;-><init>(I)V

    .line 83180
    return-void
.end method

.method public static a(LX/0QB;)LX/0ZJ;
    .locals 3

    .prologue
    .line 83167
    sget-object v0, LX/0ZJ;->b:LX/0ZJ;

    if-nez v0, :cond_1

    .line 83168
    const-class v1, LX/0ZJ;

    monitor-enter v1

    .line 83169
    :try_start_0
    sget-object v0, LX/0ZJ;->b:LX/0ZJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 83170
    if-eqz v2, :cond_0

    .line 83171
    :try_start_1
    new-instance v0, LX/0ZJ;

    invoke-direct {v0}, LX/0ZJ;-><init>()V

    .line 83172
    move-object v0, v0

    .line 83173
    sput-object v0, LX/0ZJ;->b:LX/0ZJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83174
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 83175
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 83176
    :cond_1
    sget-object v0, LX/0ZJ;->b:LX/0ZJ;

    return-object v0

    .line 83177
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 83178
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
