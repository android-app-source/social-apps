.class public LX/0S6;
.super LX/0R5;
.source ""

# interfaces
.implements LX/0R6;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0QA;


# direct methods
.method public constructor <init>(LX/0QA;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 60755
    invoke-direct {p0, p1}, LX/0R5;-><init>(LX/0QA;)V

    .line 60756
    iput-object p1, p0, LX/0S6;->b:LX/0QA;

    .line 60757
    iput-object p2, p0, LX/0S6;->a:Landroid/content/Context;

    .line 60758
    return-void
.end method

.method public static a(LX/0S7;)V
    .locals 0

    .prologue
    .line 60748
    invoke-virtual {p0}, LX/0S7;->c()V

    .line 60749
    invoke-virtual {p0}, LX/0S7;->b()V

    .line 60750
    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 60751
    invoke-virtual {p0}, LX/0S6;->c()LX/0S7;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 60723
    invoke-virtual {p0}, LX/0S6;->c()LX/0S7;

    move-result-object v1

    .line 60724
    :try_start_0
    iget-object v0, p0, LX/0S6;->b:LX/0QA;

    invoke-virtual {v0, p1, p2}, LX/0QA;->a(Ljava/lang/Class;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60725
    invoke-static {v1}, LX/0S6;->a(LX/0S7;)V

    .line 60726
    return-void

    .line 60727
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/0S6;->a(LX/0S7;)V

    throw v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 60752
    check-cast p1, LX/0S7;

    invoke-static {p1}, LX/0S6;->a(LX/0S7;)V

    .line 60753
    return-void
.end method

.method public final b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 60754
    iget-object v0, p0, LX/0S6;->a:Landroid/content/Context;

    return-object v0
.end method

.method public final c()LX/0S7;
    .locals 2

    .prologue
    .line 60740
    invoke-virtual {p0}, LX/0R5;->getInjectorThreadStack()LX/0S7;

    move-result-object v0

    .line 60741
    iget-object v1, p0, LX/0S6;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/0S7;->a(Landroid/content/Context;)V

    .line 60742
    invoke-virtual {v0, p0}, LX/0S7;->a(LX/0R6;)V

    .line 60743
    return-object v0
.end method

.method public final d()LX/0SI;
    .locals 2

    .prologue
    .line 60744
    iget-object v0, p0, LX/0S6;->a:Landroid/content/Context;

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 60745
    if-eqz v0, :cond_0

    .line 60746
    invoke-static {p0}, LX/0l5;->a(LX/0QB;)LX/0SI;

    move-result-object v0

    check-cast v0, LX/0SI;

    .line 60747
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/0WI;->a(LX/0QB;)LX/0SI;

    move-result-object v0

    check-cast v0, LX/0SI;

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 60739
    const/4 v0, 0x0

    return v0
.end method

.method public final getInstance(LX/0RI;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 60736
    invoke-virtual {p0}, LX/0S6;->c()LX/0S7;

    move-result-object v1

    .line 60737
    :try_start_0
    iget-object v0, p0, LX/0S6;->b:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getScopeUnawareInjector()LX/0QD;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getInstance(LX/0RI;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 60738
    invoke-static {v1}, LX/0S6;->a(LX/0S7;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/0S6;->a(LX/0S7;)V

    throw v0
.end method

.method public final getLazy(LX/0RI;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Ot",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 60735
    iget-object v0, p0, LX/0S6;->b:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getScopeUnawareInjector()LX/0QD;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    invoke-static {v0, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public final getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;>;)",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60732
    invoke-virtual {p0}, LX/0S6;->c()LX/0S7;

    move-result-object v1

    .line 60733
    :try_start_0
    iget-object v0, p0, LX/0S6;->b:LX/0QA;

    invoke-virtual {v0, p1}, LX/0QA;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 60734
    invoke-static {v1}, LX/0S6;->a(LX/0S7;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/0S6;->a(LX/0S7;)V

    throw v0
.end method

.method public final getProvider(LX/0RI;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 60729
    iget-object v0, p0, LX/0S6;->b:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getScopeUnawareInjector()LX/0QD;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    .line 60730
    new-instance p1, LX/18F;

    invoke-direct {p1, p0, v0}, LX/18F;-><init>(LX/0S6;LX/0Or;)V

    move-object v0, p1

    .line 60731
    return-object v0
.end method

.method public final getScopeAwareInjector()LX/0R6;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60728
    return-object p0
.end method
