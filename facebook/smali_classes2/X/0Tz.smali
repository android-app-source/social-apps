.class public abstract LX/0Tz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64062
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64063
    iput-object p1, p0, LX/0Tz;->a:Ljava/lang/String;

    .line 64064
    iput-object p2, p0, LX/0Tz;->b:LX/0Px;

    .line 64065
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Tz;->c:LX/0Px;

    .line 64066
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Px;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;",
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64068
    iput-object p1, p0, LX/0Tz;->a:Ljava/lang/String;

    .line 64069
    iput-object p2, p0, LX/0Tz;->b:LX/0Px;

    .line 64070
    iput-object p3, p0, LX/0Tz;->c:LX/0Px;

    .line 64071
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Px;LX/0sv;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;",
            "LX/0sv;",
            ")V"
        }
    .end annotation

    .prologue
    .line 64072
    invoke-static {p3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 64073
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 64074
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DROP TABLE IF EXISTS "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;",
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 64075
    const-string v0, "CREATE TABLE"

    invoke-static {p0, p1, p2, v0}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;",
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 64076
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 64077
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64078
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64079
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64080
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64081
    const-string v1, ", "

    invoke-static {v1}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v1

    sget-object v2, LX/0U1;->b:LX/0QK;

    invoke-static {p1, v2}, LX/0PN;->a(Ljava/util/Collection;LX/0QK;)Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64082
    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 64083
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64084
    const-string v1, ", "

    invoke-static {v1}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v1

    sget-object v2, LX/2T8;->a:LX/0QK;

    invoke-static {p2, v2}, LX/0PN;->a(Ljava/util/Collection;LX/0QK;)Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64085
    :cond_0
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64086
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 64044
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ALTER TABLE "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ADD COLUMN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 64045
    iget-object v1, p1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v1

    .line 64046
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 64047
    iget-object v1, p1, LX/0U1;->e:Ljava/lang/String;

    move-object v1, v1

    .line 64048
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;LX/0U1;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 64087
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ALTER TABLE "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ADD COLUMN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 64088
    iget-object v1, p1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v1

    .line 64089
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 64090
    iget-object v1, p1, LX/0U1;->e:Ljava/lang/String;

    move-object v1, v1

    .line 64091
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DEFAULT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 64092
    invoke-static {p2}, LX/0Tz;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 64093
    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 3

    .prologue
    .line 64094
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x29

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 64095
    const-string v1, "CREATE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64096
    if-eqz p3, :cond_0

    .line 64097
    const-string v1, " UNIQUE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64098
    :cond_0
    const-string v1, " INDEX IF NOT EXISTS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64099
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64100
    const-string v1, " ON "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64101
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64102
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64103
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64104
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64105
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0U1;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 64106
    const-string v0, ", "

    invoke-static {v0}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v0

    sget-object v1, LX/0U1;->c:LX/0QK;

    invoke-static {p0, v1}, LX/0PN;->a(Ljava/util/Collection;LX/0QK;)Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/List;LX/0P1;)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0U1;",
            ">;",
            "LX/0P1",
            "<",
            "LX/0U1;",
            "LX/0QK",
            "<",
            "LX/0U1;",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 64107
    const-string v0, ", "

    invoke-static {v0}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v0

    new-instance v1, LX/491;

    invoke-direct {v1, p1}, LX/491;-><init>(LX/0P1;)V

    invoke-static {p0, v1}, LX/0PN;->a(Ljava/util/Collection;LX/0QK;)Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0P1;LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;",
            "LX/0P1",
            "<",
            "LX/0U1;",
            "LX/0QK",
            "<",
            "LX/0U1;",
            "Ljava/lang/String;",
            ">;>;",
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64049
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_temp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 64050
    invoke-static {p2}, LX/0Tz;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 64051
    invoke-static {p2, p3}, LX/0Tz;->a(Ljava/util/List;LX/0P1;)Ljava/lang/String;

    move-result-object v2

    .line 64052
    const-string v3, "INSERT INTO %s SELECT %s FROM %s"

    .line 64053
    invoke-static {v3, v0, v1, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 64054
    invoke-static {v3, p1, v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 64055
    invoke-static {v0, p2, p4}, LX/0Tz;->c(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v3

    const v4, 0x1c384242

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {p0, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v3, -0x5fb213f1

    invoke-static {v3}, LX/03h;->a(I)V

    .line 64056
    const v3, -0x3a61f79b

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x5636df2a

    invoke-static {v1}, LX/03h;->a(I)V

    .line 64057
    invoke-static {p1}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v3, -0x40b00346

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x332dbef

    invoke-static {v1}, LX/03h;->a(I)V

    .line 64058
    invoke-static {p1, p2, p4}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v1

    const v3, 0xa37adf8

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x2b98437d

    invoke-static {v1}, LX/03h;->a(I)V

    .line 64059
    const v1, 0x608ae745

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0xdda6ee2

    invoke-static {v1}, LX/03h;->a(I)V

    .line 64060
    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x4e8557d9

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x67f5bd25

    invoke-static {v0}, LX/03h;->a(I)V

    .line 64061
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0P1;LX/0sv;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;",
            "LX/0P1",
            "<",
            "LX/0U1;",
            "LX/0QK",
            "<",
            "LX/0U1;",
            "Ljava/lang/String;",
            ">;>;",
            "LX/0sv;",
            ")V"
        }
    .end annotation

    .prologue
    .line 64108
    invoke-static {p4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {p0, p1, p2, p3, v0}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0P1;LX/0Px;)V

    .line 64109
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;",
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64005
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_temp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 64006
    invoke-static {p2}, LX/0Tz;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 64007
    const-string v2, "INSERT INTO %s SELECT %s FROM %s"

    .line 64008
    invoke-static {v2, v0, v1, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 64009
    invoke-static {v2, p1, v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 64010
    invoke-static {v0, p2, p3}, LX/0Tz;->c(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v2

    const v4, -0x448a6653

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {p0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v2, -0x6a706b09

    invoke-static {v2}, LX/03h;->a(I)V

    .line 64011
    const v2, 0x32b5a30a

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p0, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v2, 0xf00a9bf

    invoke-static {v2}, LX/03h;->a(I)V

    .line 64012
    invoke-static {p1}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const v3, -0x78382a1d

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {p0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v2, 0x66129af0

    invoke-static {v2}, LX/03h;->a(I)V

    .line 64013
    invoke-static {p1, p2, p3}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v2

    const v3, 0x33184e1a

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {p0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v2, -0x30ada532

    invoke-static {v2}, LX/03h;->a(I)V

    .line 64014
    const v2, 0x1ddb8a8a

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x1dd25e9f

    invoke-static {v1}, LX/03h;->a(I)V

    .line 64015
    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x63172a68

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x3193a360

    invoke-static {v0}, LX/03h;->a(I)V

    .line 64016
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0sv;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;",
            "LX/0sv;",
            ")V"
        }
    .end annotation

    .prologue
    .line 64017
    invoke-static {p3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {p0, p1, p2, v0}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 64018
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;LX/0Px;Ljava/lang/String;)V
    .locals 6
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 64019
    invoke-virtual {p5}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0U1;

    .line 64020
    const-string v3, "DELETE FROM %s where %s NOT IN (SELECT %s FROM %s)"

    .line 64021
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v4

    .line 64022
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 64023
    invoke-static {v3, p1, v4, v0, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v3, 0x34e5fe4a

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x1947764c

    invoke-static {v0}, LX/03h;->a(I)V

    .line 64024
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 64025
    :cond_0
    new-instance v0, LX/2Qn;

    invoke-direct {v0, p5, p2, p5, p6}, LX/2Qn;-><init>(LX/0Px;Ljava/lang/String;LX/0Px;Ljava/lang/String;)V

    .line 64026
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v1, v1

    .line 64027
    new-instance v2, LX/0su;

    invoke-direct {v2, p4}, LX/0su;-><init>(LX/0Px;)V

    invoke-static {v2, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {p0, p1, p3, v1, v0}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0P1;LX/0Px;)V

    .line 64028
    return-void
.end method

.method public static b(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;",
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 64029
    const-string v0, "CREATE TABLE IF NOT EXISTS"

    invoke-static {p0, p1, p2, v0}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 64030
    const-string v0, ", "

    invoke-static {v0}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 64031
    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;",
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 64032
    const-string v0, "CREATE TEMPORARY TABLE"

    invoke-static {p0, p1, p2, v0}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 64033
    iget-object v0, p0, LX/0Tz;->a:Ljava/lang/String;

    iget-object v1, p0, LX/0Tz;->b:LX/0Px;

    iget-object v2, p0, LX/0Tz;->c:LX/0Px;

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x6b61ae3

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x463625b4

    invoke-static {v0}, LX/03h;->a(I)V

    .line 64034
    return-void
.end method

.method public a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 64035
    iget-object v0, p0, LX/0Tz;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x1daaa6fd

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x73c31d52

    invoke-static {v0}, LX/03h;->a(I)V

    .line 64036
    invoke-virtual {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 64037
    return-void
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 64038
    iget-object v0, p0, LX/0Tz;->a:Ljava/lang/String;

    iget-object v1, p0, LX/0Tz;->b:LX/0Px;

    iget-object v2, p0, LX/0Tz;->c:LX/0Px;

    invoke-static {v0, v1, v2}, LX/0Tz;->b(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x311706ed

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x7faa0b37

    invoke-static {v0}, LX/03h;->a(I)V

    .line 64039
    return-void
.end method

.method public c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 64040
    return-void
.end method

.method public d(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 64041
    return-void
.end method

.method public e(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64042
    iget-object v0, p0, LX/0Tz;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 64043
    return-void
.end method
