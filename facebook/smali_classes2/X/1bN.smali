.class public final LX/1bN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1EE;

.field public final synthetic b:LX/1aw;

.field public final synthetic c:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;LX/1EE;LX/1aw;)V
    .locals 0

    .prologue
    .line 280442
    iput-object p1, p0, LX/1bN;->c:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;

    iput-object p2, p0, LX/1bN;->a:LX/1EE;

    iput-object p3, p0, LX/1bN;->b:LX/1aw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    const/4 v2, 0x1

    const v3, -0x3327178e

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 280443
    iget-object v0, p0, LX/1bN;->c:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->i:LX/1Np;

    invoke-virtual {v0}, LX/1Np;->a()V

    .line 280444
    iget-object v0, p0, LX/1bN;->a:LX/1EE;

    invoke-virtual {v0}, LX/1EE;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1bN;->a:LX/1EE;

    invoke-virtual {v0}, LX/1EE;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280445
    iget-object v0, p0, LX/1bN;->b:LX/1aw;

    invoke-virtual {v0}, LX/1aw;->a()V

    .line 280446
    :goto_0
    const v0, 0x4b41fac2    # 1.2712642E7f

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    .line 280447
    :cond_0
    iget-object v0, p0, LX/1bN;->a:LX/1EE;

    invoke-virtual {v0}, LX/1EE;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280448
    iget-object v0, p0, LX/1bN;->c:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nq;

    iget-object v3, p0, LX/1bN;->a:LX/1EE;

    .line 280449
    iget-object v4, v3, LX/1EE;->i:Ljava/lang/String;

    move-object v3, v4

    .line 280450
    invoke-static {v3}, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;->a(Ljava/lang/String;)Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v0

    .line 280451
    :goto_1
    iget-object v3, p0, LX/1bN;->b:LX/1aw;

    const-string v4, "inlineComposerHeader"

    iget-object v5, p0, LX/1bN;->c:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;

    iget-object v5, v5, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->n:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v3, v1, v4, v0, v5}, LX/1aw;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;Landroid/app/Activity;)V

    goto :goto_0

    .line 280452
    :cond_1
    iget-object v0, p0, LX/1bN;->c:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->k:LX/0ad;

    sget-short v3, LX/1EB;->y:S

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 280453
    iget-object v0, p0, LX/1bN;->c:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nq;

    invoke-static {}, Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfig;->c()Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfig;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method
