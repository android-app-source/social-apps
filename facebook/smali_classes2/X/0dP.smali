.class public abstract LX/0dP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0yU;

.field public volatile b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/0yY;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Tn;


# direct methods
.method public constructor <init>(LX/0Tn;LX/0yU;)V
    .locals 0

    .prologue
    .line 90317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90318
    iput-object p1, p0, LX/0dP;->c:LX/0Tn;

    .line 90319
    iput-object p2, p0, LX/0dP;->a:LX/0yU;

    .line 90320
    return-void
.end method


# virtual methods
.method public a()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0yY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90311
    iget-object v0, p0, LX/0dP;->b:LX/0Rf;

    if-nez v0, :cond_0

    .line 90312
    iget-object v0, p0, LX/0dP;->a:LX/0yU;

    iget-object v1, p0, LX/0dP;->c:LX/0Tn;

    invoke-virtual {v1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0yU;->a(Ljava/lang/String;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/0dP;->b:LX/0Rf;

    .line 90313
    :cond_0
    iget-object v0, p0, LX/0dP;->b:LX/0Rf;

    return-object v0
.end method

.method public final a(LX/0yY;)Z
    .locals 1

    .prologue
    .line 90314
    sget-object v0, LX/0yY;->DIALTONE_SWITCHER_ZERO_BALANCE_REMINDER:LX/0yY;

    if-ne p1, v0, :cond_0

    .line 90315
    const/4 v0, 0x1

    .line 90316
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/0dP;->a()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 90307
    iget-object v0, p0, LX/0dP;->a:LX/0yU;

    iget-object v1, p0, LX/0dP;->c:LX/0Tn;

    invoke-virtual {v1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0yU;->a(Ljava/lang/String;)LX/0Rf;

    move-result-object v0

    .line 90308
    iget-object v1, p0, LX/0dP;->b:LX/0Rf;

    invoke-virtual {v0, v1}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 90309
    iput-object v0, p0, LX/0dP;->b:LX/0Rf;

    .line 90310
    :cond_0
    return-void
.end method
