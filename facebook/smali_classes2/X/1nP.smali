.class public LX/1nP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1nP;


# instance fields
.field public a:LX/1nQ;

.field public b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(LX/1nQ;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 316293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 316294
    iput-object p1, p0, LX/1nP;->a:LX/1nQ;

    .line 316295
    iput-object p2, p0, LX/1nP;->b:Lcom/facebook/content/SecureContextHelper;

    .line 316296
    return-void
.end method

.method public static a(LX/0QB;)LX/1nP;
    .locals 5

    .prologue
    .line 316297
    sget-object v0, LX/1nP;->c:LX/1nP;

    if-nez v0, :cond_1

    .line 316298
    const-class v1, LX/1nP;

    monitor-enter v1

    .line 316299
    :try_start_0
    sget-object v0, LX/1nP;->c:LX/1nP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 316300
    if-eqz v2, :cond_0

    .line 316301
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 316302
    new-instance p0, LX/1nP;

    invoke-static {v0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v3

    check-cast v3, LX/1nQ;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3, v4}, LX/1nP;-><init>(LX/1nQ;Lcom/facebook/content/SecureContextHelper;)V

    .line 316303
    move-object v0, p0

    .line 316304
    sput-object v0, LX/1nP;->c:LX/1nP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 316305
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 316306
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 316307
    :cond_1
    sget-object v0, LX/1nP;->c:LX/1nP;

    return-object v0

    .line 316308
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 316309
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a()Lcom/facebook/events/common/EventAnalyticsParams;
    .locals 5

    .prologue
    .line 316310
    new-instance v0, Lcom/facebook/events/common/EventActionContext;

    sget-object v1, Lcom/facebook/events/common/ActionSource;->NEWSFEED:Lcom/facebook/events/common/ActionSource;

    sget-object v2, Lcom/facebook/events/common/ActionSource;->UNKNOWN:Lcom/facebook/events/common/ActionSource;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/EventActionContext;-><init>(Lcom/facebook/events/common/ActionSource;Lcom/facebook/events/common/ActionSource;Z)V

    .line 316311
    new-instance v1, Lcom/facebook/events/common/EventAnalyticsParams;

    const-string v2, "unknown"

    const-string v3, "native_newsfeed"

    const/4 v4, 0x0

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Lcom/facebook/events/common/EventAnalyticsParams;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 316312
    new-instance v0, LX/7vc;

    invoke-direct {v0, p0, p1, p3, p2}, LX/7vc;-><init>(LX/1nP;Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)V

    return-object v0
.end method
