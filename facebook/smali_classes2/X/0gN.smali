.class public LX/0gN;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0tX;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:LX/0gX;

.field public final e:Ljava/lang/String;

.field public f:LX/0gM;

.field public g:LX/AEx;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 111922
    const-class v0, LX/0gN;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0gN;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;LX/0tX;LX/0gX;LX/0Or;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "LX/0tX;",
            "LX/0gX;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 111923
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111924
    iput-object p1, p0, LX/0gN;->c:Ljava/util/concurrent/Executor;

    .line 111925
    iput-object p2, p0, LX/0gN;->b:LX/0tX;

    .line 111926
    iput-object p3, p0, LX/0gN;->d:LX/0gX;

    .line 111927
    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/0gN;->e:Ljava/lang/String;

    .line 111928
    return-void
.end method
