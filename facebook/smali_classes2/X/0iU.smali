.class public LX/0iU;
.super LX/0iV;
.source ""


# instance fields
.field public a:LX/10Q;

.field private b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 120864
    invoke-direct {p0, p2}, LX/0iV;-><init>(Landroid/content/res/Resources;)V

    .line 120865
    sget-object v0, LX/10Q;->HIDDEN:LX/10Q;

    iput-object v0, p0, LX/0iU;->a:LX/10Q;

    .line 120866
    iput-object p1, p0, LX/0iU;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 120867
    return-void
.end method

.method public static a$redex0(LX/0iU;LX/10Q;)V
    .locals 2

    .prologue
    .line 120868
    iput-object p1, p0, LX/0iU;->a:LX/10Q;

    .line 120869
    sget-object v0, LX/7gA;->a:[I

    invoke-virtual {p1}, LX/10Q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 120870
    :cond_0
    :goto_0
    return-void

    .line 120871
    :pswitch_0
    invoke-virtual {p0}, LX/0iV;->h()V

    .line 120872
    sget-object v0, LX/10Q;->SHOWN_TABS:LX/10Q;

    invoke-direct {p0, v0}, LX/0iU;->b(LX/10Q;)Lcom/facebook/nux/ui/NuxBubbleView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0iV;->a(Lcom/facebook/nux/ui/NuxBubbleView;Lcom/facebook/apptab/state/TabTag;)V

    goto :goto_0

    .line 120873
    :pswitch_1
    invoke-virtual {p0}, LX/0iV;->h()V

    .line 120874
    invoke-virtual {p0}, LX/0iV;->g()I

    move-result v0

    if-eqz v0, :cond_0

    .line 120875
    sget-object v0, LX/10Q;->SHOWN_MORE:LX/10Q;

    invoke-direct {p0, v0}, LX/0iU;->b(LX/10Q;)Lcom/facebook/nux/ui/NuxBubbleView;

    move-result-object v0

    sget-object v1, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    invoke-virtual {p0, v0, v1}, LX/0iV;->a(Lcom/facebook/nux/ui/NuxBubbleView;Lcom/facebook/apptab/state/TabTag;)V

    .line 120876
    invoke-virtual {p0}, LX/0iU;->c()V

    goto :goto_0

    .line 120877
    :pswitch_2
    invoke-virtual {p0}, LX/0iV;->h()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b(LX/10Q;)Lcom/facebook/nux/ui/NuxBubbleView;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 120878
    const-string v1, ""

    .line 120879
    const-string v0, ""

    .line 120880
    sget-object v2, LX/7gA;->a:[I

    invoke-virtual {p1}, LX/10Q;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 120881
    :goto_0
    const/4 v2, 0x1

    const/4 v5, -0x2

    .line 120882
    iget-object v3, p0, LX/0iV;->c:Landroid/view/ViewGroup;

    if-nez v3, :cond_1

    .line 120883
    const/4 v3, 0x0

    .line 120884
    :goto_1
    move-object v0, v3

    .line 120885
    if-eqz v0, :cond_0

    .line 120886
    new-instance v1, LX/7gB;

    invoke-direct {v1, p0}, LX/7gB;-><init>(LX/0iU;)V

    invoke-virtual {v0, v1}, Lcom/facebook/nux/ui/NuxBubbleView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 120887
    :cond_0
    return-object v0

    .line 120888
    :pswitch_0
    iget-object v0, p0, LX/0iV;->e:Landroid/content/res/Resources;

    move-object v0, v0

    .line 120889
    const v1, 0x7f080a22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 120890
    iget-object v0, p0, LX/0iV;->e:Landroid/content/res/Resources;

    move-object v0, v0

    .line 120891
    const v2, 0x7f080a21

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 120892
    :pswitch_1
    iget-object v0, p0, LX/0iV;->e:Landroid/content/res/Resources;

    move-object v0, v0

    .line 120893
    const v1, 0x7f080a24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 120894
    iget-object v0, p0, LX/0iV;->e:Landroid/content/res/Resources;

    move-object v0, v0

    .line 120895
    const v2, 0x7f080a23

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 120896
    :cond_1
    new-instance v3, Lcom/facebook/nux/ui/NuxBubbleView;

    iget-object v4, p0, LX/0iV;->c:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/nux/ui/NuxBubbleView;-><init>(Landroid/content/Context;)V

    .line 120897
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 120898
    invoke-virtual {v3, v4}, Lcom/facebook/nux/ui/NuxBubbleView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 120899
    iget-object v5, p0, LX/0iV;->e:Landroid/content/res/Resources;

    const p1, 0x7f0b0409

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, v4, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 120900
    invoke-virtual {v3, v2}, Lcom/facebook/nux/ui/NuxBubbleView;->setNubPosition(I)V

    .line 120901
    if-eqz v1, :cond_2

    .line 120902
    invoke-virtual {v3, v1}, Lcom/facebook/nux/ui/NuxBubbleView;->setBubbleTitle(Ljava/lang/String;)V

    .line 120903
    :cond_2
    invoke-virtual {v3, v0}, Lcom/facebook/nux/ui/NuxBubbleView;->setBubbleBody(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final c()V
    .locals 3

    .prologue
    .line 120904
    iget-object v0, p0, LX/0iU;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/10R;->d:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 120905
    return-void
.end method
