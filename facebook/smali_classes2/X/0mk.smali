.class public LX/0mk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:LX/0me;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/analytics2/logger/Uploader;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0mI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:LX/0mI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/flexiblesampling/SamplingPolicyConfig;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/analytics2/logger/HandlerThreadFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0mm;

.field private final i:LX/0mM;

.field private final j:LX/0Zh;

.field private final k:LX/0ma;

.field private final l:LX/0mg;

.field private final m:LX/0mg;

.field private final n:LX/0me;

.field private final o:LX/0mU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final p:LX/40D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:LX/0pE;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private r:LX/0pE;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private s:LX/0q8;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 133005
    new-instance v0, LX/0ml;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/0ml;-><init>(I)V

    sput-object v0, LX/0mk;->a:LX/0me;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;LX/0mI;LX/0mI;Ljava/lang/Class;Ljava/lang/Class;LX/0mm;LX/0mM;LX/0Zh;LX/0ma;LX/0mg;LX/0mg;LX/0me;LX/0mU;LX/40D;)V
    .locals 0
    .param p3    # LX/0mI;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0mI;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Class;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p14    # LX/0mU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p15    # LX/40D;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/analytics2/logger/Uploader;",
            ">;",
            "LX/0mI;",
            "LX/0mI;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/flexiblesampling/SamplingPolicyConfig;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/analytics2/logger/HandlerThreadFactory;",
            ">;",
            "LX/0mm;",
            "LX/0mM;",
            "LX/0Zh;",
            "Lcom/facebook/analytics2/logger/AppBackgroundedProvider;",
            "LX/0mg;",
            "LX/0mg;",
            "LX/0me;",
            "Lcom/facebook/analytics2/logger/BeginWritingBlock;",
            "Lcom/facebook/analytics2/logger/Analytics2EventSchemaValidationManager;",
            ")V"
        }
    .end annotation

    .prologue
    .line 133006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133007
    iput-object p1, p0, LX/0mk;->b:Landroid/content/Context;

    .line 133008
    iput-object p2, p0, LX/0mk;->c:Ljava/lang/Class;

    .line 133009
    iput-object p3, p0, LX/0mk;->d:LX/0mI;

    .line 133010
    iput-object p4, p0, LX/0mk;->e:LX/0mI;

    .line 133011
    iput-object p5, p0, LX/0mk;->f:Ljava/lang/Class;

    .line 133012
    iput-object p6, p0, LX/0mk;->g:Ljava/lang/Class;

    .line 133013
    iput-object p7, p0, LX/0mk;->h:LX/0mm;

    .line 133014
    iput-object p8, p0, LX/0mk;->i:LX/0mM;

    .line 133015
    iput-object p9, p0, LX/0mk;->j:LX/0Zh;

    .line 133016
    iput-object p10, p0, LX/0mk;->k:LX/0ma;

    .line 133017
    iput-object p11, p0, LX/0mk;->l:LX/0mg;

    .line 133018
    iput-object p12, p0, LX/0mk;->m:LX/0mg;

    .line 133019
    iput-object p13, p0, LX/0mk;->n:LX/0me;

    .line 133020
    iput-object p14, p0, LX/0mk;->o:LX/0mU;

    .line 133021
    iput-object p15, p0, LX/0mk;->p:LX/40D;

    .line 133022
    return-void
.end method

.method private a(Ljava/lang/String;I)Landroid/os/HandlerThread;
    .locals 2

    .prologue
    .line 133023
    iget-object v0, p0, LX/0mk;->b:Landroid/content/Context;

    invoke-static {v0}, LX/0pF;->a(Landroid/content/Context;)LX/0pF;

    move-result-object v0

    iget-object v1, p0, LX/0mk;->g:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0pF;->b(Ljava/lang/String;)Lcom/facebook/analytics2/logger/HandlerThreadFactory;

    move-result-object v0

    .line 133024
    invoke-interface {v0, p1, p2}, Lcom/facebook/analytics2/logger/HandlerThreadFactory;->a(Ljava/lang/String;I)Landroid/os/HandlerThread;

    move-result-object v0

    .line 133025
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 133026
    return-object v0
.end method

.method private declared-synchronized e()LX/0q8;
    .locals 2
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 133027
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0mk;->s:LX/0q8;

    if-nez v0, :cond_0

    .line 133028
    new-instance v0, LX/0q8;

    iget-object v1, p0, LX/0mk;->i:LX/0mM;

    invoke-direct {v0, v1}, LX/0q8;-><init>(LX/0mM;)V

    iput-object v0, p0, LX/0mk;->s:LX/0q8;

    .line 133029
    :cond_0
    iget-object v0, p0, LX/0mk;->s:LX/0q8;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 133030
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()LX/0pE;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 133031
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0mk;->q:LX/0pE;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/0pE;
    .locals 17
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 133032
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/0mk;->q:LX/0pE;

    if-nez v1, :cond_0

    .line 133033
    new-instance v1, LX/0pC;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0mk;->c:Ljava/lang/Class;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0mk;->f:Ljava/lang/Class;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0mk;->g:Ljava/lang/Class;

    sget-object v5, LX/0pD;->NORMAL:LX/0pD;

    const-string v6, "regular"

    invoke-direct/range {v1 .. v6}, LX/0pC;-><init>(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;LX/0pD;Ljava/lang/String;)V

    .line 133034
    new-instance v14, LX/0pE;

    const-string v2, "Analytics-NormalPri-Proc"

    const/16 v3, 0xa

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, LX/0mk;->a(Ljava/lang/String;I)Landroid/os/HandlerThread;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0mk;->d:LX/0mI;

    move-object/from16 v16, v0

    new-instance v2, LX/0q6;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0mk;->b:Landroid/content/Context;

    const v4, 0x7f0d000c

    const-string v5, "normal"

    move-object/from16 v0, p0

    iget-object v6, v0, LX/0mk;->n:LX/0me;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/0mk;->n:LX/0me;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/0mk;->h:LX/0mm;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/0mk;->j:LX/0Zh;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/0mk;->k:LX/0ma;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/0mk;->g:Ljava/lang/Class;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/0mk;->l:LX/0mg;

    move-object v10, v1

    invoke-direct/range {v2 .. v13}, LX/0q6;-><init>(Landroid/content/Context;ILjava/lang/String;LX/0me;LX/0me;LX/0mm;LX/0Zh;LX/0pC;LX/0ma;Ljava/lang/Class;LX/0mg;)V

    move-object/from16 v0, p0

    iget-object v8, v0, LX/0mk;->o:LX/0mU;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/0mk;->p:LX/40D;

    move-object v3, v14

    move-object v4, v15

    move-object/from16 v5, v16

    move-object v6, v2

    move-object/from16 v7, p0

    invoke-direct/range {v3 .. v9}, LX/0pE;-><init>(Landroid/os/HandlerThread;LX/0mI;LX/0q6;LX/0mk;LX/0mU;LX/40D;)V

    move-object/from16 v0, p0

    iput-object v14, v0, LX/0mk;->q:LX/0pE;

    .line 133035
    move-object/from16 v0, p0

    iget-object v1, v0, LX/0mk;->q:LX/0pE;

    invoke-direct/range {p0 .. p0}, LX/0mk;->e()LX/0q8;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0pE;->a(LX/0q8;)V

    .line 133036
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/0mk;->q:LX/0pE;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 133037
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final declared-synchronized c()LX/0pE;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 133038
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0mk;->r:LX/0pE;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()LX/0pE;
    .locals 17
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 133039
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/0mk;->r:LX/0pE;

    if-nez v1, :cond_0

    .line 133040
    new-instance v1, LX/0pC;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/0mk;->c:Ljava/lang/Class;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0mk;->f:Ljava/lang/Class;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0mk;->g:Ljava/lang/Class;

    sget-object v5, LX/0pD;->HIGH:LX/0pD;

    const-string v6, "ads"

    invoke-direct/range {v1 .. v6}, LX/0pC;-><init>(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;LX/0pD;Ljava/lang/String;)V

    .line 133041
    new-instance v14, LX/0pE;

    const-string v2, "Analytics-HighPri-Proc"

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, LX/0mk;->a(Ljava/lang/String;I)Landroid/os/HandlerThread;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0mk;->e:LX/0mI;

    move-object/from16 v16, v0

    new-instance v2, LX/0q6;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/0mk;->b:Landroid/content/Context;

    const v4, 0x7f0d000d

    const-string v5, "high"

    sget-object v6, LX/0mk;->a:LX/0me;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/0mk;->n:LX/0me;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/0mk;->h:LX/0mm;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/0mk;->j:LX/0Zh;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/0mk;->k:LX/0ma;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/0mk;->g:Ljava/lang/Class;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/0mk;->m:LX/0mg;

    move-object v10, v1

    invoke-direct/range {v2 .. v13}, LX/0q6;-><init>(Landroid/content/Context;ILjava/lang/String;LX/0me;LX/0me;LX/0mm;LX/0Zh;LX/0pC;LX/0ma;Ljava/lang/Class;LX/0mg;)V

    move-object/from16 v0, p0

    iget-object v8, v0, LX/0mk;->o:LX/0mU;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/0mk;->p:LX/40D;

    move-object v3, v14

    move-object v4, v15

    move-object/from16 v5, v16

    move-object v6, v2

    move-object/from16 v7, p0

    invoke-direct/range {v3 .. v9}, LX/0pE;-><init>(Landroid/os/HandlerThread;LX/0mI;LX/0q6;LX/0mk;LX/0mU;LX/40D;)V

    move-object/from16 v0, p0

    iput-object v14, v0, LX/0mk;->r:LX/0pE;

    .line 133042
    move-object/from16 v0, p0

    iget-object v1, v0, LX/0mk;->r:LX/0pE;

    invoke-direct/range {p0 .. p0}, LX/0mk;->e()LX/0q8;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0pE;->a(LX/0q8;)V

    .line 133043
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/0mk;->r:LX/0pE;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 133044
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
