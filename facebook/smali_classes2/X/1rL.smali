.class public final LX/1rL;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "LX/1rM;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/1rL;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1rM;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 332042
    invoke-direct {p0, p1}, LX/0aT;-><init>(LX/0Ot;)V

    .line 332043
    return-void
.end method

.method public static a(LX/0QB;)LX/1rL;
    .locals 4

    .prologue
    .line 332044
    sget-object v0, LX/1rL;->a:LX/1rL;

    if-nez v0, :cond_1

    .line 332045
    const-class v1, LX/1rL;

    monitor-enter v1

    .line 332046
    :try_start_0
    sget-object v0, LX/1rL;->a:LX/1rL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 332047
    if-eqz v2, :cond_0

    .line 332048
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 332049
    new-instance v3, LX/1rL;

    const/16 p0, 0x13fe

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1rL;-><init>(LX/0Ot;)V

    .line 332050
    move-object v0, v3

    .line 332051
    sput-object v0, LX/1rL;->a:LX/1rL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 332052
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 332053
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 332054
    :cond_1
    sget-object v0, LX/1rL;->a:LX/1rL;

    return-object v0

    .line 332055
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 332056
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 332057
    check-cast p3, LX/1rM;

    .line 332058
    invoke-virtual {p3}, LX/1rM;->c()V

    .line 332059
    return-void
.end method
