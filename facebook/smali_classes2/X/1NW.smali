.class public LX/1NW;
.super LX/1NX;
.source ""


# instance fields
.field private a:Lcom/facebook/feed/fragment/NewsFeedFragment;

.field private b:LX/189;

.field private c:LX/0bH;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/fragment/NewsFeedFragment;LX/0bH;LX/189;)V
    .locals 0

    .prologue
    .line 237454
    invoke-direct {p0}, LX/1NX;-><init>()V

    .line 237455
    iput-object p1, p0, LX/1NW;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    .line 237456
    iput-object p2, p0, LX/1NW;->c:LX/0bH;

    .line 237457
    iput-object p3, p0, LX/1NW;->b:LX/189;

    .line 237458
    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 14

    .prologue
    .line 237459
    check-cast p1, LX/1Ni;

    .line 237460
    iget-object v0, p0, LX/1NW;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v0

    iget-object v1, p1, LX/1Ni;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0fz;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 237461
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 237462
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 237463
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    if-eqz v2, :cond_4

    .line 237464
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    .line 237465
    iget-object v2, p0, LX/1NW;->b:LX/189;

    iget-object v3, p1, LX/1Ni;->b:Ljava/lang/String;

    .line 237466
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->I_()I

    move-result v5

    .line 237467
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 237468
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->x()LX/0Px;

    move-result-object v8

    .line 237469
    const/4 v4, 0x0

    move v6, v5

    move v5, v4

    :goto_1
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_3

    .line 237470
    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;

    .line 237471
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v9

    if-eqz v9, :cond_2

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 237472
    if-ltz v5, :cond_1

    if-ge v5, v6, :cond_1

    .line 237473
    add-int/lit8 v6, v6, -0x1

    .line 237474
    :cond_1
    :goto_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    .line 237475
    :cond_2
    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 237476
    :cond_3
    new-instance v11, LX/4Xe;

    invoke-direct {v11}, LX/4Xe;-><init>()V

    .line 237477
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 237478
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->g()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v11, LX/4Xe;->b:Ljava/lang/String;

    .line 237479
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->E_()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v11, LX/4Xe;->c:Ljava/lang/String;

    .line 237480
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->F_()J

    move-result-wide v12

    iput-wide v12, v11, LX/4Xe;->d:J

    .line 237481
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->r()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v11, LX/4Xe;->e:Ljava/lang/String;

    .line 237482
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->s()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v11, LX/4Xe;->f:Ljava/lang/String;

    .line 237483
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->u()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v11, LX/4Xe;->g:Ljava/lang/String;

    .line 237484
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->w()I

    move-result v10

    iput v10, v11, LX/4Xe;->h:I

    .line 237485
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->x()LX/0Px;

    move-result-object v10

    iput-object v10, v11, LX/4Xe;->i:LX/0Px;

    .line 237486
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLPageBrowserCategoryInfo;

    move-result-object v10

    iput-object v10, v11, LX/4Xe;->j:Lcom/facebook/graphql/model/GraphQLPageBrowserCategoryInfo;

    .line 237487
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v10

    iput-object v10, v11, LX/4Xe;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 237488
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->A()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v11, LX/4Xe;->l:Ljava/lang/String;

    .line 237489
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v10

    iput-object v10, v11, LX/4Xe;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 237490
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->c()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v11, LX/4Xe;->n:Ljava/lang/String;

    .line 237491
    invoke-static {v11, v0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 237492
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->L_()LX/0x2;

    move-result-object v10

    invoke-virtual {v10}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/0x2;

    iput-object v10, v11, LX/4Xe;->o:LX/0x2;

    .line 237493
    move-object v4, v11

    .line 237494
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 237495
    iput-object v5, v4, LX/4Xe;->i:LX/0Px;

    .line 237496
    move-object v4, v4

    .line 237497
    iget-object v5, v2, LX/189;->i:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v8

    .line 237498
    iput-wide v8, v4, LX/4Xe;->d:J

    .line 237499
    move-object v4, v4

    .line 237500
    new-instance v5, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    invoke-direct {v5, v4}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;-><init>(LX/4Xe;)V

    .line 237501
    move-object v4, v5

    .line 237502
    invoke-static {v4, v6}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 237503
    invoke-static {v0}, LX/18M;->d(Lcom/facebook/graphql/model/Sponsorable;)I

    move-result v5

    invoke-static {v4, v5}, LX/18M;->a(Lcom/facebook/graphql/model/Sponsorable;I)V

    .line 237504
    invoke-static {v4}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;)Z

    move-result v5

    if-eqz v5, :cond_6

    :goto_3
    move-object v0, v4

    .line 237505
    iget-object v2, p0, LX/1NW;->c:LX/0bH;

    new-instance v3, LX/1Ne;

    invoke-direct {v3, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    goto/16 :goto_0

    .line 237506
    :cond_4
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    if-eqz v2, :cond_0

    .line 237507
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    .line 237508
    iget-object v2, p0, LX/1NW;->b:LX/189;

    iget-object v3, p1, LX/1Ni;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    move-result-object v0

    .line 237509
    iget-object v2, p0, LX/1NW;->c:LX/0bH;

    new-instance v3, LX/1Ne;

    invoke-direct {v3, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    goto/16 :goto_0

    .line 237510
    :cond_5
    return-void

    :cond_6
    const/4 v4, 0x0

    goto :goto_3
.end method
