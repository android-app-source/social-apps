.class public LX/0aQ;
.super LX/0a6;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/01U;LX/0Sk;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 84750
    invoke-virtual {p2}, LX/01U;->getPermission()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, LX/0a6;-><init>(Landroid/content/Context;Ljava/lang/String;LX/0Sk;)V

    .line 84751
    return-void
.end method

.method public static a(LX/0QB;)LX/0aQ;
    .locals 6

    .prologue
    .line 84752
    const-class v1, LX/0aQ;

    monitor-enter v1

    .line 84753
    :try_start_0
    sget-object v0, LX/0aQ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 84754
    sput-object v2, LX/0aQ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 84755
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84756
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 84757
    new-instance p0, LX/0aQ;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0XV;->b(LX/0QB;)LX/01U;

    move-result-object v4

    check-cast v4, LX/01U;

    invoke-static {v0}, LX/0Sk;->a(LX/0QB;)LX/0Sk;

    move-result-object v5

    check-cast v5, LX/0Sk;

    invoke-direct {p0, v3, v4, v5}, LX/0aQ;-><init>(Landroid/content/Context;LX/01U;LX/0Sk;)V

    .line 84758
    move-object v0, p0

    .line 84759
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 84760
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/0aQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84761
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 84762
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
