.class public LX/0V1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0V1;


# instance fields
.field private final a:LX/0V2;

.field private final b:LX/0V3;

.field private c:Ljava/lang/Thread$UncaughtExceptionHandler;


# direct methods
.method public constructor <init>(LX/0V2;LX/0V3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 67252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67253
    iput-object p2, p0, LX/0V1;->b:LX/0V3;

    .line 67254
    iput-object p1, p0, LX/0V1;->a:LX/0V2;

    .line 67255
    return-void
.end method

.method public static a(LX/0QB;)LX/0V1;
    .locals 5

    .prologue
    .line 67256
    sget-object v0, LX/0V1;->d:LX/0V1;

    if-nez v0, :cond_1

    .line 67257
    const-class v1, LX/0V1;

    monitor-enter v1

    .line 67258
    :try_start_0
    sget-object v0, LX/0V1;->d:LX/0V1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 67259
    if-eqz v2, :cond_0

    .line 67260
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 67261
    new-instance p0, LX/0V1;

    invoke-static {v0}, LX/0V2;->a(LX/0QB;)LX/0V2;

    move-result-object v3

    check-cast v3, LX/0V2;

    invoke-static {v0}, LX/0V3;->a(LX/0QB;)LX/0V3;

    move-result-object v4

    check-cast v4, LX/0V3;

    invoke-direct {p0, v3, v4}, LX/0V1;-><init>(LX/0V2;LX/0V3;)V

    .line 67262
    move-object v0, p0

    .line 67263
    sput-object v0, LX/0V1;->d:LX/0V1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67264
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 67265
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 67266
    :cond_1
    sget-object v0, LX/0V1;->d:LX/0V1;

    return-object v0

    .line 67267
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 67268
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 1

    .prologue
    .line 67269
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iput-object v0, p0, LX/0V1;->c:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 67270
    invoke-static {p0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 67271
    return-void
.end method

.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 67272
    iget-object v0, p0, LX/0V1;->a:LX/0V2;

    .line 67273
    iget-object v1, v0, LX/0V2;->a:LX/0Uh;

    const/16 v2, 0xa0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 67274
    if-nez v0, :cond_0

    instance-of v0, p2, Ljava/lang/OutOfMemoryError;

    if-eqz v0, :cond_1

    .line 67275
    :cond_0
    iget-object v0, p0, LX/0V1;->b:LX/0V3;

    .line 67276
    :try_start_0
    invoke-static {p2}, LX/1Bz;->getRootCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 67277
    iget-object v2, v0, LX/0V3;->a:LX/0V2;

    invoke-virtual {v2}, LX/0V2;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 67278
    invoke-static {v0, v1}, LX/0V3;->b(LX/0V3;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 67279
    :cond_1
    :goto_0
    iget-object v0, p0, LX/0V1;->c:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v0, :cond_2

    .line 67280
    iget-object v0, p0, LX/0V1;->c:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 67281
    :cond_2
    return-void

    .line 67282
    :cond_3
    :try_start_1
    iget-object v2, v0, LX/0V3;->a:LX/0V2;

    .line 67283
    iget-object v3, v2, LX/0V2;->a:LX/0Uh;

    const/16 v4, 0x9f

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    move v2, v3

    .line 67284
    if-eqz v2, :cond_1

    .line 67285
    invoke-static {}, LX/0VP;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0V3;->a(LX/0V3;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 67286
    goto :goto_0

    .line 67287
    :catch_0
    move-exception v1

    .line 67288
    const-string v2, "MemoryDumper"

    const-string v3, "Error writing Hprof dump"

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 67289
    iget-object v2, v0, LX/0V3;->e:LX/03V;

    const-string v3, "hprof"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed - "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
