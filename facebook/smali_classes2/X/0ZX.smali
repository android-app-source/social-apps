.class public LX/0ZX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/0ZX;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile h:LX/0ZX;


# instance fields
.field public final b:LX/0SG;

.field private final c:LX/0Zb;

.field private final d:LX/0aG;

.field private final e:LX/0VT;

.field public final f:LX/0WP;

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83429
    const-class v0, LX/0ZX;

    sput-object v0, LX/0ZX;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/0Zb;LX/0aG;LX/0VT;LX/0WP;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 83421
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83422
    iput-object p1, p0, LX/0ZX;->b:LX/0SG;

    .line 83423
    iput-object p2, p0, LX/0ZX;->c:LX/0Zb;

    .line 83424
    iput-object p3, p0, LX/0ZX;->d:LX/0aG;

    .line 83425
    iput-object p4, p0, LX/0ZX;->e:LX/0VT;

    .line 83426
    iput-object p5, p0, LX/0ZX;->f:LX/0WP;

    .line 83427
    const/4 v0, 0x0

    iput-object v0, p0, LX/0ZX;->g:Ljava/util/Map;

    .line 83428
    return-void
.end method

.method public static a(LX/0QB;)LX/0ZX;
    .locals 9

    .prologue
    .line 83408
    sget-object v0, LX/0ZX;->h:LX/0ZX;

    if-nez v0, :cond_1

    .line 83409
    const-class v1, LX/0ZX;

    monitor-enter v1

    .line 83410
    :try_start_0
    sget-object v0, LX/0ZX;->h:LX/0ZX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 83411
    if-eqz v2, :cond_0

    .line 83412
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 83413
    new-instance v3, LX/0ZX;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v6

    check-cast v6, LX/0aG;

    invoke-static {v0}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v7

    check-cast v7, LX/0VT;

    invoke-static {v0}, LX/0WL;->a(LX/0QB;)LX/0WP;

    move-result-object v8

    check-cast v8, LX/0WP;

    invoke-direct/range {v3 .. v8}, LX/0ZX;-><init>(LX/0SG;LX/0Zb;LX/0aG;LX/0VT;LX/0WP;)V

    .line 83414
    move-object v0, v3

    .line 83415
    sput-object v0, LX/0ZX;->h:LX/0ZX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83416
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 83417
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 83418
    :cond_1
    sget-object v0, LX/0ZX;->h:LX/0ZX;

    return-object v0

    .line 83419
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 83420
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0ZX;Ljava/lang/String;Ljava/lang/String;LX/0of;Ljava/lang/String;LX/0lF;LX/1gX;)V
    .locals 7
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 83366
    sget-object v0, LX/0of;->MARAUDER:LX/0of;

    if-ne p3, v0, :cond_1

    .line 83367
    iget-object v6, p0, LX/0ZX;->c:LX/0Zb;

    new-instance v0, Lcom/facebook/abtest/qe/analytics/QuickExperimentAnalyticsEvent;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p6

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/abtest/qe/analytics/QuickExperimentAnalyticsEvent;-><init>(Ljava/lang/String;Ljava/lang/String;LX/1gX;Ljava/lang/String;LX/0lF;)V

    invoke-interface {v6, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 83368
    :cond_0
    :goto_0
    return-void

    .line 83369
    :cond_1
    sget-object v0, LX/0of;->QUICK_EXPERIMENT:LX/0of;

    if-ne p3, v0, :cond_0

    .line 83370
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 83371
    new-instance v1, LX/3yb;

    invoke-direct {v1}, LX/3yb;-><init>()V

    .line 83372
    invoke-virtual {p6}, LX/1gX;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 83373
    iput-object v2, v1, LX/3yb;->c:Ljava/lang/String;

    .line 83374
    move-object v2, v1

    .line 83375
    iput-object p1, v2, LX/3yb;->a:Ljava/lang/String;

    .line 83376
    move-object v2, v2

    .line 83377
    iput-object p4, v2, LX/3yb;->d:Ljava/lang/String;

    .line 83378
    move-object v2, v2

    .line 83379
    if-eqz p5, :cond_2

    .line 83380
    invoke-virtual {p5}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, LX/3yb;->e:Ljava/lang/String;

    .line 83381
    :cond_2
    const-string v2, "experiment_logging_params"

    invoke-virtual {v1}, LX/3yb;->a()Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 83382
    iget-object v1, p0, LX/0ZX;->d:LX/0aG;

    const-string v2, "log_to_qe"

    const v3, -0x272fa180

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    goto :goto_0
.end method

.method public static a(JJ)Z
    .locals 4

    .prologue
    .line 83406
    sub-long v0, p2, p0

    .line 83407
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    const-wide/32 v2, 0xdbba00

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0ZX;Ljava/lang/String;)Z
    .locals 9

    .prologue
    .line 83396
    iget-object v0, p0, LX/0ZX;->e:LX/0VT;

    invoke-virtual {v0}, LX/0VT;->a()LX/00G;

    move-result-object v0

    invoke-virtual {v0}, LX/00G;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83397
    const-wide/16 v7, 0x0

    .line 83398
    iget-object v1, p0, LX/0ZX;->f:LX/0WP;

    const-string v2, "QuickExperimentLogger-LastLogTimestamps"

    invoke-virtual {v1, v2}, LX/0WP;->a(Ljava/lang/String;)LX/0WS;

    move-result-object v1

    .line 83399
    iget-object v2, p0, LX/0ZX;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v3

    .line 83400
    invoke-virtual {v1, p1, v7, v8}, LX/0WS;->a(Ljava/lang/String;J)J

    move-result-wide v5

    .line 83401
    cmp-long v2, v5, v7

    if-eqz v2, :cond_0

    invoke-static {v5, v6, v3, v4}, LX/0ZX;->a(JJ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 83402
    :cond_0
    invoke-virtual {v1}, LX/0WS;->b()LX/1gW;

    move-result-object v1

    invoke-interface {v1, p1, v3, v4}, LX/1gW;->a(Ljava/lang/String;J)LX/1gW;

    move-result-object v1

    invoke-interface {v1}, LX/1gW;->c()V

    .line 83403
    const/4 v1, 0x1

    .line 83404
    :goto_0
    move v0, v1

    .line 83405
    :goto_1
    return v0

    :cond_1
    invoke-direct {p0, p1}, LX/0ZX;->c(Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private declared-synchronized c(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 83387
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0ZX;->g:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 83388
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0ZX;->g:Ljava/util/Map;

    .line 83389
    :cond_0
    iget-object v0, p0, LX/0ZX;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 83390
    iget-object v0, p0, LX/0ZX;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 83391
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1, v2, v3}, LX/0ZX;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 83392
    :cond_1
    iget-object v0, p0, LX/0ZX;->g:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83393
    const/4 v0, 0x1

    .line 83394
    :goto_0
    monitor-exit p0

    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 83395
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(ZLjava/lang/String;Ljava/lang/String;LX/0of;Ljava/lang/String;LX/0lF;)V
    .locals 7
    .param p6    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 83383
    if-nez p1, :cond_1

    .line 83384
    :cond_0
    :goto_0
    return-void

    .line 83385
    :cond_1
    invoke-static {p0, p2}, LX/0ZX;->a(LX/0ZX;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83386
    sget-object v6, LX/1gX;->EXPOSURE:LX/1gX;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-static/range {v0 .. v6}, LX/0ZX;->a(LX/0ZX;Ljava/lang/String;Ljava/lang/String;LX/0of;Ljava/lang/String;LX/0lF;LX/1gX;)V

    goto :goto_0
.end method
