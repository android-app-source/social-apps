.class public final LX/12A;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/Object;

.field public b:LX/1pL;

.field public final c:Z

.field public final d:LX/12B;

.field public e:[B

.field public f:[B

.field public g:[B

.field public h:[C

.field public i:[C

.field public j:[C


# direct methods
.method public constructor <init>(LX/12B;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 174096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174097
    iput-object v0, p0, LX/12A;->e:[B

    .line 174098
    iput-object v0, p0, LX/12A;->f:[B

    .line 174099
    iput-object v0, p0, LX/12A;->g:[B

    .line 174100
    iput-object v0, p0, LX/12A;->h:[C

    .line 174101
    iput-object v0, p0, LX/12A;->i:[C

    .line 174102
    iput-object v0, p0, LX/12A;->j:[C

    .line 174103
    iput-object p1, p0, LX/12A;->d:LX/12B;

    .line 174104
    iput-object p2, p0, LX/12A;->a:Ljava/lang/Object;

    .line 174105
    iput-boolean p3, p0, LX/12A;->c:Z

    .line 174106
    return-void
.end method

.method private static a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 174146
    if-eqz p0, :cond_0

    .line 174147
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to call same allocXxx() method second time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174148
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 174143
    if-eq p0, p1, :cond_0

    .line 174144
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Trying to release buffer not owned by the context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174145
    :cond_0
    return-void
.end method


# virtual methods
.method public final a([B)V
    .locals 2

    .prologue
    .line 174138
    if-eqz p1, :cond_0

    .line 174139
    iget-object v0, p0, LX/12A;->e:[B

    invoke-static {p1, v0}, LX/12A;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 174140
    const/4 v0, 0x0

    iput-object v0, p0, LX/12A;->e:[B

    .line 174141
    iget-object v0, p0, LX/12A;->d:LX/12B;

    sget-object v1, LX/12C;->READ_IO_BUFFER:LX/12C;

    invoke-virtual {v0, v1, p1}, LX/12B;->a(LX/12C;[B)V

    .line 174142
    :cond_0
    return-void
.end method

.method public final a([C)V
    .locals 2

    .prologue
    .line 174133
    if-eqz p1, :cond_0

    .line 174134
    iget-object v0, p0, LX/12A;->h:[C

    invoke-static {p1, v0}, LX/12A;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 174135
    const/4 v0, 0x0

    iput-object v0, p0, LX/12A;->h:[C

    .line 174136
    iget-object v0, p0, LX/12A;->d:LX/12B;

    sget-object v1, LX/12D;->TOKEN_BUFFER:LX/12D;

    invoke-virtual {v0, v1, p1}, LX/12B;->a(LX/12D;[C)V

    .line 174137
    :cond_0
    return-void
.end method

.method public final a(I)[C
    .locals 2

    .prologue
    .line 174131
    iget-object v0, p0, LX/12A;->j:[C

    invoke-static {v0}, LX/12A;->a(Ljava/lang/Object;)V

    .line 174132
    iget-object v0, p0, LX/12A;->d:LX/12B;

    sget-object v1, LX/12D;->NAME_COPY_BUFFER:LX/12D;

    invoke-virtual {v0, v1, p1}, LX/12B;->a(LX/12D;I)[C

    move-result-object v0

    iput-object v0, p0, LX/12A;->j:[C

    return-object v0
.end method

.method public final b([B)V
    .locals 2

    .prologue
    .line 174126
    if-eqz p1, :cond_0

    .line 174127
    iget-object v0, p0, LX/12A;->f:[B

    invoke-static {p1, v0}, LX/12A;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 174128
    const/4 v0, 0x0

    iput-object v0, p0, LX/12A;->f:[B

    .line 174129
    iget-object v0, p0, LX/12A;->d:LX/12B;

    sget-object v1, LX/12C;->WRITE_ENCODING_BUFFER:LX/12C;

    invoke-virtual {v0, v1, p1}, LX/12B;->a(LX/12C;[B)V

    .line 174130
    :cond_0
    return-void
.end method

.method public final b([C)V
    .locals 2

    .prologue
    .line 174121
    if-eqz p1, :cond_0

    .line 174122
    iget-object v0, p0, LX/12A;->i:[C

    invoke-static {p1, v0}, LX/12A;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 174123
    const/4 v0, 0x0

    iput-object v0, p0, LX/12A;->i:[C

    .line 174124
    iget-object v0, p0, LX/12A;->d:LX/12B;

    sget-object v1, LX/12D;->CONCAT_BUFFER:LX/12D;

    invoke-virtual {v0, v1, p1}, LX/12B;->a(LX/12D;[C)V

    .line 174125
    :cond_0
    return-void
.end method

.method public final c([C)V
    .locals 2

    .prologue
    .line 174116
    if-eqz p1, :cond_0

    .line 174117
    iget-object v0, p0, LX/12A;->j:[C

    invoke-static {p1, v0}, LX/12A;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 174118
    const/4 v0, 0x0

    iput-object v0, p0, LX/12A;->j:[C

    .line 174119
    iget-object v0, p0, LX/12A;->d:LX/12B;

    sget-object v1, LX/12D;->NAME_COPY_BUFFER:LX/12D;

    invoke-virtual {v0, v1, p1}, LX/12B;->a(LX/12D;[C)V

    .line 174120
    :cond_0
    return-void
.end method

.method public final d()LX/15x;
    .locals 2

    .prologue
    .line 174115
    new-instance v0, LX/15x;

    iget-object v1, p0, LX/12A;->d:LX/12B;

    invoke-direct {v0, v1}, LX/15x;-><init>(LX/12B;)V

    return-object v0
.end method

.method public final e()[B
    .locals 2

    .prologue
    .line 174113
    iget-object v0, p0, LX/12A;->e:[B

    invoke-static {v0}, LX/12A;->a(Ljava/lang/Object;)V

    .line 174114
    iget-object v0, p0, LX/12A;->d:LX/12B;

    sget-object v1, LX/12C;->READ_IO_BUFFER:LX/12C;

    invoke-virtual {v0, v1}, LX/12B;->a(LX/12C;)[B

    move-result-object v0

    iput-object v0, p0, LX/12A;->e:[B

    return-object v0
.end method

.method public final f()[B
    .locals 2

    .prologue
    .line 174111
    iget-object v0, p0, LX/12A;->f:[B

    invoke-static {v0}, LX/12A;->a(Ljava/lang/Object;)V

    .line 174112
    iget-object v0, p0, LX/12A;->d:LX/12B;

    sget-object v1, LX/12C;->WRITE_ENCODING_BUFFER:LX/12C;

    invoke-virtual {v0, v1}, LX/12B;->a(LX/12C;)[B

    move-result-object v0

    iput-object v0, p0, LX/12A;->f:[B

    return-object v0
.end method

.method public final g()[C
    .locals 2

    .prologue
    .line 174109
    iget-object v0, p0, LX/12A;->h:[C

    invoke-static {v0}, LX/12A;->a(Ljava/lang/Object;)V

    .line 174110
    iget-object v0, p0, LX/12A;->d:LX/12B;

    sget-object v1, LX/12D;->TOKEN_BUFFER:LX/12D;

    invoke-virtual {v0, v1}, LX/12B;->a(LX/12D;)[C

    move-result-object v0

    iput-object v0, p0, LX/12A;->h:[C

    return-object v0
.end method

.method public final h()[C
    .locals 2

    .prologue
    .line 174107
    iget-object v0, p0, LX/12A;->i:[C

    invoke-static {v0}, LX/12A;->a(Ljava/lang/Object;)V

    .line 174108
    iget-object v0, p0, LX/12A;->d:LX/12B;

    sget-object v1, LX/12D;->CONCAT_BUFFER:LX/12D;

    invoke-virtual {v0, v1}, LX/12B;->a(LX/12D;)[C

    move-result-object v0

    iput-object v0, p0, LX/12A;->i:[C

    return-object v0
.end method
