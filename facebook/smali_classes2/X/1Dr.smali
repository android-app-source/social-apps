.class public LX/1Dr;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 218954
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(J)I
    .locals 4

    .prologue
    .line 218955
    const-wide/16 v0, -0x1

    const/16 v2, 0x20

    shr-long v2, p0, v2

    and-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public static a(FF)J
    .locals 2

    .prologue
    .line 218956
    float-to-int v0, p0

    float-to-int v1, p1

    invoke-static {v0, v1}, LX/1Dr;->a(II)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(II)J
    .locals 4

    .prologue
    .line 218957
    int-to-long v0, p0

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    int-to-long v2, p1

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public static b(J)I
    .locals 2

    .prologue
    .line 218958
    const-wide/16 v0, -0x1

    and-long/2addr v0, p0

    long-to-int v0, v0

    return v0
.end method
