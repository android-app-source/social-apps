.class public LX/1eo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/24e;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field private b:LX/1LD;

.field public c:Landroid/view/View;

.field private d:Lcom/facebook/attachments/photos/ui/PostPostBadge;

.field public e:LX/24m;

.field public f:LX/24m;

.field private g:LX/1L1;

.field public h:I


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View;Lcom/facebook/attachments/photos/ui/PostPostBadge;LX/1L1;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/attachments/photos/ui/PostPostBadge;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "Landroid/view/View;",
            "Lcom/facebook/attachments/photos/ui/PostPostBadge;",
            "LX/1L1;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 289011
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289012
    const/4 v0, -0x1

    iput v0, p0, LX/1eo;->h:I

    .line 289013
    iput-object p1, p0, LX/1eo;->a:Landroid/view/View;

    .line 289014
    iput-object p2, p0, LX/1eo;->c:Landroid/view/View;

    .line 289015
    iput-object p3, p0, LX/1eo;->d:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    .line 289016
    iput-object p4, p0, LX/1eo;->g:LX/1L1;

    .line 289017
    invoke-direct {p0}, LX/1eo;->a()V

    .line 289018
    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 289008
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289009
    const/4 v0, -0x1

    .line 289010
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 288992
    iget-object v0, p0, LX/1eo;->c:Landroid/view/View;

    invoke-direct {p0}, LX/1eo;->b()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 288993
    iget-object v0, p0, LX/1eo;->d:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    invoke-direct {p0}, LX/1eo;->c()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 288994
    new-instance v0, LX/24i;

    invoke-direct {v0, p0}, LX/24i;-><init>(LX/1eo;)V

    iput-object v0, p0, LX/1eo;->b:LX/1LD;

    .line 288995
    iget-object v0, p0, LX/1eo;->g:LX/1L1;

    iget-object v1, p0, LX/1eo;->b:LX/1LD;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 288996
    return-void
.end method

.method private b()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 289007
    new-instance v0, LX/24g;

    invoke-direct {v0, p0}, LX/24g;-><init>(LX/1eo;)V

    return-object v0
.end method

.method private c()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 289006
    new-instance v0, LX/24h;

    invoke-direct {v0, p0}, LX/24h;-><init>(LX/1eo;)V

    return-object v0
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 289001
    if-nez p1, :cond_0

    .line 289002
    iget-object v0, p0, LX/1eo;->d:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->setVisibility(I)V

    .line 289003
    :goto_0
    return-void

    .line 289004
    :cond_0
    iget-object v0, p0, LX/1eo;->d:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->a(II)V

    .line 289005
    iget-object v0, p0, LX/1eo;->d:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, -0x1

    .line 288997
    invoke-static {p1}, LX/1eo;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/1eo;->h:I

    .line 288998
    iget v0, p0, LX/1eo;->h:I

    if-ne v0, v2, :cond_0

    .line 288999
    :goto_0
    return-void

    .line 289000
    :cond_0
    iget-object v1, p0, LX/1eo;->c:Landroid/view/View;

    if-eq p2, v2, :cond_1

    iget v0, p0, LX/1eo;->h:I

    if-eq p2, v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    goto :goto_1
.end method
