.class public LX/1p4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/1p4;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1p6;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Xl;

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/zero/rewrite/ZeroMqttRewriteListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Or;LX/0Xl;Ljava/util/Set;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
        .end annotation
    .end param
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1p6;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Xl;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/zero/rewrite/ZeroMqttRewriteListener;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 327761
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 327762
    iput-object p1, p0, LX/1p4;->a:LX/0Ot;

    .line 327763
    iput-object p2, p0, LX/1p4;->b:LX/0Or;

    .line 327764
    iput-object p3, p0, LX/1p4;->c:LX/0Xl;

    .line 327765
    iput-object p4, p0, LX/1p4;->d:Ljava/util/Set;

    .line 327766
    return-void
.end method

.method public static a(LX/0QB;)LX/1p4;
    .locals 9

    .prologue
    .line 327769
    sget-object v0, LX/1p4;->e:LX/1p4;

    if-nez v0, :cond_1

    .line 327770
    const-class v1, LX/1p4;

    monitor-enter v1

    .line 327771
    :try_start_0
    sget-object v0, LX/1p4;->e:LX/1p4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 327772
    if-eqz v2, :cond_0

    .line 327773
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 327774
    new-instance v4, LX/1p4;

    const/16 v3, 0xe03

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x14d1

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v3

    check-cast v3, LX/0Xl;

    .line 327775
    new-instance v7, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v8

    new-instance p0, LX/1p5;

    invoke-direct {p0, v0}, LX/1p5;-><init>(LX/0QB;)V

    invoke-direct {v7, v8, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v7, v7

    .line 327776
    invoke-direct {v4, v5, v6, v3, v7}, LX/1p4;-><init>(LX/0Ot;LX/0Or;LX/0Xl;Ljava/util/Set;)V

    .line 327777
    move-object v0, v4

    .line 327778
    sput-object v0, LX/1p4;->e:LX/1p4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 327779
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 327780
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 327781
    :cond_1
    sget-object v0, LX/1p4;->e:LX/1p4;

    return-object v0

    .line 327782
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 327783
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1p4;)V
    .locals 2

    .prologue
    .line 327784
    iget-object v0, p0, LX/1p4;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1p6;

    invoke-virtual {v0}, LX/1p6;->d()Ljava/lang/String;

    .line 327785
    invoke-static {p0}, LX/1p4;->b(LX/1p4;)V

    .line 327786
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.rti.mqtt.ACTION_ZR_SWITCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 327787
    iget-object v1, p0, LX/1p4;->c:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 327788
    return-void
.end method

.method public static b(LX/1p4;)V
    .locals 2

    .prologue
    .line 327767
    iget-object v0, p0, LX/1p4;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 327768
    :cond_0
    return-void
.end method
