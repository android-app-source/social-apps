.class public LX/1Oy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/view/View;

.field private b:Landroid/view/ViewParent;

.field public c:Z

.field private d:[I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 242727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242728
    iput-object p1, p0, LX/1Oy;->a:Landroid/view/View;

    .line 242729
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 242730
    iget-boolean v0, p0, LX/1Oy;->c:Z

    if-eqz v0, :cond_0

    .line 242731
    iget-object v0, p0, LX/1Oy;->a:Landroid/view/View;

    .line 242732
    sget-object v1, LX/0vv;->a:LX/0w2;

    invoke-interface {v1, v0}, LX/0w2;->E(Landroid/view/View;)V

    .line 242733
    :cond_0
    iput-boolean p1, p0, LX/1Oy;->c:Z

    .line 242734
    return-void
.end method

.method public final a(FF)Z
    .locals 2

    .prologue
    .line 242735
    iget-boolean v0, p0, LX/1Oy;->c:Z

    move v0, v0

    .line 242736
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Oy;->b:Landroid/view/ViewParent;

    if-eqz v0, :cond_0

    .line 242737
    iget-object v0, p0, LX/1Oy;->b:Landroid/view/ViewParent;

    iget-object v1, p0, LX/1Oy;->a:Landroid/view/View;

    .line 242738
    sget-object p0, LX/3B6;->a:LX/3B9;

    invoke-interface {p0, v0, v1, p1, p2}, LX/3B9;->a(Landroid/view/ViewParent;Landroid/view/View;FF)Z

    move-result p0

    move v0, p0

    .line 242739
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(FFZ)Z
    .locals 8

    .prologue
    .line 242658
    iget-boolean v0, p0, LX/1Oy;->c:Z

    move v0, v0

    .line 242659
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Oy;->b:Landroid/view/ViewParent;

    if-eqz v0, :cond_0

    .line 242660
    iget-object v0, p0, LX/1Oy;->b:Landroid/view/ViewParent;

    iget-object v1, p0, LX/1Oy;->a:Landroid/view/View;

    .line 242661
    sget-object v2, LX/3B6;->a:LX/3B9;

    move-object v3, v0

    move-object v4, v1

    move v5, p1

    move v6, p2

    move v7, p3

    invoke-interface/range {v2 .. v7}, LX/3B9;->a(Landroid/view/ViewParent;Landroid/view/View;FFZ)Z

    move-result v2

    move v0, v2

    .line 242662
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 242708
    invoke-virtual {p0}, LX/1Oy;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 242709
    :goto_0
    return v0

    .line 242710
    :cond_0
    iget-boolean v0, p0, LX/1Oy;->c:Z

    move v0, v0

    .line 242711
    if-eqz v0, :cond_3

    .line 242712
    iget-object v0, p0, LX/1Oy;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 242713
    iget-object v0, p0, LX/1Oy;->a:Landroid/view/View;

    .line 242714
    :goto_1
    if-eqz v1, :cond_3

    .line 242715
    iget-object v3, p0, LX/1Oy;->a:Landroid/view/View;

    .line 242716
    sget-object v4, LX/3B6;->a:LX/3B9;

    invoke-interface {v4, v1, v0, v3, p1}, LX/3B9;->a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)Z

    move-result v4

    move v3, v4

    .line 242717
    if-eqz v3, :cond_1

    .line 242718
    iput-object v1, p0, LX/1Oy;->b:Landroid/view/ViewParent;

    .line 242719
    iget-object v3, p0, LX/1Oy;->a:Landroid/view/View;

    .line 242720
    sget-object v4, LX/3B6;->a:LX/3B9;

    invoke-interface {v4, v1, v0, v3, p1}, LX/3B9;->b(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)V

    .line 242721
    move v0, v2

    .line 242722
    goto :goto_0

    .line 242723
    :cond_1
    instance-of v3, v1, Landroid/view/View;

    if-eqz v3, :cond_2

    move-object v0, v1

    .line 242724
    check-cast v0, Landroid/view/View;

    .line 242725
    :cond_2
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_1

    .line 242726
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(IIII[I)Z
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 242692
    iget-boolean v0, p0, LX/1Oy;->c:Z

    move v0, v0

    .line 242693
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1Oy;->b:Landroid/view/ViewParent;

    if-eqz v0, :cond_2

    .line 242694
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    if-nez p3, :cond_0

    if-eqz p4, :cond_3

    .line 242695
    :cond_0
    if-eqz p5, :cond_4

    .line 242696
    iget-object v0, p0, LX/1Oy;->a:Landroid/view/View;

    invoke-virtual {v0, p5}, Landroid/view/View;->getLocationInWindow([I)V

    .line 242697
    aget v1, p5, v7

    .line 242698
    aget v0, p5, v9

    move v6, v0

    move v8, v1

    .line 242699
    :goto_0
    iget-object v0, p0, LX/1Oy;->b:Landroid/view/ViewParent;

    iget-object v1, p0, LX/1Oy;->a:Landroid/view/View;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, LX/3B6;->a(Landroid/view/ViewParent;Landroid/view/View;IIII)V

    .line 242700
    if-eqz p5, :cond_1

    .line 242701
    iget-object v0, p0, LX/1Oy;->a:Landroid/view/View;

    invoke-virtual {v0, p5}, Landroid/view/View;->getLocationInWindow([I)V

    .line 242702
    aget v0, p5, v7

    sub-int/2addr v0, v8

    aput v0, p5, v7

    .line 242703
    aget v0, p5, v9

    sub-int/2addr v0, v6

    aput v0, p5, v9

    :cond_1
    move v7, v9

    .line 242704
    :cond_2
    :goto_1
    return v7

    .line 242705
    :cond_3
    if-eqz p5, :cond_2

    .line 242706
    aput v7, p5, v7

    .line 242707
    aput v7, p5, v9

    goto :goto_1

    :cond_4
    move v6, v7

    move v8, v7

    goto :goto_0
.end method

.method public final a(II[I[I)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 242669
    iget-boolean v2, p0, LX/1Oy;->c:Z

    move v2, v2

    .line 242670
    if-eqz v2, :cond_5

    iget-object v2, p0, LX/1Oy;->b:Landroid/view/ViewParent;

    if-eqz v2, :cond_5

    .line 242671
    if-nez p1, :cond_0

    if-eqz p2, :cond_6

    .line 242672
    :cond_0
    if-eqz p4, :cond_7

    .line 242673
    iget-object v2, p0, LX/1Oy;->a:Landroid/view/View;

    invoke-virtual {v2, p4}, Landroid/view/View;->getLocationInWindow([I)V

    .line 242674
    aget v3, p4, v0

    .line 242675
    aget v2, p4, v1

    .line 242676
    :goto_0
    if-nez p3, :cond_2

    .line 242677
    iget-object v4, p0, LX/1Oy;->d:[I

    if-nez v4, :cond_1

    .line 242678
    const/4 v4, 0x2

    new-array v4, v4, [I

    iput-object v4, p0, LX/1Oy;->d:[I

    .line 242679
    :cond_1
    iget-object p3, p0, LX/1Oy;->d:[I

    .line 242680
    :cond_2
    aput v0, p3, v0

    .line 242681
    aput v0, p3, v1

    .line 242682
    iget-object v4, p0, LX/1Oy;->b:Landroid/view/ViewParent;

    iget-object v5, p0, LX/1Oy;->a:Landroid/view/View;

    invoke-static {v4, v5, p1, p2, p3}, LX/3B6;->a(Landroid/view/ViewParent;Landroid/view/View;II[I)V

    .line 242683
    if-eqz p4, :cond_3

    .line 242684
    iget-object v4, p0, LX/1Oy;->a:Landroid/view/View;

    invoke-virtual {v4, p4}, Landroid/view/View;->getLocationInWindow([I)V

    .line 242685
    aget v4, p4, v0

    sub-int v3, v4, v3

    aput v3, p4, v0

    .line 242686
    aget v3, p4, v1

    sub-int v2, v3, v2

    aput v2, p4, v1

    .line 242687
    :cond_3
    aget v2, p3, v0

    if-nez v2, :cond_4

    aget v2, p3, v1

    if-eqz v2, :cond_5

    :cond_4
    move v0, v1

    .line 242688
    :cond_5
    :goto_1
    return v0

    .line 242689
    :cond_6
    if-eqz p4, :cond_5

    .line 242690
    aput v0, p4, v0

    .line 242691
    aput v0, p4, v1

    goto :goto_1

    :cond_7
    move v2, v0

    move v3, v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 242668
    iget-object v0, p0, LX/1Oy;->b:Landroid/view/ViewParent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 242663
    iget-object v0, p0, LX/1Oy;->b:Landroid/view/ViewParent;

    if-eqz v0, :cond_0

    .line 242664
    iget-object v0, p0, LX/1Oy;->b:Landroid/view/ViewParent;

    iget-object v1, p0, LX/1Oy;->a:Landroid/view/View;

    .line 242665
    sget-object v2, LX/3B6;->a:LX/3B9;

    invoke-interface {v2, v0, v1}, LX/3B9;->a(Landroid/view/ViewParent;Landroid/view/View;)V

    .line 242666
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Oy;->b:Landroid/view/ViewParent;

    .line 242667
    :cond_0
    return-void
.end method
