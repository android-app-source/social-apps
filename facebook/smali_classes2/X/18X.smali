.class public LX/18X;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/18X;


# instance fields
.field private a:LX/18Y;

.field private b:LX/18a;


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 2
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/ShouldEnableJsonParserSkipChildrenDepthFix;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 206652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206653
    new-instance v1, LX/18Y;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v1, v0}, LX/18Y;-><init>(Z)V

    iput-object v1, p0, LX/18X;->a:LX/18Y;

    .line 206654
    new-instance v0, LX/18a;

    iget-object v1, p0, LX/18X;->a:LX/18Y;

    invoke-direct {v0, v1}, LX/18a;-><init>(LX/18Y;)V

    iput-object v0, p0, LX/18X;->b:LX/18a;

    .line 206655
    return-void
.end method

.method public static a(LX/0QB;)LX/18X;
    .locals 4

    .prologue
    .line 206635
    sget-object v0, LX/18X;->c:LX/18X;

    if-nez v0, :cond_1

    .line 206636
    const-class v1, LX/18X;

    monitor-enter v1

    .line 206637
    :try_start_0
    sget-object v0, LX/18X;->c:LX/18X;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 206638
    if-eqz v2, :cond_0

    .line 206639
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 206640
    new-instance v3, LX/18X;

    const/16 p0, 0x14c7

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/18X;-><init>(LX/0Or;)V

    .line 206641
    move-object v0, v3

    .line 206642
    sput-object v0, LX/18X;->c:LX/18X;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 206643
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 206644
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 206645
    :cond_1
    sget-object v0, LX/18X;->c:LX/18X;

    return-object v0

    .line 206646
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 206647
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/2VL;)LX/18Z;
    .locals 2

    .prologue
    .line 206648
    sget-object v0, LX/2Vl;->a:[I

    invoke-virtual {p1}, LX/2VL;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 206649
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No batch controller exists for supplied type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206650
    :pswitch_0
    iget-object v0, p0, LX/18X;->a:LX/18Y;

    .line 206651
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/18X;->b:LX/18a;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
