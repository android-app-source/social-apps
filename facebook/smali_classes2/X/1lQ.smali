.class public LX/1lQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 311874
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(J)J
    .locals 2

    .prologue
    .line 311873
    const-wide/32 v0, 0x36ee80

    div-long v0, p0, v0

    return-wide v0
.end method

.method public static a(JJ)J
    .locals 4

    .prologue
    .line 311862
    long-to-double v0, p0

    long-to-double v2, p2

    div-double/2addr v0, v2

    .line 311863
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-long v0, v0

    return-wide v0
.end method

.method private static b(JJ)J
    .locals 4

    .prologue
    .line 311876
    long-to-double v0, p0

    long-to-double v2, p2

    div-double/2addr v0, v2

    .line 311877
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    return-wide v0
.end method

.method public static c(J)J
    .locals 2

    .prologue
    .line 311875
    const-wide/32 v0, 0x36ee80

    invoke-static {p0, p1, v0, v1}, LX/1lQ;->b(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public static d(J)J
    .locals 2

    .prologue
    .line 311878
    const-wide/32 v0, 0xea60

    div-long v0, p0, v0

    return-wide v0
.end method

.method public static f(J)J
    .locals 2

    .prologue
    .line 311871
    const-wide/32 v0, 0x5265c00

    div-long v0, p0, v0

    return-wide v0
.end method

.method public static h(J)J
    .locals 2

    .prologue
    .line 311872
    const-wide/32 v0, 0x5265c00

    invoke-static {p0, p1, v0, v1}, LX/1lQ;->b(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public static k(J)J
    .locals 2

    .prologue
    .line 311870
    const-wide v0, 0x757b12c00L

    div-long v0, p0, v0

    return-wide v0
.end method

.method public static m(J)J
    .locals 2

    .prologue
    .line 311869
    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    return-wide v0
.end method

.method public static n(J)D
    .locals 4

    .prologue
    .line 311868
    long-to-double v0, p0

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public static o(J)J
    .locals 2

    .prologue
    .line 311867
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p0

    return-wide v0
.end method

.method public static p(J)J
    .locals 2

    .prologue
    .line 311866
    const-wide/32 v0, 0xea60

    mul-long/2addr v0, p0

    return-wide v0
.end method

.method public static q(J)J
    .locals 4

    .prologue
    .line 311865
    const-wide/32 v0, 0x7a120

    add-long/2addr v0, p0

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public static r(J)J
    .locals 4

    .prologue
    .line 311864
    const-wide/16 v0, 0x1f4

    add-long/2addr v0, p0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method
