.class public LX/1lJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 311452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/nio/ByteBuffer;)I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/16 v2, 0xc

    .line 311436
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    if-eq v0, v1, :cond_0

    .line 311437
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Forgot to set byte order to LITTLE_ENDIAN"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 311438
    :cond_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    .line 311439
    if-ge v0, v2, :cond_1

    .line 311440
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Model file of length %d is too short to hold a model"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 311441
    :cond_1
    new-array v1, v2, [B

    .line 311442
    add-int/lit8 v0, v0, -0xc

    .line 311443
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 311444
    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 311445
    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 311446
    aget-byte v0, v1, v4

    const/16 v2, 0x70

    if-ne v0, v2, :cond_2

    aget-byte v0, v1, v3

    const/16 v2, 0x6f

    if-ne v0, v2, :cond_2

    const/4 v0, 0x2

    aget-byte v0, v1, v0

    const/16 v2, 0x73

    if-ne v0, v2, :cond_2

    const/4 v0, 0x3

    aget-byte v0, v1, v0

    const/16 v2, 0x3d

    if-eq v0, v2, :cond_3

    .line 311447
    :cond_2
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Model file is missing footer"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 311448
    :cond_3
    const/4 v0, 0x4

    const/16 v2, 0xc

    :try_start_0
    invoke-static {v1, v0, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    .line 311449
    new-instance v1, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    const/16 v0, 0x10

    invoke-static {v1, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 311450
    :catch_0
    move-exception v0

    .line 311451
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Metadata position field is in invalid format"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Ljava/nio/ByteBuffer;LX/0w5;)V
    .locals 6
    .param p1    # LX/0w5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            "LX/0w5",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 311426
    invoke-static {p0}, LX/1lJ;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 311427
    if-eqz p1, :cond_0

    if-nez v0, :cond_1

    .line 311428
    :cond_0
    return-void

    .line 311429
    :cond_1
    new-instance v1, LX/1lK;

    invoke-direct {v1, p0, v0}, LX/1lK;-><init>(Ljava/nio/ByteBuffer;I)V

    .line 311430
    iget-object v0, v1, LX/1lK;->a:Ljava/nio/ByteBuffer;

    iget v2, v1, LX/1lK;->b:I

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 311431
    invoke-virtual {p1}, LX/0w5;->b()Ljava/lang/String;

    move-result-object v2

    .line 311432
    iget-object v3, v1, LX/1lK;->a:Ljava/nio/ByteBuffer;

    iget v4, v1, LX/1lK;->b:I

    const/4 v5, 0x1

    const/4 p0, 0x0

    invoke-static {v3, v4, v5, p0}, LX/0ah;->a(Ljava/nio/ByteBuffer;III)I

    move-result v3

    move v1, v3

    .line 311433
    invoke-virtual {p1}, LX/0w5;->a()Ljava/lang/Integer;

    move-result-object v3

    .line 311434
    invoke-static {v2, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v4, v1, :cond_0

    .line 311435
    :cond_2
    new-instance v4, LX/4VR;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v4, v0, v1, v2, v3}, LX/4VR;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    throw v4
.end method

.method public static a(ILX/0w5;J)[B
    .locals 6
    .param p1    # LX/0w5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0w5",
            "<*>;J)[B"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 311416
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 311417
    if-eqz p1, :cond_1

    .line 311418
    invoke-static {p1, p2, p3}, LX/1lK;->a(LX/0w5;J)[B

    move-result-object v0

    .line 311419
    invoke-virtual {v3, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 311420
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    add-int/2addr v0, p0

    .line 311421
    :goto_0
    const-string v4, "pos=%08X"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 311422
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 311423
    array-length v4, v0

    const/16 v5, 0xc

    if-ne v4, v5, :cond_0

    move v1, v2

    :cond_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 311424
    invoke-virtual {v3, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 311425
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
