.class public LX/1YQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1YR;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pi;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "LX/1YR",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static final c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static k:LX/0Xm;


# instance fields
.field public final d:LX/0Sh;

.field private final e:LX/0iA;

.field public final f:LX/1YS;

.field private final g:LX/0tQ;

.field private h:LX/1Pi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public i:Ljava/util/Timer;

.field public j:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 273517
    const-class v0, LX/1YQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1YQ;->a:Ljava/lang/String;

    .line 273518
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_CARET_AUTOPLAY_VIDEO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/1YQ;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 273519
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_CARET_VIDEO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/1YQ;->c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/0tQ;LX/0iA;LX/1YS;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 273520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273521
    iput-object p1, p0, LX/1YQ;->d:LX/0Sh;

    .line 273522
    iput-object p3, p0, LX/1YQ;->e:LX/0iA;

    .line 273523
    iput-object p4, p0, LX/1YQ;->f:LX/1YS;

    .line 273524
    iput-object p2, p0, LX/1YQ;->g:LX/0tQ;

    .line 273525
    return-void
.end method

.method public static a(LX/0QB;)LX/1YQ;
    .locals 7

    .prologue
    .line 273526
    const-class v1, LX/1YQ;

    monitor-enter v1

    .line 273527
    :try_start_0
    sget-object v0, LX/1YQ;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 273528
    sput-object v2, LX/1YQ;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 273529
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273530
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 273531
    new-instance p0, LX/1YQ;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v4

    check-cast v4, LX/0tQ;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v5

    check-cast v5, LX/0iA;

    const-class v6, LX/1YS;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/1YS;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1YQ;-><init>(LX/0Sh;LX/0tQ;LX/0iA;LX/1YS;)V

    .line 273532
    move-object v0, p0

    .line 273533
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 273534
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1YQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 273535
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 273536
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1PW;)V
    .locals 0

    .prologue
    .line 273537
    check-cast p1, LX/1Pi;

    .line 273538
    iput-object p1, p0, LX/1YQ;->h:LX/1Pi;

    .line 273539
    return-void
.end method

.method public final a(LX/2oO;Z)V
    .locals 4

    .prologue
    .line 273540
    iget-object v0, p0, LX/1YQ;->h:LX/1Pi;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1YQ;->g:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1YQ;->g:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->f()Z

    move-result v0

    if-nez v0, :cond_2

    if-nez p2, :cond_0

    iget-object v0, p0, LX/1YQ;->g:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 273541
    :cond_0
    iget-object v0, p0, LX/1YQ;->h:LX/1Pi;

    .line 273542
    if-eqz p1, :cond_1

    .line 273543
    iget-object v1, p1, LX/2oO;->A:Ljava/lang/String;

    move-object v1, v1

    .line 273544
    iput-object v1, p0, LX/1YQ;->j:Ljava/lang/String;

    .line 273545
    :cond_1
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, LX/1YQ;->i:Ljava/util/Timer;

    .line 273546
    iget-object v2, p0, LX/1YQ;->i:Ljava/util/Timer;

    new-instance v3, Lcom/facebook/feedplugins/saved/nux/DownloadToFacebookTooltipTrigger$1;

    invoke-direct {v3, p0, p1, v0}, Lcom/facebook/feedplugins/saved/nux/DownloadToFacebookTooltipTrigger$1;-><init>(LX/1YQ;LX/2oO;LX/1Pi;)V

    if-eqz p2, :cond_3

    const-wide/16 v0, 0xbb8

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 273547
    :cond_2
    return-void

    .line 273548
    :cond_3
    const-wide/16 v0, 0x1f4

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 273549
    const/4 v0, 0x0

    iput-object v0, p0, LX/1YQ;->h:LX/1Pi;

    .line 273550
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 273551
    iget-object v0, p0, LX/1YQ;->g:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1YQ;->g:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 273552
    :cond_0
    const/4 v0, 0x0

    .line 273553
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/1YQ;->e:LX/0iA;

    iget-object v0, p0, LX/1YQ;->g:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/1YQ;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    :goto_1
    invoke-virtual {v1, v0}, LX/0iA;->b(Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v0

    goto :goto_0

    :cond_2
    sget-object v0, LX/1YQ;->c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    goto :goto_1
.end method
