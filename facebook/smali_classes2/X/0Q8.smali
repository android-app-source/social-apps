.class public LX/0Q8;
.super LX/0Q9;
.source ""

# interfaces
.implements LX/0QD;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0RI;",
            "LX/0RN;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0RI;",
            "LX/4fR;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0S6;

.field private final e:LX/0R2;

.field public final f:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/AssistedProvider",
            "<*>;>;",
            "LX/0Wm",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;",
            "Lcom/facebook/inject/Binder;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/LibraryModule;",
            ">;>;"
        }
    .end annotation
.end field

.field private final i:Z

.field private final j:Z

.field private final k:I

.field private l:Z

.field private final m:LX/0S7;

.field private final n:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "LX/0S7;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0R4;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;LX/0R2;LX/014;Z)V
    .locals 6
    .param p4    # LX/014;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<+",
            "LX/0Q4;",
            ">;",
            "LX/0R2;",
            "LX/014;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 57860
    invoke-direct {p0}, LX/0Q9;-><init>()V

    .line 57861
    new-instance v2, LX/0R3;

    invoke-direct {v2, p0}, LX/0R3;-><init>(LX/0Q8;)V

    iput-object v2, p0, LX/0Q8;->n:Ljava/lang/ThreadLocal;

    .line 57862
    new-instance v2, LX/0R4;

    invoke-direct {v2, p0, p0}, LX/0R4;-><init>(LX/0Q8;LX/0Q8;)V

    iput-object v2, p0, LX/0Q8;->o:LX/0R4;

    .line 57863
    const-string v2, "FbInjectorImpl.init"

    const v3, -0x68c92592

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 57864
    :try_start_0
    iput-object p1, p0, LX/0Q8;->a:Landroid/content/Context;

    .line 57865
    iput-object p3, p0, LX/0Q8;->e:LX/0R2;

    .line 57866
    sget-boolean v2, LX/007;->i:Z

    move v2, v2

    .line 57867
    iput-boolean v2, p0, LX/0Q8;->i:Z

    .line 57868
    iget-object v2, p0, LX/0Q8;->e:LX/0R2;

    .line 57869
    iget-boolean v3, v2, LX/0R2;->a:Z

    move v2, v3

    .line 57870
    iput-boolean v2, p0, LX/0Q8;->j:Z

    .line 57871
    if-nez p4, :cond_0

    .line 57872
    const/4 v2, 0x0

    iput v2, p0, LX/0Q8;->k:I

    .line 57873
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    if-ne p1, v2, :cond_1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 57874
    new-instance v0, LX/0R8;

    iget-boolean v4, p0, LX/0Q8;->i:Z

    iget-object v5, p0, LX/0Q8;->e:LX/0R2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, LX/0R8;-><init>(LX/0QA;Landroid/content/Context;Ljava/util/List;ZLX/0R2;)V

    .line 57875
    invoke-virtual {v0}, LX/0R8;->a()LX/0S5;

    move-result-object v1

    .line 57876
    iget-object v2, v1, LX/0S5;->c:Ljava/util/Map;

    iput-object v2, p0, LX/0Q8;->b:Ljava/util/Map;

    .line 57877
    iget-object v2, v1, LX/0S5;->d:Ljava/util/Map;

    iput-object v2, p0, LX/0Q8;->c:Ljava/util/Map;

    .line 57878
    new-instance v2, LX/0S6;

    invoke-direct {v2, p0, p1}, LX/0S6;-><init>(LX/0QA;Landroid/content/Context;)V

    iput-object v2, p0, LX/0Q8;->d:LX/0S6;

    .line 57879
    new-instance v2, LX/0S7;

    iget-object v3, p0, LX/0Q8;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/0S7;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, LX/0Q8;->m:LX/0S7;

    .line 57880
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v2

    iput-object v2, p0, LX/0Q8;->f:Ljava/util/concurrent/ConcurrentMap;

    .line 57881
    if-eqz p5, :cond_2

    .line 57882
    iget-object v2, v1, LX/0S5;->a:Ljava/util/Map;

    iput-object v2, p0, LX/0Q8;->g:Ljava/util/Map;

    .line 57883
    iget-object v1, v1, LX/0S5;->e:Ljava/util/List;

    iput-object v1, p0, LX/0Q8;->h:Ljava/util/List;

    .line 57884
    :goto_2
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/0Q8;->l:Z

    .line 57885
    invoke-virtual {v0}, LX/0R8;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57886
    const v0, -0x7f7d8912

    invoke-static {v0}, LX/02m;->a(I)V

    .line 57887
    return-void

    .line 57888
    :cond_0
    :try_start_1
    invoke-virtual {p4}, LX/014;->c()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 57889
    :try_start_2
    const/4 v3, 0x0

    move v3, v3

    .line 57890
    iput v3, p0, LX/0Q8;->k:I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 57891
    :catch_0
    move-exception v0

    .line 57892
    :try_start_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to get injector process id for process name: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 57893
    :catchall_0
    move-exception v0

    const v1, -0x72b1fb94

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_1
    move v0, v1

    .line 57894
    goto :goto_1

    .line 57895
    :cond_2
    :try_start_4
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v1, v1

    .line 57896
    iput-object v1, p0, LX/0Q8;->g:Ljava/util/Map;

    .line 57897
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 57898
    iput-object v1, p0, LX/0Q8;->h:Ljava/util/List;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2
.end method

.method public static a(LX/0QB;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0QB;",
            "I)TT;"
        }
    .end annotation

    .prologue
    .line 57857
    const/16 v0, 0x165e

    if-ge p1, v0, :cond_0

    .line 57858
    invoke-static {p0, p1}, LX/0Vn;->a(LX/0QB;I)Ljava/lang/Object;

    move-result-object v0

    .line 57859
    :goto_0
    return-object v0

    :cond_0
    add-int/lit16 v0, p1, -0x165e

    invoke-static {p0, v0}, LX/0ei;->a(LX/0QB;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/0Q8;)V
    .locals 2

    .prologue
    .line 57854
    iget-boolean v0, p0, LX/0Q8;->l:Z

    if-nez v0, :cond_0

    .line 57855
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Called injector during binding"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57856
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 57837
    const-string v0, "FbInjectorImpl.injectComponent"

    const v1, 0x347809da

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 57838
    :try_start_0
    invoke-static {p0}, LX/0Q8;->b(LX/0Q8;)V

    .line 57839
    invoke-static {p1}, LX/0RI;->a(Ljava/lang/Class;)LX/0RI;

    move-result-object v1

    .line 57840
    iget-boolean v0, p0, LX/0Q8;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/0Q8;->j:Z

    if-eqz v0, :cond_1

    .line 57841
    :cond_0
    sget-object v0, LX/4fo;->INJECT_COMPONENT:LX/4fo;

    invoke-static {v0, v1}, LX/4fp;->a(LX/4fo;LX/0RI;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 57842
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/0Q8;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4fR;

    .line 57843
    if-nez v0, :cond_4

    .line 57844
    new-instance v0, LX/4fr;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No provider bound for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 57845
    :catchall_0
    move-exception v0

    :try_start_2
    iget-boolean v1, p0, LX/0Q8;->i:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, LX/0Q8;->j:Z

    if-eqz v1, :cond_3

    .line 57846
    :cond_2
    invoke-static {}, LX/4fp;->a()V

    :cond_3
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 57847
    :catchall_1
    move-exception v0

    const v1, 0x21ac117a

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 57848
    :cond_4
    :try_start_3
    iget-object v1, v0, LX/4fR;->c:LX/4B4;

    move-object v0, v1

    .line 57849
    invoke-virtual {v0, p2}, LX/4B4;->inject(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 57850
    :try_start_4
    iget-boolean v0, p0, LX/0Q8;->i:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, LX/0Q8;->j:Z

    if-eqz v0, :cond_6

    .line 57851
    :cond_5
    invoke-static {}, LX/4fp;->a()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 57852
    :cond_6
    const v0, 0xafbfa6

    invoke-static {v0}, LX/02m;->a(I)V

    .line 57853
    return-void
.end method

.method public final a(LX/0RI;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0RI",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 57836
    iget-object v0, p0, LX/0Q8;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final getApplicationInjector()LX/0QA;
    .locals 1

    .prologue
    .line 57832
    iget-object v0, p0, LX/0Q8;->d:LX/0S6;

    return-object v0
.end method

.method public final getBinders()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;",
            "Lcom/facebook/inject/Binder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57831
    iget-object v0, p0, LX/0Q8;->g:Ljava/util/Map;

    return-object v0
.end method

.method public final getInjectorThreadStack()LX/0S7;
    .locals 2

    .prologue
    .line 57833
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 57834
    iget-object v0, p0, LX/0Q8;->m:LX/0S7;

    .line 57835
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0Q8;->n:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S7;

    goto :goto_0
.end method

.method public final getInstance(LX/0RI;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 57830
    invoke-virtual {p0, p1}, LX/0Q9;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getLazy(LX/0RI;)LX/0Ot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Ot",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57829
    invoke-virtual {p0, p1}, LX/0Q9;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    invoke-virtual {p0}, LX/0Q8;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public final getModuleInjector(Ljava/lang/Class;)LX/0QA;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;)",
            "LX/0QA;"
        }
    .end annotation

    .prologue
    .line 57826
    iget-boolean v0, p0, LX/0Q8;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/0Q8;->j:Z

    if-eqz v0, :cond_1

    .line 57827
    :cond_0
    new-instance v0, LX/4fu;

    iget-object v1, p0, LX/0Q8;->e:LX/0R2;

    invoke-direct {v0, p0, v1, p1}, LX/4fu;-><init>(LX/0QA;LX/0R2;Ljava/lang/Class;)V

    move-object p0, v0

    .line 57828
    :cond_1
    return-object p0
.end method

.method public final getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;>;)",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57792
    iget-object v0, p0, LX/0Q8;->f:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Wm;

    .line 57793
    if-eqz v0, :cond_1

    .line 57794
    :cond_0
    :goto_0
    move-object v0, v0

    .line 57795
    invoke-virtual {p0}, LX/0Q8;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    .line 57796
    invoke-interface {v1}, LX/0R7;->a()Ljava/lang/Object;

    move-result-object v2

    .line 57797
    :try_start_0
    invoke-virtual {v0}, LX/0Wm;->a()LX/0Wl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 57798
    invoke-interface {v1, v2}, LX/0R7;->a(Ljava/lang/Object;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {v1, v2}, LX/0R7;->a(Ljava/lang/Object;)V

    throw v0

    .line 57799
    :cond_1
    new-instance v1, LX/0Wm;

    invoke-direct {v1, p1}, LX/0Wm;-><init>(Ljava/lang/Class;)V

    .line 57800
    invoke-virtual {p0}, LX/0Q8;->getScopeUnawareInjector()LX/0QD;

    move-result-object v0

    check-cast v0, LX/0QA;

    .line 57801
    iput-object v0, v1, LX/0RV;->mInjector:LX/0QB;

    .line 57802
    iget-object v0, p0, LX/0Q8;->f:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Wm;

    .line 57803
    if-nez v0, :cond_0

    move-object v0, v1

    .line 57804
    goto :goto_0
.end method

.method public final getProcessIdentifier()I
    .locals 1

    .prologue
    .line 57825
    iget v0, p0, LX/0Q8;->k:I

    return v0
.end method

.method public final getProvider(LX/0RI;)LX/0Or;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57811
    invoke-static {p0}, LX/0Q8;->b(LX/0Q8;)V

    .line 57812
    iget-boolean v0, p0, LX/0Q8;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/0Q8;->j:Z

    if-eqz v0, :cond_1

    .line 57813
    :cond_0
    sget-object v0, LX/4fo;->PROVIDER_GET:LX/4fo;

    invoke-static {v0, p1}, LX/4fp;->a(LX/4fo;LX/0RI;)V

    .line 57814
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/0Q8;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0RN;

    .line 57815
    if-nez v0, :cond_7

    .line 57816
    iget-boolean v0, p0, LX/0Q8;->j:Z

    if-eqz v0, :cond_4

    .line 57817
    const/4 v0, 0x0

    invoke-static {v0}, LX/52d;->a(Ljava/lang/Object;)LX/0Or;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 57818
    :goto_0
    iget-boolean v1, p0, LX/0Q8;->i:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, LX/0Q8;->j:Z

    if-eqz v1, :cond_3

    .line 57819
    :cond_2
    invoke-static {}, LX/4fp;->a()V

    :cond_3
    return-object v0

    .line 57820
    :cond_4
    :try_start_1
    new-instance v0, LX/4fr;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No provider bound for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 57821
    :catchall_0
    move-exception v0

    iget-boolean v1, p0, LX/0Q8;->i:Z

    if-nez v1, :cond_5

    iget-boolean v1, p0, LX/0Q8;->j:Z

    if-eqz v1, :cond_6

    .line 57822
    :cond_5
    invoke-static {}, LX/4fp;->a()V

    :cond_6
    throw v0

    .line 57823
    :cond_7
    :try_start_2
    iget-object v1, v0, LX/0RN;->c:LX/0Or;

    move-object v0, v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 57824
    goto :goto_0
.end method

.method public final getScopeAwareInjector()LX/0R6;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 57806
    invoke-static {p0}, LX/0Q8;->b(LX/0Q8;)V

    .line 57807
    invoke-virtual {p0}, LX/0Q8;->getInjectorThreadStack()LX/0S7;

    move-result-object v0

    .line 57808
    invoke-virtual {v0}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    move-object v0, v0

    .line 57809
    if-eqz v0, :cond_0

    .line 57810
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0Q8;->o:LX/0R4;

    goto :goto_0
.end method

.method public final getScopeUnawareInjector()LX/0QD;
    .locals 0

    .prologue
    .line 57805
    return-object p0
.end method
