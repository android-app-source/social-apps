.class public LX/1ge;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0pV;

.field private final b:Ljava/lang/String;

.field public c:I


# direct methods
.method public constructor <init>(LX/0pV;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 294468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 294469
    const/4 v0, 0x1

    iput v0, p0, LX/1ge;->c:I

    .line 294470
    iput-object p1, p0, LX/1ge;->a:LX/0pV;

    .line 294471
    iput-object p2, p0, LX/1ge;->b:Ljava/lang/String;

    .line 294472
    return-void
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 294464
    iget-object v1, p0, LX/1ge;->a:LX/0pV;

    iget-object v2, p0, LX/1ge;->b:Ljava/lang/String;

    sget-object v3, LX/0rj;->STATUS_CHANGED:LX/0rj;

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const-string v0, "loading"

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/0pV;->a(Ljava/lang/String;LX/0rj;Ljava/lang/String;)V

    .line 294465
    iput p1, p0, LX/1ge;->c:I

    .line 294466
    return-void

    .line 294467
    :cond_0
    const-string v0, "not loading"

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 294460
    iget v0, p0, LX/1ge;->c:I

    if-ne v1, v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "%s is %d but expected state was %d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, LX/1ge;->b:Ljava/lang/String;

    aput-object v5, v4, v2

    iget v2, p0, LX/1ge;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 294461
    invoke-direct {p0, v6}, LX/1ge;->a(I)V

    .line 294462
    return-void

    :cond_0
    move v0, v2

    .line 294463
    goto :goto_0
.end method

.method public final b()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x2

    const/4 v1, 0x1

    .line 294456
    iget v0, p0, LX/1ge;->c:I

    if-ne v6, v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "%s is %d but expected state was %d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, LX/1ge;->b:Ljava/lang/String;

    aput-object v5, v4, v2

    iget v2, p0, LX/1ge;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v6

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 294457
    invoke-direct {p0, v1}, LX/1ge;->a(I)V

    .line 294458
    return-void

    :cond_0
    move v0, v2

    .line 294459
    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 294454
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/1ge;->a(I)V

    .line 294455
    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 294453
    iget v0, p0, LX/1ge;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 294452
    const-string v0, "%s: %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/1ge;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, LX/1ge;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
