.class public LX/1BI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1BI;


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0SG;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 212901
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 212902
    iput-object p1, p0, LX/1BI;->a:LX/0SG;

    .line 212903
    iput-object p2, p0, LX/1BI;->b:LX/0Zb;

    .line 212904
    return-void
.end method

.method public static a(LX/0QB;)LX/1BI;
    .locals 5

    .prologue
    .line 212905
    sget-object v0, LX/1BI;->c:LX/1BI;

    if-nez v0, :cond_1

    .line 212906
    const-class v1, LX/1BI;

    monitor-enter v1

    .line 212907
    :try_start_0
    sget-object v0, LX/1BI;->c:LX/1BI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 212908
    if-eqz v2, :cond_0

    .line 212909
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 212910
    new-instance p0, LX/1BI;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {p0, v3, v4}, LX/1BI;-><init>(LX/0SG;LX/0Zb;)V

    .line 212911
    move-object v0, p0

    .line 212912
    sput-object v0, LX/1BI;->c:LX/1BI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212913
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 212914
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 212915
    :cond_1
    sget-object v0, LX/1BI;->c:LX/1BI;

    return-object v0

    .line 212916
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 212917
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(J)V
    .locals 5

    .prologue
    .line 212918
    iget-object v0, p0, LX/1BI;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    sub-long/2addr v0, p1

    .line 212919
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "feed_unit_refreshed"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "staleness"

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 212920
    iget-object v1, p0, LX/1BI;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 212921
    return-void
.end method
