.class public final LX/1X0;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1VD;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:I

.field public d:I

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public final synthetic q:LX/1VD;


# direct methods
.method public constructor <init>(LX/1VD;)V
    .locals 1

    .prologue
    .line 270224
    iput-object p1, p0, LX/1X0;->q:LX/1VD;

    .line 270225
    move-object v0, p1

    .line 270226
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 270227
    sget v0, LX/1X2;->a:I

    iput v0, p0, LX/1X0;->c:I

    .line 270228
    const v0, 0x7fffffff

    iput v0, p0, LX/1X0;->d:I

    .line 270229
    sget v0, LX/1X2;->b:I

    iput v0, p0, LX/1X0;->l:I

    .line 270230
    sget v0, LX/1X2;->d:I

    iput v0, p0, LX/1X0;->m:I

    .line 270231
    sget v0, LX/1X2;->e:I

    iput v0, p0, LX/1X0;->n:I

    .line 270232
    sget v0, LX/1X2;->c:I

    iput v0, p0, LX/1X0;->o:I

    .line 270233
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 270234
    const-string v0, "FeedStoryHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 270235
    if-ne p0, p1, :cond_1

    .line 270236
    :cond_0
    :goto_0
    return v0

    .line 270237
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 270238
    goto :goto_0

    .line 270239
    :cond_3
    check-cast p1, LX/1X0;

    .line 270240
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 270241
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 270242
    if-eq v2, v3, :cond_0

    .line 270243
    iget-object v2, p0, LX/1X0;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/1X0;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/1X0;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 270244
    goto :goto_0

    .line 270245
    :cond_5
    iget-object v2, p1, LX/1X0;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 270246
    :cond_6
    iget-object v2, p0, LX/1X0;->b:LX/1Pb;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/1X0;->b:LX/1Pb;

    iget-object v3, p1, LX/1X0;->b:LX/1Pb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 270247
    goto :goto_0

    .line 270248
    :cond_8
    iget-object v2, p1, LX/1X0;->b:LX/1Pb;

    if-nez v2, :cond_7

    .line 270249
    :cond_9
    iget v2, p0, LX/1X0;->c:I

    iget v3, p1, LX/1X0;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 270250
    goto :goto_0

    .line 270251
    :cond_a
    iget v2, p0, LX/1X0;->d:I

    iget v3, p1, LX/1X0;->d:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 270252
    goto :goto_0

    .line 270253
    :cond_b
    iget-boolean v2, p0, LX/1X0;->e:Z

    iget-boolean v3, p1, LX/1X0;->e:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 270254
    goto :goto_0

    .line 270255
    :cond_c
    iget-boolean v2, p0, LX/1X0;->f:Z

    iget-boolean v3, p1, LX/1X0;->f:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 270256
    goto :goto_0

    .line 270257
    :cond_d
    iget-boolean v2, p0, LX/1X0;->g:Z

    iget-boolean v3, p1, LX/1X0;->g:Z

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 270258
    goto :goto_0

    .line 270259
    :cond_e
    iget-boolean v2, p0, LX/1X0;->h:Z

    iget-boolean v3, p1, LX/1X0;->h:Z

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 270260
    goto :goto_0

    .line 270261
    :cond_f
    iget-boolean v2, p0, LX/1X0;->i:Z

    iget-boolean v3, p1, LX/1X0;->i:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 270262
    goto :goto_0

    .line 270263
    :cond_10
    iget-boolean v2, p0, LX/1X0;->j:Z

    iget-boolean v3, p1, LX/1X0;->j:Z

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 270264
    goto :goto_0

    .line 270265
    :cond_11
    iget-boolean v2, p0, LX/1X0;->k:Z

    iget-boolean v3, p1, LX/1X0;->k:Z

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 270266
    goto/16 :goto_0

    .line 270267
    :cond_12
    iget v2, p0, LX/1X0;->l:I

    iget v3, p1, LX/1X0;->l:I

    if-eq v2, v3, :cond_13

    move v0, v1

    .line 270268
    goto/16 :goto_0

    .line 270269
    :cond_13
    iget v2, p0, LX/1X0;->m:I

    iget v3, p1, LX/1X0;->m:I

    if-eq v2, v3, :cond_14

    move v0, v1

    .line 270270
    goto/16 :goto_0

    .line 270271
    :cond_14
    iget v2, p0, LX/1X0;->n:I

    iget v3, p1, LX/1X0;->n:I

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 270272
    goto/16 :goto_0

    .line 270273
    :cond_15
    iget v2, p0, LX/1X0;->o:I

    iget v3, p1, LX/1X0;->o:I

    if-eq v2, v3, :cond_16

    move v0, v1

    .line 270274
    goto/16 :goto_0

    .line 270275
    :cond_16
    iget v2, p0, LX/1X0;->p:I

    iget v3, p1, LX/1X0;->p:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 270276
    goto/16 :goto_0
.end method
