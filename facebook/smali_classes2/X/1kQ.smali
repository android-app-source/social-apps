.class public LX/1kQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ry;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final b:Ljava/lang/String;

.field private static l:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final c:LX/0tX;

.field public final d:LX/1kR;

.field private final e:LX/1Ck;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0si;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/1kS;

.field public final i:LX/03V;

.field public final j:LX/0ad;

.field public final k:LX/0fO;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 309543
    const-class v0, LX/1kQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1kQ;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/1kR;LX/1Ck;LX/0Or;LX/0Or;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1kS;LX/03V;LX/0ad;LX/0fO;)V
    .locals 0
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/inlinecomposer/abtest/IsInlineComposerPromptEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/1kR;",
            "LX/1Ck;",
            "LX/0Or",
            "<",
            "LX/0si;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/1kS;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0ad;",
            "LX/0fO;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 309544
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309545
    iput-object p1, p0, LX/1kQ;->c:LX/0tX;

    .line 309546
    iput-object p2, p0, LX/1kQ;->d:LX/1kR;

    .line 309547
    iput-object p3, p0, LX/1kQ;->e:LX/1Ck;

    .line 309548
    iput-object p4, p0, LX/1kQ;->f:LX/0Or;

    .line 309549
    iput-object p5, p0, LX/1kQ;->g:LX/0Or;

    .line 309550
    iput-object p6, p0, LX/1kQ;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 309551
    iput-object p7, p0, LX/1kQ;->h:LX/1kS;

    .line 309552
    iput-object p8, p0, LX/1kQ;->i:LX/03V;

    .line 309553
    iput-object p9, p0, LX/1kQ;->j:LX/0ad;

    .line 309554
    iput-object p10, p0, LX/1kQ;->k:LX/0fO;

    .line 309555
    return-void
.end method

.method public static b(LX/0QB;)LX/1kQ;
    .locals 14

    .prologue
    .line 309556
    const-class v1, LX/1kQ;

    monitor-enter v1

    .line 309557
    :try_start_0
    sget-object v0, LX/1kQ;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 309558
    sput-object v2, LX/1kQ;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 309559
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309560
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 309561
    new-instance v3, LX/1kQ;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/1kR;->b(LX/0QB;)LX/1kR;

    move-result-object v5

    check-cast v5, LX/1kR;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    const/16 v7, 0xb0f

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x319

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v9

    check-cast v9, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/1kS;->a(LX/0QB;)LX/1kS;

    move-result-object v10

    check-cast v10, LX/1kS;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-static {v0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v13

    check-cast v13, LX/0fO;

    invoke-direct/range {v3 .. v13}, LX/1kQ;-><init>(LX/0tX;LX/1kR;LX/1Ck;LX/0Or;LX/0Or;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1kS;LX/03V;LX/0ad;LX/0fO;)V

    .line 309562
    move-object v0, v3

    .line 309563
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 309564
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1kQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 309565
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 309566
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
            ">;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "LX/1kK;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 309567
    new-instance v0, LX/1l3;

    invoke-direct {v0, p0}, LX/1l3;-><init>(LX/1kQ;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public a(Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "LX/1kK;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 309568
    iget-object v0, p0, LX/1kQ;->d:LX/1kR;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/1kR;->a(ZLX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1kQ;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 309569
    const-class v0, LX/1kW;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 309570
    invoke-virtual {p0}, LX/1kQ;->c()V

    .line 309571
    return-void
.end method

.method public a(Ljava/lang/Class;Ljava/lang/String;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 309572
    const-class v0, LX/1kW;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 309573
    new-instance v0, LX/4Ib;

    invoke-direct {v0}, LX/4Ib;-><init>()V

    .line 309574
    const-string v1, "production_prompt_id"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 309575
    move-object v1, v0

    .line 309576
    if-eqz p3, :cond_0

    const-string v0, "post-sugg"

    .line 309577
    :goto_0
    const-string v2, "user_action"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 309578
    move-object v0, v1

    .line 309579
    new-instance v1, LX/5oO;

    invoke-direct {v1}, LX/5oO;-><init>()V

    move-object v1, v1

    .line 309580
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 309581
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    move-object v0, v0

    .line 309582
    iget-object v1, p0, LX/1kQ;->e:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "dismiss_prodution_prompt_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/BMD;

    invoke-direct {v3, p0, v0}, LX/BMD;-><init>(LX/1kQ;LX/399;)V

    new-instance v0, LX/BME;

    invoke-direct {v0, p0}, LX/BME;-><init>(LX/1kQ;)V

    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 309583
    return-void

    .line 309584
    :cond_0
    const-string v0, "x-out"

    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 309585
    iget-object v0, p0, LX/1kQ;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309586
    const-class v0, LX/1kW;

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 309587
    iget-object v0, p0, LX/1kQ;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0si;

    iget-object v1, p0, LX/1kQ;->d:LX/1kR;

    invoke-virtual {v1}, LX/1kR;->a()LX/0zO;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0si;->a(LX/0zO;)V

    .line 309588
    return-void
.end method
