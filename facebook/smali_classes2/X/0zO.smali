.class public LX/0zO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/graphql/query/GraphQLRefRequest;"
    }
.end annotation


# static fields
.field private static final l:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public A:I

.field public B:I

.field public a:LX/0zS;

.field public b:LX/0zT;

.field public c:J

.field public d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/common/callercontext/CallerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/1kt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1kt",
            "<TT;>;"
        }
    .end annotation
.end field

.field public h:Z

.field public i:Ljava/lang/String;

.field public j:LX/1NI;

.field public k:Z

.field public final m:LX/0gW;

.field public final n:LX/0w5;

.field public o:Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;

.field public p:Z

.field public q:Z

.field private r:LX/0zW;

.field public s:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:LX/0sj;

.field public u:LX/1kx;

.field public final v:I

.field public final w:I

.field public x:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/4cQ;",
            ">;"
        }
    .end annotation
.end field

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 167145
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, LX/0zO;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method private constructor <init>(LX/0gW;LX/0w5;)V
    .locals 2

    .prologue
    .line 167148
    invoke-static {}, LX/0tX;->c()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, LX/0zO;-><init>(LX/0gW;LX/0w5;ILcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;)V

    .line 167149
    return-void
.end method

.method private constructor <init>(LX/0gW;LX/0w5;ILcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;)V
    .locals 5
    .param p4    # Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 167150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167151
    sget-object v0, LX/0zS;->c:LX/0zS;

    iput-object v0, p0, LX/0zO;->a:LX/0zS;

    .line 167152
    iput-boolean v3, p0, LX/0zO;->p:Z

    .line 167153
    iput-boolean v3, p0, LX/0zO;->q:Z

    .line 167154
    sget-object v0, LX/0zT;->a:LX/0zT;

    iput-object v0, p0, LX/0zO;->b:LX/0zT;

    .line 167155
    const-wide/32 v0, 0x240c8400

    iput-wide v0, p0, LX/0zO;->c:J

    .line 167156
    iput-object v2, p0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 167157
    iput-object v2, p0, LX/0zO;->f:LX/0Px;

    .line 167158
    iput-object v2, p0, LX/0zO;->g:LX/1kt;

    .line 167159
    iput-object v2, p0, LX/0zO;->t:LX/0sj;

    .line 167160
    iput-object v2, p0, LX/0zO;->u:LX/1kx;

    .line 167161
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 167162
    iput-object v0, p0, LX/0zO;->x:LX/0Px;

    .line 167163
    iput-object v2, p0, LX/0zO;->j:LX/1NI;

    .line 167164
    iput v4, p0, LX/0zO;->A:I

    .line 167165
    iput v3, p0, LX/0zO;->B:I

    .line 167166
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 167167
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 167168
    iput-object p1, p0, LX/0zO;->m:LX/0gW;

    .line 167169
    iget-object v0, p0, LX/0zO;->m:LX/0gW;

    .line 167170
    iput-boolean v4, v0, LX/0gW;->l:Z

    .line 167171
    iput-object p2, p0, LX/0zO;->n:LX/0w5;

    .line 167172
    iput-object p4, p0, LX/0zO;->o:Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;

    .line 167173
    iget-object v0, p1, LX/0gW;->f:Ljava/lang/String;

    move-object v0, v0

    .line 167174
    new-instance v1, LX/0zW;

    if-eqz v0, :cond_0

    :goto_0
    invoke-direct {v1, v0}, LX/0zW;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 167175
    iput-object v0, p0, LX/0zO;->r:LX/0zW;

    .line 167176
    sget-object v0, LX/0zO;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iput v0, p0, LX/0zO;->v:I

    .line 167177
    iput p3, p0, LX/0zO;->w:I

    .line 167178
    return-void

    :cond_0
    const-string v0, "unknown"

    goto :goto_0
.end method

.method public constructor <init>(LX/0zO;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zO",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 167179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167180
    sget-object v0, LX/0zS;->c:LX/0zS;

    iput-object v0, p0, LX/0zO;->a:LX/0zS;

    .line 167181
    iput-boolean v3, p0, LX/0zO;->p:Z

    .line 167182
    iput-boolean v3, p0, LX/0zO;->q:Z

    .line 167183
    sget-object v0, LX/0zT;->a:LX/0zT;

    iput-object v0, p0, LX/0zO;->b:LX/0zT;

    .line 167184
    const-wide/32 v0, 0x240c8400

    iput-wide v0, p0, LX/0zO;->c:J

    .line 167185
    iput-object v2, p0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 167186
    iput-object v2, p0, LX/0zO;->f:LX/0Px;

    .line 167187
    iput-object v2, p0, LX/0zO;->g:LX/1kt;

    .line 167188
    iput-object v2, p0, LX/0zO;->t:LX/0sj;

    .line 167189
    iput-object v2, p0, LX/0zO;->u:LX/1kx;

    .line 167190
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 167191
    iput-object v0, p0, LX/0zO;->x:LX/0Px;

    .line 167192
    iput-object v2, p0, LX/0zO;->j:LX/1NI;

    .line 167193
    const/4 v0, 0x1

    iput v0, p0, LX/0zO;->A:I

    .line 167194
    iput v3, p0, LX/0zO;->B:I

    .line 167195
    iget-object v0, p1, LX/0zO;->m:LX/0gW;

    invoke-virtual {v0}, LX/0gW;->p()LX/0gW;

    move-result-object v0

    iput-object v0, p0, LX/0zO;->m:LX/0gW;

    .line 167196
    iget-object v0, p1, LX/0zO;->n:LX/0w5;

    move-object v0, v0

    .line 167197
    iput-object v0, p0, LX/0zO;->n:LX/0w5;

    .line 167198
    iget-object v0, p1, LX/0zO;->o:Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;

    move-object v0, v0

    .line 167199
    iput-object v0, p0, LX/0zO;->o:Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;

    .line 167200
    iget-object v0, p1, LX/0zO;->a:LX/0zS;

    iput-object v0, p0, LX/0zO;->a:LX/0zS;

    .line 167201
    iget-boolean v0, p1, LX/0zO;->p:Z

    move v0, v0

    .line 167202
    iput-boolean v0, p0, LX/0zO;->p:Z

    .line 167203
    invoke-virtual {p1}, LX/0zO;->h()Z

    move-result v0

    iput-boolean v0, p0, LX/0zO;->q:Z

    .line 167204
    iget-object v0, p1, LX/0zO;->b:LX/0zT;

    iput-object v0, p0, LX/0zO;->b:LX/0zT;

    .line 167205
    iget-wide v0, p1, LX/0zO;->c:J

    iput-wide v0, p0, LX/0zO;->c:J

    .line 167206
    iget-object v0, p1, LX/0zO;->d:Ljava/util/Set;

    iput-object v0, p0, LX/0zO;->d:Ljava/util/Set;

    .line 167207
    iget-object v0, p1, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 167208
    iget-object v0, p1, LX/0zO;->f:LX/0Px;

    iput-object v0, p0, LX/0zO;->f:LX/0Px;

    .line 167209
    iget-object v0, p1, LX/0zO;->g:LX/1kt;

    iput-object v0, p0, LX/0zO;->g:LX/1kt;

    .line 167210
    iget-object v0, p1, LX/0zO;->i:Ljava/lang/String;

    iput-object v0, p0, LX/0zO;->i:Ljava/lang/String;

    .line 167211
    iget-object v0, p1, LX/0zO;->j:LX/1NI;

    iput-object v0, p0, LX/0zO;->j:LX/1NI;

    .line 167212
    iget-object v0, p1, LX/0zO;->r:LX/0zW;

    iput-object v0, p0, LX/0zO;->r:LX/0zW;

    .line 167213
    iget-object v0, p1, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 167214
    iget-boolean v0, p1, LX/0zO;->h:Z

    iput-boolean v0, p0, LX/0zO;->h:Z

    .line 167215
    iget v0, p1, LX/0zO;->A:I

    iput v0, p0, LX/0zO;->A:I

    .line 167216
    iget-object v0, p1, LX/0zO;->z:Ljava/lang/String;

    move-object v0, v0

    .line 167217
    iput-object v0, p0, LX/0zO;->z:Ljava/lang/String;

    .line 167218
    sget-object v0, LX/0zO;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iput v0, p0, LX/0zO;->v:I

    .line 167219
    invoke-static {}, LX/0tX;->c()I

    move-result v0

    iput v0, p0, LX/0zO;->w:I

    .line 167220
    return-void
.end method

.method public static a(LX/0gW;)LX/0zO;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0gW",
            "<TT;>;)",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167221
    instance-of v0, p0, LX/0zP;

    if-eqz v0, :cond_0

    .line 167222
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to create a "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, LX/0zO;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, LX/0zP;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Use createMutationRequest() instead."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167223
    :cond_0
    const-string v3, "profile_image_small_size"

    invoke-static {p0, v3}, LX/0zQ;->a(LX/0gW;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 167224
    invoke-static {}, LX/0wB;->b()I

    move-result v3

    .line 167225
    const-string v4, "profile_image_small_size"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v4, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 167226
    :cond_1
    :goto_0
    const-string v3, "nt_context"

    invoke-static {p0, v3}, LX/0zQ;->a(LX/0gW;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 167227
    new-instance v3, LX/0zR;

    invoke-direct {v3}, LX/0zR;-><init>()V

    const-string v4, "701268200040015"

    invoke-virtual {v3, v4}, LX/0zR;->a(Ljava/lang/String;)LX/0zR;

    move-result-object v3

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v4

    invoke-virtual {v4}, LX/0wC;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0zR;->a(Ljava/lang/Double;)LX/0zR;

    move-result-object v3

    .line 167228
    const-string v4, "nt_context"

    invoke-virtual {p0, v4, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 167229
    :cond_2
    new-instance v0, LX/0zO;

    .line 167230
    iget-object v1, p0, LX/0gW;->a:LX/0w5;

    move-object v1, v1

    .line 167231
    invoke-direct {v0, p0, v1}, LX/0zO;-><init>(LX/0gW;LX/0w5;)V

    .line 167232
    invoke-virtual {p0}, LX/0gW;->l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;

    move-result-object v1

    .line 167233
    if-eqz v1, :cond_3

    .line 167234
    iput-object v1, v0, LX/0zO;->o:Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;

    .line 167235
    :cond_3
    return-object v0

    .line 167236
    :cond_4
    const-string v3, "profile_image_big_size"

    invoke-static {p0, v3}, LX/0zQ;->a(LX/0gW;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 167237
    invoke-static {}, LX/0wB;->c()I

    move-result v3

    .line 167238
    const-string v4, "profile_image_big_size"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v4, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    goto :goto_0
.end method

.method public static a(LX/0gW;LX/0w5;)LX/0zO;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0gW;",
            "LX/0w5",
            "<TT;>;)",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 167239
    new-instance v0, LX/0zO;

    invoke-direct {v0, p0, p1}, LX/0zO;-><init>(LX/0gW;LX/0w5;)V

    return-object v0
.end method

.method public static a(LX/399;LX/3G6;)LX/0zO;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/399",
            "<TT;>;",
            "LX/3G6;",
            ")",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167240
    new-instance v1, LX/0zO;

    iget-object v0, p0, LX/399;->a:LX/0zP;

    iget-object v2, p0, LX/399;->a:LX/0zP;

    iget-object v2, v2, LX/0gW;->a:LX/0w5;

    .line 167241
    iget v3, p1, LX/3G6;->f:I

    move v3, v3

    .line 167242
    const/4 v4, 0x0

    invoke-direct {v1, v0, v2, v3, v4}, LX/0zO;-><init>(LX/0gW;LX/0w5;ILcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;)V

    .line 167243
    iget-object v0, p0, LX/399;->f:LX/0Px;

    move-object v0, v0

    .line 167244
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, v1, LX/0zO;->x:LX/0Px;

    .line 167245
    return-object v1
.end method

.method public static a(LX/0zP;)LX/399;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zP",
            "<TT;>;)",
            "LX/399",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167257
    new-instance v0, LX/399;

    invoke-direct {v0, p0}, LX/399;-><init>(LX/0zP;)V

    return-object v0
.end method

.method public static final a(Lcom/facebook/common/callercontext/CallerContext;LX/0w5;)Lcom/facebook/common/callercontext/CallerContext;
    .locals 2
    .param p0    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 167246
    if-eqz p0, :cond_0

    .line 167247
    :goto_0
    return-object p0

    :cond_0
    new-instance p0, Lcom/facebook/graphql/executor/GraphQLRequest$1;

    invoke-virtual {p1}, LX/0w5;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1, v1, v1}, Lcom/facebook/graphql/executor/GraphQLRequest$1;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(J)LX/0zO;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167255
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p1

    iput-wide v0, p0, LX/0zO;->c:J

    .line 167256
    return-object p0
.end method

.method public final a(LX/0w7;)LX/0zO;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0w7;",
            ")",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 167258
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 167259
    iget-object v0, p0, LX/0zO;->m:LX/0gW;

    .line 167260
    iput-object p1, v0, LX/0gW;->e:LX/0w7;

    .line 167261
    return-object p0
.end method

.method public final a(LX/0zS;)LX/0zO;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zS;",
            ")",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167252
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 167253
    iput-object p1, p0, LX/0zO;->a:LX/0zS;

    .line 167254
    return-object p0
.end method

.method public final a(LX/0zT;)LX/0zO;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zT;",
            ")",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167084
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 167085
    iput-object p1, p0, LX/0zO;->b:LX/0zT;

    .line 167086
    return-object p0
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/0zO;
    .locals 0
    .param p1    # Lcom/facebook/auth/viewercontext/ViewerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ")",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167250
    iput-object p1, p0, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 167251
    return-object p0
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;
    .locals 0
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167248
    iput-object p1, p0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 167249
    return-object p0
.end method

.method public final a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            ")",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167146
    iget-object v0, p0, LX/0zO;->r:LX/0zW;

    invoke-virtual {v0, p1}, LX/0zW;->a(Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 167147
    return-object p0
.end method

.method public final a(Ljava/lang/String;LX/4Zz;LX/4a0;)LX/4a1;
    .locals 3

    .prologue
    .line 167077
    iget-object v0, p0, LX/0zO;->m:LX/0gW;

    move-object v0, v0

    .line 167078
    invoke-virtual {v0}, LX/0gW;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "query does not contain token "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 167079
    new-instance v0, LX/4a1;

    invoke-direct {v0, p0, p1, p2, p3}, LX/4a1;-><init>(LX/0zO;Ljava/lang/String;LX/4Zz;LX/4a0;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167080
    iget-object v0, p0, LX/0zO;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 167081
    iget-object v0, p0, LX/0zO;->m:LX/0gW;

    .line 167082
    iget-object p0, v0, LX/0gW;->f:Ljava/lang/String;

    move-object v0, p0

    .line 167083
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0zO;->i:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(LX/0t2;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 167087
    iget-object v0, p0, LX/0zO;->b:LX/0zT;

    .line 167088
    iget-object v1, p0, LX/0zO;->n:LX/0w5;

    move-object v1, v1

    .line 167089
    invoke-interface {v0, p0, v1, p1}, LX/0zT;->a(LX/0zO;LX/0w5;LX/0t2;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0t2;Lcom/facebook/graphql/executor/GraphQLResult;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0t2;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 167090
    iget-object v0, p2, Lcom/facebook/graphql/executor/GraphQLResult;->k:Ljava/util/Map;

    move-object v1, v0

    .line 167091
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 167092
    :cond_0
    invoke-virtual {p0, p1}, LX/0zO;->a(LX/0t2;)Ljava/lang/String;

    move-result-object v0

    .line 167093
    :goto_0
    return-object v0

    .line 167094
    :cond_1
    new-instance v3, LX/0zO;

    invoke-direct {v3, p0}, LX/0zO;-><init>(LX/0zO;)V

    .line 167095
    invoke-virtual {v3}, LX/0zO;->d()LX/0w7;

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v0, LX/0w7;

    invoke-virtual {v3}, LX/0zO;->d()LX/0w7;

    move-result-object v2

    invoke-virtual {v2}, LX/0w7;->e()Ljava/util/Map;

    move-result-object v2

    invoke-direct {v0, v2}, LX/0w7;-><init>(Ljava/util/Map;)V

    move-object v2, v0

    .line 167096
    :goto_1
    iget-object v0, p0, LX/0zO;->m:LX/0gW;

    move-object v4, v0

    .line 167097
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 167098
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 167099
    if-eqz v4, :cond_2

    .line 167100
    invoke-virtual {v4, v1}, LX/0gW;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 167101
    :cond_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    instance-of v6, v6, Ljava/lang/String;

    if-eqz v6, :cond_4

    .line 167102
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/String;)LX/0w7;

    goto :goto_2

    .line 167103
    :cond_3
    new-instance v0, LX/0w7;

    invoke-direct {v0}, LX/0w7;-><init>()V

    move-object v2, v0

    goto :goto_1

    .line 167104
    :cond_4
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    instance-of v6, v6, Ljava/util/List;

    if-eqz v6, :cond_5

    .line 167105
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v2, v1, v0}, LX/0w7;->a(Ljava/lang/String;Ljava/util/List;)LX/0w7;

    goto :goto_2

    .line 167106
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected type found in ref params"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167107
    :cond_6
    invoke-virtual {v3, v2}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 167108
    invoke-virtual {v3, p1}, LX/0zO;->a(LX/0t2;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()LX/0gW;
    .locals 1

    .prologue
    .line 167109
    iget-object v0, p0, LX/0zO;->m:LX/0gW;

    return-object v0
.end method

.method public final c(Z)LX/0zO;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167110
    iput-boolean p1, p0, LX/0zO;->h:Z

    .line 167111
    return-object p0
.end method

.method public final d()LX/0w7;
    .locals 1

    .prologue
    .line 167112
    iget-object v0, p0, LX/0zO;->m:LX/0gW;

    .line 167113
    iget-object p0, v0, LX/0gW;->e:LX/0w7;

    move-object v0, p0

    .line 167114
    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 167115
    iget-object v0, p0, LX/0zO;->m:LX/0gW;

    .line 167116
    iget-boolean p0, v0, LX/0gW;->m:Z

    move v0, p0

    .line 167117
    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 167118
    iget-boolean v0, p0, LX/0zO;->p:Z

    move v0, v0

    .line 167119
    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/0zO;->q:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()LX/0zW;
    .locals 3

    .prologue
    .line 167120
    iget-object v0, p0, LX/0zO;->r:LX/0zW;

    .line 167121
    iget-object v1, p0, LX/0zO;->a:LX/0zS;

    move-object v1, v1

    .line 167122
    sget-object v2, LX/0zS;->e:LX/0zS;

    if-ne v1, v2, :cond_0

    .line 167123
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    .line 167124
    :goto_0
    move-object v1, v1

    .line 167125
    invoke-virtual {v0, v1}, LX/0zW;->b(Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 167126
    iget-object v0, p0, LX/0zO;->r:LX/0zW;

    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 167127
    iget-object v0, p0, LX/0zO;->m:LX/0gW;

    invoke-virtual {v0}, LX/0gW;->a()Z

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 167128
    invoke-virtual {p0}, LX/0zO;->d()LX/0w7;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 167129
    :goto_0
    return v0

    .line 167130
    :cond_0
    invoke-virtual {p0}, LX/0zO;->d()LX/0w7;

    move-result-object v0

    invoke-virtual {v0}, LX/0w7;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4a1;

    .line 167131
    iget-object v0, v0, LX/4a1;->c:LX/4Zz;

    sget-object v3, LX/4Zz;->EACH:LX/4Zz;

    if-ne v0, v3, :cond_1

    .line 167132
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 167133
    goto :goto_0
.end method

.method public final p()LX/0w5;
    .locals 1

    .prologue
    .line 167134
    iget-object v0, p0, LX/0zO;->n:LX/0w5;

    return-object v0
.end method

.method public final v()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 167135
    iget-boolean v1, p0, LX/0zO;->p:Z

    move v1, v1

    .line 167136
    if-nez v1, :cond_1

    .line 167137
    :cond_0
    :goto_0
    return v0

    .line 167138
    :cond_1
    iget-object v1, p0, LX/0zO;->g:LX/1kt;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/0zO;->g:LX/1kt;

    instance-of v1, v1, LX/1ks;

    if-eqz v1, :cond_0

    .line 167139
    :cond_2
    iget-object v1, p0, LX/0zO;->n:LX/0w5;

    iget-object v2, p0, LX/0zO;->m:LX/0gW;

    iget-object v2, v2, LX/0gW;->a:LX/0w5;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167140
    iget-object v1, p0, LX/0zO;->m:LX/0gW;

    const/4 v2, 0x1

    .line 167141
    iget v3, v1, LX/0gW;->d:I

    if-ne v3, v2, :cond_3

    :goto_1
    move v1, v2

    .line 167142
    if-nez v1, :cond_0

    .line 167143
    iget-object v1, p0, LX/0zO;->m:LX/0gW;

    invoke-virtual {v1}, LX/0gW;->o()Z

    move-result v1

    if-nez v1, :cond_0

    .line 167144
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method
