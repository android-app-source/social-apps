.class public abstract LX/0QV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/Beta;
.end annotation

.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation


# static fields
.field public static final SYSTEM_TICKER:LX/0QV;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58165
    new-instance v0, LX/0QW;

    invoke-direct {v0}, LX/0QW;-><init>()V

    sput-object v0, LX/0QV;->SYSTEM_TICKER:LX/0QV;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static systemTicker()LX/0QV;
    .locals 1
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 58167
    sget-object v0, LX/0QV;->SYSTEM_TICKER:LX/0QV;

    return-object v0
.end method


# virtual methods
.method public abstract read()J
.end method
