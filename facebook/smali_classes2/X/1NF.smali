.class public final LX/1NF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1CY;

.field private final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Lcom/facebook/graphql/model/FeedUnit;


# direct methods
.method public constructor <init>(LX/1CY;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 237073
    iput-object p1, p0, LX/1NF;->a:LX/1CY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237074
    iput-object p2, p0, LX/1NF;->b:Ljava/lang/String;

    .line 237075
    iput-object p3, p0, LX/1NF;->c:Ljava/lang/String;

    .line 237076
    iput-object p4, p0, LX/1NF;->d:Lcom/facebook/graphql/model/FeedUnit;

    .line 237077
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 237155
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "Failed to update feed"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 237156
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 237078
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v6, 0x0

    .line 237079
    iget-object v0, p0, LX/1NF;->a:LX/1CY;

    iget-object v1, v0, LX/1CY;->f:LX/0g4;

    .line 237080
    iget-object v0, p0, LX/1NF;->a:LX/1CY;

    iget-object v2, v0, LX/1CY;->e:LX/0fz;

    .line 237081
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 237082
    :cond_0
    :goto_0
    return-void

    .line 237083
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 237084
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_2

    .line 237085
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 237086
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 237087
    invoke-static {v0, v6}, LX/0x1;->a(LX/16g;LX/162;)V

    .line 237088
    :cond_2
    instance-of v0, v1, LX/0g3;

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 237089
    check-cast v0, LX/0g3;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, LX/0g3;->a(Z)V

    .line 237090
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 237091
    if-nez v0, :cond_6

    .line 237092
    iget-object v0, p0, LX/1NF;->b:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 237093
    new-instance v0, LX/1u8;

    invoke-direct {v0}, LX/1u8;-><init>()V

    .line 237094
    iput-object v6, v0, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 237095
    move-object v0, v0

    .line 237096
    iget-object v3, p0, LX/1NF;->b:Ljava/lang/String;

    .line 237097
    iput-object v3, v0, LX/1u8;->d:Ljava/lang/String;

    .line 237098
    move-object v0, v0

    .line 237099
    iput-object v6, v0, LX/1u8;->i:Ljava/lang/String;

    .line 237100
    move-object v0, v0

    .line 237101
    iput-object v6, v0, LX/1u8;->c:Ljava/lang/String;

    .line 237102
    move-object v0, v0

    .line 237103
    invoke-virtual {v0}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0fz;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 237104
    :cond_4
    :goto_1
    invoke-interface {v1}, LX/0g4;->d()V

    goto :goto_0

    .line 237105
    :cond_5
    iget-object v0, p0, LX/1NF;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 237106
    iget-object v0, v2, LX/0fz;->d:LX/0qm;

    move-object v0, v0

    .line 237107
    iget-object v2, p0, LX/1NF;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/0qm;->b(Ljava/lang/String;)Z

    goto :goto_1

    .line 237108
    :cond_6
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 237109
    check-cast v0, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;

    iget-object v3, p0, LX/1NF;->a:LX/1CY;

    iget-object v3, v3, LX/1CY;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-static {v0, v4, v5}, LX/16t;->a(Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;J)V

    .line 237110
    iget-object v0, p0, LX/1NF;->b:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 237111
    iget-object v3, p0, LX/1NF;->b:Ljava/lang/String;

    .line 237112
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 237113
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 237114
    iget-object v7, v2, LX/0fz;->g:LX/0qu;

    invoke-virtual {v7, v3}, LX/0qu;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v7

    .line 237115
    if-eqz v7, :cond_8

    .line 237116
    new-instance v8, LX/1u8;

    invoke-direct {v8}, LX/1u8;-><init>()V

    .line 237117
    iput-object v0, v8, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 237118
    move-object v8, v8

    .line 237119
    iput-object v3, v8, LX/1u8;->d:Ljava/lang/String;

    .line 237120
    move-object v8, v8

    .line 237121
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v9

    .line 237122
    iput-object v9, v8, LX/1u8;->i:Ljava/lang/String;

    .line 237123
    move-object v8, v8

    .line 237124
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v9

    .line 237125
    iput-object v9, v8, LX/1u8;->c:Ljava/lang/String;

    .line 237126
    move-object v8, v8

    .line 237127
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->e()D

    move-result-wide v9

    .line 237128
    iput-wide v9, v8, LX/1u8;->h:D

    .line 237129
    move-object v8, v8

    .line 237130
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->l()Ljava/lang/String;

    move-result-object v9

    .line 237131
    iput-object v9, v8, LX/1u8;->f:Ljava/lang/String;

    .line 237132
    move-object v8, v8

    .line 237133
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->m()Z

    move-result v9

    .line 237134
    iput-boolean v9, v8, LX/1u8;->e:Z

    .line 237135
    move-object v8, v8

    .line 237136
    invoke-virtual {v8}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v8

    .line 237137
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, LX/0x1;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 237138
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 237139
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v9

    if-eqz v9, :cond_7

    .line 237140
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v9

    invoke-static {v9}, LX/0x1;->f(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v9

    invoke-static {v0, v9}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;I)V

    .line 237141
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v9

    invoke-static {v9}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v9}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V

    .line 237142
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v9

    invoke-static {v9}, LX/0x1;->b(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v9}, LX/0x1;->b(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V

    .line 237143
    invoke-static {v7}, LX/0fz;->d(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 237144
    :cond_7
    invoke-virtual {v2, v8}, LX/0fz;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 237145
    :cond_8
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 237146
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_4

    .line 237147
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 237148
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 237149
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 237150
    iget-object v3, v2, LX/0fz;->d:LX/0qm;

    move-object v2, v3

    .line 237151
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 237152
    iput-object v6, v0, LX/23u;->m:Ljava/lang/String;

    .line 237153
    move-object v0, v0

    .line 237154
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0qm;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    goto/16 :goto_1
.end method
