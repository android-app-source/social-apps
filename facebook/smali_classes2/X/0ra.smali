.class public LX/0ra;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;
.implements LX/0rb;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile q:LX/0ra;


# instance fields
.field public a:Z
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final c:LX/0Uo;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field private final e:LX/0SG;

.field private final f:LX/0VT;

.field public final g:LX/0ad;

.field private final h:LX/0Xl;

.field private final i:LX/0Uh;

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ps;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0rf;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/0Yb;

.field private m:LX/0YZ;

.field private n:LX/35Z;

.field private o:J

.field private p:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 149845
    const-class v0, LX/0ra;

    sput-object v0, LX/0ra;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Uo;LX/0Xl;Ljava/util/concurrent/ExecutorService;LX/0SG;LX/0VT;LX/0ad;LX/0Uh;)V
    .locals 3
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 149861
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149862
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 149863
    iput-object v0, p0, LX/0ra;->j:LX/0Ot;

    .line 149864
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0ra;->o:J

    .line 149865
    iput-boolean v2, p0, LX/0ra;->a:Z

    .line 149866
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/0ra;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 149867
    iput-object p1, p0, LX/0ra;->c:LX/0Uo;

    .line 149868
    iput-object p2, p0, LX/0ra;->h:LX/0Xl;

    .line 149869
    iput-object p3, p0, LX/0ra;->d:Ljava/util/concurrent/ExecutorService;

    .line 149870
    iput-object p4, p0, LX/0ra;->e:LX/0SG;

    .line 149871
    iput-object p5, p0, LX/0ra;->f:LX/0VT;

    .line 149872
    iput-object p6, p0, LX/0ra;->g:LX/0ad;

    .line 149873
    iput-object p7, p0, LX/0ra;->i:LX/0Uh;

    .line 149874
    new-instance v0, LX/0S8;

    invoke-direct {v0}, LX/0S8;-><init>()V

    invoke-virtual {v0}, LX/0S8;->e()LX/0S8;

    move-result-object v0

    invoke-virtual {v0}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/0ra;->k:Ljava/util/Map;

    .line 149875
    return-void
.end method

.method public static a(LX/0QB;)LX/0ra;
    .locals 11

    .prologue
    .line 149846
    sget-object v0, LX/0ra;->q:LX/0ra;

    if-nez v0, :cond_1

    .line 149847
    const-class v1, LX/0ra;

    monitor-enter v1

    .line 149848
    :try_start_0
    sget-object v0, LX/0ra;->q:LX/0ra;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 149849
    if-eqz v2, :cond_0

    .line 149850
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 149851
    new-instance v3, LX/0ra;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v4

    check-cast v4, LX/0Uo;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v5

    check-cast v5, LX/0Xl;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {v0}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v8

    check-cast v8, LX/0VT;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-direct/range {v3 .. v10}, LX/0ra;-><init>(LX/0Uo;LX/0Xl;Ljava/util/concurrent/ExecutorService;LX/0SG;LX/0VT;LX/0ad;LX/0Uh;)V

    .line 149852
    const/16 v4, 0x4c1

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    .line 149853
    iput-object v4, v3, LX/0ra;->j:LX/0Ot;

    .line 149854
    move-object v0, v3

    .line 149855
    sput-object v0, LX/0ra;->q:LX/0ra;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 149856
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 149857
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 149858
    :cond_1
    sget-object v0, LX/0ra;->q:LX/0ra;

    return-object v0

    .line 149859
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 149860
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/0ra;LX/32G;)V
    .locals 3

    .prologue
    .line 149835
    iget-object v0, p0, LX/0ra;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/common/memory/MemoryManager$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/common/memory/MemoryManager$3;-><init>(LX/0ra;LX/32G;)V

    const v2, -0x719937ba

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 149836
    return-void
.end method

.method public static e(LX/0ra;)V
    .locals 3

    .prologue
    .line 149837
    iget-object v0, p0, LX/0ra;->g:LX/0ad;

    sget-short v1, LX/2sh;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149838
    sget-object v0, LX/1ac;->a:Ljava/util/Set;

    move-object v0, v0

    .line 149839
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 149840
    iget-object v2, v0, LX/1aX;->g:LX/1ab;

    sget-object p0, LX/1at;->ON_HOLDER_TRIM:LX/1at;

    invoke-virtual {v2, p0}, LX/1ab;->a(LX/1at;)V

    .line 149841
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/1aX;->d:Z

    .line 149842
    invoke-static {v0}, LX/1aX;->n(LX/1aX;)V

    .line 149843
    goto :goto_0

    .line 149844
    :cond_0
    return-void
.end method

.method public static f(LX/0ra;)Z
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 149876
    iget-object v0, p0, LX/0ra;->c:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x1388

    .line 149877
    :goto_0
    iget-object v4, p0, LX/0ra;->e:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    .line 149878
    iget-wide v6, p0, LX/0ra;->o:J

    sub-long v6, v4, v6

    cmp-long v0, v6, v0

    if-ltz v0, :cond_1

    iget-object v0, p0, LX/0ra;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v3, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149879
    iput-wide v4, p0, LX/0ra;->o:J

    move v0, v2

    .line 149880
    :goto_1
    return v0

    .line 149881
    :cond_0
    const-wide/32 v0, 0xea60

    goto :goto_0

    :cond_1
    move v0, v3

    .line 149882
    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 149806
    iget-object v0, p0, LX/0ra;->c:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/32G;->OnSystemLowMemoryWhileAppInBackground:LX/32G;

    .line 149807
    :goto_0
    invoke-static {p0, v0}, LX/0ra;->b(LX/0ra;LX/32G;)V

    .line 149808
    return-void

    .line 149809
    :cond_0
    sget-object v0, LX/32G;->OnSystemLowMemoryWhileAppInForeground:LX/32G;

    goto :goto_0
.end method

.method public final declared-synchronized a(LX/0rf;)V
    .locals 2

    .prologue
    .line 149810
    monitor-enter p0

    :try_start_0
    const-string v0, "MemoryTrimmable cannot be null."

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149811
    iget-object v0, p0, LX/0ra;->k:Ljava/util/Map;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149812
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149813
    monitor-exit p0

    return-void

    .line 149814
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/32G;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 149815
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0ra;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rf;

    .line 149816
    invoke-interface {v0, p1}, LX/0rf;->a(LX/32G;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 149817
    :catchall_0
    move-exception v0

    :try_start_1
    iget-object v1, p0, LX/0ra;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 149818
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 149819
    :cond_0
    :try_start_2
    iget-object v0, p0, LX/0ra;->i:LX/0Uh;

    const/16 v1, 0x477

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_1

    .line 149820
    invoke-static {}, LX/0bc;->a()V

    .line 149821
    invoke-static {}, Landroid/database/sqlite/SQLiteDatabase;->releaseMemory()I

    .line 149822
    :cond_1
    invoke-virtual {p1}, LX/32G;->name()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 149823
    :try_start_3
    iget-object v0, p0, LX/0ra;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 149824
    monitor-exit p0

    return-void
.end method

.method public final init()V
    .locals 4

    .prologue
    .line 149825
    iget-boolean v0, p0, LX/0ra;->a:Z

    if-nez v0, :cond_1

    .line 149826
    new-instance v0, LX/35Z;

    invoke-direct {v0, p0}, LX/35Z;-><init>(LX/0ra;)V

    iput-object v0, p0, LX/0ra;->n:LX/35Z;

    .line 149827
    iget-object v0, p0, LX/0ra;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ps;

    iget-object v1, p0, LX/0ra;->n:LX/35Z;

    .line 149828
    iget-object v2, v0, LX/0ps;->g:Ljava/util/concurrent/ConcurrentMap;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149829
    iget-object v0, p0, LX/0ra;->f:LX/0VT;

    invoke-virtual {v0}, LX/0VT;->a()LX/00G;

    move-result-object v0

    invoke-virtual {v0}, LX/00G;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149830
    new-instance v0, LX/29N;

    invoke-direct {v0, p0}, LX/29N;-><init>(LX/0ra;)V

    iput-object v0, p0, LX/0ra;->m:LX/0YZ;

    .line 149831
    iget-object v0, p0, LX/0ra;->h:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    iget-object v2, p0, LX/0ra;->m:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    iget-object v2, p0, LX/0ra;->m:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/0ra;->l:LX/0Yb;

    .line 149832
    iget-object v0, p0, LX/0ra;->l:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 149833
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0ra;->a:Z

    .line 149834
    :cond_1
    return-void
.end method
