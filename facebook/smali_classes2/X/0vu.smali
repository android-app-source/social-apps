.class public final LX/0vu;
.super Landroid/view/View$AccessibilityDelegate;
.source ""


# instance fields
.field public final synthetic a:LX/0vt;


# direct methods
.method public constructor <init>(LX/0vt;)V
    .locals 0

    .prologue
    .line 158213
    iput-object p1, p0, LX/0vu;->a:LX/0vt;

    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public final dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 158212
    iget-object v0, p0, LX/0vu;->a:LX/0vt;

    invoke-interface {v0, p1, p2}, LX/0vt;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public final getAccessibilityNodeProvider(Landroid/view/View;)Landroid/view/accessibility/AccessibilityNodeProvider;
    .locals 1

    .prologue
    .line 158211
    iget-object v0, p0, LX/0vu;->a:LX/0vt;

    invoke-interface {v0, p1}, LX/0vt;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityNodeProvider;

    return-object v0
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 158209
    iget-object v0, p0, LX/0vu;->a:LX/0vt;

    invoke-interface {v0, p1, p2}, LX/0vt;->b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 158210
    return-void
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 158207
    iget-object v0, p0, LX/0vu;->a:LX/0vt;

    invoke-interface {v0, p1, p2}, LX/0vt;->a(Landroid/view/View;Ljava/lang/Object;)V

    .line 158208
    return-void
.end method

.method public final onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 158214
    iget-object v0, p0, LX/0vu;->a:LX/0vt;

    invoke-interface {v0, p1, p2}, LX/0vt;->c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 158215
    return-void
.end method

.method public final onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 158206
    iget-object v0, p0, LX/0vu;->a:LX/0vt;

    invoke-interface {v0, p1, p2, p3}, LX/0vt;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public final performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 158205
    iget-object v0, p0, LX/0vu;->a:LX/0vt;

    invoke-interface {v0, p1, p2, p3}, LX/0vt;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public final sendAccessibilityEvent(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 158203
    iget-object v0, p0, LX/0vu;->a:LX/0vt;

    invoke-interface {v0, p1, p2}, LX/0vt;->a(Landroid/view/View;I)V

    .line 158204
    return-void
.end method

.method public final sendAccessibilityEventUnchecked(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 158201
    iget-object v0, p0, LX/0vu;->a:LX/0vt;

    invoke-interface {v0, p1, p2}, LX/0vt;->d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 158202
    return-void
.end method
