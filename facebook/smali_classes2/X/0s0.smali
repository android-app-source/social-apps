.class public LX/0s0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0s1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/0s0;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0s9;

.field private final d:LX/0s9;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/0Uo;LX/0s2;LX/0Or;)V
    .locals 3
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/config/server/IsBootstrapEnabled;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/config/application/ApiConnectionType;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Uo;",
            "LX/0s2;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 151107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151108
    iput-object p1, p0, LX/0s0;->a:Landroid/content/Context;

    .line 151109
    iput-object p2, p0, LX/0s0;->b:LX/0Or;

    .line 151110
    new-instance v0, LX/0s8;

    invoke-direct {v0, p0}, LX/0s8;-><init>(LX/0s0;)V

    iput-object v0, p0, LX/0s0;->c:LX/0s9;

    .line 151111
    iput-object p5, p0, LX/0s0;->e:LX/0Or;

    .line 151112
    iget-object v0, p0, LX/0s0;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151113
    new-instance v0, LX/0sB;

    new-instance v1, LX/0sD;

    iget-object v2, p0, LX/0s0;->e:LX/0Or;

    invoke-direct {v1, v2}, LX/0sD;-><init>(LX/0Or;)V

    invoke-virtual {p4}, LX/0s2;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, p3, v2}, LX/0sB;-><init>(LX/0s9;LX/0Uo;Ljava/lang/String;)V

    iput-object v0, p0, LX/0s0;->d:LX/0s9;

    .line 151114
    :goto_0
    return-void

    .line 151115
    :cond_0
    iget-object v0, p0, LX/0s0;->c:LX/0s9;

    iput-object v0, p0, LX/0s0;->d:LX/0s9;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/0s0;
    .locals 9

    .prologue
    .line 151116
    sget-object v0, LX/0s0;->f:LX/0s0;

    if-nez v0, :cond_1

    .line 151117
    const-class v1, LX/0s0;

    monitor-enter v1

    .line 151118
    :try_start_0
    sget-object v0, LX/0s0;->f:LX/0s0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 151119
    if-eqz v2, :cond_0

    .line 151120
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 151121
    new-instance v3, LX/0s0;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x1471

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v6

    check-cast v6, LX/0Uo;

    invoke-static {v0}, LX/0s2;->b(LX/0QB;)LX/0s2;

    move-result-object v7

    check-cast v7, LX/0s2;

    const/16 v8, 0x15ee

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/0s0;-><init>(Landroid/content/Context;LX/0Or;LX/0Uo;LX/0s2;LX/0Or;)V

    .line 151122
    move-object v0, v3

    .line 151123
    sput-object v0, LX/0s0;->f:LX/0s0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151124
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 151125
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 151126
    :cond_1
    sget-object v0, LX/0s0;->f:LX/0s0;

    return-object v0

    .line 151127
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 151128
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0s9;
    .locals 1

    .prologue
    .line 151129
    iget-object v0, p0, LX/0s0;->c:LX/0s9;

    return-object v0
.end method

.method public final b()LX/0s9;
    .locals 1

    .prologue
    .line 151130
    iget-object v0, p0, LX/0s0;->c:LX/0s9;

    return-object v0
.end method

.method public final c()LX/0s9;
    .locals 1

    .prologue
    .line 151131
    iget-object v0, p0, LX/0s0;->d:LX/0s9;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 151132
    iget-object v0, p0, LX/0s0;->a:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, LX/0sG;->a(Landroid/content/Context;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 6

    .prologue
    .line 151133
    iget-object v0, p0, LX/0s0;->a:Landroid/content/Context;

    .line 151134
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 151135
    const-string v2, "[%s/%s;%s/%s;]"

    const-string v3, "FBAN"

    invoke-static {}, LX/0sG;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0sG;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "FBAV"

    invoke-static {v0}, LX/0sG;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/0sG;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, v3, v4, v5, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151136
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 151137
    return-object v0
.end method
