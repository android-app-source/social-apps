.class public final LX/0cr;
.super LX/0Qj;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Qj",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field public static final INSTANCE:LX/0cr;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89429
    new-instance v0, LX/0cr;

    invoke-direct {v0}, LX/0cr;-><init>()V

    sput-object v0, LX/0cr;->INSTANCE:LX/0cr;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 89430
    invoke-direct {p0}, LX/0Qj;-><init>()V

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 89431
    sget-object v0, LX/0cr;->INSTANCE:LX/0cr;

    return-object v0
.end method


# virtual methods
.method public final doEquivalent(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 89432
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final doHash(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 89433
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
