.class public LX/1Sd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/1Se;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1Sd;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/database/sqlite/SQLiteOpenHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 248744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248745
    iput-object p1, p0, LX/1Sd;->a:Landroid/content/Context;

    .line 248746
    new-instance v0, LX/1Sf;

    invoke-direct {v0}, LX/1Sf;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 248747
    new-instance v3, LX/1Sg;

    invoke-virtual {v0}, LX/1Sf;->a()LX/1Sh;

    move-result-object v5

    move-object v4, p1

    move-object v6, v0

    move-object v7, v1

    move v8, v2

    invoke-direct/range {v3 .. v8}, LX/1Sg;-><init>(Landroid/content/Context;LX/1Sh;LX/1Sf;LX/AUO;I)V

    move-object v0, v3

    .line 248748
    iput-object v0, p0, LX/1Sd;->b:Landroid/database/sqlite/SQLiteOpenHelper;

    .line 248749
    return-void
.end method

.method public static a(LX/0QB;)LX/1Sd;
    .locals 5

    .prologue
    .line 248750
    sget-object v0, LX/1Sd;->c:LX/1Sd;

    if-nez v0, :cond_1

    .line 248751
    const-class v1, LX/1Sd;

    monitor-enter v1

    .line 248752
    :try_start_0
    sget-object v0, LX/1Sd;->c:LX/1Sd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 248753
    if-eqz v2, :cond_0

    .line 248754
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 248755
    new-instance v4, LX/1Sd;

    const-class v3, Landroid/content/Context;

    const-class p0, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v3, p0}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v4, v3}, LX/1Sd;-><init>(Landroid/content/Context;)V

    .line 248756
    move-object v0, v4

    .line 248757
    sput-object v0, LX/1Sd;->c:LX/1Sd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 248758
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 248759
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 248760
    :cond_1
    sget-object v0, LX/1Sd;->c:LX/1Sd;

    return-object v0

    .line 248761
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 248762
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 248763
    iget-object v0, p0, LX/1Sd;->b:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public final clearUserData()V
    .locals 2

    .prologue
    .line 248764
    iget-object v0, p0, LX/1Sd;->b:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->close()V

    .line 248765
    iget-object v0, p0, LX/1Sd;->a:Landroid/content/Context;

    new-instance v1, LX/1Sf;

    invoke-direct {v1}, LX/1Sf;-><init>()V

    invoke-virtual {v1}, LX/1Sf;->a()LX/1Sh;

    move-result-object v1

    iget-object v1, v1, LX/1Sh;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    .line 248766
    return-void
.end method
