.class public final LX/1Ov;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:J

.field public b:LX/1Ov;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 242614
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242615
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/1Ov;->a:J

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 242616
    iget-object v0, p0, LX/1Ov;->b:LX/1Ov;

    if-nez v0, :cond_0

    .line 242617
    new-instance v0, LX/1Ov;

    invoke-direct {v0}, LX/1Ov;-><init>()V

    iput-object v0, p0, LX/1Ov;->b:LX/1Ov;

    .line 242618
    :cond_0
    return-void
.end method

.method private e(I)V
    .locals 6

    .prologue
    .line 242609
    const/16 v0, 0x40

    if-lt p1, v0, :cond_1

    .line 242610
    iget-object v0, p0, LX/1Ov;->b:LX/1Ov;

    if-eqz v0, :cond_0

    .line 242611
    iget-object v0, p0, LX/1Ov;->b:LX/1Ov;

    add-int/lit8 v1, p1, -0x40

    invoke-direct {v0, v1}, LX/1Ov;->e(I)V

    .line 242612
    :cond_0
    :goto_0
    return-void

    .line 242613
    :cond_1
    iget-wide v0, p0, LX/1Ov;->a:J

    const-wide/16 v2, 0x1

    shl-long/2addr v2, p1

    const-wide/16 v4, -0x1

    xor-long/2addr v2, v4

    and-long/2addr v0, v2

    iput-wide v0, p0, LX/1Ov;->a:J

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 242605
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/1Ov;->a:J

    .line 242606
    iget-object v0, p0, LX/1Ov;->b:LX/1Ov;

    if-eqz v0, :cond_0

    .line 242607
    iget-object v0, p0, LX/1Ov;->b:LX/1Ov;

    invoke-virtual {v0}, LX/1Ov;->a()V

    .line 242608
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 242619
    const/16 v0, 0x40

    if-lt p1, v0, :cond_0

    .line 242620
    invoke-direct {p0}, LX/1Ov;->b()V

    .line 242621
    iget-object v0, p0, LX/1Ov;->b:LX/1Ov;

    add-int/lit8 v1, p1, -0x40

    invoke-virtual {v0, v1}, LX/1Ov;->a(I)V

    .line 242622
    :goto_0
    return-void

    .line 242623
    :cond_0
    iget-wide v0, p0, LX/1Ov;->a:J

    const-wide/16 v2, 0x1

    shl-long/2addr v2, p1

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/1Ov;->a:J

    goto :goto_0
.end method

.method public final a(IZ)V
    .locals 12

    .prologue
    const-wide/16 v8, 0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 242589
    const/16 v0, 0x40

    if-lt p1, v0, :cond_1

    .line 242590
    invoke-direct {p0}, LX/1Ov;->b()V

    .line 242591
    iget-object v0, p0, LX/1Ov;->b:LX/1Ov;

    add-int/lit8 v1, p1, -0x40

    invoke-virtual {v0, v1, p2}, LX/1Ov;->a(IZ)V

    .line 242592
    :cond_0
    :goto_0
    return-void

    .line 242593
    :cond_1
    iget-wide v4, p0, LX/1Ov;->a:J

    const-wide/high16 v6, -0x8000000000000000L

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_3

    move v0, v1

    .line 242594
    :goto_1
    shl-long v4, v8, p1

    sub-long/2addr v4, v8

    .line 242595
    iget-wide v6, p0, LX/1Ov;->a:J

    and-long/2addr v6, v4

    .line 242596
    iget-wide v8, p0, LX/1Ov;->a:J

    const-wide/16 v10, -0x1

    xor-long/2addr v4, v10

    and-long/2addr v4, v8

    shl-long/2addr v4, v1

    .line 242597
    or-long/2addr v4, v6

    iput-wide v4, p0, LX/1Ov;->a:J

    .line 242598
    if-eqz p2, :cond_4

    .line 242599
    invoke-virtual {p0, p1}, LX/1Ov;->a(I)V

    .line 242600
    :goto_2
    if-nez v0, :cond_2

    iget-object v1, p0, LX/1Ov;->b:LX/1Ov;

    if-eqz v1, :cond_0

    .line 242601
    :cond_2
    invoke-direct {p0}, LX/1Ov;->b()V

    .line 242602
    iget-object v1, p0, LX/1Ov;->b:LX/1Ov;

    invoke-virtual {v1, v2, v0}, LX/1Ov;->a(IZ)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 242603
    goto :goto_1

    .line 242604
    :cond_4
    invoke-direct {p0, p1}, LX/1Ov;->e(I)V

    goto :goto_2
.end method

.method public final b(I)Z
    .locals 4

    .prologue
    .line 242585
    const/16 v0, 0x40

    if-lt p1, v0, :cond_0

    .line 242586
    invoke-direct {p0}, LX/1Ov;->b()V

    .line 242587
    iget-object v0, p0, LX/1Ov;->b:LX/1Ov;

    add-int/lit8 v1, p1, -0x40

    invoke-virtual {v0, v1}, LX/1Ov;->b(I)Z

    move-result v0

    .line 242588
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, LX/1Ov;->a:J

    const-wide/16 v2, 0x1

    shl-long/2addr v2, p1

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)Z
    .locals 14

    .prologue
    const-wide/16 v12, 0x1

    const-wide/16 v10, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 242569
    const/16 v0, 0x40

    if-lt p1, v0, :cond_1

    .line 242570
    invoke-direct {p0}, LX/1Ov;->b()V

    .line 242571
    iget-object v0, p0, LX/1Ov;->b:LX/1Ov;

    add-int/lit8 v1, p1, -0x40

    invoke-virtual {v0, v1}, LX/1Ov;->c(I)Z

    move-result v0

    .line 242572
    :cond_0
    :goto_0
    return v0

    .line 242573
    :cond_1
    shl-long v4, v12, p1

    .line 242574
    iget-wide v6, p0, LX/1Ov;->a:J

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    .line 242575
    :goto_1
    iget-wide v6, p0, LX/1Ov;->a:J

    xor-long v8, v4, v10

    and-long/2addr v6, v8

    iput-wide v6, p0, LX/1Ov;->a:J

    .line 242576
    sub-long/2addr v4, v12

    .line 242577
    iget-wide v6, p0, LX/1Ov;->a:J

    and-long/2addr v6, v4

    .line 242578
    iget-wide v8, p0, LX/1Ov;->a:J

    xor-long/2addr v4, v10

    and-long/2addr v4, v8

    invoke-static {v4, v5, v1}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v4

    .line 242579
    or-long/2addr v4, v6

    iput-wide v4, p0, LX/1Ov;->a:J

    .line 242580
    iget-object v1, p0, LX/1Ov;->b:LX/1Ov;

    if-eqz v1, :cond_0

    .line 242581
    iget-object v1, p0, LX/1Ov;->b:LX/1Ov;

    invoke-virtual {v1, v2}, LX/1Ov;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 242582
    const/16 v1, 0x3f

    invoke-virtual {p0, v1}, LX/1Ov;->a(I)V

    .line 242583
    :cond_2
    iget-object v1, p0, LX/1Ov;->b:LX/1Ov;

    invoke-virtual {v1, v2}, LX/1Ov;->c(I)Z

    goto :goto_0

    :cond_3
    move v0, v2

    .line 242584
    goto :goto_1
.end method

.method public final d(I)I
    .locals 6

    .prologue
    const/16 v1, 0x40

    const-wide/16 v4, 0x1

    .line 242560
    iget-object v0, p0, LX/1Ov;->b:LX/1Ov;

    if-nez v0, :cond_1

    .line 242561
    if-lt p1, v1, :cond_0

    .line 242562
    iget-wide v0, p0, LX/1Ov;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->bitCount(J)I

    move-result v0

    .line 242563
    :goto_0
    return v0

    .line 242564
    :cond_0
    iget-wide v0, p0, LX/1Ov;->a:J

    shl-long v2, v4, p1

    sub-long/2addr v2, v4

    and-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->bitCount(J)I

    move-result v0

    goto :goto_0

    .line 242565
    :cond_1
    if-ge p1, v1, :cond_2

    .line 242566
    iget-wide v0, p0, LX/1Ov;->a:J

    shl-long v2, v4, p1

    sub-long/2addr v2, v4

    and-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->bitCount(J)I

    move-result v0

    goto :goto_0

    .line 242567
    :cond_2
    iget-object v0, p0, LX/1Ov;->b:LX/1Ov;

    add-int/lit8 v1, p1, -0x40

    invoke-virtual {v0, v1}, LX/1Ov;->d(I)I

    move-result v0

    iget-wide v2, p0, LX/1Ov;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->bitCount(J)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 242568
    iget-object v0, p0, LX/1Ov;->b:LX/1Ov;

    if-nez v0, :cond_0

    iget-wide v0, p0, LX/1Ov;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toBinaryString(J)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/1Ov;->b:LX/1Ov;

    invoke-virtual {v1}, LX/1Ov;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "xx"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, LX/1Ov;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toBinaryString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
