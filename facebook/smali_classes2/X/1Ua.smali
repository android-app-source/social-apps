.class public LX/1Ua;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final a:LX/1Ua;

.field public static final b:LX/1Ua;

.field public static final c:LX/1Ua;

.field public static final d:LX/1Ua;

.field public static final e:LX/1Ua;

.field public static final f:LX/1Ua;

.field public static final g:LX/1Ua;

.field public static final h:LX/1Ua;

.field public static final i:LX/1Ua;

.field public static final j:LX/1Ua;

.field public static final k:LX/1Ua;

.field public static final l:LX/1Ua;

.field public static final m:LX/1Ua;

.field public static final n:LX/1Ua;

.field public static final o:LX/1Ua;

.field public static final p:LX/1Ua;

.field public static final q:LX/1Ua;

.field public static final r:LX/1Ua;


# instance fields
.field public final s:LX/1UZ;

.field public final t:LX/1Ub;

.field public final u:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/high16 v9, 0x41400000    # 12.0f

    const/high16 v8, 0x41200000    # 10.0f

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 256281
    new-instance v0, LX/1Ua;

    sget-object v1, LX/1UZ;->DEFAULT:LX/1UZ;

    invoke-direct {v0, v1}, LX/1Ua;-><init>(LX/1UZ;)V

    sput-object v0, LX/1Ua;->a:LX/1Ua;

    .line 256282
    new-instance v0, LX/1Ua;

    sget-object v1, LX/1UZ;->DEFAULT:LX/1UZ;

    new-instance v2, LX/1Ub;

    invoke-direct {v2}, LX/1Ub;-><init>()V

    invoke-direct {v0, v1, v2, v10}, LX/1Ua;-><init>(LX/1UZ;LX/1Ub;Z)V

    sput-object v0, LX/1Ua;->b:LX/1Ua;

    .line 256283
    new-instance v0, LX/1Ua;

    sget-object v1, LX/1UZ;->SHARED_ATTACHMENT:LX/1UZ;

    new-instance v2, LX/1Ub;

    const/high16 v3, -0x3fc00000    # -3.0f

    new-instance v4, LX/1Ue;

    const/high16 v5, 0x41500000    # 13.0f

    invoke-direct {v4, v5, v6}, LX/1Ue;-><init>(FF)V

    invoke-direct {v2, v6, v3, v6, v4}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-direct {v0, v1, v2, v7}, LX/1Ua;-><init>(LX/1UZ;LX/1Ub;Z)V

    sput-object v0, LX/1Ua;->c:LX/1Ua;

    .line 256284
    new-instance v0, LX/1Ua;

    sget-object v1, LX/1UZ;->DEFAULT_TEXT:LX/1UZ;

    invoke-direct {v0, v1}, LX/1Ua;-><init>(LX/1UZ;)V

    sput-object v0, LX/1Ua;->d:LX/1Ua;

    .line 256285
    new-instance v0, LX/1Ua;

    sget-object v1, LX/1UZ;->ZERO:LX/1UZ;

    invoke-direct {v0, v1}, LX/1Ua;-><init>(LX/1UZ;)V

    sput-object v0, LX/1Ua;->e:LX/1Ua;

    .line 256286
    new-instance v0, LX/1Ua;

    sget-object v1, LX/1UZ;->ZERO:LX/1UZ;

    new-instance v2, LX/1Ub;

    invoke-direct {v2}, LX/1Ub;-><init>()V

    invoke-direct {v0, v1, v2, v10}, LX/1Ua;-><init>(LX/1UZ;LX/1Ub;Z)V

    sput-object v0, LX/1Ua;->f:LX/1Ua;

    .line 256287
    new-instance v0, LX/1Ua;

    sget-object v1, LX/1UZ;->ZERO:LX/1UZ;

    new-instance v2, LX/1Ub;

    new-instance v3, LX/1Ue;

    invoke-direct {v3, v8, v9}, LX/1Ue;-><init>(FF)V

    invoke-direct {v2, v6, v6, v6, v3}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-direct {v0, v1, v2, v7}, LX/1Ua;-><init>(LX/1UZ;LX/1Ub;Z)V

    sput-object v0, LX/1Ua;->g:LX/1Ua;

    .line 256288
    new-instance v0, LX/1Ua;

    sget-object v1, LX/1UZ;->DEFAULT_HEADER:LX/1UZ;

    invoke-direct {v0, v1}, LX/1Ua;-><init>(LX/1UZ;)V

    sput-object v0, LX/1Ua;->h:LX/1Ua;

    .line 256289
    new-instance v0, LX/1Ua;

    sget-object v1, LX/1UZ;->DEFAULT_TEXT_HEADER:LX/1UZ;

    invoke-direct {v0, v1}, LX/1Ua;-><init>(LX/1UZ;)V

    sput-object v0, LX/1Ua;->i:LX/1Ua;

    .line 256290
    new-instance v0, LX/1Ua;

    sget-object v1, LX/1UZ;->SUBSTORY_HEADER:LX/1UZ;

    invoke-direct {v0, v1}, LX/1Ua;-><init>(LX/1UZ;)V

    sput-object v0, LX/1Ua;->j:LX/1Ua;

    .line 256291
    new-instance v0, LX/1Ua;

    sget-object v1, LX/1UZ;->ATTACHMENT_BOTTOM:LX/1UZ;

    new-instance v2, LX/1Ub;

    new-instance v3, LX/1Ue;

    invoke-direct {v3, v9, v6}, LX/1Ue;-><init>(FF)V

    invoke-direct {v2, v6, v6, v6, v3}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-direct {v0, v1, v2, v7}, LX/1Ua;-><init>(LX/1UZ;LX/1Ub;Z)V

    sput-object v0, LX/1Ua;->k:LX/1Ua;

    .line 256292
    new-instance v0, LX/1Ua;

    sget-object v1, LX/1UZ;->ATTACHMENT_TOP:LX/1UZ;

    new-instance v2, LX/1Ub;

    new-instance v3, LX/1Ue;

    invoke-direct {v3, v8, v9}, LX/1Ue;-><init>(FF)V

    invoke-direct {v2, v6, v6, v6, v3}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-direct {v0, v1, v2, v7}, LX/1Ua;-><init>(LX/1UZ;LX/1Ub;Z)V

    sput-object v0, LX/1Ua;->l:LX/1Ua;

    .line 256293
    new-instance v0, LX/1Ua;

    sget-object v1, LX/1UZ;->ATTACHMENT_BOTTOM:LX/1UZ;

    new-instance v2, LX/1Ub;

    new-instance v3, LX/1Ue;

    invoke-direct {v3, v8, v9}, LX/1Ue;-><init>(FF)V

    invoke-direct {v2, v6, v6, v6, v3}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-direct {v0, v1, v2, v7}, LX/1Ua;-><init>(LX/1UZ;LX/1Ub;Z)V

    sput-object v0, LX/1Ua;->m:LX/1Ua;

    .line 256294
    new-instance v0, LX/1Ua;

    sget-object v1, LX/1UZ;->ZERO:LX/1UZ;

    new-instance v2, LX/1Ub;

    const/high16 v3, -0x3f800000    # -4.0f

    new-instance v4, LX/1Ue;

    invoke-direct {v4, v8, v6}, LX/1Ue;-><init>(FF)V

    invoke-direct {v2, v6, v3, v6, v4}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-direct {v0, v1, v2, v10}, LX/1Ua;-><init>(LX/1UZ;LX/1Ub;Z)V

    sput-object v0, LX/1Ua;->n:LX/1Ua;

    .line 256295
    new-instance v0, LX/1Ua;

    sget-object v1, LX/1UZ;->DEFAULT:LX/1UZ;

    new-instance v2, LX/1Ub;

    new-instance v3, LX/1Ue;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-direct {v3, v4, v6}, LX/1Ue;-><init>(FF)V

    invoke-direct {v2, v6, v6, v6, v3}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-direct {v0, v1, v2, v7}, LX/1Ua;-><init>(LX/1UZ;LX/1Ub;Z)V

    sput-object v0, LX/1Ua;->o:LX/1Ua;

    .line 256296
    new-instance v0, LX/1Ua;

    sget-object v1, LX/1UZ;->LEGACY_DEFAULT:LX/1UZ;

    invoke-direct {v0, v1}, LX/1Ua;-><init>(LX/1UZ;)V

    sput-object v0, LX/1Ua;->p:LX/1Ua;

    .line 256297
    new-instance v0, LX/1Ua;

    sget-object v1, LX/1UZ;->LEGACY_ZERO:LX/1UZ;

    invoke-direct {v0, v1}, LX/1Ua;-><init>(LX/1UZ;)V

    sput-object v0, LX/1Ua;->q:LX/1Ua;

    .line 256298
    new-instance v0, LX/1Ua;

    sget-object v1, LX/1UZ;->STORY_EDGE:LX/1UZ;

    invoke-direct {v0, v1}, LX/1Ua;-><init>(LX/1UZ;)V

    sput-object v0, LX/1Ua;->r:LX/1Ua;

    return-void
.end method

.method private constructor <init>(LX/1UZ;)V
    .locals 1

    .prologue
    .line 256276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256277
    iput-object p1, p0, LX/1Ua;->s:LX/1UZ;

    .line 256278
    new-instance v0, LX/1Ub;

    invoke-direct {v0}, LX/1Ub;-><init>()V

    iput-object v0, p0, LX/1Ua;->t:LX/1Ub;

    .line 256279
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Ua;->u:Z

    .line 256280
    return-void
.end method

.method public constructor <init>(LX/1UZ;LX/1Ub;Z)V
    .locals 0

    .prologue
    .line 256271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256272
    iput-object p1, p0, LX/1Ua;->s:LX/1UZ;

    .line 256273
    iput-object p2, p0, LX/1Ua;->t:LX/1Ub;

    .line 256274
    iput-boolean p3, p0, LX/1Ua;->u:Z

    .line 256275
    return-void
.end method


# virtual methods
.method public final d()Z
    .locals 1

    .prologue
    .line 256269
    iget-object v0, p0, LX/1Ua;->s:LX/1UZ;

    invoke-virtual {v0}, LX/1UZ;->isZero()Z

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 256270
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/1Ua;->s:LX/1UZ;

    invoke-virtual {v1}, LX/1UZ;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, LX/1Ua;->u:Z

    if-eqz v0, :cond_0

    const-string v0, " w/shadowPadder Values:"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1Ua;->t:LX/1Ub;

    invoke-virtual {v1}, LX/1Ub;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, " values:"

    goto :goto_0
.end method
