.class public interface abstract LX/1MF;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract cancel()Z
.end method

.method public abstract changePriority(Lcom/facebook/http/interfaces/RequestPriority;)Z
.end method

.method public abstract dispose()V
.end method

.method public abstract getCallerContext()Lcom/facebook/common/callercontext/CallerContext;
.end method

.method public abstract getCanRunBeforeAppInit()Z
.end method

.method public abstract getFireAndForget()Z
.end method

.method public abstract getOperationProgressIndicator()LX/4At;
.end method

.method public abstract getOperationType()Ljava/lang/String;
.end method

.method public abstract getParam()Landroid/os/Bundle;
.end method

.method public abstract isDisposed()Z
.end method

.method public abstract isRunning()Z
.end method

.method public abstract setCanRunBeforeAppInit(Z)LX/1MF;
.end method

.method public abstract setFireAndForget(Z)LX/1MF;
.end method

.method public abstract setOnProgressListener(LX/4An;)LX/1MF;
.end method

.method public abstract setOperationProgressIndicator(LX/4At;)LX/1MF;
.end method

.method public abstract start()LX/1ML;
.end method

.method public abstract startOnMainThread()LX/1ML;
.end method

.method public abstract startTryNotToUseMainThread()LX/1ML;
.end method
