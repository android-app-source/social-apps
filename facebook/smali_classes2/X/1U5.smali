.class public LX/1U5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 255489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255490
    iput-object p1, p0, LX/1U5;->a:LX/0Ot;

    .line 255491
    return-void
.end method

.method public static b(LX/0QB;)LX/1U5;
    .locals 2

    .prologue
    .line 255492
    new-instance v0, LX/1U5;

    const/16 v1, 0xfb8

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1U5;-><init>(LX/0Ot;)V

    .line 255493
    return-object v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 255494
    sget-object v0, Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255495
    sget-object v0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255496
    sget-object v0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255497
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 255498
    const-class v0, LX/1U6;

    iget-object v1, p0, LX/1U5;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 255499
    return-void
.end method
