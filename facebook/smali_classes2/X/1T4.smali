.class public final LX/1T4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1TA;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1TA;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 249666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 249667
    iput-object p1, p0, LX/1T4;->a:LX/0QB;

    .line 249668
    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/1TA;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249665
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/1T4;

    invoke-direct {v2, p0}, LX/1T4;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 249669
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/1T4;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 249620
    packed-switch p2, :pswitch_data_0

    .line 249621
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 249622
    :pswitch_0
    invoke-static {p1}, LX/1T9;->a(LX/0QB;)LX/1T9;

    move-result-object v0

    .line 249623
    :goto_0
    return-object v0

    .line 249624
    :pswitch_1
    invoke-static {p1}, LX/1TB;->a(LX/0QB;)LX/1TB;

    move-result-object v0

    goto :goto_0

    .line 249625
    :pswitch_2
    invoke-static {p1}, LX/1TC;->a(LX/0QB;)LX/1TC;

    move-result-object v0

    goto :goto_0

    .line 249626
    :pswitch_3
    invoke-static {p1}, LX/1TD;->a(LX/0QB;)LX/1TD;

    move-result-object v0

    goto :goto_0

    .line 249627
    :pswitch_4
    invoke-static {p1}, LX/1TF;->a(LX/0QB;)LX/1TF;

    move-result-object v0

    goto :goto_0

    .line 249628
    :pswitch_5
    invoke-static {p1}, LX/1TH;->a(LX/0QB;)LX/1TH;

    move-result-object v0

    goto :goto_0

    .line 249629
    :pswitch_6
    invoke-static {p1}, LX/1TI;->a(LX/0QB;)LX/1TI;

    move-result-object v0

    goto :goto_0

    .line 249630
    :pswitch_7
    invoke-static {p1}, LX/1TJ;->a(LX/0QB;)LX/1TJ;

    move-result-object v0

    goto :goto_0

    .line 249631
    :pswitch_8
    invoke-static {p1}, LX/1TK;->a(LX/0QB;)LX/1TK;

    move-result-object v0

    goto :goto_0

    .line 249632
    :pswitch_9
    invoke-static {p1}, LX/1TL;->a(LX/0QB;)LX/1TL;

    move-result-object v0

    goto :goto_0

    .line 249633
    :pswitch_a
    invoke-static {p1}, LX/1TM;->a(LX/0QB;)LX/1TM;

    move-result-object v0

    goto :goto_0

    .line 249634
    :pswitch_b
    invoke-static {p1}, LX/1TN;->a(LX/0QB;)LX/1TN;

    move-result-object v0

    goto :goto_0

    .line 249635
    :pswitch_c
    invoke-static {p1}, LX/1TO;->a(LX/0QB;)LX/1TO;

    move-result-object v0

    goto :goto_0

    .line 249636
    :pswitch_d
    invoke-static {p1}, LX/1TP;->a(LX/0QB;)LX/1TP;

    move-result-object v0

    goto :goto_0

    .line 249637
    :pswitch_e
    invoke-static {p1}, LX/1TQ;->a(LX/0QB;)LX/1TQ;

    move-result-object v0

    goto :goto_0

    .line 249638
    :pswitch_f
    invoke-static {p1}, LX/1TS;->a(LX/0QB;)LX/1TS;

    move-result-object v0

    goto :goto_0

    .line 249639
    :pswitch_10
    invoke-static {p1}, LX/1TT;->a(LX/0QB;)LX/1TT;

    move-result-object v0

    goto :goto_0

    .line 249640
    :pswitch_11
    invoke-static {p1}, LX/1TV;->a(LX/0QB;)LX/1TV;

    move-result-object v0

    goto :goto_0

    .line 249641
    :pswitch_12
    invoke-static {p1}, LX/1TW;->a(LX/0QB;)LX/1TW;

    move-result-object v0

    goto :goto_0

    .line 249642
    :pswitch_13
    invoke-static {p1}, LX/1TX;->a(LX/0QB;)LX/1TX;

    move-result-object v0

    goto :goto_0

    .line 249643
    :pswitch_14
    invoke-static {p1}, LX/1TY;->a(LX/0QB;)LX/1TY;

    move-result-object v0

    goto :goto_0

    .line 249644
    :pswitch_15
    invoke-static {p1}, LX/1TZ;->a(LX/0QB;)LX/1TZ;

    move-result-object v0

    goto :goto_0

    .line 249645
    :pswitch_16
    invoke-static {p1}, LX/1Ta;->a(LX/0QB;)LX/1Ta;

    move-result-object v0

    goto :goto_0

    .line 249646
    :pswitch_17
    invoke-static {p1}, LX/1Tb;->a(LX/0QB;)LX/1Tb;

    move-result-object v0

    goto :goto_0

    .line 249647
    :pswitch_18
    invoke-static {p1}, LX/1Tc;->a(LX/0QB;)LX/1Tc;

    move-result-object v0

    goto :goto_0

    .line 249648
    :pswitch_19
    invoke-static {p1}, LX/1Td;->a(LX/0QB;)LX/1Td;

    move-result-object v0

    goto :goto_0

    .line 249649
    :pswitch_1a
    invoke-static {p1}, LX/1Te;->a(LX/0QB;)LX/1Te;

    move-result-object v0

    goto/16 :goto_0

    .line 249650
    :pswitch_1b
    invoke-static {p1}, LX/1Tf;->a(LX/0QB;)LX/1Tf;

    move-result-object v0

    goto/16 :goto_0

    .line 249651
    :pswitch_1c
    invoke-static {p1}, LX/1Tg;->a(LX/0QB;)LX/1Tg;

    move-result-object v0

    goto/16 :goto_0

    .line 249652
    :pswitch_1d
    invoke-static {p1}, LX/1Th;->a(LX/0QB;)LX/1Th;

    move-result-object v0

    goto/16 :goto_0

    .line 249653
    :pswitch_1e
    invoke-static {p1}, LX/1Ti;->a(LX/0QB;)LX/1Ti;

    move-result-object v0

    goto/16 :goto_0

    .line 249654
    :pswitch_1f
    invoke-static {p1}, LX/1Tj;->a(LX/0QB;)LX/1Tj;

    move-result-object v0

    goto/16 :goto_0

    .line 249655
    :pswitch_20
    invoke-static {p1}, LX/1Tk;->a(LX/0QB;)LX/1Tk;

    move-result-object v0

    goto/16 :goto_0

    .line 249656
    :pswitch_21
    invoke-static {p1}, LX/1Tl;->a(LX/0QB;)LX/1Tl;

    move-result-object v0

    goto/16 :goto_0

    .line 249657
    :pswitch_22
    invoke-static {p1}, LX/1Tm;->a(LX/0QB;)LX/1Tm;

    move-result-object v0

    goto/16 :goto_0

    .line 249658
    :pswitch_23
    invoke-static {p1}, LX/1Tn;->a(LX/0QB;)LX/1Tn;

    move-result-object v0

    goto/16 :goto_0

    .line 249659
    :pswitch_24
    invoke-static {p1}, LX/1U1;->a(LX/0QB;)LX/1U1;

    move-result-object v0

    goto/16 :goto_0

    .line 249660
    :pswitch_25
    invoke-static {p1}, LX/1U2;->a(LX/0QB;)LX/1U2;

    move-result-object v0

    goto/16 :goto_0

    .line 249661
    :pswitch_26
    invoke-static {p1}, LX/1U3;->a(LX/0QB;)LX/1U3;

    move-result-object v0

    goto/16 :goto_0

    .line 249662
    :pswitch_27
    invoke-static {p1}, LX/1U4;->a(LX/0QB;)LX/1U4;

    move-result-object v0

    goto/16 :goto_0

    .line 249663
    :pswitch_28
    invoke-static {p1}, LX/1U5;->b(LX/0QB;)LX/1U5;

    move-result-object v0

    goto/16 :goto_0

    .line 249664
    :pswitch_29
    invoke-static {p1}, LX/1U9;->a(LX/0QB;)LX/1U9;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 249619
    const/16 v0, 0x2a

    return v0
.end method
