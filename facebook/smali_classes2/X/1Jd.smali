.class public LX/1Jd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/Object;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1BN;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Ljava/util/Set;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/Set",
            "<",
            "LX/1BN;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 230219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230220
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/1Jd;->a:Ljava/lang/Object;

    .line 230221
    iput-object p1, p0, LX/1Jd;->b:Ljava/util/concurrent/ExecutorService;

    .line 230222
    iput-object p2, p0, LX/1Jd;->c:Ljava/util/Set;

    .line 230223
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/prefetch/PrefetchParams;)V
    .locals 4

    .prologue
    .line 230224
    iget-object v1, p0, LX/1Jd;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 230225
    :try_start_0
    iget-object v0, p0, LX/1Jd;->d:Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Jd;->d:Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;

    .line 230226
    iget-boolean v2, v0, Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;->c:Z

    move v0, v2

    .line 230227
    if-eqz v0, :cond_0

    .line 230228
    iget-object v0, p0, LX/1Jd;->d:Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;

    invoke-static {v0, p1}, Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;->a$redex0(Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;Lcom/facebook/photos/prefetch/PrefetchParams;)V

    .line 230229
    monitor-exit v1

    .line 230230
    :goto_0
    return-void

    .line 230231
    :cond_0
    new-instance v0, Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;-><init>(LX/1Jd;)V

    iput-object v0, p0, LX/1Jd;->d:Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;

    .line 230232
    iget-object v0, p0, LX/1Jd;->d:Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;

    invoke-static {v0, p1}, Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;->a$redex0(Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;Lcom/facebook/photos/prefetch/PrefetchParams;)V

    .line 230233
    iget-object v0, p0, LX/1Jd;->b:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, LX/1Jd;->d:Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;

    const v3, 0x4db80bb9    # 3.85972E8f

    invoke-static {v0, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 230234
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
