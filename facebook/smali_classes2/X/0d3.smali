.class public final LX/0d3;
.super Ljava/util/concurrent/locks/ReentrantLock;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/locks/ReentrantLock;"
    }
.end annotation


# instance fields
.field public volatile count:I

.field public final evictionQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/0qF",
            "<TK;TV;>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final expirationQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/0qF",
            "<TK;TV;>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final keyReferenceQueue:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;"
        }
    .end annotation
.end field

.field public final map:LX/0cn;
    .annotation build Lcom/google/j2objc/annotations/Weak;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0cn",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final maxSegmentSize:I

.field public modCount:I

.field public final readCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final recencyQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/0qF",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field public volatile table:Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceArray",
            "<",
            "LX/0qF",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field public threshold:I

.field public final valueReferenceQueue:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0cn;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0cn",
            "<TK;TV;>;II)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 89838
    invoke-direct {p0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    .line 89839
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, LX/0d3;->readCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 89840
    iput-object p1, p0, LX/0d3;->map:LX/0cn;

    .line 89841
    iput p3, p0, LX/0d3;->maxSegmentSize:I

    .line 89842
    invoke-static {p2}, LX/0d3;->a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-result-object v0

    .line 89843
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result p2

    mul-int/lit8 p2, p2, 0x3

    div-int/lit8 p2, p2, 0x4

    iput p2, p0, LX/0d3;->threshold:I

    .line 89844
    iget p2, p0, LX/0d3;->threshold:I

    iget p3, p0, LX/0d3;->maxSegmentSize:I

    if-ne p2, p3, :cond_0

    .line 89845
    iget p2, p0, LX/0d3;->threshold:I

    add-int/lit8 p2, p2, 0x1

    iput p2, p0, LX/0d3;->threshold:I

    .line 89846
    :cond_0
    iput-object v0, p0, LX/0d3;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 89847
    invoke-virtual {p1}, LX/0cn;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :goto_0
    iput-object v0, p0, LX/0d3;->keyReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    .line 89848
    invoke-virtual {p1}, LX/0cn;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v1, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v1}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :cond_1
    iput-object v1, p0, LX/0d3;->valueReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    .line 89849
    invoke-virtual {p1}, LX/0cn;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, LX/0cn;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    :goto_1
    iput-object v0, p0, LX/0d3;->recencyQueue:Ljava/util/Queue;

    .line 89850
    invoke-virtual {p1}, LX/0cn;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, LX/4zT;

    invoke-direct {v0}, LX/4zT;-><init>()V

    :goto_2
    iput-object v0, p0, LX/0d3;->evictionQueue:Ljava/util/Queue;

    .line 89851
    invoke-virtual {p1}, LX/0cn;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, LX/4zW;

    invoke-direct {v0}, LX/4zW;-><init>()V

    :goto_3
    iput-object v0, p0, LX/0d3;->expirationQueue:Ljava/util/Queue;

    .line 89852
    return-void

    :cond_3
    move-object v0, v1

    .line 89853
    goto :goto_0

    .line 89854
    :cond_4
    sget-object v0, LX/0cn;->f:Ljava/util/Queue;

    move-object v0, v0

    .line 89855
    goto :goto_1

    .line 89856
    :cond_5
    sget-object v0, LX/0cn;->f:Ljava/util/Queue;

    move-object v0, v0

    .line 89857
    goto :goto_2

    .line 89858
    :cond_6
    sget-object v0, LX/0cn;->f:Ljava/util/Queue;

    move-object v0, v0

    .line 89859
    goto :goto_3
.end method

.method private static a(LX/0d3;LX/0qF;LX/0qF;)LX/0qF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;",
            "LX/0qF",
            "<TK;TV;>;)",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 89860
    invoke-interface {p1}, LX/0qF;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 89861
    :cond_0
    :goto_0
    return-object v0

    .line 89862
    :cond_1
    invoke-interface {p1}, LX/0qF;->getValueReference()LX/0cp;

    move-result-object v1

    .line 89863
    invoke-interface {v1}, LX/0cp;->get()Ljava/lang/Object;

    move-result-object v2

    .line 89864
    if-nez v2, :cond_2

    invoke-interface {v1}, LX/0cp;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 89865
    :cond_2
    iget-object v0, p0, LX/0d3;->map:LX/0cn;

    iget-object v0, v0, LX/0cn;->d:LX/0cs;

    invoke-virtual {v0, p0, p1, p2}, LX/0cs;->copyEntry(LX/0d3;LX/0qF;LX/0qF;)LX/0qF;

    move-result-object v0

    .line 89866
    iget-object v3, p0, LX/0d3;->valueReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-interface {v1, v3, v2, v0}, LX/0cp;->a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;LX/0qF;)LX/0cp;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0qF;->setValueReference(LX/0cp;)V

    goto :goto_0
.end method

.method private static a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/concurrent/atomic/AtomicReferenceArray",
            "<",
            "LX/0qF",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 89867
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-direct {v0, p0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;-><init>(I)V

    return-object v0
.end method

.method public static a(LX/0d3;LX/0qF;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;J)V"
        }
    .end annotation

    .prologue
    .line 89868
    iget-object v0, p0, LX/0d3;->map:LX/0cn;

    iget-object v0, v0, LX/0cn;->ticker:LX/0QV;

    invoke-virtual {v0}, LX/0QV;->read()J

    move-result-wide v0

    add-long/2addr v0, p2

    invoke-interface {p1, v0, v1}, LX/0qF;->setExpirationTime(J)V

    .line 89869
    return-void
.end method

.method private static a(LX/0d3;LX/0qF;LX/18j;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;",
            "LX/18j;",
            ")V"
        }
    .end annotation

    .prologue
    .line 89836
    invoke-interface {p1}, LX/0qF;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1}, LX/0qF;->getHash()I

    invoke-interface {p1}, LX/0qF;->getValueReference()LX/0cp;

    move-result-object v1

    invoke-interface {v1}, LX/0cp;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {p0, v0, v1, p2}, LX/0d3;->a(LX/0d3;Ljava/lang/Object;Ljava/lang/Object;LX/18j;)V

    .line 89837
    return-void
.end method

.method private static a(LX/0d3;LX/0qF;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;TV;)V"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 89870
    iget-object v0, p0, LX/0d3;->map:LX/0cn;

    iget-object v0, v0, LX/0cn;->valueStrength:LX/0ci;

    invoke-virtual {v0, p0, p1, p2}, LX/0ci;->referenceValue(LX/0d3;LX/0qF;Ljava/lang/Object;)LX/0cp;

    move-result-object v0

    .line 89871
    invoke-interface {p1, v0}, LX/0qF;->setValueReference(LX/0cp;)V

    .line 89872
    invoke-static {p0}, LX/0d3;->j(LX/0d3;)V

    .line 89873
    iget-object v1, p0, LX/0d3;->evictionQueue:Ljava/util/Queue;

    invoke-interface {v1, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 89874
    iget-object v1, p0, LX/0d3;->map:LX/0cn;

    invoke-virtual {v1}, LX/0cn;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89875
    iget-object v1, p0, LX/0d3;->map:LX/0cn;

    invoke-virtual {v1}, LX/0cn;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/0d3;->map:LX/0cn;

    iget-wide v1, v1, LX/0cn;->expireAfterAccessNanos:J

    .line 89876
    :goto_0
    invoke-static {p0, p1, v1, v2}, LX/0d3;->a(LX/0d3;LX/0qF;J)V

    .line 89877
    iget-object v1, p0, LX/0d3;->expirationQueue:Ljava/util/Queue;

    invoke-interface {v1, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 89878
    :cond_0
    return-void

    .line 89879
    :cond_1
    iget-object v1, p0, LX/0d3;->map:LX/0cn;

    iget-wide v1, v1, LX/0cn;->expireAfterWriteNanos:J

    goto :goto_0
.end method

.method private static a(LX/0d3;Ljava/lang/Object;Ljava/lang/Object;LX/18j;)V
    .locals 2
    .param p0    # LX/0d3;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;",
            "LX/18j;",
            ")V"
        }
    .end annotation

    .prologue
    .line 89880
    iget-object v0, p0, LX/0d3;->map:LX/0cn;

    iget-object v0, v0, LX/0cn;->removalNotificationQueue:Ljava/util/Queue;

    sget-object v1, LX/0cn;->f:Ljava/util/Queue;

    if-eq v0, v1, :cond_0

    .line 89881
    new-instance v0, LX/4zM;

    invoke-direct {v0, p1, p2, p3}, LX/4zM;-><init>(Ljava/lang/Object;Ljava/lang/Object;LX/18j;)V

    .line 89882
    iget-object v1, p0, LX/0d3;->map:LX/0cn;

    iget-object v1, v1, LX/0cn;->removalNotificationQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 89883
    :cond_0
    return-void
.end method

.method private static a(LX/0cp;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0cp",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 89884
    invoke-interface {p0}, LX/0cp;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 89885
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p0}, LX/0cp;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(LX/0qF;ILX/18j;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;I",
            "LX/18j;",
            ")Z"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 89886
    iget-object v2, p0, LX/0d3;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 89887
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v3, p2, v0

    .line 89888
    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qF;

    move-object v1, v0

    .line 89889
    :goto_0
    if-eqz v1, :cond_1

    .line 89890
    if-ne v1, p1, :cond_0

    .line 89891
    iget v4, p0, LX/0d3;->modCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/0d3;->modCount:I

    .line 89892
    invoke-interface {v1}, LX/0qF;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1}, LX/0qF;->getValueReference()LX/0cp;

    move-result-object v5

    invoke-interface {v5}, LX/0cp;->get()Ljava/lang/Object;

    move-result-object v5

    invoke-static {p0, v4, v5, p3}, LX/0d3;->a(LX/0d3;Ljava/lang/Object;Ljava/lang/Object;LX/18j;)V

    .line 89893
    invoke-static {p0, v0, v1}, LX/0d3;->b(LX/0d3;LX/0qF;LX/0qF;)LX/0qF;

    move-result-object v0

    .line 89894
    iget v1, p0, LX/0d3;->count:I

    add-int/lit8 v1, v1, -0x1

    .line 89895
    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 89896
    iput v1, p0, LX/0d3;->count:I

    .line 89897
    const/4 v0, 0x1

    .line 89898
    :goto_1
    return v0

    .line 89899
    :cond_0
    invoke-interface {v1}, LX/0qF;->getNext()LX/0qF;

    move-result-object v1

    goto :goto_0

    .line 89900
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static b(LX/0d3;LX/0qF;LX/0qF;)LX/0qF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;",
            "LX/0qF",
            "<TK;TV;>;)",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 89962
    iget-object v0, p0, LX/0d3;->evictionQueue:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 89963
    iget-object v0, p0, LX/0d3;->expirationQueue:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 89964
    iget v2, p0, LX/0d3;->count:I

    .line 89965
    invoke-interface {p2}, LX/0qF;->getNext()LX/0qF;

    move-result-object v1

    .line 89966
    :goto_0
    if-eq p1, p2, :cond_1

    .line 89967
    invoke-static {p0, p1, v1}, LX/0d3;->a(LX/0d3;LX/0qF;LX/0qF;)LX/0qF;

    move-result-object v0

    .line 89968
    if-eqz v0, :cond_0

    move v1, v2

    .line 89969
    :goto_1
    invoke-interface {p1}, LX/0qF;->getNext()LX/0qF;

    move-result-object p1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 89970
    :cond_0
    invoke-static {p0, p1}, LX/0d3;->e(LX/0d3;LX/0qF;)V

    .line 89971
    add-int/lit8 v0, v2, -0x1

    move-object v3, v1

    move v1, v0

    move-object v0, v3

    goto :goto_1

    .line 89972
    :cond_1
    iput v2, p0, LX/0d3;->count:I

    .line 89973
    return-object v1
.end method

.method public static c(LX/0d3;)V
    .locals 1

    .prologue
    .line 89901
    invoke-virtual {p0}, LX/0d3;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89902
    :try_start_0
    invoke-direct {p0}, LX/0d3;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89903
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89904
    :cond_0
    return-void

    .line 89905
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0d3;->unlock()V

    throw v0
.end method

.method private static c(LX/0d3;LX/0qF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 89957
    iget-object v0, p0, LX/0d3;->evictionQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 89958
    iget-object v0, p0, LX/0d3;->map:LX/0cn;

    invoke-virtual {v0}, LX/0cn;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89959
    iget-object v0, p0, LX/0d3;->map:LX/0cn;

    iget-wide v0, v0, LX/0cn;->expireAfterAccessNanos:J

    invoke-static {p0, p1, v0, v1}, LX/0d3;->a(LX/0d3;LX/0qF;J)V

    .line 89960
    iget-object v0, p0, LX/0d3;->expirationQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 89961
    :cond_0
    return-void
.end method

.method private d()V
    .locals 6
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 89937
    iget-object v0, p0, LX/0d3;->map:LX/0cn;

    invoke-virtual {v0}, LX/0cn;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89938
    const/4 v0, 0x0

    move v1, v0

    .line 89939
    :goto_0
    iget-object v0, p0, LX/0d3;->keyReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 89940
    check-cast v0, LX/0qF;

    .line 89941
    iget-object v2, p0, LX/0d3;->map:LX/0cn;

    .line 89942
    invoke-interface {v0}, LX/0qF;->getHash()I

    move-result v3

    .line 89943
    invoke-static {v2, v3}, LX/0cn;->b(LX/0cn;I)LX/0d3;

    move-result-object v4

    invoke-virtual {v4, v0, v3}, LX/0d3;->a(LX/0qF;I)Z

    .line 89944
    add-int/lit8 v0, v1, 0x1

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    move v1, v0

    .line 89945
    goto :goto_0

    .line 89946
    :cond_0
    iget-object v0, p0, LX/0d3;->map:LX/0cn;

    invoke-virtual {v0}, LX/0cn;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89947
    const/4 v0, 0x0

    move v1, v0

    .line 89948
    :goto_1
    iget-object v0, p0, LX/0d3;->valueReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 89949
    check-cast v0, LX/0cp;

    .line 89950
    iget-object v2, p0, LX/0d3;->map:LX/0cn;

    .line 89951
    invoke-interface {v0}, LX/0cp;->a()LX/0qF;

    move-result-object v3

    .line 89952
    invoke-interface {v3}, LX/0qF;->getHash()I

    move-result v4

    .line 89953
    invoke-static {v2, v4}, LX/0cn;->b(LX/0cn;I)LX/0d3;

    move-result-object v5

    invoke-interface {v3}, LX/0qF;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v5, v3, v4, v0}, LX/0d3;->a(Ljava/lang/Object;ILX/0cp;)Z

    .line 89954
    add-int/lit8 v0, v1, 0x1

    const/16 v1, 0x10

    if-eq v0, v1, :cond_1

    move v1, v0

    .line 89955
    goto :goto_1

    .line 89956
    :cond_1
    return-void
.end method

.method private static e(LX/0d3;Ljava/lang/Object;I)LX/0qF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 89921
    iget v1, p0, LX/0d3;->count:I

    if-eqz v1, :cond_4

    .line 89922
    iget-object v1, p0, LX/0d3;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 89923
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    and-int/2addr v2, p2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0qF;

    move-object v1, v1

    .line 89924
    :goto_0
    if-eqz v1, :cond_4

    .line 89925
    invoke-interface {v1}, LX/0qF;->getHash()I

    move-result v2

    if-ne v2, p2, :cond_0

    .line 89926
    invoke-interface {v1}, LX/0qF;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 89927
    if-nez v2, :cond_1

    .line 89928
    invoke-static {p0}, LX/0d3;->c(LX/0d3;)V

    .line 89929
    :cond_0
    invoke-interface {v1}, LX/0qF;->getNext()LX/0qF;

    move-result-object v1

    goto :goto_0

    .line 89930
    :cond_1
    iget-object v3, p0, LX/0d3;->map:LX/0cn;

    iget-object v3, v3, LX/0cn;->keyEquivalence:LX/0Qj;

    invoke-virtual {v3, p1, v2}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89931
    :goto_1
    move-object v1, v1

    .line 89932
    if-nez v1, :cond_2

    .line 89933
    :goto_2
    return-object v0

    .line 89934
    :cond_2
    iget-object v2, p0, LX/0d3;->map:LX/0cn;

    invoke-virtual {v2}, LX/0cn;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/0d3;->map:LX/0cn;

    invoke-virtual {v2, v1}, LX/0cn;->c(LX/0qF;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 89935
    invoke-static {p0}, LX/0d3;->k(LX/0d3;)V

    goto :goto_2

    :cond_3
    move-object v0, v1

    .line 89936
    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static e(LX/0d3;LX/0qF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 89917
    sget-object v0, LX/18j;->COLLECTED:LX/18j;

    invoke-static {p0, p1, v0}, LX/0d3;->a(LX/0d3;LX/0qF;LX/18j;)V

    .line 89918
    iget-object v0, p0, LX/0d3;->evictionQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 89919
    iget-object v0, p0, LX/0d3;->expirationQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 89920
    return-void
.end method

.method public static j(LX/0d3;)V
    .locals 2
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 89911
    :cond_0
    :goto_0
    iget-object v0, p0, LX/0d3;->recencyQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qF;

    if-eqz v0, :cond_2

    .line 89912
    iget-object v1, p0, LX/0d3;->evictionQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 89913
    iget-object v1, p0, LX/0d3;->evictionQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 89914
    :cond_1
    iget-object v1, p0, LX/0d3;->map:LX/0cn;

    invoke-virtual {v1}, LX/0cn;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0d3;->expirationQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89915
    iget-object v1, p0, LX/0d3;->expirationQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 89916
    :cond_2
    return-void
.end method

.method private static k(LX/0d3;)V
    .locals 1

    .prologue
    .line 89906
    invoke-virtual {p0}, LX/0d3;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89907
    :try_start_0
    invoke-direct {p0}, LX/0d3;->l()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89908
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89909
    :cond_0
    return-void

    .line 89910
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0d3;->unlock()V

    throw v0
.end method

.method private l()V
    .locals 5
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 89829
    invoke-static {p0}, LX/0d3;->j(LX/0d3;)V

    .line 89830
    iget-object v0, p0, LX/0d3;->expirationQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89831
    :cond_0
    return-void

    .line 89832
    :cond_1
    iget-object v0, p0, LX/0d3;->map:LX/0cn;

    iget-object v0, v0, LX/0cn;->ticker:LX/0QV;

    invoke-virtual {v0}, LX/0QV;->read()J

    move-result-wide v2

    .line 89833
    :cond_2
    iget-object v0, p0, LX/0d3;->expirationQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qF;

    if-eqz v0, :cond_0

    invoke-static {v0, v2, v3}, LX/0cn;->a(LX/0qF;J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89834
    invoke-interface {v0}, LX/0qF;->getHash()I

    move-result v1

    sget-object v4, LX/18j;->EXPIRED:LX/18j;

    invoke-direct {p0, v0, v1, v4}, LX/0d3;->a(LX/0qF;ILX/18j;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 89835
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private m()Z
    .locals 3
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 89502
    iget-object v0, p0, LX/0d3;->map:LX/0cn;

    invoke-virtual {v0}, LX/0cn;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, LX/0d3;->count:I

    iget v1, p0, LX/0d3;->maxSegmentSize:I

    if-lt v0, v1, :cond_1

    .line 89503
    invoke-static {p0}, LX/0d3;->j(LX/0d3;)V

    .line 89504
    iget-object v0, p0, LX/0d3;->evictionQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qF;

    .line 89505
    invoke-interface {v0}, LX/0qF;->getHash()I

    move-result v1

    sget-object v2, LX/18j;->SIZE:LX/18j;

    invoke-direct {p0, v0, v1, v2}, LX/0d3;->a(LX/0qF;ILX/18j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 89506
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 89507
    :cond_0
    const/4 v0, 0x1

    .line 89508
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()V
    .locals 11
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 89509
    iget-object v7, p0, LX/0d3;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 89510
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v8

    .line 89511
    const/high16 v0, 0x40000000    # 2.0f

    if-lt v8, v0, :cond_0

    .line 89512
    :goto_0
    return-void

    .line 89513
    :cond_0
    iget v5, p0, LX/0d3;->count:I

    .line 89514
    shl-int/lit8 v0, v8, 0x1

    invoke-static {v0}, LX/0d3;->a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-result-object v9

    .line 89515
    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/0d3;->threshold:I

    .line 89516
    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    .line 89517
    const/4 v0, 0x0

    move v6, v0

    :goto_1
    if-ge v6, v8, :cond_5

    .line 89518
    invoke-virtual {v7, v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qF;

    .line 89519
    if-eqz v0, :cond_7

    .line 89520
    invoke-interface {v0}, LX/0qF;->getNext()LX/0qF;

    move-result-object v3

    .line 89521
    invoke-interface {v0}, LX/0qF;->getHash()I

    move-result v1

    and-int v2, v1, v10

    .line 89522
    if-nez v3, :cond_2

    .line 89523
    invoke-virtual {v9, v2, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v1, v5

    .line 89524
    :cond_1
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v5, v1

    goto :goto_1

    :cond_2
    move-object v4, v0

    .line 89525
    :goto_3
    if-eqz v3, :cond_3

    .line 89526
    invoke-interface {v3}, LX/0qF;->getHash()I

    move-result v1

    and-int/2addr v1, v10

    .line 89527
    if-eq v1, v2, :cond_6

    move-object v2, v3

    .line 89528
    :goto_4
    invoke-interface {v3}, LX/0qF;->getNext()LX/0qF;

    move-result-object v3

    move-object v4, v2

    move v2, v1

    goto :goto_3

    .line 89529
    :cond_3
    invoke-virtual {v9, v2, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move-object v2, v0

    move v1, v5

    .line 89530
    :goto_5
    if-eq v2, v4, :cond_1

    .line 89531
    invoke-interface {v2}, LX/0qF;->getHash()I

    move-result v0

    and-int v3, v0, v10

    .line 89532
    invoke-virtual {v9, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qF;

    .line 89533
    invoke-static {p0, v2, v0}, LX/0d3;->a(LX/0d3;LX/0qF;LX/0qF;)LX/0qF;

    move-result-object v0

    .line 89534
    if-eqz v0, :cond_4

    .line 89535
    invoke-virtual {v9, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v0, v1

    .line 89536
    :goto_6
    invoke-interface {v2}, LX/0qF;->getNext()LX/0qF;

    move-result-object v1

    move-object v2, v1

    move v1, v0

    goto :goto_5

    .line 89537
    :cond_4
    invoke-static {p0, v2}, LX/0d3;->e(LX/0d3;LX/0qF;)V

    .line 89538
    add-int/lit8 v0, v1, -0x1

    goto :goto_6

    .line 89539
    :cond_5
    iput-object v9, p0, LX/0d3;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 89540
    iput v5, p0, LX/0d3;->count:I

    goto :goto_0

    :cond_6
    move v1, v2

    move-object v2, v4

    goto :goto_4

    :cond_7
    move v1, v5

    goto :goto_2
.end method

.method public static r(LX/0d3;)V
    .locals 2

    .prologue
    .line 89541
    invoke-virtual {p0}, LX/0d3;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89542
    :try_start_0
    invoke-direct {p0}, LX/0d3;->d()V

    .line 89543
    invoke-direct {p0}, LX/0d3;->l()V

    .line 89544
    iget-object v0, p0, LX/0d3;->readCount:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89545
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89546
    :cond_0
    return-void

    .line 89547
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0d3;->unlock()V

    throw v0
.end method

.method public static s(LX/0d3;)V
    .locals 4

    .prologue
    .line 89548
    invoke-virtual {p0}, LX/0d3;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 89549
    iget-object v0, p0, LX/0d3;->map:LX/0cn;

    .line 89550
    :goto_0
    iget-object v1, v0, LX/0cn;->removalNotificationQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4zM;

    if-eqz v1, :cond_0

    .line 89551
    :try_start_0
    iget-object v2, v0, LX/0cn;->removalListener:LX/0d2;

    invoke-interface {v2, v1}, LX/0d2;->onRemoval(LX/4zM;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 89552
    :catch_0
    move-exception v1

    .line 89553
    sget-object v2, LX/0cn;->j:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string p0, "Exception thrown by removal listener"

    invoke-virtual {v2, v3, p0, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 89554
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0qF;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)TV;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 89555
    invoke-interface {p1}, LX/0qF;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 89556
    invoke-static {p0}, LX/0d3;->c(LX/0d3;)V

    .line 89557
    :goto_0
    return-object v0

    .line 89558
    :cond_0
    invoke-interface {p1}, LX/0qF;->getValueReference()LX/0cp;

    move-result-object v1

    invoke-interface {v1}, LX/0cp;->get()Ljava/lang/Object;

    move-result-object v1

    .line 89559
    if-nez v1, :cond_1

    .line 89560
    invoke-static {p0}, LX/0d3;->c(LX/0d3;)V

    goto :goto_0

    .line 89561
    :cond_1
    iget-object v2, p0, LX/0d3;->map:LX/0cn;

    invoke-virtual {v2}, LX/0cn;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/0d3;->map:LX/0cn;

    invoke-virtual {v2, p1}, LX/0cn;->c(LX/0qF;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 89562
    invoke-static {p0}, LX/0d3;->k(LX/0d3;)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 89563
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)TV;"
        }
    .end annotation

    .prologue
    .line 89564
    :try_start_0
    invoke-static {p0, p1, p2}, LX/0d3;->e(LX/0d3;Ljava/lang/Object;I)LX/0qF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 89565
    if-nez v1, :cond_0

    .line 89566
    invoke-virtual {p0}, LX/0d3;->b()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 89567
    :cond_0
    :try_start_1
    invoke-interface {v1}, LX/0qF;->getValueReference()LX/0cp;

    move-result-object v0

    invoke-interface {v0}, LX/0cp;->get()Ljava/lang/Object;

    move-result-object v0

    .line 89568
    if-eqz v0, :cond_2

    .line 89569
    iget-object v2, p0, LX/0d3;->map:LX/0cn;

    invoke-virtual {v2}, LX/0cn;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 89570
    iget-object v2, p0, LX/0d3;->map:LX/0cn;

    iget-wide v2, v2, LX/0cn;->expireAfterAccessNanos:J

    invoke-static {p0, v1, v2, v3}, LX/0d3;->a(LX/0d3;LX/0qF;J)V

    .line 89571
    :cond_1
    iget-object v2, p0, LX/0d3;->recencyQueue:Ljava/util/Queue;

    invoke-interface {v2, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89572
    :goto_1
    invoke-virtual {p0}, LX/0d3;->b()V

    goto :goto_0

    .line 89573
    :cond_2
    :try_start_2
    invoke-static {p0}, LX/0d3;->c(LX/0d3;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 89574
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0d3;->b()V

    throw v0
.end method

.method public final a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;)TV;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 89575
    invoke-virtual {p0}, LX/0d3;->lock()V

    .line 89576
    :try_start_0
    invoke-static {p0}, LX/0d3;->r(LX/0d3;)V

    .line 89577
    iget-object v4, p0, LX/0d3;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 89578
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    .line 89579
    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qF;

    move-object v3, v0

    .line 89580
    :goto_0
    if-eqz v3, :cond_3

    .line 89581
    invoke-interface {v3}, LX/0qF;->getKey()Ljava/lang/Object;

    move-result-object v6

    .line 89582
    invoke-interface {v3}, LX/0qF;->getHash()I

    move-result v2

    if-ne v2, p2, :cond_2

    if-eqz v6, :cond_2

    iget-object v2, p0, LX/0d3;->map:LX/0cn;

    iget-object v2, v2, LX/0cn;->keyEquivalence:LX/0Qj;

    invoke-virtual {v2, p1, v6}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 89583
    invoke-interface {v3}, LX/0qF;->getValueReference()LX/0cp;

    move-result-object v7

    .line 89584
    invoke-interface {v7}, LX/0cp;->get()Ljava/lang/Object;

    move-result-object v2

    .line 89585
    if-nez v2, :cond_1

    .line 89586
    invoke-static {v7}, LX/0d3;->a(LX/0cp;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 89587
    iget v7, p0, LX/0d3;->modCount:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, LX/0d3;->modCount:I

    .line 89588
    sget-object v7, LX/18j;->COLLECTED:LX/18j;

    invoke-static {p0, v6, v2, v7}, LX/0d3;->a(LX/0d3;Ljava/lang/Object;Ljava/lang/Object;LX/18j;)V

    .line 89589
    invoke-static {p0, v0, v3}, LX/0d3;->b(LX/0d3;LX/0qF;LX/0qF;)LX/0qF;

    move-result-object v0

    .line 89590
    iget v2, p0, LX/0d3;->count:I

    add-int/lit8 v2, v2, -0x1

    .line 89591
    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 89592
    iput v2, p0, LX/0d3;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89593
    :cond_0
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89594
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    move-object v0, v1

    :goto_1
    return-object v0

    .line 89595
    :cond_1
    :try_start_1
    iget v0, p0, LX/0d3;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0d3;->modCount:I

    .line 89596
    sget-object v0, LX/18j;->REPLACED:LX/18j;

    invoke-static {p0, p1, v2, v0}, LX/0d3;->a(LX/0d3;Ljava/lang/Object;Ljava/lang/Object;LX/18j;)V

    .line 89597
    invoke-static {p0, v3, p3}, LX/0d3;->a(LX/0d3;LX/0qF;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89598
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89599
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    move-object v0, v2

    goto :goto_1

    .line 89600
    :cond_2
    :try_start_2
    invoke-interface {v3}, LX/0qF;->getNext()LX/0qF;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    move-object v3, v2

    goto :goto_0

    .line 89601
    :cond_3
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89602
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    move-object v0, v1

    goto :goto_1

    .line 89603
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89604
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    throw v0
.end method

.method public final a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;Z)TV;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 89605
    invoke-virtual {p0}, LX/0d3;->lock()V

    .line 89606
    :try_start_0
    invoke-static {p0}, LX/0d3;->r(LX/0d3;)V

    .line 89607
    iget v0, p0, LX/0d3;->count:I

    add-int/lit8 v2, v0, 0x1

    .line 89608
    iget v0, p0, LX/0d3;->threshold:I

    if-le v2, v0, :cond_0

    .line 89609
    invoke-direct {p0}, LX/0d3;->n()V

    .line 89610
    iget v0, p0, LX/0d3;->count:I

    add-int/lit8 v2, v0, 0x1

    .line 89611
    :cond_0
    iget-object v4, p0, LX/0d3;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 89612
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    .line 89613
    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qF;

    move-object v3, v0

    .line 89614
    :goto_0
    if-eqz v3, :cond_6

    .line 89615
    invoke-interface {v3}, LX/0qF;->getKey()Ljava/lang/Object;

    move-result-object v6

    .line 89616
    invoke-interface {v3}, LX/0qF;->getHash()I

    move-result v7

    if-ne v7, p2, :cond_5

    if-eqz v6, :cond_5

    iget-object v7, p0, LX/0d3;->map:LX/0cn;

    iget-object v7, v7, LX/0cn;->keyEquivalence:LX/0Qj;

    invoke-virtual {v7, p1, v6}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 89617
    invoke-interface {v3}, LX/0qF;->getValueReference()LX/0cp;

    move-result-object v4

    .line 89618
    invoke-interface {v4}, LX/0cp;->get()Ljava/lang/Object;

    move-result-object v0

    .line 89619
    if-nez v0, :cond_3

    .line 89620
    iget v5, p0, LX/0d3;->modCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, LX/0d3;->modCount:I

    .line 89621
    invoke-static {p0, v3, p3}, LX/0d3;->a(LX/0d3;LX/0qF;Ljava/lang/Object;)V

    .line 89622
    invoke-interface {v4}, LX/0cp;->b()Z

    move-result v3

    if-nez v3, :cond_2

    .line 89623
    sget-object v2, LX/18j;->COLLECTED:LX/18j;

    invoke-static {p0, p1, v0, v2}, LX/0d3;->a(LX/0d3;Ljava/lang/Object;Ljava/lang/Object;LX/18j;)V

    .line 89624
    iget v2, p0, LX/0d3;->count:I

    .line 89625
    :cond_1
    :goto_1
    iput v2, p0, LX/0d3;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89626
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89627
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    move-object v0, v1

    :goto_2
    return-object v0

    .line 89628
    :cond_2
    :try_start_1
    invoke-direct {p0}, LX/0d3;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89629
    iget v0, p0, LX/0d3;->count:I

    add-int/lit8 v2, v0, 0x1

    goto :goto_1

    .line 89630
    :cond_3
    if-eqz p4, :cond_4

    .line 89631
    invoke-static {p0, v3}, LX/0d3;->c(LX/0d3;LX/0qF;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89632
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89633
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    goto :goto_2

    .line 89634
    :cond_4
    :try_start_2
    iget v1, p0, LX/0d3;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0d3;->modCount:I

    .line 89635
    sget-object v1, LX/18j;->REPLACED:LX/18j;

    invoke-static {p0, p1, v0, v1}, LX/0d3;->a(LX/0d3;Ljava/lang/Object;Ljava/lang/Object;LX/18j;)V

    .line 89636
    invoke-static {p0, v3, p3}, LX/0d3;->a(LX/0d3;LX/0qF;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 89637
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89638
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    goto :goto_2

    .line 89639
    :cond_5
    :try_start_3
    invoke-interface {v3}, LX/0qF;->getNext()LX/0qF;

    move-result-object v3

    goto :goto_0

    .line 89640
    :cond_6
    iget v3, p0, LX/0d3;->modCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/0d3;->modCount:I

    .line 89641
    iget-object v3, p0, LX/0d3;->map:LX/0cn;

    iget-object v3, v3, LX/0cn;->d:LX/0cs;

    invoke-virtual {v3, p0, p1, p2, v0}, LX/0cs;->newEntry(LX/0d3;Ljava/lang/Object;ILX/0qF;)LX/0qF;

    move-result-object v3

    move-object v0, v3

    .line 89642
    invoke-static {p0, v0, p3}, LX/0d3;->a(LX/0d3;LX/0qF;Ljava/lang/Object;)V

    .line 89643
    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 89644
    invoke-direct {p0}, LX/0d3;->m()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 89645
    iget v0, p0, LX/0d3;->count:I

    add-int/lit8 v0, v0, 0x1

    .line 89646
    :goto_3
    iput v0, p0, LX/0d3;->count:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 89647
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89648
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    move-object v0, v1

    goto :goto_2

    .line 89649
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89650
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    throw v0

    :cond_7
    move v0, v2

    goto :goto_3
.end method

.method public final a()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 89651
    iget v0, p0, LX/0d3;->count:I

    if-eqz v0, :cond_8

    .line 89652
    invoke-virtual {p0}, LX/0d3;->lock()V

    .line 89653
    :try_start_0
    iget-object v3, p0, LX/0d3;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 89654
    iget-object v0, p0, LX/0d3;->map:LX/0cn;

    iget-object v0, v0, LX/0cn;->removalNotificationQueue:Ljava/util/Queue;

    sget-object v2, LX/0cn;->f:Ljava/util/Queue;

    if-eq v0, v2, :cond_2

    move v2, v1

    .line 89655
    :goto_0
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 89656
    invoke-virtual {v3, v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qF;

    :goto_1
    if-eqz v0, :cond_1

    .line 89657
    invoke-interface {v0}, LX/0qF;->getValueReference()LX/0cp;

    move-result-object v4

    invoke-interface {v4}, LX/0cp;->b()Z

    move-result v4

    if-nez v4, :cond_0

    .line 89658
    sget-object v4, LX/18j;->EXPLICIT:LX/18j;

    invoke-static {p0, v0, v4}, LX/0d3;->a(LX/0d3;LX/0qF;LX/18j;)V

    .line 89659
    :cond_0
    invoke-interface {v0}, LX/0qF;->getNext()LX/0qF;

    move-result-object v0

    goto :goto_1

    .line 89660
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 89661
    :goto_2
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 89662
    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 89663
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 89664
    :cond_3
    iget-object v0, p0, LX/0d3;->map:LX/0cn;

    invoke-virtual {v0}, LX/0cn;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 89665
    :cond_4
    iget-object v0, p0, LX/0d3;->keyReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_4

    .line 89666
    :cond_5
    iget-object v0, p0, LX/0d3;->map:LX/0cn;

    invoke-virtual {v0}, LX/0cn;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 89667
    :cond_6
    iget-object v0, p0, LX/0d3;->valueReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_6

    .line 89668
    :cond_7
    iget-object v0, p0, LX/0d3;->evictionQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 89669
    iget-object v0, p0, LX/0d3;->expirationQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 89670
    iget-object v0, p0, LX/0d3;->readCount:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 89671
    iget v0, p0, LX/0d3;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0d3;->modCount:I

    .line 89672
    const/4 v0, 0x0

    iput v0, p0, LX/0d3;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89673
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89674
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    .line 89675
    :cond_8
    return-void

    .line 89676
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89677
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    throw v0
.end method

.method public final a(LX/0qF;I)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;I)Z"
        }
    .end annotation

    .prologue
    .line 89678
    invoke-virtual {p0}, LX/0d3;->lock()V

    .line 89679
    :try_start_0
    iget-object v2, p0, LX/0d3;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 89680
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v3, p2, v0

    .line 89681
    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qF;

    move-object v1, v0

    .line 89682
    :goto_0
    if-eqz v1, :cond_1

    .line 89683
    if-ne v1, p1, :cond_0

    .line 89684
    iget v4, p0, LX/0d3;->modCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/0d3;->modCount:I

    .line 89685
    invoke-interface {v1}, LX/0qF;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1}, LX/0qF;->getValueReference()LX/0cp;

    move-result-object v5

    invoke-interface {v5}, LX/0cp;->get()Ljava/lang/Object;

    move-result-object v5

    sget-object v6, LX/18j;->COLLECTED:LX/18j;

    invoke-static {p0, v4, v5, v6}, LX/0d3;->a(LX/0d3;Ljava/lang/Object;Ljava/lang/Object;LX/18j;)V

    .line 89686
    invoke-static {p0, v0, v1}, LX/0d3;->b(LX/0d3;LX/0qF;LX/0qF;)LX/0qF;

    move-result-object v0

    .line 89687
    iget v1, p0, LX/0d3;->count:I

    add-int/lit8 v1, v1, -0x1

    .line 89688
    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 89689
    iput v1, p0, LX/0d3;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89690
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89691
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 89692
    :cond_0
    :try_start_1
    invoke-interface {v1}, LX/0qF;->getNext()LX/0qF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 89693
    :cond_1
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89694
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    const/4 v0, 0x0

    goto :goto_1

    .line 89695
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89696
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    throw v0
.end method

.method public final a(Ljava/lang/Object;ILX/0cp;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LX/0cp",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 89697
    invoke-virtual {p0}, LX/0d3;->lock()V

    .line 89698
    :try_start_0
    iget-object v3, p0, LX/0d3;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 89699
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    .line 89700
    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qF;

    move-object v2, v0

    .line 89701
    :goto_0
    if-eqz v2, :cond_4

    .line 89702
    invoke-interface {v2}, LX/0qF;->getKey()Ljava/lang/Object;

    move-result-object v5

    .line 89703
    invoke-interface {v2}, LX/0qF;->getHash()I

    move-result v6

    if-ne v6, p2, :cond_3

    if-eqz v5, :cond_3

    iget-object v6, p0, LX/0d3;->map:LX/0cn;

    iget-object v6, v6, LX/0cn;->keyEquivalence:LX/0Qj;

    invoke-virtual {v6, p1, v5}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 89704
    invoke-interface {v2}, LX/0qF;->getValueReference()LX/0cp;

    move-result-object v5

    .line 89705
    if-ne v5, p3, :cond_1

    .line 89706
    iget v1, p0, LX/0d3;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0d3;->modCount:I

    .line 89707
    invoke-interface {p3}, LX/0cp;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v5, LX/18j;->COLLECTED:LX/18j;

    invoke-static {p0, p1, v1, v5}, LX/0d3;->a(LX/0d3;Ljava/lang/Object;Ljava/lang/Object;LX/18j;)V

    .line 89708
    invoke-static {p0, v0, v2}, LX/0d3;->b(LX/0d3;LX/0qF;LX/0qF;)LX/0qF;

    move-result-object v0

    .line 89709
    iget v1, p0, LX/0d3;->count:I

    add-int/lit8 v1, v1, -0x1

    .line 89710
    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 89711
    iput v1, p0, LX/0d3;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89712
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89713
    invoke-virtual {p0}, LX/0d3;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 89714
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 89715
    :cond_1
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89716
    invoke-virtual {p0}, LX/0d3;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_2

    .line 89717
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    :cond_2
    move v0, v1

    goto :goto_1

    .line 89718
    :cond_3
    :try_start_1
    invoke-interface {v2}, LX/0qF;->getNext()LX/0qF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 89719
    :cond_4
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89720
    invoke-virtual {p0}, LX/0d3;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_5

    .line 89721
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    :cond_5
    move v0, v1

    goto :goto_1

    .line 89722
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89723
    invoke-virtual {p0}, LX/0d3;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_6

    .line 89724
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    :cond_6
    throw v0
.end method

.method public final a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;TV;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 89725
    invoke-virtual {p0}, LX/0d3;->lock()V

    .line 89726
    :try_start_0
    invoke-static {p0}, LX/0d3;->r(LX/0d3;)V

    .line 89727
    iget-object v3, p0, LX/0d3;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 89728
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    .line 89729
    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qF;

    move-object v2, v0

    .line 89730
    :goto_0
    if-eqz v2, :cond_4

    .line 89731
    invoke-interface {v2}, LX/0qF;->getKey()Ljava/lang/Object;

    move-result-object v5

    .line 89732
    invoke-interface {v2}, LX/0qF;->getHash()I

    move-result v6

    if-ne v6, p2, :cond_3

    if-eqz v5, :cond_3

    iget-object v6, p0, LX/0d3;->map:LX/0cn;

    iget-object v6, v6, LX/0cn;->keyEquivalence:LX/0Qj;

    invoke-virtual {v6, p1, v5}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 89733
    invoke-interface {v2}, LX/0qF;->getValueReference()LX/0cp;

    move-result-object v6

    .line 89734
    invoke-interface {v6}, LX/0cp;->get()Ljava/lang/Object;

    move-result-object v7

    .line 89735
    if-nez v7, :cond_1

    .line 89736
    invoke-static {v6}, LX/0d3;->a(LX/0cp;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 89737
    iget v6, p0, LX/0d3;->modCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, LX/0d3;->modCount:I

    .line 89738
    sget-object v6, LX/18j;->COLLECTED:LX/18j;

    invoke-static {p0, v5, v7, v6}, LX/0d3;->a(LX/0d3;Ljava/lang/Object;Ljava/lang/Object;LX/18j;)V

    .line 89739
    invoke-static {p0, v0, v2}, LX/0d3;->b(LX/0d3;LX/0qF;LX/0qF;)LX/0qF;

    move-result-object v0

    .line 89740
    iget v2, p0, LX/0d3;->count:I

    add-int/lit8 v2, v2, -0x1

    .line 89741
    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 89742
    iput v2, p0, LX/0d3;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89743
    :cond_0
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89744
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    move v0, v1

    :goto_1
    return v0

    .line 89745
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/0d3;->map:LX/0cn;

    iget-object v0, v0, LX/0cn;->valueEquivalence:LX/0Qj;

    invoke-virtual {v0, p3, v7}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 89746
    iget v0, p0, LX/0d3;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0d3;->modCount:I

    .line 89747
    sget-object v0, LX/18j;->REPLACED:LX/18j;

    invoke-static {p0, p1, v7, v0}, LX/0d3;->a(LX/0d3;Ljava/lang/Object;Ljava/lang/Object;LX/18j;)V

    .line 89748
    invoke-static {p0, v2, p4}, LX/0d3;->a(LX/0d3;LX/0qF;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89749
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89750
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    const/4 v0, 0x1

    goto :goto_1

    .line 89751
    :cond_2
    :try_start_2
    invoke-static {p0, v2}, LX/0d3;->c(LX/0d3;LX/0qF;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 89752
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89753
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    move v0, v1

    goto :goto_1

    .line 89754
    :cond_3
    :try_start_3
    invoke-interface {v2}, LX/0qF;->getNext()LX/0qF;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 89755
    :cond_4
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89756
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    move v0, v1

    goto :goto_1

    .line 89757
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89758
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    throw v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 89759
    iget-object v0, p0, LX/0d3;->readCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    and-int/lit8 v0, v0, 0x3f

    if-nez v0, :cond_0

    .line 89760
    invoke-static {p0}, LX/0d3;->r(LX/0d3;)V

    .line 89761
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    .line 89762
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 89763
    :try_start_0
    iget v1, p0, LX/0d3;->count:I

    if-eqz v1, :cond_2

    .line 89764
    invoke-static {p0, p1, p2}, LX/0d3;->e(LX/0d3;Ljava/lang/Object;I)LX/0qF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 89765
    if-nez v1, :cond_0

    .line 89766
    invoke-virtual {p0}, LX/0d3;->b()V

    :goto_0
    return v0

    .line 89767
    :cond_0
    :try_start_1
    invoke-interface {v1}, LX/0qF;->getValueReference()LX/0cp;

    move-result-object v1

    invoke-interface {v1}, LX/0cp;->get()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 89768
    :cond_1
    invoke-virtual {p0}, LX/0d3;->b()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, LX/0d3;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0d3;->b()V

    throw v0
.end method

.method public final b(Ljava/lang/Object;ILjava/lang/Object;)Z
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 89769
    invoke-virtual {p0}, LX/0d3;->lock()V

    .line 89770
    :try_start_0
    invoke-static {p0}, LX/0d3;->r(LX/0d3;)V

    .line 89771
    iget-object v4, p0, LX/0d3;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 89772
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    .line 89773
    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qF;

    move-object v3, v0

    .line 89774
    :goto_0
    if-eqz v3, :cond_4

    .line 89775
    invoke-interface {v3}, LX/0qF;->getKey()Ljava/lang/Object;

    move-result-object v6

    .line 89776
    invoke-interface {v3}, LX/0qF;->getHash()I

    move-result v2

    if-ne v2, p2, :cond_3

    if-eqz v6, :cond_3

    iget-object v2, p0, LX/0d3;->map:LX/0cn;

    iget-object v2, v2, LX/0cn;->keyEquivalence:LX/0Qj;

    invoke-virtual {v2, p1, v6}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 89777
    invoke-interface {v3}, LX/0qF;->getValueReference()LX/0cp;

    move-result-object v2

    .line 89778
    invoke-interface {v2}, LX/0cp;->get()Ljava/lang/Object;

    move-result-object v7

    .line 89779
    iget-object v8, p0, LX/0d3;->map:LX/0cn;

    iget-object v8, v8, LX/0cn;->valueEquivalence:LX/0Qj;

    invoke-virtual {v8, p3, v7}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 89780
    sget-object v2, LX/18j;->EXPLICIT:LX/18j;

    .line 89781
    :goto_1
    iget v8, p0, LX/0d3;->modCount:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, LX/0d3;->modCount:I

    .line 89782
    invoke-static {p0, v6, v7, v2}, LX/0d3;->a(LX/0d3;Ljava/lang/Object;Ljava/lang/Object;LX/18j;)V

    .line 89783
    invoke-static {p0, v0, v3}, LX/0d3;->b(LX/0d3;LX/0qF;LX/0qF;)LX/0qF;

    move-result-object v0

    .line 89784
    iget v3, p0, LX/0d3;->count:I

    add-int/lit8 v3, v3, -0x1

    .line 89785
    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 89786
    iput v3, p0, LX/0d3;->count:I

    .line 89787
    sget-object v0, LX/18j;->EXPLICIT:LX/18j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v2, v0, :cond_2

    const/4 v0, 0x1

    .line 89788
    :goto_2
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89789
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    move v1, v0

    :goto_3
    return v1

    .line 89790
    :cond_0
    :try_start_1
    invoke-static {v2}, LX/0d3;->a(LX/0cp;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 89791
    sget-object v2, LX/18j;->COLLECTED:LX/18j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 89792
    :cond_1
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89793
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    goto :goto_3

    :cond_2
    move v0, v1

    .line 89794
    goto :goto_2

    .line 89795
    :cond_3
    :try_start_2
    invoke-interface {v3}, LX/0qF;->getNext()LX/0qF;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    move-object v3, v2

    goto :goto_0

    .line 89796
    :cond_4
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89797
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    goto :goto_3

    .line 89798
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89799
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    throw v0
.end method

.method public final c(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)TV;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 89800
    invoke-virtual {p0}, LX/0d3;->lock()V

    .line 89801
    :try_start_0
    invoke-static {p0}, LX/0d3;->r(LX/0d3;)V

    .line 89802
    iget-object v4, p0, LX/0d3;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 89803
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    .line 89804
    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qF;

    move-object v3, v0

    .line 89805
    :goto_0
    if-eqz v3, :cond_3

    .line 89806
    invoke-interface {v3}, LX/0qF;->getKey()Ljava/lang/Object;

    move-result-object v6

    .line 89807
    invoke-interface {v3}, LX/0qF;->getHash()I

    move-result v2

    if-ne v2, p2, :cond_2

    if-eqz v6, :cond_2

    iget-object v2, p0, LX/0d3;->map:LX/0cn;

    iget-object v2, v2, LX/0cn;->keyEquivalence:LX/0Qj;

    invoke-virtual {v2, p1, v6}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 89808
    invoke-interface {v3}, LX/0qF;->getValueReference()LX/0cp;

    move-result-object v7

    .line 89809
    invoke-interface {v7}, LX/0cp;->get()Ljava/lang/Object;

    move-result-object v2

    .line 89810
    if-eqz v2, :cond_0

    .line 89811
    sget-object v1, LX/18j;->EXPLICIT:LX/18j;

    .line 89812
    :goto_1
    iget v7, p0, LX/0d3;->modCount:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, LX/0d3;->modCount:I

    .line 89813
    invoke-static {p0, v6, v2, v1}, LX/0d3;->a(LX/0d3;Ljava/lang/Object;Ljava/lang/Object;LX/18j;)V

    .line 89814
    invoke-static {p0, v0, v3}, LX/0d3;->b(LX/0d3;LX/0qF;LX/0qF;)LX/0qF;

    move-result-object v0

    .line 89815
    iget v1, p0, LX/0d3;->count:I

    add-int/lit8 v1, v1, -0x1

    .line 89816
    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 89817
    iput v1, p0, LX/0d3;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89818
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89819
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    move-object v0, v2

    :goto_2
    return-object v0

    .line 89820
    :cond_0
    :try_start_1
    invoke-static {v7}, LX/0d3;->a(LX/0cp;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 89821
    sget-object v1, LX/18j;->COLLECTED:LX/18j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 89822
    :cond_1
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89823
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    move-object v0, v1

    goto :goto_2

    .line 89824
    :cond_2
    :try_start_2
    invoke-interface {v3}, LX/0qF;->getNext()LX/0qF;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    move-object v3, v2

    goto :goto_0

    .line 89825
    :cond_3
    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89826
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    move-object v0, v1

    goto :goto_2

    .line 89827
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0d3;->unlock()V

    .line 89828
    invoke-static {p0}, LX/0d3;->s(LX/0d3;)V

    throw v0
.end method
