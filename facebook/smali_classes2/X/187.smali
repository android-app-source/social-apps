.class public LX/187;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/187;


# instance fields
.field private final a:LX/0pk;

.field private final b:LX/0pk;

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1qe;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pn;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0jU;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0pi;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 205173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 205174
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 205175
    iput-object v0, p0, LX/187;->c:LX/0Ot;

    .line 205176
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 205177
    iput-object v0, p0, LX/187;->d:LX/0Ot;

    .line 205178
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 205179
    iput-object v0, p0, LX/187;->e:LX/0Ot;

    .line 205180
    const-string v0, "feed_db_request"

    invoke-virtual {p1, v0}, LX/0pi;->b(Ljava/lang/String;)LX/0pk;

    move-result-object v0

    iput-object v0, p0, LX/187;->a:LX/0pk;

    .line 205181
    const-string v0, "feed_db_entries"

    invoke-virtual {p1, v0}, LX/0pi;->b(Ljava/lang/String;)LX/0pk;

    move-result-object v0

    iput-object v0, p0, LX/187;->b:LX/0pk;

    .line 205182
    return-void
.end method

.method public static a(LX/0QB;)LX/187;
    .locals 6

    .prologue
    .line 205155
    sget-object v0, LX/187;->f:LX/187;

    if-nez v0, :cond_1

    .line 205156
    const-class v1, LX/187;

    monitor-enter v1

    .line 205157
    :try_start_0
    sget-object v0, LX/187;->f:LX/187;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 205158
    if-eqz v2, :cond_0

    .line 205159
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 205160
    new-instance v4, LX/187;

    invoke-static {v0}, LX/0pi;->a(LX/0QB;)LX/0pi;

    move-result-object v3

    check-cast v3, LX/0pi;

    invoke-direct {v4, v3}, LX/187;-><init>(LX/0pi;)V

    .line 205161
    const/16 v3, 0xed

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v5, 0xe8

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0xfc

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 205162
    iput-object v3, v4, LX/187;->c:LX/0Ot;

    iput-object v5, v4, LX/187;->d:LX/0Ot;

    iput-object p0, v4, LX/187;->e:LX/0Ot;

    .line 205163
    move-object v0, v4

    .line 205164
    sput-object v0, LX/187;->f:LX/187;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 205165
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 205166
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 205167
    :cond_1
    sget-object v0, LX/187;->f:LX/187;

    return-object v0

    .line 205168
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 205169
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedResult;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feed/FetchFeedResult;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205183
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v1, v0

    .line 205184
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v0, v0

    .line 205185
    iget-object v2, v0, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v2, v2

    .line 205186
    invoke-virtual {v2}, Lcom/facebook/api/feedtype/FeedType;->e()LX/0pL;

    move-result-object v0

    sget-object v3, LX/0pL;->NO_CACHE:LX/0pL;

    if-eq v0, v3, :cond_1

    const/4 v0, 0x1

    .line 205187
    :goto_0
    if-eqz v0, :cond_0

    .line 205188
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 205189
    iget-object v0, p0, LX/187;->a:LX/0pk;

    invoke-virtual {v0}, LX/0pk;->c()V

    .line 205190
    iget-object v0, p0, LX/187;->b:LX/0pk;

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v0, v4, v5}, LX/0pk;->b(J)V

    .line 205191
    invoke-virtual {v2}, Lcom/facebook/api/feedtype/FeedType;->e()LX/0pL;

    move-result-object v0

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    if-ne v0, v2, :cond_3

    .line 205192
    invoke-virtual {p0, p1}, LX/187;->b(Lcom/facebook/api/feed/FetchFeedResult;)LX/0Px;

    move-result-object v0

    .line 205193
    :goto_1
    invoke-virtual {p0, p1}, LX/187;->c(Lcom/facebook/api/feed/FetchFeedResult;)V

    move-object v1, v0

    .line 205194
    :cond_0
    :goto_2
    return-object v1

    .line 205195
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 205196
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/api/feedtype/FeedType;->e()LX/0pL;

    move-result-object v0

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    if-ne v0, v2, :cond_0

    .line 205197
    iget-object v0, p0, LX/187;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pn;

    invoke-virtual {v0, p1}, LX/0pn;->b(Lcom/facebook/api/feed/FetchFeedResult;)V

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public final b(Lcom/facebook/api/feed/FetchFeedResult;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feed/FetchFeedResult;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205172
    iget-object v0, p0, LX/187;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qe;

    invoke-virtual {v0, p1}, LX/1qe;->a(Lcom/facebook/api/feed/FetchFeedResult;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/api/feed/FetchFeedResult;)V
    .locals 2

    .prologue
    .line 205170
    iget-object v0, p0, LX/187;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jU;

    invoke-virtual {v0, p1}, LX/0jU;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 205171
    return-void
.end method
