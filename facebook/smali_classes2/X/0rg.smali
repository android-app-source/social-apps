.class public final enum LX/0rg;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0rg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0rg;

.field public static final enum COUNT:LX/0rg;

.field public static final enum COUNT_AND_SIZE:LX/0rg;

.field public static final enum SIZE:LX/0rg;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 150051
    new-instance v0, LX/0rg;

    const-string v1, "COUNT"

    invoke-direct {v0, v1, v2}, LX/0rg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rg;->COUNT:LX/0rg;

    .line 150052
    new-instance v0, LX/0rg;

    const-string v1, "SIZE"

    invoke-direct {v0, v1, v3}, LX/0rg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rg;->SIZE:LX/0rg;

    .line 150053
    new-instance v0, LX/0rg;

    const-string v1, "COUNT_AND_SIZE"

    invoke-direct {v0, v1, v4}, LX/0rg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rg;->COUNT_AND_SIZE:LX/0rg;

    .line 150054
    const/4 v0, 0x3

    new-array v0, v0, [LX/0rg;

    sget-object v1, LX/0rg;->COUNT:LX/0rg;

    aput-object v1, v0, v2

    sget-object v1, LX/0rg;->SIZE:LX/0rg;

    aput-object v1, v0, v3

    sget-object v1, LX/0rg;->COUNT_AND_SIZE:LX/0rg;

    aput-object v1, v0, v4

    sput-object v0, LX/0rg;->$VALUES:[LX/0rg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 150050
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0rg;
    .locals 1

    .prologue
    .line 150048
    const-class v0, LX/0rg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0rg;

    return-object v0
.end method

.method public static values()[LX/0rg;
    .locals 1

    .prologue
    .line 150049
    sget-object v0, LX/0rg;->$VALUES:[LX/0rg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0rg;

    return-object v0
.end method
