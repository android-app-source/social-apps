.class public final LX/18D;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/0jU;

.field public volatile b:Lcom/facebook/graphql/model/FeedUnit;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:LX/18C;


# direct methods
.method public constructor <init>(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/18C;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 206026
    iput-object p1, p0, LX/18D;->a:LX/0jU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206027
    iput-object p2, p0, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    .line 206028
    iput-object p3, p0, LX/18D;->c:Ljava/lang/String;

    .line 206029
    iput-object p4, p0, LX/18D;->d:Ljava/lang/String;

    .line 206030
    iput-object p5, p0, LX/18D;->e:Ljava/lang/String;

    .line 206031
    iput-object p6, p0, LX/18D;->f:LX/18C;

    .line 206032
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 206033
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    .line 206034
    :goto_0
    return v0

    .line 206035
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 206036
    :cond_2
    check-cast p1, LX/18D;

    .line 206037
    iget-object v0, p0, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 206038
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
