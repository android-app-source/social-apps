.class public LX/1qJ;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/2GJ;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 330484
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 330485
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)LX/2GJ;
    .locals 21

    .prologue
    .line 330482
    new-instance v1, LX/2GJ;

    invoke-static/range {p0 .. p0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static/range {p0 .. p0}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v4

    check-cast v4, LX/0Sy;

    invoke-static/range {p0 .. p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v5

    check-cast v5, LX/0Uo;

    invoke-static/range {p0 .. p0}, LX/29m;->a(LX/0QB;)LX/0Xw;

    move-result-object v6

    check-cast v6, LX/0Xw;

    invoke-static/range {p0 .. p0}, LX/2GK;->a(LX/0QB;)LX/0Ot;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/0Un;->a(LX/0QB;)LX/0Un;

    move-result-object v8

    check-cast v8, LX/0Un;

    const/16 v9, 0x1bd

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v10

    check-cast v10, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v11

    check-cast v11, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v12

    check-cast v12, LX/0Zm;

    invoke-static/range {p0 .. p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v13

    check-cast v13, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static/range {p0 .. p0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v14

    check-cast v14, LX/0Sj;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v15

    check-cast v15, LX/03V;

    invoke-static/range {p0 .. p0}, LX/1mP;->a(LX/0QB;)LX/1mP;

    move-result-object v16

    check-cast v16, LX/1mP;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v17

    check-cast v17, LX/0Zb;

    move-object/from16 v18, p1

    move-object/from16 v19, p2

    move-object/from16 v20, p3

    invoke-direct/range {v1 .. v20}, LX/2GJ;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ScheduledExecutorService;LX/0Sy;LX/0Uo;LX/0Xw;LX/0Ot;LX/0Un;LX/0Or;LX/0Sh;LX/0SG;LX/0Zm;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Sj;LX/03V;LX/1mP;LX/0Zb;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V

    .line 330483
    return-object v1
.end method
