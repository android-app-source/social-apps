.class public abstract LX/0rY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/0re;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0re",
            "<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0rd;)V
    .locals 2

    .prologue
    .line 149780
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149781
    const/16 v0, 0xc8

    const-string v1, "recent_vpv_state"

    invoke-virtual {p1, v0, v1}, LX/0rd;->a(ILjava/lang/String;)LX/0re;

    move-result-object v0

    iput-object v0, p0, LX/0rY;->a:LX/0re;

    .line 149782
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 149783
    const/16 v0, 0x78

    .line 149784
    new-instance v2, Ljava/util/ArrayList;

    iget-object v1, p0, LX/0rY;->a:LX/0re;

    invoke-virtual {v1}, LX/0re;->c()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 149785
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 149786
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 149787
    :goto_0
    if-ltz v1, :cond_0

    if-lez v0, :cond_0

    .line 149788
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 149789
    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 149790
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 149791
    return-object v0
.end method
