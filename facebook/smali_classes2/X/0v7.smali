.class public final LX/0v7;
.super LX/0v8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0v8",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field public final b:LX/0vG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/ReplaySubject$ReplayState",
            "<TT;*>;"
        }
    .end annotation
.end field

.field public final c:LX/0vK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vK",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/0vL;LX/0vK;LX/0vG;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0vL",
            "<TT;>;",
            "LX/0vK",
            "<TT;>;",
            "Lrx/subjects/ReplaySubject$ReplayState",
            "<TT;*>;)V"
        }
    .end annotation

    .prologue
    .line 157642
    invoke-direct {p0, p1}, LX/0v8;-><init>(LX/0vL;)V

    .line 157643
    iput-object p2, p0, LX/0v7;->c:LX/0vK;

    .line 157644
    iput-object p3, p0, LX/0v7;->b:LX/0vG;

    .line 157645
    return-void
.end method

.method public static a(I)LX/0v7;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I)",
            "LX/0v7",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 157637
    new-instance v0, LX/0vG;

    invoke-direct {v0, p0}, LX/0vG;-><init>(I)V

    .line 157638
    new-instance v1, LX/0vK;

    invoke-direct {v1}, LX/0vK;-><init>()V

    .line 157639
    new-instance v2, LX/0vS;

    invoke-direct {v2, v0}, LX/0vS;-><init>(LX/0vG;)V

    iput-object v2, v1, LX/0vK;->f:LX/0vM;

    .line 157640
    new-instance v2, LX/0vT;

    invoke-direct {v2, v0}, LX/0vT;-><init>(LX/0vG;)V

    iput-object v2, v1, LX/0vK;->h:LX/0vM;

    .line 157641
    new-instance v2, LX/0v7;

    invoke-direct {v2, v1, v1, v0}, LX/0v7;-><init>(LX/0vL;LX/0vK;LX/0vG;)V

    return-object v2
.end method

.method private a(LX/0vO;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0vO",
            "<-TT;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 157630
    iget-boolean v1, p1, LX/0vO;->c:Z

    if-nez v1, :cond_0

    .line 157631
    iput-boolean v0, p1, LX/0vO;->c:Z

    .line 157632
    iget-object v0, p0, LX/0v7;->b:LX/0vG;

    invoke-virtual {v0, p1}, LX/0vG;->a(LX/0vO;)V

    .line 157633
    const/4 v0, 0x0

    .line 157634
    iput-object v0, p1, LX/0vO;->d:Ljava/lang/Object;

    .line 157635
    const/4 v0, 0x0

    .line 157636
    :cond_0
    return v0
.end method

.method public static d()LX/0v7;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "LX/0v7",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 157581
    const/16 v0, 0x10

    invoke-static {v0}, LX/0v7;->a(I)LX/0v7;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final R_()V
    .locals 5

    .prologue
    .line 157615
    iget-object v0, p0, LX/0v7;->c:LX/0vK;

    iget-boolean v0, v0, LX/0vK;->e:Z

    if-eqz v0, :cond_2

    .line 157616
    iget-object v0, p0, LX/0v7;->b:LX/0vG;

    .line 157617
    iget-boolean v1, v0, LX/0vG;->e:Z

    if-nez v1, :cond_0

    .line 157618
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0vG;->e:Z

    .line 157619
    iget-object v1, v0, LX/0vG;->d:Ljava/util/ArrayList;

    .line 157620
    sget-object v2, LX/0vH;->b:Ljava/lang/Object;

    move-object v2, v2

    .line 157621
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157622
    sget-object v1, LX/0vG;->b:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->getAndIncrement(Ljava/lang/Object;)I

    .line 157623
    :cond_0
    iget-object v0, p0, LX/0v7;->c:LX/0vK;

    .line 157624
    sget-object v1, LX/0vH;->b:Ljava/lang/Object;

    move-object v1, v1

    .line 157625
    invoke-virtual {v0, v1}, LX/0vK;->b(Ljava/lang/Object;)[LX/0vO;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 157626
    invoke-direct {p0, v3}, LX/0v7;->a(LX/0vO;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 157627
    invoke-virtual {v3}, LX/0vO;->R_()V

    .line 157628
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157629
    :cond_2
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 157603
    iget-object v0, p0, LX/0v7;->c:LX/0vK;

    iget-boolean v0, v0, LX/0vK;->e:Z

    if-eqz v0, :cond_2

    .line 157604
    iget-object v0, p0, LX/0v7;->b:LX/0vG;

    .line 157605
    iget-boolean v1, v0, LX/0vG;->e:Z

    if-nez v1, :cond_0

    .line 157606
    iget-object v1, v0, LX/0vG;->d:Ljava/util/ArrayList;

    invoke-static {p1}, LX/0vH;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157607
    sget-object v1, LX/0vG;->b:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->getAndIncrement(Ljava/lang/Object;)I

    .line 157608
    :cond_0
    iget-object v0, p0, LX/0v7;->c:LX/0vK;

    .line 157609
    iget-object v1, v0, LX/0vK;->a:LX/0vN;

    iget-object v1, v1, LX/0vN;->b:[LX/0vO;

    move-object v1, v1

    .line 157610
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 157611
    invoke-direct {p0, v3}, LX/0v7;->a(LX/0vO;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 157612
    invoke-virtual {v3, p1}, LX/0vO;->a(Ljava/lang/Object;)V

    .line 157613
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157614
    :cond_2
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 157582
    iget-object v0, p0, LX/0v7;->c:LX/0vK;

    iget-boolean v0, v0, LX/0vK;->e:Z

    if-eqz v0, :cond_4

    .line 157583
    iget-object v0, p0, LX/0v7;->b:LX/0vG;

    .line 157584
    iget-boolean v1, v0, LX/0vG;->e:Z

    if-nez v1, :cond_0

    .line 157585
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0vG;->e:Z

    .line 157586
    iget-object v1, v0, LX/0vG;->d:Ljava/util/ArrayList;

    invoke-static {p1}, LX/0vH;->a(Ljava/lang/Throwable;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157587
    sget-object v1, LX/0vG;->b:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->getAndIncrement(Ljava/lang/Object;)I

    .line 157588
    :cond_0
    const/4 v0, 0x0

    .line 157589
    iget-object v1, p0, LX/0v7;->c:LX/0vK;

    .line 157590
    invoke-static {p1}, LX/0vH;->a(Ljava/lang/Throwable;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0vK;->b(Ljava/lang/Object;)[LX/0vO;

    move-result-object v4

    array-length v5, v4

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_3

    aget-object v1, v4, v2

    .line 157591
    :try_start_0
    invoke-direct {p0, v1}, LX/0v7;->a(LX/0vO;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 157592
    invoke-virtual {v1, p1}, LX/0vO;->a(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 157593
    :cond_1
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 157594
    :catch_0
    move-exception v1

    .line 157595
    if-nez v0, :cond_2

    .line 157596
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 157597
    :cond_2
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 157598
    :cond_3
    if-eqz v0, :cond_4

    .line 157599
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 157600
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, LX/52y;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    .line 157601
    :cond_4
    return-void

    .line 157602
    :cond_5
    new-instance v1, LX/52x;

    invoke-direct {v1, v0}, LX/52x;-><init>(Ljava/util/Collection;)V

    throw v1
.end method
