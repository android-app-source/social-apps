.class public LX/1VH;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1XD;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1xR;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 258717
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1VH;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1xR;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 258714
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 258715
    iput-object p1, p0, LX/1VH;->b:LX/0Ot;

    .line 258716
    return-void
.end method

.method public static a(LX/0QB;)LX/1VH;
    .locals 4

    .prologue
    .line 258672
    const-class v1, LX/1VH;

    monitor-enter v1

    .line 258673
    :try_start_0
    sget-object v0, LX/1VH;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 258674
    sput-object v2, LX/1VH;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 258675
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258676
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 258677
    new-instance v3, LX/1VH;

    const/16 p0, 0x6ff

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1VH;-><init>(LX/0Ot;)V

    .line 258678
    move-object v0, v3

    .line 258679
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 258680
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1VH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 258681
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 258682
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 11

    .prologue
    .line 258705
    check-cast p2, LX/1XC;

    .line 258706
    iget-object v0, p0, LX/1VH;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1xR;

    iget-object v1, p2, LX/1XC;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/1XC;->c:LX/1EO;

    iget-object v3, p2, LX/1XC;->d:Lcom/facebook/graphql/model/GraphQLComment;

    .line 258707
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 258708
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 258709
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 258710
    :cond_0
    :goto_0
    return-void

    .line 258711
    :cond_1
    sget-object v4, LX/1vY;->FEED_STORY_MESSAGE_FLYOUT:LX/1vY;

    invoke-static {p1, v4}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 258712
    iget-object v4, v0, LX/1xR;->b:LX/1EN;

    sget-object v6, LX/1Qt;->FEED:LX/1Qt;

    sget-object v9, LX/An0;->MESSAGE:LX/An0;

    move-object v5, v1

    move-object v7, v3

    move-object v8, p1

    move-object v10, v2

    invoke-virtual/range {v4 .. v10}, LX/1EN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Qt;Lcom/facebook/graphql/model/GraphQLComment;Landroid/view/View;LX/An0;LX/1EO;)V

    goto :goto_0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 258713
    const v0, -0x577feb00

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 258697
    check-cast p2, LX/1XC;

    .line 258698
    iget-object v0, p0, LX/1VH;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1xR;

    iget-object v1, p2, LX/1XC;->a:LX/1X1;

    .line 258699
    invoke-static {p1, v1}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object p2

    .line 258700
    iget-object p0, v0, LX/1xR;->a:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 258701
    const p0, -0x577feb00

    const/4 v0, 0x0

    invoke-static {p1, p0, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 258702
    invoke-interface {p2, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    .line 258703
    :cond_0
    invoke-interface {p2}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 258704
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 258691
    invoke-static {}, LX/1dS;->b()V

    .line 258692
    iget v0, p1, LX/1dQ;->b:I

    .line 258693
    packed-switch v0, :pswitch_data_0

    .line 258694
    :goto_0
    return-object v2

    .line 258695
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 258696
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/1VH;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x577feb00
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/1XD;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 258683
    new-instance v1, LX/1XC;

    invoke-direct {v1, p0}, LX/1XC;-><init>(LX/1VH;)V

    .line 258684
    sget-object v2, LX/1VH;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1XD;

    .line 258685
    if-nez v2, :cond_0

    .line 258686
    new-instance v2, LX/1XD;

    invoke-direct {v2}, LX/1XD;-><init>()V

    .line 258687
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/1XD;->a$redex0(LX/1XD;LX/1De;IILX/1XC;)V

    .line 258688
    move-object v1, v2

    .line 258689
    move-object v0, v1

    .line 258690
    return-object v0
.end method
