.class public final LX/0zL;
.super LX/0nA;
.source ""


# instance fields
.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 167038
    invoke-direct {p0}, LX/0nA;-><init>()V

    .line 167039
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/0zL;->b:Ljava/util/ArrayList;

    .line 167040
    return-void
.end method

.method public static a(LX/0zL;Ljava/lang/Object;)V
    .locals 1
    .param p0    # LX/0zL;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 167035
    invoke-virtual {p0}, LX/0nA;->g()V

    .line 167036
    iget-object v0, p0, LX/0zL;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167037
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 167030
    iget-object v0, p0, LX/0zL;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int/2addr v0, p1

    .line 167031
    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_0

    .line 167032
    iget-object v0, p0, LX/0zL;->b:Ljava/util/ArrayList;

    iget-object v2, p0, LX/0zL;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v0, v1

    goto :goto_0

    .line 167033
    :cond_0
    iget-object v0, p0, LX/0zL;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->trimToSize()V

    .line 167034
    return-void
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 167029
    iget-object v0, p0, LX/0zL;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/0nA;)V
    .locals 1

    .prologue
    .line 167020
    const-string v0, "subParams cannot be null!"

    invoke-static {p1, v0}, LX/0nE;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 167021
    invoke-virtual {p0}, LX/0nA;->g()V

    .line 167022
    invoke-virtual {p1}, LX/0nA;->c()V

    .line 167023
    invoke-static {p0, p1}, LX/0zL;->a(LX/0zL;Ljava/lang/Object;)V

    .line 167024
    invoke-virtual {p1, p0}, LX/0nA;->a(LX/0nA;)V

    .line 167025
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 167041
    invoke-virtual {p0}, LX/0zL;->j()I

    move-result v2

    .line 167042
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 167043
    invoke-virtual {p0, v1}, LX/0zL;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 167044
    instance-of v3, v0, LX/0nA;

    if-eqz v3, :cond_0

    .line 167045
    check-cast v0, LX/0nA;

    invoke-virtual {v0}, LX/0nA;->b()V

    .line 167046
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 167047
    :cond_1
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 167026
    invoke-virtual {p0}, LX/0nA;->i()LX/0Zh;

    move-result-object v0

    .line 167027
    iget-object v1, v0, LX/0Zh;->b:LX/0Zi;

    invoke-virtual {v1, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 167028
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 167018
    iget-object v0, p0, LX/0zL;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 167019
    return-void
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 167017
    iget-object v0, p0, LX/0zL;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final k()LX/0n9;
    .locals 1

    .prologue
    .line 167014
    invoke-virtual {p0}, LX/0nA;->i()LX/0Zh;

    move-result-object v0

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v0

    .line 167015
    invoke-virtual {p0, v0}, LX/0zL;->c(LX/0nA;)V

    .line 167016
    return-object v0
.end method

.method public final l()LX/0zL;
    .locals 1

    .prologue
    .line 167011
    invoke-virtual {p0}, LX/0nA;->i()LX/0Zh;

    move-result-object v0

    invoke-virtual {v0}, LX/0Zh;->c()LX/0zL;

    move-result-object v0

    .line 167012
    invoke-virtual {p0, v0}, LX/0zL;->c(LX/0nA;)V

    .line 167013
    return-object v0
.end method
