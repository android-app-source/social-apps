.class public LX/0lp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0lE;
.implements Ljava/io/Serializable;


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:I

.field public static final d:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/ref/SoftReference",
            "<",
            "LX/12B;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final g:LX/0lc;

.field private static final serialVersionUID:J = 0x791a681719524b4aL


# instance fields
.field public _characterEscapes:LX/4pa;

.field public _factoryFeatures:I

.field public _generatorFeatures:I

.field public _inputDecorator:LX/4pb;

.field public _objectCodec:LX/0lD;

.field public _outputDecorator:LX/4pd;

.field public _parserFeatures:I

.field public _rootValueSeparator:LX/0lc;

.field public final transient e:LX/0lt;

.field public final transient f:LX/0lv;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 130814
    invoke-static {}, LX/0lq;->collectDefaults()I

    move-result v0

    sput v0, LX/0lp;->a:I

    .line 130815
    invoke-static {}, LX/0lr;->collectDefaults()I

    move-result v0

    sput v0, LX/0lp;->b:I

    .line 130816
    invoke-static {}, LX/0ls;->collectDefaults()I

    move-result v0

    sput v0, LX/0lp;->c:I

    .line 130817
    sget-object v0, LX/0lY;->a:LX/0lb;

    sput-object v0, LX/0lp;->g:LX/0lc;

    .line 130818
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, LX/0lp;->d:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 130819
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/0lp;-><init>(LX/0lD;)V

    return-void
.end method

.method public constructor <init>(LX/0lD;)V
    .locals 1

    .prologue
    .line 130820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130821
    invoke-static {}, LX/0lt;->a()LX/0lt;

    move-result-object v0

    iput-object v0, p0, LX/0lp;->e:LX/0lt;

    .line 130822
    invoke-static {}, LX/0lv;->a()LX/0lv;

    move-result-object v0

    iput-object v0, p0, LX/0lp;->f:LX/0lv;

    .line 130823
    sget v0, LX/0lp;->a:I

    iput v0, p0, LX/0lp;->_factoryFeatures:I

    .line 130824
    sget v0, LX/0lp;->b:I

    iput v0, p0, LX/0lp;->_parserFeatures:I

    .line 130825
    sget v0, LX/0lp;->c:I

    iput v0, p0, LX/0lp;->_generatorFeatures:I

    .line 130826
    sget-object v0, LX/0lp;->g:LX/0lc;

    iput-object v0, p0, LX/0lp;->_rootValueSeparator:LX/0lc;

    .line 130827
    iput-object p1, p0, LX/0lp;->_objectCodec:LX/0lD;

    return-void
.end method

.method public constructor <init>(LX/0lp;)V
    .locals 1

    .prologue
    .line 130828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130829
    invoke-static {}, LX/0lt;->a()LX/0lt;

    move-result-object v0

    iput-object v0, p0, LX/0lp;->e:LX/0lt;

    .line 130830
    invoke-static {}, LX/0lv;->a()LX/0lv;

    move-result-object v0

    iput-object v0, p0, LX/0lp;->f:LX/0lv;

    .line 130831
    sget v0, LX/0lp;->a:I

    iput v0, p0, LX/0lp;->_factoryFeatures:I

    .line 130832
    sget v0, LX/0lp;->b:I

    iput v0, p0, LX/0lp;->_parserFeatures:I

    .line 130833
    sget v0, LX/0lp;->c:I

    iput v0, p0, LX/0lp;->_generatorFeatures:I

    .line 130834
    sget-object v0, LX/0lp;->g:LX/0lc;

    iput-object v0, p0, LX/0lp;->_rootValueSeparator:LX/0lc;

    .line 130835
    const/4 v0, 0x0

    iput-object v0, p0, LX/0lp;->_objectCodec:LX/0lD;

    .line 130836
    iget v0, p1, LX/0lp;->_factoryFeatures:I

    iput v0, p0, LX/0lp;->_factoryFeatures:I

    .line 130837
    iget v0, p1, LX/0lp;->_parserFeatures:I

    iput v0, p0, LX/0lp;->_parserFeatures:I

    .line 130838
    iget v0, p1, LX/0lp;->_generatorFeatures:I

    iput v0, p0, LX/0lp;->_generatorFeatures:I

    .line 130839
    iget-object v0, p1, LX/0lp;->_characterEscapes:LX/4pa;

    iput-object v0, p0, LX/0lp;->_characterEscapes:LX/4pa;

    .line 130840
    iget-object v0, p1, LX/0lp;->_inputDecorator:LX/4pb;

    iput-object v0, p0, LX/0lp;->_inputDecorator:LX/4pb;

    .line 130841
    iget-object v0, p1, LX/0lp;->_outputDecorator:LX/4pd;

    iput-object v0, p0, LX/0lp;->_outputDecorator:LX/4pd;

    .line 130842
    iget-object v0, p1, LX/0lp;->_rootValueSeparator:LX/0lc;

    iput-object v0, p0, LX/0lp;->_rootValueSeparator:LX/0lc;

    .line 130843
    return-void
.end method

.method public static final a(Ljava/lang/Object;Z)LX/12A;
    .locals 2

    .prologue
    .line 130844
    new-instance v0, LX/12A;

    invoke-static {}, LX/0lp;->b()LX/12B;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, LX/12A;-><init>(LX/12B;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method public static b()LX/12B;
    .locals 3

    .prologue
    .line 130845
    sget-object v0, LX/0lp;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    .line 130846
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 130847
    :goto_0
    if-nez v0, :cond_0

    .line 130848
    new-instance v0, LX/12B;

    invoke-direct {v0}, LX/12B;-><init>()V

    .line 130849
    sget-object v1, LX/0lp;->d:Ljava/lang/ThreadLocal;

    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 130850
    :cond_0
    return-object v0

    .line 130851
    :cond_1
    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12B;

    goto :goto_0
.end method


# virtual methods
.method public a()LX/0lD;
    .locals 1

    .prologue
    .line 130890
    iget-object v0, p0, LX/0lp;->_objectCodec:LX/0lD;

    return-object v0
.end method

.method public final a(Ljava/io/File;LX/1pL;)LX/0nX;
    .locals 3

    .prologue
    .line 130852
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 130853
    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/0lp;->a(Ljava/lang/Object;Z)LX/12A;

    move-result-object v1

    .line 130854
    iput-object p2, v1, LX/12A;->b:LX/1pL;

    .line 130855
    sget-object v2, LX/1pL;->UTF8:LX/1pL;

    if-ne p2, v2, :cond_1

    .line 130856
    iget-object v2, p0, LX/0lp;->_outputDecorator:LX/4pd;

    if-eqz v2, :cond_0

    .line 130857
    iget-object v0, p0, LX/0lp;->_outputDecorator:LX/4pd;

    invoke-virtual {v0}, LX/4pd;->a()Ljava/io/OutputStream;

    move-result-object v0

    .line 130858
    :cond_0
    invoke-virtual {p0, v0, v1}, LX/0lp;->a(Ljava/io/OutputStream;LX/12A;)LX/0nX;

    move-result-object v0

    .line 130859
    :goto_0
    return-object v0

    .line 130860
    :cond_1
    invoke-virtual {p0, v0, p2, v1}, LX/0lp;->a(Ljava/io/OutputStream;LX/1pL;LX/12A;)Ljava/io/Writer;

    move-result-object v0

    .line 130861
    iget-object v2, p0, LX/0lp;->_outputDecorator:LX/4pd;

    if-eqz v2, :cond_2

    .line 130862
    iget-object v0, p0, LX/0lp;->_outputDecorator:LX/4pd;

    invoke-virtual {v0}, LX/4pd;->b()Ljava/io/Writer;

    move-result-object v0

    .line 130863
    :cond_2
    invoke-virtual {p0, v0, v1}, LX/0lp;->a(Ljava/io/Writer;LX/12A;)LX/0nX;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/io/OutputStream;)LX/0nX;
    .locals 1

    .prologue
    .line 130864
    sget-object v0, LX/1pL;->UTF8:LX/1pL;

    invoke-virtual {p0, p1, v0}, LX/0lp;->a(Ljava/io/OutputStream;LX/1pL;)LX/0nX;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/OutputStream;LX/12A;)LX/0nX;
    .locals 3

    .prologue
    .line 130872
    new-instance v0, LX/2S8;

    iget v1, p0, LX/0lp;->_generatorFeatures:I

    iget-object v2, p0, LX/0lp;->_objectCodec:LX/0lD;

    invoke-direct {v0, p2, v1, v2, p1}, LX/2S8;-><init>(LX/12A;ILX/0lD;Ljava/io/OutputStream;)V

    .line 130873
    iget-object v1, p0, LX/0lp;->_characterEscapes:LX/4pa;

    if-eqz v1, :cond_0

    .line 130874
    iget-object v1, p0, LX/0lp;->_characterEscapes:LX/4pa;

    invoke-virtual {v0, v1}, LX/0nX;->a(LX/4pa;)LX/0nX;

    .line 130875
    :cond_0
    iget-object v1, p0, LX/0lp;->_rootValueSeparator:LX/0lc;

    .line 130876
    sget-object v2, LX/0lp;->g:LX/0lc;

    if-eq v1, v2, :cond_1

    .line 130877
    invoke-virtual {v0, v1}, LX/0nX;->a(LX/0lc;)LX/0nX;

    .line 130878
    :cond_1
    return-object v0
.end method

.method public a(Ljava/io/OutputStream;LX/1pL;)LX/0nX;
    .locals 3

    .prologue
    .line 130879
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/0lp;->a(Ljava/lang/Object;Z)LX/12A;

    move-result-object v1

    .line 130880
    iput-object p2, v1, LX/12A;->b:LX/1pL;

    .line 130881
    sget-object v0, LX/1pL;->UTF8:LX/1pL;

    if-ne p2, v0, :cond_1

    .line 130882
    iget-object v0, p0, LX/0lp;->_outputDecorator:LX/4pd;

    if-eqz v0, :cond_0

    .line 130883
    iget-object v0, p0, LX/0lp;->_outputDecorator:LX/4pd;

    invoke-virtual {v0}, LX/4pd;->a()Ljava/io/OutputStream;

    move-result-object p1

    .line 130884
    :cond_0
    invoke-virtual {p0, p1, v1}, LX/0lp;->a(Ljava/io/OutputStream;LX/12A;)LX/0nX;

    move-result-object v0

    .line 130885
    :goto_0
    return-object v0

    .line 130886
    :cond_1
    invoke-virtual {p0, p1, p2, v1}, LX/0lp;->a(Ljava/io/OutputStream;LX/1pL;LX/12A;)Ljava/io/Writer;

    move-result-object v0

    .line 130887
    iget-object v2, p0, LX/0lp;->_outputDecorator:LX/4pd;

    if-eqz v2, :cond_2

    .line 130888
    iget-object v0, p0, LX/0lp;->_outputDecorator:LX/4pd;

    invoke-virtual {v0}, LX/4pd;->b()Ljava/io/Writer;

    move-result-object v0

    .line 130889
    :cond_2
    invoke-virtual {p0, v0, v1}, LX/0lp;->a(Ljava/io/Writer;LX/12A;)LX/0nX;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/io/Writer;)LX/0nX;
    .locals 2

    .prologue
    .line 130810
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/0lp;->a(Ljava/lang/Object;Z)LX/12A;

    move-result-object v0

    .line 130811
    iget-object v1, p0, LX/0lp;->_outputDecorator:LX/4pd;

    if-eqz v1, :cond_0

    .line 130812
    iget-object v1, p0, LX/0lp;->_outputDecorator:LX/4pd;

    invoke-virtual {v1}, LX/4pd;->b()Ljava/io/Writer;

    move-result-object p1

    .line 130813
    :cond_0
    invoke-virtual {p0, p1, v0}, LX/0lp;->a(Ljava/io/Writer;LX/12A;)LX/0nX;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/Writer;LX/12A;)LX/0nX;
    .locals 3

    .prologue
    .line 130865
    new-instance v0, LX/12E;

    iget v1, p0, LX/0lp;->_generatorFeatures:I

    iget-object v2, p0, LX/0lp;->_objectCodec:LX/0lD;

    invoke-direct {v0, p2, v1, v2, p1}, LX/12E;-><init>(LX/12A;ILX/0lD;Ljava/io/Writer;)V

    .line 130866
    iget-object v1, p0, LX/0lp;->_characterEscapes:LX/4pa;

    if-eqz v1, :cond_0

    .line 130867
    iget-object v1, p0, LX/0lp;->_characterEscapes:LX/4pa;

    invoke-virtual {v0, v1}, LX/0nX;->a(LX/4pa;)LX/0nX;

    .line 130868
    :cond_0
    iget-object v1, p0, LX/0lp;->_rootValueSeparator:LX/0lc;

    .line 130869
    sget-object v2, LX/0lp;->g:LX/0lc;

    if-eq v1, v2, :cond_1

    .line 130870
    invoke-virtual {v0, v1}, LX/0nX;->a(LX/0lc;)LX/0nX;

    .line 130871
    :cond_1
    return-object v0
.end method

.method public a(Ljava/io/File;)LX/15w;
    .locals 3

    .prologue
    .line 130805
    const/4 v0, 0x1

    invoke-static {p1, v0}, LX/0lp;->a(Ljava/lang/Object;Z)LX/12A;

    move-result-object v1

    .line 130806
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 130807
    iget-object v2, p0, LX/0lp;->_inputDecorator:LX/4pb;

    if-eqz v2, :cond_0

    .line 130808
    iget-object v0, p0, LX/0lp;->_inputDecorator:LX/4pb;

    invoke-virtual {v0}, LX/4pb;->a()Ljava/io/InputStream;

    move-result-object v0

    .line 130809
    :cond_0
    invoke-virtual {p0, v0, v1}, LX/0lp;->a(Ljava/io/InputStream;LX/12A;)LX/15w;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/InputStream;)LX/15w;
    .locals 2

    .prologue
    .line 130801
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/0lp;->a(Ljava/lang/Object;Z)LX/12A;

    move-result-object v0

    .line 130802
    iget-object v1, p0, LX/0lp;->_inputDecorator:LX/4pb;

    if-eqz v1, :cond_0

    .line 130803
    iget-object v1, p0, LX/0lp;->_inputDecorator:LX/4pb;

    invoke-virtual {v1}, LX/4pb;->a()Ljava/io/InputStream;

    move-result-object p1

    .line 130804
    :cond_0
    invoke-virtual {p0, p1, v0}, LX/0lp;->a(Ljava/io/InputStream;LX/12A;)LX/15w;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/InputStream;LX/12A;)LX/15w;
    .locals 7

    .prologue
    .line 130800
    new-instance v0, LX/1pG;

    invoke-direct {v0, p2, p1}, LX/1pG;-><init>(LX/12A;Ljava/io/InputStream;)V

    iget v1, p0, LX/0lp;->_parserFeatures:I

    iget-object v2, p0, LX/0lp;->_objectCodec:LX/0lD;

    iget-object v3, p0, LX/0lp;->f:LX/0lv;

    iget-object v4, p0, LX/0lp;->e:LX/0lt;

    sget-object v5, LX/0lq;->CANONICALIZE_FIELD_NAMES:LX/0lq;

    invoke-virtual {p0, v5}, LX/0lp;->a(LX/0lq;)Z

    move-result v5

    sget-object v6, LX/0lq;->INTERN_FIELD_NAMES:LX/0lq;

    invoke-virtual {p0, v6}, LX/0lp;->a(LX/0lq;)Z

    move-result v6

    invoke-virtual/range {v0 .. v6}, LX/1pG;->a(ILX/0lD;LX/0lv;LX/0lt;ZZ)LX/15w;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/Reader;LX/12A;)LX/15w;
    .locals 6

    .prologue
    .line 130799
    new-instance v0, LX/15t;

    iget v2, p0, LX/0lp;->_parserFeatures:I

    iget-object v4, p0, LX/0lp;->_objectCodec:LX/0lD;

    iget-object v1, p0, LX/0lp;->e:LX/0lt;

    sget-object v3, LX/0lq;->CANONICALIZE_FIELD_NAMES:LX/0lq;

    invoke-virtual {p0, v3}, LX/0lp;->a(LX/0lq;)Z

    move-result v3

    sget-object v5, LX/0lq;->INTERN_FIELD_NAMES:LX/0lq;

    invoke-virtual {p0, v5}, LX/0lp;->a(LX/0lq;)Z

    move-result v5

    invoke-virtual {v1, v3, v5}, LX/0lt;->a(ZZ)LX/0lt;

    move-result-object v5

    move-object v1, p2

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/15t;-><init>(LX/12A;ILjava/io/Reader;LX/0lD;LX/0lt;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)LX/15w;
    .locals 3

    .prologue
    .line 130779
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 130780
    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/0lp;->a(Ljava/lang/Object;Z)LX/12A;

    move-result-object v1

    .line 130781
    iget-object v2, p0, LX/0lp;->_inputDecorator:LX/4pb;

    if-eqz v2, :cond_0

    .line 130782
    iget-object v0, p0, LX/0lp;->_inputDecorator:LX/4pb;

    invoke-virtual {v0}, LX/4pb;->c()Ljava/io/Reader;

    move-result-object v0

    .line 130783
    :cond_0
    invoke-virtual {p0, v0, v1}, LX/0lp;->a(Ljava/io/Reader;LX/12A;)LX/15w;

    move-result-object v0

    return-object v0
.end method

.method public a([B)LX/15w;
    .locals 3

    .prologue
    .line 130793
    const/4 v0, 0x1

    invoke-static {p1, v0}, LX/0lp;->a(Ljava/lang/Object;Z)LX/12A;

    move-result-object v0

    .line 130794
    iget-object v1, p0, LX/0lp;->_inputDecorator:LX/4pb;

    if-eqz v1, :cond_0

    .line 130795
    iget-object v1, p0, LX/0lp;->_inputDecorator:LX/4pb;

    invoke-virtual {v1}, LX/4pb;->b()Ljava/io/InputStream;

    move-result-object v1

    .line 130796
    if-eqz v1, :cond_0

    .line 130797
    invoke-virtual {p0, v1, v0}, LX/0lp;->a(Ljava/io/InputStream;LX/12A;)LX/15w;

    move-result-object v0

    .line 130798
    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    array-length v2, p1

    invoke-virtual {p0, p1, v1, v2, v0}, LX/0lp;->a([BIILX/12A;)LX/15w;

    move-result-object v0

    goto :goto_0
.end method

.method public a([BIILX/12A;)LX/15w;
    .locals 7

    .prologue
    .line 130792
    new-instance v0, LX/1pG;

    invoke-direct {v0, p4, p1, p2, p3}, LX/1pG;-><init>(LX/12A;[BII)V

    iget v1, p0, LX/0lp;->_parserFeatures:I

    iget-object v2, p0, LX/0lp;->_objectCodec:LX/0lD;

    iget-object v3, p0, LX/0lp;->f:LX/0lv;

    iget-object v4, p0, LX/0lp;->e:LX/0lt;

    sget-object v5, LX/0lq;->CANONICALIZE_FIELD_NAMES:LX/0lq;

    invoke-virtual {p0, v5}, LX/0lp;->a(LX/0lq;)Z

    move-result v5

    sget-object v6, LX/0lq;->INTERN_FIELD_NAMES:LX/0lq;

    invoke-virtual {p0, v6}, LX/0lp;->a(LX/0lq;)Z

    move-result v6

    invoke-virtual/range {v0 .. v6}, LX/1pG;->a(ILX/0lD;LX/0lv;LX/0lt;ZZ)LX/15w;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/OutputStream;LX/1pL;LX/12A;)Ljava/io/Writer;
    .locals 2

    .prologue
    .line 130789
    sget-object v0, LX/1pL;->UTF8:LX/1pL;

    if-ne p2, v0, :cond_0

    .line 130790
    new-instance v0, LX/4pf;

    invoke-direct {v0, p3, p1}, LX/4pf;-><init>(LX/12A;Ljava/io/OutputStream;)V

    .line 130791
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/io/OutputStreamWriter;

    invoke-virtual {p2}, LX/1pL;->getJavaName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(LX/0lq;)Z
    .locals 2

    .prologue
    .line 130788
    iget v0, p0, LX/0lp;->_factoryFeatures:I

    invoke-virtual {p1}, LX/0lq;->getMask()I

    move-result v1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/io/OutputStream;)LX/0nX;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 130787
    sget-object v0, LX/1pL;->UTF8:LX/1pL;

    invoke-virtual {p0, p1, v0}, LX/0lp;->a(Ljava/io/OutputStream;LX/1pL;)LX/0nX;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/15w;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 130786
    invoke-virtual {p0, p1}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v0

    return-object v0
.end method

.method public readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 130785
    new-instance v0, LX/0lp;

    invoke-direct {v0, p0}, LX/0lp;-><init>(LX/0lp;)V

    return-object v0
.end method

.method public version()LX/0ne;
    .locals 1

    .prologue
    .line 130784
    sget-object v0, LX/4ph;->VERSION:LX/0ne;

    return-object v0
.end method
