.class public LX/1BK;
.super LX/1BL;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1BK;


# instance fields
.field private final b:LX/1BM;

.field private final c:LX/1BO;


# direct methods
.method public constructor <init>(LX/1BM;LX/1BO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 212938
    invoke-direct {p0}, LX/1BL;-><init>()V

    .line 212939
    iput-object p1, p0, LX/1BK;->b:LX/1BM;

    .line 212940
    iput-object p2, p0, LX/1BK;->c:LX/1BO;

    .line 212941
    return-void
.end method

.method public static a(LX/0QB;)LX/1BK;
    .locals 5

    .prologue
    .line 212942
    sget-object v0, LX/1BK;->d:LX/1BK;

    if-nez v0, :cond_1

    .line 212943
    const-class v1, LX/1BK;

    monitor-enter v1

    .line 212944
    :try_start_0
    sget-object v0, LX/1BK;->d:LX/1BK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 212945
    if-eqz v2, :cond_0

    .line 212946
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 212947
    new-instance p0, LX/1BK;

    invoke-static {v0}, LX/1BM;->a(LX/0QB;)LX/1BM;

    move-result-object v3

    check-cast v3, LX/1BM;

    invoke-static {v0}, LX/1BO;->a(LX/0QB;)LX/1BO;

    move-result-object v4

    check-cast v4, LX/1BO;

    invoke-direct {p0, v3, v4}, LX/1BK;-><init>(LX/1BM;LX/1BO;)V

    .line 212948
    move-object v0, p0

    .line 212949
    sput-object v0, LX/1BK;->d:LX/1BK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212950
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 212951
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 212952
    :cond_1
    sget-object v0, LX/1BK;->d:LX/1BK;

    return-object v0

    .line 212953
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 212954
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)LX/1fG;
    .locals 3

    .prologue
    .line 212955
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/1fG;

    invoke-direct {v0, p3, p1}, LX/1fG;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 212956
    iget-object v1, p0, LX/1BL;->a:LX/0vX;

    invoke-interface {v1, p1, v0}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 212957
    iget-object v1, p0, LX/1BK;->b:LX/1BM;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, LX/1BM;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212958
    monitor-exit p0

    return-object v0

    .line 212959
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/1bf;)LX/2xm;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 212960
    iget-object v0, p0, LX/1BK;->c:LX/1BO;

    .line 212961
    invoke-virtual {v0}, LX/1BO;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 212962
    const/4 v1, 0x0

    .line 212963
    :goto_0
    move-object v0, v1

    .line 212964
    return-object v0

    .line 212965
    :cond_0
    new-instance v1, LX/2xm;

    iget-wide v3, v0, LX/1BO;->h:J

    invoke-direct {v1, v0, v3, v4}, LX/2xm;-><init>(LX/1BP;J)V

    .line 212966
    iget-object v2, v0, LX/1BO;->d:LX/1BS;

    .line 212967
    iget-object v3, v2, LX/1BS;->a:LX/0aq;

    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212968
    goto :goto_0
.end method
