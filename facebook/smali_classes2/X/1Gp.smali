.class public final LX/1Gp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1Iq;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1Iq;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 226101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226102
    iput-object p1, p0, LX/1Gp;->a:LX/0QB;

    .line 226103
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 226104
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/1Gp;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 226105
    packed-switch p2, :pswitch_data_0

    .line 226106
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 226107
    :pswitch_0
    invoke-static {p1}, LX/1Ii;->a(LX/0QB;)Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;

    move-result-object v0

    .line 226108
    :goto_0
    return-object v0

    .line 226109
    :pswitch_1
    invoke-static {p1}, LX/1Ir;->a(LX/0QB;)Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;

    move-result-object v0

    goto :goto_0

    .line 226110
    :pswitch_2
    invoke-static {p1}, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->a(LX/0QB;)Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 226111
    const/4 v0, 0x3

    return v0
.end method
