.class public LX/1XS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1B5;

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "LX/1fx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1B5;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 271196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271197
    iput-object p1, p0, LX/1XS;->a:LX/1B5;

    .line 271198
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/1XS;->b:Ljava/util/Map;

    .line 271199
    return-void
.end method

.method public static a(LX/1XS;Lcom/facebook/graphql/model/FeedUnit;LX/1fx;I)Z
    .locals 1

    .prologue
    .line 271200
    if-eqz p2, :cond_0

    .line 271201
    iget v0, p2, LX/1fx;->c:I

    move v0, v0

    .line 271202
    if-eq v0, p3, :cond_0

    .line 271203
    iget-object v0, p0, LX/1XS;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 271204
    const/4 v0, 0x1

    .line 271205
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/FeedUnit;I)I
    .locals 2
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 271206
    iget-object v0, p0, LX/1XS;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fx;

    .line 271207
    invoke-static {p0, p1, v0, p2}, LX/1XS;->a(LX/1XS;Lcom/facebook/graphql/model/FeedUnit;LX/1fx;I)Z

    move-result v1

    .line 271208
    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {v0}, LX/1fx;->a()I

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;III)V
    .locals 2

    .prologue
    .line 271209
    iget-object v0, p0, LX/1XS;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fx;

    .line 271210
    invoke-static {p0, p1, v0, p4}, LX/1XS;->a(LX/1XS;Lcom/facebook/graphql/model/FeedUnit;LX/1fx;I)Z

    move-result v1

    .line 271211
    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    .line 271212
    :cond_0
    new-instance v0, LX/1fx;

    invoke-direct {v0, p0, p4}, LX/1fx;-><init>(LX/1XS;I)V

    .line 271213
    iget-object v1, p0, LX/1XS;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271214
    move-object v0, v0

    .line 271215
    :cond_1
    invoke-virtual {v0, p2}, LX/1fx;->b(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 271216
    :goto_0
    invoke-static {v0}, LX/1fx;->c(LX/1fx;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 271217
    iget-object v1, p0, LX/1XS;->a:LX/1B5;

    invoke-virtual {v0}, LX/1fx;->a()I

    move-result v0

    .line 271218
    invoke-static {p1}, LX/1fz;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/162;

    move-result-object p0

    .line 271219
    new-instance p2, LX/1g1;

    sget-object p3, LX/1g2;->FEED_UNIT_HEIGHT:LX/1g2;

    invoke-direct {p2, p3, p1, p0}, LX/1g1;-><init>(LX/1g2;Ljava/lang/Object;LX/162;)V

    .line 271220
    iput v0, p2, LX/1g1;->j:I

    .line 271221
    move-object p0, p2

    .line 271222
    invoke-static {v1, p0}, LX/1B5;->a(LX/1B5;LX/1g1;)V

    .line 271223
    :cond_2
    return-void

    .line 271224
    :cond_3
    iget v1, v0, LX/1fx;->d:I

    add-int/2addr v1, p3

    iput v1, v0, LX/1fx;->d:I

    .line 271225
    iget-object v1, v0, LX/1fx;->b:[I

    aput p3, v1, p2

    .line 271226
    iget v1, v0, LX/1fx;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/1fx;->e:I

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/FeedUnit;II)Z
    .locals 2

    .prologue
    .line 271227
    iget-object v0, p0, LX/1XS;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fx;

    .line 271228
    invoke-static {p0, p1, v0, p3}, LX/1XS;->a(LX/1XS;Lcom/facebook/graphql/model/FeedUnit;LX/1fx;I)Z

    move-result v1

    .line 271229
    if-eqz v0, :cond_0

    if-nez v1, :cond_0

    invoke-virtual {v0, p2}, LX/1fx;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
