.class public LX/1DL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1DL;


# instance fields
.field private final a:LX/1DN;

.field private final b:LX/1DM;

.field private final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/1DM;LX/1DN;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 217396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217397
    iput-object p1, p0, LX/1DL;->b:LX/1DM;

    .line 217398
    iput-object p2, p0, LX/1DL;->a:LX/1DN;

    .line 217399
    iput-object p3, p0, LX/1DL;->c:LX/0ad;

    .line 217400
    return-void
.end method

.method public static a(LX/0QB;)LX/1DL;
    .locals 6

    .prologue
    .line 217381
    sget-object v0, LX/1DL;->d:LX/1DL;

    if-nez v0, :cond_1

    .line 217382
    const-class v1, LX/1DL;

    monitor-enter v1

    .line 217383
    :try_start_0
    sget-object v0, LX/1DL;->d:LX/1DL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 217384
    if-eqz v2, :cond_0

    .line 217385
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 217386
    new-instance p0, LX/1DL;

    const-class v3, LX/1DM;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1DM;

    const-class v4, LX/1DN;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1DN;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/1DL;-><init>(LX/1DM;LX/1DN;LX/0ad;)V

    .line 217387
    move-object v0, p0

    .line 217388
    sput-object v0, LX/1DL;->d:LX/1DL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 217389
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 217390
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 217391
    :cond_1
    sget-object v0, LX/1DL;->d:LX/1DL;

    return-object v0

    .line 217392
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 217393
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Jg;
    .locals 7

    .prologue
    const/16 v6, 0x14

    .line 217394
    new-instance v0, LX/1JV;

    iget-object v1, p0, LX/1DL;->c:LX/0ad;

    sget v2, LX/1JW;->b:I

    const/16 v3, 0x32

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    iget-object v2, p0, LX/1DL;->c:LX/0ad;

    sget v3, LX/1JW;->e:I

    invoke-interface {v2, v3, v6}, LX/0ad;->a(II)I

    move-result v2

    iget-object v3, p0, LX/1DL;->c:LX/0ad;

    sget v4, LX/1JW;->a:I

    const/16 v5, 0x3c

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    iget-object v4, p0, LX/1DL;->c:LX/0ad;

    sget v5, LX/1JW;->d:I

    invoke-interface {v4, v5, v6}, LX/0ad;->a(II)I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, LX/1JV;-><init>(IIII)V

    .line 217395
    iget-object v1, p0, LX/1DL;->a:LX/1DN;

    iget-object v2, p0, LX/1DL;->b:LX/1DM;

    invoke-virtual {v2, v0}, LX/1DM;->a(LX/1JV;)LX/1JX;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1DN;->a(LX/1JX;)LX/1Jf;

    move-result-object v0

    return-object v0
.end method
