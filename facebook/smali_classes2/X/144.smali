.class public LX/144;
.super LX/140;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 177882
    invoke-direct {p0}, LX/140;-><init>()V

    .line 177883
    iput-object p1, p0, LX/144;->a:LX/0Or;

    .line 177884
    return-void
.end method


# virtual methods
.method public final a()LX/2o6;
    .locals 1

    .prologue
    .line 177881
    sget-object v0, LX/2o6;->ZERO_RATING:LX/2o6;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177879
    const-string v0, "zero_rating"

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 177880
    iget-object v0, p0, LX/144;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
