.class public final enum LX/0xq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0xq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0xq;

.field public static final enum BUILTIN:LX/0xq;

.field public static final enum HELVETICA_NEUE:LX/0xq;

.field public static final enum ROBOTO:LX/0xq;


# instance fields
.field public final paths:[Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 163488
    new-instance v0, LX/0xq;

    const-string v1, "BUILTIN"

    new-array v2, v5, [Ljava/lang/String;

    invoke-direct {v0, v1, v5, v2}, LX/0xq;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, LX/0xq;->BUILTIN:LX/0xq;

    .line 163489
    new-instance v0, LX/0xq;

    const-string v1, "HELVETICA_NEUE"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "fonts/HelveticaNeue-Thin.ttf"

    aput-object v3, v2, v5

    const-string v3, "fonts/HelveticaNeue-Light.ttf"

    aput-object v3, v2, v6

    const-string v3, "fonts/HelveticaNeue-Roman.ttf"

    aput-object v3, v2, v7

    const-string v3, "fonts/HelveticaNeue-Medium.ttf"

    aput-object v3, v2, v8

    const-string v3, "fonts/HelveticaNeue-Bold.ttf"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "fonts/HelveticaNeue-Black.ttf"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v6, v2}, LX/0xq;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, LX/0xq;->HELVETICA_NEUE:LX/0xq;

    .line 163490
    new-instance v0, LX/0xq;

    const-string v1, "ROBOTO"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "fonts/Roboto-Thin.ttf"

    aput-object v3, v2, v5

    const-string v3, "fonts/Roboto-Light.ttf"

    aput-object v3, v2, v6

    const-string v3, "fonts/Roboto-Regular.ttf"

    aput-object v3, v2, v7

    const-string v3, "fonts/Roboto-Medium.ttf"

    aput-object v3, v2, v8

    const-string v3, "fonts/Roboto-Bold.ttf"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "fonts/Roboto-Black.ttf"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v7, v2}, LX/0xq;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, LX/0xq;->ROBOTO:LX/0xq;

    .line 163491
    new-array v0, v8, [LX/0xq;

    sget-object v1, LX/0xq;->BUILTIN:LX/0xq;

    aput-object v1, v0, v5

    sget-object v1, LX/0xq;->HELVETICA_NEUE:LX/0xq;

    aput-object v1, v0, v6

    sget-object v1, LX/0xq;->ROBOTO:LX/0xq;

    aput-object v1, v0, v7

    sput-object v0, LX/0xq;->$VALUES:[LX/0xq;

    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;I[Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 163492
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 163493
    iput-object p3, p0, LX/0xq;->paths:[Ljava/lang/String;

    .line 163494
    return-void
.end method

.method public static fromIndex(I)LX/0xq;
    .locals 1

    .prologue
    .line 163495
    invoke-static {}, LX/0xq;->values()[LX/0xq;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0xq;
    .locals 1

    .prologue
    .line 163496
    const-class v0, LX/0xq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0xq;

    return-object v0
.end method

.method public static values()[LX/0xq;
    .locals 1

    .prologue
    .line 163497
    sget-object v0, LX/0xq;->$VALUES:[LX/0xq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0xq;

    return-object v0
.end method
