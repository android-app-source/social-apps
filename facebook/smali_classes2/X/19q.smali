.class public LX/19q;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 208781
    invoke-direct {p0}, LX/0Q6;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;LX/0wp;LX/0wq;LX/0ad;LX/0So;LX/0Ot;LX/19j;LX/0Xl;LX/0kb;LX/0Ot;LX/19l;LX/19m;LX/03k;LX/0Or;)LX/19s;
    .locals 20
    .param p4    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .param p7    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p13    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0wp;",
            "LX/0wq;",
            "LX/0ad;",
            "LX/0So;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/19j;",
            "LX/0Xl;",
            "LX/0kb;",
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;",
            "LX/19l;",
            "LX/19m;",
            "LX/03k;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)",
            "LX/19s;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 208659
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 208660
    sget-object v2, LX/040;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v3, v0, LX/0wp;->A:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208661
    sget-object v2, LX/040;->b:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v3, v0, LX/0wp;->z:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208662
    sget-object v2, LX/040;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v3, v0, LX/0wp;->B:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208663
    sget-object v2, LX/040;->d:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-object v3, v0, LX/19j;->c:LX/19k;

    invoke-virtual {v3}, LX/19k;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208664
    sget-object v3, LX/040;->e:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->aA:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208665
    sget-object v2, LX/040;->f:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->aB:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208666
    sget-object v2, LX/040;->g:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->aC:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208667
    sget-object v2, LX/040;->h:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->aD:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208668
    sget-object v2, LX/040;->i:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->aE:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208669
    sget-object v2, LX/040;->j:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->aF:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208670
    sget-object v2, LX/040;->k:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->aG:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208671
    sget-object v2, LX/040;->m:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->aH:F

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208672
    sget-object v2, LX/040;->n:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->aI:F

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208673
    sget-object v3, LX/040;->l:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->aJ:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208674
    sget-object v3, LX/040;->o:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->aK:Z

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208675
    sget-object v3, LX/040;->p:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->aL:Z

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208676
    sget-object v3, LX/040;->r:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->aM:Z

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208677
    sget-object v3, LX/040;->s:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->aO:Z

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    :goto_5
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208678
    sget-object v3, LX/040;->t:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->m:Z

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    :goto_6
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208679
    sget-object v3, LX/040;->u:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->Z:Z

    if-eqz v2, :cond_b

    const/4 v2, 0x1

    :goto_7
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208680
    sget-object v3, LX/040;->S:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->aa:Z

    if-eqz v2, :cond_c

    const/4 v2, 0x1

    :goto_8
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208681
    sget-object v3, LX/040;->C:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->X:Z

    if-eqz v2, :cond_d

    const/4 v2, 0x1

    :goto_9
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208682
    sget-object v3, LX/040;->D:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->Y:Z

    if-eqz v2, :cond_e

    const/4 v2, 0x1

    :goto_a
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208683
    sget-object v3, LX/040;->v:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->n:Z

    if-eqz v2, :cond_f

    const/4 v2, 0x1

    :goto_b
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208684
    sget-object v3, LX/040;->w:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->q:Z

    if-eqz v2, :cond_10

    const/4 v2, 0x1

    :goto_c
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208685
    sget-object v2, LX/040;->z:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->U:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208686
    sget-object v2, LX/040;->A:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->V:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208687
    sget-object v3, LX/040;->B:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->W:Z

    if-eqz v2, :cond_11

    const/4 v2, 0x1

    :goto_d
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208688
    sget-object v2, LX/040;->K:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->y:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208689
    sget-object v2, LX/040;->M:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->z:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208690
    sget-object v2, LX/040;->y:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-wide v4, v0, LX/19j;->I:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208691
    sget-object v2, LX/040;->Q:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->L:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208692
    sget-object v2, LX/040;->R:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->M:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208693
    sget-object v3, LX/040;->N:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->N:Z

    if-eqz v2, :cond_12

    const/4 v2, 0x1

    :goto_e
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208694
    sget-object v2, LX/040;->O:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->O:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208695
    sget-object v2, LX/040;->P:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->P:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208696
    sget-object v3, LX/040;->U:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->ab:Z

    if-eqz v2, :cond_13

    const/4 v2, 0x1

    :goto_f
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208697
    sget-object v2, LX/040;->T:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->af:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208698
    sget-object v2, LX/040;->E:Ljava/lang/String;

    move-object/from16 v0, p2

    iget v3, v0, LX/0wq;->k:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208699
    sget-object v2, LX/040;->G:Ljava/lang/String;

    move-object/from16 v0, p2

    iget v3, v0, LX/0wq;->R:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208700
    sget-object v2, LX/040;->F:Ljava/lang/String;

    move-object/from16 v0, p2

    iget v3, v0, LX/0wq;->l:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208701
    sget-object v2, LX/040;->H:Ljava/lang/String;

    move-object/from16 v0, p2

    iget v3, v0, LX/0wq;->m:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208702
    sget-object v2, LX/040;->I:Ljava/lang/String;

    move-object/from16 v0, p2

    iget v3, v0, LX/0wq;->n:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208703
    sget-object v2, LX/040;->J:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v3, v0, LX/0wp;->H:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208704
    sget-object v2, LX/040;->L:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v3, v0, LX/0wp;->I:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208705
    sget-object v2, LX/040;->V:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v3, v0, LX/0wp;->J:F

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208706
    sget-object v2, LX/040;->W:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v3, v0, LX/0wp;->K:F

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208707
    sget-object v2, LX/040;->X:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->A:F

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208708
    sget-object v2, LX/040;->Y:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->B:F

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208709
    sget-object v3, LX/040;->Z:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-boolean v2, v0, LX/0wp;->L:Z

    if-eqz v2, :cond_14

    const-string v2, "1"

    :goto_10
    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208710
    sget-object v3, LX/040;->aa:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->x:Z

    if-eqz v2, :cond_15

    const-string v2, "1"

    :goto_11
    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208711
    sget-object v3, LX/040;->ac:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-boolean v2, v0, LX/0wq;->N:Z

    if-eqz v2, :cond_16

    const/4 v2, 0x1

    :goto_12
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208712
    sget-object v2, LX/040;->ae:Ljava/lang/String;

    move-object/from16 v0, p2

    iget v3, v0, LX/0wq;->P:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208713
    sget-object v3, LX/040;->af:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-boolean v2, v0, LX/0wq;->Q:Z

    if-eqz v2, :cond_17

    const/4 v2, 0x1

    :goto_13
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208714
    sget-object v3, LX/040;->ak:Ljava/lang/String;

    invoke-interface/range {p13 .. p13}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    sget-object v4, LX/03R;->YES:LX/03R;

    if-eq v2, v4, :cond_0

    invoke-static {}, LX/007;->g()Z

    move-result v2

    if-eqz v2, :cond_18

    :cond_0
    const/4 v2, 0x1

    :goto_14
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208715
    sget-object v3, LX/040;->ag:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->u:Z

    if-eqz v2, :cond_19

    const/4 v2, 0x1

    :goto_15
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208716
    sget-object v3, LX/040;->ad:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->w:Z

    if-eqz v2, :cond_1a

    const/4 v2, 0x1

    :goto_16
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208717
    sget-object v2, LX/040;->al:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->ai:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208718
    sget-object v2, LX/040;->am:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-wide v4, v0, LX/19j;->aj:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208719
    sget-object v3, LX/040;->an:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->ak:Z

    if-eqz v2, :cond_1b

    const/4 v2, 0x1

    :goto_17
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208720
    sget-object v2, LX/040;->ap:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-wide v4, v0, LX/19j;->an:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208721
    sget-object v3, LX/040;->aq:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->ao:Z

    if-eqz v2, :cond_1c

    const/4 v2, 0x1

    :goto_18
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208722
    sget-object v3, LX/040;->ar:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->ap:Z

    if-eqz v2, :cond_1d

    const/4 v2, 0x1

    :goto_19
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208723
    sget-object v3, LX/040;->ay:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->ay:Z

    if-eqz v2, :cond_1e

    const/4 v2, 0x1

    :goto_1a
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208724
    move-object/from16 v0, p6

    iget v2, v0, LX/19j;->C:I

    if-lez v2, :cond_1

    .line 208725
    const-string v2, "video.live.min_buffer_ms"

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->C:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208726
    :cond_1
    move-object/from16 v0, p6

    iget v2, v0, LX/19j;->D:I

    if-lez v2, :cond_2

    .line 208727
    const-string v2, "video.live.min_rebuffer_ms"

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->D:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208728
    :cond_2
    sget-object v3, LX/040;->ah:Ljava/lang/String;

    invoke-virtual/range {p11 .. p11}, LX/19m;->k()Z

    move-result v2

    if-eqz v2, :cond_1f

    const/4 v2, 0x1

    :goto_1b
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208729
    sget-object v2, LX/040;->ai:Ljava/lang/String;

    invoke-virtual/range {p11 .. p11}, LX/19m;->q()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208730
    sget-object v2, LX/040;->aj:Ljava/lang/String;

    invoke-virtual/range {p11 .. p11}, LX/19m;->r()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208731
    sget-object v3, LX/040;->ao:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->al:Z

    if-eqz v2, :cond_20

    const/4 v2, 0x1

    :goto_1c
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208732
    sget-object v3, LX/040;->as:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->aq:Z

    if-eqz v2, :cond_21

    const/4 v2, 0x1

    :goto_1d
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208733
    sget-object v3, LX/040;->at:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->ar:Z

    if-eqz v2, :cond_22

    const/4 v2, 0x1

    :goto_1e
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208734
    sget-object v2, LX/040;->au:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->as:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208735
    sget-object v2, LX/040;->av:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->at:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208736
    sget-object v2, LX/040;->aw:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->au:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208737
    sget-object v3, LX/040;->ax:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->aw:Z

    if-eqz v2, :cond_23

    const/4 v2, 0x1

    :goto_1f
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208738
    sget-object v3, LX/040;->az:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-boolean v2, v0, LX/19j;->ax:Z

    if-eqz v2, :cond_24

    const/4 v2, 0x1

    :goto_20
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208739
    sget-object v2, LX/040;->q:Ljava/lang/String;

    move-object/from16 v0, p6

    iget v3, v0, LX/19j;->aW:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208740
    new-instance v19, Landroid/os/Bundle;

    invoke-direct/range {v19 .. v19}, Landroid/os/Bundle;-><init>()V

    .line 208741
    new-instance v2, LX/0P2;

    invoke-direct {v2}, LX/0P2;-><init>()V

    sget-object v3, LX/0p3;->EXCELLENT:LX/0p3;

    move-object/from16 v0, p6

    iget v4, v0, LX/19j;->J:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    sget-object v3, LX/0p3;->GOOD:LX/0p3;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    sget-object v3, LX/0p3;->MODERATE:LX/0p3;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    sget-object v3, LX/0p3;->POOR:LX/0p3;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    sget-object v3, LX/0p3;->UNKNOWN:LX/0p3;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v14

    .line 208742
    new-instance v8, LX/19r;

    move-object/from16 v0, p10

    move-object/from16 v1, p5

    invoke-direct {v8, v0, v1}, LX/19r;-><init>(LX/19l;LX/0Ot;)V

    .line 208743
    new-instance v2, LX/19s;

    move-object/from16 v0, p6

    iget-boolean v12, v0, LX/19j;->G:Z

    move-object/from16 v0, p6

    iget-boolean v15, v0, LX/19j;->Q:Z

    move-object/from16 v0, p6

    iget-object v0, v0, LX/19j;->R:Ljava/util/Map;

    move-object/from16 v16, v0

    move-object/from16 v0, p6

    iget-object v0, v0, LX/19j;->c:LX/19k;

    move-object/from16 v17, v0

    move-object/from16 v0, p6

    iget-boolean v0, v0, LX/19j;->K:Z

    move/from16 v18, v0

    move-object/from16 v3, p0

    move-object/from16 v4, p3

    move-object/from16 v5, p2

    move-object/from16 v7, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v13, p9

    invoke-direct/range {v2 .. v18}, LX/19s;-><init>(Landroid/content/Context;LX/0ad;LX/0wq;Ljava/util/Map;LX/0So;LX/19r;LX/0Ot;LX/0Xl;LX/0kb;ZLX/0Ot;Ljava/util/Map;ZLjava/util/Map;LX/19k;Z)V

    .line 208744
    invoke-virtual/range {p2 .. p2}, LX/0wq;->i()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 208745
    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, LX/19s;->a(Landroid/os/Bundle;)V

    .line 208746
    :cond_3
    move-object/from16 v0, p12

    move-object/from16 v1, p3

    invoke-static {v0, v1}, LX/19q;->a(LX/03k;LX/0ad;)V

    .line 208747
    return-object v2

    .line 208748
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 208749
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 208750
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 208751
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 208752
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 208753
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 208754
    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_6

    .line 208755
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 208756
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_8

    .line 208757
    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_9

    .line 208758
    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_a

    .line 208759
    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_b

    .line 208760
    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_c

    .line 208761
    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_d

    .line 208762
    :cond_12
    const/4 v2, 0x0

    goto/16 :goto_e

    .line 208763
    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_f

    .line 208764
    :cond_14
    const-string v2, "0"

    goto/16 :goto_10

    .line 208765
    :cond_15
    const-string v2, "0"

    goto/16 :goto_11

    .line 208766
    :cond_16
    const/4 v2, 0x0

    goto/16 :goto_12

    .line 208767
    :cond_17
    const/4 v2, 0x0

    goto/16 :goto_13

    .line 208768
    :cond_18
    const/4 v2, 0x0

    goto/16 :goto_14

    .line 208769
    :cond_19
    const/4 v2, 0x0

    goto/16 :goto_15

    .line 208770
    :cond_1a
    const/4 v2, 0x0

    goto/16 :goto_16

    .line 208771
    :cond_1b
    const/4 v2, 0x0

    goto/16 :goto_17

    .line 208772
    :cond_1c
    const/4 v2, 0x0

    goto/16 :goto_18

    .line 208773
    :cond_1d
    const/4 v2, 0x0

    goto/16 :goto_19

    .line 208774
    :cond_1e
    const/4 v2, 0x0

    goto/16 :goto_1a

    .line 208775
    :cond_1f
    const/4 v2, 0x0

    goto/16 :goto_1b

    .line 208776
    :cond_20
    const/4 v2, 0x0

    goto/16 :goto_1c

    .line 208777
    :cond_21
    const/4 v2, 0x0

    goto/16 :goto_1d

    .line 208778
    :cond_22
    const/4 v2, 0x0

    goto/16 :goto_1e

    .line 208779
    :cond_23
    const/4 v2, 0x0

    goto/16 :goto_1f

    .line 208780
    :cond_24
    const/4 v2, 0x0

    goto/16 :goto_20
.end method

.method private static a(LX/03k;LX/0ad;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 208654
    sget-short v0, LX/0ws;->eZ:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208655
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/03k;->a(Z)V

    .line 208656
    :goto_0
    return-void

    .line 208657
    :cond_0
    invoke-virtual {p0, v1}, LX/03k;->a(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 208658
    return-void
.end method
