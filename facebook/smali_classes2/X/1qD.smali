.class public LX/1qD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final mAnalyticsLogger:LX/0Zb;

.field private final mAppInitLock:LX/0Uq;

.field private final mBackgroundWorkLogger:LX/0Sj;

.field private final mBlueServiceHandlerProvider:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1qM;",
            ">;"
        }
    .end annotation
.end field

.field public final mBlueServiceQueueManager:LX/1mM;

.field private final mClock:LX/0So;

.field private final mCompletedOperations:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/1qE;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public volatile mCurrentDeferredOperationHolder:LX/1qF;

.field private mCurrentOperationHolder:LX/1qF;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final mFbErrorReporter:LX/03V;

.field public mHandlerExecutorService:LX/0Te;

.field public final mHandlerExecutorServiceFactory:LX/0YF;

.field private mIsQueueEmptyNotified:Z

.field private final mOperationsById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1qF;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final mQueueHooks:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/fbservice/service/BlueServiceQueueHook;",
            ">;"
        }
    .end annotation
.end field

.field public final mQueueName:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private final mQueuedOperations:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/1qE;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final mSoftErrorHelper:LX/1mP;

.field private final mSoftErrorReportingGkProvider:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final mStoppedCalled:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final mThreadPriority:LX/0TP;

.field private final mViewerContextManager:LX/0SI;


# direct methods
.method public constructor <init>(Ljava/lang/Class;LX/0Or;Ljava/util/Set;LX/0YF;LX/1mM;LX/0SI;LX/03V;LX/0Zb;LX/0So;LX/0Sj;LX/0TP;LX/1mP;LX/0Or;LX/0Uq;)V
    .locals 3
    .param p13    # LX/0Or;
        .annotation runtime Lcom/facebook/fbservice/service/OrcaServiceSoftErrorReportingGk;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1qM;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/fbservice/service/BlueServiceQueueHook;",
            ">;",
            "Lcom/facebook/common/executors/HandlerExecutorServiceFactory;",
            "LX/1mM;",
            "LX/0SI;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Zb;",
            "LX/0So;",
            "LX/0Sj;",
            "LX/0TP;",
            "LX/1mP;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Uq;",
            ")V"
        }
    .end annotation

    .prologue
    .line 330373
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330374
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, LX/1qD;->mStoppedCalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 330375
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/1qD;->mIsQueueEmptyNotified:Z

    .line 330376
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v1

    iput-object v1, p0, LX/1qD;->mQueuedOperations:Ljava/util/LinkedList;

    .line 330377
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v1

    iput-object v1, p0, LX/1qD;->mCompletedOperations:Ljava/util/LinkedList;

    .line 330378
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, LX/1qD;->mOperationsById:Ljava/util/Map;

    .line 330379
    iput-object p1, p0, LX/1qD;->mQueueName:Ljava/lang/Class;

    .line 330380
    iput-object p2, p0, LX/1qD;->mBlueServiceHandlerProvider:LX/0Or;

    .line 330381
    iput-object p3, p0, LX/1qD;->mQueueHooks:Ljava/util/Set;

    .line 330382
    iput-object p4, p0, LX/1qD;->mHandlerExecutorServiceFactory:LX/0YF;

    .line 330383
    iput-object p5, p0, LX/1qD;->mBlueServiceQueueManager:LX/1mM;

    .line 330384
    iput-object p6, p0, LX/1qD;->mViewerContextManager:LX/0SI;

    .line 330385
    iput-object p7, p0, LX/1qD;->mFbErrorReporter:LX/03V;

    .line 330386
    iput-object p8, p0, LX/1qD;->mAnalyticsLogger:LX/0Zb;

    .line 330387
    iput-object p9, p0, LX/1qD;->mClock:LX/0So;

    .line 330388
    iput-object p10, p0, LX/1qD;->mBackgroundWorkLogger:LX/0Sj;

    .line 330389
    iput-object p11, p0, LX/1qD;->mThreadPriority:LX/0TP;

    .line 330390
    iput-object p12, p0, LX/1qD;->mSoftErrorHelper:LX/1mP;

    .line 330391
    move-object/from16 v0, p13

    iput-object v0, p0, LX/1qD;->mSoftErrorReportingGkProvider:LX/0Or;

    .line 330392
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1qD;->mAppInitLock:LX/0Uq;

    .line 330393
    return-void
.end method

.method private cleanUpThreadState()V
    .locals 0

    .prologue
    .line 330370
    sget-object p0, LX/129;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {p0}, Ljava/lang/ThreadLocal;->remove()V

    .line 330371
    sget-object p0, LX/14T;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {p0}, Ljava/lang/ThreadLocal;->remove()V

    .line 330372
    return-void
.end method

.method private static declared-synchronized countOldOperations(LX/1qD;)I
    .locals 1

    .prologue
    .line 330369
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1qD;->mCompletedOperations:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized operationDeferred(LX/1qF;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1qF;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 330361
    monitor-enter p0

    :try_start_0
    const-string v0, "BlueServiceQueue.operationDeferred"

    const v1, -0x6def63ec

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 330362
    :try_start_1
    iput-object p2, p1, LX/1qF;->future:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 330363
    iput-object p1, p0, LX/1qD;->mCurrentDeferredOperationHolder:LX/1qF;

    .line 330364
    iget-object v0, p1, LX/1qF;->future:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/4BA;

    invoke-direct {v1, p0, p1}, LX/4BA;-><init>(LX/1qD;LX/1qF;)V

    iget-object v2, p0, LX/1qD;->mHandlerExecutorService:LX/0Te;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 330365
    const v0, 0x30330a1f

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 330366
    monitor-exit p0

    return-void

    .line 330367
    :catchall_0
    move-exception v0

    const v1, 0x235f9366

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 330368
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized operationDone(LX/1qD;LX/1qF;Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 4

    .prologue
    .line 330345
    monitor-enter p0

    :try_start_0
    const-string v0, "BlueServiceQueue.operationDone"

    const v1, 0x1d448d05

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 330346
    :try_start_1
    iput-object p2, p1, LX/1qF;->result:Lcom/facebook/fbservice/service/OperationResult;

    .line 330347
    iget-object v0, p0, LX/1qD;->mClock:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 330348
    iput-wide v0, p1, LX/1qF;->endTime:J

    .line 330349
    iget-object v0, p0, LX/1qD;->mCompletedOperations:Ljava/util/LinkedList;

    iget-object v1, p1, LX/1qF;->operation:LX/1qE;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 330350
    iget-object v0, p0, LX/1qD;->mCurrentOperationHolder:LX/1qF;

    if-ne v0, p1, :cond_0

    .line 330351
    const/4 v0, 0x0

    iput-object v0, p0, LX/1qD;->mCurrentOperationHolder:LX/1qF;

    .line 330352
    :cond_0
    iget-object v0, p1, LX/1qF;->handlers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 330353
    :try_start_2
    iget-object v2, p1, LX/1qF;->result:Lcom/facebook/fbservice/service/OperationResult;

    invoke-interface {v0, v2}, LX/1qB;->onOperationCompleted(Lcom/facebook/fbservice/service/OperationResult;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 330354
    :catch_0
    goto :goto_0

    .line 330355
    :cond_1
    const/4 v0, 0x0

    .line 330356
    :try_start_3
    iput-object v0, p1, LX/1qF;->handlers:Ljava/util/List;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 330357
    const v0, 0x658b1a21

    :try_start_4
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 330358
    monitor-exit p0

    return-void

    .line 330359
    :catchall_0
    move-exception v0

    const v1, -0x59d1058a

    :try_start_5
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 330360
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized operationProgress(LX/1qD;LX/1qF;Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 2

    .prologue
    .line 330339
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, LX/1qF;->handlers:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 330340
    iget-object v0, p1, LX/1qF;->handlers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qB;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330341
    :try_start_1
    invoke-interface {v0, p2}, LX/1qB;->onOperationProgress(Lcom/facebook/fbservice/service/OperationResult;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 330342
    :catch_0
    goto :goto_0

    .line 330343
    :cond_0
    monitor-exit p0

    return-void

    .line 330344
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static processQueue(LX/1qD;)V
    .locals 17

    .prologue
    const/4 v11, 0x0

    const-wide/16 v14, 0x0

    .line 330256
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1qD;->mCurrentDeferredOperationHolder:LX/1qF;

    if-nez v2, :cond_2

    .line 330257
    invoke-static/range {p0 .. p0}, LX/1qD;->countOldOperations(LX/1qD;)I

    move-result v2

    const/16 v3, 0x20

    if-lt v2, v3, :cond_0

    .line 330258
    invoke-virtual/range {p0 .. p0}, LX/1qD;->getQueueName()Ljava/lang/Class;

    .line 330259
    invoke-static/range {p0 .. p0}, LX/1qD;->removeOldOperations(LX/1qD;)V

    .line 330260
    :cond_0
    monitor-enter p0

    .line 330261
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1qD;->mQueuedOperations:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 330262
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/1qD;->mIsQueueEmptyNotified:Z

    if-nez v2, :cond_1

    .line 330263
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/1qD;->mIsQueueEmptyNotified:Z

    .line 330264
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1qD;->mQueueHooks:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1qI;

    .line 330265
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1qD;->mQueueName:Ljava/lang/Class;

    invoke-virtual {v2, v4}, LX/1qI;->onQueueEmpty(Ljava/lang/Class;)V

    goto :goto_1

    .line 330266
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 330267
    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 330268
    :cond_2
    invoke-static/range {p0 .. p0}, LX/1qD;->removeOldOperations(LX/1qD;)V

    .line 330269
    return-void

    .line 330270
    :cond_3
    const/4 v2, 0x0

    :try_start_2
    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/1qD;->mIsQueueEmptyNotified:Z

    .line 330271
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1qD;->mQueuedOperations:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, LX/1qE;

    move-object v9, v0

    .line 330272
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1qD;->mOperationsById:Ljava/util/Map;

    invoke-virtual {v9}, LX/1qE;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, LX/1qF;

    move-object v10, v0

    .line 330273
    if-nez v10, :cond_4

    .line 330274
    const-string v2, "BlueServiceQueue"

    const-string v3, "No holder for queued operation!"

    invoke-static {v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 330275
    monitor-exit p0

    goto :goto_0

    .line 330276
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1qD;->mClock:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    # setter for: LX/1qF;->startTime:J
    invoke-static {v10, v2, v3}, LX/1qF;->access$702(LX/1qF;J)J

    .line 330277
    move-object/from16 v0, p0

    iput-object v10, v0, LX/1qD;->mCurrentOperationHolder:LX/1qF;

    .line 330278
    invoke-virtual {v9}, LX/1qE;->getCanRunBeforeAppInit()Z

    move-result v2

    if-nez v2, :cond_5

    .line 330279
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1qD;->mAppInitLock:LX/0Uq;

    invoke-virtual {v2}, LX/0Uq;->b()V

    .line 330280
    :cond_5
    invoke-static {v10}, LX/1qF;->startTask(LX/1qF;)V

    .line 330281
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 330282
    const/4 v2, 0x3

    invoke-static {v2}, LX/01m;->b(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 330283
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "****** BlueService ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, LX/1qE;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330284
    :cond_6
    const-string v2, "BlueService (%s)"

    invoke-virtual {v9}, LX/1qE;->getType()Ljava/lang/String;

    move-result-object v3

    const v4, 0x4e8e906

    invoke-static {v2, v3, v4}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 330285
    :try_start_3
    const-string v2, "queuedTime: %d ms"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v10}, LX/1qF;->getElapsedQueuedTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, LX/0PR;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 330286
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1qD;->mStoppedCalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 330287
    new-instance v2, Ljava/util/concurrent/CancellationException;

    const-string v3, "Queue stopped"

    invoke-direct {v2, v3}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 330288
    :catch_0
    move-exception v2

    .line 330289
    :try_start_4
    const-string v3, "BlueServiceQueue"

    const-string v4, "Exception during service"

    invoke-static {v3, v4, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 330290
    invoke-virtual {v9}, LX/1qE;->getType()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/1qD;->maybeReportSoftError(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 330291
    invoke-virtual {v9}, LX/1qE;->getUseExceptionResult()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 330292
    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 330293
    :goto_2
    move-object/from16 v0, p0

    invoke-static {v0, v10, v2}, LX/1qD;->operationDone(LX/1qD;LX/1qF;Lcom/facebook/fbservice/service/OperationResult;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 330294
    const v2, 0x61e00e24

    invoke-static {v14, v15, v2}, LX/02m;->a(JI)V

    .line 330295
    invoke-static {v10}, LX/1qF;->stopTask(LX/1qF;)V

    goto/16 :goto_0

    .line 330296
    :cond_7
    :try_start_5
    invoke-virtual {v9}, LX/1qE;->getParams()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "overridden_viewer_context"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/auth/viewercontext/ViewerContext;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 330297
    :try_start_6
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1qD;->mViewerContextManager:LX/0SI;

    invoke-interface {v3, v2}, LX/0SI;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    move-result-object v12

    .line 330298
    :try_start_7
    invoke-virtual {v9}, LX/1qE;->getCallerContext()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    .line 330299
    iget-object v2, v10, LX/1qF;->requestState:LX/0zW;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v7}, LX/1qD;->setUpThreadState(LX/0zW;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 330300
    new-instance v2, LX/1qK;

    invoke-virtual {v9}, LX/1qE;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, LX/1qE;->getParams()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v9}, LX/1qE;->getId()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v10, LX/1qF;->requestState:LX/0zW;

    iget-object v8, v10, LX/1qF;->callback:LX/1qH;

    invoke-direct/range {v2 .. v8}, LX/1qK;-><init>(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;LX/0zW;Lcom/facebook/common/callercontext/CallerContext;LX/1qH;)V

    .line 330301
    const-string v3, "BlueServiceHandlerProvider.get()"

    const v4, -0x31adeb77

    invoke-static {v3, v4}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_6

    .line 330302
    :try_start_8
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1qD;->mBlueServiceHandlerProvider:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1qM;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 330303
    const v4, -0x2aaaa317

    :try_start_9
    invoke-static {v4}, LX/02m;->a(I)V

    .line 330304
    monitor-enter p0
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    .line 330305
    :try_start_a
    iget-boolean v4, v10, LX/1qF;->cancelled:Z

    if-eqz v4, :cond_f

    .line 330306
    sget-object v4, LX/1nY;->CANCELLED:LX/1nY;

    invoke-static {v4}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v4

    .line 330307
    :goto_3
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    .line 330308
    if-nez v4, :cond_e

    .line 330309
    :try_start_b
    const-string v4, "BlueServiceHandler.handleOperation"

    const v5, 0x3cf58293

    invoke-static {v4, v5}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_6

    .line 330310
    :try_start_c
    invoke-interface {v3, v2}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_c
    .catch Ljava/util/concurrent/CancellationException; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_8

    move-result-object v2

    .line 330311
    const v3, -0x653df11a

    :try_start_d
    invoke-static {v3}, LX/02m;->a(I)V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    .line 330312
    :goto_4
    if-eqz v12, :cond_8

    :try_start_e
    invoke-interface {v12}, LX/1mW;->close()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    .line 330313
    :cond_8
    :try_start_f
    invoke-direct/range {p0 .. p0}, LX/1qD;->cleanUpThreadState()V

    .line 330314
    instance-of v3, v2, Lcom/facebook/fbservice/service/FutureOperationResult;

    if-eqz v3, :cond_c

    .line 330315
    check-cast v2, Lcom/facebook/fbservice/service/FutureOperationResult;

    .line 330316
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/FutureOperationResult;->getFuture()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2}, LX/1qD;->operationDeferred(LX/1qF;Lcom/google/common/util/concurrent/ListenableFuture;)V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_0
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    .line 330317
    :goto_5
    const v2, 0x165b1511

    invoke-static {v14, v15, v2}, LX/02m;->a(JI)V

    .line 330318
    invoke-static {v10}, LX/1qF;->stopTask(LX/1qF;)V

    goto/16 :goto_0

    .line 330319
    :catchall_1
    move-exception v2

    const v3, 0xc7b28fe

    :try_start_10
    invoke-static {v3}, LX/02m;->a(I)V

    throw v2
    :try_end_10
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_1
    .catchall {:try_start_10 .. :try_end_10} :catchall_6

    .line 330320
    :catch_1
    move-exception v2

    :try_start_11
    throw v2
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    .line 330321
    :catchall_2
    move-exception v3

    move-object/from16 v16, v3

    move-object v3, v2

    move-object/from16 v2, v16

    :goto_6
    if-eqz v12, :cond_9

    if-eqz v3, :cond_b

    :try_start_12
    invoke-interface {v12}, LX/1mW;->close()V
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_3
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    :cond_9
    :goto_7
    :try_start_13
    throw v2
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_3

    .line 330322
    :catchall_3
    move-exception v2

    :try_start_14
    invoke-direct/range {p0 .. p0}, LX/1qD;->cleanUpThreadState()V

    throw v2
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_0
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    .line 330323
    :catchall_4
    move-exception v2

    const v3, 0x27f475d4

    invoke-static {v14, v15, v3}, LX/02m;->a(JI)V

    .line 330324
    invoke-static {v10}, LX/1qF;->stopTask(LX/1qF;)V

    throw v2

    .line 330325
    :catchall_5
    move-exception v2

    :try_start_15
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_5

    :try_start_16
    throw v2
    :try_end_16
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_1
    .catchall {:try_start_16 .. :try_end_16} :catchall_6

    .line 330326
    :catchall_6
    move-exception v2

    move-object v3, v11

    goto :goto_6

    .line 330327
    :catch_2
    move-exception v2

    .line 330328
    :try_start_17
    monitor-enter p0
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_8

    .line 330329
    :try_start_18
    iget-boolean v3, v10, LX/1qF;->cancelled:Z

    if-eqz v3, :cond_a

    .line 330330
    sget-object v2, LX/1nY;->CANCELLED:LX/1nY;

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 330331
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_7

    .line 330332
    const v3, 0x22854630

    :try_start_19
    invoke-static {v3}, LX/02m;->a(I)V
    :try_end_19
    .catch Ljava/lang/Throwable; {:try_start_19 .. :try_end_19} :catch_1
    .catchall {:try_start_19 .. :try_end_19} :catchall_6

    goto :goto_4

    .line 330333
    :cond_a
    :try_start_1a
    throw v2

    .line 330334
    :catchall_7
    move-exception v2

    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_7

    :try_start_1b
    throw v2
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_8

    .line 330335
    :catchall_8
    move-exception v2

    const v3, -0x2012bc59

    :try_start_1c
    invoke-static {v3}, LX/02m;->a(I)V

    throw v2
    :try_end_1c
    .catch Ljava/lang/Throwable; {:try_start_1c .. :try_end_1c} :catch_1
    .catchall {:try_start_1c .. :try_end_1c} :catchall_6

    .line 330336
    :catch_3
    move-exception v4

    :try_start_1d
    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_7

    :cond_b
    invoke-interface {v12}, LX/1mW;->close()V
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_3

    goto :goto_7

    .line 330337
    :cond_c
    :try_start_1e
    move-object/from16 v0, p0

    invoke-static {v0, v10, v2}, LX/1qD;->operationDone(LX/1qD;LX/1qF;Lcom/facebook/fbservice/service/OperationResult;)V
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_1e .. :try_end_1e} :catch_0
    .catchall {:try_start_1e .. :try_end_1e} :catchall_4

    goto :goto_5

    .line 330338
    :cond_d
    :try_start_1f
    invoke-static {v2}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v3

    invoke-static {v2}, LX/3d6;->bundleForException(Ljava/lang/Throwable;)Landroid/os/Bundle;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Landroid/os/Bundle;Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_4

    move-result-object v2

    goto/16 :goto_2

    :cond_e
    move-object v2, v4

    goto/16 :goto_4

    :cond_f
    move-object v4, v11

    goto/16 :goto_3
.end method

.method public static processQueueOnHandler(LX/1qD;J)V
    .locals 3

    .prologue
    .line 330254
    iget-object v0, p0, LX/1qD;->mHandlerExecutorService:LX/0Te;

    new-instance v1, Lcom/facebook/fbservice/service/BlueServiceQueue$3;

    invoke-direct {v1, p0}, Lcom/facebook/fbservice/service/BlueServiceQueue$3;-><init>(LX/1qD;)V

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, p1, p2, v2}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    .line 330255
    return-void
.end method

.method private static declared-synchronized removeOldOperations(LX/1qD;)V
    .locals 10

    .prologue
    .line 330394
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1qD;->mClock:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 330395
    iget-object v0, p0, LX/1qD;->mCompletedOperations:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 330396
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 330397
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qE;

    .line 330398
    iget-object v1, p0, LX/1qD;->mOperationsById:Ljava/util/Map;

    .line 330399
    iget-object v5, v0, LX/1qE;->mId:Ljava/lang/String;

    move-object v5, v5

    .line 330400
    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1qF;

    .line 330401
    if-nez v1, :cond_0

    .line 330402
    const-string v0, "BlueServiceQueue"

    const-string v1, "No holder for recently completed operation!"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 330403
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 330404
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 330405
    :cond_0
    :try_start_1
    iget-wide v6, v1, LX/1qF;->endTime:J

    sub-long v6, v2, v6

    const-wide/16 v8, 0x7530

    cmp-long v1, v6, v8

    if-lez v1, :cond_1

    .line 330406
    iget-object v1, p0, LX/1qD;->mOperationsById:Ljava/util/Map;

    .line 330407
    iget-object v5, v0, LX/1qE;->mId:Ljava/lang/String;

    move-object v0, v5

    .line 330408
    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 330409
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 330410
    :cond_1
    iget-object v0, p0, LX/1qD;->mCompletedOperations:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 330411
    const-wide/16 v0, 0x3a98

    invoke-static {p0, v0, v1}, LX/1qD;->processQueueOnHandler(LX/1qD;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 330412
    :cond_2
    monitor-exit p0

    return-void
.end method

.method private setUpThreadState(LX/0zW;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 330251
    sget-object p0, LX/129;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {p0, p2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 330252
    sget-object p0, LX/14T;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {p0, p1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 330253
    return-void
.end method


# virtual methods
.method public addOperation(LX/1qE;)V
    .locals 1

    .prologue
    .line 330249
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/1qD;->addOperation(LX/1qE;LX/1qB;)V

    .line 330250
    return-void
.end method

.method public addOperation(LX/1qE;LX/1qB;)V
    .locals 6
    .param p2    # LX/1qB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 330217
    iget-object v0, p0, LX/1qD;->mStoppedCalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot add an operation after queue was stopped"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 330218
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Blue_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/1qD;->mQueueName:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 330219
    iget-object v1, p1, LX/1qE;->mType:Ljava/lang/String;

    move-object v1, v1

    .line 330220
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 330221
    iget-object v1, p1, LX/1qE;->mId:Ljava/lang/String;

    move-object v1, v1

    .line 330222
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 330223
    new-instance v1, LX/0zW;

    invoke-direct {v1, v0}, LX/0zW;-><init>(Ljava/lang/String;)V

    move-object v2, v1

    .line 330224
    monitor-enter p0

    .line 330225
    :try_start_0
    iget-object v0, p0, LX/1qD;->mBackgroundWorkLogger:LX/0Sj;

    const-string v1, "BlueServiceQueue"

    .line 330226
    if-eqz p1, :cond_3

    .line 330227
    iget-object v3, p1, LX/1qE;->mType:Ljava/lang/String;

    move-object v3, v3

    .line 330228
    if-eqz v3, :cond_3

    .line 330229
    iget-object v3, p1, LX/1qE;->mType:Ljava/lang/String;

    move-object v3, v3

    .line 330230
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, LX/1qD;->mQueueName:Ljava/lang/Class;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 330231
    invoke-interface {v0, v1, v3}, LX/0Sj;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0cV;

    move-result-object v3

    .line 330232
    new-instance v0, LX/1qF;

    iget-object v1, p0, LX/1qD;->mClock:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/1qF;-><init>(LX/1qE;LX/0zW;LX/0cV;J)V

    .line 330233
    new-instance v1, LX/1qG;

    invoke-direct {v1, p0, v0}, LX/1qG;-><init>(LX/1qD;LX/1qF;)V

    .line 330234
    iput-object v1, v0, LX/1qF;->callback:LX/1qH;

    .line 330235
    if-eqz p2, :cond_0

    iget-object v1, v0, LX/1qF;->handlers:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 330236
    iget-object v1, v0, LX/1qF;->handlers:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 330237
    :cond_0
    iget-object v1, p0, LX/1qD;->mQueuedOperations:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 330238
    iget-object v1, p0, LX/1qD;->mOperationsById:Ljava/util/Map;

    .line 330239
    iget-object v2, p1, LX/1qE;->mId:Ljava/lang/String;

    move-object v2, v2

    .line 330240
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330241
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330242
    iget-object v0, p0, LX/1qD;->mQueueHooks:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qI;

    .line 330243
    iget-object v2, p0, LX/1qD;->mQueueName:Ljava/lang/Class;

    invoke-virtual {v0, v2, p1}, LX/1qI;->onOperationQueued(Ljava/lang/Class;LX/1qE;)V

    goto :goto_2

    .line 330244
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 330245
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 330246
    :cond_2
    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1}, LX/1qD;->processQueueOnHandler(LX/1qD;J)V

    .line 330247
    return-void

    .line 330248
    :cond_3
    const-string v3, "Unknown"

    goto :goto_1
.end method

.method public declared-synchronized cancelOperation(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 330189
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1qD;->mOperationsById:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330190
    if-nez v0, :cond_0

    move v0, v2

    .line 330191
    :goto_0
    monitor-exit p0

    return v0

    .line 330192
    :cond_0
    :try_start_1
    iget-object v3, v0, LX/1qF;->result:Lcom/facebook/fbservice/service/OperationResult;

    if-eqz v3, :cond_1

    move v0, v2

    .line 330193
    goto :goto_0

    .line 330194
    :cond_1
    iget-object v3, v0, LX/1qF;->operation:LX/1qE;

    .line 330195
    iget-object v4, v0, LX/1qF;->future:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v4, :cond_2

    .line 330196
    const/4 v1, 0x1

    .line 330197
    iput-boolean v1, v0, LX/1qF;->cancelled:Z

    .line 330198
    iget-object v0, v0, LX/1qF;->future:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    move-result v0

    goto :goto_0

    .line 330199
    :cond_2
    iget-object v4, p0, LX/1qD;->mQueuedOperations:Ljava/util/LinkedList;

    invoke-virtual {v4, v3}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 330200
    sget-object v2, LX/1nY;->CANCELLED:LX/1nY;

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    invoke-static {p0, v0, v2}, LX/1qD;->operationDone(LX/1qD;LX/1qF;Lcom/facebook/fbservice/service/OperationResult;)V

    .line 330201
    const/4 v2, 0x1

    .line 330202
    iput-boolean v2, v0, LX/1qF;->cancelled:Z

    .line 330203
    move v0, v1

    .line 330204
    goto :goto_0

    .line 330205
    :cond_3
    iget-object v1, p0, LX/1qD;->mCurrentOperationHolder:LX/1qF;

    if-eqz v1, :cond_5

    iget-object v1, p0, LX/1qD;->mCurrentOperationHolder:LX/1qF;

    iget-object v1, v1, LX/1qF;->operation:LX/1qE;

    if-ne v1, v3, :cond_5

    .line 330206
    iget-object v1, p0, LX/1qD;->mCurrentOperationHolder:LX/1qF;

    const/4 v3, 0x1

    .line 330207
    iput-boolean v3, v1, LX/1qF;->cancelled:Z

    .line 330208
    iget-object v1, p0, LX/1qD;->mBlueServiceHandlerProvider:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1qM;

    .line 330209
    instance-of v3, v1, LX/4B8;

    if-eqz v3, :cond_4

    .line 330210
    iget-object v2, p0, LX/1qD;->mHandlerExecutorService:LX/0Te;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 330211
    const/4 v2, 0x1

    .line 330212
    iput-boolean v2, v0, LX/1qF;->cancelled:Z

    .line 330213
    check-cast v1, LX/4B8;

    invoke-interface {v1, p1}, LX/4B8;->cancelOperation(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :cond_4
    move v0, v2

    .line 330214
    goto :goto_0

    :cond_5
    move v0, v2

    .line 330215
    goto :goto_0

    .line 330216
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized changePriority(Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 330180
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1qD;->mOperationsById:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330181
    if-nez v0, :cond_0

    move v0, v1

    .line 330182
    :goto_0
    monitor-exit p0

    return v0

    .line 330183
    :cond_0
    :try_start_1
    iget-object v0, v0, LX/1qF;->requestState:LX/0zW;

    .line 330184
    if-nez v0, :cond_1

    move v0, v1

    .line 330185
    goto :goto_0

    .line 330186
    :cond_1
    invoke-virtual {v0, p2}, LX/0zW;->c(Lcom/facebook/http/interfaces/RequestPriority;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 330187
    const/4 v0, 0x1

    goto :goto_0

    .line 330188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getQueueName()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 330179
    iget-object v0, p0, LX/1qD;->mQueueName:Ljava/lang/Class;

    return-object v0
.end method

.method public declared-synchronized hasOperation(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 330178
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1qD;->mOperationsById:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public maybeReportSoftError(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 330170
    iget-object v0, p0, LX/1qD;->mSoftErrorReportingGkProvider:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 330171
    :goto_0
    return-void

    .line 330172
    :cond_0
    invoke-static {p1}, LX/1mP;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 330173
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "orca_service_exception"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "type"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "msg"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 330174
    if-eqz p2, :cond_1

    .line 330175
    const-string v1, "operation"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 330176
    :cond_1
    iget-object v1, p0, LX/1qD;->mAnalyticsLogger:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 330177
    :cond_2
    iget-object v0, p0, LX/1qD;->mFbErrorReporter:LX/03V;

    const-string v1, "BlueServiceQueue"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed BlueService operation ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public registerCompletionHandlerInternal(Ljava/lang/String;LX/1qB;)Z
    .locals 3

    .prologue
    .line 330156
    const/4 v1, 0x0

    .line 330157
    monitor-enter p0

    .line 330158
    :try_start_0
    iget-object v0, p0, LX/1qD;->mOperationsById:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qF;

    .line 330159
    if-nez v0, :cond_0

    .line 330160
    const/4 v0, 0x0

    monitor-exit p0

    .line 330161
    :goto_0
    return v0

    .line 330162
    :cond_0
    iget-object v2, v0, LX/1qF;->result:Lcom/facebook/fbservice/service/OperationResult;

    if-eqz v2, :cond_2

    .line 330163
    iget-object v0, v0, LX/1qF;->result:Lcom/facebook/fbservice/service/OperationResult;

    .line 330164
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330165
    if-eqz v0, :cond_1

    .line 330166
    :try_start_1
    invoke-interface {p2, v0}, LX/1qB;->onOperationCompleted(Lcom/facebook/fbservice/service/OperationResult;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 330167
    :cond_1
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 330168
    :cond_2
    :try_start_2
    iget-object v0, v0, LX/1qF;->handlers:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    goto :goto_1

    .line 330169
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    goto :goto_2
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 330153
    iget-object v0, p0, LX/1qD;->mStoppedCalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 330154
    iget-object v0, p0, LX/1qD;->mHandlerExecutorService:LX/0Te;

    new-instance v1, Lcom/facebook/fbservice/service/BlueServiceQueue$1;

    invoke-direct {v1, p0}, Lcom/facebook/fbservice/service/BlueServiceQueue$1;-><init>(LX/1qD;)V

    const v2, -0x561ac9a4

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 330155
    :cond_0
    return-void
.end method
