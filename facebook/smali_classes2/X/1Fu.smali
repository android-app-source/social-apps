.class public final enum LX/1Fu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1Fu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1Fu;

.field public static final enum HIGH:LX/1Fu;

.field public static final enum LOW:LX/1Fu;

.field public static final enum NORMAL:LX/1Fu;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 224723
    new-instance v0, LX/1Fu;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v2}, LX/1Fu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Fu;->LOW:LX/1Fu;

    .line 224724
    new-instance v0, LX/1Fu;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3}, LX/1Fu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Fu;->NORMAL:LX/1Fu;

    .line 224725
    new-instance v0, LX/1Fu;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v4}, LX/1Fu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Fu;->HIGH:LX/1Fu;

    .line 224726
    const/4 v0, 0x3

    new-array v0, v0, [LX/1Fu;

    sget-object v1, LX/1Fu;->LOW:LX/1Fu;

    aput-object v1, v0, v2

    sget-object v1, LX/1Fu;->NORMAL:LX/1Fu;

    aput-object v1, v0, v3

    sget-object v1, LX/1Fu;->HIGH:LX/1Fu;

    aput-object v1, v0, v4

    sput-object v0, LX/1Fu;->$VALUES:[LX/1Fu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 224720
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1Fu;
    .locals 1

    .prologue
    .line 224722
    const-class v0, LX/1Fu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1Fu;

    return-object v0
.end method

.method public static values()[LX/1Fu;
    .locals 1

    .prologue
    .line 224721
    sget-object v0, LX/1Fu;->$VALUES:[LX/1Fu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1Fu;

    return-object v0
.end method
