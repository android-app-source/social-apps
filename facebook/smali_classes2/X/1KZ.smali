.class public LX/1KZ;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hk;
.implements LX/1KU;


# instance fields
.field public a:Landroid/widget/TextView;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public c:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 232798
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 232799
    iput-object p1, p0, LX/1KZ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 232800
    return-void
.end method


# virtual methods
.method public final a(LX/1OP;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 232801
    invoke-interface {p1}, LX/1OP;->ij_()I

    move-result v5

    move v3, v4

    move v1, v4

    move v2, v4

    :goto_0
    if-ge v3, v5, :cond_0

    .line 232802
    invoke-interface {p1, v3}, LX/1OP;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 232803
    instance-of v6, v0, LX/1Rk;

    if-eqz v6, :cond_2

    .line 232804
    check-cast v0, LX/1Rk;

    .line 232805
    add-int/lit8 v1, v1, 0x1

    .line 232806
    iget-object v6, v0, LX/1Rk;->a:LX/1RA;

    iget v0, v0, LX/1Rk;->b:I

    invoke-virtual {v6, v0}, LX/1RA;->c(I)LX/1Ra;

    move-result-object v0

    .line 232807
    instance-of v6, v0, LX/1RZ;

    if-eqz v6, :cond_2

    .line 232808
    check-cast v0, LX/1RZ;

    .line 232809
    iget-object v6, v0, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-object v0, v6

    .line 232810
    instance-of v0, v0, Lcom/facebook/components/feed/ComponentPartDefinition;

    if-eqz v0, :cond_2

    .line 232811
    add-int/lit8 v0, v2, 0x1

    move v7, v1

    move v1, v0

    move v0, v7

    .line 232812
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 232813
    :cond_0
    iget-object v0, p0, LX/1KZ;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 232814
    iget-object v0, p0, LX/1KZ;->a:Landroid/widget/TextView;

    const-string v3, "%d / %d = %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    const/4 v4, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    const/4 v4, 0x2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    int-to-float v2, v2

    int-to-float v1, v1

    div-float v1, v2, v1

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v4

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232815
    iget-object v0, p0, LX/1KZ;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->bringToFront()V

    .line 232816
    :cond_1
    return-void

    :cond_2
    move v0, v1

    move v1, v2

    goto :goto_1
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 232817
    iget-object v0, p0, LX/1KZ;->c:LX/1Iu;

    .line 232818
    iget-object v1, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 232819
    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const/4 v3, -0x2

    .line 232820
    iget-object v1, p0, LX/1KZ;->a:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 232821
    :goto_0
    return-void

    .line 232822
    :cond_0
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/1KZ;->a:Landroid/widget/TextView;

    .line 232823
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, 0x3

    invoke-direct {v2, v3, v3, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 232824
    iget-object v1, p0, LX/1KZ;->a:Landroid/widget/TextView;

    const/high16 v3, 0x43480000    # 200.0f

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setY(F)V

    .line 232825
    iget-object v1, p0, LX/1KZ;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->bringToFront()V

    .line 232826
    iget-object v3, p0, LX/1KZ;->a:Landroid/widget/TextView;

    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, LX/1KZ;->c:LX/1Iu;

    .line 232827
    iget-object v5, v1, LX/1Iu;->a:Ljava/lang/Object;

    move-object v1, v5

    .line 232828
    check-cast v1, Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x106000b

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v4, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 232829
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    iget-object v3, p0, LX/1KZ;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v3, v2}, Landroid/view/Window;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 232830
    return-void
.end method

.method public final kw_()Z
    .locals 3

    .prologue
    .line 232831
    iget-object v0, p0, LX/1KZ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0eJ;->m:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method
