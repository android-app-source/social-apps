.class public LX/1bf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/1bb;

.field public final b:Landroid/net/Uri;

.field public final c:LX/1ny;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Ljava/io/File;

.field public final e:Z

.field public final f:Z

.field public final g:LX/1bZ;

.field public final h:LX/1o9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:LX/1bd;

.field public final j:LX/1bc;

.field public final k:LX/1bY;

.field public final l:Z

.field public final m:LX/33B;

.field public final n:LX/1BU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1bX;)V
    .locals 1

    .prologue
    .line 280808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 280809
    iget-object v0, p1, LX/1bX;->f:LX/1bb;

    move-object v0, v0

    .line 280810
    iput-object v0, p0, LX/1bf;->a:LX/1bb;

    .line 280811
    iget-object v0, p1, LX/1bX;->a:Landroid/net/Uri;

    move-object v0, v0

    .line 280812
    iput-object v0, p0, LX/1bf;->b:Landroid/net/Uri;

    .line 280813
    iget-object v0, p1, LX/1bX;->m:LX/1ny;

    move-object v0, v0

    .line 280814
    iput-object v0, p0, LX/1bf;->c:LX/1ny;

    .line 280815
    iget-boolean v0, p1, LX/1bX;->g:Z

    move v0, v0

    .line 280816
    iput-boolean v0, p0, LX/1bf;->e:Z

    .line 280817
    iget-boolean v0, p1, LX/1bX;->h:Z

    move v0, v0

    .line 280818
    iput-boolean v0, p0, LX/1bf;->f:Z

    .line 280819
    iget-object v0, p1, LX/1bX;->e:LX/1bZ;

    move-object v0, v0

    .line 280820
    iput-object v0, p0, LX/1bf;->g:LX/1bZ;

    .line 280821
    iget-object v0, p1, LX/1bX;->c:LX/1o9;

    move-object v0, v0

    .line 280822
    iput-object v0, p0, LX/1bf;->h:LX/1o9;

    .line 280823
    iget-object v0, p1, LX/1bX;->d:LX/1bd;

    move-object v0, v0

    .line 280824
    if-nez v0, :cond_0

    invoke-static {}, LX/1bd;->a()LX/1bd;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/1bf;->i:LX/1bd;

    .line 280825
    iget-object v0, p1, LX/1bX;->i:LX/1bc;

    move-object v0, v0

    .line 280826
    iput-object v0, p0, LX/1bf;->j:LX/1bc;

    .line 280827
    iget-object v0, p1, LX/1bX;->b:LX/1bY;

    move-object v0, v0

    .line 280828
    iput-object v0, p0, LX/1bf;->k:LX/1bY;

    .line 280829
    iget-boolean v0, p1, LX/1bX;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/1bX;->a:Landroid/net/Uri;

    invoke-static {v0}, LX/1be;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 280830
    iput-boolean v0, p0, LX/1bf;->l:Z

    .line 280831
    iget-object v0, p1, LX/1bX;->j:LX/33B;

    move-object v0, v0

    .line 280832
    iput-object v0, p0, LX/1bf;->m:LX/33B;

    .line 280833
    iget-object v0, p1, LX/1bX;->l:LX/1BU;

    move-object v0, v0

    .line 280834
    iput-object v0, p0, LX/1bf;->n:LX/1BU;

    .line 280835
    return-void

    .line 280836
    :cond_0
    iget-object v0, p1, LX/1bX;->d:LX/1bd;

    move-object v0, v0

    .line 280837
    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/net/Uri;)LX/1bf;
    .locals 1
    .param p0    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 280807
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)LX/1bf;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 280806
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1bb;
    .locals 1

    .prologue
    .line 280805
    iget-object v0, p0, LX/1bf;->a:LX/1bb;

    return-object v0
.end method

.method public final b()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 280804
    iget-object v0, p0, LX/1bf;->b:Landroid/net/Uri;

    return-object v0
.end method

.method public final c()LX/1ny;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 280838
    iget-object v0, p0, LX/1bf;->c:LX/1ny;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 280792
    instance-of v1, p1, LX/1bf;

    if-nez v1, :cond_1

    .line 280793
    :cond_0
    :goto_0
    return v0

    .line 280794
    :cond_1
    check-cast p1, LX/1bf;

    .line 280795
    iget-object v1, p0, LX/1bf;->b:Landroid/net/Uri;

    iget-object v2, p1, LX/1bf;->b:Landroid/net/Uri;

    invoke-static {v1, v2}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1bf;->a:LX/1bb;

    iget-object v2, p1, LX/1bf;->a:LX/1bb;

    invoke-static {v1, v2}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1bf;->c:LX/1ny;

    iget-object v2, p1, LX/1bf;->c:LX/1ny;

    invoke-static {v1, v2}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1bf;->d:Ljava/io/File;

    iget-object v2, p1, LX/1bf;->d:Ljava/io/File;

    invoke-static {v1, v2}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final f()LX/1o9;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 280796
    iget-object v0, p0, LX/1bf;->h:LX/1o9;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 280797
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/1bf;->a:LX/1bb;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/1bf;->b:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/1bf;->c:LX/1ny;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/1bf;->d:Ljava/io/File;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/15f;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 280798
    iget-boolean v0, p0, LX/1bf;->l:Z

    return v0
.end method

.method public final declared-synchronized n()Ljava/io/File;
    .locals 2

    .prologue
    .line 280799
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1bf;->d:Ljava/io/File;

    if-nez v0, :cond_0

    .line 280800
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/1bf;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/1bf;->d:Ljava/io/File;

    .line 280801
    :cond_0
    iget-object v0, p0, LX/1bf;->d:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 280802
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 280803
    invoke-static {p0}, LX/15f;->a(Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "uri"

    iget-object v2, p0, LX/1bf;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "cacheChoice"

    iget-object v2, p0, LX/1bf;->a:LX/1bb;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "decodeOptions"

    iget-object v2, p0, LX/1bf;->g:LX/1bZ;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "postprocessor"

    iget-object v2, p0, LX/1bf;->m:LX/33B;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "priority"

    iget-object v2, p0, LX/1bf;->j:LX/1bc;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "resizeOptions"

    iget-object v2, p0, LX/1bf;->h:LX/1o9;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "rotationOptions"

    iget-object v2, p0, LX/1bf;->i:LX/1bd;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "mediaVariations"

    iget-object v2, p0, LX/1bf;->c:LX/1ny;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    invoke-virtual {v0}, LX/15g;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
