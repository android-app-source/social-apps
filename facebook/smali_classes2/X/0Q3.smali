.class public abstract LX/0Q3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Q4;


# instance fields
.field public mBinder:LX/0RH;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57755
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public assertBindingInstalled(LX/0RI;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 57753
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    invoke-virtual {v0, p1}, LX/0RH;->e(LX/0RI;)V

    .line 57754
    return-void
.end method

.method public assertBindingInstalled(Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 57748
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    .line 57749
    iget-object v1, v0, LX/0RH;->e:Ljava/util/Set;

    if-nez v1, :cond_0

    .line 57750
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    iput-object v1, v0, LX/0RH;->e:Ljava/util/Set;

    .line 57751
    :cond_0
    iget-object v1, v0, LX/0RH;->e:Ljava/util/Set;

    invoke-static {p1}, LX/0RI;->a(Ljava/lang/Class;)LX/0RI;

    move-result-object p0

    invoke-interface {v1, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 57752
    return-void
.end method

.method public assertBindingInstalled(Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57746
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    invoke-static {p1, p2}, LX/0RI;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/0RI;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0RH;->e(LX/0RI;)V

    .line 57747
    return-void
.end method

.method public bind(Ljava/lang/Class;)LX/0RO;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lcom/facebook/inject/binder/AnnotatedBindingBuilder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57745
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    invoke-virtual {v0, p1}, LX/0RH;->a(Ljava/lang/Class;)LX/0RO;

    move-result-object v0

    return-object v0
.end method

.method public bind(LX/0RI;)LX/0RS;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0RS",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57741
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    .line 57742
    invoke-static {v0, p1}, LX/0RH;->f(LX/0RH;LX/0RI;)LX/0RN;

    move-result-object v1

    .line 57743
    new-instance p0, LX/0RO;

    invoke-direct {p0, v1}, LX/0RO;-><init>(LX/0RN;)V

    move-object v0, p0

    .line 57744
    return-object v0
.end method

.method public bindAssistedProvider(Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 57738
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    .line 57739
    invoke-virtual {v0, p1}, LX/0RH;->a(Ljava/lang/Class;)LX/0RO;

    move-result-object v1

    new-instance p0, LX/0Wm;

    invoke-direct {p0, p1}, LX/0Wm;-><init>(Ljava/lang/Class;)V

    invoke-interface {v1, p0}, LX/0RS;->a(LX/0Or;)LX/0RR;

    .line 57740
    return-void
.end method

.method public bindComponent(Ljava/lang/Class;)LX/4fx;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lcom/facebook/inject/binder/LinkedComponentBindingBuilder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57726
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    .line 57727
    invoke-static {p1}, LX/0RI;->a(Ljava/lang/Class;)LX/0RI;

    move-result-object v1

    .line 57728
    iget-object v2, v0, LX/0RH;->c:Ljava/util/List;

    if-nez v2, :cond_0

    .line 57729
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, v0, LX/0RH;->c:Ljava/util/List;

    .line 57730
    :cond_0
    new-instance v2, LX/4fR;

    invoke-direct {v2}, LX/4fR;-><init>()V

    .line 57731
    iget-object p0, v0, LX/0RH;->a:LX/0Q4;

    .line 57732
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v2, LX/4fR;->a:Ljava/lang/String;

    .line 57733
    iput-object v1, v2, LX/4fR;->b:LX/0RI;

    .line 57734
    iget-object p0, v0, LX/0RH;->c:Ljava/util/List;

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57735
    move-object v1, v2

    .line 57736
    new-instance v2, LX/4fx;

    invoke-direct {v2, v1}, LX/4fx;-><init>(LX/4fR;)V

    move-object v0, v2

    .line 57737
    return-object v0
.end method

.method public bindDefault(Ljava/lang/Class;)LX/0RO;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lcom/facebook/inject/binder/AnnotatedBindingBuilder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57725
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    invoke-virtual {v0, p1}, LX/0RH;->b(Ljava/lang/Class;)LX/0RO;

    move-result-object v0

    return-object v0
.end method

.method public bindDefault(LX/0RI;)LX/0RS;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0RS",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57756
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    .line 57757
    invoke-static {v0, p1}, LX/0RH;->f(LX/0RH;LX/0RI;)LX/0RN;

    move-result-object v1

    .line 57758
    const/4 p0, 0x1

    invoke-virtual {v1, p0}, LX/0RN;->a(Z)V

    .line 57759
    new-instance p0, LX/0RO;

    invoke-direct {p0, v1}, LX/0RO;-><init>(LX/0RN;)V

    move-object v0, p0

    .line 57760
    return-object v0
.end method

.method public bindMulti(LX/0RI;)LX/4fl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/4fl",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57697
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    .line 57698
    invoke-static {v0, p1}, LX/0RH;->h(LX/0RH;LX/0RI;)LX/4fl;

    move-result-object p0

    move-object v0, p0

    .line 57699
    return-object v0
.end method

.method public bindMulti(Ljava/lang/Class;)LX/4fl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LX/4fl",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57700
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    .line 57701
    invoke-static {p1}, LX/0RI;->a(Ljava/lang/Class;)LX/0RI;

    move-result-object p0

    invoke-static {v0, p0}, LX/0RH;->h(LX/0RH;LX/0RI;)LX/4fl;

    move-result-object p0

    move-object v0, p0

    .line 57702
    return-object v0
.end method

.method public bindMulti(Ljava/lang/Class;Ljava/lang/Class;)LX/4fl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/4fl",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57703
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    .line 57704
    invoke-static {p1, p2}, LX/0RI;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/0RI;

    move-result-object p0

    invoke-static {v0, p0}, LX/0RH;->h(LX/0RH;LX/0RI;)LX/4fl;

    move-result-object p0

    move-object v0, p0

    .line 57705
    return-object v0
.end method

.method public bindScope(Ljava/lang/Class;LX/0RC;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "LX/0RC;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57706
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    .line 57707
    iget-object p0, v0, LX/0RH;->i:Ljava/util/Map;

    if-nez p0, :cond_0

    .line 57708
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object p0

    iput-object p0, v0, LX/0RH;->i:Ljava/util/Map;

    .line 57709
    :cond_0
    iget-object p0, v0, LX/0RH;->i:Ljava/util/Map;

    invoke-interface {p0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57710
    return-void
.end method

.method public abstract configure()V
.end method

.method public final configure(LX/0RH;)V
    .locals 0

    .prologue
    .line 57711
    iput-object p1, p0, LX/0Q3;->mBinder:LX/0RH;

    .line 57712
    invoke-virtual {p0}, LX/0Q3;->configure()V

    .line 57713
    return-void
.end method

.method public declareMultiBinding(LX/0RI;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 57714
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    invoke-virtual {v0, p1}, LX/0RH;->c(LX/0RI;)V

    .line 57715
    return-void
.end method

.method public declareMultiBinding(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 57716
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    .line 57717
    invoke-static {p1}, LX/0RI;->a(Ljava/lang/Class;)LX/0RI;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/0RH;->c(LX/0RI;)V

    .line 57718
    return-void
.end method

.method public declareMultiBinding(Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57719
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    .line 57720
    invoke-static {p1, p2}, LX/0RI;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/0RI;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/0RH;->c(LX/0RI;)V

    .line 57721
    return-void
.end method

.method public getBinder()LX/0RH;
    .locals 1

    .prologue
    .line 57722
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    return-object v0
.end method

.method public require(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/LibraryModule;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57723
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    invoke-virtual {v0, p1}, LX/0RH;->h(Ljava/lang/Class;)V

    .line 57724
    return-void
.end method
