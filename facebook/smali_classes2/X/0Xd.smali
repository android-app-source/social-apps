.class public LX/0Xd;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79187
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 79188
    return-void
.end method

.method public static a(LX/0XZ;)LX/0Xe;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 79186
    new-instance v0, LX/0Xe;

    invoke-direct {v0, p0}, LX/0Xe;-><init>(LX/0XZ;)V

    return-object v0
.end method

.method public static a(LX/0mh;)LX/17K;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 79185
    new-instance v0, LX/17K;

    invoke-direct {v0, p0}, LX/17K;-><init>(LX/0mh;)V

    return-object v0
.end method

.method public static a(LX/1BA;)LX/4l4;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 79177
    new-instance v0, LX/4l4;

    invoke-direct {v0, p0}, LX/4l4;-><init>(LX/1BA;)V

    return-object v0
.end method

.method public static a(LX/0Ot;LX/0Xe;LX/0Ot;LX/0So;LX/0SG;LX/0Uo;Ljava/util/concurrent/ExecutorService;LX/03e;LX/0Xh;Ljava/util/concurrent/ScheduledExecutorService;LX/0Xl;Ljava/util/Set;Ljava/util/Set;LX/0Y0;)Lcom/facebook/quicklog/QuickPerformanceLogger;
    .locals 15
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/AnalyticsThreadExecutor;
        .end annotation
    .end param
    .param p9    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p10    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/quicklog/HoneyClientLogger;",
            ">;",
            "Lcom/facebook/quicklog/HoneySamplingPolicy;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/quicklog/StatsLogger;",
            ">;",
            "LX/0So;",
            "LX/0SG;",
            "LX/0Uo;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/03e;",
            "Lcom/facebook/quicklog/DebugAndTestConfig;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0Xl;",
            "Ljava/util/Set",
            "<",
            "LX/0Y8;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/0Y4;",
            ">;",
            "LX/0Y0;",
            ")",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 79181
    new-instance v2, LX/0Y1;

    new-instance v9, LX/0Y2;

    move-object/from16 v0, p5

    invoke-direct {v9, v0}, LX/0Y2;-><init>(LX/0Uo;)V

    new-instance v10, LX/0Y3;

    move-object/from16 v0, p6

    move-object/from16 v1, p9

    invoke-direct {v10, v0, v1}, LX/0Y3;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ScheduledExecutorService;)V

    invoke-interface/range {p12 .. p12}, Ljava/util/Set;->size()I

    move-result v3

    new-array v3, v3, [LX/0Y4;

    move-object/from16 v0, p12

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [LX/0Y4;

    invoke-interface/range {p11 .. p11}, Ljava/util/Set;->size()I

    move-result v3

    new-array v3, v3, [LX/0Y8;

    move-object/from16 v0, p11

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [LX/0Y8;

    new-instance v13, LX/03f;

    move-object/from16 v0, p7

    invoke-direct {v13, v0}, LX/03f;-><init>(LX/03e;)V

    move-object v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p8

    move-object/from16 v14, p13

    invoke-direct/range {v2 .. v14}, LX/0Y1;-><init>(LX/0Or;LX/0Xe;LX/0Or;LX/0So;LX/0SG;LX/0Xh;LX/0Y2;LX/0Y3;[LX/0Y4;[LX/0Y8;LX/03f;LX/0Y0;)V

    .line 79182
    invoke-static {v2}, Lcom/facebook/quicklog/QuickPerformanceLoggerProvider;->a(Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 79183
    move-object/from16 v0, p10

    invoke-static {v2, v0}, LX/0Xd;->a(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Xl;)V

    .line 79184
    return-object v2
.end method

.method private static a(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Xl;)V
    .locals 3

    .prologue
    .line 79179
    invoke-interface {p1}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance v2, LX/0YY;

    invoke-direct {v2, p0}, LX/0YY;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 79180
    return-void
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 79178
    return-void
.end method
