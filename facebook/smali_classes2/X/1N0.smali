.class public LX/1N0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/1N0;


# instance fields
.field private final a:LX/0SI;

.field public final b:LX/03V;

.field private final c:LX/1N2;

.field private d:LX/1N3;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private e:Lcom/facebook/reactivesocket/GatewayConnectionImpl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2zL;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SI;LX/0Xl;LX/1N2;LX/03V;)V
    .locals 2
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 236634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236635
    sget-object v0, LX/1N3;->AVAILABLE:LX/1N3;

    iput-object v0, p0, LX/1N0;->d:LX/1N3;

    .line 236636
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/1N0;->f:Ljava/util/List;

    .line 236637
    iput-object p1, p0, LX/1N0;->a:LX/0SI;

    .line 236638
    iput-object p3, p0, LX/1N0;->c:LX/1N2;

    .line 236639
    iput-object p4, p0, LX/1N0;->b:LX/03V;

    .line 236640
    invoke-interface {p2}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    new-instance p1, LX/1N4;

    invoke-direct {p1, p0}, LX/1N4;-><init>(LX/1N0;)V

    invoke-interface {v0, v1, p1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance p1, LX/1N5;

    invoke-direct {p1, p0}, LX/1N5;-><init>(LX/1N0;)V

    invoke-interface {v0, v1, p1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.login.AuthStateMachineMonitor.LOGOUT_COMPLETE"

    new-instance p1, LX/1N6;

    invoke-direct {p1, p0}, LX/1N6;-><init>(LX/1N0;)V

    invoke-interface {v0, v1, p1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 236641
    return-void
.end method

.method public static a(LX/0QB;)LX/1N0;
    .locals 7

    .prologue
    .line 236642
    sget-object v0, LX/1N0;->g:LX/1N0;

    if-nez v0, :cond_1

    .line 236643
    const-class v1, LX/1N0;

    monitor-enter v1

    .line 236644
    :try_start_0
    sget-object v0, LX/1N0;->g:LX/1N0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 236645
    if-eqz v2, :cond_0

    .line 236646
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 236647
    new-instance p0, LX/1N0;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v3

    check-cast v3, LX/0SI;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    .line 236648
    invoke-static {}, LX/1N1;->a()LX/1N2;

    move-result-object v5

    move-object v5, v5

    .line 236649
    check-cast v5, LX/1N2;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1N0;-><init>(LX/0SI;LX/0Xl;LX/1N2;LX/03V;)V

    .line 236650
    move-object v0, p0

    .line 236651
    sput-object v0, LX/1N0;->g:LX/1N0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236652
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 236653
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 236654
    :cond_1
    sget-object v0, LX/1N0;->g:LX/1N0;

    return-object v0

    .line 236655
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 236656
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a()Lcom/facebook/reactivesocket/GatewayConnectionImpl;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 236657
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1N0;->e:Lcom/facebook/reactivesocket/GatewayConnectionImpl;

    if-nez v0, :cond_1

    .line 236658
    iget-object v0, p0, LX/1N0;->a:LX/0SI;

    invoke-interface {v0}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 236659
    if-nez v0, :cond_0

    .line 236660
    iget-object v0, p0, LX/1N0;->b:LX/03V;

    const-string v1, "LithiumClient"

    const-string v2, "subscribe without LoggedInUserViewerContext"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236661
    const/4 v0, 0x0

    .line 236662
    :goto_0
    monitor-exit p0

    return-object v0

    .line 236663
    :cond_0
    :try_start_1
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v1

    .line 236664
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v0, v2

    .line 236665
    const-string v3, "https://lithium.facebook.com"

    .line 236666
    new-instance v4, Lcom/facebook/reactivesocket/GatewayConnectionImpl;

    invoke-direct {v4, v3, v1, v0}, Lcom/facebook/reactivesocket/GatewayConnectionImpl;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v4

    .line 236667
    iput-object v0, p0, LX/1N0;->e:Lcom/facebook/reactivesocket/GatewayConnectionImpl;

    .line 236668
    :cond_1
    iget-object v0, p0, LX/1N0;->e:Lcom/facebook/reactivesocket/GatewayConnectionImpl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 236669
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b$redex0(LX/1N0;)V
    .locals 2

    .prologue
    .line 236670
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1N0;->d:LX/1N3;

    sget-object v1, LX/1N3;->PAUSED:LX/1N3;

    if-ne v0, v1, :cond_0

    .line 236671
    sget-object v0, LX/1N3;->AVAILABLE:LX/1N3;

    iput-object v0, p0, LX/1N0;->d:LX/1N3;

    .line 236672
    invoke-static {p0}, LX/1N0;->c$redex0(LX/1N0;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236673
    :cond_0
    monitor-exit p0

    return-void

    .line 236674
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized c$redex0(LX/1N0;)V
    .locals 7

    .prologue
    .line 236675
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1N0;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 236676
    iget-object v0, p0, LX/1N0;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zL;

    .line 236677
    iget-object v1, v0, LX/2zL;->a:Ljava/lang/String;

    iget-object v2, v0, LX/2zL;->b:Ljava/lang/String;

    iget v3, v0, LX/2zL;->c:I

    iget-object v4, v0, LX/2zL;->d:Lcom/facebook/reactivesocket/GatewayCallback;

    iget-object v5, v0, LX/2zL;->e:LX/4VW;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/1N0;->a(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/reactivesocket/GatewayCallback;LX/4VW;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 236678
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 236679
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1N0;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236680
    monitor-exit p0

    return-void
.end method

.method public static declared-synchronized d$redex0(LX/1N0;)V
    .locals 2

    .prologue
    .line 236681
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1N0;->d:LX/1N3;

    sget-object v1, LX/1N3;->AVAILABLE:LX/1N3;

    if-ne v0, v1, :cond_1

    .line 236682
    iget-object v0, p0, LX/1N0;->e:Lcom/facebook/reactivesocket/GatewayConnectionImpl;

    if-eqz v0, :cond_0

    .line 236683
    iget-object v0, p0, LX/1N0;->e:Lcom/facebook/reactivesocket/GatewayConnectionImpl;

    invoke-virtual {v0}, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->a()V

    .line 236684
    :cond_0
    sget-object v0, LX/1N3;->PAUSED:LX/1N3;

    iput-object v0, p0, LX/1N0;->d:LX/1N3;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236685
    :cond_1
    monitor-exit p0

    return-void

    .line 236686
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized e(LX/1N0;)V
    .locals 1

    .prologue
    .line 236687
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1N0;->e:Lcom/facebook/reactivesocket/GatewayConnectionImpl;

    if-eqz v0, :cond_0

    .line 236688
    iget-object v0, p0, LX/1N0;->e:Lcom/facebook/reactivesocket/GatewayConnectionImpl;

    invoke-virtual {v0}, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->b()V

    .line 236689
    const/4 v0, 0x0

    iput-object v0, p0, LX/1N0;->e:Lcom/facebook/reactivesocket/GatewayConnectionImpl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236690
    :cond_0
    monitor-exit p0

    return-void

    .line 236691
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/reactivesocket/GatewayCallback;LX/4VW;)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 236692
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1N0;->d:LX/1N3;

    sget-object v1, LX/1N3;->PAUSED:LX/1N3;

    if-ne v0, v1, :cond_0

    .line 236693
    new-instance v0, LX/2zL;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/2zL;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/reactivesocket/GatewayCallback;LX/4VW;)V

    .line 236694
    iget-object v1, p0, LX/1N0;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236695
    :goto_0
    monitor-exit p0

    return-void

    .line 236696
    :cond_0
    :try_start_1
    invoke-direct {p0}, LX/1N0;->a()Lcom/facebook/reactivesocket/GatewayConnectionImpl;

    move-result-object v0

    .line 236697
    if-nez v0, :cond_1

    .line 236698
    sget-object v0, Lcom/facebook/reactivesocket/Subscription;->a:Lcom/facebook/reactivesocket/Subscription;

    invoke-virtual {p5, v0}, LX/4VW;->a(Lcom/facebook/reactivesocket/Subscription;)V

    .line 236699
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no viewer context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-interface {p4, v0}, Lcom/facebook/reactivesocket/GatewayCallback;->onFailure(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 236700
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 236701
    :cond_1
    :try_start_2
    new-instance v1, LX/2Ey;

    invoke-direct {v1, p0, p4}, LX/2Ey;-><init>(LX/1N0;Lcom/facebook/reactivesocket/GatewayCallback;)V

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->a(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/reactivesocket/GatewayCallback;)Lcom/facebook/reactivesocket/Subscription;

    move-result-object v0

    .line 236702
    invoke-virtual {p5, v0}, LX/4VW;->a(Lcom/facebook/reactivesocket/Subscription;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
