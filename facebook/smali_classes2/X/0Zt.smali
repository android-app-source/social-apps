.class public LX/0Zt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/0Zt;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p1    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p2    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/GlobalFbBroadcast;
        .end annotation
    .end param
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbAppBroadcast;
        .end annotation
    .end param
    .param p4    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 83978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83979
    iput-object p1, p0, LX/0Zt;->a:LX/0Ot;

    .line 83980
    iput-object p2, p0, LX/0Zt;->b:LX/0Ot;

    .line 83981
    iput-object p3, p0, LX/0Zt;->c:LX/0Ot;

    .line 83982
    iput-object p4, p0, LX/0Zt;->d:LX/0Ot;

    .line 83983
    return-void
.end method

.method public static a(LX/0QB;)LX/0Zt;
    .locals 7

    .prologue
    .line 83984
    sget-object v0, LX/0Zt;->e:LX/0Zt;

    if-nez v0, :cond_1

    .line 83985
    const-class v1, LX/0Zt;

    monitor-enter v1

    .line 83986
    :try_start_0
    sget-object v0, LX/0Zt;->e:LX/0Zt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 83987
    if-eqz v2, :cond_0

    .line 83988
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 83989
    new-instance v3, LX/0Zt;

    const/16 v4, 0x1ce

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1cc

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1c5

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x1c6

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, v6, p0}, LX/0Zt;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 83990
    move-object v0, v3

    .line 83991
    sput-object v0, LX/0Zt;->e:LX/0Zt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83992
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 83993
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 83994
    :cond_1
    sget-object v0, LX/0Zt;->e:LX/0Zt;

    return-object v0

    .line 83995
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 83996
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Zz;)LX/0Xl;
    .locals 2

    .prologue
    .line 83997
    sget-object v0, LX/0a0;->a:[I

    invoke-virtual {p1}, LX/0Zz;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 83998
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown broadcast manager type!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83999
    :pswitch_0
    iget-object v0, p0, LX/0Zt;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    .line 84000
    :goto_0
    return-object v0

    .line 84001
    :pswitch_1
    iget-object v0, p0, LX/0Zt;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    goto :goto_0

    .line 84002
    :pswitch_2
    iget-object v0, p0, LX/0Zt;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    goto :goto_0

    .line 84003
    :pswitch_3
    iget-object v0, p0, LX/0Zt;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
