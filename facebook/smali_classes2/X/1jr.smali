.class public LX/1jr;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final i:LX/1jt;


# instance fields
.field public a:LX/1jp;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0u5;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0uE;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0u5;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0uE;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:[LX/0uE;

.field public h:LX/0uc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 300937
    new-instance v0, LX/1js;

    invoke-direct {v0}, LX/1js;-><init>()V

    sput-object v0, LX/1jr;->i:LX/1jt;

    return-void
.end method

.method public constructor <init>(LX/1jp;LX/0u5;LX/0uE;Ljava/lang/String;[LX/0uE;LX/0uc;)V
    .locals 9
    .param p2    # LX/0u5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0uE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 300938
    if-nez p2, :cond_0

    move-object v2, v5

    :goto_0
    if-nez p3, :cond_1

    move-object v3, v5

    :goto_1
    if-nez p4, :cond_2

    move-object v4, v5

    :goto_2
    move-object v0, p0

    move-object v1, p1

    move-object v6, v5

    move-object v7, p5

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, LX/1jr;-><init>(LX/1jp;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;[LX/0uE;LX/0uc;)V

    .line 300939
    return-void

    .line 300940
    :cond_0
    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    goto :goto_0

    :cond_1
    invoke-static {p3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    goto :goto_1

    :cond_2
    invoke-static {p4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    goto :goto_2
.end method

.method public constructor <init>(LX/1jp;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;[LX/0uE;LX/0uc;)V
    .locals 0
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1jp;",
            "Ljava/util/List",
            "<",
            "LX/0u5;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/0uE;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/0u5;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/0uE;",
            ">;[",
            "LX/0uE;",
            "LX/0uc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 300941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 300942
    iput-object p1, p0, LX/1jr;->a:LX/1jp;

    .line 300943
    iput-object p2, p0, LX/1jr;->b:Ljava/util/List;

    .line 300944
    iput-object p3, p0, LX/1jr;->c:Ljava/util/List;

    .line 300945
    iput-object p4, p0, LX/1jr;->d:Ljava/util/List;

    .line 300946
    iput-object p5, p0, LX/1jr;->e:Ljava/util/List;

    .line 300947
    iput-object p6, p0, LX/1jr;->f:Ljava/util/List;

    .line 300948
    iput-object p7, p0, LX/1jr;->g:[LX/0uE;

    .line 300949
    iput-object p8, p0, LX/1jr;->h:LX/0uc;

    .line 300950
    return-void
.end method

.method public constructor <init>(LX/1jp;[LX/0uE;LX/0uc;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 300951
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, LX/1jr;-><init>(LX/1jp;LX/0u5;LX/0uE;Ljava/lang/String;[LX/0uE;LX/0uc;)V

    .line 300952
    return-void
.end method

.method private static a(LX/1jr;I)LX/0uE;
    .locals 2

    .prologue
    .line 300953
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/1jr;->g:[LX/0uE;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 300954
    iget-object v0, p0, LX/1jr;->g:[LX/0uE;

    aget-object v0, v0, p1

    .line 300955
    if-nez v0, :cond_1

    .line 300956
    new-instance v0, LX/5MH;

    const-string v1, "Param value not specified"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300957
    :cond_0
    new-instance v0, LX/5MH;

    const-string v1, "Output index out of range"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300958
    :cond_1
    return-object v0
.end method

.method private static a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/util/Collection;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<+",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 300909
    if-eqz p2, :cond_1

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 300910
    if-eqz p1, :cond_0

    .line 300911
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300912
    :cond_0
    const-string v2, ", "

    sget-object v3, LX/1jr;->i:LX/1jt;

    new-array v4, v0, [Ljava/lang/Object;

    aput-object p2, v4, v1

    invoke-static {p0, v2, v3, v4}, LX/0YN;->a(Ljava/lang/StringBuilder;Ljava/lang/String;LX/1jt;[Ljava/lang/Object;)V

    .line 300913
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(ID)D
    .locals 4

    .prologue
    .line 300959
    invoke-virtual {p0}, LX/1jr;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 300960
    :goto_0
    return-wide p2

    .line 300961
    :cond_0
    :try_start_0
    invoke-static {p0, p1}, LX/1jr;->a(LX/1jr;I)LX/0uE;

    move-result-object v0

    invoke-virtual {v0}, LX/0uE;->d()D
    :try_end_0
    .catch LX/5MH; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    goto :goto_0

    .line 300962
    :catch_0
    move-exception v0

    .line 300963
    iget-object v1, p0, LX/1jr;->h:LX/0uc;

    iget-object v2, p0, LX/1jr;->a:LX/1jp;

    invoke-interface {v2}, LX/1jp;->a()LX/1jk;

    move-result-object v2

    invoke-virtual {v0}, LX/5MH;->getMessage()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LX/1jr;->a:LX/1jp;

    invoke-interface {v3}, LX/1jp;->b()I

    move-result v3

    invoke-interface {v1, v2, v0, v3}, LX/0uc;->a(LX/1jk;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 300964
    invoke-virtual {p0}, LX/1jr;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300965
    iget-object v0, p0, LX/1jr;->a:LX/1jp;

    invoke-interface {v0, p1}, LX/1jp;->a(Ljava/lang/String;)I

    move-result v0

    .line 300966
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(IJ)J
    .locals 4

    .prologue
    .line 300967
    invoke-virtual {p0}, LX/1jr;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 300968
    :goto_0
    return-wide p2

    .line 300969
    :cond_0
    :try_start_0
    invoke-static {p0, p1}, LX/1jr;->a(LX/1jr;I)LX/0uE;

    move-result-object v0

    invoke-virtual {v0}, LX/0uE;->c()J
    :try_end_0
    .catch LX/5MH; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    goto :goto_0

    .line 300970
    :catch_0
    move-exception v0

    .line 300971
    iget-object v1, p0, LX/1jr;->h:LX/0uc;

    iget-object v2, p0, LX/1jr;->a:LX/1jp;

    invoke-interface {v2}, LX/1jp;->a()LX/1jk;

    move-result-object v2

    invoke-virtual {v0}, LX/5MH;->getMessage()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LX/1jr;->a:LX/1jp;

    invoke-interface {v3}, LX/1jp;->b()I

    move-result v3

    invoke-interface {v1, v2, v0, v3}, LX/0uc;->a(LX/1jk;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;J)J
    .locals 4

    .prologue
    .line 300972
    invoke-virtual {p0, p1}, LX/1jr;->a(Ljava/lang/String;)I

    move-result v0

    .line 300973
    if-gez v0, :cond_0

    .line 300974
    iget-object v0, p0, LX/1jr;->h:LX/0uc;

    iget-object v1, p0, LX/1jr;->a:LX/1jp;

    invoke-interface {v1}, LX/1jp;->a()LX/1jk;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Param not found: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/1jr;->a:LX/1jp;

    invoke-interface {v3}, LX/1jp;->b()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, LX/0uc;->a(LX/1jk;Ljava/lang/String;I)V

    .line 300975
    :goto_0
    return-wide p2

    :cond_0
    invoke-virtual {p0, v0, p2, p3}, LX/1jr;->a(IJ)J

    move-result-wide p2

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 300932
    invoke-virtual {p0}, LX/1jr;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 300933
    :goto_0
    return-object p2

    .line 300934
    :cond_0
    :try_start_0
    invoke-static {p0, p1}, LX/1jr;->a(LX/1jr;I)LX/0uE;

    move-result-object v0

    invoke-virtual {v0}, LX/0uE;->toString()Ljava/lang/String;
    :try_end_0
    .catch LX/5MH; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    goto :goto_0

    .line 300935
    :catch_0
    move-exception v0

    .line 300936
    iget-object v1, p0, LX/1jr;->h:LX/0uc;

    iget-object v2, p0, LX/1jr;->a:LX/1jp;

    invoke-interface {v2}, LX/1jp;->a()LX/1jk;

    move-result-object v2

    invoke-virtual {v0}, LX/5MH;->getMessage()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LX/1jr;->a:LX/1jp;

    invoke-interface {v3}, LX/1jp;->b()I

    move-result v3

    invoke-interface {v1, v2, v0, v3}, LX/0uc;->a(LX/1jk;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public final a(IZ)Z
    .locals 4

    .prologue
    .line 300976
    invoke-virtual {p0}, LX/1jr;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 300977
    :goto_0
    return p2

    .line 300978
    :cond_0
    :try_start_0
    invoke-static {p0, p1}, LX/1jr;->a(LX/1jr;I)LX/0uE;

    move-result-object v0

    invoke-virtual {v0}, LX/0uE;->b()Z
    :try_end_0
    .catch LX/5MH; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    goto :goto_0

    .line 300979
    :catch_0
    move-exception v0

    .line 300980
    iget-object v1, p0, LX/1jr;->h:LX/0uc;

    iget-object v2, p0, LX/1jr;->a:LX/1jp;

    invoke-interface {v2}, LX/1jp;->a()LX/1jk;

    move-result-object v2

    invoke-virtual {v0}, LX/5MH;->getMessage()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LX/1jr;->a:LX/1jp;

    invoke-interface {v3}, LX/1jp;->b()I

    move-result v3

    invoke-interface {v1, v2, v0, v3}, LX/0uc;->a(LX/1jk;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 4

    .prologue
    .line 300928
    invoke-virtual {p0, p1}, LX/1jr;->a(Ljava/lang/String;)I

    move-result v0

    .line 300929
    if-gez v0, :cond_0

    .line 300930
    iget-object v0, p0, LX/1jr;->h:LX/0uc;

    iget-object v1, p0, LX/1jr;->a:LX/1jp;

    invoke-interface {v1}, LX/1jp;->a()LX/1jk;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Param not found: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/1jr;->a:LX/1jp;

    invoke-interface {v3}, LX/1jp;->b()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, LX/0uc;->a(LX/1jk;Ljava/lang/String;I)V

    .line 300931
    :goto_0
    return p2

    :cond_0
    invoke-virtual {p0, v0, p2}, LX/1jr;->a(IZ)Z

    move-result p2

    goto :goto_0
.end method

.method public final a(Ljava/lang/StringBuilder;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 300927
    iget-object v0, p0, LX/1jr;->b:Ljava/util/List;

    invoke-static {p1, p2, v0}, LX/1jr;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 300926
    iget-object v0, p0, LX/1jr;->a:LX/1jp;

    invoke-interface {v0}, LX/1jp;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/StringBuilder;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 300925
    iget-object v0, p0, LX/1jr;->c:Ljava/util/List;

    invoke-static {p1, p2, v0}, LX/1jr;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 300924
    iget-object v0, p0, LX/1jr;->a:LX/1jp;

    invoke-interface {v0}, LX/1jp;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/StringBuilder;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 300923
    iget-object v0, p0, LX/1jr;->d:Ljava/util/List;

    invoke-static {p1, p2, v0}, LX/1jr;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 300922
    iget-object v0, p0, LX/1jr;->g:[LX/0uE;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/StringBuilder;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 300921
    iget-object v0, p0, LX/1jr;->e:Ljava/util/List;

    invoke-static {p1, p2, v0}, LX/1jr;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final e(Ljava/lang/StringBuilder;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 300920
    iget-object v0, p0, LX/1jr;->f:Ljava/util/List;

    invoke-static {p1, p2, v0}, LX/1jr;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final f(Ljava/lang/StringBuilder;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 300914
    iget-object v0, p0, LX/1jr;->g:[LX/0uE;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1jr;->g:[LX/0uE;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 300915
    if-eqz p2, :cond_0

    .line 300916
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300917
    :cond_0
    const-string v0, ", "

    sget-object v1, LX/1jr;->i:LX/1jt;

    iget-object v2, p0, LX/1jr;->g:[LX/0uE;

    invoke-static {p1, v0, v1, v2}, LX/0YN;->a(Ljava/lang/StringBuilder;Ljava/lang/String;LX/1jt;[Ljava/lang/Object;)V

    .line 300918
    const/4 v0, 0x1

    .line 300919
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
