.class public LX/1cK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cF",
        "<",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/1cF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1Ib;


# direct methods
.method public constructor <init>(LX/1cF;LX/1Ib;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;",
            "LX/1Ib;",
            ")V"
        }
    .end annotation

    .prologue
    .line 281870
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 281871
    iput-object p1, p0, LX/1cK;->a:LX/1cF;

    .line 281872
    iput-object p2, p0, LX/1cK;->b:LX/1Ib;

    .line 281873
    return-void
.end method

.method public static a(LX/1BV;Ljava/lang/String;Z)Ljava/util/Map;
    .locals 2
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1BV;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 281895
    invoke-interface {p0, p1}, LX/1BV;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 281896
    const/4 v0, 0x0

    .line 281897
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "cached_value_found"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/2oC;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1cd;LX/1cW;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 281874
    iget-object v0, p2, LX/1cW;->a:LX/1bf;

    move-object v0, v0

    .line 281875
    iget-boolean v1, v0, LX/1bf;->l:Z

    move v1, v1

    .line 281876
    if-nez v1, :cond_0

    .line 281877
    iget-object v0, p2, LX/1cW;->e:LX/1bY;

    move-object v0, v0

    .line 281878
    invoke-virtual {v0}, LX/1bY;->getValue()I

    move-result v0

    sget-object v1, LX/1bY;->DISK_CACHE:LX/1bY;

    invoke-virtual {v1}, LX/1bY;->getValue()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 281879
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 281880
    :goto_0
    return-void

    .line 281881
    :cond_0
    iget-object v1, p2, LX/1cW;->c:LX/1BV;

    move-object v1, v1

    .line 281882
    iget-object v2, p2, LX/1cW;->b:Ljava/lang/String;

    move-object v2, v2

    .line 281883
    const-string v3, "DiskCacheProducer"

    invoke-interface {v1, v2, v3}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 281884
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 281885
    iget-object v2, p0, LX/1cK;->b:LX/1Ib;

    .line 281886
    iget-object v3, p2, LX/1cW;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 281887
    invoke-interface {v2, v0, v3, v1}, LX/1Ib;->a(LX/1bf;Ljava/lang/Object;Ljava/util/concurrent/atomic/AtomicBoolean;)LX/1eg;

    move-result-object v0

    .line 281888
    iget-object v4, p2, LX/1cW;->b:Ljava/lang/String;

    move-object v7, v4

    .line 281889
    iget-object v4, p2, LX/1cW;->c:LX/1BV;

    move-object v6, v4

    .line 281890
    new-instance v4, LX/1ew;

    move-object v5, p0

    move-object v8, p1

    move-object v9, p2

    invoke-direct/range {v4 .. v9}, LX/1ew;-><init>(LX/1cK;LX/1BV;Ljava/lang/String;LX/1cd;LX/1cW;)V

    move-object v2, v4

    .line 281891
    invoke-virtual {v0, v2}, LX/1eg;->a(LX/1ex;)LX/1eg;

    .line 281892
    new-instance v0, LX/1ez;

    invoke-direct {v0, p0, v1}, LX/1ez;-><init>(LX/1cK;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    invoke-virtual {p2, v0}, LX/1cW;->a(LX/1cg;)V

    .line 281893
    goto :goto_0

    .line 281894
    :cond_1
    iget-object v0, p0, LX/1cK;->a:LX/1cF;

    invoke-interface {v0, p1, p2}, LX/1cF;->a(LX/1cd;LX/1cW;)V

    goto :goto_0
.end method
