.class public LX/0d9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0d9;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 90005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90006
    iput-object p1, p0, LX/0d9;->a:Landroid/content/Context;

    .line 90007
    return-void
.end method

.method public static a(LX/0QB;)LX/0d9;
    .locals 4

    .prologue
    .line 90008
    sget-object v0, LX/0d9;->b:LX/0d9;

    if-nez v0, :cond_1

    .line 90009
    const-class v1, LX/0d9;

    monitor-enter v1

    .line 90010
    :try_start_0
    sget-object v0, LX/0d9;->b:LX/0d9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 90011
    if-eqz v2, :cond_0

    .line 90012
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 90013
    new-instance p0, LX/0d9;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/0d9;-><init>(Landroid/content/Context;)V

    .line 90014
    move-object v0, p0

    .line 90015
    sput-object v0, LX/0d9;->b:LX/0d9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90016
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 90017
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 90018
    :cond_1
    sget-object v0, LX/0d9;->b:LX/0d9;

    return-object v0

    .line 90019
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 90020
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0d9;I)V
    .locals 3

    .prologue
    .line 90021
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, LX/0d9;->a:Landroid/content/Context;

    const-class v2, Lcom/facebook/photos/upload/receiver/ConnectivityChangeReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 90022
    iget-object v1, p0, LX/0d9;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v1

    .line 90023
    if-eq v1, p1, :cond_0

    .line 90024
    iget-object v1, p0, LX/0d9;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p1, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 90025
    :cond_0
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 90026
    const/4 v0, 0x2

    invoke-static {p0, v0}, LX/0d9;->a(LX/0d9;I)V

    .line 90027
    return-void
.end method
