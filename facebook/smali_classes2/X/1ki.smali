.class public abstract LX/1ki;
.super LX/0SP;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<InputT:",
        "Ljava/lang/Object;",
        "OutputT:",
        "Ljava/lang/Object;",
        ">",
        "LX/0SP",
        "<TOutputT;>;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/logging/Logger;


# instance fields
.field public b:Lcom/google/common/util/concurrent/AggregateFuture$RunningState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ki",
            "<TInputT;TOutputT;>.RunningState;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 310169
    const-class v0, LX/1ki;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, LX/1ki;->a:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 310167
    invoke-direct {p0}, LX/0SP;-><init>()V

    .line 310168
    return-void
.end method

.method public static synthetic a(LX/1ki;Z)Z
    .locals 1

    .prologue
    .line 310166
    invoke-super {p0, p1}, LX/0SQ;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method public static b(Ljava/util/Set;Ljava/lang/Throwable;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Throwable;",
            ">;",
            "Ljava/lang/Throwable;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 310170
    :goto_0
    if-eqz p1, :cond_1

    .line 310171
    invoke-interface {p0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 310172
    if-nez v0, :cond_0

    .line 310173
    const/4 v0, 0x0

    .line 310174
    :goto_1
    return v0

    .line 310175
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    goto :goto_0

    .line 310176
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final cancel(Z)Z
    .locals 3

    .prologue
    .line 310157
    iget-object v0, p0, LX/1ki;->b:Lcom/google/common/util/concurrent/AggregateFuture$RunningState;

    .line 310158
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->b:LX/0Py;

    .line 310159
    :goto_0
    invoke-super {p0, p1}, LX/0SP;->cancel(Z)Z

    move-result v2

    .line 310160
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_1
    and-int/2addr v1, v2

    if-eqz v1, :cond_2

    .line 310161
    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 310162
    invoke-interface {v0, p1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    goto :goto_2

    .line 310163
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 310164
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 310165
    :cond_2
    return v2
.end method

.method public final done()V
    .locals 1

    .prologue
    .line 310154
    invoke-super {p0}, LX/0SP;->done()V

    .line 310155
    const/4 v0, 0x0

    iput-object v0, p0, LX/1ki;->b:Lcom/google/common/util/concurrent/AggregateFuture$RunningState;

    .line 310156
    return-void
.end method

.method public final interruptTask()V
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "Interruption not supported"
    .end annotation

    .prologue
    .line 310151
    iget-object v0, p0, LX/1ki;->b:Lcom/google/common/util/concurrent/AggregateFuture$RunningState;

    .line 310152
    if-eqz v0, :cond_0

    .line 310153
    :cond_0
    return-void
.end method
