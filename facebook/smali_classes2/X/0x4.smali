.class public LX/0x4;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hk;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/os/Handler;

.field public c:LX/0f7;

.field public d:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 161868
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 161869
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 161870
    iput-object v0, p0, LX/0x4;->e:LX/0Ot;

    .line 161871
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/0x4;->b:Landroid/os/Handler;

    .line 161872
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 5

    .prologue
    .line 161873
    iget-object v0, p0, LX/0x4;->c:LX/0f7;

    sget-object v1, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    invoke-virtual {v1}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0f7;->a(Ljava/lang/String;)LX/0hO;

    move-result-object v0

    .line 161874
    if-nez v0, :cond_1

    .line 161875
    :cond_0
    :goto_0
    return-void

    .line 161876
    :cond_1
    sget-object v0, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/0x4;->c:LX/0f7;

    invoke-interface {v1}, LX/0f7;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 161877
    iget-object v0, p0, LX/0x4;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iA;

    .line 161878
    const-string v1, "3907"

    const-class v2, LX/16D;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/16D;

    .line 161879
    const-string v2, "4163"

    const-class v3, LX/16S;

    invoke-virtual {v0, v2, v3}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/16S;

    .line 161880
    if-nez v1, :cond_2

    if-eqz v0, :cond_0

    .line 161881
    :cond_2
    new-instance v0, Lcom/facebook/katana/fragment/maintab/controllercallbacks/FbMainTabFragmentSavedBookmarkNuxController$1;

    invoke-direct {v0, p0}, Lcom/facebook/katana/fragment/maintab/controllercallbacks/FbMainTabFragmentSavedBookmarkNuxController$1;-><init>(LX/0x4;)V

    .line 161882
    iget-object v1, p0, LX/0x4;->b:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    const v4, 0x7bec4992

    invoke-static {v1, v0, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 161867
    return-void
.end method
