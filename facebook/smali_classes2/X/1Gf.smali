.class public LX/1Gf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field public final d:J

.field public final e:J

.field public final f:J

.field public final g:LX/1GU;

.field public final h:LX/1GQ;

.field public final i:LX/1GE;

.field public final j:LX/0pr;

.field public final k:Landroid/content/Context;

.field public final l:Z


# direct methods
.method public constructor <init>(LX/1Gg;)V
    .locals 2

    .prologue
    .line 225800
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225801
    iget v0, p1, LX/1Gg;->a:I

    iput v0, p0, LX/1Gf;->a:I

    .line 225802
    iget-object v0, p1, LX/1Gg;->b:Ljava/lang/String;

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/1Gf;->b:Ljava/lang/String;

    .line 225803
    iget-object v0, p1, LX/1Gg;->c:LX/1Gd;

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Gd;

    iput-object v0, p0, LX/1Gf;->c:LX/1Gd;

    .line 225804
    iget-wide v0, p1, LX/1Gg;->d:J

    iput-wide v0, p0, LX/1Gf;->d:J

    .line 225805
    iget-wide v0, p1, LX/1Gg;->e:J

    iput-wide v0, p0, LX/1Gf;->e:J

    .line 225806
    iget-wide v0, p1, LX/1Gg;->f:J

    iput-wide v0, p0, LX/1Gf;->f:J

    .line 225807
    iget-object v0, p1, LX/1Gg;->g:LX/1GU;

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1GU;

    iput-object v0, p0, LX/1Gf;->g:LX/1GU;

    .line 225808
    iget-object v0, p1, LX/1Gg;->h:LX/1GQ;

    if-nez v0, :cond_0

    invoke-static {}, LX/43X;->a()LX/43X;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/1Gf;->h:LX/1GQ;

    .line 225809
    iget-object v0, p1, LX/1Gg;->i:LX/1GE;

    if-nez v0, :cond_1

    invoke-static {}, LX/43Y;->b()LX/43Y;

    move-result-object v0

    :goto_1
    iput-object v0, p0, LX/1Gf;->i:LX/1GE;

    .line 225810
    iget-object v0, p1, LX/1Gg;->j:LX/0pr;

    if-nez v0, :cond_2

    invoke-static {}, LX/44I;->a()LX/44I;

    move-result-object v0

    :goto_2
    iput-object v0, p0, LX/1Gf;->j:LX/0pr;

    .line 225811
    iget-object v0, p1, LX/1Gg;->l:Landroid/content/Context;

    iput-object v0, p0, LX/1Gf;->k:Landroid/content/Context;

    .line 225812
    iget-boolean v0, p1, LX/1Gg;->k:Z

    iput-boolean v0, p0, LX/1Gf;->l:Z

    .line 225813
    return-void

    .line 225814
    :cond_0
    iget-object v0, p1, LX/1Gg;->h:LX/1GQ;

    goto :goto_0

    .line 225815
    :cond_1
    iget-object v0, p1, LX/1Gg;->i:LX/1GE;

    goto :goto_1

    .line 225816
    :cond_2
    iget-object v0, p1, LX/1Gg;->j:LX/0pr;

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;)LX/1Gg;
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 225817
    new-instance v0, LX/1Gg;

    invoke-direct {v0, p0}, LX/1Gg;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final d()J
    .locals 2

    .prologue
    .line 225818
    iget-wide v0, p0, LX/1Gf;->d:J

    return-wide v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 225819
    iget-wide v0, p0, LX/1Gf;->e:J

    return-wide v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 225820
    iget-wide v0, p0, LX/1Gf;->f:J

    return-wide v0
.end method
