.class public LX/1Tg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;
.implements LX/1TG;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/researchpoll/ResearchPollGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/researchpoll/ResearchPollGroupPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 253910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253911
    iput-object p1, p0, LX/1Tg;->a:LX/0Ot;

    .line 253912
    return-void
.end method

.method public static a(LX/0QB;)LX/1Tg;
    .locals 4

    .prologue
    .line 253899
    const-class v1, LX/1Tg;

    monitor-enter v1

    .line 253900
    :try_start_0
    sget-object v0, LX/1Tg;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 253901
    sput-object v2, LX/1Tg;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 253902
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253903
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 253904
    new-instance v3, LX/1Tg;

    const/16 p0, 0x2140

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1Tg;-><init>(LX/0Ot;)V

    .line 253905
    move-object v0, v3

    .line 253906
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 253907
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Tg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 253908
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 253909
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 1

    .prologue
    .line 253913
    sget-object v0, Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;->a:LX/1Cz;

    invoke-static {p1, v0}, LX/99p;->a(LX/0ja;LX/1Cz;)V

    .line 253914
    return-void
.end method

.method public final a(LX/1KB;)V
    .locals 4

    .prologue
    .line 253894
    sget-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->a:LX/1Cz;

    sget-object v1, Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;->a:LX/1Cz;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 253895
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cz;

    .line 253896
    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 253897
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 253898
    :cond_0
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 253892
    const-class v0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    iget-object v1, p0, LX/1Tg;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 253893
    return-void
.end method
