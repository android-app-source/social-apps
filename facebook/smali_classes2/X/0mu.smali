.class public final LX/0mu;
.super LX/0m3;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0m3",
        "<",
        "LX/0mv;",
        "LX/0mu;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x3aab0708427a1a1fL


# instance fields
.field public final _deserFeatures:I

.field public final _nodeFactory:LX/0mC;

.field public final _problemHandlers:LX/4rn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4rn",
            "<",
            "LX/4q4;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0lh;LX/0m0;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lh;",
            "LX/0m0;",
            "Ljava/util/Map",
            "<",
            "LX/1Xc;",
            "Ljava/lang/Class",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 133201
    invoke-direct {p0, p1, p2, p3}, LX/0m3;-><init>(LX/0lh;LX/0m0;Ljava/util/Map;)V

    .line 133202
    const-class v0, LX/0mv;

    invoke-static {v0}, LX/0m4;->a(Ljava/lang/Class;)I

    move-result v0

    iput v0, p0, LX/0mu;->_deserFeatures:I

    .line 133203
    sget-object v0, LX/0mC;->a:LX/0mC;

    iput-object v0, p0, LX/0mu;->_nodeFactory:LX/0mC;

    .line 133204
    const/4 v0, 0x0

    iput-object v0, p0, LX/0mu;->_problemHandlers:LX/4rn;

    .line 133205
    return-void
.end method

.method private constructor <init>(LX/0mu;II)V
    .locals 1

    .prologue
    .line 133196
    invoke-direct {p0, p1, p2}, LX/0m3;-><init>(LX/0m3;I)V

    .line 133197
    iput p3, p0, LX/0mu;->_deserFeatures:I

    .line 133198
    iget-object v0, p1, LX/0mu;->_nodeFactory:LX/0mC;

    iput-object v0, p0, LX/0mu;->_nodeFactory:LX/0mC;

    .line 133199
    iget-object v0, p1, LX/0mu;->_problemHandlers:LX/4rn;

    iput-object v0, p0, LX/0mu;->_problemHandlers:LX/4rn;

    .line 133200
    return-void
.end method

.method private constructor <init>(LX/0mu;LX/0lh;)V
    .locals 1

    .prologue
    .line 133191
    invoke-direct {p0, p1, p2}, LX/0m3;-><init>(LX/0m3;LX/0lh;)V

    .line 133192
    iget v0, p1, LX/0mu;->_deserFeatures:I

    iput v0, p0, LX/0mu;->_deserFeatures:I

    .line 133193
    iget-object v0, p1, LX/0mu;->_nodeFactory:LX/0mC;

    iput-object v0, p0, LX/0mu;->_nodeFactory:LX/0mC;

    .line 133194
    iget-object v0, p1, LX/0mu;->_problemHandlers:LX/4rn;

    iput-object v0, p0, LX/0mu;->_problemHandlers:LX/4rn;

    .line 133195
    return-void
.end method

.method private final a(LX/0lh;)LX/0mu;
    .locals 1

    .prologue
    .line 133190
    iget-object v0, p0, LX/0m4;->_base:LX/0lh;

    if-ne v0, p1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/0mu;

    invoke-direct {v0, p0, p1}, LX/0mu;-><init>(LX/0mu;LX/0lh;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0lJ;)LX/0lS;
    .locals 1

    .prologue
    .line 133189
    invoke-virtual {p0}, LX/0m4;->j()LX/0lM;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p0}, LX/0lM;->b(LX/0m4;LX/0lJ;LX/0m5;)LX/0lS;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/0lU;
    .locals 1

    .prologue
    .line 133186
    sget-object v0, LX/0m6;->USE_ANNOTATIONS:LX/0m6;

    invoke-virtual {p0, v0}, LX/0m4;->a(LX/0m6;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133187
    invoke-super {p0}, LX/0m3;->a()LX/0lU;

    move-result-object v0

    .line 133188
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/4qr;->a:LX/4qr;

    goto :goto_0
.end method

.method public final a(LX/0li;)LX/0mu;
    .locals 1

    .prologue
    .line 133185
    iget-object v0, p0, LX/0m4;->_base:LX/0lh;

    invoke-virtual {v0, p1}, LX/0lh;->a(LX/0li;)LX/0lh;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0mu;->a(LX/0lh;)LX/0mu;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0mv;)LX/0mu;
    .locals 3

    .prologue
    .line 133183
    iget v0, p0, LX/0mu;->_deserFeatures:I

    invoke-virtual {p1}, LX/0mv;->getMask()I

    move-result v1

    or-int/2addr v1, v0

    .line 133184
    iget v0, p0, LX/0mu;->_deserFeatures:I

    if-ne v1, v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/0mu;

    iget v2, p0, LX/0m4;->_mapperFeatures:I

    invoke-direct {v0, p0, v2, v1}, LX/0mu;-><init>(LX/0mu;II)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/0np;LX/0lX;)LX/0mu;
    .locals 1

    .prologue
    .line 133163
    iget-object v0, p0, LX/0m4;->_base:LX/0lh;

    invoke-virtual {v0, p1, p2}, LX/0lh;->a(LX/0np;LX/0lX;)LX/0lh;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0mu;->a(LX/0lh;)LX/0mu;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/0lJ;)LX/0lS;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/0lS;",
            ">(",
            "LX/0lJ;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 133182
    invoke-virtual {p0}, LX/0m4;->j()LX/0lM;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p0}, LX/0lM;->d(LX/0mu;LX/0lJ;LX/0m5;)LX/0lS;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/0mv;)LX/0mu;
    .locals 3

    .prologue
    .line 133180
    iget v0, p0, LX/0mu;->_deserFeatures:I

    invoke-virtual {p1}, LX/0mv;->getMask()I

    move-result v1

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v1, v0

    .line 133181
    iget v0, p0, LX/0mu;->_deserFeatures:I

    if-ne v1, v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/0mu;

    iget v2, p0, LX/0m4;->_mapperFeatures:I

    invoke-direct {v0, p0, v2, v1}, LX/0mu;-><init>(LX/0mu;II)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 133175
    iget-object v0, p0, LX/0m3;->_rootName:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 133176
    iget-object v0, p0, LX/0m3;->_rootName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 133177
    :goto_0
    return v0

    .line 133178
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 133179
    :cond_1
    sget-object v0, LX/0mv;->UNWRAP_ROOT_VALUE:LX/0mv;

    invoke-virtual {p0, v0}, LX/0mu;->c(LX/0mv;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c(LX/0lJ;)LX/0lS;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/0lS;",
            ">(",
            "LX/0lJ;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 133174
    invoke-virtual {p0}, LX/0m4;->j()LX/0lM;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p0}, LX/0lM;->b(LX/0mu;LX/0lJ;LX/0m5;)LX/0lS;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/0lW;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0lW",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 133166
    invoke-super {p0}, LX/0m3;->c()LX/0lW;

    move-result-object v0

    .line 133167
    sget-object v1, LX/0m6;->AUTO_DETECT_SETTERS:LX/0m6;

    invoke-virtual {p0, v1}, LX/0m4;->a(LX/0m6;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 133168
    sget-object v1, LX/0lX;->NONE:LX/0lX;

    invoke-interface {v0, v1}, LX/0lW;->c(LX/0lX;)LX/0lW;

    move-result-object v0

    .line 133169
    :cond_0
    sget-object v1, LX/0m6;->AUTO_DETECT_CREATORS:LX/0m6;

    invoke-virtual {p0, v1}, LX/0m4;->a(LX/0m6;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 133170
    sget-object v1, LX/0lX;->NONE:LX/0lX;

    invoke-interface {v0, v1}, LX/0lW;->d(LX/0lX;)LX/0lW;

    move-result-object v0

    .line 133171
    :cond_1
    sget-object v1, LX/0m6;->AUTO_DETECT_FIELDS:LX/0m6;

    invoke-virtual {p0, v1}, LX/0m4;->a(LX/0m6;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 133172
    sget-object v1, LX/0lX;->NONE:LX/0lX;

    invoke-interface {v0, v1}, LX/0lW;->e(LX/0lX;)LX/0lW;

    move-result-object v0

    .line 133173
    :cond_2
    return-object v0
.end method

.method public final c(LX/0mv;)Z
    .locals 2

    .prologue
    .line 133165
    iget v0, p0, LX/0mu;->_deserFeatures:I

    invoke-virtual {p1}, LX/0mv;->getMask()I

    move-result v1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(LX/0lJ;)LX/0lS;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/0lS;",
            ">(",
            "LX/0lJ;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 133164
    invoke-virtual {p0}, LX/0m4;->j()LX/0lM;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p0}, LX/0lM;->c(LX/0mu;LX/0lJ;LX/0m5;)LX/0lS;

    move-result-object v0

    return-object v0
.end method
