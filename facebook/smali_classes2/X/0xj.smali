.class public final LX/0xj;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/apptab/glyph/BadgableGlyphView;


# direct methods
.method public constructor <init>(Lcom/facebook/apptab/glyph/BadgableGlyphView;)V
    .locals 0

    .prologue
    .line 163405
    iput-object p1, p0, LX/0xj;->a:Lcom/facebook/apptab/glyph/BadgableGlyphView;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/apptab/glyph/BadgableGlyphView;B)V
    .locals 0

    .prologue
    .line 163404
    invoke-direct {p0, p1}, LX/0xj;-><init>(Lcom/facebook/apptab/glyph/BadgableGlyphView;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 4

    .prologue
    .line 163398
    iget-object v0, p0, LX/0xj;->a:Lcom/facebook/apptab/glyph/BadgableGlyphView;

    iget-object v0, v0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->b:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, LX/0xj;->a:Lcom/facebook/apptab/glyph/BadgableGlyphView;

    iget-object v1, v1, Lcom/facebook/apptab/glyph/BadgableGlyphView;->e:LX/0xk;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/0xk;->a(D)Landroid/graphics/ColorFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 163399
    iget-object v0, p0, LX/0xj;->a:Lcom/facebook/apptab/glyph/BadgableGlyphView;

    iget-object v1, p0, LX/0xj;->a:Lcom/facebook/apptab/glyph/BadgableGlyphView;

    iget-object v1, v1, Lcom/facebook/apptab/glyph/BadgableGlyphView;->e:LX/0xk;

    .line 163400
    iget-object v2, v1, LX/0xk;->d:LX/0xl;

    sget-object v3, LX/0xl;->LIGHT:LX/0xl;

    if-ne v2, v3, :cond_0

    const/4 v2, -0x1

    :goto_0
    move v1, v2

    .line 163401
    invoke-virtual {v0, v1}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setBadgeOutlineColor(I)V

    .line 163402
    iget-object v0, p0, LX/0xj;->a:Lcom/facebook/apptab/glyph/BadgableGlyphView;

    iget-object v1, p0, LX/0xj;->a:Lcom/facebook/apptab/glyph/BadgableGlyphView;

    iget-object v1, v1, Lcom/facebook/apptab/glyph/BadgableGlyphView;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->invalidate(Landroid/graphics/Rect;)V

    .line 163403
    return-void

    :cond_0
    const/high16 v2, -0x1000000

    goto :goto_0
.end method
