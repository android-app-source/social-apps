.class public LX/0k2;
.super LX/0k3;
.source ""


# instance fields
.field private final a:Landroid/support/v4/app/FragmentActivity;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;)V
    .locals 0

    .prologue
    .line 126013
    invoke-direct {p0}, LX/0k3;-><init>()V

    .line 126014
    iput-object p1, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    .line 126015
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;ZZ)LX/0k4;
    .locals 1

    .prologue
    .line 126002
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->a(Ljava/lang/String;ZZ)LX/0k4;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 126003
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0gc;)V
    .locals 1

    .prologue
    .line 126004
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    .line 126005
    check-cast p1, LX/0jz;

    iput-object p1, v0, Landroid/support/v4/app/FragmentActivity;->c:LX/0jz;

    .line 126006
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 126007
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 126008
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 126009
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    .line 126010
    return-void
.end method

.method public final a(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1

    .prologue
    .line 126011
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 126012
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 125979
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    .line 125980
    iget-object v1, v0, Landroid/support/v4/app/FragmentActivity;->m:LX/01J;

    if-eqz v1, :cond_0

    .line 125981
    iget-object v1, v0, Landroid/support/v4/app/FragmentActivity;->m:LX/01J;

    invoke-virtual {v1, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0k4;

    .line 125982
    if-eqz v1, :cond_0

    iget-boolean p0, v1, LX/0k4;->g:Z

    if-nez p0, :cond_0

    .line 125983
    invoke-virtual {v1}, LX/0k4;->h()V

    .line 125984
    iget-object v1, v0, Landroid/support/v4/app/FragmentActivity;->m:LX/01J;

    invoke-virtual {v1, p1}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 125985
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 126016
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v4/app/FragmentActivity;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 126017
    return-void
.end method

.method public final h()Landroid/content/Context;
    .locals 1

    .prologue
    .line 126000
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method public final i()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 126001
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public final j()Landroid/view/Window;
    .locals 1

    .prologue
    .line 125999
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    return-object v0
.end method

.method public final k()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 125998
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 125996
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iA_()V

    .line 125997
    return-void
.end method

.method public final m()Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 125995
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    return-object v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 125994
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    iget-boolean v0, v0, Landroid/support/v4/app/FragmentActivity;->i:Z

    return v0
.end method

.method public final o()LX/0jz;
    .locals 1

    .prologue
    .line 125993
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    return-object v0
.end method

.method public final p()LX/0gc;
    .locals 1

    .prologue
    .line 125992
    invoke-virtual {p0}, LX/0k2;->o()LX/0jz;

    move-result-object v0

    return-object v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 125991
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    return v0
.end method

.method public final r()LX/0gc;
    .locals 1

    .prologue
    .line 125988
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    .line 125989
    iget-object p0, v0, Landroid/support/v4/app/FragmentActivity;->c:LX/0jz;

    move-object v0, p0

    .line 125990
    return-object v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 125986
    iget-object v0, p0, LX/0k2;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 125987
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/Window;->peekDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
