.class public LX/1AN;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0wT;


# instance fields
.field public final b:LX/0wW;

.field public final c:Landroid/graphics/Rect;

.field public final d:[I

.field public final e:Landroid/os/Handler;

.field public final f:LX/0ad;

.field public final g:J

.field public final h:Ljava/lang/Runnable;

.field public final i:Ljava/lang/Runnable;

.field public j:LX/1AP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:Landroid/view/View;

.field public l:LX/0wd;

.field public m:LX/30w;

.field public n:LX/1AO;

.field public o:LX/0g8;

.field public p:LX/30v;

.field public q:LX/0fx;

.field public r:Z

.field public s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 210400
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/1AN;->a:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;LX/0ad;LX/0wW;)V
    .locals 4
    .param p1    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 210368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210369
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, LX/1AN;->d:[I

    .line 210370
    new-instance v0, Lcom/facebook/feed/pill/FeedPillUIController$1;

    invoke-direct {v0, p0}, Lcom/facebook/feed/pill/FeedPillUIController$1;-><init>(LX/1AN;)V

    iput-object v0, p0, LX/1AN;->h:Ljava/lang/Runnable;

    .line 210371
    new-instance v0, Lcom/facebook/feed/pill/FeedPillUIController$2;

    invoke-direct {v0, p0}, Lcom/facebook/feed/pill/FeedPillUIController$2;-><init>(LX/1AN;)V

    iput-object v0, p0, LX/1AN;->i:Ljava/lang/Runnable;

    .line 210372
    sget-object v0, LX/1AO;->HIDDEN:LX/1AO;

    iput-object v0, p0, LX/1AN;->n:LX/1AO;

    .line 210373
    iput-object p1, p0, LX/1AN;->e:Landroid/os/Handler;

    .line 210374
    iput-object p2, p0, LX/1AN;->f:LX/0ad;

    .line 210375
    iput-object p3, p0, LX/1AN;->b:LX/0wW;

    .line 210376
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1AN;->c:Landroid/graphics/Rect;

    .line 210377
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, LX/1AN;->f:LX/0ad;

    sget v2, LX/0fe;->az:I

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    iput-wide v0, p0, LX/1AN;->g:J

    .line 210378
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public static a(LX/0QB;)LX/1AN;
    .locals 4

    .prologue
    .line 210404
    new-instance v3, LX/1AN;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v2

    check-cast v2, LX/0wW;

    invoke-direct {v3, v0, v1, v2}, LX/1AN;-><init>(Landroid/os/Handler;LX/0ad;LX/0wW;)V

    .line 210405
    invoke-static {p0}, LX/1AP;->a(LX/0QB;)LX/1AP;

    move-result-object v0

    check-cast v0, LX/1AP;

    .line 210406
    iput-object v0, v3, LX/1AN;->j:LX/1AP;

    .line 210407
    move-object v0, v3

    .line 210408
    return-object v0
.end method

.method public static g(LX/1AN;)V
    .locals 4

    .prologue
    .line 210401
    invoke-static {p0}, LX/1AN;->k(LX/1AN;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 210402
    :goto_0
    return-void

    .line 210403
    :cond_0
    iget-object v0, p0, LX/1AN;->l:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0
.end method

.method public static h$redex0(LX/1AN;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 210382
    invoke-static {p0}, LX/1AN;->j(LX/1AN;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, LX/1AN;->k(LX/1AN;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, LX/1AN;->r:Z

    if-nez v1, :cond_2

    .line 210383
    :cond_0
    const/4 v0, 0x0

    .line 210384
    :cond_1
    :goto_0
    return v0

    .line 210385
    :cond_2
    sget-object v1, LX/1AO;->REVEALING:LX/1AO;

    iput-object v1, p0, LX/1AN;->n:LX/1AO;

    .line 210386
    iget-object v1, p0, LX/1AN;->l:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 210387
    iget-object v1, p0, LX/1AN;->p:LX/30v;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, LX/1AN;->s:Z

    if-eqz v1, :cond_1

    .line 210388
    iget-object v1, p0, LX/1AN;->p:LX/30v;

    .line 210389
    iput-boolean v0, v1, LX/30v;->b:Z

    .line 210390
    goto :goto_0
.end method

.method public static i(LX/1AN;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 210391
    sget-object v1, LX/1AO;->HIDDEN:LX/1AO;

    iget-object v2, p0, LX/1AN;->n:LX/1AO;

    invoke-virtual {v1, v2}, LX/1AO;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v1, v1

    .line 210392
    if-nez v1, :cond_0

    invoke-static {p0}, LX/1AN;->m(LX/1AN;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, LX/1AN;->r:Z

    if-nez v1, :cond_1

    .line 210393
    :cond_0
    :goto_0
    return v0

    .line 210394
    :cond_1
    sget-object v1, LX/1AO;->HIDING:LX/1AO;

    iput-object v1, p0, LX/1AN;->n:LX/1AO;

    .line 210395
    iget-object v1, p0, LX/1AN;->l:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 210396
    iget-object v1, p0, LX/1AN;->p:LX/30v;

    if-eqz v1, :cond_2

    iget-boolean v1, p0, LX/1AN;->s:Z

    if-eqz v1, :cond_2

    .line 210397
    iget-object v1, p0, LX/1AN;->p:LX/30v;

    .line 210398
    iput-boolean v0, v1, LX/30v;->b:Z

    .line 210399
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static j(LX/1AN;)Z
    .locals 2

    .prologue
    .line 210381
    sget-object v0, LX/1AO;->REVEALING:LX/1AO;

    iget-object v1, p0, LX/1AN;->n:LX/1AO;

    invoke-virtual {v0, v1}, LX/1AO;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static k(LX/1AN;)Z
    .locals 2

    .prologue
    .line 210380
    sget-object v0, LX/1AO;->SHOWN:LX/1AO;

    iget-object v1, p0, LX/1AN;->n:LX/1AO;

    invoke-virtual {v0, v1}, LX/1AO;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static m(LX/1AN;)Z
    .locals 2

    .prologue
    .line 210379
    sget-object v0, LX/1AO;->HIDING:LX/1AO;

    iget-object v1, p0, LX/1AN;->n:LX/1AO;

    invoke-virtual {v0, v1}, LX/1AO;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final d()Z
    .locals 2

    .prologue
    .line 210365
    invoke-static {p0}, LX/1AN;->i(LX/1AN;)Z

    move-result v0

    .line 210366
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/1AN;->r:Z

    .line 210367
    return v0
.end method
