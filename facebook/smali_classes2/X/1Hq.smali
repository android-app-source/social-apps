.class public LX/1Hq;
.super LX/03m;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/1Hq;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 227720
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "conceal"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, LX/03m;-><init>(Ljava/util/List;)V

    .line 227721
    return-void
.end method

.method public static a(LX/0QB;)LX/1Hq;
    .locals 3

    .prologue
    .line 227722
    sget-object v0, LX/1Hq;->a:LX/1Hq;

    if-nez v0, :cond_1

    .line 227723
    const-class v1, LX/1Hq;

    monitor-enter v1

    .line 227724
    :try_start_0
    sget-object v0, LX/1Hq;->a:LX/1Hq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 227725
    if-eqz v2, :cond_0

    .line 227726
    :try_start_1
    new-instance v0, LX/1Hq;

    invoke-direct {v0}, LX/1Hq;-><init>()V

    .line 227727
    move-object v0, v0

    .line 227728
    sput-object v0, LX/1Hq;->a:LX/1Hq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 227729
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 227730
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 227731
    :cond_1
    sget-object v0, LX/1Hq;->a:LX/1Hq;

    return-object v0

    .line 227732
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 227733
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 227734
    :try_start_0
    invoke-virtual {p0}, LX/03m;->b()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 227735
    return-void

    .line 227736
    :catch_0
    move-exception v0

    .line 227737
    new-instance v1, LX/48f;

    invoke-direct {v1, v0}, LX/48f;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 227738
    invoke-static {}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->c()V

    .line 227739
    return-void
.end method
