.class public LX/0Zp;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Landroid/os/Looper;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83908
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Landroid/os/Looper;
    .locals 1

    .prologue
    .line 83909
    invoke-static {p0}, LX/0Zp;->b(LX/0QB;)Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)Landroid/os/Looper;
    .locals 1

    .prologue
    .line 83910
    invoke-static {p0}, LX/0Zq;->a(LX/0QB;)Landroid/os/HandlerThread;

    move-result-object v0

    check-cast v0, Landroid/os/HandlerThread;

    invoke-static {v0}, LX/0Su;->a(Landroid/os/HandlerThread;)Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 83911
    invoke-static {p0}, LX/0Zq;->a(LX/0QB;)Landroid/os/HandlerThread;

    move-result-object v0

    check-cast v0, Landroid/os/HandlerThread;

    invoke-static {v0}, LX/0Su;->a(Landroid/os/HandlerThread;)Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method
