.class public LX/1BF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/mediaavailability/MediaAvailabilityListener;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/offlineavailability/OfflineFeedMediaAvailabilityListener$;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/Set;Ljava/util/Set;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/mediaavailability/MediaAvailabilityListener;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/offlineavailability/OfflineFeedMediaAvailabilityListener$;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 212838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 212839
    iput-object p1, p0, LX/1BF;->a:Ljava/util/Set;

    .line 212840
    iput-object p2, p0, LX/1BF;->b:Ljava/util/Set;

    .line 212841
    return-void
.end method

.method public static a(LX/0QB;)LX/1BF;
    .locals 1

    .prologue
    .line 212883
    invoke-static {p0}, LX/1BF;->b(LX/0QB;)LX/1BF;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1BF;
    .locals 4

    .prologue
    .line 212879
    new-instance v0, LX/1BF;

    .line 212880
    new-instance v1, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v2

    new-instance v3, LX/1BG;

    invoke-direct {v3, p0}, LX/1BG;-><init>(LX/0QB;)V

    invoke-direct {v1, v2, v3}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v1, v1

    .line 212881
    invoke-static {p0}, LX/1BH;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/1BF;-><init>(Ljava/util/Set;Ljava/util/Set;)V

    .line 212882
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 212871
    iget-object v0, p0, LX/1BF;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fI;

    .line 212872
    const-string v2, "VIDEO"

    const/4 v3, 0x1

    invoke-static {v0, p1, v2, v3}, LX/1fI;->a(LX/1fI;Ljava/lang/String;Ljava/lang/String;I)V

    .line 212873
    goto :goto_0

    .line 212874
    :cond_0
    iget-object v0, p0, LX/1BF;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    .line 212875
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 212876
    invoke-static {v0, v2, p1}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->b(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 212877
    goto :goto_1

    .line 212878
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 212861
    iget-object v0, p0, LX/1BF;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fI;

    .line 212862
    const-string v3, "VIDEO"

    if-eqz p2, :cond_2

    const/4 v2, 0x1

    :goto_1
    invoke-static {v0, p1, v3, v2}, LX/1fI;->a(LX/1fI;Ljava/lang/String;Ljava/lang/String;I)V

    .line 212863
    goto :goto_0

    .line 212864
    :cond_0
    if-eqz p2, :cond_1

    .line 212865
    iget-object v0, p0, LX/1BF;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    .line 212866
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 212867
    invoke-static {v0, v2, p1}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->b(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 212868
    goto :goto_2

    .line 212869
    :cond_1
    return-void

    .line 212870
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 212857
    iget-object v0, p0, LX/1BF;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fI;

    .line 212858
    const-string v2, "VIDEO"

    const/4 p0, 0x0

    invoke-static {v0, p1, v2, p0}, LX/1fI;->a(LX/1fI;Ljava/lang/String;Ljava/lang/String;I)V

    .line 212859
    goto :goto_0

    .line 212860
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 212848
    iget-object v0, p0, LX/1BF;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fI;

    .line 212849
    const-string v3, "PHOTO"

    if-eqz p2, :cond_2

    const/4 v2, 0x1

    :goto_1
    invoke-static {v0, p1, v3, v2}, LX/1fI;->a(LX/1fI;Ljava/lang/String;Ljava/lang/String;I)V

    .line 212850
    goto :goto_0

    .line 212851
    :cond_0
    iget-object v0, p0, LX/1BF;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    .line 212852
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 212853
    invoke-static {v0, v2, p1}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->b(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 212854
    goto :goto_2

    .line 212855
    :cond_1
    return-void

    .line 212856
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 212842
    iget-object v0, p0, LX/1BF;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fI;

    .line 212843
    const-string v2, "PHOTO"

    invoke-static {v0, p1, p2, v2}, LX/1fI;->b(LX/1fI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 212844
    goto :goto_0

    .line 212845
    :cond_0
    iget-object v0, p0, LX/1BF;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    .line 212846
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, p1, p2}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->a(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 212847
    :cond_1
    return-void
.end method
