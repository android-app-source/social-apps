.class public LX/0oT;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/00u;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/00u;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 141586
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/00u;
    .locals 4

    .prologue
    .line 141587
    sget-object v0, LX/0oT;->a:LX/00u;

    if-nez v0, :cond_1

    .line 141588
    const-class v1, LX/0oT;

    monitor-enter v1

    .line 141589
    :try_start_0
    sget-object v0, LX/0oT;->a:LX/00u;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 141590
    if-eqz v2, :cond_0

    .line 141591
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 141592
    const-class v3, Landroid/content/Context;

    const-class p0, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v3, p0}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object p0

    check-cast p0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v3, p0}, LX/0oU;->a(Landroid/content/Context;Lcom/facebook/quicklog/QuickPerformanceLogger;)LX/00u;

    move-result-object v3

    move-object v0, v3

    .line 141593
    sput-object v0, LX/0oT;->a:LX/00u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 141594
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 141595
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 141596
    :cond_1
    sget-object v0, LX/0oT;->a:LX/00u;

    return-object v0

    .line 141597
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 141598
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 141599
    const-class v0, Landroid/content/Context;

    const-class v1, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0, v1}, LX/0oU;->a(Landroid/content/Context;Lcom/facebook/quicklog/QuickPerformanceLogger;)LX/00u;

    move-result-object v0

    return-object v0
.end method
