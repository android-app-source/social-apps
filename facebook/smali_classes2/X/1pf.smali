.class public LX/1pf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "LX/1pg;",
            ">;"
        }
    .end annotation
.end field

.field private b:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 329591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 329592
    invoke-static {}, LX/0PM;->f()Ljava/util/TreeMap;

    move-result-object v0

    iput-object v0, p0, LX/1pf;->a:Ljava/util/TreeMap;

    .line 329593
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(J)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x1

    .line 329594
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1pf;->a:Ljava/util/TreeMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pg;

    .line 329595
    if-nez v0, :cond_0

    .line 329596
    new-instance v0, LX/1pg;

    invoke-direct {v0}, LX/1pg;-><init>()V

    .line 329597
    iget-object v1, p0, LX/1pf;->a:Ljava/util/TreeMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329598
    :cond_0
    iget-wide v2, v0, LX/1pg;->a:J

    add-long/2addr v2, v4

    iput-wide v2, v0, LX/1pg;->a:J

    .line 329599
    iget-wide v0, p0, LX/1pf;->b:J

    add-long/2addr v0, v4

    iput-wide v0, p0, LX/1pf;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 329600
    monitor-exit p0

    return-void

    .line 329601
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
