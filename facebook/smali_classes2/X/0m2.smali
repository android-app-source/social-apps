.class public final LX/0m2;
.super LX/0m3;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0m3",
        "<",
        "LX/0mt;",
        "LX/0m2;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x7ace4b0d59bb8a49L


# instance fields
.field public final _filterProvider:LX/4rN;

.field public final _serFeatures:I

.field public _serializationInclusion:LX/0nr;


# direct methods
.method public constructor <init>(LX/0lh;LX/0m0;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lh;",
            "LX/0m0;",
            "Ljava/util/Map",
            "<",
            "LX/1Xc;",
            "Ljava/lang/Class",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 131604
    invoke-direct {p0, p1, p2, p3}, LX/0m3;-><init>(LX/0lh;LX/0m0;Ljava/util/Map;)V

    .line 131605
    iput-object v1, p0, LX/0m2;->_serializationInclusion:LX/0nr;

    .line 131606
    const-class v0, LX/0mt;

    invoke-static {v0}, LX/0m4;->a(Ljava/lang/Class;)I

    move-result v0

    iput v0, p0, LX/0m2;->_serFeatures:I

    .line 131607
    iput-object v1, p0, LX/0m2;->_filterProvider:LX/4rN;

    .line 131608
    return-void
.end method

.method private constructor <init>(LX/0m2;II)V
    .locals 1

    .prologue
    .line 131598
    invoke-direct {p0, p1, p2}, LX/0m3;-><init>(LX/0m3;I)V

    .line 131599
    const/4 v0, 0x0

    iput-object v0, p0, LX/0m2;->_serializationInclusion:LX/0nr;

    .line 131600
    iput p3, p0, LX/0m2;->_serFeatures:I

    .line 131601
    iget-object v0, p1, LX/0m2;->_serializationInclusion:LX/0nr;

    iput-object v0, p0, LX/0m2;->_serializationInclusion:LX/0nr;

    .line 131602
    iget-object v0, p1, LX/0m2;->_filterProvider:LX/4rN;

    iput-object v0, p0, LX/0m2;->_filterProvider:LX/4rN;

    .line 131603
    return-void
.end method

.method private constructor <init>(LX/0m2;LX/0lh;)V
    .locals 1

    .prologue
    .line 131592
    invoke-direct {p0, p1, p2}, LX/0m3;-><init>(LX/0m3;LX/0lh;)V

    .line 131593
    const/4 v0, 0x0

    iput-object v0, p0, LX/0m2;->_serializationInclusion:LX/0nr;

    .line 131594
    iget v0, p1, LX/0m2;->_serFeatures:I

    iput v0, p0, LX/0m2;->_serFeatures:I

    .line 131595
    iget-object v0, p1, LX/0m2;->_serializationInclusion:LX/0nr;

    iput-object v0, p0, LX/0m2;->_serializationInclusion:LX/0nr;

    .line 131596
    iget-object v0, p1, LX/0m2;->_filterProvider:LX/4rN;

    iput-object v0, p0, LX/0m2;->_filterProvider:LX/4rN;

    .line 131597
    return-void
.end method

.method private constructor <init>(LX/0m2;LX/0nr;)V
    .locals 1

    .prologue
    .line 131586
    invoke-direct {p0, p1}, LX/0m3;-><init>(LX/0m3;)V

    .line 131587
    const/4 v0, 0x0

    iput-object v0, p0, LX/0m2;->_serializationInclusion:LX/0nr;

    .line 131588
    iget v0, p1, LX/0m2;->_serFeatures:I

    iput v0, p0, LX/0m2;->_serFeatures:I

    .line 131589
    iput-object p2, p0, LX/0m2;->_serializationInclusion:LX/0nr;

    .line 131590
    iget-object v0, p1, LX/0m2;->_filterProvider:LX/4rN;

    iput-object v0, p0, LX/0m2;->_filterProvider:LX/4rN;

    .line 131591
    return-void
.end method

.method private final a(LX/0lh;)LX/0m2;
    .locals 1

    .prologue
    .line 131585
    iget-object v0, p0, LX/0m4;->_base:LX/0lh;

    if-ne v0, p1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/0m2;

    invoke-direct {v0, p0, p1}, LX/0m2;-><init>(LX/0m2;LX/0lh;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0lJ;)LX/0lS;
    .locals 1

    .prologue
    .line 131584
    invoke-virtual {p0}, LX/0m4;->j()LX/0lM;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p0}, LX/0lM;->b(LX/0m4;LX/0lJ;LX/0m5;)LX/0lS;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/0lU;
    .locals 1

    .prologue
    .line 131558
    sget-object v0, LX/0m6;->USE_ANNOTATIONS:LX/0m6;

    invoke-virtual {p0, v0}, LX/0m4;->a(LX/0m6;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131559
    invoke-super {p0}, LX/0m3;->a()LX/0lU;

    move-result-object v0

    .line 131560
    :goto_0
    return-object v0

    .line 131561
    :cond_0
    sget-object v0, LX/4qr;->a:LX/4qr;

    move-object v0, v0

    .line 131562
    goto :goto_0
.end method

.method public final a(LX/0li;)LX/0m2;
    .locals 1

    .prologue
    .line 131583
    iget-object v0, p0, LX/0m4;->_base:LX/0lh;

    invoke-virtual {v0, p1}, LX/0lh;->a(LX/0li;)LX/0lh;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0m2;->a(LX/0lh;)LX/0m2;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0mt;)LX/0m2;
    .locals 3

    .prologue
    .line 131581
    iget v0, p0, LX/0m2;->_serFeatures:I

    invoke-virtual {p1}, LX/0mt;->getMask()I

    move-result v1

    or-int/2addr v1, v0

    .line 131582
    iget v0, p0, LX/0m2;->_serFeatures:I

    if-ne v1, v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/0m2;

    iget v2, p0, LX/0m4;->_mapperFeatures:I

    invoke-direct {v0, p0, v2, v1}, LX/0m2;-><init>(LX/0m2;II)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/0np;LX/0lX;)LX/0m2;
    .locals 1

    .prologue
    .line 131580
    iget-object v0, p0, LX/0m4;->_base:LX/0lh;

    invoke-virtual {v0, p1, p2}, LX/0lh;->a(LX/0np;LX/0lX;)LX/0lh;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0m2;->a(LX/0lh;)LX/0m2;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0nr;)LX/0m2;
    .locals 1

    .prologue
    .line 131579
    iget-object v0, p0, LX/0m2;->_serializationInclusion:LX/0nr;

    if-ne v0, p1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/0m2;

    invoke-direct {v0, p0, p1}, LX/0m2;-><init>(LX/0m2;LX/0nr;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/0lJ;)LX/0lS;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/0lS;",
            ">(",
            "LX/0lJ;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 131578
    invoke-virtual {p0}, LX/0m4;->j()LX/0lM;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p0}, LX/0lM;->b(LX/0m2;LX/0lJ;LX/0m5;)LX/0lS;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/0mt;)LX/0m2;
    .locals 3

    .prologue
    .line 131576
    iget v0, p0, LX/0m2;->_serFeatures:I

    invoke-virtual {p1}, LX/0mt;->getMask()I

    move-result v1

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v1, v0

    .line 131577
    iget v0, p0, LX/0m2;->_serFeatures:I

    if-ne v1, v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/0m2;

    iget v2, p0, LX/0m4;->_mapperFeatures:I

    invoke-direct {v0, p0, v2, v1}, LX/0m2;-><init>(LX/0m2;II)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final b()LX/0nr;
    .locals 1

    .prologue
    .line 131573
    iget-object v0, p0, LX/0m2;->_serializationInclusion:LX/0nr;

    if-eqz v0, :cond_0

    .line 131574
    iget-object v0, p0, LX/0m2;->_serializationInclusion:LX/0nr;

    .line 131575
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/0nr;->ALWAYS:LX/0nr;

    goto :goto_0
.end method

.method public final c()LX/0lW;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0lW",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 131565
    invoke-super {p0}, LX/0m3;->c()LX/0lW;

    move-result-object v0

    .line 131566
    sget-object v1, LX/0m6;->AUTO_DETECT_GETTERS:LX/0m6;

    invoke-virtual {p0, v1}, LX/0m4;->a(LX/0m6;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 131567
    sget-object v1, LX/0lX;->NONE:LX/0lX;

    invoke-interface {v0, v1}, LX/0lW;->a(LX/0lX;)LX/0lW;

    move-result-object v0

    .line 131568
    :cond_0
    sget-object v1, LX/0m6;->AUTO_DETECT_IS_GETTERS:LX/0m6;

    invoke-virtual {p0, v1}, LX/0m4;->a(LX/0m6;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 131569
    sget-object v1, LX/0lX;->NONE:LX/0lX;

    invoke-interface {v0, v1}, LX/0lW;->b(LX/0lX;)LX/0lW;

    move-result-object v0

    .line 131570
    :cond_1
    sget-object v1, LX/0m6;->AUTO_DETECT_FIELDS:LX/0m6;

    invoke-virtual {p0, v1}, LX/0m4;->a(LX/0m6;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 131571
    sget-object v1, LX/0lX;->NONE:LX/0lX;

    invoke-interface {v0, v1}, LX/0lW;->e(LX/0lX;)LX/0lW;

    move-result-object v0

    .line 131572
    :cond_2
    return-object v0
.end method

.method public final c(LX/0mt;)Z
    .locals 2

    .prologue
    .line 131564
    iget v0, p0, LX/0m2;->_serFeatures:I

    invoke-virtual {p1}, LX/0mt;->getMask()I

    move-result v1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 131563
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[SerializationConfig: flags=0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/0m2;->_serFeatures:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
