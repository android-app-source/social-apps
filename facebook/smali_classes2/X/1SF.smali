.class public LX/1SF;
.super LX/1SG;
.source ""

# interfaces
.implements LX/1SH;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1SG",
        "<",
        "Ljava/lang/Boolean;",
        ">;",
        "LX/1SH;"
    }
.end annotation


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:LX/1SI;

.field public final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Ot;LX/1SI;LX/0ad;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1SP;",
            ">;",
            "LX/1SI;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 247820
    invoke-direct {p0}, LX/1SG;-><init>()V

    .line 247821
    iput-object p2, p0, LX/1SF;->b:LX/1SI;

    .line 247822
    iput-object p3, p0, LX/1SF;->c:LX/0ad;

    .line 247823
    new-instance v0, LX/1SJ;

    invoke-direct {v0, p0}, LX/1SJ;-><init>(LX/1SF;)V

    .line 247824
    sget-object v1, LX/131;->INSTANCE:LX/131;

    move-object v1, v1

    .line 247825
    invoke-static {p1, v0, v1}, LX/1SK;->a(LX/0Ot;LX/0QK;Ljava/util/concurrent/Executor;)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/1SF;->a:LX/0Ot;

    .line 247826
    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 247827
    invoke-virtual {p0}, LX/1SF;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 247828
    iget-object v0, p0, LX/1SF;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 247829
    invoke-virtual {p0}, LX/1SF;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
