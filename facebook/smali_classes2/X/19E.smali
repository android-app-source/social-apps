.class public LX/19E;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/19F;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public final a:LX/19D;

.field public final b:Landroid/view/Choreographer;

.field public final c:Landroid/view/Choreographer$FrameCallback;

.field public d:J

.field public e:J

.field private f:Z


# direct methods
.method public constructor <init>(LX/19D;Landroid/view/Choreographer;)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 207475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207476
    iput-wide v0, p0, LX/19E;->d:J

    .line 207477
    iput-wide v0, p0, LX/19E;->e:J

    .line 207478
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/19E;->f:Z

    .line 207479
    iput-object p1, p0, LX/19E;->a:LX/19D;

    .line 207480
    iput-object p2, p0, LX/19E;->b:Landroid/view/Choreographer;

    .line 207481
    new-instance v0, LX/19G;

    invoke-direct {v0, p0}, LX/19G;-><init>(LX/19E;)V

    iput-object v0, p0, LX/19E;->c:Landroid/view/Choreographer$FrameCallback;

    .line 207482
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 207486
    if-eqz p1, :cond_0

    iget-boolean v0, p0, LX/19E;->f:Z

    if-nez v0, :cond_0

    .line 207487
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/19E;->d:J

    .line 207488
    :cond_0
    iput-boolean p1, p0, LX/19E;->f:Z

    .line 207489
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 207490
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/19E;->a(Z)V

    .line 207491
    iget-object v0, p0, LX/19E;->b:Landroid/view/Choreographer;

    iget-object v1, p0, LX/19E;->c:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 207492
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 207483
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/19E;->a(Z)V

    .line 207484
    iget-object v0, p0, LX/19E;->b:Landroid/view/Choreographer;

    iget-object v1, p0, LX/19E;->c:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->removeFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 207485
    return-void
.end method
