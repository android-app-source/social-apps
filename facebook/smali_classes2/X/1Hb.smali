.class public LX/1Hb;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/nio/charset/Charset;

.field private static final b:Ljava/nio/charset/Charset;


# instance fields
.field public final c:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 227240
    const-string v0, "UTF-16"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, LX/1Hb;->a:Ljava/nio/charset/Charset;

    .line 227241
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, LX/1Hb;->b:Ljava/nio/charset/Charset;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 227242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227243
    sget-object v0, LX/1Hb;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    iput-object v0, p0, LX/1Hb;->c:[B

    .line 227244
    return-void
.end method

.method private constructor <init>([B)V
    .locals 0

    .prologue
    .line 227245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227246
    iput-object p1, p0, LX/1Hb;->c:[B

    .line 227247
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/1Hb;
    .locals 2

    .prologue
    .line 227248
    new-instance v0, LX/1Hb;

    sget-object v1, LX/1Hb;->b:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Hb;-><init>([B)V

    return-object v0
.end method
