.class public LX/1mM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile sInstance__com_facebook_fbservice_service_BlueServiceQueueManager__INJECTED_BY_TemplateInjector:LX/1mM;


# instance fields
.field private final context:Landroid/content/Context;

.field private lameDuckMode:Z

.field private final startedQueues:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1qD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 313692
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313693
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, LX/1mM;->startedQueues:Ljava/util/Set;

    .line 313694
    iput-object p1, p0, LX/1mM;->context:Landroid/content/Context;

    .line 313695
    return-void
.end method

.method public static getInstance__com_facebook_fbservice_service_BlueServiceQueueManager__INJECTED_BY_TemplateInjector(LX/0QB;)LX/1mM;
    .locals 4

    .prologue
    .line 313696
    sget-object v0, LX/1mM;->sInstance__com_facebook_fbservice_service_BlueServiceQueueManager__INJECTED_BY_TemplateInjector:LX/1mM;

    if-nez v0, :cond_1

    .line 313697
    const-class v1, LX/1mM;

    monitor-enter v1

    .line 313698
    :try_start_0
    sget-object v0, LX/1mM;->sInstance__com_facebook_fbservice_service_BlueServiceQueueManager__INJECTED_BY_TemplateInjector:LX/1mM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 313699
    if-eqz v2, :cond_0

    .line 313700
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 313701
    new-instance p0, LX/1mM;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/1mM;-><init>(Landroid/content/Context;)V

    .line 313702
    move-object v0, p0

    .line 313703
    sput-object v0, LX/1mM;->sInstance__com_facebook_fbservice_service_BlueServiceQueueManager__INJECTED_BY_TemplateInjector:LX/1mM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 313704
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 313705
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 313706
    :cond_1
    sget-object v0, LX/1mM;->sInstance__com_facebook_fbservice_service_BlueServiceQueueManager__INJECTED_BY_TemplateInjector:LX/1mM;

    return-object v0

    .line 313707
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 313708
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static queueShouldBeIgnored(LX/1qD;)Z
    .locals 2

    .prologue
    .line 313709
    iget-object v0, p0, LX/1qD;->mQueueName:Ljava/lang/Class;

    move-object v0, v0

    .line 313710
    const-class v1, Lcom/facebook/fbservice/service/AuthQueue;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized blockUntilAllQueuesDrained()V
    .locals 1

    .prologue
    .line 313711
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1mM;->lameDuckMode:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 313712
    :goto_0
    iget-object v0, p0, LX/1mM;->startedQueues:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 313713
    iget-object v0, p0, LX/1mM;->startedQueues:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313714
    const v0, 0x55476f0c

    :try_start_1
    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 313715
    :catch_0
    goto :goto_0

    .line 313716
    :cond_0
    monitor-exit p0

    return-void

    .line 313717
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized enterLameDuckMode()V
    .locals 3

    .prologue
    .line 313718
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/1mM;->lameDuckMode:Z

    .line 313719
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/1mM;->context:Landroid/content/Context;

    const-class v2, Lcom/facebook/fbservice/service/DefaultBlueService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 313720
    const-string v1, "Orca.DRAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 313721
    iget-object v1, p0, LX/1mM;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313722
    monitor-exit p0

    return-void

    .line 313723
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized exitLameDuckMode()V
    .locals 1

    .prologue
    .line 313724
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/1mM;->lameDuckMode:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313725
    monitor-exit p0

    return-void

    .line 313726
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isInLameDuckMode()Z
    .locals 1

    .prologue
    .line 313727
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1mM;->lameDuckMode:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onServiceQueueStarted(LX/1qD;)V
    .locals 1

    .prologue
    .line 313728
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/1mM;->queueShouldBeIgnored(LX/1qD;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 313729
    :goto_0
    monitor-exit p0

    return-void

    .line 313730
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1mM;->startedQueues:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 313731
    const v0, -0x155c9c79

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 313732
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onServiceQueueStopped(LX/1qD;)V
    .locals 5

    .prologue
    .line 313733
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/1mM;->queueShouldBeIgnored(LX/1qD;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 313734
    :goto_0
    monitor-exit p0

    return-void

    .line 313735
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1mM;->startedQueues:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 313736
    if-nez v0, :cond_1

    .line 313737
    const-string v0, "BlueServiceQueueManager"

    const-string v1, "Unknown queue [%s]"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 313738
    iget-object v4, p1, LX/1qD;->mQueueName:Ljava/lang/Class;

    move-object v4, v4

    .line 313739
    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 313740
    :cond_1
    const v0, -0x199eef0f

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 313741
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
