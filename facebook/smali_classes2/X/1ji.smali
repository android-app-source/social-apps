.class public LX/1ji;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1ji;


# instance fields
.field public final a:LX/1Da;

.field public final b:LX/0Zb;

.field public c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/1kA;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Da;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 300796
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 300797
    iput-object p1, p0, LX/1ji;->a:LX/1Da;

    .line 300798
    iput-object p2, p0, LX/1ji;->b:LX/0Zb;

    .line 300799
    return-void
.end method

.method public static a(LX/0QB;)LX/1ji;
    .locals 5

    .prologue
    .line 300800
    sget-object v0, LX/1ji;->d:LX/1ji;

    if-nez v0, :cond_1

    .line 300801
    const-class v1, LX/1ji;

    monitor-enter v1

    .line 300802
    :try_start_0
    sget-object v0, LX/1ji;->d:LX/1ji;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 300803
    if-eqz v2, :cond_0

    .line 300804
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 300805
    new-instance p0, LX/1ji;

    invoke-static {v0}, LX/1Da;->b(LX/0QB;)LX/1Da;

    move-result-object v3

    check-cast v3, LX/1Da;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {p0, v3, v4}, LX/1ji;-><init>(LX/1Da;LX/0Zb;)V

    .line 300806
    move-object v0, p0

    .line 300807
    sput-object v0, LX/1ji;->d:LX/1ji;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 300808
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 300809
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 300810
    :cond_1
    sget-object v0, LX/1ji;->d:LX/1ji;

    return-object v0

    .line 300811
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 300812
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1kA;
    .locals 1

    .prologue
    .line 300813
    iget-object v0, p0, LX/1ji;->c:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    .line 300814
    const/4 v0, 0x0

    .line 300815
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1ji;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1kA;

    goto :goto_0
.end method
