.class public final enum LX/0m6;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/0m7;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0m6;",
        ">;",
        "LX/0m7;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0m6;

.field public static final enum ALLOW_FINAL_FIELDS_AS_MUTATORS:LX/0m6;

.field public static final enum AUTO_DETECT_CREATORS:LX/0m6;

.field public static final enum AUTO_DETECT_FIELDS:LX/0m6;

.field public static final enum AUTO_DETECT_GETTERS:LX/0m6;

.field public static final enum AUTO_DETECT_IS_GETTERS:LX/0m6;

.field public static final enum AUTO_DETECT_SETTERS:LX/0m6;

.field public static final enum CAN_OVERRIDE_ACCESS_MODIFIERS:LX/0m6;

.field public static final enum DEFAULT_VIEW_INCLUSION:LX/0m6;

.field public static final enum INFER_PROPERTY_MUTATORS:LX/0m6;

.field public static final enum REQUIRE_SETTERS_FOR_GETTERS:LX/0m6;

.field public static final enum SORT_PROPERTIES_ALPHABETICALLY:LX/0m6;

.field public static final enum USE_ANNOTATIONS:LX/0m6;

.field public static final enum USE_GETTERS_AS_SETTERS:LX/0m6;

.field public static final enum USE_STATIC_TYPING:LX/0m6;

.field public static final enum USE_WRAPPER_NAME_AS_PROPERTY_NAME:LX/0m6;


# instance fields
.field private final _defaultState:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 131704
    new-instance v0, LX/0m6;

    const-string v1, "USE_ANNOTATIONS"

    invoke-direct {v0, v1, v4, v3}, LX/0m6;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0m6;->USE_ANNOTATIONS:LX/0m6;

    .line 131705
    new-instance v0, LX/0m6;

    const-string v1, "AUTO_DETECT_CREATORS"

    invoke-direct {v0, v1, v3, v3}, LX/0m6;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0m6;->AUTO_DETECT_CREATORS:LX/0m6;

    .line 131706
    new-instance v0, LX/0m6;

    const-string v1, "AUTO_DETECT_FIELDS"

    invoke-direct {v0, v1, v5, v3}, LX/0m6;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0m6;->AUTO_DETECT_FIELDS:LX/0m6;

    .line 131707
    new-instance v0, LX/0m6;

    const-string v1, "AUTO_DETECT_GETTERS"

    invoke-direct {v0, v1, v6, v3}, LX/0m6;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0m6;->AUTO_DETECT_GETTERS:LX/0m6;

    .line 131708
    new-instance v0, LX/0m6;

    const-string v1, "AUTO_DETECT_IS_GETTERS"

    invoke-direct {v0, v1, v7, v3}, LX/0m6;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0m6;->AUTO_DETECT_IS_GETTERS:LX/0m6;

    .line 131709
    new-instance v0, LX/0m6;

    const-string v1, "AUTO_DETECT_SETTERS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/0m6;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0m6;->AUTO_DETECT_SETTERS:LX/0m6;

    .line 131710
    new-instance v0, LX/0m6;

    const-string v1, "REQUIRE_SETTERS_FOR_GETTERS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v4}, LX/0m6;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0m6;->REQUIRE_SETTERS_FOR_GETTERS:LX/0m6;

    .line 131711
    new-instance v0, LX/0m6;

    const-string v1, "USE_GETTERS_AS_SETTERS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/0m6;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0m6;->USE_GETTERS_AS_SETTERS:LX/0m6;

    .line 131712
    new-instance v0, LX/0m6;

    const-string v1, "CAN_OVERRIDE_ACCESS_MODIFIERS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v3}, LX/0m6;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0m6;->CAN_OVERRIDE_ACCESS_MODIFIERS:LX/0m6;

    .line 131713
    new-instance v0, LX/0m6;

    const-string v1, "INFER_PROPERTY_MUTATORS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v3}, LX/0m6;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0m6;->INFER_PROPERTY_MUTATORS:LX/0m6;

    .line 131714
    new-instance v0, LX/0m6;

    const-string v1, "ALLOW_FINAL_FIELDS_AS_MUTATORS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v3}, LX/0m6;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0m6;->ALLOW_FINAL_FIELDS_AS_MUTATORS:LX/0m6;

    .line 131715
    new-instance v0, LX/0m6;

    const-string v1, "USE_STATIC_TYPING"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v4}, LX/0m6;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0m6;->USE_STATIC_TYPING:LX/0m6;

    .line 131716
    new-instance v0, LX/0m6;

    const-string v1, "DEFAULT_VIEW_INCLUSION"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v3}, LX/0m6;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0m6;->DEFAULT_VIEW_INCLUSION:LX/0m6;

    .line 131717
    new-instance v0, LX/0m6;

    const-string v1, "SORT_PROPERTIES_ALPHABETICALLY"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2, v4}, LX/0m6;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0m6;->SORT_PROPERTIES_ALPHABETICALLY:LX/0m6;

    .line 131718
    new-instance v0, LX/0m6;

    const-string v1, "USE_WRAPPER_NAME_AS_PROPERTY_NAME"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2, v4}, LX/0m6;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0m6;->USE_WRAPPER_NAME_AS_PROPERTY_NAME:LX/0m6;

    .line 131719
    const/16 v0, 0xf

    new-array v0, v0, [LX/0m6;

    sget-object v1, LX/0m6;->USE_ANNOTATIONS:LX/0m6;

    aput-object v1, v0, v4

    sget-object v1, LX/0m6;->AUTO_DETECT_CREATORS:LX/0m6;

    aput-object v1, v0, v3

    sget-object v1, LX/0m6;->AUTO_DETECT_FIELDS:LX/0m6;

    aput-object v1, v0, v5

    sget-object v1, LX/0m6;->AUTO_DETECT_GETTERS:LX/0m6;

    aput-object v1, v0, v6

    sget-object v1, LX/0m6;->AUTO_DETECT_IS_GETTERS:LX/0m6;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0m6;->AUTO_DETECT_SETTERS:LX/0m6;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0m6;->REQUIRE_SETTERS_FOR_GETTERS:LX/0m6;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0m6;->USE_GETTERS_AS_SETTERS:LX/0m6;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0m6;->CAN_OVERRIDE_ACCESS_MODIFIERS:LX/0m6;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0m6;->INFER_PROPERTY_MUTATORS:LX/0m6;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0m6;->ALLOW_FINAL_FIELDS_AS_MUTATORS:LX/0m6;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0m6;->USE_STATIC_TYPING:LX/0m6;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/0m6;->DEFAULT_VIEW_INCLUSION:LX/0m6;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/0m6;->SORT_PROPERTIES_ALPHABETICALLY:LX/0m6;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/0m6;->USE_WRAPPER_NAME_AS_PROPERTY_NAME:LX/0m6;

    aput-object v2, v0, v1

    sput-object v0, LX/0m6;->$VALUES:[LX/0m6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 131701
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 131702
    iput-boolean p3, p0, LX/0m6;->_defaultState:Z

    .line 131703
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0m6;
    .locals 1

    .prologue
    .line 131720
    const-class v0, LX/0m6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0m6;

    return-object v0
.end method

.method public static values()[LX/0m6;
    .locals 1

    .prologue
    .line 131700
    sget-object v0, LX/0m6;->$VALUES:[LX/0m6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0m6;

    return-object v0
.end method


# virtual methods
.method public final enabledByDefault()Z
    .locals 1

    .prologue
    .line 131698
    iget-boolean v0, p0, LX/0m6;->_defaultState:Z

    return v0
.end method

.method public final getMask()I
    .locals 2

    .prologue
    .line 131699
    const/4 v0, 0x1

    invoke-virtual {p0}, LX/0m6;->ordinal()I

    move-result v1

    shl-int/2addr v0, v1

    return v0
.end method
