.class public LX/1Ey;
.super LX/0Vx;
.source ""

# interfaces
.implements LX/1Ez;


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private final b:LX/0SG;

.field private final c:Ljava/lang/String;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0lF;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private f:J

.field private g:LX/1FR;


# direct methods
.method public constructor <init>(LX/2WA;LX/0SG;Ljava/lang/String;)V
    .locals 1
    .param p1    # LX/2WA;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 221474
    invoke-direct {p0, p1}, LX/0Vx;-><init>(LX/2WA;)V

    .line 221475
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    iput-object v0, p0, LX/1Ey;->b:LX/0SG;

    .line 221476
    invoke-static {p3}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/1Ey;->c:Ljava/lang/String;

    .line 221477
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/1Ey;->d:Ljava/util/List;

    .line 221478
    return-void
.end method

.method private e()V
    .locals 6

    .prologue
    .line 221499
    iget-object v0, p0, LX/1Ey;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/1Ey;->f:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 221500
    :goto_0
    return-void

    .line 221501
    :cond_0
    iget-object v0, p0, LX/1Ey;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 221502
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {p0, v1, v4, v5}, LX/0Vx;->b(Ljava/lang/String;J)V

    goto :goto_1

    .line 221503
    :cond_1
    iget-object v0, p0, LX/1Ey;->d:Ljava/util/List;

    invoke-virtual {p0}, LX/0Vx;->c()LX/0lF;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221504
    invoke-direct {p0}, LX/1Ey;->f()V

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 221505
    invoke-virtual {p0}, LX/0Vx;->W_()V

    .line 221506
    iget-object v0, p0, LX/1Ey;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/1Ey;->f:J

    .line 221507
    iget-object v0, p0, LX/1Ey;->g:LX/1FR;

    invoke-virtual {v0}, LX/1FR;->b()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/1Ey;->e:Ljava/util/Map;

    .line 221508
    return-void
.end method


# virtual methods
.method public final V_()V
    .locals 4

    .prologue
    .line 221510
    invoke-direct {p0}, LX/1Ey;->e()V

    .line 221511
    const-string v0, "hard_cap_exceeded"

    const-wide/16 v2, 0x1

    invoke-virtual {p0, v0, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 221512
    return-void
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221509
    iget-object v0, p0, LX/1Ey;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 221493
    invoke-direct {p0}, LX/1Ey;->e()V

    .line 221494
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "value_reuse_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, 0x1

    invoke-virtual {p0, v0, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 221495
    return-void
.end method

.method public final a(LX/1FR;)V
    .locals 0

    .prologue
    .line 221496
    iput-object p1, p0, LX/1Ey;->g:LX/1FR;

    .line 221497
    invoke-direct {p0}, LX/1Ey;->f()V

    .line 221498
    return-void
.end method

.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 221489
    iget-object v0, p0, LX/1Ey;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 221490
    invoke-virtual {p0}, LX/1Ey;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/1Ey;->d:Ljava/util/List;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 221491
    iget-object v0, p0, LX/1Ey;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 221492
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 221486
    invoke-direct {p0}, LX/1Ey;->e()V

    .line 221487
    const-string v0, "soft_cap_exceeded"

    const-wide/16 v2, 0x1

    invoke-virtual {p0, v0, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 221488
    return-void
.end method

.method public final b(I)V
    .locals 4

    .prologue
    .line 221483
    invoke-direct {p0}, LX/1Ey;->e()V

    .line 221484
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pool_alloc_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, 0x1

    invoke-virtual {p0, v0, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 221485
    return-void
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 221481
    invoke-direct {p0}, LX/1Ey;->e()V

    .line 221482
    return-void
.end method

.method public final d(I)V
    .locals 0

    .prologue
    .line 221479
    invoke-direct {p0}, LX/1Ey;->e()V

    .line 221480
    return-void
.end method
