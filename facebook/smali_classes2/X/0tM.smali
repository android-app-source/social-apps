.class public LX/0tM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0tM;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 154447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154448
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 154449
    iput-object v0, p0, LX/0tM;->a:LX/0Ot;

    .line 154450
    return-void
.end method

.method public static a(LX/0QB;)LX/0tM;
    .locals 4

    .prologue
    .line 154451
    sget-object v0, LX/0tM;->d:LX/0tM;

    if-nez v0, :cond_1

    .line 154452
    const-class v1, LX/0tM;

    monitor-enter v1

    .line 154453
    :try_start_0
    sget-object v0, LX/0tM;->d:LX/0tM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 154454
    if-eqz v2, :cond_0

    .line 154455
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 154456
    new-instance v3, LX/0tM;

    invoke-direct {v3}, LX/0tM;-><init>()V

    .line 154457
    const/16 p0, 0x1032

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 154458
    iput-object p0, v3, LX/0tM;->a:LX/0Ot;

    .line 154459
    move-object v0, v3

    .line 154460
    sput-object v0, LX/0tM;->d:LX/0tM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154461
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 154462
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 154463
    :cond_1
    sget-object v0, LX/0tM;->d:LX/0tM;

    return-object v0

    .line 154464
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 154465
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 154466
    iget-object v0, p0, LX/0tM;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 154467
    iget-object v0, p0, LX/0tM;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/0wj;->g:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/0tM;->b:Ljava/lang/Boolean;

    .line 154468
    :cond_0
    iget-object v0, p0, LX/0tM;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 154469
    iget-object v0, p0, LX/0tM;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 154470
    iget-object v0, p0, LX/0tM;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/0wj;->h:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/0tM;->c:Ljava/lang/Boolean;

    .line 154471
    :cond_0
    iget-object v0, p0, LX/0tM;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
