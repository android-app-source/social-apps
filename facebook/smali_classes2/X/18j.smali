.class public abstract enum LX/18j;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/18j;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/18j;

.field public static final enum COLLECTED:LX/18j;

.field public static final enum EXPIRED:LX/18j;

.field public static final enum EXPLICIT:LX/18j;

.field public static final enum REPLACED:LX/18j;

.field public static final enum SIZE:LX/18j;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 206985
    new-instance v0, LX/18k;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1, v2}, LX/18k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/18j;->EXPLICIT:LX/18j;

    .line 206986
    new-instance v0, LX/18l;

    const-string v1, "REPLACED"

    invoke-direct {v0, v1, v3}, LX/18l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/18j;->REPLACED:LX/18j;

    .line 206987
    new-instance v0, LX/18m;

    const-string v1, "COLLECTED"

    invoke-direct {v0, v1, v4}, LX/18m;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/18j;->COLLECTED:LX/18j;

    .line 206988
    new-instance v0, LX/18n;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v5}, LX/18n;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/18j;->EXPIRED:LX/18j;

    .line 206989
    new-instance v0, LX/18o;

    const-string v1, "SIZE"

    invoke-direct {v0, v1, v6}, LX/18o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/18j;->SIZE:LX/18j;

    .line 206990
    const/4 v0, 0x5

    new-array v0, v0, [LX/18j;

    sget-object v1, LX/18j;->EXPLICIT:LX/18j;

    aput-object v1, v0, v2

    sget-object v1, LX/18j;->REPLACED:LX/18j;

    aput-object v1, v0, v3

    sget-object v1, LX/18j;->COLLECTED:LX/18j;

    aput-object v1, v0, v4

    sget-object v1, LX/18j;->EXPIRED:LX/18j;

    aput-object v1, v0, v5

    sget-object v1, LX/18j;->SIZE:LX/18j;

    aput-object v1, v0, v6

    sput-object v0, LX/18j;->$VALUES:[LX/18j;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 206991
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/18j;
    .locals 1

    .prologue
    .line 206992
    const-class v0, LX/18j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/18j;

    return-object v0
.end method

.method public static values()[LX/18j;
    .locals 1

    .prologue
    .line 206993
    sget-object v0, LX/18j;->$VALUES:[LX/18j;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/18j;

    return-object v0
.end method


# virtual methods
.method public abstract wasEvicted()Z
.end method
