.class public LX/0Sk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sl;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/0Sk;


# instance fields
.field public final a:LX/0So;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Sl;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:LX/0XB;

.field private d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0Sl;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/0Sl;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private volatile f:Z

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/0XB;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0So;LX/0Ot;)V
    .locals 0
    .param p2    # LX/0Ot;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0So;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Sl;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 62188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62189
    iput-object p1, p0, LX/0Sk;->a:LX/0So;

    .line 62190
    iput-object p2, p0, LX/0Sk;->b:LX/0Ot;

    .line 62191
    return-void
.end method

.method public static a(LX/0QB;)LX/0Sk;
    .locals 6

    .prologue
    .line 62172
    sget-object v0, LX/0Sk;->h:LX/0Sk;

    if-nez v0, :cond_1

    .line 62173
    const-class v1, LX/0Sk;

    monitor-enter v1

    .line 62174
    :try_start_0
    sget-object v0, LX/0Sk;->h:LX/0Sk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 62175
    if-eqz v2, :cond_0

    .line 62176
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 62177
    new-instance v4, LX/0Sk;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-interface {v0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v5

    .line 62178
    new-instance p0, LX/0Sp;

    invoke-interface {v5}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0Sp;-><init>(LX/0QB;)V

    move-object p0, p0

    .line 62179
    invoke-interface {v5}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v0

    invoke-static {p0, v0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object p0

    move-object v5, p0

    .line 62180
    invoke-direct {v4, v3, v5}, LX/0Sk;-><init>(LX/0So;LX/0Ot;)V

    .line 62181
    move-object v0, v4

    .line 62182
    sput-object v0, LX/0Sk;->h:LX/0Sk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62183
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 62184
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 62185
    :cond_1
    sget-object v0, LX/0Sk;->h:LX/0Sk;

    return-object v0

    .line 62186
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 62187
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized g()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0Sl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62162
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Sk;->d:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 62163
    iget-object v0, p0, LX/0Sk;->d:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62164
    :goto_0
    monitor-exit p0

    return-object v0

    .line 62165
    :cond_0
    :try_start_1
    new-instance v0, LX/0XA;

    invoke-direct {v0, p0}, LX/0XA;-><init>(LX/0Sk;)V

    iput-object v0, p0, LX/0Sk;->c:LX/0XB;

    .line 62166
    iget-object v0, p0, LX/0Sk;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 62167
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Sl;

    .line 62168
    iget-object v3, p0, LX/0Sk;->c:LX/0XB;

    invoke-interface {v1, v3}, LX/0Sl;->a(LX/0XB;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 62169
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 62170
    :cond_1
    :try_start_2
    iput-object v0, p0, LX/0Sk;->d:Ljava/util/Set;

    .line 62171
    iget-object v0, p0, LX/0Sk;->d:Ljava/util/Set;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private declared-synchronized h()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "LX/0Sl;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 62149
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0Sk;->f:Z

    if-eqz v0, :cond_0

    .line 62150
    iget-object v0, p0, LX/0Sk;->e:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62151
    :goto_0
    monitor-exit p0

    return-object v0

    .line 62152
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, LX/0Sk;->e:Ljava/util/ArrayList;

    .line 62153
    invoke-direct {p0}, LX/0Sk;->g()Ljava/util/Set;

    move-result-object v1

    .line 62154
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sl;

    .line 62155
    invoke-interface {v0}, LX/0Sl;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 62156
    iget-object v3, p0, LX/0Sk;->e:Ljava/util/ArrayList;

    if-nez v3, :cond_2

    .line 62157
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, LX/0Sk;->e:Ljava/util/ArrayList;

    .line 62158
    :cond_2
    iget-object v3, p0, LX/0Sk;->e:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 62159
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 62160
    :cond_3
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, LX/0Sk;->f:Z

    .line 62161
    iget-object v0, p0, LX/0Sk;->e:Ljava/util/ArrayList;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized j(LX/0Sk;)V
    .locals 1

    .prologue
    .line 62145
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/0Sk;->f:Z

    .line 62146
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Sk;->e:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62147
    monitor-exit p0

    return-void

    .line 62148
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static k(LX/0Sk;)V
    .locals 5

    .prologue
    .line 62120
    const/4 v0, 0x0

    .line 62121
    monitor-enter p0

    .line 62122
    :try_start_0
    iget-object v1, p0, LX/0Sk;->g:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 62123
    monitor-exit p0

    .line 62124
    :cond_0
    return-void

    .line 62125
    :cond_1
    iget-object v1, p0, LX/0Sk;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 62126
    iget-object v1, p0, LX/0Sk;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v0

    .line 62127
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 62128
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 62129
    if-nez v0, :cond_2

    .line 62130
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 62131
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 62132
    :cond_2
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0XB;

    .line 62133
    if-nez v0, :cond_3

    .line 62134
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 62135
    :cond_3
    if-nez v2, :cond_5

    .line 62136
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 62137
    :goto_1
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v2, v1

    .line 62138
    goto :goto_0

    .line 62139
    :cond_4
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62140
    if-eqz v2, :cond_0

    .line 62141
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 62142
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_0

    .line 62143
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0XB;

    invoke-interface {v0}, LX/0XB;->a()V

    .line 62144
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_5
    move-object v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(ILjava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 62073
    invoke-direct {p0}, LX/0Sk;->g()Ljava/util/Set;

    move-result-object v0

    .line 62074
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sl;

    .line 62075
    invoke-interface {v0, p1, p2, p3}, LX/0Sl;->a(ILjava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 62076
    :cond_0
    invoke-static {p0}, LX/0Sk;->j(LX/0Sk;)V

    .line 62077
    return-void
.end method

.method public final a(JLandroid/content/Intent;Ljava/lang/Class;)V
    .locals 5
    .param p3    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Class;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Landroid/content/Intent;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 62113
    invoke-direct {p0}, LX/0Sk;->h()Ljava/util/ArrayList;

    move-result-object v2

    .line 62114
    if-nez v2, :cond_1

    .line 62115
    :cond_0
    return-void

    .line 62116
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 62117
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 62118
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sl;

    invoke-interface {v0, p1, p2, p3, p4}, LX/0Sl;->a(JLandroid/content/Intent;Ljava/lang/Class;)V

    .line 62119
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final a(JLjava/lang/Class;)V
    .locals 5
    .param p3    # Ljava/lang/Class;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Service;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 62106
    invoke-direct {p0}, LX/0Sk;->h()Ljava/util/ArrayList;

    move-result-object v2

    .line 62107
    if-nez v2, :cond_1

    .line 62108
    :cond_0
    return-void

    .line 62109
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 62110
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 62111
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sl;

    invoke-interface {v0, p1, p2, p3}, LX/0Sl;->a(JLjava/lang/Class;)V

    .line 62112
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final a(JLjava/lang/Class;Landroid/content/Intent;)V
    .locals 5
    .param p3    # Ljava/lang/Class;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Service;",
            ">;",
            "Landroid/content/Intent;",
            ")V"
        }
    .end annotation

    .prologue
    .line 62099
    invoke-direct {p0}, LX/0Sk;->h()Ljava/util/ArrayList;

    move-result-object v2

    .line 62100
    if-nez v2, :cond_1

    .line 62101
    :cond_0
    return-void

    .line 62102
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 62103
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 62104
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sl;

    invoke-interface {v0, p1, p2, p3, p4}, LX/0Sl;->a(JLjava/lang/Class;Landroid/content/Intent;)V

    .line 62105
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final a(JLjava/lang/String;Ljava/lang/Object;)V
    .locals 5
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 62092
    invoke-direct {p0}, LX/0Sk;->h()Ljava/util/ArrayList;

    move-result-object v2

    .line 62093
    if-nez v2, :cond_1

    .line 62094
    :cond_0
    return-void

    .line 62095
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 62096
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 62097
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sl;

    invoke-interface {v0, p1, p2, p3, p4}, LX/0Sl;->a(JLjava/lang/String;Ljava/lang/Object;)V

    .line 62098
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final declared-synchronized a(LX/0XB;)V
    .locals 2

    .prologue
    .line 62087
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Sk;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 62088
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/0Sk;->g:Ljava/util/ArrayList;

    .line 62089
    :cond_0
    iget-object v0, p0, LX/0Sk;->g:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62090
    monitor-exit p0

    return-void

    .line 62091
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 62085
    invoke-direct {p0}, LX/0Sk;->h()Ljava/util/ArrayList;

    move-result-object v0

    .line 62086
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(JLjava/lang/Class;)V
    .locals 5
    .param p3    # Ljava/lang/Class;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Service;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 62078
    invoke-direct {p0}, LX/0Sk;->h()Ljava/util/ArrayList;

    move-result-object v2

    .line 62079
    if-nez v2, :cond_1

    .line 62080
    :cond_0
    return-void

    .line 62081
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 62082
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 62083
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sl;

    invoke-interface {v0, p1, p2, p3}, LX/0Sl;->b(JLjava/lang/Class;)V

    .line 62084
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
