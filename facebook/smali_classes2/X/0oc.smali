.class public final enum LX/0oc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0oc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0oc;

.field public static final enum ASSIGNED:LX/0oc;

.field public static final enum EFFECTIVE:LX/0oc;

.field public static final enum OVERRIDE:LX/0oc;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 141802
    new-instance v0, LX/0oc;

    const-string v1, "ASSIGNED"

    invoke-direct {v0, v1, v2}, LX/0oc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0oc;->ASSIGNED:LX/0oc;

    .line 141803
    new-instance v0, LX/0oc;

    const-string v1, "OVERRIDE"

    invoke-direct {v0, v1, v3}, LX/0oc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0oc;->OVERRIDE:LX/0oc;

    .line 141804
    new-instance v0, LX/0oc;

    const-string v1, "EFFECTIVE"

    invoke-direct {v0, v1, v4}, LX/0oc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0oc;->EFFECTIVE:LX/0oc;

    .line 141805
    const/4 v0, 0x3

    new-array v0, v0, [LX/0oc;

    sget-object v1, LX/0oc;->ASSIGNED:LX/0oc;

    aput-object v1, v0, v2

    sget-object v1, LX/0oc;->OVERRIDE:LX/0oc;

    aput-object v1, v0, v3

    sget-object v1, LX/0oc;->EFFECTIVE:LX/0oc;

    aput-object v1, v0, v4

    sput-object v0, LX/0oc;->$VALUES:[LX/0oc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 141806
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0oc;
    .locals 1

    .prologue
    .line 141807
    const-class v0, LX/0oc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0oc;

    return-object v0
.end method

.method public static values()[LX/0oc;
    .locals 1

    .prologue
    .line 141808
    sget-object v0, LX/0oc;->$VALUES:[LX/0oc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0oc;

    return-object v0
.end method
