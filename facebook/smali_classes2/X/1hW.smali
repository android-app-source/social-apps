.class public LX/1hW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/proxygen/PushCallbacks;


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/http/engine/HttpPushCallback;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/http/engine/HttpPushCallback;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 296091
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296092
    iput-object p1, p0, LX/1hW;->a:Ljava/util/Set;

    .line 296093
    return-void
.end method


# virtual methods
.method public final pushStarted(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 296094
    iget-object v0, p0, LX/1hW;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/push/ImagePushSubscriber;

    .line 296095
    sget-object v2, Lcom/facebook/http/push/ImagePushSubscriber;->a:[Ljava/lang/String;

    move-object v2, v2

    .line 296096
    const/4 v3, 0x0

    .line 296097
    array-length v5, v2

    move v4, v3

    :goto_0
    if-ge v4, v5, :cond_1

    aget-object p0, v2, v4

    .line 296098
    invoke-virtual {p1, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 296099
    const/4 v3, 0x1

    .line 296100
    :cond_1
    move v2, v3

    .line 296101
    if-eqz v2, :cond_0

    .line 296102
    invoke-virtual {v0, p1, p2}, Lcom/facebook/http/push/ImagePushSubscriber;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 296103
    :cond_2
    return-void

    .line 296104
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method
