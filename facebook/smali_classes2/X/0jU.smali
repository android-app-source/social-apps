.class public LX/0jU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jV;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/0jU;


# instance fields
.field public final a:LX/0re;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0re",
            "<",
            "Ljava/lang/String;",
            "LX/18D;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final b:LX/0SG;

.field private final c:LX/0t5;

.field private final d:LX/0aG;

.field private final e:LX/18A;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0fi;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final g:LX/189;


# direct methods
.method public constructor <init>(LX/0V6;Ljava/lang/Integer;LX/189;LX/0rd;LX/0SG;LX/0t5;LX/0aG;LX/18A;)V
    .locals 2
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/api/feed/annotation/StoryMemoryCacheSize;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 123468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123469
    invoke-virtual {p1}, LX/0V6;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    div-int/lit8 v0, v0, 0x3

    .line 123470
    :goto_0
    iput-object p3, p0, LX/0jU;->g:LX/189;

    .line 123471
    iput-object p5, p0, LX/0jU;->b:LX/0SG;

    .line 123472
    iput-object p7, p0, LX/0jU;->d:LX/0aG;

    .line 123473
    const-string v1, "feed_unit"

    invoke-virtual {p4, v0, v1}, LX/0rd;->b(ILjava/lang/String;)LX/0re;

    move-result-object v0

    iput-object v0, p0, LX/0jU;->a:LX/0re;

    .line 123474
    iput-object p6, p0, LX/0jU;->c:LX/0t5;

    .line 123475
    iput-object p8, p0, LX/0jU;->e:LX/18A;

    .line 123476
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0jU;->f:Ljava/util/List;

    .line 123477
    return-void

    .line 123478
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/0jU;
    .locals 12

    .prologue
    .line 123479
    sget-object v0, LX/0jU;->h:LX/0jU;

    if-nez v0, :cond_1

    .line 123480
    const-class v1, LX/0jU;

    monitor-enter v1

    .line 123481
    :try_start_0
    sget-object v0, LX/0jU;->h:LX/0jU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 123482
    if-eqz v2, :cond_0

    .line 123483
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 123484
    new-instance v3, LX/0jU;

    invoke-static {v0}, LX/0V4;->a(LX/0QB;)LX/0V6;

    move-result-object v4

    check-cast v4, LX/0V6;

    .line 123485
    invoke-static {}, LX/188;->b()Ljava/lang/Integer;

    move-result-object v5

    move-object v5, v5

    .line 123486
    check-cast v5, Ljava/lang/Integer;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v6

    check-cast v6, LX/189;

    invoke-static {v0}, LX/0rZ;->a(LX/0QB;)LX/0rd;

    move-result-object v7

    check-cast v7, LX/0rd;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {v0}, LX/0t5;->a(LX/0QB;)LX/0t5;

    move-result-object v9

    check-cast v9, LX/0t5;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v10

    check-cast v10, LX/0aG;

    invoke-static {v0}, LX/18A;->b(LX/0QB;)LX/18A;

    move-result-object v11

    check-cast v11, LX/18A;

    invoke-direct/range {v3 .. v11}, LX/0jU;-><init>(LX/0V6;Ljava/lang/Integer;LX/189;LX/0rd;LX/0SG;LX/0t5;LX/0aG;LX/18A;)V

    .line 123487
    move-object v0, v3

    .line 123488
    sput-object v0, LX/0jU;->h:LX/0jU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123489
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 123490
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 123491
    :cond_1
    sget-object v0, LX/0jU;->h:LX/0jU;

    return-object v0

    .line 123492
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 123493
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/api/story/FetchSingleStoryResult;
    .locals 4

    .prologue
    .line 123494
    if-nez p0, :cond_0

    .line 123495
    const/4 v0, 0x0

    .line 123496
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/api/story/FetchSingleStoryResult;

    .line 123497
    sget-object v1, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    move-object v1, v1

    .line 123498
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->F_()J

    move-result-wide v2

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/facebook/api/story/FetchSingleStoryResult;-><init>(Lcom/facebook/graphql/model/GraphQLStory;LX/0ta;J)V

    goto :goto_0
.end method

.method public static declared-synchronized a(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 123499
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_1

    .line 123500
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 123501
    :cond_1
    if-nez p2, :cond_2

    .line 123502
    :try_start_1
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/0jU;->h(LX/0jU;Ljava/lang/String;)LX/18D;

    move-result-object v0

    .line 123503
    if-eqz v0, :cond_2

    .line 123504
    iget-object p2, v0, LX/18D;->d:Ljava/lang/String;

    .line 123505
    :cond_2
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/0jU;->h(LX/0jU;Ljava/lang/String;)LX/18D;

    move-result-object v0

    .line 123506
    if-eqz v0, :cond_3

    iget-object v1, v0, LX/18D;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    if-eqz p3, :cond_3

    iget-object v1, v0, LX/18D;->e:Ljava/lang/String;

    invoke-virtual {v1, p3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_0

    .line 123507
    :cond_3
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v1

    .line 123508
    if-eqz v1, :cond_4

    .line 123509
    iget-boolean v2, v1, LX/15i;->g:Z

    move v2, v2

    .line 123510
    if-eqz v2, :cond_4

    .line 123511
    invoke-direct {p0, v1}, LX/0jU;->a(LX/15i;)V

    .line 123512
    :cond_4
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, p2, p3}, LX/0jU;->a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123513
    if-eqz v0, :cond_0

    .line 123514
    iget-object v0, p0, LX/0jU;->c:LX/0t5;

    const/4 v1, -0x1

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0t5;->a(ILjava/util/Set;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 123515
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(LX/15i;)V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 123516
    invoke-virtual {p1, v7}, LX/15i;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 123517
    invoke-virtual {p1, v8}, LX/15i;->b(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 123518
    invoke-virtual {p1, v9}, LX/15i;->b(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 123519
    invoke-virtual {p1, v10}, LX/15i;->b(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 123520
    iget-object v4, p0, LX/0jU;->e:LX/18A;

    if-eqz v4, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 123521
    :cond_0
    const-string v4, "FeedUnitCache"

    const-string v5, "Error saving mutations. dedupKey=%s, sortKey=%s, feedType=%s, fetchTime=%s"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v7

    aput-object v2, v6, v8

    aput-object v0, v6, v9

    aput-object v3, v6, v10

    invoke-static {v4, v5, v6}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 123522
    :cond_1
    return-void
.end method

.method private declared-synchronized a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 123523
    monitor-enter p0

    if-nez p1, :cond_0

    .line 123524
    :goto_0
    monitor-exit p0

    return-void

    .line 123525
    :cond_0
    :try_start_0
    new-instance v6, LX/18C;

    invoke-direct {v6, p1}, LX/18C;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 123526
    iget-object v7, p0, LX/0jU;->a:LX/0re;

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v8

    new-instance v0, LX/18D;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, LX/18D;-><init>(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/18C;)V

    invoke-virtual {v7, v8, v0}, LX/0re;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123527
    invoke-static {p0, p1}, LX/0jU;->f(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 123528
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static f(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 123529
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v1

    .line 123530
    if-eqz v1, :cond_0

    .line 123531
    iget-boolean v2, v1, LX/15i;->g:Z

    move v2, v2

    .line 123532
    if-nez v2, :cond_1

    .line 123533
    :cond_0
    :goto_0
    return-void

    .line 123534
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/15i;->b(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 123535
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, LX/15i;->b(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 123536
    const/4 v4, 0x2

    invoke-virtual {v1, v4}, LX/15i;->b(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 123537
    const/4 v5, 0x3

    invoke-virtual {v1, v5}, LX/15i;->b(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 123538
    iget-object v6, p0, LX/0jU;->e:LX/18A;

    if-eqz v6, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 123539
    invoke-virtual {v1}, LX/15i;->b()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 123540
    invoke-virtual {v1}, LX/15i;->c()Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 123541
    if-eqz v6, :cond_4

    .line 123542
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    .line 123543
    :goto_1
    invoke-virtual {v1}, LX/15i;->d()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 123544
    invoke-virtual {v1}, LX/15i;->e()Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 123545
    if-eqz v1, :cond_3

    .line 123546
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    move-object v7, v0

    .line 123547
    :goto_2
    if-nez v6, :cond_2

    if-eqz v7, :cond_0

    .line 123548
    :cond_2
    iget-object v0, p0, LX/0jU;->e:LX/18A;

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, LX/18A;->a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[B)V

    goto :goto_0

    :cond_3
    move-object v7, v0

    goto :goto_2

    :cond_4
    move-object v6, v0

    goto :goto_1
.end method

.method private g(Lcom/facebook/graphql/model/FeedUnit;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123549
    new-instance v0, LX/18C;

    invoke-direct {v0, p1}, LX/18C;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 123550
    invoke-static {p0, p1}, LX/0jU;->f(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 123551
    return-object v0
.end method

.method private declared-synchronized g(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 123552
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0jU;->a:LX/0re;

    invoke-virtual {v0, p1}, LX/0re;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 123553
    new-instance v0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v2}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, LX/55G;->LOCAL_ONLY:LX/55G;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/api/feed/DeleteStoryMethod$Params;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;LX/55G;)V

    .line 123554
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 123555
    const-string v2, "deleteStoryParams"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 123556
    iget-object v0, p0, LX/0jU;->d:LX/0aG;

    const-string v2, "feed_delete_story"

    const v3, -0xcb8ff24

    invoke-static {v0, v2, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123557
    monitor-exit p0

    return-void

    .line 123558
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static h(LX/0jU;Ljava/lang/String;)LX/18D;
    .locals 1

    .prologue
    .line 123559
    if-nez p1, :cond_0

    .line 123560
    const/4 v0, 0x0

    .line 123561
    :goto_0
    return-object v0

    .line 123562
    :cond_0
    monitor-enter p0

    .line 123563
    :try_start_0
    iget-object v0, p0, LX/0jU;->a:LX/0re;

    invoke-virtual {v0, p1}, LX/0re;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18D;

    monitor-exit p0

    goto :goto_0

    .line 123564
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static declared-synchronized i(LX/0jU;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3

    .prologue
    .line 123565
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 123566
    invoke-static {p0, p1}, LX/0jU;->j(LX/0jU;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 123567
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 123568
    invoke-virtual {p0, v0}, LX/0jU;->d(Ljava/lang/String;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 123569
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_0

    .line 123570
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 123571
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    .line 123572
    :goto_0
    monitor-exit p0

    return-object v0

    .line 123573
    :cond_1
    :try_start_1
    invoke-static {v0, p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 123574
    if-eqz v0, :cond_0

    goto :goto_0

    .line 123575
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 123576
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized j(LX/0jU;Ljava/lang/String;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123577
    monitor-enter p0

    :try_start_0
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 123578
    iget-object v0, p0, LX/0jU;->a:LX/0re;

    invoke-virtual {v0}, LX/0re;->c()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18D;

    .line 123579
    iget-object v1, v0, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {p0, v1}, LX/0jU;->e(Lcom/facebook/graphql/model/FeedUnit;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 123580
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 123581
    iget-object v1, v0, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 123582
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 123583
    :cond_2
    :try_start_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 123584
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0jU;->a:LX/0re;

    invoke-virtual {v0}, LX/0re;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123585
    monitor-exit p0

    return-void

    .line 123586
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0fi;)V
    .locals 1

    .prologue
    .line 123587
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0jU;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 123588
    :goto_0
    monitor-exit p0

    return-void

    .line 123589
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0jU;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 123590
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/3Bq;)V
    .locals 8

    .prologue
    .line 123453
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 123454
    invoke-interface {p1}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 123455
    invoke-static {p0, v0}, LX/0jU;->j(LX/0jU;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 123456
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 123457
    :cond_0
    :try_start_1
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 123458
    invoke-static {p0, v0}, LX/0jU;->h(LX/0jU;Ljava/lang/String;)LX/18D;

    move-result-object v2

    .line 123459
    if-eqz v2, :cond_1

    .line 123460
    iget-object v0, v2, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    const/4 v3, 0x0

    invoke-interface {p1, v0, v3}, LX/3Bq;->a(Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 123461
    if-nez v0, :cond_2

    .line 123462
    iget-object v0, v2, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0jU;->g(Ljava/lang/String;)V

    goto :goto_1

    .line 123463
    :cond_2
    iget-object v3, v2, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    if-eq v0, v3, :cond_3

    .line 123464
    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    invoke-static {v0, v4, v5}, LX/16t;->a(Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;J)V

    .line 123465
    iget-object v3, v2, LX/18D;->c:Ljava/lang/String;

    iget-object v4, v2, LX/18D;->d:Ljava/lang/String;

    iget-object v2, v2, LX/18D;->e:Ljava/lang/String;

    invoke-direct {p0, v0, v3, v4, v2}, LX/0jU;->a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 123466
    :cond_3
    invoke-static {p0, v0}, LX/0jU;->f(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 123467
    :cond_4
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lcom/facebook/api/feed/FetchFeedResult;)V
    .locals 6

    .prologue
    .line 123591
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 123592
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 123593
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v4

    invoke-static {v0}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v4, v5, v0}, LX/0jU;->a(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123594
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 123595
    :cond_0
    monitor-exit p0

    return-void

    .line 123596
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 2

    .prologue
    .line 123346
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0jU;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fi;

    .line 123347
    invoke-interface {v0, p1}, LX/0fi;->a(Lcom/facebook/graphql/model/FeedUnit;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 123348
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 123349
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lcom/facebook/graphql/model/FeedUnit;LX/69s;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 123350
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0jU;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fi;

    .line 123351
    invoke-interface {v0, p2, p3}, LX/0fi;->a(LX/69s;Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 123352
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 123353
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 123354
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 123355
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->t()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    .line 123356
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLProfile;->X()Z

    move-result v5

    if-nez v5, :cond_0

    .line 123357
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 123358
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 123359
    :cond_1
    invoke-static {p1}, LX/6X8;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/6X8;

    move-result-object v0

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6X8;->a(LX/0Px;)LX/6X8;

    move-result-object v0

    invoke-virtual {v0}, LX/6X8;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 123360
    invoke-static {p0, v0}, LX/0jU;->f(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123361
    monitor-exit p0

    return-void

    .line 123362
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 123363
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/0jU;->f(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123364
    monitor-exit p0

    return-void

    .line 123365
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 123366
    monitor-enter p0

    if-nez p1, :cond_1

    .line 123367
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 123368
    :cond_1
    :try_start_0
    invoke-static {p0, p1}, LX/0jU;->h(LX/0jU;Ljava/lang/String;)LX/18D;

    move-result-object v0

    .line 123369
    if-eqz v0, :cond_0

    iget-object v1, v0, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v1, :cond_0

    .line 123370
    iget-object v1, v0, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    instance-of v1, v1, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    if-eqz v1, :cond_2

    .line 123371
    iget-object v0, v0, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 123372
    invoke-static {v0, p2}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 123373
    invoke-static {p0, v0}, LX/0jU;->f(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 123374
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 123375
    :cond_2
    :try_start_1
    const-class v1, LX/0jU;

    const-string v2, "setVisibleItemIndex called with wrong type: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, v0, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 123376
    monitor-enter p0

    if-nez p1, :cond_1

    .line 123377
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 123378
    :cond_1
    :try_start_0
    invoke-static {p0, p1}, LX/0jU;->h(LX/0jU;Ljava/lang/String;)LX/18D;

    move-result-object v1

    .line 123379
    if-eqz v1, :cond_0

    .line 123380
    iget-object v0, v1, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    if-eqz v0, :cond_2

    .line 123381
    iget-object v0, v1, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v0}, LX/6X8;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/6X8;

    move-result-object v0

    invoke-static {p2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/6X8;->a(LX/0Px;)LX/6X8;

    move-result-object v0

    invoke-virtual {v0}, LX/6X8;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    .line 123382
    iput-object v0, v1, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    .line 123383
    invoke-static {p0, v0}, LX/0jU;->f(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 123384
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 123385
    :cond_2
    :try_start_1
    const-class v0, LX/0jU;

    const-string v2, "setValidPYMKOrPlaceReviewItems called with wrong type: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v1, v1, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v0, v2, v3}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 123386
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v1

    .line 123387
    iget-object v0, p0, LX/0jU;->a:LX/0re;

    invoke-virtual {v0}, LX/0re;->c()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 123388
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 123389
    iget-object v3, p0, LX/0jU;->a:LX/0re;

    invoke-virtual {v3, v0}, LX/0re;->b(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 123390
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 123391
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 123392
    monitor-enter p0

    if-nez p1, :cond_1

    .line 123393
    :cond_0
    monitor-exit p0

    return-void

    .line 123394
    :cond_1
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 123395
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    invoke-static {v0}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v2, v3, v0}, LX/0jU;->a(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 123396
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/api/story/FetchSingleStoryResult;
    .locals 2

    .prologue
    .line 123397
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, LX/0jU;->d(Ljava/lang/String;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 123398
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v1, :cond_0

    .line 123399
    const/4 v0, 0x0

    .line 123400
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/0jU;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/api/story/FetchSingleStoryResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final declared-synchronized b(LX/0fi;)V
    .locals 1

    .prologue
    .line 123401
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0jU;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123402
    monitor-exit p0

    return-void

    .line 123403
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 123404
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    .line 123405
    if-nez v1, :cond_0

    .line 123406
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cache id should not be null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123407
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 123408
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0jU;->a:LX/0re;

    invoke-virtual {v0, v1}, LX/0re;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18D;

    .line 123409
    if-nez v0, :cond_1

    .line 123410
    const-string v0, "FeedUnitCache"

    const-string v2, "Cache entry doesn\'t exist. cacheId=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 123411
    :goto_0
    invoke-virtual {p0, p1}, LX/0jU;->a(Lcom/facebook/graphql/model/FeedUnit;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123412
    monitor-exit p0

    return-void

    .line 123413
    :cond_1
    :try_start_2
    iput-object p1, v0, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 123414
    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123415
    invoke-static {p0, p1}, LX/0jU;->j(LX/0jU;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized c(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 123416
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/0jU;->h(LX/0jU;Ljava/lang/String;)LX/18D;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 123417
    if-nez v0, :cond_0

    .line 123418
    :goto_0
    monitor-exit p0

    return-void

    .line 123419
    :cond_0
    :try_start_1
    iget-object v1, v0, LX/18D;->d:Ljava/lang/String;

    iget-object v0, v0, LX/18D;->e:Ljava/lang/String;

    invoke-static {p0, p1, v1, v0}, LX/0jU;->a(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 123420
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(Ljava/lang/String;)Lcom/facebook/graphql/model/FeedUnit;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 123421
    monitor-enter p0

    if-nez p1, :cond_1

    .line 123422
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 123423
    :cond_1
    :try_start_0
    invoke-static {p0, p1}, LX/0jU;->h(LX/0jU;Ljava/lang/String;)LX/18D;

    move-result-object v1

    .line 123424
    if-eqz v1, :cond_0

    .line 123425
    iget-object v0, v1, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 123426
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e(Lcom/facebook/graphql/model/FeedUnit;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123427
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 123428
    invoke-direct {p0, p1}, LX/0jU;->g(Lcom/facebook/graphql/model/FeedUnit;)Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 123429
    :goto_0
    monitor-exit p0

    return-object v0

    .line 123430
    :cond_0
    :try_start_1
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/0jU;->h(LX/0jU;Ljava/lang/String;)LX/18D;

    move-result-object v0

    .line 123431
    if-eqz v0, :cond_1

    .line 123432
    iget-object v0, v0, LX/18D;->f:LX/18C;

    goto :goto_0

    .line 123433
    :cond_1
    invoke-direct {p0, p1}, LX/0jU;->g(Lcom/facebook/graphql/model/FeedUnit;)Ljava/util/Set;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 123434
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e(Ljava/lang/String;)V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 123435
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/0jU;->h(LX/0jU;Ljava/lang/String;)LX/18D;

    move-result-object v1

    .line 123436
    if-eqz v1, :cond_0

    iget-object v0, v1, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    instance-of v0, v0, Lcom/facebook/graphql/model/Sponsorable;

    if-eqz v0, :cond_0

    iget-object v0, v1, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    check-cast v0, Lcom/facebook/graphql/model/Sponsorable;

    invoke-static {v0}, LX/18M;->c(Lcom/facebook/graphql/model/Sponsorable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 123437
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 123438
    :cond_1
    :try_start_1
    iget-object v0, v1, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    check-cast v0, Lcom/facebook/graphql/model/Sponsorable;

    invoke-static {v0}, LX/18M;->b(Lcom/facebook/graphql/model/Sponsorable;)V

    .line 123439
    iget-object v0, v1, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    iget-object v2, p0, LX/0jU;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/16t;->a(Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;J)V

    .line 123440
    iget-object v0, v1, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {p0, v0}, LX/0jU;->f(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 123441
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f(Ljava/lang/String;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 123442
    monitor-enter p0

    if-nez p1, :cond_1

    .line 123443
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 123444
    :cond_1
    :try_start_0
    invoke-static {p0, p1}, LX/0jU;->h(LX/0jU;Ljava/lang/String;)LX/18D;

    move-result-object v0

    .line 123445
    if-eqz v0, :cond_0

    iget-object v1, v0, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v1, :cond_0

    .line 123446
    iget-object v1, v0, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    instance-of v1, v1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    if-nez v1, :cond_2

    iget-object v1, v0, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    instance-of v1, v1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    if-eqz v1, :cond_3

    .line 123447
    :cond_2
    iget-object v1, v0, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    .line 123448
    invoke-static {v1}, LX/6X8;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/6X8;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v1, v2}, LX/6X8;->a(Lcom/facebook/graphql/enums/StoryVisibility;)LX/6X8;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/6X8;->a(Z)LX/6X8;

    move-result-object v1

    invoke-virtual {v1}, LX/6X8;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 123449
    iput-object v1, v0, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    .line 123450
    invoke-static {p0, v1}, LX/0jU;->f(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 123451
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 123452
    :cond_3
    :try_start_1
    const-class v1, LX/0jU;

    const-string v2, "setSurveyOrResearchPollCompleted called with wrong type: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, v0, LX/18D;->b:Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
