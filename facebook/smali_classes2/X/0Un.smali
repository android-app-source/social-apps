.class public LX/0Un;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile c:LX/0Un;


# instance fields
.field public final b:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66792
    const-class v0, LX/0Un;

    sput-object v0, LX/0Un;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 66810
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66811
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/0Un;->b:Ljava/util/concurrent/ConcurrentMap;

    .line 66812
    return-void
.end method

.method public static a(LX/0QB;)LX/0Un;
    .locals 3

    .prologue
    .line 66798
    sget-object v0, LX/0Un;->c:LX/0Un;

    if-nez v0, :cond_1

    .line 66799
    const-class v1, LX/0Un;

    monitor-enter v1

    .line 66800
    :try_start_0
    sget-object v0, LX/0Un;->c:LX/0Un;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 66801
    if-eqz v2, :cond_0

    .line 66802
    :try_start_1
    new-instance v0, LX/0Un;

    invoke-direct {v0}, LX/0Un;-><init>()V

    .line 66803
    move-object v0, v0

    .line 66804
    sput-object v0, LX/0Un;->c:LX/0Un;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 66805
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 66806
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 66807
    :cond_1
    sget-object v0, LX/0Un;->c:LX/0Un;

    return-object v0

    .line 66808
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 66809
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Class;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 66793
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 66794
    if-eqz p2, :cond_0

    .line 66795
    iget-object v0, p0, LX/0Un;->b:Ljava/util/concurrent/ConcurrentMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66796
    :goto_0
    return-void

    .line 66797
    :cond_0
    iget-object v0, p0, LX/0Un;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
