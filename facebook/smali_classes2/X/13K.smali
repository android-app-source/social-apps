.class public final LX/13K;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:J


# instance fields
.field public final b:LX/13D;

.field public final c:LX/13H;
    .annotation runtime Lcom/facebook/quickpromotion/annotations/DefinitionValidator;
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation runtime Lcom/facebook/quickpromotion/annotations/ContextualFilterValidator;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13H;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/13H;
    .annotation runtime Lcom/facebook/quickpromotion/annotations/ActionLimitValidator;
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation runtime Lcom/facebook/quickpromotion/annotations/ClientsideDynamicParametersValidator;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13H;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2hf;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0SG;

.field private final k:LX/13N;

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/76X;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation
.end field

.field public p:J

.field public q:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

.field public r:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 176491
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 176492
    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    sput-wide v0, LX/13K;->a:J

    return-void

    :cond_0
    const-wide/32 v0, 0xdbba0

    goto :goto_0
.end method

.method public constructor <init>(LX/13D;LX/13H;LX/0Ot;LX/13H;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0SG;LX/0Ot;LX/13N;LX/0Ot;)V
    .locals 1
    .param p1    # LX/13D;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/13H;
        .annotation runtime Lcom/facebook/quickpromotion/annotations/DefinitionValidator;
        .end annotation
    .end param
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/quickpromotion/annotations/ContextualFilterValidator;
        .end annotation
    .end param
    .param p4    # LX/13H;
        .annotation runtime Lcom/facebook/quickpromotion/annotations/ActionLimitValidator;
        .end annotation
    .end param
    .param p5    # LX/0Ot;
        .annotation runtime Lcom/facebook/quickpromotion/annotations/ClientsideDynamicParametersValidator;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/13D;",
            "LX/13H;",
            "LX/0Ot",
            "<",
            "LX/13H;",
            ">;",
            "LX/13H;",
            "LX/0Ot",
            "<",
            "LX/13H;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "LX/2hf;",
            ">;",
            "LX/13N;",
            "LX/0Ot",
            "<",
            "LX/76X;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 176472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176473
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 176474
    iput-object v0, p0, LX/13K;->m:LX/0Px;

    .line 176475
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 176476
    iput-object v0, p0, LX/13K;->n:LX/0Px;

    .line 176477
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 176478
    iput-object v0, p0, LX/13K;->o:LX/0Px;

    .line 176479
    iput-object p1, p0, LX/13K;->b:LX/13D;

    .line 176480
    iput-object p2, p0, LX/13K;->c:LX/13H;

    .line 176481
    iput-object p3, p0, LX/13K;->d:LX/0Ot;

    .line 176482
    iput-object p4, p0, LX/13K;->e:LX/13H;

    .line 176483
    iput-object p5, p0, LX/13K;->f:LX/0Ot;

    .line 176484
    iput-object p6, p0, LX/13K;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 176485
    iput-object p7, p0, LX/13K;->h:LX/0Ot;

    .line 176486
    iput-object p8, p0, LX/13K;->j:LX/0SG;

    .line 176487
    iput-object p9, p0, LX/13K;->i:LX/0Ot;

    .line 176488
    iput-object p10, p0, LX/13K;->k:LX/13N;

    .line 176489
    iput-object p11, p0, LX/13K;->l:LX/0Ot;

    .line 176490
    return-void
.end method

.method public static a(LX/13K;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 176467
    iget-object v1, p0, LX/13K;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2fw;->b:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    move v1, v1

    .line 176468
    if-eqz v1, :cond_1

    .line 176469
    :cond_0
    :goto_0
    return v0

    .line 176470
    :cond_1
    if-eqz p2, :cond_2

    iget-object v1, p2, Lcom/facebook/interstitial/manager/InterstitialTrigger;->action:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VOIP_CALL_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    if-eq v1, v2, :cond_0

    .line 176471
    :cond_2
    invoke-static {p1}, LX/13K;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)Z
    .locals 2

    .prologue
    .line 176466
    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->f()LX/0Rf;

    move-result-object v0

    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Attribute;->IS_UNCANCELABLE:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Attribute;

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)Z
    .locals 12

    .prologue
    const-wide/16 v8, 0x3e8

    const-wide/16 v10, 0x0

    .line 176493
    iget-object v0, p0, LX/13K;->j:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 176494
    iget-wide v2, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->startTime:J

    mul-long/2addr v2, v8

    .line 176495
    iget-wide v4, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->endTime:J

    mul-long/2addr v4, v8

    .line 176496
    iget-wide v6, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->clientTtlSeconds:J

    mul-long/2addr v6, v8

    .line 176497
    cmp-long v8, v2, v10

    if-eqz v8, :cond_0

    cmp-long v2, v0, v2

    if-lez v2, :cond_3

    :cond_0
    cmp-long v2, v4, v10

    if-eqz v2, :cond_1

    cmp-long v2, v0, v4

    if-gez v2, :cond_3

    :cond_1
    cmp-long v2, v6, v10

    if-eqz v2, :cond_2

    iget-wide v2, p0, LX/13K;->p:J

    cmp-long v2, v2, v10

    if-eqz v2, :cond_2

    iget-wide v2, p0, LX/13K;->p:J

    add-long/2addr v2, v6

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/13K;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
    .locals 14
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 176421
    const/4 v3, 0x0

    .line 176422
    iget-object v0, p0, LX/13K;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v5, :cond_6

    iget-object v0, p0, LX/13K;->m:LX/0Px;

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 176423
    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 176424
    iget-object v1, p0, LX/13K;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/13P;

    .line 176425
    const-string v2, "client_force_mode"

    invoke-virtual {v1, v0, v2}, LX/13P;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;)V

    .line 176426
    iget-object v2, p0, LX/13K;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v6, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-static {v6}, LX/2fw;->c(Ljava/lang/String;)LX/0Tn;

    move-result-object v6

    sget-object v7, LX/2fx;->DEFAULT:LX/2fx;

    invoke-virtual {v7}, LX/2fx;->ordinal()I

    move-result v7

    invoke-interface {v2, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    .line 176427
    invoke-static {}, LX/2fx;->values()[LX/2fx;

    move-result-object v6

    aget-object v2, v6, v2

    .line 176428
    sget-object v6, LX/2fx;->FORCE_ON:LX/2fx;

    invoke-virtual {v2, v6}, LX/2fx;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 176429
    :goto_1
    return-object v0

    .line 176430
    :cond_0
    sget-object v6, LX/2fx;->FORCE_OFF:LX/2fx;

    invoke-virtual {v2, v6}, LX/2fx;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 176431
    const-string v6, "client_enabled_time"

    invoke-virtual {v1, v0, v6}, LX/13P;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;)V

    .line 176432
    sget-object v6, LX/2fx;->IGNORE_ENABLE_TIME:LX/2fx;

    invoke-virtual {v2, v6}, LX/2fx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0, v0}, LX/13K;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 176433
    :cond_1
    const-string v2, "client_surface_delay"

    invoke-virtual {v1, v0, v2}, LX/13P;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;)V

    .line 176434
    invoke-static {p0, v0, p1}, LX/13K;->a(LX/13K;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 176435
    iget-object v8, p0, LX/13K;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v9, p0, LX/13K;->b:LX/13D;

    invoke-virtual {v9}, LX/13D;->b()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/2fw;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v9

    const-wide/16 v10, 0x0

    invoke-interface {v8, v9, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v8

    .line 176436
    iget-object v10, p0, LX/13K;->j:LX/0SG;

    invoke-interface {v10}, LX/0SG;->a()J

    move-result-wide v10

    sget-wide v12, LX/13K;->a:J

    add-long/2addr v8, v12

    cmp-long v8, v10, v8

    if-ltz v8, :cond_7

    const/4 v8, 0x1

    :goto_2
    move v2, v8

    .line 176437
    if-eqz v2, :cond_5

    invoke-direct {p0}, LX/13K;->g()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 176438
    :cond_2
    const-string v2, "client_action_limit"

    invoke-virtual {v1, v0, v2}, LX/13P;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;)V

    .line 176439
    iget-object v2, p0, LX/13K;->e:LX/13H;

    invoke-interface {v2, v0, p1}, LX/13H;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/1Y7;

    move-result-object v2

    .line 176440
    iget-boolean v2, v2, LX/1Y7;->c:Z

    if-eqz v2, :cond_5

    .line 176441
    const-string v2, "client_filters"

    invoke-virtual {v1, v0, v2}, LX/13P;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;)V

    .line 176442
    iget-object v2, p0, LX/13K;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/13H;

    invoke-interface {v2, v0, p1}, LX/13H;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/1Y7;

    move-result-object v2

    .line 176443
    iget-boolean v2, v2, LX/1Y7;->c:Z

    if-eqz v2, :cond_5

    .line 176444
    const-string v2, "client_parameters"

    invoke-virtual {v1, v0, v2}, LX/13P;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;)V

    .line 176445
    iget-object v2, p0, LX/13K;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/13H;

    invoke-interface {v2, v0, p1}, LX/13H;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/1Y7;

    move-result-object v2

    .line 176446
    iget-boolean v2, v2, LX/1Y7;->c:Z

    if-eqz v2, :cond_5

    .line 176447
    const-string v2, "client_exposure_log"

    invoke-virtual {v1, v0, v2}, LX/13P;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;)V

    .line 176448
    iget-boolean v1, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->isExposureHoldout:Z

    if-eqz v1, :cond_4

    .line 176449
    iget-object v1, p0, LX/13K;->k:LX/13N;

    sget-object v2, LX/2fy;->IMPRESSION:LX/2fy;

    invoke-virtual {v1, v0, v2}, LX/13N;->e(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)V

    .line 176450
    iget-object v1, p0, LX/13K;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/13P;

    .line 176451
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "qp_holdout_exposure"

    invoke-direct {v2, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 176452
    invoke-static {v2}, LX/13P;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 176453
    invoke-static {v1, v2, v0, p1}, LX/13P;->a(LX/13P;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    .line 176454
    invoke-static {v1, v2}, LX/13P;->b(LX/13P;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 176455
    iget-object v6, v1, LX/13P;->a:LX/0Zb;

    invoke-interface {v6, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 176456
    move-object v0, v3

    .line 176457
    :cond_3
    :goto_3
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move-object v3, v0

    goto/16 :goto_0

    .line 176458
    :cond_4
    iget-object v1, p0, LX/13K;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/13P;

    .line 176459
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "qp_exposure"

    invoke-direct {v2, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 176460
    invoke-static {v2}, LX/13P;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 176461
    invoke-static {v1, v2, v0, p1}, LX/13P;->a(LX/13P;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    .line 176462
    invoke-static {v1, v2}, LX/13P;->b(LX/13P;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 176463
    iget-object v6, v1, LX/13P;->a:LX/0Zb;

    invoke-interface {v6, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 176464
    if-eqz v3, :cond_3

    :cond_5
    move-object v0, v3

    goto :goto_3

    :cond_6
    move-object v0, v3

    .line 176465
    goto/16 :goto_1

    :cond_7
    const/4 v8, 0x0

    goto/16 :goto_2
.end method

.method private g()Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x1

    .line 176416
    iget-object v1, p0, LX/13K;->b:LX/13D;

    invoke-virtual {v1}, LX/13D;->f()J

    move-result-wide v2

    .line 176417
    cmp-long v1, v2, v6

    if-nez v1, :cond_1

    .line 176418
    :cond_0
    :goto_0
    return v0

    .line 176419
    :cond_1
    iget-object v1, p0, LX/13K;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v4, p0, LX/13K;->b:LX/13D;

    invoke-virtual {v4}, LX/13D;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/2fw;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v4

    invoke-interface {v1, v4, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 176420
    iget-object v1, p0, LX/13K;->j:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v6

    add-long/2addr v2, v4

    cmp-long v1, v6, v2

    if-gez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LX/13K;)V
    .locals 4

    .prologue
    .line 176413
    iget-object v0, p0, LX/13K;->b:LX/13D;

    invoke-virtual {v0}, LX/13D;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2fw;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 176414
    iget-object v1, p0, LX/13K;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    iget-object v2, p0, LX/13K;->j:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v1, v0, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 176415
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 176402
    iget-object v0, p0, LX/13K;->q:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    if-nez v0, :cond_1

    .line 176403
    const/4 p1, 0x0

    .line 176404
    :cond_0
    :goto_0
    return-object p1

    .line 176405
    :cond_1
    iget-object v0, p0, LX/13K;->q:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v1, p0, LX/13K;->r:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {p0, v0, v1}, LX/13K;->a(LX/13K;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 176406
    invoke-static {p0}, LX/13K;->h(LX/13K;)V

    .line 176407
    :cond_2
    const-string v0, "qp_definition"

    iget-object v1, p0, LX/13K;->q:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 176408
    const-string v0, "qp_controller_id"

    iget-object v1, p0, LX/13K;->b:LX/13D;

    invoke-virtual {v1}, LX/13D;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 176409
    const-string v0, "qp_trigger"

    iget-object v1, p0, LX/13K;->r:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 176410
    iget-object v0, p0, LX/13K;->q:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-static {v0}, LX/13K;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176411
    const/high16 v0, 0x10000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 176412
    const v0, 0x8000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final c(Lcom/facebook/interstitial/manager/InterstitialTrigger;)V
    .locals 7

    .prologue
    .line 176399
    iget-object v0, p0, LX/13K;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13P;

    iget-object v1, p0, LX/13K;->b:LX/13D;

    invoke-virtual {v1}, LX/13D;->b()Ljava/lang/String;

    move-result-object v1

    .line 176400
    iget-object v2, v0, LX/13P;->h:LX/13Q;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "qp_trigger_hit:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/facebook/interstitial/manager/InterstitialTrigger;->action:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x1

    const-string v6, "qp_counters"

    invoke-virtual {v2, v3, v4, v5, v6}, LX/13Q;->a(Ljava/lang/String;JLjava/lang/String;)V

    .line 176401
    return-void
.end method
