.class public final LX/1AC;
.super LX/0T0;
.source ""


# instance fields
.field public final synthetic a:LX/19P;


# direct methods
.method public constructor <init>(LX/19P;)V
    .locals 0

    .prologue
    .line 210141
    iput-object p1, p0, LX/1AC;->a:LX/19P;

    invoke-direct {p0}, LX/0T0;-><init>()V

    return-void
.end method


# virtual methods
.method public final c(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 210142
    iget-object v0, p0, LX/1AC;->a:LX/19P;

    iget-object v0, v0, LX/19P;->M:LX/19s;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/19s;->a(Z)V

    .line 210143
    iget-object v0, p0, LX/1AC;->a:LX/19P;

    invoke-virtual {v0}, LX/19P;->d()V

    .line 210144
    return-void
.end method

.method public final d(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 210145
    iget-object v0, p0, LX/1AC;->a:LX/19P;

    iget-object v0, v0, LX/19P;->M:LX/19s;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/19s;->a(Z)V

    .line 210146
    iget-object v0, p0, LX/1AC;->a:LX/19P;

    .line 210147
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/19P;->E:Z

    .line 210148
    iget-object v1, v0, LX/19P;->a:Ljava/util/List;

    .line 210149
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    .line 210150
    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 210151
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 210152
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 210153
    invoke-interface {p0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 210154
    :cond_1
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    iget-object v1, v0, LX/19P;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_4

    .line 210155
    iget-object v1, v0, LX/19P;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2qU;

    .line 210156
    if-eqz v1, :cond_3

    .line 210157
    iget-object p0, v1, LX/2qU;->b:LX/2pu;

    if-eqz p0, :cond_2

    .line 210158
    iget-object p0, v1, LX/2qU;->b:LX/2pu;

    invoke-virtual {p0}, LX/2pu;->d()V

    .line 210159
    :cond_2
    invoke-static {v1}, LX/2qU;->B(LX/2qU;)V

    .line 210160
    iget-object p0, v0, LX/19P;->R:LX/0Uh;

    const/16 p1, 0x4aa

    invoke-virtual {p0, p1}, LX/0Uh;->a(I)LX/03R;

    move-result-object p0

    sget-object p1, LX/03R;->YES:LX/03R;

    if-ne p0, p1, :cond_3

    .line 210161
    invoke-virtual {v1}, LX/2qU;->n()V

    .line 210162
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 210163
    :cond_4
    sget-object v1, LX/04g;->BY_MANAGER:LX/04g;

    invoke-static {v0, v1}, LX/19P;->b(LX/19P;LX/04g;)V

    .line 210164
    return-void
.end method
