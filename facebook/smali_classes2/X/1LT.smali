.class public LX/1LT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/0Xl;

.field private b:LX/0Yb;

.field public c:LX/0fl;


# direct methods
.method public constructor <init>(LX/0Xl;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 233793
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233794
    iput-object p1, p0, LX/1LT;->a:LX/0Xl;

    .line 233795
    return-void
.end method

.method public static b(LX/0QB;)LX/1LT;
    .locals 2

    .prologue
    .line 233791
    new-instance v1, LX/1LT;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v0

    check-cast v0, LX/0Xl;

    invoke-direct {v1, v0}, LX/1LT;-><init>(LX/0Xl;)V

    .line 233792
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 233786
    iget-object v0, p0, LX/1LT;->a:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance v2, LX/1MA;

    invoke-direct {v2, p0}, LX/1MA;-><init>(LX/1LT;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    new-instance v2, LX/1MB;

    invoke-direct {v2, p0}, LX/1MB;-><init>(LX/1LT;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/1LT;->b:LX/0Yb;

    .line 233787
    iget-object v0, p0, LX/1LT;->b:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 233788
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 233789
    iget-object v0, p0, LX/1LT;->b:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 233790
    return-void
.end method
