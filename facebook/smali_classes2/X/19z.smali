.class public final LX/19z;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;

.field public static final m:LX/0U1;

.field public static final n:LX/0U1;

.field public static final o:LX/0U1;

.field public static final p:LX/0U1;

.field public static final q:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 209847
    new-instance v0, LX/0U1;

    const-string v1, "video_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/19z;->a:LX/0U1;

    .line 209848
    new-instance v0, LX/0U1;

    const-string v1, "video_url"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/19z;->b:LX/0U1;

    .line 209849
    new-instance v0, LX/0U1;

    const-string v1, "video_size"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/19z;->c:LX/0U1;

    .line 209850
    new-instance v0, LX/0U1;

    const-string v1, "video_downloaded_size"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/19z;->d:LX/0U1;

    .line 209851
    new-instance v0, LX/0U1;

    const-string v1, "download_status"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/19z;->e:LX/0U1;

    .line 209852
    new-instance v0, LX/0U1;

    const-string v1, "video_file"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/19z;->f:LX/0U1;

    .line 209853
    new-instance v0, LX/0U1;

    const-string v1, "last_check_time"

    const-string v2, "LONG"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/19z;->g:LX/0U1;

    .line 209854
    new-instance v0, LX/0U1;

    const-string v1, "scheduling_policy"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/19z;->h:LX/0U1;

    .line 209855
    new-instance v0, LX/0U1;

    const-string v1, "download_type"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/19z;->i:LX/0U1;

    .line 209856
    new-instance v0, LX/0U1;

    const-string v1, "last_update_time"

    const-string v2, "LONG"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/19z;->j:LX/0U1;

    .line 209857
    new-instance v0, LX/0U1;

    const-string v1, "audio_url"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/19z;->k:LX/0U1;

    .line 209858
    new-instance v0, LX/0U1;

    const-string v1, "audio_size"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/19z;->l:LX/0U1;

    .line 209859
    new-instance v0, LX/0U1;

    const-string v1, "audio_downloaded_size"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/19z;->m:LX/0U1;

    .line 209860
    new-instance v0, LX/0U1;

    const-string v1, "audio_file"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/19z;->n:LX/0U1;

    .line 209861
    new-instance v0, LX/0U1;

    const-string v1, "video_format_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/19z;->o:LX/0U1;

    .line 209862
    new-instance v0, LX/0U1;

    const-string v1, "audio_format_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/19z;->p:LX/0U1;

    .line 209863
    new-instance v0, LX/0U1;

    const-string v1, "dash_manifest"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/19z;->q:LX/0U1;

    return-void
.end method
