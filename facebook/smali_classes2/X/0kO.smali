.class public LX/0kO;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 127116
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 127117
    return-void
.end method

.method public static a(LX/0kK;)LX/0kN;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 127118
    iget-object v0, p0, LX/0kK;->c:LX/0kN;

    move-object v0, v0

    .line 127119
    return-object v0
.end method

.method public static a(LX/03R;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lB;LX/0SG;)LX/6Fe;
    .locals 1
    .param p0    # LX/03R;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 127120
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 127121
    if-nez v0, :cond_0

    sget-object v0, LX/03R;->YES:LX/03R;

    if-ne p0, v0, :cond_1

    .line 127122
    :cond_0
    new-instance v0, LX/6Ge;

    invoke-direct {v0, p1, p2, p3}, LX/6Ge;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lB;LX/0SG;)V

    .line 127123
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, LX/6Gk;

    invoke-direct {v0}, LX/6Gk;-><init>()V

    goto :goto_0
.end method

.method public static a(LX/0Uh;)Ljava/lang/Boolean;
    .locals 2
    .annotation runtime Lcom/facebook/bugreporter/annotations/IsNotSendBugReportEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 127124
    const/16 v0, 0x140

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 127125
    return-void
.end method
