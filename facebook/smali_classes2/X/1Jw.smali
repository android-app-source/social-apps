.class public LX/1Jw;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1Jw;

.field public static final b:LX/1Jx;

.field public static final c:LX/1Jw;


# instance fields
.field public d:I

.field public e:I

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v0, -0x1

    const/high16 v2, -0x80000000

    .line 230752
    invoke-static {v0, v0}, LX/1Jw;->c(II)LX/1Jw;

    move-result-object v0

    .line 230753
    sput-object v0, LX/1Jw;->a:LX/1Jw;

    const/4 v1, 0x0

    .line 230754
    new-instance v3, LX/1Jx;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, LX/1Jx;-><init>(Z)V

    .line 230755
    invoke-virtual {v3, v0, v1}, LX/1Jx;->b(LX/1Jw;LX/1Jw;)V

    .line 230756
    move-object v0, v3

    .line 230757
    sput-object v0, LX/1Jw;->b:LX/1Jx;

    .line 230758
    invoke-static {v2, v2}, LX/1Jw;->c(II)LX/1Jw;

    move-result-object v0

    sput-object v0, LX/1Jw;->c:LX/1Jw;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 230747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230748
    iput v0, p0, LX/1Jw;->d:I

    .line 230749
    iput v0, p0, LX/1Jw;->e:I

    .line 230750
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Jw;->f:Z

    .line 230751
    return-void
.end method

.method public static b(II)LX/1Jw;
    .locals 2

    .prologue
    .line 230744
    if-lt p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "End must be greater than or equal to start"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 230745
    invoke-static {p0, p1}, LX/A8e;->a(II)LX/1Jw;

    move-result-object v0

    return-object v0

    .line 230746
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(II)LX/1Jw;
    .locals 2

    .prologue
    .line 230740
    new-instance v0, LX/1Jw;

    invoke-direct {v0}, LX/1Jw;-><init>()V

    .line 230741
    invoke-virtual {v0, p0, p1}, LX/1Jw;->a(II)V

    .line 230742
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/1Jw;->f:Z

    .line 230743
    return-object v0
.end method


# virtual methods
.method public final a(LX/1Jw;)LX/1Jx;
    .locals 6

    .prologue
    .line 230707
    invoke-virtual {p0}, LX/1Jw;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LX/1Jw;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 230708
    :cond_0
    iget v0, p0, LX/1Jw;->d:I

    move v0, v0

    .line 230709
    iget v1, p0, LX/1Jw;->e:I

    move v1, v1

    .line 230710
    invoke-static {v0, v1}, LX/1Jw;->b(II)LX/1Jw;

    move-result-object v0

    move-object v0, v0

    .line 230711
    invoke-static {v0}, LX/A8e;->b(LX/1Jw;)LX/1Jx;

    move-result-object v0

    .line 230712
    :goto_0
    return-object v0

    .line 230713
    :cond_1
    iget v0, p0, LX/1Jw;->d:I

    .line 230714
    iget v1, p0, LX/1Jw;->e:I

    .line 230715
    iget v2, p1, LX/1Jw;->d:I

    move v2, v2

    .line 230716
    iget v3, p1, LX/1Jw;->e:I

    move v3, v3

    .line 230717
    iget v4, p0, LX/1Jw;->d:I

    move v4, v4

    .line 230718
    iget v5, p1, LX/1Jw;->d:I

    move v5, v5

    .line 230719
    if-ge v4, v5, :cond_6

    .line 230720
    iget v4, p0, LX/1Jw;->e:I

    move v4, v4

    .line 230721
    iget v5, p1, LX/1Jw;->e:I

    move v5, v5

    .line 230722
    if-le v4, v5, :cond_6

    const/4 v4, 0x1

    :goto_1
    move v4, v4

    .line 230723
    if-eqz v4, :cond_2

    .line 230724
    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v2}, LX/A8e;->a(II)LX/1Jw;

    move-result-object v0

    .line 230725
    add-int/lit8 v2, v3, 0x1

    invoke-static {v2, v1}, LX/A8e;->a(II)LX/1Jw;

    move-result-object v1

    .line 230726
    invoke-static {v0, v1}, LX/A8e;->a(LX/1Jw;LX/1Jw;)LX/1Jx;

    move-result-object v0

    goto :goto_0

    .line 230727
    :cond_2
    iget v4, p1, LX/1Jw;->d:I

    move v4, v4

    .line 230728
    iget v5, p0, LX/1Jw;->d:I

    move v5, v5

    .line 230729
    if-gt v4, v5, :cond_7

    .line 230730
    iget v4, p1, LX/1Jw;->e:I

    move v4, v4

    .line 230731
    iget v5, p0, LX/1Jw;->e:I

    move v5, v5

    .line 230732
    if-lt v4, v5, :cond_7

    const/4 v4, 0x1

    :goto_2
    move v4, v4

    .line 230733
    if-eqz v4, :cond_3

    .line 230734
    sget-object v0, LX/1Jw;->b:LX/1Jx;

    goto :goto_0

    .line 230735
    :cond_3
    if-ge v0, v2, :cond_4

    if-lt v1, v2, :cond_4

    .line 230736
    add-int/lit8 v1, v2, -0x1

    .line 230737
    :cond_4
    if-le v1, v3, :cond_5

    if-gt v0, v3, :cond_5

    .line 230738
    add-int/lit8 v0, v3, 0x1

    .line 230739
    :cond_5
    invoke-static {v0, v1}, LX/1Jw;->b(II)LX/1Jw;

    move-result-object v0

    invoke-static {v0}, LX/A8e;->b(LX/1Jw;)LX/1Jx;

    move-result-object v0

    goto :goto_0

    :cond_6
    const/4 v4, 0x0

    goto :goto_1

    :cond_7
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public final a()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 230704
    iput v0, p0, LX/1Jw;->d:I

    .line 230705
    iput v0, p0, LX/1Jw;->e:I

    .line 230706
    return-void
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 230701
    iput p1, p0, LX/1Jw;->d:I

    .line 230702
    iput p2, p0, LX/1Jw;->e:I

    .line 230703
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 230700
    sget-object v0, LX/1Jw;->a:LX/1Jw;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/1Jw;->a:LX/1Jw;

    invoke-virtual {p0, v0}, LX/1Jw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 230683
    instance-of v1, p1, LX/1Jw;

    if-nez v1, :cond_1

    .line 230684
    :cond_0
    :goto_0
    return v0

    .line 230685
    :cond_1
    check-cast p1, LX/1Jw;

    .line 230686
    iget v1, p0, LX/1Jw;->d:I

    move v1, v1

    .line 230687
    iget v2, p1, LX/1Jw;->d:I

    move v2, v2

    .line 230688
    if-ne v1, v2, :cond_0

    .line 230689
    iget v1, p0, LX/1Jw;->e:I

    move v1, v1

    .line 230690
    iget v2, p1, LX/1Jw;->e:I

    move v2, v2

    .line 230691
    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 230697
    iget v0, p0, LX/1Jw;->d:I

    move v0, v0

    .line 230698
    iget v1, p0, LX/1Jw;->e:I

    move v1, v1

    .line 230699
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 230692
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "start"

    .line 230693
    iget v2, p0, LX/1Jw;->d:I

    move v2, v2

    .line 230694
    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "end"

    .line 230695
    iget v2, p0, LX/1Jw;->e:I

    move v2, v2

    .line 230696
    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
