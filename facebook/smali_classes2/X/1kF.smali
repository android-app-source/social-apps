.class public LX/1kF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0ad;

.field public final c:LX/1kG;

.field private final d:LX/1E1;

.field public final e:LX/1kI;

.field private final f:LX/1kD;

.field private final g:LX/11R;

.field private final h:Landroid/content/res/Resources;

.field public i:Z


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;LX/1kG;Ljava/lang/Boolean;LX/1E1;LX/1kI;LX/1kD;LX/11R;Landroid/content/res/Resources;)V
    .locals 1
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 309245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309246
    iput-object p1, p0, LX/1kF;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 309247
    iput-object p2, p0, LX/1kF;->b:LX/0ad;

    .line 309248
    iput-object p3, p0, LX/1kF;->c:LX/1kG;

    .line 309249
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/1kF;->i:Z

    .line 309250
    iput-object p5, p0, LX/1kF;->d:LX/1E1;

    .line 309251
    iput-object p6, p0, LX/1kF;->e:LX/1kI;

    .line 309252
    iput-object p7, p0, LX/1kF;->f:LX/1kD;

    .line 309253
    iput-object p8, p0, LX/1kF;->g:LX/11R;

    .line 309254
    iput-object p9, p0, LX/1kF;->h:Landroid/content/res/Resources;

    .line 309255
    return-void
.end method

.method public static a(LX/1kF;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 309243
    iget-object v0, p0, LX/1kF;->f:LX/1kD;

    invoke-virtual {v0, p2, p1}, LX/1kD;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 309244
    return-void
.end method

.method private static a(J)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 309238
    const-wide/16 v2, 0x0

    cmp-long v1, p0, v2

    if-gtz v1, :cond_1

    .line 309239
    :cond_0
    :goto_0
    return v0

    .line 309240
    :cond_1
    sget-object v1, LX/0SF;->a:LX/0SF;

    move-object v1, v1

    .line 309241
    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v2

    .line 309242
    const-wide/32 v4, 0x5265c00

    sub-long/2addr v2, v4

    cmp-long v1, v2, p0

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(LX/1kF;J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 309256
    iget-object v0, p0, LX/1kF;->g:LX/11R;

    sget-object v1, LX/1lB;->EXACT_TIME_DATE_STYLE:LX/1lB;

    invoke-virtual {v0, v1, p1, p2}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static c(J)D
    .locals 2

    .prologue
    .line 309237
    invoke-static {p0, p1}, LX/1lQ;->a(J)J

    move-result-wide v0

    long-to-double v0, v0

    return-wide v0
.end method

.method private static c(LX/1kF;)Z
    .locals 6

    .prologue
    .line 309235
    sget-object v0, LX/0SF;->a:LX/0SF;

    move-object v0, v0

    .line 309236
    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v0

    invoke-static {p0}, LX/1kF;->d(LX/1kF;)J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(LX/1kF;)J
    .locals 4

    .prologue
    .line 309234
    iget-object v0, p0, LX/1kF;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1kp;->c:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a(LX/1lR;Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLPromptType;)LX/1lR;
    .locals 12
    .param p1    # LX/1lR;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1lR;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLPromptType;",
            ")",
            "LX/1lR;"
        }
    .end annotation

    .prologue
    const-wide/32 v10, 0x5265c00

    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 309191
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPromptType;

    if-ne p3, v0, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 309192
    iget-object v0, p0, LX/1kF;->e:LX/1kI;

    .line 309193
    iget-object v3, v0, LX/1kI;->a:LX/0Uh;

    sget v4, LX/1ko;->a:I

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    move v0, v3

    .line 309194
    if-nez v0, :cond_4

    invoke-static {p0}, LX/1kF;->c(LX/1kF;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    .line 309195
    iget-object v3, p0, LX/1kF;->e:LX/1kI;

    .line 309196
    iget-object v4, v3, LX/1kI;->a:LX/0Uh;

    sget v5, LX/1ko;->b:I

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v4

    move v3, v4

    .line 309197
    if-nez v3, :cond_0

    iget-object v3, p0, LX/1kF;->b:LX/0ad;

    sget-short v4, LX/1Nu;->d:S

    invoke-interface {v3, v4, v0}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    move v0, v0

    .line 309198
    if-nez v0, :cond_4

    .line 309199
    iget-object v0, p0, LX/1kF;->h:Landroid/content/res/Resources;

    const v3, 0x7f081b39

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {p0}, LX/1kF;->d(LX/1kF;)J

    move-result-wide v6

    invoke-static {p0, v6, v7}, LX/1kF;->b(LX/1kF;J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v10, v11}, LX/1kF;->c(J)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLPromptType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/1kF;->a(LX/1kF;Ljava/lang/String;Ljava/lang/String;)V

    .line 309200
    sget-object p1, LX/1lR;->a:LX/1lR;

    .line 309201
    :cond_2
    :goto_1
    return-object p1

    :cond_3
    move v0, v2

    .line 309202
    goto :goto_0

    .line 309203
    :cond_4
    iget-object v0, p0, LX/1kF;->c:LX/1kG;

    invoke-virtual {v0}, LX/1kG;->b()J

    move-result-wide v4

    .line 309204
    invoke-static {v4, v5}, LX/1kF;->a(J)Z

    move-result v0

    if-nez v0, :cond_5

    .line 309205
    iget-object v0, p0, LX/1kF;->h:Landroid/content/res/Resources;

    const v3, 0x7f081b3a

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {p0, v4, v5}, LX/1kF;->b(LX/1kF;J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v2

    invoke-static {v10, v11}, LX/1kF;->c(J)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v6, v1

    invoke-virtual {v0, v3, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLPromptType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/1kF;->a(LX/1kF;Ljava/lang/String;Ljava/lang/String;)V

    .line 309206
    sget-object p1, LX/1lR;->a:LX/1lR;

    goto :goto_1

    .line 309207
    :cond_5
    sget-object v0, LX/0SF;->a:LX/0SF;

    move-object v0, v0

    .line 309208
    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v4

    .line 309209
    iget-object v0, p0, LX/1kF;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/1kp;->d:LX/0Tn;

    const-wide/16 v6, 0x0

    invoke-interface {v0, v3, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 309210
    sub-long/2addr v4, v10

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 309211
    iget-object v0, p0, LX/1kF;->c:LX/1kG;

    sget-object v3, LX/1po;->VIDEO:LX/1po;

    invoke-virtual {v0, v4, v5, v3}, LX/1kG;->a(JLX/1po;)I

    move-result v0

    .line 309212
    iget-object v3, p0, LX/1kF;->c:LX/1kG;

    sget-object v6, LX/1po;->PHOTO:LX/1po;

    invoke-virtual {v3, v4, v5, v6}, LX/1kG;->a(JLX/1po;)I

    move-result v3

    .line 309213
    add-int v4, v3, v0

    .line 309214
    if-lez v4, :cond_8

    const/4 v5, 0x1

    :goto_2
    move v4, v5

    .line 309215
    if-nez v4, :cond_6

    .line 309216
    if-nez p1, :cond_2

    .line 309217
    iget-object v4, p0, LX/1kF;->h:Landroid/content/res/Resources;

    const v5, 0x7f081b3b

    new-array v6, v8, [Ljava/lang/Object;

    add-int/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLPromptType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/1kF;->a(LX/1kF;Ljava/lang/String;Ljava/lang/String;)V

    .line 309218
    sget-object p1, LX/1lR;->a:LX/1lR;

    goto/16 :goto_1

    .line 309219
    :cond_6
    iget-object v1, p0, LX/1kF;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 309220
    sget-object v2, LX/1kp;->e:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v1, v2, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 309221
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 309222
    if-eqz p2, :cond_9

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    .line 309223
    :goto_3
    if-eqz p2, :cond_7

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 309224
    :cond_7
    sget-object v1, LX/1lR;->a:LX/1lR;

    .line 309225
    :goto_4
    move-object p1, v1

    .line 309226
    goto/16 :goto_1

    :cond_8
    const/4 v5, 0x0

    goto :goto_2

    .line 309227
    :cond_9
    iget-object v1, p0, LX/1kF;->c:LX/1kG;

    .line 309228
    iget-object v2, v1, LX/1kG;->h:LX/1kH;

    const/4 p0, 0x1

    const/16 p2, 0x1e

    invoke-virtual {v2, p0, p2}, LX/1kH;->a(ZI)Ljava/util/List;

    move-result-object v2

    move-object p2, v2

    .line 309229
    goto :goto_3

    .line 309230
    :cond_a
    new-instance v1, LX/1lR;

    invoke-direct {v1, p2}, LX/1lR;-><init>(Ljava/util/List;)V

    .line 309231
    iput v0, v1, LX/1lR;->g:I

    .line 309232
    iput v3, v1, LX/1lR;->f:I

    .line 309233
    goto :goto_4
.end method
