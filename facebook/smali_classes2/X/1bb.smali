.class public final enum LX/1bb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1bb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1bb;

.field public static final enum DEFAULT:LX/1bb;

.field public static final enum SMALL:LX/1bb;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 280724
    new-instance v0, LX/1bb;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v2}, LX/1bb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1bb;->SMALL:LX/1bb;

    .line 280725
    new-instance v0, LX/1bb;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v3}, LX/1bb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1bb;->DEFAULT:LX/1bb;

    .line 280726
    const/4 v0, 0x2

    new-array v0, v0, [LX/1bb;

    sget-object v1, LX/1bb;->SMALL:LX/1bb;

    aput-object v1, v0, v2

    sget-object v1, LX/1bb;->DEFAULT:LX/1bb;

    aput-object v1, v0, v3

    sput-object v0, LX/1bb;->$VALUES:[LX/1bb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 280727
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1bb;
    .locals 1

    .prologue
    .line 280728
    const-class v0, LX/1bb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1bb;

    return-object v0
.end method

.method public static values()[LX/1bb;
    .locals 1

    .prologue
    .line 280729
    sget-object v0, LX/1bb;->$VALUES:[LX/1bb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1bb;

    return-object v0
.end method
