.class public LX/1CM;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/graphics/Rect;

.field public final c:[I

.field public final d:Landroid/os/Handler;

.field public final e:Ljava/lang/Runnable;

.field public f:LX/Am6;

.field public g:LX/Am3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 216116
    const-class v0, LX/1CM;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1CM;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 216109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216110
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, LX/1CM;->c:[I

    .line 216111
    new-instance v0, Lcom/facebook/feed/pill/FeedNewStoriesBelowPillUIController$1;

    invoke-direct {v0, p0}, Lcom/facebook/feed/pill/FeedNewStoriesBelowPillUIController$1;-><init>(LX/1CM;)V

    iput-object v0, p0, LX/1CM;->e:Ljava/lang/Runnable;

    .line 216112
    iput-object p1, p0, LX/1CM;->d:Landroid/os/Handler;

    .line 216113
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1CM;->b:Landroid/graphics/Rect;

    .line 216114
    return-void

    .line 216115
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public static a(LX/0QB;)LX/1CM;
    .locals 2

    .prologue
    .line 216106
    new-instance v1, LX/1CM;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    invoke-direct {v1, v0}, LX/1CM;-><init>(Landroid/os/Handler;)V

    .line 216107
    move-object v0, v1

    .line 216108
    return-object v0
.end method

.method public static i(LX/1CM;)Z
    .locals 2

    .prologue
    .line 216105
    iget-object v0, p0, LX/1CM;->g:LX/Am3;

    sget-object v1, LX/Am3;->HIDDEN:LX/Am3;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(LX/1CM;)Z
    .locals 2

    .prologue
    .line 216099
    iget-object v0, p0, LX/1CM;->g:LX/Am3;

    sget-object v1, LX/Am3;->SHOWING_SPINNER:LX/Am3;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k(LX/1CM;)Z
    .locals 2

    .prologue
    .line 216104
    iget-object v0, p0, LX/1CM;->g:LX/Am3;

    sget-object v1, LX/Am3;->SHOWING_LOADING_TEXT:LX/Am3;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final c()V
    .locals 2

    .prologue
    .line 216100
    invoke-static {p0}, LX/1CM;->i(LX/1CM;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216101
    :goto_0
    return-void

    .line 216102
    :cond_0
    sget-object v0, LX/Am3;->HIDDEN:LX/Am3;

    iput-object v0, p0, LX/1CM;->g:LX/Am3;

    .line 216103
    iget-object v0, p0, LX/1CM;->f:LX/Am6;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/Am6;->setVisibility(I)V

    goto :goto_0
.end method
