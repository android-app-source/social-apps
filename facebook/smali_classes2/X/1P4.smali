.class public LX/1P4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/03V;

.field private b:LX/1P1;

.field private c:LX/1P5;


# direct methods
.method public constructor <init>(LX/1P1;)V
    .locals 0

    .prologue
    .line 243570
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243571
    iput-object p1, p0, LX/1P4;->b:LX/1P1;

    .line 243572
    return-void
.end method

.method private a(IIZ)Landroid/view/View;
    .locals 6

    .prologue
    .line 243573
    iget-object v0, p0, LX/1P4;->b:LX/1P1;

    invoke-virtual {v0}, LX/1OR;->r()Z

    move-result v1

    .line 243574
    if-eqz v1, :cond_0

    iget-object v0, p0, LX/1P4;->c:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->c()I

    move-result v0

    move v3, v0

    .line 243575
    :goto_0
    if-eqz v1, :cond_1

    iget-object v0, p0, LX/1P4;->c:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->d()I

    move-result v0

    move v2, v0

    .line 243576
    :goto_1
    if-le p2, p1, :cond_2

    const/4 v0, 0x1

    .line 243577
    :goto_2
    if-eq p1, p2, :cond_5

    .line 243578
    iget-object v1, p0, LX/1P4;->b:LX/1P1;

    invoke-virtual {v1, p1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v1

    .line 243579
    if-eqz v1, :cond_4

    .line 243580
    iget-object v4, p0, LX/1P4;->c:LX/1P5;

    invoke-virtual {v4, v1}, LX/1P5;->a(Landroid/view/View;)I

    move-result v4

    .line 243581
    iget-object v5, p0, LX/1P4;->c:LX/1P5;

    invoke-virtual {v5, v1}, LX/1P5;->b(Landroid/view/View;)I

    move-result v5

    .line 243582
    if-ge v4, v2, :cond_4

    if-lt v5, v3, :cond_4

    .line 243583
    if-eqz p3, :cond_3

    .line 243584
    if-lt v4, v3, :cond_4

    if-gt v5, v2, :cond_4

    move-object v0, v1

    .line 243585
    :goto_3
    return-object v0

    .line 243586
    :cond_0
    const/4 v0, 0x0

    move v3, v0

    goto :goto_0

    .line 243587
    :cond_1
    iget-object v0, p0, LX/1P4;->c:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->e()I

    move-result v0

    move v2, v0

    goto :goto_1

    .line 243588
    :cond_2
    const/4 v0, -0x1

    goto :goto_2

    :cond_3
    move-object v0, v1

    .line 243589
    goto :goto_3

    .line 243590
    :cond_4
    add-int/2addr p1, v0

    goto :goto_2

    .line 243591
    :cond_5
    const/4 v0, 0x0

    goto :goto_3
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 243592
    iget-object v0, p0, LX/1P4;->c:LX/1P5;

    if-nez v0, :cond_0

    .line 243593
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setOrientation call must be passed from the LayoutManager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 243594
    :cond_0
    iget-object v0, p0, LX/1P4;->b:LX/1P1;

    invoke-virtual {v0}, LX/1OR;->v()I

    move-result v0

    invoke-direct {p0, v1, v0, v1}, LX/1P4;->a(IIZ)Landroid/view/View;

    move-result-object v0

    .line 243595
    if-nez v0, :cond_1

    .line 243596
    const/4 v0, -0x1

    .line 243597
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    invoke-virtual {v0}, LX/1a3;->f()I

    move-result v0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 243598
    iget-object v0, p0, LX/1P4;->b:LX/1P1;

    invoke-static {v0, p1}, LX/1P5;->a(LX/1OR;I)LX/1P5;

    move-result-object v0

    iput-object v0, p0, LX/1P4;->c:LX/1P5;

    .line 243599
    return-void
.end method
