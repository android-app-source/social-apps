.class public LX/1q8;
.super LX/1ln;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/1FL;

.field public b:LX/52j;


# direct methods
.method public constructor <init>(LX/1FL;)V
    .locals 1

    .prologue
    .line 330107
    invoke-direct {p0}, LX/1ln;-><init>()V

    .line 330108
    invoke-static {p1}, LX/1FL;->a(LX/1FL;)LX/1FL;

    move-result-object v0

    iput-object v0, p0, LX/1q8;->a:LX/1FL;

    .line 330109
    iget-object v0, p0, LX/1q8;->a:LX/1FL;

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 330110
    const-string v0, "gifdrawable"

    invoke-static {v0}, LX/02C;->a(Ljava/lang/String;)V

    .line 330111
    :try_start_0
    invoke-virtual {p1}, LX/1FL;->b()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->a(Ljava/io/InputStream;)LX/52j;

    move-result-object v0

    iput-object v0, p0, LX/1q8;->b:LX/52j;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 330112
    return-void

    .line 330113
    :catch_0
    move-exception v0

    .line 330114
    invoke-static {v0}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public final b()I
    .locals 2

    .prologue
    .line 330091
    iget-object v0, p0, LX/1q8;->a:LX/1FL;

    invoke-virtual {v0}, LX/1FL;->a()LX/1FJ;

    move-result-object v1

    .line 330092
    :try_start_0
    invoke-virtual {p0}, LX/1ln;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v1}, LX/1FJ;->a(LX/1FJ;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 330093
    :goto_0
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    return v0

    .line 330094
    :cond_1
    :try_start_1
    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    invoke-virtual {v0}, LX/1FK;->a()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 330095
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 330090
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1q8;->b:LX/52j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 330096
    monitor-enter p0

    .line 330097
    :try_start_0
    iget-object v0, p0, LX/1q8;->b:LX/52j;

    if-nez v0, :cond_0

    .line 330098
    monitor-exit p0

    .line 330099
    :goto_0
    return-void

    .line 330100
    :cond_0
    iget-object v0, p0, LX/1q8;->b:LX/52j;

    .line 330101
    const/4 v1, 0x0

    iput-object v1, p0, LX/1q8;->b:LX/52j;

    .line 330102
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330103
    :try_start_1
    invoke-static {v0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->a(LX/52j;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 330104
    iget-object v0, p0, LX/1q8;->a:LX/1FL;

    invoke-virtual {v0}, LX/1FL;->close()V

    goto :goto_0

    .line 330105
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 330106
    :catchall_1
    move-exception v0

    iget-object v1, p0, LX/1q8;->a:LX/1FL;

    invoke-virtual {v1}, LX/1FL;->close()V

    throw v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 330083
    const/4 v0, 0x1

    return v0
.end method

.method public final g()I
    .locals 2

    .prologue
    .line 330084
    invoke-virtual {p0}, LX/1ln;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1q8;->b:LX/52j;

    .line 330085
    iget-object v1, v0, LX/52j;->b:[I

    const/4 p0, 0x0

    aget v1, v1, p0

    move v0, v1

    .line 330086
    goto :goto_0
.end method

.method public final h()I
    .locals 2

    .prologue
    .line 330087
    invoke-virtual {p0}, LX/1ln;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1q8;->b:LX/52j;

    .line 330088
    iget-object v1, v0, LX/52j;->b:[I

    const/4 p0, 0x1

    aget v1, v1, p0

    move v0, v1

    .line 330089
    goto :goto_0
.end method
