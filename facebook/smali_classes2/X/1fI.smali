.class public LX/1fI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1fI;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1fM;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/1fJ;

.field private final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Ot;LX/1fJ;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1fM;",
            ">;",
            "LX/1fJ;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 291218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291219
    iput-object p1, p0, LX/1fI;->a:LX/0Ot;

    .line 291220
    iput-object p2, p0, LX/1fI;->b:LX/1fJ;

    .line 291221
    iput-object p3, p0, LX/1fI;->c:LX/0ad;

    .line 291222
    return-void
.end method

.method public static a(LX/0QB;)LX/1fI;
    .locals 6

    .prologue
    .line 291205
    sget-object v0, LX/1fI;->d:LX/1fI;

    if-nez v0, :cond_1

    .line 291206
    const-class v1, LX/1fI;

    monitor-enter v1

    .line 291207
    :try_start_0
    sget-object v0, LX/1fI;->d:LX/1fI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 291208
    if-eqz v2, :cond_0

    .line 291209
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 291210
    new-instance v5, LX/1fI;

    const/16 v3, 0xf5

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/1fJ;->a(LX/0QB;)LX/1fJ;

    move-result-object v3

    check-cast v3, LX/1fJ;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {v5, p0, v3, v4}, LX/1fI;-><init>(LX/0Ot;LX/1fJ;LX/0ad;)V

    .line 291211
    move-object v0, v5

    .line 291212
    sput-object v0, LX/1fI;->d:LX/1fI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291213
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 291214
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 291215
    :cond_1
    sget-object v0, LX/1fI;->d:LX/1fI;

    return-object v0

    .line 291216
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 291217
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1fI;Lcom/facebook/graphql/model/FeedUnit;II)V
    .locals 3

    .prologue
    .line 291202
    new-instance v1, LX/69f;

    iget-object v0, p0, LX/1fI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fM;

    new-instance v2, LX/69e;

    invoke-direct {v2, p1, p2, p3}, LX/69e;-><init>(Lcom/facebook/graphql/model/FeedUnit;II)V

    invoke-direct {v1, v0, v2}, LX/69f;-><init>(LX/1fM;LX/1fd;)V

    .line 291203
    iget-object v0, p0, LX/1fI;->b:LX/1fJ;

    invoke-virtual {v0, v1}, LX/1fJ;->a(LX/1fL;)V

    .line 291204
    return-void
.end method

.method public static a(LX/1fI;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 291199
    new-instance v1, LX/30z;

    iget-object v0, p0, LX/1fI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fM;

    new-instance v2, LX/310;

    invoke-direct {v2, p1, p2, p3}, LX/310;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-direct {v1, v0, v2}, LX/30z;-><init>(LX/1fM;LX/1fd;)V

    .line 291200
    iget-object v0, p0, LX/1fI;->b:LX/1fJ;

    invoke-virtual {v0, v1}, LX/1fJ;->a(LX/1fL;)V

    .line 291201
    return-void
.end method

.method public static a(LX/1fI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 291223
    new-instance v1, LX/69S;

    iget-object v0, p0, LX/1fI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fM;

    new-instance v2, LX/69Z;

    invoke-direct {v2, p1, p2, p3}, LX/69Z;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v0, v2}, LX/69S;-><init>(LX/1fM;LX/1fd;)V

    .line 291224
    iget-object v0, p0, LX/1fI;->b:LX/1fJ;

    invoke-virtual {v0, v1}, LX/1fJ;->a(LX/1fL;)V

    .line 291225
    return-void
.end method

.method public static a(LX/1fI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 291196
    new-instance v1, LX/69U;

    iget-object v0, p0, LX/1fI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fM;

    new-instance v2, LX/69b;

    invoke-direct {v2, p1, p2, p3, p4}, LX/69b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-direct {v1, v0, v2}, LX/69U;-><init>(LX/1fM;LX/1fd;)V

    .line 291197
    iget-object v0, p0, LX/1fI;->b:LX/1fJ;

    invoke-virtual {v0, v1}, LX/1fJ;->a(LX/1fL;)V

    .line 291198
    return-void
.end method

.method public static b(LX/1fI;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 291193
    new-instance v1, LX/69V;

    iget-object v0, p0, LX/1fI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fM;

    new-instance v2, LX/69c;

    invoke-direct {v2, p1, p2, p3}, LX/69c;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-direct {v1, v0, v2}, LX/69V;-><init>(LX/1fM;LX/1fd;)V

    .line 291194
    iget-object v0, p0, LX/1fI;->b:LX/1fJ;

    invoke-virtual {v0, v1}, LX/1fJ;->a(LX/1fL;)V

    .line 291195
    return-void
.end method

.method public static b(LX/1fI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 291190
    new-instance v1, LX/1fK;

    iget-object v0, p0, LX/1fI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fM;

    new-instance v2, LX/1fc;

    invoke-direct {v2, p1, p2, p3}, LX/1fc;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v0, v2}, LX/1fK;-><init>(LX/1fM;LX/1fd;)V

    .line 291191
    iget-object v0, p0, LX/1fI;->b:LX/1fJ;

    invoke-virtual {v0, v1}, LX/1fJ;->a(LX/1fL;)V

    .line 291192
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 291187
    new-instance v1, LX/69R;

    iget-object v0, p0, LX/1fI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fM;

    new-instance v2, LX/69Y;

    invoke-direct {v2, p1}, LX/69Y;-><init>(LX/0Px;)V

    invoke-direct {v1, v0, v2}, LX/69R;-><init>(LX/1fM;LX/1fd;)V

    .line 291188
    iget-object v0, p0, LX/1fI;->b:LX/1fJ;

    invoke-virtual {v0, v1}, LX/1fJ;->a(LX/1fL;)V

    .line 291189
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[B)V
    .locals 10

    .prologue
    .line 291184
    new-instance v9, LX/3dT;

    iget-object v0, p0, LX/1fI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/1fM;

    new-instance v0, LX/3dU;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v7}, LX/3dU;-><init>(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[B)V

    invoke-direct {v9, v8, v0}, LX/3dT;-><init>(LX/1fM;LX/1fd;)V

    .line 291185
    iget-object v0, p0, LX/1fI;->b:LX/1fJ;

    invoke-virtual {v0, v9}, LX/1fJ;->a(LX/1fL;)V

    .line 291186
    return-void
.end method
