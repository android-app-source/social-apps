.class public LX/1HF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:I

.field public final b:Z

.field private final c:Z

.field public final d:Z

.field public final e:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/4ec;

.field private final g:Z

.field public final h:Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;

.field private final i:Z

.field public final j:Z


# direct methods
.method public constructor <init>(LX/1H9;LX/1H8;)V
    .locals 1

    .prologue
    .line 226624
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226625
    iget v0, p1, LX/1H9;->b:I

    iput v0, p0, LX/1HF;->a:I

    .line 226626
    iget-boolean v0, p1, LX/1H9;->c:Z

    iput-boolean v0, p0, LX/1HF;->b:Z

    .line 226627
    iget-boolean v0, p2, LX/1H8;->f:Z

    move v0, v0

    .line 226628
    if-eqz v0, :cond_0

    iget-boolean v0, p1, LX/1H9;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/1HF;->c:Z

    .line 226629
    iget-boolean v0, p1, LX/1H9;->e:Z

    iput-boolean v0, p0, LX/1HF;->d:Z

    .line 226630
    iget-object v0, p1, LX/1H9;->f:LX/1Gd;

    if-eqz v0, :cond_1

    .line 226631
    iget-object v0, p1, LX/1H9;->f:LX/1Gd;

    iput-object v0, p0, LX/1HF;->e:LX/1Gd;

    .line 226632
    :goto_1
    iget-object v0, p1, LX/1H9;->g:LX/4ec;

    iput-object v0, p0, LX/1HF;->f:LX/4ec;

    .line 226633
    iget-boolean v0, p1, LX/1H9;->h:Z

    iput-boolean v0, p0, LX/1HF;->g:Z

    .line 226634
    iget-object v0, p1, LX/1H9;->i:Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;

    iput-object v0, p0, LX/1HF;->h:Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;

    .line 226635
    iget-boolean v0, p1, LX/1H9;->j:Z

    iput-boolean v0, p0, LX/1HF;->i:Z

    .line 226636
    iget-boolean v0, p1, LX/1H9;->k:Z

    iput-boolean v0, p0, LX/1HF;->j:Z

    .line 226637
    return-void

    .line 226638
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 226639
    :cond_1
    new-instance v0, LX/4dz;

    invoke-direct {v0, p0}, LX/4dz;-><init>(LX/1HF;)V

    iput-object v0, p0, LX/1HF;->e:LX/1Gd;

    goto :goto_1
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 226640
    iget-boolean v0, p0, LX/1HF;->c:Z

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 226641
    iget v0, p0, LX/1HF;->a:I

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 226642
    iget-boolean v0, p0, LX/1HF;->g:Z

    return v0
.end method
