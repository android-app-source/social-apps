.class public LX/13O;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/13O;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 176657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176658
    iput-object p1, p0, LX/13O;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 176659
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/13O;->b:Ljava/util/WeakHashMap;

    .line 176660
    return-void
.end method

.method public static a(LX/0QB;)LX/13O;
    .locals 4

    .prologue
    .line 176661
    sget-object v0, LX/13O;->c:LX/13O;

    if-nez v0, :cond_1

    .line 176662
    const-class v1, LX/13O;

    monitor-enter v1

    .line 176663
    :try_start_0
    sget-object v0, LX/13O;->c:LX/13O;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 176664
    if-eqz v2, :cond_0

    .line 176665
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 176666
    new-instance p0, LX/13O;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/13O;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 176667
    move-object v0, p0

    .line 176668
    sput-object v0, LX/13O;->c:LX/13O;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 176669
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 176670
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 176671
    :cond_1
    sget-object v0, LX/13O;->c:LX/13O;

    return-object v0

    .line 176672
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 176673
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static d(Ljava/lang/String;)LX/0Tn;
    .locals 2

    .prologue
    .line 176674
    sget-object v0, LX/2g0;->b:LX/0Tn;

    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static e(LX/13O;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 176680
    iget-object v0, p0, LX/13O;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 176681
    :cond_0
    return-void
.end method

.method public static f(Ljava/lang/String;Ljava/lang/String;)LX/0Tn;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 176675
    invoke-static {p0}, LX/13O;->d(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 176676
    if-eqz p1, :cond_0

    .line 176677
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 176678
    :cond_0
    const-string v1, "/count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 176679
    return-object v0
.end method

.method public static g(Ljava/lang/String;Ljava/lang/String;)LX/0Tn;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 176632
    invoke-static {p0}, LX/13O;->d(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 176633
    if-eqz p1, :cond_0

    .line 176634
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 176635
    :cond_0
    const-string v1, "/timestamp"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 176636
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 176655
    invoke-static {p1, p2}, LX/13O;->f(Ljava/lang/String;Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 176656
    iget-object v1, p0, LX/13O;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 176654
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/13O;->b(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)J
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 176652
    invoke-static {p1, p2}, LX/13O;->g(Ljava/lang/String;Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 176653
    iget-object v1, p0, LX/13O;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-wide/16 v2, 0x0

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 176650
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/13O;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 176651
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 176643
    invoke-virtual {p0, p1, p2}, LX/13O;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 176644
    iget-object v1, p0, LX/13O;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 176645
    invoke-static {p1, p2}, LX/13O;->f(Ljava/lang/String;Ljava/lang/String;)LX/0Tn;

    move-result-object v2

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 176646
    invoke-static {p1, p2}, LX/13O;->g(Ljava/lang/String;Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v1, v0, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 176647
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 176648
    invoke-static {p0, p1, p2}, LX/13O;->e(LX/13O;Ljava/lang/String;Ljava/lang/String;)V

    .line 176649
    return-void
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 176637
    iget-object v0, p0, LX/13O;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 176638
    invoke-static {p1, p2}, LX/13O;->f(Ljava/lang/String;Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 176639
    invoke-static {p1, p2}, LX/13O;->g(Ljava/lang/String;Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 176640
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 176641
    invoke-static {p0, p1, p2}, LX/13O;->e(LX/13O;Ljava/lang/String;Ljava/lang/String;)V

    .line 176642
    return-void
.end method
