.class public LX/0ii;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0ii;


# instance fields
.field private final a:LX/15q;

.field private final b:LX/0gh;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/15q;LX/0gh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 122062
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122063
    iput-object p1, p0, LX/0ii;->a:LX/15q;

    .line 122064
    iput-object p2, p0, LX/0ii;->b:LX/0gh;

    .line 122065
    return-void
.end method

.method public static a(LX/0QB;)LX/0ii;
    .locals 5

    .prologue
    .line 122066
    sget-object v0, LX/0ii;->d:LX/0ii;

    if-nez v0, :cond_1

    .line 122067
    const-class v1, LX/0ii;

    monitor-enter v1

    .line 122068
    :try_start_0
    sget-object v0, LX/0ii;->d:LX/0ii;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 122069
    if-eqz v2, :cond_0

    .line 122070
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 122071
    new-instance p0, LX/0ii;

    invoke-static {v0}, LX/15q;->a(LX/0QB;)LX/15q;

    move-result-object v3

    check-cast v3, LX/15q;

    invoke-static {v0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v4

    check-cast v4, LX/0gh;

    invoke-direct {p0, v3, v4}, LX/0ii;-><init>(LX/15q;LX/0gh;)V

    .line 122072
    move-object v0, p0

    .line 122073
    sput-object v0, LX/0ii;->d:LX/0ii;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 122074
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 122075
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 122076
    :cond_1
    sget-object v0, LX/0ii;->d:LX/0ii;

    return-object v0

    .line 122077
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 122078
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 122079
    iget-object v0, p0, LX/0ii;->a:LX/15q;

    invoke-virtual {v0}, LX/15q;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122080
    iget-object v0, p0, LX/0ii;->b:LX/0gh;

    const-string v1, "notification_logging_data"

    .line 122081
    iget-object v2, v0, LX/0gh;->u:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 122082
    const/4 v0, 0x0

    iput-object v0, p0, LX/0ii;->c:Ljava/lang/String;

    .line 122083
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V
    .locals 3

    .prologue
    .line 122084
    iget-object v0, p0, LX/0ii;->a:LX/15q;

    invoke-virtual {v0}, LX/15q;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122085
    invoke-virtual {p0}, LX/0ii;->a()V

    .line 122086
    if-eqz p1, :cond_0

    .line 122087
    iget-object v0, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->i:Ljava/lang/String;

    move-object v0, v0

    .line 122088
    iput-object v0, p0, LX/0ii;->c:Ljava/lang/String;

    .line 122089
    iget-object v0, p0, LX/0ii;->b:LX/0gh;

    const-string v1, "notification_logging_data"

    iget-object v2, p0, LX/0ii;->c:Ljava/lang/String;

    .line 122090
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_1

    if-eqz v2, :cond_1

    .line 122091
    iget-object p0, v0, LX/0gh;->u:Ljava/util/Map;

    invoke-interface {p0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122092
    :cond_0
    :goto_0
    return-void

    :cond_1
    goto :goto_0
.end method
