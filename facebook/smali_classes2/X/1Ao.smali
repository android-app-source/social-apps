.class public LX/1Ao;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/1Ao;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 211188
    const/4 v0, 0x0

    sput-object v0, LX/1Ao;->a:LX/1Ao;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 211159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211160
    return-void
.end method

.method public static declared-synchronized a()LX/1Ao;
    .locals 2

    .prologue
    .line 211183
    const-class v1, LX/1Ao;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1Ao;->a:LX/1Ao;

    if-nez v0, :cond_0

    .line 211184
    new-instance v0, LX/1Ao;

    invoke-direct {v0}, LX/1Ao;-><init>()V

    sput-object v0, LX/1Ao;->a:LX/1Ao;

    .line 211185
    :cond_0
    sget-object v0, LX/1Ao;->a:LX/1Ao;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 211186
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(LX/1bf;Ljava/lang/Object;)LX/1bh;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 211176
    new-instance v0, LX/1bg;

    .line 211177
    iget-object v1, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 211178
    invoke-virtual {p0, v1}, LX/1Ao;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 211179
    iget-object v2, p1, LX/1bf;->h:LX/1o9;

    move-object v2, v2

    .line 211180
    iget-object v3, p1, LX/1bf;->i:LX/1bd;

    move-object v3, v3

    .line 211181
    iget-object v4, p1, LX/1bf;->g:LX/1bZ;

    move-object v4, v4

    .line 211182
    move-object v6, v5

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, LX/1bg;-><init>(Ljava/lang/String;LX/1o9;LX/1bd;LX/1bZ;LX/1bh;Ljava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public a(Landroid/net/Uri;Ljava/lang/Object;)LX/1bh;
    .locals 2

    .prologue
    .line 211187
    new-instance v0, LX/1ed;

    invoke-virtual {p0, p1}, LX/1Ao;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1ed;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 211175
    return-object p1
.end method

.method public b(LX/1bf;Ljava/lang/Object;)LX/1bh;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 211163
    iget-object v0, p1, LX/1bf;->m:LX/33B;

    move-object v0, v0

    .line 211164
    if-eqz v0, :cond_0

    .line 211165
    invoke-interface {v0}, LX/33B;->a()LX/1bh;

    move-result-object v5

    .line 211166
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    .line 211167
    :goto_0
    new-instance v0, LX/1bg;

    .line 211168
    iget-object v1, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 211169
    invoke-virtual {p0, v1}, LX/1Ao;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 211170
    iget-object v2, p1, LX/1bf;->h:LX/1o9;

    move-object v2, v2

    .line 211171
    iget-object v3, p1, LX/1bf;->i:LX/1bd;

    move-object v3, v3

    .line 211172
    iget-object v4, p1, LX/1bf;->g:LX/1bZ;

    move-object v4, v4

    .line 211173
    move-object v7, p2

    invoke-direct/range {v0 .. v7}, LX/1bg;-><init>(Ljava/lang/String;LX/1o9;LX/1bd;LX/1bZ;LX/1bh;Ljava/lang/String;Ljava/lang/Object;)V

    return-object v0

    :cond_0
    move-object v5, v6

    .line 211174
    goto :goto_0
.end method

.method public final c(LX/1bf;Ljava/lang/Object;)LX/1bh;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 211161
    iget-object v0, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 211162
    invoke-virtual {p0, v0, p2}, LX/1Ao;->a(Landroid/net/Uri;Ljava/lang/Object;)LX/1bh;

    move-result-object v0

    return-object v0
.end method
