.class public LX/1K9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Object;

.field private static volatile e:LX/1K9;


# instance fields
.field private final b:LX/0Xq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xq",
            "<",
            "Landroid/util/Pair;",
            "LX/6Ve",
            "<*>;>;"
        }
    .end annotation
.end field

.field public c:Z

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1KJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 231063
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/1K9;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 231059
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231060
    invoke-static {}, LX/0Xq;->t()LX/0Xq;

    move-result-object v0

    iput-object v0, p0, LX/1K9;->b:LX/0Xq;

    .line 231061
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/1K9;->d:Ljava/util/List;

    .line 231062
    return-void
.end method

.method public static a(LX/0QB;)LX/1K9;
    .locals 3

    .prologue
    .line 231047
    sget-object v0, LX/1K9;->e:LX/1K9;

    if-nez v0, :cond_1

    .line 231048
    const-class v1, LX/1K9;

    monitor-enter v1

    .line 231049
    :try_start_0
    sget-object v0, LX/1K9;->e:LX/1K9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 231050
    if-eqz v2, :cond_0

    .line 231051
    :try_start_1
    new-instance v0, LX/1K9;

    invoke-direct {v0}, LX/1K9;-><init>()V

    .line 231052
    move-object v0, v0

    .line 231053
    sput-object v0, LX/1K9;->e:LX/1K9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 231054
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 231055
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 231056
    :cond_1
    sget-object v0, LX/1K9;->e:LX/1K9;

    return-object v0

    .line 231057
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 231058
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;)Landroid/util/Pair;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1KJ",
            "<TK;>;K:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;TK;)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Class",
            "<TE;>;*>;"
        }
    .end annotation

    .prologue
    .line 231044
    if-nez p1, :cond_0

    .line 231045
    sget-object v0, LX/1K9;->a:Ljava/lang/Object;

    invoke-static {p0, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 231046
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/1K9;LX/6Ve;Landroid/util/Pair;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "LX/6Ve",
            "<TE;>;",
            "Landroid/util/Pair;",
            ")V"
        }
    .end annotation

    .prologue
    .line 231042
    iget-object v0, p0, LX/1K9;->b:LX/0Xq;

    invoke-virtual {v0, p2, p1}, LX/0Xs;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 231043
    return-void
.end method

.method private a(LX/1KJ;Ljava/lang/Object;)V
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1KJ",
            "<TK;>;K:",
            "Ljava/lang/Object;",
            ">(TE;TK;)V"
        }
    .end annotation

    .prologue
    .line 231035
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, p2}, LX/1K9;->a(Ljava/lang/Class;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 231036
    iget-object v1, p0, LX/1K9;->b:LX/0Xq;

    invoke-virtual {v1, v0}, LX/0Xq;->f(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 231037
    iget-object v1, p0, LX/1K9;->b:LX/0Xq;

    invoke-virtual {v1, v0}, LX/0Xr;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 231038
    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 231039
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Ve;

    invoke-interface {v0, p1}, LX/6Ve;->a(Ljava/lang/Object;)V

    .line 231040
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 231041
    :cond_0
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 231064
    iget-boolean v0, p0, LX/1K9;->c:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 231065
    :goto_0
    :try_start_0
    iget-object v0, p0, LX/1K9;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 231066
    iget-object v0, p0, LX/1K9;->d:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1KJ;

    .line 231067
    instance-of v1, v0, LX/6Vf;

    if-eqz v1, :cond_1

    .line 231068
    check-cast v0, LX/6Vf;

    .line 231069
    iget-object v1, v0, LX/6Vf;->a:Landroid/util/Pair;

    move-object v1, v1

    .line 231070
    iget-object v3, v0, LX/6Vf;->b:LX/6Ve;

    move-object v3, v3

    .line 231071
    invoke-static {p0, v3, v1}, LX/1K9;->a(LX/1K9;LX/6Ve;Landroid/util/Pair;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 231072
    :goto_1
    goto :goto_0

    .line 231073
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1K9;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 231074
    iput-boolean v2, p0, LX/1K9;->c:Z

    throw v0

    .line 231075
    :cond_0
    iget-object v0, p0, LX/1K9;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 231076
    iput-boolean v2, p0, LX/1K9;->c:Z

    .line 231077
    return-void

    .line 231078
    :cond_1
    instance-of v1, v0, LX/6Vg;

    if-eqz v1, :cond_2

    .line 231079
    invoke-interface {v0}, LX/1KJ;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Vi;

    invoke-static {p0, v1}, LX/1K9;->b(LX/1K9;LX/6Vi;)V

    goto :goto_1

    .line 231080
    :cond_2
    invoke-static {p0, v0}, LX/1K9;->b(LX/1K9;LX/1KJ;)V

    goto :goto_1
.end method

.method public static b(LX/1K9;LX/1KJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1KJ",
            "<TK;>;K:",
            "Ljava/lang/Object;",
            ">(TE;)V"
        }
    .end annotation

    .prologue
    .line 231031
    invoke-interface {p1}, LX/1KJ;->c()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 231032
    invoke-interface {p1}, LX/1KJ;->c()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/1K9;->a(LX/1KJ;Ljava/lang/Object;)V

    .line 231033
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/1K9;->a(LX/1KJ;Ljava/lang/Object;)V

    .line 231034
    return-void
.end method

.method public static b(LX/1K9;LX/6Vi;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(",
            "LX/6Vi",
            "<+",
            "LX/1KJ",
            "<TK;>;TK;>;)V"
        }
    .end annotation

    .prologue
    .line 231028
    iget-object v0, p1, LX/6Vi;->a:Ljava/lang/Class;

    iget-object v1, p1, LX/6Vi;->b:Ljava/lang/Object;

    invoke-static {v0, v1}, LX/1K9;->a(Ljava/lang/Class;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 231029
    iget-object v1, p0, LX/1K9;->b:LX/0Xq;

    iget-object v2, p1, LX/6Vi;->c:LX/6Ve;

    invoke-virtual {v1, v0, v2}, LX/0Xq;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 231030
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;LX/6Ve;)LX/6Vi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1KJ",
            "<TK;>;K:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "LX/6Ve",
            "<TE;>;)",
            "LX/6Vi",
            "<TE;TK;>;"
        }
    .end annotation

    .prologue
    .line 231009
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, LX/1K9;->a(Ljava/lang/Class;Ljava/lang/Object;LX/6Ve;)LX/6Vi;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/Object;LX/6Ve;)LX/6Vi;
    .locals 3
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1KJ",
            "<TK;>;K:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;TK;",
            "LX/6Ve",
            "<TE;>;)",
            "LX/6Vi",
            "<TE;TK;>;"
        }
    .end annotation

    .prologue
    .line 231023
    invoke-static {p1, p2}, LX/1K9;->a(Ljava/lang/Class;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 231024
    iget-boolean v1, p0, LX/1K9;->c:Z

    if-nez v1, :cond_0

    .line 231025
    invoke-static {p0, p3, v0}, LX/1K9;->a(LX/1K9;LX/6Ve;Landroid/util/Pair;)V

    .line 231026
    :goto_0
    new-instance v0, LX/6Vi;

    invoke-direct {v0, p1, p2, p3}, LX/6Vi;-><init>(Ljava/lang/Class;Ljava/lang/Object;LX/6Ve;)V

    return-object v0

    .line 231027
    :cond_0
    new-instance v1, LX/6Vf;

    invoke-direct {v1, v0, p3}, LX/6Vf;-><init>(Landroid/util/Pair;LX/6Ve;)V

    invoke-virtual {p0, v1}, LX/1K9;->a(LX/1KJ;)V

    goto :goto_0
.end method

.method public final a(LX/1KJ;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1KJ",
            "<TK;>;K:",
            "Ljava/lang/Object;",
            ">(TE;)V"
        }
    .end annotation

    .prologue
    .line 231014
    iget-boolean v0, p0, LX/1K9;->c:Z

    if-eqz v0, :cond_0

    .line 231015
    iget-object v0, p0, LX/1K9;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231016
    :goto_0
    return-void

    .line 231017
    :cond_0
    const/4 v1, 0x1

    .line 231018
    iget-boolean v0, p0, LX/1K9;->c:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 231019
    iput-boolean v1, p0, LX/1K9;->c:Z

    .line 231020
    :try_start_0
    invoke-static {p0, p1}, LX/1K9;->b(LX/1K9;LX/1KJ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 231021
    invoke-direct {p0}, LX/1K9;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, LX/1K9;->b()V

    throw v0

    .line 231022
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(LX/6Vi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(",
            "LX/6Vi",
            "<+",
            "LX/1KJ",
            "<TK;>;TK;>;)V"
        }
    .end annotation

    .prologue
    .line 231010
    iget-boolean v0, p0, LX/1K9;->c:Z

    if-nez v0, :cond_0

    .line 231011
    invoke-static {p0, p1}, LX/1K9;->b(LX/1K9;LX/6Vi;)V

    .line 231012
    :goto_0
    return-void

    .line 231013
    :cond_0
    new-instance v0, LX/6Vg;

    invoke-direct {v0, p1}, LX/6Vg;-><init>(LX/6Vi;)V

    invoke-virtual {p0, v0}, LX/1K9;->a(LX/1KJ;)V

    goto :goto_0
.end method
