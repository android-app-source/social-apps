.class public LX/1PN;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hk;
.implements LX/0fm;
.implements LX/0fs;


# instance fields
.field private final a:LX/1PO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1PO",
            "<",
            "LX/1Py;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/1Py;


# direct methods
.method public constructor <init>(LX/1PO;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 243973
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 243974
    iput-object p1, p0, LX/1PN;->a:LX/1PO;

    .line 243975
    return-void
.end method


# virtual methods
.method public final a(LX/1Qr;LX/0g8;LX/1Pf;)V
    .locals 0

    .prologue
    .line 243976
    check-cast p3, LX/1Py;

    iput-object p3, p0, LX/1PN;->b:LX/1Py;

    .line 243977
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 243978
    iget-object v0, p0, LX/1PN;->a:LX/1PO;

    .line 243979
    invoke-static {v0}, LX/1PO;->d(LX/1PO;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 243980
    :goto_0
    return-void

    .line 243981
    :cond_0
    iget-object v1, v0, LX/1PO;->a:LX/0bH;

    iget-object v2, v0, LX/1PO;->d:LX/1PP;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 243982
    iget-object v1, v0, LX/1PO;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v1

    iput-wide v1, v0, LX/1PO;->e:J

    .line 243983
    const-wide/16 v1, -0x1

    iput-wide v1, v0, LX/1PO;->f:J

    goto :goto_0
.end method

.method public final c()V
    .locals 7

    .prologue
    .line 243984
    iget-object v0, p0, LX/1PN;->a:LX/1PO;

    iget-object v1, p0, LX/1PN;->b:LX/1Py;

    .line 243985
    invoke-static {v0}, LX/1PO;->d(LX/1PO;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 243986
    :cond_0
    :goto_0
    return-void

    .line 243987
    :cond_1
    iget-wide v3, v0, LX/1PO;->f:J

    iget-wide v5, v0, LX/1PO;->e:J

    cmp-long v3, v3, v5

    if-lez v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    move v2, v3

    .line 243988
    if-eqz v2, :cond_0

    .line 243989
    invoke-interface {v1}, LX/1Py;->n()V

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 243990
    iget-object v0, p0, LX/1PN;->a:LX/1PO;

    .line 243991
    invoke-static {v0}, LX/1PO;->d(LX/1PO;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 243992
    :goto_0
    return-void

    .line 243993
    :cond_0
    iget-object v1, v0, LX/1PO;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v1

    iput-wide v1, v0, LX/1PO;->e:J

    goto :goto_0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 243994
    iget-object v0, p0, LX/1PN;->a:LX/1PO;

    .line 243995
    invoke-static {v0}, LX/1PO;->d(LX/1PO;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 243996
    :goto_0
    return-void

    .line 243997
    :cond_0
    iget-object v1, v0, LX/1PO;->a:LX/0bH;

    iget-object p0, v0, LX/1PO;->d:LX/1PP;

    invoke-virtual {v1, p0}, LX/0b4;->b(LX/0b2;)Z

    goto :goto_0
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 243998
    const/4 v0, 0x0

    iput-object v0, p0, LX/1PN;->b:LX/1Py;

    .line 243999
    return-void
.end method
