.class public LX/0q6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:LX/0me;

.field public final e:LX/0me;

.field public final f:LX/0mm;

.field public final g:LX/0Zh;

.field public final h:LX/0pC;

.field public final i:LX/0ma;

.field public final j:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/analytics2/logger/HandlerThreadFactory;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0mg;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;LX/0me;LX/0me;LX/0mm;LX/0Zh;LX/0pC;LX/0ma;Ljava/lang/Class;LX/0mg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "LX/0me;",
            "LX/0me;",
            "LX/0mm;",
            "LX/0Zh;",
            "LX/0pC;",
            "Lcom/facebook/analytics2/logger/AppBackgroundedProvider;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/analytics2/logger/HandlerThreadFactory;",
            ">;",
            "LX/0mg;",
            ")V"
        }
    .end annotation

    .prologue
    .line 147009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147010
    iput-object p1, p0, LX/0q6;->a:Landroid/content/Context;

    .line 147011
    iput-object p4, p0, LX/0q6;->d:LX/0me;

    .line 147012
    iput-object p5, p0, LX/0q6;->e:LX/0me;

    .line 147013
    iput-object p6, p0, LX/0q6;->f:LX/0mm;

    .line 147014
    iput-object p7, p0, LX/0q6;->g:LX/0Zh;

    .line 147015
    iput-object p9, p0, LX/0q6;->i:LX/0ma;

    .line 147016
    iput-object p10, p0, LX/0q6;->j:Ljava/lang/Class;

    .line 147017
    iput-object p8, p0, LX/0q6;->h:LX/0pC;

    .line 147018
    iput p2, p0, LX/0q6;->b:I

    .line 147019
    iput-object p3, p0, LX/0q6;->c:Ljava/lang/String;

    .line 147020
    iput-object p11, p0, LX/0q6;->k:LX/0mg;

    .line 147021
    return-void
.end method
