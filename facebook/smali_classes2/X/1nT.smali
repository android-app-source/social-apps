.class public final LX/1nT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1nA;


# direct methods
.method public constructor <init>(LX/1nA;)V
    .locals 0

    .prologue
    .line 316456
    iput-object p1, p0, LX/1nT;->a:LX/1nA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    const/4 v2, 0x1

    const v3, -0x349891d8

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 316457
    const v0, 0x7f0d006a

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 316458
    invoke-static {v0}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 316459
    invoke-static {v0, p1}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 316460
    :cond_0
    iget-object v2, p0, LX/1nT;->a:LX/1nA;

    iget-object v2, v2, LX/1nA;->d:LX/0Zb;

    invoke-interface {v2, v0}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 316461
    iget-object v0, p0, LX/1nT;->a:LX/1nA;

    iget-object v2, v0, LX/1nA;->e:LX/0gh;

    const v0, 0x7f0d0070

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 316462
    const v0, 0x7f0d006d

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    const v0, 0x7f0d006d

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v2, v0

    .line 316463
    :goto_0
    const v0, 0x7f0d006e

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    const v0, 0x7f0d006e

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v3, v1

    .line 316464
    :goto_1
    const v0, 0x7f0d006c

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 316465
    const v1, 0x7f0d006f

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 316466
    iget-object v5, v1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->c:Ljava/lang/String;

    move-object v5, v5

    .line 316467
    if-nez v5, :cond_2

    .line 316468
    new-instance v5, LX/21A;

    invoke-direct {v5}, LX/21A;-><init>()V

    invoke-static {v1}, LX/21A;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/21A;

    move-result-object v1

    iget-object v5, p0, LX/1nT;->a:LX/1nA;

    .line 316469
    iget-object v6, v5, LX/1nA;->b:Landroid/content/Context;

    instance-of v6, v6, LX/0f2;

    if-nez v6, :cond_6

    .line 316470
    const-string v6, "unknown"

    .line 316471
    :cond_1
    :goto_2
    move-object v5, v6

    .line 316472
    iput-object v5, v1, LX/21A;->c:Ljava/lang/String;

    .line 316473
    move-object v1, v1

    .line 316474
    invoke-virtual {v1}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v1

    .line 316475
    :cond_2
    new-instance v5, LX/8qL;

    invoke-direct {v5}, LX/8qL;-><init>()V

    .line 316476
    iput-object v0, v5, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 316477
    move-object v5, v5

    .line 316478
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v6

    .line 316479
    iput-object v6, v5, LX/8qL;->d:Ljava/lang/String;

    .line 316480
    move-object v5, v5

    .line 316481
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    .line 316482
    iput-object v0, v5, LX/8qL;->e:Ljava/lang/String;

    .line 316483
    move-object v0, v5

    .line 316484
    iput-object v1, v0, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 316485
    move-object v0, v0

    .line 316486
    iput-boolean v2, v0, LX/8qL;->i:Z

    .line 316487
    move-object v0, v0

    .line 316488
    iput-boolean v3, v0, LX/8qL;->j:Z

    .line 316489
    move-object v0, v0

    .line 316490
    invoke-virtual {v0}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v0

    .line 316491
    iget-object v1, p0, LX/1nT;->a:LX/1nA;

    iget-object v1, v1, LX/1nA;->m:LX/1nI;

    iget-object v2, p0, LX/1nT;->a:LX/1nA;

    iget-object v2, v2, LX/1nA;->b:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, LX/1nI;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V

    .line 316492
    const v0, 0x7f0d0067

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0bJ;

    .line 316493
    if-eqz v0, :cond_3

    .line 316494
    iget-object v1, p0, LX/1nT;->a:LX/1nA;

    iget-object v1, v1, LX/1nA;->j:LX/0bH;

    invoke-virtual {v1, v0}, LX/0b4;->a(LX/0b7;)V

    .line 316495
    :cond_3
    const v0, 0x66591afa

    invoke-static {v0, v4}, LX/02F;->a(II)V

    return-void

    :cond_4
    move v2, v1

    .line 316496
    goto/16 :goto_0

    :cond_5
    move v3, v1

    .line 316497
    goto :goto_1

    .line 316498
    :cond_6
    iget-object v6, v5, LX/1nA;->b:Landroid/content/Context;

    check-cast v6, LX/0f2;

    invoke-interface {v6}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v6

    .line 316499
    if-nez v6, :cond_1

    const-string v6, "unknown"

    goto :goto_2
.end method
