.class public LX/1J1;
.super LX/0jZ;
.source ""


# static fields
.field private static final d:[I


# instance fields
.field private final a:LX/0r8;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1J2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 229573
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, LX/1J1;->d:[I

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;LX/0r8;Ljava/util/List;LX/1J2;)V
    .locals 1
    .param p1    # Landroid/os/Looper;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0r8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1J2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Looper;",
            "LX/0r8;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;",
            "Lcom/facebook/feed/data/autorefresh/AutoRefreshUIHandler$Listener;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 229574
    sget-object v0, LX/1J1;->d:[I

    invoke-direct {p0, p1, v0}, LX/0jZ;-><init>(Landroid/os/Looper;[I)V

    .line 229575
    iput-object p2, p0, LX/1J1;->a:LX/0r8;

    .line 229576
    iput-object p3, p0, LX/1J1;->b:Ljava/util/List;

    .line 229577
    iput-object p4, p0, LX/1J1;->c:LX/1J2;

    .line 229578
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 229579
    const-string v0, "AutoRefreshUIHandler.doFailNetworkRequest"

    const v1, -0xdf95235

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 229580
    :try_start_0
    iget-object v0, p0, LX/1J1;->a:LX/0r8;

    invoke-virtual {v0}, LX/0r8;->h()V

    .line 229581
    iget-object v0, p0, LX/1J1;->c:LX/1J2;

    .line 229582
    iget-object v1, v0, LX/1J2;->a:LX/1Iv;

    invoke-virtual {v1}, LX/1Iv;->l()V

    .line 229583
    iget-object v1, v0, LX/1J2;->a:LX/1Iv;

    iget-object v1, v1, LX/1Iv;->p:LX/0fp;

    if-eqz v1, :cond_0

    .line 229584
    iget-object v1, v0, LX/1J2;->a:LX/1Iv;

    iget-object v1, v1, LX/1Iv;->p:LX/0fp;

    invoke-interface {v1}, LX/0fp;->m()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229585
    :cond_0
    const v0, -0x31f28a81    # -5.9332192E8f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 229586
    return-void

    .line 229587
    :catchall_0
    move-exception v0

    const v1, 0x1661d34b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private b(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 229588
    const-string v0, "AutoRefreshUIHandler.doStageStories"

    const v1, -0xf2903c3

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 229589
    :try_start_0
    iget-object v0, p0, LX/1J1;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 229590
    iget-object v0, p0, LX/1J1;->c:LX/1J2;

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    .line 229591
    iget-object p0, v0, LX/1J2;->a:LX/1Iv;

    iget-object p0, p0, LX/1Iv;->p:LX/0fp;

    if-eqz p0, :cond_0

    .line 229592
    iget-object p0, v0, LX/1J2;->a:LX/1Iv;

    iget-object p0, p0, LX/1Iv;->p:LX/0fp;

    invoke-interface {p0, v1}, LX/0fp;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229593
    :cond_0
    const v0, -0x7e3a85

    invoke-static {v0}, LX/02m;->a(I)V

    .line 229594
    return-void

    .line 229595
    :catchall_0
    move-exception v0

    const v1, -0x7d1fc748

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 229596
    const-string v0, "AutoRefreshUIHandler.doNetworkCompleteRequest"

    const v1, 0x3b374712

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 229597
    :try_start_0
    iget-object v0, p0, LX/1J1;->a:LX/0r8;

    invoke-virtual {v0}, LX/0r8;->g()V

    .line 229598
    iget-object v0, p0, LX/1J1;->c:LX/1J2;

    .line 229599
    iget-object v1, v0, LX/1J2;->a:LX/1Iv;

    invoke-virtual {v1}, LX/1Iv;->l()V

    .line 229600
    iget-object v1, v0, LX/1J2;->a:LX/1Iv;

    iget-object v1, v1, LX/1Iv;->p:LX/0fp;

    if-eqz v1, :cond_0

    .line 229601
    iget-object v1, v0, LX/1J2;->a:LX/1Iv;

    iget-object v1, v1, LX/1Iv;->p:LX/0fp;

    invoke-interface {v1}, LX/0fp;->m()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229602
    :cond_0
    const v0, -0x446045c7

    invoke-static {v0}, LX/02m;->a(I)V

    .line 229603
    return-void

    .line 229604
    :catchall_0
    move-exception v0

    const v1, 0x7c8475d5

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 229605
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 229606
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown what="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 229607
    :sswitch_0
    invoke-direct {p0}, LX/1J1;->c()V

    .line 229608
    :goto_0
    return-void

    .line 229609
    :sswitch_1
    invoke-direct {p0}, LX/1J1;->b()V

    goto :goto_0

    .line 229610
    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/0Px;

    invoke-direct {p0, v0}, LX/1J1;->b(LX/0Px;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x7 -> :sswitch_2
    .end sparse-switch
.end method
