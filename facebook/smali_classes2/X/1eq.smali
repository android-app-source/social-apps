.class public LX/1eq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/1eq;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/75B;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1fj;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SF;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0y3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1fj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SF;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/75B;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0y3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 289201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289202
    iput-object p1, p0, LX/1eq;->b:LX/0Ot;

    .line 289203
    iput-object p2, p0, LX/1eq;->c:LX/0Ot;

    .line 289204
    iput-object p3, p0, LX/1eq;->a:LX/0Ot;

    .line 289205
    iput-object p4, p0, LX/1eq;->d:LX/0Ot;

    .line 289206
    return-void
.end method

.method public static a(LX/0QB;)LX/1eq;
    .locals 7

    .prologue
    .line 289188
    sget-object v0, LX/1eq;->e:LX/1eq;

    if-nez v0, :cond_1

    .line 289189
    const-class v1, LX/1eq;

    monitor-enter v1

    .line 289190
    :try_start_0
    sget-object v0, LX/1eq;->e:LX/1eq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 289191
    if-eqz v2, :cond_0

    .line 289192
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 289193
    new-instance v3, LX/1eq;

    const/16 v4, 0x209

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2e3

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2e17

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0xc83

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, v6, p0}, LX/1eq;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 289194
    move-object v0, v3

    .line 289195
    sput-object v0, LX/1eq;->e:LX/1eq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 289196
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 289197
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 289198
    :cond_1
    sget-object v0, LX/1eq;->e:LX/1eq;

    return-object v0

    .line 289199
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 289200
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1eq;Lcom/facebook/graphql/model/GraphQLStory;J)Z
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 289184
    if-nez p1, :cond_0

    move v0, v1

    .line 289185
    :goto_0
    return v0

    .line 289186
    :cond_0
    iget-object v0, p0, LX/1eq;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SF;

    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 289187
    cmp-long v0, v2, p2

    if-gez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static c(LX/1eq;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMedia;)Z
    .locals 4
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 289183
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    const-wide/16 v2, 0x1c20

    invoke-static {p0, v0, v2, v3}, LX/1eq;->a(LX/1eq;Lcom/facebook/graphql/model/GraphQLStory;J)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, LX/1eq;->b(Lcom/facebook/graphql/model/GraphQLMedia;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/1eq;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMedia;)Z
    .locals 1
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 289137
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMedia;->av()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289138
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 289139
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VF;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1eq;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMedia;)I
    .locals 5
    .param p2    # Lcom/facebook/graphql/model/GraphQLMedia;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ")I"
        }
    .end annotation

    .prologue
    .line 289174
    invoke-static {p0, p1, p2}, LX/1eq;->e(LX/1eq;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 289175
    invoke-static {p0, p1, p2}, LX/1eq;->c(LX/1eq;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289176
    const/4 v0, 0x1

    .line 289177
    :goto_0
    return v0

    .line 289178
    :cond_0
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 289179
    const-wide/16 v3, 0x1c20

    invoke-static {p0, v1, v3, v4}, LX/1eq;->a(LX/1eq;Lcom/facebook/graphql/model/GraphQLStory;J)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMedia;->H()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    if-nez v1, :cond_2

    iget-object v1, p0, LX/1eq;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0y3;

    invoke-virtual {v1}, LX/0y3;->b()LX/1rv;

    move-result-object v1

    iget-object v1, v1, LX/1rv;->a:LX/0yG;

    sget-object v2, LX/0yG;->OKAY:LX/0yG;

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 289180
    if-eqz v0, :cond_1

    .line 289181
    const/4 v0, 0x2

    goto :goto_0

    .line 289182
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2

    .prologue
    .line 289169
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 289170
    :cond_0
    const/4 v0, 0x0

    .line 289171
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/1eq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fj;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 289172
    iget-object p0, v0, LX/1fj;->a:LX/0aq;

    invoke-virtual {p0, v1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_2

    const/4 p0, 0x1

    :goto_1
    move v0, p0

    .line 289173
    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    goto :goto_1
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLMedia;)I
    .locals 18

    .prologue
    .line 289145
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->I()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->I()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->I()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 289146
    :cond_0
    const/4 v5, 0x0

    .line 289147
    :cond_1
    :goto_0
    return v5

    .line 289148
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->I()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;->a()LX/0Px;

    move-result-object v5

    .line 289149
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bt()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bt()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->a()LX/0Px;

    move-result-object v2

    if-nez v2, :cond_5

    :cond_3
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v2

    move-object v3, v2

    .line 289150
    :goto_1
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v2

    invoke-static {v2}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v6

    .line 289151
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v4, v2

    :goto_2
    if-ge v4, v7, :cond_6

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFaceBox;

    .line 289152
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFaceBox;->j()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 289153
    new-instance v8, Landroid/graphics/RectF;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFaceBox;->j()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v10

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFaceBox;->k()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v12

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double/2addr v12, v14

    sub-double/2addr v10, v12

    double-to-float v9, v10

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFaceBox;->j()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v10

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFaceBox;->k()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v12

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double/2addr v12, v14

    sub-double/2addr v10, v12

    double-to-float v10, v10

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFaceBox;->j()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v12

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFaceBox;->k()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v14

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    div-double v14, v14, v16

    add-double/2addr v12, v14

    double-to-float v11, v12

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFaceBox;->j()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v12

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFaceBox;->k()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v14

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    div-double v14, v14, v16

    add-double/2addr v12, v14

    double-to-float v2, v12

    invoke-direct {v8, v9, v10, v11, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 289154
    new-instance v2, Lcom/facebook/photos/base/tagging/FaceBox;

    invoke-direct {v2, v8}, Lcom/facebook/photos/base/tagging/FaceBox;-><init>(Landroid/graphics/RectF;)V

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 289155
    :cond_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_2

    .line 289156
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bt()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->a()LX/0Px;

    move-result-object v2

    move-object v3, v2

    goto/16 :goto_1

    .line 289157
    :cond_6
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v2

    invoke-static {v2}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v5

    .line 289158
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v4, v2

    :goto_3
    if-ge v4, v7, :cond_8

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;

    .line 289159
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->k()Lcom/facebook/graphql/model/GraphQLPhotoTag;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->j()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v8

    if-eqz v8, :cond_7

    .line 289160
    new-instance v8, Landroid/graphics/PointF;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->k()Lcom/facebook/graphql/model/GraphQLPhotoTag;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->j()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v10

    double-to-float v9, v10

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->k()Lcom/facebook/graphql/model/GraphQLPhotoTag;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhotoTag;->j()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v10

    double-to-float v2, v10

    invoke-direct {v8, v9, v2}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 289161
    :cond_7
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    .line 289162
    :cond_8
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    .line 289163
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v5, v2

    :cond_9
    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    .line 289164
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_a
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 289165
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1eq;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/75B;

    invoke-virtual {v3}, Lcom/facebook/photos/base/tagging/FaceBox;->f()Landroid/graphics/PointF;

    move-result-object v9

    invoke-virtual {v3}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v3

    invoke-static {v2, v9, v3}, LX/75B;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/RectF;)D

    move-result-wide v10

    const-wide/high16 v12, 0x4010000000000000L    # 4.0

    cmpg-double v3, v10, v12

    if-gez v3, :cond_a

    .line 289166
    add-int/lit8 v2, v5, -0x1

    move v5, v2

    .line 289167
    goto :goto_4

    .line 289168
    :cond_b
    if-gez v5, :cond_1

    const/4 v5, 0x0

    goto/16 :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 4

    .prologue
    .line 289140
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 289141
    :cond_0
    :goto_0
    return-void

    .line 289142
    :cond_1
    iget-object v0, p0, LX/1eq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fj;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v2

    .line 289143
    iget-object p0, v0, LX/1fj;->a:LX/0aq;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p0, v1, p1}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289144
    goto :goto_0
.end method
