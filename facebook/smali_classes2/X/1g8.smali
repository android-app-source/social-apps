.class public LX/1g8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/1g8;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/xanalytics/XAnalyticsProvider;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0if;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0Ot;LX/0Or;LX/0if;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/groups/analytics/XAnalyticsGateKeeper;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/xanalytics/XAnalyticsProvider;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 293325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 293326
    iput-object p1, p0, LX/1g8;->a:LX/0Zb;

    .line 293327
    iput-object p2, p0, LX/1g8;->b:LX/0Ot;

    .line 293328
    iput-object p3, p0, LX/1g8;->c:LX/0Or;

    .line 293329
    iput-object p4, p0, LX/1g8;->d:LX/0if;

    .line 293330
    return-void
.end method

.method public static a(LX/0QB;)LX/1g8;
    .locals 7

    .prologue
    .line 293372
    sget-object v0, LX/1g8;->e:LX/1g8;

    if-nez v0, :cond_1

    .line 293373
    const-class v1, LX/1g8;

    monitor-enter v1

    .line 293374
    :try_start_0
    sget-object v0, LX/1g8;->e:LX/1g8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 293375
    if-eqz v2, :cond_0

    .line 293376
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 293377
    new-instance v5, LX/1g8;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    const/16 v4, 0x13b7

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v4, 0x31c

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v4

    check-cast v4, LX/0if;

    invoke-direct {v5, v3, v6, p0, v4}, LX/1g8;-><init>(LX/0Zb;LX/0Ot;LX/0Or;LX/0if;)V

    .line 293378
    move-object v0, v5

    .line 293379
    sput-object v0, LX/1g8;->e:LX/1g8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 293380
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 293381
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 293382
    :cond_1
    sget-object v0, LX/1g8;->e:LX/1g8;

    return-object v0

    .line 293383
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 293384
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1g8;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 3

    .prologue
    .line 293359
    iget-object v0, p0, LX/1g8;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    sget-object v1, LX/03R;->YES:LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 293360
    if-nez v0, :cond_1

    .line 293361
    :cond_0
    :goto_0
    return-void

    .line 293362
    :cond_1
    if-eqz p1, :cond_0

    .line 293363
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "fbacore_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 293364
    iget-object v1, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v1, v1

    .line 293365
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 293366
    invoke-virtual {p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->s()Ljava/lang/String;

    move-result-object v0

    .line 293367
    if-nez v0, :cond_2

    .line 293368
    const-string v0, ""

    move-object v1, v0

    .line 293369
    :goto_1
    iget-object v0, p0, LX/1g8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/29a;

    .line 293370
    iget-object p0, v0, LX/29a;->c:Lcom/facebook/xanalytics/XAnalyticsNative;

    move-object v0, p0

    .line 293371
    invoke-virtual {v0, v2, v1}, Lcom/facebook/xanalytics/XAnalyticsNative;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v1, v0

    goto :goto_1
.end method

.method public static b(LX/1g8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 293356
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "recommendation_key"

    invoke-virtual {v0, v1, p3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "group_id"

    invoke-virtual {v0, v1, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "invitee_id"

    invoke-virtual {v0, v1, p2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "tracking"

    invoke-virtual {v0, v1, p4}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 293357
    iget-object v1, p0, LX/1g8;->d:LX/0if;

    sget-object v2, LX/0ig;->aK:LX/0ih;

    const-string v3, "GROUP_FEED_PYMI"

    invoke-virtual {v1, v2, p5, v3, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 293358
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 293353
    if-eqz p5, :cond_0

    const-string v5, "gpymi_undo_click"

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, LX/1g8;->b(LX/1g8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 293354
    return-void

    .line 293355
    :cond_0
    const-string v5, "gpymi_add_click"

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 293347
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "creation_cancelled"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "group_creation"

    .line 293348
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 293349
    move-object v0, v0

    .line 293350
    iget-object v1, p0, LX/1g8;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293351
    invoke-static {p0, v0}, LX/1g8;->a(LX/1g8;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 293352
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 293341
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "mobile_group_mall_visit"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "group_feed"

    .line 293342
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 293343
    move-object v0, v0

    .line 293344
    const-string v1, "source"

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string p1, "unknown"

    :cond_0
    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "group_id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 293345
    iget-object v1, p0, LX/1g8;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 293346
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 293334
    iget-object v0, p0, LX/1g8;->a:LX/0Zb;

    const-string v1, "group_member_info_page_view"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 293335
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 293336
    const-string v1, "group_id"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 293337
    const-string v1, "member_id"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 293338
    const-string v1, "source"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 293339
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 293340
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 293331
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "group_id"

    invoke-virtual {v0, v1, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 293332
    iget-object v1, p0, LX/1g8;->d:LX/0if;

    sget-object v2, LX/0ig;->aF:LX/0ih;

    const-string p1, "newsfeed_group_member_welcome_unit"

    invoke-virtual {v1, v2, p2, p1, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 293333
    return-void
.end method
