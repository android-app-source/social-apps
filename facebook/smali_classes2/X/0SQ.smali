.class public abstract LX/0SQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/google/common/util/concurrent/ListenableFuture;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<TV;>;"
    }
.end annotation


# static fields
.field public static final ATOMIC_HELPER:LX/0SS;

.field private static final GENERATE_CANCELLATION_CAUSES:Z

.field private static final NULL:Ljava/lang/Object;

.field private static final log:Ljava/util/logging/Logger;


# instance fields
.field public volatile listeners:LX/0SV;

.field public volatile value:Ljava/lang/Object;

.field public volatile waiters:LX/0SU;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    .line 61286
    const-string v0, "guava.concurrent.generate_cancellation_cause"

    const-string v1, "false"

    invoke-static {v0, v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, LX/0SQ;->GENERATE_CANCELLATION_CAUSES:Z

    .line 61287
    const-class v0, LX/0SQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, LX/0SQ;->log:Ljava/util/logging/Logger;

    .line 61288
    :try_start_0
    new-instance v0, LX/0SR;

    invoke-direct {v0}, LX/0SR;-><init>()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 61289
    :goto_0
    sput-object v0, LX/0SQ;->ATOMIC_HELPER:LX/0SS;

    .line 61290
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/0SQ;->NULL:Ljava/lang/Object;

    return-void

    .line 61291
    :catch_0
    move-exception v0

    move-object v6, v0

    .line 61292
    :try_start_1
    new-instance v0, LX/52C;

    const-class v1, LX/0SU;

    const-class v2, Ljava/lang/Thread;

    const-string v3, "b"

    invoke-static {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v1

    const-class v2, LX/0SU;

    const-class v3, LX/0SU;

    const-string v4, "c"

    invoke-static {v2, v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v2

    const-class v3, LX/0SQ;

    const-class v4, LX/0SU;

    const-string v5, "waiters"

    invoke-static {v3, v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v3

    const-class v4, LX/0SQ;

    const-class v5, LX/0SV;

    const-string v7, "listeners"

    invoke-static {v4, v5, v7}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v4

    const-class v5, LX/0SQ;

    const-class v7, Ljava/lang/Object;

    const-string v8, "value"

    invoke-static {v5, v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/52C;-><init>(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 61293
    :catch_1
    move-exception v0

    .line 61294
    sget-object v1, LX/0SQ;->log:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "UnsafeAtomicHelper is broken!"

    invoke-virtual {v1, v2, v3, v6}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 61295
    sget-object v1, LX/0SQ;->log:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "SafeAtomicHelper is broken!"

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 61296
    new-instance v0, LX/52D;

    invoke-direct {v0}, LX/52D;-><init>()V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final cancellationExceptionWithCause(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/util/concurrent/CancellationException;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 61235
    new-instance v0, Ljava/util/concurrent/CancellationException;

    invoke-direct {v0, p0}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    .line 61236
    invoke-virtual {v0, p1}, Ljava/util/concurrent/CancellationException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 61237
    return-object v0
.end method

.method private complete()V
    .locals 3

    .prologue
    .line 61238
    :cond_0
    iget-object v0, p0, LX/0SQ;->waiters:LX/0SU;

    .line 61239
    sget-object v1, LX/0SQ;->ATOMIC_HELPER:LX/0SS;

    sget-object v2, LX/0SU;->a:LX/0SU;

    invoke-virtual {v1, p0, v0, v2}, LX/0SS;->a(LX/0SQ;LX/0SU;LX/0SU;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61240
    move-object v0, v0

    .line 61241
    :goto_0
    if-eqz v0, :cond_2

    .line 61242
    iget-object v1, v0, LX/0SU;->b:Ljava/lang/Thread;

    .line 61243
    if-eqz v1, :cond_1

    .line 61244
    const/4 v2, 0x0

    iput-object v2, v0, LX/0SU;->b:Ljava/lang/Thread;

    .line 61245
    invoke-static {v1}, Ljava/util/concurrent/locks/LockSupport;->unpark(Ljava/lang/Thread;)V

    .line 61246
    :cond_1
    iget-object v0, v0, LX/0SU;->c:LX/0SU;

    goto :goto_0

    .line 61247
    :cond_2
    iget-object v0, p0, LX/0SQ;->listeners:LX/0SV;

    .line 61248
    sget-object v1, LX/0SQ;->ATOMIC_HELPER:LX/0SS;

    sget-object v2, LX/0SV;->a:LX/0SV;

    invoke-virtual {v1, p0, v0, v2}, LX/0SS;->a(LX/0SQ;LX/0SV;LX/0SV;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 61249
    move-object v1, v0

    .line 61250
    const/4 v0, 0x0

    .line 61251
    :goto_1
    if-eqz v1, :cond_3

    .line 61252
    iget-object v2, v1, LX/0SV;->d:LX/0SV;

    .line 61253
    iput-object v0, v1, LX/0SV;->d:LX/0SV;

    move-object v0, v1

    move-object v1, v2

    .line 61254
    goto :goto_1

    .line 61255
    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    .line 61256
    iget-object v1, v0, LX/0SV;->b:Ljava/lang/Runnable;

    iget-object v2, v0, LX/0SV;->c:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2}, LX/0SQ;->executeListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 61257
    iget-object v0, v0, LX/0SV;->d:LX/0SV;

    goto :goto_2

    .line 61258
    :cond_4
    invoke-virtual {p0}, LX/0SQ;->done()V

    .line 61259
    return-void
.end method

.method public static completeWithFuture(LX/0SQ;Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 61260
    instance-of v0, p1, LX/0SP;

    if-eqz v0, :cond_1

    .line 61261
    check-cast p1, LX/0SQ;

    iget-object v0, p1, LX/0SQ;->value:Ljava/lang/Object;

    .line 61262
    :cond_0
    :goto_0
    sget-object v2, LX/0SQ;->ATOMIC_HELPER:LX/0SS;

    invoke-virtual {v2, p0, p2, v0}, LX/0SS;->a(LX/0SQ;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 61263
    invoke-direct {p0}, LX/0SQ;->complete()V

    .line 61264
    const/4 v0, 0x1

    .line 61265
    :goto_1
    return v0

    .line 61266
    :cond_1
    :try_start_0
    invoke-static {p1}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    .line 61267
    if-nez v0, :cond_0

    sget-object v0, LX/0SQ;->NULL:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 61268
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 61269
    new-instance v0, LX/0Sc;

    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-direct {v0, v2}, LX/0Sc;-><init>(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 61270
    :catch_1
    move-exception v0

    move-object v2, v0

    .line 61271
    new-instance v0, LX/0Sb;

    invoke-direct {v0, v1, v2}, LX/0Sb;-><init>(ZLjava/lang/Throwable;)V

    goto :goto_0

    .line 61272
    :catch_2
    move-exception v0

    move-object v2, v0

    .line 61273
    new-instance v0, LX/0Sc;

    invoke-direct {v0, v2}, LX/0Sc;-><init>(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 61274
    goto :goto_1
.end method

.method private static executeListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 5

    .prologue
    .line 61282
    const v0, 0x52973179

    :try_start_0
    invoke-static {p1, p0, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61283
    :goto_0
    return-void

    .line 61284
    :catch_0
    move-exception v0

    .line 61285
    sget-object v1, LX/0SQ;->log:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "RuntimeException while executing runnable "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with executor "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private getDoneValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 61275
    instance-of v0, p1, LX/0Sb;

    if-eqz v0, :cond_0

    .line 61276
    const-string v0, "Task was cancelled."

    check-cast p1, LX/0Sb;

    iget-object v1, p1, LX/0Sb;->b:Ljava/lang/Throwable;

    invoke-static {v0, v1}, LX/0SQ;->cancellationExceptionWithCause(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/util/concurrent/CancellationException;

    move-result-object v0

    throw v0

    .line 61277
    :cond_0
    instance-of v0, p1, LX/0Sc;

    if-eqz v0, :cond_1

    .line 61278
    new-instance v0, Ljava/util/concurrent/ExecutionException;

    check-cast p1, LX/0Sc;

    iget-object v1, p1, LX/0Sc;->b:Ljava/lang/Throwable;

    invoke-direct {v0, v1}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 61279
    :cond_1
    sget-object v0, LX/0SQ;->NULL:Ljava/lang/Object;

    if-ne p1, v0, :cond_2

    .line 61280
    const/4 p1, 0x0

    .line 61281
    :cond_2
    return-object p1
.end method

.method private removeWaiter(LX/0SU;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 61297
    iput-object v3, p1, LX/0SU;->b:Ljava/lang/Thread;

    .line 61298
    :cond_0
    iget-object v0, p0, LX/0SQ;->waiters:LX/0SU;

    .line 61299
    sget-object v1, LX/0SU;->a:LX/0SU;

    if-ne v0, v1, :cond_4

    .line 61300
    :cond_1
    return-void

    .line 61301
    :goto_0
    if-eqz v0, :cond_1

    .line 61302
    iget-object v2, v0, LX/0SU;->c:LX/0SU;

    .line 61303
    iget-object v4, v0, LX/0SU;->b:Ljava/lang/Thread;

    if-eqz v4, :cond_2

    :goto_1
    move-object v1, v0

    move-object v0, v2

    .line 61304
    goto :goto_0

    .line 61305
    :cond_2
    if-eqz v1, :cond_3

    .line 61306
    iput-object v2, v1, LX/0SU;->c:LX/0SU;

    .line 61307
    iget-object v0, v1, LX/0SU;->b:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    :goto_2
    move-object v0, v1

    goto :goto_1

    .line 61308
    :cond_3
    sget-object v4, LX/0SQ;->ATOMIC_HELPER:LX/0SS;

    invoke-virtual {v4, p0, v0, v2}, LX/0SS;->a(LX/0SQ;LX/0SU;LX/0SU;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    :cond_4
    move-object v1, v3

    goto :goto_0
.end method


# virtual methods
.method public addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 3

    .prologue
    .line 61309
    const-string v0, "Runnable was null."

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61310
    const-string v0, "Executor was null."

    invoke-static {p2, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61311
    iget-object v0, p0, LX/0SQ;->listeners:LX/0SV;

    .line 61312
    sget-object v1, LX/0SV;->a:LX/0SV;

    if-eq v0, v1, :cond_2

    .line 61313
    new-instance v1, LX/0SV;

    invoke-direct {v1, p1, p2}, LX/0SV;-><init>(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 61314
    :cond_0
    iput-object v0, v1, LX/0SV;->d:LX/0SV;

    .line 61315
    sget-object v2, LX/0SQ;->ATOMIC_HELPER:LX/0SS;

    invoke-virtual {v2, p0, v0, v1}, LX/0SS;->a(LX/0SQ;LX/0SV;LX/0SV;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61316
    :goto_0
    return-void

    .line 61317
    :cond_1
    iget-object v0, p0, LX/0SQ;->listeners:LX/0SV;

    .line 61318
    sget-object v2, LX/0SV;->a:LX/0SV;

    if-ne v0, v2, :cond_0

    .line 61319
    :cond_2
    invoke-static {p1, p2}, LX/0SQ;->executeListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public cancel(Z)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 61216
    iget-object v3, p0, LX/0SQ;->value:Ljava/lang/Object;

    .line 61217
    if-nez v3, :cond_3

    move v0, v1

    :goto_0
    instance-of v4, v3, Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;

    or-int/2addr v0, v4

    if-eqz v0, :cond_6

    .line 61218
    sget-boolean v0, LX/0SQ;->GENERATE_CANCELLATION_CAUSES:Z

    if-eqz v0, :cond_4

    .line 61219
    new-instance v0, Ljava/util/concurrent/CancellationException;

    const-string v4, "Future.cancel() was called."

    invoke-direct {v0, v4}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    move-object v0, v0

    .line 61220
    :goto_1
    new-instance v4, LX/0Sb;

    invoke-direct {v4, p1, v0}, LX/0Sb;-><init>(ZLjava/lang/Throwable;)V

    move-object v0, v3

    .line 61221
    :cond_0
    sget-object v3, LX/0SQ;->ATOMIC_HELPER:LX/0SS;

    invoke-virtual {v3, p0, v0, v4}, LX/0SS;->a(LX/0SQ;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 61222
    if-eqz p1, :cond_1

    .line 61223
    invoke-virtual {p0}, LX/0SQ;->interruptTask()V

    .line 61224
    :cond_1
    invoke-direct {p0}, LX/0SQ;->complete()V

    .line 61225
    instance-of v2, v0, Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;

    if-eqz v2, :cond_2

    .line 61226
    check-cast v0, Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;

    iget-object v0, v0, Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, p1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 61227
    :cond_2
    :goto_2
    return v1

    :cond_3
    move v0, v2

    .line 61228
    goto :goto_0

    .line 61229
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 61230
    :cond_5
    iget-object v0, p0, LX/0SQ;->value:Ljava/lang/Object;

    .line 61231
    instance-of v3, v0, Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;

    if-nez v3, :cond_0

    :cond_6
    move v1, v2

    .line 61232
    goto :goto_2
.end method

.method public done()V
    .locals 0

    .prologue
    .line 61233
    return-void
.end method

.method public get()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 61193
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61194
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 61195
    :cond_0
    iget-object v4, p0, LX/0SQ;->value:Ljava/lang/Object;

    .line 61196
    if-eqz v4, :cond_1

    move v0, v1

    :goto_0
    instance-of v3, v4, Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;

    if-nez v3, :cond_2

    move v3, v1

    :goto_1
    and-int/2addr v0, v3

    if-eqz v0, :cond_3

    .line 61197
    invoke-direct {p0, v4}, LX/0SQ;->getDoneValue(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 61198
    :goto_2
    return-object v0

    :cond_1
    move v0, v2

    .line 61199
    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    .line 61200
    :cond_3
    iget-object v0, p0, LX/0SQ;->waiters:LX/0SU;

    .line 61201
    sget-object v3, LX/0SU;->a:LX/0SU;

    if-eq v0, v3, :cond_a

    .line 61202
    new-instance v4, LX/0SU;

    invoke-direct {v4}, LX/0SU;-><init>()V

    .line 61203
    :cond_4
    invoke-virtual {v4, v0}, LX/0SU;->a(LX/0SU;)V

    .line 61204
    sget-object v3, LX/0SQ;->ATOMIC_HELPER:LX/0SS;

    invoke-virtual {v3, p0, v0, v4}, LX/0SS;->a(LX/0SQ;LX/0SU;LX/0SU;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 61205
    :cond_5
    invoke-static {p0}, Ljava/util/concurrent/locks/LockSupport;->park(Ljava/lang/Object;)V

    .line 61206
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 61207
    invoke-direct {p0, v4}, LX/0SQ;->removeWaiter(LX/0SU;)V

    .line 61208
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 61209
    :cond_6
    iget-object v5, p0, LX/0SQ;->value:Ljava/lang/Object;

    .line 61210
    if-eqz v5, :cond_7

    move v0, v1

    :goto_3
    instance-of v3, v5, Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;

    if-nez v3, :cond_8

    move v3, v1

    :goto_4
    and-int/2addr v0, v3

    if-eqz v0, :cond_5

    .line 61211
    invoke-direct {p0, v5}, LX/0SQ;->getDoneValue(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2

    :cond_7
    move v0, v2

    .line 61212
    goto :goto_3

    :cond_8
    move v3, v2

    goto :goto_4

    .line 61213
    :cond_9
    iget-object v0, p0, LX/0SQ;->waiters:LX/0SU;

    .line 61214
    sget-object v3, LX/0SU;->a:LX/0SU;

    if-ne v0, v3, :cond_4

    .line 61215
    :cond_a
    iget-object v0, p0, LX/0SQ;->value:Ljava/lang/Object;

    invoke-direct {p0, v0}, LX/0SQ;->getDoneValue(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2
.end method

.method public get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 61114
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    .line 61115
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61116
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 61117
    :cond_0
    iget-object v4, p0, LX/0SQ;->value:Ljava/lang/Object;

    .line 61118
    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :goto_0
    instance-of v1, v4, Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    and-int/2addr v0, v1

    if-eqz v0, :cond_3

    .line 61119
    invoke-direct {p0, v4}, LX/0SQ;->getDoneValue(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 61120
    :goto_2
    return-object v0

    .line 61121
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 61122
    :cond_3
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_6

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v4, v0

    .line 61123
    :goto_3
    const-wide/16 v0, 0x3e8

    cmp-long v0, v2, v0

    if-ltz v0, :cond_12

    .line 61124
    iget-object v0, p0, LX/0SQ;->waiters:LX/0SU;

    .line 61125
    sget-object v1, LX/0SU;->a:LX/0SU;

    if-eq v0, v1, :cond_c

    .line 61126
    new-instance v6, LX/0SU;

    invoke-direct {v6}, LX/0SU;-><init>()V

    .line 61127
    :cond_4
    invoke-virtual {v6, v0}, LX/0SU;->a(LX/0SU;)V

    .line 61128
    sget-object v1, LX/0SQ;->ATOMIC_HELPER:LX/0SS;

    invoke-virtual {v1, p0, v0, v6}, LX/0SS;->a(LX/0SQ;LX/0SU;LX/0SU;)Z

    move-result v0

    if-eqz v0, :cond_b

    move-wide v0, v2

    .line 61129
    :cond_5
    invoke-static {p0, v0, v1}, Ljava/util/concurrent/locks/LockSupport;->parkNanos(Ljava/lang/Object;J)V

    .line 61130
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 61131
    invoke-direct {p0, v6}, LX/0SQ;->removeWaiter(LX/0SU;)V

    .line 61132
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 61133
    :cond_6
    const-wide/16 v0, 0x0

    move-wide v4, v0

    goto :goto_3

    .line 61134
    :cond_7
    iget-object v2, p0, LX/0SQ;->value:Ljava/lang/Object;

    .line 61135
    if-eqz v2, :cond_8

    const/4 v0, 0x1

    :goto_4
    instance-of v1, v2, Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;

    if-nez v1, :cond_9

    const/4 v1, 0x1

    :goto_5
    and-int/2addr v0, v1

    if-eqz v0, :cond_a

    .line 61136
    invoke-direct {p0, v2}, LX/0SQ;->getDoneValue(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2

    .line 61137
    :cond_8
    const/4 v0, 0x0

    goto :goto_4

    :cond_9
    const/4 v1, 0x0

    goto :goto_5

    .line 61138
    :cond_a
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    sub-long v0, v4, v0

    .line 61139
    const-wide/16 v2, 0x3e8

    cmp-long v2, v0, v2

    if-gez v2, :cond_5

    .line 61140
    invoke-direct {p0, v6}, LX/0SQ;->removeWaiter(LX/0SU;)V

    .line 61141
    :goto_6
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_11

    .line 61142
    iget-object v2, p0, LX/0SQ;->value:Ljava/lang/Object;

    .line 61143
    if-eqz v2, :cond_d

    const/4 v0, 0x1

    :goto_7
    instance-of v1, v2, Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;

    if-nez v1, :cond_e

    const/4 v1, 0x1

    :goto_8
    and-int/2addr v0, v1

    if-eqz v0, :cond_f

    .line 61144
    invoke-direct {p0, v2}, LX/0SQ;->getDoneValue(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2

    .line 61145
    :cond_b
    iget-object v0, p0, LX/0SQ;->waiters:LX/0SU;

    .line 61146
    sget-object v1, LX/0SU;->a:LX/0SU;

    if-ne v0, v1, :cond_4

    .line 61147
    :cond_c
    iget-object v0, p0, LX/0SQ;->value:Ljava/lang/Object;

    invoke-direct {p0, v0}, LX/0SQ;->getDoneValue(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_2

    .line 61148
    :cond_d
    const/4 v0, 0x0

    goto :goto_7

    :cond_e
    const/4 v1, 0x0

    goto :goto_8

    .line 61149
    :cond_f
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 61150
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 61151
    :cond_10
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    sub-long v0, v4, v0

    goto :goto_6

    .line 61152
    :cond_11
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v0

    :cond_12
    move-wide v0, v2

    goto :goto_6
.end method

.method public interruptTask()V
    .locals 0

    .prologue
    .line 61192
    return-void
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 61190
    iget-object v0, p0, LX/0SQ;->value:Ljava/lang/Object;

    .line 61191
    instance-of v0, v0, LX/0Sb;

    return v0
.end method

.method public isDone()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 61188
    iget-object v3, p0, LX/0SQ;->value:Ljava/lang/Object;

    .line 61189
    if-eqz v3, :cond_0

    move v0, v1

    :goto_0
    instance-of v3, v3, Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;

    if-nez v3, :cond_1

    :goto_1
    and-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final maybePropagateCancellation(Ljava/util/concurrent/Future;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/Future;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 61184
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, LX/0SQ;->isCancelled()Z

    move-result v1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 61185
    invoke-virtual {p0}, LX/0SQ;->wasInterrupted()Z

    move-result v0

    invoke-interface {p1, v0}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 61186
    :cond_0
    return-void

    .line 61187
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public set(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)Z"
        }
    .end annotation

    .prologue
    .line 61179
    if-nez p1, :cond_0

    sget-object p1, LX/0SQ;->NULL:Ljava/lang/Object;

    .line 61180
    :cond_0
    sget-object v0, LX/0SQ;->ATOMIC_HELPER:LX/0SS;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1, p1}, LX/0SS;->a(LX/0SQ;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61181
    invoke-direct {p0}, LX/0SQ;->complete()V

    .line 61182
    const/4 v0, 0x1

    .line 61183
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setException(Ljava/lang/Throwable;)Z
    .locals 3

    .prologue
    .line 61174
    new-instance v1, LX/0Sc;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    invoke-direct {v1, v0}, LX/0Sc;-><init>(Ljava/lang/Throwable;)V

    .line 61175
    sget-object v0, LX/0SQ;->ATOMIC_HELPER:LX/0SS;

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2, v1}, LX/0SS;->a(LX/0SQ;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61176
    invoke-direct {p0}, LX/0SQ;->complete()V

    .line 61177
    const/4 v0, 0x1

    .line 61178
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFuture(Lcom/google/common/util/concurrent/ListenableFuture;)Z
    .locals 3
    .annotation build Lcom/google/common/annotations/Beta;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 61155
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61156
    iget-object v0, p0, LX/0SQ;->value:Ljava/lang/Object;

    .line 61157
    if-nez v0, :cond_2

    .line 61158
    invoke-interface {p1}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61159
    invoke-static {p0, p1, v1}, LX/0SQ;->completeWithFuture(LX/0SQ;Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Object;)Z

    move-result v0

    .line 61160
    :goto_0
    return v0

    .line 61161
    :cond_0
    new-instance v2, Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;

    invoke-direct {v2, p0, p1}, Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;-><init>(LX/0SQ;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 61162
    sget-object v0, LX/0SQ;->ATOMIC_HELPER:LX/0SS;

    invoke-virtual {v0, p0, v1, v2}, LX/0SS;->a(LX/0SQ;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61163
    :try_start_0
    sget-object v0, LX/131;->INSTANCE:LX/131;

    move-object v0, v0

    .line 61164
    invoke-interface {p1, v2, v0}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 61165
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 61166
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 61167
    :try_start_1
    new-instance v0, LX/0Sc;

    invoke-direct {v0, v1}, LX/0Sc;-><init>(Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 61168
    :goto_2
    sget-object v1, LX/0SQ;->ATOMIC_HELPER:LX/0SS;

    invoke-virtual {v1, p0, v2, v0}, LX/0SS;->a(LX/0SQ;Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_1

    .line 61169
    :catch_1
    sget-object v0, LX/0Sc;->a:LX/0Sc;

    goto :goto_2

    .line 61170
    :cond_1
    iget-object v0, p0, LX/0SQ;->value:Ljava/lang/Object;

    .line 61171
    :cond_2
    instance-of v1, v0, LX/0Sb;

    if-eqz v1, :cond_3

    .line 61172
    check-cast v0, LX/0Sb;

    iget-boolean v0, v0, LX/0Sb;->a:Z

    invoke-interface {p1, v0}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 61173
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final wasInterrupted()Z
    .locals 2

    .prologue
    .line 61153
    iget-object v0, p0, LX/0SQ;->value:Ljava/lang/Object;

    .line 61154
    instance-of v1, v0, LX/0Sb;

    if-eqz v1, :cond_0

    check-cast v0, LX/0Sb;

    iget-boolean v0, v0, LX/0Sb;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
