.class public LX/0cX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0cX;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 88926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88927
    return-void
.end method

.method public static a(LX/0QB;)LX/0cX;
    .locals 3

    .prologue
    .line 88928
    sget-object v0, LX/0cX;->a:LX/0cX;

    if-nez v0, :cond_1

    .line 88929
    const-class v1, LX/0cX;

    monitor-enter v1

    .line 88930
    :try_start_0
    sget-object v0, LX/0cX;->a:LX/0cX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 88931
    if-eqz v2, :cond_0

    .line 88932
    :try_start_1
    new-instance v0, LX/0cX;

    invoke-direct {v0}, LX/0cX;-><init>()V

    .line 88933
    move-object v0, v0

    .line 88934
    sput-object v0, LX/0cX;->a:LX/0cX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 88935
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 88936
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 88937
    :cond_1
    sget-object v0, LX/0cX;->a:LX/0cX;

    return-object v0

    .line 88938
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 88939
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/Exception;)LX/73z;
    .locals 1

    .prologue
    .line 88925
    new-instance v0, LX/73z;

    invoke-direct {v0, p0}, LX/73z;-><init>(Ljava/lang/Exception;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Exception;Z)LX/73z;
    .locals 1

    .prologue
    .line 88924
    new-instance v0, LX/73z;

    invoke-direct {v0, p0, p1}, LX/73z;-><init>(Ljava/lang/Exception;Z)V

    return-object v0
.end method
