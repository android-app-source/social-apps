.class public final LX/0QL;
.super LX/0QM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0QM",
        "<",
        "Landroid/content/Context;",
        "LX/0S6;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57990
    invoke-direct {p0}, LX/0QM;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 57991
    check-cast p1, Landroid/content/Context;

    .line 57992
    invoke-static {p1}, LX/0QA;->b(Landroid/content/Context;)Lcom/facebook/base/app/AbstractApplicationLike;

    move-result-object v0

    .line 57993
    invoke-virtual {v0}, Lcom/facebook/base/app/AbstractApplicationLike;->a()LX/0QA;

    move-result-object v0

    .line 57994
    if-nez v0, :cond_0

    .line 57995
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can NOT get FbInjector instance! Possible reasons: (1) This method was called in ContentProvider\'s onCreate. (2) This is a test, and you forgot to initialize the MockInjector. For example, using RobolectricTestUtil.initializeMockInjector()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57996
    :cond_0
    new-instance v1, LX/0S6;

    invoke-direct {v1, v0, p1}, LX/0S6;-><init>(LX/0QA;Landroid/content/Context;)V

    return-object v1
.end method
