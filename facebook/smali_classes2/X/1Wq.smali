.class public LX/1Wq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 269708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;
    .locals 1

    .prologue
    .line 269709
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aA()Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aA()Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->a()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v0

    if-nez v0, :cond_1

    .line 269710
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 269711
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aA()Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->a()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v0

    goto :goto_0
.end method
