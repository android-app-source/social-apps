.class public LX/0YV;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/0Pn;",
            ">;"
        }
    .end annotation
.end field

.field private final b:[Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/Collection",
            "<+",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private d:LX/0Pn;

.field private final f:Ljava/lang/Object;

.field private volatile g:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mGuessLock"
    .end annotation
.end field

.field private final h:Ljava/lang/Object;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/quicklog/StatsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private j:[LX/0Y4;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81606
    const-class v0, LX/0YV;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0YV;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;[LX/0Y4;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/quicklog/StatsLogger;",
            ">;[",
            "LX/0Y4;",
            ")V"
        }
    .end annotation

    .prologue
    .line 81607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81608
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/0YV;->a:Landroid/util/SparseArray;

    .line 81609
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/0YV;->c:Landroid/util/SparseArray;

    .line 81610
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/0YV;->f:Ljava/lang/Object;

    .line 81611
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/0YV;->h:Ljava/lang/Object;

    .line 81612
    iput-object p1, p0, LX/0YV;->i:LX/0Or;

    .line 81613
    iput-object p2, p0, LX/0YV;->j:[LX/0Y4;

    .line 81614
    const/16 v0, 0xd0

    new-array v0, v0, [Ljava/util/ArrayList;

    iput-object v0, p0, LX/0YV;->b:[Ljava/util/ArrayList;

    .line 81615
    sget-object v0, LX/00w;->b:LX/00w;

    move-object v0, v0

    .line 81616
    iget-object p1, v0, LX/00w;->e:Ljava/lang/Boolean;

    move-object v0, p1

    .line 81617
    iput-object v0, p0, LX/0YV;->g:Ljava/lang/Boolean;

    .line 81618
    return-void
.end method

.method public static a(LX/0YV;I)LX/0Pn;
    .locals 2

    .prologue
    .line 81619
    iget-object v1, p0, LX/0YV;->a:Landroid/util/SparseArray;

    monitor-enter v1

    .line 81620
    :try_start_0
    iget-object v0, p0, LX/0YV;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pn;

    monitor-exit v1

    return-object v0

    .line 81621
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static a(LX/0YV;LX/0Pn;JJSI)Lcom/facebook/quicklog/PerformanceLoggingEvent;
    .locals 4

    .prologue
    .line 81622
    sget-object v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->c:LX/0aw;

    invoke-virtual {v0}, LX/0aw;->b()LX/0Po;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;

    .line 81623
    iget-wide v2, p1, LX/0Pn;->d:J

    sub-long v2, p2, v2

    long-to-int v1, v2

    iput v1, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->i:I

    .line 81624
    iget-wide v2, p1, LX/0Pn;->e:J

    invoke-virtual {v0, v2, v3, p4, p5}, Lcom/facebook/quicklog/PerformanceLoggingEvent;->a(JJ)V

    .line 81625
    iget-object v1, p1, LX/0Pn;->r:Ljava/util/ArrayList;

    move-object v1, v1

    .line 81626
    invoke-virtual {v0, v1}, Lcom/facebook/quicklog/PerformanceLoggingEvent;->a(Ljava/util/List;)V

    .line 81627
    iget-object v1, p1, LX/0Pn;->s:Ljava/util/ArrayList;

    move-object v1, v1

    .line 81628
    invoke-virtual {v0, v1}, Lcom/facebook/quicklog/PerformanceLoggingEvent;->b(Ljava/util/List;)V

    .line 81629
    iget v1, p1, LX/0Pn;->g:I

    shr-int/lit8 v1, v1, 0x10

    int-to-short v1, v1

    .line 81630
    if-ltz v1, :cond_0

    const/16 v2, 0xc6

    if-gt v1, v2, :cond_0

    .line 81631
    iget-object v2, p0, LX/0YV;->b:[Ljava/util/ArrayList;

    aget-object v1, v2, v1

    invoke-virtual {v0, v1}, Lcom/facebook/quicklog/PerformanceLoggingEvent;->b(Ljava/util/List;)V

    .line 81632
    :cond_0
    invoke-virtual {v0, p7}, Lcom/facebook/quicklog/PerformanceLoggingEvent;->a(I)V

    .line 81633
    iget v1, p1, LX/0Pn;->g:I

    iput v1, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->k:I

    .line 81634
    iput-short p6, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->o:S

    .line 81635
    iget-wide v2, p1, LX/0Pn;->f:J

    iput-wide v2, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->g:J

    .line 81636
    iget-wide v2, p1, LX/0Pn;->d:J

    iput-wide v2, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->h:J

    .line 81637
    iget v1, p1, LX/0Pn;->b:I

    iput v1, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->f:I

    .line 81638
    iget v1, p1, LX/0Pn;->h:I

    iput v1, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->l:I

    .line 81639
    iget-boolean v1, p1, LX/0Pn;->i:Z

    iput-boolean v1, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->m:Z

    .line 81640
    iget-boolean v1, p1, LX/0Pn;->j:Z

    iput-boolean v1, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->n:Z

    .line 81641
    iget-short v1, p1, LX/0Pn;->t:S

    iput-short v1, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->p:S

    .line 81642
    iget-wide v2, p1, LX/0Pn;->u:J

    sub-long v2, p2, v2

    long-to-int v1, v2

    iput v1, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->q:I

    .line 81643
    iget-object v1, p1, LX/0Pn;->k:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->y:Ljava/lang/String;

    .line 81644
    iget-boolean v1, p1, LX/0Pn;->v:Z

    iput-boolean v1, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->d:Z

    .line 81645
    iget-object v1, p1, LX/0Pn;->o:LX/00q;

    iput-object v1, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->t:LX/00q;

    .line 81646
    iget-object v1, p1, LX/0Pn;->p:LX/03R;

    move-object v1, v1

    .line 81647
    iput-object v1, v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->v:LX/03R;

    .line 81648
    return-object v0
.end method

.method private static a(LX/0YV;ILX/0Pn;)V
    .locals 2

    .prologue
    .line 81649
    iget-object v1, p0, LX/0YV;->a:Landroid/util/SparseArray;

    monitor-enter v1

    .line 81650
    :try_start_0
    iget-object v0, p0, LX/0YV;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->setValueAt(ILjava/lang/Object;)V

    .line 81651
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static a(LX/0YV;LX/0Pn;)V
    .locals 4

    .prologue
    .line 81652
    iget-object v0, p0, LX/0YV;->j:[LX/0Y4;

    if-eqz v0, :cond_0

    .line 81653
    iget-object v1, p0, LX/0YV;->j:[LX/0Y4;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 81654
    invoke-interface {v3, p1}, LX/0Y4;->a(LX/0Pn;)V

    .line 81655
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81656
    :cond_0
    return-void
.end method

.method private static b(LX/0YV;ILX/0Pn;)V
    .locals 2

    .prologue
    .line 81657
    iget-object v1, p0, LX/0YV;->a:Landroid/util/SparseArray;

    monitor-enter v1

    .line 81658
    :try_start_0
    iget-object v0, p0, LX/0YV;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 81659
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static b(LX/0YV;LX/0Pn;)V
    .locals 4

    .prologue
    .line 81660
    iget-object v0, p0, LX/0YV;->j:[LX/0Y4;

    if-eqz v0, :cond_0

    .line 81661
    iget-object v1, p0, LX/0YV;->j:[LX/0Y4;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 81662
    invoke-interface {v3, p1}, LX/0Y4;->b(LX/0Pn;)V

    .line 81663
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81664
    :cond_0
    return-void
.end method

.method private static b(LX/0YV;)Z
    .locals 3

    .prologue
    .line 81665
    sget-object v0, LX/00w;->b:LX/00w;

    move-object v0, v0

    .line 81666
    invoke-virtual {v0}, LX/00w;->e()Z

    move-result v0

    .line 81667
    iget-object v1, p0, LX/0YV;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/0YV;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, v0, :cond_1

    .line 81668
    :cond_0
    :goto_0
    return v0

    .line 81669
    :cond_1
    iget-object v1, p0, LX/0YV;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 81670
    :try_start_0
    iget-object v2, p0, LX/0YV;->g:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/0YV;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-ne v2, v0, :cond_2

    .line 81671
    monitor-exit v1

    goto :goto_0

    .line 81672
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 81673
    :cond_2
    :try_start_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, LX/0YV;->g:Ljava/lang/Boolean;

    .line 81674
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81675
    iget-object v1, p0, LX/0YV;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81676
    invoke-direct {p0}, LX/0YV;->c()V

    goto :goto_0
.end method

.method private static b(LX/0YV;I)Z
    .locals 2

    .prologue
    .line 81712
    iget-object v1, p0, LX/0YV;->a:Landroid/util/SparseArray;

    monitor-enter v1

    .line 81713
    :try_start_0
    iget-object v0, p0, LX/0YV;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pn;

    .line 81714
    if-eqz v0, :cond_0

    iget-boolean v0, v0, LX/0Pn;->w:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 81715
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static c(LX/0YV;I)LX/0Pn;
    .locals 2

    .prologue
    .line 81677
    iget-object v1, p0, LX/0YV;->a:Landroid/util/SparseArray;

    monitor-enter v1

    .line 81678
    :try_start_0
    iget-object v0, p0, LX/0YV;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pn;

    monitor-exit v1

    return-object v0

    .line 81679
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 81680
    iget-object v1, p0, LX/0YV;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 81681
    :try_start_0
    invoke-static {p0}, LX/0YV;->d(LX/0YV;)I

    move-result v2

    .line 81682
    :goto_0
    if-ge v0, v2, :cond_0

    .line 81683
    invoke-static {p0, v0}, LX/0YV;->c(LX/0YV;I)LX/0Pn;

    move-result-object v3

    .line 81684
    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Pn;->a(ZZ)V

    .line 81685
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81686
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static c(LX/0YV;LX/0Pn;)V
    .locals 4

    .prologue
    .line 81687
    iget-object v0, p0, LX/0YV;->j:[LX/0Y4;

    if-eqz v0, :cond_0

    .line 81688
    iget-object v1, p0, LX/0YV;->j:[LX/0Y4;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 81689
    invoke-interface {v3, p1}, LX/0Y4;->c(LX/0Pn;)V

    .line 81690
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81691
    :cond_0
    return-void
.end method

.method private static d(LX/0YV;)I
    .locals 2

    .prologue
    .line 81692
    iget-object v1, p0, LX/0YV;->a:Landroid/util/SparseArray;

    monitor-enter v1

    .line 81693
    :try_start_0
    iget-object v0, p0, LX/0YV;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 81694
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static d(LX/0YV;I)I
    .locals 2

    .prologue
    .line 81695
    iget-object v1, p0, LX/0YV;->a:Landroid/util/SparseArray;

    monitor-enter v1

    .line 81696
    :try_start_0
    iget-object v0, p0, LX/0YV;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 81697
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static d(LX/0YV;LX/0Pn;)V
    .locals 4

    .prologue
    .line 81698
    iget-object v0, p0, LX/0YV;->j:[LX/0Y4;

    if-eqz v0, :cond_0

    .line 81699
    iget-object v1, p0, LX/0YV;->j:[LX/0Y4;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 81700
    invoke-interface {v3, p1}, LX/0Y4;->d(LX/0Pn;)V

    .line 81701
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81702
    :cond_0
    return-void
.end method

.method private e(LX/0Pn;)V
    .locals 4

    .prologue
    .line 81703
    iget-object v0, p0, LX/0YV;->j:[LX/0Y4;

    if-eqz v0, :cond_0

    .line 81704
    iget-object v1, p0, LX/0YV;->j:[LX/0Y4;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 81705
    invoke-interface {v3, p1}, LX/0Y4;->e(LX/0Pn;)V

    .line 81706
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81707
    :cond_0
    return-void
.end method

.method private static e(LX/0YV;I)V
    .locals 2

    .prologue
    .line 81708
    iget-object v1, p0, LX/0YV;->a:Landroid/util/SparseArray;

    monitor-enter v1

    .line 81709
    :try_start_0
    iget-object v0, p0, LX/0YV;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 81710
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static f(LX/0YV;I)V
    .locals 2

    .prologue
    .line 81603
    iget-object v1, p0, LX/0YV;->a:Landroid/util/SparseArray;

    monitor-enter v1

    .line 81604
    :try_start_0
    iget-object v0, p0, LX/0YV;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->removeAt(I)V

    .line 81605
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static i(II)I
    .locals 1

    .prologue
    .line 81711
    const v0, 0xab1d4f5

    mul-int/2addr v0, p1

    xor-int/2addr v0, p0

    return v0
.end method


# virtual methods
.method public final a(IISJZJLX/03R;)Lcom/facebook/quicklog/PerformanceLoggingEvent;
    .locals 12

    .prologue
    .line 81410
    invoke-static {p1, p2}, LX/0YV;->i(II)I

    move-result v2

    .line 81411
    iget-object v10, p0, LX/0YV;->h:Ljava/lang/Object;

    monitor-enter v10

    .line 81412
    :try_start_0
    invoke-static {p0, v2}, LX/0YV;->a(LX/0YV;I)LX/0Pn;

    move-result-object v3

    .line 81413
    const/4 v4, 0x0

    .line 81414
    if-eqz v3, :cond_3

    .line 81415
    invoke-static {p0, v2}, LX/0YV;->e(LX/0YV;I)V

    .line 81416
    iget-wide v6, v3, LX/0Pn;->d:J

    sub-long v6, p4, v6

    .line 81417
    iget-boolean v2, v3, LX/0Pn;->m:Z

    if-eqz v2, :cond_0

    .line 81418
    iget-object v2, p0, LX/0YV;->i:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4l4;

    invoke-virtual {v2, p1, p3, v6, v7}, LX/4l4;->a(ISJ)V

    .line 81419
    :cond_0
    iget-boolean v2, v3, LX/0Pn;->w:Z

    if-eqz v2, :cond_2

    .line 81420
    invoke-virtual {v3}, LX/0Pn;->h()V

    .line 81421
    if-eqz p9, :cond_1

    sget-object v2, LX/03R;->UNSET:LX/03R;

    move-object/from16 v0, p9

    if-eq v0, v2, :cond_1

    .line 81422
    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v3, v0, v2}, LX/0Pn;->a(LX/03R;Z)V

    .line 81423
    :goto_0
    const/4 v9, 0x2

    move-object v2, p0

    move-wide/from16 v4, p4

    move-wide/from16 v6, p7

    move v8, p3

    invoke-static/range {v2 .. v9}, LX/0YV;->a(LX/0YV;LX/0Pn;JJSI)Lcom/facebook/quicklog/PerformanceLoggingEvent;

    move-result-object v2

    .line 81424
    :goto_1
    move-wide/from16 v0, p4

    iput-wide v0, v3, LX/0Pn;->u:J

    .line 81425
    iput-short p3, v3, LX/0Pn;->t:S

    .line 81426
    move/from16 v0, p6

    invoke-virtual {v3, v0}, LX/0Pn;->a(Z)V

    .line 81427
    invoke-static {p0, v3}, LX/0YV;->c(LX/0YV;LX/0Pn;)V

    .line 81428
    sget-object v4, LX/0Pn;->a:LX/0aw;

    invoke-virtual {v4, v3}, LX/0aw;->a(LX/0Po;)V

    .line 81429
    :goto_2
    monitor-exit v10

    return-object v2

    .line 81430
    :cond_1
    invoke-static {p0}, LX/0YV;->b(LX/0YV;)Z

    move-result v2

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, LX/0Pn;->a(ZZ)V

    goto :goto_0

    .line 81431
    :catchall_0
    move-exception v2

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_2
    move-object v2, v4

    goto :goto_1

    :cond_3
    move-object v2, v4

    goto :goto_2
.end method

.method public final a(IISJZJLjava/lang/String;Ljava/lang/String;)Lcom/facebook/quicklog/PerformanceLoggingEvent;
    .locals 12

    .prologue
    .line 81387
    invoke-static {p1, p2}, LX/0YV;->i(II)I

    move-result v2

    .line 81388
    invoke-static {p0, v2}, LX/0YV;->b(LX/0YV;I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 81389
    const/4 v2, 0x0

    .line 81390
    :goto_0
    return-object v2

    .line 81391
    :cond_0
    iget-object v10, p0, LX/0YV;->h:Ljava/lang/Object;

    monitor-enter v10

    .line 81392
    :try_start_0
    invoke-static {p0, v2}, LX/0YV;->a(LX/0YV;I)LX/0Pn;

    move-result-object v3

    .line 81393
    if-eqz v3, :cond_4

    .line 81394
    iget-boolean v2, v3, LX/0Pn;->m:Z

    if-eqz v2, :cond_1

    .line 81395
    iget-wide v4, v3, LX/0Pn;->d:J

    sub-long v4, p4, v4

    .line 81396
    iget-object v2, p0, LX/0YV;->i:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4l4;

    invoke-virtual {v2, p1, p3, v4, v5}, LX/4l4;->a(ISJ)V

    .line 81397
    :cond_1
    iget-boolean v2, v3, LX/0Pn;->w:Z

    if-nez v2, :cond_2

    .line 81398
    const/4 v2, 0x0

    monitor-exit v10

    goto :goto_0

    .line 81399
    :catchall_0
    move-exception v2

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 81400
    :cond_2
    if-eqz p9, :cond_3

    .line 81401
    :try_start_1
    move-object/from16 v0, p9

    move-object/from16 v1, p10

    invoke-virtual {v3, v0, v1}, LX/0Pn;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 81402
    :cond_3
    const/4 v9, 0x2

    move-object v2, p0

    move-wide/from16 v4, p4

    move-wide/from16 v6, p7

    move v8, p3

    invoke-static/range {v2 .. v9}, LX/0YV;->a(LX/0YV;LX/0Pn;JJSI)Lcom/facebook/quicklog/PerformanceLoggingEvent;

    move-result-object v2

    .line 81403
    move-wide/from16 v0, p4

    iput-wide v0, v3, LX/0Pn;->u:J

    .line 81404
    iput-short p3, v3, LX/0Pn;->t:S

    .line 81405
    move/from16 v0, p6

    invoke-virtual {v3, v0}, LX/0Pn;->a(Z)V

    .line 81406
    invoke-direct {p0, v3}, LX/0YV;->e(LX/0Pn;)V

    .line 81407
    monitor-exit v10

    goto :goto_0

    .line 81408
    :cond_4
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81409
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81432
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 81433
    iget-object v2, p0, LX/0YV;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 81434
    :try_start_0
    invoke-static {p0}, LX/0YV;->d(LX/0YV;)I

    move-result v3

    .line 81435
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 81436
    invoke-static {p0, v0}, LX/0YV;->c(LX/0YV;I)LX/0Pn;

    move-result-object v4

    .line 81437
    invoke-virtual {v4}, LX/0Pn;->i()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81438
    invoke-static {p0, v4}, LX/0YV;->d(LX/0YV;LX/0Pn;)V

    .line 81439
    sget-object v5, LX/0Pn;->a:LX/0aw;

    invoke-virtual {v5, v4}, LX/0aw;->a(LX/0Po;)V

    .line 81440
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81441
    :cond_0
    iget-object v0, p0, LX/0YV;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 81442
    iget-object v0, p0, LX/0YV;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 81443
    monitor-exit v2

    .line 81444
    return-object v1

    .line 81445
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(IJ)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81446
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 81447
    iget-object v2, p0, LX/0YV;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 81448
    const/4 v0, 0x0

    .line 81449
    :try_start_0
    invoke-static {p0}, LX/0YV;->d(LX/0YV;)I

    move-result v3

    .line 81450
    :goto_0
    if-ge v0, v3, :cond_2

    .line 81451
    invoke-static {p0, v0}, LX/0YV;->c(LX/0YV;I)LX/0Pn;

    move-result-object v4

    .line 81452
    iget v5, v4, LX/0Pn;->g:I

    if-ne v5, p1, :cond_1

    .line 81453
    const-wide/16 v6, -0x1

    cmp-long v5, p2, v6

    if-eqz v5, :cond_0

    iget-wide v6, v4, LX/0Pn;->d:J

    cmp-long v5, v6, p2

    if-gez v5, :cond_1

    .line 81454
    :cond_0
    iget v5, v4, LX/0Pn;->g:I

    iget v4, v4, LX/0Pn;->c:I

    invoke-static {v5, v4}, LX/0YV;->i(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81455
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 81456
    goto :goto_0

    .line 81457
    :cond_2
    monitor-exit v2

    .line 81458
    return-object v1

    .line 81459
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81460
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 81461
    iget-object v4, p0, LX/0YV;->h:Ljava/lang/Object;

    monitor-enter v4

    .line 81462
    const/4 v1, 0x0

    .line 81463
    :try_start_0
    invoke-static {p0}, LX/0YV;->d(LX/0YV;)I

    move-result v0

    move v2, v1

    move v1, v0

    .line 81464
    :goto_0
    if-ge v2, v1, :cond_1

    .line 81465
    invoke-static {p0, v2}, LX/0YV;->c(LX/0YV;I)LX/0Pn;

    move-result-object v5

    .line 81466
    iget-object v0, p0, LX/0YV;->c:Landroid/util/SparseArray;

    iget v6, v5, LX/0Pn;->g:I

    iget v7, v5, LX/0Pn;->c:I

    invoke-static {v6, v7}, LX/0YV;->i(II)I

    move-result v6

    invoke-virtual {v0, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 81467
    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 81468
    invoke-virtual {v5}, LX/0Pn;->i()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81469
    invoke-static {p0, v2}, LX/0YV;->f(LX/0YV;I)V

    .line 81470
    invoke-static {p0, v5}, LX/0YV;->d(LX/0YV;LX/0Pn;)V

    .line 81471
    sget-object v0, LX/0Pn;->a:LX/0aw;

    invoke-virtual {v0, v5}, LX/0aw;->a(LX/0Po;)V

    .line 81472
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 81473
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 81474
    goto :goto_0

    .line 81475
    :cond_1
    monitor-exit v4

    .line 81476
    return-object v3

    .line 81477
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(IILjava/lang/String;)V
    .locals 2

    .prologue
    .line 81478
    invoke-static {p1, p2}, LX/0YV;->i(II)I

    move-result v0

    .line 81479
    iget-object v1, p0, LX/0YV;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 81480
    :try_start_0
    invoke-static {p0, v0}, LX/0YV;->a(LX/0YV;I)LX/0Pn;

    move-result-object v0

    .line 81481
    if-nez v0, :cond_0

    .line 81482
    monitor-exit v1

    .line 81483
    :goto_0
    return-void

    .line 81484
    :cond_0
    iget-object p0, v0, LX/0Pn;->s:Ljava/util/ArrayList;

    if-nez p0, :cond_1

    .line 81485
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    iput-object p0, v0, LX/0Pn;->s:Ljava/util/ArrayList;

    .line 81486
    :cond_1
    iget-object p0, v0, LX/0Pn;->s:Ljava/util/ArrayList;

    invoke-virtual {p0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81487
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(IILjava/lang/String;JZZ)V
    .locals 8

    .prologue
    .line 81488
    invoke-static {p1, p2}, LX/0YV;->i(II)I

    move-result v6

    .line 81489
    iget-object v7, p0, LX/0YV;->h:Ljava/lang/Object;

    monitor-enter v7

    .line 81490
    :try_start_0
    invoke-static {p0, v6}, LX/0YV;->d(LX/0YV;I)I

    move-result v0

    .line 81491
    if-ltz v0, :cond_0

    .line 81492
    invoke-static {p0, v0}, LX/0YV;->c(LX/0YV;I)LX/0Pn;

    move-result-object v0

    .line 81493
    iput-wide p4, v0, LX/0Pn;->d:J

    .line 81494
    iput-boolean p6, v0, LX/0Pn;->n:Z

    .line 81495
    :goto_0
    invoke-static {p0, v0}, LX/0YV;->a(LX/0YV;LX/0Pn;)V

    .line 81496
    monitor-exit v7

    return-void

    :cond_0
    move v0, p1

    move v1, p2

    move-wide v2, p4

    move v4, p6

    move v5, p7

    .line 81497
    invoke-static/range {v0 .. v5}, LX/0Pn;->a(IIJZZ)LX/0Pn;

    move-result-object v0

    .line 81498
    invoke-static {p0, v6, v0}, LX/0YV;->b(LX/0YV;ILX/0Pn;)V

    .line 81499
    iput-object p3, v0, LX/0Pn;->k:Ljava/lang/String;

    goto :goto_0

    .line 81500
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(IILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 81501
    invoke-static {p1, p2}, LX/0YV;->i(II)I

    move-result v0

    .line 81502
    iget-object v1, p0, LX/0YV;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 81503
    :try_start_0
    invoke-static {p0, v0}, LX/0YV;->a(LX/0YV;I)LX/0Pn;

    move-result-object v0

    .line 81504
    if-nez v0, :cond_0

    .line 81505
    monitor-exit v1

    .line 81506
    :goto_0
    return-void

    .line 81507
    :cond_0
    invoke-virtual {v0, p3, p4}, LX/0Pn;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 81508
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(IILjava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Collection",
            "<+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81509
    iget-object v1, p0, LX/0YV;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 81510
    :try_start_0
    iget-object v0, p0, LX/0YV;->c:Landroid/util/SparseArray;

    invoke-static {p1, p2}, LX/0YV;->i(II)I

    move-result v2

    invoke-virtual {v0, v2, p3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 81511
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(IILjava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81512
    invoke-static {p1, p2}, LX/0YV;->i(II)I

    move-result v0

    .line 81513
    iget-object v2, p0, LX/0YV;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 81514
    :try_start_0
    invoke-static {p0, v0}, LX/0YV;->a(LX/0YV;I)LX/0Pn;

    move-result-object v3

    .line 81515
    if-nez v3, :cond_0

    .line 81516
    monitor-exit v2

    .line 81517
    :goto_0
    return-void

    .line 81518
    :cond_0
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 81519
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v1, v0}, LX/0Pn;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 81520
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(LX/0Pn;LX/03R;)V
    .locals 5

    .prologue
    .line 81521
    iget v0, p1, LX/0Pn;->g:I

    iget v1, p1, LX/0Pn;->c:I

    invoke-static {v0, v1}, LX/0YV;->i(II)I

    move-result v1

    .line 81522
    const/4 v0, 0x0

    .line 81523
    iget-object v2, p0, LX/0YV;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 81524
    if-eqz p2, :cond_2

    :try_start_0
    sget-object v3, LX/03R;->UNSET:LX/03R;

    if-eq p2, v3, :cond_2

    .line 81525
    const/4 v3, 0x0

    invoke-virtual {p1, p2, v3}, LX/0Pn;->a(LX/03R;Z)V

    .line 81526
    :goto_0
    iput-object p1, p0, LX/0YV;->d:LX/0Pn;

    .line 81527
    invoke-static {p0, v1}, LX/0YV;->d(LX/0YV;I)I

    move-result v3

    .line 81528
    if-ltz v3, :cond_3

    .line 81529
    invoke-static {p0, v3}, LX/0YV;->c(LX/0YV;I)LX/0Pn;

    move-result-object v0

    .line 81530
    invoke-static {p0, v3, p1}, LX/0YV;->a(LX/0YV;ILX/0Pn;)V

    .line 81531
    :goto_1
    invoke-static {p0, p1}, LX/0YV;->a(LX/0YV;LX/0Pn;)V

    .line 81532
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81533
    if-eqz v0, :cond_1

    .line 81534
    const/4 v1, 0x3

    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/0Pn;->k:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/0Pn;->k:Ljava/lang/String;

    iget-object v2, p1, LX/0Pn;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 81535
    :cond_0
    sget-object v1, LX/0Pn;->a:LX/0aw;

    invoke-virtual {v1, v0}, LX/0aw;->a(LX/0Po;)V

    .line 81536
    :cond_1
    return-void

    .line 81537
    :cond_2
    :try_start_1
    invoke-static {p0}, LX/0YV;->b(LX/0YV;)Z

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, LX/0Pn;->a(ZZ)V

    goto :goto_0

    .line 81538
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 81539
    :cond_3
    :try_start_2
    invoke-static {p0, v1, p1}, LX/0YV;->b(LX/0YV;ILX/0Pn;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final a(S)V
    .locals 2

    .prologue
    .line 81540
    iget-object v1, p0, LX/0YV;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 81541
    :try_start_0
    iget-object v0, p0, LX/0YV;->b:[Ljava/util/ArrayList;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 81542
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(SLjava/lang/String;)V
    .locals 3

    .prologue
    .line 81543
    iget-object v1, p0, LX/0YV;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 81544
    :try_start_0
    iget-object v0, p0, LX/0YV;->b:[Ljava/util/ArrayList;

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 81545
    iget-object v0, p0, LX/0YV;->b:[Ljava/util/ArrayList;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    aput-object v2, v0, p1

    .line 81546
    :cond_0
    iget-object v0, p0, LX/0YV;->b:[Ljava/util/ArrayList;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81547
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(II)Z
    .locals 1

    .prologue
    .line 81548
    invoke-static {p1, p2}, LX/0YV;->i(II)I

    move-result v0

    invoke-static {p0, v0}, LX/0YV;->a(LX/0YV;I)LX/0Pn;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(IIJZJJLX/00q;LX/03R;Z)Z
    .locals 5

    .prologue
    .line 81549
    invoke-static {p1, p2}, LX/0YV;->i(II)I

    move-result v1

    .line 81550
    invoke-static {p0, v1}, LX/0YV;->b(LX/0YV;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 81551
    const/4 v1, 0x0

    .line 81552
    :goto_0
    return v1

    .line 81553
    :cond_0
    iget-object v2, p0, LX/0YV;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 81554
    :try_start_0
    invoke-static {p0, v1}, LX/0YV;->a(LX/0YV;I)LX/0Pn;

    move-result-object v1

    .line 81555
    if-eqz v1, :cond_2

    iget-boolean v3, v1, LX/0Pn;->w:Z

    if-eqz v3, :cond_2

    .line 81556
    iput-wide p3, v1, LX/0Pn;->d:J

    .line 81557
    iput-boolean p5, v1, LX/0Pn;->n:Z

    .line 81558
    iput-wide p3, v1, LX/0Pn;->u:J

    .line 81559
    iput-wide p6, v1, LX/0Pn;->f:J

    .line 81560
    iput-wide p8, v1, LX/0Pn;->e:J

    .line 81561
    const/4 v3, 0x1

    iput-short v3, v1, LX/0Pn;->t:S

    .line 81562
    const/4 v3, 0x0

    iput-boolean v3, v1, LX/0Pn;->l:Z

    .line 81563
    const/4 v3, 0x0

    iput-object v3, v1, LX/0Pn;->r:Ljava/util/ArrayList;

    .line 81564
    const/4 v3, 0x0

    iput-object v3, v1, LX/0Pn;->s:Ljava/util/ArrayList;

    .line 81565
    iput-object p10, v1, LX/0Pn;->o:LX/00q;

    .line 81566
    move/from16 v0, p12

    iput-boolean v0, v1, LX/0Pn;->m:Z

    .line 81567
    if-eqz p11, :cond_1

    sget-object v3, LX/03R;->UNSET:LX/03R;

    move-object/from16 v0, p11

    if-eq v0, v3, :cond_1

    .line 81568
    const/4 v3, 0x1

    move-object/from16 v0, p11

    invoke-virtual {v1, v0, v3}, LX/0Pn;->a(LX/03R;Z)V

    .line 81569
    :goto_1
    invoke-static {p0, v1}, LX/0YV;->b(LX/0YV;LX/0Pn;)V

    .line 81570
    const/4 v1, 0x1

    monitor-exit v2

    goto :goto_0

    .line 81571
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 81572
    :cond_1
    :try_start_1
    invoke-static {p0}, LX/0YV;->b(LX/0YV;)Z

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, LX/0Pn;->a(ZZ)V

    goto :goto_1

    .line 81573
    :cond_2
    const/4 v1, 0x0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final b(II)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81574
    invoke-static {p1, p2}, LX/0YV;->i(II)I

    move-result v0

    invoke-static {p0, v0}, LX/0YV;->a(LX/0YV;I)LX/0Pn;

    move-result-object v0

    .line 81575
    if-eqz v0, :cond_0

    .line 81576
    iget-object v0, v0, LX/0Pn;->k:Ljava/lang/String;

    .line 81577
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(II)Z
    .locals 1

    .prologue
    .line 81578
    invoke-static {p1, p2}, LX/0YV;->i(II)I

    move-result v0

    invoke-static {p0, v0}, LX/0YV;->b(LX/0YV;I)Z

    move-result v0

    return v0
.end method

.method public final f(II)J
    .locals 5

    .prologue
    const/16 v4, 0x20

    .line 81579
    invoke-static {p1, p2}, LX/0YV;->i(II)I

    move-result v0

    invoke-static {p0, v0}, LX/0YV;->a(LX/0YV;I)LX/0Pn;

    move-result-object v0

    .line 81580
    if-eqz v0, :cond_0

    iget-boolean v1, v0, LX/0Pn;->w:Z

    if-eqz v1, :cond_0

    .line 81581
    iget v1, v0, LX/0Pn;->b:I

    int-to-long v2, v1

    shl-long/2addr v2, v4

    iget v0, v0, LX/0Pn;->h:I

    int-to-long v0, v0

    or-long/2addr v0, v2

    .line 81582
    :goto_0
    return-wide v0

    .line 81583
    :cond_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/0YV;->i(II)I

    move-result v0

    invoke-static {p0, v0}, LX/0YV;->a(LX/0YV;I)LX/0Pn;

    move-result-object v0

    .line 81584
    if-eqz v0, :cond_1

    iget-boolean v1, v0, LX/0Pn;->w:Z

    if-eqz v1, :cond_1

    .line 81585
    iget v1, v0, LX/0Pn;->b:I

    int-to-long v2, v1

    shl-long/2addr v2, v4

    iget v0, v0, LX/0Pn;->h:I

    int-to-long v0, v0

    or-long/2addr v0, v2

    goto :goto_0

    .line 81586
    :cond_1
    const-wide/32 v0, 0x7fffffff

    goto :goto_0
.end method

.method public final g(II)V
    .locals 4

    .prologue
    .line 81587
    invoke-static {p1, p2}, LX/0YV;->i(II)I

    move-result v0

    .line 81588
    iget-object v1, p0, LX/0YV;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 81589
    :try_start_0
    invoke-static {p0, v0}, LX/0YV;->d(LX/0YV;I)I

    move-result v0

    .line 81590
    if-ltz v0, :cond_0

    .line 81591
    invoke-static {p0, v0}, LX/0YV;->c(LX/0YV;I)LX/0Pn;

    move-result-object v2

    .line 81592
    invoke-static {p0, v2}, LX/0YV;->d(LX/0YV;LX/0Pn;)V

    .line 81593
    sget-object v3, LX/0Pn;->a:LX/0aw;

    invoke-virtual {v3, v2}, LX/0aw;->a(LX/0Po;)V

    .line 81594
    invoke-static {p0, v0}, LX/0YV;->f(LX/0YV;I)V

    .line 81595
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final h(II)V
    .locals 2

    .prologue
    .line 81596
    iget-object v1, p0, LX/0YV;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 81597
    :try_start_0
    invoke-static {p0, p1}, LX/0YV;->a(LX/0YV;I)LX/0Pn;

    move-result-object v0

    .line 81598
    if-eqz v0, :cond_0

    .line 81599
    invoke-static {p0, p1}, LX/0YV;->e(LX/0YV;I)V

    .line 81600
    iput p2, v0, LX/0Pn;->g:I

    .line 81601
    invoke-static {p0, p2, v0}, LX/0YV;->b(LX/0YV;ILX/0Pn;)V

    .line 81602
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
