.class public LX/13h;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/13h;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 177096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177097
    iput-object p1, p0, LX/13h;->a:LX/0Zb;

    .line 177098
    return-void
.end method

.method public static a(LX/0QB;)LX/13h;
    .locals 4

    .prologue
    .line 177112
    sget-object v0, LX/13h;->b:LX/13h;

    if-nez v0, :cond_1

    .line 177113
    const-class v1, LX/13h;

    monitor-enter v1

    .line 177114
    :try_start_0
    sget-object v0, LX/13h;->b:LX/13h;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 177115
    if-eqz v2, :cond_0

    .line 177116
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 177117
    new-instance p0, LX/13h;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/13h;-><init>(LX/0Zb;)V

    .line 177118
    move-object v0, p0

    .line 177119
    sput-object v0, LX/13h;->b:LX/13h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177120
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 177121
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 177122
    :cond_1
    sget-object v0, LX/13h;->b:LX/13h;

    return-object v0

    .line 177123
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 177124
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/13h;LX/499;)V
    .locals 3

    .prologue
    .line 177105
    iget-object v0, p0, LX/13h;->a:LX/0Zb;

    .line 177106
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "fb4a_dsm_event"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "event_type"

    invoke-virtual {p1}, LX/499;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, v2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "data_sensitivity"

    .line 177107
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 177108
    move-object v1, v1

    .line 177109
    move-object v1, v1

    .line 177110
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 177111
    return-void
.end method


# virtual methods
.method public final e()V
    .locals 1

    .prologue
    .line 177125
    sget-object v0, LX/499;->SETTINGS_DSM_ENABLED:LX/499;

    invoke-static {p0, v0}, LX/13h;->a(LX/13h;LX/499;)V

    .line 177126
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 177103
    sget-object v0, LX/499;->SETTINGS_DSM_DISABLED:LX/499;

    invoke-static {p0, v0}, LX/13h;->a(LX/13h;LX/499;)V

    .line 177104
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 177101
    sget-object v0, LX/499;->SETTINGS_DSM_AUTO_ENABLED:LX/499;

    invoke-static {p0, v0}, LX/13h;->a(LX/13h;LX/499;)V

    .line 177102
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 177099
    sget-object v0, LX/499;->SETTINGS_DSM_AUTO_DISABLED:LX/499;

    invoke-static {p0, v0}, LX/13h;->a(LX/13h;LX/499;)V

    .line 177100
    return-void
.end method
