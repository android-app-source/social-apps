.class public LX/1Th;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;
.implements LX/1TG;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 253915
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253916
    iput-object p1, p0, LX/1Th;->a:LX/0Ot;

    .line 253917
    return-void
.end method

.method public static a(LX/0QB;)LX/1Th;
    .locals 4

    .prologue
    .line 253918
    const-class v1, LX/1Th;

    monitor-enter v1

    .line 253919
    :try_start_0
    sget-object v0, LX/1Th;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 253920
    sput-object v2, LX/1Th;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 253921
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253922
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 253923
    new-instance v3, LX/1Th;

    const/16 p0, 0x214f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1Th;-><init>(LX/0Ot;)V

    .line 253924
    move-object v0, v3

    .line 253925
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 253926
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Th;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 253927
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 253928
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 1

    .prologue
    .line 253929
    sget-object v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;->a:LX/1Cz;

    invoke-static {p1, v0}, LX/99p;->a(LX/0ja;LX/1Cz;)V

    .line 253930
    return-void
.end method

.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 253931
    sget-object v0, LX/3Ym;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 253932
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 253933
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    iget-object v1, p0, LX/1Th;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 253934
    return-void
.end method
