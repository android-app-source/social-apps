.class public LX/1EY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/1EY;


# instance fields
.field public final a:LX/1EZ;

.field private final b:Ljava/util/concurrent/Executor;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$NotifyChange;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/user/model/User;

.field public final f:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ri",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/audience/model/UploadShot;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1EZ;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/0Or;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1EZ;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 220160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220161
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1EY;->d:Ljava/util/List;

    .line 220162
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1EY;->f:Ljava/util/HashSet;

    .line 220163
    invoke-static {}, LX/1Ei;->a()LX/1Ei;

    move-result-object v0

    iput-object v0, p0, LX/1EY;->g:LX/0Ri;

    .line 220164
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1EY;->h:Ljava/util/HashMap;

    .line 220165
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1EY;->i:Ljava/util/Set;

    .line 220166
    iput-object p1, p0, LX/1EY;->a:LX/1EZ;

    .line 220167
    iput-object p2, p0, LX/1EY;->b:Ljava/util/concurrent/Executor;

    .line 220168
    iput-object p3, p0, LX/1EY;->c:Ljava/util/concurrent/Executor;

    .line 220169
    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, LX/1EY;->e:Lcom/facebook/user/model/User;

    .line 220170
    return-void
.end method

.method public static a(LX/0QB;)LX/1EY;
    .locals 7

    .prologue
    .line 220147
    sget-object v0, LX/1EY;->j:LX/1EY;

    if-nez v0, :cond_1

    .line 220148
    const-class v1, LX/1EY;

    monitor-enter v1

    .line 220149
    :try_start_0
    sget-object v0, LX/1EY;->j:LX/1EY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 220150
    if-eqz v2, :cond_0

    .line 220151
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 220152
    new-instance v6, LX/1EY;

    invoke-static {v0}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v3

    check-cast v3, LX/1EZ;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    const/16 p0, 0x12cb

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/1EY;-><init>(LX/1EZ;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/0Or;)V

    .line 220153
    move-object v0, v6

    .line 220154
    sput-object v0, LX/1EY;->j:LX/1EY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 220155
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 220156
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 220157
    :cond_1
    sget-object v0, LX/1EY;->j:LX/1EY;

    return-object v0

    .line 220158
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 220159
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/audience/model/UploadShot;Ljava/lang/String;Lcom/facebook/audience/model/AudienceControlData;LX/7gz;)LX/7h0;
    .locals 5

    .prologue
    .line 220119
    new-instance v0, LX/7gx;

    invoke-direct {v0}, LX/7gx;-><init>()V

    .line 220120
    iput-object p1, v0, LX/7gx;->i:Ljava/lang/String;

    .line 220121
    move-object v0, v0

    .line 220122
    sget-object v1, LX/7gy;->OUTGOING_STORY:LX/7gy;

    .line 220123
    iput-object v1, v0, LX/7gx;->b:LX/7gy;

    .line 220124
    move-object v0, v0

    .line 220125
    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/7h8;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 220126
    iput-object v1, v0, LX/7gx;->e:Landroid/net/Uri;

    .line 220127
    move-object v0, v0

    .line 220128
    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/7h8;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 220129
    iput-object v1, v0, LX/7gx;->f:Landroid/net/Uri;

    .line 220130
    move-object v0, v0

    .line 220131
    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/7h8;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 220132
    iput-object v1, v0, LX/7gx;->g:Landroid/net/Uri;

    .line 220133
    move-object v0, v0

    .line 220134
    iput-object p3, v0, LX/7gx;->a:LX/7gz;

    .line 220135
    move-object v0, v0

    .line 220136
    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getCreatedAtTime()J

    move-result-wide v2

    .line 220137
    iput-wide v2, v0, LX/7gx;->c:J

    .line 220138
    move-object v0, v0

    .line 220139
    const/4 v1, 0x1

    .line 220140
    iput-boolean v1, v0, LX/7gx;->h:Z

    .line 220141
    move-object v0, v0

    .line 220142
    iput-object p2, v0, LX/7gx;->d:Lcom/facebook/audience/model/AudienceControlData;

    .line 220143
    move-object v0, v0

    .line 220144
    iput-object p1, v0, LX/7gx;->k:Ljava/lang/String;

    .line 220145
    move-object v0, v0

    .line 220146
    invoke-virtual {v0}, LX/7gx;->a()LX/7h0;

    move-result-object v0

    return-object v0
.end method

.method private static c(LX/1EY;)V
    .locals 3

    .prologue
    .line 220115
    iget-object v0, p0, LX/1EY;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AF3;

    .line 220116
    iget-object v2, v0, LX/AF3;->a:LX/0gI;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, LX/0gI;->a(Z)V

    .line 220117
    goto :goto_0

    .line 220118
    :cond_0
    return-void
.end method

.method private static declared-synchronized c(LX/1EY;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 220109
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1EY;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 220110
    iget-object v0, p0, LX/1EY;->g:LX/0Ri;

    invoke-interface {v0}, LX/0Ri;->a_()LX/0Ri;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0Ri;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220111
    iget-object v0, p0, LX/1EY;->g:LX/0Ri;

    invoke-interface {v0}, LX/0Ri;->a_()LX/0Ri;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0Ri;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 220112
    invoke-virtual {p0, v0}, LX/1EY;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220113
    :cond_0
    monitor-exit p0

    return-void

    .line 220114
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/0Px;LX/AF8;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/7h0;",
            ">;",
            "Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$Callback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 220100
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7h0;

    .line 220101
    iget-object v3, v0, LX/7h0;->k:Ljava/lang/String;

    move-object v0, v3

    .line 220102
    invoke-static {p0, v0}, LX/1EY;->c(LX/1EY;Ljava/lang/String;)V

    .line 220103
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 220104
    :cond_0
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 220105
    invoke-virtual {v0, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 220106
    iget-object v1, p0, LX/1EY;->b:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;

    invoke-direct {v2, p0, v0, p2}, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;-><init>(LX/1EY;LX/0Pz;LX/AF8;)V

    const v0, 0x100120bb

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220107
    monitor-exit p0

    return-void

    .line 220108
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 2

    .prologue
    .line 220071
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1EY;->i:Ljava/util/Set;

    .line 220072
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 220073
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 220074
    invoke-static {p0}, LX/1EY;->c(LX/1EY;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220075
    monitor-exit p0

    return-void

    .line 220076
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 220095
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1EY;->f:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 220096
    iget-object v0, p0, LX/1EY;->i:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 220097
    invoke-static {p0}, LX/1EY;->c(LX/1EY;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220098
    monitor-exit p0

    return-void

    .line 220099
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 2

    .prologue
    .line 220083
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1EY;->i:Ljava/util/Set;

    .line 220084
    iget-object v1, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 220085
    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 220086
    iget-object v0, p0, LX/1EY;->g:LX/0Ri;

    .line 220087
    iget-object v1, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 220088
    invoke-interface {v0, v1, p1}, LX/0Ri;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220089
    iget-object v0, p0, LX/1EY;->h:Ljava/util/HashMap;

    .line 220090
    iget-object v1, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->am:Lcom/facebook/audience/model/UploadShot;

    move-object v1, v1

    .line 220091
    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220092
    invoke-static {p0}, LX/1EY;->c(LX/1EY;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220093
    monitor-exit p0

    return-void

    .line 220094
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 220082
    iget-object v0, p0, LX/1EY;->i:Ljava/util/Set;

    iget-object v1, p0, LX/1EY;->i:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 220077
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1EY;->g:LX/0Ri;

    invoke-interface {v0, p1}, LX/0Ri;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 220078
    iget-object v0, p0, LX/1EY;->i:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 220079
    iget-object v0, p0, LX/1EY;->f:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220080
    monitor-exit p0

    return-void

    .line 220081
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
