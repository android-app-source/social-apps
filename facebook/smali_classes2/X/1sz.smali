.class public abstract LX/1sz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/app/ActivityManager;


# direct methods
.method public constructor <init>(Landroid/app/ActivityManager;)V
    .locals 0

    .prologue
    .line 335696
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335697
    iput-object p1, p0, LX/1sz;->a:Landroid/app/ActivityManager;

    .line 335698
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/app/ActivityManager$MemoryInfo;)J
    .param p1    # Landroid/app/ActivityManager$MemoryInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public final a()LX/2GO;
    .locals 4

    .prologue
    .line 335699
    new-instance v0, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v0}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 335700
    iget-object v1, p0, LX/1sz;->a:Landroid/app/ActivityManager;

    invoke-virtual {v1, v0}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 335701
    new-instance v1, LX/2GO;

    invoke-virtual {p0, v0}, LX/1sz;->a(Landroid/app/ActivityManager$MemoryInfo;)J

    move-result-wide v2

    invoke-direct {v1, v0, v2, v3}, LX/2GO;-><init>(Landroid/app/ActivityManager$MemoryInfo;J)V

    return-object v1
.end method
