.class public final LX/0do;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0do;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 90909
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90910
    iput-object p1, p0, LX/0do;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 90911
    iput-object p2, p0, LX/0do;->b:LX/0Ot;

    .line 90912
    return-void
.end method

.method public static a(LX/0QB;)LX/0do;
    .locals 5

    .prologue
    .line 90913
    sget-object v0, LX/0do;->c:LX/0do;

    if-nez v0, :cond_1

    .line 90914
    const-class v1, LX/0do;

    monitor-enter v1

    .line 90915
    :try_start_0
    sget-object v0, LX/0do;->c:LX/0do;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 90916
    if-eqz v2, :cond_0

    .line 90917
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 90918
    new-instance v4, LX/0do;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 p0, 0x3cf

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/0do;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;)V

    .line 90919
    move-object v0, v4

    .line 90920
    sput-object v0, LX/0do;->c:LX/0do;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90921
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 90922
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 90923
    :cond_1
    sget-object v0, LX/0do;->c:LX/0do;

    return-object v0

    .line 90924
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 90925
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 90926
    iget-object v0, p0, LX/0do;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dp;->f:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90927
    iget-object v0, p0, LX/0do;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-static {v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->b(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;)V

    .line 90928
    :cond_0
    return-void
.end method
