.class public LX/0YD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/0YD;


# instance fields
.field public final b:LX/0YE;

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80746
    const-class v0, LX/0YD;

    sput-object v0, LX/0YD;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0YE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 80747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80748
    iput-object p1, p0, LX/0YD;->b:LX/0YE;

    .line 80749
    return-void
.end method

.method public static a(LX/0QB;)LX/0YD;
    .locals 4

    .prologue
    .line 80750
    sget-object v0, LX/0YD;->f:LX/0YD;

    if-nez v0, :cond_1

    .line 80751
    const-class v1, LX/0YD;

    monitor-enter v1

    .line 80752
    :try_start_0
    sget-object v0, LX/0YD;->f:LX/0YD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 80753
    if-eqz v2, :cond_0

    .line 80754
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 80755
    new-instance p0, LX/0YD;

    invoke-static {v0}, LX/0YE;->a(LX/0QB;)LX/0YE;

    move-result-object v3

    check-cast v3, LX/0YE;

    invoke-direct {p0, v3}, LX/0YD;-><init>(LX/0YE;)V

    .line 80756
    move-object v0, p0

    .line 80757
    sput-object v0, LX/0YD;->f:LX/0YD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80758
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 80759
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 80760
    :cond_1
    sget-object v0, LX/0YD;->f:LX/0YD;

    return-object v0

    .line 80761
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 80762
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 80763
    :try_start_0
    invoke-static {}, Landroid/os/Debug;->stopMethodTracing()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80764
    iget-object v0, p0, LX/0YD;->b:LX/0YE;

    invoke-virtual {v0}, LX/0YE;->b()V

    .line 80765
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 80766
    :catch_0
    move-exception v1

    .line 80767
    :try_start_1
    sget-object v2, LX/0YD;->a:Ljava/lang/Class;

    const-string v3, "RuntimeException while trying to stop trace"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80768
    iget-object v1, p0, LX/0YD;->b:LX/0YE;

    invoke-virtual {v1}, LX/0YE;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0YD;->b:LX/0YE;

    invoke-virtual {v1}, LX/0YE;->b()V

    throw v0
.end method
