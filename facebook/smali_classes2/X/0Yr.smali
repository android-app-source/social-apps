.class public final LX/0Yr;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static volatile a:I

.field public static volatile b:J

.field private static final f:[Ljava/lang/String;


# instance fields
.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0Tw;",
            ">;"
        }
    .end annotation
.end field

.field private final d:I

.field private final e:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 82517
    sput v2, LX/0Yr;->a:I

    .line 82518
    const-wide/16 v0, -0x1

    sput-wide v0, LX/0Yr;->b:J

    .line 82519
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "version"

    aput-object v1, v0, v2

    sput-object v0, LX/0Yr;->f:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;ILandroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/0Tw;",
            ">;I",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 82512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82513
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/0Yr;->c:LX/0Px;

    .line 82514
    iput p2, p0, LX/0Yr;->d:I

    .line 82515
    invoke-virtual {p3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/0Yr;->e:Landroid/content/Context;

    .line 82516
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 82636
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PRAGMA "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 82637
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    .line 82638
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 82639
    if-eqz v1, :cond_0

    .line 82640
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    return v0

    .line 82641
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 82642
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 82631
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 82632
    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 82633
    const-string v1, "version"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 82634
    const-string v1, "_shared_version"

    const/4 v2, 0x0

    const v3, -0xbbcb3c2

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {p0, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x164ff455

    invoke-static {v0}, LX/03h;->a(I)V

    .line 82635
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 82624
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PRAGMA "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 82625
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82626
    if-eqz v0, :cond_0

    .line 82627
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 82628
    :cond_0
    return-void

    .line 82629
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    .line 82630
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v1
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 82616
    const-string v1, "_shared_version"

    sget-object v2, LX/0Yr;->f:[Ljava/lang/String;

    const-string v3, "name=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 82617
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82618
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 82619
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 82620
    :goto_0
    return v0

    .line 82621
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 82622
    const/4 v0, -0x1

    goto :goto_0

    .line 82623
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static c(LX/0Yr;Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    .line 82585
    const/4 v0, 0x0

    .line 82586
    iget-object v2, p0, LX/0Yr;->e:Landroid/content/Context;

    .line 82587
    sget v3, LX/0Yr;->a:I

    if-nez v3, :cond_0

    .line 82588
    :try_start_0
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget v3, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    sput v3, LX/0Yr;->a:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82589
    :cond_0
    :goto_0
    sget v3, LX/0Yr;->a:I

    move v2, v3

    .line 82590
    invoke-static {p1}, LX/0Yr;->d(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v3

    .line 82591
    if-eq v2, v3, :cond_1

    .line 82592
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 82593
    const-string v3, "name"

    const-string v4, "app_build_number"

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 82594
    const-string v3, "version"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 82595
    const-string v3, "_shared_version"

    const/4 v4, 0x0

    const v5, -0x189ffdfd

    invoke-static {v5}, LX/03h;->a(I)V

    invoke-virtual {p1, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x5028f2fd

    invoke-static {v0}, LX/03h;->a(I)V

    .line 82596
    move v0, v1

    .line 82597
    :cond_1
    iget-object v2, p0, LX/0Yr;->e:Landroid/content/Context;

    .line 82598
    sget-wide v6, LX/0Yr;->b:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_2

    .line 82599
    :try_start_1
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget-wide v6, v6, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    sput-wide v6, LX/0Yr;->b:J
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 82600
    :cond_2
    :goto_1
    sget-wide v6, LX/0Yr;->b:J

    move-wide v2, v6

    .line 82601
    invoke-static {p1}, LX/0Yr;->e(Landroid/database/sqlite/SQLiteDatabase;)J

    move-result-wide v4

    .line 82602
    cmp-long v4, v2, v4

    if-eqz v4, :cond_3

    .line 82603
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 82604
    const-string v4, "name"

    const-string v5, "app_upgrade_time"

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 82605
    const-string v4, "version"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 82606
    const-string v4, "_shared_version"

    const/4 v5, 0x0

    const v6, 0x3b51f028

    invoke-static {v6}, LX/03h;->a(I)V

    invoke-virtual {p1, v4, v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0xc8299aa

    invoke-static {v0}, LX/03h;->a(I)V

    .line 82607
    move v0, v1

    .line 82608
    :cond_3
    iget-object v2, p0, LX/0Yr;->e:Landroid/content/Context;

    invoke-static {v2}, LX/008;->getLastCompilationTime(Landroid/content/Context;)J

    move-result-wide v2

    .line 82609
    invoke-static {p1}, LX/0Yr;->f(Landroid/database/sqlite/SQLiteDatabase;)J

    move-result-wide v4

    .line 82610
    cmp-long v4, v2, v4

    if-eqz v4, :cond_4

    .line 82611
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 82612
    const-string v4, "name"

    const-string v5, "dex_update_time"

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 82613
    const-string v4, "version"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 82614
    const-string v4, "_shared_version"

    const/4 v5, 0x0

    const v6, -0x4b7bc982

    invoke-static {v6}, LX/03h;->a(I)V

    invoke-virtual {p1, v4, v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x78bea55f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 82615
    :goto_2
    return v1

    :cond_4
    move v1, v0

    goto :goto_2

    :catch_0
    goto/16 :goto_0

    :catch_1
    goto :goto_1
.end method

.method private static d(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 82577
    const-string v1, "_shared_version"

    sget-object v2, LX/0Yr;->f:[Ljava/lang/String;

    const-string v3, "name=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, "app_build_number"

    aput-object v0, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 82578
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82579
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 82580
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 82581
    :goto_0
    return v0

    .line 82582
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 82583
    const/4 v0, -0x1

    goto :goto_0

    .line 82584
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static e(Landroid/database/sqlite/SQLiteDatabase;)J
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 82569
    const-string v1, "_shared_version"

    sget-object v2, LX/0Yr;->f:[Ljava/lang/String;

    const-string v3, "name=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, "app_upgrade_time"

    aput-object v0, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 82570
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82571
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 82572
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 82573
    :goto_0
    return-wide v0

    .line 82574
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 82575
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 82576
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static f(Landroid/database/sqlite/SQLiteDatabase;)J
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 82561
    const-string v1, "_shared_version"

    sget-object v2, LX/0Yr;->f:[Ljava/lang/String;

    const-string v3, "name=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, "dex_update_time"

    aput-object v0, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 82562
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82563
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 82564
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 82565
    :goto_0
    return-wide v0

    .line 82566
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 82567
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 82568
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, -0x1

    .line 82520
    iget v0, p0, LX/0Yr;->d:I

    .line 82521
    const-string v2, "page_size"

    invoke-static {p1, v2}, LX/0Yr;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v2

    .line 82522
    const/4 v3, 0x1

    div-int v2, v0, v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 82523
    const-string v3, "journal_mode"

    const-string v4, "PERSIST"

    invoke-static {p1, v3, v4}, LX/0Yr;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 82524
    const-string v3, "wal_autocheckpoint"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v3, v2}, LX/0Yr;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 82525
    const-string v2, "journal_size_limit"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, LX/0Yr;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 82526
    const v0, -0x74842b05

    invoke-static {p1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 82527
    :try_start_0
    invoke-static {p0, p1}, LX/0Yr;->c(LX/0Yr;Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v3

    .line 82528
    iget-object v0, p0, LX/0Yr;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_7

    iget-object v0, p0, LX/0Yr;->c:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tw;

    .line 82529
    iget-object v5, v0, LX/0Tw;->a:Ljava/lang/String;

    move-object v5, v5

    .line 82530
    invoke-static {p1, v5}, LX/0Yr;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v5

    .line 82531
    if-ne v5, v7, :cond_4

    .line 82532
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 82533
    new-instance v0, Landroid/database/sqlite/SQLiteException;

    const-string v1, "Can\'t upgrade readonly database"

    invoke-direct {v0, v1}, Landroid/database/sqlite/SQLiteException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82534
    :catchall_0
    move-exception v0

    const v1, -0x5e9ded2a

    invoke-static {p1, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 82535
    :cond_0
    :try_start_1
    invoke-virtual {v0, p1}, LX/0Tw;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 82536
    :cond_1
    :goto_1
    if-eqz v3, :cond_2

    if-eq v5, v7, :cond_2

    .line 82537
    iget-object v6, p0, LX/0Yr;->e:Landroid/content/Context;

    invoke-virtual {v0, p1, v6}, LX/0Tw;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V

    .line 82538
    :cond_2
    iget v6, v0, LX/0Tw;->b:I

    move v6, v6

    .line 82539
    if-eq v5, v6, :cond_3

    .line 82540
    iget-object v5, v0, LX/0Tw;->a:Ljava/lang/String;

    move-object v5, v5

    .line 82541
    iget v6, v0, LX/0Tw;->b:I

    move v0, v6

    .line 82542
    invoke-static {p1, v5, v0}, LX/0Yr;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    .line 82543
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 82544
    :cond_4
    iget v6, v0, LX/0Tw;->b:I

    move v6, v6

    .line 82545
    if-ge v5, v6, :cond_6

    .line 82546
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 82547
    new-instance v0, Landroid/database/sqlite/SQLiteException;

    const-string v1, "Can\'t upgrade readonly database"

    invoke-direct {v0, v1}, Landroid/database/sqlite/SQLiteException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82548
    :cond_5
    iget v6, v0, LX/0Tw;->b:I

    move v6, v6

    .line 82549
    invoke-virtual {v0, p1, v5, v6}, LX/0Tw;->a(Landroid/database/sqlite/SQLiteDatabase;II)V

    goto :goto_1

    .line 82550
    :cond_6
    iget v6, v0, LX/0Tw;->b:I

    move v6, v6

    .line 82551
    if-le v5, v6, :cond_1

    .line 82552
    new-instance v1, Landroid/database/sqlite/SQLiteException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can\'t downgrade version for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 82553
    iget-object v3, v0, LX/0Tw;->a:Ljava/lang/String;

    move-object v0, v3

    .line 82554
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/database/sqlite/SQLiteException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 82555
    :cond_7
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82556
    const v0, 0x7d4e52eb

    invoke-static {p1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 82557
    iget-object v0, p0, LX/0Yr;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    :goto_2
    if-ge v1, v2, :cond_8

    iget-object v0, p0, LX/0Yr;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tw;

    .line 82558
    invoke-virtual {v0, p1}, LX/0Tw;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 82559
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 82560
    :cond_8
    return-void
.end method
