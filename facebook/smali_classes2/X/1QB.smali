.class public LX/1QB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pp;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/1BK;


# direct methods
.method public constructor <init>(LX/1BK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 244396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244397
    iput-object p1, p0, LX/1QB;->a:LX/1BK;

    .line 244398
    return-void
.end method

.method public static a(LX/0QB;)LX/1QB;
    .locals 1

    .prologue
    .line 244399
    invoke-static {p0}, LX/1QB;->b(LX/0QB;)LX/1QB;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1QB;
    .locals 2

    .prologue
    .line 244400
    new-instance v1, LX/1QB;

    invoke-static {p0}, LX/1BK;->a(LX/0QB;)LX/1BK;

    move-result-object v0

    check-cast v0, LX/1BK;

    invoke-direct {v1, v0}, LX/1QB;-><init>(LX/1BK;)V

    .line 244401
    return-object v1
.end method


# virtual methods
.method public final a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 244402
    if-nez p2, :cond_0

    .line 244403
    :goto_0
    return-void

    .line 244404
    :cond_0
    iget-object v0, p0, LX/1QB;->a:LX/1BK;

    invoke-virtual {v0, p1, p2, p3}, LX/1BL;->a(LX/1aZ;Ljava/lang/String;LX/1bf;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 244405
    if-nez p1, :cond_0

    .line 244406
    :goto_0
    return-void

    .line 244407
    :cond_0
    iget-object v0, p0, LX/1QB;->a:LX/1BK;

    invoke-virtual {v0, p1}, LX/1BL;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
