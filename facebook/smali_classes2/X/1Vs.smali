.class public LX/1Vs;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1Vs",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 266573
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 266574
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1Vs;->b:LX/0Zi;

    .line 266575
    iput-object p1, p0, LX/1Vs;->a:LX/0Ot;

    .line 266576
    return-void
.end method

.method public static a(LX/0QB;)LX/1Vs;
    .locals 4

    .prologue
    .line 266577
    const-class v1, LX/1Vs;

    monitor-enter v1

    .line 266578
    :try_start_0
    sget-object v0, LX/1Vs;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 266579
    sput-object v2, LX/1Vs;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 266580
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266581
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 266582
    new-instance v3, LX/1Vs;

    const/16 p0, 0x1d36

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1Vs;-><init>(LX/0Ot;)V

    .line 266583
    move-object v0, v3

    .line 266584
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 266585
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Vs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266586
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 266587
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 266588
    const v0, -0x33670b37    # -8.0193096E7f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 266589
    check-cast p2, LX/Bse;

    .line 266590
    iget-object v0, p0, LX/1Vs;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;

    iget-object v1, p2, LX/Bse;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    .line 266591
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 266592
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v4

    .line 266593
    if-nez v4, :cond_0

    .line 266594
    :goto_0
    move-object v0, v3

    .line 266595
    return-object v0

    .line 266596
    :cond_0
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 266597
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 266598
    :goto_1
    iget-object v3, v0, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;->d:LX/0ad;

    sget-object v5, LX/0c0;->Cached:LX/0c0;

    sget-short v6, LX/0wi;->h:S

    const/4 v1, 0x0

    invoke-interface {v3, v5, v6, v1}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 266599
    iget-object v3, v0, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;->e:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0gt;

    const-string v5, "1126269887489287"

    .line 266600
    iput-object v5, v3, LX/0gt;->a:Ljava/lang/String;

    .line 266601
    move-object v3, v3

    .line 266602
    invoke-virtual {v3, p1}, LX/0gt;->a(Landroid/content/Context;)V

    .line 266603
    :cond_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v5, 0x0

    invoke-interface {v3, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x1

    const/4 p2, 0x0

    .line 266604
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x4

    const v7, 0x7f0b010f

    invoke-interface {v5, v6, v7}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    .line 266605
    const v6, -0x33670b37    # -8.0193096E7f

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 266606
    invoke-interface {v5, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v5

    .line 266607
    iget-object v6, v0, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;->d:LX/0ad;

    sget-object v7, LX/0c0;->Cached:LX/0c0;

    sget-short p0, LX/0wi;->i:S

    invoke-interface {v6, v7, p0, p2}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 266608
    if-nez v2, :cond_4

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v6

    const v7, 0x7f020a8f

    invoke-virtual {v6, v7}, LX/1o5;->h(I)LX/1o5;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    .line 266609
    :goto_2
    const/4 v7, 0x2

    invoke-interface {v6, v7}, LX/1Di;->b(I)LX/1Di;

    move-result-object v6

    const/4 v7, 0x5

    const p0, 0x7f0b0982

    invoke-interface {v6, v7, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b109c

    invoke-interface {v6, v7}, LX/1Di;->i(I)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b109c

    invoke-interface {v6, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    move-object v6, v6

    .line 266610
    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 266611
    :cond_2
    const v6, 0x7f0e0129

    invoke-static {p1, p2, v6}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const v7, 0x1010099

    invoke-virtual {v6, v7}, LX/1ne;->o(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0a044e

    invoke-virtual {v6, v7}, LX/1ne;->v(I)LX/1ne;

    move-result-object v6

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v1}, LX/1ne;->c(Z)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-interface {v6, v7}, LX/1Di;->a(F)LX/1Di;

    move-result-object v6

    const/4 v7, 0x7

    const p0, 0x7f0b08fe

    invoke-interface {v6, v7, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v6

    const v7, 0x7f020a90

    invoke-virtual {v6, v7}, LX/1o5;->h(I)LX/1o5;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v7, 0x7f0b109b

    invoke-interface {v6, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b0915

    invoke-interface {v6, v1, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    const/4 v7, 0x5

    const p0, 0x7f0b0915

    invoke-interface {v6, v7, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    move-object v2, v5

    .line 266612
    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 266613
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v3

    const v4, 0x7f0a0044

    invoke-virtual {v3, v4}, LX/25Q;->i(I)LX/25Q;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/1Di;->r(I)LX/1Di;

    move-result-object v3

    move-object v3, v3

    .line 266614
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    goto/16 :goto_0

    :cond_3
    move-object v2, v3

    goto/16 :goto_1

    .line 266615
    :cond_4
    iget-object v6, v0, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;->c:LX/1nu;

    invoke-virtual {v6, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v6

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/1nw;->a(LX/4Ab;)LX/1nw;

    move-result-object v6

    sget-object v7, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v7}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    goto/16 :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 266616
    invoke-static {}, LX/1dS;->b()V

    .line 266617
    iget v0, p1, LX/1dQ;->b:I

    .line 266618
    packed-switch v0, :pswitch_data_0

    .line 266619
    :goto_0
    return-object v2

    .line 266620
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 266621
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 266622
    check-cast v1, LX/Bse;

    .line 266623
    iget-object v3, p0, LX/1Vs;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;

    iget-object p1, v1, LX/Bse;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 266624
    iget-object p2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p2, p2

    .line 266625
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object p2

    .line 266626
    iget-object p0, v3, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;->b:LX/1nA;

    invoke-static {p2}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLProfile;)LX/1y5;

    move-result-object p2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    .line 266627
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x33670b37
        :pswitch_0
    .end packed-switch
.end method
