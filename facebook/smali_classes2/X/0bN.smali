.class public final LX/0bN;
.super LX/0b1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b1",
        "<",
        "Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;",
        "LX/8Mf;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0b3;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0b3;",
            "LX/0Ot",
            "<",
            "LX/8Mf;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 86825
    invoke-direct {p0, p1, p2}, LX/0b1;-><init>(LX/0b4;LX/0Ot;)V

    .line 86826
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86865
    const-class v0, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;

    return-object v0
.end method

.method public final a(LX/0b7;Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 86827
    check-cast p1, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;

    check-cast p2, LX/8Mf;

    const/4 v3, 0x0

    .line 86828
    iget-boolean v0, p1, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;->b:Z

    move v0, v0

    .line 86829
    if-eqz v0, :cond_2

    sget-object v2, LX/7m7;->CANCELLED:LX/7m7;

    .line 86830
    :goto_0
    new-instance v0, LX/2rc;

    invoke-direct {v0}, LX/2rc;-><init>()V

    .line 86831
    iget-boolean v1, p1, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;->c:Z

    move v1, v1

    .line 86832
    iput-boolean v1, v0, LX/2rc;->a:Z

    .line 86833
    move-object v0, v0

    .line 86834
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 86835
    if-eqz v1, :cond_0

    .line 86836
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 86837
    iget-object v4, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    move-object v1, v4

    .line 86838
    if-eqz v1, :cond_0

    .line 86839
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 86840
    iget-object v4, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    move-object v1, v4

    .line 86841
    iget-object v4, v1, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->c:Ljava/lang/String;

    move-object v4, v4

    .line 86842
    invoke-virtual {v0, v4}, LX/2rc;->a(Ljava/lang/String;)LX/2rc;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->d()Ljava/lang/String;

    move-result-object v5

    .line 86843
    iput-object v5, v4, LX/2rc;->c:Ljava/lang/String;

    .line 86844
    move-object v4, v4

    .line 86845
    invoke-virtual {v1}, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->e()I

    move-result v5

    .line 86846
    iput v5, v4, LX/2rc;->d:I

    .line 86847
    iget-object v4, v1, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->d:LX/73x;

    move-object v1, v4

    .line 86848
    sget-object v4, LX/73x;->PERMANENT_VIDEO_PROCESSING_ERROR:LX/73x;

    if-ne v1, v4, :cond_0

    .line 86849
    const/4 v1, 0x1

    .line 86850
    iput-boolean v1, v0, LX/2rc;->h:Z

    .line 86851
    :cond_0
    iget-object v1, p2, LX/8Mf;->c:LX/7m8;

    .line 86852
    iget-object v4, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v4, v4

    .line 86853
    iget-object v5, v4, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v5, v5

    .line 86854
    iget-object v4, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v4, v4

    .line 86855
    iget-wide v9, v4, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v6, v9

    .line 86856
    invoke-virtual {v0}, LX/2rc;->a()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v8

    move-object v4, v3

    invoke-virtual/range {v1 .. v8}, LX/7m8;->a(LX/7m7;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;JLcom/facebook/composer/publish/common/ErrorDetails;)V

    .line 86857
    iget-boolean v0, p1, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;->b:Z

    move v0, v0

    .line 86858
    if-eqz v0, :cond_1

    .line 86859
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 86860
    iget-object v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 86861
    iget-object v0, p2, LX/8Mf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7md;

    invoke-virtual {v0, v1}, LX/7md;->b(Ljava/lang/String;)V

    .line 86862
    iget-object v0, p2, LX/8Mf;->a:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(Ljava/lang/String;)V

    .line 86863
    :cond_1
    return-void

    .line 86864
    :cond_2
    sget-object v2, LX/7m7;->EXCEPTION:LX/7m7;

    goto :goto_0
.end method
