.class public final LX/1hf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1iZ;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1iZ;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 296264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296265
    iput-object p1, p0, LX/1hf;->a:LX/0QB;

    .line 296266
    return-void
.end method

.method public static a(LX/0QB;)LX/0Or;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1iZ;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 296214
    new-instance v0, LX/1hf;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1hf;-><init>(LX/0QB;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 296263
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/1hf;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 296216
    packed-switch p2, :pswitch_data_0

    .line 296217
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 296218
    :pswitch_0
    new-instance v1, Lcom/facebook/analytics/AnalyticsHttpDataLogger;

    invoke-static {p1}, Lcom/facebook/analytics/NetworkDataLogger;->a(LX/0QB;)Lcom/facebook/analytics/NetworkDataLogger;

    move-result-object v0

    check-cast v0, Lcom/facebook/analytics/NetworkDataLogger;

    invoke-direct {v1, v0}, Lcom/facebook/analytics/AnalyticsHttpDataLogger;-><init>(Lcom/facebook/analytics/NetworkDataLogger;)V

    .line 296219
    move-object v0, v1

    .line 296220
    :goto_0
    return-object v0

    .line 296221
    :pswitch_1
    new-instance p2, LX/1id;

    invoke-static {p1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p1}, LX/0nI;->b(LX/0QB;)Landroid/net/ConnectivityManager;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-static {p1}, LX/1Gk;->a(LX/0QB;)LX/1Gk;

    move-result-object p0

    check-cast p0, LX/1Gk;

    invoke-direct {p2, v0, v1, p0}, LX/1id;-><init>(LX/0Zb;Landroid/net/ConnectivityManager;LX/1Gk;)V

    .line 296222
    move-object v0, p2

    .line 296223
    goto :goto_0

    .line 296224
    :pswitch_2
    new-instance p0, LX/1ie;

    .line 296225
    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x5a0

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    move-object v0, v0

    .line 296226
    check-cast v0, LX/03R;

    invoke-static {p1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-direct {p0, v0, v1}, LX/1ie;-><init>(LX/03R;LX/0Zb;)V

    .line 296227
    move-object v0, p0

    .line 296228
    goto :goto_0

    .line 296229
    :pswitch_3
    new-instance v1, LX/1if;

    invoke-static {p1}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v0

    check-cast v0, LX/0WJ;

    invoke-direct {v1, v0}, LX/1if;-><init>(LX/0WJ;)V

    .line 296230
    move-object v0, v1

    .line 296231
    goto :goto_0

    .line 296232
    :pswitch_4
    new-instance v1, LX/1ig;

    invoke-static {p1}, LX/1ih;->a(LX/0QB;)LX/1ih;

    move-result-object v0

    check-cast v0, LX/1ih;

    invoke-direct {v1, v0}, LX/1ig;-><init>(LX/1ih;)V

    .line 296233
    move-object v0, v1

    .line 296234
    goto :goto_0

    .line 296235
    :pswitch_5
    new-instance v2, LX/1ij;

    invoke-static {p1}, LX/1Gl;->a(LX/0QB;)LX/1Gl;

    move-result-object v3

    check-cast v3, LX/1Gl;

    invoke-static {p1}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {p1}, LX/1ik;->a(LX/0QB;)LX/1ik;

    move-result-object v5

    check-cast v5, LX/1ik;

    const/16 v6, 0x2ba

    invoke-static {p1, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p1}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v7

    check-cast v7, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-direct/range {v2 .. v7}, LX/1ij;-><init>(LX/1Gl;LX/0So;LX/1ik;LX/0Ot;Lcom/facebook/common/perftest/PerfTestConfig;)V

    .line 296236
    move-object v0, v2

    .line 296237
    goto :goto_0

    .line 296238
    :pswitch_6
    new-instance v3, LX/1im;

    invoke-static {p1}, Lcom/facebook/http/debug/NetworkStats;->a(LX/0QB;)Lcom/facebook/http/debug/NetworkStats;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/debug/NetworkStats;

    invoke-static {p1}, LX/1in;->a(LX/0QB;)LX/1in;

    move-result-object v1

    check-cast v1, LX/1in;

    invoke-static {p1}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v2

    check-cast v2, LX/0So;

    invoke-direct {v3, v0, v1, v2}, LX/1im;-><init>(Lcom/facebook/http/debug/NetworkStats;LX/1in;LX/0So;)V

    .line 296239
    move-object v0, v3

    .line 296240
    goto/16 :goto_0

    .line 296241
    :pswitch_7
    new-instance v1, LX/1io;

    invoke-static {p1}, LX/1ip;->a(LX/0QB;)LX/1ip;

    move-result-object v0

    check-cast v0, LX/1ip;

    invoke-direct {v1, v0}, LX/1io;-><init>(LX/1ip;)V

    .line 296242
    move-object v0, v1

    .line 296243
    goto/16 :goto_0

    .line 296244
    :pswitch_8
    new-instance v1, LX/1is;

    invoke-static {p1}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {v1, v0}, LX/1is;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;)V

    .line 296245
    move-object v0, v1

    .line 296246
    goto/16 :goto_0

    .line 296247
    :pswitch_9
    new-instance v1, LX/1it;

    invoke-static {p1}, LX/1iu;->a(LX/0QB;)LX/1iu;

    move-result-object v0

    check-cast v0, LX/1iu;

    invoke-direct {v1, v0}, LX/1it;-><init>(LX/1iu;)V

    .line 296248
    move-object v0, v1

    .line 296249
    goto/16 :goto_0

    .line 296250
    :pswitch_a
    new-instance v0, LX/04h;

    invoke-direct {v0}, LX/04h;-><init>()V

    .line 296251
    move-object v0, v0

    .line 296252
    move-object v0, v0

    .line 296253
    goto/16 :goto_0

    .line 296254
    :pswitch_b
    new-instance v2, LX/1iw;

    invoke-static {p1}, LX/0yW;->a(LX/0QB;)LX/0yW;

    move-result-object v3

    check-cast v3, LX/0yW;

    const/16 v4, 0x15e7

    invoke-static {p1, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x15b1

    invoke-static {p1, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const-class v6, LX/00H;

    invoke-interface {p1, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/00H;

    invoke-static {p1}, LX/1hs;->a(LX/0QB;)LX/1hs;

    move-result-object v7

    check-cast v7, LX/1hs;

    invoke-direct/range {v2 .. v7}, LX/1iw;-><init>(LX/0yW;LX/0Or;LX/0Or;LX/00H;LX/1hs;)V

    .line 296255
    move-object v0, v2

    .line 296256
    goto/16 :goto_0

    .line 296257
    :pswitch_c
    new-instance v2, LX/1ix;

    invoke-static {p1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v3, 0x388

    invoke-static {p1, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x13c1

    invoke-static {p1, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-direct {v2, v0, v1, v3, v4}, LX/1ix;-><init>(LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Or;)V

    .line 296258
    move-object v0, v2

    .line 296259
    goto/16 :goto_0

    .line 296260
    :pswitch_d
    new-instance v2, LX/1iy;

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p1}, LX/0yR;->a(LX/0QB;)LX/0yR;

    move-result-object v1

    check-cast v1, LX/0yR;

    invoke-direct {v2, v0, v1}, LX/1iy;-><init>(LX/0Uh;LX/0yR;)V

    .line 296261
    move-object v0, v2

    .line 296262
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 296215
    const/16 v0, 0xe

    return v0
.end method
