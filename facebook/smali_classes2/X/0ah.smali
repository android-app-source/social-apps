.class public final LX/0ah;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/nio/charset/Charset;

.field private static final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class;",
            "[",
            "Ljava/lang/Enum;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85594
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, LX/0ah;->a:Ljava/nio/charset/Charset;

    .line 85595
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/0ah;->b:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85526
    return-void
.end method

.method public static a(Ljava/nio/ByteBuffer;IIB)B
    .locals 1

    .prologue
    .line 85527
    invoke-static {p0, p1, p2}, LX/0ah;->k(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 85528
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p3

    :cond_0
    return p3
.end method

.method public static a(Ljava/nio/ByteBuffer;IID)D
    .locals 1

    .prologue
    .line 85529
    invoke-static {p0, p1, p2}, LX/0ah;->k(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 85530
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->getDouble(I)D

    move-result-wide p3

    :cond_0
    return-wide p3
.end method

.method public static a(Ljava/nio/ByteBuffer;IIF)F
    .locals 1

    .prologue
    .line 85531
    invoke-static {p0, p1, p2}, LX/0ah;->k(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 85532
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result p3

    :cond_0
    return p3
.end method

.method public static a(Ljava/nio/ByteBuffer;)I
    .locals 4

    .prologue
    .line 85533
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 85534
    monitor-enter p0

    .line 85535
    :try_start_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    .line 85536
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85537
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_1

    .line 85538
    add-int/lit8 v2, v1, 0x4

    add-int/2addr v2, v0

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    const-string v3, "FLAT"

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    if-eq v2, v3, :cond_0

    .line 85539
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Flatbuffer has an invalid identifier"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85540
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 85541
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 85542
    :cond_1
    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method

.method public static a(Ljava/nio/ByteBuffer;III)I
    .locals 1

    .prologue
    .line 85543
    invoke-static {p0, p1, p2}, LX/0ah;->k(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 85544
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result p3

    :cond_0
    return p3
.end method

.method public static a(Ljava/nio/ByteBuffer;IILjava/lang/String;)I
    .locals 10

    .prologue
    const/4 v0, -0x1

    .line 85545
    invoke-static {p0, p1, p2}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v1

    .line 85546
    if-eqz v1, :cond_0

    .line 85547
    invoke-static {p0, v1}, LX/0ah;->d(Ljava/nio/ByteBuffer;I)I

    move-result v3

    .line 85548
    if-nez v3, :cond_1

    .line 85549
    :cond_0
    :goto_0
    return v0

    .line 85550
    :cond_1
    add-int/lit8 v2, v1, 0x4

    move v4, v2

    .line 85551
    const/4 v2, 0x0

    .line 85552
    add-int/lit8 v1, v3, 0x0

    add-int/lit8 v1, v1, -0x1

    move v3, v2

    move v2, v1

    .line 85553
    :goto_1
    if-lt v2, v3, :cond_0

    .line 85554
    sub-int v1, v2, v3

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v3

    .line 85555
    mul-int/lit8 v5, v1, 0x4

    add-int/2addr v5, v4

    .line 85556
    invoke-virtual {p0, v5}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v6

    .line 85557
    if-eqz v6, :cond_0

    .line 85558
    add-int/2addr v5, v6

    const/4 v6, 0x0

    .line 85559
    invoke-virtual {p0, v5}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v8

    .line 85560
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v9

    move v7, v6

    .line 85561
    :goto_2
    if-ge v7, v8, :cond_5

    if-ge v6, v9, :cond_5

    .line 85562
    add-int/lit8 p1, v5, 0x4

    add-int/2addr p1, v7

    invoke-virtual {p0, p1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    and-int/lit16 p1, p1, 0xff

    .line 85563
    invoke-virtual {p3, v6}, Ljava/lang/String;->charAt(I)C

    move-result p2

    and-int/lit16 p2, p2, 0xff

    .line 85564
    if-eq p1, p2, :cond_4

    .line 85565
    sub-int v6, p1, p2

    .line 85566
    :goto_3
    move v5, v6

    .line 85567
    if-nez v5, :cond_2

    move v0, v1

    .line 85568
    goto :goto_0

    .line 85569
    :cond_2
    if-lez v5, :cond_3

    .line 85570
    add-int/lit8 v1, v1, -0x1

    move v2, v1

    goto :goto_1

    .line 85571
    :cond_3
    add-int/lit8 v1, v1, 0x1

    move v3, v1

    .line 85572
    goto :goto_1

    .line 85573
    :cond_4
    add-int/lit8 v7, v7, 0x1

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 85574
    :cond_5
    sub-int v6, v8, v9

    goto :goto_3
.end method

.method public static a(Ljava/nio/ByteBuffer;IIJ)J
    .locals 1

    .prologue
    .line 85575
    invoke-static {p0, p1, p2}, LX/0ah;->k(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 85576
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide p3

    :cond_0
    return-wide p3
.end method

.method public static a(Ljava/nio/ByteBuffer;ILX/1VQ;Ljava/lang/Object;)LX/1VR;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<THelper:",
            "Ljava/lang/Object;",
            "TItem:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/nio/ByteBuffer;",
            "I",
            "LX/1VQ",
            "<TTHelper;TTItem;>;TTHelper;)",
            "LX/1VR",
            "<TTHelper;TTItem;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 85577
    if-nez p1, :cond_0

    .line 85578
    const/4 v0, 0x0

    .line 85579
    :goto_0
    return-object v0

    .line 85580
    :cond_0
    add-int/lit8 v0, p1, 0x4

    move v2, v0

    .line 85581
    invoke-static {p0, p1}, LX/0ah;->d(Ljava/nio/ByteBuffer;I)I

    move-result v3

    .line 85582
    new-instance v0, LX/1VR;

    move-object v1, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/1VR;-><init>(Ljava/nio/ByteBuffer;IILX/1VQ;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Ljava/nio/ByteBuffer;IILX/1VQ;Ljava/lang/Object;)LX/22d;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<THelper:",
            "Ljava/lang/Object;",
            "TItem:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/nio/ByteBuffer;",
            "II",
            "LX/1VQ",
            "<TTHelper;TTItem;>;TTHelper;)",
            "LX/22d",
            "<TTHelper;TTItem;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 85583
    invoke-static {p0, p1, p2}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 85584
    if-nez v0, :cond_0

    .line 85585
    const/4 v0, 0x0

    .line 85586
    :goto_0
    return-object v0

    .line 85587
    :cond_0
    add-int/lit8 v1, v0, 0x4

    move v2, v1

    .line 85588
    invoke-static {p0, v0}, LX/0ah;->d(Ljava/nio/ByteBuffer;I)I

    move-result v3

    .line 85589
    new-instance v0, LX/22d;

    move-object v1, p0

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/22d;-><init>(Ljava/nio/ByteBuffer;IILX/1VQ;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Ljava/nio/ByteBuffer;IILjava/lang/Class;)Ljava/lang/Enum;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(",
            "Ljava/nio/ByteBuffer;",
            "II",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 85590
    invoke-static {p0, p1, p2, v1}, LX/0ah;->a(Ljava/nio/ByteBuffer;IIS)S

    move-result v0

    .line 85591
    if-eq v0, v1, :cond_0

    .line 85592
    invoke-static {v0, p3}, LX/0ah;->a(SLjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    .line 85593
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(SLjava/lang/Class;)Ljava/lang/Enum;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(S",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 85515
    sget-object v0, LX/0ah;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    .line 85516
    if-nez v0, :cond_0

    .line 85517
    invoke-virtual {p1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    .line 85518
    sget-object v1, LX/0ah;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85519
    :cond_0
    aget-object v0, v0, p0

    return-object v0
.end method

.method public static a(Ljava/nio/ByteBuffer;IILX/4Bv;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/nio/ByteBuffer;",
            "II",
            "LX/4Bv",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 85596
    invoke-static {p0, p1, p2}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 85597
    if-eqz v0, :cond_0

    .line 85598
    :try_start_0
    invoke-interface {p3, p0, v0}, LX/4Bv;->a(Ljava/nio/ByteBuffer;I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 85599
    :goto_0
    return-object v0

    .line 85600
    :catch_0
    move-exception v0

    .line 85601
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Not able to create object"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 85602
    :catch_1
    move-exception v0

    .line 85603
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Access to constructor denied"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 85604
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/nio/ByteBuffer;IILjava/lang/Class;LX/4Bv;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            "L::Ljava/util/List",
            "<TV;>;F::",
            "LX/4Bv",
            "<TV;>;>(",
            "Ljava/nio/ByteBuffer;",
            "II",
            "Ljava/lang/Class",
            "<T",
            "L;",
            ">;TF;)T",
            "L;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 85605
    :try_start_0
    invoke-static {p0, p1, p2}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v1

    .line 85606
    if-eqz v1, :cond_1

    .line 85607
    invoke-static {p0, v1}, LX/0ah;->d(Ljava/nio/ByteBuffer;I)I

    move-result v2

    .line 85608
    add-int/lit8 v0, v1, 0x4

    move v3, v0

    .line 85609
    invoke-virtual {p3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 85610
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 85611
    mul-int/lit8 v4, v1, 0x4

    add-int/2addr v4, v3

    .line 85612
    invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v5

    .line 85613
    if-nez v5, :cond_0

    .line 85614
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85615
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 85616
    :cond_0
    add-int/2addr v4, v5

    invoke-interface {p4, p0, v4}, LX/4Bv;->a(Ljava/nio/ByteBuffer;I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 85617
    :catch_0
    move-exception v0

    .line 85618
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Not able to create object"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 85619
    :catch_1
    move-exception v0

    .line 85620
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Access to constructor denied"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 85621
    :cond_1
    return-object v0
.end method

.method private static a(Ljava/lang/Class;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M::",
            "Ljava/util/Map",
            "<**>;>(",
            "Ljava/lang/Class",
            "<TM;>;)TM;"
        }
    .end annotation

    .prologue
    .line 85622
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 85623
    return-object v0

    .line 85624
    :catch_0
    move-exception v0

    .line 85625
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Not able to create object"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 85626
    :catch_1
    move-exception v0

    .line 85627
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Access to constructor denied"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Ljava/lang/Class;Ljava/util/Iterator;Ljava/util/Iterator;)Ljava/util/Map;
    .locals 1
    .param p1    # Ljava/util/Iterator;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/Iterator;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            "M::",
            "Ljava/util/Map",
            "<TK;TV;>;>(",
            "Ljava/lang/Class",
            "<TM;>;",
            "Ljava/util/Iterator",
            "<TK;>;",
            "Ljava/util/Iterator",
            "<TV;>;)TM;"
        }
    .end annotation

    .prologue
    .line 85520
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 85521
    :cond_0
    invoke-static {p0}, LX/0ah;->a(Ljava/lang/Class;)Ljava/util/Map;

    move-result-object v0

    .line 85522
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0, p1, p2}, LX/0ah;->b(Ljava/lang/Class;Ljava/util/Iterator;Ljava/util/Iterator;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/nio/ByteBuffer;IIS)S
    .locals 1

    .prologue
    .line 85523
    invoke-static {p0, p1, p2}, LX/0ah;->k(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 85524
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result p3

    :cond_0
    return p3
.end method

.method public static a(Ljava/nio/ByteBuffer;II)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 85429
    invoke-static {p0, p1, p2}, LX/0ah;->k(Ljava/nio/ByteBuffer;II)I

    move-result v2

    .line 85430
    if-eqz v2, :cond_1

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static b(Ljava/nio/ByteBuffer;III)I
    .locals 2

    .prologue
    .line 85431
    if-nez p1, :cond_1

    .line 85432
    :cond_0
    :goto_0
    return p3

    .line 85433
    :cond_1
    invoke-static {p0, p1}, LX/0ah;->d(Ljava/nio/ByteBuffer;I)I

    move-result v0

    .line 85434
    if-ltz p2, :cond_0

    if-ge p2, v0, :cond_0

    .line 85435
    add-int/lit8 v0, p1, 0x4

    move v0, v0

    .line 85436
    mul-int/lit8 v1, p2, 0x4

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result p3

    goto :goto_0
.end method

.method public static b(Ljava/nio/ByteBuffer;IILX/1VQ;Ljava/lang/Object;)LX/1VR;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<THelper:",
            "Ljava/lang/Object;",
            "TItem:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/nio/ByteBuffer;",
            "II",
            "LX/1VQ",
            "<TTHelper;TTItem;>;TTHelper;)",
            "LX/1VR",
            "<TTHelper;TTItem;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 85437
    invoke-static {p0, p1, p2}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 85438
    invoke-static {p0, v0, p3, p4}, LX/0ah;->a(Ljava/nio/ByteBuffer;ILX/1VQ;Ljava/lang/Object;)LX/1VR;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 85439
    invoke-static {p0, p1, p2}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 85440
    if-eqz v0, :cond_0

    .line 85441
    invoke-static {p0, v0}, LX/0ah;->c(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v0

    .line 85442
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/nio/ByteBuffer;IILjava/lang/Class;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<",
            "L::Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>(",
            "Ljava/nio/ByteBuffer;",
            "II",
            "Ljava/lang/Class",
            "<T",
            "L;",
            ">;)T",
            "L;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 85443
    invoke-static {p0, p1, p2}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 85444
    if-eqz v0, :cond_1

    .line 85445
    invoke-static {p0, v0}, LX/0ah;->d(Ljava/nio/ByteBuffer;I)I

    move-result v3

    .line 85446
    add-int/lit8 v2, v0, 0x4

    move v4, v2

    .line 85447
    :try_start_0
    invoke-virtual {p3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 85448
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_2

    .line 85449
    mul-int/lit8 v5, v2, 0x4

    add-int/2addr v5, v4

    .line 85450
    invoke-virtual {p0, v5}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v6

    .line 85451
    if-nez v6, :cond_0

    .line 85452
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85453
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 85454
    :catch_0
    move-exception v0

    .line 85455
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Not able to create object"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 85456
    :catch_1
    move-exception v0

    .line 85457
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Access to constructor denied"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 85458
    :cond_0
    add-int/2addr v5, v6

    invoke-static {p0, v5}, LX/0ah;->c(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 85459
    :cond_2
    return-object v0
.end method

.method private static b(Ljava/lang/Class;Ljava/util/Iterator;Ljava/util/Iterator;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            "M::",
            "Ljava/util/Map",
            "<TK;TV;>;>(",
            "Ljava/lang/Class",
            "<TM;>;",
            "Ljava/util/Iterator",
            "<TK;>;",
            "Ljava/util/Iterator",
            "<TV;>;)TM;"
        }
    .end annotation

    .prologue
    .line 85460
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 85461
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85462
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 85463
    :catch_0
    move-exception v0

    .line 85464
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Not able to create object"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 85465
    :catch_1
    move-exception v0

    .line 85466
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Access to constructor denied"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 85467
    :cond_0
    return-object v0
.end method

.method public static c(Ljava/nio/ByteBuffer;I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 85468
    invoke-virtual {p0, p1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    .line 85469
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->hasArray()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85470
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v3

    add-int/2addr v3, p1

    add-int/lit8 v3, v3, 0x4

    sget-object v4, LX/0ah;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, v2, v3, v1, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    .line 85471
    :goto_0
    return-object v0

    .line 85472
    :cond_0
    new-array v2, v1, [B

    .line 85473
    monitor-enter p0

    .line 85474
    :try_start_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    .line 85475
    add-int/lit8 v3, p1, 0x4

    invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 85476
    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 85477
    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 85478
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85479
    new-instance v0, Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, LX/0ah;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, v2, v3, v1, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    goto :goto_0

    .line 85480
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static c(Ljava/nio/ByteBuffer;IILjava/lang/Class;)Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(",
            "Ljava/nio/ByteBuffer;",
            "II",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 85481
    sget-object v0, LX/1VP;->a:LX/1VP;

    .line 85482
    invoke-static {p0, p1, p2, v0, p3}, LX/0ah;->b(Ljava/nio/ByteBuffer;IILX/1VQ;Ljava/lang/Object;)LX/1VR;

    move-result-object v0

    return-object v0
.end method

.method public static d(Ljava/nio/ByteBuffer;I)I
    .locals 1

    .prologue
    .line 85483
    invoke-virtual {p0, p1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    return v0
.end method

.method public static d(Ljava/nio/ByteBuffer;II)Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            "II)",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 85484
    sget-object v0, LX/4Bs;->a:LX/4Bs;

    sget-object v1, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    invoke-static {p0, p1, p2, v0, v1}, LX/0ah;->b(Ljava/nio/ByteBuffer;IILX/1VQ;Ljava/lang/Object;)LX/1VR;

    move-result-object v0

    return-object v0
.end method

.method public static e(Ljava/nio/ByteBuffer;IILjava/lang/Class;)Ljava/util/Iterator;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(",
            "Ljava/nio/ByteBuffer;",
            "II",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 85485
    :try_start_0
    const-string v0, "fromString"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p3, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 85486
    :goto_0
    new-instance v1, LX/3DE;

    invoke-direct {v1, v0}, LX/3DE;-><init>(Ljava/lang/reflect/Method;)V

    invoke-static {p0, p1, p2, v1, p3}, LX/0ah;->b(Ljava/nio/ByteBuffer;IILX/1VQ;Ljava/lang/Object;)LX/1VR;

    move-result-object v0

    return-object v0

    .line 85487
    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Ljava/nio/ByteBuffer;II)Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            "II)",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 85488
    sget-object v0, LX/1VT;->a:LX/1VT;

    sget-object v1, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    invoke-static {p0, p1, p2, v0, v1}, LX/0ah;->b(Ljava/nio/ByteBuffer;IILX/1VQ;Ljava/lang/Object;)LX/1VR;

    move-result-object v0

    return-object v0
.end method

.method public static g(Ljava/nio/ByteBuffer;IILjava/lang/Class;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M::",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>(",
            "Ljava/nio/ByteBuffer;",
            "II",
            "Ljava/lang/Class",
            "<TM;>;)TM;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 85489
    invoke-static {p0, p1, p2}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 85490
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 85491
    :goto_0
    return-object v0

    .line 85492
    :cond_0
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/0ah;->g(Ljava/nio/ByteBuffer;II)Ljava/util/Iterator;

    move-result-object v1

    .line 85493
    const/4 v2, 0x1

    invoke-static {p0, v0, v2}, LX/0ah;->g(Ljava/nio/ByteBuffer;II)Ljava/util/Iterator;

    move-result-object v0

    .line 85494
    invoke-static {p3, v1, v0}, LX/0ah;->a(Ljava/lang/Class;Ljava/util/Iterator;Ljava/util/Iterator;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public static i(Ljava/nio/ByteBuffer;II)I
    .locals 2

    .prologue
    .line 85495
    if-nez p1, :cond_0

    .line 85496
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    .line 85497
    :cond_0
    invoke-static {p0, p1}, LX/0ah;->d(Ljava/nio/ByteBuffer;I)I

    move-result v0

    .line 85498
    if-ltz p2, :cond_1

    if-lt p2, v0, :cond_2

    .line 85499
    :cond_1
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    .line 85500
    :cond_2
    add-int/lit8 v0, p1, 0x4

    move v0, v0

    .line 85501
    mul-int/lit8 v1, p2, 0x4

    add-int/2addr v0, v1

    .line 85502
    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    .line 85503
    if-nez v1, :cond_3

    .line 85504
    const/4 v0, 0x0

    .line 85505
    :goto_0
    return v0

    :cond_3
    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public static j(Ljava/nio/ByteBuffer;II)I
    .locals 2

    .prologue
    .line 85506
    invoke-static {p0, p1, p2}, LX/0ah;->k(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 85507
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k(Ljava/nio/ByteBuffer;II)I
    .locals 3

    .prologue
    .line 85508
    invoke-virtual {p0, p1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    sub-int v0, p1, v0

    .line 85509
    mul-int/lit8 v1, p2, 0x2

    add-int/lit8 v1, v1, 0x4

    .line 85510
    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v2

    if-ge v1, v2, :cond_0

    .line 85511
    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    .line 85512
    if-eqz v0, :cond_0

    .line 85513
    add-int/2addr v0, p1

    .line 85514
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
