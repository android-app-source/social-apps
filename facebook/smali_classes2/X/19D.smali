.class public LX/19D;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static final a:Z


# instance fields
.field private final b:LX/0Sh;

.field public final c:LX/03V;

.field public final d:Z

.field private e:LX/19F;

.field public f:LX/196;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 207449
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/19D;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/Boolean;LX/0Sh;LX/03V;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 207450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207451
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/19D;->d:Z

    .line 207452
    iput-object p2, p0, LX/19D;->b:LX/0Sh;

    .line 207453
    const/4 v0, 0x0

    iput-object v0, p0, LX/19D;->f:LX/196;

    .line 207454
    iput-object p3, p0, LX/19D;->c:LX/03V;

    .line 207455
    sget-boolean v0, LX/19D;->a:Z

    if-nez v0, :cond_0

    .line 207456
    :goto_0
    return-void

    .line 207457
    :cond_0
    iget-boolean v0, p0, LX/19D;->d:Z

    if-eqz v0, :cond_1

    .line 207458
    new-instance v0, LX/1KD;

    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object p1

    iget-object p2, p0, LX/19D;->c:LX/03V;

    invoke-direct {v0, p0, p1, p2}, LX/1KD;-><init>(LX/19D;Landroid/view/Choreographer;LX/03V;)V

    invoke-static {p0, v0}, LX/19D;->a(LX/19D;LX/19F;)V

    goto :goto_0

    .line 207459
    :cond_1
    new-instance v0, LX/19E;

    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object p1

    invoke-direct {v0, p0, p1}, LX/19E;-><init>(LX/19D;Landroid/view/Choreographer;)V

    invoke-static {p0, v0}, LX/19D;->a(LX/19D;LX/19F;)V

    goto :goto_0
.end method

.method public static a(LX/19D;LX/19F;)V
    .locals 1

    .prologue
    .line 207460
    iget-object v0, p0, LX/19D;->e:LX/19F;

    if-eqz v0, :cond_0

    .line 207461
    iget-object v0, p0, LX/19D;->e:LX/19F;

    invoke-interface {v0}, LX/19F;->b()V

    .line 207462
    :cond_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207463
    iput-object p1, p0, LX/19D;->e:LX/19F;

    .line 207464
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 207465
    sget-boolean v0, LX/19D;->a:Z

    if-nez v0, :cond_0

    .line 207466
    :goto_0
    return-void

    .line 207467
    :cond_0
    iget-object v0, p0, LX/19D;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 207468
    iget-object v0, p0, LX/19D;->e:LX/19F;

    invoke-interface {v0}, LX/19F;->a()V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 207469
    iget-object v0, p0, LX/19D;->f:LX/196;

    invoke-interface {v0, p1}, LX/196;->a(I)V

    .line 207470
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 207471
    sget-boolean v0, LX/19D;->a:Z

    if-nez v0, :cond_0

    .line 207472
    :goto_0
    return-void

    .line 207473
    :cond_0
    iget-object v0, p0, LX/19D;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 207474
    iget-object v0, p0, LX/19D;->e:LX/19F;

    invoke-interface {v0}, LX/19F;->b()V

    goto :goto_0
.end method
