.class public LX/0uD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0u4;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Landroid/content/Context;

.field private c:LX/0uE;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 156097
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/0uD;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 156098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156099
    iput-object p1, p0, LX/0uD;->b:Landroid/content/Context;

    .line 156100
    return-void
.end method

.method private b()Ljava/lang/String;
    .locals 8

    .prologue
    .line 156101
    const/16 v2, 0x64

    .line 156102
    :try_start_0
    iget-object v0, p0, LX/0uD;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "camera"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/CameraManager;

    .line 156103
    invoke-virtual {v0}, Landroid/hardware/camera2/CameraManager;->getCameraIdList()[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    const/4 v1, 0x0

    move v3, v1

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v1, v4, v3

    .line 156104
    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v1

    .line 156105
    sget-object v6, Landroid/hardware/camera2/CameraCharacteristics;->INFO_SUPPORTED_HARDWARE_LEVEL:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v1, v6}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 156106
    sget-object v6, LX/0uD;->a:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 156107
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result p0

    if-eq p0, v2, :cond_1

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result p0

    if-ne p0, v1, :cond_0

    .line 156108
    :cond_1
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 156109
    :goto_1
    move v2, v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 156110
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 156111
    :goto_2
    const-string v1, "NONE"

    .line 156112
    const/4 v2, 0x3

    if-lt v0, v2, :cond_4

    .line 156113
    const-string v1, "LEVEL_%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 156114
    :goto_3
    move-object v0, v1

    .line 156115
    return-object v0

    .line 156116
    :catch_0
    const/4 v0, 0x2

    goto :goto_2

    :cond_3
    :try_start_1
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v6

    goto :goto_1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 156117
    :cond_4
    packed-switch v0, :pswitch_data_0

    goto :goto_3

    .line 156118
    :pswitch_0
    const-string v1, "LIMITED"

    goto :goto_3

    .line 156119
    :pswitch_1
    const-string v1, "LEGACY"

    goto :goto_3

    .line 156120
    :pswitch_2
    const-string v1, "FULL"

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(J)LX/0uE;
    .locals 3

    .prologue
    .line 156121
    iget-object v0, p0, LX/0uD;->c:LX/0uE;

    if-nez v0, :cond_0

    .line 156122
    new-instance v1, LX/0uE;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v0, v2, :cond_1

    const-string v0, "Camera1HardwareSupportedLevel"

    :goto_0
    invoke-direct {v1, v0}, LX/0uE;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, LX/0uD;->c:LX/0uE;

    .line 156123
    :cond_0
    iget-object v0, p0, LX/0uD;->c:LX/0uE;

    return-object v0

    .line 156124
    :cond_1
    invoke-direct {p0}, LX/0uD;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/0u5;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156125
    new-instance v0, LX/0u5;

    const-string v1, "camera_hardware_supported_level"

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, p0, v2, v3}, LX/0u5;-><init>(Ljava/lang/String;LX/0u4;J)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
