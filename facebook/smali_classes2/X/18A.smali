.class public LX/18A;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/storyavailability/StoryAvailabilityListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/Set;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/storyavailability/StoryAvailabilityListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 205983
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 205984
    iput-object p1, p0, LX/18A;->a:Ljava/util/Set;

    .line 205985
    return-void
.end method

.method public static b(LX/0QB;)LX/18A;
    .locals 4

    .prologue
    .line 205986
    new-instance v0, LX/18A;

    .line 205987
    new-instance v1, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v2

    new-instance v3, LX/18B;

    invoke-direct {v3, p0}, LX/18B;-><init>(LX/0QB;)V

    invoke-direct {v1, v2, v3}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v1, v1

    .line 205988
    invoke-direct {v0, v1}, LX/18A;-><init>(Ljava/util/Set;)V

    .line 205989
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[B)V
    .locals 9

    .prologue
    .line 205990
    iget-object v0, p0, LX/18A;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fI;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    .line 205991
    invoke-virtual/range {v0 .. v7}, LX/1fI;->a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[B)V

    goto :goto_0

    .line 205992
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 205993
    iget-object v0, p0, LX/18A;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fI;

    .line 205994
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/1fI;->a(LX/0Px;)V

    .line 205995
    goto :goto_0

    .line 205996
    :cond_0
    return-void
.end method
