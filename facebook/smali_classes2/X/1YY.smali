.class public LX/1YY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0i1;


# instance fields
.field private final a:LX/0SG;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 273627
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273628
    iput-object p2, p0, LX/1YY;->a:LX/0SG;

    .line 273629
    iput-object p1, p0, LX/1YY;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 273630
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 273631
    const-wide/32 v0, 0xa4cb800

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 273632
    const-string v0, "2943"

    invoke-static {v0}, LX/11b;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 273633
    iget-object v1, p0, LX/1YY;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 273634
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/1YY;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/32 v4, 0xa4cb800

    add-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    .line 273635
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 273636
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 273637
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 273638
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273639
    const-string v0, "2862"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 273640
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_CARET:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
