.class public final enum LX/1Hy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1Hy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1Hy;

.field public static final enum KEY_128:LX/1Hy;

.field public static final enum KEY_256:LX/1Hy;


# instance fields
.field public final cipherId:B

.field public final ivLength:I

.field public final keyLength:I

.field public final tagLength:I


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    .prologue
    const/16 v5, 0xc

    const/4 v9, 0x2

    const/4 v2, 0x0

    const/16 v4, 0x10

    const/4 v3, 0x1

    .line 227998
    new-instance v0, LX/1Hy;

    const-string v1, "KEY_128"

    move v6, v4

    invoke-direct/range {v0 .. v6}, LX/1Hy;-><init>(Ljava/lang/String;IBIII)V

    sput-object v0, LX/1Hy;->KEY_128:LX/1Hy;

    .line 227999
    new-instance v6, LX/1Hy;

    const-string v7, "KEY_256"

    const/16 v10, 0x20

    move v8, v3

    move v11, v5

    move v12, v4

    invoke-direct/range {v6 .. v12}, LX/1Hy;-><init>(Ljava/lang/String;IBIII)V

    sput-object v6, LX/1Hy;->KEY_256:LX/1Hy;

    .line 228000
    new-array v0, v9, [LX/1Hy;

    sget-object v1, LX/1Hy;->KEY_128:LX/1Hy;

    aput-object v1, v0, v2

    sget-object v1, LX/1Hy;->KEY_256:LX/1Hy;

    aput-object v1, v0, v3

    sput-object v0, LX/1Hy;->$VALUES:[LX/1Hy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IBIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(BIII)V"
        }
    .end annotation

    .prologue
    .line 228001
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 228002
    iput-byte p3, p0, LX/1Hy;->cipherId:B

    .line 228003
    iput p4, p0, LX/1Hy;->keyLength:I

    .line 228004
    iput p5, p0, LX/1Hy;->ivLength:I

    .line 228005
    iput p6, p0, LX/1Hy;->tagLength:I

    .line 228006
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1Hy;
    .locals 1

    .prologue
    .line 228007
    const-class v0, LX/1Hy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1Hy;

    return-object v0
.end method

.method public static values()[LX/1Hy;
    .locals 1

    .prologue
    .line 228008
    sget-object v0, LX/1Hy;->$VALUES:[LX/1Hy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1Hy;

    return-object v0
.end method
