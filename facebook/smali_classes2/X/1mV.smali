.class public LX/1mV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/3cy;",
        "LX/1mb;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 0

    .prologue
    .line 313849
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313850
    iput-object p1, p0, LX/1mV;->a:LX/0SG;

    .line 313851
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 313860
    check-cast p1, LX/3cy;

    .line 313861
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 313862
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "attribution"

    iget-object v2, p1, LX/3cy;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313863
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "fb_device"

    iget-object v2, p1, LX/3cy;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313864
    iget-object v0, p1, LX/3cy;->b:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    if-eqz v0, :cond_0

    .line 313865
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "gms_advertiser_id"

    iget-object v2, p1, LX/3cy;->b:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    iget-object v3, v2, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->a:Ljava/lang/String;

    move-object v2, v3

    .line 313866
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313867
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "tracking_enabled"

    iget-object v0, p1, LX/3cy;->b:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    iget-boolean v3, v0, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->b:Z

    move v0, v3

    .line 313868
    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313869
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "gms_interop_fix"

    iget-boolean v2, p1, LX/3cy;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313870
    :cond_0
    iget-object v0, p1, LX/3cy;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 313871
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "previous_advertising_id"

    iget-object v2, p1, LX/3cy;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313872
    :cond_1
    iget-object v0, p1, LX/3cy;->g:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 313873
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "oxygen_attribution"

    iget-object v2, p1, LX/3cy;->g:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313874
    :cond_2
    new-instance v0, LX/14N;

    const-string v1, "postNewAttributionId"

    const-string v2, "POST"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v6, p1, LX/3cy;->f:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/attributions"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0

    .line 313875
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 313852
    check-cast p1, LX/3cy;

    const/4 v7, 0x0

    const/4 v0, 0x0

    .line 313853
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v1

    .line 313854
    if-eqz v1, :cond_2

    .line 313855
    const-string v2, "should_relay_android_id"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 313856
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/0lF;->F()Z

    move-result v0

    .line 313857
    :cond_0
    const-string v2, "apps"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    move v6, v0

    .line 313858
    :goto_0
    new-instance v0, LX/1mb;

    iget-object v1, p1, LX/3cy;->a:Ljava/lang/String;

    iget-wide v2, p1, LX/3cy;->f:J

    iget-object v4, p0, LX/1mV;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    iget-object v9, p1, LX/3cy;->b:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    if-eqz v9, :cond_1

    iget-object v7, p1, LX/3cy;->b:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    iget-object v9, v7, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->a:Ljava/lang/String;

    move-object v7, v9

    .line 313859
    :cond_1
    invoke-direct/range {v0 .. v8}, LX/1mb;-><init>(Ljava/lang/String;JJZLjava/lang/String;LX/0lF;)V

    return-object v0

    :cond_2
    move-object v8, v7

    move v6, v0

    goto :goto_0
.end method
