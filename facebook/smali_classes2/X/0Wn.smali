.class public LX/0Wn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0Wn;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 76736
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76737
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Wn;->c:Z

    .line 76738
    iput-object p1, p0, LX/0Wn;->a:LX/0Ot;

    .line 76739
    iput-object p2, p0, LX/0Wn;->b:LX/0Ot;

    .line 76740
    return-void
.end method

.method public static a(LX/0QB;)LX/0Wn;
    .locals 5

    .prologue
    .line 76723
    sget-object v0, LX/0Wn;->d:LX/0Wn;

    if-nez v0, :cond_1

    .line 76724
    const-class v1, LX/0Wn;

    monitor-enter v1

    .line 76725
    :try_start_0
    sget-object v0, LX/0Wn;->d:LX/0Wn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 76726
    if-eqz v2, :cond_0

    .line 76727
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 76728
    new-instance v3, LX/0Wn;

    const/16 v4, 0xf12

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0xbc

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/0Wn;-><init>(LX/0Ot;LX/0Ot;)V

    .line 76729
    move-object v0, v3

    .line 76730
    sput-object v0, LX/0Wn;->d:LX/0Wn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76731
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 76732
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 76733
    :cond_1
    sget-object v0, LX/0Wn;->d:LX/0Wn;

    return-object v0

    .line 76734
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 76735
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0Wn;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 76721
    iget-object v0, p0, LX/0Wn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 76722
    return-void
.end method

.method public static a(LX/0Wn;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 76716
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "fbresources_bad_language_pack_info"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 76717
    const-string v0, "type"

    invoke-virtual {v1, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 76718
    invoke-virtual {v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 76719
    iget-object v0, p0, LX/0Wn;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 76720
    return-void
.end method

.method public static b(LX/0Wn;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 76714
    iget-object v0, p0, LX/0Wn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 76715
    return-void
.end method

.method public static c(LX/0Wn;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 76741
    iget-object v0, p0, LX/0Wn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 76742
    return-void
.end method


# virtual methods
.method public final a(LX/0ec;LX/0ed;Ljava/lang/Throwable;)V
    .locals 3
    .param p1    # LX/0ec;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0ed;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 76705
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "fbresources_loading_failure"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 76706
    if-eqz p1, :cond_0

    .line 76707
    const-string v0, "source"

    invoke-virtual {p1}, LX/0ec;->getLoggingValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 76708
    :cond_0
    if-eqz p2, :cond_1

    .line 76709
    const-string v0, "locale"

    invoke-virtual {p2}, LX/0ed;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 76710
    :cond_1
    if-eqz p3, :cond_2

    .line 76711
    const-string v0, "error"

    invoke-virtual {p3}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 76712
    :cond_2
    iget-object v0, p0, LX/0Wn;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 76713
    return-void
.end method

.method public final c(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 76703
    const-string v0, "invalid"

    invoke-static {p0, v0, p1}, LX/0Wn;->a(LX/0Wn;Ljava/lang/String;Ljava/util/Map;)V

    .line 76704
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 76701
    const v0, 0x440004

    const-string v1, "FbResourcesWaitingActivity"

    invoke-static {p0, v0, v1}, LX/0Wn;->a(LX/0Wn;ILjava/lang/String;)V

    .line 76702
    return-void
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 76699
    const v0, 0x440003

    const-string v1, "FbResourcesDownloadFile"

    invoke-static {p0, v0, v1}, LX/0Wn;->a(LX/0Wn;ILjava/lang/String;)V

    .line 76700
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 76697
    const v0, 0x440003

    const-string v1, "FbResourcesDownloadFile"

    invoke-static {p0, v0, v1}, LX/0Wn;->b(LX/0Wn;ILjava/lang/String;)V

    .line 76698
    return-void
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 76695
    const v0, 0x440003

    const-string v1, "FbResourcesDownloadFile"

    invoke-static {p0, v0, v1}, LX/0Wn;->c(LX/0Wn;ILjava/lang/String;)V

    .line 76696
    return-void
.end method
