.class public LX/1Zr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0ih;

.field public final b:J

.field public final c:I

.field public final d:J

.field public final e:Z

.field public f:J

.field public g:LX/0UE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0UE",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1Zt;",
            ">;"
        }
    .end annotation
.end field

.field public i:Z


# direct methods
.method public constructor <init>(LX/0ih;JIJ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 275552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275553
    iput-boolean v2, p0, LX/1Zr;->i:Z

    .line 275554
    iput-object p1, p0, LX/1Zr;->a:LX/0ih;

    .line 275555
    iput-wide p2, p0, LX/1Zr;->b:J

    .line 275556
    iput-wide p5, p0, LX/1Zr;->d:J

    .line 275557
    iget-wide v0, p0, LX/1Zr;->d:J

    iput-wide v0, p0, LX/1Zr;->f:J

    .line 275558
    iput p4, p0, LX/1Zr;->c:I

    .line 275559
    iput-boolean v2, p0, LX/1Zr;->e:Z

    .line 275560
    return-void
.end method

.method private constructor <init>(LX/0ih;JIJZ)V
    .locals 2

    .prologue
    .line 275476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275477
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Zr;->i:Z

    .line 275478
    iput-object p1, p0, LX/1Zr;->a:LX/0ih;

    .line 275479
    iput-wide p2, p0, LX/1Zr;->b:J

    .line 275480
    iput-wide p5, p0, LX/1Zr;->d:J

    .line 275481
    iget-wide v0, p0, LX/1Zr;->d:J

    iput-wide v0, p0, LX/1Zr;->f:J

    .line 275482
    iput p4, p0, LX/1Zr;->c:I

    .line 275483
    iput-boolean p7, p0, LX/1Zr;->e:Z

    .line 275484
    return-void
.end method

.method public constructor <init>(LX/2o7;)V
    .locals 2

    .prologue
    .line 275540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275541
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Zr;->i:Z

    .line 275542
    iget-object v0, p1, LX/2o7;->a:LX/0ih;

    iput-object v0, p0, LX/1Zr;->a:LX/0ih;

    .line 275543
    iget-wide v0, p1, LX/2o7;->b:J

    iput-wide v0, p0, LX/1Zr;->b:J

    .line 275544
    iget-wide v0, p1, LX/2o7;->d:J

    iput-wide v0, p0, LX/1Zr;->d:J

    .line 275545
    iget-wide v0, p1, LX/2o7;->e:J

    iput-wide v0, p0, LX/1Zr;->f:J

    .line 275546
    iget v0, p1, LX/2o7;->c:I

    iput v0, p0, LX/1Zr;->c:I

    .line 275547
    iget-object v0, p1, LX/2o7;->f:LX/0UE;

    iput-object v0, p0, LX/1Zr;->g:LX/0UE;

    .line 275548
    iget-object v0, p1, LX/2o7;->g:Ljava/util/List;

    iput-object v0, p0, LX/1Zr;->h:Ljava/util/List;

    .line 275549
    iget-boolean v0, p1, LX/2o7;->h:Z

    iput-boolean v0, p0, LX/1Zr;->i:Z

    .line 275550
    iget-boolean v0, p1, LX/2o7;->i:Z

    iput-boolean v0, p0, LX/1Zr;->e:Z

    .line 275551
    return-void
.end method

.method public static a(LX/0ih;J)LX/1Zr;
    .locals 9

    .prologue
    .line 275539
    new-instance v0, LX/1Zr;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x1

    move-object v1, p0

    move-wide v5, p1

    invoke-direct/range {v0 .. v7}, LX/1Zr;-><init>(LX/0ih;JIJZ)V

    return-object v0
.end method

.method private static o(LX/1Zr;)S
    .locals 1

    .prologue
    .line 275535
    invoke-static {p0}, LX/1Zr;->p(LX/1Zr;)V

    .line 275536
    iget-object v0, p0, LX/1Zr;->a:LX/0ih;

    .line 275537
    iget-short p0, v0, LX/0ih;->b:S

    move v0, p0

    .line 275538
    return v0
.end method

.method public static p(LX/1Zr;)V
    .locals 2

    .prologue
    .line 275531
    iget-boolean v0, p0, LX/1Zr;->e:Z

    move v0, v0

    .line 275532
    if-eqz v0, :cond_0

    .line 275533
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This method is not supported for noop funnels"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 275534
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/1Zt;J)V
    .locals 2

    .prologue
    .line 275524
    iput-wide p2, p0, LX/1Zr;->f:J

    .line 275525
    iget-boolean v0, p0, LX/1Zr;->e:Z

    move v0, v0

    .line 275526
    if-eqz v0, :cond_0

    .line 275527
    :goto_0
    return-void

    .line 275528
    :cond_0
    iget-object v0, p0, LX/1Zr;->h:Ljava/util/List;

    if-nez v0, :cond_1

    .line 275529
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Zr;->h:Ljava/util/List;

    .line 275530
    :cond_1
    iget-object v0, p0, LX/1Zr;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 275521
    iget-object v0, p0, LX/1Zr;->a:LX/0ih;

    .line 275522
    iget-object p0, v0, LX/0ih;->a:Ljava/lang/String;

    move-object v0, p0

    .line 275523
    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 275519
    invoke-static {p0}, LX/1Zr;->p(LX/1Zr;)V

    .line 275520
    iget-boolean v0, p0, LX/1Zr;->i:Z

    return v0
.end method

.method public final n()Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 6

    .prologue
    .line 275485
    invoke-static {p0}, LX/1Zr;->p(LX/1Zr;)V

    .line 275486
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "funnel_analytics"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 275487
    const-string v0, "name"

    invoke-virtual {p0}, LX/1Zr;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 275488
    const-string v0, "funnel_id"

    invoke-static {p0}, LX/1Zr;->o(LX/1Zr;)S

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 275489
    const-string v0, "instance_id"

    iget-wide v2, p0, LX/1Zr;->b:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 275490
    const-string v0, "start_time"

    iget-wide v2, p0, LX/1Zr;->d:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 275491
    const-string v0, "sampling_rate"

    iget v2, p0, LX/1Zr;->c:I

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 275492
    iget-boolean v0, p0, LX/1Zr;->i:Z

    if-eqz v0, :cond_1

    .line 275493
    iget-object v0, p0, LX/1Zr;->g:LX/0UE;

    if-nez v0, :cond_0

    .line 275494
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, LX/1Zr;->g:LX/0UE;

    .line 275495
    :cond_0
    iget-object v0, p0, LX/1Zr;->g:LX/0UE;

    const-string v2, "tracked"

    invoke-virtual {v0, v2}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 275496
    :cond_1
    iget-object v0, p0, LX/1Zr;->g:LX/0UE;

    if-eqz v0, :cond_3

    .line 275497
    new-instance v2, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/162;-><init>(LX/0mC;)V

    .line 275498
    iget-object v0, p0, LX/1Zr;->g:LX/0UE;

    invoke-virtual {v0}, LX/0UE;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 275499
    invoke-virtual {v2, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_0

    .line 275500
    :cond_2
    const-string v0, "tags"

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 275501
    :cond_3
    iget-object v0, p0, LX/1Zr;->h:Ljava/util/List;

    if-eqz v0, :cond_7

    .line 275502
    new-instance v2, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/162;-><init>(LX/0mC;)V

    .line 275503
    iget-object v0, p0, LX/1Zr;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Zt;

    .line 275504
    new-instance v4, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v5}, LX/0m9;-><init>(LX/0mC;)V

    .line 275505
    const-string v5, "name"

    iget-object p0, v0, LX/1Zt;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 275506
    const-string v5, "relative_time"

    iget p0, v0, LX/1Zt;->e:I

    invoke-virtual {v4, v5, p0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 275507
    iget-object v5, v0, LX/1Zt;->b:Ljava/lang/String;

    if-eqz v5, :cond_4

    .line 275508
    const-string v5, "tag"

    iget-object p0, v0, LX/1Zt;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 275509
    :cond_4
    iget-object v5, v0, LX/1Zt;->c:LX/1rQ;

    if-eqz v5, :cond_8

    .line 275510
    const-string v5, "payload"

    iget-object p0, v0, LX/1Zt;->c:LX/1rQ;

    .line 275511
    iget-object v0, p0, LX/1rQ;->a:LX/0m9;

    move-object p0, v0

    .line 275512
    invoke-virtual {v4, v5, p0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 275513
    :cond_5
    :goto_2
    move-object v0, v4

    .line 275514
    invoke-virtual {v2, v0}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_1

    .line 275515
    :cond_6
    const-string v0, "actions"

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 275516
    :cond_7
    return-object v1

    .line 275517
    :cond_8
    iget-object v5, v0, LX/1Zt;->d:Ljava/lang/String;

    if-eqz v5, :cond_5

    .line 275518
    const-string v5, "payload"

    iget-object p0, v0, LX/1Zt;->d:Ljava/lang/String;

    invoke-virtual {v4, v5, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_2
.end method
