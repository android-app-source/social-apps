.class public LX/0WW;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0Uh;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0Uh;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76435
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0Uh;
    .locals 5

    .prologue
    .line 76422
    sget-object v0, LX/0WW;->a:LX/0Uh;

    if-nez v0, :cond_1

    .line 76423
    const-class v1, LX/0WW;

    monitor-enter v1

    .line 76424
    :try_start_0
    sget-object v0, LX/0WW;->a:LX/0Uh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 76425
    if-eqz v2, :cond_0

    .line 76426
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 76427
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0WX;->a(LX/0QB;)LX/0UW;

    move-result-object v4

    check-cast v4, LX/0UW;

    invoke-static {v0}, LX/0WZ;->a(LX/0QB;)LX/0UY;

    move-result-object p0

    check-cast p0, LX/0UY;

    invoke-static {v3, v4, p0}, Lcom/facebook/gk/sessionless/GkSessionlessModule;->a(Landroid/content/Context;LX/0UW;LX/0UY;)LX/0Uh;

    move-result-object v3

    move-object v0, v3

    .line 76428
    sput-object v0, LX/0WW;->a:LX/0Uh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76429
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 76430
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 76431
    :cond_1
    sget-object v0, LX/0WW;->a:LX/0Uh;

    return-object v0

    .line 76432
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 76433
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 76434
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0WX;->a(LX/0QB;)LX/0UW;

    move-result-object v1

    check-cast v1, LX/0UW;

    invoke-static {p0}, LX/0WZ;->a(LX/0QB;)LX/0UY;

    move-result-object v2

    check-cast v2, LX/0UY;

    invoke-static {v0, v1, v2}, Lcom/facebook/gk/sessionless/GkSessionlessModule;->a(Landroid/content/Context;LX/0UW;LX/0UY;)LX/0Uh;

    move-result-object v0

    return-object v0
.end method
