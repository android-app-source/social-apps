.class public LX/1rx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0ad;

.field public final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 333599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333600
    iput-object p2, p0, LX/1rx;->a:LX/0ad;

    .line 333601
    iput-object p1, p0, LX/1rx;->b:Landroid/content/res/Resources;

    .line 333602
    return-void
.end method

.method public static a(LX/0QB;)LX/1rx;
    .locals 1

    .prologue
    .line 333593
    invoke-static {p0}, LX/1rx;->b(LX/0QB;)LX/1rx;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1rx;
    .locals 3

    .prologue
    .line 333597
    new-instance v2, LX/1rx;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v2, v0, v1}, LX/1rx;-><init>(Landroid/content/res/Resources;LX/0ad;)V

    .line 333598
    return-object v2
.end method


# virtual methods
.method public final h()I
    .locals 3

    .prologue
    .line 333595
    iget-object v0, p0, LX/1rx;->a:LX/0ad;

    sget v1, LX/15r;->m:I

    const/4 v2, 0x5

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 333596
    mul-int/lit16 v0, v0, 0x3e8

    return v0
.end method

.method public final j()Z
    .locals 3

    .prologue
    .line 333594
    iget-object v0, p0, LX/1rx;->a:LX/0ad;

    sget-short v1, LX/15r;->I:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
