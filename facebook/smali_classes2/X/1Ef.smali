.class public LX/1Ef;
.super LX/1Eg;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/1Ef;


# instance fields
.field private a:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private volatile b:Z

.field private c:LX/1EZ;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0SG;

.field private final f:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0SG;LX/0Uh;)V
    .locals 2
    .param p1    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "LX/0SG;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 220720
    const-string v0, "FailedUploadRetryTask"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 220721
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/1Ef;->a:J

    .line 220722
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Ef;->b:Z

    .line 220723
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Ef;->c:LX/1EZ;

    .line 220724
    iput-object p1, p0, LX/1Ef;->d:LX/0Ot;

    .line 220725
    iput-object p2, p0, LX/1Ef;->e:LX/0SG;

    .line 220726
    iput-object p3, p0, LX/1Ef;->f:LX/0Uh;

    .line 220727
    return-void
.end method

.method public static a(LX/0QB;)LX/1Ef;
    .locals 6

    .prologue
    .line 220732
    sget-object v0, LX/1Ef;->g:LX/1Ef;

    if-nez v0, :cond_1

    .line 220733
    const-class v1, LX/1Ef;

    monitor-enter v1

    .line 220734
    :try_start_0
    sget-object v0, LX/1Ef;->g:LX/1Ef;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 220735
    if-eqz v2, :cond_0

    .line 220736
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 220737
    new-instance v5, LX/1Ef;

    const/16 v3, 0x1430

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {v5, p0, v3, v4}, LX/1Ef;-><init>(LX/0Ot;LX/0SG;LX/0Uh;)V

    .line 220738
    move-object v0, v5

    .line 220739
    sput-object v0, LX/1Ef;->g:LX/1Ef;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 220740
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 220741
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 220742
    :cond_1
    sget-object v0, LX/1Ef;->g:LX/1Ef;

    return-object v0

    .line 220743
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 220744
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(J)V
    .locals 9

    .prologue
    const-wide/16 v6, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 220745
    monitor-enter p0

    :try_start_0
    iget-wide v2, p0, LX/1Ef;->a:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    iget-wide v2, p0, LX/1Ef;->a:J

    cmp-long v2, p1, v2

    if-gez v2, :cond_2

    :cond_0
    move v2, v0

    .line 220746
    :goto_0
    iput-wide p1, p0, LX/1Ef;->a:J

    .line 220747
    iget-wide v4, p0, LX/1Ef;->a:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    :goto_1
    iput-boolean v0, p0, LX/1Ef;->b:Z

    .line 220748
    iget-boolean v0, p0, LX/1Ef;->b:Z

    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    .line 220749
    invoke-virtual {p0}, LX/1Eg;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220750
    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    move v2, v1

    .line 220751
    goto :goto_0

    :cond_3
    move v0, v1

    .line 220752
    goto :goto_1

    .line 220753
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/1EZ;)V
    .locals 1

    .prologue
    .line 220728
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/1Ef;->c:LX/1EZ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220729
    monitor-exit p0

    return-void

    .line 220730
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 220731
    const-class v0, Lcom/facebook/photos/upload/service/UploadServiceQueue;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized f()J
    .locals 2

    .prologue
    .line 220705
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/1Ef;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final h()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220706
    sget-object v0, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    sget-object v1, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 220707
    iget-object v1, p0, LX/1Ef;->f:LX/0Uh;

    const/16 v2, 0x39c

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 220708
    if-eqz v1, :cond_1

    .line 220709
    monitor-enter p0

    .line 220710
    :try_start_0
    iget-boolean v1, p0, LX/1Ef;->b:Z

    if-eqz v1, :cond_0

    iget-wide v2, p0, LX/1Ef;->a:J

    iget-object v1, p0, LX/1Ef;->e:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    monitor-exit p0

    .line 220711
    :goto_0
    return v0

    .line 220712
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 220713
    :cond_1
    iget-boolean v0, p0, LX/1Ef;->b:Z

    goto :goto_0
.end method

.method public final declared-synchronized j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220714
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Ef;->c:LX/1EZ;

    if-eqz v0, :cond_0

    .line 220715
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, LX/1Ef;->a(J)V

    .line 220716
    iget-object v1, p0, LX/1Ef;->c:LX/1EZ;

    .line 220717
    iget-object v0, p0, LX/1Ef;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/facebook/photos/upload/retry/FailedUploadRetryTask$1;

    invoke-direct {v2, p0, v1}, Lcom/facebook/photos/upload/retry/FailedUploadRetryTask$1;-><init>(LX/1Ef;LX/1EZ;)V

    const v1, 0x3b53b38

    invoke-static {v0, v2, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220718
    :cond_0
    const/4 v0, 0x0

    monitor-exit p0

    return-object v0

    .line 220719
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
