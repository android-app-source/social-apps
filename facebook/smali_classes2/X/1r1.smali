.class public LX/1r1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/1r1;


# instance fields
.field private final a:Landroid/os/PowerManager;

.field public final b:LX/0So;

.field public final c:Ljava/util/concurrent/ScheduledExecutorService;

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1ql;",
            ">;"
        }
    .end annotation
.end field

.field private e:J

.field private f:I


# direct methods
.method public constructor <init>(Landroid/os/PowerManager;LX/0So;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 331639
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331640
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1r1;->d:Ljava/util/Map;

    .line 331641
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/1r1;->e:J

    .line 331642
    const/4 v0, 0x0

    iput v0, p0, LX/1r1;->f:I

    .line 331643
    iput-object p1, p0, LX/1r1;->a:Landroid/os/PowerManager;

    .line 331644
    iput-object p2, p0, LX/1r1;->b:LX/0So;

    .line 331645
    iput-object p3, p0, LX/1r1;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 331646
    return-void
.end method

.method public static a(LX/0QB;)LX/1r1;
    .locals 6

    .prologue
    .line 331647
    sget-object v0, LX/1r1;->g:LX/1r1;

    if-nez v0, :cond_1

    .line 331648
    const-class v1, LX/1r1;

    monitor-enter v1

    .line 331649
    :try_start_0
    sget-object v0, LX/1r1;->g:LX/1r1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 331650
    if-eqz v2, :cond_0

    .line 331651
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 331652
    new-instance p0, LX/1r1;

    invoke-static {v0}, LX/0kZ;->b(LX/0QB;)Landroid/os/PowerManager;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {p0, v3, v4, v5}, LX/1r1;-><init>(Landroid/os/PowerManager;LX/0So;Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 331653
    move-object v0, p0

    .line 331654
    sput-object v0, LX/1r1;->g:LX/1r1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331655
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 331656
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 331657
    :cond_1
    sget-object v0, LX/1r1;->g:LX/1r1;

    return-object v0

    .line 331658
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 331659
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(ILjava/lang/String;)LX/1ql;
    .locals 3

    .prologue
    .line 331660
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1r1;->a:Landroid/os/PowerManager;

    invoke-virtual {v0, p1, p2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 331661
    new-instance v1, LX/1ql;

    invoke-direct {v1, p0, v0, p1, p2}, LX/1ql;-><init>(LX/1r1;Landroid/os/PowerManager$WakeLock;ILjava/lang/String;)V

    .line 331662
    iget-object v0, p0, LX/1r1;->d:Ljava/util/Map;

    .line 331663
    iget-object v2, v1, LX/1ql;->e:Ljava/lang/String;

    move-object v2, v2

    .line 331664
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331665
    monitor-exit p0

    return-object v1

    .line 331666
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/1ql;)V
    .locals 4

    .prologue
    .line 331667
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1r1;->d:Ljava/util/Map;

    .line 331668
    iget-object v1, p1, LX/1ql;->e:Ljava/lang/String;

    move-object v1, v1

    .line 331669
    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331670
    iget-object v0, p0, LX/1r1;->d:Ljava/util/Map;

    .line 331671
    iget-object v1, p1, LX/1ql;->e:Ljava/lang/String;

    move-object v1, v1

    .line 331672
    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 331673
    invoke-static {p1}, LX/1ql;->k(LX/1ql;)V

    .line 331674
    iget-wide v0, p0, LX/1r1;->e:J

    invoke-virtual {p1}, LX/1ql;->i()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/1r1;->e:J

    .line 331675
    iget v0, p0, LX/1r1;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1r1;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331676
    :cond_0
    monitor-exit p0

    return-void

    .line 331677
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 331678
    iget-wide v0, p0, LX/1r1;->e:J

    return-wide v0
.end method
