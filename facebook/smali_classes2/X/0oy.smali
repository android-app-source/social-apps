.class public LX/0oy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile I:LX/0oy;


# instance fields
.field private volatile A:Lorg/json/JSONObject;

.field public volatile B:Lorg/json/JSONObject;

.field public volatile C:Z

.field private volatile D:Z

.field public volatile E:I

.field private volatile F:J

.field public volatile G:I

.field public volatile H:I

.field public final a:LX/0W3;

.field private final b:LX/0oz;

.field public volatile c:I

.field public volatile d:I

.field public volatile e:I

.field public volatile f:I

.field private volatile g:I

.field public volatile h:I

.field public volatile i:I

.field private volatile j:I

.field private volatile k:I

.field private volatile l:I

.field private volatile m:I

.field private volatile n:I

.field public volatile o:I

.field public volatile p:I

.field public volatile q:I

.field public volatile r:I

.field public volatile s:I

.field public volatile t:Z

.field public volatile u:Lorg/json/JSONObject;

.field private volatile v:Z

.field private volatile w:Lorg/json/JSONObject;

.field private volatile x:Z

.field private volatile y:Lorg/json/JSONObject;

.field private volatile z:Z


# direct methods
.method public constructor <init>(LX/0W3;LX/0oz;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v2, -0x1

    .line 143409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143410
    iput v2, p0, LX/0oy;->c:I

    .line 143411
    iput v2, p0, LX/0oy;->d:I

    .line 143412
    iput v2, p0, LX/0oy;->e:I

    .line 143413
    iput v2, p0, LX/0oy;->f:I

    .line 143414
    iput v2, p0, LX/0oy;->g:I

    .line 143415
    iput v2, p0, LX/0oy;->h:I

    .line 143416
    iput v2, p0, LX/0oy;->i:I

    .line 143417
    iput v2, p0, LX/0oy;->j:I

    .line 143418
    iput v2, p0, LX/0oy;->k:I

    .line 143419
    iput v2, p0, LX/0oy;->l:I

    .line 143420
    iput v2, p0, LX/0oy;->m:I

    .line 143421
    iput v2, p0, LX/0oy;->n:I

    .line 143422
    iput v2, p0, LX/0oy;->o:I

    .line 143423
    iput v2, p0, LX/0oy;->p:I

    .line 143424
    iput v2, p0, LX/0oy;->q:I

    .line 143425
    iput v2, p0, LX/0oy;->r:I

    .line 143426
    iput v2, p0, LX/0oy;->s:I

    .line 143427
    iput-boolean v0, p0, LX/0oy;->t:Z

    .line 143428
    iput-object v1, p0, LX/0oy;->u:Lorg/json/JSONObject;

    .line 143429
    iput-boolean v0, p0, LX/0oy;->v:Z

    .line 143430
    iput-object v1, p0, LX/0oy;->w:Lorg/json/JSONObject;

    .line 143431
    iput-boolean v0, p0, LX/0oy;->x:Z

    .line 143432
    iput-object v1, p0, LX/0oy;->y:Lorg/json/JSONObject;

    .line 143433
    iput-boolean v0, p0, LX/0oy;->z:Z

    .line 143434
    iput-boolean v0, p0, LX/0oy;->C:Z

    .line 143435
    iput-boolean v0, p0, LX/0oy;->D:Z

    .line 143436
    iput v2, p0, LX/0oy;->E:I

    .line 143437
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/0oy;->F:J

    .line 143438
    iput v2, p0, LX/0oy;->G:I

    .line 143439
    iput v2, p0, LX/0oy;->H:I

    .line 143440
    iput-object p1, p0, LX/0oy;->a:LX/0W3;

    .line 143441
    iput-object p2, p0, LX/0oy;->b:LX/0oz;

    .line 143442
    return-void
.end method

.method public static a(LX/0oy;Lorg/json/JSONObject;)I
    .locals 1

    .prologue
    .line 143406
    iget-object v0, p0, LX/0oy;->b:LX/0oz;

    invoke-virtual {v0}, LX/0oz;->c()LX/0p3;

    move-result-object v0

    .line 143407
    invoke-virtual {v0}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 143408
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static a(LX/0QB;)LX/0oy;
    .locals 5

    .prologue
    .line 143393
    sget-object v0, LX/0oy;->I:LX/0oy;

    if-nez v0, :cond_1

    .line 143394
    const-class v1, LX/0oy;

    monitor-enter v1

    .line 143395
    :try_start_0
    sget-object v0, LX/0oy;->I:LX/0oy;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 143396
    if-eqz v2, :cond_0

    .line 143397
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 143398
    new-instance p0, LX/0oy;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v4

    check-cast v4, LX/0oz;

    invoke-direct {p0, v3, v4}, LX/0oy;-><init>(LX/0W3;LX/0oz;)V

    .line 143399
    move-object v0, p0

    .line 143400
    sput-object v0, LX/0oy;->I:LX/0oy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 143401
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 143402
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 143403
    :cond_1
    sget-object v0, LX/0oy;->I:LX/0oy;

    return-object v0

    .line 143404
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 143405
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0oy;J)Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 143391
    iget-object v0, p0, LX/0oy;->a:LX/0W3;

    const-string v1, ""

    invoke-interface {v0, p1, p2, v1}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 143392
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method public static v(LX/0oy;)I
    .locals 4

    .prologue
    .line 143388
    iget v0, p0, LX/0oy;->g:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 143389
    iget-object v0, p0, LX/0oy;->a:LX/0W3;

    sget-wide v2, LX/0X5;->gr:J

    const/16 v1, 0xa

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/0oy;->g:I

    .line 143390
    :cond_0
    iget v0, p0, LX/0oy;->g:I

    return v0
.end method

.method private w()I
    .locals 4

    .prologue
    .line 143385
    iget v0, p0, LX/0oy;->k:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 143386
    iget-object v0, p0, LX/0oy;->a:LX/0W3;

    sget-wide v2, LX/0X5;->gv:J

    const/4 v1, 0x1

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/0oy;->k:I

    .line 143387
    :cond_0
    iget v0, p0, LX/0oy;->k:I

    return v0
.end method

.method private x()I
    .locals 4

    .prologue
    .line 143443
    iget v0, p0, LX/0oy;->l:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 143444
    iget-object v0, p0, LX/0oy;->a:LX/0W3;

    sget-wide v2, LX/0X5;->gw:J

    const/16 v1, 0xa

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/0oy;->l:I

    .line 143445
    :cond_0
    iget v0, p0, LX/0oy;->l:I

    return v0
.end method


# virtual methods
.method public final h()I
    .locals 4

    .prologue
    .line 143382
    iget v0, p0, LX/0oy;->j:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 143383
    iget-object v0, p0, LX/0oy;->a:LX/0W3;

    sget-wide v2, LX/0X5;->gu:J

    const v1, 0x83d60

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/0oy;->j:I

    .line 143384
    :cond_0
    iget v0, p0, LX/0oy;->j:I

    return v0
.end method

.method public final i()I
    .locals 4

    .prologue
    .line 143379
    iget v0, p0, LX/0oy;->m:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 143380
    iget-object v0, p0, LX/0oy;->a:LX/0W3;

    sget-wide v2, LX/0X5;->gC:J

    const/16 v1, 0x48

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/0oy;->m:I

    .line 143381
    :cond_0
    iget v0, p0, LX/0oy;->m:I

    return v0
.end method

.method public final n()I
    .locals 2

    .prologue
    .line 143371
    iget-boolean v0, p0, LX/0oy;->v:Z

    if-eqz v0, :cond_1

    .line 143372
    :try_start_0
    iget-object v0, p0, LX/0oy;->w:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    .line 143373
    sget-wide v0, LX/0X5;->gy:J

    invoke-static {p0, v0, v1}, LX/0oy;->a(LX/0oy;J)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, p0, LX/0oy;->w:Lorg/json/JSONObject;

    .line 143374
    :cond_0
    iget-object v0, p0, LX/0oy;->w:Lorg/json/JSONObject;

    invoke-static {p0, v0}, LX/0oy;->a(LX/0oy;Lorg/json/JSONObject;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 143375
    :goto_0
    return v0

    .line 143376
    :catch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0oy;->v:Z

    .line 143377
    invoke-direct {p0}, LX/0oy;->w()I

    move-result v0

    goto :goto_0

    .line 143378
    :cond_1
    invoke-direct {p0}, LX/0oy;->w()I

    move-result v0

    goto :goto_0
.end method

.method public final o()I
    .locals 2

    .prologue
    .line 143363
    iget-boolean v0, p0, LX/0oy;->x:Z

    if-eqz v0, :cond_1

    .line 143364
    :try_start_0
    iget-object v0, p0, LX/0oy;->y:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    .line 143365
    sget-wide v0, LX/0X5;->gz:J

    invoke-static {p0, v0, v1}, LX/0oy;->a(LX/0oy;J)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, p0, LX/0oy;->y:Lorg/json/JSONObject;

    .line 143366
    :cond_0
    iget-object v0, p0, LX/0oy;->y:Lorg/json/JSONObject;

    invoke-static {p0, v0}, LX/0oy;->a(LX/0oy;Lorg/json/JSONObject;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 143367
    :goto_0
    return v0

    .line 143368
    :catch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0oy;->x:Z

    .line 143369
    invoke-direct {p0}, LX/0oy;->x()I

    move-result v0

    goto :goto_0

    .line 143370
    :cond_1
    invoke-direct {p0}, LX/0oy;->x()I

    move-result v0

    goto :goto_0
.end method

.method public final p()I
    .locals 2

    .prologue
    .line 143355
    iget-boolean v0, p0, LX/0oy;->z:Z

    if-eqz v0, :cond_1

    .line 143356
    :try_start_0
    iget-object v0, p0, LX/0oy;->A:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    .line 143357
    sget-wide v0, LX/0X5;->gA:J

    invoke-static {p0, v0, v1}, LX/0oy;->a(LX/0oy;J)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, p0, LX/0oy;->A:Lorg/json/JSONObject;

    .line 143358
    :cond_0
    iget-object v0, p0, LX/0oy;->A:Lorg/json/JSONObject;

    invoke-static {p0, v0}, LX/0oy;->a(LX/0oy;Lorg/json/JSONObject;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 143359
    :goto_0
    return v0

    .line 143360
    :catch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0oy;->z:Z

    .line 143361
    invoke-virtual {p0}, LX/0oy;->n()I

    move-result v0

    goto :goto_0

    .line 143362
    :cond_1
    invoke-virtual {p0}, LX/0oy;->n()I

    move-result v0

    goto :goto_0
.end method

.method public final s()Z
    .locals 4

    .prologue
    .line 143354
    iget-object v0, p0, LX/0oy;->a:LX/0W3;

    sget-wide v2, LX/0X5;->gE:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method

.method public final u()J
    .locals 6

    .prologue
    .line 143351
    iget-wide v0, p0, LX/0oy;->F:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 143352
    iget-object v0, p0, LX/0oy;->a:LX/0W3;

    sget-wide v2, LX/0X5;->gK:J

    const-wide/16 v4, 0x4e20

    invoke-interface {v0, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/0oy;->F:J

    .line 143353
    :cond_0
    iget-wide v0, p0, LX/0oy;->F:J

    return-wide v0
.end method
