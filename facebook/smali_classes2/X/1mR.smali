.class public LX/1mR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/1mR;


# instance fields
.field public final a:LX/0si;

.field private final b:LX/0TD;

.field public final c:Ljava/lang/Object;

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mDeleteByTagsLock"
    .end annotation
.end field

.field public e:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mDeleteByTagsLock"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0si;LX/0TD;)V
    .locals 1
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 313791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313792
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/1mR;->c:Ljava/lang/Object;

    .line 313793
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1mR;->d:Ljava/util/Set;

    .line 313794
    const/4 v0, 0x0

    iput-object v0, p0, LX/1mR;->e:Lcom/google/common/util/concurrent/SettableFuture;

    .line 313795
    iput-object p1, p0, LX/1mR;->a:LX/0si;

    .line 313796
    iput-object p2, p0, LX/1mR;->b:LX/0TD;

    .line 313797
    return-void
.end method

.method public static a(LX/0QB;)LX/1mR;
    .locals 5

    .prologue
    .line 313798
    sget-object v0, LX/1mR;->f:LX/1mR;

    if-nez v0, :cond_1

    .line 313799
    const-class v1, LX/1mR;

    monitor-enter v1

    .line 313800
    :try_start_0
    sget-object v0, LX/1mR;->f:LX/1mR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 313801
    if-eqz v2, :cond_0

    .line 313802
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 313803
    new-instance p0, LX/1mR;

    invoke-static {v0}, LX/0sh;->a(LX/0QB;)LX/0sh;

    move-result-object v3

    check-cast v3, LX/0si;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-direct {p0, v3, v4}, LX/1mR;-><init>(LX/0si;LX/0TD;)V

    .line 313804
    move-object v0, p0

    .line 313805
    sput-object v0, LX/1mR;->f:LX/1mR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 313806
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 313807
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 313808
    :cond_1
    sget-object v0, LX/1mR;->f:LX/1mR;

    return-object v0

    .line 313809
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 313810
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zO;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 313811
    iget-object v0, p0, LX/1mR;->b:LX/0TD;

    new-instance v1, Lcom/facebook/graphql/executor/cache/GraphQLCacheManager$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/graphql/executor/cache/GraphQLCacheManager$1;-><init>(LX/1mR;LX/0zO;)V

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0TD;->a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 313812
    const/4 v0, 0x0

    .line 313813
    iget-object v1, p0, LX/1mR;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 313814
    :try_start_0
    iget-object v2, p0, LX/1mR;->e:Lcom/google/common/util/concurrent/SettableFuture;

    if-nez v2, :cond_0

    .line 313815
    const/4 v0, 0x1

    .line 313816
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v2

    iput-object v2, p0, LX/1mR;->e:Lcom/google/common/util/concurrent/SettableFuture;

    .line 313817
    :cond_0
    iget-object v2, p0, LX/1mR;->e:Lcom/google/common/util/concurrent/SettableFuture;

    .line 313818
    iget-object v3, p0, LX/1mR;->d:Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 313819
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313820
    if-eqz v0, :cond_1

    .line 313821
    iget-object v0, p0, LX/1mR;->b:LX/0TD;

    new-instance v1, Lcom/facebook/graphql/executor/cache/GraphQLCacheManager$CacheDeleteRunner;

    invoke-direct {v1, p0}, Lcom/facebook/graphql/executor/cache/GraphQLCacheManager$CacheDeleteRunner;-><init>(LX/1mR;)V

    const v3, 0x24d20b92

    invoke-static {v0, v1, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 313822
    :cond_1
    return-object v2

    .line 313823
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
