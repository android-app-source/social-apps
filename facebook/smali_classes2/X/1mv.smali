.class public final enum LX/1mv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1mv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1mv;

.field public static final enum DRAWABLE:LX/1mv;

.field public static final enum NONE:LX/1mv;

.field public static final enum VIEW:LX/1mv;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 314267
    new-instance v0, LX/1mv;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/1mv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1mv;->NONE:LX/1mv;

    .line 314268
    new-instance v0, LX/1mv;

    const-string v1, "DRAWABLE"

    invoke-direct {v0, v1, v3}, LX/1mv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1mv;->DRAWABLE:LX/1mv;

    .line 314269
    new-instance v0, LX/1mv;

    const-string v1, "VIEW"

    invoke-direct {v0, v1, v4}, LX/1mv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1mv;->VIEW:LX/1mv;

    .line 314270
    const/4 v0, 0x3

    new-array v0, v0, [LX/1mv;

    sget-object v1, LX/1mv;->NONE:LX/1mv;

    aput-object v1, v0, v2

    sget-object v1, LX/1mv;->DRAWABLE:LX/1mv;

    aput-object v1, v0, v3

    sget-object v1, LX/1mv;->VIEW:LX/1mv;

    aput-object v1, v0, v4

    sput-object v0, LX/1mv;->$VALUES:[LX/1mv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 314271
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1mv;
    .locals 1

    .prologue
    .line 314272
    const-class v0, LX/1mv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1mv;

    return-object v0
.end method

.method public static values()[LX/1mv;
    .locals 1

    .prologue
    .line 314273
    sget-object v0, LX/1mv;->$VALUES:[LX/1mv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1mv;

    return-object v0
.end method
