.class public LX/0UG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0UH;

.field public final b:LX/0UJ;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64378
    new-instance v0, LX/0UH;

    invoke-direct {v0}, LX/0UH;-><init>()V

    iput-object v0, p0, LX/0UG;->a:LX/0UH;

    .line 64379
    new-instance v0, LX/0UJ;

    invoke-direct {v0}, LX/0UJ;-><init>()V

    iput-object v0, p0, LX/0UG;->b:LX/0UJ;

    return-void
.end method


# virtual methods
.method public final a(LX/0Tn;LX/0dN;)V
    .locals 1

    .prologue
    .line 64380
    iget-object v0, p0, LX/0UG;->a:LX/0UH;

    invoke-virtual {v0, p1, p2}, LX/0UI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 64381
    return-void
.end method

.method public final declared-synchronized a(Ljava/util/Set;LX/0dN;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;",
            "LX/0dN;",
            ")V"
        }
    .end annotation

    .prologue
    .line 64382
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 64383
    invoke-virtual {p0, v0, p2}, LX/0UG;->a(LX/0Tn;LX/0dN;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 64384
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 64385
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final b(LX/0Tn;LX/0dN;)V
    .locals 1

    .prologue
    .line 64386
    iget-object v0, p0, LX/0UG;->a:LX/0UH;

    invoke-virtual {v0, p1, p2}, LX/0UI;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 64387
    return-void
.end method
