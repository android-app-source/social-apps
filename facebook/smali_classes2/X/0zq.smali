.class public abstract LX/0zq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0zr;


# instance fields
.field public final a:LX/0zt;


# direct methods
.method public constructor <init>(LX/0zt;)V
    .locals 0

    .prologue
    .line 168046
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168047
    iput-object p1, p0, LX/0zq;->a:LX/0zt;

    .line 168048
    return-void
.end method


# virtual methods
.method public abstract a()Z
.end method

.method public final a(Ljava/lang/CharSequence;II)Z
    .locals 1

    .prologue
    .line 168049
    if-eqz p1, :cond_0

    if-ltz p2, :cond_0

    if-ltz p3, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    sub-int/2addr v0, p3

    if-ge v0, p2, :cond_1

    .line 168050
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 168051
    :cond_1
    iget-object v0, p0, LX/0zq;->a:LX/0zt;

    if-nez v0, :cond_2

    .line 168052
    invoke-virtual {p0}, LX/0zq;->a()Z

    move-result v0

    .line 168053
    :goto_0
    return v0

    .line 168054
    :cond_2
    iget-object v0, p0, LX/0zq;->a:LX/0zt;

    invoke-interface {v0, p1, p2, p3}, LX/0zt;->a(Ljava/lang/CharSequence;II)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 168055
    invoke-virtual {p0}, LX/0zq;->a()Z

    move-result v0

    :goto_1
    move v0, v0

    .line 168056
    goto :goto_0

    .line 168057
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_1

    .line 168058
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
