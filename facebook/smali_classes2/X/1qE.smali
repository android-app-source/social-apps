.class public LX/1qE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

.field public mCanRunBeforeAppInit:Z

.field public final mId:Ljava/lang/String;

.field private final mParams:Landroid/os/Bundle;

.field public final mType:Ljava/lang/String;

.field private final mUseExceptionResult:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;ZLcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 330419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330420
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1qE;->mCanRunBeforeAppInit:Z

    .line 330421
    iput-object p1, p0, LX/1qE;->mId:Ljava/lang/String;

    .line 330422
    iput-object p2, p0, LX/1qE;->mType:Ljava/lang/String;

    .line 330423
    iput-object p3, p0, LX/1qE;->mParams:Landroid/os/Bundle;

    .line 330424
    iput-boolean p4, p0, LX/1qE;->mUseExceptionResult:Z

    .line 330425
    iput-object p5, p0, LX/1qE;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    .line 330426
    return-void
.end method


# virtual methods
.method public getCallerContext()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 330418
    iget-object v0, p0, LX/1qE;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public getCanRunBeforeAppInit()Z
    .locals 1

    .prologue
    .line 330413
    iget-boolean v0, p0, LX/1qE;->mCanRunBeforeAppInit:Z

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 330417
    iget-object v0, p0, LX/1qE;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getParams()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 330416
    iget-object v0, p0, LX/1qE;->mParams:Landroid/os/Bundle;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 330415
    iget-object v0, p0, LX/1qE;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public getUseExceptionResult()Z
    .locals 1

    .prologue
    .line 330414
    iget-boolean v0, p0, LX/1qE;->mUseExceptionResult:Z

    return v0
.end method
