.class public LX/0if;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile k:LX/0if;


# instance fields
.field private final b:LX/0Zb;

.field private final c:LX/11B;

.field public final d:LX/0SG;

.field public final e:Ljava/util/Random;

.field public final f:LX/11C;

.field public final g:LX/11Y;

.field public final h:LX/11D;

.field public i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1Zr;",
            ">;"
        }
    .end annotation
.end field

.field private volatile j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121658
    const-class v0, LX/0if;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0if;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/11B;LX/0SG;Ljava/util/Random;LX/0Xl;LX/11C;LX/0Zr;LX/11D;)V
    .locals 2
    .param p4    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .param p5    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 121659
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121660
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0if;->i:Ljava/util/Map;

    .line 121661
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0if;->j:Z

    .line 121662
    iput-object p1, p0, LX/0if;->b:LX/0Zb;

    .line 121663
    iput-object p2, p0, LX/0if;->c:LX/11B;

    .line 121664
    iput-object p3, p0, LX/0if;->d:LX/0SG;

    .line 121665
    iput-object p4, p0, LX/0if;->e:Ljava/util/Random;

    .line 121666
    iput-object p6, p0, LX/0if;->f:LX/11C;

    .line 121667
    iput-object p8, p0, LX/0if;->h:LX/11D;

    .line 121668
    const-string v0, "funnellogger-worker"

    invoke-virtual {p7, v0}, LX/0Zr;->a(Ljava/lang/String;)Landroid/os/HandlerThread;

    move-result-object v0

    .line 121669
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 121670
    new-instance v1, LX/11Y;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, p0, v0}, LX/11Y;-><init>(LX/0if;Landroid/os/Looper;)V

    iput-object v1, p0, LX/0if;->g:LX/11Y;

    .line 121671
    invoke-interface {p5}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance p1, LX/11Z;

    invoke-direct {p1, p0}, LX/11Z;-><init>(LX/0if;)V

    invoke-interface {v0, v1, p1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 121672
    return-void
.end method

.method public static a(LX/0QB;)LX/0if;
    .locals 15

    .prologue
    .line 121551
    sget-object v0, LX/0if;->k:LX/0if;

    if-nez v0, :cond_1

    .line 121552
    const-class v1, LX/0if;

    monitor-enter v1

    .line 121553
    :try_start_0
    sget-object v0, LX/0if;->k:LX/0if;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 121554
    if-eqz v2, :cond_0

    .line 121555
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 121556
    new-instance v3, LX/0if;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/11B;->a(LX/0QB;)LX/11B;

    move-result-object v5

    check-cast v5, LX/11B;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v7

    check-cast v7, Ljava/util/Random;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v8

    check-cast v8, LX/0Xl;

    invoke-static {v0}, LX/11C;->a(LX/0QB;)LX/11C;

    move-result-object v9

    check-cast v9, LX/11C;

    invoke-static {v0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v10

    check-cast v10, LX/0Zr;

    .line 121557
    new-instance v14, LX/11D;

    const-class v11, Landroid/content/Context;

    invoke-interface {v0, v11}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/Context;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v12

    check-cast v12, LX/0Zb;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v14, v11, v12, v13}, LX/11D;-><init>(Landroid/content/Context;LX/0Zb;Ljava/util/concurrent/ExecutorService;)V

    .line 121558
    move-object v11, v14

    .line 121559
    check-cast v11, LX/11D;

    invoke-direct/range {v3 .. v11}, LX/0if;-><init>(LX/0Zb;LX/11B;LX/0SG;Ljava/util/Random;LX/0Xl;LX/11C;LX/0Zr;LX/11D;)V

    .line 121560
    move-object v0, v3

    .line 121561
    sput-object v0, LX/0if;->k:LX/0if;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121562
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 121563
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 121564
    :cond_1
    sget-object v0, LX/0if;->k:LX/0if;

    return-object v0

    .line 121565
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 121566
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0if;LX/0ih;JJ)LX/1Zr;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 121673
    iget-object v0, p0, LX/0if;->c:LX/11B;

    const v1, 0x7fffffff

    .line 121674
    iget-object v2, v0, LX/11B;->a:LX/0XZ;

    const-string v3, "funnel_analytics"

    .line 121675
    iget-object v4, p1, LX/0ih;->a:Ljava/lang/String;

    move-object v4, v4

    .line 121676
    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/0XZ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 121677
    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 121678
    sget-object v2, LX/2Wm;->a:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    sget-object v2, LX/2Wm;->a:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_0
    move v2, v2

    .line 121679
    :cond_0
    if-gtz v2, :cond_4

    .line 121680
    :cond_1
    :goto_1
    move v4, v1

    .line 121681
    const v0, 0x7fffffff

    if-ne v4, v0, :cond_3

    .line 121682
    iget-boolean v0, p1, LX/0ih;->e:Z

    move v0, v0

    .line 121683
    if-eqz v0, :cond_2

    .line 121684
    invoke-static {p1, p4, p5}, LX/1Zr;->a(LX/0ih;J)LX/1Zr;

    move-result-object v0

    .line 121685
    :goto_2
    return-object v0

    .line 121686
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 121687
    :cond_3
    new-instance v0, LX/1Zr;

    move-object v1, p1

    move-wide v2, p2

    move-wide v5, p4

    invoke-direct/range {v0 .. v6}, LX/1Zr;-><init>(LX/0ih;JIJ)V

    goto :goto_2

    .line 121688
    :cond_4
    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    move v1, v2

    .line 121689
    goto :goto_1

    .line 121690
    :cond_5
    iget-object v3, v0, LX/11B;->b:Ljava/util/Random;

    invoke-virtual {v3, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    .line 121691
    if-nez v3, :cond_1

    move v1, v2

    goto :goto_1

    :cond_6
    const/16 v2, 0x64

    goto :goto_0
.end method

.method public static a(LX/15o;)Ljava/lang/String;
    .locals 5
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 121692
    iget-object v0, p0, LX/15o;->b:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 121693
    iget-object v0, p0, LX/15o;->a:LX/0ih;

    .line 121694
    iget-object v1, v0, LX/0ih;->a:Ljava/lang/String;

    move-object v1, v1

    .line 121695
    move-object v0, v1

    .line 121696
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/15o;->a:LX/0ih;

    iget-object v1, p0, LX/15o;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 121697
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 121698
    iget-object v4, v0, LX/0ih;->a:Ljava/lang/String;

    move-object v4, v4

    .line 121699
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ":"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 121700
    goto :goto_0
.end method

.method public static a(LX/0if;LX/1Zr;LX/2o8;J)V
    .locals 7

    .prologue
    .line 121701
    iget-boolean v0, p1, LX/1Zr;->e:Z

    move v0, v0

    .line 121702
    if-eqz v0, :cond_0

    .line 121703
    invoke-virtual {p1}, LX/1Zr;->d()Ljava/lang/String;

    invoke-virtual {p2}, LX/2o8;->tag()Ljava/lang/String;

    .line 121704
    :goto_0
    return-void

    .line 121705
    :cond_0
    new-instance v0, LX/1Zt;

    const-string v1, "funnel_end"

    .line 121706
    iget-wide v5, p1, LX/1Zr;->d:J

    move-wide v2, v5

    .line 121707
    sub-long v2, p3, v2

    long-to-int v2, v2

    invoke-virtual {p2}, LX/2o8;->tag()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/1Zt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {p1, v0, p3, p4}, LX/1Zr;->a(LX/1Zt;J)V

    .line 121708
    iget-object v0, p0, LX/0if;->h:LX/11D;

    sget-object v1, LX/1Zs;->FUNNEL_ENDED:LX/1Zs;

    invoke-virtual {v0, v1, p1}, LX/11D;->a(LX/1Zs;LX/1Zr;)V

    .line 121709
    iget-object v0, p0, LX/0if;->b:LX/0Zb;

    invoke-virtual {p1}, LX/1Zr;->n()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 121710
    invoke-virtual {p1}, LX/1Zr;->d()Ljava/lang/String;

    invoke-virtual {p2}, LX/2o8;->tag()Ljava/lang/String;

    goto :goto_0
.end method

.method public static synthetic a(LX/0if;Ljava/lang/String;LX/0ih;JJ)V
    .locals 8

    .prologue
    .line 121711
    invoke-static {p0}, LX/0if;->b$redex0(LX/0if;)V

    .line 121712
    iget-object v1, p0, LX/0if;->i:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Zr;

    .line 121713
    if-eqz v1, :cond_0

    .line 121714
    sget-object v2, LX/2o8;->RESTART:LX/2o8;

    invoke-static {p0, v1, v2, p5, p6}, LX/0if;->a(LX/0if;LX/1Zr;LX/2o8;J)V

    .line 121715
    iget-object v1, p0, LX/0if;->i:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    move-object v1, p0

    move-object v2, p2

    move-wide v3, p3

    move-wide v5, p5

    .line 121716
    invoke-static/range {v1 .. v6}, LX/0if;->a(LX/0if;LX/0ih;JJ)LX/1Zr;

    move-result-object v1

    .line 121717
    if-eqz v1, :cond_1

    .line 121718
    iget-object v2, p0, LX/0if;->h:LX/11D;

    sget-object v3, LX/1Zs;->FUNNEL_STARTED:LX/1Zs;

    invoke-virtual {v2, v3, v1}, LX/11D;->a(LX/1Zs;LX/1Zr;)V

    .line 121719
    iget-object v2, p0, LX/0if;->i:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121720
    :cond_1
    return-void
.end method

.method public static synthetic a(LX/0if;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1rQ;J)V
    .locals 8

    .prologue
    .line 121721
    invoke-static {p0}, LX/0if;->b$redex0(LX/0if;)V

    .line 121722
    iget-object v1, p0, LX/0if;->i:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Zr;

    .line 121723
    if-eqz v1, :cond_1

    .line 121724
    new-instance v2, LX/1Zt;

    .line 121725
    iget-wide v6, v1, LX/1Zr;->d:J

    move-wide v3, v6

    .line 121726
    sub-long v3, p5, v3

    long-to-int v3, v3

    invoke-direct {v2, p2, v3, p3, p4}, LX/1Zt;-><init>(Ljava/lang/String;ILjava/lang/String;LX/1rQ;)V

    invoke-virtual {v1, v2, p5, p6}, LX/1Zr;->a(LX/1Zt;J)V

    .line 121727
    iget-object v2, v1, LX/1Zr;->h:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, v1, LX/1Zr;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/16 v3, 0x64

    if-lt v2, v3, :cond_0

    .line 121728
    iget-boolean v2, v1, LX/1Zr;->e:Z

    move v2, v2

    .line 121729
    if-eqz v2, :cond_2

    :cond_0
    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 121730
    if-nez v2, :cond_1

    .line 121731
    sget-object v2, LX/2o8;->ACTIONS_FULL:LX/2o8;

    iget-object v3, p0, LX/0if;->d:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    invoke-static {p0, v1, v2, v3, v4}, LX/0if;->a(LX/0if;LX/1Zr;LX/2o8;J)V

    .line 121732
    iget-object v1, p0, LX/0if;->i:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 121733
    :cond_1
    return-void

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private a(LX/0ih;JLjava/lang/String;Ljava/lang/String;LX/1rQ;)V
    .locals 4
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/1rQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 121734
    invoke-static {p1}, LX/0if;->e(LX/0ih;)V

    .line 121735
    invoke-static {}, LX/15o;->a()LX/15p;

    move-result-object v0

    .line 121736
    iput-object p1, v0, LX/15p;->a:LX/0ih;

    .line 121737
    move-object v0, v0

    .line 121738
    invoke-virtual {v0, p2, p3}, LX/15p;->a(J)LX/15p;

    move-result-object v0

    .line 121739
    iput-object p4, v0, LX/15p;->d:Ljava/lang/String;

    .line 121740
    move-object v0, v0

    .line 121741
    iput-object p5, v0, LX/15p;->e:Ljava/lang/String;

    .line 121742
    move-object v0, v0

    .line 121743
    iput-object p6, v0, LX/15p;->f:LX/1rQ;

    .line 121744
    move-object v0, v0

    .line 121745
    iget-object v1, p0, LX/0if;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/15p;->b(J)LX/15p;

    move-result-object v0

    invoke-virtual {v0}, LX/15p;->a()LX/15o;

    move-result-object v0

    .line 121746
    iget-object v1, p0, LX/0if;->g:LX/11Y;

    iget-object v2, p0, LX/0if;->g:LX/11Y;

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v0}, LX/11Y;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/11Y;->sendMessage(Landroid/os/Message;)Z

    .line 121747
    return-void
.end method

.method private static b(LX/0if;LX/1Zr;)Z
    .locals 8

    .prologue
    .line 121748
    iget-object v0, p0, LX/0if;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 121749
    iget-wide v6, p1, LX/1Zr;->f:J

    move-wide v2, v6

    .line 121750
    sub-long/2addr v0, v2

    .line 121751
    iget-object v2, p1, LX/1Zr;->a:LX/0ih;

    move-object v2, v2

    .line 121752
    iget v3, v2, LX/0ih;->c:I

    const v4, 0x15180

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    move v2, v3

    .line 121753
    int-to-long v2, v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 121754
    sget-object v0, LX/2o8;->TIMEOUT:LX/2o8;

    iget-object v1, p0, LX/0if;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {p0, p1, v0, v2, v3}, LX/0if;->a(LX/0if;LX/1Zr;LX/2o8;J)V

    .line 121755
    const/4 v0, 0x1

    .line 121756
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b$redex0(LX/0if;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 121757
    iget-boolean v0, p0, LX/0if;->j:Z

    if-eqz v0, :cond_0

    .line 121758
    :goto_0
    return-void

    .line 121759
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/0if;->f:LX/11C;

    invoke-virtual {v0}, LX/11C;->a()Ljava/util/Map;

    move-result-object v0

    .line 121760
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 121761
    iput-object v0, p0, LX/0if;->i:Ljava/util/Map;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121762
    :cond_1
    iput-boolean v4, p0, LX/0if;->j:Z

    goto :goto_0

    .line 121763
    :catch_0
    move-exception v0

    .line 121764
    :try_start_1
    sget-object v1, LX/0if;->a:Ljava/lang/String;

    const-string v2, "Failed to load funnels"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121765
    iput-boolean v4, p0, LX/0if;->j:Z

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-boolean v4, p0, LX/0if;->j:Z

    throw v0
.end method

.method public static b$redex0(LX/0if;Ljava/lang/String;LX/0ih;JJ)V
    .locals 7

    .prologue
    .line 121766
    iget-boolean v0, p2, LX/0ih;->e:Z

    move v0, v0

    .line 121767
    if-nez v0, :cond_0

    .line 121768
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must enable noop funnels in the FunnelDefinition to use startFunnelIfNotStarted()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121769
    :cond_0
    invoke-static {p0}, LX/0if;->b$redex0(LX/0if;)V

    .line 121770
    iget-object v0, p0, LX/0if;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 121771
    :cond_1
    :goto_0
    return-void

    :cond_2
    move-object v0, p0

    move-object v1, p2

    move-wide v2, p3

    move-wide v4, p5

    .line 121772
    invoke-static/range {v0 .. v5}, LX/0if;->a(LX/0if;LX/0ih;JJ)LX/1Zr;

    move-result-object v0

    .line 121773
    if-eqz v0, :cond_1

    .line 121774
    iget-object v1, p0, LX/0if;->h:LX/11D;

    sget-object v2, LX/1Zs;->FUNNEL_STARTED:LX/1Zs;

    invoke-virtual {v1, v2, v0}, LX/11D;->a(LX/1Zs;LX/1Zr;)V

    .line 121775
    iget-object v1, p0, LX/0if;->i:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static d(LX/0if;)V
    .locals 12
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 121641
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 121642
    iget-object v0, p0, LX/0if;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 121643
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Zr;

    .line 121644
    :try_start_0
    iget-object v8, v1, LX/1Zr;->a:LX/0ih;

    move-object v8, v8

    .line 121645
    iget-boolean v9, v8, LX/0ih;->d:Z

    move v8, v9

    .line 121646
    if-eqz v8, :cond_4

    .line 121647
    sget-object v8, LX/2o8;->SESSION_END:LX/2o8;

    iget-object v9, p0, LX/0if;->d:LX/0SG;

    invoke-interface {v9}, LX/0SG;->a()J

    move-result-wide v10

    invoke-static {p0, v1, v8, v10, v11}, LX/0if;->a(LX/0if;LX/1Zr;LX/2o8;J)V

    .line 121648
    const/4 v8, 0x1

    .line 121649
    :goto_1
    move v5, v8

    .line 121650
    if-nez v5, :cond_1

    invoke-static {p0, v1}, LX/0if;->b(LX/0if;LX/1Zr;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 121651
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 121652
    :catch_0
    move-exception v1

    .line 121653
    sget-object v5, LX/0if;->a:Ljava/lang/String;

    const-string v6, "NPE for key: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v7, v2

    invoke-static {v5, v1, v6, v7}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 121654
    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_2
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 121655
    iget-object v2, p0, LX/0if;->i:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 121656
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 121657
    :cond_3
    return-void

    :cond_4
    const/4 v8, 0x0

    goto :goto_1
.end method

.method public static e(LX/0ih;)V
    .locals 2

    .prologue
    .line 121776
    if-nez p0, :cond_0

    .line 121777
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FunnelDefinition is null, expecting non-null value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121778
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0ih;)V
    .locals 4

    .prologue
    .line 121634
    invoke-static {p1}, LX/0if;->e(LX/0ih;)V

    .line 121635
    invoke-static {}, LX/15o;->a()LX/15p;

    move-result-object v0

    .line 121636
    iput-object p1, v0, LX/15p;->a:LX/0ih;

    .line 121637
    move-object v0, v0

    .line 121638
    iget-object v1, p0, LX/0if;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/15p;->b(J)LX/15p;

    move-result-object v0

    invoke-virtual {v0}, LX/15p;->a()LX/15o;

    move-result-object v0

    .line 121639
    iget-object v1, p0, LX/0if;->g:LX/11Y;

    iget-object v2, p0, LX/0if;->g:LX/11Y;

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v0}, LX/11Y;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/11Y;->sendMessage(Landroid/os/Message;)Z

    .line 121640
    return-void
.end method

.method public final a(LX/0ih;J)V
    .locals 4

    .prologue
    .line 121627
    invoke-static {p1}, LX/0if;->e(LX/0ih;)V

    .line 121628
    invoke-static {}, LX/15o;->a()LX/15p;

    move-result-object v0

    .line 121629
    iput-object p1, v0, LX/15p;->a:LX/0ih;

    .line 121630
    move-object v0, v0

    .line 121631
    invoke-virtual {v0, p2, p3}, LX/15p;->a(J)LX/15p;

    move-result-object v0

    iget-object v1, p0, LX/0if;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/15p;->b(J)LX/15p;

    move-result-object v0

    invoke-virtual {v0}, LX/15p;->a()LX/15o;

    move-result-object v0

    .line 121632
    iget-object v1, p0, LX/0if;->g:LX/11Y;

    iget-object v2, p0, LX/0if;->g:LX/11Y;

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v0}, LX/11Y;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/11Y;->sendMessage(Landroid/os/Message;)Z

    .line 121633
    return-void
.end method

.method public final a(LX/0ih;JLjava/lang/String;)V
    .locals 4

    .prologue
    .line 121617
    invoke-static {p1}, LX/0if;->e(LX/0ih;)V

    .line 121618
    invoke-static {}, LX/15o;->a()LX/15p;

    move-result-object v0

    .line 121619
    iput-object p1, v0, LX/15p;->a:LX/0ih;

    .line 121620
    move-object v0, v0

    .line 121621
    invoke-virtual {v0, p2, p3}, LX/15p;->a(J)LX/15p;

    move-result-object v0

    .line 121622
    iput-object p4, v0, LX/15p;->c:Ljava/lang/String;

    .line 121623
    move-object v0, v0

    .line 121624
    iget-object v1, p0, LX/0if;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/15p;->b(J)LX/15p;

    move-result-object v0

    invoke-virtual {v0}, LX/15p;->a()LX/15o;

    move-result-object v0

    .line 121625
    iget-object v1, p0, LX/0if;->g:LX/11Y;

    iget-object v2, p0, LX/0if;->g:LX/11Y;

    const/4 v3, 0x4

    invoke-virtual {v2, v3, v0}, LX/11Y;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/11Y;->sendMessage(Landroid/os/Message;)Z

    .line 121626
    return-void
.end method

.method public final a(LX/0ih;JLjava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 121615
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, LX/0if;->a(LX/0ih;JLjava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 121616
    return-void
.end method

.method public final a(LX/0ih;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 121606
    invoke-static {p1}, LX/0if;->e(LX/0ih;)V

    .line 121607
    invoke-static {}, LX/15o;->a()LX/15p;

    move-result-object v0

    .line 121608
    iput-object p1, v0, LX/15p;->a:LX/0ih;

    .line 121609
    move-object v0, v0

    .line 121610
    iput-object p2, v0, LX/15p;->c:Ljava/lang/String;

    .line 121611
    move-object v0, v0

    .line 121612
    iget-object v1, p0, LX/0if;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/15p;->b(J)LX/15p;

    move-result-object v0

    invoke-virtual {v0}, LX/15p;->a()LX/15o;

    move-result-object v0

    .line 121613
    iget-object v1, p0, LX/0if;->g:LX/11Y;

    iget-object v2, p0, LX/0if;->g:LX/11Y;

    const/4 v3, 0x4

    invoke-virtual {v2, v3, v0}, LX/11Y;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/11Y;->sendMessage(Landroid/os/Message;)Z

    .line 121614
    return-void
.end method

.method public final a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121604
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 121605
    return-void
.end method

.method public final a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/1rQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 121591
    invoke-static {p1}, LX/0if;->e(LX/0ih;)V

    .line 121592
    invoke-static {}, LX/15o;->a()LX/15p;

    move-result-object v0

    .line 121593
    iput-object p1, v0, LX/15p;->a:LX/0ih;

    .line 121594
    move-object v0, v0

    .line 121595
    iput-object p2, v0, LX/15p;->d:Ljava/lang/String;

    .line 121596
    move-object v0, v0

    .line 121597
    iput-object p3, v0, LX/15p;->e:Ljava/lang/String;

    .line 121598
    move-object v0, v0

    .line 121599
    iput-object p4, v0, LX/15p;->f:LX/1rQ;

    .line 121600
    move-object v0, v0

    .line 121601
    iget-object v1, p0, LX/0if;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/15p;->b(J)LX/15p;

    move-result-object v0

    invoke-virtual {v0}, LX/15p;->a()LX/15o;

    move-result-object v0

    .line 121602
    iget-object v1, p0, LX/0if;->g:LX/11Y;

    iget-object v2, p0, LX/0if;->g:LX/11Y;

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v0}, LX/11Y;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/11Y;->sendMessage(Landroid/os/Message;)Z

    .line 121603
    return-void
.end method

.method public final b(LX/0ih;)V
    .locals 4

    .prologue
    .line 121585
    invoke-static {}, LX/15o;->a()LX/15p;

    move-result-object v0

    .line 121586
    iput-object p1, v0, LX/15p;->a:LX/0ih;

    .line 121587
    move-object v0, v0

    .line 121588
    iget-object v1, p0, LX/0if;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/15p;->b(J)LX/15p;

    move-result-object v0

    invoke-virtual {v0}, LX/15p;->a()LX/15o;

    move-result-object v0

    .line 121589
    iget-object v1, p0, LX/0if;->g:LX/11Y;

    iget-object v2, p0, LX/0if;->g:LX/11Y;

    const/4 v3, 0x7

    invoke-virtual {v2, v3, v0}, LX/11Y;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/11Y;->sendMessage(Landroid/os/Message;)Z

    .line 121590
    return-void
.end method

.method public final b(LX/0ih;JLjava/lang/String;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 121583
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, LX/0if;->a(LX/0ih;JLjava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 121584
    return-void
.end method

.method public final b(LX/0ih;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 121581
    invoke-virtual {p0, p1, p2, v0, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 121582
    return-void
.end method

.method public final c(LX/0ih;)V
    .locals 4

    .prologue
    .line 121574
    invoke-static {p1}, LX/0if;->e(LX/0ih;)V

    .line 121575
    invoke-static {}, LX/15o;->a()LX/15p;

    move-result-object v0

    .line 121576
    iput-object p1, v0, LX/15p;->a:LX/0ih;

    .line 121577
    move-object v0, v0

    .line 121578
    iget-object v1, p0, LX/0if;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/15p;->b(J)LX/15p;

    move-result-object v0

    invoke-virtual {v0}, LX/15p;->a()LX/15o;

    move-result-object v0

    .line 121579
    iget-object v1, p0, LX/0if;->g:LX/11Y;

    iget-object v2, p0, LX/0if;->g:LX/11Y;

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v0}, LX/11Y;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/11Y;->sendMessage(Landroid/os/Message;)Z

    .line 121580
    return-void
.end method

.method public final c(LX/0ih;J)V
    .locals 4

    .prologue
    .line 121567
    invoke-static {p1}, LX/0if;->e(LX/0ih;)V

    .line 121568
    invoke-static {}, LX/15o;->a()LX/15p;

    move-result-object v0

    .line 121569
    iput-object p1, v0, LX/15p;->a:LX/0ih;

    .line 121570
    move-object v0, v0

    .line 121571
    invoke-virtual {v0, p2, p3}, LX/15p;->a(J)LX/15p;

    move-result-object v0

    iget-object v1, p0, LX/0if;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/15p;->b(J)LX/15p;

    move-result-object v0

    invoke-virtual {v0}, LX/15p;->a()LX/15o;

    move-result-object v0

    .line 121572
    iget-object v1, p0, LX/0if;->g:LX/11Y;

    iget-object v2, p0, LX/0if;->g:LX/11Y;

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v0}, LX/11Y;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/11Y;->sendMessage(Landroid/os/Message;)Z

    .line 121573
    return-void
.end method
