.class public LX/1n1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1mr;


# instance fields
.field private final a:Landroid/view/RenderNode;


# direct methods
.method public constructor <init>(Landroid/view/RenderNode;)V
    .locals 0

    .prologue
    .line 314342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 314343
    iput-object p1, p0, LX/1n1;->a:Landroid/view/RenderNode;

    .line 314344
    return-void
.end method


# virtual methods
.method public final a(II)Landroid/graphics/Canvas;
    .locals 1

    .prologue
    .line 314345
    iget-object v0, p0, LX/1n1;->a:Landroid/view/RenderNode;

    invoke-virtual {v0, p1, p2}, Landroid/view/RenderNode;->start(II)Landroid/view/HardwareCanvas;

    move-result-object v0

    .line 314346
    check-cast v0, Landroid/graphics/Canvas;

    return-object v0
.end method

.method public final a(IIII)V
    .locals 2

    .prologue
    .line 314347
    iget-object v0, p0, LX/1n1;->a:Landroid/view/RenderNode;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/RenderNode;->setLeftTopRightBottom(IIII)Z

    .line 314348
    iget-object v0, p0, LX/1n1;->a:Landroid/view/RenderNode;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/RenderNode;->setClipToBounds(Z)Z

    .line 314349
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 314350
    iget-object v0, p0, LX/1n1;->a:Landroid/view/RenderNode;

    check-cast p1, Landroid/view/HardwareCanvas;

    invoke-virtual {v0, p1}, Landroid/view/RenderNode;->end(Landroid/view/HardwareCanvas;)V

    .line 314351
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 314352
    iget-object v0, p0, LX/1n1;->a:Landroid/view/RenderNode;

    invoke-virtual {v0}, Landroid/view/RenderNode;->isValid()Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 314353
    instance-of v0, p1, Landroid/view/HardwareCanvas;

    if-nez v0, :cond_0

    .line 314354
    new-instance v0, LX/32E;

    new-instance v1, Ljava/lang/ClassCastException;

    invoke-direct {v1}, Ljava/lang/ClassCastException;-><init>()V

    invoke-direct {v0, v1}, LX/32E;-><init>(Ljava/lang/Exception;)V

    throw v0

    .line 314355
    :cond_0
    check-cast p1, Landroid/view/HardwareCanvas;

    iget-object v0, p0, LX/1n1;->a:Landroid/view/RenderNode;

    invoke-virtual {p1, v0}, Landroid/view/HardwareCanvas;->drawRenderNode(Landroid/view/RenderNode;)V

    .line 314356
    return-void
.end method
