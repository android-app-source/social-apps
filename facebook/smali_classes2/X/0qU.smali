.class public LX/0qU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17P;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/17P;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 147648
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147649
    iput-object p1, p0, LX/0qU;->a:LX/0Ot;

    .line 147650
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;ZLcom/facebook/api/feedtype/FeedType;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 147651
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 147652
    :cond_0
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 147653
    if-eqz v0, :cond_2

    .line 147654
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-nez v0, :cond_1

    .line 147655
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The feedUnit cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 147656
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "If the FeedType has a caching policy, the cursor cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move-object p1, v1

    .line 147657
    :cond_3
    :goto_0
    return-object p1

    .line 147658
    :cond_4
    iget-object v0, p0, LX/0qU;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17P;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    invoke-static {p3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/17P;->a(Lcom/facebook/graphql/model/FeedUnit;LX/0am;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 147659
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    if-eq v0, v2, :cond_3

    .line 147660
    if-nez v0, :cond_5

    move-object p1, v1

    .line 147661
    goto :goto_0

    .line 147662
    :cond_5
    invoke-static {p1}, LX/1u8;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)LX/1u8;

    move-result-object v1

    .line 147663
    iput-object v0, v1, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 147664
    move-object v0, v1

    .line 147665
    invoke-virtual {v0}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object p1

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/0qU;
    .locals 2

    .prologue
    .line 147666
    new-instance v0, LX/0qU;

    const/16 v1, 0x761

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0qU;-><init>(LX/0Ot;)V

    .line 147667
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedResult;Z)Lcom/facebook/api/feed/FetchFeedResult;
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 147668
    if-eqz p1, :cond_2

    .line 147669
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-object v1, v1

    .line 147670
    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 147671
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 147672
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 147673
    iget-object v6, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v6, v6

    .line 147674
    iget-object v7, v6, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v6, v7

    .line 147675
    invoke-direct {p0, v0, p2, v6}, LX/0qU;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;ZLcom/facebook/api/feedtype/FeedType;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v6

    .line 147676
    if-eq v6, v0, :cond_3

    .line 147677
    const/4 v0, 0x1

    .line 147678
    :goto_1
    if-eqz v6, :cond_0

    .line 147679
    invoke-virtual {v3, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 147680
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 147681
    :cond_1
    if-eqz v1, :cond_2

    .line 147682
    new-instance v0, Lcom/facebook/api/feed/FetchFeedResult;

    .line 147683
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v1, v1

    .line 147684
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->c()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->d()Ljava/lang/String;

    move-result-object v4

    .line 147685
    iget-object v5, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v5, v5

    .line 147686
    iget-wide v9, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v6, v9

    .line 147687
    iget-boolean v8, p1, Lcom/facebook/api/feed/FetchFeedResult;->c:Z

    move v8, v8

    .line 147688
    invoke-direct/range {v0 .. v8}, Lcom/facebook/api/feed/FetchFeedResult;-><init>(Lcom/facebook/api/feed/FetchFeedParams;LX/0Px;Lcom/facebook/graphql/model/GraphQLPageInfo;Ljava/lang/String;LX/0ta;JZ)V

    move-object p1, v0

    .line 147689
    :cond_2
    return-object p1

    :cond_3
    move v0, v1

    goto :goto_1
.end method
