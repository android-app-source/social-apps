.class public LX/0WJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile l:LX/0WJ;


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0WK;

.field private final d:LX/03V;

.field private final e:LX/0X1;

.field private final f:Landroid/content/Context;

.field private g:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private h:Lcom/facebook/user/model/User;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private i:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private j:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private k:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75725
    const-class v0, LX/0WJ;

    sput-object v0, LX/0WJ;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0WK;LX/03V;LX/0X1;Landroid/content/Context;)V
    .locals 0
    .param p5    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 75799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75800
    iput-object p1, p0, LX/0WJ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 75801
    iput-object p2, p0, LX/0WJ;->c:LX/0WK;

    .line 75802
    iput-object p3, p0, LX/0WJ;->d:LX/03V;

    .line 75803
    iput-object p4, p0, LX/0WJ;->e:LX/0X1;

    .line 75804
    iput-object p5, p0, LX/0WJ;->f:Landroid/content/Context;

    .line 75805
    return-void
.end method

.method public static a(LX/0QB;)LX/0WJ;
    .locals 11

    .prologue
    .line 75806
    sget-object v0, LX/0WJ;->l:LX/0WJ;

    if-nez v0, :cond_1

    .line 75807
    const-class v1, LX/0WJ;

    monitor-enter v1

    .line 75808
    :try_start_0
    sget-object v0, LX/0WJ;->l:LX/0WJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 75809
    if-eqz v2, :cond_0

    .line 75810
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 75811
    new-instance v3, LX/0WJ;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0WK;->a(LX/0QB;)LX/0WK;

    move-result-object v5

    check-cast v5, LX/0WK;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    .line 75812
    new-instance v8, LX/0X1;

    invoke-static {v0}, LX/0WL;->a(LX/0QB;)LX/0WP;

    move-result-object v7

    check-cast v7, LX/0WP;

    const/16 v9, 0xf9a

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x12d0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct {v8, v7, v9, v10}, LX/0X1;-><init>(LX/0WP;LX/0Ot;LX/0Ot;)V

    .line 75813
    move-object v7, v8

    .line 75814
    check-cast v7, LX/0X1;

    const-class v8, Landroid/content/Context;

    const-class v9, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v8, v9}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-direct/range {v3 .. v8}, LX/0WJ;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0WK;LX/03V;LX/0X1;Landroid/content/Context;)V

    .line 75815
    move-object v0, v3

    .line 75816
    sput-object v0, LX/0WJ;->l:LX/0WJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75817
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 75818
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 75819
    :cond_1
    sget-object v0, LX/0WJ;->l:LX/0WJ;

    return-object v0

    .line 75820
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 75821
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized d(Lcom/facebook/user/model/User;)V
    .locals 3

    .prologue
    .line 75822
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0WJ;->d:LX/03V;

    .line 75823
    iget-object v1, p1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v1

    .line 75824
    invoke-virtual {v0, v1}, LX/03V;->c(Ljava/lang/String;)V

    .line 75825
    iget-object v0, p0, LX/0WJ;->d:LX/03V;

    const-string v1, "partial_user"

    .line 75826
    iget-boolean v2, p1, Lcom/facebook/user/model/User;->F:Z

    move v2, v2

    .line 75827
    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75828
    monitor-exit p0

    return-void

    .line 75829
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized m()V
    .locals 1

    .prologue
    .line 75787
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75788
    monitor-exit p0

    return-void

    .line 75789
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized n()Lcom/facebook/user/model/User;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 75830
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    if-eqz v1, :cond_1

    .line 75831
    iget-object v0, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75832
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 75833
    :cond_1
    :try_start_1
    invoke-virtual {p0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 75834
    if-eqz v1, :cond_0

    .line 75835
    iget-object v2, p0, LX/0WJ;->e:LX/0X1;

    .line 75836
    iget-object v3, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v3

    .line 75837
    const/4 v5, 0x0

    .line 75838
    invoke-static {v2, v3}, LX/0X1;->d(LX/0X1;Ljava/lang/String;)LX/0WS;

    move-result-object v6

    .line 75839
    if-nez v6, :cond_4

    .line 75840
    :cond_2
    :goto_1
    move-object v2, v5

    .line 75841
    iput-object v2, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 75842
    iget-object v2, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    if-eqz v2, :cond_0

    .line 75843
    iget-object v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v2

    .line 75844
    if-eqz v2, :cond_3

    .line 75845
    iget-object v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v2

    .line 75846
    iget-object v3, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 75847
    iget-object v4, v3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v4

    .line 75848
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-boolean v2, p0, LX/0WJ;->i:Z

    if-nez v2, :cond_3

    .line 75849
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "User ID in credential does not match me user. current user ID "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 75850
    iget-object v3, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v3

    .line 75851
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", me user ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 75852
    iget-object v3, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v3

    .line 75853
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 75854
    iget-object v2, p0, LX/0WJ;->d:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 75855
    invoke-static {p0}, LX/0WJ;->o(LX/0WJ;)V

    .line 75856
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/0WJ;->i:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 75857
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 75858
    :cond_3
    :try_start_2
    iget-object v0, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    invoke-direct {p0, v0}, LX/0WJ;->d(Lcom/facebook/user/model/User;)V

    .line 75859
    iget-object v0, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 75860
    :cond_4
    const-string v4, "is_imported"

    const/4 v7, 0x0

    invoke-virtual {v6, v4, v7}, LX/0WS;->a(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 75861
    sget-object v4, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-static {v4, v6}, LX/0XH;->a(LX/0XG;LX/0WS;)Lcom/facebook/user/model/User;

    move-result-object v5

    goto :goto_1

    .line 75862
    :cond_5
    iget-object v4, v2, LX/0X1;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 75863
    invoke-virtual {v6}, LX/0WS;->b()LX/1gW;

    move-result-object v6

    .line 75864
    iget-object v4, v2, LX/0X1;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 75865
    sget-object v7, LX/26p;->u:LX/0Tn;

    invoke-interface {v4, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 75866
    sget-object v7, LX/26p;->u:LX/0Tn;

    invoke-interface {v4, v7, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 75867
    if-eqz v7, :cond_6

    .line 75868
    iget-object v5, v2, LX/0X1;->c:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2P7;

    sget-object v8, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v5, v8, v7}, LX/2P7;->a(LX/0XG;Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v5

    .line 75869
    if-eqz v5, :cond_6

    .line 75870
    invoke-static {v5, v6}, LX/0XH;->a(Lcom/facebook/user/model/User;LX/1gW;)V

    .line 75871
    :cond_6
    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    .line 75872
    sget-object v7, LX/26p;->u:LX/0Tn;

    invoke-interface {v4, v7}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 75873
    invoke-interface {v4}, LX/0hN;->commit()V

    .line 75874
    :cond_7
    const-string v4, "is_imported"

    const/4 v7, 0x1

    invoke-interface {v6, v4, v7}, LX/1gW;->a(Ljava/lang/String;Z)LX/1gW;

    .line 75875
    invoke-interface {v6}, LX/1gW;->b()Z

    goto/16 :goto_1
.end method

.method private static declared-synchronized o(LX/0WJ;)V
    .locals 1

    .prologue
    .line 75876
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/0WJ;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75877
    monitor-exit p0

    return-void

    .line 75878
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized p()V
    .locals 3

    .prologue
    .line 75879
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/0WJ;->o(LX/0WJ;)V

    .line 75880
    const/4 v0, 0x0

    iput-object v0, p0, LX/0WJ;->k:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 75881
    iget-object v0, p0, LX/0WJ;->c:LX/0WK;

    .line 75882
    iget-object v1, v0, LX/0WK;->a:LX/0WS;

    invoke-virtual {v1}, LX/0WS;->b()LX/1gW;

    move-result-object v1

    const-string v2, "uid"

    invoke-interface {v1, v2}, LX/1gW;->a(Ljava/lang/String;)LX/1gW;

    move-result-object v1

    const-string v2, "access_token"

    invoke-interface {v1, v2}, LX/1gW;->a(Ljava/lang/String;)LX/1gW;

    move-result-object v1

    const-string v2, "session_cookies_string"

    invoke-interface {v1, v2}, LX/1gW;->a(Ljava/lang/String;)LX/1gW;

    move-result-object v1

    const-string v2, "secret"

    invoke-interface {v1, v2}, LX/1gW;->a(Ljava/lang/String;)LX/1gW;

    move-result-object v1

    const-string v2, "session_key"

    invoke-interface {v1, v2}, LX/1gW;->a(Ljava/lang/String;)LX/1gW;

    move-result-object v1

    const-string v2, "username"

    invoke-interface {v1, v2}, LX/1gW;->a(Ljava/lang/String;)LX/1gW;

    move-result-object v1

    invoke-interface {v1}, LX/1gW;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75883
    monitor-exit p0

    return-void

    .line 75884
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 12

    .prologue
    .line 75885
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0WJ;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 75886
    const/4 v0, 0x0

    .line 75887
    :goto_0
    monitor-exit p0

    return-object v0

    .line 75888
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0WJ;->k:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-nez v0, :cond_2

    .line 75889
    iget-object v0, p0, LX/0WJ;->c:LX/0WK;

    .line 75890
    iget-object v1, v0, LX/0WK;->a:LX/0WS;

    const-string v2, "is_imported"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0WS;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 75891
    const/4 v1, 0x0

    .line 75892
    iget-object v2, v0, LX/0WK;->a:LX/0WS;

    const-string v3, "uid"

    invoke-virtual {v2, v3, v1}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 75893
    iget-object v3, v0, LX/0WK;->a:LX/0WS;

    const-string v4, "access_token"

    invoke-virtual {v3, v4, v1}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 75894
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 75895
    :cond_1
    :goto_1
    move-object v1, v1

    .line 75896
    :goto_2
    move-object v0, v1

    .line 75897
    iput-object v0, p0, LX/0WJ;->k:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 75898
    :cond_2
    iget-object v0, p0, LX/0WJ;->k:Lcom/facebook/auth/viewercontext/ViewerContext;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 75899
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    const/4 v5, 0x0

    .line 75900
    iget-object v4, v0, LX/0WK;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 75901
    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v6

    if-nez v6, :cond_5

    .line 75902
    :goto_3
    move-object v1, v5

    .line 75903
    goto :goto_2

    :cond_4
    invoke-static {}, Lcom/facebook/auth/viewercontext/ViewerContext;->newBuilder()LX/0SK;

    move-result-object v4

    .line 75904
    iput-object v2, v4, LX/0SK;->a:Ljava/lang/String;

    .line 75905
    move-object v2, v4

    .line 75906
    iput-object v3, v2, LX/0SK;->b:Ljava/lang/String;

    .line 75907
    move-object v2, v2

    .line 75908
    iget-object v3, v0, LX/0WK;->a:LX/0WS;

    const-string v4, "session_cookies_string"

    invoke-virtual {v3, v4, v1}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 75909
    iput-object v3, v2, LX/0SK;->c:Ljava/lang/String;

    .line 75910
    move-object v2, v2

    .line 75911
    iget-object v3, v0, LX/0WK;->a:LX/0WS;

    const-string v4, "secret"

    invoke-virtual {v3, v4, v1}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 75912
    iput-object v3, v2, LX/0SK;->e:Ljava/lang/String;

    .line 75913
    move-object v2, v2

    .line 75914
    iget-object v3, v0, LX/0WK;->a:LX/0WS;

    const-string v4, "session_key"

    invoke-virtual {v3, v4, v1}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 75915
    iput-object v3, v2, LX/0SK;->f:Ljava/lang/String;

    .line 75916
    move-object v2, v2

    .line 75917
    iget-object v3, v0, LX/0WK;->a:LX/0WS;

    const-string v4, "username"

    invoke-virtual {v3, v4, v1}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 75918
    iput-object v1, v2, LX/0SK;->g:Ljava/lang/String;

    .line 75919
    move-object v1, v2

    .line 75920
    invoke-virtual {v1}, LX/0SK;->h()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    goto :goto_1

    .line 75921
    :cond_5
    iget-object v6, v0, LX/0WK;->a:LX/0WS;

    invoke-virtual {v6}, LX/0WS;->b()LX/1gW;

    move-result-object v10

    .line 75922
    sget-object v6, LX/26p;->c:LX/0Tn;

    invoke-interface {v4, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v6

    if-nez v6, :cond_6

    sget-object v6, LX/26p;->d:LX/0Tn;

    invoke-interface {v4, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 75923
    :cond_6
    sget-object v6, LX/26p;->c:LX/0Tn;

    invoke-interface {v4, v6, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 75924
    sget-object v7, LX/26p;->d:LX/0Tn;

    invoke-interface {v4, v7, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 75925
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_8

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_8

    .line 75926
    invoke-static {}, Lcom/facebook/auth/viewercontext/ViewerContext;->newBuilder()LX/0SK;

    move-result-object v8

    .line 75927
    iput-object v6, v8, LX/0SK;->a:Ljava/lang/String;

    .line 75928
    move-object v6, v8

    .line 75929
    iput-object v7, v6, LX/0SK;->b:Ljava/lang/String;

    .line 75930
    move-object v6, v6

    .line 75931
    sget-object v7, LX/26p;->e:LX/0Tn;

    invoke-interface {v4, v7, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 75932
    iput-object v7, v6, LX/0SK;->c:Ljava/lang/String;

    .line 75933
    move-object v6, v6

    .line 75934
    sget-object v7, LX/26p;->g:LX/0Tn;

    invoke-interface {v4, v7, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 75935
    iput-object v7, v6, LX/0SK;->e:Ljava/lang/String;

    .line 75936
    move-object v6, v6

    .line 75937
    sget-object v7, LX/26p;->h:LX/0Tn;

    invoke-interface {v4, v7, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 75938
    iput-object v7, v6, LX/0SK;->f:Ljava/lang/String;

    .line 75939
    move-object v6, v6

    .line 75940
    sget-object v7, LX/26p;->i:LX/0Tn;

    invoke-interface {v4, v7, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 75941
    iput-object v4, v6, LX/0SK;->g:Ljava/lang/String;

    .line 75942
    move-object v4, v6

    .line 75943
    invoke-virtual {v4}, LX/0SK;->h()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v11

    .line 75944
    iget-object v4, v11, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v4, v4

    .line 75945
    iget-object v5, v11, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v5, v5

    .line 75946
    iget-object v6, v11, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v6, v6

    .line 75947
    iget-object v7, v11, Lcom/facebook/auth/viewercontext/ViewerContext;->e:Ljava/lang/String;

    move-object v7, v7

    .line 75948
    iget-object v8, v11, Lcom/facebook/auth/viewercontext/ViewerContext;->f:Ljava/lang/String;

    move-object v8, v8

    .line 75949
    iget-object v9, v11, Lcom/facebook/auth/viewercontext/ViewerContext;->g:Ljava/lang/String;

    move-object v9, v9

    .line 75950
    invoke-static/range {v4 .. v10}, LX/0WK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1gW;)V

    move-object v4, v11

    .line 75951
    :goto_4
    invoke-static {v0}, LX/0WK;->f(LX/0WK;)V

    move-object v5, v4

    .line 75952
    :cond_7
    const-string v4, "is_imported"

    const/4 v6, 0x1

    invoke-interface {v10, v4, v6}, LX/1gW;->a(Ljava/lang/String;Z)LX/1gW;

    .line 75953
    invoke-interface {v10}, LX/1gW;->b()Z

    goto/16 :goto_3

    :cond_8
    move-object v4, v5

    goto :goto_4
.end method

.method public final declared-synchronized a(Lcom/facebook/auth/credentials/FacebookCredentials;)V
    .locals 8

    .prologue
    .line 75954
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0WJ;->c:LX/0WK;

    .line 75955
    iget-object v1, v0, LX/0WK;->a:LX/0WS;

    const-string v2, "is_imported"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0WS;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 75956
    iget-object v1, v0, LX/0WK;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 75957
    sget-object v2, LX/26p;->c:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, LX/26p;->d:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 75958
    :cond_0
    invoke-static {v0}, LX/0WK;->f(LX/0WK;)V

    .line 75959
    :cond_1
    iget-object v1, v0, LX/0WK;->a:LX/0WS;

    invoke-virtual {v1}, LX/0WS;->b()LX/1gW;

    move-result-object v7

    .line 75960
    iget-object v1, p1, Lcom/facebook/auth/credentials/FacebookCredentials;->a:Ljava/lang/String;

    move-object v1, v1

    .line 75961
    iget-object v2, p1, Lcom/facebook/auth/credentials/FacebookCredentials;->b:Ljava/lang/String;

    move-object v2, v2

    .line 75962
    iget-object v3, p1, Lcom/facebook/auth/credentials/FacebookCredentials;->c:Ljava/lang/String;

    move-object v3, v3

    .line 75963
    iget-object v4, p1, Lcom/facebook/auth/credentials/FacebookCredentials;->d:Ljava/lang/String;

    move-object v4, v4

    .line 75964
    iget-object v5, p1, Lcom/facebook/auth/credentials/FacebookCredentials;->e:Ljava/lang/String;

    move-object v5, v5

    .line 75965
    iget-object v6, p1, Lcom/facebook/auth/credentials/FacebookCredentials;->f:Ljava/lang/String;

    move-object v6, v6

    .line 75966
    invoke-static/range {v1 .. v7}, LX/0WK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1gW;)V

    .line 75967
    const-string v1, "is_imported"

    const/4 v2, 0x1

    invoke-interface {v7, v1, v2}, LX/1gW;->a(Ljava/lang/String;Z)LX/1gW;

    .line 75968
    invoke-interface {v7}, LX/1gW;->b()Z

    .line 75969
    const/4 v0, 0x0

    iput-object v0, p0, LX/0WJ;->k:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 75970
    const/4 v0, 0x0

    iput-object v0, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 75971
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0WJ;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75972
    monitor-exit p0

    return-void

    .line 75973
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/user/model/User;)V
    .locals 8

    .prologue
    .line 75974
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    if-eqz v0, :cond_e

    .line 75975
    new-instance v0, LX/0XI;

    invoke-direct {v0}, LX/0XI;-><init>()V

    invoke-virtual {v0, p1}, LX/0XI;->a(Lcom/facebook/user/model/User;)LX/0XI;

    move-result-object v0

    .line 75976
    iget-object v1, v0, LX/0XI;->u:LX/03R;

    move-object v1, v1

    .line 75977
    sget-object v2, LX/03R;->UNSET:LX/03R;

    if-ne v1, v2, :cond_0

    .line 75978
    iget-object v1, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 75979
    iget-object v2, v1, Lcom/facebook/user/model/User;->n:LX/03R;

    move-object v1, v2

    .line 75980
    iput-object v1, v0, LX/0XI;->u:LX/03R;

    .line 75981
    :cond_0
    iget-object v1, v0, LX/0XI;->c:Ljava/util/List;

    move-object v1, v1

    .line 75982
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 75983
    iget-object v1, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 75984
    iget-object v2, v1, Lcom/facebook/user/model/User;->c:LX/0Px;

    move-object v1, v2

    .line 75985
    iput-object v1, v0, LX/0XI;->c:Ljava/util/List;

    .line 75986
    :cond_1
    iget-object v1, v0, LX/0XI;->d:Ljava/util/List;

    move-object v1, v1

    .line 75987
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 75988
    iget-object v1, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->p()LX/0Px;

    move-result-object v1

    .line 75989
    iput-object v1, v0, LX/0XI;->d:Ljava/util/List;

    .line 75990
    :cond_2
    iget-object v1, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 75991
    iget-boolean v2, v1, Lcom/facebook/user/model/User;->o:Z

    move v1, v2

    .line 75992
    if-eqz v1, :cond_3

    .line 75993
    const/4 v1, 0x1

    .line 75994
    iput-boolean v1, v0, LX/0XI;->v:Z

    .line 75995
    :cond_3
    iget-object v1, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 75996
    iget-boolean v2, v1, Lcom/facebook/user/model/User;->p:Z

    move v1, v2

    .line 75997
    if-eqz v1, :cond_4

    .line 75998
    const/4 v1, 0x1

    .line 75999
    iput-boolean v1, v0, LX/0XI;->w:Z

    .line 76000
    :cond_4
    iget-object v1, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 76001
    iget-boolean v2, v1, Lcom/facebook/user/model/User;->F:Z

    move v1, v2

    .line 76002
    if-eqz v1, :cond_5

    .line 76003
    const/4 v1, 0x1

    .line 76004
    iput-boolean v1, v0, LX/0XI;->M:Z

    .line 76005
    :cond_5
    iget-object v1, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 76006
    iget-boolean v2, v1, Lcom/facebook/user/model/User;->P:Z

    move v1, v2

    .line 76007
    if-eqz v1, :cond_6

    .line 76008
    const/4 v1, 0x1

    .line 76009
    iput-boolean v1, v0, LX/0XI;->S:Z

    .line 76010
    :cond_6
    iget-object v1, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 76011
    iget-boolean v2, v1, Lcom/facebook/user/model/User;->aa:Z

    move v1, v2

    .line 76012
    if-eqz v1, :cond_7

    .line 76013
    const/4 v1, 0x1

    .line 76014
    iput-boolean v1, v0, LX/0XI;->af:Z

    .line 76015
    :cond_7
    iget-object v1, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 76016
    iget-boolean v2, v1, Lcom/facebook/user/model/User;->G:Z

    move v1, v2

    .line 76017
    if-eqz v1, :cond_8

    .line 76018
    const/4 v1, 0x1

    .line 76019
    iput-boolean v1, v0, LX/0XI;->N:Z

    .line 76020
    :cond_8
    iget v1, v0, LX/0XI;->K:I

    move v1, v1

    .line 76021
    if-eqz v1, :cond_9

    .line 76022
    iget v1, v0, LX/0XI;->L:I

    move v1, v1

    .line 76023
    if-nez v1, :cond_a

    .line 76024
    :cond_9
    iget v1, v0, LX/0XI;->J:I

    move v1, v1

    .line 76025
    if-nez v1, :cond_f

    .line 76026
    iget-object v1, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 76027
    iget v2, v1, Lcom/facebook/user/model/User;->C:I

    move v1, v2

    .line 76028
    iget-object v2, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 76029
    iget v3, v2, Lcom/facebook/user/model/User;->D:I

    move v2, v3

    .line 76030
    iget-object v3, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 76031
    iget v4, v3, Lcom/facebook/user/model/User;->E:I

    move v3, v4

    .line 76032
    invoke-virtual {v0, v1, v2, v3}, LX/0XI;->a(III)LX/0XI;

    .line 76033
    :cond_a
    :goto_0
    iget-object v1, v0, LX/0XI;->O:LX/03R;

    move-object v1, v1

    .line 76034
    sget-object v2, LX/03R;->UNSET:LX/03R;

    if-ne v1, v2, :cond_b

    .line 76035
    iget-object v1, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 76036
    iget-object v2, v1, Lcom/facebook/user/model/User;->H:LX/03R;

    move-object v1, v2

    .line 76037
    iput-object v1, v0, LX/0XI;->O:LX/03R;

    .line 76038
    :cond_b
    iget-wide v6, v0, LX/0XI;->X:J

    move-wide v2, v6

    .line 76039
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_c

    .line 76040
    iget-object v1, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 76041
    iget-wide v6, v1, Lcom/facebook/user/model/User;->N:J

    move-wide v2, v6

    .line 76042
    iput-wide v2, v0, LX/0XI;->X:J

    .line 76043
    :cond_c
    iget-object v1, v0, LX/0XI;->ah:LX/0XN;

    move-object v1, v1

    .line 76044
    if-nez v1, :cond_d

    .line 76045
    iget-object v1, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 76046
    iget-object v2, v1, Lcom/facebook/user/model/User;->ac:LX/0XN;

    move-object v1, v2

    .line 76047
    iput-object v1, v0, LX/0XI;->ah:LX/0XN;

    .line 76048
    :cond_d
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object p1

    .line 76049
    :cond_e
    invoke-virtual {p0, p1}, LX/0WJ;->c(Lcom/facebook/user/model/User;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76050
    monitor-exit p0

    return-void

    .line 76051
    :cond_f
    :try_start_1
    iget-object v1, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 76052
    iget v2, v1, Lcom/facebook/user/model/User;->D:I

    move v1, v2

    .line 76053
    iget-object v2, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 76054
    iget v3, v2, Lcom/facebook/user/model/User;->E:I

    move v2, v3

    .line 76055
    invoke-virtual {v0, v1, v2}, LX/0XI;->a(II)LX/0XI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 76056
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 76057
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0WJ;->e:LX/0X1;

    .line 76058
    invoke-static {v0, p1}, LX/0X1;->d(LX/0X1;Ljava/lang/String;)LX/0WS;

    move-result-object v1

    .line 76059
    if-nez v1, :cond_0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76060
    :goto_0
    monitor-exit p0

    return-void

    .line 76061
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 76062
    :cond_0
    const-string v2, "is_imported"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0WS;->a(Ljava/lang/String;Z)Z

    move-result v2

    .line 76063
    invoke-virtual {v1}, LX/0WS;->b()LX/1gW;

    move-result-object v1

    .line 76064
    invoke-interface {v1}, LX/1gW;->a()LX/1gW;

    .line 76065
    if-eqz v2, :cond_1

    .line 76066
    const-string v2, "is_imported"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/1gW;->a(Ljava/lang/String;Z)LX/1gW;

    .line 76067
    :cond_1
    invoke-interface {v1}, LX/1gW;->b()Z

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 75790
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0WJ;->g:Z

    if-nez v0, :cond_0

    .line 75791
    const/4 v0, 0x0

    iput-object v0, p0, LX/0WJ;->k:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 75792
    iget-object v0, p0, LX/0WJ;->c:LX/0WK;

    .line 75793
    iget-object v1, v0, LX/0WK;->a:LX/0WS;

    const-string v2, "access_token"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75794
    iget-object v1, v0, LX/0WK;->a:LX/0WS;

    invoke-virtual {v1}, LX/0WS;->b()LX/1gW;

    move-result-object v1

    .line 75795
    const-string v2, "access_token"

    invoke-interface {v1, v2, p2}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;

    .line 75796
    invoke-interface {v1}, LX/1gW;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75797
    :cond_0
    monitor-exit p0

    return-void

    .line 75798
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 75786
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/0WJ;->n()Lcom/facebook/user/model/User;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Lcom/facebook/user/model/User;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 75782
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/0WJ;->g()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 75783
    const/4 v0, 0x0

    .line 75784
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-direct {p0}, LX/0WJ;->n()Lcom/facebook/user/model/User;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 75785
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Lcom/facebook/user/model/User;)V
    .locals 5

    .prologue
    .line 75757
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0WJ;->e:LX/0X1;

    .line 75758
    if-nez p1, :cond_1

    .line 75759
    :cond_0
    :goto_0
    iput-object p1, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    .line 75760
    iget-object v0, p0, LX/0WJ;->h:Lcom/facebook/user/model/User;

    invoke-direct {p0, v0}, LX/0WJ;->d(Lcom/facebook/user/model/User;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75761
    :try_start_1
    iget-object v0, p0, LX/0WJ;->f:Landroid/content/Context;

    .line 75762
    iget-boolean v1, p1, Lcom/facebook/user/model/User;->o:Z

    move v1, v1

    .line 75763
    invoke-static {v0, v1}, LX/0BH;->a(Landroid/content/Context;Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75764
    :goto_1
    monitor-exit p0

    return-void

    .line 75765
    :catch_0
    move-exception v0

    .line 75766
    :try_start_2
    sget-object v1, LX/0WJ;->a:Ljava/lang/Class;

    const-string v2, "could not set employee flag"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 75767
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 75768
    :cond_1
    iget-object v1, p1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v1

    .line 75769
    invoke-static {v0, v1}, LX/0X1;->d(LX/0X1;Ljava/lang/String;)LX/0WS;

    move-result-object v1

    .line 75770
    if-eqz v1, :cond_0

    .line 75771
    invoke-virtual {v1}, LX/0WS;->b()LX/1gW;

    move-result-object v2

    .line 75772
    const-string v3, "is_imported"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/0WS;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_2

    .line 75773
    iget-object v1, v0, LX/0X1;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 75774
    sget-object v3, LX/26p;->u:LX/0Tn;

    invoke-interface {v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 75775
    iget-object v1, v0, LX/0X1;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 75776
    sget-object v3, LX/26p;->u:LX/0Tn;

    invoke-interface {v1, v3}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 75777
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 75778
    :cond_2
    invoke-interface {v2}, LX/1gW;->a()LX/1gW;

    .line 75779
    invoke-static {p1, v2}, LX/0XH;->a(Lcom/facebook/user/model/User;LX/1gW;)V

    .line 75780
    const-string v1, "is_imported"

    const/4 v3, 0x1

    invoke-interface {v2, v1, v3}, LX/1gW;->a(Ljava/lang/String;Z)LX/1gW;

    .line 75781
    invoke-interface {v2}, LX/1gW;->b()Z

    goto :goto_0
.end method

.method public final declared-synchronized d()Z
    .locals 1

    .prologue
    .line 75756
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0WJ;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Z
    .locals 3

    .prologue
    .line 75743
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 75744
    if-nez v0, :cond_0

    .line 75745
    const/4 v0, 0x0

    .line 75746
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v1, p0, LX/0WJ;->e:LX/0X1;

    .line 75747
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v2

    .line 75748
    invoke-static {v1, v0}, LX/0X1;->d(LX/0X1;Ljava/lang/String;)LX/0WS;

    move-result-object v2

    .line 75749
    if-nez v2, :cond_1

    .line 75750
    const/4 v2, 0x0

    .line 75751
    :goto_1
    move v0, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75752
    goto :goto_0

    .line 75753
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 75754
    :cond_1
    const-string v1, "uid"

    const/4 v0, 0x0

    invoke-virtual {v2, v1, v0}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    move v2, v1

    .line 75755
    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final declared-synchronized f()V
    .locals 1

    .prologue
    .line 75739
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/0WJ;->m()V

    .line 75740
    invoke-direct {p0}, LX/0WJ;->p()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75741
    monitor-exit p0

    return-void

    .line 75742
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()Z
    .locals 1

    .prologue
    .line 75738
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0WJ;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()Z
    .locals 1

    .prologue
    .line 75737
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0WJ;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i()V
    .locals 3

    .prologue
    .line 75732
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0WJ;->c:LX/0WK;

    .line 75733
    iget-object v1, v0, LX/0WK;->a:LX/0WS;

    invoke-virtual {v1}, LX/0WS;->b()LX/1gW;

    move-result-object v1

    const-string v2, "session_cookies_string"

    invoke-interface {v1, v2}, LX/1gW;->a(Ljava/lang/String;)LX/1gW;

    move-result-object v1

    invoke-interface {v1}, LX/1gW;->b()Z

    .line 75734
    const/4 v0, 0x0

    iput-object v0, p0, LX/0WJ;->k:Lcom/facebook/auth/viewercontext/ViewerContext;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75735
    monitor-exit p0

    return-void

    .line 75736
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized j()V
    .locals 1

    .prologue
    .line 75729
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/0WJ;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75730
    monitor-exit p0

    return-void

    .line 75731
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized k()V
    .locals 1

    .prologue
    .line 75726
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/0WJ;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75727
    monitor-exit p0

    return-void

    .line 75728
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
