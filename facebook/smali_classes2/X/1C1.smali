.class public LX/1C1;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 214673
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 214674
    return-void
.end method

.method public static a(LX/0Zr;)Landroid/os/Handler;
    .locals 3
    .annotation runtime Lcom/facebook/browser/prefetch/BrowserPrefetchHandler;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 214675
    const-string v0, "BrowserPrefetcher"

    sget-object v1, LX/0TP;->NORMAL:LX/0TP;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, LX/0Zr;->a(Ljava/lang/String;LX/0TP;Z)Landroid/os/HandlerThread;

    move-result-object v0

    .line 214676
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 214677
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-object v1
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 214678
    return-void
.end method
