.class public LX/0XI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public A:Z

.field public B:LX/4nY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/4nX;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:J

.field public E:J

.field public F:Z

.field public G:Z

.field public H:Z

.field public I:Z

.field public J:I

.field public K:I

.field public L:I

.field public M:Z

.field public N:Z

.field public O:LX/03R;

.field public P:Z

.field public Q:Z

.field public R:Z

.field public S:Z

.field public T:F

.field public U:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Z

.field public W:Z

.field public X:J

.field public Y:Z

.field public Z:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:LX/0XG;

.field public aa:Z

.field public ab:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:I

.field public af:Z

.field public ag:Z

.field public ah:LX/0XN;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:Lcom/facebook/user/model/User;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:LX/0XK;

.field public al:Lcom/facebook/user/model/User;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:Z

.field public an:J

.field public b:Ljava/lang/String;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/UserEmailAddress;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/UserPhoneNumber;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserCustomTag;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/String;

.field public g:Lcom/facebook/user/model/Name;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Lcom/facebook/user/model/Name;

.field public l:Ljava/lang/String;

.field public m:LX/0XJ;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Lcom/facebook/user/model/PicSquare;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:F

.field public u:LX/03R;

.field public v:Z

.field public w:Z

.field public x:Ljava/lang/String;

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 77998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77999
    iput-object v0, p0, LX/0XI;->c:Ljava/util/List;

    .line 78000
    iput-object v0, p0, LX/0XI;->d:Ljava/util/List;

    .line 78001
    iput-object v0, p0, LX/0XI;->e:LX/0Px;

    .line 78002
    sget-object v0, LX/0XJ;->UNKNOWN:LX/0XJ;

    iput-object v0, p0, LX/0XI;->m:LX/0XJ;

    .line 78003
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/0XI;->u:LX/03R;

    .line 78004
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/0XI;->O:LX/03R;

    .line 78005
    sget-object v0, LX/0XK;->UNSET:LX/0XK;

    iput-object v0, p0, LX/0XI;->ak:LX/0XK;

    .line 78006
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0XI;->am:Z

    return-void
.end method

.method private static c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 77951
    if-nez p0, :cond_0

    const-string p0, ""

    .line 77952
    :cond_0
    const-string v0, "%s:%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    if-nez p1, :cond_1

    const-string p1, ""

    :cond_1
    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static o(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 77953
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 77954
    :cond_0
    :goto_0
    return-object v0

    .line 77955
    :cond_1
    const/16 v1, 0x3a

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 77956
    if-ltz v1, :cond_0

    .line 77957
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 77958
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method public static p(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 77959
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 77960
    :cond_0
    :goto_0
    return-object v0

    .line 77961
    :cond_1
    const/16 v1, 0x3a

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 77962
    if-ltz v1, :cond_0

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 77963
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(F)LX/0XI;
    .locals 0

    .prologue
    .line 77964
    iput p1, p0, LX/0XI;->t:F

    .line 77965
    return-object p0
.end method

.method public final a(I)LX/0XI;
    .locals 0

    .prologue
    .line 77966
    iput p1, p0, LX/0XI;->ae:I

    .line 77967
    return-object p0
.end method

.method public final a(II)LX/0XI;
    .locals 0

    .prologue
    .line 77968
    iput p1, p0, LX/0XI;->K:I

    .line 77969
    iput p2, p0, LX/0XI;->L:I

    .line 77970
    return-object p0
.end method

.method public final a(III)LX/0XI;
    .locals 0

    .prologue
    .line 77971
    iput p1, p0, LX/0XI;->J:I

    .line 77972
    iput p2, p0, LX/0XI;->K:I

    .line 77973
    iput p3, p0, LX/0XI;->L:I

    .line 77974
    return-object p0
.end method

.method public final a(J)LX/0XI;
    .locals 1

    .prologue
    .line 77975
    iput-wide p1, p0, LX/0XI;->D:J

    .line 77976
    return-object p0
.end method

.method public final a(LX/03R;)LX/0XI;
    .locals 0

    .prologue
    .line 77977
    iput-object p1, p0, LX/0XI;->u:LX/03R;

    .line 77978
    return-object p0
.end method

.method public final a(LX/0Px;)LX/0XI;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserCustomTag;",
            ">;)",
            "LX/0XI;"
        }
    .end annotation

    .prologue
    .line 77979
    iput-object p1, p0, LX/0XI;->e:LX/0Px;

    .line 77980
    return-object p0
.end method

.method public final a(LX/0XG;Ljava/lang/String;)LX/0XI;
    .locals 0

    .prologue
    .line 77981
    iput-object p1, p0, LX/0XI;->a:LX/0XG;

    .line 77982
    iput-object p2, p0, LX/0XI;->b:Ljava/lang/String;

    .line 77983
    return-object p0
.end method

.method public final a(LX/0XK;)LX/0XI;
    .locals 0

    .prologue
    .line 77984
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77985
    iput-object p1, p0, LX/0XI;->ak:LX/0XK;

    .line 77986
    return-object p0
.end method

.method public final a(LX/4nY;)LX/0XI;
    .locals 0
    .param p1    # LX/4nY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 77989
    iput-object p1, p0, LX/0XI;->B:LX/4nY;

    .line 77990
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;)LX/0XI;
    .locals 0
    .param p1    # Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 77987
    iput-object p1, p0, LX/0XI;->ai:Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;

    .line 77988
    return-object p0
.end method

.method public final a(Lcom/facebook/user/model/PicSquare;)LX/0XI;
    .locals 0

    .prologue
    .line 78130
    iput-object p1, p0, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 78131
    return-object p0
.end method

.method public final a(Lcom/facebook/user/model/User;)LX/0XI;
    .locals 4

    .prologue
    .line 78015
    iget-object v0, p1, Lcom/facebook/user/model/User;->b:LX/0XG;

    move-object v0, v0

    .line 78016
    iput-object v0, p0, LX/0XI;->a:LX/0XG;

    .line 78017
    iget-object v0, p1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v0

    .line 78018
    iput-object v0, p0, LX/0XI;->b:Ljava/lang/String;

    .line 78019
    iget-object v0, p1, Lcom/facebook/user/model/User;->c:LX/0Px;

    move-object v0, v0

    .line 78020
    iput-object v0, p0, LX/0XI;->c:Ljava/util/List;

    .line 78021
    iget-object v0, p1, Lcom/facebook/user/model/User;->d:LX/0Px;

    move-object v0, v0

    .line 78022
    iput-object v0, p0, LX/0XI;->e:LX/0Px;

    .line 78023
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->p()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/0XI;->d:Ljava/util/List;

    .line 78024
    iget-object v0, p1, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 78025
    iput-object v0, p0, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 78026
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0XI;->h:Ljava/lang/String;

    .line 78027
    iget-object v0, p1, Lcom/facebook/user/model/User;->g:Ljava/lang/String;

    move-object v0, v0

    .line 78028
    iput-object v0, p0, LX/0XI;->l:Ljava/lang/String;

    .line 78029
    iget-object v0, p1, Lcom/facebook/user/model/User;->h:LX/0XJ;

    move-object v0, v0

    .line 78030
    iput-object v0, p0, LX/0XI;->m:LX/0XJ;

    .line 78031
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0XI;->n:Ljava/lang/String;

    .line 78032
    iget-object v0, p1, Lcom/facebook/user/model/User;->j:Ljava/lang/String;

    move-object v0, v0

    .line 78033
    iput-object v0, p0, LX/0XI;->o:Ljava/lang/String;

    .line 78034
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->w()Lcom/facebook/user/model/PicSquare;

    move-result-object v0

    iput-object v0, p0, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 78035
    iget-object v0, p1, Lcom/facebook/user/model/User;->k:Ljava/lang/String;

    move-object v0, v0

    .line 78036
    iput-object v0, p0, LX/0XI;->r:Ljava/lang/String;

    .line 78037
    iget-object v0, p1, Lcom/facebook/user/model/User;->l:Ljava/lang/String;

    move-object v0, v0

    .line 78038
    iput-object v0, p0, LX/0XI;->s:Ljava/lang/String;

    .line 78039
    iget v0, p1, Lcom/facebook/user/model/User;->m:F

    move v0, v0

    .line 78040
    iput v0, p0, LX/0XI;->t:F

    .line 78041
    iget-object v0, p1, Lcom/facebook/user/model/User;->n:LX/03R;

    move-object v0, v0

    .line 78042
    iput-object v0, p0, LX/0XI;->u:LX/03R;

    .line 78043
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->o:Z

    move v0, v0

    .line 78044
    iput-boolean v0, p0, LX/0XI;->v:Z

    .line 78045
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->p:Z

    move v0, v0

    .line 78046
    iput-boolean v0, p0, LX/0XI;->w:Z

    .line 78047
    iget-object v0, p1, Lcom/facebook/user/model/User;->q:Ljava/lang/String;

    move-object v0, v0

    .line 78048
    iput-object v0, p0, LX/0XI;->x:Ljava/lang/String;

    .line 78049
    iget-object v0, p1, Lcom/facebook/user/model/User;->r:Ljava/lang/String;

    move-object v0, v0

    .line 78050
    iput-object v0, p0, LX/0XI;->y:Ljava/lang/String;

    .line 78051
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->s:Z

    move v0, v0

    .line 78052
    iput-boolean v0, p0, LX/0XI;->z:Z

    .line 78053
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->t:Z

    move v0, v0

    .line 78054
    iput-boolean v0, p0, LX/0XI;->A:Z

    .line 78055
    iget-object v0, p1, Lcom/facebook/user/model/User;->u:LX/4nY;

    move-object v0, v0

    .line 78056
    iput-object v0, p0, LX/0XI;->B:LX/4nY;

    .line 78057
    iget-object v0, p1, Lcom/facebook/user/model/User;->v:LX/0Px;

    move-object v0, v0

    .line 78058
    iput-object v0, p0, LX/0XI;->C:LX/0Px;

    .line 78059
    iget-wide v2, p1, Lcom/facebook/user/model/User;->w:J

    move-wide v0, v2

    .line 78060
    iput-wide v0, p0, LX/0XI;->D:J

    .line 78061
    iget-wide v2, p1, Lcom/facebook/user/model/User;->x:J

    move-wide v0, v2

    .line 78062
    iput-wide v0, p0, LX/0XI;->E:J

    .line 78063
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->y:Z

    move v0, v0

    .line 78064
    iput-boolean v0, p0, LX/0XI;->F:Z

    .line 78065
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->z:Z

    move v0, v0

    .line 78066
    iput-boolean v0, p0, LX/0XI;->G:Z

    .line 78067
    iget v0, p1, Lcom/facebook/user/model/User;->C:I

    move v0, v0

    .line 78068
    iput v0, p0, LX/0XI;->J:I

    .line 78069
    iget v0, p1, Lcom/facebook/user/model/User;->D:I

    move v0, v0

    .line 78070
    iput v0, p0, LX/0XI;->K:I

    .line 78071
    iget v0, p1, Lcom/facebook/user/model/User;->E:I

    move v0, v0

    .line 78072
    iput v0, p0, LX/0XI;->L:I

    .line 78073
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->F:Z

    move v0, v0

    .line 78074
    iput-boolean v0, p0, LX/0XI;->M:Z

    .line 78075
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->G:Z

    move v0, v0

    .line 78076
    iput-boolean v0, p0, LX/0XI;->N:Z

    .line 78077
    iget-object v0, p1, Lcom/facebook/user/model/User;->H:LX/03R;

    move-object v0, v0

    .line 78078
    iput-object v0, p0, LX/0XI;->O:LX/03R;

    .line 78079
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->I:Z

    move v0, v0

    .line 78080
    iput-boolean v0, p0, LX/0XI;->P:Z

    .line 78081
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->J:Z

    move v0, v0

    .line 78082
    iput-boolean v0, p0, LX/0XI;->Q:Z

    .line 78083
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->K:Z

    move v0, v0

    .line 78084
    iput-boolean v0, p0, LX/0XI;->R:Z

    .line 78085
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->L:Z

    move v0, v0

    .line 78086
    iput-boolean v0, p0, LX/0XI;->am:Z

    .line 78087
    iget-wide v2, p1, Lcom/facebook/user/model/User;->M:J

    move-wide v0, v2

    .line 78088
    iput-wide v0, p0, LX/0XI;->an:J

    .line 78089
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->P:Z

    move v0, v0

    .line 78090
    iput-boolean v0, p0, LX/0XI;->S:Z

    .line 78091
    iget v0, p1, Lcom/facebook/user/model/User;->Q:F

    move v0, v0

    .line 78092
    iput v0, p0, LX/0XI;->T:F

    .line 78093
    iget-object v0, p1, Lcom/facebook/user/model/User;->R:LX/0Px;

    move-object v0, v0

    .line 78094
    iput-object v0, p0, LX/0XI;->U:LX/0Px;

    .line 78095
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->S:Z

    move v0, v0

    .line 78096
    iput-boolean v0, p0, LX/0XI;->V:Z

    .line 78097
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->T:Z

    move v0, v0

    .line 78098
    iput-boolean v0, p0, LX/0XI;->W:Z

    .line 78099
    iget-wide v2, p1, Lcom/facebook/user/model/User;->N:J

    move-wide v0, v2

    .line 78100
    iput-wide v0, p0, LX/0XI;->X:J

    .line 78101
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->O:Z

    move v0, v0

    .line 78102
    iput-boolean v0, p0, LX/0XI;->Y:Z

    .line 78103
    iget-object v0, p1, Lcom/facebook/user/model/User;->U:LX/0Px;

    move-object v0, v0

    .line 78104
    iput-object v0, p0, LX/0XI;->Z:LX/0Px;

    .line 78105
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->V:Z

    move v0, v0

    .line 78106
    iput-boolean v0, p0, LX/0XI;->aa:Z

    .line 78107
    iget-object v0, p1, Lcom/facebook/user/model/User;->W:Landroid/net/Uri;

    move-object v0, v0

    .line 78108
    iput-object v0, p0, LX/0XI;->ab:Landroid/net/Uri;

    .line 78109
    iget-object v0, p1, Lcom/facebook/user/model/User;->X:Ljava/lang/String;

    move-object v0, v0

    .line 78110
    iput-object v0, p0, LX/0XI;->ac:Ljava/lang/String;

    .line 78111
    iget-object v0, p1, Lcom/facebook/user/model/User;->Y:Ljava/lang/String;

    move-object v0, v0

    .line 78112
    iput-object v0, p0, LX/0XI;->ad:Ljava/lang/String;

    .line 78113
    iget v0, p1, Lcom/facebook/user/model/User;->Z:I

    move v0, v0

    .line 78114
    iput v0, p0, LX/0XI;->ae:I

    .line 78115
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->aa:Z

    move v0, v0

    .line 78116
    iput-boolean v0, p0, LX/0XI;->af:Z

    .line 78117
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->ab:Z

    move v0, v0

    .line 78118
    iput-boolean v0, p0, LX/0XI;->ag:Z

    .line 78119
    iget-object v0, p1, Lcom/facebook/user/model/User;->ac:LX/0XN;

    move-object v0, v0

    .line 78120
    iput-object v0, p0, LX/0XI;->ah:LX/0XN;

    .line 78121
    iget-object v0, p1, Lcom/facebook/user/model/User;->ad:Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;

    move-object v0, v0

    .line 78122
    iput-object v0, p0, LX/0XI;->ai:Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;

    .line 78123
    iget-object v0, p1, Lcom/facebook/user/model/User;->ae:Lcom/facebook/user/model/User;

    move-object v0, v0

    .line 78124
    iput-object v0, p0, LX/0XI;->aj:Lcom/facebook/user/model/User;

    .line 78125
    iget-object v0, p1, Lcom/facebook/user/model/User;->af:LX/0XK;

    move-object v0, v0

    .line 78126
    iput-object v0, p0, LX/0XI;->ak:LX/0XK;

    .line 78127
    iget-object v0, p1, Lcom/facebook/user/model/User;->ag:Lcom/facebook/user/model/User;

    move-object v0, v0

    .line 78128
    iput-object v0, p0, LX/0XI;->al:Lcom/facebook/user/model/User;

    .line 78129
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/0XI;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 78012
    sget-object v0, LX/0XG;->PHONE_NUMBER:LX/0XG;

    iput-object v0, p0, LX/0XI;->a:LX/0XG;

    .line 78013
    invoke-static {p1, p2}, LX/0XI;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0XI;->b:Ljava/lang/String;

    .line 78014
    return-object p0
.end method

.method public final aj()Lcom/facebook/user/model/User;
    .locals 1

    .prologue
    .line 78011
    new-instance v0, Lcom/facebook/user/model/User;

    invoke-direct {v0, p0}, Lcom/facebook/user/model/User;-><init>(LX/0XI;)V

    return-object v0
.end method

.method public final b(F)LX/0XI;
    .locals 0

    .prologue
    .line 78009
    iput p1, p0, LX/0XI;->T:F

    .line 78010
    return-object p0
.end method

.method public final b(J)LX/0XI;
    .locals 1

    .prologue
    .line 78007
    iput-wide p1, p0, LX/0XI;->E:J

    .line 78008
    return-object p0
.end method

.method public final b(LX/0Px;)LX/0XI;
    .locals 0
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/4nX;",
            ">;)",
            "LX/0XI;"
        }
    .end annotation

    .prologue
    .line 77895
    iput-object p1, p0, LX/0XI;->C:LX/0Px;

    .line 77896
    return-object p0
.end method

.method public final b(Lcom/facebook/user/model/Name;)LX/0XI;
    .locals 0

    .prologue
    .line 77996
    iput-object p1, p0, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 77997
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/0XI;
    .locals 0

    .prologue
    .line 77994
    iput-object p1, p0, LX/0XI;->h:Ljava/lang/String;

    .line 77995
    return-object p0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)LX/0XI;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 77991
    sget-object v0, LX/0XG;->EMAIL:LX/0XG;

    iput-object v0, p0, LX/0XI;->a:LX/0XG;

    .line 77992
    invoke-static {p1, p2}, LX/0XI;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0XI;->b:Ljava/lang/String;

    .line 77993
    return-object p0
.end method

.method public final b(Ljava/util/List;)LX/0XI;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/UserPhoneNumber;",
            ">;)",
            "LX/0XI;"
        }
    .end annotation

    .prologue
    .line 77947
    iput-object p1, p0, LX/0XI;->d:Ljava/util/List;

    .line 77948
    return-object p0
.end method

.method public final c(J)LX/0XI;
    .locals 1

    .prologue
    .line 77949
    iput-wide p1, p0, LX/0XI;->an:J

    .line 77950
    return-object p0
.end method

.method public final c(LX/0Px;)LX/0XI;
    .locals 0
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;",
            ">;)",
            "LX/0XI;"
        }
    .end annotation

    .prologue
    .line 77893
    iput-object p1, p0, LX/0XI;->Z:LX/0Px;

    .line 77894
    return-object p0
.end method

.method public final c(Lcom/facebook/user/model/User;)LX/0XI;
    .locals 0
    .param p1    # Lcom/facebook/user/model/User;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 77897
    iput-object p1, p0, LX/0XI;->al:Lcom/facebook/user/model/User;

    .line 77898
    return-object p0
.end method

.method public final c(Z)LX/0XI;
    .locals 0

    .prologue
    .line 77899
    iput-boolean p1, p0, LX/0XI;->z:Z

    .line 77900
    return-object p0
.end method

.method public final d(J)LX/0XI;
    .locals 1

    .prologue
    .line 77901
    iput-wide p1, p0, LX/0XI;->X:J

    .line 77902
    return-object p0
.end method

.method public final d(LX/0Px;)LX/0XI;
    .locals 0
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;",
            ">;)",
            "LX/0XI;"
        }
    .end annotation

    .prologue
    .line 77903
    iput-object p1, p0, LX/0XI;->U:LX/0Px;

    .line 77904
    return-object p0
.end method

.method public final d(Z)LX/0XI;
    .locals 0

    .prologue
    .line 77905
    iput-boolean p1, p0, LX/0XI;->A:Z

    .line 77906
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/0XI;
    .locals 0

    .prologue
    .line 77907
    iput-object p1, p0, LX/0XI;->l:Ljava/lang/String;

    .line 77908
    return-object p0
.end method

.method public final e(Z)LX/0XI;
    .locals 0

    .prologue
    .line 77909
    iput-boolean p1, p0, LX/0XI;->P:Z

    .line 77910
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/0XI;
    .locals 0

    .prologue
    .line 77911
    iput-object p1, p0, LX/0XI;->n:Ljava/lang/String;

    .line 77912
    return-object p0
.end method

.method public final f(Z)LX/0XI;
    .locals 0

    .prologue
    .line 77913
    iput-boolean p1, p0, LX/0XI;->Q:Z

    .line 77914
    return-object p0
.end method

.method public final g(Z)LX/0XI;
    .locals 0

    .prologue
    .line 77915
    iput-boolean p1, p0, LX/0XI;->R:Z

    .line 77916
    return-object p0
.end method

.method public final h(Z)LX/0XI;
    .locals 0

    .prologue
    .line 77917
    iput-boolean p1, p0, LX/0XI;->F:Z

    .line 77918
    return-object p0
.end method

.method public final i(Ljava/lang/String;)LX/0XI;
    .locals 0

    .prologue
    .line 77919
    iput-object p1, p0, LX/0XI;->s:Ljava/lang/String;

    .line 77920
    return-object p0
.end method

.method public final j(Z)LX/0XI;
    .locals 0

    .prologue
    .line 77921
    iput-boolean p1, p0, LX/0XI;->H:Z

    .line 77922
    return-object p0
.end method

.method public final k(Ljava/lang/String;)LX/0XI;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 77923
    iput-object p1, p0, LX/0XI;->y:Ljava/lang/String;

    .line 77924
    return-object p0
.end method

.method public final k(Z)LX/0XI;
    .locals 0

    .prologue
    .line 77925
    iput-boolean p1, p0, LX/0XI;->I:Z

    .line 77926
    return-object p0
.end method

.method public final l(Ljava/lang/String;)LX/0XI;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 77927
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/0XI;->ab:Landroid/net/Uri;

    .line 77928
    return-object p0

    .line 77929
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l(Z)LX/0XI;
    .locals 0

    .prologue
    .line 77930
    iput-boolean p1, p0, LX/0XI;->am:Z

    .line 77931
    return-object p0
.end method

.method public final m(Ljava/lang/String;)LX/0XI;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 77932
    iput-object p1, p0, LX/0XI;->ac:Ljava/lang/String;

    .line 77933
    return-object p0
.end method

.method public final m(Z)LX/0XI;
    .locals 0

    .prologue
    .line 77934
    iput-boolean p1, p0, LX/0XI;->M:Z

    .line 77935
    return-object p0
.end method

.method public final n(Ljava/lang/String;)LX/0XI;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 77936
    iput-object p1, p0, LX/0XI;->ad:Ljava/lang/String;

    .line 77937
    return-object p0
.end method

.method public final p(Z)LX/0XI;
    .locals 0

    .prologue
    .line 77938
    iput-boolean p1, p0, LX/0XI;->V:Z

    .line 77939
    return-object p0
.end method

.method public final q(Z)LX/0XI;
    .locals 0

    .prologue
    .line 77940
    iput-boolean p1, p0, LX/0XI;->W:Z

    .line 77941
    return-object p0
.end method

.method public final r(Z)LX/0XI;
    .locals 0

    .prologue
    .line 77942
    iput-boolean p1, p0, LX/0XI;->Y:Z

    .line 77943
    return-object p0
.end method

.method public final t()F
    .locals 1

    .prologue
    .line 77944
    iget v0, p0, LX/0XI;->t:F

    return v0
.end method

.method public final u(Z)LX/0XI;
    .locals 0

    .prologue
    .line 77945
    iput-boolean p1, p0, LX/0XI;->ag:Z

    .line 77946
    return-object p0
.end method
