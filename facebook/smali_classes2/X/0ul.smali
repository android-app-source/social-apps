.class public LX/0ul;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

.field public final b:LX/0uk;


# direct methods
.method public constructor <init>(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;)V
    .locals 0
    .param p2    # LX/0uk;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 156932
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156933
    iput-object p1, p0, LX/0ul;->a:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    .line 156934
    iput-object p2, p0, LX/0ul;->b:LX/0uk;

    .line 156935
    return-void
.end method


# virtual methods
.method public final a(LX/0uh;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 156911
    iget-object v0, p0, LX/0ul;->a:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    iget-object v1, p0, LX/0ul;->b:LX/0uk;

    .line 156912
    invoke-static {v0, v1, p2}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->b(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;Ljava/lang/String;)V

    .line 156913
    goto :goto_0

    .line 156914
    :goto_0
    invoke-virtual {v1}, LX/0uk;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 156915
    :cond_0
    :goto_1
    return-void

    .line 156916
    :cond_1
    sget-object v2, LX/0uh;->UNKNOWN:LX/0uh;

    if-ne p1, v2, :cond_2

    .line 156917
    invoke-virtual {v0, v1}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a(LX/0uk;)LX/1Ep;

    move-result-object v2

    const-string p0, "unknown_priority"

    invoke-virtual {v2, p2, p0}, LX/1Ep;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 156918
    :cond_2
    invoke-virtual {v1, p1}, LX/0uk;->a(LX/0uh;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a(LX/0uk;)LX/1Ep;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1Ep;->d(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 156919
    iget-object v2, v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->f:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ConcurrentSkipListSet;

    .line 156920
    if-nez v2, :cond_3

    .line 156921
    new-instance p0, Ljava/util/concurrent/ConcurrentSkipListSet;

    invoke-direct {p0}, Ljava/util/concurrent/ConcurrentSkipListSet;-><init>()V

    .line 156922
    iget-object v2, v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->f:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, v1, p0}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ConcurrentSkipListSet;

    .line 156923
    if-eqz v2, :cond_4

    .line 156924
    :cond_3
    :goto_2
    move-object v2, v2

    .line 156925
    invoke-virtual {v2, p2}, Ljava/util/concurrent/ConcurrentSkipListSet;->add(Ljava/lang/Object;)Z

    .line 156926
    invoke-static {v0}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->b(Lcom/facebook/api/prefetch/GraphQLPrefetchController;)V

    goto :goto_1

    .line 156927
    :cond_4
    move-object v2, p0

    goto :goto_2
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 4

    .prologue
    .line 156928
    iget-object v0, p0, LX/0ul;->a:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    iget-object v1, p0, LX/0ul;->b:LX/0uk;

    .line 156929
    iget-object v2, v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v3, LX/1w7;

    const/4 p0, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-direct {v3, p1, v1, p0}, LX/1w7;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;LX/0uk;Ljava/lang/Integer;)V

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    .line 156930
    invoke-static {v0}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->b(Lcom/facebook/api/prefetch/GraphQLPrefetchController;)V

    .line 156931
    return-void
.end method
