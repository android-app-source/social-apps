.class public LX/1F6;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 221585
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    sput v0, LX/1F6;->a:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 221586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221587
    return-void
.end method

.method public static a(III)Landroid/util/SparseIntArray;
    .locals 1

    .prologue
    .line 221588
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 221589
    :goto_0
    if-gt p0, p1, :cond_0

    .line 221590
    invoke-virtual {v0, p0, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 221591
    mul-int/lit8 p0, p0, 0x2

    goto :goto_0

    .line 221592
    :cond_0
    return-object v0
.end method
