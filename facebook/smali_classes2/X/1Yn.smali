.class public final LX/1Yn;
.super LX/0aq;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aq",
        "<",
        "Landroid/net/Uri;",
        "LX/7K1;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Yl;


# direct methods
.method public constructor <init>(LX/1Yl;I)V
    .locals 0

    .prologue
    .line 274043
    iput-object p1, p0, LX/1Yn;->a:LX/1Yl;

    .line 274044
    invoke-direct {p0, p2}, LX/0aq;-><init>(I)V

    .line 274045
    return-void
.end method


# virtual methods
.method public final a(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 274046
    check-cast p2, Landroid/net/Uri;

    check-cast p3, LX/7K1;

    check-cast p4, LX/7K1;

    .line 274047
    if-eqz p1, :cond_0

    .line 274048
    invoke-virtual {p3}, LX/7K1;->g()V

    .line 274049
    :goto_0
    return-void

    .line 274050
    :cond_0
    iget v0, p3, LX/7K1;->f:I

    move v0, v0

    .line 274051
    invoke-static {v0}, LX/7K1;->a(I)Ljava/lang/String;

    iget-object v0, p0, LX/1Yn;->a:LX/1Yl;

    iget-object v0, v0, LX/1Yl;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    .line 274052
    if-nez p4, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v1, "Entry shall not be removed by overwriting using put(), it should be remove()ed"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
