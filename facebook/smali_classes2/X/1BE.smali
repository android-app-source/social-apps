.class public LX/1BE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Handler$Callback;
.implements LX/1B3;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile J:LX/1BE;


# instance fields
.field public A:Z

.field public B:Z

.field private C:Ljava/lang/String;

.field private D:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private E:J

.field private F:Ljava/lang/Boolean;

.field private G:Ljava/lang/Boolean;

.field public H:Ljava/lang/Boolean;

.field public I:Ljava/lang/Boolean;

.field public a:Landroid/os/Handler;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final b:LX/0SG;

.field private final c:LX/1BF;

.field public final d:LX/0rW;

.field private final e:LX/1B5;

.field private final f:LX/1BI;

.field private final g:LX/1BJ;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Bd;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AkS;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/1BK;

.field private final m:LX/1BX;

.field public final n:LX/0ad;

.field public final o:LX/0pf;

.field private final p:LX/1BY;

.field public final q:Ljava/util/concurrent/ExecutorService;

.field private final r:LX/0So;

.field private final s:LX/0y5;

.field private final t:LX/0qX;

.field private final u:LX/0qR;

.field public final v:LX/03V;

.field public final w:LX/1BO;

.field private final x:LX/1Bc;

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(LX/0SG;LX/1BF;LX/0rW;LX/1B5;LX/1BI;LX/1BJ;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/1BK;LX/0pf;LX/1BX;LX/0ad;LX/1BY;LX/0So;Ljava/util/concurrent/ExecutorService;LX/0y5;LX/0qX;LX/0qR;LX/03V;LX/1BO;LX/1Bc;)V
    .locals 4
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/analytics/vpvlogging/annotations/IsVpvDuplicateLoggingEnabled;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsCacheStateLoggingEnabled;
        .end annotation
    .end param
    .param p17    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/1BF;",
            "LX/0rW;",
            "LX/1B5;",
            "LX/1BI;",
            "LX/1BJ;",
            "LX/0Ot",
            "<",
            "LX/1Bd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AkS;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1BK;",
            "LX/0pf;",
            "LX/1BX;",
            "LX/0ad;",
            "LX/1BY;",
            "LX/0So;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0y5;",
            "LX/0qX;",
            "LX/0qR;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1BO;",
            "LX/1Bc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 212634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 212635
    const-string v2, "unknown"

    iput-object v2, p0, LX/1BE;->C:Ljava/lang/String;

    .line 212636
    const-wide/16 v2, 0x3e8

    iput-wide v2, p0, LX/1BE;->E:J

    .line 212637
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v2, p0, LX/1BE;->a:Landroid/os/Handler;

    .line 212638
    iput-object p1, p0, LX/1BE;->b:LX/0SG;

    .line 212639
    iput-object p2, p0, LX/1BE;->c:LX/1BF;

    .line 212640
    iput-object p3, p0, LX/1BE;->d:LX/0rW;

    .line 212641
    iput-object p4, p0, LX/1BE;->e:LX/1B5;

    .line 212642
    iput-object p5, p0, LX/1BE;->f:LX/1BI;

    .line 212643
    iput-object p6, p0, LX/1BE;->g:LX/1BJ;

    .line 212644
    iput-object p7, p0, LX/1BE;->h:LX/0Ot;

    .line 212645
    iput-object p8, p0, LX/1BE;->i:LX/0Ot;

    .line 212646
    iput-object p9, p0, LX/1BE;->j:LX/0Or;

    .line 212647
    iput-object p10, p0, LX/1BE;->k:LX/0Or;

    .line 212648
    iput-object p11, p0, LX/1BE;->l:LX/1BK;

    .line 212649
    move-object/from16 v0, p13

    iput-object v0, p0, LX/1BE;->m:LX/1BX;

    .line 212650
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1BE;->n:LX/0ad;

    .line 212651
    move-object/from16 v0, p12

    iput-object v0, p0, LX/1BE;->o:LX/0pf;

    .line 212652
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1BE;->p:LX/1BY;

    .line 212653
    move-object/from16 v0, p17

    iput-object v0, p0, LX/1BE;->q:Ljava/util/concurrent/ExecutorService;

    .line 212654
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1BE;->r:LX/0So;

    .line 212655
    move-object/from16 v0, p18

    iput-object v0, p0, LX/1BE;->s:LX/0y5;

    .line 212656
    move-object/from16 v0, p19

    iput-object v0, p0, LX/1BE;->t:LX/0qX;

    .line 212657
    move-object/from16 v0, p20

    iput-object v0, p0, LX/1BE;->u:LX/0qR;

    .line 212658
    move-object/from16 v0, p21

    iput-object v0, p0, LX/1BE;->v:LX/03V;

    .line 212659
    move-object/from16 v0, p22

    iput-object v0, p0, LX/1BE;->w:LX/1BO;

    .line 212660
    move-object/from16 v0, p23

    iput-object v0, p0, LX/1BE;->x:LX/1Bc;

    .line 212661
    invoke-virtual {p0}, LX/1BE;->a()V

    .line 212662
    return-void
.end method

.method public static a(LX/0QB;)LX/1BE;
    .locals 3

    .prologue
    .line 212663
    sget-object v0, LX/1BE;->J:LX/1BE;

    if-nez v0, :cond_1

    .line 212664
    const-class v1, LX/1BE;

    monitor-enter v1

    .line 212665
    :try_start_0
    sget-object v0, LX/1BE;->J:LX/1BE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 212666
    if-eqz v2, :cond_0

    .line 212667
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1BE;->b(LX/0QB;)LX/1BE;

    move-result-object v0

    sput-object v0, LX/1BE;->J:LX/1BE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212668
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 212669
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 212670
    :cond_1
    sget-object v0, LX/1BE;->J:LX/1BE;

    return-object v0

    .line 212671
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 212672
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;LX/1g0;)LX/1g0;
    .locals 1
    .param p3    # LX/1g0;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 212673
    if-eqz p3, :cond_0

    if-ne p2, p1, :cond_0

    .line 212674
    :goto_0
    return-object p3

    :cond_0
    iget-object v0, p0, LX/1BE;->o:LX/0pf;

    invoke-virtual {v0, p1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object p3

    goto :goto_0
.end method

.method private static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 212675
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 212676
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 212677
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v1, :cond_1

    .line 212678
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v0}, LX/39w;->a(Lcom/facebook/graphql/model/GraphQLStorySet;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p0

    .line 212679
    :cond_0
    :goto_0
    return-object p0

    .line 212680
    :cond_1
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    .line 212681
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 212682
    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v1

    .line 212683
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    .line 212684
    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    .line 212685
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->I_()I

    move-result v0

    .line 212686
    if-le v2, v0, :cond_0

    .line 212687
    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {p0, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 212688
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/1BE;Lcom/facebook/graphql/model/FeedUnit;LX/1fy;)V
    .locals 2

    .prologue
    .line 212689
    iget-object v0, p0, LX/1BE;->C:Ljava/lang/String;

    invoke-static {p1, v0}, LX/1BE;->a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212690
    invoke-static {p1}, LX/1fz;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/162;

    move-result-object v0

    .line 212691
    iget-object v1, p0, LX/1BE;->e:LX/1B5;

    invoke-virtual {v1, v0, p2}, LX/1B5;->a(LX/162;LX/1fy;)V

    .line 212692
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1g0;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Z)V
    .locals 10
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "LX/1g0;",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 212693
    if-eqz p5, :cond_5

    .line 212694
    iget-boolean v0, p2, LX/1g0;->g:Z

    move v0, v0

    .line 212695
    if-nez v0, :cond_5

    .line 212696
    const-wide/16 v7, 0xfa

    .line 212697
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 212698
    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    .line 212699
    iget-object v2, p0, LX/1BE;->o:LX/0pf;

    invoke-virtual {v2, v1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v2

    .line 212700
    iget-object v3, p0, LX/1BE;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    .line 212701
    iput-wide v3, v2, LX/1g0;->b:J

    .line 212702
    iget-boolean v2, p0, LX/1BE;->B:Z

    if-eqz v2, :cond_0

    instance-of v2, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_0

    .line 212703
    iget-object v2, p0, LX/1BE;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AkS;

    move-object v3, v1

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, v3}, LX/AkS;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 212704
    :cond_0
    iget-boolean v2, p0, LX/1BE;->A:Z

    if-eqz v2, :cond_1

    instance-of v2, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_1

    .line 212705
    iget-object v2, p0, LX/1BE;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Bd;

    invoke-virtual {v2, p1}, LX/1Bd;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 212706
    :cond_1
    invoke-static {p0}, LX/1BE;->b(LX/1BE;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p0, v1}, LX/1BE;->a(LX/1BE;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz p4, :cond_2

    .line 212707
    iget-object v2, p0, LX/1BE;->d:LX/0rW;

    iget-object v3, p0, LX/1BE;->a:Landroid/os/Handler;

    const/4 v4, 0x2

    move-object v5, p4

    move-object v6, p3

    .line 212708
    iget-boolean v9, v2, LX/0rW;->a:Z

    if-eqz v9, :cond_6

    :goto_0
    invoke-virtual {v3, v4, v6}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v9

    .line 212709
    invoke-virtual {v3, v9, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 212710
    :cond_2
    invoke-static {p0}, LX/1BE;->c(LX/1BE;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 212711
    iget-object v2, p0, LX/1BE;->a:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3, p3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 212712
    iget-object v3, p0, LX/1BE;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 212713
    :cond_3
    sget-object v2, LX/1fy;->VPVD_ELIGIBLE:LX/1fy;

    invoke-static {p0, v1, v2}, LX/1BE;->a(LX/1BE;Lcom/facebook/graphql/model/FeedUnit;LX/1fy;)V

    .line 212714
    :cond_4
    :goto_1
    iput-boolean p5, p2, LX/1g0;->g:Z

    .line 212715
    return-void

    .line 212716
    :cond_5
    if-nez p5, :cond_4

    .line 212717
    iget-boolean v0, p2, LX/1g0;->g:Z

    move v0, v0

    .line 212718
    if-eqz v0, :cond_4

    .line 212719
    invoke-direct {p0, p1, p3, p4}, LX/1BE;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    move-object v6, v5

    .line 212720
    goto :goto_0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/FeedUnit;J)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 212721
    iget-object v1, p0, LX/1BE;->e:LX/1B5;

    iget-object v6, p0, LX/1BE;->C:Ljava/lang/String;

    iget-object v7, p0, LX/1BE;->D:LX/0P1;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-virtual/range {v1 .. v7}, LX/1B5;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/FeedUnit;JLjava/lang/String;LX/0P1;)V

    .line 212722
    return-void
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;LX/1g0;)V
    .locals 10
    .param p2    # Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/FeedEdge;",
            "LX/1g0;",
            ")V"
        }
    .end annotation

    .prologue
    .line 212806
    if-eqz p1, :cond_1

    iget-object v0, p0, LX/1BE;->C:Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 212807
    const/4 v3, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v3, :pswitch_data_0

    move v1, v2

    .line 212808
    :pswitch_0
    move v0, v1

    .line 212809
    if-nez v0, :cond_2

    .line 212810
    :cond_1
    :goto_1
    return-void

    .line 212811
    :cond_2
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 212812
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 212813
    invoke-static {p2}, LX/1BE;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    .line 212814
    iget-wide v8, p3, LX/1g0;->a:J

    move-wide v2, v8

    .line 212815
    iget-object v1, p0, LX/1BE;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    .line 212816
    sub-long v2, v4, v2

    .line 212817
    sget-object v1, LX/1fy;->BEFORE_TIME_THRESHOLD:LX/1fy;

    invoke-static {p0, v0, v1}, LX/1BE;->a(LX/1BE;Lcom/facebook/graphql/model/FeedUnit;LX/1fy;)V

    .line 212818
    iget-wide v6, p0, LX/1BE;->E:J

    cmp-long v1, v2, v6

    if-ltz v1, :cond_1

    .line 212819
    iget-object v1, p0, LX/1BE;->f:LX/1BI;

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/1BI;->a(J)V

    .line 212820
    iget-object v1, p0, LX/1BE;->C:Ljava/lang/String;

    .line 212821
    const-string v2, "group_feed"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 212822
    iget-object v2, p0, LX/1BE;->I:Ljava/lang/Boolean;

    if-nez v2, :cond_3

    .line 212823
    iget-object v2, p0, LX/1BE;->n:LX/0ad;

    sget-short v3, LX/3E9;->c:S

    const/4 v1, 0x0

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, LX/1BE;->I:Ljava/lang/Boolean;

    .line 212824
    :cond_3
    iget-object v2, p0, LX/1BE;->I:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v2, v2

    .line 212825
    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :goto_2
    move v1, v2

    .line 212826
    if-eqz v1, :cond_4

    .line 212827
    iget-object v1, p0, LX/1BE;->r:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, LX/1BE;->a(Lcom/facebook/graphql/model/FeedUnit;J)V

    .line 212828
    :cond_4
    sget-object v1, LX/1fy;->BEFORE_DEDUP:LX/1fy;

    invoke-static {p0, v0, v1}, LX/1BE;->a(LX/1BE;Lcom/facebook/graphql/model/FeedUnit;LX/1fy;)V

    .line 212829
    iget-boolean v0, p0, LX/1BE;->y:Z

    if-nez v0, :cond_5

    .line 212830
    iget-boolean v0, p3, LX/1g0;->d:Z

    move v0, v0

    .line 212831
    if-nez v0, :cond_1

    .line 212832
    :cond_5
    iget-object v0, p0, LX/1BE;->e:LX/1B5;

    iget-object v1, p0, LX/1BE;->C:Ljava/lang/String;

    iget-object v2, p0, LX/1BE;->D:LX/0P1;

    invoke-virtual {v0, p1, v1, v2}, LX/1B5;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/0P1;)V

    .line 212833
    const/4 v0, 0x1

    .line 212834
    iput-boolean v0, p3, LX/1g0;->d:Z

    .line 212835
    iput-wide v4, p3, LX/1g0;->e:J

    .line 212836
    goto :goto_1

    .line 212837
    :sswitch_0
    const-string v4, "native_newsfeed"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v1

    goto/16 :goto_0

    :sswitch_1
    const-string v4, "video_home"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v2

    goto/16 :goto_0

    :sswitch_2
    const-string v4, "native_timeline"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x2

    goto/16 :goto_0

    :cond_6
    const/4 v2, 0x0

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        -0x6073d1dd -> :sswitch_1
        -0x387e25e7 -> :sswitch_0
        -0x7712097 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lcom/facebook/graphql/model/FeedUnit;J)V
    .locals 8

    .prologue
    .line 212723
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 212724
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v2

    .line 212725
    const/4 v1, 0x0

    .line 212726
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v3, "mf_story_key"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 212727
    :goto_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 212728
    iget-object v1, p0, LX/1BE;->p:LX/1BY;

    .line 212729
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 212730
    :cond_0
    :goto_1
    return-void

    .line 212731
    :catch_0
    move-exception v0

    .line 212732
    const-class v3, LX/1BE;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Unable to parse story key for group story from tracking data: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {v3, v0, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_0

    .line 212733
    :cond_1
    iget-object v2, v1, LX/1BY;->a:LX/0re;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/0re;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Lcom/facebook/graphql/model/FeedUnit;Z)V
    .locals 10
    .param p1    # Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 212734
    if-nez p2, :cond_1

    .line 212735
    :cond_0
    :goto_0
    return-void

    .line 212736
    :cond_1
    iget-object v0, p0, LX/1BE;->o:LX/0pf;

    invoke-virtual {v0, p2}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v6

    .line 212737
    iput-boolean v5, v6, LX/1g0;->f:Z

    .line 212738
    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    .line 212739
    invoke-static {v7}, LX/1BE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 212740
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 212741
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-direct {p0, v0, p2, v6}, LX/1BE;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;LX/1g0;)LX/1g0;

    move-result-object v2

    .line 212742
    invoke-static {p1}, LX/1BE;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, LX/1BE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1g0;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Z)V

    .line 212743
    if-nez p3, :cond_2

    instance-of v0, p2, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v0, :cond_2

    move-object v0, p2

    .line 212744
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-direct {p0, v0, v5}, LX/1BE;->a(Lcom/facebook/graphql/model/GraphQLStorySet;Z)V

    .line 212745
    :cond_2
    iget-wide v8, v6, LX/1g0;->a:J

    move-wide v0, v8

    .line 212746
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 212747
    sget-object v0, LX/1fy;->BEFORE_EXIT_VIEWPORT:LX/1fy;

    invoke-static {p0, p2, v0}, LX/1BE;->a(LX/1BE;Lcom/facebook/graphql/model/FeedUnit;LX/1fy;)V

    .line 212748
    invoke-direct {p0, v7, p1, v6}, LX/1BE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;LX/1g0;)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStorySet;Z)V
    .locals 9

    .prologue
    .line 212749
    iget-object v0, p0, LX/1BE;->o:LX/0pf;

    invoke-virtual {v0, p1}, LX/0pf;->a(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/1g0;

    move-result-object v6

    .line 212750
    iget-boolean v0, v6, LX/1g0;->g:Z

    move v0, v0

    .line 212751
    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    .line 212752
    iget-object v0, p0, LX/1BE;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 212753
    iput-wide v0, v6, LX/1g0;->b:J

    .line 212754
    :cond_0
    :goto_0
    iput-boolean p2, v6, LX/1g0;->g:Z

    .line 212755
    return-void

    .line 212756
    :cond_1
    iget-boolean v0, v6, LX/1g0;->g:Z

    move v0, v0

    .line 212757
    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 212758
    iget-object v0, p0, LX/1BE;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 212759
    iget-wide v7, v6, LX/1g0;->b:J

    move-wide v2, v7

    .line 212760
    sub-long v2, v0, v2

    .line 212761
    const-wide/16 v0, 0x64

    cmp-long v0, v2, v0

    if-ltz v0, :cond_0

    .line 212762
    iget-object v0, p0, LX/1BE;->e:LX/1B5;

    iget-object v4, p0, LX/1BE;->C:Ljava/lang/String;

    iget-object v5, p0, LX/1BE;->D:LX/0P1;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/1B5;->a(Lcom/facebook/graphql/model/GraphQLStorySet;JLjava/lang/String;LX/0P1;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/Object;Z)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 212763
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/1BE;->a(Ljava/lang/Object;ZZ)V

    .line 212764
    return-void
.end method

.method private a(Ljava/lang/Object;ZZ)V
    .locals 6

    .prologue
    .line 212765
    invoke-static {p1}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    .line 212766
    if-nez v3, :cond_1

    .line 212767
    :cond_0
    :goto_0
    return-void

    .line 212768
    :cond_1
    invoke-static {v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 212769
    invoke-static {v0}, LX/1BE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 212770
    iget-object v2, p0, LX/1BE;->o:LX/0pf;

    .line 212771
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 212772
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v2, v0}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v2

    .line 212773
    invoke-static {p1}, LX/1fr;->b(Ljava/lang/Object;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    .line 212774
    invoke-static {v0}, LX/1BE;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move v5, p2

    invoke-direct/range {v0 .. v5}, LX/1BE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1g0;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Z)V

    .line 212775
    if-nez p3, :cond_0

    instance-of v0, v3, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v0, :cond_0

    .line 212776
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-direct {p0, v3, p2}, LX/1BE;->a(Lcom/facebook/graphql/model/GraphQLStorySet;Z)V

    goto :goto_0
.end method

.method public static a(LX/1BE;Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 212777
    iget-object v0, p0, LX/1BE;->C:Ljava/lang/String;

    invoke-static {p1, v0}, LX/1BE;->a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 212778
    if-eqz p0, :cond_0

    const-string v0, "native_newsfeed"

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/1BE;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;J)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0xfa

    .line 212779
    const-string v0, "ViewportLoggingHandler.logFeedUnitDuration"

    const v1, 0xb0d3b90

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 212780
    if-eqz p1, :cond_4

    .line 212781
    :try_start_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 212782
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 212783
    :goto_0
    iget-object v1, p0, LX/1BE;->o:LX/0pf;

    invoke-virtual {v1, v0}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v1

    .line 212784
    invoke-virtual {v1}, LX/1g0;->b()J

    move-result-wide v2

    .line 212785
    sub-long v4, p4, v2

    .line 212786
    const-wide/16 v6, 0x64

    cmp-long v1, v4, v6

    if-ltz v1, :cond_0

    .line 212787
    invoke-direct {p0, p1, p2, v4, v5}, LX/1BE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/FeedUnit;J)V

    .line 212788
    :cond_0
    cmp-long v1, v4, v8

    if-ltz v1, :cond_1

    invoke-static {p0, v0}, LX/1BE;->a(LX/1BE;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, LX/1BE;->b(LX/1BE;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 212789
    iget-object v1, p0, LX/1BE;->d:LX/0rW;

    invoke-virtual {v1, p3, v0, p4, p5}, LX/0rW;->a(Ljava/lang/String;Lcom/facebook/graphql/model/FeedUnit;J)V

    .line 212790
    :cond_1
    invoke-static {p0}, LX/1BE;->c(LX/1BE;)Z

    move-result v0

    if-nez v0, :cond_2

    cmp-long v0, v4, v8

    if-ltz v0, :cond_2

    .line 212791
    invoke-static {p0, p2}, LX/1BE;->b(LX/1BE;Lcom/facebook/graphql/model/FeedUnit;)V

    :cond_2
    move-object v0, p0

    move-object v1, p2

    move-wide v4, p4

    .line 212792
    iget-object v6, v0, LX/1BE;->w:LX/1BO;

    invoke-virtual {v6}, LX/1BO;->a()Z

    move-result v6

    if-nez v6, :cond_5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212793
    :cond_3
    :goto_1
    const v0, -0x6dc5522e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 212794
    return-void

    .line 212795
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 212796
    :catchall_0
    move-exception v0

    const v1, -0x52729783

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 212797
    :cond_5
    :try_start_1
    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v7

    .line 212798
    if-eqz v7, :cond_3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212799
    :try_start_2
    iget-object v6, v0, LX/1BE;->l:LX/1BK;

    invoke-virtual {v6, v7}, LX/1BL;->c(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v6

    .line 212800
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_6
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1fG;

    .line 212801
    iget-object v9, v6, LX/1fG;->h:LX/2xm;

    move-object v6, v9

    .line 212802
    if-eqz v6, :cond_6

    .line 212803
    invoke-virtual {v6, v2, v3, v4, v5}, LX/2xm;->a(JJ)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 212804
    :catch_0
    move-exception v6

    .line 212805
    iget-object v8, v0, LX/1BE;->v:LX/03V;

    const-string v9, "fresco ppr logging failed for feed unit"

    const/4 p0, 0x0

    const-string p1, "cacheId=%s e=%s"

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p3, 0x0

    aput-object v7, p2, p3

    const/4 v7, 0x1

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p3

    aput-object p3, p2, v7

    invoke-static {p0, p1, p2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v9, v7, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private static b(LX/0QB;)LX/1BE;
    .locals 25

    .prologue
    .line 212598
    new-instance v1, LX/1BE;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/1BF;->a(LX/0QB;)LX/1BF;

    move-result-object v3

    check-cast v3, LX/1BF;

    invoke-static/range {p0 .. p0}, LX/0rW;->a(LX/0QB;)LX/0rW;

    move-result-object v4

    check-cast v4, LX/0rW;

    invoke-static/range {p0 .. p0}, LX/1B5;->a(LX/0QB;)LX/1B5;

    move-result-object v5

    check-cast v5, LX/1B5;

    invoke-static/range {p0 .. p0}, LX/1BI;->a(LX/0QB;)LX/1BI;

    move-result-object v6

    check-cast v6, LX/1BI;

    invoke-static/range {p0 .. p0}, LX/1BJ;->a(LX/0QB;)LX/1BJ;

    move-result-object v7

    check-cast v7, LX/1BJ;

    const/16 v8, 0x675

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1ca3

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x1493

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x1497

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/1BK;->a(LX/0QB;)LX/1BK;

    move-result-object v12

    check-cast v12, LX/1BK;

    invoke-static/range {p0 .. p0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v13

    check-cast v13, LX/0pf;

    invoke-static/range {p0 .. p0}, LX/1BX;->a(LX/0QB;)LX/1BX;

    move-result-object v14

    check-cast v14, LX/1BX;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v15

    check-cast v15, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/1BY;->a(LX/0QB;)LX/1BY;

    move-result-object v16

    check-cast v16, LX/1BY;

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v17

    check-cast v17, LX/0So;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v18

    check-cast v18, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0y5;->a(LX/0QB;)LX/0y5;

    move-result-object v19

    check-cast v19, LX/0y5;

    invoke-static/range {p0 .. p0}, LX/0qX;->a(LX/0QB;)LX/0qX;

    move-result-object v20

    check-cast v20, LX/0qX;

    invoke-static/range {p0 .. p0}, LX/0qR;->a(LX/0QB;)LX/0qR;

    move-result-object v21

    check-cast v21, LX/0qR;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v22

    check-cast v22, LX/03V;

    invoke-static/range {p0 .. p0}, LX/1BO;->a(LX/0QB;)LX/1BO;

    move-result-object v23

    check-cast v23, LX/1BO;

    invoke-static/range {p0 .. p0}, LX/1BZ;->a(LX/0QB;)LX/1Bc;

    move-result-object v24

    check-cast v24, LX/1Bc;

    invoke-direct/range {v1 .. v24}, LX/1BE;-><init>(LX/0SG;LX/1BF;LX/0rW;LX/1B5;LX/1BI;LX/1BJ;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/1BK;LX/0pf;LX/1BX;LX/0ad;LX/1BY;LX/0So;Ljava/util/concurrent/ExecutorService;LX/0y5;LX/0qX;LX/0qR;LX/03V;LX/1BO;LX/1Bc;)V

    .line 212599
    return-object v1
.end method

.method public static b(LX/1BE;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 3

    .prologue
    .line 212468
    iget-boolean v0, p0, LX/1BE;->z:Z

    if-nez v0, :cond_1

    .line 212469
    :cond_0
    :goto_0
    return-void

    .line 212470
    :cond_1
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 212471
    iget-object v0, p0, LX/1BE;->t:LX/0qX;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0qX;->d(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 212472
    invoke-direct {p0, p1}, LX/1BE;->c(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 212473
    :cond_2
    iget-object v0, p0, LX/1BE;->c:LX/1BF;

    iget-object v1, p0, LX/1BE;->l:LX/1BK;

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1BL;->a(Ljava/lang/String;)I

    move-result v1

    .line 212474
    iget-object v2, v0, LX/1BF;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1fI;

    .line 212475
    const/4 v0, 0x1

    invoke-static {v2, p1, v0, v1}, LX/1fI;->a(LX/1fI;Lcom/facebook/graphql/model/FeedUnit;II)V

    .line 212476
    goto :goto_1

    .line 212477
    :cond_3
    goto :goto_0
.end method

.method private b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 212600
    if-eqz p1, :cond_5

    .line 212601
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 212602
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    move-object v2, v0

    .line 212603
    :goto_0
    iget-object v4, p0, LX/1BE;->b:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v10

    .line 212604
    iget-object v4, p0, LX/1BE;->H:Ljava/lang/Boolean;

    if-nez v4, :cond_0

    .line 212605
    iget-object v4, p0, LX/1BE;->n:LX/0ad;

    sget-short v5, LX/0fe;->bO:S

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, LX/1BE;->H:Ljava/lang/Boolean;

    .line 212606
    :cond_0
    iget-object v4, p0, LX/1BE;->H:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    move v4, v4

    .line 212607
    if-eqz v4, :cond_6

    .line 212608
    iget-object v4, p0, LX/1BE;->q:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/facebook/feed/logging/ViewportLoggingHandler$1;

    move-object v6, p0

    move-object v7, p1

    move-object v8, p2

    move-object v9, p3

    invoke-direct/range {v5 .. v11}, Lcom/facebook/feed/logging/ViewportLoggingHandler$1;-><init>(LX/1BE;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;J)V

    const v6, 0x41568a36

    invoke-static {v4, v5, v6}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 212609
    :goto_1
    iget-boolean v0, p0, LX/1BE;->A:Z

    if-eqz v0, :cond_1

    instance-of v0, v2, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    .line 212610
    iget-object v0, p0, LX/1BE;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bd;

    move-object v1, v2

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 212611
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    .line 212612
    iget-object v3, v0, LX/1Bd;->b:Ljava/util/HashMap;

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Runnable;

    .line 212613
    if-eqz v3, :cond_1

    .line 212614
    iget-object v5, v0, LX/1Bd;->b:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 212615
    iget-object v4, v0, LX/1Bd;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Be;

    .line 212616
    iget-object v5, v4, LX/1Be;->j:Landroid/os/Handler;

    move-object v4, v5

    .line 212617
    invoke-static {v4, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 212618
    :cond_1
    iget-boolean v0, p0, LX/1BE;->B:Z

    if-eqz v0, :cond_2

    instance-of v0, v2, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_2

    .line 212619
    iget-object v0, p0, LX/1BE;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AkS;

    move-object v1, v2

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 212620
    iget-object v3, v0, LX/AkS;->f:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Runnable;

    .line 212621
    iget-object v4, v0, LX/AkS;->d:Landroid/os/Handler;

    invoke-static {v4, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 212622
    :cond_2
    invoke-static {p0}, LX/1BE;->b(LX/1BE;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 212623
    iget-object v0, p0, LX/1BE;->d:LX/0rW;

    iget-object v1, p0, LX/1BE;->a:Landroid/os/Handler;

    const/4 v3, 0x2

    .line 212624
    iget-boolean v4, v0, LX/0rW;->a:Z

    if-eqz v4, :cond_7

    instance-of v4, p2, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v4, :cond_7

    .line 212625
    invoke-virtual {v1, v3, p2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 212626
    :cond_3
    :goto_2
    invoke-static {p0}, LX/1BE;->c(LX/1BE;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 212627
    iget-object v0, p0, LX/1BE;->a:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 212628
    :cond_4
    sget-object v0, LX/1fy;->VPVD_INELIGIBLE:LX/1fy;

    invoke-static {p0, v2, v0}, LX/1BE;->a(LX/1BE;Lcom/facebook/graphql/model/FeedUnit;LX/1fy;)V

    .line 212629
    return-void

    .line 212630
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_6
    move-object v6, p0

    move-object v7, p1

    move-object v8, p2

    move-object v9, p3

    .line 212631
    invoke-static/range {v6 .. v11}, LX/1BE;->a$redex0(LX/1BE;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;J)V

    goto/16 :goto_1

    .line 212632
    :cond_7
    if-eqz p3, :cond_3

    .line 212633
    invoke-virtual {v1, v3, p3}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    goto :goto_2
.end method

.method public static b(LX/1BE;)Z
    .locals 3

    .prologue
    .line 212478
    iget-object v0, p0, LX/1BE;->F:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 212479
    iget-object v0, p0, LX/1BE;->n:LX/0ad;

    sget-short v1, LX/0fe;->bR:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1BE;->F:Ljava/lang/Boolean;

    .line 212480
    :cond_0
    iget-object v0, p0, LX/1BE;->F:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private c(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 4

    .prologue
    .line 212481
    iget-object v0, p0, LX/1BE;->s:LX/0y5;

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0y5;->a(Ljava/lang/String;)LX/14s;

    move-result-object v0

    .line 212482
    if-nez v0, :cond_1

    .line 212483
    :cond_0
    :goto_0
    return-void

    .line 212484
    :cond_1
    iget v1, v0, LX/14s;->mSeenState:I

    move v1, v1

    .line 212485
    if-nez v1, :cond_0

    .line 212486
    iget v1, v0, LX/14s;->mVideoCacheState:I

    move v1, v1

    .line 212487
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 212488
    iget-boolean v1, v0, LX/14s;->mStoryHasVideo:Z

    move v0, v1

    .line 212489
    if-eqz v0, :cond_0

    .line 212490
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 212491
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/14w;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 212492
    if-eqz v0, :cond_0

    .line 212493
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 212494
    iget-object v1, p0, LX/1BE;->g:LX/1BJ;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/1BE;->u:LX/0qR;

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0qR;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 212495
    iget-object v3, v1, LX/1BJ;->a:LX/0Zb;

    const-string p0, "android_bumped_downloaded_video_stories"

    const/4 p1, 0x0

    invoke-interface {v3, p0, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 212496
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result p0

    if-eqz p0, :cond_2

    .line 212497
    const-string p0, "video_id"

    invoke-virtual {v3, p0, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 212498
    const-string p0, "dedup_key"

    invoke-virtual {v3, p0, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 212499
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 212500
    :cond_2
    goto :goto_0
.end method

.method public static c(LX/1BE;)Z
    .locals 3

    .prologue
    .line 212501
    iget-object v0, p0, LX/1BE;->G:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 212502
    iget-object v0, p0, LX/1BE;->n:LX/0ad;

    sget-short v1, LX/0fe;->bS:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1BE;->G:Ljava/lang/Boolean;

    .line 212503
    :cond_0
    iget-object v0, p0, LX/1BE;->G:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private e(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 212504
    invoke-static {p1}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 212505
    if-nez v0, :cond_0

    .line 212506
    :goto_0
    return-void

    .line 212507
    :cond_0
    sget-object v1, LX/1fy;->AFTER_ENTER_VIEWPORT:LX/1fy;

    invoke-static {p0, v0, v1}, LX/1BE;->a(LX/1BE;Lcom/facebook/graphql/model/FeedUnit;LX/1fy;)V

    .line 212508
    iget-object v1, p0, LX/1BE;->o:LX/0pf;

    invoke-virtual {v1, v0}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v1

    .line 212509
    const/4 v2, 0x1

    .line 212510
    iput-boolean v2, v1, LX/1g0;->f:Z

    .line 212511
    iget-object v2, p0, LX/1BE;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 212512
    iput-wide v2, v1, LX/1g0;->a:J

    .line 212513
    iget-object v1, p0, LX/1BE;->e:LX/1B5;

    .line 212514
    invoke-static {v0}, LX/1fz;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/162;

    move-result-object v2

    .line 212515
    new-instance v3, LX/1g1;

    sget-object v4, LX/1g2;->NON_SPONSORED_IMPRESSION:LX/1g2;

    invoke-direct {v3, v4, v0, v2}, LX/1g1;-><init>(LX/1g2;Ljava/lang/Object;LX/162;)V

    invoke-static {v1, v3}, LX/1B5;->a(LX/1B5;LX/1g1;)V

    .line 212516
    iget-wide v0, p0, LX/1BE;->E:J

    .line 212517
    iget-object v2, p0, LX/1BE;->a:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 212518
    iget-object v3, p0, LX/1BE;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2, v0, v1}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 212519
    goto :goto_0
.end method

.method private f(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 212520
    iget-object v0, p0, LX/1BE;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 212521
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 212522
    iget-object v0, p0, LX/1BE;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/1BE;->y:Z

    .line 212523
    iget-object v0, p0, LX/1BE;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/1BE;->z:Z

    .line 212524
    iget-object v0, p0, LX/1BE;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bd;

    .line 212525
    iget-object v1, v0, LX/1Bd;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Be;

    .line 212526
    invoke-virtual {v1}, LX/1Be;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, LX/1Be;->e()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 212527
    iput-boolean v0, p0, LX/1BE;->A:Z

    .line 212528
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1BE;->B:Z

    .line 212529
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(LX/01J;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/01J",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v5, 0x0

    .line 212530
    invoke-virtual {p1}, LX/01J;->size()I

    move-result v13

    move v12, v5

    :goto_0
    if-ge v12, v13, :cond_1

    .line 212531
    invoke-virtual {p1, v12}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 212532
    invoke-static {v0}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    .line 212533
    invoke-static {v0}, LX/1fr;->b(Ljava/lang/Object;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v4

    .line 212534
    if-eqz v3, :cond_0

    .line 212535
    invoke-static {v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 212536
    invoke-static {v0}, LX/1BE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 212537
    iget-object v2, p0, LX/1BE;->o:LX/0pf;

    .line 212538
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 212539
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v2, v0}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v2

    .line 212540
    invoke-static {v4}, LX/1BE;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    .line 212541
    invoke-direct/range {v0 .. v5}, LX/1BE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1g0;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Z)V

    move-object v6, p0

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    .line 212542
    invoke-direct/range {v6 .. v11}, LX/1BE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1g0;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Z)V

    .line 212543
    instance-of v0, v3, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v0, :cond_0

    move-object v0, v3

    .line 212544
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-direct {p0, v0, v5}, LX/1BE;->a(Lcom/facebook/graphql/model/GraphQLStorySet;Z)V

    .line 212545
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-direct {p0, v3, v11}, LX/1BE;->a(Lcom/facebook/graphql/model/GraphQLStorySet;Z)V

    .line 212546
    :cond_0
    add-int/lit8 v0, v12, 0x1

    move v12, v0

    goto :goto_0

    .line 212547
    :cond_1
    return-void
.end method

.method public final a(LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 212548
    iput-object p1, p0, LX/1BE;->D:LX/0P1;

    .line 212549
    return-void
.end method

.method public final a(LX/0g8;II)V
    .locals 0

    .prologue
    .line 212550
    return-void
.end method

.method public final a(LX/0g8;Ljava/lang/Object;I)V
    .locals 3

    .prologue
    .line 212551
    invoke-static {p1}, LX/1BX;->b(LX/0g8;)LX/1Qr;

    move-result-object v0

    .line 212552
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/1Qr;->d()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, LX/0g8;->B()Z

    move-result v1

    if-nez v1, :cond_1

    .line 212553
    :cond_0
    :goto_0
    return-void

    .line 212554
    :cond_1
    invoke-static {p2}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 212555
    if-eqz v1, :cond_0

    .line 212556
    invoke-interface {p1, p3}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v1

    .line 212557
    iget-object v2, p0, LX/1BE;->m:LX/1BX;

    invoke-virtual {v2, v0, p1, v1}, LX/1BX;->a(LX/1Qr;LX/0g8;Landroid/view/View;)Z

    move-result v0

    .line 212558
    invoke-direct {p0, p2, v0}, LX/1BE;->a(Ljava/lang/Object;Z)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 212559
    invoke-static {p1}, LX/1fr;->b(Ljava/lang/Object;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    .line 212560
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v2}, LX/1BE;->a(Ljava/lang/Object;ZZ)V

    .line 212561
    invoke-direct {p0, v0, p1, v2}, LX/1BE;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Lcom/facebook/graphql/model/FeedUnit;Z)V

    .line 212562
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 212563
    iput-object p1, p0, LX/1BE;->C:Ljava/lang/String;

    .line 212564
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 212565
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 212566
    invoke-direct {p0, p1}, LX/1BE;->f(Ljava/lang/Object;)V

    .line 212567
    invoke-direct {p0, p1}, LX/1BE;->e(Ljava/lang/Object;)V

    .line 212568
    invoke-direct {p0, p1, v0, v0}, LX/1BE;->a(Ljava/lang/Object;ZZ)V

    .line 212569
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 212570
    invoke-direct {p0, p1}, LX/1BE;->e(Ljava/lang/Object;)V

    .line 212571
    return-void
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 212572
    invoke-static {p1}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 212573
    invoke-static {p1}, LX/1fr;->b(Ljava/lang/Object;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v1

    .line 212574
    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, LX/1BE;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Lcom/facebook/graphql/model/FeedUnit;Z)V

    .line 212575
    invoke-direct {p0, p1}, LX/1BE;->f(Ljava/lang/Object;)V

    .line 212576
    return-void
.end method

.method public final d(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 212577
    const/4 v0, 0x1

    return v0
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 10

    .prologue
    .line 212578
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 212579
    iget-object v0, p0, LX/1BE;->d:LX/0rW;

    const/4 v6, 0x0

    .line 212580
    iget-boolean v4, v0, LX/0rW;->a:Z

    if-eqz v4, :cond_4

    move-object v5, v6

    :goto_0
    iget-boolean v4, v0, LX/0rW;->a:Z

    if-eqz v4, :cond_0

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Lcom/facebook/graphql/model/FeedUnit;

    move-object v6, v4

    :cond_0
    iget-object v4, v0, LX/0rW;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v8

    invoke-virtual {v0, v5, v6, v8, v9}, LX/0rW;->a(Ljava/lang/String;Lcom/facebook/graphql/model/FeedUnit;J)V

    .line 212581
    const/4 v4, 0x1

    move v0, v4

    .line 212582
    :goto_1
    return v0

    .line 212583
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 212584
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {p0, v0}, LX/1BE;->b(LX/1BE;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 212585
    const/4 v0, 0x1

    move v0, v0

    .line 212586
    goto :goto_1

    .line 212587
    :cond_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 212588
    invoke-static {v0}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 212589
    invoke-static {v0}, LX/1fr;->b(Ljava/lang/Object;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    .line 212590
    iget-object v2, p0, LX/1BE;->o:LX/0pf;

    invoke-virtual {v2, v1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v2

    .line 212591
    iget-boolean v3, v2, LX/1g0;->f:Z

    move v3, v3

    .line 212592
    if-eqz v3, :cond_3

    .line 212593
    sget-object v3, LX/1fy;->IN_VIEWPORT_FOR_MORE_THAN_VPV_DURATION:LX/1fy;

    invoke-static {p0, v1, v3}, LX/1BE;->a(LX/1BE;Lcom/facebook/graphql/model/FeedUnit;LX/1fy;)V

    .line 212594
    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-direct {p0, v3, v0, v2}, LX/1BE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;LX/1g0;)V

    .line 212595
    iget-object v0, p0, LX/1BE;->x:LX/1Bc;

    invoke-interface {v0, v1}, LX/1Bc;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 212596
    :cond_3
    const/4 v0, 0x1

    goto :goto_1

    .line 212597
    :cond_4
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    move-object v5, v4

    goto :goto_0
.end method
