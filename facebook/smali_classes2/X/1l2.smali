.class public final LX/1l2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Executor;


# instance fields
.field public volatile a:Z

.field public final synthetic b:Ljava/util/concurrent/Executor;

.field public final synthetic c:LX/0SQ;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/0SQ;)V
    .locals 1

    .prologue
    .line 310878
    iput-object p1, p0, LX/1l2;->b:Ljava/util/concurrent/Executor;

    iput-object p2, p0, LX/1l2;->c:LX/0SQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310879
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1l2;->a:Z

    return-void
.end method


# virtual methods
.method public final execute(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 310880
    :try_start_0
    iget-object v0, p0, LX/1l2;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/common/util/concurrent/Futures$2$1;

    invoke-direct {v1, p0, p1}, Lcom/google/common/util/concurrent/Futures$2$1;-><init>(LX/1l2;Ljava/lang/Runnable;)V

    const v2, 0xbbaf86a

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 310881
    :cond_0
    :goto_0
    return-void

    .line 310882
    :catch_0
    move-exception v0

    .line 310883
    iget-boolean v1, p0, LX/1l2;->a:Z

    if-eqz v1, :cond_0

    .line 310884
    iget-object v1, p0, LX/1l2;->c:LX/0SQ;

    invoke-virtual {v1, v0}, LX/0SQ;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
