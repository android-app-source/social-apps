.class public LX/0qP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/0qP;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile d:LX/0qP;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0v1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 147444
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, LX/0qQ;->c:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/0qQ;->b:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/0qQ;->d:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, LX/0qP;->a:[Ljava/lang/String;

    .line 147445
    const-class v0, LX/0qP;

    sput-object v0, LX/0qP;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0v1;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 147446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147447
    iput-object p1, p0, LX/0qP;->b:LX/0Ot;

    .line 147448
    return-void
.end method

.method public static a(LX/0QB;)LX/0qP;
    .locals 4

    .prologue
    .line 147449
    sget-object v0, LX/0qP;->d:LX/0qP;

    if-nez v0, :cond_1

    .line 147450
    const-class v1, LX/0qP;

    monitor-enter v1

    .line 147451
    :try_start_0
    sget-object v0, LX/0qP;->d:LX/0qP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 147452
    if-eqz v2, :cond_0

    .line 147453
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 147454
    new-instance v3, LX/0qP;

    const/16 p0, 0xea

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/0qP;-><init>(LX/0Ot;)V

    .line 147455
    move-object v0, v3

    .line 147456
    sput-object v0, LX/0qP;->d:LX/0qP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147457
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 147458
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 147459
    :cond_1
    sget-object v0, LX/0qP;->d:LX/0qP;

    return-object v0

    .line 147460
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 147461
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Px;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 147462
    if-nez p1, :cond_0

    .line 147463
    sget-object v0, LX/0qP;->c:Ljava/lang/Class;

    const-string v2, "Inconsistent input for deleteStories!"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    move v0, v1

    .line 147464
    :goto_0
    return v0

    .line 147465
    :cond_0
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 147466
    sget-object v0, LX/0qP;->c:Ljava/lang/Class;

    const-string v2, "No keys to delete"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    move v0, v1

    .line 147467
    goto :goto_0

    .line 147468
    :cond_1
    const/4 v2, 0x0

    .line 147469
    :try_start_0
    iget-object v0, p0, LX/0qP;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 147470
    const v0, -0x4da4ec08

    invoke-static {v2, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 147471
    invoke-static {}, LX/0uu;->b()LX/0uw;

    move-result-object v4

    .line 147472
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v3, v1

    :goto_1
    if-ge v3, v5, :cond_2

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 147473
    sget-object v6, LX/0qQ;->a:LX/0U1;

    invoke-virtual {v6, v0}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147474
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 147475
    :cond_2
    :try_start_1
    const-string v0, "home_stories_media"

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 147476
    :goto_2
    :try_start_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 147477
    const v1, -0x61e2ad08

    :try_start_3
    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 147478
    :catch_0
    move-exception v1

    .line 147479
    sget-object v2, LX/0qP;->c:Ljava/lang/Class;

    const-string v3, "Failed to close the connection to the DB!"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 147480
    :catch_1
    move-exception v0

    .line 147481
    :try_start_4
    sget-object v3, LX/0qP;->c:Ljava/lang/Class;

    const-string v4, "One Delete operation failed!"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v0, v1

    goto :goto_2

    .line 147482
    :catch_2
    move-exception v0

    move-object v7, v0

    move v0, v1

    move-object v1, v7

    .line 147483
    :goto_3
    :try_start_5
    sget-object v3, LX/0qP;->c:Ljava/lang/Class;

    const-string v4, "Delete Stories failed!"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 147484
    const v1, -0x5fb555d

    :try_start_6
    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_0

    .line 147485
    :catch_3
    move-exception v1

    .line 147486
    sget-object v2, LX/0qP;->c:Ljava/lang/Class;

    const-string v3, "Failed to close the connection to the DB!"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 147487
    :catchall_0
    move-exception v0

    .line 147488
    const v1, -0x3467d3ab    # -1.9945642E7f

    :try_start_7
    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    .line 147489
    :goto_4
    throw v0

    .line 147490
    :catch_4
    move-exception v1

    .line 147491
    sget-object v2, LX/0qP;->c:Ljava/lang/Class;

    const-string v3, "Failed to close the connection to the DB!"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 147492
    :catch_5
    move-exception v1

    goto :goto_3
.end method

.method public final a(Ljava/lang/String;)LX/14t;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 147493
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 147494
    const-string v1, "home_stories_media"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 147495
    sget-object v1, LX/0qQ;->a:LX/0U1;

    .line 147496
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 147497
    invoke-static {v1, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 147498
    iget-object v1, p0, LX/0qP;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0v1;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, LX/0qP;->a:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 147499
    sget-object v0, LX/0qQ;->d:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 147500
    sget-object v2, LX/0qQ;->c:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 147501
    sget-object v3, LX/0qQ;->b:LX/0U1;

    invoke-virtual {v3, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 147502
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 147503
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 147504
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 147505
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 147506
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 147507
    new-instance v8, LX/16v;

    invoke-direct {v8, v7, v5, v6}, LX/16v;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 147508
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 147509
    new-instance v0, LX/14t;

    invoke-direct {v0, v4}, LX/14t;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 147510
    iget-object v0, p0, LX/0qP;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 147511
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 147512
    sget-object v2, LX/0qQ;->b:LX/0U1;

    .line 147513
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 147514
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 147515
    const/4 v2, 0x2

    new-array v2, v2, [LX/0ux;

    const/4 v3, 0x0

    sget-object v4, LX/0qQ;->d:LX/0U1;

    invoke-virtual {v4, p2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, LX/0qQ;->c:LX/0U1;

    invoke-virtual {v4, p1}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v2

    .line 147516
    const-string v3, "home_stories_media"

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v1, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 147517
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 147518
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "dedup key is null"

    invoke-static {v0, v3}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 147519
    if-eqz p2, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "media id is null"

    invoke-static {v0, v3}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 147520
    if-eqz p3, :cond_2

    :goto_2
    const-string v0, "type is null"

    invoke-static {v1, v0}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 147521
    iget-object v0, p0, LX/0qP;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 147522
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 147523
    const-string v2, "home_stories_media"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 147524
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 147525
    sget-object v2, LX/0qQ;->a:LX/0U1;

    .line 147526
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 147527
    invoke-virtual {v0, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 147528
    sget-object v2, LX/0qQ;->c:LX/0U1;

    .line 147529
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 147530
    invoke-virtual {v0, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 147531
    sget-object v2, LX/0qQ;->d:LX/0U1;

    .line 147532
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 147533
    invoke-virtual {v0, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 147534
    sget-object v2, LX/0qQ;->b:LX/0U1;

    .line 147535
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 147536
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 147537
    const v2, 0x862c6ce

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 147538
    :try_start_0
    const-string v2, "home_stories_media"

    const-string v3, ""

    const/4 v4, 0x5

    const v5, -0x1abc6cf6

    invoke-static {v5}, LX/03h;->a(I)V

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    const v0, 0xad9aa4b

    invoke-static {v0}, LX/03h;->a(I)V

    .line 147539
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147540
    const v0, 0x28b62a41

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 147541
    return-void

    :cond_0
    move v0, v2

    .line 147542
    goto :goto_0

    :cond_1
    move v0, v2

    .line 147543
    goto :goto_1

    :cond_2
    move v1, v2

    .line 147544
    goto :goto_2

    .line 147545
    :catchall_0
    move-exception v0

    const v2, 0x2c75ecee

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 147546
    const/4 v0, 0x1

    new-array v0, v0, [LX/0ux;

    const/4 v1, 0x0

    sget-object v2, LX/0qQ;->a:LX/0U1;

    invoke-virtual {v2, p1}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v2

    .line 147547
    const/4 v1, 0x0

    .line 147548
    :try_start_0
    iget-object v0, p0, LX/0qP;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 147549
    const v0, 0x5b947cff

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 147550
    const-string v0, "home_stories_media"

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 147551
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147552
    const v0, -0x74a06390

    :try_start_1
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 147553
    :goto_0
    return-void

    .line 147554
    :catch_0
    move-exception v0

    .line 147555
    sget-object v1, LX/0qP;->c:Ljava/lang/Class;

    const-string v2, "Failed to close the connection to the DB!"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 147556
    :catch_1
    move-exception v0

    .line 147557
    :try_start_2
    sget-object v2, LX/0qP;->c:Ljava/lang/Class;

    const-string v3, "Delete story failed!"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 147558
    const v0, -0x324049da

    :try_start_3
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 147559
    :catch_2
    move-exception v0

    .line 147560
    sget-object v1, LX/0qP;->c:Ljava/lang/Class;

    const-string v2, "Failed to close the connection to the DB!"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 147561
    :catchall_0
    move-exception v0

    .line 147562
    const v2, -0x245d02cf

    :try_start_4
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 147563
    :goto_1
    throw v0

    .line 147564
    :catch_3
    move-exception v1

    .line 147565
    sget-object v2, LX/0qP;->c:Ljava/lang/Class;

    const-string v3, "Failed to close the connection to the DB!"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
