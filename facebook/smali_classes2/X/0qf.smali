.class public LX/0qf;
.super LX/0qg;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:LX/0Tn;

.field private static final g:Ljava/lang/Object;


# instance fields
.field public final b:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "LX/0qi;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0qi;

.field public d:Z

.field private e:LX/0qi;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 148273
    sget-object v0, LX/0qh;->a:LX/0Tn;

    const-string v1, "feedClashUnitCachedID"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0qf;->a:LX/0Tn;

    .line 148274
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/0qf;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 148174
    invoke-direct {p0}, LX/0qg;-><init>()V

    .line 148175
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/0qf;->b:Lcom/google/common/util/concurrent/SettableFuture;

    .line 148176
    invoke-static {v1}, LX/0qi;->a(Z)LX/0qi;

    move-result-object v0

    iput-object v0, p0, LX/0qf;->c:LX/0qi;

    .line 148177
    iput-boolean v1, p0, LX/0qf;->d:Z

    .line 148178
    const/4 v0, 0x0

    iput-object v0, p0, LX/0qf;->e:LX/0qi;

    .line 148179
    iput-object p1, p0, LX/0qf;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 148180
    return-void
.end method

.method public static a(LX/0QB;)LX/0qf;
    .locals 7

    .prologue
    .line 148244
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 148245
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 148246
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 148247
    if-nez v1, :cond_0

    .line 148248
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148249
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 148250
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 148251
    sget-object v1, LX/0qf;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 148252
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 148253
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 148254
    :cond_1
    if-nez v1, :cond_4

    .line 148255
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 148256
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 148257
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 148258
    new-instance p0, LX/0qf;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v1}, LX/0qf;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 148259
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 148260
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 148261
    if-nez v1, :cond_2

    .line 148262
    sget-object v0, LX/0qf;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qf;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 148263
    :goto_1
    if-eqz v0, :cond_3

    .line 148264
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 148265
    :goto_3
    check-cast v0, LX/0qf;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 148266
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 148267
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 148268
    :catchall_1
    move-exception v0

    .line 148269
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 148270
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 148271
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 148272
    :cond_2
    :try_start_8
    sget-object v0, LX/0qf;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qf;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(LX/0qi;)V
    .locals 2

    .prologue
    .line 148240
    iget-boolean v0, p0, LX/0qf;->d:Z

    if-nez v0, :cond_0

    .line 148241
    :goto_0
    return-void

    .line 148242
    :cond_0
    iget-object v0, p0, LX/0qf;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, -0x3d49ea4a

    invoke-static {v0, p1, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 148243
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0qf;->d:Z

    goto :goto_0
.end method

.method private a(LX/0qi;Lcom/facebook/api/feed/FetchFeedResult;)V
    .locals 3

    .prologue
    .line 148229
    invoke-virtual {p2}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 148230
    :cond_0
    :goto_0
    return-void

    .line 148231
    :cond_1
    iget-object v0, p0, LX/0qf;->e:LX/0qi;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0qf;->e:LX/0qi;

    invoke-virtual {v0, p1}, LX/0qi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 148232
    :cond_2
    iput-object p1, p0, LX/0qf;->e:LX/0qi;

    .line 148233
    iget-object v0, p0, LX/0qf;->e:LX/0qi;

    .line 148234
    iget-object v1, v0, LX/0qi;->b:Ljava/lang/String;

    move-object v0, v1

    .line 148235
    if-nez v0, :cond_3

    const-string v0, ""

    .line 148236
    :goto_1
    iget-object v1, p0, LX/0qf;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0qf;->a:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0

    .line 148237
    :cond_3
    iget-object v0, p0, LX/0qf;->e:LX/0qi;

    .line 148238
    iget-object v1, v0, LX/0qi;->b:Ljava/lang/String;

    move-object v0, v1

    .line 148239
    goto :goto_1
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 148275
    iget-boolean v0, p0, LX/0qf;->d:Z

    if-nez v0, :cond_0

    .line 148276
    :goto_0
    return-void

    .line 148277
    :cond_0
    iget-object v0, p0, LX/0qf;->b:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 148278
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0qf;->d:Z

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0rH;)LX/0qi;
    .locals 1

    .prologue
    .line 148228
    iget-object v0, p0, LX/0qf;->c:LX/0qi;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148227
    const-string v0, "feed"

    return-object v0
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedResult;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 148187
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 148188
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v5, v0

    .line 148189
    iget-object v0, v5, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 148190
    if-eqz v0, :cond_5

    .line 148191
    iget-object v0, v5, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 148192
    if-nez v0, :cond_5

    move v0, v1

    .line 148193
    :goto_0
    iget-object v6, v5, Lcom/facebook/api/feed/FetchFeedParams;->a:LX/0rS;

    move-object v5, v6

    .line 148194
    sget-object v6, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    if-eq v5, v6, :cond_6

    move v5, v1

    .line 148195
    :goto_1
    iget-object v6, p1, Lcom/facebook/api/feed/FetchFeedResult;->d:LX/0qw;

    move-object v6, v6

    .line 148196
    sget-object v7, LX/0qw;->CHUNKED_INITIAL:LX/0qw;

    if-eq v6, v7, :cond_0

    .line 148197
    iget-object v6, p1, Lcom/facebook/api/feed/FetchFeedResult;->d:LX/0qw;

    move-object v6, v6

    .line 148198
    sget-object v7, LX/0qw;->FULL:LX/0qw;

    if-ne v6, v7, :cond_7

    :cond_0
    move v6, v1

    .line 148199
    :goto_2
    if-eqz v0, :cond_8

    if-eqz v5, :cond_8

    if-eqz v6, :cond_8

    :goto_3
    move v0, v1

    .line 148200
    if-nez v0, :cond_1

    .line 148201
    :goto_4
    return-void

    .line 148202
    :cond_1
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-object v0, v0

    .line 148203
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->o()Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    move-result-object v0

    .line 148204
    if-nez v0, :cond_2

    .line 148205
    const-string v0, "Feed response returns empty result!"

    invoke-direct {p0, v0}, LX/0qf;->a(Ljava/lang/String;)V

    goto :goto_4

    .line 148206
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;->a()Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    move-result-object v1

    .line 148207
    sget-object v2, LX/1pV;->a:[I

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 148208
    const-string v0, "Feed response returns unknown result!"

    invoke-direct {p0, v0}, LX/0qf;->a(Ljava/lang/String;)V

    goto :goto_4

    .line 148209
    :pswitch_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    invoke-static {v4}, LX/0qi;->a(Z)LX/0qi;

    move-result-object v0

    .line 148210
    :goto_5
    invoke-direct {p0, v0}, LX/0qf;->a(LX/0qi;)V

    .line 148211
    iput-object v0, p0, LX/0qf;->c:LX/0qi;

    .line 148212
    invoke-direct {p0, v0, p1}, LX/0qf;->a(LX/0qi;Lcom/facebook/api/feed/FetchFeedResult;)V

    goto :goto_4

    .line 148213
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0qi;->a(Ljava/lang/String;)LX/0qi;

    move-result-object v0

    goto :goto_5

    .line 148214
    :pswitch_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    invoke-static {v4}, LX/0qi;->a(Z)LX/0qi;

    move-result-object v0

    .line 148215
    :goto_6
    invoke-static {v3}, LX/0qi;->a(Z)LX/0qi;

    move-result-object v1

    invoke-direct {p0, v1}, LX/0qf;->a(LX/0qi;)V

    .line 148216
    iput-object v0, p0, LX/0qf;->c:LX/0qi;

    .line 148217
    invoke-static {v3}, LX/0qi;->a(Z)LX/0qi;

    move-result-object v0

    invoke-direct {p0, v0, p1}, LX/0qf;->a(LX/0qi;Lcom/facebook/api/feed/FetchFeedResult;)V

    goto :goto_4

    .line 148218
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0qi;->a(Ljava/lang/String;)LX/0qi;

    move-result-object v0

    goto :goto_6

    .line 148219
    :pswitch_2
    invoke-static {v3}, LX/0qi;->a(Z)LX/0qi;

    move-result-object v0

    .line 148220
    invoke-direct {p0, v0}, LX/0qf;->a(LX/0qi;)V

    .line 148221
    iput-object v0, p0, LX/0qf;->c:LX/0qi;

    .line 148222
    invoke-direct {p0, v0, p1}, LX/0qf;->a(LX/0qi;Lcom/facebook/api/feed/FetchFeedResult;)V

    goto :goto_4

    :cond_5
    move v0, v2

    .line 148223
    goto/16 :goto_0

    :cond_6
    move v5, v2

    .line 148224
    goto/16 :goto_1

    :cond_7
    move v6, v2

    .line 148225
    goto/16 :goto_2

    :cond_8
    move v1, v2

    .line 148226
    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0rH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148186
    const/4 v0, 0x1

    new-array v0, v0, [LX/0rH;

    const/4 v1, 0x0

    sget-object v2, LX/0rH;->NEWS_FEED:LX/0rH;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/0qi;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 148181
    iget-object v1, p0, LX/0qf;->e:LX/0qi;

    if-nez v1, :cond_0

    .line 148182
    iget-object v1, p0, LX/0qf;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0qf;->a:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 148183
    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 148184
    :goto_0
    invoke-static {v0}, LX/0qi;->a(Ljava/lang/String;)LX/0qi;

    move-result-object v0

    iput-object v0, p0, LX/0qf;->e:LX/0qi;

    .line 148185
    :cond_0
    iget-object v0, p0, LX/0qf;->e:LX/0qi;

    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
