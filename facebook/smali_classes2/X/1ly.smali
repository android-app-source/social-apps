.class public LX/1ly;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1JF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1JF",
        "<",
        "LX/1mT;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/1Li;

.field private final c:Lcom/facebook/common/callercontext/CallerContext;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Lg;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/1mB;

.field public final f:Landroid/os/Handler;

.field private final g:LX/03V;

.field private final h:LX/1Bv;

.field private final i:LX/1Lr;

.field public final j:LX/0ad;

.field private k:LX/1M7;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:LX/1Lg;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final m:LX/19s;

.field private final n:LX/19j;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 312794
    const-class v0, LX/1ly;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1ly;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Li;Lcom/facebook/common/callercontext/CallerContext;LX/0Or;LX/1mB;Landroid/os/Handler;LX/03V;LX/1Bv;LX/1Lr;LX/0ad;LX/19s;LX/19j;)V
    .locals 0
    .param p1    # LX/1Li;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Li;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/0Or",
            "<",
            "LX/1Lg;",
            ">;",
            "LX/1mB;",
            "Landroid/os/Handler;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1Bv;",
            "LX/1Lr;",
            "LX/0ad;",
            "LX/19s;",
            "LX/19j;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 312795
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 312796
    iput-object p1, p0, LX/1ly;->b:LX/1Li;

    .line 312797
    iput-object p2, p0, LX/1ly;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 312798
    iput-object p3, p0, LX/1ly;->d:LX/0Or;

    .line 312799
    iput-object p4, p0, LX/1ly;->e:LX/1mB;

    .line 312800
    iput-object p5, p0, LX/1ly;->f:Landroid/os/Handler;

    .line 312801
    iput-object p6, p0, LX/1ly;->g:LX/03V;

    .line 312802
    iput-object p7, p0, LX/1ly;->h:LX/1Bv;

    .line 312803
    iput-object p8, p0, LX/1ly;->i:LX/1Lr;

    .line 312804
    iput-object p9, p0, LX/1ly;->j:LX/0ad;

    .line 312805
    iput-object p10, p0, LX/1ly;->m:LX/19s;

    .line 312806
    iput-object p11, p0, LX/1ly;->n:LX/19j;

    .line 312807
    return-void
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 312824
    iget-object v0, p0, LX/1ly;->m:LX/19s;

    if-eqz v0, :cond_0

    .line 312825
    iget-object v7, p0, LX/1ly;->m:LX/19s;

    new-instance v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v1, LX/379;->FEED:LX/379;

    invoke-virtual {v1}, LX/379;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, LX/19s;->a(Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;)J

    .line 312826
    :cond_0
    return-void
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 4

    .prologue
    .line 312808
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 312809
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1ly;->b(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 312810
    :cond_0
    invoke-static {p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 312811
    invoke-direct {p0, v0}, LX/1ly;->b(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 312812
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 312813
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 312814
    invoke-direct {p0, v0}, LX/1ly;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    goto :goto_1

    .line 312815
    :cond_2
    return-void
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 4

    .prologue
    .line 312816
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 312817
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 312818
    invoke-direct {p0, v0}, LX/1ly;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 312819
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 312820
    :cond_0
    iget-object v0, p0, LX/1ly;->j:LX/0ad;

    sget-short v1, LX/1wC;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, LX/1VO;->v(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 312821
    :goto_1
    return-void

    .line 312822
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 312823
    iget-object v1, p0, LX/1ly;->f:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/feed/ui/feedprefetch/VideoPrefetchVisitor$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/feed/ui/feedprefetch/VideoPrefetchVisitor$1;-><init>(LX/1ly;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    const v0, -0x6d980be3

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_1
.end method

.method private e()LX/1M7;
    .locals 2

    .prologue
    .line 312753
    iget-object v0, p0, LX/1ly;->k:LX/1M7;

    if-nez v0, :cond_1

    .line 312754
    iget-object v0, p0, LX/1ly;->l:LX/1Lg;

    if-nez v0, :cond_0

    .line 312755
    iget-object v0, p0, LX/1ly;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lg;

    iput-object v0, p0, LX/1ly;->l:LX/1Lg;

    .line 312756
    :cond_0
    iget-object v0, p0, LX/1ly;->l:LX/1Lg;

    move-object v0, v0

    .line 312757
    iget-object v1, p0, LX/1ly;->b:LX/1Li;

    invoke-virtual {v0, v1}, LX/1Lg;->a(LX/1Li;)LX/1M7;

    move-result-object v0

    iput-object v0, p0, LX/1ly;->k:LX/1M7;

    .line 312758
    :cond_1
    iget-object v0, p0, LX/1ly;->k:LX/1M7;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/Void;
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 312759
    invoke-static {p1}, LX/1VO;->v(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 312760
    :cond_0
    :goto_0
    return-object v8

    .line 312761
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 312762
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 312763
    :goto_1
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v0, v4, :cond_2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_DIRECT_RESPONSE_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v0, v4, :cond_2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_CINEMAGRAPH:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v0, v4, :cond_2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v0, v4, :cond_2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v0, v4, :cond_2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v0, v4, :cond_2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSPIRATION_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne v0, v4, :cond_9

    .line 312764
    :cond_2
    const/4 v4, 0x1

    .line 312765
    :goto_2
    move v0, v4

    .line 312766
    if-eqz v0, :cond_5

    iget-object v0, p0, LX/1ly;->h:LX/1Bv;

    invoke-virtual {v0}, LX/1Bv;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 312767
    :goto_3
    if-nez v0, :cond_3

    iget-object v0, p0, LX/1ly;->i:LX/1Lr;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v4

    invoke-virtual {v0, v4}, LX/1Lr;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312768
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 312769
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->aW()Ljava/lang/String;

    move-result-object v0

    .line 312770
    iget-object v1, p0, LX/1ly;->n:LX/19j;

    iget-boolean v1, v1, LX/19j;->q:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 312771
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 312772
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".mpd"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 312773
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->aT()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/1ly;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 312774
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto :goto_1

    :cond_5
    move v0, v2

    .line 312775
    goto :goto_3

    .line 312776
    :cond_6
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 312777
    if-eqz v0, :cond_0

    .line 312778
    new-instance v4, LX/36s;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v0, v5}, LX/36s;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 312779
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->r()I

    move-result v0

    int-to-long v6, v0

    .line 312780
    iput-wide v6, v4, LX/36s;->f:J

    .line 312781
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->n()I

    move-result v0

    int-to-long v6, v0

    .line 312782
    iput-wide v6, v4, LX/36s;->e:J

    .line 312783
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->aR()I

    move-result v0

    int-to-long v6, v0

    .line 312784
    iput-wide v6, v4, LX/36s;->g:J

    .line 312785
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->at()Z

    move-result v0

    .line 312786
    iput-boolean v0, v4, LX/36s;->i:Z

    .line 312787
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->aT()Ljava/lang/String;

    move-result-object v0

    .line 312788
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 312789
    iget-object v5, p0, LX/1ly;->e:LX/1mB;

    invoke-direct {p0}, LX/1ly;->e()LX/1M7;

    move-result-object v6

    invoke-virtual {v5, v0, v6, v4}, LX/1mB;->a(Ljava/lang/String;LX/1M7;LX/36s;)V

    .line 312790
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    .line 312791
    :cond_7
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, LX/1ly;->e:LX/1mB;

    invoke-virtual {v0}, LX/1mB;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 312792
    :cond_8
    invoke-direct {p0}, LX/1ly;->e()LX/1M7;

    move-result-object v0

    new-array v1, v1, [LX/36s;

    aput-object v4, v1, v2

    invoke-interface {v0, v1}, LX/1M7;->b([LX/36s;)V

    .line 312793
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    goto/16 :goto_0

    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 312751
    invoke-direct {p0}, LX/1ly;->e()LX/1M7;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1M7;->a(Z)V

    .line 312752
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 2

    .prologue
    .line 312746
    const-string v0, "VideoPrefetchVisitor.visitStory"

    const v1, -0x6ce9127d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 312747
    :try_start_0
    invoke-direct {p0, p1}, LX/1ly;->b(Lcom/facebook/graphql/model/GraphQLStory;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312748
    const v0, -0xa9b13e3

    invoke-static {v0}, LX/02m;->b(I)J

    .line 312749
    return-void

    .line 312750
    :catchall_0
    move-exception v0

    const v1, 0x45f42268

    invoke-static {v1}, LX/02m;->b(I)J

    throw v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 312742
    check-cast p1, LX/1mT;

    .line 312743
    iget-object v0, p1, LX/1mT;->b:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v0

    .line 312744
    invoke-virtual {p0, v0}, LX/1ly;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 312745
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 312740
    invoke-direct {p0}, LX/1ly;->e()LX/1M7;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1M7;->a(Z)V

    .line 312741
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 312739
    invoke-direct {p0}, LX/1ly;->e()LX/1M7;

    move-result-object v0

    invoke-interface {v0}, LX/1M7;->a()Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 312737
    invoke-direct {p0}, LX/1ly;->e()LX/1M7;

    move-result-object v0

    invoke-interface {v0}, LX/1M7;->b()V

    .line 312738
    return-void
.end method
