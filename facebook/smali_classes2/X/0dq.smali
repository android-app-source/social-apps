.class public final LX/0dq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0dq;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .prologue
    .line 90954
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90955
    iput-object p1, p0, LX/0dq;->a:LX/0Ot;

    .line 90956
    iput-object p2, p0, LX/0dq;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 90957
    return-void
.end method

.method public static a(LX/0QB;)LX/0dq;
    .locals 5

    .prologue
    .line 90961
    sget-object v0, LX/0dq;->c:LX/0dq;

    if-nez v0, :cond_1

    .line 90962
    const-class v1, LX/0dq;

    monitor-enter v1

    .line 90963
    :try_start_0
    sget-object v0, LX/0dq;->c:LX/0dq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 90964
    if-eqz v2, :cond_0

    .line 90965
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 90966
    new-instance v4, LX/0dq;

    const/16 v3, 0x3d5

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v4, p0, v3}, LX/0dq;-><init>(LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 90967
    move-object v0, v4

    .line 90968
    sput-object v0, LX/0dq;->c:LX/0dq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90969
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 90970
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 90971
    :cond_1
    sget-object v0, LX/0dq;->c:LX/0dq;

    return-object v0

    .line 90972
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 90973
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 90958
    iget-object v0, p0, LX/0dq;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90959
    iget-object v0, p0, LX/0dq;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    invoke-virtual {v0}, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->c()V

    .line 90960
    :cond_0
    return-void
.end method
