.class public LX/1ri;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1rj;


# direct methods
.method public constructor <init>(LX/1rU;Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 332898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 332899
    invoke-virtual {p1}, LX/1rU;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332900
    iput-object p3, p0, LX/1ri;->a:LX/1rj;

    .line 332901
    :goto_0
    return-void

    .line 332902
    :cond_0
    iput-object p2, p0, LX/1ri;->a:LX/1rj;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1ri;
    .locals 1

    .prologue
    .line 332903
    invoke-static {p0}, LX/1ri;->b(LX/0QB;)LX/1ri;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1ri;
    .locals 4

    .prologue
    .line 332904
    new-instance v3, LX/1ri;

    invoke-static {p0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v0

    check-cast v0, LX/1rU;

    invoke-static {p0}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->a(LX/0QB;)Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    invoke-static {p0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a(LX/0QB;)Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    move-result-object v2

    check-cast v2, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-direct {v3, v0, v1, v2}, LX/1ri;-><init>(LX/1rU;Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;)V

    .line 332905
    return-object v3
.end method


# virtual methods
.method public final a()LX/1rj;
    .locals 1

    .prologue
    .line 332906
    iget-object v0, p0, LX/1ri;->a:LX/1rj;

    return-object v0
.end method
