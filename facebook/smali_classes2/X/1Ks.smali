.class public final LX/1Ks;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/0fx;

.field public final synthetic c:LX/1Kq;

.field public final synthetic d:LX/1AP;


# direct methods
.method public constructor <init>(LX/1AP;ZLX/0fx;LX/1Kq;)V
    .locals 0

    .prologue
    .line 233199
    iput-object p1, p0, LX/1Ks;->d:LX/1AP;

    iput-boolean p2, p0, LX/1Ks;->a:Z

    iput-object p3, p0, LX/1Ks;->b:LX/0fx;

    iput-object p4, p0, LX/1Ks;->c:LX/1Kq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 4

    .prologue
    .line 233200
    iget-boolean v0, p0, LX/1Ks;->a:Z

    if-eqz v0, :cond_0

    .line 233201
    iget-object v0, p0, LX/1Ks;->d:LX/1AP;

    invoke-static {v0, p2}, LX/1AP;->a$redex0(LX/1AP;I)V

    .line 233202
    :cond_0
    if-nez p2, :cond_1

    .line 233203
    invoke-interface {p1}, LX/0g8;->q()I

    move-result v0

    .line 233204
    invoke-interface {p1}, LX/0g8;->r()I

    move-result v1

    sub-int/2addr v1, v0

    add-int/lit8 v1, v1, 0x1

    .line 233205
    invoke-interface {p1}, LX/0g8;->s()I

    move-result v2

    .line 233206
    const/4 v3, -0x1

    if-ne v0, v3, :cond_2

    iget-object v3, p0, LX/1Ks;->d:LX/1AP;

    iget-boolean v3, v3, LX/1AP;->e:Z

    if-nez v3, :cond_2

    .line 233207
    iget-object v0, p0, LX/1Ks;->d:LX/1AP;

    iget-object v0, v0, LX/1AP;->c:LX/03V;

    sget-object v1, LX/1AP;->a:Ljava/lang/String;

    const-string v2, "t9619287: callFinalOnScroll with NO_POSITION firstItemIndex"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 233208
    iget-object v0, p0, LX/1Ks;->d:LX/1AP;

    const/4 v1, 0x1

    .line 233209
    iput-boolean v1, v0, LX/1AP;->e:Z

    .line 233210
    :cond_1
    :goto_0
    iget-object v0, p0, LX/1Ks;->b:LX/0fx;

    invoke-interface {v0, p1, p2}, LX/0fx;->a(LX/0g8;I)V

    .line 233211
    return-void

    .line 233212
    :cond_2
    iget-object v3, p0, LX/1Ks;->b:LX/0fx;

    invoke-interface {v3, p1, v0, v1, v2}, LX/0fx;->a(LX/0g8;III)V

    goto :goto_0
.end method

.method public final a(LX/0g8;III)V
    .locals 1

    .prologue
    .line 233213
    iget-object v0, p0, LX/1Ks;->c:LX/1Kq;

    invoke-interface {v0}, LX/1Kq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233214
    :goto_0
    return-void

    .line 233215
    :cond_0
    iget-object v0, p0, LX/1Ks;->b:LX/0fx;

    invoke-interface {v0, p1, p2, p3, p4}, LX/0fx;->a(LX/0g8;III)V

    goto :goto_0
.end method
