.class public LX/1GK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1GL;


# instance fields
.field private final a:LX/1GA;

.field private final b:Landroid/graphics/Bitmap$Config;

.field private final c:LX/1FG;

.field private final d:LX/1GL;

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1lW;",
            "LX/1GL;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1GA;LX/1FG;Landroid/graphics/Bitmap$Config;)V
    .locals 1

    .prologue
    .line 225278
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/1GK;-><init>(LX/1GA;LX/1FG;Landroid/graphics/Bitmap$Config;Ljava/util/Map;)V

    .line 225279
    return-void
.end method

.method public constructor <init>(LX/1GA;LX/1FG;Landroid/graphics/Bitmap$Config;Ljava/util/Map;)V
    .locals 1
    .param p4    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/animated/factory/AnimatedImageFactory;",
            "LX/1FG;",
            "Landroid/graphics/Bitmap$Config;",
            "Ljava/util/Map",
            "<",
            "LX/1lW;",
            "LX/1GL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 225271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225272
    new-instance v0, LX/1GN;

    invoke-direct {v0, p0}, LX/1GN;-><init>(LX/1GK;)V

    iput-object v0, p0, LX/1GK;->d:LX/1GL;

    .line 225273
    iput-object p1, p0, LX/1GK;->a:LX/1GA;

    .line 225274
    iput-object p3, p0, LX/1GK;->b:Landroid/graphics/Bitmap$Config;

    .line 225275
    iput-object p2, p0, LX/1GK;->c:LX/1FG;

    .line 225276
    iput-object p4, p0, LX/1GK;->e:Ljava/util/Map;

    .line 225277
    return-void
.end method


# virtual methods
.method public final a(LX/1FL;ILX/1lk;LX/1bZ;)LX/1ln;
    .locals 2

    .prologue
    .line 225280
    iget-object v0, p1, LX/1FL;->c:LX/1lW;

    move-object v0, v0

    .line 225281
    if-eqz v0, :cond_0

    sget-object v1, LX/1lW;->a:LX/1lW;

    if-ne v0, v1, :cond_1

    .line 225282
    :cond_0
    invoke-virtual {p1}, LX/1FL;->b()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, LX/1la;->b(Ljava/io/InputStream;)LX/1lW;

    move-result-object v0

    .line 225283
    iput-object v0, p1, LX/1FL;->c:LX/1lW;

    .line 225284
    :cond_1
    iget-object v1, p0, LX/1GK;->e:Ljava/util/Map;

    if-eqz v1, :cond_2

    .line 225285
    iget-object v1, p0, LX/1GK;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1GL;

    .line 225286
    if-eqz v0, :cond_2

    .line 225287
    invoke-interface {v0, p1, p2, p3, p4}, LX/1GL;->a(LX/1FL;ILX/1lk;LX/1bZ;)LX/1ln;

    move-result-object v0

    .line 225288
    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p0, LX/1GK;->d:LX/1GL;

    invoke-interface {v0, p1, p2, p3, p4}, LX/1GL;->a(LX/1FL;ILX/1lk;LX/1bZ;)LX/1ln;

    move-result-object v0

    goto :goto_0
.end method

.method public a(LX/1FL;LX/1bZ;)LX/1ln;
    .locals 3

    .prologue
    .line 225262
    invoke-virtual {p1}, LX/1FL;->b()Ljava/io/InputStream;

    move-result-object v1

    .line 225263
    if-nez v1, :cond_0

    .line 225264
    const/4 v0, 0x0

    .line 225265
    :goto_0
    return-object v0

    .line 225266
    :cond_0
    :try_start_0
    iget-boolean v0, p2, LX/1bZ;->e:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/1GK;->a:LX/1GA;

    if-eqz v0, :cond_1

    invoke-static {v1}, LX/4dB;->a(Ljava/io/InputStream;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 225267
    iget-object v0, p0, LX/1GK;->a:LX/1GA;

    iget-object v2, p0, LX/1GK;->b:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, p1, p2, v2}, LX/1GA;->a(LX/1FL;LX/1bZ;Landroid/graphics/Bitmap$Config;)LX/1ln;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 225268
    invoke-static {v1}, LX/1vz;->a(Ljava/io/InputStream;)V

    goto :goto_0

    .line 225269
    :cond_1
    :try_start_1
    invoke-virtual {p0, p1, p2}, LX/1GK;->b(LX/1FL;LX/1bZ;)LX/1ll;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 225270
    invoke-static {v1}, LX/1vz;->a(Ljava/io/InputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1vz;->a(Ljava/io/InputStream;)V

    throw v0
.end method

.method public final b(LX/1FL;ILX/1lk;LX/1bZ;)LX/1ll;
    .locals 3

    .prologue
    .line 225257
    iget-object v0, p0, LX/1GK;->c:LX/1FG;

    iget-object v1, p4, LX/1bZ;->f:Landroid/graphics/Bitmap$Config;

    invoke-interface {v0, p1, v1, p2}, LX/1FG;->a(LX/1FL;Landroid/graphics/Bitmap$Config;I)LX/1FJ;

    move-result-object v1

    .line 225258
    :try_start_0
    new-instance v0, LX/1ll;

    .line 225259
    iget v2, p1, LX/1FL;->d:I

    move v2, v2

    .line 225260
    invoke-direct {v0, v1, p3, v2}, LX/1ll;-><init>(LX/1FJ;LX/1lk;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225261
    invoke-virtual {v1}, LX/1FJ;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/1FJ;->close()V

    throw v0
.end method

.method public final b(LX/1FL;LX/1bZ;)LX/1ll;
    .locals 4

    .prologue
    .line 225252
    iget-object v0, p0, LX/1GK;->c:LX/1FG;

    iget-object v1, p2, LX/1bZ;->f:Landroid/graphics/Bitmap$Config;

    invoke-interface {v0, p1, v1}, LX/1FG;->a(LX/1FL;Landroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v1

    .line 225253
    :try_start_0
    new-instance v0, LX/1ll;

    sget-object v2, LX/1lk;->a:LX/1lk;

    .line 225254
    iget v3, p1, LX/1FL;->d:I

    move v3, v3

    .line 225255
    invoke-direct {v0, v1, v2, v3}, LX/1ll;-><init>(LX/1FJ;LX/1lk;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225256
    invoke-virtual {v1}, LX/1FJ;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/1FJ;->close()V

    throw v0
.end method

.method public c(LX/1FL;LX/1bZ;)LX/1ln;
    .locals 2

    .prologue
    .line 225251
    iget-object v0, p0, LX/1GK;->a:LX/1GA;

    iget-object v1, p0, LX/1GK;->b:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, p1, p2, v1}, LX/1GA;->b(LX/1FL;LX/1bZ;Landroid/graphics/Bitmap$Config;)LX/1ln;

    move-result-object v0

    return-object v0
.end method
