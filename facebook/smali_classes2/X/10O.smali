.class public LX/10O;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private b:LX/0Zb;

.field private c:LX/0W3;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Zb;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 168932
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168933
    iput-object p1, p0, LX/10O;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 168934
    iput-object p2, p0, LX/10O;->b:LX/0Zb;

    .line 168935
    iput-object p3, p0, LX/10O;->c:LX/0W3;

    .line 168936
    return-void
.end method

.method public static a(LX/0QB;)LX/10O;
    .locals 1

    .prologue
    .line 168920
    invoke-static {p0}, LX/10O;->b(LX/0QB;)LX/10O;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/10O;
    .locals 4

    .prologue
    .line 168930
    new-instance v3, LX/10O;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v2

    check-cast v2, LX/0W3;

    invoke-direct {v3, v0, v1, v2}, LX/10O;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Zb;LX/0W3;)V

    .line 168931
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 168921
    if-eqz p1, :cond_1

    .line 168922
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 168923
    iget-object v0, p0, LX/10O;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/26p;->f:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 168924
    const-string v2, "source_datr"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 168925
    if-eqz p2, :cond_0

    .line 168926
    invoke-virtual {p2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 168927
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 168928
    :cond_0
    iget-object v0, p0, LX/10O;->b:LX/0Zb;

    iget-object v2, p0, LX/10O;->c:LX/0W3;

    sget-wide v4, LX/0X5;->bM:J

    const/4 v3, 0x1

    invoke-interface {v2, v4, v5, v3}, LX/0W4;->a(JI)I

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 168929
    :cond_1
    return-void
.end method
