.class public LX/1hv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1hr;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/1hv;


# instance fields
.field private b:LX/0Uh;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0W3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 297293
    const-class v0, LX/1hv;

    sput-object v0, LX/1hv;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Uh;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 297294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297295
    iput-object p1, p0, LX/1hv;->b:LX/0Uh;

    .line 297296
    iput-object p2, p0, LX/1hv;->d:LX/0W3;

    .line 297297
    invoke-virtual {p0}, LX/1hv;->b()V

    .line 297298
    return-void
.end method

.method public static a(LX/0QB;)LX/1hv;
    .locals 5

    .prologue
    .line 297299
    sget-object v0, LX/1hv;->e:LX/1hv;

    if-nez v0, :cond_1

    .line 297300
    const-class v1, LX/1hv;

    monitor-enter v1

    .line 297301
    :try_start_0
    sget-object v0, LX/1hv;->e:LX/1hv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 297302
    if-eqz v2, :cond_0

    .line 297303
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 297304
    new-instance p0, LX/1hv;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-direct {p0, v3, v4}, LX/1hv;-><init>(LX/0Uh;LX/0W3;)V

    .line 297305
    move-object v0, p0

    .line 297306
    sput-object v0, LX/1hv;->e:LX/1hv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297307
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 297308
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 297309
    :cond_1
    sget-object v0, LX/1hv;->e:LX/1hv;

    return-object v0

    .line 297310
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 297311
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1hv;I)Z
    .locals 2

    .prologue
    .line 297312
    iget-object v0, p0, LX/1hv;->b:LX/0Uh;

    invoke-virtual {v0, p1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    .line 297313
    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 297314
    const v0, 0x7fffffff

    return v0
.end method

.method public final a(Lorg/apache/http/impl/client/RequestWrapper;)V
    .locals 8

    .prologue
    .line 297315
    invoke-virtual {p1}, Lorg/apache/http/impl/client/RequestWrapper;->getURI()Ljava/net/URI;

    move-result-object v7

    .line 297316
    const/4 v0, 0x0

    .line 297317
    const/16 v1, 0x5e0

    invoke-static {p0, v1}, LX/1hv;->a(LX/1hv;I)Z

    move-result v1

    move v1, v1

    .line 297318
    if-nez v1, :cond_4

    .line 297319
    :cond_0
    :goto_0
    move v0, v0

    .line 297320
    if-nez v0, :cond_2

    .line 297321
    :cond_1
    :goto_1
    return-void

    .line 297322
    :cond_2
    invoke-virtual {v7}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 297323
    const/4 v1, 0x0

    .line 297324
    iget-object v2, p0, LX/1hv;->c:Ljava/util/List;

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/1hv;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 297325
    :cond_3
    :goto_2
    move v0, v1

    .line 297326
    if-eqz v0, :cond_1

    .line 297327
    :try_start_0
    new-instance v0, Ljava/net/URI;

    invoke-virtual {v7}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, Ljava/net/URI;->getUserInfo()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x2

    .line 297328
    const-string v4, "^(.+)\\.(facebook\\.com)$"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    .line 297329
    invoke-virtual {v4, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 297330
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v5

    if-eq v5, v6, :cond_6

    .line 297331
    :goto_3
    move-object v3, v3

    .line 297332
    invoke-virtual {v7}, Ljava/net/URI;->getPort()I

    move-result v4

    invoke-virtual {v7}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7}, Ljava/net/URI;->getQuery()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, Ljava/net/URI;->getFragment()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 297333
    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/RequestWrapper;->setURI(Ljava/net/URI;)V

    goto :goto_1

    .line 297334
    :catch_0
    move-exception v0

    .line 297335
    sget-object v1, LX/1hv;->a:Ljava/lang/Class;

    const-string v2, "Failed to create output URI object"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 297336
    :cond_4
    const/16 v1, 0x4b7

    invoke-static {p0, v1}, LX/1hv;->a(LX/1hv;I)Z

    move-result v1

    move v1, v1

    .line 297337
    if-eqz v1, :cond_0

    .line 297338
    const/4 v0, 0x1

    goto :goto_0

    .line 297339
    :cond_5
    iget-object v2, p0, LX/1hv;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 297340
    const/4 v1, 0x1

    goto :goto_2

    .line 297341
    :cond_6
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    .line 297342
    invoke-virtual {v4, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    .line 297343
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".alpha."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_3
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 297344
    iget-object v0, p0, LX/1hv;->d:LX/0W3;

    invoke-virtual {v0}, LX/0W3;->a()LX/0W4;

    move-result-object v0

    sget-wide v2, LX/0X5;->if:J

    const-string v4, "[]"

    invoke-interface {v0, v2, v3, v4}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 297345
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1hv;->c:Ljava/util/List;

    .line 297346
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    move v0, v1

    .line 297347
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 297348
    iget-object v4, p0, LX/1hv;->c:Ljava/util/List;

    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 297349
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 297350
    :catch_0
    move-exception v0

    .line 297351
    sget-object v3, LX/1hv;->a:Ljava/lang/Class;

    const-string v4, "failed to parse json: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v1

    invoke-static {v3, v0, v4, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 297352
    :cond_0
    return-void
.end method
