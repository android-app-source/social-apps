.class public abstract LX/1ln;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 312390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract b()I
.end method

.method public abstract c()Z
.end method

.method public abstract close()V
.end method

.method public d()LX/1lk;
    .locals 1

    .prologue
    .line 312391
    sget-object v0, LX/1lk;->a:LX/1lk;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 312389
    const/4 v0, 0x0

    return v0
.end method

.method public final finalize()V
    .locals 5

    .prologue
    .line 312384
    invoke-virtual {p0}, LX/1ln;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312385
    :goto_0
    return-void

    .line 312386
    :cond_0
    const-string v0, "CloseableImage"

    const-string v1, "finalize: %s %x still open."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 312387
    :try_start_0
    invoke-virtual {p0}, LX/1ln;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312388
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public abstract g()I
.end method

.method public abstract h()I
.end method
