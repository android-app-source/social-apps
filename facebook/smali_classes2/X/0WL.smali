.class public LX/0WL;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0WP;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0WP;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76100
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0WP;
    .locals 6

    .prologue
    .line 76101
    sget-object v0, LX/0WL;->a:LX/0WP;

    if-nez v0, :cond_1

    .line 76102
    const-class v1, LX/0WL;

    monitor-enter v1

    .line 76103
    :try_start_0
    sget-object v0, LX/0WL;->a:LX/0WP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 76104
    if-eqz v2, :cond_0

    .line 76105
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 76106
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0WM;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v5

    check-cast v5, LX/0Sg;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    invoke-static {v3, v4, v5, p0}, LX/0WN;->a(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/0Sg;LX/0Uh;)LX/0WP;

    move-result-object v3

    move-object v0, v3

    .line 76107
    sput-object v0, LX/0WL;->a:LX/0WP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76108
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 76109
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 76110
    :cond_1
    sget-object v0, LX/0WL;->a:LX/0WP;

    return-object v0

    .line 76111
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 76112
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 76099
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0WM;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v2

    check-cast v2, LX/0Sg;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0, v1, v2, v3}, LX/0WN;->a(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/0Sg;LX/0Uh;)LX/0WP;

    move-result-object v0

    return-object v0
.end method
