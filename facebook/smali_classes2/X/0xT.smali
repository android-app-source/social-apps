.class public final enum LX/0xT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0xT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0xT;

.field public static final enum DEFAULT:LX/0xT;

.field public static final enum STATE_READY:LX/0xT;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 163108
    new-instance v0, LX/0xT;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, LX/0xT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0xT;->DEFAULT:LX/0xT;

    .line 163109
    new-instance v0, LX/0xT;

    const-string v1, "STATE_READY"

    invoke-direct {v0, v1, v3}, LX/0xT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0xT;->STATE_READY:LX/0xT;

    .line 163110
    const/4 v0, 0x2

    new-array v0, v0, [LX/0xT;

    sget-object v1, LX/0xT;->DEFAULT:LX/0xT;

    aput-object v1, v0, v2

    sget-object v1, LX/0xT;->STATE_READY:LX/0xT;

    aput-object v1, v0, v3

    sput-object v0, LX/0xT;->$VALUES:[LX/0xT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 163103
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static of(Ljava/lang/String;)LX/0xT;
    .locals 1

    .prologue
    .line 163106
    :try_start_0
    invoke-static {p0}, LX/0xT;->valueOf(Ljava/lang/String;)LX/0xT;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 163107
    :goto_0
    return-object v0

    :catch_0
    sget-object v0, LX/0xT;->DEFAULT:LX/0xT;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0xT;
    .locals 1

    .prologue
    .line 163105
    const-class v0, LX/0xT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0xT;

    return-object v0
.end method

.method public static values()[LX/0xT;
    .locals 1

    .prologue
    .line 163104
    sget-object v0, LX/0xT;->$VALUES:[LX/0xT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0xT;

    return-object v0
.end method
