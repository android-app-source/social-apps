.class public LX/1qK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final mBundle:Landroid/os/Bundle;

.field public final mCallback:LX/1qH;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mCallerContext:Lcom/facebook/common/callercontext/CallerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mId:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mRequestState:LX/0zW;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mType:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/4BI;)V
    .locals 7

    .prologue
    .line 330489
    iget-object v0, p1, LX/4BI;->mType:Ljava/lang/String;

    move-object v1, v0

    .line 330490
    iget-object v0, p1, LX/4BI;->mBundle:Landroid/os/Bundle;

    move-object v2, v0

    .line 330491
    iget-object v0, p1, LX/4BI;->mId:Ljava/lang/String;

    move-object v3, v0

    .line 330492
    iget-object v0, p1, LX/4BI;->mRequestState:LX/0zW;

    move-object v4, v0

    .line 330493
    iget-object v0, p1, LX/4BI;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v5, v0

    .line 330494
    iget-object v0, p1, LX/4BI;->mCallback:LX/1qH;

    move-object v6, v0

    .line 330495
    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LX/1qK;-><init>(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;LX/0zW;Lcom/facebook/common/callercontext/CallerContext;LX/1qH;)V

    .line 330496
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 330525
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, LX/1qK;-><init>(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;LX/0zW;Lcom/facebook/common/callercontext/CallerContext;LX/1qH;)V

    .line 330526
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 330523
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, LX/1qK;-><init>(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;LX/0zW;Lcom/facebook/common/callercontext/CallerContext;LX/1qH;)V

    .line 330524
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;LX/0zW;Lcom/facebook/common/callercontext/CallerContext;LX/1qH;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0zW;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/1qH;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 330515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330516
    iput-object p1, p0, LX/1qK;->mType:Ljava/lang/String;

    .line 330517
    iput-object p2, p0, LX/1qK;->mBundle:Landroid/os/Bundle;

    .line 330518
    iput-object p4, p0, LX/1qK;->mRequestState:LX/0zW;

    .line 330519
    iput-object p5, p0, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    .line 330520
    iput-object p6, p0, LX/1qK;->mCallback:LX/1qH;

    .line 330521
    iput-object p3, p0, LX/1qK;->mId:Ljava/lang/String;

    .line 330522
    return-void
.end method

.method public static builder()LX/4BI;
    .locals 1

    .prologue
    .line 330514
    new-instance v0, LX/4BI;

    invoke-direct {v0}, LX/4BI;-><init>()V

    return-object v0
.end method


# virtual methods
.method public chain(LX/4B6;)LX/1qK;
    .locals 3

    .prologue
    .line 330510
    invoke-static {}, LX/1qK;->builder()LX/4BI;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/4BI;->from(LX/1qK;)LX/4BI;

    move-result-object v0

    new-instance v1, LX/4B7;

    iget-object v2, p0, LX/1qK;->mCallback:LX/1qH;

    invoke-direct {v1, v2, p1}, LX/4B7;-><init>(LX/1qH;LX/4B6;)V

    .line 330511
    iput-object v1, v0, LX/4BI;->mCallback:LX/1qH;

    .line 330512
    move-object v0, v0

    .line 330513
    invoke-virtual {v0}, LX/4BI;->build()LX/1qK;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 330501
    instance-of v1, p1, LX/1qK;

    if-eqz v1, :cond_0

    .line 330502
    check-cast p1, LX/1qK;

    .line 330503
    iget-object v1, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v1, v1

    .line 330504
    iget-object v2, p0, LX/1qK;->mType:Ljava/lang/String;

    move-object v2, v2

    .line 330505
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 330506
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 330507
    iget-object v2, p0, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v2

    .line 330508
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 330509
    :cond_0
    return v0
.end method

.method public getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 330500
    iget-object v0, p0, LX/1qK;->mBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method public getCallback()LX/1qH;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 330499
    iget-object v0, p0, LX/1qK;->mCallback:LX/1qH;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 330498
    iget-object v0, p0, LX/1qK;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 330497
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/1qK;->mType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/1qK;->mBundle:Landroid/os/Bundle;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
