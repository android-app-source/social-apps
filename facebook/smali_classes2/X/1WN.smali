.class public LX/1WN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 268070
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 268071
    iput-object p1, p0, LX/1WN;->a:LX/0Zb;

    .line 268072
    return-void
.end method

.method public static a(LX/0QB;)LX/1WN;
    .locals 1

    .prologue
    .line 268073
    invoke-static {p0}, LX/1WN;->b(LX/0QB;)LX/1WN;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1WN;
    .locals 2

    .prologue
    .line 268074
    new-instance v1, LX/1WN;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/1WN;-><init>(LX/0Zb;)V

    .line 268075
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 268076
    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    .line 268077
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 268078
    if-eqz p2, :cond_1

    const-string v0, "graphic_content"

    .line 268079
    :goto_0
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 268080
    const-string v0, "content_id"

    invoke-virtual {v1, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 268081
    iget-object v0, p0, LX/1WN;->a:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 268082
    :cond_0
    return-void

    .line 268083
    :cond_1
    const-string v0, "objectionable_content"

    goto :goto_0
.end method
