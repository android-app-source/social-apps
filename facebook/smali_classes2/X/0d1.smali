.class public final enum LX/0d1;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/0d2;


# annotations
.annotation build Lcom/google/common/annotations/GwtIncompatible;
    value = "To be supported"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0d1;",
        ">;",
        "LX/0d2",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0d1;

.field public static final enum INSTANCE:LX/0d1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 89496
    new-instance v0, LX/0d1;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, LX/0d1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0d1;->INSTANCE:LX/0d1;

    .line 89497
    const/4 v0, 0x1

    new-array v0, v0, [LX/0d1;

    sget-object v1, LX/0d1;->INSTANCE:LX/0d1;

    aput-object v1, v0, v2

    sput-object v0, LX/0d1;->$VALUES:[LX/0d1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 89498
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0d1;
    .locals 1

    .prologue
    .line 89499
    const-class v0, LX/0d1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0d1;

    return-object v0
.end method

.method public static values()[LX/0d1;
    .locals 1

    .prologue
    .line 89500
    sget-object v0, LX/0d1;->$VALUES:[LX/0d1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0d1;

    return-object v0
.end method


# virtual methods
.method public final onRemoval(LX/4zM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4zM",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 89501
    return-void
.end method
