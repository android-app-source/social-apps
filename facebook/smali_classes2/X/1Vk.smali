.class public LX/1Vk;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Vl;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1Vk",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1Vl;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 266351
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 266352
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1Vk;->b:LX/0Zi;

    .line 266353
    iput-object p1, p0, LX/1Vk;->a:LX/0Ot;

    .line 266354
    return-void
.end method

.method public static a(LX/0QB;)LX/1Vk;
    .locals 4

    .prologue
    .line 266355
    const-class v1, LX/1Vk;

    monitor-enter v1

    .line 266356
    :try_start_0
    sget-object v0, LX/1Vk;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 266357
    sput-object v2, LX/1Vk;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 266358
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266359
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 266360
    new-instance v3, LX/1Vk;

    const/16 p0, 0x968

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1Vk;-><init>(LX/0Ot;)V

    .line 266361
    move-object v0, v3

    .line 266362
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 266363
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Vk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266364
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 266365
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 266366
    check-cast p2, LX/C8X;

    .line 266367
    iget-object v0, p0, LX/1Vk;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Vl;

    iget-object v1, p2, LX/C8X;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C8X;->b:LX/1Pp;

    .line 266368
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 266369
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v3}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    move-object v3, v3

    .line 266370
    invoke-static {v3}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v4

    .line 266371
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 266372
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->C()LX/0Px;

    move-result-object v3

    .line 266373
    new-instance p0, LX/C8Y;

    invoke-direct {p0, v0, v3}, LX/C8Y;-><init>(LX/1Vl;LX/0Px;)V

    move-object v3, p0

    .line 266374
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    iget-object p2, v0, LX/1Vl;->a:LX/1Vm;

    invoke-virtual {p2, p1}, LX/1Vm;->c(LX/1De;)LX/C2N;

    move-result-object p2

    invoke-virtual {p2, v3}, LX/C2N;->a(Landroid/view/View$OnClickListener;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/C2N;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/C2N;

    move-result-object v3

    const v4, 0x7f020d77

    .line 266375
    iget-object p2, v3, LX/C2N;->a:LX/C2M;

    invoke-virtual {v3, v4}, LX/1Dp;->g(I)LX/1dc;

    move-result-object v1

    iput-object v1, p2, LX/C2M;->d:LX/1dc;

    .line 266376
    move-object v3, v3

    .line 266377
    invoke-virtual {p1}, LX/1De;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 266378
    invoke-static {v0, v4}, LX/1Vl;->a$redex0(LX/1Vl;Landroid/content/Context;)Z

    move-result p2

    if-eqz p2, :cond_0

    const p2, 0x7f082a0c

    :goto_0
    invoke-virtual {v4, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    move-object v4, p2

    .line 266379
    iget-object p2, v3, LX/C2N;->a:LX/C2M;

    iput-object v4, p2, LX/C2M;->f:Ljava/lang/String;

    .line 266380
    move-object v3, v3

    .line 266381
    invoke-virtual {p1}, LX/1De;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 266382
    invoke-static {v0, v4}, LX/1Vl;->a$redex0(LX/1Vl;Landroid/content/Context;)Z

    move-result p2

    if-eqz p2, :cond_1

    const p2, 0x7f082a0e

    :goto_1
    invoke-virtual {v4, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    move-object v4, p2

    .line 266383
    iget-object p2, v3, LX/C2N;->a:LX/C2M;

    iput-object v4, p2, LX/C2M;->g:Ljava/lang/String;

    .line 266384
    move-object v3, v3

    .line 266385
    invoke-virtual {v3, v2}, LX/C2N;->a(LX/1Pp;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {p0, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 266386
    return-object v0

    :cond_0
    const p2, 0x7f082a0d

    goto :goto_0

    :cond_1
    const p2, 0x7f082a0f

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 266387
    invoke-static {}, LX/1dS;->b()V

    .line 266388
    const/4 v0, 0x0

    return-object v0
.end method
