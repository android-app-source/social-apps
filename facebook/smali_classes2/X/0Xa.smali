.class public LX/0Xa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0Xa;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 79130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79131
    iput-object p1, p0, LX/0Xa;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 79132
    return-void
.end method

.method public static a(LX/0QB;)LX/0Xa;
    .locals 4

    .prologue
    .line 79133
    sget-object v0, LX/0Xa;->b:LX/0Xa;

    if-nez v0, :cond_1

    .line 79134
    const-class v1, LX/0Xa;

    monitor-enter v1

    .line 79135
    :try_start_0
    sget-object v0, LX/0Xa;->b:LX/0Xa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 79136
    if-eqz v2, :cond_0

    .line 79137
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 79138
    new-instance p0, LX/0Xa;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/0Xa;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 79139
    move-object v0, p0

    .line 79140
    sput-object v0, LX/0Xa;->b:LX/0Xa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79141
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 79142
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 79143
    :cond_1
    sget-object v0, LX/0Xa;->b:LX/0Xa;

    return-object v0

    .line 79144
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 79145
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
