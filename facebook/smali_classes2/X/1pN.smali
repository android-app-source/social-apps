.class public LX/1pN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/14N;

.field public final b:I

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/Object;

.field private final e:LX/11M;

.field public final f:Z


# direct methods
.method public constructor <init>(LX/14N;ILX/0lF;LX/11M;Z)V
    .locals 7

    .prologue
    .line 329327
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v6, v0

    .line 329328
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, LX/1pN;-><init>(LX/14N;ILjava/lang/Object;LX/11M;ZLjava/util/List;)V

    .line 329329
    return-void
.end method

.method public constructor <init>(LX/14N;ILX/15w;LX/11M;Z)V
    .locals 7

    .prologue
    .line 329330
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v6, v0

    .line 329331
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, LX/1pN;-><init>(LX/14N;ILjava/lang/Object;LX/11M;ZLjava/util/List;)V

    .line 329332
    return-void
.end method

.method public constructor <init>(LX/14N;ILjava/io/InputStream;LX/11M;Z)V
    .locals 7

    .prologue
    .line 329333
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v6, v0

    .line 329334
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, LX/1pN;-><init>(LX/14N;ILjava/lang/Object;LX/11M;ZLjava/util/List;)V

    .line 329335
    return-void
.end method

.method public constructor <init>(LX/14N;ILjava/lang/Object;LX/11M;ZLjava/util/List;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/14N;",
            "I",
            "Ljava/lang/Object;",
            "LX/11M;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 329339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 329340
    iput-object p1, p0, LX/1pN;->a:LX/14N;

    .line 329341
    iput p2, p0, LX/1pN;->b:I

    .line 329342
    iput-object p3, p0, LX/1pN;->d:Ljava/lang/Object;

    .line 329343
    iput-object p4, p0, LX/1pN;->e:LX/11M;

    .line 329344
    iput-boolean p5, p0, LX/1pN;->f:Z

    .line 329345
    invoke-static {p6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/1pN;->c:LX/0Px;

    .line 329346
    return-void
.end method

.method public constructor <init>(LX/14N;ILjava/lang/String;LX/11M;Z)V
    .locals 7

    .prologue
    .line 329336
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v6, v0

    .line 329337
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, LX/1pN;-><init>(LX/14N;ILjava/lang/Object;LX/11M;ZLjava/util/List;)V

    .line 329338
    return-void
.end method


# virtual methods
.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 329321
    iget-object v0, p0, LX/1pN;->d:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/String;

    const-string v1, "No response body."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 329322
    invoke-virtual {p0}, LX/1pN;->j()V

    .line 329323
    iget-object v0, p0, LX/1pN;->d:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0lF;
    .locals 2

    .prologue
    .line 329324
    iget-object v0, p0, LX/1pN;->d:Ljava/lang/Object;

    instance-of v0, v0, LX/0lF;

    const-string v1, "No response json node."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 329325
    invoke-virtual {p0}, LX/1pN;->j()V

    .line 329326
    iget-object v0, p0, LX/1pN;->d:Ljava/lang/Object;

    check-cast v0, LX/0lF;

    return-object v0
.end method

.method public final e()LX/15w;
    .locals 2

    .prologue
    .line 329302
    iget-object v0, p0, LX/1pN;->d:Ljava/lang/Object;

    instance-of v0, v0, LX/15w;

    const-string v1, "No response json parser."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 329303
    iget-object v0, p0, LX/1pN;->d:Ljava/lang/Object;

    check-cast v0, LX/15w;

    return-object v0
.end method

.method public final f()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 329304
    iget-object v0, p0, LX/1pN;->d:Ljava/lang/Object;

    instance-of v0, v0, Ljava/io/InputStream;

    const-string v1, "No response input stream."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 329305
    iget-object v0, p0, LX/1pN;->d:Ljava/lang/Object;

    check-cast v0, Ljava/io/InputStream;

    return-object v0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 329306
    iget-object v0, p0, LX/1pN;->d:Ljava/lang/Object;

    instance-of v0, v0, LX/15w;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1pN;->d:Ljava/lang/Object;

    instance-of v0, v0, Ljava/io/InputStream;

    if-eqz v0, :cond_1

    .line 329307
    :cond_0
    iget-object v0, p0, LX/1pN;->d:Ljava/lang/Object;

    check-cast v0, Ljava/io/Closeable;

    invoke-static {v0}, LX/1pX;->a(Ljava/io/Closeable;)V

    .line 329308
    :cond_1
    return-void
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 329309
    iget-object v0, p0, LX/1pN;->d:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 329310
    iget-object v1, p0, LX/1pN;->e:LX/11M;

    iget-object v0, p0, LX/1pN;->d:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/11M;->a(Ljava/lang/String;)V

    .line 329311
    :cond_0
    :goto_0
    return-void

    .line 329312
    :cond_1
    iget-object v0, p0, LX/1pN;->d:Ljava/lang/Object;

    instance-of v0, v0, LX/0lF;

    if-eqz v0, :cond_0

    .line 329313
    iget-object v1, p0, LX/1pN;->e:LX/11M;

    iget-object v0, p0, LX/1pN;->d:Ljava/lang/Object;

    check-cast v0, LX/0lF;

    .line 329314
    if-nez v0, :cond_2

    .line 329315
    :goto_1
    goto :goto_0

    .line 329316
    :cond_2
    :try_start_0
    iget-object p0, v1, LX/11M;->b:LX/0lC;

    invoke-static {v1, v0, p0}, LX/11M;->a(LX/11M;LX/0lF;LX/0lC;)V
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 329317
    :catch_0
    goto :goto_1
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 329318
    iget-object v0, p0, LX/1pN;->a:LX/14N;

    .line 329319
    iget-object p0, v0, LX/14N;->a:Ljava/lang/String;

    move-object v0, p0

    .line 329320
    return-object v0
.end method
