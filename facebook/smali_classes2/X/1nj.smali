.class public final LX/1nj;
.super LX/1dc;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1dc",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 317338
    invoke-static {}, LX/1ni;->a()LX/1ni;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1dc;-><init>(LX/1n4;)V

    .line 317339
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 317340
    const-string v0, "ResourceDrawableReference"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 317341
    if-ne p0, p1, :cond_1

    .line 317342
    :cond_0
    :goto_0
    return v0

    .line 317343
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 317344
    goto :goto_0

    .line 317345
    :cond_3
    check-cast p1, LX/1nj;

    .line 317346
    iget v2, p0, LX/1nj;->a:I

    iget v3, p1, LX/1nj;->a:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 317347
    iget v0, p0, LX/1nj;->a:I

    return v0
.end method
