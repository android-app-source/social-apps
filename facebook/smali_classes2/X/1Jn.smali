.class public LX/1Jn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/1Db;

.field public final b:LX/1DS;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/permalink/PermalinkRootPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/1Jo;


# direct methods
.method public constructor <init>(LX/1Db;LX/0Ot;LX/1DS;LX/0Ot;LX/1Jo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Db;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/permalink/PermalinkRootPartDefinition;",
            ">;",
            "LX/1Jo;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 230536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230537
    iput-object p1, p0, LX/1Jn;->a:LX/1Db;

    .line 230538
    iput-object p2, p0, LX/1Jn;->d:LX/0Ot;

    .line 230539
    iput-object p3, p0, LX/1Jn;->b:LX/1DS;

    .line 230540
    iput-object p4, p0, LX/1Jn;->c:LX/0Ot;

    .line 230541
    iput-object p5, p0, LX/1Jn;->e:LX/1Jo;

    .line 230542
    return-void
.end method

.method public static a(LX/0QB;)LX/1Jn;
    .locals 9

    .prologue
    .line 230543
    const-class v1, LX/1Jn;

    monitor-enter v1

    .line 230544
    :try_start_0
    sget-object v0, LX/1Jn;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 230545
    sput-object v2, LX/1Jn;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 230546
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230547
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 230548
    new-instance v3, LX/1Jn;

    invoke-static {v0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v4

    check-cast v4, LX/1Db;

    const/16 v5, 0x6bd

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v6

    check-cast v6, LX/1DS;

    const/16 v7, 0x1cd3

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const-class v8, LX/1Jo;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/1Jo;

    invoke-direct/range {v3 .. v8}, LX/1Jn;-><init>(LX/1Db;LX/0Ot;LX/1DS;LX/0Ot;LX/1Jo;)V

    .line 230549
    move-object v0, v3

    .line 230550
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 230551
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Jn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 230552
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 230553
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
