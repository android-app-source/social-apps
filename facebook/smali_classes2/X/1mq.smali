.class public LX/1mq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1mr;


# static fields
.field public static a:Z

.field public static b:Z

.field private static d:Ljava/lang/Class;

.field private static e:Ljava/lang/reflect/Method;


# instance fields
.field public final c:Landroid/view/RenderNode;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 314223
    sput-boolean v0, LX/1mq;->a:Z

    .line 314224
    sput-boolean v0, LX/1mq;->b:Z

    return-void
.end method

.method public constructor <init>(Landroid/view/RenderNode;)V
    .locals 0

    .prologue
    .line 314220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 314221
    iput-object p1, p0, LX/1mq;->c:Landroid/view/RenderNode;

    .line 314222
    return-void
.end method

.method public static b()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 314215
    sget-boolean v0, LX/1mq;->a:Z

    if-nez v0, :cond_0

    sget-boolean v0, LX/1mq;->b:Z

    if-eqz v0, :cond_1

    .line 314216
    :cond_0
    :goto_0
    return-void

    .line 314217
    :cond_1
    const-string v0, "android.view.RenderNode"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 314218
    sput-object v0, LX/1mq;->d:Ljava/lang/Class;

    const-string v1, "start"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, LX/1mq;->e:Ljava/lang/reflect/Method;

    .line 314219
    sput-boolean v5, LX/1mq;->a:Z

    goto :goto_0
.end method


# virtual methods
.method public final a(II)Landroid/graphics/Canvas;
    .locals 5

    .prologue
    .line 314214
    sget-object v0, LX/1mq;->e:Ljava/lang/reflect/Method;

    iget-object v1, p0, LX/1mq;->c:Landroid/view/RenderNode;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/32F;->a(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Canvas;

    return-object v0
.end method

.method public final a(IIII)V
    .locals 2

    .prologue
    .line 314211
    iget-object v0, p0, LX/1mq;->c:Landroid/view/RenderNode;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/RenderNode;->setLeftTopRightBottom(IIII)Z

    .line 314212
    iget-object v0, p0, LX/1mq;->c:Landroid/view/RenderNode;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/RenderNode;->setClipToBounds(Z)Z

    .line 314213
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 314204
    iget-object v0, p0, LX/1mq;->c:Landroid/view/RenderNode;

    check-cast p1, Landroid/view/DisplayListCanvas;

    invoke-virtual {v0, p1}, Landroid/view/RenderNode;->end(Landroid/view/DisplayListCanvas;)V

    .line 314205
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 314210
    iget-object v0, p0, LX/1mq;->c:Landroid/view/RenderNode;

    invoke-virtual {v0}, Landroid/view/RenderNode;->isValid()Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 314206
    instance-of v0, p1, Landroid/view/DisplayListCanvas;

    if-nez v0, :cond_0

    .line 314207
    new-instance v0, LX/32E;

    new-instance v1, Ljava/lang/ClassCastException;

    invoke-direct {v1}, Ljava/lang/ClassCastException;-><init>()V

    invoke-direct {v0, v1}, LX/32E;-><init>(Ljava/lang/Exception;)V

    throw v0

    .line 314208
    :cond_0
    check-cast p1, Landroid/view/DisplayListCanvas;

    iget-object v0, p0, LX/1mq;->c:Landroid/view/RenderNode;

    invoke-virtual {p1, v0}, Landroid/view/DisplayListCanvas;->drawRenderNode(Landroid/view/RenderNode;)V

    .line 314209
    return-void
.end method
