.class public abstract LX/0Pq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z

.field public final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field public final e:I

.field public f:I

.field private volatile g:Z


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 57337
    const/4 v0, 0x0

    .line 57338
    sget-object v1, LX/0Re;->a:LX/0Re;

    move-object v1, v1

    .line 57339
    invoke-direct {p0, p1, p2, v0, v1}, LX/0Pq;-><init>(ILjava/lang/String;ZLX/0Rf;)V

    .line 57340
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;ZLX/0Rf;)V
    .locals 6
    .param p4    # LX/0Rf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Z",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57320
    const-string v5, "perf_sequence"

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/0Pq;-><init>(ILjava/lang/String;ZLX/0Rf;Ljava/lang/String;)V

    .line 57321
    return-void
.end method

.method private constructor <init>(ILjava/lang/String;ZLX/0Rf;Ljava/lang/String;)V
    .locals 1
    .param p4    # LX/0Rf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Z",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57327
    const v0, 0x7fffffff

    iput v0, p0, LX/0Pq;->f:I

    .line 57328
    iput p1, p0, LX/0Pq;->e:I

    .line 57329
    iput-object p2, p0, LX/0Pq;->a:Ljava/lang/String;

    .line 57330
    iput-boolean p3, p0, LX/0Pq;->b:Z

    .line 57331
    if-eqz p4, :cond_0

    .line 57332
    iput-object p4, p0, LX/0Pq;->c:LX/0Rf;

    .line 57333
    :goto_0
    iput-object p5, p0, LX/0Pq;->d:Ljava/lang/String;

    .line 57334
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Pq;->g:Z

    .line 57335
    return-void

    .line 57336
    :cond_0
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v0

    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/0Pq;->c:LX/0Rf;

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 57325
    iget-boolean v0, p0, LX/0Pq;->b:Z

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57324
    iget-object v0, p0, LX/0Pq;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57323
    iget-object v0, p0, LX/0Pq;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 57322
    iget v0, p0, LX/0Pq;->e:I

    return v0
.end method
