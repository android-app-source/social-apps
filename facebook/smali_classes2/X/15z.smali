.class public final enum LX/15z;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/15z;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/15z;

.field public static final enum END_ARRAY:LX/15z;

.field public static final enum END_OBJECT:LX/15z;

.field public static final enum FIELD_NAME:LX/15z;

.field public static final enum NOT_AVAILABLE:LX/15z;

.field public static final enum START_ARRAY:LX/15z;

.field public static final enum START_OBJECT:LX/15z;

.field public static final enum VALUE_EMBEDDED_OBJECT:LX/15z;

.field public static final enum VALUE_FALSE:LX/15z;

.field public static final enum VALUE_NULL:LX/15z;

.field public static final enum VALUE_NUMBER_FLOAT:LX/15z;

.field public static final enum VALUE_NUMBER_INT:LX/15z;

.field public static final enum VALUE_STRING:LX/15z;

.field public static final enum VALUE_TRUE:LX/15z;


# instance fields
.field public final _serialized:Ljava/lang/String;

.field public final _serializedBytes:[B

.field public final _serializedChars:[C


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 183224
    new-instance v0, LX/15z;

    const-string v1, "NOT_AVAILABLE"

    invoke-direct {v0, v1, v5, v4}, LX/15z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/15z;->NOT_AVAILABLE:LX/15z;

    .line 183225
    new-instance v0, LX/15z;

    const-string v1, "START_OBJECT"

    const-string v2, "{"

    invoke-direct {v0, v1, v6, v2}, LX/15z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/15z;->START_OBJECT:LX/15z;

    .line 183226
    new-instance v0, LX/15z;

    const-string v1, "END_OBJECT"

    const-string v2, "}"

    invoke-direct {v0, v1, v7, v2}, LX/15z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/15z;->END_OBJECT:LX/15z;

    .line 183227
    new-instance v0, LX/15z;

    const-string v1, "START_ARRAY"

    const-string v2, "["

    invoke-direct {v0, v1, v8, v2}, LX/15z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/15z;->START_ARRAY:LX/15z;

    .line 183228
    new-instance v0, LX/15z;

    const-string v1, "END_ARRAY"

    const/4 v2, 0x4

    const-string v3, "]"

    invoke-direct {v0, v1, v2, v3}, LX/15z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/15z;->END_ARRAY:LX/15z;

    .line 183229
    new-instance v0, LX/15z;

    const-string v1, "FIELD_NAME"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v4}, LX/15z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/15z;->FIELD_NAME:LX/15z;

    .line 183230
    new-instance v0, LX/15z;

    const-string v1, "VALUE_EMBEDDED_OBJECT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v4}, LX/15z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    .line 183231
    new-instance v0, LX/15z;

    const-string v1, "VALUE_STRING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v4}, LX/15z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/15z;->VALUE_STRING:LX/15z;

    .line 183232
    new-instance v0, LX/15z;

    const-string v1, "VALUE_NUMBER_INT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v4}, LX/15z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    .line 183233
    new-instance v0, LX/15z;

    const-string v1, "VALUE_NUMBER_FLOAT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v4}, LX/15z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/15z;->VALUE_NUMBER_FLOAT:LX/15z;

    .line 183234
    new-instance v0, LX/15z;

    const-string v1, "VALUE_TRUE"

    const/16 v2, 0xa

    const-string v3, "true"

    invoke-direct {v0, v1, v2, v3}, LX/15z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/15z;->VALUE_TRUE:LX/15z;

    .line 183235
    new-instance v0, LX/15z;

    const-string v1, "VALUE_FALSE"

    const/16 v2, 0xb

    const-string v3, "false"

    invoke-direct {v0, v1, v2, v3}, LX/15z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/15z;->VALUE_FALSE:LX/15z;

    .line 183236
    new-instance v0, LX/15z;

    const-string v1, "VALUE_NULL"

    const/16 v2, 0xc

    const-string v3, "null"

    invoke-direct {v0, v1, v2, v3}, LX/15z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/15z;->VALUE_NULL:LX/15z;

    .line 183237
    const/16 v0, 0xd

    new-array v0, v0, [LX/15z;

    sget-object v1, LX/15z;->NOT_AVAILABLE:LX/15z;

    aput-object v1, v0, v5

    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    aput-object v1, v0, v6

    sget-object v1, LX/15z;->END_OBJECT:LX/15z;

    aput-object v1, v0, v7

    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    aput-object v1, v0, v8

    const/4 v1, 0x4

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/15z;->FIELD_NAME:LX/15z;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/15z;->VALUE_STRING:LX/15z;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/15z;->VALUE_NUMBER_FLOAT:LX/15z;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/15z;->VALUE_TRUE:LX/15z;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/15z;->VALUE_FALSE:LX/15z;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/15z;->VALUE_NULL:LX/15z;

    aput-object v2, v0, v1

    sput-object v0, LX/15z;->$VALUES:[LX/15z;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 183211
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 183212
    if-nez p3, :cond_1

    .line 183213
    iput-object v0, p0, LX/15z;->_serialized:Ljava/lang/String;

    .line 183214
    iput-object v0, p0, LX/15z;->_serializedChars:[C

    .line 183215
    iput-object v0, p0, LX/15z;->_serializedBytes:[B

    .line 183216
    :cond_0
    return-void

    .line 183217
    :cond_1
    iput-object p3, p0, LX/15z;->_serialized:Ljava/lang/String;

    .line 183218
    invoke-virtual {p3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, LX/15z;->_serializedChars:[C

    .line 183219
    iget-object v0, p0, LX/15z;->_serializedChars:[C

    array-length v1, v0

    .line 183220
    new-array v0, v1, [B

    iput-object v0, p0, LX/15z;->_serializedBytes:[B

    .line 183221
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 183222
    iget-object v2, p0, LX/15z;->_serializedBytes:[B

    iget-object v3, p0, LX/15z;->_serializedChars:[C

    aget-char v3, v3, v0

    int-to-byte v3, v3

    aput-byte v3, v2, v0

    .line 183223
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/15z;
    .locals 1

    .prologue
    .line 183210
    const-class v0, LX/15z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/15z;

    return-object v0
.end method

.method public static values()[LX/15z;
    .locals 1

    .prologue
    .line 183209
    sget-object v0, LX/15z;->$VALUES:[LX/15z;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/15z;

    return-object v0
.end method


# virtual methods
.method public final asByteArray()[B
    .locals 1

    .prologue
    .line 183208
    iget-object v0, p0, LX/15z;->_serializedBytes:[B

    return-object v0
.end method

.method public final asCharArray()[C
    .locals 1

    .prologue
    .line 183207
    iget-object v0, p0, LX/15z;->_serializedChars:[C

    return-object v0
.end method

.method public final asString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183204
    iget-object v0, p0, LX/15z;->_serialized:Ljava/lang/String;

    return-object v0
.end method

.method public final isNumeric()Z
    .locals 1

    .prologue
    .line 183206
    sget-object v0, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/15z;->VALUE_NUMBER_FLOAT:LX/15z;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isScalarValue()Z
    .locals 2

    .prologue
    .line 183205
    invoke-virtual {p0}, LX/15z;->ordinal()I

    move-result v0

    sget-object v1, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    invoke-virtual {v1}, LX/15z;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
