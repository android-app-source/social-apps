.class public abstract LX/0RV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QB;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0QB;",
        "Lcom/facebook/inject/ProviderWithInjector",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public mInjector:LX/0QB;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getApplicationInjector()LX/0QA;
    .locals 1

    .prologue
    .line 60056
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v0

    return-object v0
.end method

.method public getBinders()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;",
            "Lcom/facebook/inject/Binder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60057
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getBinders()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getInjectorThreadStack()LX/0S7;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60058
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getInjectorThreadStack()LX/0S7;

    move-result-object v0

    return-object v0
.end method

.method public getInstance(LX/0RI;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TS;>;)TS;"
        }
    .end annotation

    .prologue
    .line 60059
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getInstance(LX/0RI;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getInstance(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)TS;"
        }
    .end annotation

    .prologue
    .line 60060
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)TS;"
        }
    .end annotation

    .prologue
    .line 60062
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0, p1, p2}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getLazy(LX/0RI;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TS;>;)",
            "LX/0Ot",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 60061
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getLazy(LX/0RI;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getLazy(Ljava/lang/Class;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)",
            "LX/0Ot",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 60067
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getLazy(Ljava/lang/Class;Ljava/lang/Class;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0Ot",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 60066
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0, p1, p2}, LX/0QC;->getLazy(Ljava/lang/Class;Ljava/lang/Class;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getLazySet(LX/0RI;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 60065
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getLazySet(LX/0RI;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getLazySet(Ljava/lang/Class;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<TS;>;>;"
        }
    .end annotation

    .prologue
    .line 60064
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getLazySet(Ljava/lang/Class;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getLazySet(Ljava/lang/Class;Ljava/lang/Class;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<TS;>;>;"
        }
    .end annotation

    .prologue
    .line 60063
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0, p1, p2}, LX/0QC;->getLazySet(Ljava/lang/Class;Ljava/lang/Class;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getModuleInjector(Ljava/lang/Class;)LX/0QA;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;)",
            "LX/0QA;"
        }
    .end annotation

    .prologue
    .line 60053
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QB;->getModuleInjector(Ljava/lang/Class;)LX/0QA;

    move-result-object v0

    return-object v0
.end method

.method public getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;>;)",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 60054
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    return-object v0
.end method

.method public getProcessIdentifier()I
    .locals 1

    .prologue
    .line 60039
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getProcessIdentifier()I

    move-result v0

    return v0
.end method

.method public getProvider(LX/0RI;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TS;>;)",
            "LX/0Or",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 60040
    invoke-virtual {p0}, LX/0RV;->getScopeAwareInjectorInternal()LX/0QC;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public getProvider(Ljava/lang/Class;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)",
            "LX/0Or",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 60041
    invoke-virtual {p0}, LX/0RV;->getScopeAwareInjectorInternal()LX/0QC;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getProvider(Ljava/lang/Class;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public getProvider(Ljava/lang/Class;Ljava/lang/Class;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0Or",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 60042
    invoke-virtual {p0}, LX/0RV;->getScopeAwareInjectorInternal()LX/0QC;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/0QC;->getProvider(Ljava/lang/Class;Ljava/lang/Class;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public getScopeAwareInjector()LX/0R6;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60043
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v0

    return-object v0
.end method

.method public getScopeAwareInjectorInternal()LX/0QC;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 60044
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v0

    return-object v0
.end method

.method public getScopeUnawareInjector()LX/0QD;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60045
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getScopeUnawareInjector()LX/0QD;

    move-result-object v0

    return-object v0
.end method

.method public getSet(LX/0RI;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "Ljava/util/Set",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 60046
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getSet(LX/0RI;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getSet(Ljava/lang/Class;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)",
            "Ljava/util/Set",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 60047
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getSet(Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getSet(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "Ljava/util/Set",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 60048
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0, p1, p2}, LX/0QC;->getSet(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getSetProvider(LX/0RI;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 60049
    invoke-virtual {p0}, LX/0RV;->getScopeAwareInjectorInternal()LX/0QC;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getSetProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public getSetProvider(Ljava/lang/Class;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<TS;>;>;"
        }
    .end annotation

    .prologue
    .line 60050
    invoke-virtual {p0}, LX/0RV;->getScopeAwareInjectorInternal()LX/0QC;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getSetProvider(Ljava/lang/Class;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public getSetProvider(Ljava/lang/Class;Ljava/lang/Class;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<TS;>;>;"
        }
    .end annotation

    .prologue
    .line 60051
    invoke-virtual {p0}, LX/0RV;->getScopeAwareInjectorInternal()LX/0QC;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/0QC;->getSetProvider(Ljava/lang/Class;Ljava/lang/Class;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public hasBinding(Ljava/lang/Class;Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 60052
    iget-object v0, p0, LX/0RV;->mInjector:LX/0QB;

    invoke-interface {v0, p1, p2}, LX/0QC;->hasBinding(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method
