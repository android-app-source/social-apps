.class public LX/0q3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0q3;


# instance fields
.field public a:LX/0V6;


# direct methods
.method public constructor <init>(LX/0V6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 146903
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146904
    iput-object p1, p0, LX/0q3;->a:LX/0V6;

    .line 146905
    return-void
.end method

.method public static a(LX/0QB;)LX/0q3;
    .locals 4

    .prologue
    .line 146906
    sget-object v0, LX/0q3;->b:LX/0q3;

    if-nez v0, :cond_1

    .line 146907
    const-class v1, LX/0q3;

    monitor-enter v1

    .line 146908
    :try_start_0
    sget-object v0, LX/0q3;->b:LX/0q3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 146909
    if-eqz v2, :cond_0

    .line 146910
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 146911
    new-instance p0, LX/0q3;

    invoke-static {v0}, LX/0V4;->a(LX/0QB;)LX/0V6;

    move-result-object v3

    check-cast v3, LX/0V6;

    invoke-direct {p0, v3}, LX/0q3;-><init>(LX/0V6;)V

    .line 146912
    move-object v0, p0

    .line 146913
    sput-object v0, LX/0q3;->b:LX/0q3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146914
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 146915
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 146916
    :cond_1
    sget-object v0, LX/0q3;->b:LX/0q3;

    return-object v0

    .line 146917
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 146918
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
