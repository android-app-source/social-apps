.class public LX/1Cn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1Cn;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/0Xl;


# direct methods
.method public constructor <init>(LX/0Xl;LX/0Zb;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 216895
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216896
    iput-object p1, p0, LX/1Cn;->b:LX/0Xl;

    .line 216897
    iput-object p2, p0, LX/1Cn;->a:LX/0Zb;

    .line 216898
    return-void
.end method

.method public static a(LX/0QB;)LX/1Cn;
    .locals 5

    .prologue
    .line 216882
    sget-object v0, LX/1Cn;->c:LX/1Cn;

    if-nez v0, :cond_1

    .line 216883
    const-class v1, LX/1Cn;

    monitor-enter v1

    .line 216884
    :try_start_0
    sget-object v0, LX/1Cn;->c:LX/1Cn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 216885
    if-eqz v2, :cond_0

    .line 216886
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 216887
    new-instance p0, LX/1Cn;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {p0, v3, v4}, LX/1Cn;-><init>(LX/0Xl;LX/0Zb;)V

    .line 216888
    move-object v0, p0

    .line 216889
    sput-object v0, LX/1Cn;->c:LX/1Cn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216890
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 216891
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 216892
    :cond_1
    sget-object v0, LX/1Cn;->c:LX/1Cn;

    return-object v0

    .line 216893
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 216894
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CEy;)LX/0Yb;
    .locals 9

    .prologue
    .line 216880
    new-instance v0, LX/CEx;

    move-object v1, p0

    move-object/from16 v2, p7

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    move-object v6, p4

    move-object v7, p5

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, LX/CEx;-><init>(LX/1Cn;LX/CEy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216881
    iget-object v1, p0, LX/1Cn;->b:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.STREAM_PUBLISH_COMPLETE"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/3lw;)V
    .locals 3

    .prologue
    .line 216875
    iget-object v0, p0, LX/1Cn;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v2, p1, LX/3lw;->name:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "goodwill"

    .line 216876
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 216877
    move-object v1, v1

    .line 216878
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 216879
    return-void
.end method

.method public final a(LX/CF0;Z)V
    .locals 4

    .prologue
    .line 216869
    iget-object v1, p0, LX/1Cn;->a:LX/0Zb;

    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v2, LX/3lw;->THROWBACK_CHANGED_NOTIFICATION_SUBSCRIPTION:LX/3lw;

    iget-object v2, v2, LX/3lw;->name:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "goodwill"

    .line 216870
    iput-object v2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 216871
    move-object v0, v0

    .line 216872
    const-string v2, "subscribe_source"

    iget-object v3, p1, LX/CF0;->name:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "new_subscription_status"

    if-eqz p2, :cond_0

    const-string v0, "subscribed_all"

    :goto_0
    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 216873
    return-void

    .line 216874
    :cond_0
    const-string v0, "unsubscribed"

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 216899
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/3lw;->GOODWILL_THROWBACK_SHARE_COMPOSER_OPEN:LX/3lw;

    iget-object v1, v1, LX/3lw;->name:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "goodwill"

    .line 216900
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 216901
    move-object v0, v0

    .line 216902
    const-string v1, "story_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "share_source"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 216903
    iget-object v1, p0, LX/1Cn;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 216904
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/3lw;)V
    .locals 3

    .prologue
    .line 216864
    iget-object v0, p0, LX/1Cn;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v2, p3, LX/3lw;->name:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "goodwill"

    .line 216865
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 216866
    move-object v1, v1

    .line 216867
    const-string v2, "campaign_id"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "source"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 216868
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/CEz;)V
    .locals 3

    .prologue
    .line 216858
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/3lw;->GOODWILL_THROWBACK_MESSAGE_COMPOSER_OPEN:LX/3lw;

    iget-object v1, v1, LX/3lw;->name:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "goodwill"

    .line 216859
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 216860
    move-object v0, v0

    .line 216861
    const-string v1, "campaign_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "render_style"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "share_source"

    iget-object v2, p3, LX/CEz;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 216862
    iget-object v1, p0, LX/1Cn;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 216863
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 216852
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/3lw;->GOODWILL_VIDEO_SHARE_COMPOSER_OPENED:LX/3lw;

    iget-object v1, v1, LX/3lw;->name:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "goodwill"

    .line 216853
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 216854
    move-object v0, v0

    .line 216855
    const-string v1, "campaign_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "source"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "direct_source"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "editor_type"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 216856
    iget-object v1, p0, LX/1Cn;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 216857
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 216846
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/3lw;->GOODWILL_THROWBACK_SHARE_COMPOSER_OPEN:LX/3lw;

    iget-object v1, v1, LX/3lw;->name:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "goodwill"

    .line 216847
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 216848
    move-object v0, v0

    .line 216849
    const-string v1, "campaign_id"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "story_id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "source"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "story_type"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "share_source"

    invoke-virtual {v0, v1, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "render_style"

    invoke-virtual {v0, v1, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 216850
    iget-object v1, p0, LX/1Cn;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 216851
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;LX/CEz;)V
    .locals 3

    .prologue
    .line 216840
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/3lw;->GOODWILL_THROWBACK_SHARE_MENU_OPEN:LX/3lw;

    iget-object v1, v1, LX/3lw;->name:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "goodwill"

    .line 216841
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 216842
    move-object v0, v0

    .line 216843
    const-string v1, "campaign_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "render_style"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "share_source"

    iget-object v2, p3, LX/CEz;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 216844
    iget-object v1, p0, LX/1Cn;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 216845
    return-void
.end method
