.class public final LX/0bv;
.super LX/0bi;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0bi",
        "<",
        "LX/0yI;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0bv;


# instance fields
.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0yI;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 87432
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x472

    aput v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x685

    aput v2, v0, v1

    invoke-direct {p0, p1, v0}, LX/0bi;-><init>(LX/0Ot;[I)V

    .line 87433
    iput-object p2, p0, LX/0bv;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 87434
    return-void
.end method

.method public static a(LX/0QB;)LX/0bv;
    .locals 5

    .prologue
    .line 87435
    sget-object v0, LX/0bv;->c:LX/0bv;

    if-nez v0, :cond_1

    .line 87436
    const-class v1, LX/0bv;

    monitor-enter v1

    .line 87437
    :try_start_0
    sget-object v0, LX/0bv;->c:LX/0bv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 87438
    if-eqz v2, :cond_0

    .line 87439
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 87440
    new-instance v4, LX/0bv;

    const/16 v3, 0x13f6

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v4, p0, v3}, LX/0bv;-><init>(LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 87441
    move-object v0, v4

    .line 87442
    sput-object v0, LX/0bv;->c:LX/0bv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87443
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 87444
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 87445
    :cond_1
    sget-object v0, LX/0bv;->c:LX/0bv;

    return-object v0

    .line 87446
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 87447
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Uh;ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 87448
    check-cast p3, LX/0yI;

    .line 87449
    const/16 v0, 0x472

    if-ne v0, p2, :cond_1

    .line 87450
    invoke-virtual {p3}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->j()V

    .line 87451
    :cond_0
    :goto_0
    return-void

    .line 87452
    :cond_1
    const/16 v0, 0x685

    if-ne v0, p2, :cond_0

    .line 87453
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 87454
    iget-object v1, p0, LX/0bv;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0df;->D:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0
.end method
