.class public LX/1Mw;
.super LX/0hD;
.source ""

# interfaces
.implements LX/1DD;
.implements LX/0hk;


# instance fields
.field public a:LX/0YZ;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/Bhd;

.field public final d:LX/0oz;

.field private final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public f:LX/0p3;

.field public g:LX/0p3;

.field public h:LX/0p3;

.field public i:Z

.field public j:Landroid/support/v4/app/Fragment;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0oz;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 2
    .param p1    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0oz;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 236425
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 236426
    iput-object p3, p0, LX/1Mw;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 236427
    iput-object p1, p0, LX/1Mw;->b:LX/0Ot;

    .line 236428
    iput-object p2, p0, LX/1Mw;->d:LX/0oz;

    .line 236429
    iget-object v0, p0, LX/1Mw;->d:LX/0oz;

    invoke-virtual {v0}, LX/0oz;->b()LX/0p3;

    move-result-object v0

    iput-object v0, p0, LX/1Mw;->f:LX/0p3;

    .line 236430
    iget-object v0, p0, LX/1Mw;->d:LX/0oz;

    invoke-virtual {v0}, LX/0oz;->c()LX/0p3;

    move-result-object v0

    iput-object v0, p0, LX/1Mw;->g:LX/0p3;

    .line 236431
    iget-object v0, p0, LX/1Mw;->g:LX/0p3;

    sget-object v1, LX/0p3;->UNKNOWN:LX/0p3;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/1Mw;->i:Z

    .line 236432
    new-instance v0, LX/1Mx;

    invoke-direct {v0, p0}, LX/1Mx;-><init>(LX/1Mw;)V

    iput-object v0, p0, LX/1Mw;->a:LX/0YZ;

    .line 236433
    return-void

    .line 236434
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0p3;)I
    .locals 1

    .prologue
    .line 236415
    sget-object v0, LX/0p3;->POOR:LX/0p3;

    if-ne p0, v0, :cond_0

    .line 236416
    const/high16 v0, -0x10000

    .line 236417
    :goto_0
    return v0

    .line 236418
    :cond_0
    sget-object v0, LX/0p3;->MODERATE:LX/0p3;

    if-ne p0, v0, :cond_1

    .line 236419
    const v0, -0x9a00

    goto :goto_0

    .line 236420
    :cond_1
    sget-object v0, LX/0p3;->GOOD:LX/0p3;

    if-ne p0, v0, :cond_2

    .line 236421
    const/16 v0, -0x100

    goto :goto_0

    .line 236422
    :cond_2
    sget-object v0, LX/0p3;->EXCELLENT:LX/0p3;

    if-ne p0, v0, :cond_3

    .line 236423
    const v0, -0xff0100

    goto :goto_0

    .line 236424
    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 236413
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Mw;->j:Landroid/support/v4/app/Fragment;

    .line 236414
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 236405
    iget-object v0, p0, LX/1Mw;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0eJ;->f:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236406
    iget-object v0, p0, LX/1Mw;->j:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 236407
    iget-object v1, p0, LX/1Mw;->c:LX/Bhd;

    if-nez v1, :cond_0

    .line 236408
    new-instance v1, LX/Bhd;

    invoke-direct {v1, v0, p0}, LX/Bhd;-><init>(Landroid/content/Context;LX/1Mw;)V

    iput-object v1, p0, LX/1Mw;->c:LX/Bhd;

    .line 236409
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v2, 0x64

    const/16 v3, 0x23

    const/4 v4, 0x3

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 236410
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    iget-object v3, p0, LX/1Mw;->c:LX/Bhd;

    invoke-virtual {v2, v3, v1}, Landroid/view/Window;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 236411
    iget-object v1, p0, LX/1Mw;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    sget-object v2, LX/0oz;->a:Ljava/lang/String;

    iget-object v3, p0, LX/1Mw;->a:LX/0YZ;

    invoke-interface {v1, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 236412
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 236404
    return-void
.end method
