.class public LX/1BX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile c:LX/1BX;


# instance fields
.field private final b:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 213302
    const-class v0, LX/1BX;

    sput-object v0, LX/1BX;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 213299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213300
    iput-object p1, p0, LX/1BX;->b:LX/03V;

    .line 213301
    return-void
.end method

.method public static a(LX/162;LX/0Rf;LX/0lB;)LX/162;
    .locals 7
    .param p0    # LX/162;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/162;",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0lB;",
            ")",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 213284
    :try_start_0
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 213285
    if-nez p0, :cond_0

    move-object p0, v1

    .line 213286
    :goto_0
    return-object p0

    .line 213287
    :cond_0
    invoke-virtual {p0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 213288
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 213289
    invoke-virtual {p2, v0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 213290
    new-instance v4, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 213291
    invoke-virtual {v3}, LX/0lF;->j()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 213292
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 213293
    invoke-virtual {p1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 213294
    invoke-virtual {v3, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-virtual {v4, v0, v6}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 213295
    :catch_0
    move-exception v0

    .line 213296
    sget-object v1, LX/1BX;->a:Ljava/lang/Class;

    const-string v2, "Failed to strip tracking code - %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 213297
    :cond_2
    :try_start_1
    invoke-virtual {v4}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :cond_3
    move-object p0, v1

    .line 213298
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1BX;
    .locals 4

    .prologue
    .line 213271
    sget-object v0, LX/1BX;->c:LX/1BX;

    if-nez v0, :cond_1

    .line 213272
    const-class v1, LX/1BX;

    monitor-enter v1

    .line 213273
    :try_start_0
    sget-object v0, LX/1BX;->c:LX/1BX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 213274
    if-eqz v2, :cond_0

    .line 213275
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 213276
    new-instance p0, LX/1BX;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/1BX;-><init>(LX/03V;)V

    .line 213277
    move-object v0, p0

    .line 213278
    sput-object v0, LX/1BX;->c:LX/1BX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 213279
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 213280
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 213281
    :cond_1
    sget-object v0, LX/1BX;->c:LX/1BX;

    return-object v0

    .line 213282
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 213283
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0g8;)LX/1XQ;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 213223
    invoke-static {p0}, LX/1BX;->b(LX/0g8;)LX/1Qr;

    move-result-object v0

    .line 213224
    instance-of v1, v0, LX/1UQ;

    if-eqz v1, :cond_0

    .line 213225
    check-cast v0, LX/1UQ;

    .line 213226
    iget-object v1, v0, LX/1UQ;->e:LX/1XQ;

    move-object v0, v1

    .line 213227
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(ILX/0g8;II)Landroid/view/View;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 213266
    invoke-interface {p2, p1}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v0

    .line 213267
    if-eqz v0, :cond_0

    .line 213268
    :goto_0
    return-object v0

    .line 213269
    :cond_0
    iget-object v0, p0, LX/1BX;->b:LX/03V;

    sget-object v1, LX/1BX;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid position %d (internal positions %d to %d, proxy positions %d to %d)"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-interface {p2}, LX/0g8;->q()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-interface {p2}, LX/0g8;->r()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 213270
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/view/View;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 213264
    if-nez p0, :cond_1

    .line 213265
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    if-ltz v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v1

    if-gt v1, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 213259
    invoke-static {p0}, LX/18J;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/Sponsorable;

    move-result-object v1

    .line 213260
    if-nez v1, :cond_1

    .line 213261
    :cond_0
    :goto_0
    return v0

    .line 213262
    :cond_1
    invoke-static {v1}, LX/18J;->a(Lcom/facebook/graphql/model/Sponsorable;)Lcom/facebook/graphql/model/BaseImpression;

    move-result-object v1

    .line 213263
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/BaseImpression;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(LX/0g8;)LX/1Qr;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 213258
    instance-of v0, p0, LX/0gA;

    if-eqz v0, :cond_0

    check-cast p0, LX/0gA;

    invoke-interface {p0}, LX/0gA;->a()LX/1Qr;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1Qr;LX/0g8;Landroid/view/View;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 213228
    invoke-interface {p2}, LX/0g8;->q()I

    move-result v2

    .line 213229
    invoke-interface {p2}, LX/0g8;->r()I

    move-result v4

    .line 213230
    invoke-interface {p2}, LX/0g8;->d()I

    move-result v5

    .line 213231
    invoke-interface {p2, p3}, LX/0g8;->c(Landroid/view/View;)I

    move-result v3

    .line 213232
    const/4 v6, -0x1

    if-eq v3, v6, :cond_0

    invoke-interface {p2}, LX/0g8;->B()Z

    move-result v6

    if-nez v6, :cond_1

    .line 213233
    :cond_0
    :goto_0
    return v0

    .line 213234
    :cond_1
    div-int/lit8 v6, v5, 0x2

    .line 213235
    invoke-interface {p1, v3}, LX/1Qr;->h_(I)I

    move-result v3

    .line 213236
    const/high16 v7, -0x80000000

    if-eq v3, v7, :cond_0

    .line 213237
    invoke-interface {p1, v3}, LX/1Qr;->m_(I)I

    move-result v7

    .line 213238
    invoke-interface {p1, v3}, LX/1Qr;->n_(I)I

    move-result v8

    .line 213239
    invoke-static {v7, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    sub-int/2addr v3, v2

    .line 213240
    invoke-static {v8, v4}, Ljava/lang/Math;->min(II)I

    move-result v9

    sub-int/2addr v9, v2

    .line 213241
    if-lt v7, v2, :cond_2

    if-gt v8, v4, :cond_2

    .line 213242
    invoke-direct {p0, v3, p2, v3, v9}, LX/1BX;->a(ILX/0g8;II)Landroid/view/View;

    move-result-object v2

    .line 213243
    invoke-direct {p0, v9, p2, v3, v9}, LX/1BX;->a(ILX/0g8;II)Landroid/view/View;

    move-result-object v4

    .line 213244
    if-eqz v2, :cond_0

    if-eqz v4, :cond_0

    .line 213245
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    if-ltz v2, :cond_2

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v2

    if-gt v2, v5, :cond_2

    move v0, v1

    .line 213246
    goto :goto_0

    :cond_2
    move v2, v3

    move v4, v0

    .line 213247
    :goto_1
    if-gt v2, v9, :cond_0

    .line 213248
    invoke-direct {p0, v2, p2, v3, v9}, LX/1BX;->a(ILX/0g8;II)Landroid/view/View;

    move-result-object v7

    .line 213249
    if-eqz v7, :cond_0

    .line 213250
    const/4 p3, 0x0

    .line 213251
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v8

    invoke-static {p3, v8}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 213252
    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result p1

    invoke-static {v5, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 213253
    sub-int v8, p1, v8

    invoke-static {p3, v8}, Ljava/lang/Math;->max(II)I

    move-result v8

    move v7, v8

    .line 213254
    add-int/2addr v4, v7

    .line 213255
    if-lt v4, v6, :cond_3

    move v0, v1

    .line 213256
    goto :goto_0

    .line 213257
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method
