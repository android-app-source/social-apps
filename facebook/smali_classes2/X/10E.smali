.class public final LX/10E;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Zj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zj",
            "<",
            "LX/10E;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:I

.field public c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 168532
    new-instance v0, LX/0Zj;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/0Zj;-><init>(I)V

    sput-object v0, LX/10E;->a:LX/0Zj;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 168533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 168534
    if-ne p0, p1, :cond_1

    .line 168535
    :cond_0
    :goto_0
    return v0

    .line 168536
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 168537
    goto :goto_0

    .line 168538
    :cond_3
    check-cast p1, LX/10E;

    .line 168539
    iget v2, p0, LX/10E;->b:I

    iget v3, p1, LX/10E;->b:I

    if-ne v2, v3, :cond_4

    iget v2, p0, LX/10E;->c:I

    iget v3, p1, LX/10E;->c:I

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 168540
    iget v0, p0, LX/10E;->b:I

    .line 168541
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/10E;->c:I

    add-int/2addr v0, v1

    .line 168542
    return v0
.end method
