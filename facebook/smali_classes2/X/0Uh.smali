.class public LX/0Uh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/gk/store/GatekeeperWriter;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final a:LX/0Ua;

.field public final b:LX/0UW;

.field public final c:LX/4CT;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:LX/0UY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:LX/0Ug;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/0a8;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/4CS;

.field public final h:LX/0Ui;

.field private i:Z


# direct methods
.method public constructor <init>(LX/0UW;LX/0Ua;LX/4CT;LX/0UY;LX/0Ug;)V
    .locals 2
    .param p4    # LX/0UY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/0Ug;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 66652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66653
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Uh;->i:Z

    .line 66654
    iput-object p1, p0, LX/0Uh;->b:LX/0UW;

    .line 66655
    iput-object p2, p0, LX/0Uh;->a:LX/0Ua;

    .line 66656
    iput-object p3, p0, LX/0Uh;->c:LX/4CT;

    .line 66657
    iput-object p4, p0, LX/0Uh;->d:LX/0UY;

    .line 66658
    iput-object p5, p0, LX/0Uh;->e:LX/0Ug;

    .line 66659
    new-instance v0, LX/0Ui;

    invoke-interface {p1}, LX/0UW;->a()I

    move-result v1

    invoke-direct {v0, v1}, LX/0Ui;-><init>(I)V

    iput-object v0, p0, LX/0Uh;->h:LX/0Ui;

    .line 66660
    return-void
.end method

.method public static a$redex0(LX/0Uh;[LX/03R;[LX/03R;ZZ)V
    .locals 2

    .prologue
    .line 66661
    invoke-direct {p0, p1, p2, p3, p4}, LX/0Uh;->b([LX/03R;[LX/03R;ZZ)Ljava/util/List;

    move-result-object v0

    .line 66662
    invoke-direct {p0}, LX/0Uh;->f()LX/0a8;

    move-result-object v1

    .line 66663
    if-eqz v1, :cond_0

    .line 66664
    iget-object p1, v1, LX/0a8;->b:LX/0a9;

    iget-object p2, v1, LX/0a8;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {p1, v0, p0, p2}, LX/0UI;->a(Ljava/util/Collection;Ljava/lang/Object;Ljava/util/concurrent/Executor;)V

    .line 66665
    :cond_0
    return-void
.end method

.method private declared-synchronized b([LX/03R;[LX/03R;ZZ)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "LX/03R;",
            "[",
            "LX/03R;",
            "ZZ)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 66666
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/0Uh;->h(LX/0Uh;)V

    .line 66667
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 66668
    array-length v2, p1

    :goto_0
    if-ge v0, v2, :cond_6

    .line 66669
    if-eqz p3, :cond_0

    iget-object v3, p0, LX/0Uh;->h:LX/0Ui;

    invoke-virtual {v3, v0}, LX/0Ui;->d(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 66670
    :cond_0
    iget-object v3, p0, LX/0Uh;->h:LX/0Ui;

    invoke-virtual {v3, v0}, LX/0Ui;->a(I)LX/03R;

    move-result-object v3

    .line 66671
    aget-object v4, p1, v0

    .line 66672
    if-eqz v4, :cond_1

    .line 66673
    sget-object v5, LX/03R;->UNSET:LX/03R;

    if-ne v4, v5, :cond_4

    .line 66674
    iget-object v4, p0, LX/0Uh;->h:LX/0Ui;

    invoke-virtual {v4, v0}, LX/0Ui;->e(I)V

    .line 66675
    :cond_1
    :goto_1
    aget-object v4, p2, v0

    .line 66676
    if-eqz v4, :cond_2

    .line 66677
    sget-object v5, LX/03R;->UNSET:LX/03R;

    if-ne v4, v5, :cond_5

    .line 66678
    iget-object v4, p0, LX/0Uh;->h:LX/0Ui;

    invoke-virtual {v4, v0}, LX/0Ui;->f(I)V

    .line 66679
    :cond_2
    :goto_2
    if-eqz p4, :cond_3

    iget-object v4, p0, LX/0Uh;->h:LX/0Ui;

    invoke-virtual {v4, v0}, LX/0Ui;->a(I)LX/03R;

    move-result-object v4

    if-eq v3, v4, :cond_3

    .line 66680
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66681
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66682
    :cond_4
    iget-object v5, p0, LX/0Uh;->h:LX/0Ui;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, LX/03R;->asBoolean(Z)Z

    move-result v4

    invoke-virtual {v5, v0, v4}, LX/0Ui;->a(IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 66683
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 66684
    :cond_5
    :try_start_1
    iget-object v5, p0, LX/0Uh;->h:LX/0Ui;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, LX/03R;->asBoolean(Z)Z

    move-result v4

    invoke-virtual {v5, v0, v4}, LX/0Ui;->b(IZ)V

    goto :goto_2

    .line 66685
    :cond_6
    invoke-direct {p0}, LX/0Uh;->k()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 66686
    monitor-exit p0

    return-object v1
.end method

.method public static f(LX/0Uh;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 66687
    invoke-static {p0}, LX/0Uh;->g(LX/0Uh;)LX/4CS;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/4CS;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 66688
    if-nez v0, :cond_1

    const/4 v0, -0x1

    :goto_0
    move v0, v0

    .line 66689
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 66690
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown gatekeeper: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66691
    :cond_0
    return v0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method private declared-synchronized f()LX/0a8;
    .locals 1

    .prologue
    .line 66692
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Uh;->f:LX/0a8;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized g(LX/0Uh;)LX/4CS;
    .locals 2

    .prologue
    .line 66693
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Uh;->g:LX/4CS;

    if-nez v0, :cond_0

    .line 66694
    new-instance v0, LX/4CS;

    iget-object v1, p0, LX/0Uh;->b:LX/0UW;

    invoke-direct {v0, v1}, LX/4CS;-><init>(LX/0UW;)V

    iput-object v0, p0, LX/0Uh;->g:LX/4CS;

    .line 66695
    :cond_0
    iget-object v0, p0, LX/0Uh;->g:LX/4CS;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 66696
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static h(LX/0Uh;)V
    .locals 6

    .prologue
    .line 66697
    iget-boolean v0, p0, LX/0Uh;->i:Z

    if-nez v0, :cond_4

    .line 66698
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Uh;->i:Z

    .line 66699
    iget-object v0, p0, LX/0Uh;->d:LX/0UY;

    if-eqz v0, :cond_0

    .line 66700
    iget-object v0, p0, LX/0Uh;->d:LX/0UY;

    .line 66701
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, LX/0UY;->b:J

    .line 66702
    const-string v2, "%s.load"

    iget-object v3, v0, LX/0UY;->a:Ljava/lang/String;

    const v4, 0x1bb472f5

    invoke-static {v2, v3, v4}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 66703
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/0Uh;->a:LX/0Ua;

    iget-object v1, p0, LX/0Uh;->h:LX/0Ui;

    invoke-virtual {v0, v1}, LX/0Ua;->a(LX/0Ui;)Z

    move-result v0

    .line 66704
    if-nez v0, :cond_3

    .line 66705
    iget-object v0, p0, LX/0Uh;->c:LX/4CT;

    if-eqz v0, :cond_3

    .line 66706
    iget-object v0, p0, LX/0Uh;->c:LX/4CT;

    invoke-interface {v0}, LX/4CT;->a()Ljava/util/Map;

    move-result-object v0

    .line 66707
    iget-object v1, p0, LX/0Uh;->b:LX/0UW;

    invoke-interface {v1}, LX/0UW;->b()Ljava/util/ArrayList;

    move-result-object v3

    .line 66708
    iget-object v1, p0, LX/0Uh;->b:LX/0UW;

    invoke-interface {v1}, LX/0UW;->a()I

    move-result v4

    .line 66709
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    .line 66710
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 66711
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 66712
    if-eqz v1, :cond_1

    .line 66713
    iget-object v5, p0, LX/0Uh;->h:LX/0Ui;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v5, v2, v1}, LX/0Ui;->a(IZ)V

    .line 66714
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 66715
    :cond_2
    iget-object v0, p0, LX/0Uh;->a:LX/0Ua;

    iget-object v1, p0, LX/0Uh;->h:LX/0Ui;

    invoke-virtual {v0, v1}, LX/0Ua;->b(LX/0Ui;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66716
    :cond_3
    iget-object v0, p0, LX/0Uh;->d:LX/0UY;

    if-eqz v0, :cond_4

    .line 66717
    iget-object v0, p0, LX/0Uh;->d:LX/0UY;

    invoke-virtual {v0}, LX/0UY;->d()V

    .line 66718
    :cond_4
    return-void

    .line 66719
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0Uh;->d:LX/0UY;

    if-eqz v1, :cond_5

    .line 66720
    iget-object v1, p0, LX/0Uh;->d:LX/0UY;

    invoke-virtual {v1}, LX/0UY;->d()V

    :cond_5
    throw v0
.end method

.method private k()V
    .locals 4

    .prologue
    .line 66721
    iget-object v0, p0, LX/0Uh;->d:LX/0UY;

    if-eqz v0, :cond_0

    .line 66722
    iget-object v0, p0, LX/0Uh;->d:LX/0UY;

    .line 66723
    const-string v1, "%s.save"

    iget-object v2, v0, LX/0UY;->a:Ljava/lang/String;

    const v3, 0x157aa76

    invoke-static {v1, v2, v3}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 66724
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/0Uh;->a:LX/0Ua;

    iget-object v1, p0, LX/0Uh;->h:LX/0Ui;

    invoke-virtual {v0, v1}, LX/0Ua;->b(LX/0Ui;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66725
    iget-object v0, p0, LX/0Uh;->d:LX/0UY;

    if-eqz v0, :cond_1

    .line 66726
    iget-object v0, p0, LX/0Uh;->d:LX/0UY;

    invoke-virtual {v0}, LX/0UY;->b()V

    .line 66727
    :cond_1
    return-void

    .line 66728
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0Uh;->d:LX/0UY;

    if-eqz v1, :cond_2

    .line 66729
    iget-object v1, p0, LX/0Uh;->d:LX/0UY;

    invoke-virtual {v1}, LX/0UY;->b()V

    :cond_2
    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(I)LX/03R;
    .locals 1

    .prologue
    .line 66649
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/0Uh;->h(LX/0Uh;)V

    .line 66650
    iget-object v0, p0, LX/0Uh;->h:LX/0Ui;

    invoke-virtual {v0, p1}, LX/0Ui;->a(I)LX/03R;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 66651
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)LX/03R;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 66730
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/0Uh;->f(LX/0Uh;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, LX/0Uh;->a(I)LX/03R;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Ljava/util/SortedMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66639
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    .line 66640
    iget-object v0, p0, LX/0Uh;->b:LX/0UW;

    invoke-interface {v0}, LX/0UW;->b()Ljava/util/ArrayList;

    move-result-object v3

    .line 66641
    iget-object v0, p0, LX/0Uh;->b:LX/0UW;

    invoke-interface {v0}, LX/0UW;->a()I

    move-result v4

    .line 66642
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 66643
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 66644
    invoke-virtual {p0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v5

    .line 66645
    invoke-virtual {v5}, LX/03R;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v0, v5}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66646
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 66647
    :cond_0
    monitor-exit p0

    return-object v2

    .line 66648
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0a8;)V
    .locals 1

    .prologue
    .line 66636
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/0Uh;->f:LX/0a8;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66637
    monitor-exit p0

    return-void

    .line 66638
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(IZ)Z
    .locals 1

    .prologue
    .line 66598
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/0Uh;->h(LX/0Uh;)V

    .line 66599
    iget-object v0, p0, LX/0Uh;->h:LX/0Ui;

    invoke-virtual {v0, p1}, LX/0Ui;->a(I)LX/03R;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/03R;->asBoolean(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 66600
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 66633
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/0Uh;->h(LX/0Uh;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66634
    monitor-exit p0

    return-void

    .line 66635
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 66632
    invoke-static {p0}, LX/0Uh;->g(LX/0Uh;)LX/4CS;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/4CS;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized c()V
    .locals 3

    .prologue
    .line 66621
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Uh;->h:LX/0Ui;

    .line 66622
    const/4 v1, 0x0

    iget-object v2, v0, LX/0Ui;->a:[LX/03R;

    array-length v2, v2

    :goto_0
    if-ge v1, v2, :cond_0

    .line 66623
    invoke-virtual {v0, v1}, LX/0Ui;->e(I)V

    .line 66624
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 66625
    :cond_0
    iget-object v0, p0, LX/0Uh;->h:LX/0Ui;

    .line 66626
    const/4 v1, 0x0

    iget-object v2, v0, LX/0Ui;->a:[LX/03R;

    array-length v2, v2

    :goto_1
    if-ge v1, v2, :cond_1

    .line 66627
    invoke-virtual {v0, v1}, LX/0Ui;->f(I)V

    .line 66628
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 66629
    :cond_1
    iget-object v0, p0, LX/0Uh;->a:LX/0Ua;

    iget-object v1, p0, LX/0Uh;->h:LX/0Ui;

    invoke-virtual {v0, v1}, LX/0Ua;->b(LX/0Ui;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66630
    monitor-exit p0

    return-void

    .line 66631
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 66603
    iget-object v0, p0, LX/0Uh;->e:LX/0Ug;

    if-nez v0, :cond_0

    .line 66604
    :goto_0
    return-void

    .line 66605
    :cond_0
    iget-object v0, p0, LX/0Uh;->e:LX/0Ug;

    .line 66606
    new-instance v1, LX/0Ui;

    iget-object v2, v0, LX/0Ug;->a:LX/0UW;

    invoke-interface {v2}, LX/0UW;->a()I

    move-result v2

    invoke-direct {v1, v2}, LX/0Ui;-><init>(I)V

    .line 66607
    invoke-static {v0, p1}, LX/0Ug;->b(LX/0Ug;Ljava/lang/String;)LX/0Ua;

    move-result-object v2

    .line 66608
    if-nez v2, :cond_3

    .line 66609
    :goto_1
    move-object v0, v1

    .line 66610
    monitor-enter p0

    .line 66611
    :try_start_0
    iget-object v1, p0, LX/0Uh;->h:LX/0Ui;

    const/4 v3, 0x0

    .line 66612
    iget-object v2, v0, LX/0Ui;->a:[LX/03R;

    array-length v2, v2

    iget-object v4, v1, LX/0Ui;->a:[LX/03R;

    array-length v4, v4

    if-ne v2, v4, :cond_1

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, LX/0Tp;->b(Z)V

    .line 66613
    iget-object v2, v1, LX/0Ui;->a:[LX/03R;

    array-length v2, v2

    :goto_3
    if-ge v3, v2, :cond_2

    .line 66614
    iget-object v4, v1, LX/0Ui;->a:[LX/03R;

    iget-object p1, v0, LX/0Ui;->a:[LX/03R;

    aget-object p1, p1, v3

    aput-object p1, v4, v3

    .line 66615
    iget-object v4, v1, LX/0Ui;->b:[LX/03R;

    iget-object p1, v0, LX/0Ui;->b:[LX/03R;

    aget-object p1, p1, v3

    aput-object p1, v4, v3

    .line 66616
    iget-object v4, v1, LX/0Ui;->c:[LX/03R;

    iget-object p1, v0, LX/0Ui;->c:[LX/03R;

    aget-object p1, p1, v3

    aput-object p1, v4, v3

    .line 66617
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_1
    move v2, v3

    .line 66618
    goto :goto_2

    .line 66619
    :cond_2
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 66620
    :cond_3
    invoke-virtual {v2, v1}, LX/0Ua;->a(LX/0Ui;)Z

    goto :goto_1
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 66602
    iget-object v0, p0, LX/0Uh;->h:LX/0Ui;

    invoke-virtual {v0}, LX/0Ui;->c()Z

    move-result v0

    return v0
.end method

.method public final e()LX/2LD;
    .locals 2

    .prologue
    .line 66601
    new-instance v0, LX/2LC;

    invoke-direct {v0, p0}, LX/2LC;-><init>(LX/0Uh;)V

    return-object v0
.end method
