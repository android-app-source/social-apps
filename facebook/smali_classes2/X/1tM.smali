.class public LX/1tM;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/atomic/AtomicReference",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/1tM;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 336430
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 336431
    return-void
.end method

.method public static a(LX/0QB;)LX/1tM;
    .locals 3

    .prologue
    .line 336418
    sget-object v0, LX/1tM;->a:LX/1tM;

    if-nez v0, :cond_1

    .line 336419
    const-class v1, LX/1tM;

    monitor-enter v1

    .line 336420
    :try_start_0
    sget-object v0, LX/1tM;->a:LX/1tM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 336421
    if-eqz v2, :cond_0

    .line 336422
    :try_start_1
    new-instance v0, LX/1tM;

    invoke-direct {v0}, LX/1tM;-><init>()V

    .line 336423
    move-object v0, v0

    .line 336424
    sput-object v0, LX/1tM;->a:LX/1tM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 336425
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 336426
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 336427
    :cond_1
    sget-object v0, LX/1tM;->a:LX/1tM;

    return-object v0

    .line 336428
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 336429
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
