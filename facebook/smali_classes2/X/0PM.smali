.class public final LX/0PM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation


# static fields
.field public static final a:LX/0PQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56127
    sget-object v0, LX/0PN;->a:LX/0PO;

    const-string v1, "="

    invoke-virtual {v0, v1}, LX/0PO;->withKeyValueSeparator(Ljava/lang/String;)LX/0PQ;

    move-result-object v0

    sput-object v0, LX/0PM;->a:LX/0PQ;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 56125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Iterable;LX/0QK;)LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<TV;>;",
            "LX/0QK",
            "<-TV;TK;>;)",
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 56126
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, LX/0PM;->a(Ljava/util/Iterator;LX/0QK;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Iterator;LX/0QK;)LX/0P1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator",
            "<TV;>;",
            "LX/0QK",
            "<-TV;TK;>;)",
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 56109
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56110
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 56111
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56112
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 56113
    invoke-interface {p1, v1}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 56114
    :cond_0
    :try_start_0
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 56115
    :catch_0
    move-exception v0

    .line 56116
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". To index multiple values under a key, use Multimaps.index."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static a(LX/0Rl;)LX/0Rl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Rl",
            "<-TK;>;)",
            "LX/0Rl",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;*>;>;"
        }
    .end annotation

    .prologue
    .line 56128
    sget-object v0, LX/2zy;->KEY:LX/2zy;

    move-object v0, v0

    .line 56129
    invoke-static {p0, v0}, LX/0Rj;->compose(LX/0Rl;LX/0QK;)LX/0Rl;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<*TV;>;",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 56130
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56131
    :try_start_0
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 56132
    :goto_0
    return-object v0

    .line 56133
    :catch_0
    goto :goto_0

    .line 56134
    :catch_1
    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;)Ljava/util/EnumMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Enum",
            "<TK;>;V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TK;>;)",
            "Ljava/util/EnumMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 56135
    new-instance v1, Ljava/util/EnumMap;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-direct {v1, v0}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    return-object v1
.end method

.method public static a(I)Ljava/util/HashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(I)",
            "Ljava/util/HashMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 56136
    new-instance v0, Ljava/util/HashMap;

    invoke-static {p0}, LX/0PM;->b(I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    return-object v0
.end method

.method public static a(Ljava/util/Map;)Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)",
            "Ljava/util/HashMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 56165
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public static a(Ljava/util/Iterator;)Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;)",
            "Ljava/util/Iterator",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 56137
    sget-object v0, LX/2zy;->KEY:LX/2zy;

    move-object v0, v0

    .line 56138
    invoke-static {p0, v0}, LX/0RZ;->a(Ljava/util/Iterator;LX/0QK;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Set;LX/0QK;)Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Set",
            "<TK;>;",
            "LX/0QK",
            "<-TK;TV;>;)",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 56139
    new-instance v0, LX/4zm;

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/4zm;-><init>(Ljava/util/Iterator;LX/0QK;)V

    return-object v0
.end method

.method public static a(LX/4zq;Ljava/util/Map$Entry;)Ljava/util/Map$Entry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V2:",
            "Ljava/lang/Object;",
            "K:",
            "Ljava/lang/Object;",
            "V1:",
            "Ljava/lang/Object;",
            ">(",
            "LX/4zq",
            "<-TK;-TV1;TV2;>;",
            "Ljava/util/Map$Entry",
            "<TK;TV1;>;)",
            "Ljava/util/Map$Entry",
            "<TK;TV2;>;"
        }
    .end annotation

    .prologue
    .line 56140
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56141
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56142
    new-instance v0, LX/4zk;

    invoke-direct {v0, p1, p0}, LX/4zk;-><init>(Ljava/util/Map$Entry;LX/4zq;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/GwtCompatible;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(TK;TV;)",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 56143
    new-instance v0, LX/0P4;

    invoke-direct {v0, p0, p1}, LX/0P4;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map$Entry",
            "<+TK;+TV;>;)",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 56144
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56145
    new-instance v0, LX/4zn;

    invoke-direct {v0, p0}, LX/4zn;-><init>(Ljava/util/Map$Entry;)V

    return-object v0
.end method

.method public static a(Ljava/util/Map;LX/0Rl;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<TK;TV;>;",
            "LX/0Rl",
            "<-TV;>;)",
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 56146
    instance-of v0, p0, Ljava/util/SortedMap;

    if-eqz v0, :cond_0

    .line 56147
    check-cast p0, Ljava/util/SortedMap;

    .line 56148
    invoke-static {p1}, LX/0PM;->b(LX/0Rl;)LX/0Rl;

    move-result-object v0

    invoke-static {p0, v0}, LX/0P9;->a(Ljava/util/SortedMap;LX/0Rl;)Ljava/util/SortedMap;

    move-result-object v0

    move-object v0, v0

    .line 56149
    :goto_0
    return-object v0

    .line 56150
    :cond_0
    instance-of v0, p0, LX/0Ri;

    if-eqz v0, :cond_1

    .line 56151
    check-cast p0, LX/0Ri;

    .line 56152
    invoke-static {p1}, LX/0PM;->b(LX/0Rl;)LX/0Rl;

    move-result-object v0

    invoke-static {p0, v0}, LX/0PM;->b(LX/0Ri;LX/0Rl;)LX/0Ri;

    move-result-object v0

    move-object v0, v0

    .line 56153
    goto :goto_0

    .line 56154
    :cond_1
    invoke-static {p1}, LX/0PM;->b(LX/0Rl;)LX/0Rl;

    move-result-object v0

    .line 56155
    instance-of v1, p0, Ljava/util/SortedMap;

    if-eqz v1, :cond_2

    .line 56156
    check-cast p0, Ljava/util/SortedMap;

    invoke-static {p0, v0}, LX/0P9;->a(Ljava/util/SortedMap;LX/0Rl;)Ljava/util/SortedMap;

    move-result-object v1

    .line 56157
    :goto_1
    move-object v0, v1

    .line 56158
    goto :goto_0

    .line 56159
    :cond_2
    instance-of v1, p0, LX/0Ri;

    if-eqz v1, :cond_3

    .line 56160
    check-cast p0, LX/0Ri;

    invoke-static {p0, v0}, LX/0PM;->b(LX/0Ri;LX/0Rl;)LX/0Ri;

    move-result-object v1

    goto :goto_1

    .line 56161
    :cond_3
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56162
    instance-of v1, p0, LX/300;

    if-eqz v1, :cond_4

    check-cast p0, LX/300;

    .line 56163
    new-instance v1, LX/301;

    iget-object v2, p0, LX/300;->a:Ljava/util/Map;

    iget-object p1, p0, LX/300;->b:LX/0Rl;

    invoke-static {p1, v0}, LX/0Rj;->and(LX/0Rl;LX/0Rl;)LX/0Rl;

    move-result-object p1

    invoke-direct {v1, v2, p1}, LX/301;-><init>(Ljava/util/Map;LX/0Rl;)V

    move-object v1, v1

    .line 56164
    goto :goto_1

    :cond_4
    new-instance v2, LX/301;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-direct {v2, v1, v0}, LX/301;-><init>(Ljava/util/Map;LX/0Rl;)V

    move-object v1, v2

    goto :goto_1
.end method

.method public static a(Ljava/util/Map;LX/4zq;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V1:",
            "Ljava/lang/Object;",
            "V2:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<TK;TV1;>;",
            "LX/4zq",
            "<-TK;-TV1;TV2;>;)",
            "Ljava/util/Map",
            "<TK;TV2;>;"
        }
    .end annotation

    .prologue
    .line 56117
    instance-of v0, p0, Ljava/util/SortedMap;

    if-eqz v0, :cond_0

    .line 56118
    check-cast p0, Ljava/util/SortedMap;

    invoke-static {p0, p1}, LX/0P9;->a(Ljava/util/SortedMap;LX/4zq;)Ljava/util/SortedMap;

    move-result-object v0

    .line 56119
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/501;

    invoke-direct {v0, p0, p1}, LX/501;-><init>(Ljava/util/Map;LX/4zq;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/NavigableMap;LX/0Rl;)Ljava/util/NavigableMap;
    .locals 3
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableMap"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/NavigableMap",
            "<TK;TV;>;",
            "LX/0Rl",
            "<-",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;)",
            "Ljava/util/NavigableMap",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 56120
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56121
    instance-of v0, p0, LX/4zw;

    if-eqz v0, :cond_0

    check-cast p0, LX/4zw;

    .line 56122
    iget-object v0, p0, LX/4zw;->b:LX/0Rl;

    invoke-static {v0, p1}, LX/0Rj;->and(LX/0Rl;LX/0Rl;)LX/0Rl;

    move-result-object v0

    .line 56123
    new-instance v1, LX/4zw;

    iget-object v2, p0, LX/4zw;->a:Ljava/util/NavigableMap;

    invoke-direct {v1, v2, v0}, LX/4zw;-><init>(Ljava/util/NavigableMap;LX/0Rl;)V

    move-object v0, v1

    .line 56124
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, LX/4zw;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    invoke-direct {v1, v0, p1}, LX/4zw;-><init>(Ljava/util/NavigableMap;LX/0Rl;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/util/NavigableMap;LX/4zq;)Ljava/util/NavigableMap;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableMap"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V1:",
            "Ljava/lang/Object;",
            "V2:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/NavigableMap",
            "<TK;TV1;>;",
            "LX/4zq",
            "<-TK;-TV1;TV2;>;)",
            "Ljava/util/NavigableMap",
            "<TK;TV2;>;"
        }
    .end annotation

    .prologue
    .line 56067
    new-instance v0, LX/503;

    invoke-direct {v0, p0, p1}, LX/503;-><init>(Ljava/util/NavigableMap;LX/4zq;)V

    return-object v0
.end method

.method public static a(Ljava/util/Collection;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 56068
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_0

    .line 56069
    const/4 v0, 0x0

    .line 56070
    :goto_0
    return v0

    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    invoke-static {p1}, LX/0PM;->a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static b(I)I
    .locals 2

    .prologue
    .line 56071
    const/4 v0, 0x3

    if-ge p0, v0, :cond_0

    .line 56072
    const-string v0, "expectedSize"

    invoke-static {p0, v0}, LX/0P6;->a(ILjava/lang/String;)I

    .line 56073
    add-int/lit8 v0, p0, 0x1

    .line 56074
    :goto_0
    return v0

    .line 56075
    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    if-ge p0, v0, :cond_1

    .line 56076
    int-to-float v0, p0

    const/high16 v1, 0x3f400000    # 0.75f

    div-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0

    .line 56077
    :cond_1
    const v0, 0x7fffffff

    goto :goto_0
.end method

.method public static b(LX/0Ri;LX/0Rl;)LX/0Ri;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Ri",
            "<TK;TV;>;",
            "LX/0Rl",
            "<-",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;)",
            "LX/0Ri",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 56078
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56079
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56080
    instance-of v0, p0, LX/4zs;

    if-eqz v0, :cond_0

    check-cast p0, LX/4zs;

    .line 56081
    iget-object v0, p0, LX/300;->b:LX/0Rl;

    invoke-static {v0, p1}, LX/0Rj;->and(LX/0Rl;LX/0Rl;)LX/0Rl;

    move-result-object v0

    .line 56082
    new-instance v1, LX/4zs;

    invoke-virtual {p0}, LX/4zs;->d()LX/0Ri;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/4zs;-><init>(LX/0Ri;LX/0Rl;)V

    move-object v0, v1

    .line 56083
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/4zs;

    invoke-direct {v0, p0, p1}, LX/4zs;-><init>(LX/0Ri;LX/0Rl;)V

    goto :goto_0
.end method

.method public static b(LX/0Rl;)LX/0Rl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Rl",
            "<-TV;>;)",
            "LX/0Rl",
            "<",
            "Ljava/util/Map$Entry",
            "<*TV;>;>;"
        }
    .end annotation

    .prologue
    .line 56084
    sget-object v0, LX/2zy;->VALUE:LX/2zy;

    move-object v0, v0

    .line 56085
    invoke-static {p0, v0}, LX/0Rj;->compose(LX/0Rl;LX/0QK;)LX/0Rl;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/util/Map$Entry;)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/util/Map$Entry;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map$Entry",
            "<TK;*>;)TK;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 56086
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Ljava/util/Iterator;)Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;)",
            "Ljava/util/Iterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 56087
    sget-object v0, LX/2zy;->VALUE:LX/2zy;

    move-object v0, v0

    .line 56088
    invoke-static {p0, v0}, LX/0RZ;->a(Ljava/util/Iterator;LX/0QK;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/util/Map;Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<**>;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 56089
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56090
    :try_start_0
    invoke-interface {p0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 56091
    :goto_0
    return v0

    .line 56092
    :catch_0
    goto :goto_0

    .line 56093
    :catch_1
    goto :goto_0
.end method

.method public static c(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<*TV;>;",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 56094
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56095
    :try_start_0
    invoke-interface {p0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 56096
    :goto_0
    return-object v0

    .line 56097
    :catch_0
    goto :goto_0

    .line 56098
    :catch_1
    goto :goto_0
.end method

.method public static c(Ljava/util/Map;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<**>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 56099
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0}, LX/0PN;->a(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 56100
    sget-object v1, LX/0PM;->a:LX/0PQ;

    .line 56101
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    .line 56102
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-virtual {v1, v0, p0}, LX/0PQ;->appendTo(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    .line 56103
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/HashMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 56104
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0
.end method

.method public static c(I)Ljava/util/LinkedHashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(I)",
            "Ljava/util/LinkedHashMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 56105
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-static {p0}, LX/0PM;->b(I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    return-object v0
.end method

.method public static d()Ljava/util/LinkedHashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/LinkedHashMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 56106
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    return-object v0
.end method

.method public static e()Ljava/util/concurrent/ConcurrentMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/concurrent/ConcurrentMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 56107
    new-instance v0, LX/0S8;

    invoke-direct {v0}, LX/0S8;-><init>()V

    invoke-virtual {v0}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    return-object v0
.end method

.method public static f()Ljava/util/TreeMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K::",
            "Ljava/lang/Comparable;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/TreeMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 56108
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    return-object v0
.end method
