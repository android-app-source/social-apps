.class public final LX/11W;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/11X;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;)V
    .locals 0

    .prologue
    .line 172597
    iput-object p1, p0, LX/11W;->a:Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 172598
    iget-object v0, p0, LX/11W;->a:Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;

    iget-object v0, v0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->d:LX/0v6;

    invoke-virtual {v0}, LX/0v6;->g()V

    .line 172599
    return-void
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;LX/0zO;)V
    .locals 3

    .prologue
    .line 172600
    iget-object v0, p0, LX/11W;->a:Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;

    .line 172601
    iget v1, v0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->h:I

    add-int/lit8 v2, v1, 0x1

    iput v2, v0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->h:I

    .line 172602
    iget-object v0, p0, LX/11W;->a:Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;

    iget-object v0, v0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->d:LX/0v6;

    invoke-virtual {v0, p2, p1}, LX/0v6;->a(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 172603
    return-void
.end method

.method public final a(Ljava/lang/Exception;LX/0zO;)V
    .locals 4

    .prologue
    .line 172604
    iget-object v0, p0, LX/11W;->a:Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;

    .line 172605
    iget v1, v0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->i:I

    add-int/lit8 v2, v1, 0x1

    iput v2, v0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->i:I

    .line 172606
    iget-object v0, p0, LX/11W;->a:Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;

    iget-object v0, v0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->d:LX/0v6;

    invoke-virtual {v0, p2, p1}, LX/0v6;->a(LX/0zO;Ljava/lang/Exception;)V

    .line 172607
    iget-object v0, p0, LX/11W;->a:Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;

    iget-object v0, v0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->e:LX/03V;

    const-string v1, "GraphQLBatchResultCallback.onException"

    iget-object v2, p0, LX/11W;->a:Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;

    iget-object v2, v2, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->d:LX/0v6;

    .line 172608
    iget-object v3, v2, LX/0v6;->c:Ljava/lang/String;

    move-object v2, v3

    .line 172609
    const/16 v3, 0x7d0

    invoke-virtual {v0, v1, v2, p1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 172610
    return-void
.end method
