.class public LX/0Tu;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile c:LX/0Tu;


# instance fields
.field private b:LX/0Tx;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63819
    const-class v0, LX/0Tu;

    sput-object v0, LX/0Tu;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Tx;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 63820
    const-string v0, "preferences"

    const/4 v1, 0x2

    new-instance v2, LX/0Ty;

    invoke-direct {v2}, LX/0Ty;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 63821
    iput-object p1, p0, LX/0Tu;->b:LX/0Tx;

    .line 63822
    return-void
.end method

.method public static a(LX/0QB;)LX/0Tu;
    .locals 5

    .prologue
    .line 63823
    sget-object v0, LX/0Tu;->c:LX/0Tu;

    if-nez v0, :cond_1

    .line 63824
    const-class v1, LX/0Tu;

    monitor-enter v1

    .line 63825
    :try_start_0
    sget-object v0, LX/0Tu;->c:LX/0Tu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 63826
    if-eqz v2, :cond_0

    .line 63827
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 63828
    new-instance v4, LX/0Tu;

    .line 63829
    new-instance v3, LX/0Tx;

    const/16 p0, 0x2ba

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/0Tx;-><init>(LX/0Ot;)V

    .line 63830
    move-object v3, v3

    .line 63831
    check-cast v3, LX/0Tx;

    invoke-direct {v4, v3}, LX/0Tu;-><init>(LX/0Tx;)V

    .line 63832
    move-object v0, v4

    .line 63833
    sput-object v0, LX/0Tu;->c:LX/0Tu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63834
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 63835
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 63836
    :cond_1
    sget-object v0, LX/0Tu;->c:LX/0Tu;

    return-object v0

    .line 63837
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 63838
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/0Tu;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/SortedMap;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/SortedMap",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63839
    iget-object v0, p0, LX/0Tu;->b:LX/0Tx;

    const/4 v3, 0x6

    .line 63840
    invoke-static {v0, p2, v3}, LX/0Tx;->a(LX/0Tx;Ljava/util/SortedMap;I)Ljava/util/SortedMap;

    move-result-object v1

    .line 63841
    invoke-static {v1}, LX/0Tx;->b(Ljava/util/Map;)I

    move-result v2

    .line 63842
    invoke-static {v3, v2}, LX/45Y;->a(II)V

    .line 63843
    move-object v0, v1

    .line 63844
    const-string v1, "#migrate"

    const v2, 0x13981d31

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 63845
    :try_start_0
    const-string v1, "preferences"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 63846
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 63847
    invoke-interface {v0}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 63848
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Tn;

    .line 63849
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 63850
    invoke-static {v2, v1, v0}, LX/0bf;->a(Landroid/content/ContentValues;LX/0Tn;Ljava/lang/Object;)V

    .line 63851
    const-string v0, "preferences"

    const/4 v1, 0x0

    const v4, -0x13549fa7

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x29d9cea5

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 63852
    :catchall_0
    move-exception v0

    const v1, -0x59593b4a

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_0
    const v0, -0x2e46e224

    invoke-static {v0}, LX/02m;->a(I)V

    .line 63853
    return-void
.end method

.method private d(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 63854
    const-string v0, "#maybeMigrate"

    const v1, 0x73e41a98

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 63855
    const v0, -0x25dc8208

    :try_start_0
    invoke-static {p1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 63856
    :try_start_1
    invoke-static {p1}, LX/0Tu;->e(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/SortedMap;

    move-result-object v0

    .line 63857
    invoke-static {v0}, LX/0Tx;->b(Ljava/util/Map;)I

    move-result v1

    .line 63858
    const/4 v2, 0x6

    if-eq v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 63859
    if-eqz v1, :cond_0

    .line 63860
    invoke-static {p0, p1, v0}, LX/0Tu;->a(LX/0Tu;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/SortedMap;)V

    .line 63861
    :cond_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63862
    const v0, 0x46a2f85f

    :try_start_2
    invoke-static {p1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 63863
    const v0, 0x235eca7a

    invoke-static {v0}, LX/02m;->a(I)V

    .line 63864
    return-void

    .line 63865
    :catchall_0
    move-exception v0

    const v1, 0x3e98844d

    :try_start_3
    invoke-static {p1, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 63866
    :catchall_1
    move-exception v0

    const v1, 0x26ec4580

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static e(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/SortedMap;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/SortedMap",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 63867
    invoke-static {}, LX/0PM;->f()Ljava/util/TreeMap;

    move-result-object v8

    .line 63868
    const-string v1, "preferences"

    sget-object v2, LX/0be;->a:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 63869
    :try_start_0
    invoke-static {v0, v8}, LX/0bf;->a(Landroid/database/Cursor;Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63870
    if-eqz v0, :cond_0

    .line 63871
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 63872
    :cond_0
    return-object v8

    .line 63873
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    .line 63874
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v1
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    .line 63875
    if-ge p2, v2, :cond_0

    if-gt v2, p3, :cond_0

    .line 63876
    invoke-direct {p0, p1}, LX/0Tu;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 63877
    :cond_0
    if-ge v2, p3, :cond_1

    .line 63878
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You are upgrading to %d from %d and do not have update code. Write some damn upgrade code!!!1!"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63879
    :cond_1
    return-void
.end method
