.class public LX/0WD;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0WV;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0WV;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 75621
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0WV;
    .locals 3

    .prologue
    .line 75622
    sget-object v0, LX/0WD;->a:LX/0WV;

    if-nez v0, :cond_1

    .line 75623
    const-class v1, LX/0WD;

    monitor-enter v1

    .line 75624
    :try_start_0
    sget-object v0, LX/0WD;->a:LX/0WV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 75625
    if-eqz v2, :cond_0

    .line 75626
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 75627
    invoke-static {v0}, LX/0WE;->a(LX/0QB;)Landroid/content/pm/PackageInfo;

    move-result-object p0

    check-cast p0, Landroid/content/pm/PackageInfo;

    invoke-static {p0}, LX/0WT;->a(Landroid/content/pm/PackageInfo;)LX/0WV;

    move-result-object p0

    move-object v0, p0

    .line 75628
    sput-object v0, LX/0WD;->a:LX/0WV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75629
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 75630
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 75631
    :cond_1
    sget-object v0, LX/0WD;->a:LX/0WV;

    return-object v0

    .line 75632
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 75633
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 75634
    invoke-static {p0}, LX/0WE;->a(LX/0QB;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    invoke-static {v0}, LX/0WT;->a(Landroid/content/pm/PackageInfo;)LX/0WV;

    move-result-object v0

    return-object v0
.end method
