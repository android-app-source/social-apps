.class public final LX/0cS;
.super LX/0V0;
.source ""


# instance fields
.field public final synthetic a:LX/0QA;

.field public final synthetic b:Lcom/facebook/katana/app/FacebookApplicationImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/app/FacebookApplicationImpl;LX/0QA;)V
    .locals 0

    .prologue
    .line 88610
    iput-object p1, p0, LX/0cS;->b:Lcom/facebook/katana/app/FacebookApplicationImpl;

    iput-object p2, p0, LX/0cS;->a:LX/0QA;

    invoke-direct {p0}, LX/0V0;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 88611
    const-string v0, "FacebookApplicationImpl.AppInitLock.onInitialized"

    const v1, 0x721d3fd6

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 88612
    :try_start_0
    iget-object v0, p0, LX/0cS;->b:Lcom/facebook/katana/app/FacebookApplicationImpl;

    iget-object v0, v0, Lcom/facebook/katana/app/FacebookApplicationImpl;->k:LX/0Pw;

    invoke-virtual {v0}, LX/0Pw;->getProcessName()LX/00G;

    move-result-object v0

    invoke-virtual {v0}, LX/00G;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88613
    iget-object v1, p0, LX/0cS;->b:Lcom/facebook/katana/app/FacebookApplicationImpl;

    iget-object v0, p0, LX/0cS;->a:LX/0QA;

    invoke-static {v0}, LX/0Yl;->b(LX/0QB;)LX/0Yl;

    move-result-object v0

    check-cast v0, LX/0Yl;

    .line 88614
    invoke-static {v1, v0}, Lcom/facebook/katana/app/FacebookApplicationImpl;->a$redex0(Lcom/facebook/katana/app/FacebookApplicationImpl;LX/0Yl;)V

    .line 88615
    :cond_0
    iget-object v0, p0, LX/0cS;->a:LX/0QA;

    .line 88616
    sget-object v1, LX/00p;->b:Lcom/facebook/analytics/logger/HoneyClientEvent;

    if-eqz v1, :cond_1

    .line 88617
    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    .line 88618
    sget-object v2, LX/00p;->b:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 88619
    :cond_1
    sget-object v1, LX/00p;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    sget-object v1, LX/00p;->d:Ljava/lang/Throwable;

    if-eqz v1, :cond_2

    .line 88620
    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    .line 88621
    sget-object v2, LX/00p;->c:Ljava/lang/String;

    sget-object p0, LX/00p;->d:Ljava/lang/Throwable;

    invoke-virtual {v1, v2, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88622
    :cond_2
    const v0, 0xc223fb4

    invoke-static {v0}, LX/02m;->a(I)V

    .line 88623
    return-void

    .line 88624
    :catchall_0
    move-exception v0

    const v1, -0x35596d94    # -5458230.0f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
