.class public LX/0bw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0bw;


# instance fields
.field public a:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 87455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0bw;
    .locals 5

    .prologue
    .line 87456
    sget-object v0, LX/0bw;->c:LX/0bw;

    if-nez v0, :cond_1

    .line 87457
    const-class v1, LX/0bw;

    monitor-enter v1

    .line 87458
    :try_start_0
    sget-object v0, LX/0bw;->c:LX/0bw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 87459
    if-eqz v2, :cond_0

    .line 87460
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 87461
    new-instance p0, LX/0bw;

    invoke-direct {p0}, LX/0bw;-><init>()V

    .line 87462
    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    .line 87463
    iput-object v3, p0, LX/0bw;->a:LX/0Uh;

    iput-object v4, p0, LX/0bw;->b:LX/03V;

    .line 87464
    move-object v0, p0

    .line 87465
    sput-object v0, LX/0bw;->c:LX/0bw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87466
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 87467
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 87468
    :cond_1
    sget-object v0, LX/0bw;->c:LX/0bw;

    return-object v0

    .line 87469
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 87470
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 4

    .prologue
    .line 87471
    iget-object v0, p0, LX/0bw;->a:LX/0Uh;

    const/16 v1, 0x56

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87472
    :try_start_0
    invoke-static {}, LX/Ee9;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 87473
    :cond_0
    :goto_0
    return-void

    .line 87474
    :catch_0
    move-exception v0

    .line 87475
    iget-object v1, p0, LX/0bw;->b:LX/03V;

    const-string v2, "watchdogStopFailed"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
