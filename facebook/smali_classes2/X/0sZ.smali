.class public LX/0sZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final m:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final n:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0sa;

.field public final b:LX/0se;

.field public final c:LX/0SG;

.field private d:LX/0sf;

.field public e:LX/0tE;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3HE;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0tG;

.field public final h:LX/0tI;

.field public final i:LX/0ad;

.field private final j:LX/0Uh;

.field public final k:LX/0sU;

.field public final l:LX/0sX;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 152253
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "max_comments"

    aput-object v1, v0, v2

    const-string v1, "comment_order"

    aput-object v1, v0, v3

    const-string v1, "include_comments_disabled_fields"

    aput-object v1, v0, v4

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/0sZ;->m:Ljava/util/Collection;

    .line 152254
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "max_comments"

    aput-object v1, v0, v2

    const-string v1, "comment_order"

    aput-object v1, v0, v3

    const-string v1, "include_comments_disabled_fields"

    aput-object v1, v0, v4

    const-string v1, "enable_private_reply"

    aput-object v1, v0, v5

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/0sZ;->n:Ljava/util/Collection;

    return-void
.end method

.method public constructor <init>(LX/0sa;LX/0se;LX/0SG;LX/0sf;LX/0tE;LX/0Ot;LX/0tG;LX/0tI;LX/0ad;LX/0Uh;LX/0sU;LX/0sX;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0sa;",
            "LX/0se;",
            "LX/0SG;",
            "LX/0sf;",
            "LX/0tE;",
            "LX/0Ot",
            "<",
            "LX/3HE;",
            ">;",
            "LX/0tG;",
            "LX/0tI;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0sU;",
            "LX/0sX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 152140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152141
    iput-object p1, p0, LX/0sZ;->a:LX/0sa;

    .line 152142
    iput-object p2, p0, LX/0sZ;->b:LX/0se;

    .line 152143
    iput-object p3, p0, LX/0sZ;->c:LX/0SG;

    .line 152144
    iput-object p4, p0, LX/0sZ;->d:LX/0sf;

    .line 152145
    iput-object p5, p0, LX/0sZ;->e:LX/0tE;

    .line 152146
    iput-object p6, p0, LX/0sZ;->f:LX/0Ot;

    .line 152147
    iput-object p7, p0, LX/0sZ;->g:LX/0tG;

    .line 152148
    iput-object p8, p0, LX/0sZ;->h:LX/0tI;

    .line 152149
    iput-object p9, p0, LX/0sZ;->i:LX/0ad;

    .line 152150
    iput-object p10, p0, LX/0sZ;->j:LX/0Uh;

    .line 152151
    iput-object p11, p0, LX/0sZ;->k:LX/0sU;

    .line 152152
    iput-object p12, p0, LX/0sZ;->l:LX/0sX;

    .line 152153
    return-void
.end method

.method public static a(LX/0QB;)LX/0sZ;
    .locals 1

    .prologue
    .line 152255
    invoke-static {p0}, LX/0sZ;->b(LX/0QB;)LX/0sZ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0sZ;Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;LX/0gW;)LX/0zO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;",
            "LX/0gW;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152160
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v0

    move-object v0, v0

    .line 152161
    invoke-static {p2, v0}, LX/0zO;->a(LX/0gW;LX/0w5;)LX/0zO;

    move-result-object v0

    .line 152162
    iget-boolean v1, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->g:Z

    move v1, v1

    .line 152163
    if-eqz v1, :cond_0

    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    :goto_0
    move-object v1, v2

    .line 152164
    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 152165
    new-instance v1, LX/1w4;

    invoke-direct {v1, p0}, LX/1w4;-><init>(LX/0sZ;)V

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zT;)LX/0zO;

    .line 152166
    iget-object v1, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->d:LX/0rS;

    move-object v1, v1

    .line 152167
    sget-object v2, LX/1w5;->a:[I

    invoke-virtual {v1}, LX/0rS;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 152168
    sget-object v2, LX/0zS;->c:LX/0zS;

    :goto_1
    move-object v1, v2

    .line 152169
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 152170
    const-wide/32 v2, 0x3f480

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    .line 152171
    const/4 v1, 0x1

    .line 152172
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 152173
    invoke-virtual {p0, v0}, LX/0sZ;->a(LX/0zO;)LX/1kt;

    move-result-object v1

    .line 152174
    iput-object v1, v0, LX/0zO;->g:LX/1kt;

    .line 152175
    return-object v0

    :cond_0
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0

    .line 152176
    :pswitch_0
    sget-object v2, LX/0zS;->a:LX/0zS;

    goto :goto_1

    .line 152177
    :pswitch_1
    sget-object v2, LX/0zS;->d:LX/0zS;

    goto :goto_1

    .line 152178
    :pswitch_2
    sget-object v2, LX/0zS;->b:LX/0zS;

    goto :goto_1

    .line 152179
    :pswitch_3
    sget-object v2, LX/0zS;->a:LX/0zS;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(LX/0sZ;Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;)LX/0gW;
    .locals 7

    .prologue
    .line 152180
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->c:LX/21x;

    sget-object v1, LX/21x;->COMPLETE:LX/21x;

    if-ne v0, v1, :cond_6

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 152181
    if-eqz v0, :cond_4

    .line 152182
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 152183
    new-instance v2, LX/21z;

    invoke-direct {v2}, LX/21z;-><init>()V

    move-object v4, v2

    .line 152184
    iput-boolean v0, v4, LX/0gW;->l:Z

    .line 152185
    iget-object v2, p0, LX/0sZ;->i:LX/0ad;

    sget-short v3, LX/227;->b:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 152186
    const-string v2, "enable_facepile_blingbar"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 152187
    :cond_0
    iget-boolean v2, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->f:Z

    move v2, v2

    .line 152188
    const-string v3, "include_comments_disabled_fields"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v4, v3, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 152189
    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 152190
    if-eqz v2, :cond_1

    .line 152191
    const-string v3, "feedback_id"

    invoke-virtual {v4, v3, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 152192
    :cond_1
    const-string v2, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 152193
    const-string v2, "angora_attachment_cover_image_size"

    iget-object v3, p0, LX/0sZ;->a:LX/0sa;

    invoke-virtual {v3}, LX/0sa;->s()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "angora_attachment_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 152194
    const-string v2, "reading_attachment_profile_image_width"

    iget-object v3, p0, LX/0sZ;->a:LX/0sa;

    invoke-virtual {v3}, LX/0sa;->M()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "reading_attachment_profile_image_height"

    iget-object v5, p0, LX/0sZ;->a:LX/0sa;

    invoke-virtual {v5}, LX/0sa;->N()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 152195
    const-string v2, "fetch_reshare_counts"

    iget-object v3, p0, LX/0sZ;->i:LX/0ad;

    sget-short v5, LX/0wn;->aF:S

    invoke-interface {v3, v5, v1}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 152196
    const-string v2, "automatic_photo_captioning_enabled"

    iget-object v3, p0, LX/0sZ;->l:LX/0sX;

    invoke-virtual {v3}, LX/0sX;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 152197
    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->e:LX/21y;

    move-object v2, v2

    .line 152198
    if-eqz v2, :cond_2

    .line 152199
    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->e:LX/21y;

    move-object v2, v2

    .line 152200
    sget-object v3, LX/21y;->DEFAULT_ORDER:LX/21y;

    invoke-virtual {v2, v3}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 152201
    const-string v2, "comment_order"

    .line 152202
    iget-object v3, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->e:LX/21y;

    move-object v3, v3

    .line 152203
    iget-object v3, v3, LX/21y;->toString:Ljava/lang/String;

    invoke-virtual {v4, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 152204
    :cond_2
    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->i:Ljava/lang/String;

    move-object v2, v2

    .line 152205
    if-eqz v2, :cond_7

    move v3, v0

    .line 152206
    :goto_1
    if-eqz v3, :cond_8

    .line 152207
    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->i:Ljava/lang/String;

    move-object v2, v2

    .line 152208
    :goto_2
    if-eqz v2, :cond_9

    sget-object v5, LX/21y;->RANKED_ORDER:LX/21y;

    .line 152209
    iget-object v6, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->e:LX/21y;

    move-object v6, v6

    .line 152210
    invoke-virtual {v5, v6}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 152211
    :goto_3
    if-eqz v0, :cond_a

    .line 152212
    const-string v0, "surround_comment_id"

    invoke-virtual {v4, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "num_before_surround"

    .line 152213
    iget v2, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->b:I

    move v2, v2

    .line 152214
    add-int/lit8 v2, v2, -0x1

    div-int/lit8 v2, v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "surround_max_comments"

    .line 152215
    iget v2, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->b:I

    move v2, v2

    .line 152216
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 152217
    :cond_3
    :goto_4
    const-string v0, "use_default_actor"

    .line 152218
    iget-boolean v1, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->k:Z

    move v1, v1

    .line 152219
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 152220
    if-eqz v3, :cond_b

    .line 152221
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->h:Ljava/lang/String;

    move-object v0, v0

    .line 152222
    :goto_5
    iget-object v1, p0, LX/0sZ;->e:LX/0tE;

    .line 152223
    iget-boolean v2, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->j:Z

    move v2, v2

    .line 152224
    invoke-virtual {v1, v4, v0, v2}, LX/0tE;->a(LX/0gW;Ljava/lang/String;Z)V

    .line 152225
    iget-object v0, p0, LX/0sZ;->g:LX/0tG;

    invoke-virtual {v0, v4}, LX/0tG;->a(LX/0gW;)V

    .line 152226
    iget-object v0, p0, LX/0sZ;->h:LX/0tI;

    invoke-virtual {v0, v4}, LX/0tI;->a(LX/0gW;)V

    .line 152227
    iget-object v0, p0, LX/0sZ;->k:LX/0sU;

    invoke-virtual {v0, v4}, LX/0sU;->a(LX/0gW;)V

    .line 152228
    iget-object v0, p0, LX/0sZ;->b:LX/0se;

    invoke-virtual {v0, v4}, LX/0se;->a(LX/0gW;)LX/0gW;

    move-result-object v0

    move-object v0, v0

    .line 152229
    :goto_6
    return-object v0

    .line 152230
    :cond_4
    new-instance v0, LX/5BS;

    invoke-direct {v0}, LX/5BS;-><init>()V

    move-object v0, v0

    .line 152231
    iget-object v1, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->a:Ljava/lang/String;

    move-object v1, v1

    .line 152232
    if-eqz v1, :cond_5

    .line 152233
    const-string v2, "feedback_id"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 152234
    :cond_5
    const-string v1, "use_default_actor"

    .line 152235
    iget-boolean v2, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->k:Z

    move v2, v2

    .line 152236
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 152237
    iget-object v1, p0, LX/0sZ;->e:LX/0tE;

    invoke-virtual {v1, v0}, LX/0tE;->c(LX/0gW;)V

    .line 152238
    iget-object v1, p0, LX/0sZ;->h:LX/0tI;

    invoke-virtual {v1, v0}, LX/0tI;->a(LX/0gW;)V

    .line 152239
    move-object v0, v0

    .line 152240
    goto :goto_6

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_7
    move v3, v1

    .line 152241
    goto/16 :goto_1

    .line 152242
    :cond_8
    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->h:Ljava/lang/String;

    move-object v2, v2

    .line 152243
    goto/16 :goto_2

    :cond_9
    move v0, v1

    .line 152244
    goto/16 :goto_3

    .line 152245
    :cond_a
    const-string v0, "max_comments"

    .line 152246
    iget v1, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->b:I

    move v1, v1

    .line 152247
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 152248
    sget-object v0, LX/21y;->RANKED_ORDER:LX/21y;

    .line 152249
    iget-object v1, p1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->e:LX/21y;

    move-object v1, v1

    .line 152250
    invoke-virtual {v0, v1}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 152251
    const-string v0, "comment_id"

    invoke-virtual {v4, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    goto/16 :goto_4

    .line 152252
    :cond_b
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public static b(LX/0QB;)LX/0sZ;
    .locals 13

    .prologue
    .line 152158
    new-instance v0, LX/0sZ;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v1

    check-cast v1, LX/0sa;

    invoke-static {p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v2

    check-cast v2, LX/0se;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {p0}, LX/0sf;->a(LX/0QB;)LX/0sf;

    move-result-object v4

    check-cast v4, LX/0sf;

    invoke-static {p0}, LX/0tE;->b(LX/0QB;)LX/0tE;

    move-result-object v5

    check-cast v5, LX/0tE;

    const/16 v6, 0x114

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p0}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v7

    check-cast v7, LX/0tG;

    invoke-static {p0}, LX/0tI;->a(LX/0QB;)LX/0tI;

    move-result-object v8

    check-cast v8, LX/0tI;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static {p0}, LX/0sU;->a(LX/0QB;)LX/0sU;

    move-result-object v11

    check-cast v11, LX/0sU;

    invoke-static {p0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v12

    check-cast v12, LX/0sX;

    invoke-direct/range {v0 .. v12}, LX/0sZ;-><init>(LX/0sa;LX/0se;LX/0SG;LX/0sf;LX/0tE;LX/0Ot;LX/0tG;LX/0tI;LX/0ad;LX/0Uh;LX/0sU;LX/0sX;)V

    .line 152159
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;)LX/0zO;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152157
    invoke-static {p0, p1}, LX/0sZ;->b(LX/0sZ;Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;)LX/0gW;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/0sZ;->a(LX/0sZ;Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0zO;)LX/1kt;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO;",
            ")",
            "LX/1kt",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 152156
    new-instance v0, LX/228;

    iget-object v1, p0, LX/0sZ;->d:LX/0sf;

    invoke-virtual {v1, p1}, LX/0sf;->a(LX/0zO;)LX/1ks;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/228;-><init>(LX/0sZ;LX/1ks;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/api/ufiservices/common/FetchNodeListParams;LX/0zO;)LX/1kt;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/api/ufiservices/common/FetchNodeListParams;",
            "LX/0zO",
            "<TT;>;)",
            "LX/1kt",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 152154
    iget-object v0, p0, LX/0sZ;->d:LX/0sf;

    invoke-virtual {v0, p2}, LX/0sf;->a(LX/0zO;)LX/1ks;

    move-result-object v0

    .line 152155
    new-instance v1, LX/228;

    invoke-direct {v1, p0, p1, v0}, LX/228;-><init>(LX/0sZ;Lcom/facebook/api/ufiservices/common/FetchNodeListParams;LX/1ks;)V

    return-object v1
.end method
