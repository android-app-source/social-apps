.class public final LX/1gw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0qi;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/0qg;

.field public final synthetic c:LX/0rJ;


# direct methods
.method public constructor <init>(LX/0rJ;JLX/0qg;)V
    .locals 0

    .prologue
    .line 295274
    iput-object p1, p0, LX/1gw;->c:LX/0rJ;

    iput-wide p2, p0, LX/1gw;->a:J

    iput-object p4, p0, LX/1gw;->b:LX/0qg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 295273
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 295254
    check-cast p1, LX/0qi;

    .line 295255
    monitor-enter p0

    if-nez p1, :cond_0

    .line 295256
    :goto_0
    monitor-exit p0

    return-void

    .line 295257
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/1gw;->c:LX/0rJ;

    iget-object v0, v0, LX/0rJ;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0rM;

    .line 295258
    iget-boolean v0, p1, LX/0qi;->a:Z

    move v0, v0

    .line 295259
    if-eqz v0, :cond_1

    .line 295260
    iget-object v0, p1, LX/0qi;->b:Ljava/lang/String;

    move-object v2, v0

    .line 295261
    iget-object v0, p0, LX/1gw;->c:LX/0rJ;

    iget-object v0, v0, LX/0rJ;->a:LX/0rH;

    .line 295262
    const-string v3, "slot_upgrade_request"

    invoke-static {v1, v3, v0}, LX/0rM;->a(LX/0rM;Ljava/lang/String;LX/0rH;)LX/0rM;

    move-result-object v3

    move-object v0, v3

    .line 295263
    iget-object v3, p0, LX/1gw;->c:LX/0rJ;

    iget-object v3, v3, LX/0rJ;->g:LX/0qg;

    iget-object v4, p0, LX/1gw;->c:LX/0rJ;

    iget-object v4, v4, LX/0rJ;->h:Ljava/lang/String;

    iget-object v5, p0, LX/1gw;->c:LX/0rJ;

    iget-object v5, v5, LX/0rJ;->i:LX/0rL;

    iget-object v6, p0, LX/1gw;->c:LX/0rJ;

    iget-wide v8, p0, LX/1gw;->a:J

    invoke-static {v6, v8, v9}, LX/0rJ;->e(LX/0rJ;J)LX/0rN;

    move-result-object v6

    invoke-virtual {v0, v3, v4, v5, v6}, LX/0rM;->a(LX/0qg;Ljava/lang/String;LX/0rL;LX/0rN;)LX/0rM;

    move-result-object v0

    iget-object v3, p0, LX/1gw;->c:LX/0rJ;

    iget-wide v4, v3, LX/0rJ;->j:J

    iget-object v3, p0, LX/1gw;->c:LX/0rJ;

    iget v3, v3, LX/0rJ;->m:I

    invoke-virtual {v0, v4, v5, v3}, LX/0rM;->a(JI)LX/0rM;

    move-result-object v3

    iget-object v4, p0, LX/1gw;->b:LX/0qg;

    iget-object v5, p0, LX/1gw;->c:LX/0rJ;

    iget-object v0, p0, LX/1gw;->c:LX/0rJ;

    iget-object v0, v0, LX/0rJ;->f:Ljava/util/Map;

    iget-object v6, p0, LX/1gw;->b:LX/0qg;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rK;

    invoke-static {v5, v0, v2}, LX/0rJ;->a$redex0(LX/0rJ;LX/0rK;Ljava/lang/String;)LX/0rL;

    move-result-object v0

    invoke-virtual {v3, v4, v2, v0}, LX/0rM;->a(LX/0qg;Ljava/lang/String;LX/0rL;)LX/0rM;

    .line 295264
    iget-object v0, p0, LX/1gw;->c:LX/0rJ;

    iget-wide v2, p0, LX/1gw;->a:J

    iget-object v4, p0, LX/1gw;->b:LX/0qg;

    .line 295265
    iget-object v5, p1, LX/0qi;->b:Ljava/lang/String;

    move-object v5, v5

    .line 295266
    invoke-static {v0, v2, v3, v4, v5}, LX/0rJ;->b(LX/0rJ;JLX/0qg;Ljava/lang/String;)Z

    move-result v0

    .line 295267
    :goto_1
    iget-object v2, p0, LX/1gw;->c:LX/0rJ;

    iget-object v2, v2, LX/0rJ;->g:LX/0qg;

    iget-object v3, p0, LX/1gw;->c:LX/0rJ;

    iget-object v3, v3, LX/0rJ;->h:Ljava/lang/String;

    iget-object v4, p0, LX/1gw;->c:LX/0rJ;

    iget-object v4, v4, LX/0rJ;->i:LX/0rL;

    iget-object v5, p0, LX/1gw;->c:LX/0rJ;

    iget-wide v6, p0, LX/1gw;->a:J

    invoke-static {v5, v6, v7}, LX/0rJ;->e(LX/0rJ;J)LX/0rN;

    move-result-object v5

    iget-object v6, p0, LX/1gw;->c:LX/0rJ;

    iget-wide v6, v6, LX/0rJ;->j:J

    iget-object v8, p0, LX/1gw;->c:LX/0rJ;

    iget v8, v8, LX/0rJ;->m:I

    invoke-virtual/range {v1 .. v8}, LX/0rM;->a(LX/0qg;Ljava/lang/String;LX/0rL;LX/0rN;JI)LX/0rM;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0rM;->a(Z)LX/0rM;

    move-result-object v0

    invoke-virtual {v0}, LX/0rM;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 295268
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 295269
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/1gw;->c:LX/0rJ;

    iget-object v0, v0, LX/0rJ;->a:LX/0rH;

    .line 295270
    const-string v2, "slot_release_request"

    invoke-static {v1, v2, v0}, LX/0rM;->a(LX/0rM;Ljava/lang/String;LX/0rH;)LX/0rM;

    move-result-object v2

    move-object v0, v2

    .line 295271
    iget-object v2, p0, LX/1gw;->c:LX/0rJ;

    iget-object v2, v2, LX/0rJ;->g:LX/0qg;

    iget-object v3, p0, LX/1gw;->c:LX/0rJ;

    iget-object v3, v3, LX/0rJ;->h:Ljava/lang/String;

    iget-object v4, p0, LX/1gw;->c:LX/0rJ;

    iget-object v4, v4, LX/0rJ;->i:LX/0rL;

    iget-object v5, p0, LX/1gw;->c:LX/0rJ;

    iget-wide v6, p0, LX/1gw;->a:J

    invoke-static {v5, v6, v7}, LX/0rJ;->e(LX/0rJ;J)LX/0rN;

    move-result-object v5

    invoke-virtual {v0, v2, v3, v4, v5}, LX/0rM;->a(LX/0qg;Ljava/lang/String;LX/0rL;LX/0rN;)LX/0rM;

    move-result-object v0

    iget-object v2, p0, LX/1gw;->c:LX/0rJ;

    iget-wide v2, v2, LX/0rJ;->j:J

    iget-object v4, p0, LX/1gw;->c:LX/0rJ;

    iget v4, v4, LX/0rJ;->m:I

    invoke-virtual {v0, v2, v3, v4}, LX/0rM;->a(JI)LX/0rM;

    move-result-object v0

    iget-object v2, p0, LX/1gw;->b:LX/0qg;

    const/4 v3, 0x0

    sget-object v4, LX/0rL;->NORMAL:LX/0rL;

    invoke-virtual {v0, v2, v3, v4}, LX/0rM;->a(LX/0qg;Ljava/lang/String;LX/0rL;)LX/0rM;

    .line 295272
    iget-object v0, p0, LX/1gw;->c:LX/0rJ;

    iget-wide v2, p0, LX/1gw;->a:J

    iget-object v4, p0, LX/1gw;->b:LX/0qg;

    invoke-static {v0, v2, v3, v4}, LX/0rJ;->b(LX/0rJ;JLX/0qg;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_1
.end method
