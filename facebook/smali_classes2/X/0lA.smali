.class public LX/0lA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0hV;

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/12t;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0XJ;


# direct methods
.method public constructor <init>(LX/0hV;)V
    .locals 1

    .prologue
    .line 128324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128325
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/0lA;->b:Ljava/util/Map;

    .line 128326
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/0lA;->c:Ljava/util/Map;

    .line 128327
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/0lA;->d:Ljava/util/Map;

    .line 128328
    sget-object v0, LX/0XJ;->UNKNOWN:LX/0XJ;

    iput-object v0, p0, LX/0lA;->e:LX/0XJ;

    .line 128329
    iput-object p1, p0, LX/0lA;->a:LX/0hV;

    .line 128330
    return-void
.end method

.method private a(LX/0XJ;)V
    .locals 1

    .prologue
    .line 128331
    iget-object v0, p0, LX/0lA;->e:LX/0XJ;

    if-eq v0, p1, :cond_0

    .line 128332
    iget-object v0, p0, LX/0lA;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 128333
    iget-object v0, p0, LX/0lA;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 128334
    iget-object v0, p0, LX/0lA;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 128335
    iput-object p1, p0, LX/0lA;->e:LX/0XJ;

    .line 128336
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ILX/0XJ;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 128337
    invoke-direct {p0, p2}, LX/0lA;->a(LX/0XJ;)V

    .line 128338
    iget-object v0, p0, LX/0lA;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128339
    iget-object v0, p0, LX/0lA;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 128340
    :goto_0
    return-object v0

    .line 128341
    :cond_0
    iget-object v0, p0, LX/0lA;->a:LX/0hV;

    invoke-interface {v0, p1, p2}, LX/0hV;->a(ILX/0XJ;)Ljava/lang/String;

    move-result-object v0

    .line 128342
    if-nez v0, :cond_1

    .line 128343
    new-instance v0, LX/0xp;

    const-string v1, "string"

    invoke-direct {v0, v1, p1}, LX/0xp;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 128344
    :cond_1
    iget-object v1, p0, LX/0lA;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(ILX/0XJ;LX/0W6;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 128345
    invoke-direct {p0, p2}, LX/0lA;->a(LX/0XJ;)V

    .line 128346
    iget-object v0, p0, LX/0lA;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 128347
    iget-object v0, p0, LX/0lA;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12t;

    .line 128348
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, LX/12t;->a(LX/0W6;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 128349
    :cond_1
    if-eqz v0, :cond_3

    sget-object v1, LX/0W6;->OTHER:LX/0W6;

    invoke-virtual {v0, v1}, LX/12t;->a(LX/0W6;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 128350
    sget-object v1, LX/0W6;->OTHER:LX/0W6;

    invoke-virtual {v0, v1}, LX/12t;->b(LX/0W6;)Ljava/lang/String;

    move-result-object v0

    .line 128351
    :goto_1
    return-object v0

    .line 128352
    :cond_2
    iget-object v0, p0, LX/0lA;->a:LX/0hV;

    invoke-interface {v0, p1, p2}, LX/0hV;->b(ILX/0XJ;)LX/12t;

    move-result-object v0

    .line 128353
    if-eqz v0, :cond_0

    .line 128354
    iget-object v1, p0, LX/0lA;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 128355
    :cond_3
    new-instance v0, LX/0xp;

    const-string v1, "plural"

    invoke-direct {v0, v1, p1}, LX/0xp;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 128356
    :cond_4
    invoke-virtual {v0, p3}, LX/12t;->b(LX/0W6;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final b(ILX/0XJ;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 128357
    invoke-direct {p0, p2}, LX/0lA;->a(LX/0XJ;)V

    .line 128358
    iget-object v0, p0, LX/0lA;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128359
    iget-object v0, p0, LX/0lA;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 128360
    :goto_0
    return-object v0

    .line 128361
    :cond_0
    iget-object v0, p0, LX/0lA;->a:LX/0hV;

    invoke-interface {v0, p1, p2}, LX/0hV;->c(ILX/0XJ;)[Ljava/lang/String;

    move-result-object v0

    .line 128362
    if-nez v0, :cond_1

    .line 128363
    new-instance v0, LX/0xp;

    const-string v1, "string array"

    invoke-direct {v0, v1, p1}, LX/0xp;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 128364
    :cond_1
    iget-object v1, p0, LX/0lA;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
