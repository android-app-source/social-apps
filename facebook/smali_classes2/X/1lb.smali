.class public LX/1lb;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:[B

.field private static final c:I

.field public static final d:[B

.field private static final e:I

.field public static final f:[B

.field public static final g:[B

.field public static final h:[B

.field private static final i:I


# instance fields
.field public final a:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 312114
    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    .line 312115
    sput-object v0, LX/1lb;->b:[B

    array-length v0, v0

    sput v0, LX/1lb;->c:I

    .line 312116
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    .line 312117
    sput-object v0, LX/1lb;->d:[B

    array-length v0, v0

    sput v0, LX/1lb;->e:I

    .line 312118
    const-string v0, "GIF87a"

    invoke-static {v0}, LX/1lc;->a(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, LX/1lb;->f:[B

    .line 312119
    const-string v0, "GIF89a"

    invoke-static {v0}, LX/1lc;->a(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, LX/1lb;->g:[B

    .line 312120
    const-string v0, "BM"

    invoke-static {v0}, LX/1lc;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 312121
    sput-object v0, LX/1lb;->h:[B

    array-length v0, v0

    sput v0, LX/1lb;->i:I

    return-void

    nop

    :array_0
    .array-data 1
        -0x1t
        -0x28t
        -0x1t
    .end array-data

    .line 312122
    :array_1
    .array-data 1
        -0x77t
        0x50t
        0x4et
        0x47t
        0xdt
        0xat
        0x1at
        0xat
    .end array-data
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 312172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 312173
    new-array v0, v3, [I

    const/4 v1, 0x0

    const/16 v2, 0x15

    aput v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x14

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, LX/1lb;->c:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, LX/1lb;->e:I

    aput v2, v0, v1

    const/4 v1, 0x4

    aput v3, v0, v1

    const/4 v1, 0x5

    sget v2, LX/1lb;->i:I

    aput v2, v0, v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 312174
    array-length v1, v0

    if-lez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, LX/03g;->a(Z)V

    .line 312175
    aget v1, v0, v3

    .line 312176
    :goto_1
    array-length v3, v0

    if-ge v2, v3, :cond_2

    .line 312177
    aget v3, v0, v2

    if-le v3, v1, :cond_0

    .line 312178
    aget v1, v0, v2

    .line 312179
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move v1, v3

    .line 312180
    goto :goto_0

    .line 312181
    :cond_2
    move v0, v1

    .line 312182
    iput v0, p0, LX/1lb;->a:I

    return-void
.end method


# virtual methods
.method public final a([BI)LX/1lW;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 312123
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 312124
    const/4 v0, 0x0

    invoke-static {p1, v0, p2}, LX/1cG;->b([BII)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312125
    const/4 v1, 0x0

    .line 312126
    invoke-static {p1, v1, p2}, LX/1cG;->b([BII)Z

    move-result v0

    invoke-static {v0}, LX/03g;->a(Z)V

    .line 312127
    add-int/lit8 v0, v1, 0xc

    sget-object v2, LX/1cG;->i:[B

    invoke-static {p1, v0, v2}, LX/1cG;->a([BI[B)Z

    move-result v0

    move v0, v0

    .line 312128
    if-eqz v0, :cond_6

    .line 312129
    sget-object v0, LX/1ld;->e:LX/1lW;

    .line 312130
    :goto_0
    move-object v0, v0

    .line 312131
    :goto_1
    return-object v0

    .line 312132
    :cond_0
    sget-object v0, LX/1lb;->b:[B

    array-length v0, v0

    if-lt p2, v0, :cond_10

    sget-object v0, LX/1lb;->b:[B

    invoke-static {p1, v0}, LX/1lc;->a([B[B)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 312133
    if-eqz v0, :cond_1

    .line 312134
    sget-object v0, LX/1ld;->a:LX/1lW;

    goto :goto_1

    .line 312135
    :cond_1
    sget-object v0, LX/1lb;->d:[B

    array-length v0, v0

    if-lt p2, v0, :cond_11

    sget-object v0, LX/1lb;->d:[B

    invoke-static {p1, v0}, LX/1lc;->a([B[B)Z

    move-result v0

    if-eqz v0, :cond_11

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 312136
    if-eqz v0, :cond_2

    .line 312137
    sget-object v0, LX/1ld;->b:LX/1lW;

    goto :goto_1

    .line 312138
    :cond_2
    const/4 v0, 0x0

    .line 312139
    const/4 v1, 0x6

    if-ge p2, v1, :cond_12

    .line 312140
    :cond_3
    :goto_4
    move v0, v0

    .line 312141
    if-eqz v0, :cond_4

    .line 312142
    sget-object v0, LX/1ld;->c:LX/1lW;

    goto :goto_1

    .line 312143
    :cond_4
    sget-object v0, LX/1lb;->h:[B

    array-length v0, v0

    if-ge p2, v0, :cond_14

    .line 312144
    const/4 v0, 0x0

    .line 312145
    :goto_5
    move v0, v0

    .line 312146
    if-eqz v0, :cond_5

    .line 312147
    sget-object v0, LX/1ld;->d:LX/1lW;

    goto :goto_1

    .line 312148
    :cond_5
    sget-object v0, LX/1lW;->a:LX/1lW;

    goto :goto_1

    .line 312149
    :cond_6
    add-int/lit8 v0, v1, 0xc

    sget-object v2, LX/1cG;->j:[B

    invoke-static {p1, v0, v2}, LX/1cG;->a([BI[B)Z

    move-result v0

    move v0, v0

    .line 312150
    if-eqz v0, :cond_7

    .line 312151
    sget-object v0, LX/1ld;->f:LX/1lW;

    goto :goto_0

    .line 312152
    :cond_7
    const/16 v0, 0x15

    if-lt p2, v0, :cond_b

    add-int/lit8 v0, v1, 0xc

    sget-object v2, LX/1cG;->k:[B

    invoke-static {p1, v0, v2}, LX/1cG;->a([BI[B)Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    :goto_6
    move v0, v0

    .line 312153
    if-eqz v0, :cond_a

    .line 312154
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 312155
    add-int/lit8 v3, v1, 0xc

    sget-object p0, LX/1cG;->k:[B

    invoke-static {p1, v3, p0}, LX/1cG;->a([BI[B)Z

    move-result p0

    .line 312156
    add-int/lit8 v3, v1, 0x14

    aget-byte v3, p1, v3

    and-int/lit8 v3, v3, 0x2

    const/4 p2, 0x2

    if-ne v3, p2, :cond_c

    move v3, v0

    .line 312157
    :goto_7
    if-eqz p0, :cond_d

    if-eqz v3, :cond_d

    :goto_8
    move v0, v0

    .line 312158
    if-eqz v0, :cond_8

    .line 312159
    sget-object v0, LX/1ld;->i:LX/1lW;

    goto :goto_0

    .line 312160
    :cond_8
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 312161
    add-int/lit8 v3, v1, 0xc

    sget-object p0, LX/1cG;->k:[B

    invoke-static {p1, v3, p0}, LX/1cG;->a([BI[B)Z

    move-result p0

    .line 312162
    add-int/lit8 v3, v1, 0x14

    aget-byte v3, p1, v3

    and-int/lit8 v3, v3, 0x10

    const/16 p2, 0x10

    if-ne v3, p2, :cond_e

    move v3, v0

    .line 312163
    :goto_9
    if-eqz p0, :cond_f

    if-eqz v3, :cond_f

    :goto_a
    move v0, v0

    .line 312164
    if-eqz v0, :cond_9

    .line 312165
    sget-object v0, LX/1ld;->h:LX/1lW;

    goto/16 :goto_0

    .line 312166
    :cond_9
    sget-object v0, LX/1ld;->g:LX/1lW;

    goto/16 :goto_0

    .line 312167
    :cond_a
    sget-object v0, LX/1lW;->a:LX/1lW;

    goto/16 :goto_0

    :cond_b
    const/4 v0, 0x0

    goto :goto_6

    :cond_c
    move v3, v2

    .line 312168
    goto :goto_7

    :cond_d
    move v0, v2

    .line 312169
    goto :goto_8

    :cond_e
    move v3, v2

    .line 312170
    goto :goto_9

    :cond_f
    move v0, v2

    .line 312171
    goto :goto_a

    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_12
    sget-object v1, LX/1lb;->f:[B

    invoke-static {p1, v1}, LX/1lc;->a([B[B)Z

    move-result v1

    if-nez v1, :cond_13

    sget-object v1, LX/1lb;->g:[B

    invoke-static {p1, v1}, LX/1lc;->a([B[B)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_13
    const/4 v0, 0x1

    goto/16 :goto_4

    :cond_14
    sget-object v0, LX/1lb;->h:[B

    invoke-static {p1, v0}, LX/1lc;->a([B[B)Z

    move-result v0

    goto/16 :goto_5
.end method
