.class public LX/1sN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1sN;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0aU;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0aU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 334702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 334703
    iput-object p1, p0, LX/1sN;->a:Landroid/content/Context;

    .line 334704
    iput-object p2, p0, LX/1sN;->b:LX/0aU;

    .line 334705
    return-void
.end method

.method public static a(LX/0QB;)LX/1sN;
    .locals 5

    .prologue
    .line 334706
    sget-object v0, LX/1sN;->c:LX/1sN;

    if-nez v0, :cond_1

    .line 334707
    const-class v1, LX/1sN;

    monitor-enter v1

    .line 334708
    :try_start_0
    sget-object v0, LX/1sN;->c:LX/1sN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 334709
    if-eqz v2, :cond_0

    .line 334710
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 334711
    new-instance p0, LX/1sN;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v4

    check-cast v4, LX/0aU;

    invoke-direct {p0, v3, v4}, LX/1sN;-><init>(Landroid/content/Context;LX/0aU;)V

    .line 334712
    move-object v0, p0

    .line 334713
    sput-object v0, LX/1sN;->c:LX/1sN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 334714
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 334715
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 334716
    :cond_1
    sget-object v0, LX/1sN;->c:LX/1sN;

    return-object v0

    .line 334717
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 334718
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1sN;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 334719
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 334720
    iget-object v1, p0, LX/1sN;->b:LX/0aU;

    invoke-virtual {v1, p1}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 334721
    iget-object v1, p0, LX/1sN;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 334722
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 1

    .prologue
    .line 334723
    const-string v0, "FOREGROUND_LOCATION_CHECK_FAILED"

    invoke-static {p0, v0}, LX/1sN;->a(LX/1sN;Ljava/lang/String;)V

    .line 334724
    return-void
.end method
