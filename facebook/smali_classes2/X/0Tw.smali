.class public abstract LX/0Tw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 63908
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63909
    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Version must be positive"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 63910
    iput-object p1, p0, LX/0Tw;->a:Ljava/lang/String;

    .line 63911
    iput p2, p0, LX/0Tw;->b:I

    .line 63912
    return-void

    .line 63913
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Landroid/database/sqlite/SQLiteDatabase;)V
.end method

.method public abstract a(Landroid/database/sqlite/SQLiteDatabase;II)V
.end method

.method public a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 63914
    return-void
.end method

.method public abstract b(Landroid/database/sqlite/SQLiteDatabase;)V
.end method

.method public c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 63915
    return-void
.end method
